package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.cms.shared.TempData;
import com.nirvasoft.cms.shared.WalletTransData;
import com.nirvasoft.cms.shared.WalletTransDataRequest;
import com.nirvasoft.cms.shared.WalletTransDataResponse;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.rp.dao.UserReportDao;
import com.nirvasoft.rp.data.FeatureData;
import com.nirvasoft.rp.shared.UserDetailReportData;
import com.nirvasoft.rp.shared.UserReportData;
import com.nirvasoft.rp.util.Geneal;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.GeneralUtility;
import com.sun.jersey.multipart.FormDataBodyPart;

public class ReportDao {
	public void getCustomerID(String userid, String branchCode, String tempTable, Connection conn) throws SQLException {

		String cif = "", sql = "";
		ArrayList<String> datalist = new ArrayList<String>();
		String whereClause = "";
		if (!branchCode.trim().equals("")) {
			whereClause += "and CustomerID Like ? ";
		}
		sql = "select * from ucjunction where userid =? " + whereClause;
		// System.out.println("getcustomerid:::::"+sql);
		PreparedStatement stmt = conn.prepareStatement(sql);
		int j = 1;
		stmt.setString(j++, userid);
		if (!branchCode.trim().equals("")) {
			stmt.setString(j++, branchCode + "%");
		}

		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			datalist.add(res.getString("CustomerID"));
		}

		for (int i = 0; i < datalist.size(); i++) {
			if (i == datalist.size() - 1) {
				cif += "" + datalist.get(i) + "";

			} else {
				cif += "" + datalist.get(i) + "" + " , ";
			}
		}
		sql = "update " + tempTable + " set CustomerID=? where userid=?";
		// System.out.println("update tempTable :::::::"+sql);
		PreparedStatement updateStmt = conn.prepareStatement(sql);
		int i = 1;
		updateStmt.setString(i++, cif);
		updateStmt.setString(i++, userid);
		updateStmt.executeUpdate();
	}

	public WalletTransDataResponse getReport(WalletTransDataRequest request, Connection conn) throws SQLException {
		WalletTransDataResponse response = null;

		String fromAccount = request.getFromAccount();
		String toAccount = request.getToAccount();
		int currentPage = request.getCurrentPage();
		int pageSize = request.getPageSize();
		String fromDate = request.getFromDate();
		String toDate = request.getToDate();

		String whereclause = "";
		int i = 2;

		if (fromAccount != null && fromAccount.trim().length() != 0) {
			whereclause = " AND FromAccount = ?";
			i++;
		}

		if (toAccount != null && toAccount.trim().length() != 0) {
			whereclause = whereclause.concat(" AND ToAccount = ?");
			i++;
		}

		int startPage = (currentPage - 1) * pageSize;
		int endPage = pageSize + startPage;
		int startPageIndex = i + 1;
		int endPageIndex = i + 2;

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			String query = "SELECT * FROM (" + "SELECT ROW_NUMBER() OVER (ORDER BY TransDate) AS RowNum, TransDate,"
					+ "FromAccount, ToAccount, Amount  FROM WalletTransaction"
					+ " WHERE (TransDate <= ? AND TransDate >= ?)" + whereclause
					+ ") AS RowConstrainedResult WHERE (RowNum > ? AND RowNum <= ?);";
			preparedStatement = conn.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			preparedStatement.setString(1, toDate);
			preparedStatement.setString(2, fromDate);
			System.out.println("AccountTransferReport-->" + query);

			int x = i;

			if (toAccount != null && toAccount.trim().length() != 0) {
				preparedStatement.setString(x--, toAccount);
			}

			if (fromAccount != null && fromAccount.trim().length() != 0) {
				preparedStatement.setString(x, fromAccount);
			}

			preparedStatement.setInt(startPageIndex, startPage);
			preparedStatement.setInt(endPageIndex, endPage);
			resultSet = preparedStatement.executeQuery();

			resultSet.last();
			int totalRow = resultSet.getRow();

			if (totalRow == 0) {
				response = new WalletTransDataResponse();
				response.setMsgCode("0000");
				response.setMsgDesc("Data Not Found.");
				response.setPageSize(pageSize);
				response.setCurrentPage(currentPage);
				response.setWldata(null);
				response.setTotalCount(0);
			}

			if (totalRow != 0) {
				WalletTransData[] dataarr = new WalletTransData[totalRow];
				resultSet.beforeFirst();

				double totalAmount = 0.00;
				double totalBankCharges = 0.00;
				int j = 0;

				while (resultSet.next()) {
					WalletTransData data = new WalletTransData();
					data.setTransDate(resultSet.getString("TransDate"));
					data.setFromAccount(resultSet.getString("FromAccount"));
					data.setToAccount(resultSet.getString("ToAccount"));
					double Amount = resultSet.getDouble("Amount");
					data.setAmount(ServerUtil.formatNumber(Amount));
					totalAmount += Amount;

					double BankCharges = resultSet.getDouble("BankCharges");
					data.setBankCharges(ServerUtil.formatNumber(BankCharges));
					totalBankCharges += BankCharges;
					dataarr[j++] = data;
				}
				preparedStatement.close();
				resultSet.close();
				preparedStatement = null;
				resultSet = null;

				query = "SELECT COUNT(*) AS total FROM WalletTransaction WHERE " + "(TransDate <= ? AND TransDate >= ?)"
						+ whereclause + ";";
				preparedStatement = conn.prepareStatement(query);
				preparedStatement.setString(1, toDate);
				preparedStatement.setString(2, fromDate);

				int y = i;

				if (toAccount != null && toAccount.trim().length() != 0) {
					preparedStatement.setString(y--, toAccount);
				}

				if (fromAccount != null && fromAccount.trim().length() != 0) {
					preparedStatement.setString(y, fromAccount);
				}

				resultSet = preparedStatement.executeQuery();
				resultSet.next();

				response = new WalletTransDataResponse();
				response.setMsgCode("0000");
				response.setMsgDesc("Select Successfully.");
				response.setPageSize(pageSize);
				response.setCurrentPage(currentPage);
				response.setWldata(dataarr);
				response.setTotalCount(resultSet.getInt("total"));

				if (totalAmount != 0.0) {
					response.setTotalAmount(ServerUtil.formatNumber(totalAmount));
				}

				if (totalBankCharges != 0.0) {
					response.setTotalBankCharges(ServerUtil.formatNumber(totalBankCharges));
				}
			}
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}

			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}

		return response;
	}

	public ArrayList<WalletTransData> getReportData(WalletTransDataRequest request, Connection conn)
			throws SQLException {
		ArrayList<WalletTransData> datalist = new ArrayList<WalletTransData>();

		String fromAccount = request.getFromAccount();
		String toAccount = request.getToAccount();
		String fromDate = request.getFromDate();
		String toDate = request.getToDate();
		String fromName = request.getT1();
		String toName = request.getT2();
		String transtype = request.getTranstype();

		String whereclause = "";
		int i = 0;

		if (!request.isAlldate()) {
			whereclause += " and TransDate >= '" + fromDate + "' and TransDate <= '" + toDate + "'";
		}
		if (fromAccount != null && fromAccount.trim().length() != 0) {// 1
			whereclause += " And FromAccount = ?";
			i++;
		}
		if (toAccount != null && toAccount.trim().length() != 0) {
			whereclause = whereclause.concat(" And ToAccount =?");
			i++;
		}
		if (fromName != null && fromName.trim().length() != 0) {// 2
			whereclause = whereclause.concat(" And t1 like '%" + fromName + "%'");
		}

		if (toName != null && toName.trim().length() != 0) {// 3
			whereclause = whereclause.concat(" And t2 like '%" + toName + "%'");
		}
		if (transtype != null && transtype.trim().length() != 0) {
			whereclause = whereclause.concat(" And transtype='" + transtype + "'");
		}

		PreparedStatement ps = null;
		ResultSet resultSet = null;
		String sql = "";
		String TransDate = "";

		sql = "SELECT createddate,TransID,FromAccount,ToAccount,Amount,Bankcharges,T1,T2,CurrencyCode FROM WalletTransaction"
				+ " where n1=0" + whereclause;
		ps = conn.prepareStatement(sql);

		// ps.setString(1,toDate);
		// ps.setString(2,fromDate);
		System.out.println("AccountTransferReport-->" + sql);
		int x = i;
		if (toAccount != null && toAccount.trim().length() != 0) {
			ps.setString(x--, toAccount);
		}
		if (fromAccount != null && fromAccount.trim().length() != 0) {
			ps.setString(x, fromAccount);
		}
		resultSet = ps.executeQuery();
		// long srno = l_startRecord + 1;
		while (resultSet.next()) {
			WalletTransData data = new WalletTransData();
			data.setT1(resultSet.getString("T1"));
			data.setT2(resultSet.getString("T2"));
			data.setCurrencyCode(resultSet.getString("currencycode"));
			TransDate = resultSet.getString("createddate");
			data.setTransDate(
					TransDate.substring(0, 4) + "/" + TransDate.substring(5, 7) + "/" + TransDate.substring(8));
			data.setTransID(resultSet.getString("TransID"));
			data.setFromAccount(resultSet.getString("FromAccount"));
			data.setToAccount(resultSet.getString("ToAccount"));
			double Amount = resultSet.getDouble("Amount");
			data.setAmount(ServerUtil.formatNumber(Amount));

			double BankCharges = resultSet.getDouble("BankCharges");
			if (BankCharges == 0.0) {
				data.setBankCharges("0.00");
			} else {
				data.setBankCharges(ServerUtil.formatNumber(BankCharges));
			}
			datalist.add(data);
		}
		return datalist;

	}

//	public ArrayList<UserDetailReportData> getUserReportList1(UserReportData p,String download, Connection conn) throws SQLException {
	public UserReportData getUserReportList1(UserReportData p,String download, Connection conn) throws SQLException {
		
		int l_startRecord = (p.getCurrentPage() - 1) * p.getPageSize();
		int l_endRecord = l_startRecord + p.getPageSize();
		String aFromDate = p.getaFromDate();
		String aToDate = p.getaToDate();
		String userName = p.getUsername();
		String nrc = p.getNrc();
		String phoneno = p.getPhoneno();
		String usertype = p.getUsertype();
		String status = p.getStatus();
		UserReportData userdata=new UserReportData();
		ArrayList<UserDetailReportData> datalist = new ArrayList<UserDetailReportData>();
		// if (click == 0) {
		String aCriteria = "";
		if (usertype.equals("1")) {
			if (!p.isAlldate()) {
				if(aFromDate.equals(aToDate)){
					aFromDate= new GeneralUtil().formatDDMMYYYY2YYYYMMDD(aFromDate);							
					aCriteria += " and u.createddate like '" + aFromDate + "%' ";
				}else{
					//aCriteria += " and u.createddate >= '%" + aFromDate + "' and u.createddate <= '" + aToDate + "%' ";
					aCriteria += " and u.createddate >= '" + aFromDate + "' and u.createddate <= '" + aToDate + "' ";
					
				}				
			}
			if (!userName.trim().equals("")) {
				aCriteria += "and p.t2 =? ";
			}
			if (!nrc.trim().equals("")) {
				aCriteria += "and u.t7 = ? ";
			}
			if (!phoneno.trim().equals("")) {
				aCriteria += "and u.t4 = ? ";
			}
			if (!status.trim().equalsIgnoreCase("ALL")) {
				if (status.trim().equalsIgnoreCase("Save")) {
					aCriteria += " and u.RecordStatus = '1'";// MM
				} else if (status.trim().equalsIgnoreCase("Activate")) {
					aCriteria += " and u.RecordStatus = '2' and u.n7 != 11";// MM
				} else if (status.trim().equalsIgnoreCase("Deactivate")) {
					aCriteria += " and u.RecordStatus = '21'";// MM
				} else if (status.trim().equalsIgnoreCase("Delete")) {
					aCriteria += " and u.RecordStatus = '4'";// MM
				} else if (status.trim().equalsIgnoreCase("Lock")) {
					aCriteria += " and u.n7 = 11 and u.RecordStatus <> 4";// MM
				} else {
					aCriteria += " and u.n7 = 0 ";// MM
				}
			}
		} else {
			if (!p.isAlldate()) {
				if(aFromDate.equals(aToDate)){
					aFromDate= new GeneralUtil().formatDDMMYYYY2YYYYMMDD(aFromDate);							
					aCriteria += " and createddate like '" + aFromDate + "%' ";
				}else{
					aCriteria += " and createddate >= '" + aFromDate + "' and createddate <= '" + aToDate + "' ";
				}				
			}
			if (!p.isAlldate()) {
				if (!userName.trim().equals("")) {
					aCriteria += "and username =? ";
				}
				if (!nrc.trim().equals("")) {
					aCriteria += "and t21 = ? ";
				}
				if (!phoneno.trim().equals("")) {
					aCriteria += "and userid = ? ";
				}
			} else {
				if (!userName.trim().equals("")) {
//					aCriteria += "where username =? ";
					aCriteria += "and username =? ";//ndh
					if (!nrc.trim().equals("")) {
						aCriteria += "and t21 = ? ";
						if (!phoneno.trim().equals("")) {
							aCriteria += "and userid = ? ";
						}
					} else {
						if (!phoneno.trim().equals("")) {
							aCriteria += "and userid = ? ";
						}
					}
				} else {
					if (!nrc.trim().equals("")) {
						//aCriteria += "where t21 = ? ";
						aCriteria += "and t21 = ? ";
						if (!phoneno.trim().equals("")) {
							aCriteria += "and userid = ? ";
						}
					} else {
						if (!phoneno.trim().equals("")) {
//							aCriteria += "where userid = ? ";
							aCriteria += "and userid = ? ";//ndh
							if (!status.trim().equalsIgnoreCase("ALL")) {
								if (status.trim().equalsIgnoreCase("Save")) {
									aCriteria += " and RecordStatus = '1'";// MM
								} else if (status.trim().equalsIgnoreCase("Activate")) {
									aCriteria += " and RecordStatus = '2' and t10 != 'L01'";// MM
								} else if (status.trim().equalsIgnoreCase("Deactivate")) {
									aCriteria += " and RecordStatus = '21'";// MM
								} else if (status.trim().equalsIgnoreCase("Delete")) {
									aCriteria += " and RecordStatus = '4'";// MM
								} else if (status.trim().equalsIgnoreCase("Lock")) {
									aCriteria += " and t10 = 'L01' and RecordStatus <> 4";// MM
								} else {
									aCriteria += " and t10 = 'L02' ";// MM
								}
							}
						} else {
							if (!status.trim().equalsIgnoreCase("ALL")) {
								if (status.trim().equalsIgnoreCase("Save")) {
									aCriteria += " where RecordStatus = '1'";// MM
								} else if (status.trim().equalsIgnoreCase("Activate")) {
									aCriteria += " where RecordStatus = '2' and n1 =0";// MM
								} else if (status.trim().equalsIgnoreCase("Deactivate")) {
									aCriteria += " where RecordStatus = '21'";// MM
								} else if (status.trim().equalsIgnoreCase("Delete")) {
									aCriteria += " where RecordStatus = '4'";// MM
								} else if (status.trim().equalsIgnoreCase("Lock")) {
									aCriteria += " where n1=11 and RecordStatus <> 4";// MM
								} else {
									aCriteria += " where t10 = 'L02' ";// MM
								}
							}

						}
					}
				}
			}

		}

		// aCriteria += " and u.n2 = ? ";// BranchUser

		String l_Query = "";
		UserReportDao dao = new UserReportDao();
		if (usertype.equals("1")) {
			if(download.equals("0")){
			l_Query = "SELECT * from (SELECT ROW_NUMBER() OVER (ORDER BY u.createdDate desc) AS RowNum, u.t1 as UserID, p.t2 AS UserName,u.t7 as NRC,u.t4 as PhoneNo, u.createddate,u.modifieddate,u.RecordStatus,u.n7 as lockstatus FROM  UVM005_A u, UVM012_A p "
					+ " where u.t1 = p.t1 and u.n4 = p.syskey " + aCriteria+ ") as r WHERE 1=1 and RowNum >"+ l_startRecord +" And RowNum <="+ l_endRecord;
			
	}
			else{
			l_Query = "SELECT u.t1 as UserID, p.t2 AS UserName,u.t7 as NRC,u.t4 as PhoneNo, u.createddate,u.modifieddate,u.RecordStatus,u.n7 as lockstatus"
					+ " FROM  UVM005_A u, UVM012_A p where u.t1 = p.t1 and u.n4 = p.syskey " + aCriteria;
			}
		} else {
			if(download.equals("0")){
				l_Query = "SELECT * from (SELECT ROW_NUMBER() OVER (ORDER BY createdDate desc) AS RowNum, UserID,UserName,t21 as NRC,userid as PhoneNo,createddate,modifieddate,RecordStatus,n1 FROM register  WHERE 1=1 "
						+ aCriteria+ ") as r  WHERE 1=1" + " and RowNum >  " + l_startRecord
						+ "  and RowNum <= " + l_endRecord;
				}
			else{
				l_Query = "select UserID,UserName,t21 as NRC,userid as PhoneNo,createddate,modifieddate,RecordStatus,n1 from register where 1=1 "		
			+ aCriteria;
			
			}
			
//			l_Query = "select UserID,UserName,t21 as NRC,userid as PhoneNo,createddate,modifieddate,RecordStatus,n1 from register where 1=1 "
//					+ aCriteria;
		}
		PreparedStatement stmt = conn.prepareStatement(l_Query);
		int i = 1;
		if (!userName.trim().equals("")) {
			stmt.setString(i++, userName.trim());
		}
		if (!nrc.trim().equals("")) {
			stmt.setString(i++, nrc.trim());
		}
		if (!phoneno.trim().equals("")) {
			stmt.setString(i++, phoneno.trim());
		}
		// stmt.setString(i++, usertype);
		ResultSet resultSet = stmt.executeQuery();
		int srno = l_startRecord + 1;
		while (resultSet.next()) {
			UserDetailReportData data = new UserDetailReportData();
			String aCreatedDate = "", aModifiedDate = "";
			if (usertype.equals("1")) {
				data.setUserID(resultSet.getString("UserID"));
				data.setUsername(resultSet.getString("UserName"));
				data.setNrc(resultSet.getString("NRC"));
				data.setSyskey(srno++);
				data.setPhoneno(resultSet.getString("PhoneNo"));
				aCreatedDate = resultSet.getString("createddate");
				data.setCreateddate(aCreatedDate.substring(6) + "/" + aCreatedDate.substring(4, 6) + "/"
						+ aCreatedDate.substring(0, 4));
				aModifiedDate = resultSet.getString("modifieddate");
				data.setModifieddate(aModifiedDate.substring(6) + "/" + aModifiedDate.substring(4, 6) + "/"
						+ aModifiedDate.substring(0, 4));
				// data.setSyskey(resultSet.getLong("Autokey"));
				// data.setCreatedby(resultSet.getString("createdby"));
				// data.setModifiedby(resultSet.getString("ModifiedBy"));
				data.setN7(resultSet.getInt("lockstatus"));
				data.setRecordStatus(resultSet.getInt("RecordStatus"));
				if (data.getRecordStatus() != 4) {
					data.setUserstatus("save");
				}
				if (data.getRecordStatus() == 4) {
					data.setUserstatus("delete");
				}
			} else {
				data.setUserID(resultSet.getString("UserID"));
				data.setUsername(resultSet.getString("UserName"));
				data.setNrc(resultSet.getString("NRC"));
				data.setPhoneno(resultSet.getString("PhoneNo"));
				data.setSyskey(srno++);
				// data.setPhoneno(resultSet.getString("PhoneNo").replace("+959",
				// "09"));
				aCreatedDate = resultSet.getString("createddate").replace("-", "");
				aCreatedDate = aCreatedDate.substring(0, 8);
				data.setCreateddate(aCreatedDate.substring(6) + "/" + aCreatedDate.substring(4, 6) + "/"
						+ aCreatedDate.substring(0, 4));
				aModifiedDate = resultSet.getString("modifieddate").replace("-", "");
				aModifiedDate = aCreatedDate.substring(0, 8);
				data.setModifieddate(aModifiedDate.substring(6) + "/" + aModifiedDate.substring(4, 6) + "/"
						+ aModifiedDate.substring(0, 4));
				data.setN1(resultSet.getInt("n1"));
				data.setRecordStatus(resultSet.getInt("RecordStatus"));
				if (data.getRecordStatus() == 1) {
					data.setUserstatus("save");
				}
				if (data.getRecordStatus() == 2 && data.getN1() == 0) {
					data.setUserstatus("activate");
				}
				if (data.getRecordStatus() == 21) {
					data.setUserstatus("deactivate");
				}
				if (data.getRecordStatus() == 4) {
					data.setUserstatus("delete");
				}
				if (data.getN1() == 11) {
					data.setUserstatus("lock");
				}
			}

			datalist.add(data);
		}
			String countSQL = "";
			if (usertype.equals("1")) {
				countSQL = "select COUNT(*) as recCount from ( SELECT  u.* FROM   UVM005_A u, UVM012_A p where u.t1 = p.t1 and u.n4 = p.syskey " + aCriteria + ") As r";
			}
			else{
			countSQL = "select COUNT(*) as recCount from ( SELECT * FROM register "+ "WHERE 1=1"
					 + aCriteria+")As r ";
			}
			PreparedStatement ps1 = conn.prepareStatement(countSQL);
			int h = 1;
			if (!userName.trim().equals("")) {
				ps1.setString(h++, userName.trim());
			}
			if (!nrc.trim().equals("")) {
				ps1.setString(h++, nrc.trim());
			}
			if (!phoneno.trim().equals("")) {
				ps1.setString(h++, phoneno.trim());
			}
			
			ResultSet result = ps1.executeQuery();
			result.next();
			userdata.setTotalCount(result.getInt("recCount"));
			
			UserDetailReportData[] dataarry = new UserDetailReportData[datalist.size()];
			for (int j = 0; j< datalist.size(); j++) {
				dataarry[j] = datalist.get(j);
			}
			userdata.setUsdata(dataarry);
			userdata.setPageSize(p.getPageSize());
			userdata.setCurrentPage(p.getCurrentPage());
		
		return userdata;

	}
	public WalletTransDataResponse getWalletTopupList(WalletTransDataRequest request,String download, Connection conn)
			throws SQLException {
		int l_startRecord = (request.getCurrentPage() - 1) * request.getPageSize();
		int l_endRecord = l_startRecord + request.getPageSize();
		ArrayList<WalletTransData> datalist = new ArrayList<WalletTransData>();
		WalletTransDataResponse list  = new WalletTransDataResponse();

		String fromAccount = request.getFromAccount();
		String toAccount = request.getToAccount();
		String fromDate = request.getFromDate();
		String toDate = request.getToDate();
		String fromName = request.getT1();
		String toName = request.getT2();
		String transtype = request.getTranstype();

		String whereclause = "";
		int i = 0;		
		if (fromAccount != null && fromAccount.trim().length() != 0) {// 1
			whereclause += " And FromAccount = ?";
			i++;
		}
		if (toAccount != null && toAccount.trim().length() != 0) {
			whereclause = whereclause.concat(" And ToAccount =?");
			i++;
		}
		if (fromName != null && fromName.trim().length() != 0) {// 2
			whereclause = whereclause.concat(" And t1 like ?");
			i++;
		}

		if (toName != null && toName.trim().length() != 0) {// 3
			whereclause = whereclause.concat(" And t2 like ?");
			i++;
		}

		if (transtype != null && transtype.trim().length() != 0) {
			if(!transtype.equals("All")){
				whereclause = whereclause.concat(" And transtype=?");
				i++;
			}
		}
		
		if (!request.isAlldate()) {
			whereclause += " and TransDate >= ? and TransDate <= ?";
		}

		PreparedStatement ps = null;
		ResultSet resultSet = null;
		String sql = "";
		String TransDate = "";
		int fromDateIndex = i + 1;
		int toDateIndex = i + 2;
	if(download.equals("0")){
			sql = "SELECT * from (SELECT ROW_NUMBER() OVER (ORDER BY createdDate desc) AS RowNum,TransType,refkey,TransDate,TransID,FromAccount,ToAccount,Amount,Bankcharges,T1,T2,CommissionCharges FROM WalletTransaction"
					+ " where n1=0" + whereclause+ ") as r  WHERE 1=1" + " and RowNum >  " + l_startRecord
					+ "  and RowNum <= " + l_endRecord;
	}
	else{
	sql = "SELECT TransType,Transid,refkey,TransDate,TransID,FromAccount,ToAccount,Amount,Bankcharges,T1,T2,CommissionCharges FROM WalletTransaction"
				+ " where n1=0" + whereclause;//CurrencyCode
		}
		
		ps = conn.prepareStatement(sql);

		// ps.setString(1,toDate);
		// ps.setString(2,fromDate);
		System.out.println("AccountTransferReport-->" + sql);
		int x = i;
		
		if(transtype != null && transtype.trim().length() != 0 ){
			if(!transtype.equals("All")){
			ps.setString(x--, transtype);
			}
		}
		if(toName != null && toName.trim().length() != 0){
			ps.setString(x--, "%" +toName +"%");
		}
		if(fromName != null && fromName.trim().length() != 0){
			ps.setString(x--, "%" +fromName +"%");
		}
		if (toAccount != null && toAccount.trim().length() != 0) {
			ps.setString(x--, toAccount);
		}
		if (fromAccount != null && fromAccount.trim().length() != 0) {
			ps.setString(x--, fromAccount);
		}		
		if (!request.isAlldate()) {
			ps.setString(fromDateIndex, fromDate);
			ps.setString(toDateIndex, toDate);
		}
		resultSet = ps.executeQuery();
		int srno = l_startRecord + 1;
		while (resultSet.next()) {
			WalletTransData data = new WalletTransData();
			data.setT1(resultSet.getString("T1"));
			data.setT2(resultSet.getString("T2"));
			//TransDate = resultSet.getString("TransDate");
			//data.setTransDate(
					//TransDate.substring(0, 4) + "/" + TransDate.substring(5, 7) + "/" + TransDate.substring(8));
			data.setAutokey(srno++);
			data.setTransDate(GeneralUtility.formatMIT2DDMMYYYY(resultSet.getString("TransDate")));
			data.setTransType(resultSet.getString("TransType"));
			data.setTransID(resultSet.getString("TransID"));
			data.setMbankingkey(resultSet.getString("refkey"));
			data.setFromAccount(resultSet.getString("FromAccount"));
			data.setToAccount(resultSet.getString("ToAccount"));
			//data.setCommissionCharges(resultSet.getString("CommissionCharges"));
			double Amount = resultSet.getDouble("Amount");
			double Commission=resultSet.getDouble("CommissionCharges");
			
			if(Amount ==0.0 || Amount == 0.00){
				data.setAmount("0.00");
			}
			if(Commission ==0.0 || Commission == 0.00){
				data.setCommissionCharges("0.00");
			}
			data.setAmount(String.format("%,.2f", Amount));	
			data.setCommissionCharges(String.format("%,.2f", Commission));	
			double BankCharges = resultSet.getDouble("BankCharges");
			if (BankCharges == 0.0 || BankCharges == 0.00) {
				data.setBankCharges("0.00");
			} else {
				data.setBankCharges(String.format("%,.2f", BankCharges));
			}
			datalist.add(data);
		}
		String countSQL = "";
		countSQL = "select COUNT(*) as recCount from ( SELECT * FROM WalletTransaction "+  " where n1=0"
				 + whereclause+")As r ";
		
		PreparedStatement ps1 = conn.prepareStatement(countSQL);
		int h = i;
		if(transtype != null && transtype.trim().length() != 0 ){
			if(!transtype.equals("All")){
			ps1.setString(h--, transtype);
			}
		}
		if(toName != null && toName.trim().length() != 0){
			ps1.setString(h--, "%" +toName +"%");
		}
		if(fromName != null && fromName.trim().length() != 0){
			ps1.setString(h--, "%" +fromName +"%");
		}
		if (toAccount != null && toAccount.trim().length() != 0) {
			ps1.setString(h--, toAccount);
		}
		if (fromAccount != null && fromAccount.trim().length() != 0) {
			ps1.setString(h--, fromAccount);
		}		
		if (!request.isAlldate()) {
			ps1.setString(fromDateIndex, fromDate);
			ps1.setString(toDateIndex, toDate);
		}
		ResultSet result = ps1.executeQuery();
		result.next();
		list.setTotalCount(result.getInt("recCount"));
		//list.setTotalCount(list.getInt("recCount"));
		//list.setTotalCount(datalist.size());
		WalletTransData[] dataarry = new WalletTransData[datalist.size()];
		for (int j = 0; j< datalist.size(); j++) {
			dataarry[j] = datalist.get(j);
		}
		list.setWldata(dataarry);
		list.setPageSize(request.getPageSize());
		list.setCurrentPage(request.getCurrentPage());

		return list;

	}
	/*public ArrayList<WalletTransData> getSettlementList(String fromDate, ArrayList<FeatureData> fdata, Connection conn1,Connection conn2)
			throws SQLException {
		ArrayList<WalletTransData> datalist = new ArrayList<WalletTransData>();		
		String whereclause = "";	
		String dbName = conn1.getCatalog();
		if (fromDate != null && fromDate.trim().length() != 0) {// 1
			whereclause += " And EffectiveDate = ?";
		}	
		PreparedStatement ps = null;
		for(int j=0; j<fdata.size(); j++){
			//Create temp table
			String tmpTableName = "Tmp" + fdata.get(j).getAccountNumber();
			String sql1 = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[" + tmpTableName
					+ "]') AND type in (N'U')) " + "	DROP TABLE " + tmpTableName;
			ps = conn1.prepareStatement(sql1);
			ps.executeUpdate();
			//Insert temp table
			String sql2 = "Select Transref,Amount,CurrencyCode into " + dbName +".."+ tmpTableName 
					+" FROM AccountTransaction Where  1<>0 "
					+ " And AccNumber=? " + whereclause;//AND TransType < 500
			ps = conn2.prepareStatement(sql2);
			System.out.println("AccountTransferReport-->" + sql2);	
			ps.setString(1, fdata.get(j).getAccountNumber());
			ps.setString(2, fromDate);
			ps.executeUpdate();
			//sum amount,commissioncharges 
			String sql3 = "Select SUM(CASE WHEN TransType>500 THEN A.Amount*-1 ELSE A.Amount END) Settlement,"
					+ "SUM(CommissionCharges) CommissionCharges,B.CurrencyCode "
					+ "FROM WalletTransaction A INNER JOIN " + tmpTableName +" B ON A.TransID=B.TransRef GROUP BY B.CurrencyCode";
			ps = conn1.prepareStatement(sql3);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				WalletTransData data = new WalletTransData();			
				data.setBankCharges(rs.getString("CommissionCharges"));
				data.setCurrencyCode(rs.getString("CurrencyCode"));
				double Amount = rs.getDouble("Settlement");				
				if(Amount ==0.0 || Amount == 0.00){
					data.setAmount("0.00");
				}
				//System.out.println("Dobuble amt=" + String.format("%.2f", Amount));
				data.setAmount(String.format("%.2f", Amount));					
				String amt = Double.toString(Amount);
				data.setAmount(amt.substring(0,amt.indexOf(".")));
				data.setFromAccount("WTR001");
				data.setToAccount(fdata.get(j).getAccountNumber());
				data.setSettlementDate(new Geneal().datetimeTostring(fromDate));
				data.setMerchantID(fdata.get(j).getMerchantID());				
				data.setMerchantName(fdata.get(j).getFeature());
				if(Amount > -1){
					data.setTransType("C");
				}else{
					data.setTransType("D");
				}
				
				datalist.add(data);
			}
		}
		return datalist;

	}*/
	public ArrayList<FeatureData> getDataByID(Connection conn) throws SQLException {
		ArrayList<FeatureData> data = new ArrayList<FeatureData>();
		String sql = "select UserName,t6,t7 from CMSMerchant";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
		FeatureData resultdata = new FeatureData();
		resultdata.setAccountNumber(rs.getString("t7"));
		resultdata.setMerchantID(rs.getString("t6"));
		resultdata.setFeature(rs.getString("UserName"));
		data.add(resultdata);
		}
		return data;
	}
	
	public ArrayList<TempData> getWalletTransferReportDownload(WalletTransDataRequest data, Connection pConn) throws SQLException {
		ArrayList<TempData> l_List = new ArrayList<TempData>();
		PreparedStatement l_pstmt=null;
		String Cri="";
		String Cri1="";
		String MerchantID = data.getMerchantID();
		String FDate = data.getFromDate();
		String TDate = data.getToDate();
		String allDate ="true";
		boolean alldate = data.isAlldate();
		if (!MerchantID.equals("")) {
			if (!MerchantID.equalsIgnoreCase("All")) {
				Cri = "where UserId =(?)";
			}
		}
		if (alldate == false) {
			Cri1 += " and B.TransDate >= ? and B.TransDate <= ? ";
			allDate="false";

		}
		/*if(alldate==false){
			Cri1=" And B.TransDate>=? and B.TransDate<=? ";
		}*/
		
//		String l_Query ="SET DateFormat DMY;" +
//						" select B.T1 As FromName,FromAccount,B.TransID As TransferID,TransDate, " +
//						"A.UserId As MerchantID,B.Amount,B.CommissionCharges,A.UserName  " +
//						"from (select T7,UserName,UserId from CMSMerchant  "+Cri +
//						" )As A  " +
//						"inner join WalletTransaction As B on A.T7=B.ToAccount  " +
//						"and B.TransStatus=2 and B.Transtype=2"+Cri1;
		String l_Query ="SET DateFormat DMY;" +
				" select B.T1 As FromName,FromAccount,B.TransID As TransferID,TransDate, " +
				"A.UserId As MerchantID,B.Amount,B.CommissionCharges,A.UserName  " +
				"from (select T7,UserName,UserId from CMSMerchant  "+Cri +
				" )As A  " +
				"inner join WalletTransaction As B on A.UserId=B.merchantId   " +
				"and B.TransStatus=2 and B.Transtype=2"+Cri1;
		
		l_pstmt = pConn.prepareStatement(l_Query);
		int index=1;
		if (!MerchantID.equals("")) {
			if (!MerchantID.equalsIgnoreCase("All")) {
				l_pstmt.setString(index++, MerchantID);
			}
		}
		if (alldate == false) {
			l_pstmt.setString(index++, FDate);
			l_pstmt.setString(index++, TDate);

		}
		ResultSet l_RS = l_pstmt.executeQuery();
		while(l_RS.next()) {
			TempData l_data = new TempData();
			l_data.setT1(l_RS.getString("FromName"));
			l_data.setT2(l_RS.getString("FromAccount"));
			l_data.setT3(l_RS.getString("TransferID"));
			l_data.setT4(GeneralUtility.formatMIT2DDMMYYYY(l_RS.getString("TransDate")));
			//l_data.setT4(l_RS.getString("modifiedDate"));
			l_data.setT5(l_RS.getString("MerchantID"));
			l_data.setN1(l_RS.getDouble("Amount"));
			l_data.setN2(l_RS.getDouble("CommissionCharges"));
			l_data.setT6(l_RS.getString("UserName"));
			l_data.setT7(new GeneralUtil().formatYYYYMMDD2DDMMYYYY(FDate));
			l_data.setT8(new GeneralUtil().formatYYYYMMDD2DDMMYYYY(TDate));
			l_data.setT9(allDate);
			l_List.add(l_data);
		}
		
		return l_List;
	}
	public WalletTransDataResponse getWalletTransferReport(WalletTransDataRequest data, Connection pConn) throws SQLException {
		
		int l_startRecord = (data.getCurrentPage() - 1) * data.getPageSize();
		int l_endRecord = l_startRecord + data.getPageSize();
		ArrayList<TempData> l_List = new ArrayList<TempData>();
		WalletTransDataResponse merchantdata = new WalletTransDataResponse();
		PreparedStatement l_pstmt=null;
		String Cri="";
		String Cri1="";
		String MerchantID = data.getMerchantID();
		String FDate = data.getFromDate();
		String TDate = data.getToDate();
		String allDate ="true";
		boolean alldate = data.isAlldate();
		if (!MerchantID.equals("")) {
			if (!MerchantID.equalsIgnoreCase("All")) {
				Cri = "where UserId =(?)";
			}
		}
		if (alldate == false) {
			Cri1 += " and B.TransDate >= ? and B.TransDate <= ? ";
			allDate="false";

		}
		/*if(alldate==false){
			Cri1=" And B.TransDate>=? and B.TransDate<=? ";
		}*/
		
//		String l_Query ="SET DateFormat DMY;" +
//						" select  * from ( SELECT ROW_NUMBER() OVER (ORDER BY B.CreatedDate desc) AS RowNum, B.T1 As FromName,FromAccount,B.TransID As TransferID,TransDate, " +
//						"A.UserId As MerchantID,B.Amount,B.CommissionCharges,A.UserName  " +
//						"from (select T7,UserName,UserId from CMSMerchant  "+Cri +
//						" )As A  " +
//						"inner join WalletTransaction As B on A.T7=B.ToAccount  " +
//						"and B.TransStatus=2 and B.Transtype=2"+Cri1+ ") As r  WHERE 1=1 and RowNum > " + l_startRecord
//						+ " and RowNum <= " + l_endRecord;;
		String l_Query ="SET DateFormat DMY;" +
				" select  * from ( SELECT ROW_NUMBER() OVER (ORDER BY B.CreatedDate desc) AS RowNum, B.T1 As FromName,FromAccount,B.TransID As TransferID,TransDate, " +
				"A.UserId As MerchantID,B.Amount,B.CommissionCharges,A.UserName  " +
				"from (select T7,UserName,UserId from CMSMerchant  "+Cri +
				" )As A  " +
				"inner join WalletTransaction As B on A.UserId=B.merchantId   " +
				"and B.TransStatus=2 and B.Transtype=2"+Cri1+ ") As r  WHERE 1=1 and RowNum > " + l_startRecord
				+ " and RowNum <= " + l_endRecord;;
		
		l_pstmt = pConn.prepareStatement(l_Query);
		int index=1;
		if (!MerchantID.equals("")) {
			if (!MerchantID.equalsIgnoreCase("All")) {
				l_pstmt.setString(index++, MerchantID);
			}
		}
		if (alldate == false) {
			l_pstmt.setString(index++, FDate);
			l_pstmt.setString(index++, TDate);

		}
		ResultSet l_RS = l_pstmt.executeQuery();
		int srno = l_startRecord + 1;
		while(l_RS.next()) {
			TempData l_data = new TempData();
			l_data.setT1(l_RS.getString("FromName"));
			l_data.setT2(l_RS.getString("FromAccount"));
			l_data.setT3(l_RS.getString("TransferID"));
			l_data.setT4(GeneralUtility.formatMIT2DDMMYYYY(l_RS.getString("TransDate")));
			//l_data.setT4(l_RS.getString("modifiedDate"));
			l_data.setAutokey(srno++);
			l_data.setT5(l_RS.getString("MerchantID"));
			l_data.setN1(l_RS.getDouble("Amount"));
			l_data.setN2(l_RS.getDouble("CommissionCharges"));
			l_data.setT6(l_RS.getString("UserName"));
			l_data.setT7(new GeneralUtil().formatYYYYMMDD2DDMMYYYY(FDate));
			l_data.setT8(new GeneralUtil().formatYYYYMMDD2DDMMYYYY(TDate));
			l_data.setT9(allDate);
//			double Amount = l_RS.getDouble("Amount");
//			if(Amount ==0.0 || Amount == 0.00){
//			
//				l_data.setN1("0.00");
//			}
			l_List.add(l_data);
			}
		
			String countSQL = "";
//			countSQL = "select COUNT(*) as recCount from ( SELECT  A.* FROM   CMSMerchant A, WalletTransaction B where A.T7=B.ToAccount and B.TransStatus=2 and B.Transtype=2" + Cri1 + ") As r";
			
//			countSQL="select COUNT(*) as recCount from ( select T7,UserName,UserId from CMSMerchant  "+Cri +
//						" )As A  " +
//						"inner join WalletTransaction As B on A.T7=B.ToAccount  " +
//						"and B.TransStatus=2 and B.Transtype=2"+ Cri1;
			countSQL="select COUNT(*) as recCount from ( select T7,UserName,UserId from CMSMerchant  "+Cri +
					" )As A  " +
					"inner join WalletTransaction As B on A.UserId=B.merchantId  " +
					"and B.TransStatus=2 and B.Transtype=2"+ Cri1;
			PreparedStatement ps1 = pConn.prepareStatement(countSQL);
			int h = 1;
			if (!MerchantID.equals("")) {
				if (!MerchantID.equalsIgnoreCase("All")) {
				ps1.setString(h++, MerchantID);
				}
			}
			if (alldate == false) {
				ps1.setString(h++, FDate);
				ps1.setString(h++, TDate);

			}
			ResultSet result = ps1.executeQuery();
			result.next();
			merchantdata.setTotalCount(result.getInt("recCount"));
			
		TempData[] dataarry = new TempData[l_List.size()];
		for (int i = 0; i < l_List.size(); i++) {
			dataarry[i] = l_List.get(i);
		}
		merchantdata.setMerchantdata(dataarry);
		merchantdata.setPageSize(data.getPageSize());
		merchantdata.setCurrentPage(data.getCurrentPage());
		
		
		return merchantdata;
	}
	public ArrayList<TempData> getWalletTransferCountReport(WalletTransDataRequest data, Connection pConn) throws SQLException {
		ArrayList<TempData> l_List = new ArrayList<TempData>();
		PreparedStatement l_pstmt=null;
		String Cri="";
		String Cri1="";
		String MerchantID = data.getMerchantID();
		String FDate = data.getFromDate();
		String TDate = data.getToDate();
		boolean alldate = data.isAlldate();
		if(!MerchantID.equalsIgnoreCase("All")){
			Cri="where UserId =(?)";
		}
		if (alldate == false) {
			Cri1 += " and B.TransDate >= ? and B.TransDate <= ? ";
		}
//		String l_Query ="SET DateFormat DMY;" +
//						"Select Count(A.UserId)As Mercount,Sum(Amount)As Amount, " +
//						"Sum(BankCharges)As BankCharges,A.UserName from (select T7,UserName,UserId from CMSMerchant  "+Cri +
//						" )As A  " +
//						"inner join WalletTransaction As B on A.T7=B.ToAccount  " +
//						"and B.TransStatus=2 and B.Transtype=2"+Cri1;
		String l_Query ="SET DateFormat DMY;" +
				"Select Count(A.UserId)As Mercount,Sum(Amount)As Amount, " +
				"Sum(BankCharges)As BankCharges,A.UserName from (select T7,UserName,UserId from CMSMerchant  "+Cri +
				" )As A  " +
				"inner join WalletTransaction As B on A.UserId=B.merchantId   " +
				"and B.TransStatus=2 and B.Transtype=2"+Cri1;
		l_Query+=" group by A.UserId,A.UserName";
		l_pstmt = pConn.prepareStatement(l_Query);
		int index=1;
		if(!MerchantID.equalsIgnoreCase("All")){
			l_pstmt.setString(index++, MerchantID);
		}
		if (alldate == false) {
			l_pstmt.setString(index++, FDate);
			l_pstmt.setString(index++, TDate);

		}
		ResultSet l_RS = l_pstmt.executeQuery();
		while(l_RS.next()) {
			TempData l_data = new TempData();
			l_data.setI1(l_RS.getInt("Mercount"));
			l_data.setT1(l_RS.getString("UserName"));
			l_data.setN1(l_RS.getDouble("Amount"));
			l_data.setN2(l_RS.getDouble("BankCharges"));
			l_List.add(l_data);
		}
		return l_List;
	}
	public WalletTransDataResponse getSettlementList(String fromDate, Connection conn1,Connection conn2)
			throws SQLException {
		ArrayList<WalletTransData> datalist = new ArrayList<WalletTransData>();		
		WalletTransDataResponse response = new WalletTransDataResponse();
		String whereclause = "";	
		ResultSet rs;
		String dbName = conn1.getCatalog();
		ArrayList<FeatureData> fdata = new ArrayList<FeatureData>();		
		PreparedStatement ps = null;
			//Create temp table
			//String tmpTableName = "Tmp" + fdata.get(j).getAccountNumber();
			String tmpTableName1 = "TmpWLGL";
			String tmpTableName2 = "tmpWallet";
			String query1 = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[" + tmpTableName1
					+ "]') AND type in (N'U')) " + "	DROP TABLE " + tmpTableName1;			
			ps = conn1.prepareStatement(query1);
			ps.executeUpdate();
			String query2 = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[" + tmpTableName2
					+ "]') AND type in (N'U')) " + "	DROP TABLE " + tmpTableName2;		
			ps = conn1.prepareStatement(query2);
			ps.executeUpdate();

			String query ="Select T7 As AccNumber,T6 As MerchantType, 1 As Type ,Convert(varchar(20),'') As Code INTO " + tmpTableName1 +" FROM CMSMerchant";
			ps = conn1.prepareStatement(query);
			ps.executeUpdate();
						
			String query3 = "SELECT * FROM " + dbName + ".." + tmpTableName1//TmpWLGL 
					+ " INSERT INTO "+ dbName + ".." + tmpTableName1 +" SELECT  GLCode,0,2,GLAccNumber FROM ReferenceAccounts Where GLAccNumber like ? AND GLAccNumber not like ? "
					+ "AND GLAccNumber not like ?";
			ps = conn2.prepareStatement(query3);
			ps.setString(1, "WTR%%02");
			ps.setString(2, "WTRCOM%%02");
			ps.setString(3, "WTRNL%%02");			
			rs = ps.executeQuery();
			if(rs.next()){}
			/*while (rs.next()) {
			FeatureData data = new FeatureData();
			data.setAccountNumber(rs.getString("AccNumber"));
			data.setMerchantID(rs.getString("MerchantType"));
			data.setTransType(rs.getString("Type"));
			fdata.add(data);
			}*/
		//for (int j = 0; j < fdata.size(); j++) {
			String tmpTableName3 = "Tmp";
			String query4 = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].["
					+ tmpTableName3 + "]') AND type in (N'U')) " + "	DROP TABLE " + tmpTableName3;
			ps = conn1.prepareStatement(query4);
			ps.executeUpdate();

			if (fromDate != null && fromDate.trim().length() != 0) {// 1
				whereclause += " And EffectiveDate = ?";
			}	
			String query5 = "Select TrAmount ,TrCurCode,A.AccNumber,TransType,Transref into " + dbName + ".." +tmpTableName3 // WL_YCB..TmpL020501001001001
					+ " FROM AccountTransaction A INNER JOIN AccountGLTransaction B ON A.TransNo=B.TransNo  "
					+ "INNER JOIN " + dbName + ".." + tmpTableName1 +" C ON C.AccNumber=A.AccNumber   Where A.status<254 " + whereclause;//TmpWLGL
			ps = conn2.prepareStatement(query5);
			ps.setString(1, fromDate);
			ps.executeUpdate();

			String query6 = "INSERT INTO  " + dbName + ".." + tmpTableName3
					+ " SELECT TrAmount ,TrCurCode,A.AccNumber,TransType,Transref "
					+ "FROM AccountTransactionOlD A INNER JOIN AccountGLTransaction B ON A.TransNo=B.TransNo "
					+ "INNER JOIN " + dbName + ".." + tmpTableName1 +" C ON C.AccNumber=A.AccNumber  Where A.status<254 " + whereclause;;//TmpWLGL,counttransactonold
			ps = conn2.prepareStatement(query6);
			ps.setString(1, fromDate);
			ps.executeUpdate();

			String query7 = "Select C.AccNumber,SUM(CASE WHEN B.TransType>500 THEN B.TrAmount*-1 ELSE B.TrAmount END) Settlement,"
					+ "SUM(CommissionCharges) CommissionCharges,B.TrCurCode,C.MerchantType,C.Type,C.Code INTO "+ tmpTableName2
					+ " FROM WalletTransaction A INNER JOIN " + tmpTableName3
					+ " B ON A.TransID=B.TransRef  "// TmpL020501001001001
					+ "INNER JOIN  " + tmpTableName1//tmpWallet
					+ " C ON C.AccNumber=B.AccNumber GROUP BY C.AccNumber,B.TrCurCode,C.MerchantType,C.Type,C.Code";// TmpWLGL
			ps = conn1.prepareStatement(query7);
			ps.executeUpdate();

			String DrAcc = "";
			String query8 = "SELECT GLCode FROM ReferenceAccounts  Where GLAccNumber = ?";
			ps = conn2.prepareStatement(query8);
			ps.setString(1, "WTRNILMMK02");
			rs = ps.executeQuery();
			if (rs.next()) {
				DrAcc = rs.getString("GLCode");
			}

			String query9 = "SELECT (CASE WHEN Settlement>0 THEN AccNumber ELSE ? END) As DrAccNumber,"
					+ "(CASE WHEN Settlement<0 THEN AccNumber ELSE ? END) "
					+ "As CrAccNumber,Settlement,CommissionCharges,TrCurCode,MerchantType,Type,  "
					+ " ISNULL(B.UserName,'Wallet Account') UserName,A.Code FROM " + tmpTableName2 +" A LEFT JOIN CMSMerchant B ON A.MerchantType=B.T6";
			ps = conn1.prepareStatement(query9);
			ps.setString(1, DrAcc);
			ps.setString(2, DrAcc);
			rs = ps.executeQuery();
			while (rs.next()) {
				WalletTransData data = new WalletTransData();
				data.setDrAccNumber(rs.getString("DrAccNumber"));
				data.setCrAccNumber(rs.getString("CrAccNumber"));
				double Amount = rs.getDouble("Settlement");
				if (Amount == 0.0 || Amount == 0.00) {
					data.setAmount("0.00");
				}
				data.setAmount(String.format("%.2f", Amount));
				data.setAmount(data.getAmount().replace("-", ""));
				data.setCommissionCharges(rs.getString("CommissionCharges"));
				data.setCurrencyCode(rs.getString("TrCurCode"));
				data.setMerchantID(rs.getString("MerchantType"));
				data.setMerchantName(rs.getString("UserName"));
				data.setCode(rs.getString("code"));
				if(Amount > -1){
					data.setTransType("C");
				}else{
					data.setTransType("D");
				}
				data.setSettlementDate(new Geneal().datetimeTostring(fromDate));	
				data.setState(true);
				datalist.add(data);
			}
			WalletTransData[] dataarry = new WalletTransData[datalist.size()];
			for (int i = 0; i < datalist.size(); i++) {
				dataarry[i] = datalist.get(i);
			}
			response.setWldata(dataarry);
		//}
		return response;
	}
	
	public boolean  saveExportLogs(String pDate,int pType,String pBCode,int pTransref,Connection pConn) throws SQLException{
		boolean ret = false;
		PreparedStatement pstmt = pConn.prepareStatement("INSERT INTO SettlementExportLog([SettlementDate],[Typ],[BranchCode],[Transref],"
				+ "[T1],[T2],[T3],[T4],[T5],[N1],[N2],[N3],[N4],[N5]) Values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		pstmt.setString(1, pDate);
		pstmt.setInt(2, pType);
		pstmt.setString(3, pBCode);
		pstmt.setInt(4, pTransref);
		pstmt.setString(5, "");
		pstmt.setString(6, "");
		pstmt.setString(7, "");
		pstmt.setString(8, "");
		pstmt.setString(9, "");
		pstmt.setInt(10, 0);
		pstmt.setInt(11, 0);
		pstmt.setInt(12, 0);
		pstmt.setDouble(13, 0);
		pstmt.setDouble(14, 0);
		if(pstmt.executeUpdate()>0)
			ret = true;
		return ret;		
	}
	
	public boolean  checkExportLogs(String pDate,int pType,String pBCode,Connection pConn) throws SQLException{
		boolean ret = false;
		PreparedStatement pstmt = pConn.prepareStatement("SELECT [SettlementDate] FROM SettlementExportLog WHERE SettlementDate=? AND Typ=? AND [BranchCode]=?");
		pstmt.setString(1, pDate);
		pstmt.setInt(2, pType);
		pstmt.setString(3, pBCode);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()){
			ret = true; break;
		}
		return ret;
	}
}
