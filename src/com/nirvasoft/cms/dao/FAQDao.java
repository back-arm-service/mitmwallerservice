package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.cms.shared.FAQData;
import com.nirvasoft.cms.shared.FAQListingData;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;
import com.nirvasoft.rp.framework.FAQDataset;
import com.nirvasoft.rp.shared.FilterData;
import com.nirvasoft.rp.shared.FilterDataset;

public class FAQDao {

	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("FAQ");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("autokey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("status", (byte) 2));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("questionEng", (byte) 5));
		ret.getFields().add(new DBField("answerEng", (byte) 5));
		ret.getFields().add(new DBField("questionUni", (byte) 5));
		ret.getFields().add(new DBField("answerUni", (byte) 5));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		return ret;
	}

	// delete FAQ
	public static FAQDataset delete(Long syskey, Connection conn) throws SQLException {
		FAQDataset res = new FAQDataset();
		String sql = "UPDATE FAQ SET Status=4 WHERE Status = 1 AND syskey=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Successfully");
		} else {
			res.setMsgDesc("Deleting Unsuccessful");
		}
		return res;
	}

	public static FAQData getFAQbysyskey(long autokey, Connection conn) throws SQLException {
		FAQData ret = new FAQData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "where  Status<>4 AND autokey=" + autokey, "", conn);
		if (dbrs.size() > 0)
			ret = getFAQRecord(dbrs.get(0));
		return ret;
	}

	public static FAQListingData getFAQList(FilterDataset req, Connection conn) throws SQLException {
		FAQListingData res = new FAQListingData();

		int startPage = (req.getPageNo() - 1) * req.getPageSize();
		int endPage = req.getPageSize() + startPage;

		ArrayList<FAQData> datalist = new ArrayList<FAQData>();
		PreparedStatement stat;
		String whereclause = "";
		whereclause = " where  Status<>4 AND Status=1";

		// Get Filter List
		ArrayList<FilterData> l_FilterList = req.getFilterList();

		// 1 - Simple Search String [ , , , ]
		String simplesearch = "";
		FilterData l_FilterData = getFilterData("1", l_FilterList);
		if (l_FilterData != null)
			simplesearch = l_FilterData.getT1();

		// 2 - State
		String state = "";
		l_FilterData = getFilterData("2", l_FilterList);
		if (l_FilterData != null)
			state = l_FilterData.getT1();

		// 3 - status
		String statusString = "";
		l_FilterData = getFilterData("3", l_FilterList);
		if (l_FilterData != null)
			statusString = l_FilterData.getT1();

		if (!simplesearch.equals("")) {
			whereclause += " AND (questionEng LIKE N'%" + simplesearch + "%' " + "OR answerEng LIKE N'%" + simplesearch
					+ "%' " + "OR questionUni LIKE N'%" + simplesearch + "%' " + "OR answerUni LIKE N'%" + simplesearch
					+ "%') ";
		}

		String sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY autokey desc) AS RowNum,* FROM"
				+ " ( select syskey,autokey,questionEng,answerEng,questionUni,answerUni from FAQ" + whereclause
				+ ") b) " + " AS RowConstrainedResult" + " WHERE (RowNum > " + startPage + " AND RowNum <= " + endPage
				+ ")";

		stat = conn.prepareStatement(sql);
		ResultSet rs = stat.executeQuery();
		int srno = startPage + 1;
		while (rs.next()) {
			FAQData data = new FAQData();
			data.setSrno(srno++);// srno
			data.setSyskey(rs.getLong("syskey"));
			data.setAutokey(rs.getLong("autokey")); // Autokey
			data.setQuestionEng(rs.getString("questionEng")); // questionEng
			data.setAnswerEng(rs.getString("answerEng")); // answerEng
			data.setQuestionUni(rs.getString("questionUni"));// questionUni
			data.setAnswerUni(rs.getString("answerUni"));// answerUni
			datalist.add(data);
		}
		res.setPageSize(req.getPageSize());
		res.setCurrentPage(req.getPageNo());

		FAQData[] dataarray = null;
		if (datalist.size() > 0) {
			dataarray = new FAQData[datalist.size()];
			dataarray = datalist.toArray(dataarray);
		}

		String qry = "SELECT count(*) as recCount FROM (SELECT ROW_NUMBER() "
				+ "OVER (ORDER BY autokey desc) AS RowNum,* FROM ("
				+ "select autokey,questionEng,answerEng,questionUni,answerUni from FAQ " + whereclause
				+ ") b) AS RowConstrainedResult";

		stat = conn.prepareStatement(qry);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		res.setFaqData(dataarray);
		return res;
	}

	public static FAQData getFAQRecord(DBRecord adbr) {
		FAQData ret = new FAQData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setAutokey(adbr.getLong("autokey"));
		ret.setCreatedDate(adbr.getString("createddate"));
		ret.setModifiedDate(adbr.getString("modifieddate"));
		ret.setStatus(adbr.getInt("status"));
		ret.setUserID(adbr.getString("userid"));
		ret.setQuestionEng(adbr.getString("questionEng"));
		ret.setAnswerEng(adbr.getString("answerEng"));
		ret.setQuestionUni(adbr.getString("questionUni"));
		ret.setAnswerUni(adbr.getString("answerUni"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getLong("n5"));

		return ret;
	}

	public static FilterData getFilterData(String aItemID, ArrayList<FilterData> aFilterList) {
		FilterData l_FilterData = null;

		for (FilterData iFilterData : aFilterList) {
			if (iFilterData.getItemid().equals(aItemID)) {
				l_FilterData = iFilterData;
				break;
			}
		}

		return l_FilterData;
	}

	public static FAQDataset insertFAQ(FAQData data, Connection conn) throws SQLException {
		FAQDataset res = new FAQDataset();
		int effectedrow = 0;
		String query = " Insert into FAQ (syskey,createddate,modifieddate,status,UserId,QuestionEng,AnswerEng,QuestionUni,AnswerUni,T1,T2,T3,T4,T5,N1,N2,N3,N4,N5) values (?,?,?,?,?,?,?,?,?,'','','','','',0,0,0,0,0) ";
		try {
			if (!isCodeExist(data, conn)) {
				PreparedStatement ps = conn.prepareStatement(query);
				int i = 1;
				ps.setLong(i++, data.getSyskey()); // syskey
				ps.setString(i++, data.getCreatedDate()); // createddate
				ps.setString(i++, data.getModifiedDate()); // modifiedDate
				ps.setInt(i++, data.getStatus()); // status
				ps.setString(i++, data.getUserID()); // loginuserid
				ps.setString(i++, data.getQuestionEng()); // QuestionEng
				ps.setString(i++, data.getAnswerEng()); // AnswerEng
				ps.setString(i++, data.getQuestionUni()); // QuestionUni
				ps.setString(i++, data.getAnswerUni()); // AnswerUni
				effectedrow = ps.executeUpdate();
				if (effectedrow > 0) {
					res.setState(true);
					res.setMsgDesc("Saved Successfully!");
				} else {
					res.setMsgDesc("Article Already Exist!");
				}
			}
		} catch (SQLException e) {

		}
		return res;
	}

	public static boolean isCodeExist(FAQData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				" where Status<>4 AND  Status = 1 AND syskey = " + obj.getSyskey() + " ", "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static FAQDataset updateFAQ(FAQData data, Connection conn) throws SQLException {
		FAQDataset res = new FAQDataset();
		int effectedrow = 0;
		String query = " Update FAQ SET [syskey]=?,[modifieddate]=?,[status]=?,[UserId]=?,[QuestionEng]=?,"
				+ "[AnswerEng]=?,[QuestionUni]=?,[AnswerUni]=?,[T1]='',[T2]='',[T3]='',[T4]='',[T5]='',[N1]=0,[N2]=0,[N3]=0,"
				+ "[N4]=0,[N5]=0 WHERE Status<>4 AND Status = 1 AND Syskey=" + data.getSyskey() + "";

		try {
			if (isCodeExist(data, conn)) {
				PreparedStatement pstmt = conn.prepareStatement(query);
				int i = 1;
				pstmt.setLong(i++, data.getSyskey());// syskey
				// pstmt.setString(i++, data.getCreatedDate());
				pstmt.setString(i++, data.getModifiedDate());// createddate
				pstmt.setInt(i++, data.getStatus());// status
				pstmt.setString(i++, data.getUserID());// loginuserid
				pstmt.setString(i++, data.getQuestionEng());// questionEng
				pstmt.setString(i++, data.getAnswerEng());// answerEng
				pstmt.setString(i++, data.getQuestionUni());// questionUni
				pstmt.setString(i++, data.getAnswerUni());// answerUni
				effectedrow = pstmt.executeUpdate();
				if (effectedrow > 0) {
					res.setState(true);
					res.setMsgDesc("Updated Successfully!");
				} else {
					res.setMsgDesc("No Such Article to Update!");
				}
			}
		} catch (SQLException e) {

		}

		return res;
	}

}
