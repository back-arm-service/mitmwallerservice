package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.AppHistoryData;
import com.nirvasoft.cms.shared.AppHistoryDataSet;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;

public class AppHistoryDao {

	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("FMR014");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t13", (byte) 5));
		ret.getFields().add(new DBField("t14", (byte) 5));
		ret.getFields().add(new DBField("t15", (byte) 5));
		ret.getFields().add(new DBField("t16", (byte) 5));
		ret.getFields().add(new DBField("t17", (byte) 5));
		ret.getFields().add(new DBField("t18", (byte) 5));
		ret.getFields().add(new DBField("t19", (byte) 5));
		ret.getFields().add(new DBField("t20", (byte) 5));
		ret.getFields().add(new DBField("t21", (byte) 5));
		ret.getFields().add(new DBField("t22", (byte) 5));
		ret.getFields().add(new DBField("t23", (byte) 5));
		ret.getFields().add(new DBField("t24", (byte) 5));
		ret.getFields().add(new DBField("t25", (byte) 5));
		ret.getFields().add(new DBField("t26", (byte) 5));
		ret.getFields().add(new DBField("t27", (byte) 5));
		ret.getFields().add(new DBField("t28", (byte) 5));
		ret.getFields().add(new DBField("t29", (byte) 5));
		ret.getFields().add(new DBField("t30", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		ret.getFields().add(new DBField("n11", (byte) 2));
		ret.getFields().add(new DBField("n12", (byte) 2));
		ret.getFields().add(new DBField("n13", (byte) 2));
		ret.getFields().add(new DBField("n14", (byte) 2));
		ret.getFields().add(new DBField("n15", (byte) 2));
		ret.getFields().add(new DBField("n16", (byte) 2));
		ret.getFields().add(new DBField("n17", (byte) 2));
		ret.getFields().add(new DBField("n18", (byte) 2));
		ret.getFields().add(new DBField("n19", (byte) 2));
		ret.getFields().add(new DBField("n20", (byte) 2));
		ret.getFields().add(new DBField("n21", (byte) 2));
		ret.getFields().add(new DBField("n22", (byte) 2));
		ret.getFields().add(new DBField("n23", (byte) 2));
		ret.getFields().add(new DBField("n24", (byte) 2));
		ret.getFields().add(new DBField("n25", (byte) 2));
		ret.getFields().add(new DBField("n26", (byte) 2));
		ret.getFields().add(new DBField("n27", (byte) 2));
		ret.getFields().add(new DBField("n28", (byte) 2));
		ret.getFields().add(new DBField("n29", (byte) 2));
		ret.getFields().add(new DBField("n30", (byte) 2));
		return ret;
	}

	public static DBRecord define(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t13", (byte) 5));
		ret.getFields().add(new DBField("t14", (byte) 5));
		ret.getFields().add(new DBField("t15", (byte) 5));
		ret.getFields().add(new DBField("t16", (byte) 5));
		ret.getFields().add(new DBField("t17", (byte) 5));
		ret.getFields().add(new DBField("t18", (byte) 5));
		ret.getFields().add(new DBField("t19", (byte) 5));
		ret.getFields().add(new DBField("t20", (byte) 5));
		ret.getFields().add(new DBField("t21", (byte) 5));
		ret.getFields().add(new DBField("t22", (byte) 5));
		ret.getFields().add(new DBField("t23", (byte) 5));
		ret.getFields().add(new DBField("t24", (byte) 5));
		ret.getFields().add(new DBField("t25", (byte) 5));
		ret.getFields().add(new DBField("t26", (byte) 5));
		ret.getFields().add(new DBField("t27", (byte) 5));
		ret.getFields().add(new DBField("t28", (byte) 5));
		ret.getFields().add(new DBField("t29", (byte) 5));
		ret.getFields().add(new DBField("t30", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		ret.getFields().add(new DBField("n11", (byte) 2));
		ret.getFields().add(new DBField("n12", (byte) 2));
		ret.getFields().add(new DBField("n13", (byte) 2));
		ret.getFields().add(new DBField("n14", (byte) 2));
		ret.getFields().add(new DBField("n15", (byte) 2));
		ret.getFields().add(new DBField("n16", (byte) 2));
		ret.getFields().add(new DBField("n17", (byte) 2));
		ret.getFields().add(new DBField("n18", (byte) 2));
		ret.getFields().add(new DBField("n19", (byte) 2));
		ret.getFields().add(new DBField("n20", (byte) 2));
		ret.getFields().add(new DBField("n21", (byte) 2));
		ret.getFields().add(new DBField("n22", (byte) 2));
		ret.getFields().add(new DBField("n23", (byte) 2));
		ret.getFields().add(new DBField("n24", (byte) 2));
		ret.getFields().add(new DBField("n25", (byte) 2));
		ret.getFields().add(new DBField("n26", (byte) 2));
		ret.getFields().add(new DBField("n27", (byte) 2));
		ret.getFields().add(new DBField("n28", (byte) 2));
		ret.getFields().add(new DBField("n29", (byte) 2));
		ret.getFields().add(new DBField("n30", (byte) 2));

		return ret;
	}

	public static DBRecord defines(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t13", (byte) 5));
		ret.getFields().add(new DBField("t14", (byte) 5));
		ret.getFields().add(new DBField("t15", (byte) 5));
		ret.getFields().add(new DBField("t16", (byte) 5));
		ret.getFields().add(new DBField("t17", (byte) 5));
		ret.getFields().add(new DBField("t18", (byte) 5));
		ret.getFields().add(new DBField("t19", (byte) 5));
		ret.getFields().add(new DBField("t20", (byte) 5));
		ret.getFields().add(new DBField("t21", (byte) 5));
		ret.getFields().add(new DBField("t22", (byte) 5));
		ret.getFields().add(new DBField("t23", (byte) 5));
		ret.getFields().add(new DBField("t24", (byte) 5));
		ret.getFields().add(new DBField("t25", (byte) 5));
		ret.getFields().add(new DBField("t26", (byte) 5));
		ret.getFields().add(new DBField("t27", (byte) 5));
		ret.getFields().add(new DBField("t28", (byte) 5));
		ret.getFields().add(new DBField("t29", (byte) 5));
		ret.getFields().add(new DBField("t30", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		ret.getFields().add(new DBField("n11", (byte) 2));
		ret.getFields().add(new DBField("n12", (byte) 2));
		ret.getFields().add(new DBField("n13", (byte) 2));
		ret.getFields().add(new DBField("n14", (byte) 2));
		ret.getFields().add(new DBField("n15", (byte) 2));
		ret.getFields().add(new DBField("n16", (byte) 2));
		ret.getFields().add(new DBField("n17", (byte) 2));
		ret.getFields().add(new DBField("n18", (byte) 2));
		ret.getFields().add(new DBField("n19", (byte) 2));
		ret.getFields().add(new DBField("n20", (byte) 2));
		ret.getFields().add(new DBField("n21", (byte) 2));
		ret.getFields().add(new DBField("n22", (byte) 2));
		ret.getFields().add(new DBField("n23", (byte) 2));
		ret.getFields().add(new DBField("n24", (byte) 2));
		ret.getFields().add(new DBField("n25", (byte) 2));
		ret.getFields().add(new DBField("n26", (byte) 2));
		ret.getFields().add(new DBField("n27", (byte) 2));
		ret.getFields().add(new DBField("n28", (byte) 2));
		ret.getFields().add(new DBField("n29", (byte) 2));
		ret.getFields().add(new DBField("n30", (byte) 2));

		return ret;
	}

	public static boolean ExistType(AppHistoryData obj, Connection conn) throws SQLException {
		System.out.println(obj.getT7());
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR015"),
				" where RecordStatus = 1 AND n1 = " + obj.getN1() + " AND t7 = '" + obj.getT7(), "' ", conn);
		if (dbrs.size() > 0) {
			System.out.println("exist");
			return true;

		} else {
			System.out.println("not exist");
			return false;
		}
	}

	public static long ExistUser(AppHistoryData obj, Connection conn) throws SQLException {

		long syskey = 0;

		String sql = "Select syskey from FMR014 where RecordStatus = 1 AND n1 = " + obj.getN1() + " ";
		PreparedStatement stmt = conn.prepareStatement(sql);

		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			syskey = rs.getLong("syskey");
			// System.out.println("exist");

		} else {
			syskey = 0;
			System.out.println("not exist");

		}
		return syskey;
	}

	public static boolean existUserSK(AppHistoryData obj, Connection conn) throws SQLException {
		System.out.println(obj.getT3());
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR015"),
				" where RecordStatus<>4 AND  RecordStatus = 1 AND n1 = " + obj.getN1() + " AND t3 = " + obj.getT3()
						+ " ",
				"", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static AppHistoryData getDBRecord(DBRecord adbr) {
		AppHistoryData ret = new AppHistoryData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("createddate"));
		ret.setModifiedDate(adbr.getString("modifieddate"));
		ret.setUserId(adbr.getString("userid"));
		ret.setUserName(adbr.getString("username"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setUserSyskey(adbr.getLong("usersyskey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		ret.setT8(adbr.getString("t8"));
		ret.setT9(adbr.getString("t9"));
		ret.setT10(adbr.getString("t10"));
		ret.setT11(adbr.getString("t11"));
		ret.setT12(adbr.getString("t12"));
		ret.setT13(adbr.getString("t13"));
		ret.setT14(adbr.getString("t14"));
		ret.setT15(adbr.getString("t15"));
		ret.setT16(adbr.getString("t16"));
		ret.setT17(adbr.getString("t17"));
		ret.setT18(adbr.getString("t18"));
		ret.setT19(adbr.getString("t19"));
		ret.setT20(adbr.getString("t20"));
		ret.setT21(adbr.getString("t21"));
		ret.setT22(adbr.getString("t22"));
		ret.setT23(adbr.getString("t23"));
		ret.setT24(adbr.getString("t24"));
		ret.setT25(adbr.getString("t25"));
		ret.setT26(adbr.getString("t26"));
		ret.setT27(adbr.getString("t27"));
		ret.setT28(adbr.getString("t28"));
		ret.setT29(adbr.getString("t29"));
		ret.setT30(adbr.getString("t30"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getLong("n5"));
		ret.setN6(adbr.getLong("n6"));
		ret.setN7(adbr.getLong("n7"));
		ret.setN8(adbr.getLong("n8"));
		ret.setN9(adbr.getLong("n9"));
		ret.setN10(adbr.getLong("n10"));
		ret.setN11(adbr.getLong("n11"));
		ret.setN12(adbr.getLong("n12"));
		ret.setN13(adbr.getLong("n13"));
		ret.setN14(adbr.getLong("n14"));
		ret.setN15(adbr.getLong("n15"));
		ret.setN16(adbr.getLong("n16"));
		ret.setN17(adbr.getLong("n17"));
		ret.setN18(adbr.getLong("n18"));
		ret.setN19(adbr.getLong("n19"));
		ret.setN20(adbr.getLong("n20"));
		ret.setN21(adbr.getLong("n21"));
		ret.setN22(adbr.getLong("n22"));
		ret.setN23(adbr.getLong("n23"));
		ret.setN24(adbr.getLong("n24"));
		ret.setN25(adbr.getLong("n25"));
		ret.setN26(adbr.getLong("n26"));
		ret.setN27(adbr.getLong("n27"));
		ret.setN28(adbr.getLong("n28"));
		ret.setN29(adbr.getLong("n29"));
		ret.setN30(adbr.getLong("n30"));
		return ret;
	}

	public static long getMaxBatch(Connection aConnection) {
		long ret = 0;
		String l_Query = "select ISNULL(MAX(n2) + 1, 1) as maxSys FROM fmr014";
		Statement stmt;
		try {
			stmt = aConnection.createStatement();
			ResultSet rs = stmt.executeQuery(l_Query);
			if (rs.next()) {
				ret = rs.getLong("maxSys");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return ret;
	}

	public static long getMaxBatchNo(Connection aConnection) {
		long ret = 0;
		String l_Query = "select ISNULL(MAX(n2) + 1, 1) as maxSys FROM fmr015";
		Statement stmt;
		try {
			stmt = aConnection.createStatement();
			ResultSet rs = stmt.executeQuery(l_Query);
			if (rs.next()) {
				ret = rs.getLong("maxSys");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return ret;
	}

	public static Resultb2b insertApp(AppHistoryData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();

		if (!isCodeExist(obj, conn)) {
			String sql = DBMgr.insertString(define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			int count = stmt.executeUpdate();
			if (count > 0) {
				res.setState(true);
				res.setMsgDesc("Saved Successfully!");
			}
		} else {
			res.setMsgDesc("Phone Call Already Exist!");
		}
		return res;
	}

	// TDA
	public static Resultb2b insertAppHistoryView(AppHistoryData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();

		// if (!isCodeExistforhistoryview(obj, conn)) {
		String sql = DBMgr.insertString(define("FMR015"), conn);
		PreparedStatement stmt = conn.prepareStatement(sql);
		DBRecord dbr = setDBRecord(obj);
		DBMgr.setValues(stmt, dbr);
		int count = stmt.executeUpdate();
		if (count > 0) {
			res.setState(true);
			res.setMsgDesc("Saved Successfully!");
			System.out.println("Saved Successfully!");
		}
		/*
		 * } else { res.setMsgDesc("Phone Call Already Exist!"); }
		 */
		return res;
	}

	public static Resultb2b insertPh(AppHistoryData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();

		String sql = DBMgr.insertString(define("FMR010"), conn);
		PreparedStatement stmt = conn.prepareStatement(sql);
		DBRecord dbr = setDBRecord(obj);
		DBMgr.setValues(stmt, dbr);
		int count = stmt.executeUpdate();
		if (count > 0) {
			res.setState(true);
			res.setMsgDesc("Saved Successfully!");
		}

		return res;
	}

	public static Resultb2b insertType(AppHistoryData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();

		String sqlString = "";

		if (obj.getT3().equals("Login")) {
			sqlString = "UPDATE FMR015 SET n2='" + obj.getN2() + "' where RecordStatus=1 AND n1 = '" + obj.getN1()
					+ "' ";
		}
		if (obj.getT3().equals("Weather")) {
			sqlString = "UPDATE FMR015 SET n4='" + obj.getN4() + "' where RecordStatus=1 AND n1 = '" + obj.getN1()
					+ "' ";
		}
		if (obj.getT3().equals("Price")) {
			sqlString = "UPDATE FMR015 SET n5='" + obj.getN5() + "' where RecordStatus=1 AND n1 = '" + obj.getN1()
					+ "' ";
		}
		if (obj.getT3().equals("News & Media")) {
			sqlString = "UPDATE FMR015 SET n6='" + obj.getN6() + "' where RecordStatus=1 AND n1 = '" + obj.getN1()
					+ "' ";
		}
		if (obj.getT3().equals("article")) {
			sqlString = "UPDATE FMR015 SET n7='" + obj.getN7() + "' where RecordStatus=1 AND n1 = '" + obj.getN1()
					+ "' ";
		}
		if (obj.getT3().equals("question")) {
			sqlString = "UPDATE FMR015 SET n8='" + obj.getN8() + "' where RecordStatus=1 AND n1 = '" + obj.getN1()
					+ "' ";
		}
		if (obj.getT3().equals("Save Content")) {
			sqlString = "UPDATE FMR015 SET n9='" + obj.getN9() + "' where RecordStatus=1 AND n1 = '" + obj.getN1()
					+ "' ";
		}
		if (obj.getT3().equals("Profile")) {
			sqlString = "UPDATE FMR015 SET n10='" + obj.getN10() + "' where RecordStatus=1 AND n1 = '" + obj.getN1()
					+ "' ";
		}
		if (obj.getT3().equals("Video")) {
			sqlString = "UPDATE FMR015 SET n11='" + obj.getN11() + "' where RecordStatus=1 AND n1 = '" + obj.getN1()
					+ "' ";
		}
		/*
		 * sqlString = "UPDATE FMR015 SET n2='" + obj.getN2() + "',  t7='" +
		 * obj.getT7() + "',t8='" + obj.getT8() +
		 * "'  where RecordStatus=1 AND n1 = '" + obj.getN1() + "' ";
		 */

		PreparedStatement st = conn.prepareStatement(sqlString);
		int result = st.executeUpdate();
		if (result > 0) {
			System.out.println(result);
			res.setState(true);
			res.setMsgDesc("Updated Successfully!");
		}
		return res;
	}

	public static Resultb2b insertTypeHistory(AppHistoryData obj, String hour, long num, Connection conn)
			throws SQLException {
		Resultb2b res = new Resultb2b();

		if (!isCodeExist(obj, conn)) {
			String sql = DBMgr.insertString(define("FMR015"), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			int count = stmt.executeUpdate();
			if (count > 0) {
				String sql1 = "UPDATE FMR015 SET t9 ='" + obj.getT7() + "', t10 ='" + hour
						+ "'  WHERE t3 not like 'Login' AND RecordStatus = 1 AND n4=" + num + " AND t7 ='" + obj.getT7()
						+ "' ";
				PreparedStatement stm = conn.prepareStatement(sql1);
				int rs = stm.executeUpdate();
				if (rs > 0) {
					res.setState(true);
					res.setMsgDesc("Saved Successfully!");
				}
			}
		} else {
			res.setMsgDesc("Phone Call Already Exist!");
		}
		return res;
	}

	public static boolean isCodeExist(AppHistoryData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				" where RecordStatus<>4 AND  RecordStatus = 1 AND syskey = " + obj.getSyskey() + " ", "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	// TDA
	public static boolean isCodeExistforhistoryview(AppHistoryData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR015"),
				" where RecordStatus<>4 AND  RecordStatus = 1 AND syskey = " + obj.getSyskey() + " ", "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static long isExistsysKey(AppHistoryData obj, Connection conn) throws SQLException {

		long syskey = 0;

		String sql = "Select max(syskey) as syskey from FMR015 where RecordStatus = 1 AND n3 = " + obj.getN3() + " ";
		PreparedStatement stmt = conn.prepareStatement(sql);

		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			syskey = rs.getLong("syskey");

		} else {
			syskey = 0;

		}
		return syskey;
	}

	public static AppHistoryDataSet readData(String type, Connection conn) throws SQLException {
		AppHistoryDataSet res = new AppHistoryDataSet();
		ArrayList<AppHistoryData> datalist = new ArrayList<AppHistoryData>();
		String whereclause = " WHERE RecordStatus = 1 AND t3 = '" + type + "' ";
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR015"), whereclause, " ORDER BY syskey ", conn);
		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getDBRecord(dbrs.get(i)));
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}

		AppHistoryData[] dataarray = new AppHistoryData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);

		return res;
	}

	public static AppHistoryDataSet readDataByT7(String syskey, String ph, String sdate, String edate, Connection conn)
			throws SQLException {
		AppHistoryDataSet res = new AppHistoryDataSet();
		ArrayList<AppHistoryData> datalist = new ArrayList<AppHistoryData>();
		String sql = "";
		System.out.println();
		AppHistoryData data = new AppHistoryData();
		sql = " SELECT t7 FROM FMR015 WHERE  (t7 between '" + sdate + "' and '" + edate + "' ) and "
				+ "  RecordStatus = 1 group by t7 ";
		PreparedStatement st = conn.prepareStatement(sql);
		ResultSet result = st.executeQuery();
		while (result.next()) {
			data = new AppHistoryData();
			data.setT7(result.getString("t7"));

			String whereclause = " WHERE RecordStatus = 1 AND t7 = '" + data.getT7() + "' AND (n3 = '" + syskey
					+ "' OR t1 = '" + ph + "') ";
			ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR015"), whereclause, " ORDER BY syskey ", conn);
			for (int i = 0; i < dbrs.size(); i++) {
				datalist.add(getDBRecord(dbrs.get(i)));
			}
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}

		AppHistoryData[] dataarray = new AppHistoryData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);

		return res;
	}

	public static AppHistoryDataSet readDataByTime(String syskey, String ph, String sdate, String edate,
			Connection conn) throws SQLException {
		AppHistoryDataSet res = new AppHistoryDataSet();
		ArrayList<AppHistoryData> datalist = new ArrayList<AppHistoryData>();
		String sql = "";
		AppHistoryData data;
		AppHistoryData rdata;
		sql = " SELECT t7 FROM FMR015 WHERE  (t7 between '" + sdate + "' and '" + edate + "' ) and "
				+ "  RecordStatus = 1 group by t7 ";
		PreparedStatement st = conn.prepareStatement(sql);
		ResultSet result = st.executeQuery();
		while (result.next()) {
			data = new AppHistoryData();
			data.setT7(result.getString("t7"));
			sql = "Select * from FMR015 WHERE (t7 between '" + sdate + "' and '" + edate + "' ) and "
					+ "  RecordStatus = 1 and t7 = '" + data.getT7() + "' AND (n3 = '" + syskey + "' OR t1 = '" + ph
					+ "')  and t10 not like '' ";
			PreparedStatement stm = conn.prepareStatement(sql);
			ResultSet resu = stm.executeQuery();
			while (resu.next()) {
				rdata = new AppHistoryData();
				rdata.setSyskey(resu.getLong("syskey"));
				rdata.setT1(resu.getString("t1"));
				rdata.setT2(resu.getString("t2"));
				rdata.setT3(resu.getString("t3"));
				rdata.setT5(resu.getString("t5"));
				rdata.setT6(resu.getString("t6"));
				rdata.setT7(resu.getString("t7"));
				rdata.setT8(resu.getString("t8"));
				rdata.setT10(resu.getString("t10"));
				// sql = "select datediff (day, '"+data.getT5()+"',
				// '"+data.getT7()+"') as daytotal , datediff(mi, '"+t6+"' ,
				// '"+t8+"') as total";
				sql = "select  datediff(MINUTE, '" + rdata.getT8() + "' , '" + rdata.getT10() + "') as total";
				PreparedStatement stm1 = conn.prepareStatement(sql);
				ResultSet re = stm1.executeQuery();
				while (re.next()) {
					if (re.getString("total").contains("-")) {
						rdata.setN5(Long.parseLong(re.getString("total").replace("-", "")));
					} else {
						rdata.setN5(Long.parseLong(re.getString("total")));
					}
				}
				datalist.add(rdata);

			}
		}

		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}

		AppHistoryData[] dataarray = new AppHistoryData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);

		return res;
	}

	public static AppHistoryDataSet searchAppAnalysisList(PagerData pgdata, String searchVal, String sdate,
			String edate, String frmName, Connection conn) throws SQLException {
		AppHistoryDataSet res = new AppHistoryDataSet();
		AppHistoryData data = new AppHistoryData();
		AppHistoryData gdata = new AppHistoryData();
		AppHistoryData rdata = new AppHistoryData();
		ArrayList<AppHistoryData> datalist = new ArrayList<AppHistoryData>();
		ArrayList<AppHistoryData> appList = new ArrayList<AppHistoryData>();
		AppHistoryData[] data1;
		String sql = "";
		String sql1 = "";
		if (frmName.equalsIgnoreCase("app")) {
			sql = " SELECT t7,t1,t3 FROM FMR015 WHERE  (t7 between '" + sdate + "' and '" + edate + "' ) and "
					+ "  RecordStatus = 1group by t7,t1,t3 ";
			PreparedStatement st = conn.prepareStatement(sql);
			ResultSet result = st.executeQuery();
			while (result.next()) {
				rdata.setT1(result.getString("t1"));
				rdata.setT3(result.getString("t3"));
				rdata.setT7(result.getString("t7"));
				// select count(t3) as total from fmr015 where t3='Weather' and
				// t7 = '20170313' and t1='+9595400162'
				sql1 = " SELECT t1, t7, t3,  count(t3) as total FROM FMR015 WHERE  (t7 between '" + sdate + "' and '"
						+ edate + "' ) and " + "  RecordStatus = 1 and t7 = '" + rdata.getT7() + "' and t1 = '"
						+ rdata.getT1() + "' and t3 = '" + rdata.getT3() + "'  group by t7,t1,t3 ";
				PreparedStatement stmt = conn.prepareStatement(sql1);
				ResultSet results = stmt.executeQuery();
				while (results.next()) {
					if (gdata.getT1().equals(results.getString("t1"))
							&& gdata.getT7().equals(results.getString("t7"))) {
						gdata.setT1(results.getString("t1"));
						gdata.setT7(results.getString("t7"));// date
						gdata.setT3(results.getString("t3"));
						if (gdata.getT3().equals("Login")) {
							gdata.setN1(Long.parseLong(results.getString("total")));
						}
						if (gdata.getT3().equals("Weather")) {
							gdata.setN2(Long.parseLong(results.getString("total")));
						}
						if (gdata.getT3().equals("Question")) {
							gdata.setN3(Long.parseLong(results.getString("total")));
						}
						if (gdata.getT3().equals("Price")) {
							gdata.setN4(Long.parseLong(results.getString("total")));
						}
						if (gdata.getT3().equals("Education")) {
							gdata.setN5(Long.parseLong(results.getString("total")));
						}
						if (gdata.getT3().equals("News & Media")) {
							gdata.setN6(Long.parseLong(results.getString("total")));
						}
						if (gdata.getT3().equals("Call Center")) {
							gdata.setN10(Long.parseLong(results.getString("total")));
						}
						if (gdata.getT3().equals("Video")) {
							gdata.setN7(Long.parseLong(results.getString("total")));
						}
						if (gdata.getT3().equals("Save Content")) {
							gdata.setN8(Long.parseLong(results.getString("total")));
						}

						appList.add(gdata);
					} else {
						gdata = new AppHistoryData();
						gdata.setT1(results.getString("t1"));
						gdata.setT7(results.getString("t7"));// date
						gdata.setT3(results.getString("t3"));
						if (gdata.getT3().equals("Login")) {
							gdata.setN1(Long.parseLong(results.getString("total")));
						}
						if (gdata.getT3().equals("Weather")) {
							gdata.setN2(Long.parseLong(results.getString("total")));
						}
						if (gdata.getT3().equals("Question")) {
							gdata.setN3(Long.parseLong(results.getString("total")));
						}
						if (gdata.getT3().equals("Price")) {
							gdata.setN4(Long.parseLong(results.getString("total")));
						}
						if (gdata.getT3().equals("Education")) {
							gdata.setN5(Long.parseLong(results.getString("total")));
						}
						if (gdata.getT3().equals("News & Media")) {
							gdata.setN6(Long.parseLong(results.getString("total")));
						}
						if (gdata.getT3().equals("Call Center")) {
							gdata.setN10(Long.parseLong(results.getString("total")));
						}
						if (gdata.getT3().equals("Video")) {
							gdata.setN7(Long.parseLong(results.getString("total")));
						}
						if (gdata.getT3().equals("Save Content")) {
							gdata.setN8(Long.parseLong(results.getString("total")));
						}

						appList.add(gdata);
					}

				}

			}
			for (int i = 0; i < appList.size(); i++) {
				long n2 = 0;
				int z = 0;

				for (int j = i + 1; j < appList.size(); j++) {
					if (appList.get(i).getT1().equals(appList.get(j).getT1())
							&& appList.get(i).getT7().equals(appList.get(j).getT7())) {
						if (appList.get(j).getT3().equals("Login")) {
							z = j;
							n2 = appList.get(j).getN2();
							appList.get(i).setN1(n2);// duplicate login count
							appList.remove(j);
							j--;

							break;
						}
						if (appList.get(j).getT3().equals("Weather")) {
							z = j;
							n2 = appList.get(j).getN2();
							appList.get(i).setN2(n2);// duplicate weather count
							appList.remove(j);
							j--;

							break;
						}
						if (appList.get(j).getT3().equals("Question")) {
							z = j;
							n2 = appList.get(j).getN2();
							appList.get(i).setN3(n2);
							appList.add(appList.get(i));

							appList.remove(j);
							j--;

							break;
						}
						if (appList.get(j).getT3().equals("Price")) {
							z = j;
							n2 = appList.get(j).getN2();
							appList.get(i).setN4(n2);// duplicate weather count
							appList.remove(j);
							j--;

							break;
						}
						if (appList.get(j).getT3().equals("Education")) {
							z = j;
							n2 = appList.get(j).getN2();
							appList.get(i).setN5(n2);// duplicate weather count

							appList.remove(j);
							j--;

							break;
						}
						if (appList.get(j).getT3().equals("News & Media")) {
							z = j;
							n2 = appList.get(j).getN2();
							appList.get(i).setN6(n2);// duplicate weather count

							appList.remove(j);
							j--;

							break;
						}
						if (appList.get(j).getT3().equals("Call Center")) {
							z = j;
							n2 = appList.get(j).getN2();
							appList.get(i).setN10(n2);// duplicate weather count

							appList.remove(j);
							j--;

							break;
						}
						if (appList.get(j).getT3().equals("Video")) {
							z = j;
							n2 = appList.get(j).getN2();
							appList.get(i).setN7(n2);// duplicate weather count

							appList.remove(j);
							j--;

							break;
						}
						if (appList.get(j).getT3().equals("Save Content")) {
							z = j;
							n2 = appList.get(j).getN2();
							appList.get(i).setN8(n2);// duplicate weather count

							appList.remove(j);
							j--;

							break;
						}

					}
				}
			}

			for (int i = 0; i < appList.size(); i++) {

				for (int j = i + 1; j < appList.size(); j++) {

					if (appList.get(i).getT1().equals(appList.get(j).getT1())
							&& appList.get(i).getT7().equals(appList.get(j).getT7())
							&& appList.get(i).getN1() == appList.get(j).getN1()
							&& appList.get(i).getN2() == appList.get(j).getN2()
							&& appList.get(i).getN3() == appList.get(j).getN3()
							&& appList.get(i).getN4() == appList.get(j).getN4()
							&& appList.get(i).getN5() == appList.get(j).getN5()
							&& appList.get(i).getN6() == appList.get(j).getN6()
							&& appList.get(i).getN7() == appList.get(j).getN7()
							&& appList.get(i).getN8() == appList.get(j).getN8()
							&& appList.get(i).getN9() == appList.get(j).getN9()) {
						appList.remove(j);
						j--;

					}
				}
			}

			sql = " SELECT t3, SUM(n2) AS records FROM FMR015 WHERE  (t7 between '" + sdate + "' and '" + edate
					+ "' ) and " + "  RecordStatus = 1 group by t3 ";
			PreparedStatement stm = conn.prepareStatement(sql);
			ResultSet resu = stm.executeQuery();
			while (resu.next()) {
				if (resu.getString("t3").equals("Login")) {
					data.setT1(String.valueOf(resu.getLong("records")));
				}
				if (resu.getString("t3").equals("Weather")) {
					data.setT2(String.valueOf(resu.getLong("records")));
				}
				if (resu.getString("t3").equals("Question")) {
					data.setT3(String.valueOf(resu.getLong("records")));
				}
				if (resu.getString("t3").equals("Price")) {
					data.setT4(String.valueOf(resu.getLong("records")));
				}
				if (resu.getString("t3").equals("Education")) {
					data.setT5(String.valueOf(resu.getLong("records")));
				}
				if (resu.getString("t3").equals("News & Media")) {
					data.setT6(String.valueOf(resu.getLong("records")));
				}
				if (resu.getString("t3").equals("Call Center")) {
					data.setT11(String.valueOf(resu.getLong("records")));
				}
				if (resu.getString("t3").equals("Video")) {

					data.setT10(String.valueOf(resu.getLong("records")));
				}
				if (resu.getString("t3").equals("Save Content")) {
					data.setT8(String.valueOf(resu.getLong("records")));
				}

			}
			datalist.add(data);

			data1 = new AppHistoryData[datalist.size()];
			data1 = datalist.toArray(data1);
			res.setCount(data1);

			if (appList.size() > 0) {
				res.setState(true);
			} else {
				res.setState(false);
			}

			AppHistoryData[] dataarray = new AppHistoryData[appList.size()];
			dataarray = appList.toArray(dataarray);
			res.setData(dataarray);
		} else if (frmName.equalsIgnoreCase("user")) {
			sql = " SELECT t7 FROM FMR014 WHERE  (t7 between '" + sdate + "' and '" + edate + "' ) and "
					+ "  RecordStatus = 1 group by t7 ";

			PreparedStatement st = conn.prepareStatement(sql);
			ResultSet result = st.executeQuery();
			while (result.next()) {
				data = new AppHistoryData();
				data.setT7(result.getString("t7"));
				if (data.getT7() != "") {
					sql = "Select * from FMR014 WHERE (t7 between '" + sdate + "' and '" + edate + "' ) and "
							+ "  RecordStatus = 1 and t7 = '" + data.getT7() + "'  ";
					PreparedStatement stm = conn.prepareStatement(sql);
					ResultSet resu = stm.executeQuery();
					while (resu.next()) {
						data = new AppHistoryData();
						data.setSyskey(resu.getLong("syskey"));
						data.setT1(resu.getString("t1"));
						data.setT2(resu.getString("t2"));
						data.setT5(resu.getString("t5"));
						data.setT6(resu.getString("t6"));
						data.setT7(resu.getString("t7"));
						data.setT8(resu.getString("t8"));
						// sql = "select datediff (day, '"+data.getT5()+"',
						// '"+data.getT7()+"') as daytotal , datediff(mi,
						// '"+t6+"' , '"+t8+"') as total";
						sql = "select  datediff(MINUTE, '" + data.getT6() + "' , '" + data.getT8() + "') as total";
						PreparedStatement stm1 = conn.prepareStatement(sql);
						ResultSet re = stm1.executeQuery();
						while (re.next()) {
							data.setT10(re.getString("total"));
						}
						datalist.add(data);
					}

					if (datalist.size() > 0) {
						res.setState(true);
					} else {
						res.setState(false);
					}
					AppHistoryData[] dataarray = new AppHistoryData[datalist.size()];
					dataarray = datalist.toArray(dataarray);
					res.setData(dataarray);
					res.setState(true);
				} else {
					res.setState(false);
				}
			}

		}

		return res;
	}

	// TDA
	public static AppHistoryDataSet searchApplicationHistoryViewList(PagerData pgdata, String searchVal, String date,
			String status, Connection conn) throws SQLException {
		AppHistoryDataSet res = new AppHistoryDataSet();
		ArrayList<AppHistoryData> datalist = new ArrayList<AppHistoryData>();
		String whereclause = " WHERE RecordStatus<>4  AND  RecordStatus = 1 ";

		if (date != "") {
			whereclause += " AND t5= '" + date + "'";
		}
		if (status != "") {
			whereclause += " AND t3 LIKE '%" + status + "%'";
		}

		if (!searchVal.equals("")) {
			whereclause += "AND t3 LIKE '%" + searchVal + "%' OR t5 LIKE '%" + searchVal + "%'  ";
		}
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecordSandE(define("FMR015"), whereclause, " ORDER BY syskey DESC",
				(pgdata.getStart() - 1), pgdata.getEnd(), 0, conn);

		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getDBRecord(dbrs.get(i)));
		}

		res.setPageSize(pgdata.getSize());
		res.setCurrentPage(pgdata.getCurrent());
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}

		PreparedStatement stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM FMR015 " + whereclause);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));

		AppHistoryData[] dataarray = new AppHistoryData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);

		return res;
	}

	public static AppHistoryDataSet searchAppList(PagerData pgdata, String sdate, String edate, String searchVal,
			Connection conn) throws SQLException {
		AppHistoryDataSet res = new AppHistoryDataSet();
		ArrayList<AppHistoryData> datalist = new ArrayList<AppHistoryData>();

		String whereclause = " WHERE RecordStatus = 1   ";

		// String whereclause = " WHERE RecordStatus<>4 AND RecordStatus = 1 AND
		// t4 = '" + type + "' OR t4 = 'question' OR t4 = 'topic' ";

		if (!searchVal.equals("")) {
			whereclause += "AND t1 LIKE '%" + searchVal + "%' AND t2 LIKE '%" + searchVal + "%'  ";
		}
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecordSandE(define(), whereclause, " ORDER BY syskey DESC",
				(pgdata.getStart() - 1), pgdata.getEnd(), 0, conn);

		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getDBRecord(dbrs.get(i)));
		}

		res.setPageSize(pgdata.getSize());
		res.setCurrentPage(pgdata.getCurrent());
		if (datalist.size() > 0) {
			res.setState(true);

		} else {
			res.setState(false);

		}

		PreparedStatement stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM FMR014 " + whereclause);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		AppHistoryData[] dataarray = new AppHistoryData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);

		return res;
	}

	public static AppHistoryDataSet searchAppUserList(PagerData pgdata, String sdate, String edate, String searchVal,
			Connection conn) throws SQLException {
		AppHistoryDataSet res = new AppHistoryDataSet();
		ArrayList<AppHistoryData> datalist = new ArrayList<AppHistoryData>();
		String whereclause = " WHERE RecordStatus = 1 AND (t7 between '" + sdate + "' and '" + edate + "' )  ";
		if (!searchVal.equals("")) {
			whereclause += "AND t1 LIKE '%" + searchVal + "%' AND t2 LIKE '%" + searchVal + "%'  ";
		}
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecordSandE(define(), whereclause, " ORDER BY syskey DESC",
				(pgdata.getStart() - 1), pgdata.getEnd(), 0, conn);

		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getDBRecord(dbrs.get(i)));
		}
		res.setPageSize(pgdata.getSize());
		res.setCurrentPage(pgdata.getCurrent());
		if (datalist.size() > 0) {
			res.setState(true);

		} else {
			res.setState(false);

		}

		PreparedStatement stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM FMR014 " + whereclause);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		AppHistoryData[] dataarray = new AppHistoryData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);

		return res;
	}

	public static long searchCount(AppHistoryData obj, Connection conn)

			throws SQLException {
		long syskey;
		System.out.println(obj.getN1() + " &&& " + obj.getT3());
		String sql = "Select n2 from FMR015 where RecordStatus = 1 AND n1 = " + obj.getN1() + " AND t3 = '"
				+ obj.getT3() + "' ";
		PreparedStatement stmt = conn.prepareStatement(sql);
		// stmt.setString(1, key);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			syskey = rs.getLong("n2");
			System.out.println(syskey + " role");

		} else {
			syskey = 0;
		}
		return syskey;
	}

	public static long searchCounts(AppHistoryData obj, Connection conn)

			throws SQLException {
		long syskey;

		String sql = "Select n2 from FMR014 where RecordStatus = 1 AND n1 = " + obj.getN1();
		PreparedStatement stmt = conn.prepareStatement(sql);
		// stmt.setString(1, key);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			syskey = rs.getLong("n2");

		} else {
			syskey = 0;
		}
		return syskey;
	}

	public static long searchSerial(AppHistoryData obj, Connection conn) {
		long ret = 0;
		String l_Query = "select ISNULL(MAX(n4) + 1, 1) as maxSys FROM fmr015 where RecordStatus = 1 AND t1 = '"
				+ obj.getT1() + "'";
		Statement stmt;
		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(l_Query);
			if (rs.next()) {
				ret = rs.getLong("maxSys");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return ret;
	}

	public static long searchSyskey(AppHistoryData obj, Connection conn) throws SQLException {
		long syskey;

		String sql = "Select syskey from FMR015 where RecordStatus = 1 AND n1 = " + obj.getN1() + " AND t3 = '"
				+ obj.getT3() + "' ";
		PreparedStatement stmt = conn.prepareStatement(sql);

		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			syskey = rs.getLong("syskey");
			System.out.println(syskey + " role");

		} else {
			syskey = 0;
		}
		return syskey;
	}

	public static DBRecord setDBRecord(AppHistoryData data) {
		DBRecord ret = define();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("createddate", data.getCreatedDate());
		ret.setValue("modifieddate", data.getModifiedDate());
		ret.setValue("userid", data.getUserId());
		ret.setValue("username", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("usersyskey", data.getUserSyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t5", data.getT5());
		ret.setValue("t6", data.getT6());
		ret.setValue("t7", data.getT7());
		ret.setValue("t8", data.getT8());
		ret.setValue("t9", data.getT9());
		ret.setValue("t10", data.getT10());
		ret.setValue("t11", data.getT11());
		ret.setValue("t12", data.getT12());
		ret.setValue("t13", data.getT13());
		ret.setValue("t14", data.getT14());
		ret.setValue("t15", data.getT15());
		ret.setValue("t16", data.getT16());
		ret.setValue("t17", data.getT17());
		ret.setValue("t18", data.getT18());
		ret.setValue("t19", data.getT19());
		ret.setValue("t20", data.getT20());
		ret.setValue("t21", data.getT21());
		ret.setValue("t22", data.getT22());
		ret.setValue("t23", data.getT23());
		ret.setValue("t24", data.getT24());
		ret.setValue("t25", data.getT25());
		ret.setValue("t26", data.getT26());
		ret.setValue("t27", data.getT27());
		ret.setValue("t28", data.getT28());
		ret.setValue("t29", data.getT29());
		ret.setValue("t30", data.getT30());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		ret.setValue("n6", data.getN6());
		ret.setValue("n7", data.getN7());
		ret.setValue("n8", data.getN8());
		ret.setValue("n9", data.getN9());
		ret.setValue("n10", data.getN10());
		ret.setValue("n11", data.getN11());
		ret.setValue("n12", data.getN12());
		ret.setValue("n13", data.getN13());
		ret.setValue("n14", data.getN14());
		ret.setValue("n15", data.getN15());
		ret.setValue("n16", data.getN16());
		ret.setValue("n17", data.getN17());
		ret.setValue("n18", data.getN18());
		ret.setValue("n19", data.getN19());
		ret.setValue("n20", data.getN20());
		ret.setValue("n21", data.getN21());
		ret.setValue("n22", data.getN22());
		ret.setValue("n23", data.getN23());
		ret.setValue("n24", data.getN24());
		ret.setValue("n25", data.getN25());
		ret.setValue("n26", data.getN26());
		ret.setValue("n27", data.getN27());
		ret.setValue("n28", data.getN28());
		ret.setValue("n29", data.getN29());
		ret.setValue("n30", data.getN30());
		return ret;
	}

	public static Resultb2b update(AppHistoryData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		// System.out.println(obj.getSyskey() + " " + obj.getT1() + " " +
		// obj.getT2() + " "+ obj.getT3() + " "+ obj.getT4() + " "+ obj.getT6()
		// + " ");
		String sql = DBMgr.updateString(" WHERE RecordStatus = 1 AND n1 =" + obj.getN1(), define(), conn);
		PreparedStatement stmt = conn.prepareStatement(sql);
		DBRecord dbr = setDBRecord(obj);
		DBMgr.setValues(stmt, dbr);
		int count = stmt.executeUpdate();
		if (count > 0) {
			System.out.println(count);
			res.setState(true);
			res.setMsgDesc("Updated Successfully!");
		}
		return res;
	}

	// searchAppAnalysisList with pager
	/*
	 * public static AppHistoryDataSet searchAppAnalysisList(PagerData pgdata,
	 * String searchVal,String sdate,String edate, String frmName, Connection
	 * conn) throws SQLException { AppHistoryDataSet res = new
	 * AppHistoryDataSet(); AppHistoryData data = new AppHistoryData();
	 * AppHistoryData gdata =new AppHistoryData(); AppHistoryData rdata = new
	 * AppHistoryData(); ArrayList<AppHistoryData> datalist = new
	 * ArrayList<AppHistoryData>(); ArrayList<AppHistoryData> appList = new
	 * ArrayList<AppHistoryData>(); AppHistoryData[] data1 ; String sql = "";
	 * String sql1 = ""; if(frmName.equalsIgnoreCase("app")){ sql =
	 * " SELECT t7,t1,t3 FROM FMR015 WHERE  (t7 between '"+sdate+"' and '"
	 * +edate+"' ) and " +(pgdata.getStart()-1)+ pgdata.getEnd() +"and" +
	 * "  RecordStatus = 1group by t7,t1,t3 ";
	 * 
	 * 
	 * sql =
	 * " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY t7 ) AS RowNum,* FROM ( select  f.t7, f.t1, f.t3"
	 * + " from  FMR015 f where recordstatus<>4 and (t7 between '"+sdate+
	 * "' and '"+edate+"' ) group by f.t7,f.t1,f.t3" +
	 * ") b)  AS RowConstrainedResult WHERE RowNum >= '" + (pgdata.getStart() -
	 * 1) + "' AND RowNum <= '" + pgdata.getEnd() + "' ";
	 * 
	 * PreparedStatement st = conn.prepareStatement(sql); ResultSet result =
	 * st.executeQuery(); while(result.next()){
	 * rdata.setT1(result.getString("t1")); rdata.setT3(result.getString("t3"));
	 * rdata.setT7(result.getString("t7")); //select count(t3) as total from
	 * fmr015 where t3='Weather' and t7 = '20170313' and t1='+9595400162' sql1 =
	 * " SELECT t1, t7, t3,  count(t3) as total FROM FMR015 WHERE  (t7 between '"
	 * +sdate+"' and '"+edate+"' ) and " + "  RecordStatus = 1 and t7 = '"
	 * +rdata.getT7()+"' and t1 = '"+rdata.getT1()+"' and t3 = '"+rdata.getT3()+
	 * "'  group by t7,t1,t3 "; PreparedStatement stmt =
	 * conn.prepareStatement(sql1); ResultSet results = stmt.executeQuery();
	 * while(results.next()){ if(gdata.getT1().equals(results.getString("t1"))
	 * && gdata.getT7().equals(results.getString("t7")) ){
	 * gdata.setT1(results.getString("t1"));
	 * gdata.setT7(results.getString("t7"));//date
	 * gdata.setT3(results.getString("t3")); if(gdata.getT3().equals("Login")){
	 * gdata.setN1(Long.parseLong(results.getString("total"))); }
	 * if(gdata.getT3().equals("Weather")){
	 * gdata.setN2(Long.parseLong(results.getString("total"))); }
	 * if(gdata.getT3().equals("Question")){
	 * gdata.setN3(Long.parseLong(results.getString("total"))); }
	 * if(gdata.getT3().equals("Price")){
	 * gdata.setN4(Long.parseLong(results.getString("total"))); }
	 * if(gdata.getT3().equals("Education")){
	 * gdata.setN5(Long.parseLong(results.getString("total"))); }
	 * if(gdata.getT3().equals("News & Media")){
	 * gdata.setN6(Long.parseLong(results.getString("total"))); }
	 * if(gdata.getT3().equals("Call Center")){
	 * gdata.setN10(Long.parseLong(results.getString("total"))); }
	 * if(gdata.getT3().equals("Video")){
	 * gdata.setN7(Long.parseLong(results.getString("total"))); }
	 * if(gdata.getT3().equals("Save Content")){
	 * gdata.setN8(Long.parseLong(results.getString("total"))); }
	 * 
	 * 
	 * appList.add(gdata); } else{ gdata =new AppHistoryData();
	 * gdata.setT1(results.getString("t1"));
	 * gdata.setT7(results.getString("t7"));//date
	 * gdata.setT3(results.getString("t3")); if(gdata.getT3().equals("Login")){
	 * gdata.setN1(Long.parseLong(results.getString("total"))); }
	 * if(gdata.getT3().equals("Weather")){
	 * gdata.setN2(Long.parseLong(results.getString("total"))); }
	 * if(gdata.getT3().equals("Question")){
	 * gdata.setN3(Long.parseLong(results.getString("total"))); }
	 * if(gdata.getT3().equals("Price")){
	 * gdata.setN4(Long.parseLong(results.getString("total"))); }
	 * if(gdata.getT3().equals("Education")){
	 * gdata.setN5(Long.parseLong(results.getString("total"))); }
	 * if(gdata.getT3().equals("News & Media")){
	 * gdata.setN6(Long.parseLong(results.getString("total"))); }
	 * if(gdata.getT3().equals("Call Center")){
	 * gdata.setN10(Long.parseLong(results.getString("total"))); }
	 * if(gdata.getT3().equals("Video")){
	 * gdata.setN7(Long.parseLong(results.getString("total"))); }
	 * if(gdata.getT3().equals("Save Content")){
	 * gdata.setN8(Long.parseLong(results.getString("total"))); }
	 * 
	 * appList.add(gdata); }
	 * 
	 * 
	 * 
	 * }
	 * 
	 * } for(int i=0;i<appList.size();i++){ long n2 =0; int z =0;
	 * 
	 * for(int j=i+1;j<appList.size();j++){
	 * if(appList.get(i).getT1().equals(appList.get(j).getT1()) &&
	 * appList.get(i).getT7().equals(appList.get(j).getT7())){
	 * if(appList.get(j).getT3().equals("Login")){ z = j; n2 =
	 * appList.get(j).getN2(); appList.get(i).setN1(n2);//duplicate login count
	 * appList.remove(j); j--;
	 * 
	 * break; } if(appList.get(j).getT3().equals("Weather")){ z = j; n2 =
	 * appList.get(j).getN2(); appList.get(i).setN2(n2);//duplicate weather
	 * count appList.remove(j); j--;
	 * 
	 * break; } if(appList.get(j).getT3().equals("Question")){ z = j; n2 =
	 * appList.get(j).getN2(); appList.get(i).setN3(n2);
	 * appList.add(appList.get(i));
	 * 
	 * appList.remove(j); j--;
	 * 
	 * break; } if(appList.get(j).getT3().equals("Price")){ z = j; n2 =
	 * appList.get(j).getN2(); appList.get(i).setN4(n2);//duplicate weather
	 * count appList.remove(j); j--;
	 * 
	 * break; } if(appList.get(j).getT3().equals("Education")){ z = j; n2 =
	 * appList.get(j).getN2(); appList.get(i).setN5(n2);//duplicate weather
	 * count
	 * 
	 * appList.remove(j); j--;
	 * 
	 * break; } if(appList.get(j).getT3().equals("News & Media")){ z = j; n2 =
	 * appList.get(j).getN2(); appList.get(i).setN6(n2);//duplicate weather
	 * count
	 * 
	 * appList.remove(j); j--;
	 * 
	 * break; } if(appList.get(j).getT3().equals("Call Center")){ z = j; n2 =
	 * appList.get(j).getN2(); appList.get(i).setN10(n2);//duplicate weather
	 * count
	 * 
	 * appList.remove(j); j--;
	 * 
	 * break; } if(appList.get(j).getT3().equals("Video")){ z = j; n2 =
	 * appList.get(j).getN2(); appList.get(i).setN7(n2);//duplicate weather
	 * count
	 * 
	 * appList.remove(j); j--;
	 * 
	 * break; } if(appList.get(j).getT3().equals("Save Content")){ z = j; n2 =
	 * appList.get(j).getN2(); appList.get(i).setN8(n2);//duplicate weather
	 * count
	 * 
	 * appList.remove(j); j--;
	 * 
	 * break; }
	 * 
	 * 
	 * 
	 * 
	 * } } }
	 * 
	 * for(int i=0;i<appList.size();i++){
	 * 
	 * for(int j=i+1;j<appList.size();j++){
	 * 
	 * if(appList.get(i).getT1().equals(appList.get(j).getT1()) &&
	 * appList.get(i).getT7().equals(appList.get(j).getT7()) &&
	 * appList.get(i).getN1() == appList.get(j).getN1() &&
	 * appList.get(i).getN2()==appList.get(j).getN2() && appList.get(i).getN3()
	 * == appList.get(j).getN3() && appList.get(i).getN4() ==
	 * appList.get(j).getN4() && appList.get(i).getN5() ==
	 * appList.get(j).getN5() && appList.get(i).getN6() ==
	 * appList.get(j).getN6() && appList.get(i).getN7() ==
	 * appList.get(j).getN7() && appList.get(i).getN8() ==
	 * appList.get(j).getN8() && appList.get(i).getN9() ==
	 * appList.get(j).getN9()){ appList.remove(j); j--;
	 * 
	 * } } }
	 * 
	 * 
	 * sql = " SELECT t3, SUM(n2) AS records FROM FMR015 WHERE  (t7 between '"
	 * +sdate+"' and '"+edate+"' ) and " + "  RecordStatus = 1 group by t3 ";
	 * PreparedStatement stm = conn.prepareStatement(sql); ResultSet resu =
	 * stm.executeQuery(); while(resu.next()){
	 * if(resu.getString("t3").equals("Login")){
	 * data.setT1(String.valueOf(resu.getLong("records"))); }
	 * if(resu.getString("t3").equals("Weather")){
	 * data.setT2(String.valueOf(resu.getLong("records"))); }
	 * if(resu.getString("t3").equals("Question")){
	 * data.setT3(String.valueOf(resu.getLong("records"))); }
	 * if(resu.getString("t3").equals("Price")){
	 * data.setT4(String.valueOf(resu.getLong("records"))); }
	 * if(resu.getString("t3").equals("Education")){
	 * data.setT5(String.valueOf(resu.getLong("records"))); }
	 * if(resu.getString("t3").equals("News & Media")){
	 * data.setT6(String.valueOf(resu.getLong("records"))); }
	 * if(resu.getString("t3").equals("Call Center")){
	 * data.setT11(String.valueOf(resu.getLong("records"))); }
	 * if(resu.getString("t3").equals("Video")){
	 * 
	 * data.setT10(String.valueOf(resu.getLong("records"))); }
	 * if(resu.getString("t3").equals("Save Content")){
	 * data.setT8(String.valueOf(resu.getLong("records"))); }
	 * 
	 * } datalist.add(data); data1 = new AppHistoryData[datalist.size()]; data1
	 * = datalist.toArray(data1); res.setCount(data1);
	 * 
	 * if (appList.size() > 0) { res.setState(true); } else {
	 * res.setState(false); } PreparedStatement statcount =
	 * conn.prepareStatement(
	 * "SELECT DISTINCT COUNT(*) OVER () AS recCount FROM FMR015 where recordstatus<>4 and (t7 between '"
	 * +sdate+"' and '"+edate+"' )group by t7, t1, t3 ");
	 * 
	 * ResultSet result1 = statcount.executeQuery(); result1.next();
	 * res.setTotalCount(result1.getInt("recCount")); AppHistoryData[] dataarray
	 * = new AppHistoryData[appList.size()]; dataarray =
	 * appList.toArray(dataarray); res.setData(dataarray); } else if
	 * (frmName.equalsIgnoreCase("user")){ sql =
	 * " SELECT t7 FROM FMR014 WHERE  (t7 between '"+sdate+"' and '"+edate+
	 * "' ) and " + "  RecordStatus = 1 group by t7 ";
	 * 
	 * PreparedStatement st = conn.prepareStatement(sql); ResultSet result =
	 * st.executeQuery(); while(result.next()){ data = new AppHistoryData();
	 * data.setT7(result.getString("t7")); if(data.getT7()!=""){ sql =
	 * "Select * from FMR014 WHERE (t7 between '"+sdate+"' and '"+edate+
	 * "' ) and " + "  RecordStatus = 1 and t7 = '"+data.getT7()+"'  ";
	 * PreparedStatement stm = conn.prepareStatement(sql); ResultSet resu =
	 * stm.executeQuery(); while(resu.next()){ data = new AppHistoryData();
	 * data.setSyskey(resu.getLong("syskey")); data.setT1(resu.getString("t1"));
	 * data.setT2(resu.getString("t2")); data.setT5(resu.getString("t5"));
	 * data.setT6(resu.getString("t6")); data.setT7(resu.getString("t7"));
	 * data.setT8(resu.getString("t8")); //sql = "select datediff (day, '"
	 * +data.getT5()+"', '"+data.getT7()+"') as daytotal , datediff(mi, '"+t6+
	 * "' , '"+t8+"') as total"; sql = "select  datediff(MINUTE, '"+
	 * data.getT6()+"' , '"+ data.getT8()+"') as total"; PreparedStatement stm1
	 * = conn.prepareStatement(sql); ResultSet re = stm1.executeQuery();
	 * while(re.next()){ data.setT10(re.getString("total")); }
	 * datalist.add(data); }
	 * 
	 * 
	 * if (datalist.size() > 0) { res.setState(true); } else {
	 * res.setState(false); } AppHistoryData[] dataarray = new
	 * AppHistoryData[datalist.size()]; dataarray = datalist.toArray(dataarray);
	 * res.setData(dataarray); res.setState(true); } else{ res.setState(false);
	 * } }
	 * 
	 * }
	 * 
	 * return res; }
	 */

	public static Resultb2b updateDatail(AppHistoryData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();

		String sqlString = "";

		sqlString = "UPDATE FMR015 SET n2='" + obj.getN2() + "',  t7='" + obj.getT7() + "',t8='" + obj.getT8()
				+ "'  where RecordStatus=1 AND n1 = '" + obj.getN1() + "' ";

		PreparedStatement st = conn.prepareStatement(sqlString);
		int result = st.executeUpdate();
		if (result > 0) {
			System.out.println(result);
			res.setState(true);
			res.setMsgDesc("Updated Successfully!");
		}
		return res;
	}

	public static Resultb2b updateLogin(AppHistoryData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		System.out.println(obj.getSyskey() + " " + obj.getT1() + " " + obj.getT8() + " logout time " + obj.getT6() + " "
				+ obj.getT5() + " " + obj.getT7() + " ");
		String sqlString = "";
		if (obj.getT6().equals("") && obj.getT5().equals("")) {
			System.out.println("logout");
			sqlString = "UPDATE FMR014 SET n2='" + obj.getN2() + "', t7='" + obj.getT7() + "',t8='" + obj.getT8()
					+ "' where RecordStatus=1 AND n1 = '" + obj.getN1() + "' ";
		} else {
			System.out.println("login");
			sqlString = "UPDATE FMR014 SET n2='" + obj.getN2() + "', t5='" + obj.getT5() + "',t6='" + obj.getT6()
					+ "'  where RecordStatus=1 AND n1 = '" + obj.getN1() + "' ";
		}
		PreparedStatement st = conn.prepareStatement(sqlString);
		int result = st.executeUpdate();
		if (result > 0) {
			System.out.println(result);
			res.setState(true);
			res.setMsgDesc("Updated Successfully!");
		}
		return res;
	}

	public static Resultb2b updateLogoutTime(AppHistoryData obj, long num, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();

		String sqlString = "";

		sqlString = "UPDATE FMR015 SET t9='" + obj.getT9() + "',  t10='" + obj.getT10()
				+ "' where RecordStatus=1 AND syskey = '" + obj.getN3() + "' ";

		PreparedStatement st = conn.prepareStatement(sqlString);
		int result = st.executeUpdate();
		if (result > 0) {
			String sql1 = "UPDATE FMR015 SET t9 ='" + obj.getT9() + "', t10 ='" + obj.getT10()
					+ "'  WHERE RecordStatus = 1 AND n4=" + num + " AND t1 ='" + obj.getT1() + "' ";
			PreparedStatement stm = conn.prepareStatement(sql1);
			int rs = stm.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				res.setMsgDesc("Saved Successfully!");
			}

		}
		return res;
	}

	public static Resultb2b updateTypeHistory(AppHistoryData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		// System.out.println(obj.getSyskey() + " " + obj.getT1() + " " +
		// obj.getT2() + " "+ obj.getT3() + " "+ obj.getT4() + " "+ obj.getT6()
		// + " ");
		String sql = DBMgr.updateString(" WHERE RecordStatus = 1 AND n1 =" + obj.getN1(), define("FMR015"), conn);
		PreparedStatement stmt = conn.prepareStatement(sql);
		DBRecord dbr = setDBRecord(obj);
		DBMgr.setValues(stmt, dbr);
		int count = stmt.executeUpdate();
		if (count > 0) {
			System.out.println(count);
			res.setState(true);
			res.setMsgDesc("Updated Successfully!");
		}
		return res;
	}

}
