package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;

import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.AcademicData;
import com.nirvasoft.cms.shared.AcademicDataSet;
import com.nirvasoft.cms.shared.AdvancedSearchData;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;

public class AcademicDao {

	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("ACADEMICREFERENCE");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		/*
		 * ret.getFields().add(new DBField("t11", (byte) 5));
		 * ret.getFields().add(new DBField("t12", (byte) 5));
		 * ret.getFields().add(new DBField("t13", (byte) 5));
		 * ret.getFields().add(new DBField("t14", (byte) 5));
		 * ret.getFields().add(new DBField("t15", (byte) 5));
		 * ret.getFields().add(new DBField("t16", (byte) 5));
		 * ret.getFields().add(new DBField("t17", (byte) 5));
		 * ret.getFields().add(new DBField("t18", (byte) 5));
		 * ret.getFields().add(new DBField("t19", (byte) 5));
		 * ret.getFields().add(new DBField("t20", (byte) 5));
		 */
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		/*
		 * ret.getFields().add(new DBField("n11", (byte) 2));
		 * ret.getFields().add(new DBField("n12", (byte) 2));
		 * ret.getFields().add(new DBField("n13", (byte) 2));
		 * ret.getFields().add(new DBField("n14", (byte) 2));
		 * ret.getFields().add(new DBField("n15", (byte) 2));
		 * ret.getFields().add(new DBField("n16", (byte) 2));
		 * ret.getFields().add(new DBField("n17", (byte) 2));
		 * ret.getFields().add(new DBField("n18", (byte) 2));
		 * ret.getFields().add(new DBField("n19", (byte) 2));
		 * ret.getFields().add(new DBField("n20", (byte) 2));
		 */
		return ret;
	}

	public static DBRecord define(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		/*
		 * ret.getFields().add(new DBField("t11", (byte) 5));
		 * ret.getFields().add(new DBField("t12", (byte) 5));
		 * ret.getFields().add(new DBField("t13", (byte) 5));
		 * ret.getFields().add(new DBField("t14", (byte) 5));
		 * ret.getFields().add(new DBField("t15", (byte) 5));
		 * ret.getFields().add(new DBField("t16", (byte) 5));
		 * ret.getFields().add(new DBField("t17", (byte) 5));
		 * ret.getFields().add(new DBField("t18", (byte) 5));
		 * ret.getFields().add(new DBField("t19", (byte) 5));
		 * ret.getFields().add(new DBField("t20", (byte) 5));
		 */
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		/*
		 * ret.getFields().add(new DBField("n11", (byte) 2));
		 * ret.getFields().add(new DBField("n12", (byte) 2));
		 * ret.getFields().add(new DBField("n13", (byte) 2));
		 * ret.getFields().add(new DBField("n14", (byte) 2));
		 * ret.getFields().add(new DBField("n15", (byte) 2));
		 * ret.getFields().add(new DBField("n16", (byte) 2));
		 * ret.getFields().add(new DBField("n17", (byte) 2));
		 * ret.getFields().add(new DBField("n18", (byte) 2));
		 * ret.getFields().add(new DBField("n19", (byte) 2));
		 * ret.getFields().add(new DBField("n20", (byte) 2));
		 */
		return ret;
	}

	public static Resultb2b delete(Long syskey, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = "UPDATE ACADEMICREFERENCE SET RecordStatus=4 WHERE syskey=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Successfully");
		} else {
			res.setMsgDesc("Deleting Unsuccessful");
		}
		return res;
	}

	public static String getAutoGenCode(Connection conn) throws SQLException {

		String ret = "";
		String sql = "SELECT ISNULL(MAX(CAST(SUBSTRING(t1,5,LEN(t1)) AS bigint)),0) AS ret FROM ACADEMICREFERENCE";
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		rs.next();
		int result = rs.getInt("ret") + 1;
		DecimalFormat df = new DecimalFormat("000");
		ret = "AC-" + df.format(result).toString();

		return ret;

	}

	/*
	 * public static Result insert(AcademicData aObj, Connection aConnection)
	 * throws SQLException { Result res = new Result(); if (!isIDexist(aObj,
	 * aConnection)) { String sql = DBMgr.insertString(define(), aConnection);
	 * PreparedStatement stmt = aConnection.prepareStatement(sql); DBRecord dbr
	 * = setDBRecord(aObj); DBMgr.setValues(stmt, dbr); stmt.executeUpdate();
	 * res.setMsgDesc("Saved Successfully!"); res.setMsgCode(aObj.getT1());
	 * res.setState(true); return res; } else { res.setMsgDesc(
	 * "Code already exist!"); res.setState(false); return res; } }
	 */

	public static AcademicData getDBRecord(DBRecord adbr) {
		AcademicData ret = new AcademicData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("createddate"));
		ret.setModifiedDate(adbr.getString("modifieddate"));
		ret.setUserId(adbr.getString("userid"));
		ret.setUserName(adbr.getString("username"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setUserSyskey(adbr.getLong("usersyskey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		ret.setT8(adbr.getString("t8"));
		ret.setT9(adbr.getString("t9"));
		ret.setT10(adbr.getString("t10"));
		/*
		 * ret.setT11(adbr.getString("t11")); ret.setT12(adbr.getString("t12"));
		 * ret.setT13(adbr.getString("t13")); ret.setT14(adbr.getString("t14"));
		 * ret.setT15(adbr.getString("t15")); ret.setT16(adbr.getString("t16"));
		 * ret.setT17(adbr.getString("t17")); ret.setT18(adbr.getString("t18"));
		 * ret.setT19(adbr.getString("t19")); ret.setT20(adbr.getString("t20"));
		 */
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getLong("n5"));
		ret.setN6(adbr.getLong("n6"));
		ret.setN7(adbr.getLong("n7"));
		ret.setN8(adbr.getLong("n8"));
		ret.setN9(adbr.getLong("n9"));
		ret.setN10(adbr.getLong("n10"));
		/*
		 * ret.setN11(adbr.getLong("n11")); ret.setN12(adbr.getLong("n12"));
		 * ret.setN13(adbr.getLong("n13")); ret.setN14(adbr.getLong("n14"));
		 * ret.setN15(adbr.getLong("n15")); ret.setN16(adbr.getLong("n16"));
		 * ret.setN17(adbr.getLong("n17")); ret.setN18(adbr.getLong("n18"));
		 * ret.setN19(adbr.getLong("n19")); ret.setN20(adbr.getLong("n20"));
		 */
		return ret;
	}

	/*
	 * public static Result update(AcademicData obj, Connection conn) throws
	 * SQLException { Result res = new Result(); if (!isCodeExist(obj, conn)) {
	 * String sql = DBMgr.updateString(" WHERE RecordStatus<>4 AND Syskey=" +
	 * obj.getSyskey(), define(), conn); PreparedStatement stmt =
	 * conn.prepareStatement(sql); DBRecord dbr = setDBRecord(obj);
	 * DBMgr.setValues(stmt, dbr); int count = stmt.executeUpdate();
	 * 
	 * if (count > 0) { res.setState(true); res.setMsgDesc(
	 * "Updated Successfully!"); } } else { res.setMsgDesc(
	 * "No Such State to Update!"); } res.setMsgCode(obj.getT1()); return res; }
	 */

	public static String getSerial(Connection aConn) throws SQLException {
		String l_Key = "";
		String l_Query = " SELECT MAX(n1) as n1  from ACADEMICREFERENCE";
		PreparedStatement pstmt = aConn.prepareStatement(l_Query);
		ResultSet rs = pstmt.executeQuery();
		int plus = 0;
		while (rs.next()) {
			l_Key = rs.getString("n1");
		}
		if (l_Key == null || l_Key == "") {
			l_Key = "11111";
		} else {
			plus = Integer.parseInt(l_Key) + 1;
			l_Key = String.valueOf(plus);
		}
		return l_Key;
	}

	public static String getSyskey(String tablename, Connection aConn) throws SQLException {
		String l_Key = "";
		String l_Query = " SELECT MAX(syskey) as syskey  from " + tablename;
		PreparedStatement pstmt = aConn.prepareStatement(l_Query);
		ResultSet rs = pstmt.executeQuery();
		int plus = 0;
		while (rs.next()) {
			l_Key = rs.getString("syskey");
		}
		if (l_Key == null || l_Key == "") {
			l_Key = "11111";
		} else {
			plus = Integer.parseInt(l_Key) + 1;
			l_Key = String.valueOf(plus);
		}
		return l_Key;
	}

	public static Resultb2b insert(AcademicData aObj, Connection aConnection) throws SQLException {
		Resultb2b res = new Resultb2b();
		String query = "INSERT INTO ACADEMICREFERENCE (syskey, createddate, modifieddate, userid, username,"
				+ " recordStatus, syncStatus, syncBatch,usersyskey,t1, t2, t3, t4, t5, t6, t7, t8, t9, t10,"
				+ "n1, n2, n3, n4, n5, n6, n7, n8, n9, n10)" + " VALUES (?,?,?,?,?,?,?,?,?,?," + "?,?,?,?,?,?,?,?,?,?,"
				+ "?,?,?,?,?,?,?,?,?)";

		try {
			if (!isCodeExist(aObj, aConnection)) {
				PreparedStatement ps = aConnection.prepareStatement(query);
				ps.setLong(1, aObj.getSyskey());
				ps.setString(2, aObj.getCreatedDate());
				ps.setString(3, aObj.getModifiedDate());
				ps.setString(4, aObj.getUserId());
				ps.setString(5, aObj.getUserName());
				ps.setInt(6, aObj.getRecordStatus());
				ps.setInt(7, aObj.getSyncStatus());
				ps.setLong(8, aObj.getSyncBatch());
				ps.setLong(9, aObj.getUserSyskey());
				ps.setString(10, aObj.getT1());
				ps.setString(11, aObj.getT2());
				ps.setString(12, aObj.getT3());
				ps.setString(13, aObj.getT4());
				ps.setString(14, aObj.getT5());
				ps.setString(15, aObj.getT6());
				ps.setString(16, aObj.getT7());
				ps.setString(17, aObj.getT8());
				ps.setString(18, aObj.getT9());
				ps.setString(19, aObj.getT10());
				ps.setLong(20, aObj.getN1());
				ps.setLong(21, aObj.getN2());
				ps.setLong(22, aObj.getN3());
				ps.setLong(23, aObj.getN4());
				ps.setLong(24, aObj.getN5());
				ps.setLong(25, aObj.getN6());
				ps.setLong(26, aObj.getN7());
				ps.setLong(27, aObj.getN8());
				ps.setLong(28, aObj.getN9());
				ps.setLong(29, aObj.getN10());

				if (ps.executeUpdate() > 0) {
					res.setMsgDesc("Saved Successfully!");
					res.setMsgCode(aObj.getT1());
					res.setState(true);
					return res;
				} else
					res.setMsgDesc("Save Fail");
			} else {
				res.setMsgDesc("Code already exist!");
				res.setState(false);
				return res;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			res.setMsgDesc("Save Fail");
		}
		return res;

	}

	public static boolean isCodeExist(AcademicData obj, Connection conn) throws SQLException {

		ArrayList<AcademicData> dataList = new ArrayList<AcademicData>();

		String l_Query = " SELECT * FROM ACADEMICREFERENCE WHERE RecordStatus<>4 AND syskey <> " + obj.getSyskey()
				+ " AND (t1='" + obj.getT1() + "')";

		PreparedStatement pstmt = conn.prepareStatement(l_Query);
		ResultSet rs = pstmt.executeQuery();

		while (rs.next()) {

			AcademicData data = new AcademicData();
			data.setT1(rs.getString("t1"));
			dataList.add(data);
		}

		if (dataList.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isIDexist(AcademicData aObj, Connection conn) {
		boolean b = false;
		String sqlString = "";
		sqlString = "select * from ACADEMICREFERENCE where syskey='" + aObj.getSyskey() + "'";
		try {
			Statement stmts = conn.createStatement();
			ResultSet rs = stmts.executeQuery(sqlString);
			if (rs.next()) {
				b = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return b;
	}

	public static AcademicData read(long syskey, Connection conn) throws SQLException {
		AcademicData ret = new AcademicData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "where RecordStatus<>4 AND syskey=" + syskey, "", conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));
		return ret;
	}

	public static AcademicDataSet search(AdvancedSearchData asdata, String searchVal, String sort, String type,
			Connection conn) throws SQLException {
		AcademicDataSet res = new AcademicDataSet();
		ArrayList<AcademicData> datalist = new ArrayList<AcademicData>();
		String whereclause = " WHERE RecordStatus<>4 ";
		String orderclause = "";

		if (type.equals("1")) {
			orderclause += " ORDER BY t1 ";
		} else if (type.equals("2")) {
			orderclause += " ORDER BY t2 ";
		} else if (type.equals("3")) {
			orderclause += " ORDER BY t3 ";
		}
		if (!orderclause.isEmpty()) {
			if (sort.equals("asc")) {
				orderclause += " asc ";
			} else {
				orderclause += " desc ";
			}
		}
		PagerData pgdata = asdata.getPager();
		if (!searchVal.equals("")) {
			whereclause += "AND ( t1 LIKE '%" + searchVal + "%' OR t2 LIKE '%" + searchVal + "%' OR t3 LIKE '%"
					+ searchVal + "%' OR t4 LIKE '%" + searchVal + "%')";
		}
		int start = pgdata.getStart() - 1;
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecordSandE(define(), whereclause, orderclause, start, pgdata.getEnd(), 0,
				conn);

		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getDBRecord(dbrs.get(i)));
		}

		res.setPageSize(pgdata.getSize());
		res.setCurrentPage(pgdata.getCurrent());
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}

		PreparedStatement stat = conn
				.prepareStatement("SELECT COUNT(*) AS recCount FROM ACADEMICREFERENCE " + whereclause);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));

		AcademicData[] dataarray = new AcademicData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setArr(dataarray);

		return res;
	}

	public static DBRecord setDBRecord(AcademicData data) {
		DBRecord ret = define();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("createddate", data.getCreatedDate());
		ret.setValue("modifieddate", data.getModifiedDate());
		ret.setValue("userid", data.getUserId());
		ret.setValue("username", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("usersyskey", data.getUserSyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t5", data.getT5());
		ret.setValue("t6", data.getT6());
		ret.setValue("t7", data.getT7());
		ret.setValue("t8", data.getT8());
		ret.setValue("t9", data.getT9());
		ret.setValue("t10", data.getT10());
		/*
		 * ret.setValue("t11", data.getT11()); ret.setValue("t12",
		 * data.getT12()); ret.setValue("t13", data.getT13());
		 * ret.setValue("t14", data.getT4()); ret.setValue("t15", data.getT5());
		 * ret.setValue("t16", data.getT6()); ret.setValue("t17", data.getT7());
		 * ret.setValue("t18", data.getT8()); ret.setValue("t19", data.getT9());
		 * ret.setValue("t20", data.getT10());
		 */
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		ret.setValue("n6", data.getN6());
		ret.setValue("n7", data.getN7());
		ret.setValue("n8", data.getN8());
		ret.setValue("n9", data.getN9());
		ret.setValue("n10", data.getN10());
		/*
		 * ret.setValue("n11", data.getN11()); ret.setValue("n12",
		 * data.getN12()); ret.setValue("n13", data.getN13());
		 * ret.setValue("n14", data.getN4()); ret.setValue("n15", data.getN5());
		 * ret.setValue("n16", data.getN6()); ret.setValue("n17", data.getN7());
		 * ret.setValue("n18", data.getN8()); ret.setValue("n19", data.getN9());
		 * ret.setValue("n20", data.getN10());
		 */
		return ret;
	}

	public static Resultb2b update(AcademicData aObj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String query = "UPDATE ACADEMICREFERENCE SET [syskey]=?, [createddate]=?, [modifieddate]=?, [userid]=?, [username]=?, "
				+ " [recordstatus]=?, [syncstatus]=?, [syncbatch]=?,[usersyskey]=?, [t1]=?, [t2]=?, [t3]=?,[t4]=?, [t5]=?,"
				+ " [t6]=?, [t7]=?,[t8]=?, [t9]=?,[t10]=?,"
				+ " [n1]=?, [n2]=?, [n3]=?,[n4]=?, [n5]=?,[n6]=?, [n7]=?,[n8]=?, [n9]=?,[n10]=?"
				+ "WHERE RecordStatus<>4 AND RecordStatus = 1 AND Syskey=" + aObj.getSyskey() + "";

		try {
			if (!isCodeExist(aObj, conn)) {
				PreparedStatement ps = conn.prepareStatement(query);
				ps.setLong(1, aObj.getSyskey());
				ps.setString(2, aObj.getCreatedDate());
				ps.setString(3, aObj.getModifiedDate());
				ps.setString(4, aObj.getUserId());
				ps.setString(5, aObj.getUserName());
				ps.setInt(6, aObj.getRecordStatus());
				ps.setInt(7, aObj.getSyncStatus());
				ps.setLong(8, aObj.getSyncBatch());
				ps.setLong(9, aObj.getUserSyskey());
				ps.setString(10, aObj.getT1());
				ps.setString(11, aObj.getT2());
				ps.setString(12, aObj.getT3());
				ps.setString(13, aObj.getT4());
				ps.setString(14, aObj.getT5());
				ps.setString(15, aObj.getT6());
				ps.setString(16, aObj.getT7());
				ps.setString(17, aObj.getT8());
				ps.setString(18, aObj.getT9());
				ps.setString(19, aObj.getT10());
				ps.setLong(20, aObj.getN1());
				ps.setLong(21, aObj.getN2());
				ps.setLong(22, aObj.getN3());
				ps.setLong(23, aObj.getN4());
				ps.setLong(24, aObj.getN5());
				ps.setLong(25, aObj.getN6());
				ps.setLong(26, aObj.getN7());
				ps.setLong(27, aObj.getN8());
				ps.setLong(28, aObj.getN9());
				ps.setLong(29, aObj.getN10());

				if (ps.executeUpdate() > 0) {
					res.setState(true);
					res.setMsgDesc("Updated Successfully!");
				}
			} else {
				res.setMsgDesc("No Such State to Update!");
			}
		} catch (SQLException e) {
		}
		res.setMsgCode(aObj.getT1());
		return res;
	}
}
