package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.cms.shared.RegisterData;
import com.nirvasoft.cms.shared.RegisterDataSet;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;
import com.nirvasoft.rp.shared.SessionData;
import com.nirvasoft.rp.shared.WalletCheckRes;

public class RegisterDao {
	public static DBRecord define() { // Define Database fields
		DBRecord ret = new DBRecord();
		ret.setTableName("Register"); // Table Name
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("T1", (byte) 5));
		ret.getFields().add(new DBField("T2", (byte) 5));
		ret.getFields().add(new DBField("T3", (byte) 5));
		ret.getFields().add(new DBField("T4", (byte) 5));
		ret.getFields().add(new DBField("T5", (byte) 5));
		ret.getFields().add(new DBField("T6", (byte) 5));
		ret.getFields().add(new DBField("T7", (byte) 5));
		ret.getFields().add(new DBField("T8", (byte) 5));
		ret.getFields().add(new DBField("T9", (byte) 5));
		ret.getFields().add(new DBField("T10", (byte) 5));
		ret.getFields().add(new DBField("T11", (byte) 5));
		ret.getFields().add(new DBField("T12", (byte) 5));
		ret.getFields().add(new DBField("T13", (byte) 5));
		ret.getFields().add(new DBField("T14", (byte) 5));
		ret.getFields().add(new DBField("T15", (byte) 5));
		ret.getFields().add(new DBField("T16", (byte) 5));
		ret.getFields().add(new DBField("T17", (byte) 5));
		ret.getFields().add(new DBField("T18", (byte) 5));
		ret.getFields().add(new DBField("T19", (byte) 5));
		ret.getFields().add(new DBField("T20", (byte) 5));
		ret.getFields().add(new DBField("T21", (byte) 5));
		ret.getFields().add(new DBField("T22", (byte) 5));
		ret.getFields().add(new DBField("T23", (byte) 5));
		ret.getFields().add(new DBField("T24", (byte) 5));
		ret.getFields().add(new DBField("T25", (byte) 5));
		ret.getFields().add(new DBField("T26", (byte) 5));
		ret.getFields().add(new DBField("T27", (byte) 5));
		ret.getFields().add(new DBField("T28", (byte) 5));
		ret.getFields().add(new DBField("T29", (byte) 5));
		ret.getFields().add(new DBField("T30", (byte) 5));
		ret.getFields().add(new DBField("T31", (byte) 5));
		ret.getFields().add(new DBField("T32", (byte) 5));
		ret.getFields().add(new DBField("T33", (byte) 5));
		ret.getFields().add(new DBField("T34", (byte) 5));
		ret.getFields().add(new DBField("T35", (byte) 5));
		ret.getFields().add(new DBField("T36", (byte) 5));
		ret.getFields().add(new DBField("T37", (byte) 5));
		ret.getFields().add(new DBField("T38", (byte) 5));
		ret.getFields().add(new DBField("T39", (byte) 5));
		ret.getFields().add(new DBField("T40", (byte) 5));
		ret.getFields().add(new DBField("T41", (byte) 5));
		ret.getFields().add(new DBField("T42", (byte) 5));
		ret.getFields().add(new DBField("T43", (byte) 5));
		ret.getFields().add(new DBField("T44", (byte) 5));
		ret.getFields().add(new DBField("T45", (byte) 5));
		ret.getFields().add(new DBField("T46", (byte) 5));
		ret.getFields().add(new DBField("T47", (byte) 5));
		ret.getFields().add(new DBField("T48", (byte) 5));
		ret.getFields().add(new DBField("T49", (byte) 5));
		ret.getFields().add(new DBField("T50", (byte) 5));
		ret.getFields().add(new DBField("T51", (byte) 5));
		ret.getFields().add(new DBField("T52", (byte) 5));
		ret.getFields().add(new DBField("T53", (byte) 5));
		ret.getFields().add(new DBField("T54", (byte) 5));
		ret.getFields().add(new DBField("T55", (byte) 5));
		ret.getFields().add(new DBField("T56", (byte) 5));
		ret.getFields().add(new DBField("T57", (byte) 5));
		ret.getFields().add(new DBField("T58", (byte) 5));
		ret.getFields().add(new DBField("T59", (byte) 5));
		ret.getFields().add(new DBField("T60", (byte) 5));
		ret.getFields().add(new DBField("T61", (byte) 5));
		ret.getFields().add(new DBField("T62", (byte) 5));
		ret.getFields().add(new DBField("T63", (byte) 5));
		ret.getFields().add(new DBField("T64", (byte) 5));
		ret.getFields().add(new DBField("T65", (byte) 5));
		ret.getFields().add(new DBField("T66", (byte) 5));
		return ret;
	}

	public static Resultb2b delete(Long syskey, RegisterData p, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = "UPDATE Register SET RecordStatus=4 WHERE RecordStatus = 1 AND syskey=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Successfully");
			if (res.isState()) {
				String query = "UPDATE JUN002 SET RecordStatus=4 WHERE RecordStatus = 1 AND n1='" + p.getUsersyskey()
						+ "'";
				PreparedStatement stm = conn.prepareStatement(query);
				int result = stm.executeUpdate();
				if (result > 0) {
					res.setState(true);
					res.setMsgDesc("Deleted Successfully");
					if (res.isState()) {
						String qu = "UPDATE UVM005 SET RecordStatus=4 WHERE RecordStatus = 1 AND syskey='"
								+ p.getUsersyskey() + "'";
						PreparedStatement st = conn.prepareStatement(qu);
						int resu = st.executeUpdate();
						if (resu > 0) {
							boolean flag = delete12(p, conn);
							if (flag) {
								res.setState(true);
								res.setMsgDesc("Deleted Successfully");
							}
						}
					}
				} /*
					 * else { res.setState(false); res.setMsgDesc(
					 * "Deleting Unsuccessful"); }
					 */
			}
		} else {
			res.setMsgDesc("Deleting Unsuccessful");
		}
		return res;
	}

	public static boolean delete12(RegisterData aObj, Connection conn) {// UVM012
		boolean b = false;
		String sqlString = "";
		sqlString = "UPDATE UVM012 SET RecordStatus=4 WHERE RecordStatus = 1 AND t1='" + aObj.getT1() + "'";
		try {
			PreparedStatement st = conn.prepareStatement(sqlString);
			int resu = st.executeUpdate();
			if (resu > 0) {
				b = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return b;
	}

	// Reg Exoport
	public static RegisterDataSet ExporReg(String searchVal, String type, Connection conn) throws SQLException {
		RegisterDataSet res = new RegisterDataSet();
		ArrayList<RegisterData> datalist = new ArrayList<RegisterData>();
		String whereclause = " WHERE RecordStatus<>4  AND  RecordStatus = 1 ";
		if (!searchVal.equals("")) {
			whereclause += "AND t1 LIKE '%" + searchVal + "%' OR t3 LIKE '%" + searchVal + "%'OR t23 LIKE '%"
					+ searchVal + "%' OR t5 LIKE '%" + searchVal + "%' " + "  OR t6 LIKE '%" + searchVal
					+ "%'OR t7 LIKE '%" + searchVal + "%'OR t8 LIKE '%" + searchVal + "%'" + " OR t2 LIKE '%"
					+ searchVal + "%'" + " OR t58 LIKE '%" + searchVal + "%'";
		}
		String sql = "select * from register " + whereclause;
		PreparedStatement st = conn.prepareStatement(sql);
		ResultSet result = st.executeQuery();
		while (result.next()) {
			RegisterData ret = new RegisterData();
			ret.setT1(result.getString("t1"));
			ret.setT3(result.getString("t3"));
			ret.setT23(result.getString("t23"));
			ret.setModifieddate(result.getString("modifieddate"));
			ret.setT5(result.getString("t5"));
			ret.setT6(result.getString("t6"));
			ret.setT7(result.getString("t7"));
			ret.setT8(result.getString("t8"));
			/*
			 * ret.setT22(result.getString("t22"));
			 * ret.setT9(result.getString("t9"));
			 * ret.setT10(result.getString("t10"));
			 * ret.setT11(result.getString("t11"));
			 * ret.setT12(result.getString("t12"));
			 * ret.setT18(result.getString("t18"));
			 * ret.setT19(result.getString("t19"));
			 */
			ret.setT58(result.getString("t58"));
			datalist.add(ret);
		}

		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		PreparedStatement stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM Register " + whereclause);
		ResultSet result1 = stat.executeQuery();
		result1.next();
		res.setTotalCount(result1.getInt("recCount"));
		RegisterData[] dataarray = new RegisterData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	public static RegisterData getDBRecord(DBRecord aDBRecord) { // Map DB to
		// Entity/Class
		RegisterData ret = new RegisterData();
		ret.setSyskey(aDBRecord.getLong("syskey"));
		ret.setCreateddate(aDBRecord.getString("createddate"));
		ret.setModifieddate(aDBRecord.getString("modifieddate"));
		ret.setUserid(aDBRecord.getString("userid"));
		ret.setUsername(aDBRecord.getString("username"));
		ret.setRecordStatus(aDBRecord.getInt("RecordStatus"));
		ret.setSyncStatus(aDBRecord.getInt("SyncStatus"));
		ret.setSyncBatch(aDBRecord.getLong("SyncBatch"));
		ret.setUsersyskey(aDBRecord.getLong("usersyskey"));
		ret.setT1(aDBRecord.getString("T1"));
		ret.setT2(aDBRecord.getString("T2"));
		ret.setT3(aDBRecord.getString("T3"));
		ret.setT4(aDBRecord.getString("T4"));
		ret.setT5(aDBRecord.getString("T5"));
		ret.setT6(aDBRecord.getString("T6"));
		ret.setT7(aDBRecord.getString("T7"));
		ret.setT8(aDBRecord.getString("T8"));
		ret.setT9(aDBRecord.getString("T9"));
		ret.setT10(aDBRecord.getString("T10"));
		ret.setT11(aDBRecord.getString("T11"));
		ret.setT12(aDBRecord.getString("T12"));
		ret.setT13(aDBRecord.getString("T13"));
		ret.setT14(aDBRecord.getString("T14"));
		ret.setT15(aDBRecord.getString("T15"));
		ret.setT16(aDBRecord.getString("T16"));
		ret.setT17(aDBRecord.getString("T17"));
		ret.setT18(aDBRecord.getString("T18"));
		ret.setT19(aDBRecord.getString("T19"));
		ret.setT20(aDBRecord.getString("T20"));
		ret.setT21(aDBRecord.getString("T21"));
		ret.setT22(aDBRecord.getString("T22"));
		ret.setT23(aDBRecord.getString("T23"));
		ret.setT24(aDBRecord.getString("T24"));
		ret.setT25(aDBRecord.getString("T25"));
		ret.setT26(aDBRecord.getString("T26"));
		ret.setT27(aDBRecord.getString("T27"));
		ret.setT28(aDBRecord.getString("T28"));
		ret.setT29(aDBRecord.getString("T29"));
		ret.setT30(aDBRecord.getString("T30"));
		ret.setT31(aDBRecord.getString("T31"));
		ret.setT32(aDBRecord.getString("T32"));
		ret.setT33(aDBRecord.getString("T33"));
		ret.setT34(aDBRecord.getString("T34"));
		ret.setT35(aDBRecord.getString("T35"));
		ret.setT36(aDBRecord.getString("T36"));
		ret.setT37(aDBRecord.getString("T37"));
		ret.setT38(aDBRecord.getString("T38"));
		ret.setT39(aDBRecord.getString("T39"));
		ret.setT40(aDBRecord.getString("T40"));
		ret.setT41(aDBRecord.getString("T41"));
		ret.setT42(aDBRecord.getString("T42"));
		ret.setT43(aDBRecord.getString("T43"));
		ret.setT44(aDBRecord.getString("T44"));
		ret.setT45(aDBRecord.getString("T45"));
		ret.setT46(aDBRecord.getString("T46"));
		ret.setT47(aDBRecord.getString("T47"));
		ret.setT48(aDBRecord.getString("T48"));
		ret.setT49(aDBRecord.getString("T49"));
		ret.setT50(aDBRecord.getString("T50"));
		ret.setT51(aDBRecord.getString("T51"));
		ret.setT52(aDBRecord.getString("T52"));
		ret.setT53(aDBRecord.getString("T53"));
		ret.setT54(aDBRecord.getString("T54"));
		ret.setT55(aDBRecord.getString("T55"));
		ret.setT56(aDBRecord.getString("T56"));
		ret.setT57(aDBRecord.getString("T57"));
		ret.setT58(aDBRecord.getString("T58"));
		ret.setT59(aDBRecord.getString("T59"));
		ret.setT60(aDBRecord.getString("T60"));
		ret.setT61(aDBRecord.getString("T61"));
		ret.setT62(aDBRecord.getString("T62"));
		ret.setT63(aDBRecord.getString("T63"));
		ret.setT64(aDBRecord.getString("T64"));
		ret.setT65(aDBRecord.getString("T65"));
		ret.setT66(aDBRecord.getString("T66"));
		return ret;
	}

	public static boolean IDExist(String t1, Connection conn) {
		boolean b = false;
		String sqlString = "";
		sqlString = "select * from Register where t1='" + t1 + "' AND recordstatus = 1";
		try {
			Statement stmts = conn.createStatement();
			ResultSet rs = stmts.executeQuery(sqlString);
			if (rs.next()) {
				b = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return b;
	}

	public static String insert(RegisterData aObj, Connection aConnection) throws SQLException {
		String ret = "";
		if (!isIDexist(aObj, aConnection)) {
			String sql = DBMgr.insertString(define(), aConnection); // Define
			PreparedStatement stmt = aConnection.prepareStatement(sql);
			DBRecord dbr = setDBRecord(aObj); // SetDBR
			DBMgr.setValues(stmt, dbr);
			try {
				stmt.executeUpdate();

				ret = "1";

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			// res = " ID already exists ";

			ret = "0";
		}
		return ret;
	}

	public static RegisterData insertotp(RegisterData aObj, Connection aConnection) throws SQLException {
		RegisterData ret = new RegisterData();
		if (!isIDexist(aObj, aConnection)) {
			String sql = DBMgr.insertString(define(), aConnection); // Define
			PreparedStatement stmt = aConnection.prepareStatement(sql);
			DBRecord dbr = setDBRecord(aObj); // SetDBR
			DBMgr.setValues(stmt, dbr);
			try {
				stmt.executeUpdate();

				// res = "Save Successfully!";
				// res = "1";
				ret.setSyskey(aObj.getSyskey());

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			// res = " ID already exists ";
			ret.setSyskey(0);
		}
		return ret;
	}

	public static Resultb2b insertReg(RegisterData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = DBMgr.insertString(define(), conn);
		PreparedStatement stmt = conn.prepareStatement(sql);
		DBRecord dbr = setDBRecord(obj);
		DBMgr.setValues(stmt, dbr);
		int count = stmt.executeUpdate();
		if (count > 0) {
			res.setState(true);
			res.setMsgDesc("Saved Successfully!");
		}
		return res;
	}

	public static boolean isExistMobile(RegisterData aObj, Connection conn) {
		boolean b = false;
		String sqlString = "";
		sqlString = "select * from UVM005 where RecordStatus = 0 AND t1='" + aObj.getT1() + "' AND syskey='"
				+ aObj.getT2() + "' ";
		try {
			Statement stmts = conn.createStatement();
			ResultSet rs = stmts.executeQuery(sqlString);
			if (rs.next()) {
				b = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return b;
	}

	public static boolean isExistPh(RegisterData aObj, Connection conn) {
		boolean b = false;
		String sqlString = "";
		sqlString = "select * from Register where RecordStatus = 0 AND t1='" + aObj.getT1() + "' AND UserSysKey='"
				+ aObj.getT2() + "' ";
		try {
			Statement stmts = conn.createStatement();
			ResultSet rs = stmts.executeQuery(sqlString);
			if (rs.next()) {
				b = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return b;
	}

	public static boolean isIDexist(RegisterData aObj, Connection conn) {
		boolean b = false;
		String sqlString = "";
		sqlString = "select * from Register where RecordStatus<>4 AND t1='" + aObj.getT1() + "'";
		try {
			Statement stmts = conn.createStatement();
			ResultSet rs = stmts.executeQuery(sqlString);
			if (rs.next()) {
				b = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return b;
	}

	public static boolean isIDExist(RegisterData obj, Connection conn) throws SQLException {
		String whereclause = " WHERE RecordStatus<>4 AND RecordStatus = 1 AND t1 = '" + obj.getT1() + "' ";
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereclause, "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isOTPexit(String mobile, String otpcode, Connection conn) {
		boolean b = false;
		String sqlString = "";
		sqlString = "select * from Register where RecordStatus<>4 AND t1='" + mobile + "' AND t60='" + otpcode + "'";
		try {
			Statement stmts = conn.createStatement();
			ResultSet rs = stmts.executeQuery(sqlString);
			if (rs.next()) {
				b = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return b;
	}

	public static RegisterData read(String t1, Connection conn) throws SQLException {
		RegisterData ret = new RegisterData();
		String whereclause = " WHERE RecordStatus = 1 AND t1 = '" + t1 + "' ";
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereclause, "", conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));
		return ret;
	}

	public static RegisterData readByID(String aObj, Connection conn) throws SQLException {
		RegisterData data = new RegisterData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "where RecordStatus<>4 AND t1='" + aObj + "'  ", "",
				conn);
		if (dbrs.size() > 0) {
			data = getDBRecord(dbrs.get(0));
			data.setT2(ServerUtil.decryptPIN(data.getT2()));
			data.setUsersyskey(data.getUsersyskey());

		}
		return data;
	}

	public static RegisterData readByPH(String aObj, Connection conn) throws SQLException {
		RegisterData data = new RegisterData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "where RecordStatus = 1 AND t1='" + aObj + "'", "",
				conn);
		if (dbrs.size() > 0) {
			data = getDBRecord(dbrs.get(0));
			data.setT2(ServerUtil.decryptPIN(data.getT2()));
		}
		return data;
	}

	public static RegisterData readByPH1(String aObj, Connection conn) throws SQLException {
		RegisterData data = new RegisterData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "where RecordStatus = 1 AND t1='" + aObj + "'", "",
				conn);
		if (dbrs.size() > 0) {
			data = getDBRecord(dbrs.get(0));
			data.setT2(ServerUtil.decryptPIN(data.getT2()));
		}
		return data;
	}

	public static RegisterData readByPHID(String aObj, String serialno, Connection conn) throws SQLException {
		RegisterData data = new RegisterData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				"where RecordStatus=1 AND t1='" + aObj + "' AND t58='" + serialno + "' ", "", conn);
		if (dbrs.size() > 0) {
			data = getDBRecord(dbrs.get(0));
			data.setT2(ServerUtil.decryptPIN(data.getT2()));
			data.setUsersyskey(data.getUsersyskey());

		}
		return data;
	}

	public static Resultb2b readByUser(String t1, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String whereclause = "";
		whereclause = " WHERE RecordStatus = 1 AND t1 = '" + t1 + "' ";
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereclause, "", conn);
		if (dbrs.size() > 0) {
			res.setState(true);
			res.setMsgDesc("Already Exist");
		} else {
			res.setState(false);
		}
		return res;
	}

	public static RegisterData readData(long syskey, Connection conn) throws SQLException {
		RegisterData ret = new RegisterData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "WHERE RecordStatus<>4 AND syskey=" + syskey, "",//change usersyskey to syskey
				conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));
		return ret;
	}

	public static RegisterData readDeviceID(long n5, Connection conn) {
		RegisterData res = new RegisterData();
		ArrayList<DBRecord> dbrs;
		try {
			dbrs = DBMgr.getDBRecords(define(), "WHERE RecordStatus<>4 AND usersyskey=" + n5 + " and t62<>''", "",
					conn);
			if (dbrs.size() > 0)
				res = getDBRecord(dbrs.get(0));
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	public static RegisterData readID(String aObj, Connection conn) throws SQLException {
		RegisterData data = new RegisterData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "where RecordStatus=0 AND t1='" + aObj + "'", "", conn);
		if (dbrs.size() > 0) {
			data = getDBRecord(dbrs.get(0));
			data.setT2(ServerUtil.decryptPIN(data.getT2()));
			data.setUsersyskey(data.getUsersyskey());
		}
		return data;
	}

	// TDA
	public static RegisterData readProfileReg(long syskey, Connection conn) throws SQLException {
		RegisterData ret = new RegisterData();
		String whereclause = " WHERE RecordStatus<>4 AND RecordStatus = 1 AND usersyskey = '" + syskey + "' ";
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereclause, "", conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));
		return ret;
	}

	public static RegisterData readReg(long syskey, Connection conn) throws SQLException {
		RegisterData ret = new RegisterData();
		String whereclause = " WHERE RecordStatus = 1 AND syskey = '" + syskey + "' ";
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereclause, "", conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));
		return ret;
	}

	public static String readT62(long n5, Connection conn) {
		String b = "";
		String sqlString = "";
		sqlString = "select t62 from Register  WHERE RecordStatus<>4 AND usersyskey=" + n5 + " and t62<>''";
		try {
			Statement stmts = conn.createStatement();
			ResultSet rs = stmts.executeQuery(sqlString);
			if (rs.next()) {
				b = rs.getString("t62");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return b;
	}

	public static Resultb2b save(RegisterData aObj, Connection aConnection) throws SQLException {
		Resultb2b res = new Resultb2b();
		RegisterData ret = new RegisterData();
		if (!isIDexist(aObj, aConnection)) {
			// res=insert(aObj, aConnection);
		} else {
			res = update(aObj, aConnection);
		}
		return res;
	}

	public static RegisterData saveforotp(RegisterData aObj, Connection aConnection) throws SQLException {
		String res = "";
		RegisterData ret = new RegisterData();
		if (!isIDexist(aObj, aConnection)) {
			ret = insertotp(aObj, aConnection);
		} else {
			ret = updateotp(aObj, aConnection);
		}
		return ret;
	}

	public static RegisterDataSet searchArticleLists(PagerData pgdata, String searchVal, String type, Connection conn)
			throws SQLException {
		RegisterDataSet res = new RegisterDataSet();
		ArrayList<RegisterData> datalist = new ArrayList<RegisterData>();
		String whereclause = " WHERE RecordStatus<>4  AND  RecordStatus = 1 ";
		if (!searchVal.equals("")) {
			whereclause += "AND t1 LIKE '%" + searchVal + "%' OR t3 LIKE '%" + searchVal + "%' OR t23 LIKE '%"
					+ searchVal + "%' OR t5 LIKE '%" + searchVal + "%' " + "OR t6 LIKE '%" + searchVal
					+ "%' OR t7 LIKE '%" + searchVal + "%' OR t8 LIKE '%" + searchVal + "%'OR t2 LIKE '%" + searchVal
					+ "%' OR t58 LIKE '%" + searchVal + "%'";
		}
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecordSandE(define(), whereclause, " ORDER BY syskey DESC",
				(pgdata.getStart() - 1), pgdata.getEnd(), 0, conn);
		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getDBRecord(dbrs.get(i)));
		}
		res.setPageSize(pgdata.getSize());
		res.setCurrentPage(pgdata.getCurrent());
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		PreparedStatement stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM Register " + whereclause);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		RegisterData[] dataarray = new RegisterData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	public static long searchByID(String key, Connection conn) throws SQLException {
		long syskey;
		String sql = "Select n2 from JUN002 WHERE RecordStatus=1 AND n1=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, key);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			syskey = rs.getLong("n2");
		} else {
			syskey = 0;
		}
		return syskey;
	}

	public static RegisterData searchByPh(String mobile, Connection conn) throws SQLException {
		RegisterData data = new RegisterData();
		mobile = mobile.replace(" ", "+");
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "where RecordStatus=1 AND t1='" + mobile + "'", "",
				conn);
		if (dbrs.size() > 0) {
			data = getDBRecord(dbrs.get(0));
			data.setT7((data.getT7()));
			data.setT8((data.getT8()));
			data.setT9((data.getT9())); // crop
			data.setT18((data.getT18()));
			data.setT19((data.getT19()));
		}
		return data;
	}

	public static DBRecord setDBRecord(RegisterData aObj) {
		DBRecord ret = define();
		ret.setValue("syskey", aObj.getSyskey());
		ret.setValue("createddate", aObj.getCreateddate());
		ret.setValue("modifieddate", aObj.getModifieddate());
		ret.setValue("userid", aObj.getUserid());
		ret.setValue("username", aObj.getUsername());
		ret.setValue("RecordStatus", aObj.getRecordStatus());
		ret.setValue("SyncStatus", aObj.getSyncStatus());
		ret.setValue("SyncBatch", aObj.getSyncBatch());
		ret.setValue("usersyskey", aObj.getUsersyskey());
		ret.setValue("T1", aObj.getT1());
		ret.setValue("T2", aObj.getT2());
		ret.setValue("T3", aObj.getT3());
		ret.setValue("T4", aObj.getT4());
		ret.setValue("T5", aObj.getT5());
		ret.setValue("T6", aObj.getT6());
		ret.setValue("T7", aObj.getT7());
		ret.setValue("T8", aObj.getT8());
		ret.setValue("T9", aObj.getT9());
		ret.setValue("T10", aObj.getT10());
		ret.setValue("T11", aObj.getT11());
		ret.setValue("T12", aObj.getT12());
		ret.setValue("T13", aObj.getT13());
		ret.setValue("T14", aObj.getT14());
		ret.setValue("T15", aObj.getT15());
		ret.setValue("T16", aObj.getT16());
		ret.setValue("T17", aObj.getT17());
		ret.setValue("T18", aObj.getT18());
		ret.setValue("T19", aObj.getT19());
		ret.setValue("T20", aObj.getT20());
		ret.setValue("T21", aObj.getT21());
		ret.setValue("T22", aObj.getT22());
		ret.setValue("T23", aObj.getT23());
		ret.setValue("T24", aObj.getT24());
		ret.setValue("T25", aObj.getT25());
		ret.setValue("T26", aObj.getT26());
		ret.setValue("T27", aObj.getT27());
		ret.setValue("T28", aObj.getT28());
		ret.setValue("T29", aObj.getT29());
		ret.setValue("T30", aObj.getT30());
		ret.setValue("T31", aObj.getT31());
		ret.setValue("T32", aObj.getT32());
		ret.setValue("T33", aObj.getT33());
		ret.setValue("T34", aObj.getT34());
		ret.setValue("T35", aObj.getT35());
		ret.setValue("T36", aObj.getT36());
		ret.setValue("T37", aObj.getT37());
		ret.setValue("T38", aObj.getT38());
		ret.setValue("T39", aObj.getT39());
		ret.setValue("T40", aObj.getT40());
		ret.setValue("T41", aObj.getT41());
		ret.setValue("T42", aObj.getT42());
		ret.setValue("T43", aObj.getT43());
		ret.setValue("T44", aObj.getT44());
		ret.setValue("T45", aObj.getT45());
		ret.setValue("T46", aObj.getT46());
		ret.setValue("T47", aObj.getT47());
		ret.setValue("T48", aObj.getT48());
		ret.setValue("T49", aObj.getT49());
		ret.setValue("T50", aObj.getT50());
		ret.setValue("T51", aObj.getT51());
		ret.setValue("T52", aObj.getT52());
		ret.setValue("T53", aObj.getT53());
		ret.setValue("T54", aObj.getT54());
		ret.setValue("T55", aObj.getT55());
		ret.setValue("T56", aObj.getT56());
		ret.setValue("T57", aObj.getT57());
		ret.setValue("T58", aObj.getT58());
		ret.setValue("T59", aObj.getT59());
		ret.setValue("T60", aObj.getT60());
		ret.setValue("T61", aObj.getT61());
		ret.setValue("T62", aObj.getT62());
		ret.setValue("T63", aObj.getT63());
		ret.setValue("T64", aObj.getT64());
		ret.setValue("T65", aObj.getT65());
		ret.setValue("T66", aObj.getT66());
		return ret;
	}

	public static Resultb2b update(RegisterData aObj, Connection aConnection) throws SQLException {
		Resultb2b res = new Resultb2b();
		try {
			/*
			 * String sql = DBMgr.updateString("where t1 ='" + aObj.getT1()+"'",
			 * define(), aConnection); // define PreparedStatement stmt =
			 * aConnection.prepareStatement(sql); DBRecord dbr =
			 * setDBRecord(aObj); DBMgr.setValues(stmt, dbr);
			 */
			String sql = "UPDATE Register SET t66='" + aObj.getT66() + "' WHERE t1='" + aObj.getT1() + "'";
			PreparedStatement stmt = aConnection.prepareStatement(sql);
			stmt.executeUpdate();
			if (stmt.executeUpdate() > 0) {
				res.setState(true);
				res.setMsgDesc("Updated Successfully");
			} else {
				res.setState(false);
				res.setMsgDesc("Fail to Update");
			}
			// res="Update Successfully!";

		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	// TDA Update DeviceID
	public static RegisterData updateDeviceID(RegisterData aObj, Connection conn) throws SQLException {
		RegisterData res = new RegisterData();
		try {
			String sql = "update register set t62='" + aObj.getT62() + "' where  t1='" + aObj.getT1() + "'";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.executeUpdate();
			if (stmt.executeUpdate() > 0) {
				res.setSyskey(1);
			} else {
				res.setSyskey(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	public static Resultb2b UpdateImageUpload(RegisterData aObj, MrBean user) throws SQLException {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		Resultb2b res = new Resultb2b();
		String sqlString = "";
		sqlString = "UPDATE Register SET t16 = '" + aObj.getT16() + "' where t1 = '" + aObj.getT1() + " ' ";
		PreparedStatement st = conn.prepareStatement(sqlString);
		int result = st.executeUpdate();
		if (result > 0) {
			res.setState(true);
			res.setMsgDesc("Updated Successfully!");
		} else {
			res.setState(false);
		}
		return res;
	}

	public static RegisterData updateotp(RegisterData aObj, Connection aConnection) throws SQLException {
		RegisterData res = new RegisterData();
		try {
			String sql = DBMgr.updateString("where t1 ='" + aObj.getT1() + "'", define(), aConnection); // define
			PreparedStatement stmt = aConnection.prepareStatement(sql);
			DBRecord dbr = setDBRecord(aObj);
			DBMgr.setValues(stmt, dbr);
			stmt.executeUpdate();
			res.setSyskey(aObj.getSyskey());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	public static RegisterData updateOTP(String phone, String otpcode, Connection aConnection) throws SQLException {
		RegisterData res = new RegisterData();
		try {
			String sql = "update register set recordstatus=1 where t1 ='" + phone + "' and t60 ='" + otpcode + "'";
			PreparedStatement stmt = aConnection.prepareStatement(sql);
			stmt.executeUpdate();
			if (stmt.executeUpdate() > 0) {
				res.setSyskey(1);
			} else {
				res.setSyskey(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	public static Resultb2b updateProfile(RegisterData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sqlString = "";
		if (isIDExist(obj, conn)) {
			sqlString = "UPDATE UVM012 SET t2='" + obj.getT3() + "' where RecordStatus<>4 AND t1 = '" + obj.getT1()
					+ "' ";
			try {
				PreparedStatement stmt = conn.prepareStatement(sqlString);
				int rs = stmt.executeUpdate();
				if (rs > 0) {
					sqlString = "UPDATE UVM005 SET t3='" + obj.getT13() + "',t5='" + obj.getT3()
							+ "' where RecordStatus<>4 AND t1 = '" + obj.getT1() + "' ";
					PreparedStatement st = conn.prepareStatement(sqlString);
					int result = st.executeUpdate();
					if (result > 0) {
						for (int i = 0; i < obj.getUpload().length; i++) {
							boolean flag = false;
							obj.setT16(obj.getUpload()[i]);
						}
						res = updateReg(obj, conn);
						if (res.isState()) {
							res.setMsgDesc("Updated Successfully!");
						} else {
							res.setState(false);
						}
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return res;
	}

	// TDA for profile update
	public static Resultb2b updateProfileforupdate(RegisterData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		if (isIDExist(obj, conn)) {
			res = updateReg(obj, conn);
			if (res.isState()) {
				res.setMsgDesc("Updated Successfully!");
			} else {
				res.setState(false);
			}
		}
		return res;
	}

	public static Resultb2b updateReg(RegisterData aObj, Connection aConnection) throws SQLException {
		Resultb2b res = new Resultb2b();
		try {
			String sql = DBMgr.updateString("where syskey ='" + aObj.getSyskey() + "'", define(), aConnection); // define
			PreparedStatement stmt = aConnection.prepareStatement(sql);
			DBRecord dbr = setDBRecord(aObj);
			DBMgr.setValues(stmt, dbr);
			int result = stmt.executeUpdate();
			if (result > 0) {
				res.setState(true);
				res.setMsgDesc("Updated Successfully!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	public static String updateRegData(RegisterData aObj, Connection aConnection) throws SQLException {
		String res = "";
		String sql = DBMgr.updateString("where  RecordStatus=4 AND t1 ='" + aObj.getT1() + "'", define(), aConnection); // define
		try {
			PreparedStatement stmt = aConnection.prepareStatement(sql);
			DBRecord dbr = setDBRecord(aObj);
			DBMgr.setValues(stmt, dbr);
			stmt.executeUpdate();
			res = "Update Successfully!";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	public static Resultb2b updateRegister(RegisterData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sqlString = "";
		if (isExistPh(obj, conn)) {
			sqlString = "UPDATE UVM012 SET RecordStatus = 1 where RecordStatus = 0 AND t1 = '" + obj.getT1() + "' ";
			try {
				PreparedStatement stmt = conn.prepareStatement(sqlString);
				int rs = stmt.executeUpdate();
				if (rs > 0) {
					System.out.println(rs + " success 12");
					sqlString = "UPDATE UVM005 SET RecordStatus = 1 where RecordStatus = 0 AND t1 = '" + obj.getT1()
							+ "' AND syskey = '" + obj.getT2() + "' ";
					PreparedStatement st = conn.prepareStatement(sqlString);
					int result = st.executeUpdate();
					if (result > 0) {
						for (int i = 0; i < obj.getUpload().length; i++) {
							boolean flag = false;
							obj.setT16(obj.getUpload()[i]);
						}
						res = updateRegJUN(obj, conn);
						if (res.isState()) {
							res.setMsgDesc("Updated Successfully!");
						} else {
							res.setState(false);
						}
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println(" does not exist");
		}
		return res;
	}

	public static RegisterData updateRegisterOTP(RegisterData aObj, Connection conn) throws SQLException {
		RegisterData res = new RegisterData();
		try {
			String psql = "select syskey from Register where t1 =? AND recordstatus <> 4";
			PreparedStatement pstmt = conn.prepareStatement(psql);
			pstmt.setString(1, aObj.getT1());
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				aObj.setSyskey(rs.getLong("syskey"));
				String sql = DBMgr.updateString("where t1 ='" + aObj.getT1() + "' AND syskey =" + aObj.getSyskey(),
						define(), conn); // define
				PreparedStatement stmt = conn.prepareStatement(sql);
				DBRecord dbr = setDBRecord(aObj);
				DBMgr.setValues(stmt, dbr);
				if (stmt.executeUpdate() > 0) {
					res.setSyskey(aObj.getSyskey());
				} else {
					res.setSyskey(0);
				}
			} else {
				res.setSyskey(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	public static Resultb2b updateRegJUN(RegisterData aObj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sqlString = "";
		sqlString = "UPDATE Register SET RecordStatus = 1 where RecordStatus = 0 AND t1 = '" + aObj.getT1()
				+ "' AND UserSysKey = '" + aObj.getT2() + "' ";
		PreparedStatement st = conn.prepareStatement(sqlString);
		int result = st.executeUpdate();
		if (result > 0) {
			sqlString = "UPDATE JUN002 SET RecordStatus = 1 where RecordStatus = 0 AND n1 = '" + aObj.getT2() + "'  ";
			PreparedStatement stm = conn.prepareStatement(sqlString);
			int ret = stm.executeUpdate();
			if (ret > 0) {
				res.setState(true);
				res.setMsgDesc("Updated Successfully!");
			} else {
				res.setState(false);
			}
		}
		return res;
	}

	public static String updateresetotp(String mobile, String otpcode, Connection conn) throws SQLException {
		String res = "";
		String sqlString = "";
		sqlString = "UPDATE Register SET t60='" + otpcode + "' where RecordStatus<>4 AND t1 = '" + mobile + "' ";
		PreparedStatement st = conn.prepareStatement(sqlString);
		int result = st.executeUpdate();
		if (result > 0) {
			res = otpcode;

		} else {
			res = "0";
		}
		return res;
	}

	public WalletCheckRes checkWallet(SessionData req, Connection conn) throws SQLException {
		WalletCheckRes ret = new WalletCheckRes();
		PreparedStatement ps = null;
		if(!req.getUserID().trim().equals("")){
			String sql = "select userid,username from REGISTER where userid = ? and RecordStatus <> 4";
			ps = conn.prepareStatement(sql);
			ps.setString(1, req.getUserID());
		} else {
			String sql = "select userid,username from REGISTER where t21 = ? and RecordStatus <> 4";
			ps = conn.prepareStatement(sql);
			ps.setString(1, req.getLoginID());
		}
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			int i = 1;
			ret.setWalletID(rs.getString(i++));
			ret.setName(rs.getString(i++));
			ret.setCode("0000");
			ret.setDesc("Success");
		} else {
			ret.setCode("0012");
			ret.setDesc("Wallet ID is not registerd");
		}
		return ret;
	}
	
	public static RegisterData readBySyskey(String aObj, Connection conn) throws SQLException {
		RegisterData data = new RegisterData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "where RecordStatus<>4 AND syskey=" + Long.parseLong(aObj), "",
				conn);
		if (dbrs.size() > 0) {
			data = getDBRecord(dbrs.get(0));
			data.setT2(ServerUtil.decryptPIN(data.getT2()));
			data.setUsersyskey(data.getUsersyskey());

		}
		return data;
	}
	//atn
	public static RegisterData[] readByIDReply(long syskey, Connection conn) throws SQLException {
		//RegisterData data = new RegisterData();
		ArrayList<RegisterData> datalist = new ArrayList<RegisterData>();
		String sql = "SELECT * FROM uvm012_A Where recordStatus=1 and syskey= ? ";
		/*String sql="select p.syskey,p.t1,p.t2,p.t3,p.t4,p.t16 from UVM012_A p join UVM005_A u on p.syskey = u.n4 where p.recordstatus=1 and" 
				+ " u.recordstatus=1 and u.syskey=?";*/

				PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);

		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			RegisterData data = new RegisterData();
			data.setSyskey(rs.getLong("syskey"));
			data.setT1(rs.getString("t1"));
			data.setT2(rs.getString("t2"));
			data.setT3(rs.getString("t3"));
			data.setT4(rs.getString("t4"));
			data.setT16(rs.getString("t16"));
			datalist.add(data);

		}
		RegisterData[] regarr = new RegisterData[datalist.size()];
		regarr = datalist.toArray(regarr);
		return regarr;
	}
	//atn
	public static ArrayList<RegisterData> readCommentLikePerson(long n2,Connection conn) throws SQLException {
		//RegisterData data = new RegisterData();
		ArrayList<RegisterData> arrlist = new ArrayList<RegisterData>();
		String sql = "";
		sql = "SELECT n1 FROM fmr017 Where recordStatus=1 and n2=" +n2;
		PreparedStatement stmt = conn.prepareStatement(sql);

		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			RegisterData data = new RegisterData();
			data = readByIDLike(rs.getLong("n1"),conn);
			arrlist.add(data);
		}
		return arrlist;
	}
	//atn
	public static RegisterData readByIDLike(long syskey, Connection conn) throws SQLException {
		RegisterData data = new RegisterData();

		String sql = "SELECT * FROM UVM012_A Where recordStatus=1 and syskey= ? ";

		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);

		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			data.setSyskey(rs.getLong("syskey"));
			data.setAutokey(rs.getLong("autokey"));
			data.setRecordStatus(rs.getInt("recordstatus"));
			data.setT1(rs.getString("t1"));
			data.setT2(rs.getString("t2"));
			data.setT16(rs.getString("t16"));
		}
		return data;
	}
	public String joinedAcc(String userid,Connection conn) throws SQLException{
		String res = "";
		String sql = "Select t2 From Register Where t1=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, userid);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()){
			res = rs.getString("t2");
		}
		return res;
	}
}
