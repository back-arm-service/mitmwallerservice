package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.RegisterData;
import com.nirvasoft.cms.shared.VideoData;
import com.nirvasoft.cms.shared.VideoDataSet;
import com.nirvasoft.cms.util.FontChangeUtil;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;
import com.nirvasoft.database.SysKeyMgr;

public class VideoDao {

	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("FMR011");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("createdtime", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("modifiedtime", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t13", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		ret.getFields().add(new DBField("n11", (byte) 2));
		ret.getFields().add(new DBField("n12", (byte) 2));
		ret.getFields().add(new DBField("n13", (byte) 2));
		return ret;
	}

	public static DBRecord define(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t13", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		ret.getFields().add(new DBField("n11", (byte) 2));
		ret.getFields().add(new DBField("n12", (byte) 2));
		ret.getFields().add(new DBField("n13", (byte) 2));

		return ret;
	}

	public static Resultb2b delete(Long syskey, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = "UPDATE FMR002 SET RecordStatus=4 WHERE RecordStatus = 1 AND syskey=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Successfully");
		} else {
			res.setMsgDesc("Deleting Unsuccessful");
		}
		return res;
	}

	public static VideoData getDBRecord(DBRecord adbr) {
		VideoData ret = new VideoData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("createddate"));
		ret.setCreatedTime(adbr.getString("createdtime"));
		ret.setModifiedDate(adbr.getString("modifieddate"));
		ret.setModifiedTime(adbr.getString("modifiedtime"));
		ret.setUserId(adbr.getString("userid"));
		ret.setUserName(adbr.getString("username"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setUserSyskey(adbr.getLong("usersyskey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		ret.setT8(adbr.getString("t8"));
		ret.setT9(adbr.getString("t9"));
		ret.setT10(adbr.getString("t10"));
		ret.setT11(adbr.getString("t11"));
		ret.setT12(adbr.getString("t12"));
		ret.setT13(adbr.getString("t13"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getLong("n5"));
		ret.setN6(adbr.getLong("n6"));
		ret.setN7(adbr.getLong("n7"));
		ret.setN8(adbr.getLong("n8"));
		ret.setN9(adbr.getLong("n9"));
		ret.setN10(adbr.getLong("n10"));
		ret.setN11(adbr.getLong("n11"));
		ret.setN12(adbr.getLong("n12"));
		ret.setN13(adbr.getLong("n13"));
		return ret;
	}

	// for dislike
	public static long getTotalDisLikeCount(long syskey, Connection conn) throws SQLException {
		long key = 0;
		String sql = " Select n8 from  FMR002 WHERE  RecordStatus = 1 AND syskey = " + syskey;
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = rs.getLong("n8");
		}
		return key;
	}

	public static long getTotalLikeCount(long syskey, Connection conn) throws SQLException {
		long key = 0;
		String sql = " Select n2 from  FMR002 WHERE  RecordStatus = 1 AND syskey = " + syskey;
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = rs.getLong("n2");
		}
		return key;
	}

	public static Resultb2b insert(VideoData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		if (!isCodeExist(obj, conn)) {
			String sql = DBMgr.insertString(define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			int count = stmt.executeUpdate();
			if (count > 0) {
				res.setState(true);
				res.setMsgDesc("Saved Successfully!");
			}
		} else {
			res.setMsgDesc("Phone Call Already Exist!");
		}
		return res;
	}

	public static boolean isCodeExist(VideoData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				" where RecordStatus<>4 AND  RecordStatus = 1 AND syskey = " + obj.getSyskey() + " ", "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isDisLikeExist(Long syskey, Long userkey, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR008"),
				" where RecordStatus<>4 AND t4='dislike' AND RecordStatus = 1 AND n1 = " + syskey + " AND n2 = "
						+ userkey + " ",
				"", conn);
		/*
		 * ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR008"),
		 * "  n1 = " + syskey + " AND n2 = " + userkey + " ", "", conn);
		 */
		if (dbrs.size() > 0) {

			return true;

		} else {
			return false;
		}
	}

	/*
	 * public static Result updateDisLikeCountVideo(Long syskey, String userSK,
	 * String type, Connection conn, MrBean user) throws SQLException { long key
	 * = SysKeyMgr.getSysKey(1, "syskey",
	 * ConnAdmin.getConn(user.getUser().getOrganizationID())); Result res = new
	 * Result(); long userKey = Long.parseLong(userSK); if (isLikeExist(syskey,
	 * userKey, conn)) { String sql =
	 * "Update FMR002 SET n2 = n2-1 WHERE syskey = ?"; PreparedStatement stmt =
	 * conn.prepareStatement(sql); stmt.setLong(1, syskey);
	 * stmt.executeUpdate(); String query =
	 * "UPDATE FMR008 SET RecordStatus=4 WHERE  RecordStatus = 1 AND t4='like' AND  n1 = '"
	 * + syskey + "' AND n2 = '" + userKey + "'"; PreparedStatement st =
	 * conn.prepareStatement(query); st.executeUpdate(); } RegisterData userData
	 * = RegisterDao.readData(userKey, conn); if (!isLikeExist(syskey, userKey,
	 * conn)) { String sql = "Update FMR002 SET n8 = n8+1 WHERE syskey = ?";
	 * PreparedStatement stmt = conn.prepareStatement(sql); stmt.setLong(1,
	 * syskey); int rs = stmt.executeUpdate(); if (rs > 0) { res.setState(true);
	 * String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
	 * if (!isStatusExist(syskey, userKey, "dislike", conn)) { String query =
	 * "INSERT INTO FMR008 (syskey,createddate, modifieddate, userid, username, RecordStatus, SyncStatus, SyncBatch, usersyskey,T1, T2,T3, T4,T5, T6,T7, T8,T9, T10,T11, T12,T13, T14,T15, n1,n2,n3, n4,n5, n6,n7, n8,n9, n10,n11, n12,n13, n14,n15) "
	 * +
	 * "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
	 * ; PreparedStatement psmt = conn.prepareStatement(query);
	 * 
	 * psmt.setLong(1, key); psmt.setString(2, todayDate); psmt.setString(3,
	 * todayDate); psmt.setString(4, userData.getT1()); psmt.setString(5,
	 * userData.getT3()); psmt.setInt(6, 1); psmt.setInt(7, 1); psmt.setLong(8,
	 * 0); psmt.setLong(9, 0); psmt.setString(10, userData.getT1());
	 * psmt.setString(11, userData.getT3()); psmt.setString(12, type);
	 * psmt.setString(13, "dislike"); psmt.setString(14, "0");
	 * psmt.setString(15, "0"); psmt.setString(16, "0"); psmt.setString(17,
	 * "0"); psmt.setString(18, "0"); psmt.setString(19, "0");
	 * psmt.setString(20, "0"); psmt.setString(21, "0"); psmt.setString(22,
	 * "0"); psmt.setString(23, "0"); psmt.setString(24, "0"); psmt.setLong(25,
	 * syskey); psmt.setLong(26, userKey); psmt.setLong(27, 0); psmt.setLong(28,
	 * 0); psmt.setLong(29, 0); psmt.setLong(30, 0); psmt.setLong(31, 0);
	 * psmt.setLong(32, 0); psmt.setLong(33, 0); psmt.setLong(34, 0);
	 * psmt.setLong(35, 0); psmt.setLong(36, 0); psmt.setLong(37, 0);
	 * psmt.setLong(38, 0); psmt.setLong(39, 0); if (psmt.executeUpdate() > 0) {
	 * } else { } } else { String query =
	 * "UPDATE FMR008 SET RecordStatus=1 WHERE  RecordStatus = 4 AND t4='dislike' AND n1 = '"
	 * + syskey + "' AND n2 = '" + userKey + "'"; PreparedStatement stm =
	 * conn.prepareStatement(query);
	 * 
	 * int result = stm.executeUpdate(); if (result > 0) { res.setState(true); }
	 * else { res.setState(false); } }
	 * 
	 * } } else { String sql =
	 * "Update FMR002 SET n8 = n8-1 WHERE  RecordStatus = 1 AND syskey = ?";
	 * PreparedStatement stmt = conn.prepareStatement(sql); stmt.setLong(1,
	 * syskey); int rs = stmt.executeUpdate(); if (rs > 0) { res.setState(true);
	 * String query =
	 * "UPDATE FMR008 SET RecordStatus=4 WHERE  RecordStatus = 1 AND t4='dislike' AND n1 = '"
	 * + syskey + "' AND n2 = '" + userKey + "'"; PreparedStatement stm =
	 * conn.prepareStatement(query);
	 * 
	 * int result = stm.executeUpdate(); if (result > 0) { res.setState(true); }
	 * else { res.setState(false); }
	 * 
	 * } else { res.setState(false); }
	 * 
	 * } res.setN2(getTotalLikeCount(syskey, conn));
	 * res.setKeyResult(getTotalDisLikeCount(syskey, conn)); return res;
	 * 
	 * }
	 */
	public static boolean isLikeExist(Long syskey, Long userkey, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR008"),
				" where RecordStatus<>4 AND  RecordStatus = 1 AND t4='like' AND n1 = " + syskey + " AND n2 = " + userkey
						+ " ",
				"", conn);
		/*
		 * ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR008"),
		 * "  n1 = " + syskey + " AND n2 = " + userkey + " ", "", conn);
		 */
		if (dbrs.size() > 0) {

			return true;

		} else {
			return false;
		}
	}

	public static boolean isStatusExist(Long syskey, Long userkey, String type, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR008"),
				" where RecordStatus = 4 AND t4='" + type + "' AND n1 = " + syskey + " AND n2 = " + userkey + " ", "",
				conn);
		/*
		 * ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR008"),
		 * "  n1 = " + syskey + " AND n2 = " + userkey + " ", "", conn);
		 */
		if (dbrs.size() > 0) {

			return true;

		} else {
			return false;
		}
	}

	public static VideoData read(long syskey, Connection conn) throws SQLException {
		VideoData ret = new VideoData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR002"),
				"where RecordStatus<>4 AND  RecordStatus = 1 AND syskey=" + syskey, "", conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));
		if (ServerUtil.isUniEncoded(ret.getT2())) {
			ret.setT2(FontChangeUtil.uni2zg(ret.getT2()));
		}
		return ret;
	}

	public static VideoDataSet searchLikeVideo(String type, long key, Connection conn) throws SQLException {
		// ArticleData ret = new ArticleData();
		System.out.println(type + " ^^");
		VideoDataSet res = new VideoDataSet();
		ArrayList<VideoData> datalist = new ArrayList<VideoData>();
		String whereclause = " WHERE RecordStatus<>4 AND RecordStatus = 1 AND t3 = '" + type + "'  AND n1 = " + key
				+ " ";
		System.out.println(whereclause);
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR008"), whereclause, " ORDER BY syskey ", conn);
		// .getDBRecords(define("FMR008"), conn);
		/*
		 * if (dbrs.size() > 0) ret = getDBRecord(dbrs.get(0));
		 */
		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getDBRecord(dbrs.get(i)));
		}

		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}

		VideoData[] dataarray = new VideoData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);

		return res;
	}

	public static DBRecord setDBRecord(VideoData data) {
		DBRecord ret = define();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("createddate", data.getCreatedDate());
		ret.setValue("createdtime", data.getCreatedTime());
		ret.setValue("modifieddate", data.getModifiedDate());
		ret.setValue("modifiedtime", data.getModifiedTime());
		ret.setValue("userid", data.getUserId());
		ret.setValue("username", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("usersyskey", data.getUserSyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t5", data.getT5());
		ret.setValue("t6", data.getT6());
		ret.setValue("t7", data.getT7());
		ret.setValue("t8", data.getT8());
		ret.setValue("t9", data.getT9());
		ret.setValue("t10", data.getT10());
		ret.setValue("t11", data.getT11());
		ret.setValue("t12", data.getT12());
		ret.setValue("t13", data.getT13());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		ret.setValue("n6", data.getN6());
		ret.setValue("n7", data.getN7());
		ret.setValue("n8", data.getN8());
		ret.setValue("n9", data.getN9());
		ret.setValue("n10", data.getN10());
		ret.setValue("n11", data.getN11());
		ret.setValue("n12", data.getN12());
		ret.setValue("n13", data.getN13());
		return ret;
	}

	public static Resultb2b updateLikeCountVideo(Long syskey, String userSK, String type, Connection conn, MrBean user)
			throws SQLException {
		long key = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
		Resultb2b res = new Resultb2b();
		long userKey = Long.parseLong(userSK);
		RegisterData userData = RegisterDao.readData(userKey, conn);

		res.setN1(ArticleDao.searchDisLikeOrNot(syskey, userSK, conn));
		// dislike count -1
		if (isDisLikeExist(syskey, userKey, conn)) {
			String sql = "Update FMR002 SET n8 = n8-1 WHERE syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			stmt.executeUpdate();

			// update like status
			String query = "UPDATE FMR008 SET RecordStatus=4 WHERE  RecordStatus = 1 AND t4='dislike' AND  n1 = '"
					+ syskey + "' AND n2 = '" + userKey + "'";
			PreparedStatement st = conn.prepareStatement(query);
			st.executeUpdate();

		}

		if (!isLikeExist(syskey, userKey, conn)) {
			String sql = "Update FMR002 SET n2 = n2+1 WHERE syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			// stmt.setLong(2, userKey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
				if (!isStatusExist(syskey, userKey, "like", conn)) {
					String query = "INSERT INTO FMR008 (syskey,createddate, modifieddate, userid, username, RecordStatus, SyncStatus, SyncBatch, usersyskey,T1, T2,T3, T4,T5, T6,T7, T8,T9, T10,T11, T12,T13, T14,T15, n1,n2,n3, n4,n5, n6,n7, n8,n9, n10,n11, n12,n13, n14,n15) "
							+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					PreparedStatement psmt = conn.prepareStatement(query);

					psmt.setLong(1, key);
					psmt.setString(2, todayDate);
					psmt.setString(3, todayDate);
					psmt.setString(4, userData.getT1());
					psmt.setString(5, userData.getT3());
					psmt.setInt(6, 1);
					psmt.setInt(7, 1);
					psmt.setLong(8, 0);
					psmt.setLong(9, 0);
					psmt.setString(10, userData.getT1());
					psmt.setString(11, userData.getT3());
					psmt.setString(12, type);
					psmt.setString(13, "like");
					psmt.setString(14, "0");
					psmt.setString(15, "0");
					psmt.setString(16, "0");
					psmt.setString(17, "0");
					psmt.setString(18, "0");
					psmt.setString(19, "0");
					psmt.setString(20, "0");
					psmt.setString(21, "0");
					psmt.setString(22, "0");
					psmt.setString(23, "0");
					psmt.setString(24, "0");
					psmt.setLong(25, syskey);
					psmt.setLong(26, userKey);
					psmt.setLong(27, 0);
					psmt.setLong(28, 0);
					psmt.setLong(29, 0);
					psmt.setLong(30, 0);
					psmt.setLong(31, 0);
					psmt.setLong(32, 0);
					psmt.setLong(33, 0);
					psmt.setLong(34, 0);
					psmt.setLong(35, 0);
					psmt.setLong(36, 0);
					psmt.setLong(37, 0);
					psmt.setLong(38, 0);
					psmt.setLong(39, 0);
					if (psmt.executeUpdate() > 0) {
					} else {
					}
				} else {
					String query = "UPDATE FMR008 SET RecordStatus=1 WHERE  RecordStatus = 4 AND t4='like' AND n1 = '"
							+ syskey + "' AND n2 = '" + userKey + "'";
					PreparedStatement stm = conn.prepareStatement(query);

					int result = stm.executeUpdate();
					if (result > 0) {
						res.setState(true);
					} else {
						res.setState(false);
					}

				}

			}
		} else {
			String sql = "Update FMR002 SET n2 = n2-1 WHERE  RecordStatus = 1 AND syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String query = "UPDATE FMR008 SET RecordStatus=4 WHERE  RecordStatus = 1 AND t4='like' AND n1 = '"
						+ syskey + "' AND n2 = '" + userKey + "'";
				PreparedStatement stm = conn.prepareStatement(query);

				int result = stm.executeUpdate();
				if (result > 0) {
					res.setState(true);
				} else {
					res.setState(false);
				}

			} else {
				res.setState(false);
			}

		}
		res.setN2(getTotalDisLikeCount(syskey, conn));
		res.setKeyResult(getTotalLikeCount(syskey, conn));
		return res;

	}

	/*
	 * public static VideoDataSet searchLikeCount(String id, Connection conn,
	 * MrBean user) throws SQLException { VideoDataSet res = new VideoDataSet();
	 * ArrayList<VideoData> datalist = new ArrayList<VideoData>(); long syskey =
	 * Long.parseLong(id); //long userKey =
	 * OPTDao.searchByID(user.getUser().getUserId(), conn); String whereclause =
	 * " WHERE RecordStatus<>4 AND RecordStatus = 1 AND syskey = '" + syskey +
	 * "'   "; ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR011"),
	 * whereclause, " ORDER BY syskey ", conn); for (int i = 0; i < dbrs.size();
	 * i++) { datalist.add(getDBRecord(dbrs.get(i))); } if (datalist.size() > 0)
	 * { res.setState(true); } else { res.setState(false); }
	 * 
	 * VideoData[] dataarray = new VideoData[datalist.size()]; dataarray =
	 * datalist.toArray(dataarray); res.setData(dataarray);
	 * 
	 * return res; }
	 */

}
