package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.nirvasoft.cms.framework.ResultMobile;
import com.nirvasoft.cms.shared.PagerMobileData;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;
import com.nirvasoft.cms.shared.ContentData;
import com.nirvasoft.cms.shared.ContentDataSet;
import com.nirvasoft.cms.shared.UserData;
import com.nirvasoft.cms.shared.UserDataset;
import com.nirvasoft.cms.shared.UserViewData;
import com.nirvasoft.cms.shared.UserViewDataset;

public class ContentMobileDao {

	public DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("FMR013");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t13", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		ret.getFields().add(new DBField("n11", (byte) 2));
		ret.getFields().add(new DBField("n12", (byte) 2));
		ret.getFields().add(new DBField("n13", (byte) 2));
		return ret;
	}

	public DBRecord define(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		// ret.getFields().add(new DBField("createdtime", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		// ret.getFields().add(new DBField("modifiedtime", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t13", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		ret.getFields().add(new DBField("n11", (byte) 2));
		ret.getFields().add(new DBField("n12", (byte) 2));
		ret.getFields().add(new DBField("n13", (byte) 2));

		return ret;
	}

	public DBRecord defines(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t13", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		ret.getFields().add(new DBField("n11", (byte) 2));
		ret.getFields().add(new DBField("n12", (byte) 2));
		ret.getFields().add(new DBField("n13", (byte) 2));

		return ret;
	}

	public ResultMobile delete(Long syskey, Connection conn) throws SQLException {
		ResultMobile res = new ResultMobile();
		String sql = "UPDATE FMR013 SET RecordStatus=4 WHERE RecordStatus = 1 AND n2=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Successfully");
		} else {
			res.setMsgDesc("Deleting Unsuccessful");
		}
		return res;
	}

	public boolean ExistPost(ContentData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				" where RecordStatus = 1 AND n1 = " + obj.getN1() + " AND n2 = " + obj.getN2() + " ", "", conn);
		if (dbrs.size() > 0) {
			System.out.println("exist");
			return true;

		} else {
			System.out.println("not exist");
			return false;
		}
	}

	public UserDataset getContentWriterList(PagerMobileData p, Connection conn) throws SQLException {
		UserDataset res = new UserDataset();
		ArrayList<UserData> datalist = new ArrayList<UserData>();

		String sql = "";

		sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY p.syskey) AS RowNum , p.t2,u.t3,u.t6 ,u.t7,u.t8,u.t9,u.t10 From uvm012 p join uvm005 u on p.syskey=u.n4 join jun002 j on u.syskey=j.n1 join uvm009 r on j.n2=r.syskey and p.RecordStatus=1  and u.RecordStatus<>4 and r.t2<>'master' and r.t2<>'publisher' and r.t2<>'editor') AS RowConstrainedResult  WHERE RowNum >=? and RowNum <=?";
		// sql = "select * from uvm005";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setInt(1, p.getStart());
		stmt.setInt(2, p.getEnd());

		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			UserData data = new UserData();
			data.setT2(rs.getString("t2"));
			data.setT3(rs.getString("t3"));
			data.setT6(rs.getString("t6"));
			data.setT7(rs.getString("t7"));
			data.setT8(rs.getString("t8"));
			data.setT9(rs.getString("t9"));
			data.setT10(rs.getString("t10"));
			datalist.add(data);
		}

		res.setPageSize(p.getSize());
		res.setCurrentPage(p.getCurrent());
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		PreparedStatement stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM uvm005 ");
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		UserData[] dataarray = new UserData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	public ContentData getDBRecord(DBRecord adbr) {
		ContentData ret = new ContentData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("createddate"));
		ret.setModifiedDate(adbr.getString("modifieddate"));
		ret.setUserId(adbr.getString("userid"));
		ret.setUserName(adbr.getString("username"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setUserSyskey(adbr.getLong("usersyskey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		ret.setT8(adbr.getString("t8"));
		ret.setT9(adbr.getString("t9"));
		ret.setT10(adbr.getString("t10"));
		ret.setT11(adbr.getString("t11"));
		ret.setT12(adbr.getString("t12"));
		ret.setT13(adbr.getString("t13"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getLong("n5"));
		ret.setN6(adbr.getLong("n6"));
		ret.setN7(adbr.getLong("n7"));
		ret.setN8(adbr.getLong("n8"));
		ret.setN9(adbr.getLong("n9"));
		ret.setN10(adbr.getLong("n10"));
		ret.setN11(adbr.getLong("n11"));
		ret.setN12(adbr.getLong("n12"));
		ret.setN13(adbr.getLong("n13"));
		return ret;
	}

	public long getMaxBatchNo(Connection aConnection) {
		long ret = 0;
		String l_Query = "select ISNULL(MAX(n3) + 1, 1) as maxSys FROM fmr013";
		Statement stmt;
		try {
			stmt = aConnection.createStatement();
			ResultSet rs = stmt.executeQuery(l_Query);
			if (rs.next()) {
				ret = rs.getLong("maxSys");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return ret;
	}

	public String getName(long key, Connection conn) {
		String b = "0";
		String sqlString = "";
		sqlString = "select t5 from UVM005 where RecordStatus=1 AND syskey='" + key + "'";
		try {
			Statement stmts = conn.createStatement();
			ResultSet rs = stmts.executeQuery(sqlString);
			if (rs.next()) {
				b = rs.getString("t5");
				System.out.println(b + " name ");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return b;
	}

	public ResultMobile insert(ContentData obj, Connection conn) throws SQLException {
		ResultMobile res = new ResultMobile();
		String query = "INSERT INTO FMR013 (syskey, createddate, modifieddate, userid, username,"
				+ " recordStatus, syncStatus, syncBatch,usersyskey,t1, t2, t3, t4, t5, t6, t7, t8, t9, t10,"
				+ " t11, t12, t13,n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13)"
				+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?," + "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		try {
			if (!isCodeExist(obj, conn)) {
				PreparedStatement ps = conn.prepareStatement(query);
				ps.setLong(1, obj.getSyskey());
				ps.setString(2, obj.getCreatedDate());
				ps.setString(3, obj.getModifiedDate());
				ps.setString(4, obj.getUserId());
				ps.setString(5, obj.getUserName());
				ps.setInt(6, obj.getRecordStatus());
				ps.setInt(7, obj.getSyncStatus());
				ps.setLong(8, obj.getSyncBatch());
				ps.setLong(9, obj.getUserSyskey());
				ps.setString(10, obj.getT1());
				ps.setString(11, obj.getT2());
				ps.setString(12, obj.getT3());
				ps.setString(13, obj.getT4());
				ps.setString(14, obj.getT5());
				ps.setString(15, obj.getT6());
				ps.setString(16, obj.getT7());
				ps.setString(17, obj.getT8());
				ps.setString(18, obj.getT9());
				ps.setString(19, obj.getT10());
				ps.setString(20, obj.getT11());
				ps.setString(21, obj.getT12());
				ps.setString(22, obj.getT13());
				ps.setLong(23, obj.getN1());
				ps.setLong(24, obj.getN2());
				ps.setLong(25, obj.getN3());
				ps.setLong(26, obj.getN4());
				ps.setLong(27, obj.getN5());
				ps.setLong(28, obj.getN6());
				ps.setLong(29, obj.getN7());
				ps.setLong(30, obj.getN8());
				ps.setLong(31, obj.getN9());
				ps.setLong(32, obj.getN10());
				ps.setLong(33, obj.getN11());
				ps.setLong(34, obj.getN12());
				ps.setLong(35, obj.getN13());

				if (ps.executeUpdate() > 0) {
					res.setState(true);
					res.setMsgDesc("Saved Successfully!");
				} else {
					res.setMsgDesc("Article Already Exist!");
				}
			}

		} catch (SQLException e) {

		}

		return res;

	}

	///////////////////////// Mobile//////////////////
	public void insertdownload(ContentData data, Connection conn) {
		ResultMobile res = new ResultMobile();
		try {
			String sql = DBMgr.insertString(define("FMR017"), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(data);
			DBMgr.setValues(stmt, dbr);
			int count = stmt.executeUpdate();
			if (count > 0) {
				res.setState(true);
				res.setMsgDesc("Saved Successfully!");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public boolean isCodeExist(ContentData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				" where RecordStatus = 1 AND syskey = " + obj.getSyskey() + " ", "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isPostExist(long key, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), " where RecordStatus = 1 AND n2 = " + key + " ", "",
				conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public ContentDataSet searchContent(PagerMobileData pgdata, long userSK, long postSK, String searchVal, Connection conn)
			throws SQLException {
		ContentDataSet res = new ContentDataSet();
		ArrayList<ContentData> datalist = new ArrayList<ContentData>();
		String whereclause = " WHERE RecordStatus = 1 AND n1 = '" + userSK + "'  ";
		if (!searchVal.equals("")) {
			whereclause += "AND t1 LIKE '%" + searchVal + "%' AND t2 LIKE '%" + searchVal + "%'  ";
		}
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecordSandE(define(), whereclause, " ORDER BY syskey ",
				(pgdata.getStart() - 1), pgdata.getEnd(), 0, conn);

		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getDBRecord(dbrs.get(i)));
		}
		res.setPageSize(pgdata.getSize());
		res.setCurrentPage(pgdata.getCurrent());
		if (datalist.size() > 0) {
			res.setState(true);

		} else {
			res.setState(false);
		}
		PreparedStatement stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM FMR013 " + whereclause);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));

		ContentData[] dataarray = new ContentData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);

		return res;
	}

	public ContentDataSet searchContent(PagerMobileData pgdata, long userSK, String searchVal, Connection conn)
			throws SQLException {
		ContentDataSet res = new ContentDataSet();
		ArrayList<ContentData> datalist = new ArrayList<ContentData>();
		String whereclause = " WHERE RecordStatus = 1 AND n1 = '" + userSK + "' and n3<>0  ";
		if (!searchVal.equals("")) {
			whereclause += "AND t1 LIKE '%" + searchVal + "%' AND t2 LIKE '%" + searchVal + "%'  ";
		}
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecordSandE(define(), whereclause, " ORDER BY syskey desc",
				(pgdata.getStart() - 1), pgdata.getEnd(), 0, conn);
		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getDBRecord(dbrs.get(i)));
		}
		res.setPageSize(pgdata.getSize());
		res.setCurrentPage(pgdata.getCurrent());
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		PreparedStatement stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM FMR013 " + whereclause);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		ContentData[] dataarray = new ContentData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	public UserViewDataset searchContentWriterList(PagerMobileData pager, Connection conn) throws SQLException {
		UserViewDataset res = new UserViewDataset();
		String sql = "";
		ArrayList<UserViewData> datalist = new ArrayList<UserViewData>();
		sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY p.syskey) AS RowNum , u.syskey,u.t1,p.t2,u.t3,u.t6 ,u.t7,u.t8,u.t9,u.t10 From uvm012 p join uvm005 u on p.syskey=u.n4 join jun002 j on u.syskey=j.n1 join uvm009 r on j.n2=r.syskey and p.RecordStatus=1  and u.RecordStatus<>4 and r.t2='Content writer') AS RowConstrainedResult  WHERE RowNum >="
				+ pager.getStart() + " and RowNum <=" + pager.getEnd();
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rset = stmt.executeQuery();
		while (rset.next()) {
			UserViewData ret = new UserViewData();
			ret.setSyskey(rset.getLong("syskey"));
			ret.setT1(rset.getString("t1"));
			ret.setT2(rset.getString("t2"));
			ret.setT3(rset.getString("t3"));
			ret.setT6(rset.getString("t6"));
			ret.setT7(rset.getString("t7"));
			ret.setT8(rset.getString("t8"));
			ret.setT9(rset.getString("t9"));
			ret.setT10(rset.getString("t10"));
			datalist.add(ret);
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}

		String qry = "SELECT count(*) AS recCount FROM V_U001 WHERE syskey IN (SELECT n1 FROM JUN002 WHERE RecordStatus<>4 AND n2 IN (SELECT syskey FROM UVM009 WHERE t2='Content Writer'))";

		PreparedStatement stat = conn.prepareStatement(qry);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		res.setPageSize(pager.getSize());
		res.setCurrentPage(pager.getCurrent());
		UserViewData[] dataarry = new UserViewData[datalist.size()];
		dataarry = datalist.toArray(dataarry);
		res.setData(dataarry);
		return res;
	}

	public DBRecord setDBRecord(ContentData data) {
		DBRecord ret = define();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("createddate", data.getCreatedDate());
		ret.setValue("modifieddate", data.getModifiedDate());
		ret.setValue("userid", data.getUserId());
		ret.setValue("username", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("usersyskey", data.getUserSyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t5", data.getT5());
		ret.setValue("t6", data.getT6());
		ret.setValue("t7", data.getT7());
		ret.setValue("t8", data.getT8());
		ret.setValue("t9", data.getT9());
		ret.setValue("t10", data.getT10());
		ret.setValue("t11", data.getT11());
		ret.setValue("t12", data.getT12());
		ret.setValue("t13", data.getT13());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		ret.setValue("n6", data.getN6());
		ret.setValue("n7", data.getN7());
		ret.setValue("n8", data.getN8());
		ret.setValue("n9", data.getN9());
		ret.setValue("n10", data.getN10());
		ret.setValue("n11", data.getN11());
		ret.setValue("n12", data.getN12());
		ret.setValue("n13", data.getN13());
		return ret;
	}

	/*
	 * public Result update(ContentData obj, Connection conn) throws
	 * SQLException { Result res = new Result(); String sql =
	 * DBMgr.updateString(" WHERE RecordStatus = 1 AND n1 =" + obj.getN1() +
	 * " AND n2=" + obj.getN2(),define(), conn); PreparedStatement stmt =
	 * conn.prepareStatement(sql); DBRecord dbr = setDBRecord(obj);
	 * DBMgr.setValues(stmt, dbr); int count = stmt.executeUpdate(); if (count >
	 * 0) { System.out.println(count); res.setState(true); res.setMsgDesc(
	 * "Updated Successfully!"); } return res; }
	 */
	public ResultMobile update(ContentData obj, Connection conn) throws SQLException {
		ResultMobile res = new ResultMobile();
		String query = "UPDATE FMR013 SET [syskey]=?, [createddate]=?, [modifieddate]=?, [userid]=?, [username]=?, "
				+ " [recordstatus]=?, [syncstatus]=?, [syncbatch]=?,[usersyskey]=?, [t1]=?, [t2]=?, [t3]=?,[t4]=?, [t5]=?,"
				+ " [t6]=?, [t7]=?,[t8]=?, [t9]=?,[t10]=?, [t11]=?,[t12]=?, [t13]=?, [n1]=?, [n2]=?, [n3]=?,"
				+ " [n4]=?, [n5]=?,[n6]=?, [n7]=?,[n8]=?, [n9]=?,[n10]=?, [n11]=?,[n12]=?, [n13]=?"
				+ "WHERE RecordStatus = 1 AND n1 =" + obj.getN1() + " AND n2=" + obj.getN2() + "";

		try {
			// if (isCodeExist(obj, conn)) {
			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setLong(1, obj.getSyskey());
			pstmt.setString(2, obj.getCreatedDate());
			pstmt.setString(3, obj.getModifiedDate());
			pstmt.setString(4, obj.getUserId());
			pstmt.setString(5, obj.getUserName());
			pstmt.setInt(6, obj.getRecordStatus());
			pstmt.setInt(7, obj.getSyncStatus());
			pstmt.setLong(8, obj.getSyncBatch());
			pstmt.setLong(9, obj.getUserSyskey());
			pstmt.setString(10, obj.getT1());
			pstmt.setString(11, obj.getT2());
			pstmt.setString(12, obj.getT3());
			pstmt.setString(13, obj.getT4());
			pstmt.setString(14, obj.getT5());
			pstmt.setString(15, obj.getT6());
			pstmt.setString(16, obj.getT7());
			pstmt.setString(17, obj.getT8());
			pstmt.setString(18, obj.getT9());
			pstmt.setString(19, obj.getT10());
			pstmt.setString(20, obj.getT11());
			pstmt.setString(21, obj.getT12());
			pstmt.setString(22, obj.getT13());
			pstmt.setLong(23, obj.getN1());
			pstmt.setLong(24, obj.getN2());
			pstmt.setLong(25, obj.getN3());
			pstmt.setLong(26, obj.getN4());
			pstmt.setLong(27, obj.getN5());
			pstmt.setLong(28, obj.getN6());
			pstmt.setLong(29, obj.getN7());
			pstmt.setLong(30, obj.getN8());
			pstmt.setLong(31, obj.getN9());
			pstmt.setLong(32, obj.getN10());
			pstmt.setLong(33, obj.getN11());
			pstmt.setLong(34, obj.getN12());
			pstmt.setLong(35, obj.getN13());

			if (pstmt.executeUpdate() > 0) {
				res.setState(true);
				res.setMsgDesc("Updated Successfully!");
			} else {
				res.setMsgDesc("No Such Article to Update!");
			}

		} catch (SQLException e) {

		}

		return res;
	}

	// TDA DELETE SAVE CONTENT
	public ResultMobile updatesavecontent(ContentData data, Connection conn) throws SQLException {
		ResultMobile res = new ResultMobile();
		String sql = "UPDATE FMR013 SET N3=0 WHERE n1=? and n2=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, data.getN1());
		stmt.setLong(2, data.getN2());
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Updated Successfully");
		} else {
			res.setMsgDesc("Updated Unsuccessful");
		}
		return res;
	}
}
