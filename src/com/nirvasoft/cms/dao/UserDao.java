package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.cms.shared.UserData;
import com.nirvasoft.cms.shared.UserRole;
import com.nirvasoft.cms.shared.UserViewData;
import com.nirvasoft.cms.shared.UserViewDataset;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;

public class UserDao {
	public static boolean canDelete(long key, Connection conn) throws SQLException {

		ArrayList<Long> dbrs = new ArrayList<Long>();
		String sql = "Select n1 From JUN002 Where n2=" + key;
		PreparedStatement stat = conn.prepareStatement(sql);
		ResultSet result = stat.executeQuery();
		while (result.next()) {
			dbrs.add(result.getLong("n1"));

		}
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("UVM005");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("CreatedDate", (byte) 5));
		ret.getFields().add(new DBField("ModifiedDate", (byte) 5));
		ret.getFields().add(new DBField("UserId", (byte) 5));
		ret.getFields().add(new DBField("UserName", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 1));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 1));
		ret.getFields().add(new DBField("n6", (byte) 1));
		ret.getFields().add(new DBField("n7", (byte) 1));
		ret.getFields().add(new DBField("n8", (byte) 2));

		return ret;
	}

	public static DBRecord define(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 1));
		ret.getFields().add(new DBField("n4", (byte) 1));
		ret.getFields().add(new DBField("n5", (byte) 2));

		return ret;
	}

	public static DBRecord defineView() {
		DBRecord ret = new DBRecord();
		ret.setTableName("V_U001");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("autokey", (byte) 2));
		ret.getFields().add(new DBField("CreatedDate", (byte) 5));
		ret.getFields().add(new DBField("ModifiedDate", (byte) 5));
		ret.getFields().add(new DBField("UserId", (byte) 5));
		ret.getFields().add(new DBField("UserName", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("UN", (byte) 5));

		return ret;
	}

	public static Resultb2b delete(long syskey, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();

		res = PersonDao.deletePerson(syskey, conn);

		if (res.isState()) {
			// updated by sml
			long skey = 0;
			String qry = "SELECT * FROM UVM005 WHERE n4=?";
			PreparedStatement pst = conn.prepareStatement(qry);
			pst.setLong(1, syskey);
			ResultSet result = pst.executeQuery();

			while (result.next()) {
				skey = result.getLong("syskey");
			}

			String sql = "UPDATE JUN002 SET RecordStatus=4 WHERE n1=?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, skey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {

				sql = "UPDATE UVM005 SET RecordStatus=4 WHERE Syskey=?";
				stmt = conn.prepareStatement(sql);
				stmt.setLong(1, skey);
				rs = stmt.executeUpdate();
				if (rs > 0) {
					res.setState(true);
					res.setMsgDesc("Deleted Successfully!");
				}
			}

		}

		return res;
	}

	public static UserViewDataset getAllUserData(MrBean user, Connection conn) throws SQLException {

		ArrayList<UserViewData> ret = new ArrayList<UserViewData>();
		String whereClause = " WHERE RecordStatus<>4 ";

		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(defineView(), whereClause, " ORDER BY syskey", conn);

		for (int i = 0; i < dbrs.size(); i++) {
			ret.add(getDBViewRecord(dbrs.get(i)));
		}

		UserViewDataset dataSet = new UserViewDataset();
		UserViewData[] dataarry = new UserViewData[ret.size()];
		dataarry = ret.toArray(dataarry);
		dataSet.setData(dataarry);

		return dataSet;
	}

	public static UserData getDBRecord(DBRecord adbr) {
		UserData ret = new UserData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("CreatedDate"));
		ret.setModifiedDate(adbr.getString("ModifiedDate"));
		ret.setUserId(adbr.getString("UserId"));
		ret.setUserName(adbr.getString("UserName"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));

		ret.setUsersyskey(adbr.getLong("usersysKey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(ServerUtil.decryptPIN(adbr.getString("t2")));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getInt("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getInt("n5"));
		ret.setN6(adbr.getInt("n6"));
		ret.setN7(adbr.getInt("n7"));
		ret.setN8(adbr.getLong("n8"));

		return ret;
	}

	public static UserRole getDBRecords(DBRecord adbr) {
		UserRole ret = new UserRole();
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));

		ret.setUsersyskey(adbr.getLong("usersysKey"));

		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getInt("n3"));
		ret.setN4(adbr.getInt("n4"));
		ret.setN5(adbr.getInt("n5"));

		return ret;
	}

	public static UserViewData getDBViewRecord(DBRecord adbr) {
		UserViewData ret = new UserViewData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setAutokey(adbr.getLong("autokey"));
		ret.setCreatedDate(adbr.getString("CreatedDate"));
		ret.setModifiedDate(adbr.getString("ModifiedDate"));
		ret.setUserId(adbr.getString("UserId"));
		ret.setUserName(adbr.getString("UserName"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setUsername(adbr.getString("UN"));

		return ret;
	}

	public static long getPersonSyskey(long usys, Connection con) throws SQLException {
		String sql = "SELECT * FROM UVM005 WHERE syskey=" + usys;
		PreparedStatement stat = con.prepareStatement(sql);
		ResultSet result = stat.executeQuery();
		long u = 0;
		while (result.next()) {
			u = result.getLong("n4");

		}
		return u;

	}

	public static int getUserCount(String searchVal, Connection conn) throws SQLException {
		String whereclause = " WHERE RecordStatus<>4 ";
		if (!searchVal.equals("")) {
			whereclause += "AND t1 LIKE '%" + searchVal + "%' OR UN LIKE '%" + searchVal + "%' ";
		}
		int res = 1;
		PreparedStatement stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM V_U001" + whereclause);
		ResultSet result = stat.executeQuery();
		result.next();
		res = result.getInt("recCount");
		return res;
	}

	public static Resultb2b insert(UserData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		if (!isCodeExist(obj, conn)) {
			String sql = DBMgr.insertString(define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			int rs = stmt.executeUpdate();

			if (rs > 0) {

				UserRole jun = new UserRole();
				for (long l : obj.getRolesyskey()) {

					if (l != 0) {

						jun.setRecordStatus(obj.getRecordStatus());
						jun.setSyncBatch(obj.getSyncBatch());
						jun.setSyncStatus(obj.getSyncStatus());
						jun.setUsersyskey(obj.getSyskey());
						jun.setN1(obj.getSyskey());
						jun.setN2(l);
						res = insertUserRole(jun, conn);

					}

				}

				if (res.isState()) {

					res.setState(true);
					res.setMsgDesc("Saved Successfully!");

				} else
					res.setMsgDesc("Can't Save!");

			}
		} else
			res.setMsgDesc("Code already exist!");

		return res;
	}

	public static Resultb2b insertUserRole(UserRole obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = DBMgr.insertString(define("JUN002"), conn);
		PreparedStatement stmt = conn.prepareStatement(sql);
		DBRecord dbr = setDBRecord(obj);
		DBMgr.setValues(stmt, dbr);
		int rs = stmt.executeUpdate();

		if (rs > 0) {

			res.setState(true);
		}

		return res;
	}

	public static boolean isCodeExist(UserData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				" WHERE RecordStatus<>4 AND syskey <> " + obj.getSyskey() + " AND T1=\'" + obj.getT1() + "\'", "",
				conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static UserData read(long syskey, Connection conn) throws SQLException {

		UserData ret = new UserData();
		// ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "WHERE
		// RecordStatus<>4 AND n4=" + syskey, "", conn);
		// if (dbrs.size() > 0)
		// ret = getDBRecord(dbrs.get(0));
		String sql = "select * from uvm005 where recordstatus<>4 and n4=" + syskey;
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();

		rs.next();

		ret.setSyskey(rs.getLong("syskey"));
		ret.setAutokey(rs.getLong("autokey"));
		ret.setUserId(rs.getString("userid"));
		ret.setUserName(rs.getString("username"));
		ret.setUserId(rs.getString("userid"));
		ret.setT1(rs.getString("t1"));
		ret.setT2(ServerUtil.decryptPIN(rs.getString("t2")));
		ret.setT3(rs.getString("t3"));
		ret.setT4(rs.getString("t4"));
		ret.setT5(rs.getString("t5"));
		ret.setPhoneno(rs.getString("t6"));
		ret.setAddress(rs.getString("t7"));
		// ret.setUploadlist(uploadlist);(rs.getString("t8"));
		ret.setDob(rs.getString("t9"));
		ret.setGender(rs.getString("t10"));
		return ret;

	}

	public static UserData readByID(String aObj, Connection conn) throws SQLException {
		UserData data = new UserData();

		System.out.println(aObj);
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "where RecordStatus<>4 AND t1='" + aObj + "'", "",
				conn);
		if (dbrs.size() > 0) {
			data = getDBRecord(dbrs.get(0));

			/*
			 * data.setT2(ServerUtil.decryptPIN(data.getT2()));
			 * System.out.println(dbrs.size() + " user sys" + data.getT2());
			 */

		}
		return data;
	}

	public static UserViewDataset searchContentWriterList(PagerData pager, String searchVal, Connection conn)
			throws SQLException {
		UserViewDataset res = new UserViewDataset();
		String sql = "";
		ArrayList<UserViewData> datalist = new ArrayList<UserViewData>();
		String whereclause = " ";
		if (!searchVal.equals("")) {
			whereclause += " AND (p.t2 LIKE '%" + searchVal + "%' OR u.t1 LIKE '%" + searchVal + "%' OR u.t3 LIKE '%"
					+ searchVal + "%' OR u.t6 LIKE '%" + searchVal + "%' OR u.t7 LIKE '%" + searchVal + "%') ";
		}
		sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY p.syskey) AS RowNum , u.syskey,u.t1,p.t2,u.t3,u.t6 ,u.t7 From uvm012 p join uvm005 u on p.syskey=u.n4 join jun002 j on u.syskey=j.n1 join uvm009 r on j.n2=r.syskey and p.RecordStatus=1  and u.RecordStatus<>4 and r.t2<>'master' and r.t2<>'publisher' and r.t2<>'editor' and r.t2<>'admin'"
				+ whereclause + ") AS RowConstrainedResult  WHERE RowNum >=" + pager.getStart() + " and RowNum <="
				+ pager.getEnd();
		System.out.println("sql: " + sql);
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rset = stmt.executeQuery();
		while (rset.next()) {
			UserViewData ret = new UserViewData();
			ret.setSyskey(rset.getLong("syskey"));
			ret.setT1(rset.getString("t1"));
			ret.setT2(rset.getString("t2"));
			ret.setT3(rset.getString("t3"));
			ret.setT6(rset.getString("t6"));
			ret.setT7(rset.getString("t7"));
			datalist.add(ret);
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}

		String qry = "select count(*) as recCount from ( SELECT ROW_NUMBER() OVER (ORDER BY p.syskey) AS RowNum , u.t1,p.t2,u.t3,u.t6 ,u.t7 From uvm012 p join uvm005 u on p.syskey=u.n4 join jun002 j on u.syskey=j.n1 join uvm009 r on j.n2=r.syskey and p.RecordStatus=1  and u.RecordStatus<>4 and r.t2<>'master' and r.t2<>'publisher' and r.t2<>'editor' and r.t2<>'admin'"
				+ whereclause + ") AS RowConstrainedResult  WHERE RowNum >=" + pager.getStart() + " and RowNum <="
				+ pager.getEnd();

		PreparedStatement stat = conn.prepareStatement(qry);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		res.setPageSize(pager.getSize());
		res.setCurrentPage(pager.getCurrent());
		UserViewData[] dataarry = new UserViewData[datalist.size()];
		dataarry = datalist.toArray(dataarry);
		res.setData(dataarry);
		return res;
	}

	public static UserViewDataset searchUser(PagerData pager, String searchVal, Long status, String userid,
			Connection conn) throws SQLException {
		UserViewDataset res = new UserViewDataset();
		String sql = "";
		ArrayList<UserViewData> datalist = new ArrayList<UserViewData>();
		String whereclause = " Where RecordStatus<>4 ";
		String whereclause1 = "";
		if (!searchVal.equals("")) {
			whereclause += " AND( t1 LIKE '%" + searchVal + "%' OR t2 LIKE '%" + searchVal + "%') ";
			whereclause1 += " AND( u.t1 LIKE '%" + searchVal + "%' OR p.t2 LIKE '%" + searchVal + "%') ";
		}
		if (status == 1)// contentwriter
		{
			whereclause1 += " and p.RecordStatus<>4 AND p.t1='" + userid + "' ";
			/*
			 * sql =
			 * "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY syskey)AS RowNum , * from uvm012 "
			 * + whereclause +
			 * " and syskey in (select n4 from   uvm005 where syskey in (select n1 from jun002 where n2 in(select syskey from uvm009 where t2<>'master' and t2<>'publisher' and t2<>'editor' ))))AS RowConstrainedResult  WHERE RowNum >="
			 * + pager.getStart() + " and RowNum <=" + pager.getEnd();
			 */
			sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY p.syskey) AS RowNum , p.syskey,u.t1,p.t2,u.t3,u.t6 ,u.t7,r.t2 as typ From uvm012 p join uvm005 u on p.syskey=u.n4 join jun002 j on u.syskey=j.n1 join uvm009 r on j.n2=r.syskey and p.RecordStatus=1  and u.RecordStatus<>4 where r.t2='Content Writer' "
					+ whereclause1 + ") AS RowConstrainedResult  WHERE RowNum >=" + pager.getStart() + " and RowNum <="
					+ pager.getEnd();

			System.out.println("sql : " + sql);

		}
		if (status == 2)// editor
		{
			/*
			 * sql =
			 * "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY syskey)AS RowNum , * from uvm012 "
			 * + whereclause +
			 * " and syskey in (select n4 from   uvm005 where syskey in (select n1 from jun002 where n2 in(select syskey from uvm009 where t2<>'master' and t2='editor' ))))AS RowConstrainedResult  WHERE RowNum >="
			 * + pager.getStart() + " and RowNum <=" + pager.getEnd();
			 */
			sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY p.syskey) AS RowNum , p.syskey,u.t1,p.t2,u.t3,u.t6 ,u.t7,r.t2 as typ From uvm012 p join uvm005 u on p.syskey=u.n4 join jun002 j on u.syskey=j.n1 join uvm009 r on j.n2=r.syskey and p.RecordStatus=1  and u.RecordStatus<>4 where r.t2='editor' "
					+ whereclause1 + ") AS RowConstrainedResult  WHERE RowNum >=" + pager.getStart() + " and RowNum <="
					+ pager.getEnd();

		}
		if (status == 3)// publisher
		{
			/*
			 * sql =
			 * "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY syskey)AS RowNum , * from uvm012 "
			 * + whereclause +
			 * " and syskey in (select n4 from   uvm005 where syskey in (select n1 from jun002 where n2 in(select syskey from uvm009 where t2<>'master'  ))))AS RowConstrainedResult  WHERE RowNum >="
			 * + pager.getStart() + " and RowNum <=" + pager.getEnd();
			 */
			sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY p.syskey) AS RowNum , p.syskey,u.t1,p.t2,u.t3,u.t6 ,u.t7,r.t2 as typ From uvm012 p join uvm005 u on p.syskey=u.n4 join jun002 j on u.syskey=j.n1 join uvm009 r on j.n2=r.syskey and p.RecordStatus=1  and u.RecordStatus<>4 where r.t2<>'master' "
					+ whereclause1 + ") AS RowConstrainedResult  WHERE RowNum >=" + pager.getStart() + " and RowNum <="
					+ pager.getEnd();
		}

		if (status == 4)// master
		{
			/*
			 * sql =
			 * "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY syskey)AS RowNum , * from uvm012 "
			 * + whereclause +
			 * " and syskey in (select n4 from   uvm005 where syskey in (select n1 from jun002 where n2 in(select syskey from uvm009))))AS RowConstrainedResult  WHERE RowNum >="
			 * + pager.getStart() + " and RowNum <=" + pager.getEnd();
			 */
			sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY p.syskey) AS RowNum , p.syskey,u.t1,p.t2,u.t3,u.t6 ,u.t7,r.t2 as typ From uvm012 p join uvm005 u on p.syskey=u.n4 join jun002 j on u.syskey=j.n1 join uvm009 r on j.n2=r.syskey and p.RecordStatus=1  and u.RecordStatus<>4 "
					+ whereclause1 + ") AS RowConstrainedResult  WHERE RowNum >=" + pager.getStart() + " and RowNum <="
					+ pager.getEnd();

		}
		if (status == 5)// admin
		{
			/*
			 * sql =
			 * "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey) AS RowNum,* FROM V_U001"
			 * + whereclause +
			 * " and t1<>'mit') AS RowConstrainedResult  WHERE RowNum >= " +
			 * pager.getStart() + " and RowNum <= " + pager.getEnd();
			 */
			/*
			 * sql =
			 * "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY syskey)AS RowNum , * from uvm012 "
			 * + whereclause +
			 * " and syskey in (select n4 from   uvm005 where syskey in (select n1 from jun002 where n2 in(select syskey from uvm009 where t2='Admin' or t2='Content Writer'  ))))AS RowConstrainedResult  WHERE RowNum >="
			 * + pager.getStart() + " and RowNum <=" + pager.getEnd();
			 */

			sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY p.syskey) AS RowNum , p.syskey,u.t1,p.t2,u.t3,u.t6 ,u.t7,r.t2 as typ From uvm012 p join uvm005 u on p.syskey=u.n4 join jun002 j on u.syskey=j.n1 join uvm009 r on j.n2=r.syskey and p.RecordStatus=1  and u.RecordStatus<>4 where (r.t2='Admin' or r.t2='Content Writer') "
					+ whereclause1 + ") AS RowConstrainedResult  WHERE RowNum >=" + pager.getStart() + " and RowNum <="
					+ pager.getEnd();

		}
		System.out.println("sql in list: " + sql);

		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rset = stmt.executeQuery();
		while (rset.next()) {
			UserViewData ret = new UserViewData();
			/*
			 * ret.setSyskey(rset.getLong("syskey"));
			 * ret.setAutokey(rset.getLong("autokey"));
			 * ret.setCreatedDate(rset.getString("CreatedDate"));
			 * ret.setModifiedDate(rset.getString("ModifiedDate"));
			 * ret.setUserId(rset.getString("UserId"));
			 * ret.setUserName(rset.getString("UserName"));
			 * ret.setRecordStatus(rset.getInt("RecordStatus"));
			 * ret.setSyncStatus(rset.getInt("SyncStatus"));
			 * ret.setSyncBatch(rset.getLong("SyncBatch"));
			 * ret.setT1(rset.getString("t1")); ret.setT2(rset.getString("t2"));
			 * ret.setT3(rset.getString("t3"));
			 */
			ret.setSyskey(rset.getLong("syskey"));
			ret.setT1(rset.getString("t1"));
			ret.setT2(rset.getString("t2"));
			ret.setT3(rset.getString("t3"));
			ret.setT6(rset.getString("t6"));
			ret.setT7(rset.getString("t7"));
			ret.setTyp(rset.getString("typ"));

			datalist.add(ret);
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		// updated by sml
		String qry = "";
		if (status == 4) { // master
			// qry = "SELECT count(*) AS recCount FROM V_U001 WHERE syskey IN
			// (SELECT n1 FROM JUN002 WHERE RecordStatus<>4 AND n2 IN (SELECT
			// syskey FROM UVM009))";
			qry = "SELECT count(*) AS  recCount from ( SELECT ROW_NUMBER() OVER (ORDER BY syskey)AS RowNum , * from uvm012 "
					+ whereclause
					+ " and syskey in (select n4 from   uvm005 where syskey in (select n1 from jun002 where n2 in(select syskey from uvm009))))AS RowConstrainedResult";
		} else if (status == 3) { // publisher
			// qry = "SELECT count(*) AS recCount FROM V_U001 WHERE syskey IN
			// (SELECT n1 FROM JUN002 WHERE RecordStatus<>4 AND n2 IN (SELECT
			// syskey FROM UVM009 WHERE t2<>'Master'))";
			qry = "SELECT count(*) AS recCount from ( SELECT ROW_NUMBER() OVER (ORDER BY syskey)AS RowNum , * from uvm012 "
					+ whereclause
					+ " and syskey in (select n4 from   uvm005 where syskey in (select n1 from jun002 where n2 in(select syskey from uvm009 where t2<>'master'  ))))AS RowConstrainedResult";
		} else if (status == 2) { // editor
			// qry = "SELECT count(*) AS recCount FROM V_U001 WHERE syskey IN
			// (SELECT n1 FROM JUN002 WHERE RecordStatus<>4 AND n2 IN (SELECT
			// syskey FROM UVM009 WHERE t2='Editor'))";
			qry = "SELECT count(*) AS recCount from ( SELECT ROW_NUMBER() OVER (ORDER BY syskey)AS RowNum , * from uvm012 "
					+ whereclause
					+ " and syskey in (select n4 from   uvm005 where syskey in (select n1 from jun002 where n2 in(select syskey from uvm009 where t2<>'master' and t2='editor' ))))AS RowConstrainedResult";
		} else if (status == 1) { // content writer
			// qry = "SELECT count(*) AS recCount FROM V_U001 WHERE syskey IN
			// (SELECT n1 FROM JUN002 WHERE RecordStatus<>4 AND n2 IN (SELECT
			// syskey FROM UVM009 WHERE t2<>'Master' and t2<>'publisher'))";
			// qry = "SELECT count(*) recCount FROM UVM005 WHERE t1 = '" +
			// userid + "'";
			whereclause += " AND t1='" + userid + "' ";
			qry = "SELECT count(*) AS recCount from ( SELECT ROW_NUMBER() OVER (ORDER BY syskey)AS RowNum , * from uvm012 "
					+ whereclause
					+ " and syskey in (select n4 from   uvm005 where syskey in (select n1 from jun002 where n2 in(select syskey from uvm009 where t2<>'master' and t2<>'publisher' and t2<>'editor' ))))AS RowConstrainedResult";
		} else if (status == 5) { // admin
			qry = "select count(*) AS recCount from ( SELECT ROW_NUMBER() OVER (ORDER BY syskey)AS RowNum , * from uvm012 "
					+ whereclause
					+ " and syskey in (select n4 from   uvm005 where syskey in (select n1 from jun002 where n2 in(select syskey from uvm009 where t2='Admin' or t2='Content Writer'  ))))AS RowConstrainedResult";
		}
		//

		// PreparedStatement stat = conn.prepareStatement("SELECT COUNT(*) AS
		// recCount FROM V_U001" + whereclause);
		PreparedStatement stat = conn.prepareStatement(qry);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		res.setPageSize(pager.getSize());
		res.setCurrentPage(pager.getCurrent());
		UserViewData[] dataarry = new UserViewData[datalist.size()];
		dataarry = datalist.toArray(dataarry);
		res.setData(dataarry);
		return res;
	}

	public static DBRecord setDBRecord(UserData data) {
		DBRecord ret = define();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("CreatedDate", data.getCreatedDate());
		ret.setValue("ModifiedDate", data.getModifiedDate());
		ret.setValue("UserId", data.getUserId());
		ret.setValue("UserName", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());

		ret.setValue("usersysKey", data.getUsersyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t3", data.getT3());
		ret.setValue("t5", data.getT5());
		ret.setValue("t6", data.getT6());
		ret.setValue("t7", data.getT7());
		ret.setValue("t8", data.getT8());
		ret.setValue("t9", data.getT9());
		ret.setValue("t10", data.getT10());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		ret.setValue("n6", data.getN6());
		ret.setValue("n7", data.getN7());
		ret.setValue("n8", data.getN8());
		return ret;
	}

	public static DBRecord setDBRecord(UserRole data) {
		DBRecord ret = define("JUN002");
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("usersysKey", data.getUsersyskey());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		return ret;
	}

	public static Resultb2b update(UserData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		if (!isCodeExist(obj, conn)) {
			String sql = DBMgr.updateString(" WHERE RecordStatus<>4 AND Syskey=" + obj.getSyskey(), define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			int rs = stmt.executeUpdate();

			if (rs > 0) {

				UserRole jun = new UserRole();

				for (long l : obj.getRolesyskey()) {

					jun.setN1(obj.getSyskey());
					jun.setN2(l);

					sql = "DELETE FROM JUN002 WHERE n1=?";
					stmt = conn.prepareStatement(sql);
					stmt.setLong(1, jun.getN1());
					stmt.executeUpdate();

				}

				for (long l : obj.getRolesyskey()) {

					if (l != 0) {

						jun.setRecordStatus(obj.getRecordStatus());
						jun.setSyncBatch(obj.getSyncBatch());
						jun.setSyncStatus(obj.getSyncStatus());
						jun.setUsersyskey(obj.getUsersyskey());

						jun.setN1(obj.getSyskey());
						jun.setN2(l);
						res = insertUserRole(jun, conn);

					}

				}

				if (res.isState()) {

					res.setState(true);
					res.setMsgDesc("Updated Successfully!");

				} else
					res.setMsgDesc("Can't Update!");

			}
		} else
			res.setMsgDesc("Code already exist!");
		return res;
	}

}