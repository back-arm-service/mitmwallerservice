package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.cms.shared.ButtonData;
import com.nirvasoft.cms.shared.RoleMenuData;

public class RoleMenuDao {

	public static RoleMenuData[] getChildMenuData(long skey, Connection conn) throws SQLException {

		ArrayList<RoleMenuData> ret = new ArrayList<RoleMenuData>();
		RoleMenuData data = new RoleMenuData();
		try {
			String sql = "SELECT DISTINCT t2, t3, MIN(syskey) AS syskey  ," + " MIN(n2) AS n2 FROM View_Menu WHERE n2="
					+ skey + "GROUP BY t2, t3 ORDER BY syskey";
			PreparedStatement stat = conn.prepareStatement(sql);
			ResultSet result = stat.executeQuery();

			while (result.next()) {
				data = new RoleMenuData();
				data.setSyskey(result.getLong("syskey"));
				data.setT2(result.getString("t2"));
				String t3 = result.getString("t3");
				data.setT3(t3);
				data.setN2(result.getLong("n2"));
				if (!t3.isEmpty()) {
					String[] strs = t3.split(",");
					ArrayList<ButtonData> btnlist = new ArrayList<ButtonData>();
					for (int i = 0; i < strs.length; i++) {
						ButtonData btn = ButtonDataDao.getButtonListByKey(conn, strs[i]);
						btnlist.add(btn);
					}
					ButtonData[] btns = new ButtonData[btnlist.size()];
					btns = btnlist.toArray(btns);
					data.setBtns(btns);
				}
				ret.add(data);
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		RoleMenuData[] dataarray = new RoleMenuData[ret.size()];
		dataarray = ret.toArray(dataarray);

		return dataarray;
	}

	public static RoleMenuData[] getChildMenuData(long pkey, long skey1, long skey2, Connection conn)
			throws SQLException {

		ArrayList<RoleMenuData> ret = new ArrayList<RoleMenuData>();
		RoleMenuData data = new RoleMenuData();
		try {

			String sql = "SELECT DISTINCT t2, t3, MIN(syskey) AS syskey  ," + " MIN(n2) AS n2 FROM View_Menu WHERE n2="
					+ skey2 + "GROUP BY t2, t3 ORDER BY syskey";

			PreparedStatement stat = conn.prepareStatement(sql);
			ResultSet result = stat.executeQuery();

			while (result.next()) {
				data = new RoleMenuData();
				data.setSyskey(result.getLong("syskey"));
				data.setT2(result.getString("t2"));
				String t3 = result.getString("t3");
				data.setT3(t3);
				data.setN2(result.getLong("n2"));
				if (!t3.isEmpty()) {
					String[] strs = t3.split(",");
					ArrayList<ButtonData> btnlist = new ArrayList<ButtonData>();
					for (int i = 0; i < strs.length; i++) {
						ButtonData btn = ButtonDataDao.getButtonListByKey(conn, strs[i]);
						String temp = RoleDao.getRoleMenuBtn(skey1, result.getLong("syskey"), conn);
						String[] temps = temp.split(",");
						for (int j = 0; j < temps.length; j++) {
							if (temps[j].equals(String.valueOf(btn.getSyskey()))) {
								btn.setFlag(true);
							}
						}
						btnlist.add(btn);
					}
					ButtonData[] btns = new ButtonData[btnlist.size()];
					btns = btnlist.toArray(btns);
					data.setBtns(btns);
				}
				long s[] = RoleMenuDao.getChildResult(pkey, conn);
				for (int i = 0; i < s.length; i++) {

					if (s[i] == (result.getLong("syskey")))
						data.setResult(true);
				}
				ret.add(data);
			}

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		RoleMenuData[] dataarray = new RoleMenuData[ret.size()];
		dataarray = ret.toArray(dataarray);

		return dataarray;
	}

	public static long[] getChildResult(long skey, Connection conn) throws SQLException {

		ArrayList<Long> ary = new ArrayList<Long>();
		try {
			String sql = "Select syskey from View_Menu Where syskey " + "IN (Select n2 from UVM023 Where n1=" + skey
					+ ") AND n2<>0";

			PreparedStatement stat = conn.prepareStatement(sql);
			ResultSet res = stat.executeQuery();

			while (res.next()) {
				ary.add(res.getLong("syskey"));
			}

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		long[] result = new long[ary.size()];

		for (int i = 0; i < ary.size(); i++) {
			result[i] = ary.get(i);
		}
		return result;
	}

	public static long[] getMenuResult(long skey, Connection conn) throws SQLException {

		ArrayList<Long> ary = new ArrayList<Long>();
		try {
			String sql = "Select syskey from View_Menu Where syskey " + "IN (Select n2 from UVM023 Where n1=" + skey
					+ ") AND n2=0";

			PreparedStatement stat = conn.prepareStatement(sql);
			ResultSet res = stat.executeQuery();

			while (res.next()) {
				ary.add(res.getLong("syskey"));
			}

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		long[] result = new long[ary.size()];

		for (int i = 0; i < ary.size(); i++) {
			result[i] = ary.get(i);
		}

		return result;
	}

	public static RoleMenuData[] getRoleMenuList(Connection conn) {
		ArrayList<RoleMenuData> ret = new ArrayList<RoleMenuData>();
		RoleMenuData data = new RoleMenuData();
		try {
			String sql = "SELECT syskey,t2,n2 FROM View_Menu WHERE n2=0";
			PreparedStatement stat = conn.prepareStatement(sql);
			ResultSet result = stat.executeQuery();

			while (result.next()) {
				data = new RoleMenuData();
				data.setSyskey(result.getLong("syskey"));
				data.setT2(result.getString("t2"));
				data.setN2(result.getLong("n2"));
				data.setChildmenus(RoleMenuDao.getChildMenuData(result.getLong("syskey"), conn));
				ret.add(data);
			}

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		RoleMenuData[] dataarray = new RoleMenuData[ret.size()];
		dataarray = ret.toArray(dataarray);

		return dataarray;

	}

	public static RoleMenuData[] getRoleMenuList(long pkey, long skey, Connection conn) {
		ArrayList<RoleMenuData> ret = new ArrayList<RoleMenuData>();
		RoleMenuData data = new RoleMenuData();
		try {
			String sql = "SELECT syskey,t2,t3,n2 FROM View_Menu WHERE n2=0";

			PreparedStatement stat = conn.prepareStatement(sql);
			ResultSet result = stat.executeQuery();

			while (result.next()) {
				data = new RoleMenuData();
				data.setSyskey(result.getLong("syskey"));
				data.setT2(result.getString("t2"));
				data.setT3(result.getString("t3"));
				data.setN2(result.getLong("n2"));
				long s[] = RoleMenuDao.getMenuResult(pkey, conn);
				for (int i = 0; i < s.length; i++) {
					if (s[i] == result.getLong("syskey")) {
						data.setResult(true);
					}

				}
				data.setChildmenus(RoleMenuDao.getChildMenuData(pkey, skey, result.getLong("syskey"), conn));
				ret.add(data);
			}

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		RoleMenuData[] dataarray = new RoleMenuData[ret.size()];
		dataarray = ret.toArray(dataarray);

		return dataarray;

	}

}