package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.nirvasoft.rp.data.CommRateDetailData;

public class CommRateDetailDao {

	private final String tblname = "CommRateDetail";

	public CommRateDetailData[] getComRatedataByHkey(Long pHKey, String comRef, Connection pConn) throws SQLException {

		int recCount = 0;
		String countsql = "select count(*) as recCount from " + tblname + " where hkey = ?";
		PreparedStatement ps = pConn.prepareStatement(countsql);
		ps.setLong(1, pHKey);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			recCount = rs.getInt("recCount");
		}

		CommRateDetailData[] ret = new CommRateDetailData[recCount];

		String sql = "select * from " + tblname + " where hkey = ?";

		ps = pConn.prepareStatement(sql);
		ps.setLong(1, pHKey);
		rs = ps.executeQuery();
		int j = 0;
		while (rs.next()) {
			CommRateDetailData data = new CommRateDetailData();
			data.setSyskey(rs.getLong("syskey"));
			data.setCommRef(comRef);
			data.setHkey(rs.getLong("Hkey"));
			data.setCommType(rs.getInt("CommType"));
			data.setZone(rs.getInt("Zone"));
			data.setFromAmt(rs.getDouble("FromAmt"));
			data.setToAmt(rs.getDouble("ToAmt"));
			data.setAmount(rs.getDouble("Amount"));
			data.setMinAmt(rs.getDouble("MinAmount"));
			data.setMaxAmt(rs.getDouble("MaxAmount"));
			data.setgLorAC(rs.getString("gLorAC"));
			ret[j] = data;
			j++;
		}

		return ret;

	}
}
