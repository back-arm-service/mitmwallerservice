package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.AdvancedSearchData;
import com.nirvasoft.cms.shared.ComboData;
import com.nirvasoft.cms.shared.ComboDataSet;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.cms.shared.SetupData;
import com.nirvasoft.cms.shared.SetupDataSet;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;

public class SetupDao {
	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("Configuration");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		return ret;
	}

	public static DBRecord define(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		return ret;
	}

	public static SetupData getDBRecord(DBRecord adbr) {
		SetupData ret = new SetupData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("createddate"));
		ret.setModifiedDate(adbr.getString("modifieddate"));
		ret.setUserId(adbr.getString("userid"));
		ret.setUserName(adbr.getString("username"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setUserSyskey(adbr.getLong("usersyskey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		ret.setT8(adbr.getString("t8"));
		ret.setT9(adbr.getString("t9"));
		ret.setT10(adbr.getString("t10"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getLong("n5"));
		ret.setN6(adbr.getLong("n6"));
		ret.setN7(adbr.getLong("n7"));
		ret.setN8(adbr.getLong("n8"));
		ret.setN9(adbr.getLong("n9"));
		ret.setN10(adbr.getLong("n10"));
		return ret;
	}

	public static DBRecord setDBRecord(SetupData data) {
		DBRecord ret = define();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("createddate", data.getCreatedDate());
		ret.setValue("modifieddate", data.getModifiedDate());
		ret.setValue("userid", data.getUserId());
		ret.setValue("username", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("usersyskey", data.getUserSyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t5", data.getT5());
		ret.setValue("t6", data.getT6());
		ret.setValue("t7", data.getT7());
		ret.setValue("t8", data.getT8());
		ret.setValue("t9", data.getT9());
		ret.setValue("t10", data.getT10());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		ret.setValue("n6", data.getN6());
		ret.setValue("n7", data.getN7());
		ret.setValue("n8", data.getN8());
		ret.setValue("n9", data.getN9());
		ret.setValue("n10", data.getN10());
		return ret;
	}

	/*public static Result insert(SetupData aObj, Connection aConnection)	throws SQLException {
		Result res = new Result();
		if (!isCodeExist(aObj,  aConnection)) {
			String sql = DBMgr.insertString(define(),  aConnection);
			PreparedStatement stmt =  aConnection.prepareStatement(sql);
			DBRecord dbr = setDBRecord(aObj);
			DBMgr.setValues(stmt, dbr);
			stmt.executeUpdate();
			res.setMsgDesc("Saved Successfully!");
			res.setState(true);
			return res;
		} else {
			res.setMsgDesc("Order already exist!");
			res.setState(false);
			return res;
		}
	}*/
	public static Resultb2b insert(SetupData aObj, Connection aConnection)    throws SQLException {
        Resultb2b res = new Resultb2b();    
        String query = "INSERT INTO Configuration (syskey, createddate, modifieddate, userid, username,"
                + " recordStatus, syncStatus, syncBatch,usersyskey,t1, t2, t3, t4, t5, t6, t7, t8, t9, t10,"
                + "n1, n2, n3, n4, n5, n6, n7, n8, n9, n10)"
                + " VALUES (?,?,?,?,?,?,?,?,?,?,"
                + "?,?,?,?,?,?,?,?,?,?,"
                + "?,?,?,?,?,?,?,?,?)";
        
        try {
            if (!isCodeExist(aObj,  aConnection)) {
                PreparedStatement ps = aConnection.prepareStatement(query);
                ps.setLong(1,aObj.getSyskey());
                ps.setString(2, aObj.getCreatedDate());
                ps.setString(3, aObj.getModifiedDate());
                ps.setString(4, aObj.getUserId());
                ps.setString(5, aObj.getUserName());
                ps.setInt(6, aObj.getRecordStatus());
                ps.setInt(7, aObj.getSyncStatus());
                ps.setLong(8, aObj.getSyncBatch());
                ps.setLong(9, aObj.getUserSyskey());
                ps.setString(10, aObj.getT1());
                ps.setString(11, aObj.getT2());
                ps.setString(12, aObj.getT3());
                ps.setString(13, aObj.getT4());
                ps.setString(14, aObj.getT5());
                ps.setString(15, aObj.getT6());
                ps.setString(16, aObj.getT7());
                ps.setString(17, aObj.getT8());
                ps.setString(18, aObj.getT9());
                ps.setString(19, aObj.getT10());
                ps.setLong(20, aObj.getN1());
                ps.setLong(21, aObj.getN2());
                ps.setLong(22, aObj.getN3());
                ps.setLong(23, aObj.getN4());
                ps.setLong(24, aObj.getN5());
                ps.setLong(25, aObj.getN6());
                ps.setLong(26, aObj.getN7());
                ps.setLong(27, aObj.getN8());
                ps.setLong(28, aObj.getN9());
                ps.setLong(29, aObj.getN10());
                                
                if(ps.executeUpdate()>0){
                    res.setMsgDesc("Saved Successfully!");
                    res.setState(true);
                    return res;
                }
                }
                else{
                    res.setMsgDesc("Code already exist!");
                    res.setState(false);
                    return res;
                }
           }catch (SQLException e) {
                
         }    
       
        return res;    
            
    }
	public static Resultb2b update(SetupData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		if (!isCodeExist(obj, conn)) {
			String sql = DBMgr.updateString(" WHERE RecordStatus<>4 AND Syskey=" + obj.getSyskey(), define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			int count = stmt.executeUpdate();
			if (count > 0) {
				res.setState(true);
				res.setMsgDesc("Updated Successfully!");
			}
		} else {
			res.setMsgDesc("Order already exist!");
		}
		return res;
	}
	/*public static boolean isCodeExist(SetupData aObj,Connection conn) {
		boolean b=false;
		String sqlString="";
		sqlString = "select * from Configuration where syskey='"+aObj.getSyskey()+"'";
		try{
			Statement stmts=conn.createStatement();
			ResultSet rs=stmts.executeQuery(sqlString);
			if(rs.next()){
				b=true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}	

		return b;
	}*/

	public  static  boolean isCodeExist(SetupData aObj,  Connection aConnection ) throws SQLException{

		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), " where syskey <> "+aObj.getSyskey()+" and t1 =N'" + aObj.getT1() +"'"+" and recordstatus<>4 ","" ,  aConnection  );//may 28.10.2014 Modified... syskey<>aobj.getSyskey
		if (dbrs.size()>0) {
			return true; 
		}else{
			return   false;
		}		
	}

	public static SetupDataSet callistConfiguraion(AdvancedSearchData asdata, String searchVal, String sort, String type,Connection conn) throws SQLException {
		SetupDataSet res = new SetupDataSet();
		ArrayList<SetupData> datalist = new ArrayList<SetupData>();
		String whereclause = " WHERE RecordStatus<>4 ";
		String orderclause = "";
		String sorttype="";
		String column="t1";
		if (type.equals("1")) {
			orderclause += " ORDER BY t1 ";
		} else if (type.equals("2")) {
			orderclause += " ORDER BY t2 ";
		}
		if (!orderclause.isEmpty()) {
			if (sort.equals("asc")) {
				orderclause += " asc ";
			} else {
				orderclause += " desc ";
			}
		}
		PagerData pgdata = asdata.getPager();
		if (!searchVal.equals("")) {
			whereclause += "AND ( t1 LIKE '%" + searchVal + "%' OR t2 LIKE '%" + searchVal + "%' OR t4 LIKE N'%"+ searchVal + "%' )";
		}
		String sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY "+column+""+sorttype+") AS RowNum,* FROM  Configuration"+
				whereclause+" ) AS RowConstrainedResult  WHERE RowNum >= "+pgdata.getStart()+" and RowNum <= "+pgdata.getEnd();
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rset = stmt.executeQuery();

		while(rset.next()){
			SetupData ret = new SetupData();
			ret.setSyskey(rset.getLong("syskey"));
			ret.setN1(rset.getLong("n1"));
			String sql1 = "select t2 from MobileMenu where syskey='"+ret.getN1()+"'";
			PreparedStatement stat = conn.prepareStatement(sql1);
			ResultSet reet = stat.executeQuery();
			if(reet.next()){
				ret.setT1(reet.getString("t2"));
			}
			ret.setT2(rset.getString("t1"));


			datalist.add(ret);
		}
		res.setPageSize(pgdata.getSize());
		res.setCurrentPage(pgdata.getCurrent());
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		PreparedStatement stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM Configuration " + whereclause);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		SetupData[] dataarray = new SetupData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setArr(dataarray);

		return res;
	}

	public static SetupData readConfiguration(long syskey, Connection conn)throws SQLException {
		SetupData ret = new SetupData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),"where RecordStatus<>4 AND syskey=" + syskey, "", conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));
		return ret;
	}

	public static Resultb2b deleteConfiguration(Long syskey, Connection conn)throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = "UPDATE Configuration SET RecordStatus=4 WHERE syskey=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Successfully!!!");
		} else {
			res.setMsgDesc("Deleting Unsuccessful!!!");
		}
		return res;
	}

	public static ComboDataSet getMobileMenuList(Connection conn) throws SQLException {
		ComboDataSet dataset = new ComboDataSet();
		ArrayList<ComboData> datalist = new ArrayList<ComboData>();
		ComboData combo;
		//String sql = "select syskey,t2 from Configuration WHERE  RECORDSTATUS=1 ORDER BY t1 ASC ";
		//String sql = "select syskey,t2 from uvm022 WHERE RECORDSTATUS=1 AND (t2 = 'News & Media' OR t2 = 'Education' OR t2 ='Questions & Answers' OR t2 = 'Weather' OR t2= 'Alert' OR t2 = 'Video')";
		String sql="SELECT syskey,t2 FROM MobileMenu WHERE  RECORDSTATUS=1";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			combo = new ComboData();
			combo.setCaption(res.getString("t2"));
			combo.setValue(Long.parseLong(res.getString("syskey")));
			datalist.add(combo);
		}
		ComboData[] dataarray = new ComboData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		dataset.setData(dataarray);
		return dataset;
	}

	
	public static boolean ExistPeronJun(ArrayList<ComboData> combo) {
		for(int i=0; i<combo.size(); i++){
			if(combo.get(i).getCaption().equalsIgnoreCase("ALL")){
				return true;
			}
		}
		return false;
	}
	public static SetupDataSet readconfiguration(Connection l_Conn) {
		SetupDataSet res = new SetupDataSet();
		ArrayList<SetupData> dataList = new ArrayList<SetupData>();
		try {
			String whereclause = "WHERE recordstatus <> 4";
			ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("Configuration"), whereclause, "", l_Conn);
			for (int i = 0; i < dbrs.size(); i++) {
				dataList.add(getDBRecord(dbrs.get(i)));
			}
			if(dataList.size()>0){
				res.setState(true);
				SetupData[] dataArray = new SetupData[dataList.size()];
				dataArray = dataList.toArray(dataArray);
				res.setArr(dataArray);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			ServerUtil.closeConnection(l_Conn);
		}

		return res;
	}
	
	/*searchDownloadList*/
	public static SetupDataSet searchDownloadList(PagerData pgdata, Connection conn) throws SQLException {
		SetupDataSet res = new SetupDataSet();
		ArrayList<SetupData> datalist = new ArrayList<SetupData>();
		PreparedStatement stat;
		String whereclause = " WHERE RecordStatus<>4 ";
		if (!pgdata.getT1().equals("")) {
			whereclause += "AND (t1 LIKE N'%" + pgdata.getT1() + "%' OR  modifieddate LIKE N'%" + pgdata.getT1()
					+ "%' OR  t2 LIKE N'%" + pgdata.getT1()+"%')";
		}
		/*String sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc) AS RowNum,* FROM ( select * from FMR017 "
				+ whereclause + ") b)  AS RowConstrainedResult WHERE RowNum >= '" + (pgdata.getStart() - 1)
				+ "' AND RowNum <= '" + pgdata.getEnd() + "' ";*/
		
		String sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY t1) AS RowNum,* FROM ( select  count(*)as recount ,modifieddate,t2,t1  from FMR017 "
				+ whereclause +  " group by modifieddate,t2,t1" +") b)  AS RowConstrainedResult WHERE RowNum >= '" + (pgdata.getStart() - 1)
				+ "' AND RowNum <= '" + pgdata.getEnd() + "' ";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			SetupData data = new SetupData();
			data.setT1(rs.getString("t1"));//version
			data.setT2(rs.getString("t2"));// type
			data.setModifiedDate(rs.getString("modifieddate"));
			data.setT4(rs.getString("recount"));// totalcount
			String sql1="select max(t3) as time from fmr017 where recordstatus<>4 and modifieddate='"+data.getModifiedDate()+"' and t2='"+data.getT2()+"'";
			PreparedStatement ps1 = conn.prepareStatement(sql1);
			ResultSet rs1 = ps1.executeQuery();
			if(rs1.next()){
				data.setT3(rs1.getString("time"));
			}
			datalist.add(data);
		}
		res.setPageSize(pgdata.getSize());
		res.setCurrentPage(pgdata.getCurrent());
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM FMR017 " + whereclause);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		SetupData[] dataarray = new SetupData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setArr(dataarray);
		return res;
	}

}
