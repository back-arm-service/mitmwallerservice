package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.ButtonCarryData;
import com.nirvasoft.cms.shared.MenuRole;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.cms.shared.RoleData;
import com.nirvasoft.cms.shared.RoleDataset;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;

public class RoleDao {
	public static boolean canDelete(long key, Connection conn) throws SQLException {

		ArrayList<Long> dbrs = new ArrayList<Long>();
		String sql = "Select n1 From JUN002 Where n2=" + key + " And RecordStatus<>4";
		PreparedStatement stat = conn.prepareStatement(sql);
		ResultSet result = stat.executeQuery();
		while (result.next()) {
			dbrs.add(result.getLong("n1"));
		}
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean checkParentContain(long parent[], long child, Connection conn) throws SQLException {
		long l = 0;
		String sql = "Select n2 from View_Menu where syskey=" + child;
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			l = rs.getLong("n2");
		}

		for (int i = 0; i < parent.length; i++) {

			if (l == parent[i]) {
				return true;
			}
		}
		return false;

	}

	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("UVM009");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("CreatedDate", (byte) 5));
		ret.getFields().add(new DBField("ModifiedDate", (byte) 5));
		ret.getFields().add(new DBField("UserId", (byte) 5));
		ret.getFields().add(new DBField("UserName", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		return ret;
	}

	public static DBRecord define(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("CreatedDate", (byte) 5));
		ret.getFields().add(new DBField("ModifiedDate", (byte) 5));
		ret.getFields().add(new DBField("UserId", (byte) 5));
		ret.getFields().add(new DBField("UserName", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));

		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 1));
		ret.getFields().add(new DBField("n5", (byte) 1));
		ret.getFields().add(new DBField("n6", (byte) 2));

		return ret;
	}

	public static Resultb2b delete(long syskey, Connection conn) throws SQLException {

		Resultb2b res = new Resultb2b();
		if (!canDelete(syskey, conn)) {
			String sql = "UPDATE UVM009 SET RecordStatus=4 WHERE Syskey=?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
			}
		}

		return res;
	}

	public static Resultb2b delete(String syskey, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = "UPDATE UVM009 SET RecordStatus=4 WHERE Syskey=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
		}
		return res;
	}

	public static Resultb2b deleteRoleMenu(long syskey, Connection conn) throws SQLException {

		Resultb2b res = new Resultb2b();
		if (!canDelete(syskey, conn)) {

			String sql = "UPDATE UVM023 SET RecordStatus=4 WHERE n1=?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
			}
			return res;
		} else
			return res;

	}

	public static ArrayList<RoleData> getAllRoleData(Map<String, Object> params, Connection conn) throws SQLException {

		ArrayList<RoleData> ret = new ArrayList<RoleData>();
		String whereClause = " WHERE RecordStatus<>4 ";

		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereClause, " ORDER BY syskey", conn);

		for (int i = 0; i < dbrs.size(); i++) {
			ret.add(getDBRecord(dbrs.get(i)));
		}

		return ret;
	}

	public static ArrayList<RoleData> getAllRoleData(MrBean user, Connection conn) throws SQLException {

		ArrayList<RoleData> ret = new ArrayList<RoleData>();
		String whereClause = " WHERE RecordStatus<>4 ";

		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereClause, " ORDER BY syskey", conn);

		for (int i = 0; i < dbrs.size(); i++) {
			ret.add(getDBRecord(dbrs.get(i)));
		}

		return ret;
	}

	public static long[] getChildMenukey(long syskey, Connection conn) throws SQLException {

		ArrayList<Long> key = new ArrayList<Long>();
		try {
			String sql = "SELECT syskey FROM UVM022 WHERE syskey IN " + "(SELECT n2 FROM UVM023  WHERE n1=" + syskey
					+ ") AND n2<> 0";
			PreparedStatement stat = conn.prepareStatement(sql);
			ResultSet result = stat.executeQuery();
			while (result.next()) {
				key.add((long) result.getInt("syskey"));

			}

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		long ret[] = new long[key.size()];
		for (int i = 0; i < key.size(); i++) {
			ret[i] = key.get(i);
		}

		return ret;
	}

	public static RoleData getDBRecord(DBRecord adbr) {
		RoleData ret = new RoleData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("CreatedDate"));
		ret.setModifiedDate(adbr.getString("ModifiedDate"));
		ret.setUserId(adbr.getString("UserId"));
		ret.setUserName(adbr.getString("UserName"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setUsersyskey(adbr.getLong("usersysKey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		return ret;
	}

	public static long[] getParentMenukey(long syskey, Connection conn) throws SQLException {

		ArrayList<Long> key = new ArrayList<Long>();
		try {
			String sql = "SELECT syskey FROM UVM022 WHERE syskey IN " + "(SELECT n2 FROM UVM023  WHERE n1=" + syskey
					+ ") AND n2=0";
			PreparedStatement stat = conn.prepareStatement(sql);
			ResultSet result = stat.executeQuery();
			while (result.next()) {
				key.add((long) result.getInt("syskey"));

			}

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		long ret[] = new long[key.size()];
		for (int i = 0; i < key.size(); i++) {
			ret[i] = key.get(i);
		}

		return ret;
	}

	public static ButtonCarryData[] getRoleBtns(long syskey, Connection conn) throws SQLException {

		ArrayList<ButtonCarryData> datalist = new ArrayList<ButtonCarryData>();
		PreparedStatement stat = conn
				.prepareStatement("SELECT * FROM UVM023 WHERE n1 = " + syskey + " AND RecordStatus <> 4");
		ResultSet result = stat.executeQuery();
		while (result.next()) {
			ButtonCarryData ret = new ButtonCarryData();
			ret.setSyskey(result.getLong("n2"));
			ret.setDesc(result.getString("t1"));
			datalist.add(ret);
		}
		ButtonCarryData[] dataarray = new ButtonCarryData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		return dataarray;
	}

	public static int getRoleCount(String searchVal, Connection conn) throws SQLException {
		String whereclause = " WHERE RecordStatus<>4 ";
		if (!searchVal.equals("")) {
			whereclause += "AND t1 LIKE '%" + searchVal + "%' OR t2 LIKE '%" + searchVal + "%'";
		}
		int res = 1;
		PreparedStatement stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM UVM009" + whereclause);
		ResultSet result = stat.executeQuery();
		result.next();
		res = result.getInt("recCount");
		return res;
	}

	public static List<RoleData> getRoleData(Connection conn) throws SQLException {

		List<RoleData> ret = new ArrayList<RoleData>();
		String whereClause = " WHERE RecordStatus<>4 ";

		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereClause, " ORDER BY syskey", conn);

		for (int i = 0; i < dbrs.size(); i++) {
			ret.add(getDBRecord(dbrs.get(i)));
		}

		return ret;
	}

	public static String getRoleMenuBtn(long syskey, long childsyskey, Connection conn) throws SQLException {
		String t1 = "";
		PreparedStatement stat = conn.prepareStatement(
				"SELECT t1 FROM UVM023 WHERE n1 = " + syskey + " AND n2 = " + childsyskey + " AND RecordStatus <> 4");
		ResultSet result = stat.executeQuery();
		while (result.next()) {
			t1 = result.getString("t1");
		}
		return t1;
	}

	public static Resultb2b insert(RoleData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();

		if (!isCodeExist(obj, conn)) {
			String sql = DBMgr.insertString(define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			stmt.executeUpdate();
			res.setState(true);
		}
		return res;
	}

	public static Resultb2b insertRoleMenu(MenuRole obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();

		if (!isRoleMenuExist(obj, conn)) {

			String sql = DBMgr.insertString(define("UVM023"), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			stmt.executeUpdate();
			res.setState(true);
		}

		return res;
	}

	public static boolean isCodeExist(RoleData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), " WHERE RecordStatus<>4 AND Syskey <> "
				+ obj.getSyskey() + " AND (t1='" + obj.getT1() + "'" + " OR t2='" + obj.getT2() + "')", "", conn);

		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isRoleMenuExist(MenuRole obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("UVM023"), " WHERE RecordStatus<>4 AND Syskey <> "
				+ obj.getSyskey() + " AND (n1=" + obj.getN1() + " AND n2=" + obj.getN2() + ")", "", conn);

		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static RoleData read(long syskey, Connection conn) throws SQLException {
		RoleData ret = new RoleData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "WHERE RecordStatus<>4 AND Syskey=" + syskey, "", conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));
		return ret;
	}

	public static RoleData readRole(long syskey, Connection conn) throws SQLException {
		RoleData ret = new RoleData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "WHERE RecordStatus<>4 AND Syskey=" + syskey, "", conn);
		if (dbrs.size() > 0) {
			ret = getDBRecord(dbrs.get(0));
		}

		return ret;
	}

	public static RoleDataset searchRolebyValue(PagerData pager, String searchVal, Long status, Connection conn)
			throws SQLException {
		RoleDataset res = new RoleDataset();
		String sql = "";
		ArrayList<RoleData> datalist = new ArrayList<RoleData>();
		String whereclause = " WHERE RecordStatus<>4 ";
		if (!searchVal.equals("")) {
			whereclause += "AND (t1 LIKE '%" + searchVal + "%' OR t2 LIKE '%" + searchVal + "%')";
		}
		if (status == 1)// const role
		{
			sql = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey) AS RowNum,* FROM UVM009" + whereclause
					+ " and t1='Content Writer' ) AS RowConstrainedResult  WHERE RowNum >= " + pager.getStart()
					+ " and RowNum <= " + pager.getEnd();

		}
		if (status == 2)// editor role
		{
			sql = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey) AS RowNum,* FROM UVM009" + whereclause
					+ " and t1='Editor' ) AS RowConstrainedResult  WHERE RowNum >= " + pager.getStart()
					+ " and RowNum <= " + pager.getEnd();

		}
		if (status == 3)// publisher role
		{
			sql = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey) AS RowNum,* FROM UVM009" + whereclause
					+ " and t1='publisher' ) AS RowConstrainedResult  WHERE RowNum >= " + pager.getStart()
					+ " and RowNum <= " + pager.getEnd();

		}
		if (status == 4)// master role
		{
			sql = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey) AS RowNum,* FROM UVM009" + whereclause
					+ " ) AS RowConstrainedResult  WHERE RowNum >= " + pager.getStart() + " and RowNum <= "
					+ pager.getEnd();

		}
		if (status == 5)// admin role
		{
			sql = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey) AS RowNum,* FROM UVM009" + whereclause
					+ " and t1<>'master'  ) AS RowConstrainedResult  WHERE RowNum >= " + pager.getStart()
					+ " and RowNum <= " + pager.getEnd();

		}
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rset = stmt.executeQuery();

		while (rset.next()) {
			RoleData ret = new RoleData();
			ret.setSyskey(rset.getLong("syskey"));
			ret.setCreatedDate(rset.getString("CreatedDate"));
			ret.setModifiedDate(rset.getString("ModifiedDate"));
			ret.setUserId(rset.getString("UserId"));
			ret.setUserName(rset.getString("UserName"));
			ret.setRecordStatus(rset.getInt("RecordStatus"));
			ret.setSyncStatus(rset.getInt("SyncStatus"));
			ret.setSyncBatch(rset.getLong("SyncBatch"));
			ret.setUsersyskey(rset.getLong("usersysKey"));
			ret.setT1(rset.getString("t1"));
			ret.setT2(rset.getString("t2"));
			ret.setT3(rset.getString("t3"));
			datalist.add(ret);
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		PreparedStatement stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM  UVM009" + whereclause);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		res.setPageSize(pager.getSize());
		res.setCurrentPage(pager.getCurrent());
		RoleData[] dataarry = new RoleData[datalist.size()];
		dataarry = datalist.toArray(dataarry);
		res.setData(dataarry);
		return res;
	}

	public static DBRecord setDBRecord(MenuRole data) {
		DBRecord ret = define("UVM022");
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("CreatedDate", data.getCreatedDate());
		ret.setValue("ModifiedDate", data.getModifiedDate());
		ret.setValue("UserId", data.getUserId());
		ret.setValue("UserName", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());

		ret.setValue("usersysKey", data.getUsersyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());

		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		ret.setValue("n6", data.getN6());

		return ret;
	}

	public static DBRecord setDBRecord(RoleData data) {
		DBRecord ret = define();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("CreatedDate", data.getCreatedDate());
		ret.setValue("ModifiedDate", data.getModifiedDate());
		ret.setValue("UserId", data.getUserId());
		ret.setValue("UserName", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("usersysKey", data.getUsersyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		return ret;
	}

	public static Resultb2b update(RoleData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		if (!isCodeExist(obj, conn)) {
			String sql = DBMgr.updateString(" WHERE RecordStatus<>4 AND Syskey=" + obj.getSyskey(), define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			stmt.executeUpdate();
			res.setState(true);
		}
		return res;
	}

	public static Resultb2b updateRoleMenu(RoleData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		int key = (int) obj.getSyskey();
		String sql = "DELETE FROM UVM023 WHERE n1=" + key;
		PreparedStatement stmt = conn.prepareStatement(sql);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
		}
		return res;
	}

}
