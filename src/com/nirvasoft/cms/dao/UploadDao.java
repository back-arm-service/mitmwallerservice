package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Result;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.PhotoUploadData;
import com.nirvasoft.cms.shared.UploadData;
import com.nirvasoft.cms.shared.UploadDataSet;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;

public class UploadDao {

	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("FMR006");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("createdtime", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("modifiedtime", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		return ret;
	}

	public static UploadData getDBRecord(DBRecord adbr) {
		UploadData ret = new UploadData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("createddate"));
		ret.setCreatedTime(adbr.getString("createdtime"));
		ret.setModifiedDate(adbr.getString("modifieddate"));
		ret.setModifiedTime(adbr.getString("modifiedtime"));
		ret.setUserId(adbr.getString("userid"));
		ret.setUserName(adbr.getString("username"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setUserSyskey(adbr.getLong("usersyskey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));	
		return ret;
	}

	public static DBRecord setDBRecord(UploadData data) {
		DBRecord ret = define();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("createddate", data.getCreatedDate());
		ret.setValue("createdTime", data.getCreatedTime());
		ret.setValue("modifieddate", data.getModifiedDate());
		ret.setValue("modifiedtime", data.getModifiedTime());
		ret.setValue("userid", data.getUserId());
		ret.setValue("username", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("usersyskey", data.getUserSyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t5", data.getT5());
		ret.setValue("t6", data.getT6());
		ret.setValue("t7", data.getT7());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		return ret;
	}

	public static DBRecord defineView() {
		DBRecord ret = new DBRecord();
		ret.setTableName("View_State");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		return ret;
	}

	public static UploadData getDBViewRecord(DBRecord adbr) {
		UploadData ret = new UploadData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("createddate"));
		ret.setModifiedDate(adbr.getString("modifieddate"));
		ret.setUserId(adbr.getString("userid"));
		ret.setUserName(adbr.getString("username"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setUserSyskey(adbr.getLong("usersyskey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		return ret;
	}

	public static DBRecord setDBViewRecord(UploadData data) {
		DBRecord ret = define();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("createddate", data.getCreatedDate());
		ret.setValue("modifieddate", data.getModifiedDate());
		ret.setValue("userid", data.getUserId());
		ret.setValue("username", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("usersyskey", data.getUserSyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		return ret;
	}
	/*searchVideoList*/
	public static UploadDataSet searchVideoList(String id, Connection conn) throws SQLException {
		UploadDataSet res = new UploadDataSet();
		ArrayList<UploadData> dataList = new ArrayList<UploadData>();
		ArrayList<String> strList = new ArrayList<String>();
		String whereclause = " WHERE n2 = " + id + " AND RECORDSTATUS <> 4 AND RECORDSTATUS = 1";
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereclause, "", conn);
		for (int i = 0; i < dbrs.size(); i++) {
			dataList.add(getDBRecord(dbrs.get(i)));
			strList.add(getDBRecord(dbrs.get(i)).getT2());
		}
		if (dataList.size() > 0) {
			res.setState(true);
			UploadData[] dataArray = new UploadData[dataList.size()];
			dataArray = dataList.toArray(dataArray);
			String[] strArray = new String[strList.size()];// t2
			strArray = strList.toArray(strArray);
			res.setData(dataArray);
			res.setVideoUpload(strArray);
		} else {
			res.setState(false);
		}
		return res;
	}
	
	//atnnew//
	public static Result deleteByUpdate(Long syskey, String type, Connection conn) throws SQLException {
		Result res = new Result();
		String sql = "";
		if (!type.equalsIgnoreCase("Video")) {
			//sql = "DELETE FROM FMR006 WHERE n1=?";
			sql = "UPDATE FMR006 SET recordStatus=4 WHERE recordStatus=1 AND n1=?";
		} else {
			//sql = "DELETE FROM FMR006 WHERE n2=?";
			sql = "UPDATE FMR006 SET recordStatus=4 WHERE recordStatus=1 AND n2=?";
		}
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Successfully");
		}
		return res;
	}

	public static UploadData read(long syskey, Connection conn)
			throws SQLException {
		UploadData ret = new UploadData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				"where RecordStatus<>4 AND RecordStatus = 1 AND syskey=" + syskey, "", conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));
		return ret;
	}

	public static boolean isCodeExist(UploadData obj, Connection conn)
			throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				" where RecordStatus<>4 AND RecordStatus = 1 AND syskey <> " + obj.getSyskey()
				+ " AND T1='" + obj.getT1() + "' AND T2='" + obj.getT2() + "'", "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}
	public static boolean isVDExist(UploadData obj, Connection conn)
			throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				" where RecordStatus<>4 AND RecordStatus = 1 AND syskey <> " + obj.getSyskey()
				+ " AND T2='" + obj.getT2() + "'", "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean insert(UploadData obj, Connection conn) throws SQLException {
		boolean res = false;
		String query = "INSERT INTO FMR006 (syskey, createddate, modifieddate, userid, username,"
				+ " recordStatus, syncStatus, syncBatch,usersyskey,t1, t2, t3, t4, t5, t6, n1, n2,createdtime,modifiedtime,t7) "
				+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			if (!isCodeExist(obj, conn)) {
				PreparedStatement ps = conn.prepareStatement(query);
				ps.setLong(1, obj.getSyskey());
				ps.setString(2, obj.getCreatedDate());
				ps.setString(3, obj.getModifiedDate());
				ps.setString(4, obj.getUserId());
				ps.setString(5, obj.getUserName());
				ps.setInt(6, obj.getRecordStatus());
				ps.setInt(7, obj.getSyncStatus());
				ps.setLong(8, obj.getSyncBatch());
				ps.setLong(9, obj.getUserSyskey());
				ps.setString(10, obj.getT1());
				ps.setString(11, obj.getT2());
				ps.setString(12, obj.getT3());
				ps.setString(13, obj.getT4());
				ps.setString(14, obj.getT5());
				ps.setString(15, obj.getT6());
				ps.setLong(16, obj.getN1());
				ps.setLong(17, obj.getN2());
				ps.setString(18, obj.getCreatedTime());
				ps.setString(19, obj.getModifiedTime());
				ps.setString(20, obj.getT7());
				if (ps.executeUpdate() > 0) {
					res = true;
				}
				
			}
		} catch (SQLException e) {
		}
		return res;
	}	

	//insert video
	public static boolean insertvideo(UploadData obj, Connection conn) throws SQLException {
		boolean res = false;
		String sql = DBMgr.insertString(define(), conn);
		PreparedStatement stmt = conn.prepareStatement(sql);
		DBRecord dbr = setDBRecord(obj);
		DBMgr.setValues(stmt, dbr);
		int count = stmt.executeUpdate();
		if (count > 0) {
			res = true;
		}

		return res;
	}

	public static boolean insertvideoforvideo(UploadData obj, Connection conn) throws SQLException {
		boolean res = false;
		if (!isVDExist(obj, conn)) {			
			String sql = DBMgr.insertString(define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			int count = stmt.executeUpdate();
			if (count > 0) {
				res = true;
			}
		}
		return res;
	}

	public static Resultb2b insertUpload(UploadData obj, MrBean user) throws SQLException {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		if (!isCodeExist(obj, conn)) {
			String sql = DBMgr.insertString(define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			int count = stmt.executeUpdate();
			if (count > 0) {
				res.setState(true);
				res.setMsgDesc("Saved Successfully!");
			}
		}
		return res;
	}

	public static Resultb2b update(UploadData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		if (!isCodeExist(obj, conn)) {
			String sql = DBMgr.updateString(" WHERE RecordStatus<>4 AND RECORDSTATUS = 1 AND Syskey=" + obj.getSyskey(), define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			int count = stmt.executeUpdate();
			if (count > 0) {
				res.setState(true);
				res.setMsgDesc("Updated Successfully!");
			}
		} else {
			res.setMsgDesc("No Such State to Update!");
		}
		return res;
	}

	

	public static Resultb2b deleteByUpdateforVideo(Long syskey, Connection conn)throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = "DELETE FROM FMR006 WHERE n2=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
		}
		return res;
	}
	
	//kndz
	public static ArrayList<PhotoUploadData> searchUploadList(String id, Connection conn) throws SQLException {
		UploadDataSet res = new UploadDataSet();
		ArrayList<UploadData> dataList = new ArrayList<UploadData>();
		ArrayList<PhotoUploadData> uploadlist = new ArrayList<PhotoUploadData>();
		String whereclause = " WHERE (n1 = " + id + " OR n2=" + id + ") AND RECORDSTATUS <> 4 AND RECORDSTATUS = 1 ";
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereclause, "", conn);
		PhotoUploadData uploaddata = new PhotoUploadData();
		for (int i = 0; i < dbrs.size(); i++) {
			dataList.add(getDBRecord(dbrs.get(i)));
		}	
		for(int j=0; j<dataList.size(); j++){
			uploaddata = new PhotoUploadData();
			if(!dataList.get(j).getT3().trim().equals("")){
				uploaddata.setSerial(Integer.parseInt(dataList.get(j).getT3()));
			}
			else{
			uploaddata.setSerial(j+1);}
			uploaddata.setName(dataList.get(j).getT1());
			uploaddata.setDesc(dataList.get(j).getT4());
			uploaddata.setOrder(dataList.get(j).getT5());
			uploaddata.setUrl(dataList.get(j).getT7());
			uploadlist.add(uploaddata);
		}

		if(uploadlist.size()>0){
			res.setState(true);
		}
		else{
			res.setState(false);
		}
		return uploadlist;
	}

	
	public static UploadDataSet searchImg(String id, Connection conn) throws SQLException {
		UploadDataSet res = new UploadDataSet();
		ArrayList<UploadData> dataList = new ArrayList<UploadData>();
		ArrayList<String> strList = new ArrayList<String>();
		String whereclause = " WHERE n2 = "+id+" AND RECORDSTATUS <> 4 AND RECORDSTATUS = 1";
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereclause, "", conn);

		for (int i = 0; i < dbrs.size(); i++) {
			dataList.add(getDBRecord(dbrs.get(i)));
			strList.add(getDBRecord(dbrs.get(i)).getT1());
		}	
		if(dataList.size()>0){
			res.setState(true);
			UploadData[] dataArray = new UploadData[dataList.size()];
			dataArray = dataList.toArray(dataArray);
			String[] strArray = new String[strList.size()];
			strArray = strList.toArray(strArray);
			res.setData(dataArray);	
			res.setUploads(strArray);
		}
		else{
			res.setState(false);
		}
		return res;
	}

	//TDA
	public static UploadDataSet searchAdminVideo(String id, Connection conn) throws SQLException {
		UploadDataSet res = new UploadDataSet();
		ArrayList<UploadData> dataList = new ArrayList<UploadData>();
		ArrayList<String> strList = new ArrayList<String>();
		String whereclause = " WHERE n2 = "+id+" AND RECORDSTATUS <> 4 AND RECORDSTATUS = 1";
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereclause, "", conn);
		for (int i = 0; i < dbrs.size(); i++) {
			dataList.add(getDBRecord(dbrs.get(i)));
			strList.add(getDBRecord(dbrs.get(i)).getT2());
		}		
		if(dataList.size()>0){
			res.setState(true);
			UploadData[] dataArray = new UploadData[dataList.size()];
			dataArray = dataList.toArray(dataArray);
			String[] strArray = new String[strList.size()];//t2
			strArray = strList.toArray(strArray);
			res.setData(dataArray);	
			res.setVideoUpload(strArray);
			for(int k=0;k<strArray.length;k++){
			}
		}
		else{
			res.setState(false);
		}
		return res;
	}

	public static ArrayList<UploadData> readByN1(long n1, Connection conn) throws SQLException {
		ArrayList<UploadData> ret = new ArrayList<UploadData>();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define()," WHERE RecordStatus<>4 AND RecordStatus = 1 AND n1 = '" + n1 +"' ", "", conn);
		for(int i=0; i<dbrs.size(); i++){
			ret.add(getDBRecord(dbrs.get(i)));
		}
		return ret;
	}
	//atnnew//
	public static UploadDataSet search(String id, String type,Connection conn) throws SQLException {
		UploadDataSet res = new UploadDataSet();
		ArrayList<UploadData> dataList = new ArrayList<UploadData>();
		ArrayList<String> strList = new ArrayList<String>();
		String whereclause ="";
		if(type.equalsIgnoreCase("video")){
			 whereclause = " WHERE n2 = "+id+" AND RECORDSTATUS <> 4 AND RECORDSTATUS = 1 order by t3 asc";
		}
		else{
			 whereclause = " WHERE n1 = "+id+" AND RECORDSTATUS <> 4 AND RECORDSTATUS = 1 ";
		}
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereclause, "", conn);
		for (int i = 0; i < dbrs.size(); i++) {
			dataList.add(getDBRecord(dbrs.get(i)));
			strList.add(getDBRecord(dbrs.get(i)).getT7());
			//strList.add(getDBRecord(dbrs.get(i)).getT1());
		}	
		if(dataList.size()>0){
			res.setState(true);
			UploadData[] dataArray = new UploadData[dataList.size()];
			dataArray = dataList.toArray(dataArray);
			String[] strArray = new String[strList.size()];
			strArray = strList.toArray(strArray);
			res.setData(dataArray);	
			res.setUploads(strArray);
		}
		else{
			res.setState(false);
		}
		return res;
	}
	  public static String deleteCorrect(String filename, Connection conn) {
	        String result = "";
	        try {
	            String sql = "DELETE FROM FMR006 WHERE t1=?";
	            PreparedStatement stmt = conn.prepareStatement(sql);
	            stmt.setString(1, filename);
	            int rs = stmt.executeUpdate();
	            if (rs > 0) {
	                result = "SUCCESS";
	                
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	        return result;
	    }
	


}
