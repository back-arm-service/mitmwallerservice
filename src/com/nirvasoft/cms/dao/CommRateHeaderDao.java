package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.nirvasoft.rp.data.CommRateHeaderData;

public class CommRateHeaderDao {

	private final String tblname = "CommRateHeader";

	public CommRateHeaderData getComRatedataByComRefNo(String pCommRef, Connection pConn) throws SQLException {

		CommRateHeaderData data = new CommRateHeaderData();
		String sql = "select syskey,commRef,Description, maintype from " + tblname + " where commRef = ?";

		PreparedStatement ps = pConn.prepareStatement(sql);
		ps.setString(1, pCommRef);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			data.setSyskey(rs.getLong("syskey"));
			data.setCommRef(rs.getString("CommRef"));
			data.setDescription(rs.getString("Description"));
			data.setMainType(rs.getInt("MainType"));

		}

		return data;

	}
}
