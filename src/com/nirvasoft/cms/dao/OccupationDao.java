package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.AdvancedSearchData;
import com.nirvasoft.cms.shared.OccupationData;
import com.nirvasoft.cms.shared.OccupationDataSet;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;

public class OccupationDao {
	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("OCCUPATION");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		return ret;
	}

	public static DBRecord define(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		// ret.getFields().add(new DBField("createdtime", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		// ret.getFields().add(new DBField("modifiedtime", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		return ret;
	}

	public static Resultb2b delete(Long syskey, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = "UPDATE OCCUPATION SET RecordStatus=4 WHERE syskey=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Successfully");
		} else {
			res.setMsgDesc("Deleting Unsuccessful");
		}
		return res;
	}

	public static OccupationData getDBRecord(DBRecord adbr) {
		OccupationData ret = new OccupationData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreateddate(adbr.getString("createddate"));
		// ret.setCreatedTime(adbr.getString("createdtime"));
		ret.setModifieddate(adbr.getString("modifieddate"));
		// ret.setModifiedTime(adbr.getString("modifiedtime"));
		ret.setUserid(adbr.getString("userid"));
		ret.setUsername(adbr.getString("username"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setUsersyskey(adbr.getLong("usersyskey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		ret.setT8(adbr.getString("t8"));
		ret.setT9(adbr.getString("t9"));
		ret.setT10(adbr.getString("t10"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getLong("n5"));
		ret.setN6(adbr.getLong("n6"));
		ret.setN7(adbr.getLong("n7"));
		ret.setN8(adbr.getLong("n8"));
		ret.setN9(adbr.getLong("n9"));
		ret.setN10(adbr.getLong("n10"));
		return ret;
	}

	public static String getSyskey(String tablename, Connection aConn) throws SQLException {
		String l_Key = "";
		String l_Query = " SELECT MAX(syskey) as syskey  from " + tablename;
		PreparedStatement pstmt = aConn.prepareStatement(l_Query);
		ResultSet rs = pstmt.executeQuery();
		int plus = 0;
		while (rs.next()) {
			l_Key = rs.getString("syskey");
			// l_Key = String.valueOf(rs.getInt("batch")); need to change string
			// to int
		}
		if (l_Key == null || l_Key == "") {
			l_Key = "11111";
		} else {
			plus = Integer.parseInt(l_Key) + 1;
			l_Key = String.valueOf(plus);
		}
		return l_Key;
	}

	public static Resultb2b insert(OccupationData aObj, Connection aConnection) throws SQLException {
		Resultb2b res = new Resultb2b();
		String query = "INSERT INTO OCCUPATION (syskey, createddate, modifieddate, userid, username,"
				+ " recordStatus, syncStatus, syncBatch,usersyskey,t1, t2, t3, t4, t5, t6, t7, t8, t9, t10,"
				+ " n1, n2, n3, n4, n5, n6, n7, n8, n9, n10)" + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
				+ "?,?,?,?,?,?,?,?,?)";
		try {
			if (!isIDexist(aObj, aConnection)) {
				PreparedStatement ps = aConnection.prepareStatement(query);
				ps.setLong(1, aObj.getSyskey());
				ps.setString(2, aObj.getCreateddate());
				ps.setString(3, aObj.getModifieddate());
				ps.setString(4, aObj.getUserid());
				ps.setString(5, aObj.getUsername());
				ps.setInt(6, aObj.getRecordStatus());
				ps.setInt(7, aObj.getSyncStatus());
				ps.setLong(8, aObj.getSyncBatch());
				ps.setLong(9, aObj.getUsersyskey());
				ps.setString(10, aObj.getT1());
				ps.setString(11, aObj.getT2());
				ps.setString(12, aObj.getT3());
				ps.setString(13, aObj.getT4());
				ps.setString(14, aObj.getT5());
				ps.setString(15, aObj.getT6());
				ps.setString(16, aObj.getT7());
				ps.setString(17, aObj.getT8());
				ps.setString(18, aObj.getT9());
				ps.setString(19, aObj.getT10());
				ps.setLong(20, aObj.getN1());
				ps.setLong(21, aObj.getN2());
				ps.setLong(22, aObj.getN3());
				ps.setLong(23, aObj.getN4());
				ps.setLong(24, aObj.getN5());
				ps.setLong(25, aObj.getN6());
				ps.setLong(26, aObj.getN7());
				ps.setLong(27, aObj.getN8());
				ps.setLong(28, aObj.getN9());
				ps.setLong(29, aObj.getN10());

				if (ps.executeUpdate() > 0) {
					res.setMsgDesc("Saved Successfully!");
					res.setState(true);
				}
			} else {
				res.setMsgDesc("Code already exist!");
				res.setState(false);
			}

		} catch (SQLException e) {

		}

		return res;

	}

	public static boolean isCodeExist(OccupationData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				" where RecordStatus<>4 AND syskey <> " + obj.getSyskey() + " AND T1='" + obj.getT1() + "'", "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isIDexist(OccupationData aObj, Connection conn) {
		boolean b = false;
		String sqlString = "";
		sqlString = "select * from OCCUPATION where syskey='" + aObj.getSyskey() + "'";
		try {
			Statement stmts = conn.createStatement();
			ResultSet rs = stmts.executeQuery(sqlString);
			if (rs.next()) {
				b = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return b;
	}

	public static OccupationData read(long syskey, Connection conn) throws SQLException {
		OccupationData ret = new OccupationData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "where RecordStatus<>4 AND syskey=" + syskey, "", conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));
		return ret;
	}

	public static OccupationDataSet search(AdvancedSearchData asdata, String searchVal, String sort, String type,
			Connection conn) throws SQLException {
		OccupationDataSet res = new OccupationDataSet();
		ArrayList<OccupationData> datalist = new ArrayList<OccupationData>();
		String whereclause = " WHERE RecordStatus<>4 ";
		String orderclause = "";

		if (type.equals("1")) {
			orderclause += " ORDER BY t1 ";
		} else if (type.equals("2")) {
			orderclause += " ORDER BY t2 ";
		}
		if (!orderclause.isEmpty()) {
			if (sort.equals("asc")) {
				orderclause += " asc ";
			} else {
				orderclause += " desc ";
			}
		}
		PagerData pgdata = asdata.getPager();
		if (!searchVal.equals("")) {
			whereclause += "AND ( t1 LIKE '%" + searchVal + "%' OR t2 LIKE '%" + searchVal + "%')";
		}
		int start = pgdata.getStart() - 1;
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecordSandE(define(), whereclause, orderclause, start, pgdata.getEnd(), 0,
				conn);

		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getDBRecord(dbrs.get(i)));
		}

		res.setPageSize(pgdata.getSize());
		res.setCurrentPage(pgdata.getCurrent());
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}

		PreparedStatement stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM OCCUPATION " + whereclause);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));

		OccupationData[] dataarray = new OccupationData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setArr(dataarray);

		return res;
	}

	public static DBRecord setDBRecord(OccupationData data) {
		DBRecord ret = define();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("createddate", data.getCreateddate());
		// ret.setValue("createdtime", data.getCreatedTime());
		ret.setValue("modifieddate", data.getModifieddate());
		// ret.setValue("modifiedtime", data.getModifiedTime());
		ret.setValue("userid", data.getUserid());
		ret.setValue("username", data.getUsername());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("usersyskey", data.getUsersyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t5", data.getT5());
		ret.setValue("t6", data.getT6());
		ret.setValue("t7", data.getT7());
		ret.setValue("t8", data.getT8());
		ret.setValue("t9", data.getT9());
		ret.setValue("t10", data.getT10());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		ret.setValue("n6", data.getN6());
		ret.setValue("n7", data.getN7());
		ret.setValue("n8", data.getN8());
		ret.setValue("n9", data.getN9());
		ret.setValue("n10", data.getN10());
		return ret;
	}

	public static Resultb2b update(OccupationData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String query = "UPDATE OCCUPATION SET [syskey]=?, [createddate]=?, [modifieddate]=?, [userid]=?, [username]=?, "
				+ " [recordstatus]=?, [syncstatus]=?, [syncbatch]=?,[usersyskey]=?, [t1]=?, [t2]=?, [t3]=?,[t4]=?, [t5]=?,"
				+ " [t6]=?, [t7]=?,[t8]=?, [t9]=?,[t10]=?,[n1]=?, [n2]=?, [n3]=?,"
				+ " [n4]=?, [n5]=?,[n6]=?, [n7]=?,[n8]=?, [n9]=?,[n10]=?"
				+ "WHERE RecordStatus<>4 AND RecordStatus = 1 AND Syskey=" + obj.getSyskey() + "";
		try {
			if (!isCodeExist(obj, conn)) {
				PreparedStatement pstmt = conn.prepareStatement(query);
				pstmt.setLong(1, obj.getSyskey());
				pstmt.setString(2, obj.getCreateddate());
				pstmt.setString(3, obj.getModifieddate());
				pstmt.setString(4, obj.getUserid());
				pstmt.setString(5, obj.getUsername());
				pstmt.setInt(6, obj.getRecordStatus());
				pstmt.setInt(7, obj.getSyncStatus());
				pstmt.setLong(8, obj.getSyncBatch());
				pstmt.setLong(9, obj.getUsersyskey());
				pstmt.setString(10, obj.getT1());
				pstmt.setString(11, obj.getT2());
				pstmt.setString(12, obj.getT3());
				pstmt.setString(13, obj.getT4());
				pstmt.setString(14, obj.getT5());
				pstmt.setString(15, obj.getT6());
				pstmt.setString(16, obj.getT7());
				pstmt.setString(17, obj.getT8());
				pstmt.setString(18, obj.getT9());
				pstmt.setString(19, obj.getT10());
				pstmt.setLong(20, obj.getN1());
				pstmt.setLong(21, obj.getN2());
				pstmt.setLong(22, obj.getN3());
				pstmt.setLong(23, obj.getN4());
				pstmt.setLong(24, obj.getN5());
				pstmt.setLong(25, obj.getN6());
				pstmt.setLong(26, obj.getN7());
				pstmt.setLong(27, obj.getN8());
				pstmt.setLong(28, obj.getN9());
				pstmt.setLong(29, obj.getN10());

				if (pstmt.executeUpdate() > 0) {
					res.setState(true);
					res.setMsgDesc("Updated Successfully!");
				}
			} else {
				res.setMsgDesc("No Such Article to Update!");
			}
		} catch (SQLException e) {

		}

		return res;
	}

}
