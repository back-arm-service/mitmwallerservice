package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.cms.shared.AddressRefData;
import com.nirvasoft.cms.shared.AddressRefDataSet;
import com.nirvasoft.cms.shared.BrandComboData;
import com.nirvasoft.cms.shared.BrandComboDataSet;
import com.nirvasoft.cms.shared.MessageData;
import com.nirvasoft.cms.shared.MessageTypeData;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;

public class MessageDao {

	public static DBRecord define() { // Define Database fields
		DBRecord ret = new DBRecord();
		ret.setTableName("FMR007"); // Table Name
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("createdtime", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("modifiedtime", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("T1", (byte) 5));
		ret.getFields().add(new DBField("T2", (byte) 5));
		ret.getFields().add(new DBField("T3", (byte) 5));
		ret.getFields().add(new DBField("T4", (byte) 5));
		ret.getFields().add(new DBField("T5", (byte) 5));
		ret.getFields().add(new DBField("T6", (byte) 5));
		ret.getFields().add(new DBField("T7", (byte) 5));
		ret.getFields().add(new DBField("T8", (byte) 5));
		ret.getFields().add(new DBField("T9", (byte) 5));
		ret.getFields().add(new DBField("T10", (byte) 5));
		ret.getFields().add(new DBField("T11", (byte) 5));
		ret.getFields().add(new DBField("T12", (byte) 5));
		ret.getFields().add(new DBField("T13", (byte) 5));
		ret.getFields().add(new DBField("T14", (byte) 5));
		ret.getFields().add(new DBField("T15", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		ret.getFields().add(new DBField("n11", (byte) 2));
		ret.getFields().add(new DBField("n12", (byte) 2));
		ret.getFields().add(new DBField("n13", (byte) 2));
		ret.getFields().add(new DBField("n14", (byte) 2));
		ret.getFields().add(new DBField("n15", (byte) 2));
		return ret;
	}

	public static DBRecord define(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("createdtime", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("modifiedtime", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("T1", (byte) 5));
		ret.getFields().add(new DBField("T2", (byte) 5));
		ret.getFields().add(new DBField("T3", (byte) 5));
		ret.getFields().add(new DBField("T4", (byte) 5));
		ret.getFields().add(new DBField("T5", (byte) 5));
		ret.getFields().add(new DBField("T6", (byte) 5));
		ret.getFields().add(new DBField("T7", (byte) 5));
		ret.getFields().add(new DBField("T8", (byte) 5));
		ret.getFields().add(new DBField("T9", (byte) 5));
		ret.getFields().add(new DBField("T10", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		return ret;
	}

	public static MessageData getDBRecord(DBRecord adbr) {
		MessageData ret = new MessageData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreateddate(adbr.getString("createddate"));
		ret.setCreatedtime(adbr.getString("createdtime"));
		ret.setModifieddate(adbr.getString("modifieddate"));
		ret.setModifiedtime(adbr.getString("modifiedtime"));
		ret.setUserid(adbr.getString("userid"));
		ret.setUsername(adbr.getString("username"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setUsersyskey(adbr.getLong("usersyskey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		ret.setT8(adbr.getString("t8"));
		ret.setT9(adbr.getString("t9"));
		ret.setT10(adbr.getString("t10"));
		ret.setT11(adbr.getString("t11"));
		ret.setT12(adbr.getString("t12"));
		ret.setT13(adbr.getString("t13"));
		ret.setT14(adbr.getString("t14"));
		ret.setT15(adbr.getString("t15"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getLong("n5"));
		ret.setN6(adbr.getLong("n6"));
		ret.setN7(adbr.getLong("n7"));
		ret.setN8(adbr.getLong("n8"));
		ret.setN9(adbr.getLong("n9"));
		ret.setN10(adbr.getLong("n10"));
		ret.setN11(adbr.getLong("n11"));
		ret.setN12(adbr.getLong("n12"));
		ret.setN13(adbr.getLong("n13"));
		ret.setN14(adbr.getLong("n14"));
		ret.setN15(adbr.getLong("n15"));
		return ret;
	}

	public static MessageTypeData getDBRecords(DBRecord adbr) {
		MessageTypeData ret = new MessageTypeData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreateddate(adbr.getString("createddate"));
		ret.setCreatedtime(adbr.getString("createdtime"));
		ret.setModifieddate(adbr.getString("modifieddate"));
		ret.setModifiedtime(adbr.getString("modifiedtime"));
		ret.setUserid(adbr.getString("userid"));
		ret.setUsername(adbr.getString("username"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setUsersyskey(adbr.getLong("usersyskey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		ret.setT8(adbr.getString("t8"));
		ret.setT9(adbr.getString("t9"));
		ret.setT10(adbr.getString("t10"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getLong("n5"));
		ret.setN6(adbr.getLong("n6"));
		ret.setN7(adbr.getLong("n7"));
		ret.setN8(adbr.getLong("n8"));
		ret.setN9(adbr.getLong("n9"));
		ret.setN10(adbr.getLong("n10"));
		return ret;
	}

	public static BrandComboDataSet getDistrictList(String divcode, Connection l_Conn) throws SQLException {
		BrandComboDataSet ret = new BrandComboDataSet();
		ArrayList<BrandComboData> divisionData = new ArrayList<BrandComboData>();
		String Query = "";
		String div = "";
		if (divcode.equals(""))
			Query = " SELECT code,DespEng,DespMyan FROM AddressRef WHERE (code LIKE '%000' and code NOT LIKE '%000000' AND CODE <> '00000000' and DespEng <> 'All') ORDER BY Code ";
		else {
			Query = " SELECT code,DespEng,DespMyan FROM AddressRef WHERE (code LIKE '%000' and code NOT LIKE '%000000' AND CODE <> '00000000') AND CODE like '"
					+ divcode + "%'  ORDER BY Code ";
			String sql = " Select DespEng,DespMyan From AddressRef where code =" + divcode + "000000";
			PreparedStatement st = l_Conn.prepareStatement(sql);
			ResultSet rs = st.executeQuery();
			if (rs.next()) {
				div = rs.getString("DespEng");// division name
			}
		}
		PreparedStatement pstmt = l_Conn.prepareStatement(Query);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			BrandComboData data = new BrandComboData();
			data.setCode(rs.getString("code"));
			if (div.equals("")) {
				data.setEngCaption(rs.getString("DespEng"));
				data.setMyanCaption(rs.getString("DespMyan"));
			} else {
				data.setEngCaption(rs.getString("DespEng"));
				data.setMyanCaption(rs.getString("DespMyan"));
			}
			divisionData.add(data);

		}
		BrandComboData[] dataarry = new BrandComboData[divisionData.size()];
		for (int i = 0; i < divisionData.size(); i++) {
			dataarry[i] = divisionData.get(i);
		}

		ret.setData(dataarry);

		return ret;
	}

	public static MessageData getLatestMessage(String state, String lg, Connection conn) throws SQLException {
		MessageData ret = new MessageData();
		String wherecls = "where syskey = (select max(f7.SysKey) from fmr007 as f7 join jun003 as "
				+ "j3 on f7.syskey=j3.n1 and f7.t6=1 and j3.n2='" + state + "' and  f7.t8='" + lg
				+ "' and f7.recordstatus =1 and j3.recordstatus=1 )";
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), wherecls, "", conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));
		return ret;
	}

	/*
	 * public static MessageDataSet getLatestNoti(PagerData pgdata, long userSK,
	 * long state, String lg, String date, String noti, Connection conn) throws
	 * SQLException { MessageDataSet res = new MessageDataSet();
	 * ArrayList<MessageData> datalist = new ArrayList<MessageData>();
	 * MessageData message; String whereclause =
	 * "select * from FMR007 where RecordStatus = 1 and syskey in  (select n1 From  JUN003  where n5 = "
	 * + userSK + " or n2= " + state + ") and t9 = '" + noti + "' and  (t3 <= '"
	 * + date + "' and t4 >= '" + date + "') "; PreparedStatement stmt =
	 * conn.prepareStatement(whereclause); ResultSet rs = stmt.executeQuery();
	 * while (rs.next()) { message = new MessageData();
	 * message.setSyskey(rs.getLong("syskey"));
	 * message.setT1(rs.getString("t1")); message.setT2(rs.getString("t2"));
	 * message.setT5(rs.getString("t5")); message.setT6(rs.getString("t6"));
	 * message.setT8(rs.getString("t8"));// language
	 * message.setT9(rs.getString("t9")); datalist.add(message); }
	 * res.setPageSize(pgdata.getSize());
	 * res.setCurrentPage(pgdata.getCurrent()); if (datalist.size() > 0) {
	 * res.setState(true); } else { res.setState(false); }
	 * res.setPageSize(pgdata.getSize());
	 * res.setCurrentPage(pgdata.getCurrent()); if (datalist.size() > 0) {
	 * res.setState(true); } else { res.setState(false); } PreparedStatement
	 * stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM FMR007 " +
	 * whereclause); ResultSet result = stat.executeQuery(); result.next();
	 * res.setTotalCount(result.getInt("recCount")); MessageData[] dataarray =
	 * new MessageData[datalist.size()]; dataarray =
	 * datalist.toArray(dataarray); res.setData(dataarray); return res; }
	 */

	public static BrandComboDataSet getStateGPSList(String gps, Connection conn) throws SQLException {
		String[] lat = gps.split(",");
		BrandComboDataSet retcom = new BrandComboDataSet();
		ArrayList<BrandComboData> divisionData = new ArrayList<BrandComboData>();
		BrandComboData combo1 = new BrandComboData();
		ArrayList<AddressRefData> datalist = new ArrayList<AddressRefData>();
		String sql = "select * from addressref where code like '%000000'  AND CODE <> '00000000' and DespEng <> 'All' ORDER BY Code";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			AddressRefData combo = new AddressRefData();
			combo.setCode(res.getString("code"));
			combo.setDespMyan(res.getString("DespMyan"));
			combo.setDespEng(res.getString("DespEng"));
			combo.setIsLocal(res.getLong("IsLocal"));
			combo.setMinLat(res.getString("MinLat"));
			combo.setMinLon(res.getString("MinLon"));
			combo.setMaxLat(res.getString("MaxLat"));
			combo.setMaxLon(res.getString("MaxLon"));
			datalist.add(combo);
		}
		BrandComboDataSet ret = new BrandComboDataSet();
		String districtCode = "";
		BrandComboData data = new BrandComboData();
		for (int i = 0; i < datalist.size(); i++) {
			if (datalist.get(i).getDespEng().equals("YANGON")) {

				if (!datalist.get(i).getMinLat().equals("") && !datalist.get(i).getMinLon().equals("")
						&& !datalist.get(i).getMaxLat().equals("") && !datalist.get(i).getMaxLon().equals("")) {
					if (Float.valueOf(lat[0]) >= Float.valueOf(datalist.get(i).getMinLat())
							&& Float.valueOf(lat[0]) <= Float.valueOf(datalist.get(i).getMaxLat())) {
						if ((Float.valueOf(lat[1]) >= Float.valueOf(datalist.get(i).getMinLon()))
								&& (Float.valueOf(lat[1]) <= Float.valueOf(datalist.get(i).getMaxLon()))) {
							String divcode = datalist.get(i).getCode().substring(0, 2);
							ret = getDistrictList(divcode, conn);// get District
							for (int j = 0; j < ret.getData().length; j++) {
								districtCode += ret.getData()[j].getCode().substring(0, 5) + ",";

							}
							combo1 = getTownshipGps(gps, districtCode, conn); // get
							if (!combo1.getCode().equals("")) {
								data.setEngCaption(datalist.get(i).getDespEng());
								data.setMyanCaption(datalist.get(i).getDespMyan());
								data.setCode(datalist.get(i).getCode());
								data.setFlag(true);
								divisionData.add(0, data);
								divisionData.add(1, combo1);
							}
						} else {
							data.setCode("");
							data.setEngCaption("");
							data.setMyanCaption("");
							data.setFlag(false);
							divisionData.add(data);
						}
					} else {
						data.setCode("");
						data.setEngCaption("");
						data.setMyanCaption("");
						data.setFlag(false);
						divisionData.add(data);

					}
				}
			}
		}

		BrandComboData[] dataarry = new BrandComboData[divisionData.size()];
		for (int i = 0; i < divisionData.size(); i++) {
			dataarry[i] = divisionData.get(i);
		}
		retcom.setData(dataarry);
		return retcom;
	}

	/*
	 * public static StateDataSet getStatelist(String language, Connection conn)
	 * throws SQLException { StateDataSet dataset = new StateDataSet();
	 * ArrayList<StateData> datalist = new ArrayList<StateData>(); String sql =
	 * "select * from addressref where code like '%000000'  AND CODE <> '00000000' and DespEng <> 'All' ORDER BY Code"
	 * ; PreparedStatement stmt = conn.prepareStatement(sql); ResultSet res =
	 * stmt.executeQuery(); while (res.next()) { StateData combo = new
	 * StateData(); combo.setT1(res.getString("code"));
	 * combo.setT2(res.getString("DespMyan"));
	 * combo.setT3(res.getString("DespEng"));
	 * combo.setN1(res.getLong("IsLocal"));
	 * combo.setMdata(getLatestMessage(combo.getT1(), language, conn));
	 * datalist.add(combo); } StateData[] dataarray = new
	 * StateData[datalist.size()]; dataarray = datalist.toArray(dataarray);
	 * dataset.setData(dataarray); return dataset; }
	 */
	public static BrandComboDataSet getStateTypeCombolist(Connection conn) throws SQLException {
		BrandComboDataSet dataset = new BrandComboDataSet();
		ArrayList<BrandComboData> datalist = new ArrayList<BrandComboData>();
		String sql = "select code,DespEng,DespMyan from addressref where code like '%000000'  AND CODE <> '00000000' and DespEng <> 'All' ORDER BY Code";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			BrandComboData combo = new BrandComboData();
			combo.setEngCaption(res.getString("DespEng"));
			combo.setValue(res.getLong("code"));
			combo.setMyanCaption(res.getString("DespMyan"));
			combo.setCode(res.getString("code"));
			combo.setFlag(false);
			datalist.add(combo);
		}
		BrandComboData[] dataarray = new BrandComboData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		dataset.setData(dataarray);
		return dataset;
	}

	public static BrandComboData getTownshipGps(String gps, String dist_code, Connection l_Conn) throws SQLException {
		String town = "";
		String[] lat = gps.split(",");
		BrandComboDataSet towcombo = new BrandComboDataSet();
		ArrayList<BrandComboData> townData = new ArrayList<BrandComboData>();
		BrandComboData data1 = new BrandComboData();
		AddressRefDataSet dataset = new AddressRefDataSet();
		ArrayList<AddressRefData> datalist = new ArrayList<AddressRefData>();
		BrandComboDataSet ret = new BrandComboDataSet();
		ArrayList<BrandComboData> divisionData = new ArrayList<BrandComboData>();
		String div = "";
		String district = "";
		String Query = "";
		String sqlDis = "";
		String sqlDiv = "";
		if (dist_code != "") {
			if (dist_code.contains(",")) {
				String splitdistcode[] = dist_code.split(",");
				for (int i = 0; i < splitdistcode.length; i++) {
					Query = " SELECT * FROM AddressRef WHERE code not LIKE '%000' "
							+ " and code not LIKE '%000000' AND CODE <> '00000000' and DespEng <> 'All' and "
							+ " code like '" + splitdistcode[i] + "%' order by code ";
					sqlDis = " Select DespEng,DespMyan From AddressRef where code ='" + splitdistcode[i]
							+ "000' order by code ";
					sqlDiv = " Select DespEng,DespMyan From AddressRef where code ='" + splitdistcode[i].substring(0, 2)
							+ "000000' order by code ";
					PreparedStatement psSQLDiv = l_Conn.prepareStatement(sqlDiv);
					ResultSet rsSQLDiv = psSQLDiv.executeQuery();
					if (rsSQLDiv.next())
						div = rsSQLDiv.getString("DespEng");
					PreparedStatement psSQL = l_Conn.prepareStatement(sqlDis);
					ResultSet rsSQL = psSQL.executeQuery();
					if (rsSQL.next())
						district = rsSQL.getString("DespEng");
					PreparedStatement psQuery = l_Conn.prepareStatement(Query);
					ResultSet res = psQuery.executeQuery();
					while (res.next()) {
						AddressRefData combo = new AddressRefData();
						combo.setCode(res.getString("code"));
						combo.setDespMyan(res.getString("DespMyan"));
						combo.setDespEng(res.getString("DespEng"));
						combo.setIsLocal(res.getLong("IsLocal"));
						combo.setMinLat(res.getString("MinLat"));
						combo.setMinLon(res.getString("MinLon"));
						combo.setMaxLat(res.getString("MaxLat"));
						combo.setMaxLon(res.getString("MaxLon"));
						datalist.add(combo);
					}
					for (int j = 0; j < datalist.size(); j++) {
						if (datalist.get(j).getDespEng().equalsIgnoreCase("Insein")) {
							if (!datalist.get(j).getMinLat().equals("") && !datalist.get(j).getMinLon().equals("")
									&& !datalist.get(j).getMaxLat().equals("")
									&& !datalist.get(j).getMaxLon().equals("")) {
								if (Float.valueOf(lat[0]) >= Float.valueOf(datalist.get(j).getMinLat())
										&& Float.valueOf(lat[0]) <= Float.valueOf(datalist.get(j).getMaxLat())) {
									if ((Float.valueOf(lat[1]) >= Float.valueOf(datalist.get(j).getMinLon()))
											&& (Float.valueOf(lat[1]) <= Float.valueOf(datalist.get(j).getMaxLon()))) {
										town = datalist.get(j).getDespEng();
										data1.setCode(datalist.get(j).getCode());
										data1.setEngCaption(datalist.get(j).getDespEng());
										data1.setMyanCaption(datalist.get(j).getDespMyan());
										data1.setFlag(true);

									} else {
									}
								} else {
								}
							}
						}
					}

				}
			} else {
				Query = "SELECT code,DespEng,DespMyan FROM AddressRef WHERE (code LIKE '" + dist_code.substring(0, 5)
						+ "%' and code NOT LIKE '" + dist_code.substring(0, 5)
						+ "000' and code NOT LIKE '%000000' AND CODE <> '00000000' and DespEng <> 'All') ORDER BY Code ";
				sqlDis = " Select DespEng,DespMyan From AddressRef where code ='" + dist_code.substring(0, 5)
						+ "000' order by code ";
				sqlDiv = " Select DespEng,DespMyan From AddressRef where code ='" + dist_code.substring(0, 2)
						+ "000000' order by code ";
				PreparedStatement psSQLDiv = l_Conn.prepareStatement(sqlDiv);
				ResultSet rsSQLDiv = psSQLDiv.executeQuery();
				if (rsSQLDiv.next())
					div = rsSQLDiv.getString("DespEng");
				PreparedStatement psSQL = l_Conn.prepareStatement(sqlDis);
				ResultSet rsSQL = psSQL.executeQuery();
				if (rsSQL.next())
					district = rsSQL.getString("DespEng");

				PreparedStatement psQuery = l_Conn.prepareStatement(Query);
				ResultSet rsQuery = psQuery.executeQuery();
				while (rsQuery.next()) {
					BrandComboData data = new BrandComboData();
					data.setCode(rsQuery.getString("code"));
					data.setMyanCaption(rsQuery.getString("DespMyan"));
					data.setEngCaption(rsQuery.getString("DespEng"));
					data.setFlag(false);
					divisionData.add(data);
				}
			}
		} else {
			Query = " SELECT code,DespEng,DespMyan FROM AddressRef WHERE code not LIKE '%000' and code not LIKE '%000000' AND CODE <> '00000000' and DespEng <> 'All' order by code ";
			PreparedStatement pstmt = l_Conn.prepareStatement(Query);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				BrandComboData data = new BrandComboData();
				data.setCode(rs.getString("code"));
				data.setEngCaption(rs.getString("DespEng"));
				data.setMyanCaption(rs.getString("DespMyan"));
				data.setFlag(false);
				divisionData.add(data);
			}
		}
		return data1;
	}

	public static BrandComboDataSet getTownshipList(String dist_code, Connection l_Conn) throws SQLException {
		BrandComboDataSet ret = new BrandComboDataSet();
		ArrayList<BrandComboData> divisionData = new ArrayList<BrandComboData>();
		String div = "";
		String district = "";
		String Query = "";
		String sqlDis = "";
		String sqlDiv = "";

		if (dist_code != "") {
			if (dist_code.contains(",")) {
				String splitdistcode[] = dist_code.split(",");
				for (int i = 0; i < splitdistcode.length; i++) {
					Query = " SELECT code,DespEng,DespMyan FROM AddressRef WHERE code not LIKE '%000' "
							+ " and code not LIKE '%000000' AND CODE <> '00000000' and DespEng <> 'All' and "
							+ " code like '" + splitdistcode[i] + "%' order by code ";
					sqlDis = " Select DespEng,DespMyan From AddressRef where code ='" + splitdistcode[i]
							+ "000' order by code ";
					sqlDiv = " Select DespEng,DespMyan From AddressRef where code ='" + splitdistcode[i].substring(0, 2)
							+ "000000' order by code ";
					PreparedStatement psSQLDiv = l_Conn.prepareStatement(sqlDiv);
					ResultSet rsSQLDiv = psSQLDiv.executeQuery();
					if (rsSQLDiv.next())
						div = rsSQLDiv.getString("DespEng");

					PreparedStatement psSQL = l_Conn.prepareStatement(sqlDis);
					ResultSet rsSQL = psSQL.executeQuery();
					if (rsSQL.next())
						district = rsSQL.getString("DespEng");
					PreparedStatement psQuery = l_Conn.prepareStatement(Query);
					ResultSet rsQuery = psQuery.executeQuery();
					while (rsQuery.next()) {
						BrandComboData data = new BrandComboData();
						data.setValue(Long.parseLong(rsQuery.getString("code")));
						data.setMyanCaption(rsQuery.getString("DespMyan"));
						data.setEngCaption(rsQuery.getString("DespEng"));
						data.setFlag(false);
						divisionData.add(data);
					}

				}
			} else {
				Query = "SELECT code,DespEng,DespMyan FROM AddressRef WHERE (code LIKE '" + dist_code.substring(0, 5)
						+ "%' and code NOT LIKE '" + dist_code.substring(0, 5)
						+ "000' and code NOT LIKE '%000000' AND CODE <> '00000000' and DespEng <> 'All') ORDER BY Code ";
				sqlDis = " Select DespEng,DespMyan From AddressRef where code ='" + dist_code.substring(0, 5)
						+ "000' order by code ";
				sqlDiv = " Select DespEng,DespMyan From AddressRef where code ='" + dist_code.substring(0, 2)
						+ "000000' order by code ";
				PreparedStatement psSQLDiv = l_Conn.prepareStatement(sqlDiv);
				ResultSet rsSQLDiv = psSQLDiv.executeQuery();
				if (rsSQLDiv.next())
					div = rsSQLDiv.getString("DespEng");

				PreparedStatement psSQL = l_Conn.prepareStatement(sqlDis);
				ResultSet rsSQL = psSQL.executeQuery();
				if (rsSQL.next())
					district = rsSQL.getString("DespEng");

				PreparedStatement psQuery = l_Conn.prepareStatement(Query);
				ResultSet rsQuery = psQuery.executeQuery();
				while (rsQuery.next()) {
					BrandComboData data = new BrandComboData();
					data.setValue(Long.parseLong(rsQuery.getString("code")));
					data.setMyanCaption(rsQuery.getString("DespMyan"));
					data.setEngCaption(rsQuery.getString("DespEng"));
					data.setFlag(false);
					divisionData.add(data);
				}
			}
		} else {
			Query = " SELECT code,DespEng,DespMyan FROM AddressRef WHERE code not LIKE '%000' and code not LIKE '%000000' AND CODE <> '00000000' and DespEng <> 'All' order by code ";
			PreparedStatement pstmt = l_Conn.prepareStatement(Query);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				BrandComboData data = new BrandComboData();
				data.setValue(Long.parseLong(rs.getString("code")));
				data.setMyanCaption(rs.getString("DespMyan"));
				data.setEngCaption(rs.getString("DespEng"));
				data.setFlag(false);
				divisionData.add(data);
			}
		}
		BrandComboData[] dataarry = new BrandComboData[divisionData.size()];
		for (int i = 0; i < divisionData.size(); i++) {
			dataarry[i] = divisionData.get(i);
		}
		ret.setData(dataarry);

		return ret;
	}

	public static BrandComboDataSet getVillageList(String dist_code, Connection l_Conn) throws SQLException {
		BrandComboDataSet ret = new BrandComboDataSet();
		ArrayList<BrandComboData> divisionData = new ArrayList<BrandComboData>();
		String Query = "";
		String sqlVill = "";
		if (dist_code != "") {
			if (dist_code.contains(",")) {
				String splitdistcode[] = dist_code.split(",");
				for (int i = 0; i < splitdistcode.length; i++) {

					sqlVill = " Select code,DespEng,wardcode,DespMyan From AddressExRef where code ='"
							+ splitdistcode[i] + "' order by code ";

					PreparedStatement psQuery = l_Conn.prepareStatement(sqlVill);
					ResultSet rsQuery = psQuery.executeQuery();
					while (rsQuery.next()) {
						BrandComboData data = new BrandComboData();
						data.setCode(rsQuery.getString("wardcode"));
						data.setEngCaption(rsQuery.getString("DespEng"));
						data.setMyanCaption(rsQuery.getString("DespMyan"));
						data.setFlag(false);
						divisionData.add(data);
					}

				}
			} else {
				sqlVill = " Select code,DespEng,wardcode,DespMyan From AddressExRef where code ='" + dist_code
						+ "' order by code ";

				PreparedStatement psQuery = l_Conn.prepareStatement(sqlVill);
				ResultSet rsQuery = psQuery.executeQuery();
				while (rsQuery.next()) {
					BrandComboData data = new BrandComboData();
					data.setCode(rsQuery.getString("wardcode"));
					data.setEngCaption(rsQuery.getString("DespEng"));
					data.setMyanCaption(rsQuery.getString("DespMyan"));
					data.setFlag(false);
					divisionData.add(data);
				}

			}
		} else {
			Query = " SELECT code,DespEng,DespMyan FROM AddressExRef WHERE code not LIKE '%000' and code not LIKE '%000000' AND CODE <> '00000000' and DespEng <> 'All' order by code ";
			PreparedStatement pstmt = l_Conn.prepareStatement(Query);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				BrandComboData data = new BrandComboData();
				data.setCode(rs.getString("wardcode"));
				data.setEngCaption(rs.getString("DespEng"));
				data.setMyanCaption(rs.getString("DespMyan"));
				data.setFlag(false);
				divisionData.add(data);
			}
		}
		BrandComboData[] dataarry = new BrandComboData[divisionData.size()];
		for (int i = 0; i < divisionData.size(); i++) {
			dataarry[i] = divisionData.get(i);
		}
		ret.setData(dataarry);

		return ret;
	}

	public static DBRecord setDBRecord(MessageData data) {
		DBRecord ret = define();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("createddate", data.getCreateddate());
		ret.setValue("createdtime", data.getCreatedtime());
		ret.setValue("modifieddate", data.getModifieddate());
		ret.setValue("modifiedtime", data.getModifiedtime());
		ret.setValue("userid", data.getUserid());
		ret.setValue("username", data.getUsername());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("usersyskey", data.getUsersyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t5", data.getT5());
		ret.setValue("t6", data.getT6());
		ret.setValue("t7", data.getT7());
		ret.setValue("t8", data.getT8());
		ret.setValue("t9", data.getT9());
		ret.setValue("t10", data.getT10());
		ret.setValue("t11", data.getT11());
		ret.setValue("t12", data.getT12());
		ret.setValue("t13", data.getT13());
		ret.setValue("t14", data.getT14());
		ret.setValue("t15", data.getT15());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		ret.setValue("n6", data.getN6());
		ret.setValue("n7", data.getN7());
		ret.setValue("n8", data.getN8());
		ret.setValue("n9", data.getN9());
		ret.setValue("n10", data.getN10());
		ret.setValue("n11", data.getN11());
		ret.setValue("n12", data.getN12());
		ret.setValue("n11", data.getN11());
		return ret;
	}

	public static DBRecord setDBRecord(MessageTypeData data) {
		DBRecord ret = define("JUN003");
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("createddate", data.getCreateddate());
		ret.setValue("createdtime", data.getCreatedtime());
		ret.setValue("modifieddate", data.getModifieddate());
		ret.setValue("modifiedtime", data.getModifiedtime());
		ret.setValue("userid", data.getUserid());
		ret.setValue("username", data.getUsername());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("usersyskey", data.getUsersyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t5", data.getT5());
		ret.setValue("t6", data.getT6());
		ret.setValue("t7", data.getT7());
		ret.setValue("t8", data.getT8());
		ret.setValue("t9", data.getT9());
		ret.setValue("t10", data.getT10());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		ret.setValue("n6", data.getN6());
		ret.setValue("n7", data.getN7());
		ret.setValue("n8", data.getN8());
		ret.setValue("n9", data.getN9());
		ret.setValue("n10", data.getN10());
		return ret;
	}

}
