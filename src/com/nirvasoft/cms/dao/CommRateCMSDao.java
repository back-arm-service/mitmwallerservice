package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.nirvasoft.cms.framework.Result;
import com.nirvasoft.cms.shared.CommRateDetailData;
import com.nirvasoft.cms.shared.CommRateHeaderData;
import com.nirvasoft.cms.shared.MCommRateMappingData;
import com.nirvasoft.cms.shared.MCommRateMappingMappingDataSet;
import com.nirvasoft.rp.data.BranchCodeData;
import com.nirvasoft.rp.framework.Lov3;
import com.nirvasoft.rp.framework.Ref;
import com.nirvasoft.rp.util.GeneralUtil;

public class CommRateCMSDao {

	public static String leadZero(int code, int count) {
		String l_result = "";
		String l_code = String.valueOf(code);
		if (l_code.trim().length() < count) {
			for (int i = l_code.trim().length(); i < count; i++) {
				l_result += "0";
			}
		}
		l_result += code;
		return l_result;
	}

	public boolean canDelete(String commRef, Connection conn) throws SQLException {
		boolean ret = false;
		String l_Query = " Select Count(*) Count From MCommRateMapping Where RecordStatus <> 4 And CommRef1 =? or"
				+ " CommRef2 = ?  or CommRef3 =?  or CommRef4 = ?  or CommRef5 = ? ";
		System.out.println("del query : " + l_Query);
		int count = 0;
		PreparedStatement pstmt = conn.prepareStatement(l_Query);
		for (int i = 1; i < 6; i++) {
			pstmt.setString(i, commRef);
		}
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			count = Integer.parseInt(rs.getString("Count"));
		}

		if (count == 0) {
			ret = true;
		}
		return ret;
	}

	public boolean deletebyMerchantID(String aUserID, Connection l_Conn) throws SQLException {
		// TODO Auto-generated method stub
		boolean ret = false;
		String l_Query1 = "delete from CMSMerchantAccRef where T1=?  ";
		PreparedStatement pstmt = l_Conn.prepareStatement(l_Query1);
		pstmt.setString(1, aUserID);

		if (pstmt.executeUpdate() > 0) {
			ret = true;
		} else {
			ret = false;
		}
		return ret;
	}

	public Result deleteFlexCommRate(String commRef, Connection l_Conn) throws SQLException {
		Result ret = new Result();
		CommRateCMSDao dao = new CommRateCMSDao();
		boolean response = dao.canDelete(commRef, l_Conn);
		if (response) {
			String query = "UPDATE CommRateHeader SET RecordStatus = 4 Where CommRef = ? ";
			PreparedStatement upsmt = l_Conn.prepareStatement(query);
			upsmt.setString(1, commRef);

			if (upsmt.executeUpdate() > 0) {
				String delDetailsql = "DELETE FROM CommRateDetail WHERE Hkey =(SELECT SysKey FROM CommRateHeader WHERE CommRef = ? )";
				PreparedStatement stmt2 = l_Conn.prepareStatement(delDetailsql);
				stmt2.setString(1, commRef);
				int res = stmt2.executeUpdate();
				if (res > 0) {
					ret.setState(true);
					ret.setMsgDesc("Deleted Successfully.");
					ret.setMsgCode("0000");
				} else {
					ret.setState(false);
					ret.setMsgDesc("Deleted Fail.");
					ret.setMsgCode("0014");
				}

			} else {
				ret.setUserId(commRef);
				ret.setState(false);
				ret.setMsgDesc("Cannot delete!");
			}
		} else {
			ret.setUserId(commRef);
			ret.setState(false);
			ret.setMsgDesc("Cannot delete!");
		}

		return ret;

	}

	public Result deleteMerchantCommRateMapping(String MID, int flat, String t1, Connection l_Conn)
			throws SQLException {
		Result ret = new Result();
		String whereclause = "";
		if (flat == 1) {
			whereclause = " Where RecordStatus <> 4 And MerchantID =? And T1 = ?  ";

		} else {
			whereclause = " Where RecordStatus <> 4 And MerchantID = ? ";
		}
		String query = "UPDATE MCommRateMapping SET RecordStatus = 4 " + whereclause;
		PreparedStatement upsmt = l_Conn.prepareStatement(query);
		int i = 1;
		if (flat == 1) {
			upsmt.setString(i++, MID);
			upsmt.setString(i++, t1);
		} else {
			upsmt.setString(i++, MID);
		}

		if (upsmt.executeUpdate() > 0) {
			ret.setState(true);
			ret.setMsgDesc("Deleted Successfully.");
			ret.setMsgCode("0000");
		} else {
			ret.setState(false);
			ret.setMsgDesc("Deleted Fail.");
			ret.setMsgCode("0014");
		}
		return ret;

	}

	public ArrayList<Result> getAllCommRef(Connection conn) throws SQLException {

		ArrayList<Result> datalist = new ArrayList<Result>();

		String sql = "select CommRef,Description from CommRateHeader where recordStatus <> 4 Order by Syskey";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			Result combo = new Result();
			combo.setKeyString(res.getString("CommRef"));
			combo.setKeyst(res.getString("CommRef") + " " + res.getString("Description"));
			datalist.add(combo);
		}
		return datalist;
	}

	public String getAutoGenCode(Connection con) {
		String id = "";
		String sql = "SELECT CODE,Serial,n1,n2 FROM DistributorSetting WHERE ID = (SELECT MAX(ID) FROM DistributorSetting)";
		Statement stmt;
		String codestr = "";
		String serial = "";
		String newSerial = "";
		int count = 0;
		int code = 0;
		try {
			stmt = con.createStatement();
			ResultSet res = stmt.executeQuery(sql);
			while (res.next()) {
				codestr = res.getString("CODE");
				serial = res.getString("Serial");
				count = res.getInt("n1");

			}
			code = Integer.parseInt(serial) + 1;
			newSerial = leadZero(code, count);
			id = codestr + newSerial;
			String query = "INSERT INTO DistributorSetting ( CODE ,Serial ,UserID,t1,t2,t3,t4,t5,n1,n2,n3,n4,n5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement psmt = con.prepareStatement(query);
			int j = 1;
			psmt.setString(j++, codestr);
			psmt.setString(j++, newSerial);
			psmt.setString(j++, id);
			psmt.setString(j++, "");
			psmt.setString(j++, "");
			psmt.setString(j++, "");
			psmt.setString(j++, "");
			psmt.setString(j++, "");
			psmt.setInt(j++, count);
			psmt.setInt(j++, 9999);
			psmt.setInt(j++, 0);
			psmt.setInt(j++, 0);
			psmt.setInt(j++, 0);
			if (psmt.executeUpdate() > 0) {
				System.out.println("Updated successfully");

			} else {
				System.out.println("Updating failed");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}

	public String getAutoGenCommRef(Connection con) {
		String id = "";
		String sql = "Select Format(ISNULL(Max(Convert(int,(RIGHT(CommRef,3)))),0) + 1,'COMREF000') CommRef from CommRateHeader";
		Statement stmt;
		try {
			stmt = con.createStatement();
			ResultSet res = stmt.executeQuery(sql);
			while (res.next()) {
				id = res.getString("CommRef");

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}

	public BranchCodeData getBrCode(Connection conn) throws SQLException {

		BranchCodeData data = new BranchCodeData();
		String sql = "Select BranchCodeStart,BranchCodeLength from CMSModuleConfig Where ActiveBank = 1";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			data.setEndBrCode(Integer.parseInt(res.getString("BranchCodeLength")));
			data.setStartBrCode(Integer.parseInt(res.getString("BranchCodeStart")));
		}
		return data;
	}

	public CommRateHeaderData getFlexCommRateDataByID(String id, Connection conn) {
		// TODO Auto-generated method stub
		CommRateHeaderData ret = new CommRateHeaderData();
		ArrayList<CommRateDetailData> dataList = new ArrayList<>();
		String l_Query = "Select h.CommRef,h.Description,h.MainType ,d.Hkey,d.Zone,d.CommType,d.FromAmt,d.ToAmt,d.Amount,d.MinAmount,d.MaxAmount from CommRateHeader h Inner Join CommRateDetail d on d.HKey = h.Syskey Where h.RecordStatus <> 4 and h.CommRef = ?";

		PreparedStatement pstmt;
		try {
			pstmt = conn.prepareStatement(l_Query);
			pstmt.setString(1, id);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				CommRateDetailData data = new CommRateDetailData();
				ret.setCommRef(rs.getString("CommRef"));
				ret.setDescription(rs.getString("Description"));
				ret.setMainType(rs.getInt("MainType"));
				data.setZone(rs.getInt("Zone"));
				data.setCommType(rs.getInt("CommType"));
				data.setFromAmt(rs.getDouble("FromAmt"));
				data.setToAmt(rs.getDouble("ToAmt"));
				data.setAmount(rs.getDouble("Amount"));
				data.setMinAmt(rs.getDouble("MinAmount"));
				data.setMaxAmt(rs.getDouble("MaxAmount"));
				data.setHkey(rs.getLong("Hkey"));
				dataList.add(data);
			}
			CommRateDetailData[] res = new CommRateDetailData[dataList.size()];
			for (int i = 0; i < dataList.size(); i++) {
				res[i] = dataList.get(i);
			}
			ret.setCommdetailArr(res);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ret;
	}

	public CommRateHeaderData getFlexCommRateList(String searchText, int pageSize, int currentPage, Connection conn)
			throws SQLException {
		CommRateHeaderData ret = new CommRateHeaderData();
		ArrayList<CommRateDetailData> datalist = new ArrayList<CommRateDetailData>();
		int startPage = (currentPage - 1) * pageSize;
		int endPage = pageSize + startPage;

		String whereClause = "";
		if (searchText.equals("") || searchText.isEmpty()) {
			whereClause = " where RecordStatus<>4 ";
		} else {
			whereClause = " where RecordStatus<>4 " + "and " + ("(CommRef like ? or Description like ? )");
		}

		String query = "Select * from ( Select Row_Number() Over (Order By Syskey) As RowNum, CommRef,Description from CommRateHeader "
				+ whereClause + " ) " + "As RowConstrainedResult Where (RowNum > " + startPage + " And RowNum <= "
				+ endPage + ")";

		PreparedStatement ps = conn.prepareStatement(query);
		if (searchText.equals("") || searchText.isEmpty()) {
		} else {
			int i = 1;
			ps.setString(i++, "%" + searchText + "%");
			ps.setString(i++, "%" + searchText + "%");
		}
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			CommRateDetailData data = new CommRateDetailData();
			data.setDescription(rs.getString("Description"));
			data.setCommRef(rs.getString("CommRef"));
			datalist.add(data);
		}

		CommRateDetailData[] dataarr = new CommRateDetailData[datalist.size()];
		for (int i = 0; i < datalist.size(); i++) {
			dataarr[i] = datalist.get(i);
		}

		ret.setCommdetailArr(dataarr);
		ret.setCurrentPage(currentPage);
		ret.setPageSize(pageSize);
		ret.setSearchText(searchText);

		PreparedStatement pstm = conn.prepareStatement("Select COUNT(*) As total FROM CommRateHeader" + whereClause);
		if (searchText.equals("") || searchText.isEmpty()) {
		} else {
			int i = 1;
			pstm.setString(i++, "%" + searchText + "%");
			pstm.setString(i++, "%" + searchText + "%");
		}
		ResultSet result = pstm.executeQuery();
		result.next();
		ret.setTotalCount(result.getInt("total"));

		return ret;
	}

	public MCommRateMappingData getMerchantCommData(String merchantSyskey, Connection l_Conn) throws SQLException {

		MCommRateMappingData ret = new MCommRateMappingData();
		long merchant_syskey = Long.valueOf(merchantSyskey);
		String l_Query = "SELECT MerchantID,KindOfComIssuer,CommRef1,CommRef2,CommRef3,CommRef4,CommRef5,t1,n1,n2 FROM MCommRateMapping WHERE syskey = ?";
		PreparedStatement pstmt = l_Conn.prepareStatement(l_Query);
		pstmt.setLong(1, merchant_syskey);
		ResultSet rs = pstmt.executeQuery();

		while (rs.next()) {
			ret.setMerchantID(rs.getString("MerchantID"));
			ret.setKindOfComIssuer(rs.getInt("KindOfComIssuer"));
			ret.setCommRef1(rs.getString("CommRef1"));
			ret.setCommRef2(rs.getString("CommRef2"));
			ret.setCommRef3(rs.getString("CommRef3"));
			ret.setCommRef4(rs.getString("CommRef4"));
			ret.setCommRef5(rs.getString("CommRef5"));
			ret.setT1(rs.getString("T1"));
			ret.setN1(rs.getInt("N1"));
			ret.setN2(rs.getInt("N2"));
		}

		return ret;
	}

	// atn datanotfound
	public MCommRateMappingMappingDataSet getMerchantCommRateMappingList(String searchtext, int pageSize,
			int currentPage, Connection conn) throws SQLException {
		String searchText = searchtext.replace("'", "''");
		ArrayList<MCommRateMappingData> datalist = new ArrayList<MCommRateMappingData>();
		MCommRateMappingMappingDataSet ret = new MCommRateMappingMappingDataSet();
		int startPage = (currentPage - 1) * pageSize;
		int endPage = pageSize + startPage;
		String whereClause = "";
		if (searchText.equals("")) {
			whereClause = "where r.RecordStatus <> 4";
		} else {
			whereClause = " where r.RecordStatus <> 4 and (m.UserName like ? or r.MerchantID like ? or r.KindOfComIssuer like ?"
					+ " or r.CommRef1 like ? Or  r.CommRef2 like ? or r.CommRef3 like ? Or r.CommRef4 like ?"
					+ " or r.CommRef5 like ? or r.t1 like ? )";
		}
		String l_Query = "SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY r.Syskey) AS RowNum,"
				+ " m.UserName,r.Syskey,r.MerchantID,r.KindOfComIssuer,r.CommRef1,r.CommRef2,r.CommRef3,r.CommRef4,r.CommRef5,r.T1,r.N1 "
				+ "FROM   MCommRateMapping r Inner Join CMSMerchant m On r.MerchantID = m.UserId " + whereClause
				+ " ) AS RowConstrainedResult " + " WHERE (RowNum > " + startPage + " AND RowNum <= " + endPage + ")";

		System.out.println("Merchant query : " + l_Query);

		PreparedStatement ps = conn.prepareStatement(l_Query);
		if (searchText.equals("")) {
		} else {
			for (int i = 1; i < 10; i++) {
				ps.setString(i, "%" + searchText + "%");
			}
		}
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			MCommRateMappingData data = new MCommRateMappingData();
			System.out.println(rs.getString("MerchantID"));
			data.setMerchantName(rs.getString("UserName"));
			data.setMerchantID(rs.getString("MerchantID"));
			data.setKindOfComIssuer(rs.getInt("KindOfComIssuer"));
			data.setCommRef1(rs.getString("CommRef1"));
			data.setCommRef2(rs.getString("CommRef2"));
			data.setCommRef3(rs.getString("CommRef3"));
			data.setCommRef4(rs.getString("CommRef4"));
			data.setCommRef5(rs.getString("CommRef5"));
			data.setT1(rs.getString("t1"));
			data.setSyskey(rs.getLong("Syskey"));
			datalist.add(data);
		}

		MCommRateMappingData[] dataarr = new MCommRateMappingData[datalist.size()];
		for (int i = 0; i < datalist.size(); i++) {
			dataarr[i] = datalist.get(i);
		}

		ret.setData(dataarr);
		ret.setCurrentPage(currentPage);
		ret.setPageSize(pageSize);
		ret.setSearchText(searchText);

		PreparedStatement pstm = conn.prepareStatement(
				"Select COUNT(*) As total FROM MCommRateMapping r Inner Join CMSMerchant m On r.MerchantID = m.UserId "
						+ whereClause);
		if (searchText.equals("")) {
		} else {
			for (int i = 1; i < 10; i++) {
				pstm.setString(i, "%" + searchText + "%");
			}
		}
		ResultSet result = pstm.executeQuery();
		result.next();
		ret.setTotalCount(result.getInt("total"));

		return ret;
	}

	// Merchant Account Mapping Start //

	public ArrayList<Result> getOrderList(Connection conn) throws SQLException {

		ArrayList<Result> datalist = new ArrayList<Result>();

		String sql = "select Code,Description from LOVDetails where hkey = (select syskey from LOVHeader where description = 'Order') ORDER BY Syskey";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			Result combo = new Result();
			combo.setKeyst(res.getString("Code"));
			combo.setKeyString(res.getString("Description"));
			datalist.add(combo);
		}

		return datalist;

	}

	public Lov3 getZone(Connection conn) throws SQLException {

		Lov3 lov3 = new Lov3();
		Ref[] refarray = null;
		ArrayList<Ref> reflist = new ArrayList<Ref>();
		String query = "select code,Description from lovdetails where hkey = ( select syskey from LOVHeader where Description like 'Zone')";
		PreparedStatement stmt = conn.prepareStatement(query);
		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			Ref ref = new Ref();
			ref.setvalue(rs.getString("code"));
			ref.setcaption(rs.getString("Description"));
			reflist.add(ref);
		}

		stmt.close();
		rs.close();

		refarray = new Ref[reflist.size()];
		for (int i = 0; i < reflist.size(); i++) {
			refarray[i] = reflist.get(i);
		}
		lov3.setRef026(refarray);

		return lov3;

	}

	public boolean isCodeExist(String mID, Connection conn) throws SQLException {
		boolean ret = false;
		String l_Query = " SELECT Top(1) MerchantID FROM MCommRateMapping WHERE  RecordStatus <> 4 And MerchantID = ?";

		PreparedStatement pstmt = conn.prepareStatement(l_Query);
		pstmt.setString(1, mID);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			ret = true;
		}
		return ret;
	}

	public Result saveFlexCommRate(CommRateHeaderData aData, Connection l_Conn) throws SQLException {

		String isupdate = "";
		String commRef = "";
		String sql = "";
		long l_RefKey = 0;
		Result response = new Result();
		l_RefKey = aData.getCommdetailArr()[0].getHkey();
		System.out.println("Hkey is: " + l_RefKey);
		String l_Query1 = "delete from CommRateDetail where Hkey in (Select Syskey From CommRateHeader Where CommRef = ? ) ";
		PreparedStatement pstmt = l_Conn.prepareStatement(l_Query1);
		pstmt.setString(1, aData.getCommRef());
		int rs = pstmt.executeUpdate();

		if (rs > 0) {
			isupdate = "1";
		} else {
			isupdate = "0";
			commRef = getAutoGenCommRef(l_Conn);
		}

		if (isupdate.equals("1")) {

			sql = "Update CommRateHeader Set ModifiedDate = ? ,Description = ? ,MainType = ? Where Syskey = ?";
			PreparedStatement ps = l_Conn.prepareStatement(sql);
			int i = 1;
			ps.setString(i++, GeneralUtil.datetoString());
			ps.setString(i++, aData.getDescription());
			ps.setInt(i++, aData.getMainType());
			ps.setLong(i++, l_RefKey);
			int rst = ps.executeUpdate();

			// PreparedStatement ps = l_Conn.prepareStatement(sql,
			// Statement.RETURN_GENERATED_KEYS); // need to change

			if (rst > 0) {
				aData.setState("true");
				commRef = aData.getCommRef();

			} else {
				aData.setState("false");
			}
		} else {
			sql = "INSERT INTO CommRateHeader ( CreatedDate , ModifiedDate ,CommRef ,Description ,MainType ,RecordStatus) values(?,?,?,?,?,?)";
			PreparedStatement preparedstatement = l_Conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			int i = 1;
			preparedstatement.setString(i++, GeneralUtil.datetoString());
			preparedstatement.setString(i++, GeneralUtil.datetoString());
			preparedstatement.setString(i++, commRef);
			preparedstatement.setString(i++, aData.getDescription());
			preparedstatement.setInt(i++, aData.getMainType());
			preparedstatement.setInt(i++, 0);
			preparedstatement.executeUpdate();
			ResultSet result = preparedstatement.getGeneratedKeys();
			if (result != null && result.next()) {
				l_RefKey = result.getLong(1);
				aData.setState("true");
			} else {
				aData.setState("false");
			}
		}

		if (aData.getState().equalsIgnoreCase("true")) {
			String l_Query2 = "INSERT INTO  CommRateDetail ( Hkey , Zone , CommType , FromAmt , ToAmt , Amount , MinAmount , MaxAmount ) VALUES( ?,?,?,?,?,?,?,?)";

			PreparedStatement ps2 = l_Conn.prepareStatement(l_Query2);

			CommRateDetailData[] ucArr = aData.getCommdetailArr();

			for (CommRateDetailData data : ucArr) {
				data.setHkey(l_RefKey);
				updateData(ps2, data, l_Conn);
				ps2.addBatch();
			}

			int status[] = ps2.executeBatch();

			for (int j : status) {
				if (j > 0) {
					isupdate += "1";
					break;
				} else {
					isupdate += "0";
				}
			}
		}

		response.setKeyst(isupdate);
		response.setKeyString(commRef);
		response.setKeyResult(l_RefKey);

		return response;

	}

	public Result saveMerchantCommRateMapping(MCommRateMappingData data, Connection aConn) throws SQLException {

		Result ret = new Result();
		String l_query = "", status = "", whereclause = "";
		PreparedStatement ps = null;

		/*
		 * if (!isCodeExist(data.getMerchantID(), aConn)) {} else {
		 * ret.setMsgCode("0014"); ret.setMsgDesc("Save Failed");
		 * ret.setState(false); }
		 */

		if (data.getN1() == 1) {

			l_query = "Select Count(*) Count From MCommRateMapping Where RecordStatus <> 4 And MerchantID = ? And T1 = ?";
			ps = aConn.prepareStatement(l_query);
			ps.setString(1, data.getMerchantID());
			ps.setString(2, data.getT1());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				int count = rs.getInt("Count");
				if (count > 0) {
					status = "1";// Update
					whereclause = " And t1 = '" + data.getT1() + "' ";
				} else {
					status = "0"; // Insert
				}
			}

		} else {
			l_query = "Select Count(*) Count From MCommRateMapping Where RecordStatus <> 4 And MerchantID = ?";
			ps = aConn.prepareStatement(l_query);
			ps.setString(1, data.getMerchantID());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				int count = rs.getInt("Count");
				if (count > 0) {
					status = "1";// Update
					whereclause = " ";
				} else {
					status = "0"; // Insert
				}

			}
		}

		if (status.equals("1")) {
			String query = "Update MCommRateMapping Set ModifiedDate =? , KindOfComIssuer =? ,CommRef1 =? ,CommRef2 =? ,CommRef3 =?  ,CommRef4 =? ,CommRef5 =? ,T1 = ? ,N1 = ? ,N2 = ? Where MerchantID = ? ";
			query += whereclause;

			PreparedStatement psmt = aConn.prepareStatement(query);

			int i = 1;
			psmt.setString(i++, GeneralUtil.datetoString());
			psmt.setInt(i++, data.getKindOfComIssuer());
			psmt.setString(i++, data.getCommRef1());
			psmt.setString(i++, data.getCommRef2());
			psmt.setString(i++, data.getCommRef3());
			psmt.setString(i++, data.getCommRef4());
			psmt.setString(i++, data.getCommRef5());
			psmt.setString(i++, data.getT1());
			psmt.setInt(i++, data.getN1());
			psmt.setInt(i++, data.getN2());
			psmt.setString(i++, data.getMerchantID());
			if (psmt.executeUpdate() > 0) {
				ret.setMsgCode("0000");
				ret.setMsgDesc("Updated Successfully");
				ret.setState(true);
			} else {
				ret.setMsgCode("0014");
				ret.setMsgDesc("Updated Fail");
				ret.setState(false);
			}
		} else {
			String query = "INSERT INTO MCommRateMapping (CreatedDate, ModifiedDate , MerchantID ,KindOfComIssuer ,CommRef1 ,CommRef2 ,CommRef3  ,CommRef4 ,CommRef5 ,RecordStatus ,T1 ,N1 ,N2 )"
					+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement psmt = aConn.prepareStatement(query);

			int i = 1;
			psmt.setString(i++, GeneralUtil.datetoString());
			psmt.setString(i++, GeneralUtil.datetoString());
			psmt.setString(i++, data.getMerchantID());
			psmt.setInt(i++, data.getKindOfComIssuer());
			psmt.setString(i++, data.getCommRef1());
			psmt.setString(i++, data.getCommRef2());
			psmt.setString(i++, data.getCommRef3());
			psmt.setString(i++, data.getCommRef4());
			psmt.setString(i++, data.getCommRef5());
			psmt.setInt(i++, 0);
			psmt.setString(i++, data.getT1());
			psmt.setInt(i++, data.getN1());
			psmt.setInt(i++, data.getN2());

			if (psmt.executeUpdate() > 0) {
				ret.setMsgCode("0000");
				ret.setMsgDesc("Saved Successfully");
				ret.setState(true);
			} else {
				ret.setMsgCode("0014");
				ret.setMsgDesc("Saved Fail");
				ret.setState(false);
			}
		}

		return ret;

	}

	// Merchant Account Mapping End //

	private void updateData(PreparedStatement ps, CommRateDetailData data, Connection l_Conn) throws SQLException {

		int i = 1;
		ps.setLong(i++, data.getHkey());
		ps.setInt(i++, data.getZone());
		ps.setInt(i++, data.getCommType());
		ps.setDouble(i++, data.getFromAmt());
		ps.setDouble(i++, data.getToAmt());
		ps.setDouble(i++, data.getAmount());
		ps.setDouble(i++, data.getMinAmt());
		ps.setDouble(i++, data.getMaxAmt());
	}

	public Result updateMerchantCommRateMapping(MCommRateMappingData data, Connection aConn) throws SQLException {

		Result ret = new Result();

		String query = "INSERT INTO MCommRateMapping (CreatedDate, ModifiedDate , MerchantID ,KindOfComIssuer ,CommRef1 ,CommRef2 ,CommRef3  ,CommRef4 ,CommRef5 ,RecordStatus ,T1 ,N1,N2 )"
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement psmt = aConn.prepareStatement(query);

		int i = 1;
		psmt.setString(i++, GeneralUtil.datetoString());
		psmt.setString(i++, GeneralUtil.datetoString());
		psmt.setString(i++, data.getMerchantID());
		psmt.setInt(i++, data.getKindOfComIssuer());
		psmt.setString(i++, data.getCommRef1());
		psmt.setString(i++, data.getCommRef2());
		psmt.setString(i++, data.getCommRef3());
		psmt.setString(i++, data.getCommRef4());
		psmt.setString(i++, data.getCommRef5());
		psmt.setInt(i++, 0);
		psmt.setString(i++, data.getT1());
		psmt.setInt(i++, data.getN1());
		psmt.setInt(i++, data.getN2());

		if (psmt.executeUpdate() > 0) {
			ret.setMsgCode("0000");
			ret.setMsgDesc("Saved successfully");
			ret.setState(true);
		} else {
			ret.setMsgCode("0014");
			ret.setMsgDesc("Save Failed");
			ret.setState(false);
		}

		return ret;

	}
}
