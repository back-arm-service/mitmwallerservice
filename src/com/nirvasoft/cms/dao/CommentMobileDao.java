package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;
import com.nirvasoft.database.SysKeyMgr;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.ResultMobile;
import com.nirvasoft.cms.shared.AnswerData;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.shared.ArticleDataSet;
import com.nirvasoft.cms.shared.CommentData;
import com.nirvasoft.cms.shared.RegisterData;
import com.nirvasoft.cms.util.ServerUtil;

public class CommentMobileDao {

	public ResultMobile convertToAns(ArticleData obj, Connection conn) throws SQLException {
		ResultMobile res = new ResultMobile();
		String type = "";
		if (isCodeExist(obj, conn)) {
			String sql = "Update FMR003 SET n2=1 WHERE  RecordStatus = 1  AND t3= '" + type + "' AND syskey = "
					+ obj.getSyskey();
			PreparedStatement stmt = conn.prepareStatement(sql);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				res.setMsgDesc("Updated Successfully!");
			}
		} else {
			res.setMsgDesc("No Such Article to Update!");
		}
		return res;
	}

	public DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("FMR003");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("createdtime", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("modifiedtime", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t13", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		ret.getFields().add(new DBField("n11", (byte) 2));
		ret.getFields().add(new DBField("n12", (byte) 2));
		ret.getFields().add(new DBField("n13", (byte) 2));
		return ret;
	}

	public DBRecord defineReplyRecord() {
		DBRecord ret = new DBRecord();
		ret.setTableName("FMR018");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("createdtime", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("modifiedtime", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t13", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		ret.getFields().add(new DBField("n11", (byte) 2));
		ret.getFields().add(new DBField("n12", (byte) 2));
		ret.getFields().add(new DBField("n13", (byte) 2));
		return ret;
	}

	public ResultMobile delete(Long syskey, Connection conn) throws SQLException {
		ResultMobile res = new ResultMobile();
		String sql = "UPDATE FMR003 SET RecordStatus=4 WHERE syskey=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Successfully");
		} else {
			res.setMsgDesc("Deleting Unsuccessful");
		}
		return res;
	}

	public ResultMobile deleteAllByKey(Long syskey, Connection conn) throws SQLException {
		ResultMobile res = new ResultMobile();
		String sql = "UPDATE FMR003 SET RecordStatus=4 WHERE n1=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Successfully");
		} else {
			res.setMsgDesc("Deleting Unsuccessful");
		}
		return res;
	}

	// delete_comment
	public ResultMobile deleteComment(long syskey, Connection conn) throws SQLException {

		ResultMobile res = new ResultMobile();
		// updaate in article table for count
		ArticleData ret = new ArticleData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "where RecordStatus<>4 AND syskey=" + syskey, "", conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));
		long n1 = ret.getN1();

		// delete comment table
		String sql = "update fmr003 set recordstatus=4 where syskey =" + syskey;
		PreparedStatement stmt = conn.prepareStatement(sql);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Comment Successfully");
			// update count in article table
			String qry = "update fmr002 set n3=(n3-1) where syskey =" + n1;
			PreparedStatement pst = conn.prepareStatement(qry);
			int rst = pst.executeUpdate();
			// delete reply_comment table
			String sql1 = "update fmr018 set recordstatus=4 where n1 =" + syskey;
			PreparedStatement stmt1 = conn.prepareStatement(sql1);
			int rs1 = stmt1.executeUpdate();
			if (rs1 > 0) {
				res.setMsgDesc("Deleted Comment and reply comments Successfully");
			}
			// delete comment like table
			String sql2 = "update fmr017 set recordstatus=4 where n1 =" + syskey;
			PreparedStatement stmt2 = conn.prepareStatement(sql2);
			stmt2.executeUpdate();
		} else {
			res.setMsgDesc("Deleted Unsuccessful");
		}
		return res;
	}

	// delete_reply_comment
	public ResultMobile deleteReplyComment(long syskey, Connection conn) throws SQLException {

		ResultMobile res = new ResultMobile();
		// updaate in article table for count
		ArticleData ret = new ArticleData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(defineReplyRecord(), "where RecordStatus<>4 AND syskey=" + syskey,
				"", conn);
		if (dbrs.size() > 0)
			ret = getDBRecordReplyRecord(dbrs.get(0));
		long n1 = ret.getN1();
		// delete reply comment table
		String sql = "update fmr018 set recordstatus=4 where syskey =" + syskey;
		PreparedStatement stmt = conn.prepareStatement(sql);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Reply Comment Successfully");
			// update count in article table
			String qry = "update fmr003 set n3=(n3-1) where syskey =" + n1;
			PreparedStatement pst = conn.prepareStatement(qry);
			int rst = pst.executeUpdate();
		} else {
			res.setMsgDesc("Deleted Unsuccessful");
		}
		return res;
	}

	public ArticleData[] getCommentLike(long n2, long n1, Connection conn) {

		ArrayList<RegisterData> reglist = new ArrayList<RegisterData>();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		try {
			String sql = "select * from fmr017 where recordstatus=1 and n2=?";
			PreparedStatement ps1 = conn.prepareStatement(sql);
			ps1.setLong(1, n2);
			ResultSet rs1 = ps1.executeQuery();
			while (rs1.next()) {
				ArticleData obj = new ArticleData();
				obj.setSyskey(rs1.getLong("syskey"));
				obj.setAutokey(rs1.getLong("autokey"));
				obj.setCreatedDate(rs1.getString("createddate"));
				obj.setCreatedTime(rs1.getString("createdtime"));
				obj.setModifiedDate(rs1.getString("modifieddate"));
				obj.setModifiedTime(rs1.getString("modifiedtime"));
				obj.setN1(rs1.getLong("n1"));
				obj.setN2(rs1.getLong("n2"));
				//obj.setN3(getCommentLikeCount(n1, n2, conn));
				reglist = RegisterMobileDao.readCommentLikePerson(n2, conn);
				RegisterData[] regarr = new RegisterData[reglist.size()];
				regarr = reglist.toArray(regarr);
				obj.setPerson(regarr);
				datalist.add(obj);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// if(datalist.size()>0) {
		ArticleData[] dataArr = new ArticleData[datalist.size()];
		dataArr = datalist.toArray(dataArr);
		/// }
		return dataArr;
	}

	/*
	 * public Result insert(ArticleData obj, Connection conn) throws
	 * SQLException { Result res = new Result(); if (!isCodeExist(obj, conn)) {
	 * String sql = DBMgr.insertString(define(), conn); PreparedStatement stmt =
	 * conn.prepareStatement(sql); DBRecord dbr = setDBRecord(obj);
	 * DBMgr.setValues(stmt, dbr); int count = stmt.executeUpdate(); if (count >
	 * 0) { res.setState(true); res.setMsgDesc("Saved Successfully!"); } } else
	 * { res.setMsgDesc("Article Already Exist!"); } return res; }
	 */

	public long getCommentLikeCount(long n1, long n2, Connection conn) {
		String sql = "select count(*) as recCount from fmr017 where n2=" + n2 + " and recordStatus=1";
		PreparedStatement ps;
		int count = 0;
		try {
			ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getInt("recCount");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}

	/*
	 * public Result update(ArticleData obj, Connection conn) throws
	 * SQLException { Result res = new Result(); if (isCodeExist(obj, conn)) {
	 * String sql = DBMgr.updateString(" WHERE RecordStatus=1 AND Syskey=" +
	 * obj.getSyskey(), define(), conn); PreparedStatement stmt =
	 * conn.prepareStatement(sql); DBRecord dbr = setDBRecord(obj);
	 * DBMgr.setValues(stmt, dbr); int count = stmt.executeUpdate(); if (count >
	 * 0) { res.setState(true); res.setMsgDesc("Updated Successfully!"); } }
	 * else { res.setMsgDesc("No Such Article to Update!"); } return res; }
	 */

	public ArticleData getDBRecord(DBRecord adbr) {

		ArticleData ret = new ArticleData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("createddate"));
		ret.setCreatedTime(adbr.getString("createdtime"));
		ret.setModifiedDate(adbr.getString("modifieddate"));
		ret.setModifiedTime(adbr.getString("modifiedtime"));
		ret.setUserId(adbr.getString("userid"));
		ret.setUserName(adbr.getString("username"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setUserSyskey(adbr.getLong("usersyskey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		ret.setT8(adbr.getString("t8"));
		ret.setT9(adbr.getString("t9"));
		ret.setT10(adbr.getString("t10"));
		ret.setT11(adbr.getString("t11"));
		ret.setT12(adbr.getString("t12"));
		ret.setT13(adbr.getString("t13"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getLong("n5"));
		ret.setN6(adbr.getLong("n6"));
		ret.setN7(adbr.getLong("n7"));
		ret.setN8(adbr.getLong("n8"));
		ret.setN9(adbr.getLong("n9"));
		ret.setN10(adbr.getLong("n10"));
		ret.setN11(adbr.getLong("n11"));
		ret.setN12(adbr.getLong("n12"));
		ret.setN13(adbr.getLong("n13"));
		return ret;
	}

	public ArticleData getDBRecordReplyRecord(DBRecord adbr) {

		ArticleData ret = new ArticleData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("createddate"));
		ret.setCreatedTime(adbr.getString("createdtime"));
		ret.setModifiedDate(adbr.getString("modifieddate"));
		ret.setModifiedTime(adbr.getString("modifiedtime"));
		ret.setUserId(adbr.getString("userid"));
		ret.setUserName(adbr.getString("username"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setUserSyskey(adbr.getLong("usersyskey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		ret.setT8(adbr.getString("t8"));
		ret.setT9(adbr.getString("t9"));
		ret.setT10(adbr.getString("t10"));
		ret.setT11(adbr.getString("t11"));
		ret.setT12(adbr.getString("t12"));
		ret.setT13(adbr.getString("t13"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getLong("n5"));
		ret.setN6(adbr.getLong("n6"));
		ret.setN7(adbr.getLong("n7"));
		ret.setN8(adbr.getLong("n8"));
		ret.setN9(adbr.getLong("n9"));
		ret.setN10(adbr.getLong("n10"));
		ret.setN11(adbr.getLong("n11"));
		ret.setN12(adbr.getLong("n12"));
		ret.setN13(adbr.getLong("n13"));
		return ret;
	}

	public long getTotalLikeOrUnlikeCount(long syskey, int record, Connection conn) throws SQLException {
		long key = 0;
		String sql = " SELECT n2 FROM FMR003 WHERE  RecordStatus = '" + record + "' AND n1 = " + syskey;
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = rs.getLong("n2");
		}
		return key;
	}

	/* insert */
	public ResultMobile insert(ArticleData obj, Connection conn) throws SQLException {
		ResultMobile res = new ResultMobile();
		String query = "INSERT INTO FMR003 (syskey, createddate,createdtime, modifieddate,modifiedtime, userid, username,"
				+ " recordStatus, syncStatus, syncBatch,usersyskey,t1, t2, t3, t4, t5, t6, t7, t8, t9, t10,"
				+ " t11, t12, t13,n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13)"
				+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?," + "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		try {
			if (!isCodeExist(obj, conn)) {
				PreparedStatement ps = conn.prepareStatement(query);
				ps.setLong(1, obj.getSyskey());
				ps.setString(2, obj.getCreatedDate());
				ps.setString(3, obj.getCreatedTime());
				ps.setString(4, obj.getModifiedDate());
				ps.setString(5, obj.getModifiedTime());
				ps.setString(6, obj.getUserId());
				ps.setString(7, obj.getUserName());
				ps.setInt(8, obj.getRecordStatus());
				ps.setInt(9, obj.getSyncStatus());
				ps.setLong(10, obj.getSyncBatch());
				ps.setLong(11, obj.getUserSyskey());
				ps.setString(12, obj.getT1());
				ps.setString(13, obj.getT2());
				ps.setString(14, obj.getT3());
				ps.setString(15, obj.getT4());
				ps.setString(16, obj.getT5());
				ps.setString(17, obj.getT6());
				ps.setString(18, obj.getT7());
				ps.setString(19, obj.getT8());
				ps.setString(20, obj.getT9());
				ps.setString(21, obj.getT10());
				ps.setString(22, obj.getT11());
				ps.setString(23, obj.getT12());
				ps.setString(24, obj.getT13());
				ps.setLong(25, obj.getN1());
				ps.setLong(26, obj.getN2());
				ps.setLong(27, obj.getN3());
				ps.setLong(28, obj.getN4());
				ps.setLong(29, obj.getN5());
				ps.setLong(30, obj.getN6());
				ps.setLong(31, obj.getN7());
				ps.setLong(32, obj.getN8());
				ps.setLong(33, obj.getN9());
				ps.setLong(34, obj.getN10());
				ps.setLong(35, obj.getN11());
				ps.setLong(36, obj.getN12());
				ps.setLong(37, obj.getN13());

				if (ps.executeUpdate() > 0) {
					res.setState(true);
					res.setMsgDesc("Saved Successfully!");
				} else {
					res.setMsgDesc("Article Already Exist!");
				}
			}

		} catch (SQLException e) {

		}

		return res;

	}

	// save_comment_reply
	/* insert */
	public ResultMobile insertCommentReply(ArticleData obj, Connection conn) throws SQLException {
		ResultMobile res = new ResultMobile();
		String query = "INSERT INTO FMR018 (syskey, createddate,createdtime, modifieddate,modifiedtime, userid, username,"
				+ " recordStatus, syncStatus, syncBatch,usersyskey,t1, t2, t3, t4, t5, t6, t7, t8, t9, t10,"
				+ " t11, t12, t13,n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13)"
				+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?," + "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		try {
			if (!isCodeExist(obj, conn)) {
				PreparedStatement ps = conn.prepareStatement(query);
				ps.setLong(1, obj.getSyskey());
				ps.setString(2, obj.getCreatedDate());
				ps.setString(3, obj.getCreatedTime());
				ps.setString(4, obj.getModifiedDate());
				ps.setString(5, obj.getModifiedTime());
				ps.setString(6, obj.getUserId());
				ps.setString(7, obj.getUserName());
				ps.setInt(8, obj.getRecordStatus());
				ps.setInt(9, obj.getSyncStatus());
				ps.setLong(10, obj.getSyncBatch());
				ps.setLong(11, obj.getUserSyskey());
				ps.setString(12, obj.getT1());
				ps.setString(13, obj.getT2());
				ps.setString(14, obj.getT3());
				ps.setString(15, obj.getT4());
				ps.setString(16, obj.getT5());
				ps.setString(17, obj.getT6());
				ps.setString(18, obj.getT7());
				ps.setString(19, obj.getT8());
				ps.setString(20, obj.getT9());
				ps.setString(21, obj.getT10());
				ps.setString(22, obj.getT11());
				ps.setString(23, obj.getT12());
				ps.setString(24, obj.getT13());
				ps.setLong(25, obj.getN1());
				ps.setLong(26, obj.getN2());
				ps.setLong(27, obj.getN3());
				ps.setLong(28, obj.getN4());
				ps.setLong(29, obj.getN5());
				ps.setLong(30, obj.getN6());
				ps.setLong(31, obj.getN7());
				ps.setLong(32, obj.getN8());
				ps.setLong(33, obj.getN9());
				ps.setLong(34, obj.getN10());
				ps.setLong(35, obj.getN11());
				ps.setLong(36, obj.getN12());
				ps.setLong(37, obj.getN13());

				if (ps.executeUpdate() > 0) {
					res.setState(true);
					res.setMsgDesc("Saved Successfully!");
				} else {
					res.setMsgDesc("Reply comment Already Exist!");
				}
			}

		} catch (SQLException e) {

		}

		return res;

	}

	public ResultMobile insertConvertAns(ArticleData obj, Connection conn) throws SQLException {
		ResultMobile res = new ResultMobile();
		String sql = DBMgr.insertString(define(), conn);
		PreparedStatement stmt = conn.prepareStatement(sql);
		DBRecord dbr = setDBRecord(obj);
		DBMgr.setValues(stmt, dbr);
		int count = stmt.executeUpdate();
		if (count > 0) {
			res.setState(true);
			res.setMsgDesc("Saved Successfully!");
		}
		return res;
	}

	public boolean isCodeExist(ArticleData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				" where RecordStatus<>4 AND syskey = " + obj.getSyskey() + " ", "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	private boolean IsExitUser(Long comkey, Long userSK, Connection conn) throws SQLException {
		boolean b = false;
		String sql = "SELECt * from FMR003 WHERE RecordStatus<>4 and syskey='" + comkey + "' and n5='" + userSK + "'";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			b = true;
		}
		return b;
	}

	public boolean isLikeOrUnLikeExist(Long postkey, Long comkey, Long userSK, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(new ArticleMobileDao().define("FMR016"),
				" WHERE n1 = " + postkey + " AND n2 = " + userSK + "" + " AND n3 = " + comkey + " ", "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isUnLikeExist(Long postkey, Long comkey, Long userSK, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), " WHERE RecordStatus = 4 AND  RecordStatus <> 1 AND "
				+ " n1 = " + postkey + " AND n2 = " + userSK + " AND n3 = " + comkey + " ", "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public ArticleData read(long syskey, Connection conn) throws SQLException {
		ArticleData ret = new ArticleData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "where RecordStatus<>4 AND syskey=" + syskey, "", conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));
		return ret;
	}

	public ArticleData readData(long syskey, Connection conn) throws SQLException {
		ArticleData ret = new ArticleData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "WHERE RecordStatus<>4 AND n1=" + syskey, "", conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));

		return ret;
	}

	public ArticleDataSet saveCommentLike(ArticleData obj, Connection conn) {
		ResultMobile res = new ResultMobile();
		ArrayList<ArticleData> arrlist = new ArrayList<ArticleData>();
		ArrayList<RegisterData> reglist = new ArrayList<RegisterData>();
		ArticleDataSet dataset = new ArticleDataSet();
		String query = "INSERT INTO FMR017 (syskey, createddate,createdtime, modifieddate,modifiedtime, userid, username,"
				+ " recordStatus, syncStatus, syncBatch,usersyskey,t1, t2, t3, t4, t5, t6, t7, t8, t9, t10,"
				+ " t11, t12, t13,n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13)"
				+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?," + "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		try {
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setLong(1, obj.getSyskey());
			ps.setString(2, obj.getCreatedDate());
			ps.setString(3, obj.getCreatedTime());
			ps.setString(4, obj.getModifiedDate());
			ps.setString(5, obj.getModifiedTime());
			ps.setString(6, obj.getUserId());
			ps.setString(7, obj.getUserName());
			ps.setInt(8, obj.getRecordStatus());
			ps.setInt(9, obj.getSyncStatus());
			ps.setLong(10, obj.getSyncBatch());
			ps.setLong(11, obj.getUserSyskey());
			ps.setString(12, obj.getT1());
			ps.setString(13, obj.getT2());
			ps.setString(14, obj.getT3());
			ps.setString(15, obj.getT4());
			ps.setString(16, obj.getT5());
			ps.setString(17, obj.getT6());
			ps.setString(18, obj.getT7());
			ps.setString(19, obj.getT8());
			ps.setString(20, obj.getT9());
			ps.setString(21, obj.getT10());
			ps.setString(22, obj.getT11());
			ps.setString(23, obj.getT12());
			ps.setString(24, obj.getT13());
			ps.setLong(25, obj.getN1());
			ps.setLong(26, obj.getN2());
			ps.setLong(27, obj.getN3());
			ps.setLong(28, obj.getN4());
			ps.setLong(29, obj.getN5());
			ps.setLong(30, obj.getN6());
			ps.setLong(31, obj.getN7());
			ps.setLong(32, obj.getN8());
			ps.setLong(33, obj.getN9());
			ps.setLong(34, obj.getN10());
			ps.setLong(35, obj.getN11());
			ps.setLong(36, obj.getN12());
			ps.setLong(37, obj.getN13());
			if (ps.executeUpdate() > 0) {
				obj.setN3(getCommentLikeCount(obj.getN1(), obj.getN2(), conn));
				reglist = RegisterMobileDao.readCommentLikePerson(obj.getN2(), conn);
				RegisterData[] regarr = new RegisterData[reglist.size()];
				regarr = reglist.toArray(regarr);
				obj.setPerson(regarr);
				arrlist.add(obj);
				res.setState(true);
				res.setMsgDesc("Saved Successfully!");
			} else {
				res.setMsgDesc("Article Already Exist!");
			}

		} catch (SQLException e) {

		}
		if (arrlist.size() > 0) {
			ArticleData[] dataArr = new ArticleData[arrlist.size()];
			dataArr = arrlist.toArray(dataArr);
			dataset.setData(dataArr);
			dataset.setState(true);
		}
		return dataset;

	}

	public ArticleDataSet search(String searchVal, Connection conn) throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		String whereclause = " WHERE   RecordStatus = 1 AND n1 = " + searchVal;
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereclause, " ORDER BY syskey ", conn);
		for (int i = 0; i < dbrs.size(); i++) {

			if (dbrs.get(i).getLong("n5") == 11384) {
				dbrs.get(i).setValue("username", "Administrator");
			} else {
				RegisterData uData = RegisterMobileDao.readData(dbrs.get(i).getLong("n5"), conn);

				dbrs.get(i).setValue("username", uData.getT3());
			}

			datalist.add(getDBRecord(dbrs.get(i)));
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	public ArticleDataSet searchAns(String searchVal, String type, Connection conn) throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		String whereclause = " WHERE RecordStatus = 1 AND t3 = '" + type + "' AND n1 = " + searchVal;
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereclause, " ORDER BY syskey ", conn);

		for (int i = 0; i < dbrs.size(); i++) {
			RegisterData uData = RegisterMobileDao.readData(dbrs.get(i).getLong("n5"), conn);

			dbrs.get(i).setValue("username", uData.getT3());
			datalist.add(getDBRecord(dbrs.get(i)));
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	/* answer */
	public ArticleDataSet searchAnswer(long id, String type, Connection conn) throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> dataList = new ArrayList<ArticleData>();
		ArrayList<String> strList = new ArrayList<String>();
		String whereclause = "where recordstatus=1 and t1= '" + type + "' and n1= " + id;
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereclause, "", conn);
		for (int i = 0; i < dbrs.size(); i++) {
			dataList.add(getDBRecord(dbrs.get(i)));
			strList.add(getDBRecord(dbrs.get(i)).getT2());
		}
		if (dataList.size() > 0) {
			res.setState(true);
			ArticleData[] dataArray = new ArticleData[dataList.size()];
			dataArray = dataList.toArray(dataArray);
			String[] strArray = new String[strList.size()];
			strArray = strList.toArray(strArray);
			res.setData(dataArray);
			res.setAnsData(strArray);
		} else {
			res.setState(false);
		}
		return res;
	}

	// TDA for
	public CommentData[] searchcomment(long postsk, Connection conn) throws SQLException {
		ArrayList<CommentData> datalist = new ArrayList<CommentData>();
		CommentData data;
		String sql = " select n5,t2,modifieddate,modifiedtime from fmr003 where recordstatus<>4 and n1='" + postsk
				+ "'";
		Statement stmts = conn.createStatement();
		ResultSet rs = stmts.executeQuery(sql);
		while (rs.next()) {
			data = new CommentData();
			RegisterData uData = RegisterMobileDao.readData(rs.getLong("n5"), conn);
			data.setUserId(uData.getT1());
			data.setUserName(uData.getT3());
			data.setUserSyskey(rs.getLong("n5"));
			data.setComment(rs.getString("t2"));
			String date = ServerUtil.changeDateFormat(rs.getString("modifieddate"), rs.getString("modifiedtime"));
			data.setModifieddate(date);
			datalist.add(data);
		}

		CommentData[] dataarray = new CommentData[datalist.size()];
		dataarray = datalist.toArray(dataarray);

		return dataarray;
	}

	/* search comment mobile */
	public ArticleDataSet searchComment(String searchVal, Connection conn) throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		ArrayList<ArticleData> ret = new ArrayList<ArticleData>();
		String type = "";
		String whereclause = " WHERE   RecordStatus = 1 AND t3 = '" + type + "' and n1 = " + searchVal;
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereclause, " ORDER BY syskey ", conn);
		for (int i = 0; i < dbrs.size(); i++) {
			if (dbrs.get(i).getLong("n5") == 11384) {
				dbrs.get(i).setValue("username", "Farmer");
			} else {
				RegisterData uData = RegisterMobileDao.readData(dbrs.get(i).getLong("n5"), conn);
				dbrs.get(i).setValue("username", uData.getT3());
				dbrs.get(i).setValue("t10", uData.getT16());

			}
			ret = new ArticleMobileDao().searchLikeOrNotsQuestionSaveCommen(searchVal,
					String.valueOf(dbrs.get(i).getLong("n5")), dbrs.get(i).getLong("syskey"), conn);
			for (int j = 0; j < ret.size(); j++) {
				dbrs.get(i).setValue("n6", ret.get(j).getSyskey());
			}
			datalist.add(getDBRecord(dbrs.get(i)));
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	// TDA Not Including Me
	public ArticleDataSet searchCommentNotification(String Postsk, String usersk, Connection conn) throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		String sql1 = "Select n5  from fmr002 where recordstatus<>4 and n5<>11384 and syskey='" + Postsk + "'";
		PreparedStatement ps1 = conn.prepareStatement(sql1);
		ResultSet rs1 = ps1.executeQuery();
		if (rs1.next()) {
			ArticleData data = new ArticleData();
			// if(usersk!=(String.valueOf(rs1.getLong("n5")))){
			if (!usersk.equalsIgnoreCase(String.valueOf(rs1.getLong("n5")))) {
				data.setN5(rs1.getLong("n5"));
				data.setT3(RegisterMobileDao.readT62(data.getN5(), conn));
				datalist.add(data);
			}
		}
		String sql = "SELECT  * FROM    (SELECT syskey,t1,t2,n1,n5, ROW_NUMBER() OVER (partition by n5 ORDER BY n5) AS RowNumber FROM   fmr003 "
				+ " WHERE  RecordStatus <>4  AND n1 = '" + Postsk + "' AND n5 <> '" + usersk
				+ "' ) AS a WHERE   a.RowNumber = 1";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			if (datalist.size() > 0) {
				if (datalist.get(0).getN5() != (rs.getLong("n5"))) {
					ArticleData data = new ArticleData();
					data.setSyskey(rs.getLong("syskey"));
					data.setT1(rs.getString("t1"));
					data.setT2(rs.getString("t2"));
					data.setN1(rs.getLong("n1"));
					data.setN5(rs.getLong("n5"));
					data.setT3(RegisterMobileDao.readT62(data.getN5(), conn));
					datalist.add(data);
				}
			} else {
				ArticleData data = new ArticleData();
				data.setSyskey(rs.getLong("syskey"));
				data.setT1(rs.getString("t1"));
				data.setT2(rs.getString("t2"));
				data.setN1(rs.getLong("n1"));
				data.setN5(rs.getLong("n5"));
				data.setT3(RegisterMobileDao.readT62(data.getN5(), conn));
				datalist.add(data);
			}

		}

		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	public ArticleDataSet searchCommentNotificationAdminNoti(String Postsk, String usersk, Connection conn)
			throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		String sql1 = "Select n5  from fmr003 where recordstatus<>4 and syskey='" + Postsk + "'";
		PreparedStatement ps1 = conn.prepareStatement(sql1);
		ResultSet rs1 = ps1.executeQuery();
		ArticleData data = new ArticleData();
		if (rs1.next()) {
			data.setN1(Long.parseLong(Postsk));
			data.setN5(rs1.getLong("n5"));
			data.setT3(RegisterMobileDao.readT62(data.getN5(), conn));
		}
		String sql = "Select syskey,t1,t2 from fmr003 where recordstatus<>4 and n1='" + Postsk + "' and n5='" + usersk
				+ "' ";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			data.setSyskey(rs.getLong("syskey"));
			data.setT1(rs.getString("t1"));
			data.setT2(rs.getString("t2"));
			datalist.add(data);
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	/* search comment mobile */
	public ArticleDataSet searchCommentReply(String searchVal, Connection conn) throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		ArrayList<ArticleData> ret = new ArrayList<ArticleData>();
		String type = "";
		String whereclause = " WHERE RecordStatus = 1 AND t3 = '" + type + "' and n1 = " + searchVal;
		String sql = "";
		try {
			sql = "select * from fmr018" + whereclause;
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				ArticleData obj = new ArticleData();
				obj.setT1(rs.getString("t1"));
				/*
				 * if (ServerUtil.isZawgyiEncoded(rs.getString("t2"))) {
				 * obj.setT2(FontChangeUtil.zg2uni(rs.getString("t2"))); }
				 */
				obj.setT2(rs.getString("t2"));
				obj.setN5(rs.getLong("n5"));
				obj.setCreatedDate(rs.getString("createddate"));
				obj.setCreatedTime(rs.getString("createdtime"));
				obj.setModifiedDate(rs.getString("modifieddate"));
				obj.setModifiedTime(rs.getString("modifiedtime"));

				RegisterData uData = RegisterMobileDao.readData(rs.getLong("n5"), conn);
				obj.setUserName(uData.getT3());
				obj.setT10(uData.getT16());

				ret = new ArticleMobileDao().searchLikeOrNotsQuestionSaveCommenReply(searchVal,
						String.valueOf(rs.getLong("n5")), rs.getLong("syskey"), conn);
				for (int j = 0; j < ret.size(); j++) {
					obj.setN6(ret.get(j).getSyskey());
				}
				datalist.add(obj);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	public ArticleDataSet searchCommentReplyNotification(String Postsk, String usersk, Connection conn)
			throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		String sql1 = "Select n5  from fmr003 where recordstatus<>4 and n5<>11384 and syskey='" + Postsk + "'";
		PreparedStatement ps1 = conn.prepareStatement(sql1);
		ResultSet rs1 = ps1.executeQuery();
		if (rs1.next()) {
			ArticleData data = new ArticleData();
			// if(usersk!=(String.valueOf(rs1.getLong("n5")))){
			if (!usersk.equalsIgnoreCase(String.valueOf(rs1.getLong("n5")))) {
				data.setN5(rs1.getLong("n5"));
				data.setT3(RegisterMobileDao.readT62(data.getN5(), conn));
				datalist.add(data);
			}
		}
		String sql = "SELECT  * FROM    (SELECT syskey,t1,t2,n1,n5, ROW_NUMBER() OVER (partition by n5 ORDER BY n5) AS RowNumber FROM   fmr018 "
				+ " WHERE  RecordStatus <>4  AND n1 = '" + Postsk + "' AND n5 <> '" + usersk
				+ "' ) AS a WHERE   a.RowNumber = 1";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			if (datalist.size() > 0) {
				if (datalist.get(0).getN5() != (rs.getLong("n5"))) {
					ArticleData data = new ArticleData();
					data.setSyskey(rs.getLong("syskey"));
					data.setT1(rs.getString("t1"));
					data.setT2(rs.getString("t2"));
					data.setN1(rs.getLong("n1"));
					data.setN5(rs.getLong("n5"));
					data.setT3(RegisterMobileDao.readT62(data.getN5(), conn));
					datalist.add(data);
				}
			} else {
				ArticleData data = new ArticleData();
				data.setSyskey(rs.getLong("syskey"));
				data.setT1(rs.getString("t1"));
				data.setT2(rs.getString("t2"));
				data.setN1(rs.getLong("n1"));
				data.setN5(rs.getLong("n5"));
				data.setT3(RegisterMobileDao.readT62(data.getN5(), conn));
				datalist.add(data);
			}

		}

		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	public ArticleDataSet searchCommentReplyNotificationAdminNoti(String Postsk, String usersk, Connection conn)
			throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		String sql1 = "Select n5  from fmr018 where recordstatus<>4 and syskey='" + Postsk + "'";
		PreparedStatement ps1 = conn.prepareStatement(sql1);
		ResultSet rs1 = ps1.executeQuery();
		ArticleData data = new ArticleData();
		if (rs1.next()) {
			data.setN1(Long.parseLong(Postsk));
			data.setN5(rs1.getLong("n5"));
			data.setT3(RegisterMobileDao.readT62(data.getN5(), conn));
		}
		String sql = "Select syskey,t1,t2 from fmr018 where recordstatus<>4 and n1='" + Postsk + "' and n5='" + usersk
				+ "' ";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			data.setSyskey(rs.getLong("syskey"));
			data.setT1(rs.getString("t1"));
			data.setT2(rs.getString("t2"));
			datalist.add(data);
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	///////////////////////////////////// Mobile///////////////////////////////////////////////////////////////////////
	// TDA search comment for getcomments
	/*
	 * public ArticleDataSet searchComments(String searchVal, String usersk,
	 * Connection conn) throws SQLException { ArticleDataSet res = new
	 * ArticleDataSet(); ArrayList<ArticleData> datalist = new
	 * ArrayList<ArticleData>(); ArrayList<ArticleData> ret = new
	 * ArrayList<ArticleData>(); String whereclause =
	 * " WHERE   RecordStatus = 1 AND  n1 = " + searchVal; ArrayList<DBRecord>
	 * dbrs = DBMgr.getDBRecords(define(), whereclause, " ORDER BY syskey ",
	 * conn); for (int i = 0; i < dbrs.size(); i++) { if
	 * (dbrs.get(i).getLong("n5") == 11384) { dbrs.get(i).setValue("username",
	 * "Administrator"); } else { RegisterData uData =
	 * RegisterDao.readData(dbrs.get(i).getLong("n5"), conn);
	 * dbrs.get(i).setValue("username", uData.getT3());
	 * dbrs.get(i).setValue("t10", uData.getT16());
	 * 
	 * } //dbrs.get(i).setValue("commentLike",
	 * getCommentLike(dbrs.get(i).getLong("syskey"),Long.parseLong(usersk),conn)
	 * ); ret = ArticleDao.searchLikeOrNotsQuestionSaveCommen(searchVal, usersk,
	 * dbrs.get(i).getLong("syskey"), conn); for (int j = 0; j < ret.size();
	 * j++) { dbrs.get(i).setValue("n6", ret.get(j).getSyskey()); }
	 * 
	 * datalist.add(getDBRecord(dbrs.get(i))); } if (datalist.size() > 0) {
	 * res.setState(true); } else { res.setState(false); } ArticleData[]
	 * dataarray = new ArticleData[datalist.size()]; dataarray =
	 * datalist.toArray(dataarray); res.setData(dataarray); return res; }
	 */

	public ArticleDataSet searchComments(String searchVal, String usersk, Connection conn) throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		ArrayList<ArticleData> ret = new ArrayList<ArticleData>();
		ArrayList<RegisterData> reglist = new ArrayList<RegisterData>();
		String whereclause = " WHERE RecordStatus = 1 AND  n1 = " + searchVal;
		String sql = "";
		try {
			sql = "select * from fmr003 " + whereclause;

			PreparedStatement ps1 = conn.prepareStatement(sql);
			ResultSet rs1 = ps1.executeQuery();
			while (rs1.next()) {
				ArticleData obj = new ArticleData();
				obj.setSyskey(rs1.getLong("syskey"));
				obj.setAutokey(rs1.getLong("autokey"));
				obj.setCreatedDate(rs1.getString("createddate"));
				obj.setCreatedTime(rs1.getString("createdtime"));
				obj.setModifiedDate(rs1.getString("modifieddate"));
				obj.setModifiedTime(rs1.getString("modifiedtime"));
				obj.setRecordStatus(rs1.getInt("recordStatus"));
				obj.setN1(rs1.getLong("n1"));
				obj.setN2(rs1.getLong("n2"));
				obj.setN5(rs1.getLong("n5"));
				obj.setT1(rs1.getString("t1"));
				obj.setT2(rs1.getString("t2"));
				obj.setN3(rs1.getLong("n3")); // reply_count
				if (rs1.getLong("n3") > 1) {
					ArticleData retReply = new ArticleData();
					ArrayList<ArticleData> replyList = new ArrayList<ArticleData>();
					ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(defineReplyRecord(),
							"where RecordStatus<>4 AND n1=" + rs1.getLong("syskey"), "", conn);
					if (dbrs.size() > 0)
						retReply = getDBRecordReplyRecord(dbrs.get(0));

					retReply.setPerson(RegisterMobileDao.readByIDReply(retReply.getN5(), conn));
					replyList.add(retReply);
					ArticleData[] replyarr = new ArticleData[replyList.size()];
					replyarr = replyList.toArray(replyarr);
					obj.setReplyComment(replyarr);
				}
				if (rs1.getLong("n5") == 11384) {
					obj.setUserName("Administrator");
				} else {
					RegisterData uData = RegisterMobileDao.readData(rs1.getLong("n5"), conn);
					obj.setUserName(uData.getT3());
					obj.setT10(uData.getT16());
				}
				// obj.setCommentLike(getCommentLike(rs1.getLong("syskey"),Long.parseLong(usersk),conn));
				reglist = RegisterMobileDao.readCommentLikePerson(rs1.getLong("syskey"), conn);
				RegisterData[] regarr = new RegisterData[reglist.size()];
				regarr = reglist.toArray(regarr);
				obj.setPerson(regarr);

				ret = new ArticleMobileDao().searchLikeOrNotsQuestionSaveCommen(searchVal, usersk, rs1.getLong("syskey"),
						conn);
				for (int j = 0; j < ret.size(); j++) {
					obj.setN6(rs1.getLong("syskey"));
				}
				datalist.add(obj);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	public ArticleDataSet searchComments1(String searchVal, String usersk, Connection conn) throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		ArrayList<ArticleData> ret = new ArrayList<ArticleData>();
		ArrayList<RegisterData> reglist = new ArrayList<RegisterData>();
		String whereclause = " WHERE RecordStatus = 1 AND  n1 = " + searchVal;
		String sql = "";
		try {
			sql = "select * from fmr003 " + whereclause;

			PreparedStatement ps1 = conn.prepareStatement(sql);
			ResultSet rs1 = ps1.executeQuery();
			while (rs1.next()) {
				ArticleData obj = new ArticleData();
				obj.setSyskey(rs1.getLong("syskey"));
				obj.setAutokey(rs1.getLong("autokey"));
				obj.setCreatedDate(rs1.getString("createddate"));
				obj.setCreatedTime(rs1.getString("createdtime"));
				obj.setModifiedDate(rs1.getString("modifieddate"));
				obj.setModifiedTime(rs1.getString("modifiedtime"));
				obj.setRecordStatus(rs1.getInt("recordStatus"));
				obj.setN1(rs1.getLong("n1"));
				obj.setN2(rs1.getLong("n2"));
				obj.setN5(rs1.getLong("n5"));
				obj.setT1(rs1.getString("t1"));
				/*
				 * if (ServerUtil.isZawgyiEncoded(rs1.getString("t2"))) {
				 * obj.setT2(FontChangeUtil.zg2uni(rs1.getString("t2"))); }
				 */
				obj.setT2(rs1.getString("t2"));
				obj.setN3(rs1.getLong("n3")); // reply_count
				if (rs1.getLong("n3") > 1) {
					ArticleData retReply = new ArticleData();
					ArrayList<ArticleData> replyList = new ArrayList<ArticleData>();
					ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(defineReplyRecord(),
							"where RecordStatus<>4 AND n1=" + rs1.getLong("syskey"), "", conn);
					if (dbrs.size() > 0)
						retReply = getDBRecordReplyRecord(dbrs.get(0));

					retReply.setPerson(RegisterMobileDao.readByIDReply(retReply.getN5(), conn));
					replyList.add(retReply);
					ArticleData[] replyarr = new ArticleData[replyList.size()];
					replyarr = replyList.toArray(replyarr);
					obj.setReplyComment(replyarr);
				}
				if (rs1.getLong("n5") == 11384) {
					obj.setUserName("Administrator");
				} else {
					RegisterData uData = RegisterMobileDao.readData(rs1.getLong("n5"), conn);
					obj.setUserName(uData.getT3());
					obj.setT10(uData.getT16());
				}
				// obj.setCommentLike(getCommentLike(rs1.getLong("syskey"),Long.parseLong(usersk),conn));
				reglist = RegisterMobileDao.readCommentLikePerson(rs1.getLong("syskey"), conn);
				RegisterData[] regarr = new RegisterData[reglist.size()];
				regarr = reglist.toArray(regarr);
				obj.setPerson(regarr);

				ret = new ArticleMobileDao().searchLikeOrNotsQuestionSaveCommen(searchVal, usersk, rs1.getLong("syskey"),
						conn);
				for (int j = 0; j < ret.size(); j++) {
					obj.setN6(rs1.getLong("syskey"));
				}
				datalist.add(obj);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	public ArticleDataSet searchCommentsReply(String searchVal, String usersk, Connection conn) throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		ArrayList<ArticleData> ret = new ArrayList<ArticleData>();
		String whereclause = " WHERE RecordStatus = 1 AND  n1 = " + searchVal;
		String sql = "";
		try {
			sql = "select * from fmr018 " + whereclause;

			PreparedStatement ps1 = conn.prepareStatement(sql);
			ResultSet rs1 = ps1.executeQuery();
			while (rs1.next()) {
				ArticleData obj = new ArticleData();
				obj.setSyskey(rs1.getLong("syskey"));
				obj.setAutokey(rs1.getLong("autokey"));
				obj.setCreatedDate(rs1.getString("createddate"));
				obj.setCreatedTime(rs1.getString("createdtime"));
				obj.setModifiedDate(rs1.getString("modifieddate"));
				obj.setModifiedTime(rs1.getString("modifiedtime"));
				obj.setRecordStatus(rs1.getInt("recordStatus"));
				obj.setN1(rs1.getLong("n1"));
				obj.setN2(rs1.getLong("n2"));
				obj.setN5(rs1.getLong("n5"));
				obj.setT1(rs1.getString("t1"));
				obj.setT2(rs1.getString("t2"));
				RegisterData uData = RegisterMobileDao.readData(rs1.getLong("n5"), conn);
				obj.setUserName(uData.getT3());
				obj.setT10(uData.getT16());

				datalist.add(obj);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	///////////////////////////////////////////// Mobile//////////////////////////////////////////////////////////////
	public CommentData[] searchLatest(long searchVal, Connection conn) throws SQLException {
		ArrayList<CommentData> datalist = new ArrayList<CommentData>();
		String sql = "  select n5, t2 from FMR003 as f1 where syskey = (select max(syskey) from FMR003 f2 where recordstatus<>4 and n1= "
				+ searchVal + ")";
		Statement stmts = conn.createStatement();
		ResultSet rs = stmts.executeQuery(sql);
		if (rs.next()) {
			CommentData data = new CommentData();
			RegisterData uData = RegisterMobileDao.readData(rs.getLong("n5"), conn);
			data.setUserId(uData.getT1());
			data.setUserName(uData.getT3());
			data.setUserSyskey(rs.getLong("n5"));
			data.setComment(rs.getString("t2"));
			datalist.add(data);
		}
		CommentData[] dataarray = new CommentData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		return dataarray;
	}

	public AnswerData[] searchLatestAns(long searchVal, String type, Connection conn) throws SQLException {
		ArrayList<AnswerData> datalist = new ArrayList<AnswerData>();
		String sql = "  select n5, t2 from FMR003 where recordstatus=1 and t3= '" + type + "' and n1= " + searchVal;
		Statement stmts = conn.createStatement();
		ResultSet rs = stmts.executeQuery(sql);
		if (rs.next()) {
			AnswerData data = new AnswerData();
			RegisterData uData = RegisterMobileDao.readData(rs.getLong("n5"), conn);
			data.setUserId(uData.getT1());
			data.setUserName(uData.getT3());
			data.setUserSyskey(rs.getLong("n5"));
			data.setAnswer(rs.getString("t2"));
			datalist.add(data);
		}
		AnswerData[] dataarray = new AnswerData[datalist.size()];
		dataarray = datalist.toArray(dataarray);

		return dataarray;
	}

	public DBRecord setDBRecord(ArticleData data) {
		DBRecord ret = defineReplyRecord();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("createddate", data.getCreatedDate());
		ret.setValue("createdtime", data.getCreatedTime());
		ret.setValue("modifieddate", data.getModifiedDate());
		ret.setValue("modifiedtime", data.getModifiedTime());
		ret.setValue("userid", data.getUserId());
		ret.setValue("username", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("usersyskey", data.getUserSyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t5", data.getT5());
		ret.setValue("t6", data.getT6());
		ret.setValue("t7", data.getT7());
		ret.setValue("t8", data.getT8());
		ret.setValue("t9", data.getT9());
		ret.setValue("t10", data.getT10());
		ret.setValue("t11", data.getT11());
		ret.setValue("t12", data.getT12());
		ret.setValue("t13", data.getT13());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		ret.setValue("n6", data.getN6());
		ret.setValue("n7", data.getN7());
		ret.setValue("n8", data.getN8());
		ret.setValue("n9", data.getN9());
		ret.setValue("n10", data.getN10());
		ret.setValue("n11", data.getN11());
		ret.setValue("n12", data.getN12());
		ret.setValue("n13", data.getN13());
		return ret;
	}

	public DBRecord setDBRecordReplyRecord(ArticleData data) {
		DBRecord ret = defineReplyRecord();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("createddate", data.getCreatedDate());
		ret.setValue("createdtime", data.getCreatedTime());
		ret.setValue("modifieddate", data.getModifiedDate());
		ret.setValue("modifiedtime", data.getModifiedTime());
		ret.setValue("userid", data.getUserId());
		ret.setValue("username", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("usersyskey", data.getUserSyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t5", data.getT5());
		ret.setValue("t6", data.getT6());
		ret.setValue("t7", data.getT7());
		ret.setValue("t8", data.getT8());
		ret.setValue("t9", data.getT9());
		ret.setValue("t10", data.getT10());
		ret.setValue("t11", data.getT11());
		ret.setValue("t12", data.getT12());
		ret.setValue("t13", data.getT13());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		ret.setValue("n6", data.getN6());
		ret.setValue("n7", data.getN7());
		ret.setValue("n8", data.getN8());
		ret.setValue("n9", data.getN9());
		ret.setValue("n10", data.getN10());
		ret.setValue("n11", data.getN11());
		ret.setValue("n12", data.getN12());
		ret.setValue("n13", data.getN13());
		return ret;
	}

	public ResultMobile update(ArticleData obj, Connection conn) throws SQLException {
		ResultMobile res = new ResultMobile();
		String query = "UPDATE FMR003 SET [syskey]=?, [createddate]=?,[createdtime]=?, [modifieddate]=?,[modifiedtime]=?, [userid]=?, [username]=?, "
				+ " [recordstatus]=?, [syncstatus]=?, [syncbatch]=?,[usersyskey]=?, [t1]=?, [t2]=?, [t3]=?,[t4]=?, [t5]=?,"
				+ " [t6]=?, [t7]=?,[t8]=?, [t9]=?,[t10]=?, [t11]=?,[t12]=?, [t13]=?, [n1]=?, [n2]=?, [n3]=?,"
				+ " [n4]=?, [n5]=?,[n6]=?, [n7]=?,[n8]=?, [n9]=?,[n10]=?, [n11]=?,[n12]=?, [n13]=?"
				+ " WHERE RecordStatus=1 AND Syskey=" + obj.getSyskey() + "";

		try {
			if (isCodeExist(obj, conn)) {
				PreparedStatement ps = conn.prepareStatement(query);
				ps.setLong(1, obj.getSyskey());
				ps.setString(2, obj.getCreatedDate());
				ps.setString(3, obj.getCreatedTime());
				ps.setString(4, obj.getModifiedDate());
				ps.setString(5, obj.getModifiedTime());
				ps.setString(6, obj.getUserId());
				ps.setString(7, obj.getUserName());
				ps.setInt(8, obj.getRecordStatus());
				ps.setInt(9, obj.getSyncStatus());
				ps.setLong(10, obj.getSyncBatch());
				ps.setLong(11, obj.getUserSyskey());
				ps.setString(12, obj.getT1());
				ps.setString(13, obj.getT2());
				ps.setString(14, obj.getT3());
				ps.setString(15, obj.getT4());
				ps.setString(16, obj.getT5());
				ps.setString(17, obj.getT6());
				ps.setString(18, obj.getT7());
				ps.setString(19, obj.getT8());
				ps.setString(20, obj.getT9());
				ps.setString(21, obj.getT10());
				ps.setString(22, obj.getT11());
				ps.setString(23, obj.getT12());
				ps.setString(24, obj.getT13());
				ps.setLong(25, obj.getN1());
				ps.setLong(26, obj.getN2());
				ps.setLong(27, obj.getN3());
				ps.setLong(28, obj.getN4());
				ps.setLong(29, obj.getN5());
				ps.setLong(30, obj.getN6());
				ps.setLong(31, obj.getN7());
				ps.setLong(32, obj.getN8());
				ps.setLong(33, obj.getN9());
				ps.setLong(34, obj.getN10());
				ps.setLong(35, obj.getN11());
				ps.setLong(36, obj.getN12());
				ps.setLong(37, obj.getN13());
				if (ps.executeUpdate() > 0) {
					res.setState(true);
					res.setMsgDesc("Updated Successfully!");
				} else {
					res.setMsgDesc("No Such Article to Update!");
				}
			}
		} catch (SQLException e) {

		}

		return res;
	}

	public ArticleDataSet updateCommentLike(ArticleData obj, Connection conn) {
		String recordStatus = "";
		String qry = "";
		Boolean res = false;
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		ArrayList<RegisterData> reglist = new ArrayList<RegisterData>();
		ArticleDataSet dataset = new ArticleDataSet();
		try {
			String sql = "select * from fmr017 where n1=" + obj.getN1() + " and n2=" + obj.getN2();
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				recordStatus = rs.getString("recordStatus");
			}

			if (Long.parseLong(recordStatus) == 1) {
				qry = "update fmr017 set recordstatus=4 where n1 = " + obj.getN1() + " and n2=" + obj.getN2();
			} else {
				qry = "update fmr017 set recordstatus=1 where n1 = " + obj.getN1() + " and n2=" + obj.getN2();
			}
			PreparedStatement stmt = conn.prepareStatement(qry);

			if (stmt.executeUpdate() > 0) {
				res = true;
			}
			// update_is_success
			if (res) {
				qry = "select * from fmr017 where n1=" + obj.getN1() + " and n2=" + obj.getN2();
				PreparedStatement ps1 = conn.prepareStatement(sql);
				ResultSet rs1 = ps1.executeQuery();
				while (rs1.next()) {
					obj.setSyskey(rs1.getLong("syskey"));
					obj.setAutokey(rs1.getLong("autokey"));
					obj.setCreatedDate(rs1.getString("createddate"));
					obj.setCreatedTime(rs1.getString("createdtime"));
					obj.setModifiedDate(rs1.getString("modifieddate"));
					obj.setModifiedTime(rs1.getString("modifiedtime"));
					// obj.setRecordStatus(rs1.getInt("recordStatus"));
					obj.setN1(rs1.getLong("n1"));
					obj.setN2(rs1.getLong("n2"));
					obj.setN3(getCommentLikeCount(obj.getN1(), obj.getN2(), conn));
					reglist = RegisterMobileDao.readCommentLikePerson(obj.getN2(), conn);
					RegisterData[] regarr = new RegisterData[reglist.size()];
					regarr = reglist.toArray(regarr);
					obj.setPerson(regarr);
					datalist.add(obj);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (datalist.size() > 0) {
			ArticleData[] dataArr = new ArticleData[datalist.size()];
			dataArr = datalist.toArray(dataArr);
			dataset.setData(dataArr);
			dataset.setState(true);
		}
		return dataset;
	}

	public ResultMobile updateCommentReply(ArticleData obj, Connection conn) throws SQLException {
		ResultMobile res = new ResultMobile();
		String query = "UPDATE FMR018 SET [syskey]=?, [createddate]=?,[createdtime]=?, [modifieddate]=?,[modifiedtime]=?, [userid]=?, [username]=?, "
				+ " [recordstatus]=?, [syncstatus]=?, [syncbatch]=?,[usersyskey]=?, [t1]=?, [t2]=?, [t3]=?,[t4]=?, [t5]=?,"
				+ " [t6]=?, [t7]=?,[t8]=?, [t9]=?,[t10]=?, [t11]=?,[t12]=?, [t13]=?, [n1]=?, [n2]=?, [n3]=?,"
				+ " [n4]=?, [n5]=?,[n6]=?, [n7]=?,[n8]=?, [n9]=?,[n10]=?, [n11]=?,[n12]=?, [n13]=?"
				+ " WHERE RecordStatus=1 AND Syskey=" + obj.getSyskey() + "";

		try {
			if (isCodeExist(obj, conn)) {
				PreparedStatement ps = conn.prepareStatement(query);
				ps.setLong(1, obj.getSyskey());
				ps.setString(2, obj.getCreatedDate());
				ps.setString(3, obj.getCreatedTime());
				ps.setString(4, obj.getModifiedDate());
				ps.setString(5, obj.getModifiedTime());
				ps.setString(6, obj.getUserId());
				ps.setString(7, obj.getUserName());
				ps.setInt(8, obj.getRecordStatus());
				ps.setInt(9, obj.getSyncStatus());
				ps.setLong(10, obj.getSyncBatch());
				ps.setLong(11, obj.getUserSyskey());
				ps.setString(12, obj.getT1());
				ps.setString(13, obj.getT2());
				ps.setString(14, obj.getT3());
				ps.setString(15, obj.getT4());
				ps.setString(16, obj.getT5());
				ps.setString(17, obj.getT6());
				ps.setString(18, obj.getT7());
				ps.setString(19, obj.getT8());
				ps.setString(20, obj.getT9());
				ps.setString(21, obj.getT10());
				ps.setString(22, obj.getT11());
				ps.setString(23, obj.getT12());
				ps.setString(24, obj.getT13());
				ps.setLong(25, obj.getN1());
				ps.setLong(26, obj.getN2());
				ps.setLong(27, obj.getN3());
				ps.setLong(28, obj.getN4());
				ps.setLong(29, obj.getN5());
				ps.setLong(30, obj.getN6());
				ps.setLong(31, obj.getN7());
				ps.setLong(32, obj.getN8());
				ps.setLong(33, obj.getN9());
				ps.setLong(34, obj.getN10());
				ps.setLong(35, obj.getN11());
				ps.setLong(36, obj.getN12());
				ps.setLong(37, obj.getN13());
				if (ps.executeUpdate() > 0) {
					res.setState(true);
					res.setMsgDesc("Updated Successfully!");
				} else {
					res.setMsgDesc("No Such Article to Update!");
				}
			}
		} catch (SQLException e) {

		}

		return res;
	}

	public ResultMobile updateLikeCount(Long syskey, Connection conn) throws SQLException {
		ResultMobile res = new ResultMobile();
		String sql = "Update FMR003 SET n2 = n2+1 WHERE syskey = ?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
		}
		return res;
	}

	/* updateLikeCountComment */
	public ResultMobile updateLikeCountComment(Long postkey, Long comkey, Long userSK, String type, Connection conn,
			MrBean user) throws SQLException {
		ResultMobile res = new ResultMobile();
		int rs = 0;
		RegisterData userData = RegisterMobileDao.readData(userSK, conn);
		if (IsExitUser(comkey, userSK, conn)) {
			String sql = " Update FMR003 SET n2 = n2+1 WHERE syskey = ? and n5 =? ";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, comkey);
			stmt.setLong(2, userSK);
			rs = stmt.executeUpdate();

		} else {
			String sql = " Update FMR003 SET n2 = n2+1 WHERE syskey = ? ";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, comkey);
			rs = stmt.executeUpdate();
		}
		if (!isLikeOrUnLikeExist(postkey, comkey, userSK, conn)) {
			if (rs > 0) {
				res.setState(true);
				String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
				long key = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
				String query = "INSERT INTO FMR016 (syskey,createddate, modifieddate, userid, username, RecordStatus, SyncStatus, SyncBatch, usersyskey,T1, T2,T3, T4,T5, T6,T7, T8,T9, T10,T11, T12,T13, T14,T15, n1,n2,n3, n4,n5, n6,n7, n8,n9, n10,n11, n12,n13, n14,n15) "
						+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				PreparedStatement psmt = conn.prepareStatement(query);

				psmt.setLong(1, key);
				psmt.setString(2, todayDate);
				psmt.setString(3, todayDate);
				psmt.setString(4, userData.getT1());
				psmt.setString(5, userData.getT3());
				psmt.setInt(6, 1);
				psmt.setInt(7, 1);
				psmt.setLong(8, 0);
				psmt.setLong(9, 0);
				psmt.setString(10, userData.getT1());
				psmt.setString(11, userData.getT3());
				psmt.setString(12, type);
				psmt.setString(13, "0");
				psmt.setString(14, "0");
				psmt.setString(15, "0");
				psmt.setString(16, "0");
				psmt.setString(17, "0");
				psmt.setString(18, "0");
				psmt.setString(19, "0");
				psmt.setString(20, "0");
				psmt.setString(21, "0");
				psmt.setString(22, "0");
				psmt.setString(23, "0");
				psmt.setString(24, "0");
				psmt.setLong(25, postkey);
				psmt.setLong(26, userSK);
				psmt.setLong(27, comkey);
				psmt.setLong(28, 0);
				psmt.setLong(29, 0);
				psmt.setLong(30, 0);
				psmt.setLong(31, 0);
				psmt.setLong(32, 0);
				psmt.setLong(33, 0);
				psmt.setLong(34, 0);
				psmt.setLong(35, 0);
				psmt.setLong(36, 0);
				psmt.setLong(37, 0);
				psmt.setLong(38, 0);
				psmt.setLong(39, 0);

				if (psmt.executeUpdate() > 0) {
					res.setState(true);
				} else {
					res.setState(false);
				}
			}
		} else {
			String sqlUpdate = " update FMR016 set recordstatus = 1 where n1 = " + postkey + " AND n2 = " + userSK
					+ " AND n3 = " + comkey + " ";
			PreparedStatement stmtUpdate = conn.prepareStatement(sqlUpdate);
			if (stmtUpdate.executeUpdate() > 0) {
				res.setState(true);
			} else {
				res.setState(false);
			}
		}
		res.setKeyResult(getTotalLikeOrUnlikeCount(postkey, 1, conn));
		return res;
	}

	public ResultMobile updateUnlikeCount(Long syskey, Connection conn) throws SQLException {
		ResultMobile res = new ResultMobile();
		String sql = "Update FMR003 SET n4 = n4+1 WHERE syskey = ?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
		}
		return res;
	}

	/* updateUnLikeCountComment */
	public ResultMobile updateUnLikeCountComment(Long postkey, Long comkey, Long userSK, String type, Connection conn,
			MrBean user) throws SQLException {
		long key = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
		ResultMobile res = new ResultMobile();
		int rs = 0;
		RegisterData userData = RegisterMobileDao.readData(userSK, conn);

		if (IsExitUser(comkey, userSK, conn)) {
			String sql = " update fmr003 set  n2 = (case when (n2-1)<0 then 0 else (n2-1) end ) where recordstatus <> 4 and syskey = '"
					+ comkey + "' and n5 ='" + userSK + "' ";
			PreparedStatement stmt = conn.prepareStatement(sql);
			rs = stmt.executeUpdate();
		} else {
			String sql = " update fmr003 set  n2 = (case when (n2-1)<0 then 0 else (n2-1) end ) where recordstatus <> 4 and syskey = '"
					+ comkey + "' ";
			PreparedStatement stmt = conn.prepareStatement(sql);
			rs = stmt.executeUpdate();
		}
		if (!isLikeOrUnLikeExist(postkey, comkey, userSK, conn)) {
			if (rs > 0) {
				res.setState(true);
				String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
				String query = "INSERT INTO FMR016 (syskey,createddate, modifieddate, userid, username, RecordStatus, SyncStatus, SyncBatch, usersyskey,T1, T2,T3, T4,T5, T6,T7, T8,T9, T10,T11, T12,T13, T14,T15, n1,n2,n3, n4,n5, n6,n7, n8,n9, n10,n11, n12,n13, n14,n15) "
						+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				PreparedStatement psmt = conn.prepareStatement(query);
				psmt.setLong(1, key);
				psmt.setString(2, todayDate);
				psmt.setString(3, todayDate);
				psmt.setString(4, userData.getT1());
				psmt.setString(5, userData.getT3());
				psmt.setInt(6, 4);
				psmt.setInt(7, 1);
				psmt.setLong(8, 0);
				psmt.setLong(9, 0);
				psmt.setString(10, userData.getT1());
				psmt.setString(11, userData.getT3());
				psmt.setString(12, type);
				psmt.setString(13, "0");
				psmt.setString(14, "0");
				psmt.setString(15, "0");
				psmt.setString(16, "0");
				psmt.setString(17, "0");
				psmt.setString(18, "0");
				psmt.setString(19, "0");
				psmt.setString(20, "0");
				psmt.setString(21, "0");
				psmt.setString(22, "0");
				psmt.setString(23, "0");
				psmt.setString(24, "0");
				psmt.setLong(25, postkey);
				psmt.setLong(26, userSK);
				psmt.setLong(27, comkey);
				psmt.setLong(28, 0);
				psmt.setLong(29, 0);
				psmt.setLong(30, 0);
				psmt.setLong(31, 0);
				psmt.setLong(32, 0);
				psmt.setLong(33, 0);
				psmt.setLong(34, 0);
				psmt.setLong(35, 0);
				psmt.setLong(36, 0);
				psmt.setLong(37, 0);
				psmt.setLong(38, 0);
				psmt.setLong(39, 0);
				if (psmt.executeUpdate() > 0) {
					res.setState(true);
				} else {
					res.setState(false);
				}
			}
		} else {
			String sqlUpdate = " update FMR016 set recordstatus = 4 where n1 = " + postkey + " AND n2 = " + userSK
					+ " AND n3 = " + comkey + " ";
			PreparedStatement stmtUpdate = conn.prepareStatement(sqlUpdate);
			if (stmtUpdate.executeUpdate() > 0) {
				res.setState(true);
			} else {
				res.setState(false);
			}
		}
		res.setKeyResult(getTotalLikeOrUnlikeCount(postkey, 4, conn));
		return res;
	}

}
