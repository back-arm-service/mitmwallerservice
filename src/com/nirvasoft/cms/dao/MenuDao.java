package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.MenuData;
import com.nirvasoft.cms.shared.MenuRole;
import com.nirvasoft.cms.shared.MenuViewData;
import com.nirvasoft.cms.shared.MenuViewDataset;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.cms.shared.ValueCaptionData;
import com.nirvasoft.cms.shared.ValueCaptionDataSet;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;

public class MenuDao {
	public static boolean canDelete(long key, Connection conn) throws SQLException {

		ArrayList<Long> dbrs = new ArrayList<Long>();
		String sql = "Select n1 From UVM023 Where RecordStatus<>4 AND n2=" + key + " AND n1<>1";
		PreparedStatement stat = conn.prepareStatement(sql);
		ResultSet result = stat.executeQuery();
		while (result.next()) {
			dbrs.add(result.getLong("n1"));
		}
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean canDeleteParent(long key, Connection conn) throws SQLException {

		ArrayList<Long> dbrs = new ArrayList<Long>();
		String sql = "Select n1 From UVM022 Where  RecordStatus<>4 AND n2=" + key;

		PreparedStatement stat = conn.prepareStatement(sql);
		ResultSet result = stat.executeQuery();
		while (result.next()) {
			dbrs.add(result.getLong("n1"));
		}
		if (dbrs.size() > 0) {
			return false;
		} else {
			return true;
		}
	}

	public static DBRecord define() {

		DBRecord ret = new DBRecord();
		ret.setTableName("UVM022");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("CreatedDate", (byte) 5));
		ret.getFields().add(new DBField("ModifiedDate", (byte) 5));
		ret.getFields().add(new DBField("UserId", (byte) 5));
		ret.getFields().add(new DBField("UserName", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 1));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 1));
		ret.getFields().add(new DBField("n5", (byte) 1));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 1));

		return ret;
	}

	public static DBRecord define(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("CreatedDate", (byte) 5));
		ret.getFields().add(new DBField("ModifiedDate", (byte) 5));
		ret.getFields().add(new DBField("UserId", (byte) 5));
		ret.getFields().add(new DBField("UserName", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 1));
		ret.getFields().add(new DBField("n5", (byte) 1));
		ret.getFields().add(new DBField("n6", (byte) 2));

		return ret;
	}

	public static DBRecord define23() {
		DBRecord ret = new DBRecord();
		ret.setTableName("UVM023");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("autokey", (byte) 2));
		ret.getFields().add(new DBField("CreatedDate", (byte) 5));
		ret.getFields().add(new DBField("ModifiedDate", (byte) 5));
		ret.getFields().add(new DBField("UserId", (byte) 5));
		ret.getFields().add(new DBField("UserName", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 1));
		ret.getFields().add(new DBField("n5", (byte) 1));
		ret.getFields().add(new DBField("n6", (byte) 1));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));

		return ret;
	}

	public static DBRecord defineView() {
		DBRecord ret = new DBRecord();
		ret.setTableName("View_Menu");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("ParentMenu", (byte) 5));
		return ret;
	}

	public static Resultb2b delete(long syskey, long n2, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();

		if (canDelete(syskey, conn)) {
			boolean ischild = true;
			if (n2 == 0) {
				ischild = canDeleteParent(syskey, conn);
			}

			if (ischild != false) {

				String sql = "UPDATE UVM022 SET RecordStatus=4 WHERE syskey=?";
				PreparedStatement stmt = conn.prepareStatement(sql);
				stmt.setLong(1, syskey);
				int rs = stmt.executeUpdate();
				if (rs > 0) {
					String sql23 = "UPDATE UVM023 SET RecordStatus=4 WHERE n2=?";
					PreparedStatement stmt23 = conn.prepareStatement(sql23);
					stmt23.setLong(1, syskey);
					int rss = stmt23.executeUpdate();
					if (rss > 0) {
						res.setState(true);
						res.setMsgDesc("Deleted Successfully!");
					} else {
						res.setState(false);
						res.setMsgDesc("Delete Fail!");
						return res;

					}
				} else {
					res.setState(false);
					res.setMsgDesc("Delete Fail!");
					return res;
				}
			} else {

				res.setState(false);
				res.setMsgDesc("Can't Delete!");
				return res;
			}
		} else {
			res.setState(false);
			res.setMsgDesc("Can't Delete");
			return res;
		}
		return res;
	}

	public static MenuViewDataset getAllMenuData(MrBean user, Connection conn) throws SQLException {

		ArrayList<MenuViewData> ret = new ArrayList<MenuViewData>();

		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(defineView(), "", " ORDER BY syskey", conn);

		for (int i = 0; i < dbrs.size(); i++) {
			ret.add(getDBViewRecord(dbrs.get(i)));
		}

		MenuViewDataset dataSet = new MenuViewDataset();
		MenuViewData[] dataarry = new MenuViewData[ret.size()];
		dataarry = ret.toArray(dataarry);
		dataSet.setData(dataarry);

		return dataSet;
	}

	public static MenuData getDBRecord(DBRecord adbr) {
		MenuData ret = new MenuData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("CreatedDate"));
		ret.setModifiedDate(adbr.getString("ModifiedDate"));
		ret.setUserId(adbr.getString("UserId"));
		ret.setUserName(adbr.getString("UserName"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));

		ret.setUsersyskey(adbr.getLong("usersysKey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setN1(adbr.getInt("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getInt("n4"));
		ret.setN5(adbr.getInt("n5"));
		ret.setN6(adbr.getLong("n6"));
		ret.setN7(adbr.getInt("n7"));
		return ret;
	}

	public static MenuRole getDBRecord23(DBRecord adbr) {
		MenuRole ret = new MenuRole();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("CreatedDate"));
		ret.setModifiedDate(adbr.getString("ModifiedDate"));
		ret.setUserId(adbr.getString("UserId"));
		ret.setUserName(adbr.getString("UserName"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getInt("n4"));
		ret.setN5(adbr.getInt("n5"));
		ret.setN6(adbr.getInt("n6"));
		ret.setUsersyskey(adbr.getLong("usersysKey"));

		return ret;
	}

	public static MenuViewData getDBViewRecord(DBRecord adbr) {
		MenuViewData ret = new MenuViewData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setN2(adbr.getLong("n2"));
		ret.setParentMenu(adbr.getString("ParentMenu"));

		return ret;
	}

	public static ValueCaptionDataSet getmainmenulist(Connection conn) throws SQLException {
		ValueCaptionDataSet result = new ValueCaptionDataSet();
		ArrayList<ValueCaptionData> datalist = new ArrayList<ValueCaptionData>();

		String sql = "SELECT syskey,t2 FROM UVM022  WHERE RecordStatus<>4 AND n2=0 order by SyncBatch";
		PreparedStatement stat = conn.prepareStatement(sql);
		ResultSet res = stat.executeQuery();
		while (res.next()) {
			ValueCaptionData combo = new ValueCaptionData();
			combo.setValue(String.valueOf(res.getLong("syskey")));
			combo.setCaption(res.getString("t2"));
			datalist.add(combo);
			// System.out.println(combo.getValue() + " " + combo.getCaption());
		}
		ValueCaptionData[] dataarry = new ValueCaptionData[datalist.size()];
		for (int i = 0; i < datalist.size(); i++) {
			dataarry[i] = datalist.get(i);
		}
		result.setData(dataarry);
		return result;
	}

	public static int getMenuCount(String searchVal, Connection conn) throws SQLException {
		String whereclause = " WHERE RecordStatus<>4 ";
		if (!searchVal.equals("")) {

			whereclause += "AND t1 LIKE '%" + searchVal + "%' OR t2 LIKE '%" + searchVal + "%' " + " OR syskey LIKE '%"
					+ searchVal + "%'";
		}
		int res = 1;
		PreparedStatement stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM UVM022" + whereclause);
		ResultSet result = stat.executeQuery();
		result.next();
		res = result.getInt("recCount");
		return res;
	}

	public static Resultb2b insert(MenuData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();

		if (!isMenu(obj, conn)) {
			String sql = DBMgr.insertString(define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			stmt.executeUpdate();

			res.setMsgDesc("Saved Successfully!");
			res.setState(true);
			return res;
		} else {
			res.setMsgDesc("Code already exist!");
			res.setState(false);
			return res;
		}

	}

	public static Resultb2b insertMenuRole(MenuRole obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();

		String sql = DBMgr.insertString(define("UVM023"), conn);
		PreparedStatement stmt = conn.prepareStatement(sql);
		DBRecord dbr = setDBRecord(obj);
		DBMgr.setValues(stmt, dbr);
		stmt.executeUpdate();
		res.setState(true);
		res.setMsgDesc("Saved Successfully!");
		return res;
	}

	public static boolean isChildMenuExist(MenuData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				" WHERE RecordStatus<>4 AND  T1 LIKE '" + obj.getT1() + "' AND n2= " + obj.getN2(), "", conn);
		// " WHERE RecordStatus<>4 AND n2= "+obj.getN2(), "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isCodeExist(MenuData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), " WHERE RecordStatus<>4 AND syskey =" + obj.getSyskey(),
				"", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isLinkExist(MenuData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				" WHERE RecordStatus<>4 AND syskey <> " + obj.getSyskey() + " AND T1=\'" + obj.getT1() + "\'", "",
				conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isMenu(MenuData obj, Connection conn) throws SQLException {
		boolean flag = false;
		if (obj.getN2() == 0) {
			flag = isCodeExist(obj, conn);
		} else if (obj.getN2() != 0) {
			flag = isChildMenuExist(obj, conn);
		}
		return flag;
	}

	public static MenuData read(long syskey, Connection conn) throws SQLException {
		MenuData ret = new MenuData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "WHERE RecordStatus<>4 AND syskey=" + syskey, "", conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));
		return ret;
	}

	public static MenuViewDataset searchMenu(PagerData pager, String searchVal, Connection conn) throws SQLException {
		MenuViewDataset res = new MenuViewDataset();
		ArrayList<MenuViewData> datalist = new ArrayList<MenuViewData>();
		String whereclause = " ";
		if (!searchVal.equals("")) {
			whereclause += "WHERE t1 LIKE '%" + searchVal + "%' OR t2 LIKE '%" + searchVal + "%' "
					+ "OR  syskey LIKE '%" + searchVal + "%'";
		}
		String sql = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey) AS RowNum,* FROM View_Menu"
				+ whereclause + " ) AS RowConstrainedResult  WHERE RowNum >= " + pager.getStart() + " and RowNum <= "
				+ pager.getEnd();
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rset = stmt.executeQuery();

		while (rset.next()) {
			MenuViewData ret = new MenuViewData();
			ret.setSyskey(rset.getLong("syskey"));
			ret.setT1(rset.getString("t1"));
			ret.setT2(rset.getString("t2"));
			ret.setT5(rset.getString("t5"));
			ret.setT6(rset.getString("t6"));
			ret.setN2(rset.getLong("n2"));
			ret.setParentMenu(rset.getString("ParentMenu"));
			datalist.add(ret);
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		PreparedStatement stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM  View_Menu" + whereclause);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		res.setPageSize(pager.getSize());
		res.setCurrentPage(pager.getCurrent());
		MenuViewData[] dataarry = new MenuViewData[datalist.size()];
		dataarry = datalist.toArray(dataarry);
		res.setData(dataarry);
		return res;
	}

	public static DBRecord setDBRecord(MenuData data) {
		DBRecord ret = define();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("CreatedDate", data.getCreatedDate());
		ret.setValue("ModifiedDate", data.getModifiedDate());
		ret.setValue("UserId", data.getUserId());
		ret.setValue("UserName", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());

		ret.setValue("usersysKey", data.getUsersyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t5", data.getT5());
		ret.setValue("t6", data.getT6());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		ret.setValue("n6", data.getN6());
		ret.setValue("n7", data.getN7());

		return ret;
	}

	public static DBRecord setDBRecord(MenuRole data) {
		DBRecord ret = define("UVM023");
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("CreatedDate", data.getCreatedDate());
		ret.setValue("ModifiedDate", data.getModifiedDate());
		ret.setValue("UserId", data.getUserId());
		ret.setValue("UserName", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());

		ret.setValue("usersysKey", data.getUsersyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		ret.setValue("n6", data.getN6());

		return ret;
	}

	public static Resultb2b update(MenuData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();

		if (isMenu(obj, conn)) {
			String sql = DBMgr.updateString(" WHERE RecordStatus<>4 AND Syskey=" + obj.getSyskey(), define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			stmt.executeUpdate();
			res.setState(true);
			res.setMsgDesc("Updated Successfully!");
			return res;
		} else {
			res.setMsgDesc("No Such Menu to Update!");
			res.setState(false);
			return res;
		}
	}

}