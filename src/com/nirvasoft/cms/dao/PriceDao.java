package com.nirvasoft.cms.dao;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.DivisionComboData;
import com.nirvasoft.cms.shared.DivisionComboDataSet;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.cms.shared.PriceData;
import com.nirvasoft.cms.shared.PriceDataSet;
import com.nirvasoft.cms.shared.PriceListData;
import com.nirvasoft.cms.shared.PriceListDataSet;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;

import jxl.Cell;
import jxl.CellType;
import jxl.DateCell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class PriceDao {
	public static DBRecord define() { // Define Database fields
		DBRecord ret = new DBRecord();
		ret.setTableName("FMR009"); // Table Name
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("T1", (byte) 5));
		ret.getFields().add(new DBField("T2", (byte) 5));
		ret.getFields().add(new DBField("T3", (byte) 5));
		ret.getFields().add(new DBField("T4", (byte) 5));
		ret.getFields().add(new DBField("T5", (byte) 5));
		ret.getFields().add(new DBField("T6", (byte) 5));
		ret.getFields().add(new DBField("T7", (byte) 5));
		ret.getFields().add(new DBField("T8", (byte) 5));
		ret.getFields().add(new DBField("T9", (byte) 5));
		ret.getFields().add(new DBField("T10", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		return ret;
	}

	public static Resultb2b delete(Long syskey, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = "UPDATE FMR009 SET RecordStatus=4 WHERE RecordStatus = 1 AND syskey=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Successfully");
		} else {
			res.setMsgDesc("Deleting Unsuccessful");
		}
		return res;
	}

	public static Resultb2b deletePriceBatch(PriceData p, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = "Delete From FMR009 where RecordStatus<>4 and N1=" + p.getN1();
		PreparedStatement stmt = conn.prepareStatement(sql);
		// stmt.setLong(1, p.getN1());
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Successfully");
		} else {
			res.setMsgDesc("Deleting Unsuccessful");
		}
		return res;
	}

	public static DivisionComboDataSet getBatchList(Connection conn) throws SQLException {
		DivisionComboDataSet dataset = new DivisionComboDataSet();
		ArrayList<DivisionComboData> datalist = new ArrayList<DivisionComboData>();
		DivisionComboData combo;
		String sql = "  select distinct n1 from FMR009 WHERE  RECORDSTATUS<>4 order by n1";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();

		while (res.next()) {
			combo = new DivisionComboData();
			combo.setCaption(res.getString("n1"));
			combo.setValue(res.getString("n1"));
			combo.setFlag(false);
			datalist.add(combo);
		}
		DivisionComboData[] dataarray = new DivisionComboData[datalist.size()];
		dataarray = datalist.toArray(dataarray);

		dataset.setData(dataarray);
		return dataset;
	}

	private static Map<String, Integer> getColumnIndexesForPriceList(String filename, String sheetNo)
			throws BiffException, IOException {
		File inputWorkbook = new File(filename);
		Map<String, Integer> map = new HashMap<String, Integer>();
		Workbook workbook;
		workbook = Workbook.getWorkbook(inputWorkbook);
		Sheet sheet = workbook.getSheet(sheetNo);
		map.put("သီးနှံအမည်", -1);
		map.put("�?ိုင်း", -1);
		map.put("မြို့နယ်", -1);
		map.put("​နေ့စွဲ", -1);
		map.put("​စျေးနှုန်း", -1);
		map.put("မြန်မာကျပ်", -1);
		map.put("အရေအ�?ွက်", -1);
		map.put("​ရေ�?ွက်ပုံ", -1);
		for (int i = 0; i < sheet.getColumns(); i++) {
			String columnNames = sheet.getCell(i, 0).getContents();
			if (columnNames.equals("သီးနှံအမည်")) {
				map.put("သီးနှံအမည်", i);
			} else if (columnNames.equals("�?ိုင်း")) {
				map.put("�?ိုင်း", i);
			} else if (columnNames.equals("မြို့နယ်")) {
				map.put("မြို့နယ်", i);
			} else if (columnNames.equals("​နေ့စွဲ")) {
				map.put("​နေ့စွဲ", i);
			} else if (columnNames.equalsIgnoreCase("​စျေးနှုန်း")) {
				map.put("​စျေးနှုန်း", i);
			} else if (columnNames.equalsIgnoreCase("မြန်မာကျပ်")) {
				map.put("မြန်မာကျပ်", i);
			} else if (columnNames.equalsIgnoreCase("အရေအ�?ွက်")) {
				map.put("အရေအ�?ွက်", i);
			} else if (columnNames.equalsIgnoreCase("​ရေ�?ွက်ပုံ")) {
				map.put("​ရေ�?ွက်ပုံ", i);
			}
		}
		return map;
	}

	private static String getCriString(String pSearchText) {
		String l_Cri = "";
		if (!pSearchText.equals("")) {
			l_Cri = "  and (t1 like '%" + pSearchText + "%' or " + " t2 like '%" + pSearchText.replace(" ", "")
					+ "%'  or" + " t4 like '%" + pSearchText.replace(" ", "") + "%'";
			String date = pSearchText.replace("/", "");
			if (ServerUtil.isDate(date)) {
				l_Cri += " or per.t3 = '" + ServerUtil.datetoString(pSearchText) + "'";
			}
			l_Cri += ")";
		}
		return l_Cri;
	}
	/*
	 * public static PriceListDataSet getPriceListData(PagerData pgdata,String
	 * searchVal, Connection conn) throws SQLException { String whereclause =
	 * ""; String sql = ""; String whereclausesql=""; PriceListDataSet dataset =
	 * new PriceListDataSet(); ArrayList<PriceListData> datalist = new
	 * ArrayList<PriceListData>(); PriceData price ; ArrayList<PriceData>
	 * priceList = new ArrayList<PriceData>(); sql =
	 * "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY t2) AS RowNum,* FROM "
	 * +
	 * " ( SELECT  t5,t3,t2,t1,n1 FROM FMR009 WHERE n1 IN ( SELECT max(n1) FROM FMR009 ) and recordstatus <> 4 "
	 * + getCriString(searchVal)+" ) b ) AS RowConstrainedResult " +
	 * " WHERE RowNum >= "+pgdata.getStart()+" AND RowNum <= "+pgdata.getEnd()+
	 * " "; whereclausesql =
	 * "  (SELECT  t5,t3,t2,t1,n1 FROM FMR009 WHERE n1 IN ( SELECT max(n1) FROM FMR009 )  and recordstatus <> 4 "
	 * + getCriString(searchVal)+" ) as b "; PreparedStatement st =
	 * conn.prepareStatement(sql); ResultSet rs = st.executeQuery();
	 * while(rs.next()){ price = new PriceData();
	 * price.setT5(rs.getString("t5")); price.setT3(rs.getString("t3"));
	 * price.setT2(rs.getString("t2")); price.setT1(rs.getString("t1"));
	 * price.setN1(rs.getLong("n1")); priceList.add(price);
	 * dataset.setState(true); } for(int i=0; i<priceList.size(); i++){
	 * whereclause = " WHERE  t2 = '"+priceList.get(i).getT2()+"' and  t1 = '"
	 * +priceList.get(i).getT1()+"' and t5 = '"+priceList.get(i).getT5()+
	 * "' and (t3 = '"+priceList.get(i).getT3()+"' " +
	 * " OR t3 IN (select convert(nvarchar(8), dateadd(day,-1, '"
	 * +priceList.get(i).getT3()+"'),112)  )) ";
	 * 
	 * //whereclause +=
	 * "OR n1 IN ( SELECT max(n1-1) FROM FMR009 where t3 IN (select convert(nvarchar(8), dateadd(day,-1, '"
	 * +priceList.get(i).getT3()+"'),112) ) )  "; ArrayList<DBRecord> dbrs =
	 * DBMgr.getDBRecords(define(), whereclause, "", conn);
	 * 
	 * PriceListData priceListData = new PriceListData(); PriceData [] pData =
	 * new PriceData [dbrs.size()]; int res =0;//(-) int res1 =0;//(+) for (int
	 * k = 0; k < dbrs.size(); k++) { pData[k] = getDBRecord(dbrs.get(k)); }
	 * priceListData.setData(pData);
	 * priceListData.setGroupName(pData[0].getT1());;
	 * datalist.add(priceListData); } dataset.setPageSize(pgdata.getSize());
	 * dataset.setCurrentPage(pgdata.getCurrent()); if (datalist.size() > 0) {
	 * dataset.setState(true); } else { dataset.setState(false); }
	 * PreparedStatement stat = conn.prepareStatement(
	 * "SELECT COUNT(*) AS recCount FROM " + whereclausesql); ResultSet result =
	 * stat.executeQuery(); result.next();
	 * dataset.setTotalCount(result.getInt("recCount")); PriceListData[]
	 * dataarray = new PriceListData[datalist.size()]; dataarray =
	 * datalist.toArray(dataarray); dataset.setData(dataarray); return dataset;
	 * }
	 */

	public static PriceData getDBRecord(DBRecord adbr) {
		PriceData ret = new PriceData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreateddate(adbr.getString("createddate"));
		ret.setModifieddate(adbr.getString("modifieddate"));
		ret.setUserid(adbr.getString("userid"));
		ret.setUsername(adbr.getString("username"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setUsersyskey(adbr.getLong("usersyskey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		ret.setT8(adbr.getString("t8"));
		ret.setT9(adbr.getString("t9"));
		ret.setT10(adbr.getString("t10"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getLong("n5"));
		ret.setN6(adbr.getLong("n6"));
		ret.setN7(adbr.getLong("n7"));
		ret.setN8(adbr.getLong("n8"));
		ret.setN9(adbr.getLong("n9"));
		ret.setN10(adbr.getLong("n10"));
		return ret;
	}

	public static long getMaxBatch(Connection aConnection) {
		long ret = 0;
		String l_Query = "select ISNULL(MAX(n1) + 1, 1) as maxSys FROM fmr009";
		Statement stmt;
		try {
			stmt = aConnection.createStatement();
			ResultSet rs = stmt.executeQuery(l_Query);
			if (rs.next()) {
				ret = rs.getLong("maxSys");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ret;
	}

	public static PriceListDataSet getPricelists(PagerData pgdata, String searchVal, String type, Connection conn)
			throws SQLException {
		String whereclause = "";
		String sql = "";
		String whereclausesql = "";
		PriceListDataSet dataset = new PriceListDataSet();
		ArrayList<PriceListData> datalist = new ArrayList<PriceListData>();
		PriceData price;
		ArrayList<PriceData> priceList = new ArrayList<PriceData>();
		if (type.equals("area")) {
			sql = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY t2) AS RowNum,* FROM "
					+ " ( SELECT   t2 FROM FMR009 WHERE n1 IN ( SELECT max(n1) FROM FMR009 ) and recordstatus <> 4 "
					+ getCriString(searchVal) + " GROUP BY t2 ) b ) AS RowConstrainedResult " + " WHERE RowNum >= "
					+ pgdata.getStart() + " AND RowNum <= " + pgdata.getEnd() + " ";
			whereclausesql = "  (SELECT   t2 FROM FMR009 WHERE n1 IN ( SELECT max(n1) FROM FMR009 ) and recordstatus <> 4 "
					+ getCriString(searchVal) + "  GROUP BY t2 ) as b ";
			PreparedStatement st = conn.prepareStatement(sql);
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				price = new PriceData();

				price.setT2(rs.getString("t2"));
				dataset.setState(true);
				priceList.add(price);
			}
		}
		if (type.equals("crop")) {
			sql = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY t1) AS RowNum,* FROM "
					+ " ( SELECT   t1 FROM FMR009 WHERE n1 IN ( SELECT max(n1) FROM FMR009 ) and recordstatus <> 4 "
					+ getCriString(searchVal) + " GROUP BY t1 ) b ) AS RowConstrainedResult " + " WHERE RowNum >= "
					+ pgdata.getStart() + " AND RowNum <= " + pgdata.getEnd() + " ";
			whereclausesql = " (SELECT   t1 FROM FMR009 WHERE n1 IN ( SELECT max(n1) FROM FMR009 ) and recordstatus <> 4 "
					+ getCriString(searchVal) + "  GROUP BY t1 ) as b";
			// sql = " SELECT t1 FROM FMR009 WHERE n1 IN ( SELECT max(n1) FROM
			// FMR009 ) GROUP BY t1 ";
			PreparedStatement st = conn.prepareStatement(sql);
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				price = new PriceData();
				dataset.setState(true);
				price.setT1(rs.getString("t1"));
				priceList.add(price);
			}
		}
		if (type.equals("")) {
			sql = " SELECT  t5,t3 FROM FMR009 WHERE n1 IN ( SELECT max(n1) FROM FMR009 )  ";
			PreparedStatement st = conn.prepareStatement(sql);
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				price = new PriceData();
				price.setT5(rs.getString("t5"));
				price.setT3(rs.getString("t3"));
				dataset.setState(true);
				priceList.add(price);
			}
		}
		for (int i = 0; i < priceList.size(); i++) {
			if (type.equals("")) {
				whereclause = " WHERE n1 IN ( SELECT max(n1) FROM FMR009 ) and  t5 = '" + priceList.get(i).getT5()
						+ "' and (t3 = '" + priceList.get(i).getT3() + "' "
						+ " OR t3 IN (select convert(nvarchar(8), dateadd(day,-1, '" + priceList.get(i).getT3()
						+ "'),112)))";
			}
			if (type.equals("area")) {
				whereclause = " WHERE n1 IN ( SELECT max(n1) FROM FMR009 ) and t2 = '" + priceList.get(i).getT2()
						+ "' ";
			}
			if (type.equals("crop")) {
				whereclause = " WHERE n1 IN ( SELECT max(n1) FROM FMR009 ) and t1 = '" + priceList.get(i).getT1()
						+ "' ";
			}
			ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereclause, "", conn);
			PriceListData priceListData = new PriceListData();
			PriceData[] pData = new PriceData[dbrs.size()];
			for (int j = 0; j < dbrs.size(); j++) {
				pData[j] = getDBRecord(dbrs.get(j));
			}
			for (int k = 0; k < pData.length; k++) {
				String month = ServerUtil.getMonthName(pData[k].getT3());
				pData[k].setT3(ArticleDao.MMddyyyFormat(pData[k].getT3(), month));
			}
			priceListData.setData(pData);
			if (type.equals("area")) {
				priceListData.setGroupName(pData[0].getT2());
			}
			if (type.equals("crop")) {
				priceListData.setGroupName(pData[0].getT1());
			}
			datalist.add(priceListData);
		}
		dataset.setPageSize(pgdata.getSize());
		dataset.setCurrentPage(pgdata.getCurrent());
		if (datalist.size() > 0) {
			dataset.setState(true);
		} else {
			dataset.setState(false);
		}
		PreparedStatement stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM " + whereclausesql);
		ResultSet result = stat.executeQuery();
		result.next();
		dataset.setTotalCount(result.getInt("recCount"));
		PriceListData[] dataarray = new PriceListData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		dataset.setData(dataarray);
		return dataset;
	}

	public static ArrayList<String> getSheetNames(String filename, Connection conn) {
		File inputWorkbook = new File(filename);
		ArrayList<String> sheetNames = new ArrayList<String>();
		Workbook workbook;
		try {
			workbook = Workbook.getWorkbook(inputWorkbook);
			for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
				sheetNames.add(workbook.getSheet(i).getName());
			}
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sheetNames;
	}

	public static Resultb2b insert(PriceData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = DBMgr.insertString(define(), conn);
		PreparedStatement stmt = conn.prepareStatement(sql);
		DBRecord dbr = setDBRecord(obj);
		DBMgr.setValues(stmt, dbr);
		int count = stmt.executeUpdate();
		if (count > 0) {
			res.setState(true);
			res.setMsgDesc("Saved Successfully!");
		}

		return res;
	}

	public static Resultb2b insertPrice(PriceData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		if (!isCodeExist(obj, conn)) {
			String sql = DBMgr.insertString(define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			int count = stmt.executeUpdate();
			if (count > 0) {
				res.setState(true);
				res.setMsgDesc("Saved Successfully!");
			}
		} else {
			res.setMsgDesc("Price Already Exist!");
		}
		return res;
	}

	public static boolean isCodeExist(PriceData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				" where RecordStatus<>4 AND  RecordStatus = 1 AND syskey = " + obj.getSyskey() + " ", "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static String isProduct(String product, Connection conn) {
		String str = "";
		String sql = " SELECT Code FROM CropRef WHERE lower(DespEng) = lower('" + product + "') ";
		PreparedStatement stmt;
		try {
			stmt = conn.prepareStatement(sql);
			ResultSet res = stmt.executeQuery();
			if (res.next()) {
				str = res.getString("Code");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return str;
	}

	public static String isState(String state, Connection conn) {
		String str = "";
		String sql = "select Code from AddressRef where Lower(DespEng) = Lower('" + state + "')";
		PreparedStatement stmt;
		try {
			stmt = conn.prepareStatement(sql);
			ResultSet res = stmt.executeQuery();
			if (res.next()) {
				str = res.getString("Code");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return str;
	}

	public static PriceDataSet previewExcelDataForPriceList(String aFileName, String aSheetNo, int p_pageSize,
			int p_currentPage, Connection l_Conn) {
		int l_ExtIndex = aFileName.lastIndexOf(".");
		String l_FileExt = aFileName.substring(l_ExtIndex);
		PriceDataSet l_GeneralDataSet = new PriceDataSet();
		l_GeneralDataSet.setM_pageSize(p_pageSize);
		l_GeneralDataSet.setM_currentPage(p_currentPage);
		Resultb2b res = new Resultb2b();
		ArrayList<PriceData> l_tmpList = new ArrayList<PriceData>();
		ArrayList<PriceData> l_List = new ArrayList<PriceData>();
		try {
			if (l_FileExt.equalsIgnoreCase(".xls"))
				l_List = readExcelFormat(aFileName, aSheetNo, l_Conn);
			l_GeneralDataSet.setM_totalCount(l_List.size());
			if (p_pageSize != 0) {
				int l_startRecord = 0;
				l_startRecord = (l_GeneralDataSet.getM_currentPage() - 1) * l_GeneralDataSet.getM_pageSize();
				for (int i = l_startRecord; i < l_List.size()
						&& i < (l_GeneralDataSet.getM_pageSize() + l_startRecord); i++) {
					l_tmpList.add(l_List.get(i));
				}
			} else {
				for (int i = 0; i < l_List.size(); i++) {
					l_tmpList.add(l_List.get(i));
				}
			}
			PriceData[] data = new PriceData[l_List.size()];
			for (int i = 0; i < l_tmpList.size(); i++) {
				data[i] = l_tmpList.get(i);
			}
			l_GeneralDataSet.setM_arlData(l_tmpList);
			l_GeneralDataSet.setData(data);
		} catch (Exception e) {
			res.setMsgDesc(e.getMessage());
			e.printStackTrace();
		}
		return l_GeneralDataSet;
	}

	public static PriceData read(long syskey, Connection conn) throws SQLException {
		PriceData ret = new PriceData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				"where RecordStatus<>4 AND  RecordStatus = 1 AND syskey=" + syskey, "", conn);
		if (dbrs.size() > 0) {
			ret = getDBRecord(dbrs.get(0));
		}
		return ret;
	}

	// TDA
	public static Resultb2b readDataExit(PriceData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = "select syskey  from fmr009 where t1=N'" + obj.getT1() + "'and t2=N'" + obj.getT2() + "' and t3='"
				+ obj.getT3() + "' and n1='" + obj.getN1() + "' and T10=N'" + obj.getT10() + "'";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			res.setKeyResult(rs.getLong("syskey"));
			res.setState(true);
		}

		return res;
	}

	private static ArrayList<PriceData> readExcelFormat(String aFilePath, String aSheetNo, Connection l_Conn)
			throws BiffException, IOException {
		File inputWorkbook = new File(aFilePath);
		Workbook w = Workbook.getWorkbook(inputWorkbook);
		ArrayList<PriceData> l_InfoList = new ArrayList<PriceData>();
		ArrayList<PriceData> mImportList = new ArrayList<PriceData>();
		PriceData l_Info = new PriceData();
		Map<String, Integer> columnMap = new HashMap<String, Integer>();
		String dateInStr = "";
		columnMap = getColumnIndexesForPriceList(aFilePath, aSheetNo);
		int column1 = columnMap.get("သီးနှံအမည်");
		int column2 = columnMap.get("�?ိုင်း");
		int column3 = columnMap.get("မြို့နယ်");
		int column4 = columnMap.get("​နေ့စွဲ");
		int column5 = columnMap.get("​စျေးနှုန်း");
		int column6 = columnMap.get("မြန်မာကျပ်");
		int column7 = columnMap.get("အရေအ�?ွက်");
		int column8 = columnMap.get("​ရေ�?ွက်ပုံ");
		String price = "";
		String quantity = "";
		String unitType = "";
		String currency = "";
		String state = "";
		Sheet sheet = w.getSheet(aSheetNo);
		int noOfColumns = sheet.getRow(0).length;
		if (noOfColumns != 8) {
			l_Info.setContainerror(true);
			l_InfoList.add(l_Info);
		} else {
			for (int row = 1; row < sheet.getRows(); row++) {
				l_Info = new PriceData();
				l_Info.setT1(sheet.getCell(column1, row).getContents().trim());// Product
				if (l_Info.getT1().equals(""))
					continue;
				else {
					l_Info.setT2(sheet.getCell(column3, row).getContents().trim());// township
					price = sheet.getCell(column5, row).getContents().trim();// price
					l_Info.setT4(price);
					currency = sheet.getCell(column6, row).getContents().trim();// currency
					l_Info.setT9(currency);
					quantity = sheet.getCell(column7, row).getContents().trim();// quantity
					l_Info.setT7(quantity);
					unitType = sheet.getCell(column8, row).getContents().trim();// unitType
					l_Info.setT8(unitType);
					state = sheet.getCell(column2, row).getContents().trim();// state
					l_Info.setT10(state);
					Cell dateInCell = sheet.getCell(column4, row);// date
					if (dateInCell.getType() == CellType.DATE) {
						DateCell dc1 = (DateCell) dateInCell;
						Date dateIn = dc1.getDate();
						dateInStr = ServerUtil.datetoString(dateIn);
						if (dateInStr.length() == 8) {
							l_Info.setT3(dateInStr.substring(6, 8) + "/" + dateInStr.substring(4, 6) + "/"
									+ dateInStr.substring(0, 4));
						} else
							l_Info.setT3(dateInStr);
					} else {
						l_Info.setT3(sheet.getCell(column4, row).getContents().trim());
					}
					l_Info.setT7(quantity);
					l_Info.setT8(unitType);
					l_Info.setT9(currency);
					l_Info.setT10(state);
					l_InfoList.add(l_Info);
				}
			}
		}
		for (int i = 0; i < l_InfoList.size(); i++) {
			mImportList.add(l_InfoList.get(i));
		}
		return mImportList;
	}

	/*
	 * public static PriceListDataSet getPriceListDatas(PagerData pgdata, String
	 * searchVal, Connection conn)throws SQLException { String whereclause = "";
	 * String sql = ""; String whereclausesql = ""; PriceListDataSet dataset =
	 * new PriceListDataSet(); ArrayList<PriceListData> datalist = new
	 * ArrayList<PriceListData>(); PriceData price; ArrayList<PriceData>
	 * priceList = new ArrayList<PriceData>(); sql =
	 * "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY t2) AS RowNum,* FROM "
	 * +
	 * " ( SELECT  t5,t3,t2,t1,n1 FROM FMR009 WHERE n1 IN ( SELECT max(n1) FROM FMR009 ) and recordstatus <> 4 "
	 * + getCriString(searchVal) + " ) b ) AS RowConstrainedResult " +
	 * " WHERE RowNum >= " + pgdata.getStart() + " AND RowNum <= " +
	 * pgdata.getEnd() + " "; whereclausesql =
	 * "  (SELECT  t5,t3,t2,t1,n1 FROM FMR009 WHERE n1 IN ( SELECT max(n1) FROM FMR009 )  and recordstatus <> 4 "
	 * + getCriString(searchVal) + " ) as b "; PreparedStatement st =
	 * conn.prepareStatement(sql); ResultSet rs = st.executeQuery(); while
	 * (rs.next()) { price = new PriceData(); price.setT5(rs.getString("t5"));
	 * price.setT3(rs.getString("t3")); price.setT2(rs.getString("t2"));
	 * price.setT1(rs.getString("t1")); price.setN1(rs.getLong("n1"));
	 * priceList.add(price); dataset.setState(true); } for (int i = 0; i <
	 * priceList.size(); i++) { whereclause = " WHERE  t2 = '" +
	 * priceList.get(i).getT2() + "' and  t1 = '" + priceList.get(i).getT1() +
	 * "' and t5 = '" + priceList.get(i).getT5() + "' and (t3 = '" +
	 * priceList.get(i).getT3() + "' " +
	 * " OR t3 IN (select convert(nvarchar(8), dateadd(day,-1, '" +
	 * priceList.get(i).getT3() + "'),112)  )) "; ArrayList<DBRecord> dbrs =
	 * DBMgr.getDBRecords(define(), whereclause, "", conn); PriceListData
	 * priceListData = new PriceListData(); PriceData[] pData = new
	 * PriceData[dbrs.size()]; int res = 0;// (-) int res1 = 0;// (+) for (int k
	 * = 0; k < dbrs.size(); k++) { pData[k] = getDBRecord(dbrs.get(k)); } if
	 * (pData.length >= 2) { for (int j = 0; j < pData.length; j++) {
	 * 
	 * if (Integer.parseInt(pData[0].getT4()) <
	 * Integer.parseInt(pData[1].getT4())) {// [0]// -- // last // batch // no
	 * res = Integer.parseInt(pData[0].getT4()) -
	 * Integer.parseInt(pData[1].getT4()); pData[j].setN2(res); } if
	 * (Integer.parseInt(pData[0].getT4()) > Integer.parseInt(pData[1].getT4()))
	 * { res1 = Integer.parseInt(pData[0].getT4()) -
	 * Integer.parseInt(pData[1].getT4()); pData[j].setN3(res1); } else {
	 * pData[j].setN4(0); } } } priceListData.setData(pData);
	 * priceListData.setGroupName(pData[0].getT1());
	 * datalist.add(priceListData);
	 * 
	 * } dataset.setPageSize(pgdata.getSize());
	 * dataset.setCurrentPage(pgdata.getCurrent()); if (datalist.size() > 0) {
	 * dataset.setState(true); } else { dataset.setState(false); }
	 * PreparedStatement stat = conn.prepareStatement(
	 * "SELECT COUNT(*) AS recCount FROM " + whereclausesql); ResultSet result =
	 * stat.executeQuery(); result.next();
	 * dataset.setTotalCount(result.getInt("recCount")); PriceListData[]
	 * dataarray = new PriceListData[datalist.size()]; dataarray =
	 * datalist.toArray(dataarray); dataset.setData(dataarray); return dataset;
	 * }
	 */

	public static PriceDataSet searchPriceLists(PagerData pgdata, String batchno, Connection conn) throws SQLException {
		PriceDataSet res = new PriceDataSet();
		ArrayList<PriceData> datalist = new ArrayList<PriceData>();
		String whereclause = " WHERE RecordStatus<>4  AND  RecordStatus = 1 ";
		if (!pgdata.getT1().equals("")) {
			whereclause += "AND t1 LIKE N'%" + pgdata.getT1() + "%' OR t2 LIKE N'%" + pgdata.getT1()
					+ "%' OR t3 LIKE N'%" + pgdata.getT1() + "%' OR t4 LIKE N'%" + pgdata.getT1() + "%' OR t7 LIKE N'%"
					+ pgdata.getT1() + "%' OR t8 LIKE N'%" + pgdata.getT1() + "%' OR t10 LIKE N'%" + pgdata.getT1()
					+ "%' OR N1 LIKE N'%" + pgdata.getT1() + "%'  ";
		}
		if (!batchno.equalsIgnoreCase("")) {
			whereclause += "AND N1=" + batchno;
		}
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecordSandE(define(), whereclause, " ORDER BY syskey DESC",
				(pgdata.getStart() - 1), pgdata.getEnd(), 0, conn);

		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getDBRecord(dbrs.get(i)));
		}
		for (int j = 0; j < datalist.size(); j++) {
			datalist.get(j).setT3(ServerUtil.changeEngNumber(datalist.get(j).getT3()));
		}
		res.setPageSize(pgdata.getSize());
		res.setCurrentPage(pgdata.getCurrent());
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		PreparedStatement stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM FMR009 " + whereclause);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		PriceData[] dataarray = new PriceData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	public static DBRecord setDBRecord(PriceData data) {
		DBRecord ret = define();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("createddate", data.getCreateddate());
		ret.setValue("modifieddate", data.getModifieddate());
		ret.setValue("userid", data.getUserid());
		ret.setValue("username", data.getUsername());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("usersyskey", data.getUsersyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t5", data.getT5());
		ret.setValue("t6", data.getT6());
		ret.setValue("t7", data.getT7());
		ret.setValue("t8", data.getT8());
		ret.setValue("t9", data.getT9());
		ret.setValue("t10", data.getT10());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		ret.setValue("n6", data.getN6());
		ret.setValue("n7", data.getN7());
		ret.setValue("n8", data.getN8());
		ret.setValue("n9", data.getN9());
		ret.setValue("n10", data.getN10());
		return ret;
	}

	public static Resultb2b update(PriceData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = DBMgr.updateString(" WHERE RecordStatus = 1 AND Syskey=" + obj.getSyskey(), define(), conn);
		PreparedStatement stmt = conn.prepareStatement(sql);
		DBRecord dbr = setDBRecord(obj);
		DBMgr.setValues(stmt, dbr);
		int count = stmt.executeUpdate();
		if (count > 0) {
			res.setState(true);
			res.setMsgDesc("Updated Successfully!");
		}
		return res;
	}

}
