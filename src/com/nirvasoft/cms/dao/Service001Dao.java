package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.PasswordData;
import com.nirvasoft.cms.shared.RoleData;
import com.nirvasoft.cms.shared.ValueCaptionData;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;

public class Service001Dao {

	public static Resultb2b changePassword(MrBean user, Connection con, PasswordData data) {
		Resultb2b res = new Resultb2b();

		String sql = "UPDATE UVM005 SET t2=? WHERE t2=? AND t1=?";
		PreparedStatement stmt;
		try {
			stmt = con.prepareStatement(sql);
			stmt.setString(1, data.getNewpass());
			stmt.setString(2, data.getOldpass());
			stmt.setString(3, data.getUserid());
			int count = stmt.executeUpdate();
			if (count > 0) {
				res.setState(true);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return res;
	}

	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("UVM005");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		return ret;
	}

	public static String[] getAllLinks(Connection con) {
		ArrayList<String> datalist = new ArrayList<String>();
		try {

			String sql = "Select DISTINCT(link) AS link from View_RoleBtn where link<>'/angular2' AND link<>''";

			PreparedStatement stat = con.prepareStatement(sql);
			ResultSet result = stat.executeQuery();

			while (result.next()) {
				datalist.add(result.getString("link"));
			}

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		String[] dataarr = new String[datalist.size()];
		dataarr = datalist.toArray(dataarr);
		return dataarr;
	}

	public static String[] getBtnKeys(Connection con, String key, String link) {
		ArrayList<String> datalist = new ArrayList<String>();
		try {

			String sql = "Select * from View_Rolebtn WHERE ukey = " + key + " AND link = '" + link + "'";

			PreparedStatement stat = con.prepareStatement(sql);
			ResultSet result = stat.executeQuery();

			while (result.next()) {
				datalist.add(result.getString("t1"));
			}

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		String[] dataarr = new String[datalist.size()];
		dataarr = datalist.toArray(dataarr);
		return dataarr;

	}

	public static String[] getBtnMenus(Connection con) {
		ArrayList<String> key = new ArrayList<String>();
		try {

			String sql = "SELECT syskey FROM UVM022 WHERE t3 <> ''";

			PreparedStatement stat = con.prepareStatement(sql);
			ResultSet result = stat.executeQuery();

			while (result.next()) {
				key.add(String.valueOf(result.getLong("syskey")));
			}

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		String[] arrkey = new String[key.size()];
		arrkey = key.toArray(arrkey);
		return arrkey;

	}

	public static ArrayList<String> getMainMenu(MrBean user, Connection con, String userid) {
		ArrayList<String> ret = new ArrayList<String>();
		try {
			String sql = "SELECT t2 FROM UVM022 " + "WHERE syskey IN (SELECT n2 FROM UVM023 WHERE "
					+ "n1 IN (SELECT n2 FROM JUN002 WHERE n1 IN (SELECT syskey FROM UVM005 " + "WHERE (t1='"
					+ user.getUser().getUserId() + "' OR t3='" + user.getUser().getUserId() + "'))))"
					+ " AND t2<>'System' AND n2=0 AND RecordStatus<> 4 ORDER BY SyncBatch";

			PreparedStatement stat = con.prepareStatement(sql);
			ResultSet result = stat.executeQuery();
			while (result.next()) {
				ret.add(result.getString("t2"));

			}

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		return ret;

	}

	public static long[] getParentKey(MrBean user, Connection con, String userid) {
		ArrayList<Long> key = new ArrayList<Long>();

		try {

			String sql = "SELECT DISTINCT syskey FROM UVM022 " + "WHERE syskey IN (SELECT n2 FROM UVM023 WHERE "
					+ "n1 IN (SELECT n2 FROM JUN002 WHERE n1 IN (SELECT syskey FROM UVM005 " + "WHERE t1='"
					+ user.getUser().getUserId() + "')))" + " AND n2=0 AND RecordStatus<> 4 ORDER BY syskey ";

			PreparedStatement stat = con.prepareStatement(sql);
			ResultSet result = stat.executeQuery();

			while (result.next()) {
				key.add(result.getLong("syskey"));

			}

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		long parentkey[] = new long[key.size()];

		for (int i = 0; i < key.size(); i++) {
			parentkey[i] = key.get(i);
		}

		return parentkey;

	}

	/*
	 * public static long getRole(long n1, Connection con) throws SQLException {
	 * String sql = " SELECT n2  FROM JUN002 WHERE n1='" + n1 + "'";
	 * PreparedStatement stat = con.prepareStatement(sql); ResultSet result =
	 * stat.executeQuery(); //String u = ""; long n2 = 0; if (result.next()) {
	 * n2 = result.getLong("n2");
	 * 
	 * return n2; } return n2;
	 * 
	 * }
	 */
	public static RoleData getRole(long n1, Connection con) throws SQLException {
		RoleData rd = new RoleData();
		String sql = " SELECT n2  FROM JUN002 WHERE n1='" + n1 + "'";
		PreparedStatement stat = con.prepareStatement(sql);
		ResultSet result = stat.executeQuery();
		if (result.next()) {
			rd.setSyskey(result.getLong("n2"));
		}
		String status = " SELECT t1  FROM UVM009 WHERE syskey=" + rd.getSyskey() + "";
		PreparedStatement stm = con.prepareStatement(status);
		ResultSet res = stm.executeQuery();
		if (res.next()) {
			rd.setT1(res.getString("t1"));
		}

		return rd;

	}

	public static ArrayList<ValueCaptionData> getSubMenuItem(MrBean user, Connection con, String userid,
			String parent) {
		ArrayList<ValueCaptionData> ret = new ArrayList<ValueCaptionData>();
		try {

			String sql = "SELECT DISTINCT t1,syskey, t2 FROM UVM022 " + " WHERE syskey IN (SELECT n2 FROM UVM023 WHERE "
					+ " n1 IN (SELECT n2 FROM JUN002 WHERE n1 IN " + "(SELECT syskey FROM UVM005 WHERE (t1='"
					+ user.getUser().getUserId() + "' OR t3='" + user.getUser().getUserId() + "'))))"
					+ " AND RecordStatus<> 4 AND n2 IN (SELECT DISTINCT syskey from UVM022 WHERE t2=" + "'" + parent
					+ "')order by syskey ";

			PreparedStatement stat = con.prepareStatement(sql);
			ResultSet result = stat.executeQuery();

			while (result.next()) {
				ValueCaptionData cdata = new ValueCaptionData();
				cdata.setValue(result.getString("t1"));
				cdata.setCaption(result.getString("t2"));
				ret.add(cdata);
			}

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		return ret;

	}

	public static String getUserData(String userid, Connection con) throws SQLException {
		String sql = " SELECT *  FROM View_User WHERE userid='" + userid + "'";
		PreparedStatement stat = con.prepareStatement(sql);
		ResultSet result = stat.executeQuery();
		String u = "";
		if (result.next()) {
			u = result.getString("username");
			return u;
		}
		return u;

	}

	public static ValueCaptionData login(MrBean user, Connection con) throws SQLException {
		String name = user.getUser().getUserId();
		String psw = user.getUser().getPassword();
		psw = ServerUtil.encryptPIN(psw);
		ValueCaptionData u = new ValueCaptionData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				"WHERE (t1='" + name + "' AND t2='" + psw + "')" + "OR (t3='" + name + "' AND t2='" + psw + "')", "",
				con);
		if (dbrs.size() > 0) {
			name = dbrs.get(0).getString("t1");
			long key = dbrs.get(0).getLong("syskey");
			u.setCaption(getUserData(name, con));
			u.setValue(String.valueOf(key));
		}
		return u;

	}

}
