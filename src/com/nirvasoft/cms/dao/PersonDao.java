package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.PersonData;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;

public class PersonDao {
	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("UVM012");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("CreatedDate", (byte) 5));
		ret.getFields().add(new DBField("ModifiedDate", (byte) 5));
		ret.getFields().add(new DBField("UserId", (byte) 5));
		ret.getFields().add(new DBField("UserName", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t13", (byte) 5));
		ret.getFields().add(new DBField("t14", (byte) 5));
		ret.getFields().add(new DBField("t15", (byte) 5));
		ret.getFields().add(new DBField("t16", (byte) 5));
		ret.getFields().add(new DBField("t17", (byte) 5));
		ret.getFields().add(new DBField("t18", (byte) 5));
		ret.getFields().add(new DBField("t19", (byte) 5));
		ret.getFields().add(new DBField("t20", (byte) 5));
		ret.getFields().add(new DBField("t21", (byte) 5));
		ret.getFields().add(new DBField("t22", (byte) 5));
		ret.getFields().add(new DBField("t23", (byte) 5));
		ret.getFields().add(new DBField("t24", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 1));
		ret.getFields().add(new DBField("n8", (byte) 1));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 1));

		return ret;
	}

	public static Resultb2b deletePerson(long syskey, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		// String sql = "UPDATE UVM012 SET RecordStatus=4 WHERE Syskey IN(SELECT
		// N4 FROM UVM005 WHERE Syskey=?)";
		String sql = "UPDATE UVM012 SET RecordStatus=4 WHERE Syskey=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
		}
		return res;
	}

	public static PersonData getDBRecord(DBRecord adbr) {
		PersonData ret = new PersonData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("CreatedDate"));
		ret.setModifiedDate(adbr.getString("ModifiedDate"));
		ret.setUserId(adbr.getString("UserId"));
		ret.setUserName(adbr.getString("UserName"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));

		ret.setUsersyskey(adbr.getLong("usersysKey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		ret.setT1(adbr.getString("t8"));
		ret.setT2(adbr.getString("t9"));
		ret.setT3(adbr.getString("t10"));
		ret.setT4(adbr.getString("t11"));
		ret.setT5(adbr.getString("t12"));
		ret.setT6(adbr.getString("t13"));
		ret.setT7(adbr.getString("t14"));
		ret.setT3(adbr.getString("t15"));
		ret.setT4(adbr.getString("t16"));
		ret.setT5(adbr.getString("t17"));
		ret.setT6(adbr.getString("t18"));
		ret.setT7(adbr.getString("t19"));
		ret.setT3(adbr.getString("t20"));
		ret.setT4(adbr.getString("t21"));
		ret.setT5(adbr.getString("t22"));
		ret.setT6(adbr.getString("t23"));
		ret.setT7(adbr.getString("t24"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getLong("n5"));
		ret.setN6(adbr.getLong("n6"));
		ret.setN7(adbr.getInt("n7"));
		ret.setN8(adbr.getInt("n8"));
		ret.setN9(adbr.getLong("n9"));
		ret.setN10(adbr.getInt("n10"));

		return ret;
	}

	public static String getUserName(long usys, Connection con) throws SQLException {
		// String sql = " SELECT T2 FROM UVM012 WHERE RecordStatus<>4 AND syskey
		// IN(SELECT N4 FROM UVM005 WHERE syskey="+usys+")";
		String sql = " SELECT T2  FROM UVM012 WHERE RecordStatus<>4 AND syskey =" + usys;
		PreparedStatement stat = con.prepareStatement(sql);
		ResultSet result = stat.executeQuery();
		String u = "";
		while (result.next()) {
			u = result.getString("t2");

		}
		return u;

	}

	public static Resultb2b insert(PersonData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		if (!isCodeExist(obj, conn)) {
			String sql = DBMgr.insertString(define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			stmt.executeUpdate();
			res.setState(true);
			res.getStringResult().add(obj.getT1());
		}
		return res;
	}

	public static boolean isCodeExist(PersonData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				" where RecordStatus<>4 AND syskey = " + obj.getSyskey(), "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isIDExist(PersonData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), " where RecordStatus<>4 AND t1 = " + obj.getT1(), "",
				conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static PersonData read(long syskey, Connection conn) throws SQLException {
		PersonData ret = new PersonData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "where RecordStatus<>4 AND syskey=" + syskey, "", conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));
		return ret;
	}

	public static DBRecord setDBRecord(PersonData data) {
		DBRecord ret = define();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("CreatedDate", data.getCreatedDate());
		ret.setValue("ModifiedDate", data.getModifiedDate());
		ret.setValue("UserId", data.getUserId());
		ret.setValue("UserName", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());

		ret.setValue("usersysKey", data.getUsersyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t5", data.getT5());
		ret.setValue("t6", data.getT6());
		ret.setValue("t7", data.getT7());
		ret.setValue("t8", data.getT8());
		ret.setValue("t9", data.getT9());
		ret.setValue("t10", data.getT10());
		ret.setValue("t11", data.getT11());
		ret.setValue("t12", data.getT12());
		ret.setValue("t13", data.getT13());
		ret.setValue("t14", data.getT14());
		ret.setValue("t15", data.getT15());
		ret.setValue("t16", data.getT16());
		ret.setValue("t17", data.getT17());
		ret.setValue("t18", data.getT18());
		ret.setValue("t19", data.getT19());
		ret.setValue("t20", data.getT20());
		ret.setValue("t21", data.getT21());
		ret.setValue("t22", data.getT22());
		ret.setValue("t23", data.getT23());
		ret.setValue("t24", data.getT24());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		ret.setValue("n6", data.getN6());
		ret.setValue("n7", data.getN7());
		ret.setValue("n8", data.getN8());
		ret.setValue("n9", data.getN9());
		ret.setValue("n10", data.getN10());
		return ret;
	}

	public static Resultb2b update(PersonData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		if (isCodeExist(obj, conn)) {
			String sql = DBMgr.updateString(" WHERE RecordStatus=1 AND Syskey=" + obj.getSyskey(), define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			stmt.executeUpdate();
			res.setState(true);
			res.getStringResult().add(obj.getT1());
		}
		return res;
	}

	public static Resultb2b update12(PersonData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		if (isCodeExist(obj, conn)) {
			String sql = DBMgr.updateString(" WHERE RecordStatus=0 AND Syskey=" + obj.getSyskey(), define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			stmt.executeUpdate();
			res.setState(true);
			res.getStringResult().add(obj.getT1());
		}
		return res;
	}

	/*
	 * public static Result updateStatus(PersonData obj, Connection conn) throws
	 * SQLException { Result res = new Result();
	 * 
	 * String sql =
	 * "UPDATE UVM012 SET RecordStatus = 0 where RecordStatus = 4 AND t1 = '" +
	 * obj.getT1() + "' "; PreparedStatement stm = conn.prepareStatement(sql);
	 * int result1 = stm.executeUpdate(); if (result1 > 0) { res.setState(true);
	 * res.getStringResult().add(obj.getT1()); System.out.println(" update 12**"
	 * ); } return res; }
	 */
	public static Resultb2b updateStatus(PersonData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();

		String sql = DBMgr.updateString(" WHERE RecordStatus=4 AND t1=" + obj.getT1(), define(), conn);
		PreparedStatement stmt = conn.prepareStatement(sql);
		DBRecord dbr = setDBRecord(obj);
		DBMgr.setValues(stmt, dbr);
		stmt.executeUpdate();
		res.setState(true);
		res.getStringResult().add(obj.getT1());
		return res;
	}

}