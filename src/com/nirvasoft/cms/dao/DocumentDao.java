package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.nirvasoft.rp.data.DocumentResponseData;
import com.nirvasoft.rp.shared.DocumentData;
import com.nirvasoft.rp.shared.DocumentRequest;

public class DocumentDao {

	public List<DocumentResponseData> getAllPdf(Connection conn) throws SQLException {
		List<DocumentResponseData> resList = new ArrayList<DocumentResponseData>();

		String sql = "SELECT * FROM Document;";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			DocumentResponseData response = new DocumentResponseData();
			response.setAutokey(rs.getLong("autokey"));
			response.setCreateddate(rs.getString("createddate"));
			response.setModifieddate(rs.getString("modifieddate"));
			response.setFilename(rs.getString("FileName"));
			response.setUserid(rs.getString("UserId"));
			response.setStatus(rs.getString("Status"));
			response.setRecordstatus(rs.getInt("RecordStatus"));
			response.setFiletype(rs.getString("FileType"));
			response.setRegion(rs.getString("Region"));
			response.setFilepath(rs.getString("FilePath"));
			response.setPostedby(rs.getString("PostedBy"));
			response.setModifiedby(rs.getString("ModifiedBy"));
			response.setFilesize(rs.getString("FileSize"));
			response.setPages(rs.getString("Pages"));
			response.setTitle(rs.getString("Title"));
			response.setT1(rs.getString("t1"));
			response.setT2(rs.getString("t2"));
			response.setT3(rs.getString("t3"));
			response.setT4(rs.getString("t4"));
			response.setT5(rs.getString("t5"));
			response.setN1(rs.getInt("N1"));
			response.setN2(rs.getInt("N2"));
			response.setN3(rs.getInt("N3"));
			response.setN4(rs.getInt("N4"));
			response.setN5(rs.getInt("N5"));

			resList.add(response);
		}

		rs.close();
		stmt.close();

		return resList;
	}

	public List<DocumentData> getAllPdfByRegion(DocumentRequest req, Connection conn) throws SQLException {
		List<DocumentData> pdfList = new ArrayList<DocumentData>();

		String sql = "SELECT * FROM Document " + "WHERE recordstatus=1 " + "AND status='Publish' "
				+ "AND (t2 = ? OR t2 = '00000000')";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, req.getRegion());
		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			DocumentData doc = new DocumentData();
			doc.setAutoKey("AutoKey");
			doc.setCreatedDate("CreatedDate");
			doc.setModifiedDate("ModifiedDate");
			doc.setName(rs.getString("FileName"));
			doc.setUserId("UserId");
			doc.setStatus("Status");
			doc.setRecordStatus("RecordStatus");
			doc.setFileType("FileType");
			doc.setRegion(req.getRegion());
			doc.setLink(rs.getString("FilePath"));
			doc.setPostedBy("PostedBy");
			doc.setModifiedBy("ModifiedBy");
			doc.setFileSize("FileSize");
			doc.setPages("Pages");
			doc.setTitle("Title");
			doc.setKeyword(rs.getString("T1"));
			doc.setT2("T2");
			doc.setT3("T3");
			doc.setT4("T4");
			doc.setT5("T5");
			doc.setN1("N1");
			doc.setN2("N2");
			doc.setN3("N3");
			doc.setN4("N4");
			doc.setN5("N5");
			pdfList.add(doc);
		}

		rs.close();
		stmt.close();

		return pdfList;
	}

	public List<DocumentData> getKeyword(DocumentRequest req, Connection conn) throws SQLException {
		List<DocumentData> pdfList = new ArrayList<DocumentData>();

		String where = "";

		if (req.getKeyword() != null && req.getKeyword().trim().length() != 0) {
			where = " AND t1 LIKE ?";
		}

		String sql = "SELECT * FROM Document WHERE recordstatus<>4 AND status='Publish' AND filetype=? AND (t2 = ? OR t2 = '00000000') "
				+ where;
		PreparedStatement stmt = conn.prepareStatement(sql);
		int i = 1;
		stmt.setString(i++, req.getType());
		stmt.setString(i++, req.getRegion());
		if (req.getKeyword() != null && req.getKeyword().trim().length() != 0) {
			stmt.setString(i++, "%" + req.getKeyword() + "%");
		}

		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			DocumentData doc = new DocumentData();
			doc.setAutoKey("AutoKey");
			doc.setCreatedDate("CreatedDate");
			doc.setModifiedDate("ModifiedDate");
			doc.setName(rs.getString("FileName"));
			doc.setUserId("UserId");
			doc.setStatus("Status");
			doc.setRecordStatus("RecordStatus");
			doc.setFileType("FileType");
			doc.setRegion(req.getRegion());
			doc.setLink(rs.getString("FilePath"));
			doc.setPostedBy("PostedBy");
			doc.setModifiedBy("ModifiedBy");
			doc.setFileSize("FileSize");
			doc.setPages("Pages");
			doc.setTitle("Title");
			doc.setKeyword(rs.getString("T1"));
			doc.setT2("T2");
			doc.setT3("T3");
			doc.setT4("T4");
			doc.setT5("T5");
			doc.setN1("N1");
			doc.setN2("N2");
			doc.setN3("N3");
			doc.setN4("N4");
			doc.setN5("N5");
			pdfList.add(doc);
		}

		rs.close();
		stmt.close();

		return pdfList;
	}

	public List<DocumentData> getPdfByTypeAndRegion(DocumentRequest req, Connection conn) throws SQLException {
		List<DocumentData> pdfList = new ArrayList<DocumentData>();
		String sql = "SELECT * FROM Document WHERE recordstatus<>4 AND status='Publish' AND filetype=? AND (t2 = ? OR t2 = '00000000')";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, req.getType());
		stmt.setString(2, req.getRegion());
		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			DocumentData doc = new DocumentData();
			doc.setAutoKey("AutoKey");
			doc.setCreatedDate("CreatedDate");
			doc.setModifiedDate("ModifiedDate");
			doc.setName(rs.getString("FileName"));
			doc.setUserId("UserId");
			doc.setStatus("Status");
			doc.setRecordStatus("RecordStatus");
			doc.setFileType("FileType");
			doc.setRegion(req.getRegion());
			doc.setLink(rs.getString("FilePath"));
			doc.setPostedBy("PostedBy");
			doc.setModifiedBy("ModifiedBy");
			doc.setFileSize("FileSize");
			doc.setPages("Pages");
			doc.setTitle("Title");
			doc.setKeyword(rs.getString("T1"));
			doc.setT2("T2");
			doc.setT3("T3");
			doc.setT4("T4");
			doc.setT5("T5");
			doc.setN1("N1");
			doc.setN2("N2");
			doc.setN3("N3");
			doc.setN4("N4");
			doc.setN5("N5");
			pdfList.add(doc);
		}

		rs.close();
		stmt.close();

		return pdfList;
	}

}
