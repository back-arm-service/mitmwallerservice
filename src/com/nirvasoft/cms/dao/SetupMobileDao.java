package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBRecord;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.shared.PagerMobileData;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.shared.ComboData;
import com.nirvasoft.cms.shared.MenuOrderData;
import com.nirvasoft.cms.shared.SetupData;
import com.nirvasoft.cms.shared.SetupDataSet;

public class SetupMobileDao {
	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("Configuration");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		return ret;
	}

	public static DBRecord define(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		return ret;
	}

	public static boolean ExistPeronJun(ArrayList<ComboData> combo) {
		for (int i = 0; i < combo.size(); i++) {
			if (combo.get(i).getCaption().equalsIgnoreCase("ALL")) {
				return true;
			}
		}
		return false;
	}

	// getarticlelistbycotentwriter
	public static SetupDataSet getArticleListByContentWriter(String usersk, String userid, Connection conn,
			PagerMobileData pgdata, MrBean user) throws SQLException {
		SetupDataSet dataset = new SetupDataSet();
		ArrayList<MenuOrderData> menulist = new ArrayList<MenuOrderData>();
		ArrayList<ComboData> com = new ArrayList<ComboData>();
		// String whereClause = "WHERE RecordStatus<>4";
		String sql = " SELECT t3,n1 FROM configuration WHERE recordstatus <> 4 ORDER BY t1";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			ComboData combo = new ComboData();
			combo.setCaption(res.getString("t3"));
			combo.setValue(res.getLong("n1"));
			com.add(combo);
		}
		if (com.size() > 0) {
			if (ExistPeronJun(com)) {
				MenuOrderData menu_data = new MenuOrderData();
				ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
				datalist = new ArticleMobileDao().getArticleListByContentWriter(usersk, userid, conn, pgdata, user);
				menu_data.setMenu(com.get(6).getCaption());
				menu_data.setMenusk(com.get(6).getValue());
				ArticleData[] dataarray = new ArticleData[datalist.size()];
				dataarray = datalist.toArray(dataarray);
				menu_data.setMenu_arr(dataarray);
				menulist.add(menu_data);

			} else {
				com = new ArrayList<ComboData>();
				String sql1 = " SELECT t3,n1 FROM configuration WHERE recordstatus <> 4 ORDER BY t1   ";
				PreparedStatement stmt1 = conn.prepareStatement(sql1);
				ResultSet res1 = stmt1.executeQuery();
				while (res1.next()) {
					ComboData combo = new ComboData();
					combo.setCaption(res1.getString("t3"));
					combo.setValue(res1.getLong("n1"));
					com.add(combo);
				}
				for (int i = 0; i < com.size(); i++) {
					MenuOrderData menu_data = new MenuOrderData();
					ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
					if (com.get(i).getCaption().equalsIgnoreCase("education")) {
						datalist = new ArticleMobileDao().getArticleData("education", usersk, conn, user);
					} else if (com.get(i).getCaption().equalsIgnoreCase("alert")) {
						datalist = new ArticleMobileDao().getArticleDataAlertList(usersk, conn, user);
					} else {
						datalist = new ArticleMobileDao().getArticleData(com.get(i).getCaption(), usersk, conn, user);
					}
					menu_data.setMenu(com.get(i).getCaption());
					menu_data.setMenusk(com.get(i).getValue());
					ArticleData[] dataarray = new ArticleData[datalist.size()];
					dataarray = datalist.toArray(dataarray);
					menu_data.setMenu_arr(dataarray);
					menulist.add(menu_data);
				}
			}
		}
		MenuOrderData[] menu_Arr = new MenuOrderData[menulist.size()];
		menu_Arr = menulist.toArray(menu_Arr);
		dataset.setMenu_arr(menu_Arr);
		return dataset;
	}

	/*
	 * public static Result insert(SetupData aObj, Connection aConnection)
	 * throws SQLException { Result res = new Result(); if (!isCodeExist(aObj,
	 * aConnection)) { String sql = DBMgr.insertString(define(), aConnection);
	 * PreparedStatement stmt = aConnection.prepareStatement(sql); DBRecord dbr
	 * = setDBRecord(aObj); DBMgr.setValues(stmt, dbr); stmt.executeUpdate();
	 * res.setMsgDesc("Saved Successfully!"); res.setState(true); return res; }
	 * else { res.setMsgDesc("Order already exist!"); res.setState(false);
	 * return res; } }
	 */
	/*
	 * public static Result update(SetupData obj, Connection conn) throws
	 * SQLException { Result res = new Result(); if (!isCodeExist(obj, conn)) {
	 * String sql = DBMgr.updateString(" WHERE RecordStatus<>4 AND Syskey=" +
	 * obj.getSyskey(), define(), conn); PreparedStatement stmt =
	 * conn.prepareStatement(sql); DBRecord dbr = setDBRecord(obj);
	 * DBMgr.setValues(stmt, dbr); int count = stmt.executeUpdate(); if (count >
	 * 0) { res.setState(true); res.setMsgDesc("Updated Successfully!"); } }
	 * else { res.setMsgDesc("Order already exist!"); } return res; }
	 */

	/*
	 * public static boolean isCodeExist(SetupData aObj, Connection aConnection
	 * ) throws SQLException{
	 * 
	 * ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
	 * " where syskey <> "+aObj.getSyskey()+" and t1 =N'" + aObj.getT1() +"'"+
	 * " and recordstatus<>4 ","" , aConnection );//may 28.10.2014 Modified...
	 * syskey<>aobj.getSyskey if (dbrs.size()>0) { return true; }else{ return
	 * false; } }
	 */

	///////////////////////////////////////////////////////// MObile///////////////////////////////////////////////////////////////////

	public static SetupData getDBRecord(DBRecord adbr) {
		SetupData ret = new SetupData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("createddate"));
		ret.setModifiedDate(adbr.getString("modifieddate"));
		ret.setUserId(adbr.getString("userid"));
		ret.setUserName(adbr.getString("username"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setUserSyskey(adbr.getLong("usersyskey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		ret.setT8(adbr.getString("t8"));
		ret.setT9(adbr.getString("t9"));
		ret.setT10(adbr.getString("t10"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getLong("n5"));
		ret.setN6(adbr.getLong("n6"));
		ret.setN7(adbr.getLong("n7"));
		ret.setN8(adbr.getLong("n8"));
		ret.setN9(adbr.getLong("n9"));
		ret.setN10(adbr.getLong("n10"));
		return ret;
	}

	// Menu Order List For Mobile(TDA)
	public static SetupDataSet getMenuOrderList(Connection conn, PagerMobileData pgdata, MrBean user) throws SQLException {
		SetupDataSet dataset = new SetupDataSet();
		ArrayList<MenuOrderData> menulist = new ArrayList<MenuOrderData>();
		ArrayList<ComboData> com = new ArrayList<ComboData>();
		// String whereClause = "WHERE RecordStatus<>4";
		String sql = " SELECT t3,n1 FROM configuration WHERE recordstatus <> 4 ORDER BY t1";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			ComboData combo = new ComboData();
			combo.setCaption(res.getString("t3"));
			combo.setValue(res.getLong("n1"));
			com.add(combo);
		}
		if (com.size() > 0) {
			if (ExistPeronJun(com)) {
				/*
				 * for(int j=0; j<com.size(); j++){
				 * if(com.get(j).getCaption().equalsIgnoreCase("weather")){
				 * MenuOrderData menu = new MenuOrderData();
				 * menu.setMenu(com.get(j).getCaption());
				 * menu.setMenusk(com.get(j).getValue()); menulist.add(menu); }
				 * if(com.get(j).getCaption().equalsIgnoreCase("alert")){
				 * MenuOrderData menu = new MenuOrderData();
				 * ArrayList<ArticleData> datalist1 = new
				 * ArrayList<ArticleData>(); datalist1 =
				 * ArticleDao.getArticleDataAlertList(usersk, conn, user);
				 * menu.setMenu(com.get(j).getCaption());
				 * menu.setMenusk(com.get(j).getValue()); ArticleData[]
				 * dataarray1 = new ArticleData[datalist1.size()]; dataarray1 =
				 * datalist1.toArray(dataarray1); menu.setMenu_arr(dataarray1);
				 * menulist.add(menu); } }
				 */
				MenuOrderData menu_data = new MenuOrderData();
				ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
				datalist = new ArticleMobileDao().getArticleDataAllList(conn, pgdata, user);
				menu_data.setMenu(com.get(6).getCaption());
				menu_data.setMenusk(com.get(6).getValue());
				ArticleData[] dataarray = new ArticleData[datalist.size()];
				dataarray = datalist.toArray(dataarray);
				menu_data.setMenu_arr(dataarray);
				menulist.add(menu_data);

			} else {
				com = new ArrayList<ComboData>();
				String sql1 = " SELECT t3,n1 FROM configuration WHERE recordstatus <> 4 ORDER BY t1   ";
				PreparedStatement stmt1 = conn.prepareStatement(sql1);
				ResultSet res1 = stmt1.executeQuery();
				while (res1.next()) {
					ComboData combo = new ComboData();
					combo.setCaption(res1.getString("t3"));
					combo.setValue(res1.getLong("n1"));
					com.add(combo);
				}
				for (int i = 0; i < com.size(); i++) {
					MenuOrderData menu_data = new MenuOrderData();
					ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
					if (com.get(i).getCaption().equalsIgnoreCase("education")) {
						datalist = new ArticleMobileDao().getArticleData("education", pgdata.getUsersk(), conn, user);
					} else if (com.get(i).getCaption().equalsIgnoreCase("alert")) {
						datalist = new ArticleMobileDao().getArticleDataAlertList(pgdata.getUsersk(), conn, user);
					} else {
						datalist = new ArticleMobileDao().getArticleData(com.get(i).getCaption(), pgdata.getUsersk(), conn,
								user);
					}
					menu_data.setMenu(com.get(i).getCaption());
					menu_data.setMenusk(com.get(i).getValue());
					ArticleData[] dataarray = new ArticleData[datalist.size()];
					dataarray = datalist.toArray(dataarray);
					menu_data.setMenu_arr(dataarray);
					menulist.add(menu_data);
				}
			}
		}
		MenuOrderData[] menu_Arr = new MenuOrderData[menulist.size()];
		menu_Arr = menulist.toArray(menu_Arr);
		dataset.setMenu_arr(menu_Arr);
		return dataset;
	}

	// searcharticlelistbycotentwriter
	public static SetupDataSet searchArticleListByContentWriter(String usersk, String userid, String startdate,
			String enddate, Connection conn, PagerMobileData pgdata, MrBean user) throws SQLException {
		SetupDataSet dataset = new SetupDataSet();
		ArrayList<MenuOrderData> menulist = new ArrayList<MenuOrderData>();
		ArrayList<ComboData> com = new ArrayList<ComboData>();
		String sql = " SELECT t3,n1 FROM configuration WHERE recordstatus <> 4 ORDER BY t1";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			ComboData combo = new ComboData();
			combo.setCaption(res.getString("t3"));
			combo.setValue(res.getLong("n1"));
			com.add(combo);
		}
		if (com.size() > 0) {
			if (ExistPeronJun(com)) {
				MenuOrderData menu_data = new MenuOrderData();
				ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
				datalist = new ArticleMobileDao().searchArticleListByContentWriter(usersk, userid, startdate, enddate, conn,
						pgdata, user);
				menu_data.setMenu(com.get(6).getCaption());
				menu_data.setMenusk(com.get(6).getValue());
				ArticleData[] dataarray = new ArticleData[datalist.size()];
				dataarray = datalist.toArray(dataarray);
				menu_data.setMenu_arr(dataarray);
				menulist.add(menu_data);

			} else {
				com = new ArrayList<ComboData>();
				String sql1 = " SELECT t3,n1 FROM configuration WHERE recordstatus <> 4 ORDER BY t1   ";
				PreparedStatement stmt1 = conn.prepareStatement(sql1);
				ResultSet res1 = stmt1.executeQuery();
				while (res1.next()) {
					ComboData combo = new ComboData();
					combo.setCaption(res1.getString("t3"));
					combo.setValue(res1.getLong("n1"));
					com.add(combo);
				}
				for (int i = 0; i < com.size(); i++) {
					MenuOrderData menu_data = new MenuOrderData();
					ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
					if (com.get(i).getCaption().equalsIgnoreCase("education")) {
						datalist = new ArticleMobileDao().getArticleData("education", usersk, conn, user);
					} else if (com.get(i).getCaption().equalsIgnoreCase("alert")) {
						datalist = new ArticleMobileDao().getArticleDataAlertList(usersk, conn, user);
					} else {
						datalist = new ArticleMobileDao().getArticleData(com.get(i).getCaption(), usersk, conn, user);
					}
					menu_data.setMenu(com.get(i).getCaption());
					menu_data.setMenusk(com.get(i).getValue());
					ArticleData[] dataarray = new ArticleData[datalist.size()];
					dataarray = datalist.toArray(dataarray);
					menu_data.setMenu_arr(dataarray);
					menulist.add(menu_data);
				}
			}
		}
		MenuOrderData[] menu_Arr = new MenuOrderData[menulist.size()];
		menu_Arr = menulist.toArray(menu_Arr);
		dataset.setMenu_arr(menu_Arr);
		return dataset;
	}

	public static DBRecord setDBRecord(SetupData data) {
		DBRecord ret = define();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("createddate", data.getCreatedDate());
		ret.setValue("modifieddate", data.getModifiedDate());
		ret.setValue("userid", data.getUserId());
		ret.setValue("username", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("usersyskey", data.getUserSyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t5", data.getT5());
		ret.setValue("t6", data.getT6());
		ret.setValue("t7", data.getT7());
		ret.setValue("t8", data.getT8());
		ret.setValue("t9", data.getT9());
		ret.setValue("t10", data.getT10());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		ret.setValue("n6", data.getN6());
		ret.setValue("n7", data.getN7());
		ret.setValue("n8", data.getN8());
		ret.setValue("n9", data.getN9());
		ret.setValue("n10", data.getN10());
		return ret;
	}

}
