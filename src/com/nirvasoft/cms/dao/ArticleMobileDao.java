package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;
import com.nirvasoft.database.SysKeyMgr;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.ResultMobile;
import com.nirvasoft.cms.shared.PagerMobileData;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.shared.ArticleDataSet;
import com.nirvasoft.cms.shared.RegisterData;
import com.nirvasoft.cms.shared.RegisterDataSet;
import com.nirvasoft.cms.util.ServerUtil;

public class ArticleMobileDao {
	public String ddMMyyyFormat(String aDate) {// for all list
		String l_Date = "";
		if (!aDate.equals("") && aDate != null)
			l_Date = aDate.substring(6) + "/" + aDate.substring(4, 6) + "/" + aDate.substring(0, 4);
		return l_Date;
	}

	public DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("FMR002");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("createdtime", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("modifiedtime", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t13", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		ret.getFields().add(new DBField("n11", (byte) 2));
		ret.getFields().add(new DBField("n12", (byte) 2));
		ret.getFields().add(new DBField("n13", (byte) 2));
		return ret;
	}

	public DBRecord define(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t13", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		ret.getFields().add(new DBField("n11", (byte) 2));
		ret.getFields().add(new DBField("n12", (byte) 2));
		ret.getFields().add(new DBField("n13", (byte) 2));
		return ret;
	}

	public DBRecord defines(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("createdtime", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("modifiedtime", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t13", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		ret.getFields().add(new DBField("n11", (byte) 2));
		ret.getFields().add(new DBField("n12", (byte) 2));
		ret.getFields().add(new DBField("n13", (byte) 2));
		return ret;
	}

	public ResultMobile delete(Long syskey, Connection conn) throws SQLException {
		ResultMobile res = new ResultMobile();
		String sql = "UPDATE FMR002 SET RecordStatus=4 WHERE RecordStatus = 1 AND syskey=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Successfully");
		} else {
			res.setMsgDesc("Deleting Unsuccessful");
		}
		return res;
	}

	/*
	 * public ArticleDataSet readBysyskey(long syskey, Connection conn) throws
	 * SQLException { ArticleDataSet res = new ArticleDataSet();
	 * ArrayList<ArticleData> datalist = new ArrayList<ArticleData>(); String
	 * whereclause =
	 * " WHERE RecordStatus<>4 AND RecordStatus = 1 AND syskey = '" + syskey +
	 * "' "; ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
	 * whereclause, " ORDER BY syskey ", conn); for (int i = 0; i < dbrs.size();
	 * i++) { datalist.add(getDBRecord(dbrs.get(i))); } if (datalist.size() > 0)
	 * { res.setState(true); } else { res.setState(false); } ArticleData[]
	 * dataarray = new ArticleData[datalist.size()]; dataarray =
	 * datalist.toArray(dataarray); res.setData(dataarray); return res; }
	 */

	/*
	 * public boolean isVDOExist(ArticleData obj, Connection conn) throws
	 * SQLException { ArrayList<DBRecord> dbrs =
	 * DBMgr.getDBRecords(defines("FMR002"),
	 * " where RecordStatus<>4 AND  RecordStatus = 1 AND syskey = " +
	 * obj.getSyskey() + " ", "", conn); if (dbrs.size() > 0) { return true; }
	 * else { return false; } }
	 */

	/*
	 * public boolean isIDExist(ArticleData obj, Connection conn) throws
	 * SQLException { ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
	 * " where RecordStatus<>4 AND  RecordStatus = 1 AND syskey = " +
	 * obj.getSyskey() + " ", "", conn); if (dbrs.size() > 0) { return true; }
	 * else { return false; } }
	 */

	/*
	 * public Result insert(ArticleData obj, Connection conn) throws
	 * SQLException { Result res = new Result(); if (!isCodeExist(obj, conn)) {
	 * String sql = DBMgr.insertString(define(), conn); PreparedStatement stmt =
	 * conn.prepareStatement(sql); DBRecord dbr = setDBRecord(obj);
	 * DBMgr.setValues(stmt, dbr); int count = stmt.executeUpdate(); if (count >
	 * 0) { res.setState(true); res.setMsgDesc("Saved Successfully!"); } } else
	 * { res.setMsgDesc("Article Already Exist!"); } return res; }
	 */

	/*
	 * public Result insertVideo(ArticleData obj, Connection conn) throws
	 * SQLException { Result res = new Result(); String sql =
	 * DBMgr.insertString(defines("FMR002"), conn); PreparedStatement stmt =
	 * conn.prepareStatement(sql); DBRecord dbr = setDBRecord(obj);
	 * DBMgr.setValues(stmt, dbr); int count = stmt.executeUpdate(); if (count >
	 * 0) { res.setState(true); res.setMsgDesc("Saved Successfully!"); } return
	 * res; }
	 */

	/*
	 * public Result update(ArticleData obj, Connection conn) throws
	 * SQLException { Result res = new Result(); if (isCodeExist(obj, conn)) {
	 * String sql = DBMgr.updateString(
	 * " WHERE RecordStatus<>4 AND RecordStatus = 1 AND Syskey=" +
	 * obj.getSyskey(),define(), conn); PreparedStatement stmt =
	 * conn.prepareStatement(sql); DBRecord dbr = setDBRecord(obj);
	 * DBMgr.setValues(stmt, dbr); int count = stmt.executeUpdate(); if (count >
	 * 0) { res.setState(true); res.setMsgDesc("Updated Successfully!"); } }
	 * else { res.setMsgDesc("No Such Article to Update!"); } return res; }
	 */
	/*
	 * public Result updateVideo(ArticleData obj, Connection conn) throws
	 * SQLException { Result res = new Result(); if (isVDOExist(obj, conn)) {
	 * String sql = DBMgr.updateString(
	 * " WHERE RecordStatus<>4 AND RecordStatus = 1 AND Syskey=" +
	 * obj.getSyskey(),defines("FMR002"), conn); PreparedStatement stmt =
	 * conn.prepareStatement(sql); DBRecord dbr = setDBRecord(obj);
	 * DBMgr.setValues(stmt, dbr); int count = stmt.executeUpdate(); if (count >
	 * 0) { res.setState(true); res.setMsgDesc("Updated Successfully!"); } }
	 * else { res.setMsgDesc("No Such Article to Update!"); } return res; }
	 */

	/*
	 * public Result saveAns(ArticleData obj, Connection conn) throws
	 * SQLException { Result res = new Result(); String sql =
	 * "UPDATE FMR002 SET t4='" + obj.getT2() +
	 * "' WHERE RecordStatus = 1 AND syskey=" + obj.getSyskey();
	 * PreparedStatement stmt = conn.prepareStatement(sql); int rs =
	 * stmt.executeUpdate(); if (rs > 0) { res.setState(true); res.setMsgDesc(
	 * "Saved Successfully"); } else { res.setMsgDesc(
	 * "Deleting Unsuccessful"); } return res; }
	 */

	public ResultMobile deleteByUpdate(Long syskey, Connection conn) throws SQLException {
		ResultMobile res = new ResultMobile();
		String sql = "DELETE FROM FMR008 WHERE n1=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Successfully");
		}
		return res;
	}

	/*
	 * public ArticleDataSet search(PagerData pgdata, String searchVal, String
	 * type, Connection conn)throws SQLException { ArticleDataSet res = new
	 * ArticleDataSet(); ArrayList<ArticleData> datalist = new
	 * ArrayList<ArticleData>(); String whereclause =
	 * " WHERE RecordStatus<>4  AND  RecordStatus = 1 AND t3 = '" + type + "' ";
	 * if (!searchVal.equals("")) { whereclause += "AND ( t1 LIKE '%"+searchVal+
	 * "%' OR t2 LIKE '%"+searchVal+"%' OR  )"; } ArrayList<DBRecord> dbrs =
	 * DBMgr.getDBRecordSandE(define(), whereclause, " ORDER BY syskey "
	 * ,(pgdata.getStart() - 1), pgdata.getEnd(), 0, conn); for (int i = 0; i <
	 * dbrs.size(); i++) { datalist.add(getDBRecord(dbrs.get(i))); }
	 * res.setPageSize(pgdata.getSize());
	 * res.setCurrentPage(pgdata.getCurrent()); if (datalist.size() > 0) {
	 * res.setState(true); } else { res.setState(false); } PreparedStatement
	 * stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM FMR002 " +
	 * whereclause); ResultSet result = stat.executeQuery(); result.next();
	 * res.setTotalCount(result.getInt("recCount")); ArticleData[] dataarray =
	 * new ArticleData[datalist.size()]; dataarray =
	 * datalist.toArray(dataarray); res.setData(dataarray); return res; }
	 */

	/*
	 * public ArticleDataSet searchAns(String searchVal, String type, Connection
	 * conn) throws SQLException { ArticleDataSet res = new ArticleDataSet();
	 * ArrayList<ArticleData> datalist = new ArrayList<ArticleData>(); String
	 * whereclause = " WHERE RecordStatus = 1 AND t3 = '" + type +
	 * "' AND syskey = " + searchVal; ArrayList<DBRecord> dbrs =
	 * DBMgr.getDBRecords(define(), whereclause, " ORDER BY syskey ", conn); for
	 * (int i = 0; i < dbrs.size(); i++) {
	 * datalist.add(getDBRecord(dbrs.get(i))); } if (datalist.size() > 0) {
	 * res.setState(true); } else { res.setState(false); } ArticleData[]
	 * dataarray = new ArticleData[datalist.size()]; dataarray =
	 * datalist.toArray(dataarray); res.setData(dataarray); return res; }
	 */

	/*
	 * public ArticleDataSet searchList(PagerData pgdata, String type, String
	 * croptype,long status, Connection conn)throws SQLException {
	 * ArticleDataSet res = new ArticleDataSet(); ArrayList<ArticleData>
	 * datalist = new ArrayList<ArticleData>(); ArrayList<DBRecord> dbrs;
	 * PreparedStatement stat; String clause = ""; String whereclause =
	 * " WHERE RecordStatus<>4  AND  RecordStatus = 1 AND t3 = '" + type + "' ";
	 * if(status==1){ whereclause+="AND n7='"+status+"'"; } if(status==2){
	 * whereclause+="AND (n7=1 OR n7=2 OR n7=3 OR n7=4)"; } if(status==3){
	 * whereclause+="AND (n7=2 OR n7=3 OR n7=4 OR n7=5 OR n7=6)"; } if
	 * (!croptype.equalsIgnoreCase("")) { clause +=
	 * " f join jun004 j on f.syskey=j.n1 Where f.t3='" + type +
	 * "' and j.recordstatus<>4 and f.recordstatus<>4 and j.n2='" + croptype +
	 * "'"; } if (!pgdata.getT1().equals("")) { whereclause +=
	 * "AND (t1 LIKE N'%" + pgdata.getT1() + "%' OR  modifieddate LIKE N'%" +
	 * pgdata.getT1() + "%' OR modifiedtime LIKE N'%" + pgdata.getT1() +
	 * "%' OR  t2 LIKE N'%" + pgdata.getT1() + "%' OR  username LIKE N'%" +
	 * pgdata.getT1() + "%') "; } if (!croptype.equalsIgnoreCase("")) { String
	 * sql =
	 * " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc) AS RowNum,* FROM ( select f.syskey, f.t1, f.t2, f.t3, f.modifieddate,"
	 * + " f.modifiedtime,f.username  from  fmr002 " + clause +
	 * ") b)  AS RowConstrainedResult WHERE RowNum >= '" + (pgdata.getStart() -
	 * 1) + "' AND RowNum <= '" + pgdata.getEnd() + "' "; PreparedStatement ps =
	 * conn.prepareStatement(sql); ResultSet rs = ps.executeQuery(); while
	 * (rs.next()) { ArticleData data = new ArticleData();
	 * data.setSyskey(rs.getLong("syskey")); data.setT1(rs.getString("t1"));
	 * data.setT2(rs.getString("t2")); data.setT3(rs.getString("t3"));
	 * data.setModifiedDate(rs.getString("modifieddate"));
	 * data.setModifiedTime(rs.getString("modifiedtime"));
	 * data.setUserName(rs.getString("username")); datalist.add(data); } } else
	 * { dbrs = DBMgr.getDBRecordSandE(define(), whereclause,
	 * " ORDER BY syskey DESC ", (pgdata.getStart() - 1),pgdata.getEnd(), 0,
	 * conn); for (int i = 0; i < dbrs.size(); i++) {
	 * if(dbrs.get(i).getLong("n7")==1){ dbrs.get(i).setValue("t10", "Draft"); }
	 * if(dbrs.get(i).getLong("n7")==2){ dbrs.get(i).setValue("t10", "Pending");
	 * } if(dbrs.get(i).getLong("n7")==3){ dbrs.get(i).setValue("t10",
	 * "Modification"); } if(dbrs.get(i).getLong("n7")==4){
	 * dbrs.get(i).setValue("t10", "Approve"); }
	 * if(dbrs.get(i).getLong("n7")==5){ dbrs.get(i).setValue("t10",
	 * "Publisher"); } if(dbrs.get(i).getLong("n7")==6){
	 * dbrs.get(i).setValue("t10", "Reject"); }
	 * datalist.add(getDBRecord(dbrs.get(i))); } }
	 * res.setPageSize(pgdata.getSize());
	 * res.setCurrentPage(pgdata.getCurrent()); if (datalist.size() > 0) {
	 * res.setState(true); } else { res.setState(false); } if
	 * (!croptype.contentEquals("")) { stat = conn.prepareStatement(
	 * "SELECT COUNT(*) AS recCount FROM FMR002 " + clause);
	 * 
	 * } else { stat = conn.prepareStatement(
	 * "SELECT COUNT(*) AS recCount FROM FMR002 " + whereclause); } ResultSet
	 * result = stat.executeQuery(); result.next();
	 * res.setTotalCount(result.getInt("recCount")); ArticleData[] dataarray =
	 * new ArticleData[datalist.size()]; dataarray =
	 * datalist.toArray(dataarray); res.setData(dataarray); return res; }
	 */

	/* deleteByUpdateComment */
	public ResultMobile deleteByUpdateComment(Long syskey, Connection conn) throws SQLException {
		ResultMobile res = new ResultMobile();
		String sql = "DELETE FROM FMR003 WHERE n1=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Successfully");
		}
		return res;
	}

	/* deleteTemp */
	public ResultMobile deleteTemp(String usersk, Connection conn) throws SQLException {
		ResultMobile res = new ResultMobile();
		String sql = " DELETE FROM TEMP_All WHERE N9 = '" + usersk + "'";
		PreparedStatement stmt = conn.prepareStatement(sql);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
		}
		return res;
	}

	public ArrayList<ArticleData> getArticleData(String type, String usersk, Connection conn, MrBean user)
			throws SQLException {
		ArrayList<ArticleData> list = new ArrayList<ArticleData>();
		// String sql = " SELECT TOP 3 * FROM FMR002 WHERE RecordStatus<>4 AND
		// RecordStatus = 1 AND t4<>'alert' and t3<>'sample' and n7=5 AND "
		// + " LOWER(REPLACE(t3, ' ', '')) = LOWER(REPLACE('" + type + "', ' ',
		// '')) ORDER BY syskey DESC ";

		/*
		 * String sql = " SELECT top(3)* from fmr002 " + " WHERE t3='"+type+
		 * "' and t4<>'alert' and n7=5 and modifieddate IN (SELECT max(modifieddate) FROM fmr002)"
		 * ;
		 */

		String sql = "SELECT TOP(3) * FROM fmr002 WHERE t4<>'alert' and t3='" + type
				+ "' and recordstatus <> 4 and n7=5" + " Order By syskey DESC";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ArticleData ret = new ArticleData();
			ret.setSyskey(rs.getLong("syskey"));
			ret.setCreatedDate(rs.getString("createddate"));
			ret.setCreatedTime(rs.getString("createdtime"));
			ret.setModifiedDate(rs.getString("modifieddate"));
			ret.setModifiedTime(rs.getString("modifiedtime"));
			ret.setUserId(rs.getString("userid"));
			ret.setUserName(rs.getString("username"));
			ret.setRecordStatus(rs.getInt("RecordStatus"));
			ret.setSyncStatus(rs.getInt("SyncStatus"));
			ret.setSyncBatch(rs.getInt("SyncBatch"));
			ret.setUserSyskey(rs.getLong("syskey"));
			ret.setT1(rs.getString("t1"));
			ret.setT2(rs.getString("t2"));
			ret.setT3(rs.getString("t3"));
			ret.setT4(rs.getString("t4"));
			ret.setT5(rs.getString("t5"));
			ret.setT6(rs.getString("t6"));
			ret.setT7(rs.getString("t7"));
			ret.setT8(rs.getString("t8"));
			ret.setT9(rs.getString("t9"));
			ret.setT10(rs.getString("t10"));
			ret.setT11(rs.getString("t11"));
			ret.setT12(rs.getString("t12"));
			ret.setT13(rs.getString("t13"));
			ret.setN1(rs.getLong("n1"));
			ret.setN2(rs.getLong("n2"));
			ret.setN3(rs.getLong("n3"));
			ret.setN4(rs.getLong("n4"));
			ret.setN5(rs.getLong("n5"));
			ret.setN6(new ArticleMobileDao().searchLikeOrNot(ret.getSyskey(), usersk, conn));
			// ret.setN7(new ContentMenuDao().searchSaveContentOrNot(usersk, ret.getSyskey(), conn));// 1=save
			ret.setN8(rs.getLong("n8"));
			ret.setN9(new ArticleMobileDao().searchDisLikeOrNot(ret.getSyskey(), usersk, conn));
			ret.setN10(rs.getLong("n10"));
			ret.setN11(rs.getLong("n11"));
			ret.setN12(rs.getLong("n12"));
			ret.setN13(rs.getLong("n13"));
			ret.setUploadedPhoto(new UploadMobileDao().search(String.valueOf(ret.getSyskey()), ret.getT3(), conn).getData());
			if (ret.getT3().equalsIgnoreCase("video")) {
				ret.setVideoUpload(
						new UploadMobileDao().searchVideoList(String.valueOf(ret.getSyskey()), conn).getVideoUpload());
			}
			list.add(ret);
		}
		return list;
	}

	// for alert
	public ArrayList<ArticleData> getArticleDataAlertList(String usersk, Connection conn, MrBean user)
			throws SQLException {
		ArrayList<ArticleData> list = new ArrayList<ArticleData>();
		// long sk = 0;
		// String enddate = "";
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		String sql1 = "select syskey,t11 from fmr002 where recordstatus<>4 and t4='alert' and t11<>''";
		PreparedStatement ps1 = conn.prepareStatement(sql1);
		ResultSet rs1 = ps1.executeQuery();
		while (rs1.next()) {
			ArticleData reet = new ArticleData();
			reet.setSyskey(rs1.getLong("syskey"));
			reet.setT11(rs1.getString("t11"));
			datalist.add(reet);
		}
		for (int i = 0; i < datalist.size(); i++) {
			/*
			 * String todayDate = new SimpleDateFormat("yyyyMMdd").format(new
			 * Date()); if (Integer.parseInt(datalist.get(i).getT11()) <=
			 * Integer.parseInt(todayDate)) { String sql =
			 * "select * from fmr002 where modifieddate=CONVERT(NVARCHAR(8),GETDATE(),112) and recordstatus<>4 and n7=5 and t4='alert'"
			 * ; String sql =
			 * "select * from fmr002 where recordstatus<>4 and n7=5 and t4='alert' and t11='"
			 * + datalist.get(i).getT11() + "' and syskey=" +
			 * datalist.get(i).getSyskey();
			 */
			String sql = " select * from fmr002 where recordstatus<>4 and n7=5 and t4='alert' "
					+ "and (t11 between (CONVERT(NVARCHAR(8),GETDATE(),112)) and '" + datalist.get(i).getT11()
					+ "' ) and syskey=" + datalist.get(i).getSyskey();
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				ArticleData ret = new ArticleData();
				ret.setSyskey(rs.getLong("syskey"));
				ret.setCreatedDate(rs.getString("createddate"));
				ret.setCreatedTime(rs.getString("createdtime"));
				ret.setModifiedDate(rs.getString("modifieddate"));
				ret.setModifiedTime(rs.getString("modifiedtime"));
				ret.setUserId(rs.getString("userid"));
				ret.setUserName(rs.getString("username"));
				ret.setRecordStatus(rs.getInt("RecordStatus"));
				ret.setSyncStatus(rs.getInt("SyncStatus"));
				ret.setSyncBatch(rs.getInt("SyncBatch"));
				ret.setUserSyskey(rs.getLong("syskey"));
				ret.setT1(rs.getString("t1"));
				ret.setT2(rs.getString("t2"));
				ret.setT3(rs.getString("t3"));
				ret.setT4(rs.getString("t4"));
				ret.setT5(rs.getString("t5"));
				ret.setT6(rs.getString("t6"));
				ret.setT7(rs.getString("t7"));
				ret.setT8(rs.getString("t8"));
				ret.setT9(rs.getString("t9"));
				ret.setT10(rs.getString("t10"));
				ret.setT11(rs.getString("t11"));
				ret.setT12(rs.getString("t12"));
				ret.setT13(rs.getString("t13"));
				ret.setN1(rs.getLong("n1"));
				ret.setN2(rs.getLong("n2"));
				ret.setN3(rs.getLong("n3"));
				ret.setN4(rs.getLong("n4"));
				ret.setN5(rs.getLong("n5"));
				ret.setN6(new ArticleMobileDao().searchLikeOrNot(ret.getSyskey(), usersk, conn));
				ret.setN7(ContentMenuDao.searchSaveContentOrNot(usersk, ret.getSyskey(), conn));// 1=savecontent,0=unsavecontent
				ret.setN8(rs.getLong("n8"));
				ret.setN9(new ArticleMobileDao().searchDisLikeOrNot(ret.getSyskey(), usersk, conn));
				ret.setN10(rs.getLong("n10"));
				ret.setN11(rs.getLong("n11"));
				ret.setN12(rs.getLong("n12"));
				ret.setN13(rs.getLong("n13"));
				ret.setUploadedPhoto(
						new UploadMobileDao().search(String.valueOf(ret.getSyskey()), ret.getT3(), conn).getData());
				if (ret.getT3().equalsIgnoreCase("video")) {
					ret.setVideoUpload(
							new UploadMobileDao().searchVideoList(String.valueOf(ret.getSyskey()), conn).getVideoUpload());
				}
				list.add(ret);
			}

		}
		return list;
	}

	// for alltype
	public ArrayList<ArticleData> getArticleDataAllList(Connection conn, PagerMobileData pgdata, MrBean user)
			throws SQLException {
		ArrayList<ArticleData> list = new ArrayList<ArticleData>();
		String sql = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey DESC) AS RowNum,* FROM ( "
				+ "SELECT * FROM FMR002 AS A " + "WHERE A.recordstatus<>4 "
				// + "AND A.t11=? "
				+ "AND A.n7=5 " + "AND (A.t5='00000000' OR A.t5=?) " + "AND (A.t9=0 " + "OR A.userid=? "
				+ "OR A.userid = " + "(SELECT phone FROM Contact AS B " + "WHERE B.userid=? " + "AND A.userid=B.phone "
				+ "AND A.t9!=2)) " + ") b)  AS RowConstrainedResult WHERE RowNum >= '" + (pgdata.getStart())
				+ "' AND RowNum <= '" + pgdata.getEnd() + "' ";
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		// ps.setString(i++, pgdata.getDomain());
		ps.setString(i++, pgdata.getRegionNumber());
		ps.setString(i++, pgdata.getUserPhone());
		ps.setString(i++, pgdata.getUserPhone());
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ArticleData ret = new ArticleData();
			ret.setSyskey(rs.getLong("syskey"));
			ret.setCreatedDate(rs.getString("createddate"));
			ret.setCreatedTime(rs.getString("createdtime"));
			ret.setModifiedDate(rs.getString("modifieddate"));
			ret.setModifiedTime(rs.getString("modifiedtime"));
			ret.setUserId(rs.getString("userid"));
			ret.setUserName(rs.getString("username"));
			ret.setRecordStatus(rs.getInt("RecordStatus"));
			ret.setSyncStatus(rs.getInt("SyncStatus"));
			ret.setSyncBatch(rs.getInt("SyncBatch"));
			ret.setUserSyskey(rs.getLong("syskey"));
			ret.setT1(rs.getString("t1"));
			ret.setT2(rs.getString("t2"));
			ret.setT3(rs.getString("t3"));
			ret.setT4(rs.getString("t4"));
			ret.setT5(rs.getString("t5"));
			ret.setT6(rs.getString("t6"));
			ret.setT7(rs.getString("t7"));
			ret.setT8(rs.getString("t8"));
			ret.setT9(rs.getString("t9"));
			ret.setT10(rs.getString("t10"));
			ret.setT11(rs.getString("t11"));
			ret.setT12(rs.getString("t12"));
			ret.setT13(rs.getString("t13"));
			ret.setN1(rs.getLong("n1"));
			ret.setN2(rs.getLong("n2"));
			ret.setN3(rs.getLong("n3"));
			ret.setN4(rs.getLong("n4"));
			ret.setN5(rs.getLong("n5"));
			ret.setN6(new ArticleMobileDao().searchLikeOrNot(ret.getSyskey(), pgdata.getUsersk(), conn)); // like
			new ContentMenuDao();
			ret.setN7(ContentMenuDao.searchSaveContentOrNot(pgdata.getUsersk(), ret.getSyskey(), conn));// 1=savecontent,0=unsavecontent
			ret.setAnswer(new CommentMobileDao().searchAnswer(ret.getSyskey(), "answer", conn).getAnsData());// getAnswerData
			ret.setN8(rs.getLong("n8"));
			ret.setN9(new ArticleMobileDao().searchDisLikeOrNot(ret.getSyskey(), pgdata.getUsersk(), conn));
			ret.setN10(rs.getLong("n10"));
			ret.setN11(rs.getLong("n11"));
			ret.setN12(rs.getLong("n12"));
			ret.setN13(rs.getLong("n13"));
			ret.setUploadedPhoto(new UploadMobileDao().search(String.valueOf(ret.getSyskey()), ret.getT3(), conn).getData());
			if (ret.getT3().equalsIgnoreCase("video")) {
				ret.setVideoUpload(
						new UploadMobileDao().searchVideoList(String.valueOf(ret.getSyskey()), conn).getVideoUpload());
			}
			list.add(ret);
		}
		return list;
	}

	// getarticlelistbycotentwriter
	public ArticleDataSet getArticleDataBySyskey(long syskey, Connection conn, MrBean user) throws SQLException {

		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> list = new ArrayList<ArticleData>();
		String whereClause = "";
		whereClause = "WHERE RecordStatus<>4 AND n7=5 and syskey='" + syskey + "'";

		String sql = "SELECT * FROM FMR002 " + whereClause;
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ArticleData ret = new ArticleData();
			ret.setSyskey(rs.getLong("syskey"));
			ret.setCreatedDate(rs.getString("createddate"));
			ret.setCreatedTime(rs.getString("createdtime"));
			ret.setModifiedDate(rs.getString("modifieddate"));
			ret.setModifiedTime(rs.getString("modifiedtime"));
			ret.setUserId(rs.getString("userid"));
			ret.setUserName(rs.getString("username"));
			ret.setModifiedUserId(rs.getString("modifieduserid"));
			ret.setModifiedUserName(rs.getString("modifiedusername"));
			ret.setRecordStatus(rs.getInt("RecordStatus"));
			ret.setSyncStatus(rs.getInt("SyncStatus"));
			ret.setSyncBatch(rs.getInt("SyncBatch"));
			ret.setUserSyskey(rs.getLong("syskey"));
			ret.setT1(rs.getString("t1"));
			ret.setT2(rs.getString("t2"));
			ret.setT3(rs.getString("t3"));
			ret.setT4(rs.getString("t4"));
			ret.setT5(rs.getString("t5"));
			ret.setT6(rs.getString("t6"));
			ret.setT7(rs.getString("t7"));
			ret.setT8(rs.getString("t8"));
			ret.setT9(rs.getString("t9"));
			ret.setT10(rs.getString("t10"));
			ret.setT11(rs.getString("t11"));
			ret.setT12(rs.getString("t12"));
			ret.setT13(rs.getString("t13"));
			ret.setN1(rs.getLong("n1"));
			ret.setN2(rs.getLong("n2"));
			ret.setN3(rs.getLong("n3"));
			ret.setN4(rs.getLong("n4"));
			ret.setN5(rs.getLong("n5"));
			ret.setN6(rs.getLong("n6"));
			ret.setN7(rs.getLong("n7"));
			ret.setN8(rs.getLong("n8"));
			ret.setN9(rs.getLong("n9"));
			ret.setN10(rs.getLong("n10"));
			ret.setN11(rs.getLong("n11"));
			ret.setN12(rs.getLong("n12"));
			ret.setN13(rs.getLong("n13"));
			list.add(ret);
		}
		ArticleData[] dataarray = new ArticleData[list.size()];
		dataarray = list.toArray(dataarray);
		res.setData(dataarray);

		return res;
	}

	// getArticleListByContentWriter
	public ArrayList<ArticleData> getArticleListByContentWriter(String usersk, String userid, Connection conn,
			PagerMobileData pgdata, MrBean user) throws SQLException {

		ArrayList<ArticleData> list = new ArrayList<ArticleData>();
		String whereClause = "";
		if (!userid.equalsIgnoreCase("")) {
			whereClause = "WHERE RecordStatus<>4 AND n7=5 and userid='" + userid + "'";
		} else {
			whereClause = "WHERE RecordStatus<>4 AND n7=5";
		}

		String sql = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey DESC) AS RowNum,* FROM ( SELECT * FROM fmr002 "
				+ whereClause + ") b)  AS RowConstrainedResult WHERE RowNum >= '" + (pgdata.getStart())
				+ "' AND RowNum <= '" + pgdata.getEnd() + "' ";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ArticleData ret = new ArticleData();
			ret.setSyskey(rs.getLong("syskey"));
			ret.setCreatedDate(rs.getString("createddate"));
			ret.setCreatedTime(rs.getString("createdtime"));
			ret.setModifiedDate(rs.getString("modifieddate"));
			ret.setModifiedTime(rs.getString("modifiedtime"));
			ret.setUserId(rs.getString("userid"));
			ret.setUserName(rs.getString("username"));
			ret.setRecordStatus(rs.getInt("RecordStatus"));
			ret.setSyncStatus(rs.getInt("SyncStatus"));
			ret.setSyncBatch(rs.getInt("SyncBatch"));
			ret.setUserSyskey(rs.getLong("syskey"));
			ret.setT1(rs.getString("t1"));
			ret.setT2(rs.getString("t2"));
			ret.setT3(rs.getString("t3"));
			ret.setT4(rs.getString("t4"));
			ret.setT5(rs.getString("t5"));
			ret.setT6(rs.getString("t6"));
			ret.setT7(rs.getString("t7"));
			ret.setT8(rs.getString("t8"));
			ret.setT9(rs.getString("t9"));
			ret.setT10(rs.getString("t10"));
			ret.setT11(rs.getString("t11"));
			ret.setT12(rs.getString("t12"));
			ret.setT13(rs.getString("t13"));
			ret.setN1(rs.getLong("n1"));
			ret.setN2(rs.getLong("n2"));
			ret.setN3(rs.getLong("n3"));
			ret.setN4(rs.getLong("n4"));
			ret.setN5(rs.getLong("n5"));
			ret.setN6(new ArticleMobileDao().searchLikeOrNot(ret.getSyskey(), usersk, conn)); // like
			new ContentMenuDao();
			ret.setN7(ContentMenuDao.searchSaveContentOrNot(usersk, ret.getSyskey(), conn));// 1=savecontent,0=unsavecontent
			ret.setAnswer(new CommentMobileDao().searchAnswer(ret.getSyskey(), "answer", conn).getAnsData());// getAnswerData
			ret.setN8(rs.getLong("n8"));
			ret.setN9(new ArticleMobileDao().searchDisLikeOrNot(ret.getSyskey(), usersk, conn));
			ret.setN10(rs.getLong("n10"));
			ret.setN11(rs.getLong("n11"));
			ret.setN12(rs.getLong("n12"));
			ret.setN13(rs.getLong("n13"));
			ret.setUploadedPhoto(new UploadMobileDao().search(String.valueOf(ret.getSyskey()), ret.getT3(), conn).getData());
			if (ret.getT3().equalsIgnoreCase("video")) {
				ret.setVideoUpload(
						new UploadMobileDao().searchVideoList(String.valueOf(ret.getSyskey()), conn).getVideoUpload());
			}
			list.add(ret);
		}
		return list;
	}

	// get commentsk
	public ArrayList<ArticleData> getcommentsk(String n1, String userKey, Connection conn) throws SQLException {
		ArrayList<ArticleData> res = new ArrayList<ArticleData>();
		String sql = "Select syskey from FMR003 WHERE RecordStatus<>4 AND RecordStatus = 1 AND n1 = " + n1 + "";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			ArticleData ret = new ArticleData();
			ret.setSyskey(rs.getLong("syskey"));
			res.add(ret);
		}
		return res;

	}

	public ArticleData getDBRecord(DBRecord adbr) {
		ArticleData ret = new ArticleData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("createddate"));
		ret.setCreatedTime(adbr.getString("createdtime"));
		ret.setModifiedDate(adbr.getString("modifieddate"));
		ret.setModifiedTime(adbr.getString("modifiedtime"));
		ret.setUserId(adbr.getString("userid"));
		ret.setUserName(adbr.getString("username"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setUserSyskey(adbr.getLong("usersyskey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		ret.setT8(adbr.getString("t8"));
		ret.setT9(adbr.getString("t9"));
		ret.setT10(adbr.getString("t10"));
		ret.setT11(adbr.getString("t11"));
		ret.setT12(adbr.getString("t12"));
		ret.setT13(adbr.getString("t13"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getLong("n5"));
		ret.setN6(adbr.getLong("n6"));
		ret.setN7(adbr.getLong("n7"));
		ret.setN8(adbr.getLong("n8"));
		ret.setN9(adbr.getLong("n9"));
		ret.setN10(adbr.getLong("n10"));
		ret.setN11(adbr.getLong("n11"));
		ret.setN12(adbr.getLong("n12"));
		ret.setN13(adbr.getLong("n13"));
		return ret;
	}

	// for dislike
	public long getTotalDisLikeCount(long syskey, Connection conn) throws SQLException {
		long key = 0;
		String sql = " Select n8 from  FMR002 WHERE  RecordStatus = 1 AND syskey = " + syskey;
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = rs.getLong("n8");
		}
		return key;
	}

	/*
	 * public Result reduceCommentCount(Long syskey, Connection conn) throws
	 * SQLException { Result res = new Result(); String sql =
	 * "Update FMR002 SET n3 = n3-1 WHERE  RecordStatus = 1 AND RecordStatus<>4 AND syskey = ?"
	 * ; PreparedStatement stmt = conn.prepareStatement(sql); stmt.setLong(1,
	 * syskey); int rs = stmt.executeUpdate(); if (rs > 0) { res.setState(true);
	 * } return res; }
	 */

	/*
	 * public Result reduceComment(Long syskey, Connection conn) throws
	 * SQLException { Result res = new Result(); String sql =
	 * "Update FMR002 SET n3 = n3-1 WHERE  RecordStatus = 1 AND n3 > 0 AND syskey = ?"
	 * ; PreparedStatement stmt = conn.prepareStatement(sql); stmt.setLong(1,
	 * syskey); int rs = stmt.executeUpdate(); if (rs > 0) { res.setState(true);
	 * } return res; }
	 */

	/* getTotalLikeCount */
	public long getTotalLikeCount(long syskey, Connection conn) throws SQLException {
		long key = 0;
		String sql = " Select n2 from  FMR002 WHERE  RecordStatus = 1 AND syskey = " + syskey;
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = rs.getLong("n2");
		}
		return key;
	}

	public ResultMobile insert(ArticleData obj, Connection conn) throws SQLException {
		ResultMobile res = new ResultMobile();
		String query = "INSERT INTO FMR002 (syskey, createddate, modifieddate, userid, username,"
				+ " recordStatus, syncStatus, syncBatch,usersyskey,t1, t2, t3, t4, t5, t6, t7, t8, t9, t10,"
				+ " t11, t12, t13,n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13,createdtime,modifiedtime)"
				+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?," + "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		try {
			if (!isCodeExist(obj, conn)) {
				PreparedStatement ps = conn.prepareStatement(query);
				ps.setLong(1, obj.getSyskey());
				ps.setString(2, obj.getCreatedDate());
				ps.setString(3, obj.getModifiedDate());
				ps.setString(4, obj.getUserId());
				ps.setString(5, obj.getUserName());
				ps.setInt(6, obj.getRecordStatus());
				ps.setInt(7, obj.getSyncStatus());
				ps.setLong(8, obj.getSyncBatch());
				ps.setLong(9, obj.getUserSyskey());
				ps.setString(10, obj.getT1());
				ps.setString(11, obj.getT2());
				ps.setString(12, obj.getT3());
				ps.setString(13, obj.getT4());
				ps.setString(14, obj.getT5());
				ps.setString(15, obj.getT6());
				ps.setString(16, obj.getT7());
				ps.setString(17, obj.getT8());
				ps.setString(18, obj.getT9());
				ps.setString(19, obj.getT10());
				ps.setString(20, obj.getT11());
				ps.setString(21, obj.getT12());
				ps.setString(22, obj.getT13());
				ps.setLong(23, obj.getN1());
				ps.setLong(24, obj.getN2());
				ps.setLong(25, obj.getN3());
				ps.setLong(26, obj.getN4());
				ps.setLong(27, obj.getN5());
				ps.setLong(28, obj.getN6());
				ps.setLong(29, obj.getN7());
				ps.setLong(30, obj.getN8());
				ps.setLong(31, obj.getN9());
				ps.setLong(32, obj.getN10());
				ps.setLong(33, obj.getN11());
				ps.setLong(34, obj.getN12());
				ps.setLong(35, obj.getN13());
				ps.setString(36, obj.getCreatedTime());
				ps.setString(37, obj.getModifiedTime());

				if (ps.executeUpdate() > 0) {
					res.setState(true);
					res.setMsgDesc("Saved Successfully!");
				} else {
					res.setMsgDesc("Article Already Exist!");
				}
			}

		} catch (SQLException e) {

		}

		return res;

	}

	/*
	 * public ArticleDataSet searchLikeArticle(String type, long key, Connection
	 * conn) throws SQLException { ArticleDataSet res = new ArticleDataSet();
	 * ArrayList<ArticleData> datalist = new ArrayList<ArticleData>(); String
	 * whereclause = " WHERE RecordStatus<>4 AND RecordStatus = 1 AND t3 = '" +
	 * type + "'  AND n1 = " + key + " "; ArrayList<DBRecord> dbrs =
	 * DBMgr.getDBRecords(define("FMR008"), whereclause, " ORDER BY syskey ",
	 * conn); for (int i = 0; i < dbrs.size(); i++) {
	 * datalist.add(getDBRecord(dbrs.get(i))); } if (datalist.size() > 0) {
	 * res.setState(true); } else { res.setState(false); } ArticleData[]
	 * dataarray = new ArticleData[datalist.size()]; dataarray =
	 * datalist.toArray(dataarray); res.setData(dataarray); return res; }
	 */

	public ResultMobile insertTemp(ArticleData obj, Connection conn) throws SQLException {
		ResultMobile res = new ResultMobile();
		String sql = DBMgr.insertString(defines("Temp_All"), conn);
		PreparedStatement stmt = conn.prepareStatement(sql);
		DBRecord dbr = setDBRecord(obj);
		DBMgr.setValues(stmt, dbr);
		int count = stmt.executeUpdate();
		if (count > 0) {
			res.setState(true);
		}
		return res;
	}

	/* isCodeExist */
	public boolean isCodeExist(ArticleData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				" where RecordStatus<>4 AND  RecordStatus = 1 AND syskey = " + obj.getSyskey() + " ", "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	/*
	 * public ArticleDataSet searchLikeCount(String id, Connection conn, MrBean
	 * user) throws SQLException { ArticleDataSet res = new ArticleDataSet();
	 * ArrayList<ArticleData> datalist = new ArrayList<ArticleData>(); long
	 * syskey = Long.parseLong(id); //long userKey =
	 * OPTDao.searchByID(user.getUser().getUserId(), conn); String whereclause =
	 * " WHERE RecordStatus<>4 AND RecordStatus = 1 AND syskey = '" + syskey +
	 * "'   "; ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR002"),
	 * whereclause, " ORDER BY syskey ", conn); for (int i = 0; i < dbrs.size();
	 * i++) { datalist.add(getDBRecord(dbrs.get(i))); } if (datalist.size() > 0)
	 * { res.setState(true); } else { res.setState(false); } ArticleData[]
	 * dataarray = new ArticleData[datalist.size()]; dataarray =
	 * datalist.toArray(dataarray); res.setData(dataarray); return res; }
	 */

	/*
	 * public long searchLikeUser(long n1, Connection conn, MrBean user) throws
	 * SQLException { long key; long userKey =
	 * OPTDao.searchByID(user.getUser().getUserId(), conn); String sql =
	 * "Select n2 from FMR008 WHERE RecordStatus<>4 AND RecordStatus = 1 AND n1 = "
	 * + n1 + " AND n2 = '"+ userKey + "'"; PreparedStatement stmt =
	 * conn.prepareStatement(sql); ResultSet rs = stmt.executeQuery(); if
	 * (rs.next()) { key = rs.getLong("n2"); } else { key = 0; } return key; }
	 */

	/* isDisLikeExist */
	public boolean isDisLikeExist(Long syskey, Long userkey, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR008"),
				" where RecordStatus<>4 AND t4='dislike' AND RecordStatus = 1 AND n1 = " + syskey + " AND n2 = "
						+ userkey + " ",
				"", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	/* isLikeExist */
	public boolean isLikeExist(Long syskey, Long userkey, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR008"),
				" where RecordStatus<>4 AND t4='like' AND RecordStatus = 1 AND n1 = " + syskey + " AND n2 = " + userkey
						+ " ",
				"", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	/* isStatusExist */
	public boolean isStatusExist(Long syskey, Long userkey, String type, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR008"),
				" where RecordStatus = 4 AND t4='" + type + "'  AND n1 = " + syskey + " AND n2 = " + userkey + " ", "",
				conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public String MMddyyyFormat(String aDate, String month)// format(Feb
	{// for all list
		String l_Date = "";
		if (!aDate.equals("") && aDate != null)
			l_Date = month + " " + aDate.substring(6) + ", " + aDate.substring(0, 4);
		return l_Date;
	}

	/* PrivateQuestionList */
	public ArticleDataSet PrivatePostList(PagerMobileData pgdata, String userSK, String type, Connection conn, MrBean user)
			throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		String sql = "";
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		datalist = new ArrayList<ArticleData>();
		sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey) AS RowNum,* FROM FMR002 where" + " (t3 = '"
				+ type + "') AND n7=5 AND  recordstatus<>4 and n5='" + userSK + "' " + " )AS RowConstrainedResult  "
				+ " WHERE RowNum >= " + pgdata.getStart() + " and RowNum <= " + pgdata.getEnd();
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ArticleData data = new ArticleData();
			data.setSyskey(rs.getLong("syskey"));
			data.setCreatedDate(rs.getString("createddate"));
			data.setModifiedDate(rs.getString("modifieddate"));
			data.setUserId(rs.getString("userid"));
			data.setUserName(rs.getString("username"));
			data.setT1(rs.getString("t1"));
			data.setT2(rs.getString("t2"));
			data.setT3(rs.getString("t3"));
			data.setN1(rs.getLong("n1"));
			data.setN2(rs.getLong("n2"));
			data.setN3(rs.getLong("n3"));
			data.setN5(rs.getLong("n5"));
			data.setN8(rs.getLong("n8"));
			data.setCreatedTime(rs.getString("createdtime"));
			data.setModifiedTime(rs.getString("modifiedtime"));
			data.setUploadedPhoto(new UploadMobileDao().search(String.valueOf(data.getSyskey()), "", conn).getData());
			data.setN6(searchLikeOrNot(data.getSyskey(), String.valueOf(data.getN5()), conn));
			new ContentMenuDao();
			data.setN7(
					ContentMenuDao.searchSaveContentOrNot(String.valueOf(data.getN5()), data.getSyskey(), conn));
			// data.setN9(ArticleDao.searchDisLikeOrNot(data.getN1(),String.valueOf(data.getN5()),
			// conn));
			data.setComData(new CommentMobileDao().searchLatest(data.getSyskey(), conn));
			datalist.add(data);
		}
		res.setPageSize(pgdata.getSize());
		res.setCurrentPage(pgdata.getCurrent());
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		sql = " SELECT count(*) AS records FROM FMR002 WHERE RecordStatus <> 4  AND  RecordStatus = 1  AND t3 = '"
				+ type + "' AND n7=5 AND n5='" + userSK + "'";
		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();
		if (rs.next()) {
			res.setTotalCount(rs.getInt("records"));
		}
		return res;
	}

	/*
	 * public CommentDataSet searchLikeUserList(String n1, Connection conn)
	 * throws SQLException { CommentDataSet res = new CommentDataSet();
	 * ArrayList<CommentData> datalist = new ArrayList<CommentData>(); String
	 * sql = " Select t1,t2,n2 from FMR008 WHERE RecordStatus<>4  AND n1 = " +
	 * n1; PreparedStatement stmt = conn.prepareStatement(sql); ResultSet rs =
	 * stmt.executeQuery(); while (rs.next()) { RegisterData uData =
	 * RegisterDao.readData(rs.getLong("n2"), conn); CommentData data = new
	 * CommentData(); data.setUserId(rs.getString("t1"));
	 * data.setUserName(uData.getT3()); data.setUserSyskey(rs.getLong("n2"));
	 * datalist.add(data); } CommentData[] dataarray = new
	 * CommentData[datalist.size()]; dataarray = datalist.toArray(dataarray);
	 * res.setData(dataarray); return res; }
	 */

	/* readAll */
	public ArrayList<ArticleData> readAll(String whereclause, Connection conn) throws SQLException {
		ArrayList<ArticleData> ret = new ArrayList<ArticleData>();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereclause, "", conn);
		for (int i = 0; i < dbrs.size(); i++) {
			ret.add(getDBRecord(dbrs.get(i)));
		}
		return ret;
	}

	// TDA for Q and A
	/*
	 * public long searchLikeOrNotsQuestion(String n1, String userKey, Long
	 * comsk, Connection conn)throws SQLException { long key = 0; String sql =
	 * "Select * from FMR016 WHERE RecordStatus<>4 AND RecordStatus = 1 AND n1 = "
	 * + n1 + " AND n2 = '"+ userKey + "'   AND n3 = '" + comsk + "'";
	 * PreparedStatement stmt = conn.prepareStatement(sql); ResultSet rs =
	 * stmt.executeQuery(); if (rs.next()) { key = 1; } else { key = 0; } return
	 * key; }
	 */

	// TDA for Q and A
	/*
	 * public long searchLikeOrNotsQuestionSaveC(String n1, String userKey, Long
	 * comsk, Connection conn)throws SQLException { long key = 0; String sql =
	 * "Select * from FMR016 WHERE RecordStatus<>4 AND RecordStatus = 1 AND n1 = "
	 * + n1 + " AND n2 = '"+ userKey + "'   AND n3 = '" + comsk + "'";
	 * PreparedStatement stmt = conn.prepareStatement(sql); ResultSet rs =
	 * stmt.executeQuery(); if (rs.next()) { key = 1; } else { key = 0; } return
	 * key; }
	 */

	public ArticleData readByID(long aObj, Connection conn) throws SQLException {
		ArticleData data = new ArticleData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "where RecordStatus=1 AND syskey='" + aObj + "'", "",
				conn);
		if (dbrs.size() > 0) {
			data = getDBRecord(dbrs.get(0));
			data.setT2(ServerUtil.decryptPIN(data.getT2()));
		}
		return data;
	}

	/* readByKey */
	public ArticleData readByKey(long syskey, Connection conn) throws SQLException {
		ArticleData ret = new ArticleData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "where  RecordStatus = 1 AND syskey=" + syskey, "",
				conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));
		return ret;
	}

	// TDA For Video
	/*
	 * public long searchLikeOrNotsVideo(String n1, String userKey, Connection
	 * conn) throws SQLException { long key = 0; String sql =
	 * "Select * from FMR003 WHERE RecordStatus<>4 AND RecordStatus = 1 AND n1 = "
	 * + n1 + " AND n5 = '"+ userKey + "'"; PreparedStatement stmt =
	 * conn.prepareStatement(sql); ResultSet rs = stmt.executeQuery(); if
	 * (rs.next()) { key = 1; } else { key = 0; } return key; }
	 */

	/* readForShare */
	public ArticleData readForShare(long postsk, String type, Connection conn) throws SQLException {
		ArticleData ret = new ArticleData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				"where RecordStatus<>4 AND  t3='" + type + "' AND syskey=" + postsk, "", conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));
		return ret;
	}

	public ArticleData readID(long aObj, Connection conn) throws SQLException {
		ArticleData data = new ArticleData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "where RecordStatus=1 AND syskey='" + aObj + "'", "",
				conn);
		if (dbrs.size() > 0) {

			data = getDBRecord(dbrs.get(0));
		}
		return data;
	}

	/* readShare */
	public ArticleDataSet readShare(long postsk, String type, Connection conn) {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		String whereclause = " WHERE RecordStatus<>4 AND RecordStatus = 1 AND syskey = '" + postsk + "' and t3='" + type
				+ "' ";
		ArrayList<DBRecord> dbrs;
		try {
			dbrs = DBMgr.getDBRecords(define(), whereclause, " ORDER BY syskey ", conn);
			for (int i = 0; i < dbrs.size(); i++) {
				datalist.add(getDBRecord(dbrs.get(i)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}
	//////////////////////////////////////////////// Mobile////////////////////////////////////////////////////

	/*
	 * public boolean isExistPerson(long sk,String date,Connection conn){
	 * boolean isExist = false; String sql = ""; PreparedStatement ps; ResultSet
	 * rs; try { sql =
	 * " SELECT t11 FROM FMR002 WHERE RECORDSTATUS <> 4 AND and t11='"+date+
	 * "' and syskey = "+sk ; ps = conn.prepareStatement(sql); rs =
	 * ps.executeQuery(); if(rs.next()){ isExist = true; } } catch (SQLException
	 * e) { e.printStackTrace(); } return isExist; }
	 */

	// searchArticleListByContentWriter
	public ArrayList<ArticleData> searchArticleListByContentWriter(String usersk, String userid, String startdate,
			String enddate, Connection conn, PagerMobileData pgdata, MrBean user) throws SQLException {

		ArrayList<ArticleData> list = new ArrayList<ArticleData>();
		String whereClause = "";
		if (!startdate.equalsIgnoreCase("")) {
			String[] sparts = startdate.split("/");
			String spart1 = sparts[0]; // day
			String spart2 = sparts[1]; // month
			String spart3 = sparts[2]; // year
			startdate = spart3 + spart2 + spart1;
		}

		if (!enddate.equalsIgnoreCase("")) {
			String[] eparts = enddate.split("/");
			String epart1 = eparts[0]; // day
			String epart2 = eparts[1]; // month
			String epart3 = eparts[2]; // year
			enddate = epart3 + epart2 + epart1;
		}
		if (!userid.equalsIgnoreCase("") && !startdate.equalsIgnoreCase("") && !enddate.equalsIgnoreCase("")) {
			whereClause = " AND userid='" + userid + "' AND createddate>=" + startdate + " AND createddate<=" + enddate;
		}

		else if (!userid.equalsIgnoreCase("") && startdate.equalsIgnoreCase("") && enddate.equalsIgnoreCase("")) {
			whereClause = " AND userid='" + userid + "'";
		}

		else if (!userid.equalsIgnoreCase("") && startdate.equalsIgnoreCase("") && !enddate.equalsIgnoreCase("")) {
			whereClause = " AND userid='" + userid + "' AND createddate=" + enddate;
		}

		else if (!userid.equalsIgnoreCase("") && !startdate.equalsIgnoreCase("") && enddate.equalsIgnoreCase("")) {
			whereClause = " AND userid='" + userid + "' AND createddate=" + startdate;
		}

		else if (userid.equalsIgnoreCase("") && !startdate.equalsIgnoreCase("") && !enddate.equalsIgnoreCase("")) {
			whereClause = " AND createddate>=" + startdate + " AND createddate<=" + enddate;
		}

		else if (userid.equalsIgnoreCase("") && !startdate.equalsIgnoreCase("") && enddate.equalsIgnoreCase("")) {
			whereClause = " AND createddate=" + startdate;
		}

		else if (userid.equalsIgnoreCase("") && startdate.equalsIgnoreCase("") && !enddate.equalsIgnoreCase("")) {
			whereClause = " AND createddate=" + enddate;
		}

		else if (userid.equalsIgnoreCase("") && startdate.equalsIgnoreCase("") && enddate.equalsIgnoreCase("")) {
			whereClause = "";
		}

		String sql = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey DESC) AS RowNum,* FROM ( SELECT * FROM fmr002 WHERE RecordStatus<>4 AND n7=5 "
				+ whereClause + ") b)  AS RowConstrainedResult WHERE RowNum >= '" + (pgdata.getStart())
				+ "' AND RowNum <= '" + pgdata.getEnd() + "' ";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ArticleData ret = new ArticleData();
			ret.setSyskey(rs.getLong("syskey"));
			ret.setCreatedDate(rs.getString("createddate"));
			ret.setCreatedTime(rs.getString("createdtime"));
			ret.setModifiedDate(rs.getString("modifieddate"));
			ret.setModifiedTime(rs.getString("modifiedtime"));
			ret.setUserId(rs.getString("userid"));
			ret.setUserName(rs.getString("username"));
			ret.setRecordStatus(rs.getInt("RecordStatus"));
			ret.setSyncStatus(rs.getInt("SyncStatus"));
			ret.setSyncBatch(rs.getInt("SyncBatch"));
			ret.setUserSyskey(rs.getLong("syskey"));
			ret.setT1(rs.getString("t1"));
			ret.setT2(rs.getString("t2"));
			ret.setT3(rs.getString("t3"));
			ret.setT4(rs.getString("t4"));
			ret.setT5(rs.getString("t5"));
			ret.setT6(rs.getString("t6"));
			ret.setT7(rs.getString("t7"));
			ret.setT8(rs.getString("t8"));
			ret.setT9(rs.getString("t9"));
			ret.setT10(rs.getString("t10"));
			ret.setT11(rs.getString("t11"));
			ret.setT12(rs.getString("t12"));
			ret.setT13(rs.getString("t13"));
			ret.setN1(rs.getLong("n1"));
			ret.setN2(rs.getLong("n2"));
			ret.setN3(rs.getLong("n3"));
			ret.setN4(rs.getLong("n4"));
			ret.setN5(rs.getLong("n5"));
			ret.setN6(new ArticleMobileDao().searchLikeOrNot(ret.getSyskey(), usersk, conn)); // like
			new ContentMenuDao();
			ret.setN7(ContentMenuDao.searchSaveContentOrNot(usersk, ret.getSyskey(), conn));// 1=savecontent,0=unsavecontent
			ret.setAnswer(new CommentMobileDao().searchAnswer(ret.getSyskey(), "answer", conn).getAnsData());// getAnswerData
			ret.setN8(rs.getLong("n8"));
			ret.setN9(new ArticleMobileDao().searchDisLikeOrNot(ret.getSyskey(), usersk, conn));
			ret.setN10(rs.getLong("n10"));
			ret.setN11(rs.getLong("n11"));
			ret.setN12(rs.getLong("n12"));
			ret.setN13(rs.getLong("n13"));
			ret.setUploadedPhoto(new UploadMobileDao().search(String.valueOf(ret.getSyskey()), ret.getT3(), conn).getData());
			if (ret.getT3().equalsIgnoreCase("video")) {
				ret.setVideoUpload(
						new UploadMobileDao().searchVideoList(String.valueOf(ret.getSyskey()), conn).getVideoUpload());
			}
			list.add(ret);
		}
		return list;
	}

	/* searchArticles */
	public ArticleDataSet searchArticles(PagerMobileData pgdata, String mobile, String firstRefresh, String searchVal,
			String type, String usersk, Connection conn, MrBean user) throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		String sql = "";
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		datalist = new ArrayList<ArticleData>();
		sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey) AS RowNum,* FROM FMR002 where" + " (t3 = '"
				+ type + "') AND n7=5 AND recordstatus<>4 and "
				+ " (modifieddate between (CONVERT(NVARCHAR(8),DATEADD(MONTH,-1, GETDATE()),112)) AND "
				+ " (CONVERT(NVARCHAR(8),GETDATE(),112))))AS RowConstrainedResult  " + " WHERE RowNum >= "
				+ pgdata.getStart() + " and RowNum <= " + pgdata.getEnd() + " order by syskey";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ArticleData data = new ArticleData();
			data.setSyskey(rs.getLong("syskey"));
			data.setCreatedDate(rs.getString("createddate"));
			data.setModifiedDate(rs.getString("modifieddate"));
			data.setT1(rs.getString("t1"));
			data.setT2(rs.getString("t2"));
			data.setT3(rs.getString("t3"));
			data.setT10(rs.getString("t10"));
			data.setT13(rs.getString("t13"));
			data.setN1(rs.getLong("n1"));
			data.setN2(rs.getLong("n2"));
			data.setN3(rs.getLong("n3"));
			data.setN5(rs.getLong("n5"));
			data.setN8(rs.getLong("n8")); // dislike count
			data.setCreatedTime(rs.getString("createdtime"));
			data.setModifiedTime(rs.getString("modifiedtime"));
			data.setUploadedPhoto(new UploadMobileDao().search(String.valueOf(data.getSyskey()), "", conn).getData());
			data.setN6(searchLikeOrNot(data.getSyskey(), usersk, conn));
			new ContentMenuDao();
			data.setN7(ContentMenuDao.searchSaveContentOrNot(usersk, data.getSyskey(), conn));
			datalist.add(data);
		}
		res.setPageSize(pgdata.getSize());
		res.setCurrentPage(pgdata.getCurrent());
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		sql = " SELECT count(*) AS records FROM FMR002 WHERE RecordStatus <> 4  AND  RecordStatus = 1  AND t3 = '"
				+ type + "' AND n7=5 AND  (modifieddate between (CONVERT(NVARCHAR(8),DATEADD(MONTH,-1, GETDATE()),112))"
				+ "AND  (CONVERT(NVARCHAR(8),GETDATE(),112)))";
		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();
		if (rs.next()) {
			res.setTotalCount(rs.getInt("records"));
		}
		return res;

	}

	public String searchByPh(String mobile, Connection conn) {
		String sqlString = "";
		String sender = "";
		sqlString = "select t9 from Register where RecordStatus<>4 AND t1='" + mobile + "'";
		try {
			Statement stmts = conn.createStatement();
			ResultSet rs = stmts.executeQuery(sqlString);
			if (rs.next()) {
				sender = rs.getString("t9");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return sender;
	}

	public long searchCommentCount(long syskey, Connection conn) throws SQLException {
		long key = 0;
		String sql = "Select n3 from FMR002 WHERE  RecordStatus = 1 AND syskey = " + syskey + " ";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = rs.getLong("n3");
		} else {
			key = 0;
		}
		return key;
	}

	public long searchCommentReplyCount(long syskey, Connection conn) throws SQLException {
		long key = 0;
		String sql = "Select n3 from FMR003 WHERE  RecordStatus = 1 AND syskey = " + syskey + " ";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = rs.getLong("n3");
		} else {
			key = 0;
		}
		return key;
	}

	/* searchDisLikeOrNot */
	public long searchDisLikeOrNot(long n1, String userSk, Connection conn) throws SQLException {
		long key = 0;
		String sql = "Select n2 from FMR008 WHERE RecordStatus<>4 AND RecordStatus = 1 AND t4='dislike' AND n1 = " + n1
				+ " AND n2 = '" + userSk + "'";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = 1;
		} else {
			key = 0;
		}
		return key;
	}

	public RegisterDataSet getArticleLikePerson(long key, Connection conn) throws SQLException {// String type,
		RegisterDataSet res = new RegisterDataSet();
		ArrayList<RegisterData> datalist = new ArrayList<RegisterData>();
		String query = "Select a.userName As userName,a.t16 As photo from  Register a inner join FMR008 b on a.syskey=b.n2 WHERE b.RecordStatus<>4 AND b.RecordStatus = 1 AND b.n1=? ORDER BY a.syskey desc";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setLong(1, key);
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
		RegisterData regdata = new RegisterData();
		regdata.setUsername(rs.getString("userName"));
		regdata.setT16(rs.getString("photo"));
		datalist.add(regdata);
		}			
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		RegisterData[] dataarray = new RegisterData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}
	
	public RegisterDataSet getCommentLikePerson(long key, Connection conn) throws SQLException {// String type,
		RegisterDataSet res = new RegisterDataSet();
		ArrayList<RegisterData> datalist = new ArrayList<RegisterData>();
		String query = "select reg.username As userName,reg.t16 As photo from Register reg inner join FMR017 fmr1 on reg.syskey=fmr1.n1 inner join FMR003 fmr2 on fmr1.n2 = fmr2.syskey where fmr2.syskey=? ORDER BY fmr2.syskey desc";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setLong(1, key);
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
		RegisterData regdata = new RegisterData();
		regdata.setUsername(rs.getString("userName"));
		regdata.setT16(rs.getString("photo"));
		datalist.add(regdata);
		}			
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		RegisterData[] dataarray = new RegisterData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	/* searchlikecount */
	public long searchlikecount(long syskey, Connection conn) throws SQLException {
		long key = 0;
		String sql = "Select n2 from FMR002 WHERE  RecordStatus = 1 AND syskey = " + syskey + " ";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = rs.getLong("n2");
		} else {
			key = 0;
		}
		return key;
	}

	/* searchLikeOrNot */
	public long searchLikeOrNot(long n1, String userSk, Connection conn) throws SQLException {
		long key = 0;
		String sql = " Select n2 from FMR008 WHERE RecordStatus<>4 AND RecordStatus = 1 AND t4='like' AND n1 = " + n1
				+ " AND n2 = '" + userSk + "'";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = 1;
		} else {
			key = 0;
		}
		return key;
	}

	/* searchLikeOrNotsQuestionSaveCommen */
	public ArrayList<ArticleData> searchLikeOrNotsQuestionSaveCommen(String n1, String userKey, Long comsk,
			Connection conn) throws SQLException {
		ArrayList<ArticleData> res = new ArrayList<ArticleData>();
		String sql = "Select * from FMR016 WHERE RecordStatus<>4 AND RecordStatus = 1 AND n1 = " + n1 + " AND n2 = '"
				+ userKey + "'   AND n3 = '" + comsk + "'";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			ArticleData ret = new ArticleData();
			ret.setSyskey(1);
			res.add(ret);
		}
		return res;

	}

	/* question list */
	/*
	 * public ArticleDataSet searchQnA(PagerData pgdata, String mobile, String
	 * firstRefresh, String searchVal,String type, String check, String usersk,
	 * Connection conn, MrBean user) throws SQLException { ArticleDataSet res =
	 * new ArticleDataSet(); RegisterData sender =
	 * RegisterDao.searchByPh(mobile, conn); String sql = "";
	 * ArrayList<ArticleData> datalist = new ArrayList<ArticleData>(); if
	 * (firstRefresh.equalsIgnoreCase("1")) { ArticleDao.deleteTemp(usersk,
	 * conn); datalist = new ArrayList<ArticleData>(); datalist =
	 * ArticleDao.readAll(
	 * " WHERE RecordStatus <> 4  AND  RecordStatus = 1 AND n7=5 AND t3 = '" +
	 * type + "' " +
	 * " AND syskey NOT IN ( SELECT n1 FROM jun004 WHERE recordstatus <> 4 AND n2 IN (SELECT syskey "
	 * + " FROM fmr012 WHERE t2 = '" + sender.getT9() +
	 * "' AND t3 = '0' AND recordstatus <> 4)) ",conn); for (int i = 0; i <
	 * datalist.size(); i++) {
	 * datalist.get(i).setN1(datalist.get(i).getSyskey());
	 * datalist.get(i).setSyskey(SysKeyMgr.getSysKey(1, "syskey",
	 * ConnAdmin.getConn(user.getUser().getOrganizationID())));
	 * datalist.get(i).setN9(Long.parseLong(usersk));
	 * ArticleDao.insertTemp(datalist.get(i), conn); } if
	 * (type.equalsIgnoreCase("article")) { for (int i = 0; i < datalist.size();
	 * i++) { ArrayList<UploadData> photolist = new ArrayList<UploadData>();
	 * photolist = UploadDao.readByN1(datalist.get(i).getSyskey(), conn);
	 * datalist.get(i).setUploadDatalist(photolist); } } if
	 * (!sender.getT9().equalsIgnoreCase("")) { datalist = new
	 * ArrayList<ArticleData>(); datalist = ArticleDao.readAll(
	 * " WHERE RecordStatus <> 4  AND  RecordStatus = 1  AND n7=5 AND t3 = '" +
	 * type+ "' " +
	 * " AND syskey IN ( SELECT n1 FROM jun004 WHERE recordstatus <> 4 AND n2 IN (SELECT syskey "
	 * + " FROM fmr012 WHERE t2 = '" + sender.getT9() +
	 * "' AND t3 = '0' AND recordstatus <> 4))", conn); for (int i = 0; i <
	 * datalist.size(); i++) {
	 * datalist.get(i).setN1(datalist.get(i).getSyskey());
	 * datalist.get(i).setSyskey(SysKeyMgr.getSysKey(1, "syskey",
	 * ConnAdmin.getConn(user.getUser().getOrganizationID())));
	 * datalist.get(i).setN9(Long.parseLong(usersk));
	 * ArticleDao.insertTemp(datalist.get(i), conn); } if
	 * (type.equalsIgnoreCase("article")) { for (int i = 0; i < datalist.size();
	 * i++) { ArrayList<UploadData> photolist = new ArrayList<UploadData>();
	 * photolist = UploadDao.readByN1(datalist.get(i).getSyskey(), conn);
	 * datalist.get(i).setUploadDatalist(photolist); } } } } datalist = new
	 * ArrayList<ArticleData>(); sql =
	 * " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey) AS RowNum,* FROM Temp_All where"
	 * + " (t3 = '" + type + "') AND n7=5 AND   n9='"+usersk+"' AND" +
	 * " (modifieddate between (CONVERT(NVARCHAR(8),DATEADD(MONTH,-1, GETDATE()),112)) AND "
	 * + " (CONVERT(NVARCHAR(8),GETDATE(),112))))AS RowConstrainedResult  " +
	 * " WHERE RowNum >= " + pgdata.getStart() + " and RowNum <= " +
	 * pgdata.getEnd(); PreparedStatement ps = conn.prepareStatement(sql);
	 * ResultSet rs = ps.executeQuery(); while (rs.next()) { ArticleData data =
	 * new ArticleData(); data.setSyskey(rs.getLong("syskey"));
	 * data.setCreatedDate(rs.getString("createddate"));
	 * data.setModifiedDate(rs.getString("modifieddate"));
	 * data.setUserId(rs.getString("userid"));
	 * data.setUserName(rs.getString("username"));
	 * data.setT1(rs.getString("t1")); data.setT2(rs.getString("t2"));
	 * data.setT3(rs.getString("t3")); data.setN1(rs.getLong("n1"));
	 * data.setN2(rs.getLong("n2")); data.setN3(rs.getLong("n3"));
	 * data.setN5(rs.getLong("n5")); data.setN8(rs.getLong("n8")); // dislike
	 * count data.setCreatedTime(rs.getString("createdtime"));
	 * data.setModifiedTime(rs.getString("modifiedtime")); datalist.add(data); }
	 * res.setPageSize(pgdata.getSize());
	 * res.setCurrentPage(pgdata.getCurrent()); if (datalist.size() > 0) {
	 * res.setState(true); } else { res.setState(false); } ArticleData[]
	 * dataarray = new ArticleData[datalist.size()]; dataarray =
	 * datalist.toArray(dataarray); res.setData(dataarray); sql =
	 * " SELECT count(*) AS records FROM FMR002 WHERE RecordStatus <> 4  AND  RecordStatus = 1  AND t3 = '"
	 * + type +
	 * "' AND n7=5 and (modifieddate between (CONVERT(NVARCHAR(8),DATEADD(MONTH,-1, GETDATE()),112)) AND  (CONVERT(NVARCHAR(8),GETDATE(),112)))"
	 * ; ps = conn.prepareStatement(sql); rs = ps.executeQuery(); if (rs.next())
	 * { res.setTotalCount(rs.getInt("records")); } return res; }
	 */

	public ArrayList<ArticleData> searchLikeOrNotsQuestionSaveCommenReply(String n1, String userKey, Long comsk,
			Connection conn) throws SQLException {
		ArrayList<ArticleData> res = new ArrayList<ArticleData>();
		String sql = "Select * from FMR018 WHERE RecordStatus<>4 AND RecordStatus = 1 AND n1 = " + n1 + " AND n2 = '"
				+ userKey + "'   AND n3 = '" + comsk + "'";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			ArticleData ret = new ArticleData();
			ret.setSyskey(1);
			res.add(ret);
		}
		return res;

	}

	/* searchLikesUser */
	public long searchLikesUser(long n1, long userKey, Connection conn, MrBean user) throws SQLException {
		long key;
		String sql = "Select n2 from FMR008 WHERE RecordStatus<>4 AND RecordStatus = 1 AND n1 = " + n1 + " AND n2 = '"
				+ userKey + "'";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = rs.getLong("n2");
		} else {
			key = 0;
		}
		return key;
	}

	public ArticleDataSet searchQnA(PagerMobileData pgdata, String mobile, String firstRefresh, String searchVal, String type,
			String check, String usersk, Connection conn, MrBean user) throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		// RegisterData sender = RegisterDao.searchByPh(mobile, conn);
		String sql = "";
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		/*
		 * if (firstRefresh.equalsIgnoreCase("1")) {
		 * ArticleDao.deleteTemp(usersk, conn); datalist = new
		 * ArrayList<ArticleData>(); datalist = ArticleDao.readAll(
		 * " WHERE RecordStatus <> 4  AND  RecordStatus = 1 AND n7=5 AND t3 = '"
		 * + type + "' " +
		 * " AND syskey NOT IN ( SELECT n1 FROM jun004 WHERE recordstatus <> 4 AND n2 IN (SELECT syskey "
		 * + " FROM fmr012 WHERE t2 = '" + sender.getT9() +
		 * "' AND t3 = '0' AND recordstatus <> 4)) ",conn); for (int i = 0; i <
		 * datalist.size(); i++) {
		 * datalist.get(i).setN1(datalist.get(i).getSyskey());
		 * datalist.get(i).setSyskey(SysKeyMgr.getSysKey(1, "syskey",
		 * ConnAdmin.getConn(user.getUser().getOrganizationID())));
		 * datalist.get(i).setN9(Long.parseLong(usersk));
		 * ArticleDao.insertTemp(datalist.get(i), conn); } if
		 * (type.equalsIgnoreCase("article")) { for (int i = 0; i <
		 * datalist.size(); i++) { ArrayList<UploadData> photolist = new
		 * ArrayList<UploadData>(); photolist =
		 * UploadDao.readByN1(datalist.get(i).getSyskey(), conn);
		 * datalist.get(i).setUploadDatalist(photolist); } } if
		 * (!sender.getT9().equalsIgnoreCase("")) { datalist = new
		 * ArrayList<ArticleData>(); datalist = ArticleDao.readAll(
		 * " WHERE RecordStatus <> 4  AND  RecordStatus = 1  AND n7=5 AND t3 = '"
		 * + type+ "' " +
		 * " AND syskey IN ( SELECT n1 FROM jun004 WHERE recordstatus <> 4 AND n2 IN (SELECT syskey "
		 * + " FROM fmr012 WHERE t2 = '" + sender.getT9() +
		 * "' AND t3 = '0' AND recordstatus <> 4))", conn); for (int i = 0; i <
		 * datalist.size(); i++) {
		 * datalist.get(i).setN1(datalist.get(i).getSyskey());
		 * datalist.get(i).setSyskey(SysKeyMgr.getSysKey(1, "syskey",
		 * ConnAdmin.getConn(user.getUser().getOrganizationID())));
		 * datalist.get(i).setN9(Long.parseLong(usersk));
		 * ArticleDao.insertTemp(datalist.get(i), conn); } if
		 * (type.equalsIgnoreCase("article")) { for (int i = 0; i <
		 * datalist.size(); i++) { ArrayList<UploadData> photolist = new
		 * ArrayList<UploadData>(); photolist =
		 * UploadDao.readByN1(datalist.get(i).getSyskey(), conn);
		 * datalist.get(i).setUploadDatalist(photolist); } } } }
		 */
		datalist = new ArrayList<ArticleData>();

		sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey) AS RowNum,* FROM FMR002 where" + " (t3 = '"
				+ type + "') AND n7=5 AND  recordstatus<>4 and"
				+ " (modifieddate between (CONVERT(NVARCHAR(8),DATEADD(MONTH,-1, GETDATE()),112)) AND "
				+ " (CONVERT(NVARCHAR(8),GETDATE(),112))))AS RowConstrainedResult  " + " WHERE RowNum >= "
				+ pgdata.getStart() + " and RowNum <= " + pgdata.getEnd();
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ArticleData data = new ArticleData();
			data.setSyskey(rs.getLong("syskey"));
			data.setCreatedDate(rs.getString("createddate"));
			data.setModifiedDate(rs.getString("modifieddate"));
			data.setUserId(rs.getString("userid"));
			data.setUserName(rs.getString("username"));
			data.setT1(rs.getString("t1"));
			data.setT2(rs.getString("t2"));
			data.setT3(rs.getString("t3"));
			data.setN1(rs.getLong("n1"));
			data.setN2(rs.getLong("n2"));
			data.setN3(rs.getLong("n3"));
			data.setN5(rs.getLong("n5"));
			data.setN8(rs.getLong("n8")); // dislike count
			data.setCreatedTime(rs.getString("createdtime"));
			data.setModifiedTime(rs.getString("modifiedtime"));
			data.setN6(new ArticleMobileDao().searchLikeOrNot(data.getSyskey(), usersk, conn));// like
																							new ContentMenuDao();
			// or
																							// not
			data.setN7(ContentMenuDao.searchSaveContentOrNot(usersk, data.getSyskey(), conn));// save
																									// content
																									// or
																									// not
			data.setComData(new CommentMobileDao().searchLatest(data.getSyskey(), conn));// comment
			data.setAnswer(new CommentMobileDao().searchAnswer(data.getSyskey(), "answer", conn).getAnsData());// admin
																											// answer
			data.setUploadedPhoto(new UploadMobileDao().search(String.valueOf(data.getSyskey()), "", conn).getData());// uploadedphoto
			datalist.add(data);
		}
		res.setPageSize(pgdata.getSize());
		res.setCurrentPage(pgdata.getCurrent());
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		sql = " SELECT count(*) AS records FROM FMR002 WHERE RecordStatus <> 4  AND  RecordStatus = 1  AND t3 = '"
				+ type
				+ "' AND n7=5 and (modifieddate between (CONVERT(NVARCHAR(8),DATEADD(MONTH,-1, GETDATE()),112)) AND  (CONVERT(NVARCHAR(8),GETDATE(),112)))";
		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();
		if (rs.next()) {
			res.setTotalCount(rs.getInt("records"));
		}
		return res;
	}

	/* mobile video list */
	public ArticleDataSet searchVideo(PagerMobileData pgdata, String mobile, String firstRefresh, String searchVal,
			String type, String check, String usersk, Connection conn, MrBean user) throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		String sql = "";
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		datalist = new ArrayList<ArticleData>();

		sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey) AS RowNum,* FROM FMR002 where" + " (t3 = '"
				+ type + "') AND n7=5 AND recordstatus<>4 and"
				+ " (modifieddate between (CONVERT(NVARCHAR(8),DATEADD(MONTH,-1, GETDATE()),112)) AND "
				+ " (CONVERT(NVARCHAR(8),GETDATE(),112))))AS RowConstrainedResult  " + " WHERE RowNum >= "
				+ pgdata.getStart() + " and RowNum <= " + pgdata.getEnd() + "order by syskey desc";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ArticleData data = new ArticleData();
			data.setSyskey(rs.getLong("syskey"));
			data.setCreatedDate(rs.getString("createddate"));
			data.setModifiedDate(rs.getString("modifieddate"));
			data.setT1(rs.getString("t1"));
			data.setT2(rs.getString("t2"));
			data.setT3(rs.getString("t3"));
			data.setT8(rs.getString("t8"));
			data.setN1(rs.getLong("n1"));
			data.setN2(rs.getLong("n2"));// like count
			data.setN3(rs.getLong("n3"));// comment count
			data.setN5(rs.getLong("n5"));
			data.setN8(rs.getLong("n8")); // dislike count
			data.setN10(rs.getLong("n10"));
			data.setCreatedTime(rs.getString("createdtime"));
			data.setModifiedTime(rs.getString("modifiedtime"));
			data.setN6(new ArticleMobileDao().searchLikeOrNot(data.getSyskey(), usersk, conn));// like
																							new ContentMenuDao();
			// or
																							// not
			data.setN7(ContentMenuDao.searchSaveContentOrNot(usersk, data.getSyskey(), conn));// save
																									// content
																									// or
																									// not
			data.setUploadedPhoto(new UploadMobileDao().search(String.valueOf(data.getSyskey()), "Video", conn).getData());// photo
																														// and
																														// video
			datalist.add(data);
		}
		res.setPageSize(pgdata.getSize());
		res.setCurrentPage(pgdata.getCurrent());
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		sql = " SELECT count(*) AS records FROM FMR002 WHERE RecordStatus <> 4  AND  RecordStatus = 1  AND t3 = '"
				+ type + "' ";
		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();
		if (rs.next()) {
			res.setTotalCount(rs.getInt("records"));
		}
		return res;
	}

	public DBRecord setDBRecord(ArticleData data) {
		DBRecord ret = define();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("createddate", data.getCreatedDate());
		ret.setValue("createdtime", data.getCreatedTime());
		ret.setValue("modifieddate", data.getModifiedDate());
		ret.setValue("modifiedtime", data.getModifiedTime());
		ret.setValue("userid", data.getUserId());
		ret.setValue("username", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("usersyskey", data.getUserSyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t5", data.getT5());
		ret.setValue("t6", data.getT6());
		ret.setValue("t7", data.getT7());
		ret.setValue("t8", data.getT8());
		ret.setValue("t9", data.getT9());
		ret.setValue("t10", data.getT10());
		ret.setValue("t11", data.getT11());
		ret.setValue("t12", data.getT12());
		ret.setValue("t13", data.getT13());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		ret.setValue("n6", data.getN6());
		ret.setValue("n7", data.getN7());
		ret.setValue("n8", data.getN8());
		ret.setValue("n9", data.getN9());
		ret.setValue("n10", data.getN10());
		ret.setValue("n11", data.getN11());
		ret.setValue("n12", data.getN12());
		ret.setValue("n13", data.getN13());
		return ret;
	}

	/* setStatusData */
	public ArticleData setStatusData(ArticleData data, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		String todaytime = new SimpleDateFormat("HH:mm a").format(Calendar.getInstance().getTime());
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(todaytime);
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(todaytime);
		data.setUserId(user.getUser().getUserId());
		data.setUserName(user.getUser().getUserName());
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		return data;
	}

	/* UnDislikeArticle */
	public ResultMobile UnDislikeArticle(Long syskey, Long userSK, Connection conn, MrBean user) throws SQLException {
		// long key = SysKeyMgr.getSysKey(1, "syskey",
		// ConnAdmin.getConn(user.getUser().getOrganizationID()));
		ResultMobile res = new ResultMobile();
		long userKey = userSK;
		if (isDisLikeExist(syskey, userKey, conn)) {
			res.setN1(searchLikeOrNot(syskey, String.valueOf(userSK), conn));
			String sql = "Update FMR002 SET n8 = n8-1 WHERE  RecordStatus = 1 AND syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String query = "UPDATE FMR008 SET RecordStatus=4 WHERE RecordStatus = 1 AND t4='dislike' AND n1 = '"
						+ syskey + "' AND n2 = '" + userKey + "'";
				PreparedStatement stm = conn.prepareStatement(query);
				int result = stm.executeUpdate();
				if (result > 0) {
					res.setState(true);
				} else {
					res.setState(false);
				}
			} else {
				res.setState(false);
			}
			res.setN2(getTotalLikeCount(syskey, conn));
			res.setKeyResult(getTotalDisLikeCount(syskey, conn));

		} else {

		}
		return res;
	}

	public ResultMobile UnDislikeQuestion(long syskey, long userSK, Connection conn, MrBean user) throws SQLException {
		// long key = SysKeyMgr.getSysKey(1, "syskey",
		// ConnAdmin.getConn(user.getUser().getOrganizationID()));
		ResultMobile res = new ResultMobile();
		long userKey = userSK;
		if (isDisLikeExist(syskey, userKey, conn)) {
			res.setN1(searchLikeOrNot(syskey, String.valueOf(userSK), conn));
			String sql = "Update FMR002 SET n2 = n2-1 WHERE  RecordStatus = 1 AND syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String query = "UPDATE FMR008 SET RecordStatus=4 WHERE RecordStatus = 1 AND t3='question' AND  n1 = '"
						+ syskey + "' AND n2 = '" + userKey + "'";
				PreparedStatement stm = conn.prepareStatement(query);

				int result = stm.executeUpdate();
				if (result > 0) {
					res.setState(true);
				} else {
					res.setState(false);
				}

			} else {
				res.setState(false);
			}
			res.setN2(getTotalLikeCount(syskey, conn));
			res.setKeyResult(getTotalDisLikeCount(syskey, conn));

		} else {

		}
		return res;
	}

	public ResultMobile UnDislikeVideo(Long syskey, Long userSK, Connection conn, MrBean user) throws SQLException {
		// long key = SysKeyMgr.getSysKey(1, "syskey",
		// ConnAdmin.getConn(user.getUser().getOrganizationID()));
		ResultMobile res = new ResultMobile();
		long userKey = userSK;
		String type = "Video";
		if (isDisLikeExist(syskey, userKey, conn)) {
			res.setN1(new ArticleMobileDao().searchLikeOrNot(syskey, String.valueOf(userSK), conn));
			String sql = "Update FMR002 SET n2 = n2-1 WHERE  RecordStatus = 1 AND t3='" + type + "' AND syskey ='"
					+ syskey + "'";
			PreparedStatement stmt = conn.prepareStatement(sql);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String query = "UPDATE FMR008 SET RecordStatus=4 WHERE RecordStatus = 1 AND t4='dislike' AND n1 = '"
						+ syskey + "' AND n2 = '" + userKey + "'";
				PreparedStatement stm = conn.prepareStatement(query);
				int result = stm.executeUpdate();
				if (result > 0) {
					res.setState(true);
				} else {
					res.setState(false);
				}

			} else {
				res.setState(false);
			}
			res.setN2(getTotalLikeCount(syskey, conn));
			res.setKeyResult(getTotalDisLikeCount(syskey, conn));

		} else {

		}
		return res;
	}

	/* UnlikeArticle */
	public ResultMobile UnlikeArticle(Long syskey, Long userSK, Connection conn, MrBean user) throws SQLException {
		ResultMobile res = new ResultMobile();
		long userKey = userSK;
		if (isLikeExist(syskey, userKey, conn)) {
			res.setN1(searchDisLikeOrNot(syskey, String.valueOf(userSK), conn));
			String sql = "Update FMR002 SET n2 = n2-1 WHERE  RecordStatus = 1 AND syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String query = "UPDATE FMR008 SET RecordStatus=4 WHERE RecordStatus = 1 AND t4='like' AND n1 = '"
						+ syskey + "' AND n2 = '" + userKey + "'";
				PreparedStatement stm = conn.prepareStatement(query);
				int result = stm.executeUpdate();
				if (result > 0) {
					res.setState(true);
				} else {
					res.setState(false);
				}

			} else {
				res.setState(false);
			}
			res.setN2(getTotalDisLikeCount(syskey, conn));
			res.setKeyResult(getTotalLikeCount(syskey, conn));

		} else {

		}
		return res;
	}

	/* UnlikeQuestion */
	public ResultMobile UnlikeQuestion(long syskey, long userSK, Connection conn, MrBean user) throws SQLException {
		// long key = SysKeyMgr.getSysKey(1, "syskey",
		// ConnAdmin.getConn(user.getUser().getOrganizationID()));
		ResultMobile res = new ResultMobile();
		long userKey = userSK;
		if (isLikeExist(syskey, userKey, conn)) {
			res.setN1(searchDisLikeOrNot(syskey, String.valueOf(userSK), conn));
			long n2 = new ArticleMobileDao().searchlikecount(syskey, conn);
			if (n2 > 0) {
				String sql = "Update FMR002 SET n2 = n2-1 WHERE  RecordStatus = 1 AND syskey = ?";
				PreparedStatement stmt = conn.prepareStatement(sql);
				stmt.setLong(1, syskey);
				int rs = stmt.executeUpdate();
				if (rs > 0) {
					res.setState(true);
					String query = "UPDATE FMR008 SET RecordStatus=4 WHERE RecordStatus = 1 AND t3='question' AND  n1 = '"
							+ syskey + "' AND n2 = '" + userKey + "'";
					PreparedStatement stm = conn.prepareStatement(query);
					int result = stm.executeUpdate();
					if (result > 0) {
						res.setState(true);
					} else {
						res.setState(false);
					}

				} else {
					res.setState(false);
				}
			}
			res.setN2(getTotalDisLikeCount(syskey, conn));
			res.setKeyResult(getTotalLikeCount(syskey, conn));

		} else {
		}
		return res;
	}

	public ResultMobile UnlikeVideo(Long syskey, Long userSK, Connection conn, MrBean user) throws SQLException {
		// long key = SysKeyMgr.getSysKey(1, "syskey",
		// ConnAdmin.getConn(user.getUser().getOrganizationID()));
		ResultMobile res = new ResultMobile();
		long userKey = userSK;
		String type = "Video";
		if (isLikeExist(syskey, userKey, conn)) {
			res.setN1(new ArticleMobileDao().searchDisLikeOrNot(syskey, String.valueOf(userSK), conn));
			String sql = "Update FMR002 SET n2 = n2-1 WHERE  RecordStatus = 1 AND t3='" + type + "' AND syskey ='"
					+ syskey + "'";
			PreparedStatement stmt = conn.prepareStatement(sql);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String query = "UPDATE FMR008 SET RecordStatus=4 WHERE RecordStatus = 1 AND t4='like' AND n1 = '"
						+ syskey + "' AND n2 = '" + userKey + "'";
				PreparedStatement stm = conn.prepareStatement(query);
				int result = stm.executeUpdate();
				if (result > 0) {
					res.setState(true);
				} else {
					res.setState(false);
				}

			} else {
				res.setState(false);
			}
			res.setN2(getTotalDisLikeCount(syskey, conn));
			res.setKeyResult(getTotalLikeCount(syskey, conn));

		} else {

		}
		return res;
	}

	/* update */
	public ResultMobile update(ArticleData obj, Connection conn) throws SQLException {
		ResultMobile res = new ResultMobile();
		String query = "UPDATE FMR002 SET [syskey]=?, [modifieddate]=?, [userid]=?, [username]=?, "
				+ " [recordstatus]=?, [syncstatus]=?, [syncbatch]=?,[usersyskey]=?, [t1]=?, [t2]=?, [t3]=?,[t4]=?, [t5]=?,"
				+ " [t6]=?, [t7]=?,[t8]=?, [t9]=?,[t10]=?, [t11]=?,[t12]=?, [t13]=?, [n1]=?, [n2]=?, [n3]=?,"
				+ " [n4]=?, [n5]=?,[n6]=?, [n7]=?,[n8]=?, [n9]=?,[n10]=?, [n11]=?,[n12]=?, [n13]=?, [createdtime]=?,[modifiedtime]=?"
				+ "WHERE RecordStatus<>4 AND RecordStatus = 1 AND Syskey=" + obj.getSyskey() + "";

		try {
			if (isCodeExist(obj, conn)) {
				PreparedStatement pstmt = conn.prepareStatement(query);
				pstmt.setLong(1, obj.getSyskey());

				pstmt.setString(2, obj.getModifiedDate());
				pstmt.setString(3, obj.getUserId());
				pstmt.setString(4, obj.getUserName());
				pstmt.setInt(5, obj.getRecordStatus());
				pstmt.setInt(6, obj.getSyncStatus());
				pstmt.setLong(7, obj.getSyncBatch());
				pstmt.setLong(8, obj.getUserSyskey());
				pstmt.setString(9, obj.getT1());
				pstmt.setString(10, obj.getT2());
				pstmt.setString(11, obj.getT3());
				pstmt.setString(12, obj.getT4());
				pstmt.setString(13, obj.getT5());
				pstmt.setString(14, obj.getT6());
				pstmt.setString(15, obj.getT7());
				pstmt.setString(16, obj.getT8());
				pstmt.setString(17, obj.getT9());
				pstmt.setString(18, obj.getT10());
				pstmt.setString(19, obj.getT11());
				pstmt.setString(20, obj.getT12());
				pstmt.setString(21, obj.getT13());
				pstmt.setLong(22, obj.getN1());
				pstmt.setLong(23, obj.getN2());
				pstmt.setLong(24, obj.getN3());
				pstmt.setLong(25, obj.getN4());
				pstmt.setLong(26, obj.getN5());
				pstmt.setLong(27, obj.getN6());
				pstmt.setLong(28, obj.getN7());
				pstmt.setLong(29, obj.getN8());
				pstmt.setLong(30, obj.getN9());
				pstmt.setLong(31, obj.getN10());
				pstmt.setLong(32, obj.getN11());
				pstmt.setLong(33, obj.getN12());
				pstmt.setLong(34, obj.getN13());
				pstmt.setString(35, obj.getCreatedTime());
				pstmt.setString(36, obj.getModifiedTime());

				if (pstmt.executeUpdate() > 0) {
					res.setState(true);
					res.setMsgDesc("Updated Successfully!");
				} else {
					res.setMsgDesc("No Such Article to Update!");
				}
			}
		} catch (SQLException e) {

		}

		return res;
	}

	public ResultMobile updateCommentCount(Long syskey, Connection conn) throws SQLException {
		ResultMobile res = new ResultMobile();
		String sql = "Update FMR002 SET n3 = n3+1 WHERE  RecordStatus = 1 AND syskey = ?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
		}
		return res;
	}

	// for video
	public ResultMobile updateCommentCountforVideo(Long syskey, Connection conn) throws SQLException {
		ResultMobile res = new ResultMobile();
		String sql = "Update FMR011 SET n3 = n3+1 WHERE  RecordStatus = 1 AND syskey = ?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
		}
		return res;
	}

	public ResultMobile updateDisLikeCount(Long syskey, String userSK, String type, Connection conn, MrBean user)
			throws SQLException {
		long key = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
		ResultMobile res = new ResultMobile();
		long userKey = Long.parseLong(userSK);
		RegisterData userData = RegisterMobileDao.readData(userKey, conn);
		if (isLikeExist(syskey, userKey, conn)) {
			String sql = "Update FMR002 SET n2 = n2-1 WHERE syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			stmt.executeUpdate();
			// update like status
			String query = "UPDATE FMR008 SET RecordStatus=4 WHERE  RecordStatus = 1 AND t4='like' AND  n1 = '" + syskey
					+ "' AND n2 = '" + userKey + "'";
			PreparedStatement st = conn.prepareStatement(query);
			st.executeUpdate();
		}
		if (!isDisLikeExist(syskey, userKey, conn)) {
			String sql = "Update FMR002 SET n8 = n8+1 WHERE syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
				if (!isStatusExist(syskey, userKey, "dislike", conn)) {
					String query = "INSERT INTO FMR008 (syskey,createddate, modifieddate, userid, username, RecordStatus, SyncStatus, SyncBatch, usersyskey,T1, T2,T3, T4,T5, T6,T7, T8,T9, T10,T11, T12,T13, T14,T15, n1,n2,n3, n4,n5, n6,n7, n8,n9, n10,n11, n12,n13, n14,n15) "
							+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					PreparedStatement psmt = conn.prepareStatement(query);
					psmt.setLong(1, key);
					psmt.setString(2, todayDate);
					psmt.setString(3, todayDate);
					psmt.setString(4, userData.getT1());
					psmt.setString(5, userData.getT3());
					psmt.setInt(6, 1);
					psmt.setInt(7, 1);
					psmt.setLong(8, 0);
					psmt.setLong(9, 0);
					psmt.setString(10, userData.getT1());
					psmt.setString(11, userData.getT3());
					psmt.setString(12, type);
					psmt.setString(13, "dislike"); // dislike status
					psmt.setString(14, "0");
					psmt.setString(15, "0");
					psmt.setString(16, "0");
					psmt.setString(17, "0");
					psmt.setString(18, "0");
					psmt.setString(19, "0");
					psmt.setString(20, "0");
					psmt.setString(21, "0");
					psmt.setString(22, "0");
					psmt.setString(23, "0");
					psmt.setString(24, "0");
					psmt.setLong(25, syskey);
					psmt.setLong(26, userKey);
					psmt.setLong(27, 0);
					psmt.setLong(28, 0);
					psmt.setLong(29, 0);
					psmt.setLong(30, 0);
					psmt.setLong(31, 0);
					psmt.setLong(32, 0);
					psmt.setLong(33, 0);
					psmt.setLong(34, 0);
					psmt.setLong(35, 0);
					psmt.setLong(36, 0);
					psmt.setLong(37, 0);
					psmt.setLong(38, 0);
					psmt.setLong(39, 0);
					if (psmt.executeUpdate() > 0) {

					} else {
					}
				} else {
					String query = "UPDATE FMR008 SET RecordStatus=1 WHERE  RecordStatus = 4 AND t4='dislike' AND n1 = '"
							+ syskey + "' AND n2 = '" + userKey + "'";
					PreparedStatement stm = conn.prepareStatement(query);

					int result = stm.executeUpdate();
					if (result > 0) {
						res.setState(true);
					} else {
						res.setState(false);
					}
				}
			}
		} else {
			String sql = "Update FMR002 SET n8 = n8-1 WHERE  RecordStatus = 1 AND syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String query = "UPDATE FMR008 SET RecordStatus=4 WHERE  RecordStatus = 1 AND t4='dislike' AND  n1 = '"
						+ syskey + "' AND n2 = '" + userKey + "'";
				PreparedStatement stm = conn.prepareStatement(query);
				int result = stm.executeUpdate();
				if (result > 0) {
					res.setState(true);
				} else {
					res.setState(false);
				}

			} else {
				res.setState(false);
			}

		}
		res.setN2(getTotalLikeCount(syskey, conn));
		res.setKeyResult(getTotalDisLikeCount(syskey, conn));
		return res;
	}

	/* updateDisLikeCountArticle */
	public ResultMobile updateDisLikeCountArticle(Long syskey, String userSK, String type, Connection conn, MrBean user)
			throws SQLException {
		long key = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
		ResultMobile res = new ResultMobile();
		long userKey = Long.parseLong(userSK);
		RegisterData userData = RegisterMobileDao.readData(userKey, conn);
		if (isLikeExist(syskey, userKey, conn)) {
			String sql = "Update FMR002 SET n2 = n2-1 WHERE syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			stmt.executeUpdate();
			String query = "UPDATE FMR008 SET RecordStatus=4 WHERE  RecordStatus = 1 AND t4='like' AND  n1 = '" + syskey
					+ "' AND n2 = '" + userKey + "'";
			PreparedStatement st = conn.prepareStatement(query);
			st.executeUpdate();
		}
		if (!isDisLikeExist(syskey, userKey, conn)) {
			String sql = "Update FMR002 SET n8 = n8+1 WHERE syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
				if (!isStatusExist(syskey, userKey, "dislike", conn)) {
					String query = "INSERT INTO FMR008 (syskey,createddate, modifieddate, userid, username, RecordStatus, SyncStatus, SyncBatch, usersyskey,T1, T2,T3, T4,T5, T6,T7, T8,T9, T10,T11, T12,T13, T14,T15, n1,n2,n3, n4,n5, n6,n7, n8,n9, n10,n11, n12,n13, n14,n15) "
							+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					PreparedStatement psmt = conn.prepareStatement(query);
					psmt.setLong(1, key);
					psmt.setString(2, todayDate);
					psmt.setString(3, todayDate);
					psmt.setString(4, userData.getT1());
					psmt.setString(5, userData.getT3());
					psmt.setInt(6, 1);
					psmt.setInt(7, 1);
					psmt.setLong(8, 0);
					psmt.setLong(9, 0);
					psmt.setString(10, userData.getT1());
					psmt.setString(11, userData.getT3());
					psmt.setString(12, type);
					psmt.setString(13, "dislike"); // dislike status
					psmt.setString(14, "0");
					psmt.setString(15, "0");
					psmt.setString(16, "0");
					psmt.setString(17, "0");
					psmt.setString(18, "0");
					psmt.setString(19, "0");
					psmt.setString(20, "0");
					psmt.setString(21, "0");
					psmt.setString(22, "0");
					psmt.setString(23, "0");
					psmt.setString(24, "0");
					psmt.setLong(25, syskey);
					psmt.setLong(26, userKey);
					psmt.setLong(27, 0);
					psmt.setLong(28, 0);
					psmt.setLong(29, 0);
					psmt.setLong(30, 0);
					psmt.setLong(31, 0);
					psmt.setLong(32, 0);
					psmt.setLong(33, 0);
					psmt.setLong(34, 0);
					psmt.setLong(35, 0);
					psmt.setLong(36, 0);
					psmt.setLong(37, 0);
					psmt.setLong(38, 0);
					psmt.setLong(39, 0);
					if (psmt.executeUpdate() > 0) {
					} else {
					}
				} else {
					String query = "UPDATE FMR008 SET RecordStatus=1 WHERE  RecordStatus = 4 AND t4='dislike' AND n1 = '"
							+ syskey + "' AND n2 = '" + userKey + "'";
					PreparedStatement stm = conn.prepareStatement(query);

					int result = stm.executeUpdate();
					if (result > 0) {
						res.setState(true);
					} else {
						res.setState(false);
					}

				}

			}
		} else {
			String sql = "Update FMR002 SET n8 = n8-1 WHERE  RecordStatus = 1 AND syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String query = "UPDATE FMR008 SET RecordStatus=4 WHERE  RecordStatus = 1 AND t4='dislike' AND  n1 = '"
						+ syskey + "' AND n2 = '" + userKey + "'";
				PreparedStatement stm = conn.prepareStatement(query);

				int result = stm.executeUpdate();
				if (result > 0) {
					res.setState(true);
				} else {
					res.setState(false);
				}

			} else {
				res.setState(false);
			}
		}
		res.setN2(getTotalLikeCount(syskey, conn));
		res.setKeyResult(getTotalDisLikeCount(syskey, conn));
		return res;
	}

	public ResultMobile updateLikeCount(Long syskey, String userSK, String type, Connection conn, MrBean user)
			throws SQLException {
		long key = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
		ResultMobile res = new ResultMobile();
		long userKey = Long.parseLong(userSK);
		if (isDisLikeExist(syskey, userKey, conn)) {
			String sql = "Update FMR002 SET n8 = n8-1 WHERE syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			stmt.executeUpdate();
			String query = "UPDATE FMR008 SET RecordStatus=4 WHERE  RecordStatus = 1 AND t4='dislike' AND  n1 = '"
					+ syskey + "' AND n2 = '" + userKey + "'";
			PreparedStatement st = conn.prepareStatement(query);
			st.executeUpdate();
		}
		RegisterData userData = RegisterMobileDao.readData(userKey, conn);
		if (!isLikeExist(syskey, userKey, conn)) {
			String sql = "Update FMR002 SET n2 = n2+1 WHERE syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
				if (!isStatusExist(syskey, userKey, "like", conn)) {
					String query = "INSERT INTO FMR008 (syskey,createddate, modifieddate, userid, username, RecordStatus, SyncStatus, SyncBatch, usersyskey,T1, T2,T3, T4,T5, T6,T7, T8,T9, T10,T11, T12,T13, T14,T15, n1,n2,n3, n4,n5, n6,n7, n8,n9, n10,n11, n12,n13, n14,n15) "
							+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					PreparedStatement psmt = conn.prepareStatement(query);
					psmt.setLong(1, key);
					psmt.setString(2, todayDate);
					psmt.setString(3, todayDate);
					psmt.setString(4, userData.getT1());
					psmt.setString(5, userData.getT3());
					psmt.setInt(6, 1);
					psmt.setInt(7, 1);
					psmt.setLong(8, 0);
					psmt.setLong(9, 0);
					psmt.setString(10, userData.getT1());
					psmt.setString(11, userData.getT3());
					psmt.setString(12, type);
					psmt.setString(13, "like");
					psmt.setString(14, "0");
					psmt.setString(15, "0");
					psmt.setString(16, "0");
					psmt.setString(17, "0");
					psmt.setString(18, "0");
					psmt.setString(19, "0");
					psmt.setString(20, "0");
					psmt.setString(21, "0");
					psmt.setString(22, "0");
					psmt.setString(23, "0");
					psmt.setString(24, "0");
					psmt.setLong(25, syskey);
					psmt.setLong(26, userKey);
					psmt.setLong(27, 0);
					psmt.setLong(28, 0);
					psmt.setLong(29, 0);
					psmt.setLong(30, 0);
					psmt.setLong(31, 0);
					psmt.setLong(32, 0);
					psmt.setLong(33, 0);
					psmt.setLong(34, 0);
					psmt.setLong(35, 0);
					psmt.setLong(36, 0);
					psmt.setLong(37, 0);
					psmt.setLong(38, 0);
					psmt.setLong(39, 0);
					if (psmt.executeUpdate() > 0) {
						System.out.println("Saved successfully");
					} else {
						System.out.println("Save Unsuccessfully");
					}
				} else {
					String query = "UPDATE FMR008 SET RecordStatus=1 WHERE  RecordStatus = 4 AND t4='like' AND n1 = '"
							+ syskey + "' AND n2 = '" + userKey + "'";
					PreparedStatement stm = conn.prepareStatement(query);
					int result = stm.executeUpdate();
					if (result > 0) {
						res.setState(true);
						System.out.println("Updated successfully");
					} else {
						res.setState(false);
						System.out.println("Updated Unsuccessfully");
					}
				}
			}
		} else {
			String sql = "Update FMR002 SET n2 = n2-1 WHERE  RecordStatus = 1 AND syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String query = "UPDATE FMR008 SET RecordStatus=4 WHERE  RecordStatus = 1 AND  t4='like' AND n1 = '"
						+ syskey + "' AND n2 = '" + userKey + "'";
				PreparedStatement stm = conn.prepareStatement(query);
				int result = stm.executeUpdate();
				if (result > 0) {
					res.setState(true);
				} else {
					res.setState(false);
				}
			} else {
				res.setState(false);
			}
		}
		res.setN2(getTotalDisLikeCount(syskey, conn));
		res.setKeyResult(getTotalLikeCount(syskey, conn));
		return res;
	}

	/* updateLikeCountArticle */
	public ResultMobile updateLikeCountArticle(Long syskey, String userSK, String type, Connection conn, MrBean user)
			throws SQLException {
		long key = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
		ResultMobile res = new ResultMobile();
		long userKey = Long.parseLong(userSK);
		if (isDisLikeExist(syskey, userKey, conn)) {
			String sql = "Update FMR002 SET n8 = n8-1 WHERE syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			stmt.executeUpdate();
			String query = "UPDATE FMR008 SET RecordStatus=4 WHERE  RecordStatus = 1 AND t4='dislike' AND  n1 = '"
					+ syskey + "' AND n2 = '" + userKey + "'";
			PreparedStatement st = conn.prepareStatement(query);
			st.executeUpdate();
		}
		RegisterData userData = RegisterMobileDao.readData(userKey, conn);
		if (!isLikeExist(syskey, userKey, conn)) {
			String sql = "Update FMR002 SET n2 = n2+1 WHERE syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
				if (!isStatusExist(syskey, userKey, "like", conn)) {
					String query = "INSERT INTO FMR008 (syskey,createddate, modifieddate, userid, username, RecordStatus, SyncStatus, SyncBatch, usersyskey,T1, T2,T3, T4,T5, T6,T7, T8,T9, T10,T11, T12,T13, T14,T15, n1,n2,n3, n4,n5, n6,n7, n8,n9, n10,n11, n12,n13, n14,n15) "
							+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					PreparedStatement psmt = conn.prepareStatement(query);
					psmt.setLong(1, key);
					psmt.setString(2, todayDate);
					psmt.setString(3, todayDate);
					psmt.setString(4, userData.getT1());
					psmt.setString(5, userData.getT3());
					psmt.setInt(6, 1);
					psmt.setInt(7, 1);
					psmt.setLong(8, 0);
					psmt.setLong(9, 0);
					psmt.setString(10, userData.getT1());
					psmt.setString(11, userData.getT3());
					psmt.setString(12, type);
					psmt.setString(13, "like"); // like status
					psmt.setString(14, "0");
					psmt.setString(15, "0");
					psmt.setString(16, "0");
					psmt.setString(17, "0");
					psmt.setString(18, "0");
					psmt.setString(19, "0");
					psmt.setString(20, "0");
					psmt.setString(21, "0");
					psmt.setString(22, "0");
					psmt.setString(23, "0");
					psmt.setString(24, "0");
					psmt.setLong(25, syskey);
					psmt.setLong(26, userKey);
					psmt.setLong(27, 0);
					psmt.setLong(28, 0);
					psmt.setLong(29, 0);
					psmt.setLong(30, 0);
					psmt.setLong(31, 0);
					psmt.setLong(32, 0);
					psmt.setLong(33, 0);
					psmt.setLong(34, 0);
					psmt.setLong(35, 0);
					psmt.setLong(36, 0);
					psmt.setLong(37, 0);
					psmt.setLong(38, 0);
					psmt.setLong(39, 0);
					if (psmt.executeUpdate() > 0) {

					} else {
					}
				} else {
					String query = "UPDATE FMR008 SET RecordStatus=1 WHERE  RecordStatus = 4 AND t4='like' AND  n1 = '"
							+ syskey + "' AND n2 = '" + userKey + "'";
					PreparedStatement stm = conn.prepareStatement(query);

					int result = stm.executeUpdate();
					if (result > 0) {
						res.setState(true);
					} else {
						res.setState(false);
					}
				}

			}
		} else {
			String sql = "Update FMR002 SET n2 = n2-1 WHERE  RecordStatus = 1 AND syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String query = "UPDATE FMR008 SET RecordStatus=4 WHERE  RecordStatus = 1 AND t4='like' AND  n1 = '"
						+ syskey + "' AND n2 = '" + userKey + "'";
				PreparedStatement stm = conn.prepareStatement(query);

				int result = stm.executeUpdate();
				if (result > 0) {
					res.setState(true);
				} else {
					res.setState(false);
				}

			} else {
				res.setState(false);
			}
		}
		res.setN2(getTotalDisLikeCount(syskey, conn));
		res.setKeyResult(getTotalLikeCount(syskey, conn));
		return res;

	}

	public ResultMobile updateReplyCommentCount(Long syskey, Connection conn) throws SQLException {
		ResultMobile res = new ResultMobile();
		String sql = "Update FMR003 SET n3 = n3+1 WHERE  RecordStatus = 1 AND syskey = ?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
		}
		return res;
	}

	public ResultMobile updateUnlikeCount(Long syskey, Connection conn) throws SQLException {
		ResultMobile res = new ResultMobile();
		String sql = "Update FMR002 SET n4 = n4+1 WHERE RecordStatus = 1 AND syskey = ?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
		}
		return res;
	}

	public ArticleDataSet view(String searchVal, String type, Connection conn) throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		String whereclause = " WHERE RecordStatus<>4 AND RecordStatus = 1 AND t3 = '" + type + "' ";
		if (!searchVal.equals("")) {
			whereclause += "AND syskey = '" + searchVal + "' AND RecordStatus = 1 AND RecordStatus<>4 ";
		}
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereclause, " ORDER BY syskey ", conn);
		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getDBRecord(dbrs.get(i)));
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	/////////////////////////////////////////////// Mobile//////////////////////////////////////////////////////
	/* education view detail */
	public ArticleDataSet viewByKey(String searchVal, long userSK, String type, Connection conn) throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		String whereclause = " WHERE RecordStatus = 1 AND t3 = '" + type + "'  ";
		if (!searchVal.equals("")) {
			whereclause += "AND syskey = '" + searchVal + "' AND RecordStatus = 1  ";
		}
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereclause, " ORDER BY syskey ", conn);
		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getDBRecord(dbrs.get(i)));
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}	

}
