package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.ArticleJunData;
import com.nirvasoft.cms.shared.ArticleJunDataSet;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;

public class ArticleJunDao {

	public static DBRecord define(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("T1", (byte) 5));
		ret.getFields().add(new DBField("T2", (byte) 5));
		ret.getFields().add(new DBField("T3", (byte) 5));
		ret.getFields().add(new DBField("T4", (byte) 5));
		ret.getFields().add(new DBField("T5", (byte) 5));
		ret.getFields().add(new DBField("T6", (byte) 5));
		ret.getFields().add(new DBField("T7", (byte) 5));
		ret.getFields().add(new DBField("T8", (byte) 5));
		ret.getFields().add(new DBField("T9", (byte) 5));
		ret.getFields().add(new DBField("T10", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		return ret;
	}

	public static Resultb2b deleteByUpdate(Long syskey, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = "DELETE FROM JUN004 WHERE n1=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Successfully");
		}
		return res;
	}

	public static ArticleJunData getDBRecords(DBRecord adbr) {
		ArticleJunData ret = new ArticleJunData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreateddate(adbr.getString("createddate"));
		ret.setModifieddate(adbr.getString("modifieddate"));
		ret.setUserid(adbr.getString("userid"));
		ret.setUsername(adbr.getString("username"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setUsersyskey(adbr.getLong("usersyskey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		ret.setT8(adbr.getString("t8"));
		ret.setT9(adbr.getString("t9"));
		ret.setT10(adbr.getString("t10"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getLong("n5"));
		ret.setN6(adbr.getLong("n6"));
		ret.setN7(adbr.getLong("n7"));
		ret.setN8(adbr.getLong("n8"));
		ret.setN9(adbr.getLong("n9"));
		ret.setN10(adbr.getLong("n10"));
		return ret;
	}

	public static Resultb2b insertJUN(ArticleJunData aObj, Connection aConnection) throws SQLException {
		Resultb2b res = new Resultb2b();
		String query = "INSERT INTO JUN004 (syskey, createddate, modifieddate, userid, username,"
				+ " recordStatus, syncStatus, syncBatch,usersyskey,t1, t2, t3, t4, t5, t6, t7, t8, t9, t10,"
				+ " n1, n2, n3, n4, n5, n6, n7, n8, n9, n10)" + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
				+ "?,?,?,?,?,?,?,?,?)";
		PreparedStatement ps = aConnection.prepareStatement(query);
		ps.setLong(1, aObj.getSyskey());
		ps.setString(2, aObj.getCreateddate());
		ps.setString(3, aObj.getModifieddate());
		ps.setString(4, aObj.getUserid());
		ps.setString(5, aObj.getUsername());
		ps.setInt(6, aObj.getRecordStatus());
		ps.setInt(7, aObj.getSyncStatus());
		ps.setLong(8, aObj.getSyncBatch());
		ps.setLong(9, aObj.getUsersyskey());
		ps.setString(10, aObj.getT1());
		ps.setString(11, aObj.getT2());
		ps.setString(12, aObj.getT3());
		ps.setString(13, aObj.getT4());
		ps.setString(14, aObj.getT5());
		ps.setString(15, aObj.getT6());
		ps.setString(16, aObj.getT7());
		ps.setString(17, aObj.getT8());
		ps.setString(18, aObj.getT9());
		ps.setString(19, aObj.getT10());
		ps.setLong(20, aObj.getN1());
		ps.setLong(21, aObj.getN2());
		ps.setLong(22, aObj.getN3());
		ps.setLong(23, aObj.getN4());
		ps.setLong(24, aObj.getN5());
		ps.setLong(25, aObj.getN6());
		ps.setLong(26, aObj.getN7());
		ps.setLong(27, aObj.getN8());
		ps.setLong(28, aObj.getN9());
		ps.setLong(29, aObj.getN10());

		if (ps.executeUpdate() > 0) {
			res.setState(true);
			res.setMsgDesc("Saved Successfully!");
		} else {
			res.setMsgDesc("Saved UnSuccessfully!");
			res.setState(false);
		}

		return res;

	}

	public static boolean isJunCodeExist(ArticleJunData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("Jun004"), " where RecordStatus = 1   AND t2='"
				+ obj.getT2() + "' AND t1='" + obj.getT1() + "' AND syskey = " + obj.getN1() + " ", "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static ArticleJunDataSet readByJun(long syskey, Connection conn) throws SQLException {
		ArticleJunDataSet res = new ArticleJunDataSet();
		ArrayList<ArticleJunData> dataList = new ArrayList<ArticleJunData>();
		ArrayList<String> strList = new ArrayList<String>();
		String whereclause = " WHERE n1 = " + syskey + " AND RECORDSTATUS <> 4 AND RECORDSTATUS = 1";
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("jun004"), whereclause, "", conn);
		for (int i = 0; i < dbrs.size(); i++) {
			dataList.add(getDBRecords(dbrs.get(i)));
		}
		if (dataList.size() > 0) {
			res.setState(true);
			ArticleJunData[] dataArray = new ArticleJunData[dataList.size()];
			dataArray = dataList.toArray(dataArray);
			String[] strArray = new String[strList.size()];
			strArray = strList.toArray(strArray);
			res.setData(dataArray);
		} else {
			res.setState(false);
		}
		return res;
	}

	public static ArrayList<String> removeDouble(ArrayList<String> data) {
		ArrayList<String> result = new ArrayList<String>();
		boolean isExist = false;
		for (int i = 0; i < data.size(); i++) {
			isExist = false;
			if (data.get(i) != "") {
				for (int j = 0; j < result.size(); j++) {
					if (data.get(i).equalsIgnoreCase(result.get(j))) {
						isExist = true;
						break;
					}
				}
				if (!isExist)
					result.add(data.get(i));
			}
		}
		return result;
	}

	public static DBRecord setDBRecord(ArticleJunData data) {
		DBRecord ret = define("JUN004");
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("createddate", data.getCreateddate());
		ret.setValue("modifieddate", data.getModifieddate());
		ret.setValue("userid", data.getUserid());
		ret.setValue("username", data.getUsername());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("usersyskey", data.getUsersyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t5", data.getT5());
		ret.setValue("t6", data.getT6());
		ret.setValue("t7", data.getT7());
		ret.setValue("t8", data.getT8());
		ret.setValue("t9", data.getT9());
		ret.setValue("t10", data.getT10());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		ret.setValue("n6", data.getN6());
		ret.setValue("n7", data.getN7());
		ret.setValue("n8", data.getN8());
		ret.setValue("n9", data.getN9());
		ret.setValue("n10", data.getN10());
		return ret;
	}

	public static Resultb2b update(ArticleJunData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = DBMgr.updateString(" WHERE RecordStatus = 1 AND syskey=" + obj.getN1(), define("Jun004"), conn);
		PreparedStatement stmt = conn.prepareStatement(sql);
		DBRecord dbr = setDBRecord(obj);
		DBMgr.setValues(stmt, dbr);
		int count = stmt.executeUpdate();
		if (count > 0) {
			res.setState(true);
			res.setMsgDesc("Updated Successfully!");
		}
		return res;
	}

	public ArticleJunDataSet search(String id, Connection conn) throws SQLException {
		ArticleJunDataSet res = new ArticleJunDataSet();
		ArrayList<ArticleJunData> dataList = new ArrayList<ArticleJunData>();
		ArrayList<String> cropList = new ArrayList<String>();
		String whereclause = " WHERE n1 = " + id + " AND  RECORDSTATUS = 1";
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("JUN004"), whereclause, "", conn);
		for (int i = 0; i < dbrs.size(); i++) {
			dataList.add(getDBRecords(dbrs.get(i)));
			cropList.add(getDBRecords(dbrs.get(i)).getT1());
		}
		if (dataList.size() > 0) {
			res.setState(true);
			ArticleJunData[] dataArray = new ArticleJunData[dataList.size()];
			dataArray = dataList.toArray(dataArray);
			cropList = removeDouble(cropList);
			String[] cropArray = new String[cropList.size()];
			cropArray = cropList.toArray(cropArray);
			res.setData(dataArray);
			res.setCrop(cropArray);
		} else {
			res.setState(false);
		}
		return res;
	}

}
