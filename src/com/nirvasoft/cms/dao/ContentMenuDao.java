package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.shared.DivisionComboData;
import com.nirvasoft.cms.shared.DivisionComboDataSet;
import com.nirvasoft.cms.util.FontChangeUtil;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;

public class ContentMenuDao {
	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("FMR002");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("createdtime", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("modifiedtime", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t13", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		ret.getFields().add(new DBField("n11", (byte) 2));
		ret.getFields().add(new DBField("n12", (byte) 2));
		ret.getFields().add(new DBField("n13", (byte) 2));
		return ret;
	}

	public static DBRecord defines(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("createdtime", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("modifiedtime", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t13", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		ret.getFields().add(new DBField("n11", (byte) 2));
		ret.getFields().add(new DBField("n12", (byte) 2));
		ret.getFields().add(new DBField("n13", (byte) 2));

		return ret;
	}

	public static DBRecord define(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t13", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		ret.getFields().add(new DBField("n11", (byte) 2));
		ret.getFields().add(new DBField("n12", (byte) 2));
		ret.getFields().add(new DBField("n13", (byte) 2));

		return ret;
	}

	public static ArticleData getDBRecord(DBRecord adbr) {
		ArticleData ret = new ArticleData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("createddate"));
		ret.setCreatedTime(adbr.getString("createdtime"));
		ret.setModifiedDate(adbr.getString("modifieddate"));
		ret.setModifiedTime(adbr.getString("modifiedtime"));
		ret.setUserId(adbr.getString("userid"));
		ret.setUserName(adbr.getString("username"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setUserSyskey(adbr.getLong("usersyskey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		ret.setT8(adbr.getString("t8"));
		ret.setT9(adbr.getString("t9"));
		ret.setT10(adbr.getString("t10"));
		ret.setT11(adbr.getString("t11"));
		ret.setT12(adbr.getString("t12"));
		ret.setT13(adbr.getString("t13"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getLong("n5"));
		ret.setN6(adbr.getLong("n6"));
		ret.setN7(adbr.getLong("n7"));
		ret.setN8(adbr.getLong("n8"));
		ret.setN9(adbr.getLong("n9"));
		ret.setN10(adbr.getLong("n10"));
		ret.setN11(adbr.getLong("n11"));
		ret.setN12(adbr.getLong("n12"));
		ret.setN13(adbr.getLong("n13"));
		return ret;
	}

	public static DBRecord setDBRecord(ArticleData data) {
		DBRecord ret = define();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("createddate", data.getCreatedDate());
		ret.setValue("createdtime", data.getCreatedTime());
		ret.setValue("modifieddate", data.getModifiedDate());
		ret.setValue("modifiedtime", data.getModifiedTime());
		ret.setValue("userid", data.getUserId());
		ret.setValue("username", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("usersyskey", data.getUserSyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t5", data.getT5());
		ret.setValue("t6", data.getT6());
		ret.setValue("t7", data.getT7());
		ret.setValue("t8", data.getT8());
		ret.setValue("t9", data.getT9());
		ret.setValue("t10", data.getT10());
		ret.setValue("t11", data.getT11());
		ret.setValue("t12", data.getT12());
		ret.setValue("t13", data.getT13());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		ret.setValue("n6", data.getN6());
		ret.setValue("n7", data.getN7());
		ret.setValue("n8", data.getN8());
		ret.setValue("n9", data.getN9());
		ret.setValue("n10", data.getN10());
		ret.setValue("n11", data.getN11());
		ret.setValue("n12", data.getN12());
		ret.setValue("n13", data.getN13());
		return ret;
	}

	
	public static DivisionComboDataSet getAcademicRef(Connection conn) throws SQLException {
		DivisionComboDataSet dataset = new DivisionComboDataSet();
		ArrayList<DivisionComboData> datalist = new ArrayList<DivisionComboData>();
		DivisionComboData combo;
		String sql = "select syskey,t2 from ACADEMICREFERENCE WHERE RECORDSTATUS=1  ORDER BY t2 ASC ";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			combo = new DivisionComboData();
			combo.setCaption(res.getString("t2"));
			combo.setValue(res.getString("syskey"));
			combo.setFlag(false);
			datalist.add(combo);
		}
		DivisionComboData[] dataarray = new DivisionComboData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		dataset.setData(dataarray);
		return dataset;
	}
	
	public static Resultb2b insert(ArticleData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		if (!isCodeExist(obj, conn)) {
			String sql = DBMgr.insertString(define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			int count = stmt.executeUpdate();
			if (count > 0) {
				res.setState(true);
				res.setMsgDesc("Saved Successfully!");
			}
		} else {
			res.setMsgDesc("Article Already Exist!");
		}
		return res;
	}
	public static Resultb2b update(ArticleData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		if (isCodeExist(obj, conn)) {
			String sql = DBMgr.updateString(" WHERE RecordStatus<>4 AND RecordStatus = 1 AND Syskey=" + obj.getSyskey(),define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			int count = stmt.executeUpdate();
			if (count > 0) {
				res.setState(true);
				res.setMsgDesc("Updated Successfully!");
			}
		} else {
			res.setMsgDesc("No Such Article to Update!");
		}
		return res;
	}
	
	public static boolean isCodeExist(ArticleData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define()," where RecordStatus<>4 AND  RecordStatus = 1 AND syskey = " + obj.getSyskey() + " ", "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	/*public static ArticleDataSet searchContentMenuList(PagerData pgdata, String croptype,long status,String statetype,Connection conn)throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		ArrayList<DBRecord> dbrs;
		PreparedStatement stat = null;
		String clause="";
		String whereclause = " WHERE RecordStatus<>4  AND  (t3='news & media' OR t3='education') ";
		if (statetype.equalsIgnoreCase("")) {
			if (status == 1) {
				// whereclause+="AND (n7=1 OR n7=2 OR n7=6)";
				whereclause += "AND n7=6";// draft
			}
			if (status == 2) {
				// whereclause+="AND (n7=2 OR n7=4 OR n7=6)";
				whereclause += "AND n7=2";// pending
			}
			if (status == 3) {
				// whereclause+="AND (n7=4 OR n7=5)";
				whereclause += "AND n7=4";// approved
			}
			if (status == 4) {
				whereclause += "AND n7=6";// approved
				//whereclause += "AND (n7=1 OR n7=2 OR n7=4 OR n7=5 OR n7=6)";
			
			}
			if (status == 5) {
				whereclause += "AND n7=6";// approved
				//whereclause += "AND (n7=1 OR n7=2 OR n7=4 OR n7=5 OR n7=6)";
			
			}
		}
		if(!statetype.equalsIgnoreCase("")){
			whereclause+="AND (n7='"+statetype+"')";
		}
		
		if (!croptype.equalsIgnoreCase("")) {
			if(status==1){
				clause += " f join jun004 j on f.syskey=j.n1 Where f.t3=('news & media' OR f.t3='education') and j.recordstatus<>4 and f.recordstatus<>4 and j.n2='" + croptype + "' and (f.n7=1 OR f.n7=2 OR f.n7=6)" ;
			}
			if(status==2){
				clause += " f join jun004 j on f.syskey=j.n1 Where f.t3=('news & media' OR f.t3='education') and j.recordstatus<>4 and f.recordstatus<>4 and j.n2='" + croptype + "' and (f.n7=2 OR f.n7=4 OR f.n7=6)" ;
			}
			if(status==3){
				clause += " f join jun004 j on f.syskey=j.n1 Where f.t3=('news & media' OR f.t3='education') and j.recordstatus<>4 and f.recordstatus<>4 and j.n2='" + croptype + "' and (f.n7=4 OR f.n7=5)" ;
			}
			if(status==4){
				clause += " f join jun004 j on f.syskey=j.n1 Where f.t3=('news & media' OR f.t3='education') and j.recordstatus<>4 and f.recordstatus<>4 and j.n2='" + croptype + "' and (f.n7=1 OR f.n7=2 OR f.n7=4 OR f.n7=5 OR f.n7=6)" ;
			}
		
//		if (!croptype.equalsIgnoreCase("")) {
//			clause += " f join jun004 j on f.syskey=j.n1 Where   j.recordstatus<>4 and f.recordstatus<>4 and j.n2='"
//					+ croptype + "' and f.t3='news & media' OR f.t3='education' ";
//		}
		}
		
		if (!pgdata.getT1().equals("")) {
			whereclause += "AND ( t1 LIKE N'%" + pgdata.getT1() + "%' OR  modifiedtime LIKE N'%" + pgdata.getT1()
			+ "%' OR  modifieddate LIKE N'%" + pgdata.getT1() + "%' OR t2 LIKE N'%" + pgdata.getT1() + "%' ) ";
		}
		if (!croptype.equalsIgnoreCase("")) {
				String sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc) AS RowNum,* FROM ( select f.syskey, f.t1, f.t2, f.t3, f.modifieddate,"
						+ " f.modifiedtime,f.username  from  fmr002 " + clause
						+ ") b)  AS RowConstrainedResult WHERE RowNum >= '" + (pgdata.getStart() - 1)
						+ "' AND RowNum <= '" + pgdata.getEnd() + "' ";
				PreparedStatement ps = conn.prepareStatement(sql);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					ArticleData data = new ArticleData();
					data.setSyskey(rs.getLong("syskey"));
					data.setT1(rs.getString("t1"));
					data.setT2(rs.getString("t2"));
					data.setT3(rs.getString("t3"));
					data.setModifiedDate(rs.getString("modifieddate"));
					data.setModifiedTime(rs.getString("modifiedtime"));
					data.setUserName(rs.getString("username"));
					datalist.add(data);
				}
			
		} else {
			dbrs = DBMgr.getDBRecordSandE(define(), whereclause, " ORDER BY syskey DESC", (pgdata.getStart() - 1),
					pgdata.getEnd(), 0, conn);
			for (int i = 0; i < dbrs.size(); i++) {
				if(dbrs.get(i).getLong("n7")==1){
					dbrs.get(i).setValue("t10", "Draft");
				}
				if(dbrs.get(i).getLong("n7")==2){
					dbrs.get(i).setValue("t10", "Pending");
				}
				if(dbrs.get(i).getLong("n7")==3){
					dbrs.get(i).setValue("t10", "Modification");
				}
				if(dbrs.get(i).getLong("n7")==4){
					dbrs.get(i).setValue("t10", "Approve");
				}
				if(dbrs.get(i).getLong("n7")==5){
					dbrs.get(i).setValue("t10", "Publish");
				}
				if(dbrs.get(i).getLong("n7")==6){
					dbrs.get(i).setValue("t10", "Reject");
				}
				datalist.add(getDBRecord(dbrs.get(i)));
			}
		}
		
		
		res.setPageSize(pgdata.getSize());
		res.setCurrentPage(pgdata.getCurrent());
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		
		if (!croptype.equalsIgnoreCase("")) {
			stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM FMR002 " + clause);
		} else {
			stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM FMR002 " + whereclause);
		}
			
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}
	*/
	public static ArticleData read(long syskey, Connection conn) throws SQLException {
		ArticleData ret = new ArticleData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),"where RecordStatus<>4 AND  RecordStatus = 1 AND syskey=" + syskey, "", conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));
		//if (ServerUtil.isUniEncoded(ret.getT1())) {
			ret.setT1(FontChangeUtil.uni2zg(ret.getT1()));
		//}
		//if (ServerUtil.isUniEncoded(ret.getT2())) {
			ret.setT2(FontChangeUtil.uni2zg(ret.getT2()));
		//}
		return ret;
	}
	
	public static Resultb2b delete(Long syskey, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = "UPDATE FMR002 SET RecordStatus=4 WHERE RecordStatus = 1 AND syskey=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Successfully!!!");
		} else {
			res.setMsgDesc("Deleting Unsuccessful!!!");
		}
		return res;
	}

	
	public static long searchSaveContentOrNot(String usersk, long postsk, Connection conn) throws SQLException {
		long key = 0;
		String sql = "Select n3 from FMR013 WHERE RecordStatus<>4  AND n1 = " + usersk + " AND n2 = '" + postsk + "'";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = rs.getLong("n3");
		} 
		return key;
	}
	
	
	public static long searchSaveContentOrNotEdu(long postsk, Connection conn) throws SQLException {
		long key = 0;
		String sql = "Select n3 from FMR013 WHERE RecordStatus<>4 AND n2 = '" + postsk + "'";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = rs.getLong("n3");
		} 
		return key;
	}


	
	

}
