package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Result;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.shared.ArticleDataSet;
import com.nirvasoft.cms.shared.CommentData;
import com.nirvasoft.cms.shared.CommentDataSet;
import com.nirvasoft.cms.shared.DivisionComboData;
import com.nirvasoft.cms.shared.DivisionComboDataSet;
import com.nirvasoft.cms.shared.DocumentData;
import com.nirvasoft.cms.shared.DocumentDataSet;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.cms.shared.RegisterData;
import com.nirvasoft.cms.util.FontChangeUtil;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;
import com.nirvasoft.database.SysKeyMgr;
import com.nirvasoft.rp.data.CityStateData;

public class ArticleDao {
	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("FMR002");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("createdtime", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("modifiedtime", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t13", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		ret.getFields().add(new DBField("n11", (byte) 2));
		ret.getFields().add(new DBField("n12", (byte) 2));
		ret.getFields().add(new DBField("n13", (byte) 2));
		return ret;
	}	
	public static DBRecord defines(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("createdtime", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("modifiedtime", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t13", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		ret.getFields().add(new DBField("n11", (byte) 2));
		ret.getFields().add(new DBField("n12", (byte) 2));
		ret.getFields().add(new DBField("n13", (byte) 2));
		return ret;
	}

	public static DBRecord define(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t13", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		ret.getFields().add(new DBField("n11", (byte) 2));
		ret.getFields().add(new DBField("n12", (byte) 2));
		ret.getFields().add(new DBField("n13", (byte) 2));
		return ret;
	}

	public static ArticleData getDBRecord(DBRecord adbr) {
		ArticleData ret = new ArticleData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("createddate"));
		ret.setCreatedTime(adbr.getString("createdtime"));
		ret.setModifiedDate(adbr.getString("modifieddate"));
		ret.setModifiedTime(adbr.getString("modifiedtime"));
		ret.setUserId(adbr.getString("userid"));
		ret.setUserName(adbr.getString("username"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setUserSyskey(adbr.getLong("usersyskey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		ret.setT8(adbr.getString("t8"));
		ret.setT9(adbr.getString("t9"));
		ret.setT10(adbr.getString("t10"));
		ret.setT11(adbr.getString("t11"));
		ret.setT12(adbr.getString("t12"));
		ret.setT13(adbr.getString("t13"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getLong("n5"));
		ret.setN6(adbr.getLong("n6"));
		ret.setN7(adbr.getLong("n7"));
		ret.setN8(adbr.getLong("n8"));
		ret.setN9(adbr.getLong("n9"));
		ret.setN10(adbr.getLong("n10"));
		ret.setN11(adbr.getLong("n11"));
		ret.setN12(adbr.getLong("n12"));
		ret.setN13(adbr.getLong("n13"));
		return ret;
	}

	public static DBRecord setDBRecord(ArticleData data) {
		DBRecord ret = define();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("createddate", data.getCreatedDate());
		ret.setValue("createdtime", data.getCreatedTime());
		ret.setValue("modifieddate", data.getModifiedDate());
		ret.setValue("modifiedtime", data.getModifiedTime());
		ret.setValue("userid", data.getUserId());
		ret.setValue("username", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("usersyskey", data.getUserSyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t5", data.getT5());
		ret.setValue("t6", data.getT6());
		ret.setValue("t7", data.getT7());
		ret.setValue("t8", data.getT8());
		ret.setValue("t9", data.getT9());
		ret.setValue("t10", data.getT10());
		ret.setValue("t11", data.getT11());
		ret.setValue("t12", data.getT12());
		ret.setValue("t13", data.getT13());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		ret.setValue("n6", data.getN6());
		ret.setValue("n7", data.getN7());
		ret.setValue("n8", data.getN8());
		ret.setValue("n9", data.getN9());
		ret.setValue("n10", data.getN10());
		ret.setValue("n11", data.getN11());
		ret.setValue("n12", data.getN12());
		ret.setValue("n13", data.getN13());
		return ret;
	}

	
	/*searchList*/
	public static ArticleDataSet searchList(PagerData pgdata,long status,String statetype,Connection conn) throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		PreparedStatement stat;
		String whereclause="";
		String whereclausenew="";		
		 whereclause = " WHERE RecordStatus<>4  AND  RecordStatus = 1 and t3<>'Video' ";
		 //whereclausenew=" and a.RecordStatus<>4  AND  a.RecordStatus = 1 and a.t3<>'Video' ";
/*	       if(pgdata.getN1()!=0) {
	    	   whereclause+= " AND n5="+pgdata.getN1()+" ";
	       }*/
	       if(!pgdata.getT2().equalsIgnoreCase("")) {
	    	   whereclause+= " AND userid='"+pgdata.getT2()+"' ";
	    	   //whereclausenew+= " AND a.userid='"+pgdata.getT2()+"' ";
	       }
	       System.out.println("pager.t2: " + pgdata.getT2()+"whereclause in writerlist: " + whereclause);;
	       if (statetype.equalsIgnoreCase("")) {
				if (status == 1) {
					whereclause += "AND n7=6";// draft
					//whereclausenew += "AND a.n7=6";
				}
				if (status == 2) {
					whereclause += "AND n7=2";// pending
					//whereclausenew += "AND a.n7=2";
				}
				if (status == 3) {
					whereclause += "AND n7=4";// approved
					//whereclausenew += "AND a.n7=4";
				}
				if (status == 4) {
					whereclause += "AND n7=1";// approved
					//whereclausenew += "AND a.n7=1";
				}
				if (status == 5) {
					whereclause += "AND n7=1";// approved
					//whereclausenew += "AND a.n7=1";
				}
			}
			if (!statetype.equalsIgnoreCase("")) {
				whereclause += "AND (n7='" + statetype + "')";
				//whereclausenew += "AND (a.n7='" + statetype + "')";
			}
		
		if (!pgdata.getT1().equals("")) {
			whereclause += "AND (t1 LIKE N'%" + pgdata.getT1() + "%' OR  modifieddate LIKE N'%" + pgdata.getT1()
					+ "%' OR modifiedtime LIKE N'%" + pgdata.getT1() + "%' OR  t2 LIKE N'%" + pgdata.getT1()
					+ "%' OR  username LIKE N'%" + pgdata.getT1() + "%') ";
		}
		String sql = "";
		String regionCode="";
		if(status==5) { //admin
			sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc)AS RowNum , * from fmr002 " + whereclause + " and userid in (select t1 from uvm012 where syskey in (select n4 from   uvm005 where syskey in (select n1 from jun002 where n2 in(select syskey from uvm009 where t2<>'master'  )))))AS RowConstrainedResult  WHERE RowNum >="
				+ pgdata.getStart() + " and RowNum <=" + pgdata.getEnd() + " order by syskey desc";
		}
		else if(status==1) { //content_writer
			sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc)AS RowNum , * from fmr002 " + whereclause + " and userid = '"+pgdata.getUserid()+"' and userid in (select t1 from uvm012 where syskey in (select n4 from   uvm005 where syskey in (select n1 from jun002 where n2 in(select syskey from uvm009 where t2='Content Writer')))))AS RowConstrainedResult  WHERE RowNum >="
					+ pgdata.getStart() + " and RowNum <=" + pgdata.getEnd() + " order by syskey desc";
		}
		else {
			String query="select t5 from uvm005_A where t1='"+pgdata.getUserid()+"'";
			PreparedStatement pstmt = conn.prepareStatement(query);
			ResultSet rst = pstmt.executeQuery();
			while (rst.next()) {
				regionCode=rst.getString("t5");
				if (regionCode.equals("00000000")) {
					sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc) AS RowNum,* FROM ( select * from fmr002 "
							+ whereclause + ") b)  AS RowConstrainedResult WHERE RowNum >= '" + pgdata.getStart()
							+ "' AND RowNum <= '" + pgdata.getEnd() + "' order by syskey desc";					
				}else if (regionCode.equals("10000000")) {
					sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc) AS RowNum,* FROM ( select * from fmr002 "
							+ whereclause + "and t5!='13000000') b)  AS RowConstrainedResult WHERE RowNum >= '" + pgdata.getStart()
							+ "' AND RowNum <= '" + pgdata.getEnd() + "' order by syskey desc";					
				}else if (regionCode.equals("13000000")) {
					sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc) AS RowNum,* FROM ( select * from fmr002 "
							+ whereclause + "and t5!='10000000') b)  AS RowConstrainedResult WHERE RowNum >= '" + pgdata.getStart()
							+ "' AND RowNum <= '" + pgdata.getEnd() + "' order by syskey desc";					
				}/*else {
					sql = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc) AS RowNum,* FROM "
							+ "(select a.syskey,a.t1,a.t2,a.t3,a.t5,a.t8,a.t10,a.n1,a.n2,a.n3,a.n4,a.n5,a.n6,"
							+ "a.n7,a.n8,a.n9,a.n10,a.modifieddate,a.modifiedtime,a.username,a.modifiedusername "
							+ "from fmr002 a,UVM005_A b WHERE a.t5=b.t5 and b.t1='"+pgdata.getUserid()+"'"
							+ whereclausenew +") b)AS RowConstrainedResult WHERE RowNum >= '" + (pgdata.getStart() - 1)
							+ "' AND RowNum <= '" + pgdata.getEnd() + "' order by syskey desc";		
					
					}*/
				}
			}		
		
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ArticleData data = new ArticleData();
			data.setSyskey(rs.getLong("syskey"));
			data.setT1(rs.getString("t1"));//title
			data.setT2(rs.getString("t2"));//content
			data.setT3(rs.getString("t3"));//type
			data.setT8(rs.getString("t8"));//you tube video
			data.setT13(rs.getString("t10"));
			data.setN1(rs.getLong("n1"));
			data.setN2(rs.getLong("n2"));//like count
			data.setN3(rs.getLong("n3"));//comment count
			data.setN4(rs.getLong("n4"));
			data.setN5(rs.getLong("n5"));//usersk
			data.setN6(rs.getLong("n6"));
			data.setN7(rs.getLong("n7"));//publish
			data.setN8(rs.getLong("n8"));
			data.setN9(rs.getLong("n9"));
			data.setN10(rs.getLong("n10"));//video=0,youtube=1
			if (data.getN7() == 1) {
				data.setT10("Draft");
			}
			if (data.getN7() == 2) {
				data.setT10("Pending");
			}
			if (data.getN7() == 3) {
				data.setT10("Modification");
			}
			if (data.getN7() == 4) {
				data.setT10("Approve");
			}
			if (data.getN7() == 5) {
				data.setT10("Publish");
			}
			if (data.getN7() == 6) {
				data.setT10("Reject");
			}
			data.setModifiedDate(rs.getString("modifieddate"));
			data.setModifiedTime(rs.getString("modifiedtime"));
			data.setUserName(rs.getString("username"));
			data.setModifiedUserName(rs.getString("modifiedusername"));
			data.setUpload(UploadDao.search(String.valueOf(data.getSyskey()), "", conn).getUploads());
			data.setVideoUpload(UploadDao.searchVideoList(String.valueOf(data.getSyskey()), conn).getVideoUpload());
			datalist.add(data);
		}
		res.setPageSize(pgdata.getSize());
		res.setCurrentPage(pgdata.getCurrent());
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		String qry = "";
		if(status==5) { //admin
			qry = "select count(*) as recCount from ( SELECT ROW_NUMBER() OVER (ORDER BY syskey)AS RowNum , * from fmr002 " + whereclause + " and userid in (select t1 from uvm012 where syskey in (select n4 from   uvm005 where syskey in (select n1 from jun002 where n2 in(select syskey from uvm009 where t2<>'master'  )))))AS RowConstrainedResult";
		}
		else if(status==1) { //content_writer
			qry = "select count(*) as recCount from ( SELECT ROW_NUMBER() OVER (ORDER BY syskey)AS RowNum , * from fmr002 " + whereclause + " and userid = '"+pgdata.getUserid()+"' and userid in (select t1 from uvm012 where syskey in (select n4 from   uvm005 where syskey in (select n1 from jun002 where n2 in(select syskey from uvm009 where t2='Content Writer')))))AS RowConstrainedResult";
		}
		else {
			if (regionCode.equals("00000000")) {
				qry = " SELECT count(*) as recCount FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc) AS RowNum,* FROM ( select * from fmr002 "
						+ whereclause + ") b)  AS RowConstrainedResult";
				// qry="SELECT count(*) as recCount from fmr002 "+ whereclause;
			} else if (regionCode.equals("10000000")) {
				qry = " SELECT count(*) as recCount FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc) AS RowNum,* FROM ( select * from fmr002 "
						+ whereclause + "and t5!='13000000') b)  AS RowConstrainedResult";
			}else if (regionCode.equals("13000000")) {
				qry = " SELECT count(*) as recCount FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc) AS RowNum,* FROM ( select * from fmr002 "
						+ whereclause + "and t5!='10000000') b)  AS RowConstrainedResult";
			}
			 
		}
		//stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM FMR002 " + whereclause);
		stat = conn.prepareStatement(qry);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}
	
	//atnnew//
	public static Result delete(Long syskey, Connection conn) throws SQLException {
		Result res = new Result();
		String sql = "UPDATE FMR002 SET RecordStatus=4 WHERE RecordStatus = 1 AND syskey=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Successfully!!!");
		} else {
			res.setMsgDesc("Deleting Unsuccessful!!!");
		}
		return res;
	}	
	/*read*/
	public static ArticleData read(long syskey, Connection conn) throws SQLException {
		ArticleData ret = new ArticleData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),"where RecordStatus<>4 AND  RecordStatus = 1 AND syskey=" + syskey, "", conn);
		if (dbrs.size() > 0)			
			ret = getDBRecord(dbrs.get(0));
		if(ret.getT5().equals("")){
			ret.setT5("00000000");
		}
		return ret;
	}

	public static ArticleDataSet readBysyskey(long syskey, Connection conn) throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		String whereclause = " WHERE RecordStatus<>4 AND RecordStatus = 1 AND syskey = '" + syskey + "' ";
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereclause, " ORDER BY syskey ", conn);
		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getDBRecord(dbrs.get(i)));
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	public static ArticleData readByKey(long syskey, Connection conn) throws SQLException {
		ArticleData ret = new ArticleData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "where  RecordStatus = 1 AND syskey=" + syskey, "",conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));
		return ret;
	}

	public static boolean isCodeExist(ArticleData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define()," where RecordStatus<>4 AND  RecordStatus = 1 AND syskey = " + obj.getSyskey() + " ", "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}	

	public static boolean isVDOExist(ArticleData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(defines("FMR002")," where RecordStatus<>4 AND  RecordStatus = 1 AND syskey = " + obj.getSyskey() + " ", "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isIDExist(ArticleData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define()," where RecordStatus<>4 AND  RecordStatus = 1 AND syskey = " + obj.getSyskey() + " ", "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	/*public static Result insert(ArticleData obj, Connection conn) throws SQLException {
		Result res = new Result();
		if (!isCodeExist(obj, conn)) {
			String sql = DBMgr.insertString(define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			int count = stmt.executeUpdate();
			if (count > 0) {
				res.setState(true);
				res.setMsgDesc("Saved Successfully!");
			}
		} else {
			res.setMsgDesc("Article Already Exist!");
		}
		return res;
	}*/
	
	public static Resultb2b insert(ArticleData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		/*if(obj.getT5().equals("00000000")){
			obj.setT5("");
		}*/
		String query = "INSERT INTO FMR002 (syskey, createddate, modifieddate, userid, username,"
				+ " recordStatus, syncStatus, syncBatch,usersyskey,t1, t2, t3, t4, t5, t6, t7, t8, t9, t10,"
				+ " t11, t12, t13,n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13,createdtime,modifiedtime,modifieduserid,modifiedusername)"
				+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?," 
				+ "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			if (!isCodeExist(obj, conn)) {
				PreparedStatement ps = conn.prepareStatement(query);
				ps.setLong(1, obj.getSyskey());
				ps.setString(2, obj.getCreatedDate());                  
				ps.setString(3, obj.getModifiedDate());
				ps.setString(4, obj.getUserId());
				ps.setString(5, obj.getUserName());
				ps.setInt(6, obj.getRecordStatus());
				ps.setInt(7, obj.getSyncStatus());
				ps.setLong(8, obj.getSyncBatch());
				ps.setLong(9, obj.getUserSyskey());
				ps.setString(10, obj.getT1());
				ps.setString(11, obj.getT2());
				ps.setString(12, obj.getT3());
				ps.setString(13, obj.getT4());
				ps.setString(14, obj.getT5());
				ps.setString(15, obj.getT6());
				ps.setString(16, obj.getT7());
				ps.setString(17, obj.getT8());
				ps.setString(18, obj.getT9());
				ps.setString(19, obj.getT10());
				ps.setString(20, obj.getT11());
				ps.setString(21, obj.getT12());
				ps.setString(22, obj.getT13());
				ps.setLong(23, obj.getN1());
				ps.setLong(24, obj.getN2());
				ps.setLong(25, obj.getN3());
				ps.setLong(26, obj.getN4());
				ps.setLong(27, obj.getN5());
				ps.setLong(28, obj.getN6());
				ps.setLong(29, obj.getN7());
				ps.setLong(30, obj.getN8());
				ps.setLong(31, obj.getN9());
				ps.setLong(32, obj.getN10());
				ps.setLong(33, obj.getN11());
				ps.setLong(34, obj.getN12());
				ps.setLong(35, obj.getN13());
				ps.setString(36, obj.getCreatedTime());
				ps.setString(37, obj.getModifiedTime());
				ps.setString(38, obj.getModifiedUserId());
				ps.setString(39, obj.getModifiedUserName());
				if (ps.executeUpdate() > 0) {
					res.setState(true);
					res.setMsgDesc("Saved Successfully!");
				} else {
					res.setMsgDesc("Article Already Exist!");
				}
			}
		} catch (SQLException e) {

		}
		return res;
	}
	/*atn*/
	public static DocumentData getDocRecord(DBRecord adbr) {
		DocumentData ret = new DocumentData();
		ret.setAutokey(adbr.getLong("autokey"));
		ret.setCreatedDate(adbr.getString("createddate"));
		ret.setTitle(adbr.getString("title"));
		ret.setModifiedDate(adbr.getString("modifieddate"));
		ret.setFileName(adbr.getString("fileName"));
		ret.setUserId(adbr.getString("userid"));
		ret.setStatus(adbr.getString("status"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setFilePath(adbr.getString("filepath"));
		ret.setRegion(adbr.getString("region"));
		ret.setFileType(adbr.getString("filetype"));
		ret.setFileSize(adbr.getString("filesize"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setN1(adbr.getInt("n1"));
		ret.setN2(adbr.getInt("n2"));
		ret.setN3(adbr.getInt("n3"));
		ret.setN4(adbr.getInt("n4"));
		ret.setN5(adbr.getInt("n5"));
		return ret;
	}
	//atn
		public static DBRecord definedb(String tabName) {
			DBRecord ret = new DBRecord();
			ret.setTableName(tabName);
			ret.setFields(new ArrayList<DBField>());
			ret.getFields().add(new DBField("autokey", (byte) 2));
			ret.getFields().add(new DBField("createddate", (byte) 5));
			ret.getFields().add(new DBField("modifieddate", (byte) 5));
			ret.getFields().add(new DBField("fileName", (byte) 5));
			ret.getFields().add(new DBField("RecordStatus", (byte) 1));
			ret.getFields().add(new DBField("status", (byte) 5));
			ret.getFields().add(new DBField("fileType", (byte) 5));
			ret.getFields().add(new DBField("region", (byte) 5));
			ret.getFields().add(new DBField("filePath", (byte) 5));
			ret.getFields().add(new DBField("postedBy", (byte) 5));
			ret.getFields().add(new DBField("modifiedBy", (byte) 5));
			ret.getFields().add(new DBField("fileSize", (byte) 5));
			ret.getFields().add(new DBField("title", (byte) 5));
			ret.getFields().add(new DBField("t1", (byte) 5));
			ret.getFields().add(new DBField("t2", (byte) 5));
			ret.getFields().add(new DBField("t3", (byte) 5));
			ret.getFields().add(new DBField("t4", (byte) 5));
			ret.getFields().add(new DBField("t5", (byte) 5));
			ret.getFields().add(new DBField("n1", (byte) 1));
			ret.getFields().add(new DBField("n2", (byte) 1));
			ret.getFields().add(new DBField("n3", (byte) 1));
			ret.getFields().add(new DBField("n4", (byte) 1));
			ret.getFields().add(new DBField("n5", (byte) 1));			
			return ret;
		}
	/*atn*/
	public static boolean isCodeExist(DocumentData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(definedb("document")," where RecordStatus<>4 AND  RecordStatus = 1 AND autokey = " + obj.getAutokey() + " ", "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}	
	//atn
	public static Resultb2b insert(DocumentData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String regionobj="";
		if(obj.getT2().equals("00000000")){
			obj.setRegion("-");
		}else if(obj.getT2().equals("13000000")){ 
			obj.setRegion("YANGON");
		}else if(obj.getT2().equals("10000000")){
			obj.setRegion("MANDALAY");
		}
		String query = "INSERT INTO Document (createddate, modifieddate,filename, userid,"//4
				+ " status,recordStatus,filetype,region,filepath,filesize,title,"//7
				+ "t1, t2, t3, t4, t5,n1,n2, n3, n4,postedby,modifiedby)"//11
				+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			if (!isCodeExist(obj, conn)) {				
				PreparedStatement ps = conn.prepareStatement(query);
				ps.setString(1, obj.getCreatedDate());
				ps.setString(2, obj.getModifiedDate());
				ps.setString(3, obj.getFileName());
				ps.setString(4, obj.getUserId());
				ps.setString(5, obj.getStatus());
				ps.setInt(6, obj.getRecordStatus());
				ps.setString(7, obj.getFileType());
				ps.setString(8, obj.getRegion());
				ps.setString(9, obj.getFilePath());
				ps.setString(10, obj.getFileSize());	
				ps.setString(11, obj.getTitle());	
				ps.setString(12, obj.getT1());
				ps.setString(13, obj.getT2());
				ps.setString(14, obj.getT3());
				ps.setString(15, obj.getT4());
				ps.setString(16, obj.getT5());
				ps.setInt(17, obj.getN1());
				ps.setInt(18, obj.getN2());
				ps.setInt(19, obj.getN3());
				ps.setInt(20, obj.getN4());
				//ps.setInt(22, obj.getN5());
				ps.setString(21, obj.getPostedby());
				ps.setString(22, obj.getModifiedby());
				if (ps.executeUpdate() > 0) {
					res.setState(true);
					res.setMsgDesc("Saved Successfully!");
				} else {
					res.setMsgDesc("Document Already Exist!");
				}				
			}
			String sql="select TOP 1 autokey FROM document ORDER BY autokey DESC";				
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				obj.setAutokey(rs.getInt("autokey"));
			}
		} catch (SQLException e) {

		}
		return res;
	}
	//atn
		public static Resultb2b update(DocumentData obj, Connection conn) throws SQLException {
	        Resultb2b res = new Resultb2b();
	        String regionobj="";
	        if(obj.getT2().equals("00000000")){
				obj.setRegion("-");
			}else if(obj.getT2().equals("13000000")){
				obj.setRegion("YANGON");
			}else if(obj.getT2().equals("10000000")){
				obj.setRegion("MANDALAY");
			}
	            String query = "UPDATE Document SET [modifieddate]=?, [filename]=?, [userid]=?, "
	                    + " [status]=?, [recordStatus]=?, [filetype]=?,[region]=?,[filePath]=?,[fileSize]=?,[title]=?, "
	                    + " [t1]=?, [t2]=?, [t3]=?,[t4]=?, [t5]=?,[n1]=?, [n2]=?, [n3]=?,[n4]=?,modifiedby=?"
	                    + "WHERE RecordStatus<>4 AND RecordStatus = 1 AND autokey="+ obj.getAutokey()+"";       
	            try {
	            if (isCodeExist(obj, conn)) {
	                PreparedStatement ps = conn.prepareStatement(query);
					ps.setString(1, obj.getModifiedDate());
					ps.setString(2, obj.getFileName());
					ps.setString(3, obj.getUserId());
					ps.setString(4, obj.getStatus());
					ps.setInt(5, obj.getRecordStatus());
					ps.setString(6, obj.getFileType());
					ps.setString(7, obj.getRegion());
					ps.setString(8, obj.getFilePath());
					ps.setString(9, obj.getFileSize());	
					ps.setString(10, obj.getTitle());	
					ps.setString(11, obj.getT1());
					ps.setString(12, obj.getT2());
					ps.setString(13, obj.getT3());
					ps.setString(14, obj.getT4());
					ps.setString(15, obj.getT5());
					ps.setInt(16, obj.getN1());
					ps.setInt(17, obj.getN2());
					ps.setInt(18, obj.getN3());
					ps.setInt(19, obj.getN4());
					ps.setString(20, obj.getModifiedby());					
					//ps.setInt(22, obj.getN5());             
	                
	                if(ps.executeUpdate()>0){
	                    res.setState(true);
	                    res.setMsgDesc("Updated Successfully!");
	                    }
	                else{
	                    res.setMsgDesc("No Such Document to Update!");
	                    }
		                String sql="select TOP 1 autokey FROM document ORDER BY autokey DESC";				
						ps = conn.prepareStatement(sql);
						ResultSet rs = ps.executeQuery();
						if (rs.next()) {
							obj.setAutokey(rs.getInt("autokey"));
						}				
	                }
	             }catch (SQLException e) {
	                    
	             }    
	            
	    
	            return res;
	        }
	/*atn*/
	public static Resultb2b deleteDoc(DocumentData data, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = "UPDATE Document SET RecordStatus=4,modifiedDate=?,modifiedby=? WHERE RecordStatus = 1 AND autokey=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, data.getModifiedDate());
		stmt.setString(2, data.getModifiedby());
		stmt.setLong(3, data.getAutokey());
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Successfully!!!");
		} else {
			res.setMsgDesc("Deleting Unsuccessful!!!");
		}
		return res;
	}
	/*read*/
	public static DocumentData readDoc(long autokey, Connection conn) throws SQLException {
		DocumentData ret = new DocumentData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(definedb("document"),"where RecordStatus<>4 AND  RecordStatus = 1 AND autokey=" + autokey, "", conn);
		if (dbrs.size() > 0)
			ret = getDocRecord(dbrs.get(0));
		/*if (ServerUtil.isUniEncoded(ret.getT2())) {
			ret.setT2(FontChangeUtil.uni2zg(ret.getT2()));
		}*/
		return ret;
	}
	/*atn*/
	public static DocumentDataSet searchDocumentList(PagerData pgdata,String userid,String statustype,Connection conn) throws SQLException {
		DocumentDataSet res = new DocumentDataSet();
		ArrayList<DocumentData> datalist = new ArrayList<DocumentData>();
		PreparedStatement stat;
		String whereclause="";
		whereclause = " where RecordStatus<>4  AND  RecordStatus = 1";	// AND region='"+ pgdata.getRegion()+"'	
		if (!pgdata.getT1().equals("")) {
			whereclause += "AND (title LIKE N'%" + pgdata.getT1()					
					+ "%' OR  filename LIKE N'%" + pgdata.getT1() +
					"%' OR  status LIKE N'%" + pgdata.getT1() + "%'"
					+ " OR  modifiedDate LIKE N'%" + pgdata.getT1() + "%'"
					+ " OR  postedby LIKE N'%" + pgdata.getT1() + "%') ";
		}
		if (!statustype.equalsIgnoreCase("")) {
			whereclause += " AND (status='" + statustype + "')";
		}
		String query="select t5 from uvm005_A where t1='"+userid+"'";
		PreparedStatement pstmt = conn.prepareStatement(query);
		ResultSet rst = pstmt.executeQuery();
		String sql = "";
		String regioncode="";
		while (rst.next()) {
			regioncode=rst.getString("t5");
			if (regioncode.equals("00000000")) {				
				sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY autokey desc) AS RowNum,* FROM"
						+ " ( select * from document"+whereclause+") b) "
						+ " AS RowConstrainedResult WHERE RowNum >= '" + pgdata.getStart() + "' AND RowNum <= '"
						+ pgdata.getEnd() + "' ";				
			}else if (regioncode.equals("10000000")) {				
				sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY autokey desc) AS RowNum,* FROM"
						+ " ( select * from document"+whereclause+" and t2!='13000000') b) "
						+ " AS RowConstrainedResult WHERE RowNum >= '" + pgdata.getStart() + "' AND RowNum <= '"
						+ pgdata.getEnd() + "' ";				
			}else if (regioncode.equals("13000000")) {				
				sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY autokey desc) AS RowNum,* FROM"
						+ " ( select * from document"+whereclause+" and t2!='10000000') b) "
						+ " AS RowConstrainedResult WHERE RowNum >= '" + pgdata.getStart() + "' AND RowNum <= '"
						+ pgdata.getEnd() + "' ";				
			}
			/*else {
				sql = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY autokey desc) AS RowNum,* FROM"
						+ "(select a.autokey,a.createddate,"
						+ "a.modifieddate,a.filename,a.status,a.filetype,a.region,a.filepath,a.postedby,a.ModifiedBy,a.FileSize,a.Pages,a.title,a.t1 "
						+ "FROM document a inner join uvm005_A b on a.t2=b.t5 and b.t1='" + userid + "' "
						+ whereclause + ")b)" + " AS RowConstrainedResult WHERE RowNum >= '" + (pgdata.getStart() - 1)
						+ "' AND RowNum <= '" + pgdata.getEnd() + "' ";	
			}*/
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				DocumentData data = new DocumentData();
				data.setAutokey(rs.getLong("autokey"));
				data.setTitle(rs.getString("title"));// title
				data.setFileName(rs.getString("filename"));
				data.setFilePath(rs.getString("filepath"));
				data.setModifiedDate(rs.getString("modifieddate").substring(0, 10));
				data.setModifiedTime(rs.getString("modifieddate").substring(11, 16));
				data.setStatus(rs.getString("status"));
				data.setPostedby(rs.getString("postedby"));
				data.setModifiedby(rs.getString("modifiedby"));
				datalist.add(data);
			}
		}
		res.setPageSize(pgdata.getSize());
		res.setCurrentPage(pgdata.getCurrent());
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		String qry = "";
		if (regioncode.equals("00000000")) {
			qry = " SELECT count(*) as recCount FROM ( SELECT ROW_NUMBER() OVER (ORDER BY autokey desc) AS RowNum,* FROM ( select * from document "
					+ whereclause + ") b)  AS RowConstrainedResult";
		}
		if (regioncode.equals("10000000")) {
			qry = " SELECT count(*) as recCount FROM ( SELECT ROW_NUMBER() OVER (ORDER BY autokey desc) AS RowNum,* FROM ( select * from document "
					+ whereclause +" and t2!='13000000') b)  AS RowConstrainedResult";
		}
		if (regioncode.equals("13000000")) {
			qry = " SELECT count(*) as recCount FROM ( SELECT ROW_NUMBER() OVER (ORDER BY autokey desc) AS RowNum,* FROM ( select * from document "
					+ whereclause +" and t2!='10000000') b)  AS RowConstrainedResult";
		}
		stat = conn.prepareStatement(qry);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		DocumentData[] dataarray = new DocumentData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}
	public static Resultb2b insertVideo(ArticleData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = DBMgr.insertString(defines("FMR002"), conn);
		PreparedStatement stmt = conn.prepareStatement(sql);
		DBRecord dbr = setDBRecord(obj);
		DBMgr.setValues(stmt, dbr);
		int count = stmt.executeUpdate();
		if (count > 0) {
			res.setState(true);
			res.setMsgDesc("Saved Successfully!");
		}
		return res;
	}

	/*public static Result update(ArticleData obj, Connection conn) throws SQLException {
		Result res = new Result();
		if (isCodeExist(obj, conn)) {
			String sql = DBMgr.updateString(" WHERE RecordStatus<>4 AND RecordStatus = 1 AND Syskey=" + obj.getSyskey(),define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			int count = stmt.executeUpdate();
			if (count > 0) {
				res.setState(true);
				res.setMsgDesc("Updated Successfully!");
			}
		} else {
			res.setMsgDesc("No Such Article to Update!");
		}
		return res;
	}*/
	
	public static Resultb2b update(ArticleData obj, Connection conn) throws SQLException {
        Resultb2b res = new Resultb2b();
        int effectedrow = 0;
        /*if(obj.getT5().equals("00000000")){
			obj.setT5("");
		}*/
            String query = "UPDATE FMR002 SET [syskey]=?, [modifieddate]=?, [userid]=?, [username]=?, "
                    + " [recordstatus]=?, [syncstatus]=?, [syncbatch]=?,[usersyskey]=?, [t1]=?, [t2]=?, [t3]=?,[t4]=?, [t5]=?,"
                    + " [t6]=?, [t7]=?,[t8]=?, [t9]=?,[t10]=?, [t11]=?,[t12]=?, [t13]=?, [n1]=?, [n2]=?, [n3]=?,"
                    + " [n4]=?, [n5]=?,[n6]=?, [n7]=?,[n8]=?, [n9]=?,[n10]=?, [n11]=?,[n12]=?, [n13]=?, [createdtime]=?,[modifiedtime]=?,[modifieduserid]=?,[modifiedusername]=? "
                    + "WHERE RecordStatus<>4 AND RecordStatus = 1 AND Syskey="+ obj.getSyskey()+"";
        
            try {
            if (isCodeExist(obj, conn)) {
                PreparedStatement pstmt = conn.prepareStatement(query);
                pstmt.setLong(1,obj.getSyskey());
               
                pstmt.setString(2, obj.getModifiedDate());
                pstmt.setString(3, obj.getUserId());
                pstmt.setString(4, obj.getUserName());
                pstmt.setInt(5, obj.getRecordStatus());
                pstmt.setInt(6, obj.getSyncStatus());
                pstmt.setLong(7, obj.getSyncBatch());
                pstmt.setLong(8, obj.getUserSyskey());
                pstmt.setString(9,obj.getT1());
                pstmt.setString(10, obj.getT2());
                pstmt.setString(11, obj.getT3());
                pstmt.setString(12, obj.getT4());
                pstmt.setString(13, obj.getT5());
                pstmt.setString(14, obj.getT6());
                pstmt.setString(15, obj.getT7());
                pstmt.setString(16, obj.getT8());
                pstmt.setString(17, obj.getT9());
                pstmt.setString(18, obj.getT10());
                pstmt.setString(19, obj.getT11());
                pstmt.setString(20, obj.getT12());
                pstmt.setString(21, obj.getT13());
                pstmt.setLong(22, obj.getN1());
                pstmt.setLong(23, obj.getN2());
                pstmt.setLong(24, obj.getN3());
                pstmt.setLong(25, obj.getN4());
                pstmt.setLong(26, obj.getN5());
                pstmt.setLong(27, obj.getN6());
                pstmt.setLong(28, obj.getN7());
                pstmt.setLong(29, obj.getN8());
                pstmt.setLong(30, obj.getN9());
                pstmt.setLong(31, obj.getN10());
                pstmt.setLong(32, obj.getN11());
                pstmt.setLong(33, obj.getN12());
                pstmt.setLong(34, obj.getN13());
                pstmt.setString(35,obj.getCreatedTime());
                pstmt.setString(36,obj.getModifiedTime());
                pstmt.setString(37,obj.getModifiedUserId());
                pstmt.setString(38, obj.getModifiedUserName());
                effectedrow = pstmt.executeUpdate();
                if(effectedrow>0){
                    res.setState(true);
                    res.setMsgDesc("Updated Successfully!");
                    }
                else{
                    res.setMsgDesc("No Such Article to Update!");
                    }
                }
             }catch (SQLException e) {
                    
             }    
            
    
            return res;
        }	

	public static Resultb2b updateVideo(ArticleData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		if (isVDOExist(obj, conn)) {
			String sql = DBMgr.updateString(" WHERE RecordStatus<>4 AND RecordStatus = 1 AND Syskey=" + obj.getSyskey(),defines("FMR002"), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			int count = stmt.executeUpdate();
			if (count > 0) {
				res.setState(true);
				res.setMsgDesc("Updated Successfully!");
			}
		} else {
			res.setMsgDesc("No Such Article to Update!");
		}
		return res;
	}

	public static Resultb2b saveAns(ArticleData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = "UPDATE FMR002 SET t4='" + obj.getT2() + "' WHERE RecordStatus = 1 AND syskey=" + obj.getSyskey();
		PreparedStatement stmt = conn.prepareStatement(sql);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Saved Successfully!!!");
		} else {
			res.setMsgDesc("Deleting Unsuccessful!!!");
		}
		return res;
	}

	
	/*public static ArticleDataSet search(PagerData pgdata, String searchVal, String type, Connection conn)throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		String whereclause = " WHERE RecordStatus<>4  AND  RecordStatus = 1 AND t3 = '" + type + "' ";
		if (!searchVal.equals("")) {
			whereclause += "AND ( t1 LIKE '%"+searchVal+"%' OR t2 LIKE '%"+searchVal+"%' OR  )";
		}
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecordSandE(define(), whereclause, " ORDER BY syskey ",(pgdata.getStart() - 1), pgdata.getEnd(), 0, conn);
		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getDBRecord(dbrs.get(i)));
		}
		res.setPageSize(pgdata.getSize());
		res.setCurrentPage(pgdata.getCurrent());
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		PreparedStatement stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM FMR002 " + whereclause);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}*/

	public static ArticleDataSet searchAns(String searchVal, String type, Connection conn) throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		String whereclause = " WHERE RecordStatus = 1 AND t3 = '" + type + "' AND syskey = " + searchVal;
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereclause, " ORDER BY syskey ", conn);
		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getDBRecord(dbrs.get(i)));
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	/*public static ArticleDataSet searchQnA(PagerData pgdata, String mobile, String firstRefresh, String searchVal,String type, String check, String usersk, Connection conn, MrBean user) throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		RegisterData sender = RegisterDao.searchByPh(mobile, conn);
		String sql = "";
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		if (firstRefresh.equalsIgnoreCase("1")) {
			ArticleDao.deleteTemp(usersk, conn);
			datalist = new ArrayList<ArticleData>();
			datalist = ArticleDao.readAll(" WHERE RecordStatus <> 4  AND  RecordStatus = 1 AND n7=5 AND t3 = '" + type + "' "
							            + " AND syskey NOT IN ( SELECT n1 FROM jun004 WHERE recordstatus <> 4 AND n2 IN (SELECT syskey "
							            + " FROM fmr012 WHERE t2 = '" + sender.getT9() + "' AND t3 = '0' AND recordstatus <> 4)) ",conn);
			for (int i = 0; i < datalist.size(); i++) {
				datalist.get(i).setN1(datalist.get(i).getSyskey());
				datalist.get(i).setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID())));
				datalist.get(i).setN9(Long.parseLong(usersk));
				ArticleDao.insertTemp(datalist.get(i), conn);
			}
			if (type.equalsIgnoreCase("article")) {
				for (int i = 0; i < datalist.size(); i++) {
					ArrayList<UploadData> photolist = new ArrayList<UploadData>();
					photolist = UploadDao.readByN1(datalist.get(i).getSyskey(), conn);
					datalist.get(i).setUploadDatalist(photolist);
				}
			}
			if (!sender.getT9().equalsIgnoreCase("")) {
				datalist = new ArrayList<ArticleData>();
				datalist = ArticleDao.readAll(" WHERE RecordStatus <> 4  AND  RecordStatus = 1  AND n7=5 AND t3 = '" + type+ "' "
						+ " AND syskey IN ( SELECT n1 FROM jun004 WHERE recordstatus <> 4 AND n2 IN (SELECT syskey "
						+ " FROM fmr012 WHERE t2 = '" + sender.getT9() + "' AND t3 = '0' AND recordstatus <> 4))",
						conn);
				for (int i = 0; i < datalist.size(); i++) {
					datalist.get(i).setN1(datalist.get(i).getSyskey());
					datalist.get(i).setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID())));
					datalist.get(i).setN9(Long.parseLong(usersk));
					ArticleDao.insertTemp(datalist.get(i), conn);
				}
				if (type.equalsIgnoreCase("article")) {
					for (int i = 0; i < datalist.size(); i++) {
						ArrayList<UploadData> photolist = new ArrayList<UploadData>();
						photolist = UploadDao.readByN1(datalist.get(i).getSyskey(), conn);
						datalist.get(i).setUploadDatalist(photolist);
					}
				}
			}
		}
		datalist = new ArrayList<ArticleData>();
		sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey) AS RowNum,* FROM Temp_All where" + " (t3 = '"
				+ type + "') AND n7=5 AND   n9='"+usersk+"' AND"
				+ " (modifieddate between (CONVERT(NVARCHAR(8),DATEADD(MONTH,-1, GETDATE()),112)) AND "
				+ " (CONVERT(NVARCHAR(8),GETDATE(),112))))AS RowConstrainedResult  " + " WHERE RowNum >= "
				+ pgdata.getStart() + " and RowNum <= " + pgdata.getEnd();
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ArticleData data = new ArticleData();
			data.setSyskey(rs.getLong("syskey"));
			data.setCreatedDate(rs.getString("createddate"));
			data.setModifiedDate(rs.getString("modifieddate"));
			data.setT1(rs.getString("t1"));
			data.setT2(rs.getString("t2"));
			data.setT3(rs.getString("t3"));
			data.setN1(rs.getLong("n1"));
			data.setN2(rs.getLong("n2"));
			data.setN3(rs.getLong("n3"));
			data.setN5(rs.getLong("n5"));
			data.setN8(rs.getLong("n8")); // dislike count
			data.setCreatedTime(rs.getString("createdtime"));
			data.setModifiedTime(rs.getString("modifiedtime"));
			datalist.add(data);
		}
		res.setPageSize(pgdata.getSize());
		res.setCurrentPage(pgdata.getCurrent());
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		sql = " SELECT count(*) AS records FROM FMR002 WHERE RecordStatus <> 4  AND  RecordStatus = 1  AND t3 = '"
				+ type + "' ";
		ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();
		if (rs.next()) {
			res.setTotalCount(rs.getInt("records"));
		}
		return res;
	}*/

	public static ArticleDataSet searchArticle(PagerData pgdata, String searchVal, String type, Connection conn)throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		String whereclause = " WHERE RecordStatus<>4  AND  RecordStatus = 1 AND t3 = '" + type + "'   ";
		if (!searchVal.equals("")) {
			whereclause += "AND t1 LIKE '%" + searchVal + "%' AND t2 LIKE '%" + searchVal + "%'  ";
		}
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecordSandE(define(), whereclause, " ORDER BY syskey ",(pgdata.getStart() - 1), pgdata.getEnd(), 0, conn);
		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getDBRecord(dbrs.get(i)));
		}
		res.setPageSize(pgdata.getSize());
		res.setCurrentPage(pgdata.getCurrent());
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		PreparedStatement stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM FMR002 " + whereclause);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	// video user view
	public static ArticleDataSet searchVideoUserLists(PagerData pgdata, String mobile, String searchVal, String type,Connection conn) throws SQLException {
		String whereclause = "";
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		RegisterData sender = RegisterDao.searchByPh(mobile, conn);
		whereclause = " WHERE RecordStatus<>4  AND  RecordStatus = 1 AND t3 = '" + type + "' AND ( t3 = '" + type
				+ "' AND (t4 = '" + sender.getT9() + "' OR t4 = 'Others')) OR (t3 = '" + type + "' AND (t5='"
				+ sender.getT18() + "' OR t6 ='" + sender.getT19() + "'" + ")) OR (t3 = '" + type + "' AND (t7='"
				+ sender.getT7() + "' OR t8 ='" + sender.getT8() + "'" + "))";

		if (!searchVal.equals("")) {
			whereclause += "AND t1 LIKE '%" + searchVal + "%' OR t2 LIKE '%" + searchVal + "%'  ";
		}
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecordSandE(defines("FMR011"), whereclause, " ORDER BY syskey DESC",
				(pgdata.getStart() - 1), pgdata.getEnd(), 0, conn);

		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getDBRecord(dbrs.get(i)));
		}

		res.setPageSize(pgdata.getSize());
		res.setCurrentPage(pgdata.getCurrent());
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}

		PreparedStatement stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM FMR011 " + whereclause);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	
	public static Resultb2b updateLikeCount(Long syskey, String userSK, String type, Connection conn, MrBean user)throws SQLException {
		long key = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
		Resultb2b res = new Resultb2b();
		long userKey = Long.parseLong(userSK);
		if (isDisLikeExist(syskey, userKey, conn)) {
			String sql = "Update FMR002 SET n8 = n8-1 WHERE syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			stmt.executeUpdate();
			String query = "UPDATE FMR008 SET RecordStatus=4 WHERE  RecordStatus = 1 AND t4='dislike' AND  n1 = '" + syskey+ "' AND n2 = '" + userKey + "'";
			PreparedStatement st = conn.prepareStatement(query);
			st.executeUpdate();
		}
		RegisterData userData = RegisterDao.readData(userKey, conn);
		if (!isLikeExist(syskey, userKey, conn)) {
			String sql = "Update FMR002 SET n2 = n2+1 WHERE syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
				if (!isStatusExist(syskey, userKey, "like", conn)) {
					String query = "INSERT INTO FMR008 (syskey,createddate, modifieddate, userid, username, RecordStatus, SyncStatus, SyncBatch, usersyskey,T1, T2,T3, T4,T5, T6,T7, T8,T9, T10,T11, T12,T13, T14,T15, n1,n2,n3, n4,n5, n6,n7, n8,n9, n10,n11, n12,n13, n14,n15) "+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					PreparedStatement psmt = conn.prepareStatement(query);
					psmt.setLong(1, key);
					psmt.setString(2, todayDate);
					psmt.setString(3, todayDate);
					psmt.setString(4, userData.getT1());
					psmt.setString(5, userData.getT3());
					psmt.setInt(6, 1);
					psmt.setInt(7, 1);
					psmt.setLong(8, 0);
					psmt.setLong(9, 0);
					psmt.setString(10, userData.getT1());
					psmt.setString(11, userData.getT3());
					psmt.setString(12, type);
					psmt.setString(13, "like");
					psmt.setString(14, "0");
					psmt.setString(15, "0");
					psmt.setString(16, "0");
					psmt.setString(17, "0");
					psmt.setString(18, "0");
					psmt.setString(19, "0");
					psmt.setString(20, "0");
					psmt.setString(21, "0");
					psmt.setString(22, "0");
					psmt.setString(23, "0");
					psmt.setString(24, "0");
					psmt.setLong(25, syskey);
					psmt.setLong(26, userKey);
					psmt.setLong(27, 0);
					psmt.setLong(28, 0);
					psmt.setLong(29, 0);
					psmt.setLong(30, 0);
					psmt.setLong(31, 0);
					psmt.setLong(32, 0);
					psmt.setLong(33, 0);
					psmt.setLong(34, 0);
					psmt.setLong(35, 0);
					psmt.setLong(36, 0);
					psmt.setLong(37, 0);
					psmt.setLong(38, 0);
					psmt.setLong(39, 0);
					if (psmt.executeUpdate() > 0) {
						System.out.println("Saved successfully");
					} else {
						System.out.println("Save Unsuccessfully");
					}
				} else {
					String query = "UPDATE FMR008 SET RecordStatus=1 WHERE  RecordStatus = 4 AND t4='like' AND n1 = '"+ syskey + "' AND n2 = '" + userKey + "'";
					PreparedStatement stm = conn.prepareStatement(query);
					int result = stm.executeUpdate();
					if (result > 0) {
						res.setState(true);
						System.out.println("Updated successfully");
					} else {
						res.setState(false);
						System.out.println("Updated Unsuccessfully");
					}
				}
			}
		} else {
			String sql = "Update FMR002 SET n2 = n2-1 WHERE  RecordStatus = 1 AND syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String query = "UPDATE FMR008 SET RecordStatus=4 WHERE  RecordStatus = 1 AND  t4='like' AND n1 = '"+ syskey + "' AND n2 = '" + userKey + "'";
				PreparedStatement stm = conn.prepareStatement(query);
				int result = stm.executeUpdate();
				if (result > 0) {
					res.setState(true);
				} else {
					res.setState(false);
				}
			} else {
				res.setState(false);
			}
		}
		res.setN2(getTotalDisLikeCount(syskey, conn));
		res.setKeyResult(getTotalLikeCount(syskey, conn));
		return res;
	}

	
	public static boolean isLikeExist(Long syskey, Long userkey, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR008"), " where RecordStatus<>4 AND t4='like' AND RecordStatus = 1 AND n1 = " + syskey + " AND n2 = " + userkey+ " ","", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isDisLikeExist(Long syskey, Long userkey, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR008")," where RecordStatus<>4 AND t4='dislike' AND RecordStatus = 1 AND n1 = " + syskey + " AND n2 = "+ userkey + " ","", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isStatusExist(Long syskey, Long userkey, String type, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR008"),
				" where RecordStatus = 4 AND t4='" + type + "'  AND n1 = " + syskey + " AND n2 = " + userkey + " ", "",	conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static ArticleData setStatusData(ArticleData data, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		String todaytime = new SimpleDateFormat("HH:mm a").format(Calendar.getInstance().getTime());
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(todaytime);
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(todaytime);
		data.setUserId(user.getUser().getUserId());
		data.setUserName(user.getUser().getUserName());
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		return data;
	}

	public static Resultb2b updateUnlikeCount(Long syskey, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = "Update FMR002 SET n4 = n4+1 WHERE RecordStatus = 1 AND syskey = ?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
		}
		return res;
	}
	public static Resultb2b UnlikeArticle(Long syskey, Long userSK, Connection conn, MrBean user) throws SQLException {
		Resultb2b res = new Resultb2b();
		long userKey = userSK;
		if (isLikeExist(syskey, userKey, conn)) {
			res.setN1(searchDisLikeOrNot(syskey, String.valueOf(userSK), conn));
			String sql = "Update FMR002 SET n2 = n2-1 WHERE  RecordStatus = 1 AND syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String query = "UPDATE FMR008 SET RecordStatus=4 WHERE RecordStatus = 1 AND t4='like' AND n1 = '"+ syskey + "' AND n2 = '" + userKey + "'";
				PreparedStatement stm = conn.prepareStatement(query);
				int result = stm.executeUpdate();
				if (result > 0) {
					res.setState(true);
				} else {
					res.setState(false);
				}

			} else {
				res.setState(false);
			}
			res.setN2(getTotalDisLikeCount(syskey, conn));
			res.setKeyResult(getTotalLikeCount(syskey, conn));

		} else {

		}
		return res;
	}
	public static Resultb2b UnDislikeArticle(Long syskey, Long userSK, Connection conn, MrBean user) throws SQLException {
		Resultb2b res = new Resultb2b();
		long userKey = userSK;
		if (isDisLikeExist(syskey, userKey, conn)) {
			res.setN1(searchLikeOrNot(syskey, String.valueOf(userSK), conn));
			String sql = "Update FMR002 SET n8 = n8-1 WHERE  RecordStatus = 1 AND syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String query = "UPDATE FMR008 SET RecordStatus=4 WHERE RecordStatus = 1 AND t4='dislike' AND n1 = '"+ syskey + "' AND n2 = '" + userKey + "'";
				PreparedStatement stm = conn.prepareStatement(query);
				int result = stm.executeUpdate();
				if (result > 0) {
					res.setState(true);
				} else {
					res.setState(false);
				}
			} else {
				res.setState(false);
			}
			res.setN2(getTotalLikeCount(syskey, conn));
			res.setKeyResult(getTotalDisLikeCount(syskey, conn));

		} else {

		}
		return res;
	}

	public static Resultb2b UnlikeVideo(Long syskey, Long userSK, Connection conn, MrBean user) throws SQLException {
		Resultb2b res = new Resultb2b();
		long userKey = userSK;
		String type = "Video";
		if (isLikeExist(syskey, userKey, conn)) {
			res.setN1(ArticleDao.searchDisLikeOrNot(syskey, String.valueOf(userSK), conn));
			String sql = "Update FMR002 SET n2 = n2-1 WHERE  RecordStatus = 1 AND t3='" + type + "' AND syskey ='"+ syskey + "'";
			PreparedStatement stmt = conn.prepareStatement(sql);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String query = "UPDATE FMR008 SET RecordStatus=4 WHERE RecordStatus = 1 AND t4='like' AND n1 = '" + syskey+ "' AND n2 = '" + userKey + "'";
				PreparedStatement stm = conn.prepareStatement(query);
				int result = stm.executeUpdate();
				if (result > 0) {
					res.setState(true);
				} else {
					res.setState(false);
				}
			} else {
				res.setState(false);
			}
			res.setN2(getTotalDisLikeCount(syskey, conn));
			res.setKeyResult(getTotalLikeCount(syskey, conn));

		} else {

		}
		return res;
	}


	public static Resultb2b UnDislikeVideo(Long syskey, Long userSK, Connection conn, MrBean user) throws SQLException {
		Resultb2b res = new Resultb2b();
		long userKey = userSK;
		String type = "Video";
		if (isDisLikeExist(syskey, userKey, conn)) {
			res.setN1(ArticleDao.searchLikeOrNot(syskey, String.valueOf(userSK), conn));
			String sql = "Update FMR002 SET n2 = n2-1 WHERE  RecordStatus = 1 AND t3='" + type + "' AND syskey ='"+ syskey + "'";
			PreparedStatement stmt = conn.prepareStatement(sql);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String query = "UPDATE FMR008 SET RecordStatus=4 WHERE RecordStatus = 1 AND t4='dislike' AND n1 = '" + syskey+ "' AND n2 = '" + userKey + "'";
				PreparedStatement stm = conn.prepareStatement(query);
				int result = stm.executeUpdate();
				if (result > 0) {
					res.setState(true);
				} else {
					res.setState(false);
				}
			} else {
				res.setState(false);
			}
			res.setN2(getTotalLikeCount(syskey, conn));
			res.setKeyResult(getTotalDisLikeCount(syskey, conn));

		} else {

		}
		return res;
	}

	public static Resultb2b updateCommentCount(Long syskey, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = "Update FMR002 SET n3 = n3+1 WHERE  RecordStatus = 1 AND syskey = ?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
		}
		return res;
	}

	// for video
	public static Resultb2b updateCommentCountforVideo(Long syskey, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = "Update FMR011 SET n3 = n3+1 WHERE  RecordStatus = 1 AND syskey = ?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
		}
		return res;
	}

	public static Resultb2b reduceCommentCount(Long syskey, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = "Update FMR002 SET n3 = n3-1 WHERE  RecordStatus = 1 AND RecordStatus<>4 AND syskey = ?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
		}
		return res;
	}

	public static Resultb2b reduceComment(Long syskey, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = "Update FMR002 SET n3 = n3-1 WHERE  RecordStatus = 1 AND n3 > 0 AND syskey = ?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
		}
		return res;
	}

	public static ArticleDataSet view(String searchVal, String type, Connection conn) throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		String whereclause = " WHERE RecordStatus<>4 AND RecordStatus = 1 AND t3 = '" + type + "' ";
		if (!searchVal.equals("")) {
			whereclause += "AND syskey = '" + searchVal + "' AND RecordStatus = 1 AND RecordStatus<>4 ";
		}
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereclause, " ORDER BY syskey ", conn);
		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getDBRecord(dbrs.get(i)));
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	// mobile
	public static ArticleDataSet viewByKey(String searchVal, long userSK, String type, Connection conn)throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		String whereclause = " WHERE RecordStatus = 1 AND t3 = '" + type + "'  ";
		if (!searchVal.equals("")) {
			whereclause += "AND syskey = '" + searchVal + "' AND RecordStatus = 1  ";
		}
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereclause, " ORDER BY syskey ", conn);
		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getDBRecord(dbrs.get(i)));
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	public static ArticleDataSet searchLike(long key, String type, Connection conn) throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		String whereclause = " WHERE RecordStatus<>4 AND RecordStatus = 1 AND t3 = '" + type + "'  AND n1 = " + key+ " ";
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR008"), whereclause, " ORDER BY syskey ", conn);
		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getDBRecord(dbrs.get(i)));
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	public static ArticleDataSet searchLikeArticle(String type, long key, Connection conn) throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		String whereclause = " WHERE RecordStatus<>4 AND RecordStatus = 1 AND t3 = '" + type + "'  AND n1 = " + key	+ " ";
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR008"), whereclause, " ORDER BY syskey ", conn);
		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getDBRecord(dbrs.get(i)));
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	public static ArticleDataSet viewArticle(String searchVal, Connection conn) throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		String whereclause = " WHERE RecordStatus<>4 AND RecordStatus = 1  ";
		if (!searchVal.equals("")) {
			whereclause += "AND syskey = '" + searchVal + "' AND RecordStatus = 1 AND RecordStatus<>4";
		}
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereclause, " ORDER BY syskey ", conn);
		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getDBRecord(dbrs.get(i)));
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	// new
	public static Resultb2b updateLikeCountArticle(Long syskey, String userSK, String type, Connection conn, MrBean user)throws SQLException {
		long key = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
		Resultb2b res = new Resultb2b();
		long userKey = Long.parseLong(userSK);
		if (isDisLikeExist(syskey, userKey, conn)) {
			String sql = "Update FMR002 SET n8 = n8-1 WHERE syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			stmt.executeUpdate();
			String query = "UPDATE FMR008 SET RecordStatus=4 WHERE  RecordStatus = 1 AND t4='dislike' AND  n1 = '"+ syskey + "' AND n2 = '" + userKey + "'";
			PreparedStatement st = conn.prepareStatement(query);
			st.executeUpdate();
		}
		RegisterData userData = RegisterDao.readData(userKey, conn);
		if (!isLikeExist(syskey, userKey, conn)) {
			String sql = "Update FMR002 SET n2 = n2+1 WHERE syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
				if (!isStatusExist(syskey, userKey, "like", conn)) {
					String query = "INSERT INTO FMR008 (syskey,createddate, modifieddate, userid, username, RecordStatus, SyncStatus, SyncBatch, usersyskey,T1, T2,T3, T4,T5, T6,T7, T8,T9, T10,T11, T12,T13, T14,T15, n1,n2,n3, n4,n5, n6,n7, n8,n9, n10,n11, n12,n13, n14,n15) "
							+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					PreparedStatement psmt = conn.prepareStatement(query);
					psmt.setLong(1, key);
					psmt.setString(2, todayDate);
					psmt.setString(3, todayDate);
					psmt.setString(4, userData.getT1());
					psmt.setString(5, userData.getT3());
					psmt.setInt(6, 1);
					psmt.setInt(7, 1);
					psmt.setLong(8, 0);
					psmt.setLong(9, 0);
					psmt.setString(10, userData.getT1());
					psmt.setString(11, userData.getT3());
					psmt.setString(12, type);
					psmt.setString(13, "like"); // like status
					psmt.setString(14, "0");
					psmt.setString(15, "0");
					psmt.setString(16, "0");
					psmt.setString(17, "0");
					psmt.setString(18, "0");
					psmt.setString(19, "0");
					psmt.setString(20, "0");
					psmt.setString(21, "0");
					psmt.setString(22, "0");
					psmt.setString(23, "0");
					psmt.setString(24, "0");
					psmt.setLong(25, syskey);
					psmt.setLong(26, userKey);
					psmt.setLong(27, 0);
					psmt.setLong(28, 0);
					psmt.setLong(29, 0);
					psmt.setLong(30, 0);
					psmt.setLong(31, 0);
					psmt.setLong(32, 0);
					psmt.setLong(33, 0);
					psmt.setLong(34, 0);
					psmt.setLong(35, 0);
					psmt.setLong(36, 0);
					psmt.setLong(37, 0);
					psmt.setLong(38, 0);
					psmt.setLong(39, 0);
					if (psmt.executeUpdate() > 0) {

					} else {
					}
				} else {
					String query = "UPDATE FMR008 SET RecordStatus=1 WHERE  RecordStatus = 4 AND t4='like' AND  n1 = '"+ syskey + "' AND n2 = '" + userKey + "'";
					PreparedStatement stm = conn.prepareStatement(query);

					int result = stm.executeUpdate();
					if (result > 0) {
						res.setState(true);
					} else {
						res.setState(false);
					}
				}

			}
		} else {
			String sql = "Update FMR002 SET n2 = n2-1 WHERE  RecordStatus = 1 AND syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String query = "UPDATE FMR008 SET RecordStatus=4 WHERE  RecordStatus = 1 AND t4='like' AND  n1 = '"
						+ syskey + "' AND n2 = '" + userKey + "'";
				PreparedStatement stm = conn.prepareStatement(query);

				int result = stm.executeUpdate();
				if (result > 0) {
					res.setState(true);
				} else {
					res.setState(false);
				}

			} else {
				res.setState(false);
			}
		}
		res.setN2(getTotalDisLikeCount(syskey, conn));
		res.setKeyResult(getTotalLikeCount(syskey, conn));
		return res;

	}

	// update dislike Count 20170718
	public static Resultb2b updateDisLikeCountArticle(Long syskey, String userSK, String type, Connection conn,MrBean user) throws SQLException {
		long key = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
		Resultb2b res = new Resultb2b();
		long userKey = Long.parseLong(userSK);
		RegisterData userData = RegisterDao.readData(userKey, conn);
		if (isLikeExist(syskey, userKey, conn)) {
			String sql = "Update FMR002 SET n2 = n2-1 WHERE syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			stmt.executeUpdate();
			String query = "UPDATE FMR008 SET RecordStatus=4 WHERE  RecordStatus = 1 AND t4='like' AND  n1 = '" + syskey+ "' AND n2 = '" + userKey + "'";
			PreparedStatement st = conn.prepareStatement(query);
			st.executeUpdate();
		}
		if (!isDisLikeExist(syskey, userKey, conn)) {
			String sql = "Update FMR002 SET n8 = n8+1 WHERE syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
				if (!isStatusExist(syskey, userKey, "dislike", conn)) {
					String query = "INSERT INTO FMR008 (syskey,createddate, modifieddate, userid, username, RecordStatus, SyncStatus, SyncBatch, usersyskey,T1, T2,T3, T4,T5, T6,T7, T8,T9, T10,T11, T12,T13, T14,T15, n1,n2,n3, n4,n5, n6,n7, n8,n9, n10,n11, n12,n13, n14,n15) "
							      + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					PreparedStatement psmt = conn.prepareStatement(query);
					psmt.setLong(1, key);
					psmt.setString(2, todayDate);
					psmt.setString(3, todayDate);
					psmt.setString(4, userData.getT1());
					psmt.setString(5, userData.getT3());
					psmt.setInt(6, 1);
					psmt.setInt(7, 1);
					psmt.setLong(8, 0);
					psmt.setLong(9, 0);
					psmt.setString(10, userData.getT1());
					psmt.setString(11, userData.getT3());
					psmt.setString(12, type);
					psmt.setString(13, "dislike"); // dislike status
					psmt.setString(14, "0");
					psmt.setString(15, "0");
					psmt.setString(16, "0");
					psmt.setString(17, "0");
					psmt.setString(18, "0");
					psmt.setString(19, "0");
					psmt.setString(20, "0");
					psmt.setString(21, "0");
					psmt.setString(22, "0");
					psmt.setString(23, "0");
					psmt.setString(24, "0");
					psmt.setLong(25, syskey);
					psmt.setLong(26, userKey);
					psmt.setLong(27, 0);
					psmt.setLong(28, 0);
					psmt.setLong(29, 0);
					psmt.setLong(30, 0);
					psmt.setLong(31, 0);
					psmt.setLong(32, 0);
					psmt.setLong(33, 0);
					psmt.setLong(34, 0);
					psmt.setLong(35, 0);
					psmt.setLong(36, 0);
					psmt.setLong(37, 0);
					psmt.setLong(38, 0);
					psmt.setLong(39, 0);
					if (psmt.executeUpdate() > 0) {
					} else {
					}
				} else {
					String query = "UPDATE FMR008 SET RecordStatus=1 WHERE  RecordStatus = 4 AND t4='dislike' AND n1 = '"
							     + syskey + "' AND n2 = '" + userKey + "'";
					PreparedStatement stm = conn.prepareStatement(query);

					int result = stm.executeUpdate();
					if (result > 0) {
						res.setState(true);
					} else {
						res.setState(false);
					}

				}

			}
		} else {
			String sql = "Update FMR002 SET n8 = n8-1 WHERE  RecordStatus = 1 AND syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String query = "UPDATE FMR008 SET RecordStatus=4 WHERE  RecordStatus = 1 AND t4='dislike' AND  n1 = '"
						      + syskey + "' AND n2 = '" + userKey + "'";
				PreparedStatement stm = conn.prepareStatement(query);

				int result = stm.executeUpdate();
				if (result > 0) {
					res.setState(true);
				} else {
					res.setState(false);
				}

			} else {
				res.setState(false);
			}
		}
		res.setN2(getTotalLikeCount(syskey, conn));
		res.setKeyResult(getTotalDisLikeCount(syskey, conn));
		return res;
	}

	public static ArticleDataSet searchLikeCount(String id, Connection conn, MrBean user) throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		long syskey = Long.parseLong(id);
		//long userKey = OPTDao.searchByID(user.getUser().getUserId(), conn);
		String whereclause = " WHERE RecordStatus<>4 AND RecordStatus = 1 AND syskey = '" + syskey + "'   ";
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR002"), whereclause, " ORDER BY syskey ", conn);
		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getDBRecord(dbrs.get(i)));
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	public static long searchLikeUser(long n1, Connection conn, MrBean user) throws SQLException {
		long key;
		long userKey = OPTDao.searchByID(user.getUser().getUserId(), conn);
		String sql = "Select n2 from FMR008 WHERE RecordStatus<>4 AND RecordStatus = 1 AND n1 = " + n1 + " AND n2 = '"+ userKey + "'";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = rs.getLong("n2");
		} else {
			key = 0;
		}
		return key;
	}

	public static long searchLikesUser(long n1, long userKey, Connection conn, MrBean user) throws SQLException {
		long key;
		String sql = "Select n2 from FMR008 WHERE RecordStatus<>4 AND RecordStatus = 1 AND n1 = " + n1 + " AND n2 = '"+ userKey + "'";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = rs.getLong("n2");
		} else {
			key = 0;
		}
		return key;
	}
	
	

	public static String ddMMyyyFormat(String aDate) {// for all list
		String l_Date = "";
		if (!aDate.equals("") && aDate != null)
			l_Date = aDate.substring(6) + "/" + aDate.substring(4, 6) + "/" + aDate.substring(0, 4);
		return l_Date;
	}

	public static String MMddyyyFormat(String aDate, String month)// format(Feb
	{// for all list
		String l_Date = "";
		if (!aDate.equals("") && aDate != null)
			l_Date = month + " " + aDate.substring(6) + ", " + aDate.substring(0, 4);
		return l_Date;
	}

	public static long getTotalLikeCount(long syskey, Connection conn) throws SQLException {
		long key = 0;
		String sql = " Select n2 from  FMR002 WHERE  RecordStatus = 1 AND syskey = " + syskey;
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = rs.getLong("n2");
		}
		return key;
	}

	// for dislike
	public static long getTotalDisLikeCount(long syskey, Connection conn) throws SQLException {
		long key = 0;
		String sql = " Select n8 from  FMR002 WHERE  RecordStatus = 1 AND syskey = " + syskey;
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = rs.getLong("n8");
		}
		return key;
	}

	public static CommentDataSet searchLikeUserList(String n1, Connection conn) throws SQLException {
		CommentDataSet res = new CommentDataSet();
		ArrayList<CommentData> datalist = new ArrayList<CommentData>();
		String sql = " Select t1,t2,n2 from FMR008 WHERE RecordStatus<>4  AND n1 = " + n1;
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			RegisterData uData = RegisterDao.readData(rs.getLong("n2"), conn);
			CommentData data = new CommentData();
			data.setUserId(rs.getString("t1"));
			data.setUserName(uData.getT3());
			data.setUserSyskey(rs.getLong("n2"));
			datalist.add(data);
		}
		CommentData[] dataarray = new CommentData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	public static long searchLikeOrNot(long n1, String userSk, Connection conn) throws SQLException {
		long key = 0;
		String sql = " Select n2 from FMR008 WHERE RecordStatus<>4 AND RecordStatus = 1 AND t4='like' AND n1 = " + n1+ " AND n2 = '" + userSk + "'";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = 1;
		} else {
			key = 0;
		}
		return key;
	}

	public static long searchDisLikeOrNot(long n1, String userSk, Connection conn) throws SQLException {
		long key = 0;
		String sql = "Select n2 from FMR008 WHERE RecordStatus<>4 AND RecordStatus = 1 AND t4='dislike' AND n1 = " + n1+ " AND n2 = '" + userSk + "'";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = 1;
		} else {
			key = 0;
		}
		return key;
	}	

	// get commentsk
	public static ArrayList<ArticleData> getcommentsk(String n1, String userKey, Connection conn) throws SQLException {
		ArrayList<ArticleData> res = new ArrayList<ArticleData>();
		String sql = "Select syskey from FMR003 WHERE RecordStatus<>4 AND RecordStatus = 1 AND n1 = " + n1 + "";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			ArticleData ret = new ArticleData();
			ret.setSyskey(rs.getLong("syskey"));
			res.add(ret);
		}
		return res;

	}

	// TDA for Q and A
	public static long searchLikeOrNotsQuestion(String n1, String userKey, Long comsk, Connection conn)throws SQLException {
		long key = 0;
		String sql = "Select * from FMR016 WHERE RecordStatus<>4 AND RecordStatus = 1 AND n1 = " + n1 + " AND n2 = '"+ userKey + "'   AND n3 = '" + comsk + "'";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = 1;
		} else {
			key = 0;
		}
		return key;
	}

	// TDA for Q and A
	public static long searchLikeOrNotsQuestionSaveC(String n1, String userKey, Long comsk, Connection conn)throws SQLException {
		long key = 0;
		String sql = "Select * from FMR016 WHERE RecordStatus<>4 AND RecordStatus = 1 AND n1 = " + n1 + " AND n2 = '"+ userKey + "'   AND n3 = '" + comsk + "'";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = 1;
		} else {
			key = 0;
		}
		return key;
	}

	// get savecomment
	public static ArrayList<ArticleData> searchLikeOrNotsQuestionSaveCommen(String n1, String userKey, Long comsk,Connection conn) throws SQLException {
		ArrayList<ArticleData> res = new ArrayList<ArticleData>();
		String sql = "Select * from FMR016 WHERE RecordStatus<>4 AND RecordStatus = 1 AND n1 = " + n1 + " AND n2 = '"+ userKey + "'   AND n3 = '" + comsk + "'";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			ArticleData ret = new ArticleData();
			ret.setSyskey(1);
			res.add(ret);
		}
		return res;

	}

	// TDA For Video
	public static long searchLikeOrNotsVideo(String n1, String userKey, Connection conn) throws SQLException {
		long key = 0;
		String sql = "Select * from FMR003 WHERE RecordStatus<>4 AND RecordStatus = 1 AND n1 = " + n1 + " AND n5 = '"+ userKey + "'";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = 1;
		} else {
			key = 0;
		}
		return key;
	}

	public static String searchByPh(String mobile, Connection conn) {
		String sqlString = "";
		String sender = "";
		sqlString = "select t9 from Register where RecordStatus<>4 AND t1='" + mobile + "'";
		try {
			Statement stmts = conn.createStatement();
			ResultSet rs = stmts.executeQuery(sqlString);
			if (rs.next()) {
				sender = rs.getString("t9");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return sender;
	}

	public static ArticleData readByID(long aObj, Connection conn) throws SQLException {
		ArticleData data = new ArticleData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "where RecordStatus=1 AND syskey='" + aObj + "'", "",conn);
		if (dbrs.size() > 0) {
			data = getDBRecord(dbrs.get(0));
			data.setT2(ServerUtil.decryptPIN(data.getT2()));
		}
		return data;
	}

	public static ArticleData readID(long aObj, Connection conn) throws SQLException {
		ArticleData data = new ArticleData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "where RecordStatus=1 AND syskey='" + aObj + "'", "",conn);
		if (dbrs.size() > 0) {
			data = getDBRecord(dbrs.get(0));
		}
		return data;
	}
	public static long searchCommentCount(long syskey, Connection conn) throws SQLException {
		long key = 0;
		String sql = "Select n3 from FMR002 WHERE  RecordStatus = 1 AND syskey = " + syskey + " ";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = rs.getLong("n3");
		} else {
			key = 0;
		}
		return key;
	}
	
	public static long searchlikecount(long syskey, Connection conn) throws SQLException {
		long key = 0;
		String sql = "Select n2 from FMR002 WHERE  RecordStatus = 1 AND syskey = " + syskey + " ";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = rs.getLong("n2");
		} else {
			key = 0;
		}
		return key;
	}


	// for alltype
	public static ArrayList<ArticleData> getArticleDataAllList(String usersk, Connection conn, MrBean user)
			throws SQLException {
		ArrayList<ArticleData> list = new ArrayList<ArticleData>();
		String caldate = "";
		String sql1 = "select CONVERT(NVARCHAR(8),DATEADD(DD, DATEDIFF(DY, 0, GETDATE()), -6),112) as date";
		PreparedStatement stat = conn.prepareStatement(sql1);
		ResultSet rs1 = stat.executeQuery();
		if (rs1.next()) {
			caldate = rs1.getString("date");
		}
		String sql = "select * from fmr002 where modifieddate between '" + caldate
				+ "' and GETDATE() and recordstatus<>4 and t4<>'alert' and t3<>'sample' and n7=5  order by syskey DESC";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ArticleData ret = new ArticleData();
			ret.setSyskey(rs.getLong("syskey"));
			ret.setCreatedDate(rs.getString("createddate"));
			ret.setCreatedTime(rs.getString("createdtime"));
			ret.setModifiedDate(rs.getString("modifieddate"));
			ret.setModifiedTime(rs.getString("modifiedtime"));
			ret.setUserId(rs.getString("userid"));
			ret.setUserName(rs.getString("username"));
			ret.setRecordStatus(rs.getInt("RecordStatus"));
			ret.setSyncStatus(rs.getInt("SyncStatus"));
			ret.setSyncBatch(rs.getInt("SyncBatch"));
			ret.setUserSyskey(rs.getLong("syskey"));
			ret.setT1(rs.getString("t1"));
			ret.setT2(rs.getString("t2"));
			ret.setT3(rs.getString("t3"));
			ret.setT4(rs.getString("t4"));
			ret.setT5(rs.getString("t5"));
			ret.setT6(rs.getString("t6"));
			ret.setT7(rs.getString("t7"));
			ret.setT8(rs.getString("t8"));
			ret.setT9(rs.getString("t9"));
			ret.setT10(rs.getString("t10"));
			ret.setT11(rs.getString("t11"));
			ret.setT12(rs.getString("t12"));
			ret.setT13(rs.getString("t13"));
			ret.setN1(rs.getLong("n1"));
			ret.setN2(rs.getLong("n2"));
			ret.setN3(rs.getLong("n3"));
			ret.setN4(rs.getLong("n4"));
			ret.setN5(rs.getLong("n5"));
			ret.setN6(ArticleDao.searchLikeOrNot(ret.getSyskey(), usersk, conn));
			ret.setN7(ContentMenuDao.searchSaveContentOrNot(usersk, ret.getSyskey(), conn));// 1=savecontent,0=unsavecontent
			ret.setAnswer(CommentDao.searchAnswer(ret.getSyskey(), "answer", conn).getAnsData());// getAnswerData
			ret.setN8(rs.getLong("n8"));
			ret.setN9(ArticleDao.searchDisLikeOrNot(ret.getSyskey(), usersk, conn));
			ret.setN10(rs.getLong("n10"));
			ret.setN11(rs.getLong("n11"));
			ret.setN12(rs.getLong("n12"));
			ret.setN13(rs.getLong("n13"));
			ret.setUploadedPhoto(UploadDao.search(String.valueOf(ret.getSyskey()), ret.getT3(), conn).getData());
			if (ret.getT3().equalsIgnoreCase("video")) {
				ret.setVideoUpload(UploadDao.searchVideoList(String.valueOf(ret.getSyskey()), conn).getVideoUpload());
			}
			list.add(ret);
		}
		return list;
	}

	
	public static Resultb2b insertTemp(ArticleData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = DBMgr.insertString(defines("Temp_All"), conn);
		PreparedStatement stmt = conn.prepareStatement(sql);
		DBRecord dbr = setDBRecord(obj);
		DBMgr.setValues(stmt, dbr);
		int count = stmt.executeUpdate();
		if (count > 0) {
			res.setState(true);
		}
		return res;
	}

	/*public static Result deleteTemp(String usersk, Connection conn) throws SQLException {
		Result res = new Result();
		String sql = " DELETE FROM TEMP_All WHERE N9 = '" + usersk + "'";
		PreparedStatement stmt = conn.prepareStatement(sql);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
		}
		return res;
	}*/

	public static ArrayList<ArticleData> readAll(String whereclause, Connection conn) throws SQLException {
		ArrayList<ArticleData> ret = new ArrayList<ArticleData>();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereclause, "", conn);
		for (int i = 0; i < dbrs.size(); i++) {
			ret.add(getDBRecord(dbrs.get(i)));
		}
		return ret;
	}

	//TDA for share
	/*public static ArticleData readForShare(long postsk,String type, Connection conn) throws SQLException {
		ArticleData ret = new ArticleData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),"where RecordStatus<>4 AND  t3='"+type+"' AND syskey=" + postsk, "", conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));
		return ret;
	}

	public static ArticleDataSet readShare(long postsk, String type, Connection conn) {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		String whereclause = " WHERE RecordStatus<>4 AND RecordStatus = 1 AND syskey = '" + postsk + "' and t3='" + type+ "' ";
		ArrayList<DBRecord> dbrs;
		try {
			dbrs = DBMgr.getDBRecords(define(), whereclause, " ORDER BY syskey ", conn);
			for (int i = 0; i < dbrs.size(); i++) {
				datalist.add(getDBRecord(dbrs.get(i)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}*/
	//////////////////////////////////////CMS//////////////////////////////
	public static DivisionComboDataSet getStatusList(long status,Connection conn) throws SQLException {
		DivisionComboDataSet dataset = new DivisionComboDataSet();
		ArrayList<DivisionComboData> datalist = new ArrayList<DivisionComboData>();
		DivisionComboData combo;
		String sql  = "";
		if(status==1){ //contend writer
			sql = "select n1,t1 from status where n1 in(6,1,2) order by n1 Desc";
		}
		if(status==2){ //editor
			 sql = "select n1,t1 from status where recordstatus<>4 and(n1=2 or n1=4 or n1=6)";
		}
		if(status==3){ //publisher
			 sql = "select n1,t1 from status where recordstatus<>4 and(n1=4 or n1=5 or n1=6)";
		}
		if(status==4){ //master
			 sql = "select n1,t1 from status where recordstatus<>4 and(n1=1 or n1=2 or n1=4 or n1=5 or n1=6)";
		}
		if(status==5){
			 sql = "select n1,t1 from status where recordstatus<>4 and(n1=1 or n1=2 or n1=4 or n1=5 or n1=6)";
		
		}
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			combo = new DivisionComboData();
			combo.setCaption(res.getString("t1"));
			combo.setValue(res.getString("n1"));
			datalist.add(combo);
		}
		DivisionComboData[] dataarray = new DivisionComboData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		dataset.setData(dataarray);
		return dataset;
	}

	public static long getRoleStatusCount(int status, Connection conn) {
		String sql = "";
		long count=0;
			try {
				if (status == 1) {
					sql = "select count(*) as recount from fmr002 where recordstatus<>4  and n7=6";
				}
				if (status == 2) {
					sql = "select count(*) as recount from fmr002 where recordstatus<>4 and n7=2";
				}
				if (status == 3) {
					sql = "select count(*) as recount from fmr002 where recordstatus<>4 and n7=4";
				}
				if (status == 4) {
					sql = "select count(*) as recount from fmr002 where recordstatus<>4 and n7=6";
				}
				if (status == 5) {
					sql = "select count(*) as recount from fmr002 where recordstatus<>4 and n7=6";
				}

				PreparedStatement ps = conn.prepareStatement(sql);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
				
				count=(rs.getLong("recount"));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			
		}
		return count;
	}

	
	public static ArticleDataSet getMenuCount(long status, Connection conn) {
		ArticleDataSet res=new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		try {
			String sql="select t3,count(*) as recount from fmr002 where recordstatus<>4 and n7='"+status+"'and t3='question' group by t3 ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs;
			rs = ps.executeQuery();
			while (rs.next()){
				ArticleData ret=new ArticleData();
				ret.setT3(rs.getString("t3"));
				ret.setN1(rs.getLong("recount"));
				datalist.add(ret);
			}
			
			String sql1="select t3,count(*) as recount from fmr002 where recordstatus<>4 and n7='"+status+"'and t3='news & media' group by t3 ";
			PreparedStatement ps1 = conn.prepareStatement(sql1);
			ResultSet rs1;
			rs1 = ps1.executeQuery();
			while (rs1.next()){
				ArticleData ret=new ArticleData();
				ret.setT3(rs1.getString("t3"));
				ret.setN1(rs1.getLong("recount"));
				datalist.add(ret);
			}
			String sql2="select t3,count(*) as recount from fmr002 where recordstatus<>4 and n7='"+status+"'and t3='education' group by t3 ";
			PreparedStatement ps2 = conn.prepareStatement(sql2);
			ResultSet rs2;
			rs2 = ps2.executeQuery();
			while (rs2.next()){
				ArticleData ret=new ArticleData();
				ret.setT3(rs2.getString("t3"));
				ret.setN1(rs2.getLong("recount"));
				datalist.add(ret);
			}
			String sql3="select t3,count(*) as recount from fmr002 where recordstatus<>4 and n7='"+status+"'and t3='Video' group by t3 ";
			PreparedStatement ps3 = conn.prepareStatement(sql3);
			ResultSet rs3;
			rs3 = ps3.executeQuery();
			while (rs3.next()){
				ArticleData ret=new ArticleData();
				ret.setT3(rs3.getString("t3"));
				ret.setN1(rs3.getLong("recount"));
				datalist.add(ret);
			}
			ArticleData[] dataarray = new ArticleData[datalist.size()];
			dataarray = datalist.toArray(dataarray);
			res.setData(dataarray);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return res;
	}
	
	public static Result deleteByUpdate(Long syskey, Connection conn) throws SQLException {
		Result res = new Result();
		String sql = "DELETE FROM FMR008 WHERE n1=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Successfully!!!");
		}
		return res;
	}

	//atnnew//
	public static Result deleteByUpdateComment(Long syskey, Connection conn) throws SQLException {
		Result res = new Result();
		String sql = "DELETE FROM FMR003 WHERE n1=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Successfully!!!");
		}
		return res;
	}
	
	/*atn*/
	public static ArticleDataSet searchVideoList(PagerData pgdata,long status,String statetype,Connection conn) throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		PreparedStatement stat;
		String whereclause="";
		String whereclausenew="";
			 whereclause = " WHERE RecordStatus<>4  AND  RecordStatus = 1 and t3='Video' ";
			 whereclausenew=" and a.RecordStatus<>4  AND  a.RecordStatus = 1 and a.t3='Video' ";
		if (statetype.equalsIgnoreCase("")) {
			if (status == 1) {
				whereclause += "AND n7=6";// draft
				whereclausenew += "AND a.n7=6";
			}
			if (status == 2) {
				whereclause += "AND n7=2";// pending
				whereclausenew += "AND a.n7=2";
			}
			if (status == 3) {
				whereclause += "AND n7=4";// approved
				whereclausenew += "AND a.n7=4";
			}
			if (status == 4) {
				whereclause += "AND n7=1";// approved
				whereclausenew += "AND a.n7=1";
			}
			if (status == 5) {
				whereclause += "AND n7=1";// approved
				whereclausenew += "AND a.n7=1";
			}
		}
		if (!statetype.equalsIgnoreCase("")) {
			whereclause += "AND (n7='" + statetype + "')";
			whereclausenew += "AND (a.n7='" + statetype + "')";
		}
		if (!pgdata.getT1().equals("")) {
			whereclause += "AND (t1 LIKE N'%" + pgdata.getT1() + "%' OR  modifieddate LIKE N'%" + pgdata.getT1()
					+ "%' OR modifiedtime LIKE N'%" + pgdata.getT1() + "%' OR  t2 LIKE N'%" + pgdata.getT1()
					+ "%' OR  username LIKE N'%" + pgdata.getT1() + "%') ";
		}
		String sql="";
		String regionCode="";
		if(status==5) { //admin
			sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc)AS RowNum , * from fmr002 " + whereclause + " and userid in (select t1 from uvm012 where syskey in (select n4 from   uvm005 where syskey in (select n1 from jun002 where n2 in(select syskey from uvm009 where t2<>'master'  )))))AS RowConstrainedResult  WHERE RowNum >="
				+ pgdata.getStart() + " and RowNum <=" + pgdata.getEnd();
		}
		else if(status==1) { //content_writer
			sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc)AS RowNum , * from fmr002 " + whereclause + " and userid = '"+pgdata.getUserid()+"' and userid in (select t1 from uvm012 where syskey in (select n4 from   uvm005 where syskey in (select n1 from jun002 where n2 in(select syskey from uvm009 where t2='Content Writer')))))AS RowConstrainedResult  WHERE RowNum >="
					+ pgdata.getStart() + " and RowNum <=" + pgdata.getEnd();
		}
		else {			
			String query="select t5 from uvm005_A where t1='"+pgdata.getUserid()+"'";
			PreparedStatement pstmt = conn.prepareStatement(query);
			ResultSet rst = pstmt.executeQuery();
			while (rst.next()) {
				regionCode=rst.getString("t5");
				if (regionCode.equals("00000000")) {
					sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc) AS RowNum,* FROM ( select * from fmr002 "
							+ whereclause + ") b)  AS RowConstrainedResult WHERE RowNum >= '" +pgdata.getStart() 
							+ "' AND RowNum <= '" + pgdata.getEnd() + "' ";				
				}else if (regionCode.equals("10000000")) {
					sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc) AS RowNum,* FROM ( select * from fmr002 "
							+ whereclause + "and t5!='13000000') b)  AS RowConstrainedResult WHERE RowNum >= '" +pgdata.getStart()
							+ "' AND RowNum <= '" + pgdata.getEnd() + "' ";				
				}else if (regionCode.equals("13000000")) {
					sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc) AS RowNum,* FROM ( select * from fmr002 "
							+ whereclause + "and t5!='10000000') b)  AS RowConstrainedResult WHERE RowNum >= '" + pgdata.getStart()
							+ "' AND RowNum <= '" + pgdata.getEnd() + "' ";				
				}
				/*else {
					sql = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc) AS RowNum,* FROM "
							+ "(select a.syskey,a.t1,a.t2,a.t3,a.t5,a.t8,a.t10,a.n1,a.n2,a.n3,a.n4,a.n5,a.n6,"
							+ "a.n7,a.n8,a.n9,a.n10,a.modifieddate,a.modifiedtime,a.username,a.modifieduserid,a.modifiedusername "
							+ "from fmr002 a,UVM005_A b WHERE a.t5=b.t5 and b.t1='"+pgdata.getUserid()+"'"
							+ whereclausenew +") b)AS RowConstrainedResult WHERE RowNum >= '" + (pgdata.getStart() - 1)
							+ "' AND RowNum <= '" + pgdata.getEnd() + "' order by syskey desc";							
					}*/
				}
			}
		/*else {
		sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc) AS RowNum,* FROM ( select * from fmr002 "
				+ whereclause + ") b)  AS RowConstrainedResult WHERE RowNum >= '" + (pgdata.getStart() - 1)
				+ "' AND RowNum <= '" + pgdata.getEnd() + "' ";
		}*/
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ArticleData data = new ArticleData();
			data.setSyskey(rs.getLong("syskey"));
			data.setT1(rs.getString("t1"));//title
			data.setT2(rs.getString("t2"));//content
			data.setT3(rs.getString("t3"));//type
			data.setT8(rs.getString("t8"));//you tube video
			data.setN1(rs.getLong("n1"));
			data.setN2(rs.getLong("n2"));//like count
			data.setN3(rs.getLong("n3"));//comment count
			data.setN4(rs.getLong("n4"));
			data.setN5(rs.getLong("n5"));//usersk
			data.setN6(rs.getLong("n6"));
			data.setN7(rs.getLong("n7"));//publish
			data.setN8(rs.getLong("n8"));
			data.setN9(rs.getLong("n9"));
			data.setN10(rs.getLong("n10"));//video=0,youtube=1
			if (data.getN7() == 1) {
				data.setT10("Draft");
			}
			if (data.getN7() == 2) {
				data.setT10("Pending");
			}
			if (data.getN7() == 3) {
				data.setT10("Modification");
			}
			if (data.getN7() == 4) {
				data.setT10("Approve");
			}
			if (data.getN7() == 5) {
				data.setT10("Publish");
			}
			if (data.getN7() == 6) {
				data.setT10("Reject");
			}
			data.setModifiedDate(rs.getString("modifieddate"));
			data.setModifiedTime(rs.getString("modifiedtime"));
			data.setUserName(rs.getString("username"));
			data.setModifiedUserId(rs.getString("modifieduserid"));
			data.setModifiedUserName(rs.getString("modifiedusername"));
			data.setUpload(UploadDao.search(String.valueOf(data.getSyskey()), "", conn).getUploads());
			data.setVideoUpload(UploadDao.searchVideoList(String.valueOf(data.getSyskey()), conn).getVideoUpload());
			datalist.add(data);
		}
		res.setPageSize(pgdata.getSize());
		res.setCurrentPage(pgdata.getCurrent());
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		String qry = "";
		if(status==5) { //admin
			qry = "select count(*) as recCount from ( SELECT ROW_NUMBER() OVER (ORDER BY syskey)AS RowNum , * from fmr002 " + whereclause + " and userid in (select t1 from uvm012 where syskey in (select n4 from   uvm005 where syskey in (select n1 from jun002 where n2 in(select syskey from uvm009 where t2<>'master'  )))))AS RowConstrainedResult";
		}
		else if(status==1) { //content_writer
			qry = "select count(*) as recCount from ( SELECT ROW_NUMBER() OVER (ORDER BY syskey)AS RowNum , * from fmr002 " + whereclause + " and userid = '"+pgdata.getUserid()+"' and userid in (select t1 from uvm012 where syskey in (select n4 from   uvm005 where syskey in (select n1 from jun002 where n2 in(select syskey from uvm009 where t2='Content Writer')))))AS RowConstrainedResult";
		}
		else {
			if (regionCode.equals("00000000")) {
				qry = " SELECT count(*) as recCount FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc) AS RowNum,* FROM ( select * from fmr002 "
				+ whereclause + ") b)  AS RowConstrainedResult";
			}else if(regionCode.equals("10000000")){
				qry = " SELECT count(*) as recCount FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc) AS RowNum,* FROM ( select * from fmr002 "
						+ whereclause + "and t5!='13000000') b)  AS RowConstrainedResult";
			}else if(regionCode.equals("13000000")){
				qry = " SELECT count(*) as recCount FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc) AS RowNum,* FROM ( select * from fmr002 "
						+ whereclause + "and t5!='10000000') b)  AS RowConstrainedResult";	
			}
		}
		//stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM FMR002 " + whereclause);
		stat = conn.prepareStatement(qry);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}	
	
	public static Result insertNew(ArticleData obj, Connection conn) throws SQLException {
		Result res = new Result();
		String query = "INSERT INTO FMR002 (syskey, createddate, modifieddate, userid, username,"
				+ " recordStatus, syncStatus, syncBatch,usersyskey,t1, t2, t3, t4, t5, t6, t7, t8, t9, t10,"
				+ " t11, t12, t13,n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13,createdtime,modifiedtime,modifieduserid,modifiedusername"
				+ ") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?," + "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?"
						+ ")";
		try {
			if (!isCodeExist(obj, conn)) {
				PreparedStatement ps = conn.prepareStatement(query);
				ps.setLong(1, obj.getSyskey());
				ps.setString(2, obj.getCreatedDate());
				ps.setString(3, obj.getModifiedDate());
				ps.setString(4, obj.getUserId());
				ps.setString(5, obj.getUserName());
				ps.setInt(6, obj.getRecordStatus());
				ps.setInt(7, obj.getSyncStatus());
				ps.setLong(8, obj.getSyncBatch());
				ps.setLong(9, obj.getUserSyskey());
				ps.setString(10, obj.getT1());
				ps.setString(11, obj.getT2());
				ps.setString(12, obj.getT3());
				ps.setString(13, obj.getT4());
				ps.setString(14, obj.getT5());
				ps.setString(15, obj.getT6());
				ps.setString(16, obj.getT7());
				ps.setString(17, obj.getT8());
				ps.setString(18, obj.getT9());
				ps.setString(19, obj.getT10());
				ps.setString(20, obj.getT11());
				ps.setString(21, obj.getT12());
				ps.setString(22, obj.getT13());
				ps.setLong(23, obj.getN1());
				ps.setLong(24, obj.getN2());
				ps.setLong(25, obj.getN3());
				ps.setLong(26, obj.getN4());
				ps.setLong(27, obj.getN5());
				ps.setLong(28, obj.getN6());
				ps.setLong(29, obj.getN7());
				ps.setLong(30, obj.getN8());
				ps.setLong(31, obj.getN9());
				ps.setLong(32, obj.getN10());
				ps.setLong(33, obj.getN11());
				ps.setLong(34, obj.getN12());
				ps.setLong(35, obj.getN13());
				ps.setString(36, obj.getCreatedTime());
				ps.setString(37, obj.getModifiedTime());
				ps.setString(38, obj.getModifiedUserId());
				ps.setString(39, obj.getModifiedUserName());
				if (ps.executeUpdate() > 0) {
					res.setState(true);
					res.setMsgDesc("Saved Successfully");
				} else {
					res.setMsgDesc("Article Already Exist!");
				}
			}
		} catch (SQLException e) {

		}
		return res;
	}
	public static Result updateNew(ArticleData obj, Connection conn) throws SQLException {
		Result res = new Result();
		String query = "UPDATE FMR002 SET [syskey]=?, [modifieddate]=?, [userid]=?, [username]=?, "
				+ " [recordstatus]=?, [syncstatus]=?, [syncbatch]=?,[usersyskey]=?, [t1]=?, [t2]=?, [t3]=?,[t4]=?, [t5]=?,"
				+ " [t6]=?, [t7]=?,[t8]=?, [t9]=?,[t10]=?, [t11]=?,[t12]=?, [t13]=?, [n1]=?, [n2]=?, [n3]=?,"
				+ " [n4]=?, [n5]=?,[n6]=?, [n7]=?,[n8]=?, [n9]=?,[n10]=?, [n11]=?,[n12]=?, [n13]=?, [createdtime]=?,[modifiedtime]=?,[modifieduserid]=?,[modifiedusername]=? "
				+ "WHERE RecordStatus<>4 AND RecordStatus = 1 AND Syskey=" + obj.getSyskey() + "";

		try {
			if (isCodeExist(obj, conn)) {
				PreparedStatement pstmt = conn.prepareStatement(query);
				pstmt.setLong(1, obj.getSyskey());

				pstmt.setString(2, obj.getModifiedDate());
				pstmt.setString(3, obj.getUserId());
				pstmt.setString(4, obj.getUserName());
				pstmt.setInt(5, obj.getRecordStatus());
				pstmt.setInt(6, obj.getSyncStatus());
				pstmt.setLong(7, obj.getSyncBatch());
				pstmt.setLong(8, obj.getUserSyskey());
				pstmt.setString(9, obj.getT1());
				pstmt.setString(10, obj.getT2());
				pstmt.setString(11, obj.getT3());
				pstmt.setString(12, obj.getT4());
				pstmt.setString(13, obj.getT5());
				pstmt.setString(14, obj.getT6());
				pstmt.setString(15, obj.getT7());
				pstmt.setString(16, obj.getT8());
				pstmt.setString(17, obj.getT9());
				pstmt.setString(18, obj.getT10());
				pstmt.setString(19, obj.getT11());
				pstmt.setString(20, obj.getT12());
				pstmt.setString(21, obj.getT13());
				pstmt.setLong(22, obj.getN1());
				pstmt.setLong(23, obj.getN2());
				pstmt.setLong(24, obj.getN3());
				pstmt.setLong(25, obj.getN4());
				pstmt.setLong(26, obj.getN5());
				pstmt.setLong(27, obj.getN6());
				pstmt.setLong(28, obj.getN7());
				pstmt.setLong(29, obj.getN8());
				pstmt.setLong(30, obj.getN9());
				pstmt.setLong(31, obj.getN10());
				pstmt.setLong(32, obj.getN11());
				pstmt.setLong(33, obj.getN12());
				pstmt.setLong(34, obj.getN13());
				pstmt.setString(35, obj.getCreatedTime());
				pstmt.setString(36, obj.getModifiedTime());
				pstmt.setString(37, obj.getModifiedUserId());
				pstmt.setString(38, obj.getModifiedUserName());
				if (pstmt.executeUpdate() > 0) {
					res.setState(true);
					res.setMsgDesc("Updated Successfully!");
				} else {
					res.setMsgDesc("No Such Article to Update!");
				}
			}
		} catch (SQLException e) {

		}
		return res;
	}
	/* read */
	public static ArticleData readNew(long syskey, Connection conn) throws SQLException {
		ArticleData ret = new ArticleData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				"where RecordStatus<>4 AND  RecordStatus = 1 AND syskey=" + syskey, "", conn);
		if (dbrs.size() > 0) {
				dbrs.get(0).setValue("t1", dbrs.get(0).getString("t1"));
				dbrs.get(0).setValue("t2", dbrs.get(0).getString("t2"));				
				dbrs.get(0).setValue("t6", dbrs.get(0).getString("t6"));
				dbrs.get(0).setValue("t7", dbrs.get(0).getString("t7"));
			ret = getDBRecord(dbrs.get(0));
		}

		return ret;
	}
	//atn
	public ArticleDataSet searchComments(String searchVal, String usersk, Connection conn) throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		ArrayList<ArticleData> ret = new ArrayList<ArticleData>();
		ArrayList<RegisterData> reglist = new ArrayList<RegisterData>();
		String whereclause = " WHERE RecordStatus = 1 AND  n1 = " + searchVal;
		String sql = "";
		try {
			sql = "select * from fmr003 " + whereclause;

			PreparedStatement ps1 = conn.prepareStatement(sql);
			ResultSet rs1 = ps1.executeQuery();
			while (rs1.next()) {
				ArticleData obj = new ArticleData();
				obj.setSyskey(rs1.getLong("syskey"));
				obj.setAutokey(rs1.getLong("autokey"));
				obj.setCreatedDate(rs1.getString("createddate"));
				obj.setCreatedTime(rs1.getString("createdtime"));
				obj.setModifiedDate(rs1.getString("modifieddate"));
				obj.setModifiedTime(rs1.getString("modifiedtime"));
				obj.setRecordStatus(rs1.getInt("recordStatus"));
				obj.setN1(rs1.getLong("n1"));
				obj.setN2(rs1.getLong("n2"));
				obj.setN5(rs1.getLong("n5"));
				obj.setT1(rs1.getString("t1"));
				obj.setT2(rs1.getString("t2"));
				obj.setN3(rs1.getLong("n3")); // reply_count
				if (rs1.getLong("n3") > 1) {
					ArticleData retReply = new ArticleData();
					ArrayList<ArticleData> replyList = new ArrayList<ArticleData>();
					ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
							"where RecordStatus<>4 AND n1=" + rs1.getLong("syskey"), "", conn);
					if (dbrs.size() > 0)
						retReply = getDBRecord(dbrs.get(0));

					retReply.setPerson(RegisterDao.readByIDReply(retReply.getN5(), conn));
					replyList.add(retReply);
					ArticleData[] replyarr = new ArticleData[replyList.size()];
					replyarr = replyList.toArray(replyarr);
					obj.setReplyComment(replyarr);
				}
				/*if (rs1.getLong("n5") == 11384) {
					obj.setUserName("Administrator");
				} else {*/
					RegisterData uData = RegisterDao.readData(rs1.getLong("n5"), conn);
					obj.setUserName(uData.getUsername());
					obj.setT10(uData.getT16());
				//}
				reglist = RegisterDao.readCommentLikePerson(rs1.getLong("syskey"), conn);
				RegisterData[] regarr = new RegisterData[reglist.size()];
				regarr = reglist.toArray(regarr);
				obj.setPerson(regarr);

				new ArticleDao();
				ret = ArticleDao.searchLikeOrNotsQuestionSaveCommen(searchVal, usersk, rs1.getLong("syskey"),
						conn);
				for (int j = 0; j < ret.size(); j++) {
					obj.setN6(rs1.getLong("syskey"));
				}
				datalist.add(obj);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}
	
}
