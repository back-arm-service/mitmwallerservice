package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.ButtonData;
import com.nirvasoft.cms.shared.ButtonDataSet;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;

public class ButtonDataDao {

	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("ButtonName");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 1));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 1));
		ret.getFields().add(new DBField("n5", (byte) 1));
		return ret;
	}

	public static Resultb2b delete(long syskey, String isParent, Connection conn) throws SQLException {

		Resultb2b res = new Resultb2b();
		String sql = "UPDATE ButtonName SET RecordStatus=4 WHERE syskey=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
		}
		return res;
	}

	public static ButtonDataSet getButtonList(Connection conn) throws SQLException {

		ButtonDataSet dataset = new ButtonDataSet();

		ArrayList<ButtonData> ret = new ArrayList<ButtonData>();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "", " ORDER BY syskey", conn);
		for (int i = 0; i < dbrs.size(); i++) {
			ret.add(getDBRecord(dbrs.get(i)));
		}
		ButtonData[] res = new ButtonData[ret.size()];
		res = ret.toArray(res);
		dataset.setData(res);
		if (res.length > 0) {
			dataset.setState(true);
		} else {
			dataset.setState(false);
		}
		return dataset;
	}

	public static ButtonData getButtonListByKey(Connection conn, String key) throws SQLException {

		ButtonData ret = new ButtonData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "WHERE RecordStatus<>4 AND syskey=" + key, "", conn);
		if (dbrs.size() > 0) {
			ret = getDBRecord(dbrs.get(0));
		}
		return ret;
	}

	public static ButtonData getDBRecord(DBRecord adbr) {
		ButtonData ret = new ButtonData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("createddate"));
		ret.setModifiedDate(adbr.getString("modifieddate"));
		ret.setUserId(adbr.getString("userid"));
		ret.setUserName(adbr.getString("username"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getLong("n5"));
		return ret;
	}

	public static Resultb2b insert(ButtonData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();

		if (isCodeExist(obj, conn)) {
			String sql = DBMgr.insertString(define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			stmt.executeUpdate();

			res.setMsgDesc("Saved Successfully!");
			res.setState(true);
			return res;
		} else {
			res.setMsgDesc("Code already exist!");
			res.setState(false);
			return res;
		}

	}

	public static boolean isCodeExist(ButtonData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				" where RecordStatus<>4 AND syskey <> " + obj.getSyskey() + " AND T1='" + obj.getT1() + "'", "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static ButtonData read(long syskey, Connection conn) throws SQLException {
		ButtonData ret = new ButtonData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "WHERE RecordStatus<>4 AND syskey=" + syskey, "", conn);
		if (dbrs.size() > 0) {
			ret = getDBRecord(dbrs.get(0));
		}
		return ret;
	}

	public static DBRecord setDBRecord(ButtonData data) {
		DBRecord ret = define();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("createddate", data.getCreatedDate());
		ret.setValue("modifieddate", data.getModifiedDate());
		ret.setValue("userid", data.getUserId());
		ret.setValue("username", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t5", data.getT5());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		return ret;
	}

	public static Resultb2b update(ButtonData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		if (!isCodeExist(obj, conn)) {
			String sql = DBMgr.updateString(" WHERE RecordStatus<>4 AND Syskey=" + obj.getSyskey(), define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecord(obj);
			DBMgr.setValues(stmt, dbr);
			stmt.executeUpdate();
			res.setState(true);
			res.setMsgDesc("Updated Successfully!");
			return res;
		} else {
			res.setMsgDesc("Code already exist!");
			res.setState(false);
			return res;
		}
	}

}