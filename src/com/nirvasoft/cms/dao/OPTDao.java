package com.nirvasoft.cms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.DivisionComboData;
import com.nirvasoft.cms.shared.DivisionComboDataSet;
import com.nirvasoft.cms.shared.RegisterData;
import com.nirvasoft.cms.shared.UserData;
import com.nirvasoft.cms.shared.UserRole;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;

public class OPTDao {
	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("UVM005");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("CreatedDate", (byte) 5));
		ret.getFields().add(new DBField("ModifiedDate", (byte) 5));
		ret.getFields().add(new DBField("UserId", (byte) 5));
		ret.getFields().add(new DBField("UserName", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 1));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 1));
		ret.getFields().add(new DBField("n6", (byte) 1));
		ret.getFields().add(new DBField("n7", (byte) 1));
		ret.getFields().add(new DBField("n8", (byte) 2));

		return ret;
	}

	public static DBRecord define(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 1));
		ret.getFields().add(new DBField("n4", (byte) 1));
		ret.getFields().add(new DBField("n5", (byte) 2));

		return ret;
	}

	public static Resultb2b delete(String mobile, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		long syskey = searchByID(mobile, conn);
		if (syskey != 0) {
			res = PersonDao.deletePerson(syskey, conn);// UVM0012

			if (res.isState()) {

				res = deleteUser(syskey, conn); // Update UVM005 & JUN002
				if (res.isState()) {

					String sql = "UPDATE Register SET RecordStatus=4 WHERE t1='" + mobile + "' ";
					PreparedStatement stmt = conn.prepareStatement(sql);

					int rs = stmt.executeUpdate();
					if (rs > 0) {

						res.setState(true);
						res.setMsgDesc("Deleted Successfully!");
					}
				}
			}

		} else {
			res.setState(false);
			res.setMsgDesc("Already Deleted Phone Number!");
		}
		return res;
	}

	// TDA
	public static Resultb2b deletefordeactivate(String mobile, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		long syskey = searchByID(mobile, conn);
		if (syskey != 0) {
			String sql = "UPDATE Register SET RecordStatus=4 WHERE t1='" + mobile + "' AND RecordStatus = 1";
			PreparedStatement stmt = conn.prepareStatement(sql);

			int rs = stmt.executeUpdate();
			if (rs > 0) {

				res.setState(true);
				res.setMsgDesc("Deleted Successfully!");
			}

		} else {
			res.setState(false);
			res.setMsgDesc("Already Deleted Phone Number!");
		}
		return res;
	}

	public static Resultb2b deleteOPT(String mobile, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();

		String sql = "Delete from OPTData WHERE t1='" + mobile + "' ";
		PreparedStatement stmt = conn.prepareStatement(sql);

		int rs = stmt.executeUpdate();
		if (rs > 0) {

			res.setState(true);
			res.setMsgDesc("Deleted Successfully!");
		}

		return res;
	}

	public static Resultb2b deleteUser(Long syskey, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = "UPDATE JUN002 SET RecordStatus=4 WHERE n1=? ";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {

			sql = "UPDATE UVM005 SET RecordStatus=4 WHERE Syskey=? ";
			stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				res.setMsgDesc("Deleted Successfully!");

			}
		}

		return res;
	}

	public static Resultb2b deleteUserByMail(String email, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		long syskey = searchByMail(email, conn);
		System.out.println(syskey + " oo5 syskey");
		if (syskey != 0) {
			res = PersonDao.deletePerson(syskey, conn);// UVM0012
			if (res.isState()) {
				res = deleteUser(syskey, conn); // Update UVM005 & JUN002
				if (res.isState()) {
					String sql = "UPDATE Register SET RecordStatus=4 WHERE t13='" + email + "' ";
					PreparedStatement stmt = conn.prepareStatement(sql);
					int rs = stmt.executeUpdate();
					if (rs > 0) {
						res.setState(true);
						res.setMsgDesc("Deleted Successfully!");
					}
				}
			}

		} else {
			res.setState(false);
			res.setMsgDesc("Already Deleted Phone Number!");
		}
		return res;
	}

	// TDA
	public static Resultb2b deleteUserfordeactivate(Long syskey, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = "UPDATE JUN002 SET RecordStatus=4 WHERE n1=? ";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Successfully!");

		}

		return res;
	}

	public static boolean ExistJun4(long n1, Connection conn) {
		boolean b = false;
		String sqlString = "";
		sqlString = "select * from JUN002 where RecordStatus = 4  AND n1='" + n1 + "'";
		try {
			Statement stmts = conn.createStatement();
			ResultSet rs = stmts.executeQuery(sqlString);
			if (rs.next()) {
				b = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return b;
	}

	public static long ExistStatus4(String t1, Connection conn) {
		long b = 0;
		String sqlString = "";
		sqlString = "select syskey from UVM005 where RecordStatus=4 AND t1='" + t1 + "'";
		try {
			Statement stmts = conn.createStatement();
			ResultSet rs = stmts.executeQuery(sqlString);
			if (rs.next()) {
				b = rs.getLong("syskey");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return b;
	}

	public static DivisionComboDataSet getCropCombolist(Connection conn) throws SQLException {

		DivisionComboDataSet dataset = new DivisionComboDataSet();
		ArrayList<DivisionComboData> datalist = new ArrayList<DivisionComboData>();

		String sql = "select code,DespEng from cropref ORDER BY Code";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			DivisionComboData combo = new DivisionComboData();
			combo.setCaption(res.getString("DespEng"));
			combo.setValue(res.getString("code"));
			combo.setFlag(false);
			datalist.add(combo);
			// System.out.println(combo.getCaption() + " **" +
			// combo.getValue());
		}

		DivisionComboData[] dataarray = new DivisionComboData[datalist.size()];
		dataarray = datalist.toArray(dataarray);

		dataset.setData(dataarray);
		return dataset;
	}

	public static DivisionComboDataSet getMonsoonCropCombo(Connection conn) throws SQLException {

		DivisionComboDataSet dataset = new DivisionComboDataSet();
		ArrayList<DivisionComboData> datalist = new ArrayList<DivisionComboData>();

		String sql = "select t1,t2 from FMR012 where t3=0 and t2 NOT Like 'Others' ORDER BY n1";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			DivisionComboData combo = new DivisionComboData();
			combo.setCaption(res.getString("t2"));
			combo.setValue(res.getString("t1"));
			combo.setFlag(false);
			datalist.add(combo);
			// System.out.println(combo.getCaption() + " **" +
			// combo.getValue());
		}

		DivisionComboData[] dataarray = new DivisionComboData[datalist.size()];
		dataarray = datalist.toArray(dataarray);

		dataset.setData(dataarray);
		return dataset;
	}

	public static DivisionComboDataSet getMonsoonCropCombolist(Connection conn) throws SQLException {

		DivisionComboDataSet dataset = new DivisionComboDataSet();
		ArrayList<DivisionComboData> datalist = new ArrayList<DivisionComboData>();

		String sql = "select t1,t2 from FMR012 where t3=0 ORDER BY t1";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			DivisionComboData combo = new DivisionComboData();
			combo.setCaption(res.getString("t2"));
			combo.setValue(res.getString("t1"));
			combo.setFlag(false);
			datalist.add(combo);
			// System.out.println(combo.getCaption() + " **" +
			// combo.getValue());
		}

		DivisionComboData[] dataarray = new DivisionComboData[datalist.size()];
		dataarray = datalist.toArray(dataarray);

		dataset.setData(dataarray);
		return dataset;
	}

	public static long getSyskey(String t1, Connection conn) {
		long b = 0;
		String sqlString = "";
		sqlString = "select syskey from UVM005 where RecordStatus=4 AND t1='" + t1 + "'";
		try {
			Statement stmts = conn.createStatement();
			ResultSet rs = stmts.executeQuery(sqlString);
			if (rs.next()) {
				b = rs.getLong("syskey");
				System.out.println(b + "syskey ");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return b;
	}

	public static DivisionComboDataSet getWinterCropCombolist(Connection conn) throws SQLException {

		DivisionComboDataSet dataset = new DivisionComboDataSet();
		ArrayList<DivisionComboData> datalist = new ArrayList<DivisionComboData>();

		String sql = "select t1,t2 from FMR012 where t3=1 and t2 NOT Like 'Others' ORDER BY n1";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			DivisionComboData combo = new DivisionComboData();
			combo.setCaption(res.getString("t2"));
			combo.setValue(res.getString("t1"));
			combo.setFlag(false);
			datalist.add(combo);
			// System.out.println(combo.getCaption() + " **" +
			// combo.getValue());
		}

		DivisionComboData[] dataarray = new DivisionComboData[datalist.size()];
		dataarray = datalist.toArray(dataarray);

		dataset.setData(dataarray);
		return dataset;
	}

	public static String insert(String t1, String t2, String t3, Connection conn) {
		String b = "0";
		try {
			if (!isIDexist(t1, conn)) {
				String sql = " INSERT INTO OPTData VALUES(?,?,?)";
				PreparedStatement stat = conn.prepareStatement(sql);
				stat.setString(1, t1);
				stat.setString(2, t2);
				stat.setString(3, t3);
				int count = stat.executeUpdate();
				if (count > 0) {
					b = "1";
				}
			} else {
				b = "exist";
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return b;
	}

	public static String insertData(UserData obj, Connection conn) {
		String b = "0";
		Resultb2b res = new Resultb2b();
		try {
			String t1 = obj.getT1();
			if (!isIDexist(t1, conn)) {
				String sql = DBMgr.insertString(define(), conn);
				PreparedStatement stmt = conn.prepareStatement(sql);
				DBRecord dbr = setDBRecords(obj);
				DBMgr.setValues(stmt, dbr);
				int count = stmt.executeUpdate();
				if (count > 0) {
					UserRole jun = new UserRole();
					for (long l : obj.getRolesyskey()) {
						if (l != 0) {
							jun.setRecordStatus(obj.getRecordStatus());
							jun.setSyncBatch(obj.getSyncBatch());
							jun.setSyncStatus(obj.getSyncStatus());
							jun.setUsersyskey(obj.getSyskey());
							jun.setN1(obj.getSyskey());
							jun.setN2(l);
							res = insertUserRole(jun, conn);
						}
					}
					if (res.isState()) {

						res.setState(true);
						b = "1";

					}
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return b;
	}

	// TDA
	public static String insertjunction(UserRole obj, Connection conn) throws SQLException {
		String res = "";
		String sql = DBMgr.insertString(define("JUN002"), conn);
		PreparedStatement stmt = conn.prepareStatement(sql);
		DBRecord dbr = setDBRecord(obj);
		DBMgr.setValues(stmt, dbr);

		try {
			stmt.executeUpdate();
			res = "1";
		} catch (Exception e) {
			e.printStackTrace();
		}

		return res;
	}

	public static Resultb2b insertUserRole(UserRole obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = DBMgr.insertString(define("JUN002"), conn);
		PreparedStatement stmt = conn.prepareStatement(sql);
		DBRecord dbr = setDBRecord(obj);
		DBMgr.setValues(stmt, dbr);
		int rs = stmt.executeUpdate();

		if (rs > 0) {

			res.setState(true);
		}

		return res;
	}

	public static boolean isID(String t1, Connection conn) {
		boolean b = false;
		String sqlString = "";
		sqlString = "select * from UVM005 where RecordStatus = 1 AND t1='" + t1 + "'";
		try {
			Statement stmts = conn.createStatement();
			ResultSet rs = stmts.executeQuery(sqlString);
			if (rs.next()) {
				b = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return b;
	}

	public static boolean isIDexist(String t1, Connection conn) {
		boolean b = false;
		String sqlString = "";

		sqlString = "select * from UVM005 where RecordStatus<>4 AND t1='" + t1 + "'";

		try {
			Statement stmts = conn.createStatement();
			ResultSet rs = stmts.executeQuery(sqlString);
			if (rs.next()) {
				b = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return b;
	}

	public static boolean isIDexistJun(long n1, Connection conn) {
		boolean b = false;
		String sqlString = "";
		sqlString = "select * from JUN002 where RecordStatus = 0  AND n1='" + n1 + "'";
		try {
			Statement stmts = conn.createStatement();
			ResultSet rs = stmts.executeQuery(sqlString);
			if (rs.next()) {
				b = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return b;
	}

	public static long isPhexist(String t1, Connection conn) {
		long b = 0;
		String sqlString = "";
		sqlString = "select syskey from UVM005 where RecordStatus=0 AND t1='" + t1 + "'";
		try {
			Statement stmts = conn.createStatement();
			ResultSet rs = stmts.executeQuery(sqlString);
			if (rs.next()) {
				b = rs.getLong("syskey");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return b;
	}

	public static boolean isPhExist(String t1, Connection conn) {
		boolean b = false;
		String sqlString = "";
		sqlString = "select syskey from UVM005 where RecordStatus=4 AND t1='" + t1 + "'";
		try {
			Statement stmts = conn.createStatement();
			ResultSet rs = stmts.executeQuery(sqlString);
			if (rs.next()) {
				b = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return b;
	}

	public static long isPhexistIn12(String t1, Connection conn) {
		long b = 0;
		String sqlString = "";
		sqlString = "select syskey from UVM012 where RecordStatus=0 AND t1='" + t1 + "'";
		try {
			Statement stmts = conn.createStatement();
			ResultSet rs = stmts.executeQuery(sqlString);
			if (rs.next()) {
				b = rs.getLong("syskey");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return b;
	}

	public static long readData(String t1, String t2, Connection conn) {
		long b = 0;
		String sqlString = "";
		sqlString = "select * from UVM005 where RecordStatus=1 AND t1='" + t1 + "' and t2='" + t2 + "'";
		try {
			Statement stmts = conn.createStatement();
			ResultSet rs = stmts.executeQuery(sqlString);
			if (rs.next()) {
				b = rs.getLong("syskey");
				System.out.println(b + "sss");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return b;
	}

	public static long searchByID(String mobile, Connection conn) throws SQLException {
		long syskey;
		String sql = "Select usersyskey from register WHERE RecordStatus<>4 AND t1=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, mobile);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			syskey = rs.getLong("usersyskey");

		} else {
			syskey = 0;
		}
		return syskey;
	}

	public static long searchByMail(String email, Connection conn)

			throws SQLException {
		long syskey;
		String sql = "Select syskey from UVM005 WHERE RecordStatus=1 AND t3=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, email);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			syskey = rs.getLong("syskey");

		} else {
			syskey = 0;
		}
		return syskey;
	}

	public static long searchByPostID(String postkey, String key, Connection conn) throws SQLException {
		long syskey;
		String sql = "Select n1 from FMR008 WHERE RecordStatus=1 AND n1='" + postkey + "' AND n2='" + key + "'";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			syskey = rs.getLong("n1");
		} else {
			syskey = 0;
		}
		return syskey;
	}

	public static long searchkeyBypostkey(long n1, Connection conn)

			throws SQLException {
		long syskey;
		String sql = "Select n5 from FMR003 WHERE RecordStatus<>4 AND n1=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, n1);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			syskey = rs.getLong("n5");

		} else {
			syskey = 0;
		}
		return syskey;
	}

	public static String searchMobile(long key, MrBean user)

			throws SQLException {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		String mobile = "";
		String sql = "Select t1 from UVM005 WHERE RecordStatus=1 AND syskey=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, key);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			mobile = rs.getString("t1");

		} else {
			mobile = "";
		}
		return mobile;
	}

	public static long searchSyskey(String mobile, MrBean user)

			throws SQLException {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		long syskey;
		String sql = "Select syskey from UVM005 WHERE RecordStatus<>4 AND t1=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, mobile);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			syskey = rs.getLong("syskey");

		} else {
			syskey = 0;
		}
		return syskey;
	}

	public static DBRecord setDBRecord(UserRole data) {
		DBRecord ret = define("JUN002");
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("usersysKey", data.getUsersyskey());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		return ret;
	}

	public static DBRecord setDBRecords(UserData data) {
		DBRecord ret = define();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("CreatedDate", data.getCreatedDate());
		ret.setValue("ModifiedDate", data.getModifiedDate());
		ret.setValue("UserId", data.getUserId());
		ret.setValue("UserName", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());

		ret.setValue("usersysKey", data.getUsersyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t3", data.getT3());
		ret.setValue("t5", data.getT5());
		ret.setValue("t6", data.getT6());
		ret.setValue("t7", data.getT7());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		ret.setValue("n6", data.getN6());
		ret.setValue("n7", data.getN7());
		ret.setValue("n8", data.getN8());
		return ret;
	}

	public static Resultb2b update(UserData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		if (isIDexist(obj.getT1(), conn)) {
			String sql = DBMgr.updateString(" WHERE RecordStatus<>4 AND RecordStatus = 1 AND Syskey=" + obj.getSyskey(),
					define(), conn);
			PreparedStatement stmt = conn.prepareStatement(sql);
			DBRecord dbr = setDBRecords(obj);
			DBMgr.setValues(stmt, dbr);
			int count = stmt.executeUpdate();
			if (count > 0) {
				res.setState(true);
				res.setMsgDesc("Updated Successfully!");
			}
		} else {
			res.setMsgDesc("No Such Article to Update!");
		}
		return res;
	}

	public static String Update(String t1, String t2, String t4, Connection conn) throws SQLException {
		String b = "0";
		String sqlString = "";
		sqlString = "UPDATE UVM005 SET t2='" + t2 + "',t4='" + t4 + "' where RecordStatus<>4 AND t1 = '" + t1 + "' ";
		try {
			PreparedStatement stmt = conn.prepareStatement(sqlString);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				sqlString = "UPDATE Register SET t2='" + t2 + "' where RecordStatus<>4 AND t1 = '" + t1 + "' ";
				PreparedStatement st = conn.prepareStatement(sqlString);
				int res = st.executeUpdate();
				if (res > 0) {
					b = "1";
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return b;
	}

	public static String updateData(UserData obj, Connection conn) {
		String b = "0";
		try {
			String t1 = obj.getT1();
			if (isPhexist(t1, conn) > 0) {// update UVM005
				String sql = DBMgr.updateString(" WHERE RecordStatus = 0 AND Syskey=" + obj.getSyskey(), define(),
						conn);
				PreparedStatement stmt = conn.prepareStatement(sql);
				DBRecord dbr = setDBRecords(obj);
				DBMgr.setValues(stmt, dbr);
				int count = stmt.executeUpdate();
				if (count > 0) {
					b = "1";
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return b;
	}

	public static String UpdateData(String t1, String t2, Connection conn) throws SQLException {
		String b = "0";
		String sqlString = "";
		System.out.println(t2 + " password");
		sqlString = "UPDATE UVM005 SET RecordStatus = 2 , t2='" + t2 + "' where RecordStatus = 1 AND t1 = '" + t1
				+ "' ";
		try {
			PreparedStatement stmt = conn.prepareStatement(sqlString);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				System.out.println(rs + " count");
				sqlString = "UPDATE Register SET RecordStatus = 2, t2='" + t2 + "' where RecordStatus = 1 AND t1 = '"
						+ t1 + "' ";
				PreparedStatement st = conn.prepareStatement(sqlString);
				int res = st.executeUpdate();
				if (res > 0) {
					System.out.println(rs + " count");
					b = "1";
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return b;
	}

	public static String UpdateData1(String t1, String t2, Connection conn) throws SQLException {
		String b = "0";
		String sqlString = "";
		System.out.println(t2 + " password");
		sqlString = "UPDATE UVM005 SET RecordStatus = 1  where RecordStatus = 2 AND t1 = '" + t1 + "' ";
		try {
			PreparedStatement stmt = conn.prepareStatement(sqlString);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				sqlString = "UPDATE Register SET RecordStatus = 1  where RecordStatus = 2 AND t1 = '" + t1 + "' ";
				PreparedStatement st = conn.prepareStatement(sqlString);
				int res = st.executeUpdate();
				if (res > 0) {
					b = "1";
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return b;
	}

	public static String updateRegData(UserData obj, Connection conn) {
		String b = "0";
		Resultb2b res = new Resultb2b();
		try {
			String t1 = obj.getT1();
			if (ExistStatus4(t1, conn) > 0) {// update UVM005
				String sql = DBMgr.updateString(" WHERE RecordStatus =4  AND Syskey=" + obj.getSyskey(), define(),
						conn);
				PreparedStatement stmt = conn.prepareStatement(sql);
				DBRecord dbr = setDBRecords(obj);
				DBMgr.setValues(stmt, dbr);
				int count = stmt.executeUpdate();
				if (count > 0) {

					b = "1";

					System.out.println(" update finish");
					if (ExistJun4(obj.getSyskey(), conn)) {
						System.out.println(obj.getSyskey() + "  " + obj.getRolesyskey().length);
						UserRole jun = new UserRole();

						for (long l : obj.getRolesyskey()) {

							if (l != 0) {

								jun.setRecordStatus(obj.getRecordStatus());
								jun.setSyncBatch(obj.getSyncBatch());
								jun.setSyncStatus(obj.getSyncStatus());
								jun.setUsersyskey(obj.getSyskey());
								jun.setN1(obj.getSyskey());
								jun.setN2(l);

								res = updateUserRoles(jun, conn);
								System.out.println(res.isState() + " Jun update");

							}

						}
						if (res.isState()) {

							res.setState(true);
							res.setMsgDesc("Saved Successfully!");
							b = "1";

						} else
							res.setMsgDesc("Can't Save!");

					}
				}
			} else {
				System.out.println(" t1 not exit");

			}

			// conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return b;
	}

	public static RegisterData updateRegisterOTP(RegisterData p, Connection conn) throws SQLException {
		RegisterData res = new RegisterData();

		String sql = "update Register set t60  ='" + p.getT60() + "' where  t1 ='" + p.getT1() + "'";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.executeUpdate();
		if (stmt.executeUpdate() > 0) {
			// res.setT1(p.getT1());
			// res.setSyskey(p.getT1());
			String psql = "select syskey from Register where t1 =?";
			PreparedStatement pstmt = conn.prepareStatement(psql);
			pstmt.setString(1, p.getT1());
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				res.setSyskey(rs.getLong("syskey"));
			} else {
				res.setSyskey(0);
			}

			// res.setSyskey(Long.parseLong(p.getT1()));
		} else {
			res.setSyskey(0);
		}
		return res;
	}

	public static Resultb2b updateUserRole(UserRole obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = DBMgr.updateString(" WHERE RecordStatus = 0 AND n1=" + obj.getN1(), define(), conn);
		PreparedStatement stmt = conn.prepareStatement(sql);
		DBRecord dbr = setDBRecord(obj);
		DBMgr.setValues(stmt, dbr);
		int rs = stmt.executeUpdate();

		if (rs > 0) {

			res.setState(true);
		}

		return res;
	}

	public static Resultb2b updateUserRoles(UserRole obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sqlString = "UPDATE JUN002 SET RecordStatus = 0  where RecordStatus = 4 AND n1 = '" + obj.getN1() + "' ";
		PreparedStatement st = conn.prepareStatement(sqlString);
		int result = st.executeUpdate();
		if (result > 0) {
			String sql = "UPDATE Register SET RecordStatus = 0  where RecordStatus = 4 AND UserSysKey = '" + obj.getN1()
					+ "' ";
			PreparedStatement stm = conn.prepareStatement(sql);
			int result1 = stm.executeUpdate();
			if (result1 > 0) {
				res.setState(true);
				System.out.println(" update Jun");
			}
		}

		return res;
	}

}
