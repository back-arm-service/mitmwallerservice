package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class FAQData {
	private long srno;
	private long syskey;
	private long autokey;
	private String createdDate;
	private String modifiedDate;
	private int status;
	private String userID;
	private String sessionID;
	private String questionEng;
	private String answerEng;
	private String questionUni;
	private String answerUni;
	private String t1;
	private String t2;
	private String t3;
	private String t4;
	private String t5;
	private long n1;
	private long n2;
	private long n3;
	private long n4;
	private long n5;
	private String msgCode;
	private String msgDesc;

	protected void clearProperties() {
		this.syskey = 0L;
		this.autokey = 0L;
		this.createdDate = "";
		this.modifiedDate = "";
		this.userID = "";
		this.status = 0;
		this.t1 = "";
		this.t2 = "";
		this.t3 = "";
		this.t4 = "";
		this.t5 = "";
		this.n1 = 0;
		this.n2 = 0;
		this.n3 = 0;
		this.n4 = 0;
		this.n5 = 0;
		this.srno = 0;
	}

	public String getAnswerEng() {
		return answerEng;
	}

	public String getAnswerUni() {
		return answerUni;
	}

	public long getAutokey() {
		return autokey;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public long getN1() {
		return n1;
	}

	public long getN2() {
		return n2;
	}

	public long getN3() {
		return n3;
	}

	public long getN4() {
		return n4;
	}

	public long getN5() {
		return n5;
	}

	public String getQuestionEng() {
		return questionEng;
	}

	public String getQuestionUni() {
		return questionUni;
	}

	public String getSessionID() {
		return sessionID;
	}

	public long getSrno() {
		return srno;
	}

	public int getStatus() {
		return status;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getT1() {
		return t1;
	}

	public String getT2() {
		return t2;
	}

	public String getT3() {
		return t3;
	}

	public String getT4() {
		return t4;
	}

	public String getT5() {
		return t5;
	}

	public String getUserID() {
		return userID;
	}

	public void setAnswerEng(String answerEng) {
		this.answerEng = answerEng;
	}

	public void setAnswerUni(String answerUni) {
		this.answerUni = answerUni;
	}

	public void setAutokey(long autokey) {
		this.autokey = autokey;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public void setN1(long n1) {
		this.n1 = n1;
	}

	public void setN2(long n2) {
		this.n2 = n2;
	}

	public void setN3(long n3) {
		this.n3 = n3;
	}

	public void setN4(long n4) {
		this.n4 = n4;
	}

	public void setN5(long n5) {
		this.n5 = n5;
	}

	public void setQuestionEng(String questionEng) {
		this.questionEng = questionEng;
	}

	public void setQuestionUni(String questionUni) {
		this.questionUni = questionUni;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setSrno(long srno) {
		this.srno = srno;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public void setT4(String t4) {
		this.t4 = t4;
	}

	public void setT5(String t5) {
		this.t5 = t5;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

}
