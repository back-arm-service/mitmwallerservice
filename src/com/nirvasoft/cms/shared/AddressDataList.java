package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AddressDataList {
	private AddressData[] data;
	private String searchText;
	private int totalCount;
	private int currentPage;
	private int pageSize;

	public AddressDataList() {
		clearProperty();
	}

	private void clearProperty() {
		searchText = "";
		totalCount = 0;
		currentPage = 0;
		pageSize = 0;
		data = null;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public AddressData[] getData() {
		return data;
	}

	public int getPageSize() {
		return pageSize;
	}

	public String getSearchText() {
		return searchText;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setData(AddressData[] data) {
		this.data = data;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

}
