package com.nirvasoft.cms.shared;

public class RegisterDataSet {

	private RegisterData[] data;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private boolean state;
	private String msg;
	public RegisterDataSet() {
		data = new RegisterData[0];
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public RegisterData[] getData() {
		return data;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public boolean isState() {
		return state;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setData(RegisterData[] data) {
		this.data = data;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
