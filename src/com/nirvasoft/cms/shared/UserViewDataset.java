package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserViewDataset {
	private UserViewData[] data;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private boolean state;

	public UserViewDataset() {
		super();
		data = new UserViewData[0];
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public UserViewData[] getData() {
		return data;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public boolean isState() {
		return state;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setData(UserViewData[] data) {
		this.data = data;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

}
