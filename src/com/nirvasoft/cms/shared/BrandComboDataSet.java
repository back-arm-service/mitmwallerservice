package com.nirvasoft.cms.shared;

public class BrandComboDataSet {

	private BrandComboData[] data;

	public BrandComboDataSet() {
		data = new BrandComboData[0];
	}

	public BrandComboData[] getData() {
		return data;
	}

	public void setData(BrandComboData[] data) {
		this.data = data;
	}

}
