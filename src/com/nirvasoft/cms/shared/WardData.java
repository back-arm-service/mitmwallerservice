package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class WardData {
	private long syskey;
	private String code;
	private String wardcode;
	private long IsLocal;
	private int recordstatus;
	private String division;
	private String distinc;
	private String township;
	private String DespMyan;
	private String DespEng;
	private String errorcode;
	private String errormessage;

	public WardData() {
		clearProperty();
	}

	private void clearProperty() {
		this.syskey = 0;
		this.division = "";
		this.distinc = "";
		this.township = "";
		this.DespEng = "";
		this.DespMyan = "";
		this.errorcode = "";
		this.errormessage = "";
		this.wardcode = "";
		this.IsLocal = 0;
		this.recordstatus = 0;
		this.code = "";

	}

	public String getCode() {
		return code;
	}

	public String getDespEng() {
		return DespEng;
	}

	public String getDespMyan() {
		return DespMyan;
	}

	public String getDistinc() {
		return distinc;
	}

	public String getDivision() {
		return division;
	}

	public String getErrorcode() {
		return errorcode;
	}

	public String getErrormessage() {
		return errormessage;
	}

	public long getIsLocal() {
		return IsLocal;
	}

	public int getRecordstatus() {
		return recordstatus;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getTownship() {
		return township;
	}

	public String getWardcode() {
		return wardcode;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDespEng(String despEng) {
		DespEng = despEng;
	}

	public void setDespMyan(String despMyan) {
		DespMyan = despMyan;
	}

	public void setDistinc(String distinc) {
		this.distinc = distinc;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}

	public void setErrormessage(String errormessage) {
		this.errormessage = errormessage;
	}

	public void setIsLocal(long isLocal) {
		IsLocal = isLocal;
	}

	public void setRecordstatus(int recordstatus) {
		this.recordstatus = recordstatus;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setTownship(String township) {
		this.township = township;
	}

	public void setWardcode(String wardcode) {
		this.wardcode = wardcode;
	}

}
