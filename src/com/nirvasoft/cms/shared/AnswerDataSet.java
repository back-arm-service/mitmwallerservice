package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AnswerDataSet {
	private AnswerData[] data;

	public AnswerDataSet() {
		data = new AnswerData[0];
	}

	public AnswerData[] getData() {
		return data;
	}

	public void setData(AnswerData[] data) {
		this.data = data;
	}

}
