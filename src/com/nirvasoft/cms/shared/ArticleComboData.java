package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ArticleComboData {

	private String id;
	private String name;
	private boolean flag;

	public ArticleComboData() {
		super();
		clearProperties();
	}

	public void clearProperties() {
		this.id = "";
		this.name = "";
		this.flag = false;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

}
