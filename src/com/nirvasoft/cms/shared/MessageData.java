package com.nirvasoft.cms.shared;

public class MessageData {

	private long syskey;
	private long autokey;
	private String createddate;
	private String createdtime;
	private String modifieddate;
	private String modifiedtime;
	private String userid;
	private String username;
	private int recordStatus;
	private int syncStatus;
	private long syncBatch;
	private long usersyskey;
	private String T1;
	private String T2;
	private String T3;
	private String T4;
	private String T5;
	private String T6;
	private String T7;
	private String T8;
	private String T9;
	private String T10;
	private String T11;
	private String T12;
	private String T13;
	private String T14;
	private String T15;

	private long n1;
	private long n2;
	private long n3;
	private long n4;
	private long n5;
	private long n6;
	private long n7;
	private long n8;
	private long n9;
	private long n10;
	private long n11;
	private long n12;
	private long n13;
	private long n14;
	private long n15;

	private DivisionComboData[] addDiv;
	private DivisionComboData[] addDist;
	private DivisionComboData[] addTown;
	private DivisionComboData[] users;

	public MessageData() {
		super();
		clearProperties();
	}

	private void clearProperties() {
		this.syskey = 0;
		this.autokey = 0;
		this.createddate = "";
		this.createdtime = "";
		this.modifieddate = "";
		this.modifiedtime = "";
		this.userid = "";
		this.username = "";
		this.recordStatus = 0;
		this.syncStatus = 0;
		this.syncBatch = 0;
		this.usersyskey = 0;
		this.T1 = "";
		this.T2 = "";
		this.T3 = "";
		this.T4 = "";
		this.T5 = "";
		this.T6 = "";
		this.T7 = "";
		this.T8 = "";
		this.T9 = "";
		this.T10 = "";
		this.T11 = "";
		this.T12 = "";
		this.T13 = "";
		this.T14 = "";
		this.T15 = "";

		this.n1 = 0;
		this.n2 = 0;
		this.n3 = 0;
		this.n4 = 0;
		this.n5 = 0;
		this.n6 = 0;
		this.n7 = 0;
		this.n8 = 0;
		this.n9 = 0;
		this.n10 = 0;
		this.n11 = 0;
		this.n12 = 0;
		this.n13 = 0;
		this.n14 = 0;
		this.n15 = 0;

	}

	public DivisionComboData[] getAddDist() {
		return addDist;
	}

	public DivisionComboData[] getAddDiv() {
		return addDiv;
	}

	public DivisionComboData[] getAddTown() {
		return addTown;
	}

	public long getAutokey() {
		return autokey;
	}

	public String getCreateddate() {
		return createddate;
	}

	public String getCreatedtime() {
		return createdtime;
	}

	public String getModifieddate() {
		return modifieddate;
	}

	public String getModifiedtime() {
		return modifiedtime;
	}

	public long getN1() {
		return n1;
	}

	public long getN10() {
		return n10;
	}

	public long getN11() {
		return n11;
	}

	public long getN12() {
		return n12;
	}

	public long getN13() {
		return n13;
	}

	public long getN14() {
		return n14;
	}

	public long getN15() {
		return n15;
	}

	public long getN2() {
		return n2;
	}

	public long getN3() {
		return n3;
	}

	public long getN4() {
		return n4;
	}

	public long getN5() {
		return n5;
	}

	public long getN6() {
		return n6;
	}

	public long getN7() {
		return n7;
	}

	public long getN8() {
		return n8;
	}

	public long getN9() {
		return n9;
	}

	public int getRecordStatus() {
		return recordStatus;
	}

	public long getSyncBatch() {
		return syncBatch;
	}

	public int getSyncStatus() {
		return syncStatus;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getT1() {
		return T1;
	}

	public String getT10() {
		return T10;
	}

	public String getT11() {
		return T11;
	}

	public String getT12() {
		return T12;
	}

	public String getT13() {
		return T13;
	}

	public String getT14() {
		return T14;
	}

	public String getT15() {
		return T15;
	}

	public String getT2() {
		return T2;
	}

	public String getT3() {
		return T3;
	}

	public String getT4() {
		return T4;
	}

	public String getT5() {
		return T5;
	}

	public String getT6() {
		return T6;
	}

	public String getT7() {
		return T7;
	}

	public String getT8() {
		return T8;
	}

	public String getT9() {
		return T9;
	}

	public String getUserid() {
		return userid;
	}

	public String getUsername() {
		return username;
	}

	public DivisionComboData[] getUsers() {
		return users;
	}

	public long getUsersyskey() {
		return usersyskey;
	}

	public void setAddDist(DivisionComboData[] addDist) {
		this.addDist = addDist;
	}

	public void setAddDiv(DivisionComboData[] addDiv) {
		this.addDiv = addDiv;
	}

	public void setAddTown(DivisionComboData[] addTown) {
		this.addTown = addTown;
	}

	public void setAutokey(long autokey) {
		this.autokey = autokey;
	}

	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}

	public void setCreatedtime(String createdtime) {
		this.createdtime = createdtime;
	}

	public void setModifieddate(String modifieddate) {
		this.modifieddate = modifieddate;
	}

	public void setModifiedtime(String modifiedtime) {
		this.modifiedtime = modifiedtime;
	}

	public void setN1(long n1) {
		this.n1 = n1;
	}

	public void setN10(long n10) {
		this.n10 = n10;
	}

	public void setN11(long n11) {
		this.n11 = n11;
	}

	public void setN12(long n12) {
		this.n12 = n12;
	}

	public void setN13(long n13) {
		this.n13 = n13;
	}

	public void setN14(long n14) {
		this.n14 = n14;
	}

	public void setN15(long n15) {
		this.n15 = n15;
	}

	public void setN2(long n2) {
		this.n2 = n2;
	}

	public void setN3(long n3) {
		this.n3 = n3;
	}

	public void setN4(long n4) {
		this.n4 = n4;
	}

	public void setN5(long n5) {
		this.n5 = n5;
	}

	public void setN6(long n6) {
		this.n6 = n6;
	}

	public void setN7(long n7) {
		this.n7 = n7;
	}

	public void setN8(long n8) {
		this.n8 = n8;
	}

	public void setN9(long n9) {
		this.n9 = n9;
	}

	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}

	public void setSyncBatch(long syncBatch) {
		this.syncBatch = syncBatch;
	}

	public void setSyncStatus(int syncStatus) {
		this.syncStatus = syncStatus;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setT1(String t1) {
		T1 = t1;
	}

	public void setT10(String t10) {
		T10 = t10;
	}

	public void setT11(String t11) {
		T11 = t11;
	}

	public void setT12(String t12) {
		T12 = t12;
	}

	public void setT13(String t13) {
		T13 = t13;
	}

	public void setT14(String t14) {
		T14 = t14;
	}

	public void setT15(String t15) {
		T15 = t15;
	}

	public void setT2(String t2) {
		T2 = t2;
	}

	public void setT3(String t3) {
		T3 = t3;
	}

	public void setT4(String t4) {
		T4 = t4;
	}

	public void setT5(String t5) {
		T5 = t5;
	}

	public void setT6(String t6) {
		T6 = t6;
	}

	public void setT7(String t7) {
		T7 = t7;
	}

	public void setT8(String t8) {
		T8 = t8;
	}

	public void setT9(String t9) {
		T9 = t9;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setUsers(DivisionComboData[] users) {
		this.users = users;
	}

	public void setUsersyskey(long usersyskey) {
		this.usersyskey = usersyskey;
	}

}
