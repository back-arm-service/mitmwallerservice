package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Ref1 {
	private String value;
	private String caption;
	private String t2;

	public Ref1() {
		clearProperty();
	}

	void clearProperty() {
		value = "";
		caption = "";
		t2 = "";
	}

	public String getCaption() {
		return caption;
	}

	public String getT2() {
		return t2;
	}

	public String getValue() {
		return value;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
