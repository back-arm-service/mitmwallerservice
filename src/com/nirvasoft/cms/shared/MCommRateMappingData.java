package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MCommRateMappingData {
	// "processingCode": "", "syskey": 0, "recordStatus": 0, "merchantID": "",
	// "kindOfComIssuer": 0,
	// "chkAdvance": false, "t1": "", "commRef1": "", "commRef2": "",
	// "commRef3": "", "commRef4": "", "commRef5": "",
	// "code": "", "desc": "", "userID": "", "sessionID": "", "n1": 0, "n2": 0
	private String processingCode;
	private long syskey;
	private int recordStatus;
	private String merchantID;
	private int kindOfComIssuer;
	private boolean chkAdvance;
	private String t1;
	private String commRef1;
	private String commRef2;
	private String commRef3;
	private String commRef4;
	private String commRef5;
	private String commRef6;
	private String code;
	private String desc;
	private String userID;
	private String sessionID;
	private int n1;
	private int n2;
	private String createdDate;
	private String modifiedDate;

	private String merchantName;
	private String productCode;
	private String gL;
	private CommRateHeaderData commObj1;
	private CommRateHeaderData commObj2;
	private CommRateHeaderData commObj3;
	private CommRateHeaderData commObj4;
	private CommRateHeaderData commObj5;

	public MCommRateMappingData() {
		clearProperty();
	}

	private void clearProperty() {
		syskey = 0L;
		createdDate = "";
		modifiedDate = "";
		merchantID = "";
		kindOfComIssuer = 0;
		productCode = "";
		gL = "";
		commRef1 = "";
		commRef2 = "";
		commRef3 = "";
		commRef4 = "";
		commRef5 = "";
		commRef6 = "";
		t1 = "";
		n1 = 0;
		n2 = 0;
		recordStatus = 0;
		merchantName = "";
		code = "";
		desc = "";
		userID = "";
		sessionID = "";

	}

	public String getCode() {
		return code;
	}

	public CommRateHeaderData getCommObj1() {
		return commObj1;
	}

	public CommRateHeaderData getCommObj2() {
		return commObj2;
	}

	public CommRateHeaderData getCommObj3() {
		return commObj3;
	}

	public CommRateHeaderData getCommObj4() {
		return commObj4;
	}

	public CommRateHeaderData getCommObj5() {
		return commObj5;
	}

	public String getCommRef1() {
		return commRef1;
	}

	public String getCommRef2() {
		return commRef2;
	}

	public String getCommRef3() {
		return commRef3;
	}

	public String getCommRef4() {
		return commRef4;
	}

	public String getCommRef5() {
		return commRef5;
	}

	public String getCommRef6() {
		return commRef6;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public String getDesc() {
		return desc;
	}

	public String getgL() {
		return gL;
	}

	public int getKindOfComIssuer() {
		return kindOfComIssuer;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public int getN1() {
		return n1;
	}

	public int getN2() {
		return n2;
	}

	public String getProcessingCode() {
		return processingCode;
	}

	public String getProductCode() {
		return productCode;
	}

	public int getRecordStatus() {
		return recordStatus;
	}

	public String getSessionID() {
		return sessionID;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getT1() {
		return t1;
	}

	public String getUserID() {
		return userID;
	}

	public boolean isChkAdvance() {
		return chkAdvance;
	}

	public void setChkAdvance(boolean chkAdvance) {
		this.chkAdvance = chkAdvance;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCommObj1(CommRateHeaderData commObj1) {
		this.commObj1 = commObj1;
	}

	public void setCommObj2(CommRateHeaderData commObj2) {
		this.commObj2 = commObj2;
	}

	public void setCommObj3(CommRateHeaderData commObj3) {
		this.commObj3 = commObj3;
	}

	public void setCommObj4(CommRateHeaderData commObj4) {
		this.commObj4 = commObj4;
	}

	public void setCommObj5(CommRateHeaderData commObj5) {
		this.commObj5 = commObj5;
	}

	public void setCommRef1(String commRef1) {
		this.commRef1 = commRef1;
	}

	public void setCommRef2(String commRef2) {
		this.commRef2 = commRef2;
	}

	public void setCommRef3(String commRef3) {
		this.commRef3 = commRef3;
	}

	public void setCommRef4(String commRef4) {
		this.commRef4 = commRef4;
	}

	public void setCommRef5(String commRef5) {
		this.commRef5 = commRef5;
	}

	public void setCommRef6(String commRef6) {
		this.commRef6 = commRef6;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setgL(String gL) {
		this.gL = gL;
	}

	public void setKindOfComIssuer(int kindOfComIssuer) {
		this.kindOfComIssuer = kindOfComIssuer;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public void setN2(int n2) {
		this.n2 = n2;
	}

	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

}
