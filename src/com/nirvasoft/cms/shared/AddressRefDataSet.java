package com.nirvasoft.cms.shared;

public class AddressRefDataSet {

	private AddressRefData[] data;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private boolean state;

	public AddressRefDataSet() {
		data = new AddressRefData[0];

	}

	public int getCurrentPage() {
		return currentPage;
	}

	public AddressRefData[] getData() {
		return data;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public boolean isState() {
		return state;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setData(AddressRefData[] data) {
		this.data = data;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

}
