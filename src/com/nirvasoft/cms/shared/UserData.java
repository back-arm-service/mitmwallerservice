package com.nirvasoft.cms.shared;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserData {
	private long syskey;
	private long autokey;
	private String createdDate;
	private String modifiedDate;
	private String userId;
	private String userName;
	private String password;
	private String organizationID;
	private int recordStatus;
	private int syncStatus;
	private long syncBatch;
	private long usersyskey;
	private String upload[];
	private String t1;
	private String t2;
	private String t3;
	private String t4;
	private String t5;
	private String t6;
	private String t7;
	private String t8;
	private String t9;
	private String t10;
	private String phoneno;
	private String address;
	private String photo;
	private String gender;
	private String dob;
	private String typ;
	private long n1;
	private int n2;
	private long n3;
	private long n4;
	private int n5;
	private int n6;
	private int n7;
	private long n8;
	private long[] rolesyskey;
	private UserRoleViewData[] userrolelist;
	private PersonData person;
	private String name;
	private ArrayList<PhotoUploadData> uploadlist;
	private String resizelist[];
	private ArrayList<UploadData> uploadDatalist;
	private UploadData[] uploadedPhoto;
	private String t16;

	public UserData() {
		clearProperties();
	}

	protected void clearProperties() {
		this.syskey = 0;
		this.autokey = 0;
		this.createdDate = "";
		this.modifiedDate = "";
		this.userId = "";
		this.userName = "";
		this.recordStatus = 0;
		this.syncStatus = 0;
		this.syncBatch = 0;
		this.usersyskey = 0;
		this.t1 = "";
		this.t2 = "";
		this.t3 = "";
		this.t4 = "";
		this.t5 = "";
		this.t6 = "";
		this.t7 = "";
		this.t8 = "";
		this.t9 = "";
		this.t10 = "";
		this.n1 = 0;
		this.n2 = 0;
		this.n3 = 0;
		this.n4 = 0;
		this.n5 = 0;
		this.n6 = 0;
		this.n7 = 0;
		this.n8 = 0;
		this.upload = new String[0];
		this.person = new PersonData();
		this.name = "";
		this.phoneno = "";
		this.address = "";
		this.photo = "";
		this.dob = "";
		this.gender = "";
		this.typ = "";
		this.uploadlist = new ArrayList<>();
		this.uploadDatalist = new ArrayList<>();
		this.resizelist = new String[0];
		this.uploadedPhoto = new UploadData[0];
		this.t16 = "";
	}

	public String getT16() {
		return t16;
	}

	public void setT16(String t16) {
		this.t16 = t16;
	}

	public String getAddress() {
		return address;
	}

	public long getAutokey() {
		return autokey;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public String getDob() {
		return dob;
	}

	public String getGender() {
		return gender;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public long getN1() {
		return n1;
	}

	public int getN2() {
		return n2;
	}

	public long getN3() {
		return n3;
	}

	public long getN4() {
		return n4;
	}

	public int getN5() {
		return n5;
	}

	public int getN6() {
		return n6;
	}

	public int getN7() {
		return n7;
	}

	public long getN8() {
		return n8;
	}

	public String getName() {
		return name;
	}

	public String getOrganizationID() {
		return organizationID;
	}

	public String getPassword() {
		return password;
	}

	public PersonData getPerson() {
		return person;
	}

	public String getPhoneno() {
		return phoneno;
	}

	public String getPhoto() {
		return photo;
	}

	public int getRecordStatus() {
		return recordStatus;
	}

	public String[] getResizelist() {
		return resizelist;
	}

	public long[] getRolesyskey() {
		return rolesyskey;
	}

	public long getSyncBatch() {
		return syncBatch;
	}

	public int getSyncStatus() {
		return syncStatus;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getT1() {
		return t1;
	}

	public String getT10() {
		return t10;
	}

	public String getT2() {
		return t2;
	}

	public String getT3() {
		return t3;
	}

	public String getT4() {
		return t4;
	}

	public String getT5() {
		return t5;
	}

	public String getT6() {
		return t6;
	}

	public String getT7() {
		return t7;
	}

	public String getT8() {
		return t8;
	}

	public String getT9() {
		return t9;
	}

	public String getTyp() {
		return typ;
	}

	public String[] getUpload() {
		return upload;
	}

	public ArrayList<UploadData> getUploadDatalist() {
		return uploadDatalist;
	}

	public UploadData[] getUploadedPhoto() {
		return uploadedPhoto;
	}

	public ArrayList<PhotoUploadData> getUploadlist() {
		return uploadlist;
	}

	public String getUserId() {
		return userId;
	}

	public String getUserName() {
		return userName;
	}

	public UserRoleViewData[] getUserrolelist() {
		return userrolelist;
	}

	public long getUsersyskey() {
		return usersyskey;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setAutokey(long autokey) {
		this.autokey = autokey;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public void setN1(long n1) {
		this.n1 = n1;
	}

	public void setN2(int n2) {
		this.n2 = n2;
	}

	public void setN3(long n3) {
		this.n3 = n3;
	}

	public void setN4(long n4) {
		this.n4 = n4;
	}

	public void setN5(int n5) {
		this.n5 = n5;
	}

	public void setN6(int n6) {
		this.n6 = n6;
	}

	public void setN7(int n7) {
		this.n7 = n7;
	}

	public void setN8(long n8) {
		this.n8 = n8;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setOrganizationID(String organizationID) {
		this.organizationID = organizationID;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPerson(PersonData person) {
		this.person = person;
	}

	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}

	public void setResizelist(String[] resizelist) {
		this.resizelist = resizelist;
	}

	public void setRolesyskey(long[] result) {
		this.rolesyskey = result;
	}

	public void setSyncBatch(long syncBatch) {
		this.syncBatch = syncBatch;
	}

	public void setSyncStatus(int syncStatus) {
		this.syncStatus = syncStatus;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT10(String t10) {
		this.t10 = t10;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public void setT4(String t4) {
		this.t4 = t4;
	}

	public void setT5(String t5) {
		this.t5 = t5;
	}

	public void setT6(String t6) {
		this.t6 = t6;
	}

	public void setT7(String t7) {
		this.t7 = t7;
	}

	public void setT8(String t8) {
		this.t8 = t8;
	}

	public void setT9(String t9) {
		this.t9 = t9;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public void setUpload(String[] upload) {
		this.upload = upload;
	}

	public void setUploadDatalist(ArrayList<UploadData> uploadDatalist) {
		this.uploadDatalist = uploadDatalist;
	}

	public void setUploadedPhoto(UploadData[] uploadedPhoto) {
		this.uploadedPhoto = uploadedPhoto;
	}

	public void setUploadlist(ArrayList<PhotoUploadData> uploadlist) {
		this.uploadlist = uploadlist;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setUserrolelist(UserRoleViewData[] userrolelist) {
		this.userrolelist = userrolelist;
	}

	public void setUsersyskey(long usersyskey) {
		this.usersyskey = usersyskey;
	}
}
