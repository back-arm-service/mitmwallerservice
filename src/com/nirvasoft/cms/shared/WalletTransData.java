package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class WalletTransData {
	private long autokey;
	private String createDate;
	private String modifiedDate;
	private String userID;
	private String fromAccount;
	private String toAccount;
	private String amount;
	private String bankCharges;
	private String transID;
	private String transDate;
	private String transType;
	private String T1;
	private String T2;
	private String T3;
	private String T4;
	private String T5;
	private String T6;
	private String T7;
	private String T8;	
	private double n1;
	private double n2;
	private int n3;
	private int i1;
	private String fromDate;
	private String toDate;
	private String currencyCode;
	private String mbankingkey;
	private String settlementDate;
	private String merchantID;
	private String merchantName;
	private boolean state;
	private String drAccNumber;
	private String crAccNumber;
	private String code;
	private String CommissionCharges;
	

	public String getCommissionCharges() {
		return CommissionCharges;
	}

	public void setCommissionCharges(String commissionCharges) {
		CommissionCharges = commissionCharges;
	}

	public WalletTransData() {
		clearProperties();
	}

	public void clearProperties() {
		this.createDate = "";
		this.modifiedDate = "";
		this.userID = "";
		this.fromAccount = "";
		this.toAccount = "";
		this.amount = "";
		this.bankCharges = "";
		this.transID = "";
		this.transDate = "";
		this.T1 = "";
		this.T2 = "";
		this.T3 = "";
		this.T4 = "";
		this.T5 = "";
		this.T6 = "";
		this.T7 = "";
		this.T8 = "";
		this.n1 = 0;
		this.n2 = 0;
		this.n3 = 0;
		this.i1 =0;
		this.fromDate = "";
		this.toDate = "";
		this.currencyCode = "";
		this.transType = "";
		this.settlementDate= "";
		this.merchantID= "";
		this.merchantName= "";
		this.state = false;
		this.crAccNumber="";
		this.crAccNumber="";
		this.code = "";
	}
		

	public String getDrAccNumber() {
		return drAccNumber;
	}

	public void setDrAccNumber(String drAccNumber) {
		this.drAccNumber = drAccNumber;
	}

	public String getCrAccNumber() {
		return crAccNumber;
	}

	public void setCrAccNumber(String crAccNumber) {
		this.crAccNumber = crAccNumber;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public int getI1() {
		return i1;
	}

	public void setI1(int i1) {
		this.i1 = i1;
	}

	public double getN1() {
		return n1;
	}

	public void setN1(double n1) {
		this.n1 = n1;
	}

	public double getN2() {
		return n2;
	}

	public void setN2(double n2) {
		this.n2 = n2;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}	

	public void setN2(int n2) {
		this.n2 = n2;
	}

	public int getN3() {
		return n3;
	}

	public void setN3(int n3) {
		this.n3 = n3;
	}

	public String getT4() {
		return T4;
	}

	public void setT4(String t4) {
		T4 = t4;
	}

	public String getT5() {
		return T5;
	}

	public void setT5(String t5) {
		T5 = t5;
	}

	public String getT6() {
		return T6;
	}

	public void setT6(String t6) {
		T6 = t6;
	}

	public String getT7() {
		return T7;
	}

	public void setT7(String t7) {
		T7 = t7;
	}

	public String getT8() {
		return T8;
	}

	public void setT8(String t8) {
		T8 = t8;
	}

	public String getSettlementDate() {
		return settlementDate;
	}

	public void setSettlementDate(String settlementDate) {
		this.settlementDate = settlementDate;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getAmount() {
		return amount;
	}

	public long getAutokey() {
		return autokey;
	}

	public String getBankCharges() {
		return bankCharges;
	}

	public String getCreateDate() {
		return createDate;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public String getFromAccount() {
		return fromAccount;
	}

	public String getFromDate() {
		return fromDate;
	}

	public String getMbankingkey() {
		return mbankingkey;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}
	

	public String getT1() {
		return T1;
	}

	public String getT2() {
		return T2;
	}

	public String getT3() {
		return T3;
	}

	public String getToAccount() {
		return toAccount;
	}

	public String getToDate() {
		return toDate;
	}

	public String getTransDate() {
		return transDate;
	}

	public String getTransID() {
		return transID;
	}

	public String getTransType() {
		return transType;
	}

	public String getUserID() {
		return userID;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public void setAutokey(long autokey) {
		this.autokey = autokey;
	}

	public void setBankCharges(String bankCharges) {
		this.bankCharges = bankCharges;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public void setFromAccount(String fromAccount) {
		this.fromAccount = fromAccount;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public void setMbankingkey(String mbankingkey) {
		this.mbankingkey = mbankingkey;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}


	public void setT1(String t1) {
		T1 = t1;
	}

	public void setT2(String t2) {
		T2 = t2;
	}

	public void setT3(String t3) {
		T3 = t3;
	}

	public void setToAccount(String toAccount) {
		this.toAccount = toAccount;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}

	public void setTransID(String transID) {
		this.transID = transID;
	}

	public void setTransType(String transType) {
		this.transType = transType;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

}
