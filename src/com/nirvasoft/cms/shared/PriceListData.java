package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PriceListData {
	private String groupName;
	private PriceData[] data;

	public PriceListData() {
		clearProperties();
	}

	private void clearProperties() {
		data = new PriceData[0];
	}

	public PriceData[] getData() {
		return data;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setData(PriceData[] data) {
		this.data = data;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
}
