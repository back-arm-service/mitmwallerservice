package com.nirvasoft.cms.shared;

public class BrandComboData {
	private long value;
	private String engCaption;
	private String myanCaption;
	private boolean flag;
	private String code;

	public BrandComboData() {
		super();
		clearProperties();
	}

	public void clearProperties() {
		this.value = 0;
		this.engCaption = "";
		this.myanCaption = "";
		this.code = "";
		this.flag = false;
	}

	public String getCode() {
		return code;
	}

	public String getEngCaption() {
		return engCaption;
	}

	public String getMyanCaption() {
		return myanCaption;
	}

	public long getValue() {
		return value;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setEngCaption(String engCaption) {
		this.engCaption = engCaption;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public void setMyanCaption(String myanCaption) {
		this.myanCaption = myanCaption;
	}

	public void setValue(long value) {
		this.value = value;
	}

}
