package com.nirvasoft.cms.shared;

public class ContentDataSet {

	private ContentData[] data;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private boolean state;

	public ContentDataSet() {
		data = new ContentData[0];

	}

	public int getCurrentPage() {
		return currentPage;
	}

	public ContentData[] getData() {
		return data;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public boolean isState() {
		return state;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setData(ContentData[] data) {
		this.data = data;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

}
