package com.nirvasoft.cms.shared;

public class SetupDataSet {
	private SetupData[] arr;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private boolean state;
	private boolean sessionState;
	private MenuOrderData[] menu_arr;

	public SetupData[] getArr() {
		return arr;
	}

	public boolean isSessionState() {
		return sessionState;
	}

	public void setSessionState(boolean sessionState) {
		this.sessionState = sessionState;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public MenuOrderData[] getMenu_arr() {
		return menu_arr;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public boolean isState() {
		return state;
	}

	public void setArr(SetupData[] arr) {
		this.arr = arr;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setMenu_arr(MenuOrderData[] menu_arr) {
		this.menu_arr = menu_arr;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

}
