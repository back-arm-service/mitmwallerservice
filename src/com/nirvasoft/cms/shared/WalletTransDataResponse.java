package com.nirvasoft.cms.shared;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class WalletTransDataResponse {
	public TempData[] getMerchantdata() {
		return merchantdata;
	}

	public void setMerchantdata(TempData[] merchantdata) {
		this.merchantdata = merchantdata;
	}

	private WalletTransData[] wldata;
	private TempData[] merchantdata;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private String msgCode;
	private String msgDesc;
	private String totalAmount;
	private String totalBankCharges;
	private boolean state;
	
	private ArrayList<String> stringResult = new ArrayList<String>();

	public WalletTransDataResponse() {
		clearProperty();
	}

	private void clearProperty() {
		wldata = null;
		totalCount = 0;
		currentPage = 0;
		pageSize = 0;
		totalAmount = "";
		totalBankCharges = "";
		state = false;
	}
	
	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public ArrayList<String> getStringResult() {
		return stringResult;
	}

	public void setStringResult(ArrayList<String> stringResult) {
		this.stringResult = stringResult;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public int getPageSize() {
		return pageSize;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public String getTotalBankCharges() {
		return totalBankCharges;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public WalletTransData[] getWldata() {
		return wldata;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public void setTotalBankCharges(String totalBankCharges) {
		this.totalBankCharges = totalBankCharges;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public void setWldata(WalletTransData[] wldata) {
		this.wldata = wldata;
	}

}
