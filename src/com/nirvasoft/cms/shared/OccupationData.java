package com.nirvasoft.cms.shared;

public class OccupationData {
	private long syskey;
	private long autokey;
	private String createddate;
	private String modifieddate;
	private String userid;
	private String username;
	private int recordStatus;
	private int syncStatus;
	private long syncBatch;
	private long usersyskey;
	private String T1;
	private String T2;
	private String T3;
	private String T4;
	private String T5;
	private String T6;
	private String T7;
	private String T8;
	private String T9;
	private String T10;
	private long n1;
	private long n2;
	private long n3;
	private long n4;
	private long n5;
	private long n6;
	private long n7;
	private long n8;
	private long n9;
	private long n10;

	public long getAutokey() {
		return autokey;
	}

	public String getCreateddate() {
		return createddate;
	}

	public String getModifieddate() {
		return modifieddate;
	}

	public long getN1() {
		return n1;
	}

	public long getN10() {
		return n10;
	}

	public long getN2() {
		return n2;
	}

	public long getN3() {
		return n3;
	}

	public long getN4() {
		return n4;
	}

	public long getN5() {
		return n5;
	}

	public long getN6() {
		return n6;
	}

	public long getN7() {
		return n7;
	}

	public long getN8() {
		return n8;
	}

	public long getN9() {
		return n9;
	}

	public int getRecordStatus() {
		return recordStatus;
	}

	public long getSyncBatch() {
		return syncBatch;
	}

	public int getSyncStatus() {
		return syncStatus;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getT1() {
		return T1;
	}

	public String getT10() {
		return T10;
	}

	public String getT2() {
		return T2;
	}

	public String getT3() {
		return T3;
	}

	public String getT4() {
		return T4;
	}

	public String getT5() {
		return T5;
	}

	public String getT6() {
		return T6;
	}

	public String getT7() {
		return T7;
	}

	public String getT8() {
		return T8;
	}

	public String getT9() {
		return T9;
	}

	public String getUserid() {
		return userid;
	}

	public String getUsername() {
		return username;
	}

	public long getUsersyskey() {
		return usersyskey;
	}

	public void setAutokey(long autokey) {
		this.autokey = autokey;
	}

	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}

	public void setModifieddate(String modifieddate) {
		this.modifieddate = modifieddate;
	}

	public void setN1(long n1) {
		this.n1 = n1;
	}

	public void setN10(long n10) {
		this.n10 = n10;
	}

	public void setN2(long n2) {
		this.n2 = n2;
	}

	public void setN3(long n3) {
		this.n3 = n3;
	}

	public void setN4(long n4) {
		this.n4 = n4;
	}

	public void setN5(long n5) {
		this.n5 = n5;
	}

	public void setN6(long n6) {
		this.n6 = n6;
	}

	public void setN7(long n7) {
		this.n7 = n7;
	}

	public void setN8(long n8) {
		this.n8 = n8;
	}

	public void setN9(long n9) {
		this.n9 = n9;
	}

	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}

	public void setSyncBatch(long syncBatch) {
		this.syncBatch = syncBatch;
	}

	public void setSyncStatus(int syncStatus) {
		this.syncStatus = syncStatus;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setT1(String t1) {
		T1 = t1;
	}

	public void setT10(String t10) {
		T10 = t10;
	}

	public void setT2(String t2) {
		T2 = t2;
	}

	public void setT3(String t3) {
		T3 = t3;
	}

	public void setT4(String t4) {
		T4 = t4;
	}

	public void setT5(String t5) {
		T5 = t5;
	}

	public void setT6(String t6) {
		T6 = t6;
	}

	public void setT7(String t7) {
		T7 = t7;
	}

	public void setT8(String t8) {
		T8 = t8;
	}

	public void setT9(String t9) {
		T9 = t9;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setUsersyskey(long usersyskey) {
		this.usersyskey = usersyskey;
	}

}
