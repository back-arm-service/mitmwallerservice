package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AnswerData {

	private String userId;
	private String userName;
	private long userSyskey;
	private String answer;

	public AnswerData() {
		clearProperties();
	}

	protected void clearProperties() {
		this.userId = "";
		this.userName = "";
		this.userSyskey = 0L;
		this.answer = "";

	}

	public String getAnswer() {
		return answer;
	}

	public String getUserId() {
		return userId;
	}

	public String getUserName() {
		return userName;
	}

	public long getUserSyskey() {
		return userSyskey;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setUserSyskey(long userSyskey) {
		this.userSyskey = userSyskey;
	}

}
