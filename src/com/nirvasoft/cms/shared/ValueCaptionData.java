package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ValueCaptionData {

	private String value;
	private String caption;

	public ValueCaptionData() {
		super();
		clearProperties();
	}

	public void clearProperties() {
		this.value = "";
		this.caption = "";
	}

	public String getCaption() {
		return caption;
	}

	public String getValue() {
		return value;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
