package com.nirvasoft.cms.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ArticleDataSet {
	private ArticleData[] data;
	private String[] ansData;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private boolean state;
	private boolean sessionState;

	public ArticleDataSet() {
		data = new ArticleData[0];
	}

	public String[] getAnsData() {
		return ansData;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public ArticleData[] getData() {
		return data;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public boolean isState() {
		return state;
	}

	public void setAnsData(String[] ansData) {
		this.ansData = ansData;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setData(ArticleData[] data) {
		this.data = data;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public boolean isSessionState() {
		return sessionState;
	}

	public void setSessionState(boolean sessionState) {
		this.sessionState = sessionState;
	}

	@Override
	public String toString() {
		return "ArticleDataSet [data=" + Arrays.toString(data) + ", ansData=" + Arrays.toString(ansData)
				+ ", totalCount=" + totalCount + ", currentPage=" + currentPage + ", pageSize=" + pageSize + ", state="
				+ state + ", sessionState=" + sessionState + "]";
	}

	

}
