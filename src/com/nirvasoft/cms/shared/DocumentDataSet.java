package com.nirvasoft.cms.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DocumentDataSet {
	private DocumentData[] data;
	private String[] ansData;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private boolean state;

	public DocumentDataSet() {
		data = new DocumentData[0];
	}

	public String[] getAnsData() {
		return ansData;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public DocumentData[] getData() {
		return data;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public boolean isState() {
		return state;
	}

	public void setAnsData(String[] ansData) {
		this.ansData = ansData;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setData(DocumentData[] data) {
		this.data = data;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	@Override
	public String toString() {
		return "DocumentDataSet [data=" + Arrays.toString(data) + ", ansData=" + Arrays.toString(ansData)
				+ ", totalCount=" + totalCount + ", currentPage=" + currentPage + ", pageSize=" + pageSize + ", state="
				+ state + ", getData()=" + Arrays.toString(getData()) + ", getAnsData()="
				+ Arrays.toString(getAnsData()) + ", getTotalCount()=" + getTotalCount() + ", getCurrentPage()="
				+ getCurrentPage() + ", getPageSize()=" + getPageSize() + ", isState()=" + isState() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}

}
