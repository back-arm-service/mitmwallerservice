package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CommRateDetailData {

	private Long syskey;
	private Long hkey;
	private String commRef;
	private int zone;
	private int commType;
	private double fromAmt;
	private double toAmt;
	private double amount;
	private double minAmt;
	private double maxAmt;
	/*
	 * private String fromAmt; private String toAmt; private String amount;
	 * private String minAmt; private String maxAmt;
	 */
	private String gLorAC;
	private String description;
	private int mainType;

	public CommRateDetailData() {

		clearProperty();
	}

	private void clearProperty() {
		syskey = 0L;
		hkey = 0L;
		commRef = "";
		zone = 0;
		commType = 0;
		fromAmt = 0.00;
		toAmt = 0.00;
		amount = 0.00;
		minAmt = 0.00;
		maxAmt = 0.00;
		mainType = 0;
		gLorAC = "";
		description = "";
	}

	public double getAmount() {
		return amount;
	}

	public String getCommRef() {
		return commRef;
	}

	public int getCommType() {
		return commType;
	}

	public String getDescription() {
		return description;
	}

	public double getFromAmt() {
		return fromAmt;
	}

	public String getgLorAC() {
		return gLorAC;
	}

	public Long getHkey() {
		return hkey;
	}

	public int getMainType() {
		return mainType;
	}

	public double getMaxAmt() {
		return maxAmt;
	}

	public double getMinAmt() {
		return minAmt;
	}

	public Long getSyskey() {
		return syskey;
	}

	public double getToAmt() {
		return toAmt;
	}

	public int getZone() {
		return zone;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void setCommRef(String commRef) {
		this.commRef = commRef;
	}

	/*
	 * public String getFromAmt() { return fromAmt; }
	 * 
	 * public void setFromAmt(String fromAmt) { this.fromAmt = fromAmt; }
	 * 
	 * public String getToAmt() { return toAmt; }
	 * 
	 * public void setToAmt(String toAmt) { this.toAmt = toAmt; }
	 * 
	 * public String getAmount() { return amount; }
	 * 
	 * public void setAmount(String amount) { this.amount = amount; }
	 * 
	 * public String getMinAmt() { return minAmt; }
	 * 
	 * public void setMinAmt(String minAmt) { this.minAmt = minAmt; }
	 * 
	 * public String getMaxAmt() { return maxAmt; }
	 * 
	 * public void setMaxAmt(String maxAmt) { this.maxAmt = maxAmt; }
	 */

	public void setCommType(int commType) {
		this.commType = commType;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setFromAmt(double fromAmt) {
		this.fromAmt = fromAmt;
	}

	public void setgLorAC(String gLorAC) {
		this.gLorAC = gLorAC;
	}

	public void setHkey(Long hkey) {
		this.hkey = hkey;
	}

	public void setMainType(int mainType) {
		this.mainType = mainType;
	}

	public void setMaxAmt(double maxAmt) {
		this.maxAmt = maxAmt;
	}

	public void setMinAmt(double minAmt) {
		this.minAmt = minAmt;
	}

	public void setSyskey(Long syskey) {
		this.syskey = syskey;
	}

	public void setToAmt(double toAmt) {
		this.toAmt = toAmt;
	}

	public void setZone(int zone) {
		this.zone = zone;
	}
}
