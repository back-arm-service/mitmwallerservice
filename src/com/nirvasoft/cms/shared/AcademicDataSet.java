package com.nirvasoft.cms.shared;

public class AcademicDataSet {

	private AcademicData[] arr;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private boolean state;

	public AcademicDataSet() {
		arr = new AcademicData[0];

	}

	public AcademicData[] getArr() {
		return arr;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public boolean isState() {
		return state;
	}

	public void setArr(AcademicData[] arr) {
		this.arr = arr;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

}
