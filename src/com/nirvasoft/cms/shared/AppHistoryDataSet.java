package com.nirvasoft.cms.shared;

public class AppHistoryDataSet {

	private AppHistoryData[] data;
	private AppHistoryData[] count;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private boolean state;

	public AppHistoryDataSet() {
		data = new AppHistoryData[0];
		count = new AppHistoryData[0];

	}

	public AppHistoryData[] getCount() {
		return count;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public AppHistoryData[] getData() {
		return data;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public boolean isState() {
		return state;
	}

	public void setCount(AppHistoryData[] count) {
		this.count = count;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setData(AppHistoryData[] data) {
		this.data = data;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

}
