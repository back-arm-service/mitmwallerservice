package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AddressData {
	private String wardcode;
	private int srno;
	private long syskey;
	private String division;
	private String distinct;
	private String distinc;
	private String township;
	private String code;
	private String despmyan;
	private String despeng;
	private String islocal;
	private String messagecode;
	private String messagedesc;
	private String divisioncode;
	private int recordStatus;

	public AddressData() {
		clearProperty();
	}

	private void clearProperty() {
		messagecode = "";
		messagedesc = "";
		division = "";
		distinct = "";
		distinc = "";
		township = "";
		code = "";
		despmyan = "";
		despeng = "";
		islocal = "";
		srno = 0;
		syskey = 0;
		divisioncode = "";
		wardcode = "";
	}

	public String getCode() {
		return code;
	}

	public String getDespeng() {
		return despeng;
	}

	public String getDespmyan() {
		return despmyan;
	}

	public String getDistinc() {
		return distinc;
	}

	public String getDistinct() {
		return distinct;
	}

	public String getDivision() {
		return division;
	}

	public String getDivisioncode() {
		return divisioncode;
	}

	public String getIslocal() {
		return islocal;
	}

	public String getMessagecode() {
		return messagecode;
	}

	public String getMessagedesc() {
		return messagedesc;
	}

	public int getRecordStatus() {
		return recordStatus;
	}

	public int getSrno() {
		return srno;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getTownship() {
		return township;
	}

	public String getWardcode() {
		return wardcode;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDespeng(String despeng) {
		this.despeng = despeng;
	}

	public void setDespmyan(String despmyan) {
		this.despmyan = despmyan;
	}

	public void setDistinc(String distinc) {
		this.distinc = distinc;
	}

	public void setDistinct(String distinct) {
		this.distinct = distinct;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public void setDivisioncode(String divisioncode) {
		this.divisioncode = divisioncode;
	}

	public void setIslocal(String islocal) {
		this.islocal = islocal;
	}

	public void setMessagecode(String messagecode) {
		this.messagecode = messagecode;
	}

	public void setMessagedesc(String messagedesc) {
		this.messagedesc = messagedesc;
	}

	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}

	public void setSrno(int srno) {
		this.srno = srno;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setTownship(String township) {
		this.township = township;
	}

	public void setWardcode(String wardcode) {
		this.wardcode = wardcode;
	}

}
