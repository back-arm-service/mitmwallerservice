package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AddressRefData {
	private String Code;
	private String DespMyan;
	private String DespEng;
	private long IsLocal;
	private String MinLat;
	private String MinLon;
	private String MaxLat;
	private String MaxLon;
	private String errorcode;
	private String errormessage;
	private String division;
	private String distric;
	private String township;

	public AddressRefData() {
		super();
		clearProperties();
	}

	protected void clearProperties() {
		this.Code = "";
		this.DespMyan = "";
		this.DespEng = "";
		this.IsLocal = 0;
		this.MinLat = "";
		this.MinLon = "";
		this.MaxLat = "";
		this.MaxLon = "";
		this.errorcode = "";
		this.errormessage = "";
		this.division = "";
		this.township = "";
		this.distric = "";

	}

	public String getCode() {
		return Code;
	}

	public String getDespEng() {
		return DespEng;
	}

	public String getDespMyan() {
		return DespMyan;
	}

	public String getDistric() {
		return distric;
	}

	public String getDivision() {
		return division;
	}

	public String getErrorcode() {
		return errorcode;
	}

	public String getErrormessage() {
		return errormessage;
	}

	public long getIsLocal() {
		return IsLocal;
	}

	public String getMaxLat() {
		return MaxLat;
	}

	public String getMaxLon() {
		return MaxLon;
	}

	public String getMinLat() {
		return MinLat;
	}

	public String getMinLon() {
		return MinLon;
	}

	public String getTownship() {
		return township;
	}

	public void setCode(String code) {
		Code = code;
	}

	public void setDespEng(String despEng) {
		DespEng = despEng;
	}

	public void setDespMyan(String despMyan) {
		DespMyan = despMyan;
	}

	public void setDistric(String distric) {
		this.distric = distric;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}

	public void setErrormessage(String errormessage) {
		this.errormessage = errormessage;
	}

	public void setIsLocal(long isLocal) {
		IsLocal = isLocal;
	}

	public void setMaxLat(String maxLat) {
		MaxLat = maxLat;
	}

	public void setMaxLon(String maxLon) {
		MaxLon = maxLon;
	}

	public void setMinLat(String minLat) {
		MinLat = minLat;
	}

	public void setMinLon(String minLon) {
		MinLon = minLon;
	}

	public void setTownship(String township) {
		this.township = township;
	}

}
