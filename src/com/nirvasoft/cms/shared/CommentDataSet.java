package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CommentDataSet {
	private CommentData[] data;

	public CommentDataSet() {
		data = new CommentData[0];
	}

	public CommentData[] getData() {
		return data;
	}

	public void setData(CommentData[] data) {
		this.data = data;
	}

}
