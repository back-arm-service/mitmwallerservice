package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PasswordData {

	private String userid;
	private String oldpass;
	private String newpass;
	private String confirm;

	public PasswordData() {
		super();
		clearProperties();
	}

	public void clearProperties() {
		this.userid = "";
		this.oldpass = "";
		this.newpass = "";
		this.confirm = "";
	}

	public String getConfirm() {
		return confirm;
	}

	public String getNewpass() {
		return newpass;
	}

	public String getOldpass() {
		return oldpass;
	}

	public String getUserid() {
		return userid;
	}

	public void setConfirm(String confirm) {
		this.confirm = confirm;
	}

	public void setNewpass(String newpass) {
		this.newpass = newpass;
	}

	public void setOldpass(String oldpass) {
		this.oldpass = oldpass;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

}
