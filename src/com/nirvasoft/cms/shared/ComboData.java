package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ComboData {

	private long value;
	private String caption;

	public ComboData() {
		super();
		clearProperties();
	}

	public void clearProperties() {
		this.value = 0;
		this.caption = "";
	}

	public String getCaption() {
		return caption;
	}

	public long getValue() {
		return value;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public void setValue(long value) {
		this.value = value;
	}

}
