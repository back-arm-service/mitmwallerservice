package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PagerMobileData {

	private int current;
	private int prev;
	private int last;
	private int next;
	private int start;
	private int end;
	private int size;
	private int totalcount;
	private String t1;
	private String userPhone;
	private String usersk;
	private String regionNumber;
	// private String domain;

	// public String getDomain() {
	// return domain;
	// }
	//
	// public void setDomain(String domain) {
	// this.domain = domain;
	// }

	public PagerMobileData() {
		super();
		clearProperties();
	}

	public void clearProperties() {
		this.current = 1;
		this.prev = 1;
		this.last = 1;
		this.next = 2;
		this.start = 1;
		this.end = 10;
		this.size = 10;
		this.totalcount = 1;
		this.t1 = "";
		this.userPhone = "";
		// this.domain = "";
		this.regionNumber = "";
		this.usersk = "";
	}

	public int getCurrent() {
		return current;
	}

	public int getEnd() {
		return end;
	}

	public int getLast() {
		return last;
	}

	public int getNext() {
		return next;
	}

	public int getPrev() {
		return prev;
	}

	public String getRegionNumber() {
		return regionNumber;
	}

	public int getSize() {
		return size;
	}

	public int getStart() {
		return start;
	}

	public String getT1() {
		return t1;
	}

	public int getTotalcount() {
		return totalcount;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public String getUsersk() {
		return usersk;
	}

	public void setCurrent(int current) {
		this.current = current;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public void setLast(int last) {
		this.last = last;
	}

	public void setNext(int next) {
		this.next = next;
	}

	public void setPrev(int prev) {
		this.prev = prev;
	}

	public void setRegionNumber(String regionNumber) {
		this.regionNumber = regionNumber;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setTotalcount(int totalcount) {
		this.totalcount = totalcount;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public void setUsersk(String usersk) {
		this.usersk = usersk;
	}

}
