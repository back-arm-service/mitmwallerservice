package com.nirvasoft.cms.shared;

public class OccupationDataSet {
	private OccupationData[] arr;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private boolean state;

	public OccupationDataSet() {
		arr = new OccupationData[0];

	}

	public OccupationData[] getArr() {
		return arr;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public boolean isState() {
		return state;
	}

	public void setArr(OccupationData[] arr) {
		this.arr = arr;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

}
