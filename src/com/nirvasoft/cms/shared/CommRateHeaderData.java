package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class CommRateHeaderData {
	private String createdUserID;
	private String sessionID;
	private Long hkey;
	private String msgCode;
	private String msgDesc;
	private String commRef;
	private long syskey;
	private String description;
	private int mainType;
	private String zone;
	private String amount;
	private String fromAmt;
	private String toAmt;
	private String minAmt;
	private String maxAmt;
	private String commType;
	private String createdDate;
	private String modifiedDate;

	private int recordStatus;
	private String state;

	private String searchText;
	private int totalCount;
	private int currentPage;
	private int pageSize;

	private CommRateDetailData[] commdetailArr;

	public CommRateHeaderData() {

		clearProperty();
	}

	private void clearProperty() {
		syskey = 0L;
		createdDate = "";
		modifiedDate = "";
		sessionID = "";
		commRef = "";
		description = "";
		mainType = 0;
		recordStatus = 0;
		commdetailArr = null;
		msgCode = "";
		msgDesc = "";
		state = "false";
		searchText = "";
		totalCount = 0;
		currentPage = 0;
		pageSize = 0;
		hkey = 0L;
	}

	public String getAmount() {
		return amount;
	}

	public CommRateDetailData[] getCommdetailArr() {
		return commdetailArr;
	}

	public String getCommRef() {
		return commRef;
	}

	public String getCommType() {
		return commType;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public String getCreatedUserID() {
		return createdUserID;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public String getDescription() {
		return description;
	}

	public String getFromAmt() {
		return fromAmt;
	}

	public Long getHkey() {
		return hkey;
	}

	public int getMainType() {
		return mainType;
	}

	public String getMaxAmt() {
		return maxAmt;
	}

	public String getMinAmt() {
		return minAmt;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getRecordStatus() {
		return recordStatus;
	}

	public String getSearchText() {
		return searchText;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getState() {
		return state;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getToAmt() {
		return toAmt;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public String getZone() {
		return zone;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public void setCommdetailArr(CommRateDetailData[] commdetailArr) {
		this.commdetailArr = commdetailArr;
	}

	public void setCommRef(String commRef) {
		this.commRef = commRef;
	}

	public void setCommType(String commType) {
		this.commType = commType;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public void setCreatedUserID(String createdUserID) {
		this.createdUserID = createdUserID;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setFromAmt(String fromAmt) {
		this.fromAmt = fromAmt;
	}

	public void setHkey(Long hkey) {
		this.hkey = hkey;
	}

	public void setMainType(int mainType) {
		this.mainType = mainType;
	}

	public void setMaxAmt(String maxAmt) {
		this.maxAmt = maxAmt;
	}

	public void setMinAmt(String minAmt) {
		this.minAmt = minAmt;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setToAmt(String toAmt) {
		this.toAmt = toAmt;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

}
