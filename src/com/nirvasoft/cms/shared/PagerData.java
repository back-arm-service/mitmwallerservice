package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PagerData {

	private int current;
	private int prev;
	private int last;
	private int next;
	private int start;
	private int end;
	private int size;
	private int totalcount;
	private String t1;
	private String t2;
	private int n1;
	private String userid;
	private long changestatus;
	private String phno;
	
	public PagerData() {
		super();
		clearProperties();
	}

	public void clearProperties() {
		this.current = 1;
		this.prev = 1;
		this.last = 1;
		this.next = 2;
		this.start = 1;
		this.end = 10;
		this.size = 10;
		this.totalcount = 1;
		this.t1 = "";
		this.t2 = "";
		this.n1 = 0;
		this.changestatus = 0;
		this.userid = "";
		this.phno = "";
	}

	public String getPhno() {
		return phno;
	}

	public void setPhno(String phno) {
		this.phno = phno;
	}

	public long getChangestatus() {
		return changestatus;
	}

	public int getCurrent() {
		return current;
	}

	public int getEnd() {
		return end;
	}

	public int getLast() {
		return last;
	}

	public int getN1() {
		return n1;
	}

	public int getNext() {
		return next;
	}

	public int getPrev() {
		return prev;
	}

	public int getSize() {
		return size;
	}

	public int getStart() {
		return start;
	}

	public String getT1() {
		return t1;
	}

	public String getT2() {
		return t2;
	}

	public int getTotalcount() {
		return totalcount;
	}

	public String getUserid() {
		return userid;
	}

	public void setChangestatus(long changestatus) {
		this.changestatus = changestatus;
	}

	public void setCurrent(int current) {
		this.current = current;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public void setLast(int last) {
		this.last = last;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public void setNext(int next) {
		this.next = next;
	}

	public void setPrev(int prev) {
		this.prev = prev;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setTotalcount(int totalcount) {
		this.totalcount = totalcount;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

}
