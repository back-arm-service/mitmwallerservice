package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PhotoUploadData {
	private int serial;
	private String name;
	private String desc;
	private String order;
	private String url;

	public PhotoUploadData() {
		super();
		this.clearProperties();
	}

	private void clearProperties() {
		this.serial = 0;
		this.name = "";
		this.desc = "";
		this.order = "";
		this.url = "";
	}

	public String getDesc() {
		return desc;
	}

	public String getName() {
		return name;
	}

	public String getOrder() {
		return order;
	}

	public int getSerial() {
		return serial;
	}

	public String getUrl() {
		return url;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public void setSerial(int serial) {
		this.serial = serial;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "PhotoUploadData [serial=" + serial + ", name=" + name + ", desc=" + desc + ", order=" + order + ", url="
				+ url + "]";
	}

}
