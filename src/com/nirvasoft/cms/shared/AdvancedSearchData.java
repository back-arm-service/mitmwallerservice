package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AdvancedSearchData {

	private PagerData pager;
	private SearchData[] search;

	public AdvancedSearchData() {
		super();
		clearProperties();
	}

	public void clearProperties() {
		pager = new PagerData();
		search = new SearchData[0];
	}

	public PagerData getPager() {
		return pager;
	}

	public SearchData[] getSearch() {
		return search;
	}

	public void setPager(PagerData pager) {
		this.pager = pager;
	}

	public void setSearch(SearchData[] search) {
		this.search = search;
	}

}
