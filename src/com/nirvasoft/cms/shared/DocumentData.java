package com.nirvasoft.cms.shared;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DocumentData {
	private long autokey;
	private long srno;
	private String createdDate;
	private String createdTime;
	private String modifiedDate;
	private String modifiedTime;
	private String fileName;
	private String title;
	private String status;
	private String fileSize;
	private String fileType;
	private String region;
	private String filePath;
	private int recordStatus;
	private String userId;
	private String userName;
	private String t1;
	private String t2;
	private String t3;
	private String t4;
	private String t5;
	private int n1;
	private int n2;
	private int n3;
	private int n4;
	private int n5;
	private String postedby;
	private String modifiedby;
	private ArrayList<PhotoUploadData> uploadlist;

	public DocumentData() {
		super();
		clearProperties();
	}

	protected void clearProperties() {
		this.autokey = 0L;
		this.srno = 0;
		this.createdDate = "";
		this.createdTime = "";
		this.modifiedDate = "";
		this.modifiedTime = "";
		this.postedby = "";
		this.modifiedby = "";
		this.t1 = "";
		this.t2 = "";
		this.t3 = "";
		this.t4 = "";
		this.t5 = "";
		this.n1 = 0;
		this.n2 = 0;
		this.n3 = 0;
		this.n4 = 0;
		this.n5 = 0;
		this.uploadlist = new ArrayList<>();
	}

	public long getAutokey() {
		return autokey;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public String getCreatedTime() {
		return createdTime;
	}

	public String getFileName() {
		return fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public String getFileSize() {
		return fileSize;
	}

	public String getFileType() {
		return fileType;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public String getModifiedTime() {
		return modifiedTime;
	}

	public int getN1() {
		return n1;
	}

	public int getN2() {
		return n2;
	}

	public int getN3() {
		return n3;
	}

	public int getN4() {
		return n4;
	}

	public int getN5() {
		return n5;
	}

	public String getPostedby() {
		return postedby;
	}

	public int getRecordStatus() {
		return recordStatus;
	}

	public String getRegion() {
		return region;
	}

	public long getSrno() {
		return srno;
	}

	public String getStatus() {
		return status;
	}

	public String getT1() {
		return t1;
	}

	public String getT2() {
		return t2;
	}

	public String getT3() {
		return t3;
	}

	public String getT4() {
		return t4;
	}

	public String getT5() {
		return t5;
	}

	public String getTitle() {
		return title;
	}

	public ArrayList<PhotoUploadData> getUploadlist() {
		return uploadlist;
	}

	public String getUserId() {
		return userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setAutokey(long autokey) {
		this.autokey = autokey;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public void setModifiedTime(String modifiedTime) {
		this.modifiedTime = modifiedTime;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public void setN2(int n2) {
		this.n2 = n2;
	}

	public void setN3(int n3) {
		this.n3 = n3;
	}

	public void setN4(int n4) {
		this.n4 = n4;
	}

	public void setN5(int n5) {
		this.n5 = n5;
	}

	public void setPostedby(String postedby) {
		this.postedby = postedby;
	}

	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public void setSrno(long srno) {
		this.srno = srno;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public void setT4(String t4) {
		this.t4 = t4;
	}

	public void setT5(String t5) {
		this.t5 = t5;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setUploadlist(ArrayList<PhotoUploadData> uploadlist) {
		this.uploadlist = uploadlist;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
