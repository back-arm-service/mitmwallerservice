package com.nirvasoft.cms.shared;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MenuDataset {

	private String searchText;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private ArrayList<MenuData> arlData;

	public ArrayList<MenuData> getArlData() {
		return arlData;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public String getSearchText() {
		return searchText;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setArlData(ArrayList<MenuData> ret) {
		this.arlData = ret;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

}
