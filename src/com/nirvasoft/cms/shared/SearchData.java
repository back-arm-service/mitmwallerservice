package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SearchData {

	private String dbname;
	private String caption;
	private String type;
	private String condition;
	private String t1;
	private String t2;

	public SearchData() {
		super();
		clearProperties();
	}

	public void clearProperties() {
		this.dbname = "";
		this.caption = "";
		this.type = "";
		this.condition = "";
		this.t1 = "";
		this.t2 = "";
	}

	public String getCaption() {
		return caption;
	}

	public String getCondition() {
		return condition;
	}

	public String getDbname() {
		return dbname;
	}

	public String getT1() {
		return t1;
	}

	public String getT2() {
		return t2;
	}

	public String getType() {
		return type;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public void setDbname(String dbname) {
		this.dbname = dbname;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setType(String type) {
		this.type = type;
	}

}
