package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PriceListDataSet {

	private PriceListData[] data;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private boolean state;

	public PriceListDataSet() {
		clearProperties();
	}

	private void clearProperties() {
		data = new PriceListData[0];
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public PriceListData[] getData() {
		return data;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public boolean isState() {
		return state;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setData(PriceListData[] data) {
		this.data = data;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

}
