package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DivisionComboData {

	private String value;
	private String caption;
	private boolean flag;

	public DivisionComboData() {
		super();
		clearProperties();
	}

	public void clearProperties() {
		this.value = "";
		this.caption = "";
		this.flag = false;
	}

	public String getCaption() {
		return caption;
	}

	public String getValue() {
		return value;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
