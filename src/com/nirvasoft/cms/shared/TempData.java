package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class TempData {	
		private long autokey;
		private String T1;
		private String T2;
		private String T3;
		private String T4;
		private String T5;
		private String T6;
		private String T7;
		private String T8;	
		private String T9;
		private double n1;
		private double n2;		
		private int i1;		
		private boolean state;

		public TempData() {
			clearProperties();
		}

		public void clearProperties() {
			this.T1 = "";
			this.T2 = "";
			this.T3 = "";
			this.T4 = "";
			this.T5 = "";
			this.T6 = "";
			this.T7 = "";
			this.T8 = "";
			this.T9 = "";
			this.n1 = 0;
			this.n2 = 0;			
			this.i1 =0;			
			this.state = false;
		}
		public long getAutokey() {
			return autokey;
		}

		public void setAutokey(long autokey) {
			this.autokey = autokey;
		}

		public String getT9() {
			return T9;
		}

		public void setT9(String t9) {
			T9 = t9;
		}

		public void setT1(String t1) {
			T1 = t1;
		}

		public boolean isState() {
			return state;
		}

		public void setState(boolean state) {
			this.state = state;
		}

		public int getI1() {
			return i1;
		}

		public void setI1(int i1) {
			this.i1 = i1;
		}

		public double getN1() {
			return n1;
		}

		public void setN1(double n1) {
			this.n1 = n1;
		}

		public double getN2() {
			return n2;
		}

		public void setN2(double n2) {
			this.n2 = n2;
		}

		public void setN1(int n1) {
			this.n1 = n1;
		}	

		public void setN2(int n2) {
			this.n2 = n2;
		}
		

		public String getT4() {
			return T4;
		}

		public void setT4(String t4) {
			T4 = t4;
		}

		public String getT5() {
			return T5;
		}

		public void setT5(String t5) {
			T5 = t5;
		}

		public String getT6() {
			return T6;
		}

		public void setT6(String t6) {
			T6 = t6;
		}

		public String getT7() {
			return T7;
		}

		public void setT7(String t7) {
			T7 = t7;
		}

		public String getT8() {
			return T8;
		}

		public void setT8(String t8) {
			T8 = t8;
		}		

		public String getT1() {
			return T1;
		}

		public String getT2() {
			return T2;
		}

		public String getT3() {
			return T3;
		}
		
		public void setT2(String t2) {
			T2 = t2;
		}

		public void setT3(String t3) {
			T3 = t3;
		}		

	}

