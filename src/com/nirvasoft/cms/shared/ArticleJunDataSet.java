package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ArticleJunDataSet {

	private ArticleJunData[] data;
	private String[] crop;
	private String[] agro;
	private String[] fer;
	private String[] town;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private boolean state;

	public String[] getAgro() {
		return agro;
	}

	public String[] getCrop() {
		return crop;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public ArticleJunData[] getData() {
		return data;
	}

	public String[] getFer() {
		return fer;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public String[] getTown() {
		return town;
	}

	public boolean isState() {
		return state;
	}

	public void setAgro(String[] agro) {
		this.agro = agro;
	}

	public void setCrop(String[] crop) {
		this.crop = crop;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setData(ArticleJunData[] data) {
		this.data = data;
	}

	public void setFer(String[] fer) {
		this.fer = fer;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public void setTown(String[] town) {
		this.town = town;
	}

}
