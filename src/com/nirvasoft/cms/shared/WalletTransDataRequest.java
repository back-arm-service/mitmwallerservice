package com.nirvasoft.cms.shared;

public class WalletTransDataRequest {
	private String sessionID;
	private String userID;

	private String fromDate;
	private String toDate;
	private String fromAccount;
	private String toAccount;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private boolean alldate;
	private String t1;
	private String t2;
	private String transtype;
	private String fileType;
	private String merchantID;

	public WalletTransDataRequest() {
		this.clearProperty();
	}

	private void clearProperty() {
		this.sessionID = "";
		this.userID = "";

		this.fromDate = "";
		this.toDate = "";
		this.fromAccount = "";
		this.toAccount = "";
		this.totalCount = 0;
		this.currentPage = 0;
		this.pageSize = 0;
		this.t1 = "";
		this.t2 = "";
		this.transtype = "";
		this.fileType ="";
		this.merchantID = "";
	}

	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public String getFromAccount() {
		return fromAccount;
	}

	public String getFromDate() {
		return fromDate;
	}

	public int getPageSize() {
		return pageSize;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getT1() {
		return t1;
	}

	public String getT2() {
		return t2;
	}

	public String getToAccount() {
		return toAccount;
	}

	public String getToDate() {
		return toDate;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public String getTranstype() {
		return transtype;
	}

	public String getUserID() {
		return userID;
	}

	public boolean isAlldate() {
		return alldate;
	}

	public void setAlldate(boolean alldate) {
		this.alldate = alldate;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setFromAccount(String fromAccount) {
		this.fromAccount = fromAccount;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setToAccount(String toAccount) {
		this.toAccount = toAccount;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public void setTranstype(String transtype) {
		this.transtype = transtype;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

}
