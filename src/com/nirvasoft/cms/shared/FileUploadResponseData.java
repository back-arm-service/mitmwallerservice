package com.nirvasoft.cms.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FileUploadResponseData {

	private String code;
	private String fileName;
	private String filePath;

	public FileUploadResponseData() {
		super();
		clearProperties();
	}

	public void clearProperties() {
		this.code = "ERROR";
		this.fileName = "";
		this.filePath = "";
	}

	public String getCode() {
		return code;
	}

	public String getFileName() {
		return fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

}
