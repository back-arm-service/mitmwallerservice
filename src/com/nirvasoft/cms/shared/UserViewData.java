package com.nirvasoft.cms.shared;

public class UserViewData extends UserData {

	private String username;

	public UserViewData() {
		clearProperties();
	}

	@Override
	public void clearProperties() {
		super.clearProperties();
		this.username = "";
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
