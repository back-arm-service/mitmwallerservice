package com.nirvasoft.cms.framework;

import javax.xml.bind.annotation.XmlRootElement;

import com.nirvasoft.cms.shared.UserData;

@XmlRootElement
public class MrBean {
	private UserData user;
	private String message = "";
	private String logoText = "";
	private boolean status = false;

	public MrBean() {
		this.user = new UserData();

		this.message = "";
		this.status = false;
	}

	public String getLogoText() {
		return logoText;
	}

	public String getMessage() {
		return message;
	}

	public boolean getStatus() {
		return status;
	}

	public UserData getUser() {
		return this.user;
	}

	public void setLogoText(String logoText) {
		this.logoText = logoText;
	}

	public void setMessage(String command) {
		this.message = command;
	}

	public void setStatus(boolean p) {
		status = p;
	}

	public void setUser(UserData p) {
		user = p;
	}
}
