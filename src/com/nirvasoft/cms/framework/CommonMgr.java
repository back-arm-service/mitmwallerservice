package com.nirvasoft.cms.framework;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsAbstractExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;

public class CommonMgr {

	public static void downloadFile(String path, String fileName, HttpServletResponse response) {
		response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
		File file = new File(path + "/" + fileName);
		int length = (int) file.length();
		byte[] bytes = new byte[length];
		try {
			FileInputStream fin = new FileInputStream(file);
			fin.read(bytes);
			ServletOutputStream os = response.getOutputStream();
			os.write(bytes);
			os.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void exportExcel(JasperPrint jPrint, HttpServletResponse response, String fileName)
			throws IOException, JRException {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

		JRXlsExporter excelExporter = new JRXlsExporter();

		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=" + fileName);

		excelExporter.setParameter(JRExporterParameter.JASPER_PRINT, jPrint);
		excelExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
		excelExporter.setParameter(JRXlsAbstractExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
		excelExporter.setParameter(JRXlsAbstractExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
		excelExporter.setParameter(JRXlsAbstractExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
		excelExporter.setParameter(JRXlsAbstractExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
		excelExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, fileName);
		excelExporter.exportReport();

		ServletOutputStream servletOutputStream = response.getOutputStream();
		servletOutputStream.write(byteArrayOutputStream.toByteArray());
		servletOutputStream.flush();
	}

	public static void exportPdf(JasperPrint jPrint, HttpServletResponse response) throws JRException, IOException {
		JRPdfExporter pdfExporter = new JRPdfExporter();
		response.setContentType("application/pdf");
		pdfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jPrint);
		pdfExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
		pdfExporter.exportReport();
	}

	public static JasperPrint getJrPrint(Map<String, Object> params, String fileName, List<? extends Object> list)
			throws JRException {
		JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(list);
		JasperReport jasperRpt = JasperCompileManager.compileReport(fileName);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperRpt, params, dataSource);
		return jasperPrint;
	}

}
