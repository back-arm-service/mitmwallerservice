package com.nirvasoft.cms.framework;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Resultb2b {
	private boolean state = false;

	private String msgCode = "";
	private String msgDesc = "";
	private long keyResult = 0;
	private ArrayList<Long> longResult = new ArrayList<Long>();
	private ArrayList<String> stringResult = new ArrayList<String>();
	private long[] key;
	private String[] str;
	private long n1 = 0;
	private long n2 = 0;
	private boolean sessionState;

	public Resultb2b() {
		clearProperties();
	}

	private void clearProperties() {
		state = false;
		msgCode = "";
		msgDesc = "";
		longResult = new ArrayList<Long>();
		stringResult = new ArrayList<String>();
		str = new String[0];
		sessionState = false;

	}

	public boolean isSessionState() {
		return sessionState;
	}

	public void setSessionState(boolean sessionState) {
		this.sessionState = sessionState;
	}

	public long[] getKey() {
		return key;
	}

	public long getKeyResult() {
		return keyResult;
	}

	public ArrayList<Long> getLongResult() {
		return longResult;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public long getN1() {
		return n1;
	}

	public long getN2() {
		return n2;
	}

	public String[] getStr() {
		return str;
	}

	public ArrayList<String> getStringResult() {
		return stringResult;
	}

	public boolean isState() {
		return state;
	}

	public void setKey(long[] key) {
		this.key = key;
	}

	public void setKeyResult(long keyResult) {
		this.keyResult = keyResult;
	}

	public void setLongResult(ArrayList<Long> longResult) {
		this.longResult = longResult;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public void setN1(long n1) {
		this.n1 = n1;
	}

	public void setN2(long n2) {
		this.n2 = n2;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public void setStr(String[] str) {
		this.str = str;
	}

	public void setStringResult(ArrayList<String> stringResult) {
		this.stringResult = stringResult;
	}
}
