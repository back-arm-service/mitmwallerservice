package com.nirvasoft.cms.framework;

import javax.xml.bind.annotation.XmlRootElement;

import com.nirvasoft.cms.shared.ButtonCarryData;

@XmlRootElement
public class Profile {
	private String userid;
	private String password;
	private String passcode;
	private String username;
	private String domain;
	private String logotext;
	private String logolink;
	private long role;
	private boolean commandcenter;
	private Menu[] menus;
	private Menu[] rightmenus;
	private long syskey;
	private String t1;
	private String t2;
	private String t3;
	private String t4;
	private String t5;
	private String t6;
	private double n1;
	private double n2;
	private double n3;
	private long status;
	private int loginStatus;
	private ButtonCarryData[] btndata;

	public Profile() {
		username = "";
		logotext = "B2B";
		logolink = "/empty";
		role = 99;
		commandcenter = false;
		// standard menu
		menus = new Menu[0];
		rightmenus = new Menu[0];
		btndata = new ButtonCarryData[0];
	}

	public ButtonCarryData[] getBtndata() {
		return btndata;
	}

	public boolean getCommandCenter() {
		return commandcenter;
	}

	public String getDomain() {
		return domain;
	}

	public int getLoginStatus() {
		return loginStatus;
	}

	public String getLogoLink() {
		return logolink;
	}

	public String getLogoText() {
		return logotext;
	}

	public Menu[] getMenus() {
		return menus;
	}

	public double getN1() {
		return n1;
	}

	public double getN2() {
		return n2;
	}

	public double getN3() {
		return n3;
	}

	public String getPasscode() {
		return passcode;
	}

	public String getPassword() {
		return password;
	}

	public Menu[] getRightMenus() {
		return rightmenus;
	}

	public long getRole() {
		return role;
	}

	public long getStatus() {
		return status;
	}

	public long getSysKey() {
		return syskey;
	}

	public String getT1() {
		return t1;
	}

	public String getT2() {
		return t2;
	}

	public String getT3() {
		return t3;
	}

	public String getT4() {
		return t4;
	}

	public String getT5() {
		return t5;
	}

	public String getT6() {
		return t6;
	}

	public String getUserID() {
		return userid;
	}

	public String getUserName() {
		return username;
	}

	public void setBtndata(ButtonCarryData[] btndata) {
		this.btndata = btndata;
	}

	public void setCommandCenter(boolean p) {
		this.commandcenter = p;
	}

	public void setDomain(String p) {
		this.domain = p;
	}

	public void setLoginStatus(int loginStatus) {
		this.loginStatus = loginStatus;
	}

	public void setLogoLink(String p) {
		this.logolink = p;
	}

	public void setLogoText(String p) {
		this.logotext = p;
	}

	public void setMenus(Menu[] p) {
		this.menus = p;
	}

	public void setN1(double p) {
		this.n1 = p;
	}

	public void setN2(double p) {
		this.n2 = p;
	}

	public void setN3(double p) {
		this.n3 = p;
	}

	public void setPasscode(String p) {
		this.passcode = p;
	}

	public void setPassword(String p) {
		this.password = p;
	}

	public void setRightMenus(Menu[] p) {
		this.rightmenus = p;
	}

	public void setRole(long p) {
		this.role = p;
	}

	public void setStatus(long status) {
		this.status = status;
	}

	public void setSysKey(long p) {
		this.syskey = p;
	}

	public void setT1(String p) {
		this.t1 = p;
	}

	public void setT2(String p) {
		this.t2 = p;
	}

	public void setT3(String p) {
		this.t3 = p;
	}

	public void setT4(String p) {
		this.t4 = p;
	}

	public void setT5(String p) {
		this.t5 = p;
	}

	public void setT6(String p) {
		this.t6 = p;
	}

	public void setUserID(String p) {
		this.userid = p;
	}

	public void setUserName(String p) {
		this.username = p;
	}

}
