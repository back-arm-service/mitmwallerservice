package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nirvasoft.cms.dao.OPTDao;
import com.nirvasoft.cms.dao.PersonDao;
import com.nirvasoft.cms.dao.RegisterDao;
import com.nirvasoft.cms.dao.UserDao;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.DivisionComboDataSet;
import com.nirvasoft.cms.shared.PersonData;
import com.nirvasoft.cms.shared.RegisterData;
import com.nirvasoft.cms.shared.UserData;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.database.SysKeyMgr;

public class OPTMgr {

	public static Resultb2b checkUser(String t1, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = RegisterDao.readByUser(t1, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static Resultb2b deleteOPT(String mobile, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = OPTDao.deleteOPT(mobile, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static Resultb2b deleteUser(String mobile, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = OPTDao.delete(mobile, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static Resultb2b deleteUserByMail(String email, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = OPTDao.deleteUserByMail(email, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	// TDA
	public static Resultb2b deleteUserfordeactivate(String mobile, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = OPTDao.deletefordeactivate(mobile, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static long ExistStatus4(String t1, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		long res = 0;
		res = OPTDao.ExistStatus4(t1, conn); // to match by 1 column
		return res;
	}

	public static DivisionComboDataSet getCroplist(MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		DivisionComboDataSet res = new DivisionComboDataSet();
		try {
			res = OPTDao.getCropCombolist(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static DivisionComboDataSet getMonsoonCrop(MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		DivisionComboDataSet res = new DivisionComboDataSet();
		try {
			res = OPTDao.getMonsoonCropCombo(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static DivisionComboDataSet getMonsoonCroplist(MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		DivisionComboDataSet res = new DivisionComboDataSet();
		try {
			res = OPTDao.getMonsoonCropCombolist(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static PersonData getPersonData(UserData bdata) {
		PersonData adata = new PersonData();
		adata.setSyskey(bdata.getPerson().getSyskey());
		adata.setT1(bdata.getT1());
		adata.setT2(bdata.getT5());
		adata.setCreatedDate(bdata.getCreatedDate());
		adata.setModifiedDate(bdata.getModifiedDate());
		adata.setRecordStatus(bdata.getRecordStatus());
		adata.setSyncStatus(bdata.getSyncStatus());
		adata.setSyncBatch(bdata.getSyncBatch());
		adata.setUserId(bdata.getUserId());
		adata.setUserName(bdata.getUserName());
		return adata;
	}

	public static PersonData getPersonDatas(UserData bdata) {
		PersonData adata = new PersonData();
		adata.setSyskey(bdata.getPerson().getSyskey());
		adata.setT1(bdata.getT1());
		adata.setT2(bdata.getT5());
		adata.setCreatedDate(bdata.getCreatedDate());
		adata.setModifiedDate(bdata.getModifiedDate());
		adata.setRecordStatus(bdata.getRecordStatus());
		adata.setSyncStatus(bdata.getSyncStatus());
		adata.setSyncBatch(bdata.getSyncBatch());
		adata.setUserId(bdata.getUserId());
		adata.setUserName(bdata.getUserName());
		return adata;
	}

	public static long getSyskey(String t1, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		long res = 0;
		res = OPTDao.getSyskey(t1, conn); // to match by 1 column
		return res;
	}

	public static DivisionComboDataSet getWinterCroplist(MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		DivisionComboDataSet res = new DivisionComboDataSet();
		try {
			res = OPTDao.getWinterCropCombolist(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static UserData initData(UserData data, MrBean user) {
		String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (data.getSyskey() == 0) {
			data.setCreatedDate(date);
		}
		if (data.getCreatedDate().equals("")) {
			data.setCreatedDate(date);
		}
		data.setT2(data.getT2());
		data.setUserName("User");
		data.setModifiedDate(date);
		data.setRecordStatus(1);
		data.setSyncBatch(0);
		data.setSyncStatus(1);
		return data;
	}

	public static UserData initDatas(UserData data, MrBean user) {
		String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (data.getSyskey() == 0) {
			data.setCreatedDate(date);
		}
		if (data.getCreatedDate().equals("")) {
			data.setCreatedDate(date);
		}
		data.setT2(data.getT2());
		data.setUserName("User");
		data.setModifiedDate(date);
		data.setRecordStatus(0);
		data.setSyncBatch(0);
		data.setSyncStatus(1);
		return data;
	}

	public static boolean isID(String t1, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		boolean res = false;
		res = OPTDao.isID(t1, conn); // to match by 1 column
		return res;
	}

	public static boolean isIDexist(String t1, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		boolean res = false;
		res = OPTDao.isIDexist(t1, conn); // to match by 1 column
		return res;
	}

	public static long isPhexist(String t1, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		long res = 0;
		res = OPTDao.isPhexist(t1, conn); // to match by 1 column
		return res;
	}

	public static boolean isPhExist(String t1, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		boolean res = false;
		res = OPTDao.isPhExist(t1, conn); // to match by 1 column
		return res;
	}

	public static long isPhexistIn12(String t1, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		long res = 0;
		res = OPTDao.isPhexistIn12(t1, conn); // to match by 1 column
		return res;
	}

	public static long readData(String t1, String t2, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		long res = 0;
		t2 = ServerUtil.encryptPIN(t2);
		res = OPTDao.readData(t1, t2, conn); // to match by 1 column
		return res;
	}

	public static String save(String t1, String t2, String t3, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		String res;
		res = OPTDao.insert(t1, t2, t3, conn);
		return res;
	}

	// mobile
	public static String saveData(UserData data, MrBean user) {
		// Result res = new Result();
		String res = "";
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = saveData(data, user, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	// mobile
	public static String saveData(UserData data, MrBean user, Connection conn) throws SQLException {
		Resultb2b ret = new Resultb2b();
		String res = "";
		data.getPerson().setSyskey(UserDao.getPersonSyskey(data.getSyskey(), conn));
		if (data.getPerson().getSyskey() == 0) {
			data.getPerson()
					.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID())));
			data.getT2();
			data = initDatas(data, user);
			ret = PersonDao.insert(getPersonData(data), conn);// save UVM012
			if (ret.isState()) {
				if (data.getSyskey() == 0) {
					data.setSyskey(
							SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID())));
					data.setN4(data.getPerson().getSyskey());
					data = initDatas(data, user);
					res = OPTDao.insertData(data, conn);// save UVM005
				}
			}
		} else {
		}
		return res;
	}

	public static RegisterData saveProfileData(RegisterData data, MrBean user) {
		RegisterData res = new RegisterData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = saveProfileData(data, user, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public static RegisterData saveProfileData(RegisterData data, MrBean user, Connection conn) throws SQLException {
		RegisterData res = new RegisterData();
		Resultb2b ret = new Resultb2b();
		if (data.getSyskey() != 0) {
			ret = RegisterDao.updateProfile(data, conn);
			if (ret.isState()) {
				res = RegisterMgr.readByID(data.getT1(), user);
			} else {
				res.setSyskey(0);
			}
		}
		return res;
	}

	// TDA
	public static RegisterData saveProfileDataforupdate(RegisterData data, MrBean user) {
		RegisterData res = new RegisterData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = saveProfileDataforupdate(data, user, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	// TDA
	public static RegisterData saveProfileDataforupdate(RegisterData data, MrBean user, Connection conn)
			throws SQLException {
		RegisterData res = new RegisterData();
		Resultb2b ret = new Resultb2b();
		if (data.getSyskey() != 0) {
			ret = RegisterDao.updateProfileforupdate(data, conn);
			if (ret.isState()) {
				res = RegisterMgr.readByID(data.getT1(), user);
			} else {
				res.setSyskey(0);
			}
		}

		return res;
	}

	public static String saveRegData(UserData data, MrBean user) {
		String res = "";
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = UpdateRegisterData(data, user, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public static String saveUserData(UserData data, MrBean user) {
		String res = "";
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = saveUserData(data, user, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public static String saveUserData(UserData data, MrBean user, Connection conn) throws SQLException {
		Resultb2b ret = new Resultb2b();
		String res = "";
		data.getPerson().setSyskey(UserDao.getPersonSyskey(data.getSyskey(), conn));
		if (data.getPerson().getSyskey() == 0) {
			data.getPerson()
					.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID())));
			data.getT2();
			data = initData(data, user);
			ret = PersonDao.insert(getPersonData(data), conn);// save UVM012
			if (ret.isState()) {
				if (data.getSyskey() == 0) {
					data.setSyskey(
							SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID())));
					data.setN4(data.getPerson().getSyskey());
					data = initData(data, user);
					res = OPTDao.insertData(data, conn);// save UVM005
				}
			}

		} else {

		}
		return res;

	}

	public static long searchSyskey(String mobile, MrBean user) {
		long res = 0;
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = OPTDao.searchByID(mobile, conn);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static String update(String t1, String t2, String t4, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		String res = "";
		try {
			res = OPTDao.Update(t1, t2, t4, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static String updateReg(String t1, String t2, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		String res = "";
		try {
			res = OPTDao.UpdateData(t1, t2, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static String updateReg1(String t1, String t2, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		String res = "";
		try {
			res = OPTDao.UpdateData1(t1, t2, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static String updateRegData(UserData data, MrBean user) {
		String res = "";
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = UpdateRegData4(data, user, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public static String UpdateRegData(UserData data, MrBean user, Connection conn) throws SQLException {
		Resultb2b ret = new Resultb2b();
		String res = "";
		if (data.getSyskey() != 0) {
			long syskey12 = OPTMgr.isPhexistIn12(data.getT1(), user);// get
																		// syskey
																		// UVM012
			if (syskey12 > 0) {
				data.getPerson().setSyskey(syskey12);
				data = initDatas(data, user);
				ret = PersonDao.update12(getPersonData(data), conn);// update
																	// UVM012
				if (ret.isState()) {
					data.setN4(syskey12);
					data = initDatas(data, user);
					res = OPTDao.updateData(data, conn);// update UVM005
				}
			}
		} else {
		}
		return res;
	}

	public static String UpdateRegData4(UserData data, MrBean user, Connection conn) throws SQLException {// recordstatus
		Resultb2b ret = new Resultb2b();
		String res = "";
		if (data.getSyskey() != 0) {
			long syskey12 = OPTMgr.ExistStatus4(data.getT1(), user);// get
																	// syskey
																	// UVM012
			if (syskey12 > 0) {
				data.getPerson().setSyskey(syskey12);
				data = initDatas(data, user);
				ret = PersonDao.updateStatus(getPersonData(data), conn);// update
				if (ret.isState()) {
					data.setN4(syskey12);
					data = initDatas(data, user);
					res = OPTDao.updateRegData(data, conn);// update UVM005

				}

			}
		} else {
		}
		return res;
	}

	public static String UpdateRegisterData(UserData data, MrBean user, Connection conn) throws SQLException {
		Resultb2b ret = new Resultb2b();
		String res = "";
		if (data.getSyskey() != 0) {
			long syskey12 = OPTMgr.isPhexistIn12(data.getT1(), user);// get
																		// syskey
																		// UVM012
			if (syskey12 > 0) {
				data.getPerson().setSyskey(syskey12);
				data = initDatas(data, user);
				ret = PersonDao.update12(getPersonData(data), conn);// update
																	// UVM012
				if (ret.isState()) {
					data.setN4(syskey12);
					data = initDatas(data, user);
					res = OPTDao.updateData(data, conn);// update UVM005
				}
			}
		} else {
		}
		return res;
	}

	public static Resultb2b UpdateUserData(RegisterData data, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = RegisterDao.updateRegister(data, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

}
