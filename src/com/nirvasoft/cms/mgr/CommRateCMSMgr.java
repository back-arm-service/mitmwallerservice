package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.cms.dao.CommRateCMSDao;
import com.nirvasoft.cms.framework.Result;
import com.nirvasoft.cms.shared.CommRateHeaderData;
import com.nirvasoft.cms.shared.MCommRateMappingData;
import com.nirvasoft.cms.shared.MCommRateMappingMappingDataSet;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.dao.MerchantDao;
import com.nirvasoft.rp.data.BranchCodeData;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Lov3;
import com.nirvasoft.rp.shared.CMSMerchantData;
import com.nirvasoft.rp.shared.CMSMerchantDataSet;
import com.nirvasoft.rp.util.ServerUtil;

public class CommRateCMSMgr {

	public Result deletebyMerchantID(String aUserID) {
		// TODO Auto-generated method stub
		Result res = new Result();
		MerchantDao l_DAO = new MerchantDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");

			if (l_DAO.deletebyMerchantID(aUserID, conn)) {
				res.setState(true);
				res.setMsgCode("0002");
				res.setMsgDesc("Deleted Successfully.");
			} else {
				res.setState(false);
				res.setMsgCode("0052");
				res.setMsgDesc("Delete Failed.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			res.setState(false);
			res.setMsgCode("0014");
			res.setMsgDesc(e.getMessage());
		} finally {
			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				res.setState(false);
				res.setMsgCode("0014");
				res.setMsgDesc(e.getMessage());
			}
		}

		return res;
	}

	public Result deleteFlexCommRate(String mID) {

		Result ret = new Result();
		CommRateCMSDao Dao = new CommRateCMSDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			ret = Dao.deleteFlexCommRate(mID, conn);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return ret;

	}

	public Result deleteMerchantCommRateMapping(String commRef, int flat, String t1) {

		Result ret = new Result();
		CommRateCMSDao Dao = new CommRateCMSDao();
		Connection l_Conn = null;
		try {
			l_Conn = ConnAdmin.getConn("001", "");
			ret = Dao.deleteMerchantCommRateMapping(commRef, flat, t1, l_Conn);

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return ret;

	}

	public ArrayList<Result> getAllCommRef() {
		CommRateCMSDao Dao = new CommRateCMSDao();
		ArrayList<Result> res = new ArrayList<Result>();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = Dao.getAllCommRef(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public CMSMerchantDataSet getAllMerchantData(String searchText, int pageSize, int currentPage) {
		CMSMerchantDataSet res = new CMSMerchantDataSet();
		MerchantDao m_dao = new MerchantDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = m_dao.getAllMerchantData(searchText, pageSize, currentPage, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return res;
	}

	public BranchCodeData getBrCode() {
		BranchCodeData res = new BranchCodeData();
		CommRateCMSDao m_dao = new CommRateCMSDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = m_dao.getBrCode(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return res;
	}

	public CMSMerchantData getCMSMerchantDataByID(String acode) {

		CMSMerchantData ret = new CMSMerchantData();
		DAOManager l_DAO = new DAOManager();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			MerchantDao custdao = new MerchantDao();
			ret = custdao.getCMSMerchantDataByID(acode, conn);

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return ret;
	}

	public CommRateHeaderData getFlexCommRateDataByID(String id) {
		// TODO Auto-generated method stub
		CommRateHeaderData ret = new CommRateHeaderData();
		CommRateCMSDao Dao = new CommRateCMSDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			ret = Dao.getFlexCommRateDataByID(id, conn);

		} finally {

			try {
				if (!conn.isClosed())
					conn.close();

			} catch (SQLException e) {

				e.printStackTrace();
			}
		}

		return ret;
	}

	// Merchant Account Mapping Start //
	public CommRateHeaderData getFlexCommRateList(String searchText, int pageSize, int currentPage) {
		CommRateHeaderData res = new CommRateHeaderData();
		CommRateCMSDao Dao = new CommRateCMSDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = Dao.getFlexCommRateList(searchText, pageSize, currentPage, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return res;
	}

	public MCommRateMappingData getMerchantCommData(String merchantSyskey) {

		MCommRateMappingData ret = new MCommRateMappingData();
		CommRateCMSDao comm_dao = new CommRateCMSDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			ret = comm_dao.getMerchantCommData(merchantSyskey, conn);

		} catch (SQLException e) {

			e.printStackTrace();
		} finally {

			try {
				if (!conn.isClosed())
					conn.close();

			} catch (SQLException e) {

				e.printStackTrace();
			}
		}

		return ret;
	}

	public MCommRateMappingMappingDataSet getMerchantCommRateMappingList(String searchText, int pageSize,
			int currentPage) {
		MCommRateMappingMappingDataSet res = new MCommRateMappingMappingDataSet();
		CommRateCMSDao m_dao = new CommRateCMSDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = m_dao.getMerchantCommRateMappingList(searchText, pageSize, currentPage, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return res;
	}

	public CMSMerchantData getMerchantDataByID(String merchantId) {

		CMSMerchantData ret = new CMSMerchantData();
		MerchantDao m_dao = new MerchantDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			ret = m_dao.getMerchantDataByID(merchantId, conn);

		} catch (SQLException e) {

			e.printStackTrace();
		} finally {

			try {
				if (!conn.isClosed())
					conn.close();

			} catch (SQLException e) {

				e.printStackTrace();
			}
		}

		return ret;
	}

	public Lov3 getZone() {

		Lov3 lov3 = new Lov3();
		CommRateCMSDao dao = new CommRateCMSDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			lov3 = dao.getZone(conn);

		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();

			}
		}

		return lov3;

	}

	public CommRateHeaderData saveFlexCommRate(CommRateHeaderData aData) {

		CommRateCMSDao Dao = new CommRateCMSDao();
		Result response = new Result();
		Connection l_Conn = null;
		try {
			l_Conn = ConnAdmin.getConn("001", "");
			response = Dao.saveFlexCommRate(aData, l_Conn);

			if (response.getKeyst().equals("01")) {
				aData.setMsgCode("0000");
				aData.setMsgDesc("Saved Successfully.");
				aData.setState("true");
				aData.setHkey(response.getKeyResult());
				aData.setCommRef(response.getKeyString());
			} else if (response.getKeyst().equals("00")) {
				aData.setMsgCode("0050");
				aData.setMsgDesc("Save Failed.");
				aData.setState("false");
			} else if (response.getKeyst().equals("11")) {
				aData.setMsgCode("0001");
				aData.setMsgDesc("Updated Successfully.");
				aData.setHkey(response.getKeyResult());
				aData.setState("true");
				aData.setCommRef(response.getKeyString());
			} else if (response.getKeyst().equals("10")) {
				aData.setMsgCode("0051");
				aData.setMsgDesc("Update Failed.");
				aData.setState("false");
			}

		} catch (Exception e) {

			System.out.println("Duplicate error :" + e.getMessage());
			aData.setMsgCode("0014");
			aData.setMsgDesc("Duplicate Record.");
			aData.setState("false");
			aData.setHkey(aData.getCommdetailArr()[0].getHkey());
			e.printStackTrace();
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				aData.setMsgCode("0014");
				aData.setMsgDesc(e.getMessage());
				aData.setHkey(response.getKeyResult());
			}
		}
		return aData;
	}

	public Result saveMerchantCommRateMapping(MCommRateMappingData data) {

		Result ret = new Result();
		CommRateCMSDao custdao = new CommRateCMSDao();
		Connection l_Conn = null;
		try {
			l_Conn = ConnAdmin.getConn("001", "");
			ret = custdao.saveMerchantCommRateMapping(data, l_Conn);
		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return ret;

	}

	// Merchant Account Mapping End //

	public Result updateMerchantCommRateMapping(MCommRateMappingData data) {

		Result ret = new Result();
		CommRateCMSDao Dao = new CommRateCMSDao();
		Connection l_Conn = null;
		try {
			l_Conn = ConnAdmin.getConn("001", "");
			ret = Dao.updateMerchantCommRateMapping(data, l_Conn);
		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return ret;

	}

}
