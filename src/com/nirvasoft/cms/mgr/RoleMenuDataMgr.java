package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.cms.dao.RoleMenuDao;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.shared.RoleMenuData;

public class RoleMenuDataMgr {

	public static RoleMenuData[] getRoleMenuList(MrBean user) throws SQLException {

		RoleMenuData[] dataarray;
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		dataarray = RoleMenuDao.getRoleMenuList(conn);
		// ButtonData[] btns = ButtonDataDao.getButtonList(conn).getData();
		// dataarray = setRoleButtons(dataarray,btns);
		return dataarray;
	}

	/*
	 * public static RoleMenuData[] getRoleMenus(MrBean user) throws
	 * SQLException {
	 * 
	 * RoleMenuData[] dataarray; Connection conn =
	 * ConnAdmin.getConn(user.getUser().getOrganizationID()); dataarray =
	 * RoleMenuDao.getRoleMenuList(conn); return dataarray ; }
	 */

	/*
	 * public static RoleMenuData[] setRoleButtons(RoleMenuData[] data,
	 * ButtonData[] btns) {
	 * 
	 * for (int i = 0; i < data.length; i++) { for (int j = 0; j <
	 * data[i].getChildmenus().length; j++) { String s =
	 * data[i].getChildmenus()[j].getT3(); System.out.println(s); String[] list
	 * = s.split(",");
	 * 
	 * ArrayList<ButtonData> rmlist = new ArrayList<ButtonData>(); for (int k =
	 * 0; k < list.length; k++) { for (int l = 0; l < btns.length; l++) { if
	 * (list[k].equals(String.valueOf(btns[l].getSyskey()))) {
	 * rmlist.add(btns[l]); } } } ButtonData[] dataarry = new
	 * ButtonData[rmlist.size()]; dataarry = rmlist.toArray(dataarry);
	 * data[i].getChildmenus()[j].setBtns(dataarry); } } return data;
	 * 
	 * }
	 */

}
