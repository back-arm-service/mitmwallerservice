package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nirvasoft.cms.dao.SetupDao;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.AdvancedSearchData;
import com.nirvasoft.cms.shared.ComboDataSet;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.cms.shared.SetupData;
import com.nirvasoft.cms.shared.SetupDataSet;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.database.SysKeyMgr;

public class SetupMgr {
	public static SetupDataSet callistConfiguraion(AdvancedSearchData p, String searchVal, String sort, String type,
			MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		SetupDataSet res = new SetupDataSet();
		try {
			res = SetupDao.callistConfiguraion(p, searchVal, sort, type, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	public static Resultb2b changedConfiguration(SetupDataSet p, MrBean user) {
		Connection l_Conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		Resultb2b res = new Resultb2b();
		try {
			String sql = "DELETE FROM Configuration WHERE recordstatus <> 4 ";
			PreparedStatement stmt = l_Conn.prepareStatement(sql);
			stmt.executeUpdate();
			for (SetupData data : p.getArr()) {
				data.setT4("");
				if (data.getT2().equalsIgnoreCase("Questions & Answers")) {
					data.setT3("question");
				} else if (data.getT2().equalsIgnoreCase("News & Media")) {
					data.setT3("news & media");
				} else if (data.getT2().equalsIgnoreCase("Video")) {
					data.setT3("video");
				} else if (data.getT2().equalsIgnoreCase("Education")) {
					data.setT3("education");
				} else {
					data.setT3(data.getT2());
				}

				data = SetupMgr.initData(data, user);
				data.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID())));
				res = SetupDao.insert(data, l_Conn);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(l_Conn);
		}

		return res;
	}

	public static Resultb2b deletConfiguration(Long key, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = SetupDao.deleteConfiguration(key, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static ComboDataSet getMobileMenuList(MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ComboDataSet res = new ComboDataSet();
		try {
			res = SetupDao.getMobileMenuList(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static SetupData initData(SetupData data, MrBean user) {
		String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (data.getSyskey() == 0) {
			data.setCreatedDate(date);
		} else {
			data.setCreatedDate(date);

		}
		data.setUserId(data.getT1());
		data.setUserName("User");
		data.setModifiedDate(date);
		data.setRecordStatus(1);
		data.setSyncBatch(0);
		data.setSyncStatus(1);

		return data;
	}

	public static SetupDataSet readconfiguration(MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		SetupDataSet res = new SetupDataSet();
		try {
			res = SetupDao.readconfiguration(conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;

	}

	public static SetupData readConfiguration(long key, MrBean user) {
		SetupData res = new SetupData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = SetupDao.readConfiguration(key, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	public static Resultb2b saveConfiguration(SetupData data, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			String sql1 = "select t2 from uvm022 where syskey='" + data.getN1() + "'";
			PreparedStatement stat = conn.prepareStatement(sql1);
			ResultSet reet = stat.executeQuery();
			if (reet.next()) {
				data.setT2(reet.getString("t2"));
			}
			if (data.getT2().equalsIgnoreCase("Questions & Answers")) {
				data.setT3("question");
			} else {
				data.setT3(data.getT2());
			}
			if (data.getSyskey() == 0) {
				data.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID())));

				data = initData(data, user);
				res = SetupDao.insert(data, conn);
			} else {
				res = SetupDao.update(data, conn);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public static SetupDataSet searchDownloadList(PagerData p, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		SetupDataSet res = new SetupDataSet();
		try {
			res = SetupDao.searchDownloadList(p, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
}
