package com.nirvasoft.cms.mgr;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import com.nirvasoft.rp.mgr.NotiMgr;
import com.nirvasoft.rp.shared.ResponseData;

public class NotiScheduler extends TimerTask {

	@Override
	public void run() {
		Calendar cal = Calendar.getInstance();

		Date now = cal.getTime();
		SimpleDateFormat sf1 = new SimpleDateFormat("yyyy-MM-dd");
		String date = sf1.format(now);

		SimpleDateFormat sf2 = new SimpleDateFormat("HH:mm:ss");
		String time = sf2.format(now);

		NotiMgr mgr = new NotiMgr();
		mgr.sendNoti(date, time);
	}

	public static ResponseData setSchedule(String date, String time) {
		ResponseData res = new ResponseData();
		try {
			Timer timer = new Timer();
			String userDateTime = date + " " + time;
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date userDate = df.parse(userDateTime);
			timer.schedule(new NotiScheduler(), userDate, TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS)); // period:

			res.setCode("0000");
			res.setDesc("Set schedule successfully.");
		} catch (Exception e) {
			e.printStackTrace();

			res.setCode("0014");
			res.setDesc(e.getMessage());
		}
		return res;
	}

}
