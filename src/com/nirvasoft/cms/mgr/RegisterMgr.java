package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.nirvasoft.cms.dao.OPTDao;
import com.nirvasoft.cms.dao.RegisterDao;
import com.nirvasoft.cms.dao.UserDao;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.cms.shared.RegisterData;
import com.nirvasoft.cms.shared.RegisterDataSet;
import com.nirvasoft.cms.shared.UploadData;
import com.nirvasoft.cms.shared.UserData;
import com.nirvasoft.cms.shared.UserRole;
import com.nirvasoft.cms.util.FontChangeUtil;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.database.SysKeyMgr;

public class RegisterMgr {
	public static Resultb2b deleteReg(Long key, RegisterData p, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = RegisterDao.delete(key, p, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static UserData initData(UserData data, MrBean user) {
		String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (data.getSyskey() == 0) {
			data.setCreatedDate(date);
		}
		if (data.getCreatedDate().equals("")) {
			data.setCreatedDate(date);
		}
		data.setT2(data.getT2());
		data.setUserName("User");
		data.setModifiedDate(date);
		data.setRecordStatus(1);
		data.setSyncBatch(0);
		data.setSyncStatus(1);

		return data;
	}

	public static boolean isExistMobile(RegisterData data, MrBean user) {
		boolean b = false;
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		b = RegisterDao.isExistMobile(data, conn);
		return b;
	}

	public static boolean isExistPh(RegisterData data, MrBean user) {
		boolean b = false;
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		b = RegisterDao.isExistPh(data, conn);
		return b;
	}

	public static boolean IsExitmoible(String t1, MrBean user) {
		boolean res = false;
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		res = RegisterDao.IDExist(t1, conn); // to match by 1 column
		return res;
	}

	public static RegisterData readByID(String id, MrBean user) {
		RegisterData res = null;
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = RegisterDao.readByID(id, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static UserData readByM(String id, MrBean user) {
		UserData res = null;
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = UserDao.readByID(id, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static RegisterData readByPHID(String id, String serialno, MrBean user) {
		RegisterData res = null;
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = RegisterDao.readByPHID(id, serialno, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	/*
	 * public static RegisterData readID(String id, MrBean user) { RegisterData
	 * res = null; Connection conn =
	 * ConnAdmin.getConn(user.getUser().getOrganizationID()); try { res =
	 * RegisterDao.readID(id, conn); } catch (SQLException e) {
	 * e.printStackTrace(); } return res; }
	 */

	/*
	 * public static RegisterData readByPH(String id, MrBean user) {
	 * RegisterData res = null; Connection conn =
	 * ConnAdmin.getConn(user.getUser().getOrganizationID()); try { res =
	 * RegisterDao.readByPH(id, conn); } catch (SQLException e) {
	 * e.printStackTrace(); }
	 * 
	 * return res; }
	 */

	// TDA
	public static RegisterData readByprofileSyskey(long key, MrBean user) {
		RegisterData res = new RegisterData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArrayList<String> strList = new ArrayList<String>();
		try {
			String mobile = OPTDao.searchMobile(key, user);

			if (mobile != "") {
				res = RegisterDao.read(mobile, conn);

				strList.add(res.getT16());
				String[] strArray = new String[strList.size()];
				strArray = strList.toArray(strArray);
				res.setUpload(strArray);
				res.setT2(ServerUtil.decryptPIN(res.getT2()));
			} else {
				res = RegisterDao.readProfileReg(key, conn);
				// res.setT23(res.getT4());
				strList.add(res.getT16());
				String[] strArray = new String[strList.size()];
				strArray = strList.toArray(strArray);
				res.setUpload(strArray);
				res.setT2(ServerUtil.decryptPIN(res.getT2()));
				/*
				 * if(res.getT3().contains(" ")){ int index =
				 * res.getT3().lastIndexOf(" "); String str1 =
				 * res.getT3().substring(0, index); String str =
				 * res.getT3().substring(index+1); res.setT3(str1);
				 * res.setT4(str); }
				 */

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	public static RegisterData readDataBySyskey(long key, MrBean user) {
		RegisterData res = new RegisterData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArrayList<String> strList = new ArrayList<String>();
		try {
			String mobile = OPTDao.searchMobile(key, user);
			if (mobile != "") {
				res = RegisterDao.read(mobile, conn);
				strList.add(res.getT16());
				String[] strArray = new String[strList.size()];
				strArray = strList.toArray(strArray);
				res.setUpload(strArray);
				res.setT2(ServerUtil.decryptPIN(res.getT2()));

			} else {
				res = RegisterDao.readReg(key, conn);
				strList.add(res.getT16());
				String[] strArray = new String[strList.size()];
				strArray = strList.toArray(strArray);
				res.setUpload(strArray);
				res.setT2(ServerUtil.decryptPIN(res.getT2()));

				/*
				 * if(res.getT3().contains(" ")){ int index =
				 * res.getT3().lastIndexOf(" "); String str1 =
				 * res.getT3().substring(0, index); String str =
				 * res.getT3().substring(index+1); res.setT3(str1);
				 * res.setT4(str); }
				 */

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	// TDA readDeviceID
	public static RegisterData readDeviceID(long n5, MrBean user) {
		RegisterData res = new RegisterData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		res = RegisterDao.readDeviceID(n5, conn);
		return res;
	}

	public static String resetoptforregister(String mobile, String otpcode, MrBean user) throws SQLException {
		String res = "";
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		res = RegisterDao.updateresetotp(mobile, otpcode, conn);
		return res;
	}

	public static RegisterData saveProfile(RegisterData data, MrBean user) {
		RegisterData res = new RegisterData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			conn.setAutoCommit(false);
			data = setStatusData(data, user);
			if (data.getSyskey() == 0) {
			} else {
				res = OPTMgr.saveProfileData(data, user);
			}
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static RegisterData saveProfileData(RegisterData data, MrBean user, Connection conn) throws SQLException {
		RegisterData res = new RegisterData();
		Resultb2b ret = new Resultb2b();
		if (data.getSyskey() != 0) {
			ret = RegisterDao.updateProfile(data, conn);
			if (ret.isState()) {
				res = RegisterMgr.readByID(data.getT1(), user);
			} else {
				res.setSyskey(0);
			}
		}
		return res;
	}

	// TDA
	public static RegisterData saveProfileforupdate(RegisterData data, MrBean user) {
		RegisterData res = new RegisterData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			conn.setAutoCommit(false);
			data = setStatusData(data, user);
			if (data.getSyskey() == 0) {
			} else {
				res = OPTMgr.saveProfileDataforupdate(data, user);
			}

			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	/*
	 * public static String saveRegisterData(RegisterData data, MrBean user) {
	 * String res = ""; Connection conn =
	 * ConnAdmin.getConn(user.getUser().getOrganizationID()); try { data =
	 * setStatusData(data, user); if (data.getSyskey() == 0) {
	 * data.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
	 * ConnAdmin.getConn(user.getUser().getOrganizationID()))); res =
	 * RegisterDao.save(data, conn); } else { } } catch (SQLException e) {
	 * e.printStackTrace(); }
	 * 
	 * return res; }
	 */
	public static Resultb2b saveRegisterData(RegisterData data, MrBean user) {
		Resultb2b res = new Resultb2b();

		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());

		try {
			// data.setT4(ServerUtil.encryptPIN(data.getT4()));
			res = RegisterDao.save(data, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	// TDA for confirm
	public static RegisterData saveRegisterDataforconfirm(RegisterData data, MrBean user) {
		String ret = "";
		RegisterData res = new RegisterData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			data = setStatusDatas(data, user);
			if (data.getSyskey() == 0) {
				data.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID())));
				res = RegisterDao.saveforotp(data, conn);
				if (res.getSyskey() > 0) {
					UserRole jun = new UserRole();
					jun.setRecordStatus(data.getRecordStatus());
					jun.setSyncBatch(data.getSyncBatch());
					jun.setSyncStatus(data.getSyncStatus());
					jun.setUsersyskey(data.getSyskey());
					jun.setN1(data.getUsersyskey());
					jun.setN2(11501);
					ret = OPTDao.insertjunction(jun, conn);
				}
			} else {
				res = RegisterDao.updateOTP(data.getT1(), data.getT60(), conn);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static RegisterData saveRegisterDataforotp(RegisterData data, MrBean user) {
		String res = "";
		RegisterData ret = new RegisterData();
		long rolesk = 11501;
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			data = setStatusDatas(data, user);
			if (data.getSyskey() == 0) {
				data.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID())));
				ret = RegisterDao.saveforotp(data, conn);
				if (ret.getSyskey() > 0) {
					UserRole jun = new UserRole();
					jun.setRecordStatus(data.getRecordStatus());
					jun.setSyncBatch(data.getSyncBatch());
					jun.setSyncStatus(data.getSyncStatus());
					jun.setUsersyskey(data.getSyskey());
					jun.setN1(data.getUsersyskey());
					jun.setN2(11501);
					res = OPTDao.insertjunction(jun, conn);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return ret;
	}

	/*
	 * public static Result saveRegister(RegisterData data, MrBean user) {// for
	 * Result res = new Result(); Connection conn =
	 * ConnAdmin.getConn(user.getUser().getOrganizationID()); UserData userdata
	 * = new UserData(); String ret = ""; long l[] = new long[1]; try { data =
	 * setStatusData(data, user); if (data.getSyskey() != 0) { if
	 * (data.getT8().equals("")) { } data = setAdRegData(data, user); userdata =
	 * RegisterMgr.readByM(data.getT1(), user);
	 * data.setUsersyskey(userdata.getSyskey()); data.setT17(""); res =
	 * RegisterDao.updateProfile(data, conn);
	 * 
	 * } else { data.setT2(ServerUtil.encryptPIN(data.getT2())); long syskey =
	 * SysKeyMgr.getSysKey(1, "syskey",
	 * ConnAdmin.getConn(user.getUser().getOrganizationID()));
	 * data.setSyskey(syskey); userdata.setT1(data.getT1());// mobile number
	 * userdata.setT2(data.getT2());// password userdata.setT3(data.getT14());//
	 * email userdata.setT5(data.getT3());// name l[0] =
	 * RegisterMgr.searchRole(data.getT17(), user);// rolesyskey
	 * userdata.setRolesyskey(l); ret = OPTMgr.saveUserData(userdata, user); if
	 * (ret == "1") { data = setAdRegData(data, user); userdata =
	 * RegisterMgr.readByM(data.getT1(), user);
	 * data.setUsersyskey(userdata.getSyskey()); data.setT17(""); res =
	 * RegisterDao.insertReg(data, conn); if (res.isState()) {
	 * res.setState(true); res.setMsgDesc("Saved Successfully!"); } } else {
	 * res.setState(false); res.setMsgDesc("Mobile Number Already Exist!"); } }
	 * } catch (SQLException e) { e.printStackTrace(); } finally {
	 * ServerUtil.closeConnection(conn); } return res; }
	 */

	public static RegisterDataSet searchArticleLists(PagerData p, String searchVal, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		RegisterDataSet res = new RegisterDataSet();
		try {
			res = RegisterDao.searchArticleLists(p, searchVal, "article", conn);
			RegisterData[] dataArr = res.getData();
			for (RegisterData data : dataArr) {
				if (data.getT5().equals("0")) {
					data.setT5("Male");
				} else {
					data.setT5("Female");
				}

			}
			res.setData(dataArr);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	public static long searchRole(String mobile, MrBean user) {
		long res = 0;
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = RegisterDao.searchByID(mobile, conn);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static RegisterData setAdRegData(RegisterData data, MrBean user) {
		data.setT1(data.getT1());
		data.setT2(data.getT2());
		data.setT3(data.getT3());
		data.setT5(data.getT5());
		data.setT6(data.getT6());
		data.setT7(data.getT8());
		data.setT8(data.getT9());
		data.setT9(data.getT10());
		data.setT10(data.getT11());
		data.setT11(data.getT12());
		data.setT12(data.getT13());
		data.setT13(data.getT14());
		data.setT14(data.getT15());
		data.setT15("");
		return data;
	}

	// web
	public static RegisterData setStatusData(RegisterData data, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (data.getSyskey() == 0) {
			data.setCreateddate(todayDate);
		}
		if (!data.getUserid().equals("") && !data.getUsername().equals("")) {
			data.setUserid(data.getUserid());
			data.setUsername(data.getUsername());
		} else {
			data.setUserid(user.getUser().getUserId());
			data.setUsername(user.getUser().getUserName());
		}
		data.setCreateddate(data.getCreateddate());
		data.setModifieddate(todayDate);
		if (ServerUtil.isZawgyiEncoded(data.getT3())) {
			data.setT3(FontChangeUtil.zg2uni(data.getT3()));
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		return data;
	}

	// mobile
	public static RegisterData setStatusDatas(RegisterData data, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (data.getSyskey() == 0) {
			data.setCreateddate(todayDate);
		}
		if (!data.getUserid().equals("") && !data.getUsername().equals("")) {
			data.setUserid(data.getUserid());
			data.setUsername(data.getUsername());
		} else {
			data.setUserid(user.getUser().getUserId());
			data.setUsername(user.getUser().getUserName());
		}
		data.setCreateddate(data.getCreateddate());
		data.setModifieddate(todayDate);

		if (ServerUtil.isZawgyiEncoded(data.getT3())) {
			data.setT3(FontChangeUtil.zg2uni(data.getT3()));
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setT60(data.getT60());

		return data;
	}

	/*
	 * public static Result saveImage(UploadData data, MrBean user) { Result res
	 * = new Result(); Connection conn =
	 * ConnAdmin.getConn(user.getUser().getOrganizationID()); try { if
	 * (data.getSyskey() == 0) { long syskey = SysKeyMgr.getSysKey(1, "syskey",
	 * ConnAdmin.getConn(user.getUser().getOrganizationID()));
	 * data.setSyskey(syskey); boolean flag = false; UploadData obj =
	 * setUploadData(data.getT1(), syskey, user);
	 * obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
	 * ConnAdmin.getConn(user.getUser().getOrganizationID())));
	 * obj.setModifiedTime(data.getModifiedTime());
	 * obj.setCreatedTime(data.getCreatedTime()); flag = UploadDao.insert(obj,
	 * conn); res.setState(flag);
	 * 
	 * } else {
	 * 
	 * UploadDao.deleteByUpdate(data.getSyskey(), conn); boolean flag = false;
	 * 
	 * UploadData obj = setUploadData(data.getT1(), data.getSyskey(), user);
	 * obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
	 * ConnAdmin.getConn(user.getUser().getOrganizationID())));
	 * obj.setModifiedTime(data.getModifiedTime());
	 * obj.setCreatedTime(data.getCreatedTime()); flag = UploadDao.insert(obj,
	 * conn); res.setState(flag); } } catch (SQLException e) {
	 * e.printStackTrace(); } return res; }
	 */
	public static UploadData setUploadData(String url, long articleKey, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		UploadData data = new UploadData();
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(data.getCreatedTime());
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		data.setT1(url);
		data.setN1(articleKey);
		return data;
	}

	// TDA for updateDeviceID
	public static RegisterData updateDeviceID(RegisterData p, MrBean user) throws SQLException {
		RegisterData res = new RegisterData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			p = setStatusData(p, user);
			res = RegisterDao.updateDeviceID(p, conn);
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;

	}

	/*
	 * public static String saveRegData(RegisterData data, MrBean user) { String
	 * res = ""; Connection conn =
	 * ConnAdmin.getConn(user.getUser().getOrganizationID()); try { data =
	 * setStatusDatas(data, user);
	 * //data.setT4(ServerUtil.encryptPIN(data.getT4())); if (data.getSyskey()
	 * == 0) { data.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
	 * ConnAdmin.getConn(user.getUser().getOrganizationID())));
	 * 
	 * res = RegisterDao.save(data, conn);
	 * 
	 * }else{ } } catch (SQLException e) { e.printStackTrace(); }
	 * 
	 * return res; }
	 */
	public static String updateRegData(RegisterData data, MrBean user) {
		String res = "";
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			data = setStatusDatas(data, user);
			res = RegisterDao.updateRegData(data, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static RegisterData updateRegisterDataforotpcode(RegisterData p, MrBean user) throws SQLException {
		RegisterData res = new RegisterData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			p = setStatusData(p, user);
			res = RegisterDao.updateRegisterOTP(p, conn);
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;

	}

}
