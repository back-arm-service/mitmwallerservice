package com.nirvasoft.cms.mgr;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.nirvasoft.cms.shared.WalletTransData;
import com.nirvasoft.cms.shared.WalletTransDataRequest;
import com.nirvasoft.cms.shared.WalletTransDataResponse;
import com.nirvasoft.rp.framework.ServerSession;
import com.nirvasoft.rp.shared.UserDetailReportData;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

public class ExcelFileMgr {
	private static ArrayList<WalletTransData> wlData;

	public static boolean exportUserReport(UserDetailReportData[] dataList, String fileName) {
		boolean ret = true;

		File outputStream = new File(ServerSession.serverPath + "download\\excel\\" + fileName);
		try {
			WritableWorkbook workBook = Workbook.createWorkbook(outputStream);
			WritableSheet sheet = workBook.createSheet("User Report", 0);

			// sheet.getSettings().setDefaultColumnWidth(15);
			// sheet.getSettings().setDefaultRowHeight(24 * 20);

			WritableFont titleFont = new WritableFont(WritableFont.createFont("Myanmar3"), 12, WritableFont.BOLD);
			WritableFont headerFont = new WritableFont(WritableFont.createFont("Myanmar3"), 10, WritableFont.BOLD);
			WritableFont detailFont = new WritableFont(WritableFont.createFont("Myanmar3"), 9);
			WritableCellFormat titleFormat = new WritableCellFormat(titleFont);
			WritableCellFormat headerFormat = new WritableCellFormat(headerFont);
			WritableCellFormat detailFormat = new WritableCellFormat(detailFont);
			WritableCellFormat detailFormatRight = new WritableCellFormat(detailFont);
			titleFormat.setAlignment(Alignment.CENTRE);
			titleFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
			headerFormat.setAlignment(Alignment.CENTRE);
			headerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
			detailFormat.setAlignment(Alignment.LEFT);
			detailFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
			detailFormatRight.setAlignment(Alignment.RIGHT);
			detailFormatRight.setVerticalAlignment(VerticalAlignment.CENTRE);

			int row = 0;
			int col = 0;
			int heightInPoints = 24 * 20;
			sheet.setRowView(0, 50 * 20); // row Height
			sheet.mergeCells(0, 0, 6, 0);
			sheet.addCell(new Label(col, row, "User Report", titleFormat));

			row++;
			sheet.setRowView(row, heightInPoints); // row Height
			sheet.setColumnView(col, 10); // column Width
			sheet.addCell(new Label(col++, row, "No.", headerFormat));
			sheet.setColumnView(col, 20);
			sheet.addCell(new Label(col++, row, "User ID", headerFormat));
			sheet.setColumnView(col, 30);
			sheet.addCell(new Label(col++, row, "User Name", headerFormat));
			sheet.setColumnView(col, 20);
			sheet.addCell(new Label(col++, row, "NRC", headerFormat));
			sheet.setColumnView(col, 20);
			sheet.addCell(new Label(col++, row, "Phone No", headerFormat));
			sheet.setColumnView(col, 20);
			sheet.addCell(new Label(col++, row, "Created Date", headerFormat));
			sheet.setColumnView(col, 20);
			sheet.addCell(new Label(col++, row, "Modified Date", headerFormat));
			sheet.setColumnView(col, 20);
			sheet.addCell(new Label(col++, row, "User Status", headerFormat));

			int srno = 0;
			for (UserDetailReportData data : dataList) {
				row++;
				srno++;
				col = 0;
				sheet.setRowView(row, heightInPoints);
				sheet.addCell(new Label(col++, row, srno + "", detailFormatRight));
				sheet.addCell(new Label(col++, row, data.getUserID(), detailFormat));
				sheet.addCell(new Label(col++, row, data.getUsername(), detailFormat));
				sheet.addCell(new Label(col++, row, data.getNrc(), detailFormat));
				sheet.addCell(new Label(col++, row, data.getPhoneno(), detailFormat));
				sheet.addCell(new Label(col++, row, data.getCreateddate(), detailFormat));
				sheet.addCell(new Label(col++, row, data.getModifieddate(), detailFormat));
				sheet.addCell(new Label(col++, row, data.getUserstatus(), detailFormat));
			}
			workBook.write();
			workBook.close();

		} catch (Exception e) {
			ret = false;
			e.printStackTrace();
		}

		return ret;
	}

	public static boolean exportWalletTopupReport(WalletTransData[] dataList, String fileName) {
 		boolean ret = true;

		File outputStream = new File(ServerSession.serverPath + "download\\excel\\" + fileName);
		try {
			WritableWorkbook workBook = Workbook.createWorkbook(outputStream);
			WritableSheet sheet = workBook.createSheet("Wallet Transfer Report", 0);

			// sheet.getSettings().setDefaultColumnWidth(15);
			// sheet.getSettings().setDefaultRowHeight(24 * 20);

			WritableFont titleFont = new WritableFont(WritableFont.createFont("Myanmar3"), 12, WritableFont.BOLD);
			WritableFont headerFont = new WritableFont(WritableFont.createFont("Myanmar3"), 10, WritableFont.BOLD);
			WritableFont detailFont = new WritableFont(WritableFont.createFont("Myanmar3"), 9);
			WritableCellFormat titleFormat = new WritableCellFormat(titleFont);
			WritableCellFormat headerFormat = new WritableCellFormat(headerFont);
			WritableCellFormat detailFormat = new WritableCellFormat(detailFont);
			WritableCellFormat detailFormatRight = new WritableCellFormat(detailFont);
			titleFormat.setAlignment(Alignment.CENTRE);
			titleFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
			headerFormat.setAlignment(Alignment.CENTRE);
			headerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
			detailFormat.setAlignment(Alignment.LEFT);
			detailFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
			detailFormatRight.setAlignment(Alignment.RIGHT);
			detailFormatRight.setVerticalAlignment(VerticalAlignment.CENTRE);

			int row = 0;
			int col = 0;
			int heightInPoints = 24 * 20;
			sheet.setRowView(0, 50 * 20); // row Height
			sheet.mergeCells(0, 0, 12, 0);
			sheet.addCell(new Label(col, row, "Wallet Transfer Report", titleFormat));

			row++;
			sheet.setRowView(row, heightInPoints); // row Height
			sheet.setColumnView(col, 10); // column Width
			sheet.addCell(new Label(col++, row, "No.", headerFormat));// 1
			sheet.setColumnView(col, 20);
			sheet.addCell(new Label(col++, row, "Transfer ID", headerFormat));// 2
			sheet.setColumnView(col, 30);
			sheet.addCell(new Label(col++, row, "From Name", headerFormat));// 3
			sheet.setColumnView(col, 20);
			sheet.addCell(new Label(col++, row, "To Name", headerFormat));// 4
			sheet.setColumnView(col, 20);
			sheet.addCell(new Label(col++, row, "From Account", headerFormat));// 5
			sheet.setColumnView(col, 20);
			sheet.addCell(new Label(col++, row, "To Account", headerFormat));// 6
			sheet.setColumnView(col, 20);
			sheet.addCell(new Label(col++, row, "Transfer Date/Time", headerFormat));// 7
			sheet.setColumnView(col, 20);
			sheet.addCell(new Label(col++, row, "Amount", headerFormat));// 10
			sheet.setColumnView(col, 20);
			sheet.addCell(new Label(col++, row, "Commission Charges", headerFormat));// 11
			sheet.setColumnView(col, 20);
			//sheet.addCell(new Label(col++, row, "Currency", headerFormat));// 12

			int srno = 0;
			for (WalletTransData data : dataList) {
				row++;
				srno++;
				col = 0;
				sheet.setRowView(row, heightInPoints);
				sheet.addCell(new Label(col++, row, srno + "", detailFormatRight));// 1
				sheet.addCell(new Label(col++, row, data.getTransID(), detailFormat));// 2
				sheet.addCell(new Label(col++, row, data.getT1(), detailFormat));// 3
				sheet.addCell(new Label(col++, row, data.getT2(), detailFormat));// 4
				sheet.addCell(new Label(col++, row, data.getFromAccount(), detailFormat));// 5
				sheet.addCell(new Label(col++, row, data.getToAccount(), detailFormat));// 6
				sheet.addCell(new Label(col++, row, data.getTransDate(), detailFormat));// 7
				//sheet.addCell(new Label(col++, row, data.getMbankingkey(), detailFormat));// 8
				sheet.addCell(new Label(col++, row, data.getAmount(), detailFormatRight));// 9
				sheet.addCell(new Label(col++, row, data.getCommissionCharges(), detailFormatRight));// 10
				//sheet.addCell(new Label(col++, row, data.getCurrencyCode(), detailFormat));// 11
			}
			workBook.write();
			workBook.close();

		} catch (Exception e) {
			ret = false;
			e.printStackTrace();
		}

		return ret;
	}

	public String exportTransactionReport(WalletTransDataRequest data, String path) {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		Date date = new Date();
		String param = (dateFormat.format(date));
		String filepath = "";
		try {

			filepath = "AccountTransReport_" + param + ".xls";
			File outputStream = new File(path + "/AccountTransReport_" + param + ".xls");
			System.out.println(path + "/AccountTransReport_" + param + ".xls");

			WritableWorkbook workBook = Workbook.createWorkbook(outputStream);
			WritableSheet sheet = workBook.createSheet("Account Transaction Report", 0);
			sheet.getSettings().setDefaultColumnWidth(15);
			sheet.getSettings().setDefaultRowHeight(24 * 20);

			WritableFont titleFont = new WritableFont(WritableFont.createFont("Myanmar3"), 14, WritableFont.BOLD);
			WritableFont headerFont = new WritableFont(WritableFont.createFont("Myanmar3"), 9, WritableFont.BOLD);

			WritableCellFormat titleFormat = new WritableCellFormat(titleFont);
			WritableCellFormat headerFormat = new WritableCellFormat(headerFont);

			titleFormat.setAlignment(Alignment.CENTRE);
			titleFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

			headerFormat.setAlignment(Alignment.CENTRE);
			headerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

			WritableFont dataFont1 = new WritableFont(WritableFont.createFont("Myanmar3"), 9);
			WritableCellFormat dataCellFormat1 = new WritableCellFormat(dataFont1);
			dataCellFormat1.setAlignment(Alignment.LEFT);
			dataCellFormat1.setVerticalAlignment(VerticalAlignment.CENTRE);

			WritableCellFormat dataCellFormatNumber = new WritableCellFormat(dataFont1, NumberFormats.FORMAT3);
			dataCellFormatNumber.setAlignment(Alignment.RIGHT);
			dataCellFormatNumber.setVerticalAlignment(VerticalAlignment.CENTRE);

			// WalletTransData data = new WalletTransData();
			wlData = new ReportMgr().getReportData(data);

			int row = 0; // For Excel Format
			int heightInPoints = 20 * 20;
			sheet.setRowView(row, 50 * 20);
			sheet.mergeCells(0, 0, 7, 0); // (start col:,row,last col; row)
			sheet.addCell(new Label(0, row, " Wallet Transfer Report", titleFormat));
			int row1 = 1;
			int i = 0;
			sheet.setRowView(row1, heightInPoints);
			sheet.addCell(new Label(i++, row1, "From Name", headerFormat));
			sheet.addCell(new Label(i++, row1, "To Name", headerFormat));
			sheet.addCell(new Label(i++, row1, "From Account", headerFormat));
			sheet.addCell(new Label(i++, row1, "To Account", headerFormat));
			sheet.addCell(new Label(i++, row1, "Amount", headerFormat));
			sheet.addCell(new Label(i++, row1, "Bank Charges", headerFormat));
			sheet.addCell(new Label(i++, row1, "Transfer ID", headerFormat));
			sheet.addCell(new Label(i++, row1, "Transfered Date", headerFormat));
			sheet.addCell(new Label(i++, row1, "Currency", headerFormat));

			if (wlData != null) {
				for (int j = 0; j < wlData.size(); j++) {
					sheet.addCell(new Label(0, j + 2, wlData.get(j).getT1(), dataCellFormat1));
					sheet.addCell(new Label(1, j + 2, wlData.get(j).getT2(), dataCellFormat1));
					sheet.addCell(new Label(2, j + 2, wlData.get(j).getFromAccount(), dataCellFormat1));
					sheet.addCell(new Label(3, j + 2, wlData.get(j).getToAccount(), dataCellFormat1));
					// 4
					double num4 = 0.00;
					try {
						num4 = Double.parseDouble(wlData.get(j).getAmount().replaceAll(",", ""));
						System.out.println("" + wlData.get(j).getAmount());
						System.out.println("" + Double.parseDouble(wlData.get(j).getAmount().replaceAll(",", "")));
					} catch (Exception e) {
					}
					sheet.addCell(new Number(4, j + 2, num4, dataCellFormatNumber));
					// 5
					double num5 = 0.00;
					try {
						num5 = Double.parseDouble(wlData.get(j).getBankCharges().replaceAll(",", ""));
					} catch (Exception e) {
					}
					sheet.addCell(new Number(5, j + 2, num5, dataCellFormatNumber));
					sheet.addCell(new Label(6, j + 2, wlData.get(j).getTransID(), dataCellFormat1));
					sheet.addCell(new Label(7, j + 2, wlData.get(j).getTransDate(), dataCellFormat1));
					sheet.addCell(new Label(8, j + 2, wlData.get(j).getCurrencyCode(), dataCellFormat1));
					// sheet.addCell(new Label(16, j + 2,
					// FileUtil.changeDateFormatforClient(data.getData()[j].getPlusOneMonth()),
					// dataCellFormat1));
				}
			}
			workBook.write();
			workBook.close();
		} catch (Exception e) {
			System.out.println("Error --> " + e.getMessage());
		}

		return filepath;

	}
	public static boolean exportSettlementReport(WalletTransDataResponse dataList, String fileName) {
 		boolean ret = true;
		File outputStream = new File(ServerSession.serverPath + "download\\excel\\" + fileName);
		try {
			WritableWorkbook workBook = Workbook.createWorkbook(outputStream);
			WritableSheet sheet = workBook.createSheet("Sheet1", 0);

			// sheet.getSettings().setDefaultColumnWidth(15);
			// sheet.getSettings().setDefaultRowHeight(24 * 20);

			WritableFont titleFont = new WritableFont(WritableFont.createFont("Myanmar3"), 12, WritableFont.BOLD);
			WritableFont headerFont = new WritableFont(WritableFont.createFont("Myanmar3"), 10, WritableFont.BOLD);
			WritableFont detailFont = new WritableFont(WritableFont.createFont("Myanmar3"), 9);
			WritableCellFormat titleFormat = new WritableCellFormat(titleFont);
			WritableCellFormat headerFormat = new WritableCellFormat(headerFont);
			WritableCellFormat detailFormat = new WritableCellFormat(detailFont);
			WritableCellFormat detailFormatRight = new WritableCellFormat(detailFont);
			titleFormat.setAlignment(Alignment.CENTRE);
			titleFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
			headerFormat.setAlignment(Alignment.CENTRE);
			headerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
			detailFormat.setAlignment(Alignment.LEFT);
			detailFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
			detailFormatRight.setAlignment(Alignment.RIGHT);
			detailFormatRight.setVerticalAlignment(VerticalAlignment.CENTRE);

			int row = 0;
			int col = 0;
			int heightInPoints = 24 * 20;
			
			if(row!=0)
				row++;
			sheet.setRowView(row, heightInPoints); // row Height
			sheet.setColumnView(col, 10); // column Width
			sheet.addCell(new Label(col++, row, "No.", headerFormat));// 1
			sheet.setColumnView(col, 20);
			sheet.addCell(new Label(col++, row, "From Account", headerFormat));// 2
			sheet.setColumnView(col, 20);
			sheet.addCell(new Label(col++, row, "To Account", headerFormat));// 3
			sheet.setColumnView(col, 20);
			sheet.addCell(new Label(col++, row, "Settlement Date", headerFormat));// 4
			sheet.setColumnView(col, 15);
			sheet.addCell(new Label(col++, row, "Merchant ID", headerFormat));// 5
			sheet.setColumnView(col, 20);
			sheet.addCell(new Label(col++, row, "Merchant Name", headerFormat));// 6
			sheet.setColumnView(col, 15);
			sheet.addCell(new Label(col++, row, "Currency Code", headerFormat));// 7
			sheet.setColumnView(col, 15);
			sheet.addCell(new Label(col++, row, "SettlementAmt", headerFormat));// 8
			sheet.setColumnView(col, 15);
			sheet.addCell(new Label(col++, row, "Type", headerFormat));// 9
			sheet.setColumnView(col, 20);
			sheet.addCell(new Label(col++, row, "Function ID", headerFormat));// 10
			sheet.setColumnView(col, 20);
			sheet.addCell(new Label(col++, row, "Service Fee Payable", headerFormat));// 11
			sheet.setColumnView(col, 20);
			sheet.addCell(new Label(col++, row, "Service Fee Receivable", headerFormat));
			sheet.setColumnView(col, 20);
			sheet.addCell(new Label(col++, row, "Commssion Charges", headerFormat));// 12		

			int srno = 0;
			//for (WalletTransData data : dataList) {
			for(int i=0; i < dataList.getWldata().length; i++){
				row++;
				srno++;
				col = 0;
				sheet.setRowView(row, heightInPoints);
				sheet.addCell(new Number(col++, row, srno, detailFormatRight));// 1
				sheet.addCell(new Label(col++, row, dataList.getWldata()[i].getDrAccNumber(), detailFormat));// 2
				sheet.addCell(new Label(col++, row, dataList.getWldata()[i].getCrAccNumber(), detailFormat));// 3
				sheet.addCell(new Label(col++, row, dataList.getWldata()[i].getSettlementDate(), detailFormat));// 4
				if(dataList.getWldata()[i].getMerchantID() != ""){
					sheet.addCell(new Number(col++, row, Integer.parseInt(dataList.getWldata()[i].getMerchantID()), detailFormatRight));// 5
				}else{
					sheet.addCell(new Number(col++, row, 0, detailFormatRight));// 5
				}
				sheet.addCell(new Label(col++, row, dataList.getWldata()[i].getMerchantName(), detailFormat));// 6
				sheet.addCell(new Label(col++, row, dataList.getWldata()[i].getCurrencyCode(), detailFormat));// 7					
				sheet.addCell(new Number(col++, row, Double.parseDouble(dataList.getWldata()[i].getAmount()), detailFormatRight));// 8				
				sheet.addCell(new Label(col++, row, dataList.getWldata()[i].getTransType(), detailFormat));// 9
				sheet.addCell(new Label(col++, row, dataList.getWldata()[i].getCode(), detailFormat));// 10
				sheet.addCell(new Label(col++, row, "", detailFormatRight));// 11
				sheet.addCell(new Label(col++, row, "", detailFormatRight));// 12
//				sheet.addCell(new Label(col++, row, dataList.getWldata()[i].getCommissionCharges(), detailFormat));
				sheet.addCell(new Number(col++, row, Double.parseDouble(dataList.getWldata()[i].getCommissionCharges()),detailFormatRight));
			}
			workBook.write();
			workBook.close();

		} catch (Exception e) {
			ret = false;
			e.printStackTrace();
		}

		return ret;
	}

}
