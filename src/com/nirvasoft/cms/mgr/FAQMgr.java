package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nirvasoft.cms.dao.FAQDao;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.shared.FAQData;
import com.nirvasoft.cms.shared.FAQListingData;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.database.SysKeyMgr;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.FAQDataset;
import com.nirvasoft.rp.shared.FilterDataset;

public class FAQMgr {

	/* deleteQuestion */
	public static FAQDataset deleteFAQ(Long postsk) {
		FAQDataset res = new FAQDataset();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = FAQDao.delete(postsk, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public static FAQListingData getFAQList(FilterDataset p) {
		FAQListingData res = new FAQListingData();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = FAQDao.getFAQList(p, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static FAQDataset saveFAQMenu(FAQData data) {
		FAQDataset res = new FAQDataset();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			conn.setAutoCommit(false);
			data = setStatusData1(data);
			if (data.getSyskey() == 0) {
				data.setSyskey(SysKeyMgr.getSysKey(1, "syskey", conn));
				res = FAQDao.insertFAQ(data, conn);
			} else {
				res = FAQDao.updateFAQ(data, conn);
			}
			if (res.isState()) {
				res.setKeyResult(data.getSyskey());
				res.getLongResult().add(data.getSyskey());
				conn.commit();
			} else
				conn.rollback();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public static FAQData setStatusData(FAQData data, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setModifiedDate(todayDate);
		}
		data.setModifiedDate(todayDate);
		data.setStatus(1);
		return data;
	}

	public static FAQData setStatusData1(FAQData data) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setModifiedDate(todayDate);
		}
		data.setModifiedDate(todayDate);
		data.setCreatedDate(todayDate);
		data.setStatus(1);
		return data;
	}

	public FAQData getFAQbysyskey(long key) {
		FAQData res = new FAQData();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = FAQDao.getFAQbysyskey(key, conn);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

}
