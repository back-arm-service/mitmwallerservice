package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.cms.dao.ArticleMobileDao;
import com.nirvasoft.cms.dao.VideoMobileDao;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.ResultMobile;

public class VideoMobileMgr {
	/*
	 * public VideoData setStatusData(VideoData data, MrBean user) { String
	 * todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date()); if
	 * (data.getSyskey() == 0) { data.setCreatedDate(todayDate);
	 * data.setCreatedTime(data.getCreatedTime()); }
	 * data.setModifiedDate(todayDate);
	 * data.setModifiedTime(data.getModifiedTime()); if
	 * (!data.getUserId().equals("") && !data.getUserName().equals("")) {
	 * data.setUserId(data.getUserId()); data.setUserName(data.getUserName()); }
	 * else { data.setUserId(user.getUser().getUserId());
	 * data.setUserName(user.getUser().getUserName()); }
	 * data.setRecordStatus(1); data.setSyncStatus(1); data.setSyncBatch(0);
	 * data.setUserSyskey(0); return data; }
	 */

	/*
	 * public ArticleData setStatusDatas(ArticleData data, MrBean user) { String
	 * todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date()); if
	 * (data.getSyskey() == 0) { data.setCreatedDate(todayDate);
	 * data.setCreatedTime(data.getCreatedTime()); }
	 * data.setModifiedDate(todayDate);
	 * data.setModifiedTime(data.getModifiedTime()); if
	 * (!data.getUserId().equals("") && !data.getUserName().equals("")) {
	 * data.setUserId(data.getUserId()); data.setUserName(data.getUserName()); }
	 * else { data.setUserId(user.getUser().getUserId());
	 * data.setUserName(user.getUser().getUserName()); } if
	 * (ServerUtil.isZawgyiEncoded(data.getT2())) {
	 * data.setT2(FontChangeUtil.zg2uni(data.getT2()));
	 * 
	 * } data.setRecordStatus(1); data.setSyncStatus(1); data.setSyncBatch(0);
	 * data.setUserSyskey(0); data.setVideoUpload(data.getVideoUpload()); return
	 * data; }
	 */

	/*
	 * public UploadData setUploadData(String url, long articleKey, MrBean user)
	 * { String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
	 * UploadData data = new UploadData(); if (data.getSyskey() == 0) {
	 * data.setCreatedDate(todayDate);
	 * data.setCreatedTime(data.getCreatedTime()); }
	 * data.setModifiedDate(todayDate);
	 * data.setModifiedTime(data.getModifiedTime());
	 * 
	 * if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
	 * data.setUserId(data.getUserId()); data.setUserName(data.getUserName()); }
	 * else { data.setUserId(user.getUser().getUserId());
	 * data.setUserName(user.getUser().getUserName()); }
	 * data.setRecordStatus(1); data.setSyncStatus(1); data.setSyncBatch(0);
	 * data.setUserSyskey(0); data.setT2(url); data.setN2(articleKey); return
	 * data; }
	 */

	/*
	 * public VideoDataSet searchLike(String type, long key, MrBean user) {
	 * VideoDataSet res = new VideoDataSet(); Connection conn =
	 * ConnAdmin.getConn(user.getUser().getOrganizationID()); try { res =
	 * VideoDao.searchLikeVideo(type, key, conn); } catch (SQLException e) {
	 * e.printStackTrace(); }
	 * 
	 * return res; }
	 */

	// TDA
	/*
	 * public ArticleDataSet saveComments(ArticleData data, MrBean user) {
	 * Result res = new Result(); ArticleDataSet dataSet = new ArticleDataSet();
	 * Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
	 * try { data = ArticleMgr.setStatusData(data, user); if (data.getSyskey()
	 * == 0) { long syskey = SysKeyMgr.getSysKey(1, "syskey",
	 * ConnAdmin.getConn(user.getUser().getOrganizationID()));
	 * data.setSyskey(syskey); res = CommentDao.insert(data, conn); if
	 * (res.isState()) { if (data.getN3() != -1) res =
	 * ArticleDao.updateCommentCountforVideo(data.getN1(), conn); } } else { res
	 * = CommentDao.update(data, conn); if (res.isState()) {
	 * UploadDao.deleteByUpdate(data.getSyskey(), conn); boolean flag = false;
	 * for (int i = 0; i < data.getVideoUpload().length; i++) { UploadData obj =
	 * setUploadData(data.getUpload()[i], data.getSyskey(), user);
	 * obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
	 * ConnAdmin.getConn(user.getUser().getOrganizationID()))); flag =
	 * UploadDao.insert(obj, conn); res.setState(flag); if (!flag) break; } } }
	 * 
	 * } catch (SQLException e) { e.printStackTrace(); } finally {
	 * ServerUtil.closeConnection(conn); } return dataSet; }
	 */

	/*
	 * public ArticleDataSet saveComment(ArticleData data, MrBean user) { Result
	 * res = new Result(); ArticleDataSet dataSet = new ArticleDataSet();
	 * Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
	 * try { data = ArticleMgr.setStatusData(data, user); if (data.getSyskey()
	 * == 0) { long syskey = SysKeyMgr.getSysKey(1, "syskey",
	 * ConnAdmin.getConn(user.getUser().getOrganizationID()));
	 * data.setSyskey(syskey); res = CommentDao.insert(data, conn); if
	 * (res.isState()) { if (data.getN3() != -1) res =
	 * ArticleDao.updateCommentCountforVideo(data.getN1(), conn); } } else { res
	 * = CommentDao.update(data, conn); if (res.isState()) {
	 * UploadDao.deleteByUpdate(data.getSyskey(), conn); boolean flag = false;
	 * for (int i = 0; i < data.getUpload().length; i++) { UploadData obj =
	 * setUploadData(data.getUpload()[i], data.getSyskey(), user);
	 * obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
	 * ConnAdmin.getConn(user.getUser().getOrganizationID()))); flag =
	 * UploadDao.insert(obj, conn); res.setState(flag); if (!flag) break; } } }
	 * if (res.isState()) { dataSet =
	 * CommentDao.searchComment(String.valueOf(data.getN1()), conn); }
	 * 
	 * } catch (SQLException e) { e.printStackTrace(); } finally {
	 * ServerUtil.closeConnection(conn); } return dataSet; }
	 */

	/*
	 * public UploadData setUploadVideoData(String url, String filename, String
	 * imgurl, long articleKey,MrBean user) { String todayDate = new
	 * SimpleDateFormat("yyyyMMdd").format(new Date()); UploadData data = new
	 * UploadData(); if (data.getSyskey() == 0) {
	 * data.setCreatedDate(todayDate);
	 * data.setCreatedTime(data.getCreatedTime()); }
	 * data.setModifiedDate(todayDate);
	 * data.setModifiedTime(data.getModifiedTime()); if
	 * (!data.getUserId().equals("") && !data.getUserName().equals("")) {
	 * data.setUserId(data.getUserId()); data.setUserName(data.getUserName()); }
	 * else { data.setUserId(user.getUser().getUserId());
	 * data.setUserName(user.getUser().getUserName()); }
	 * data.setRecordStatus(1); data.setSyncStatus(1); data.setSyncBatch(0);
	 * data.setUserSyskey(0); data.setT1(filename); data.setT2(url);
	 * data.setN2(articleKey); return data; }
	 */

	// convert canvas to image file for video file (WTZA)
	/*
	 * public Boolean writeToFile(String data, String uploadedFileLocation) {
	 * BASE64Decoder decoder = new BASE64Decoder(); byte[] imgBytes; try {
	 * imgBytes = decoder.decodeBuffer(data); BufferedImage bufImg =
	 * ImageIO.read(new ByteArrayInputStream(imgBytes)); File imgOutFile = new
	 * File(uploadedFileLocation); ImageIO.write(bufImg, "png", imgOutFile);
	 * return true; } catch (IOException e) { e.printStackTrace(); } return
	 * false;
	 * 
	 * }
	 */

	////////////////////////////// Mobile///////////////////////////////////////
	public ResultMobile clickLikeVideo(long id, String userSK, String type, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ResultMobile res = new ResultMobile();
		try {
			res = new VideoMobileDao().updateLikeCountVideo(id, userSK, type, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	public ResultMobile UnlikeVideo(long id, long userSK, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ResultMobile res = new ResultMobile();
		try {
			res = new ArticleMobileDao().UnlikeVideo(id, userSK, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

}
