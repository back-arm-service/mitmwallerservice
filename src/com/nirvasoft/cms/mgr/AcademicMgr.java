package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nirvasoft.cms.dao.AcademicDao;
import com.nirvasoft.cms.dao.CropDao;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.AcademicData;
import com.nirvasoft.cms.shared.AcademicDataSet;
import com.nirvasoft.cms.shared.AdvancedSearchData;
import com.nirvasoft.cms.util.ServerUtil;

public class AcademicMgr {

	public static Resultb2b deletAcademic(Long key, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = AcademicDao.delete(key, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static AcademicData initData(AcademicData data, MrBean user) {

		String date = new SimpleDateFormat("yyyyMMdd").format(new Date());

		if (data.getSyskey() == 0) {

			data.setCreatedDate(date);
		} else {
			data.setCreatedDate(date);

		}

		data.setUserId(data.getT1());
		data.setUserName("User");
		data.setModifiedDate(date);
		data.setRecordStatus(1);
		data.setSyncBatch(0);
		data.setSyncStatus(1);

		return data;
	}

	public static AcademicData readDataBySyskey(long key, MrBean user) {

		AcademicData res = new AcademicData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = AcademicDao.read(key, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	public static Resultb2b saveAcademic(AcademicData data, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			if (data.getSyskey() == 0) {
				data.setSyskey(Long.parseLong(CropDao.getSyskey("ACADEMICREFERENCE", conn)));
				data.setT1(AcademicDao.getAutoGenCode(conn));
				data.setN1(Long.parseLong(AcademicDao.getSerial(conn)));
				data = initData(data, user);
				res = AcademicDao.insert(data, conn);
			} else {
				res = AcademicDao.update(data, conn);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public static AcademicDataSet searchAcademic(AdvancedSearchData p, String searchVal, String sort, String type,
			MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		AcademicDataSet res = new AcademicDataSet();
		try {
			res = AcademicDao.search(p, searchVal, sort, type, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

}
