package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.cms.dao.ArticleMobileDao;
import com.nirvasoft.cms.dao.ContentMenuDao;
import com.nirvasoft.cms.dao.EduMobileDao;
import com.nirvasoft.cms.dao.UploadMobileDao;
import com.nirvasoft.cms.dao.OPTDao;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.ResultMobile;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.shared.ArticleDataSet;

public class EduMobileMgr {

	/*
	 * public ArticleJunData initJUNData(ArticleJunData typeData, MrBean user) {
	 * String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
	 * if (typeData.getSyskey() == 0) { typeData.setCreateddate(todayDate); }
	 * typeData.setModifieddate(todayDate); if (!typeData.getUserid().equals("")
	 * && !typeData.getUsername().equals("")) {
	 * typeData.setUserid(typeData.getUserid());
	 * typeData.setUsername(typeData.getUsername()); } else {
	 * typeData.setUserid(user.getUser().getUserId());
	 * typeData.setUsername(user.getUser().getUserName()); }
	 * typeData.setRecordStatus(1); typeData.setSyncStatus(1);
	 * typeData.setSyncBatch(0); typeData.setUsersyskey(0); return typeData; }
	 */

	/*
	 * public ArticleData setStatusData(ArticleData data, MrBean user) { String
	 * todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date()); if
	 * (data.getSyskey() == 0) { data.setCreatedDate(todayDate);
	 * data.setCreatedTime(data.getCreatedTime()); }
	 * data.setModifiedDate(todayDate);
	 * data.setModifiedTime(data.getModifiedTime()); if
	 * (!data.getUserId().equals("") && !data.getUserName().equals("")) {
	 * data.setUserId(data.getUserId()); data.setUserName(data.getUserName()); }
	 * else { data.setUserId(user.getUser().getUserId());
	 * data.setUserName(user.getUser().getUserName()); } if
	 * (ServerUtil.isZawgyiEncoded(data.getT2())) {
	 * data.setT2(FontChangeUtil.zg2uni(data.getT2())); }
	 * data.setRecordStatus(1); data.setSyncStatus(1); data.setSyncBatch(0);
	 * data.setUserSyskey(0); return data; }
	 */

	/*
	 * public UploadData setUploadData(String url, long articleKey, MrBean user)
	 * { String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
	 * UploadData data = new UploadData(); if (data.getSyskey() == 0) {
	 * data.setCreatedDate(todayDate);
	 * data.setCreatedTime(data.getCreatedTime()); }
	 * data.setModifiedDate(todayDate);
	 * data.setModifiedTime(data.getModifiedTime()); if
	 * (!data.getUserId().equals("") && !data.getUserName().equals("")) {
	 * data.setUserId(data.getUserId()); data.setUserName(data.getUserName()); }
	 * else { data.setUserId(user.getUser().getUserId());
	 * data.setUserName(user.getUser().getUserName()); }
	 * data.setRecordStatus(1); data.setSyncStatus(1); data.setSyncBatch(0);
	 * data.setUserSyskey(0); data.setT1(url); data.setN1(articleKey); return
	 * data; }
	 */

	/*
	 * public UploadData setUploadVideoData(String url, long articleKey, MrBean
	 * user) { String todayDate = new SimpleDateFormat("yyyyMMdd").format(new
	 * Date()); UploadData data = new UploadData(); if (data.getSyskey() == 0) {
	 * data.setCreatedDate(todayDate);
	 * data.setCreatedTime(data.getCreatedTime()); }
	 * data.setModifiedDate(todayDate);
	 * data.setModifiedTime(data.getModifiedTime()); if
	 * (!data.getUserId().equals("") && !data.getUserName().equals("")) {
	 * data.setUserId(data.getUserId()); data.setUserName(data.getUserName()); }
	 * else { data.setUserId(user.getUser().getUserId());
	 * data.setUserName(user.getUser().getUserName()); }
	 * data.setRecordStatus(1); data.setSyncStatus(1); data.setSyncBatch(0);
	 * data.setUserSyskey(0); data.setT2(url); data.setN2(articleKey); return
	 * data; }
	 */

	/*
	 * public ArticleDataSet searchEdu(PagerData p, String searchVal, MrBean
	 * user) { Connection conn =
	 * ConnAdmin.getConn(user.getUser().getOrganizationID()); ArticleDataSet res
	 * = new ArticleDataSet(); try { res = EduDao.searchArticle(p, searchVal,
	 * "article", conn); ArticleData[] dataArr = res.getData(); for (ArticleData
	 * data : dataArr) {
	 * data.setUpload(UploadDao.search(String.valueOf(data.getSyskey()),"",
	 * conn).getUploads()); } res.setData(dataArr); } catch (SQLException e) {
	 * e.printStackTrace(); } return res;
	 * 
	 * }
	 */
	/*
	 * public ArticleDataSet searchEduLists(PagerData p,String croptype, MrBean
	 * user) { Connection conn =
	 * ConnAdmin.getConn(user.getUser().getOrganizationID()); ArticleDataSet res
	 * = new ArticleDataSet(); try { res = EduDao.searchArticleLists(p,
	 * "article",croptype, conn); ArticleData[] dataArr = res.getData(); for
	 * (ArticleData data : dataArr) {
	 * data.setModifiedDate(ArticleDao.ddMMyyyFormat(data.getModifiedDate()));
	 * data.setUpload(UploadDao.search(String.valueOf(data.getSyskey()),"",
	 * conn).getUploads()); } res.setData(dataArr); } catch (SQLException e) {
	 * e.printStackTrace(); } return res;
	 * 
	 * }
	 */

	/*
	 * public ArticleDataSet searchVideoLists(PagerData p, String searchVal,
	 * MrBean user) { Connection conn =
	 * ConnAdmin.getConn(user.getUser().getOrganizationID()); ArticleDataSet res
	 * = new ArticleDataSet(); try { res = EduDao.searchVideoLists(p, searchVal,
	 * "Video", conn); ArticleData[] dataArr = res.getData(); for (ArticleData
	 * data : dataArr) {
	 * data.setModifiedDate(ArticleDao.ddMMyyyFormat(data.getModifiedDate()));
	 * data.setVideoUpload(UploadDao.searchVideoList(String.valueOf(data.
	 * getSyskey()), conn).getVideoUpload()); } res.setData(dataArr); } catch
	 * (SQLException e) { e.printStackTrace(); } return res;
	 * 
	 * }
	 */

	/*
	 * public Result clickUnlikeArticle(long id, MrBean user) { Connection conn
	 * = ConnAdmin.getConn(user.getUser().getOrganizationID()); Result res = new
	 * Result(); try { res = EduDao.updateUnlikeCount(id, conn); } catch
	 * (SQLException e) { e.printStackTrace(); } return res; }
	 */

	/*
	 * public ArticleDataSet viewArticle(String searchVal, MrBean user) {
	 * Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
	 * ArticleDataSet res = new ArticleDataSet(); try { long userKey =
	 * OPTDao.searchByID(user.getUser().getUserId(), conn); res =
	 * EduDao.viewArticle(searchVal, conn); ArticleData[] dataArr =
	 * res.getData(); for (ArticleData data : dataArr) {
	 * data.setUpload(UploadDao.search(String.valueOf(data.getSyskey()),"",
	 * conn).getUploads()); data.setN6(EduDao.searchLikeUser(data.getSyskey(),
	 * conn, user)); data.setN7(userKey); } res.setData(dataArr); } catch
	 * (SQLException e) { e.printStackTrace(); } return res;
	 * 
	 * }
	 */

	// kndz
	/*
	 * public UploadData setPhotoUploadData(PhotoUploadData d, long articleKey,
	 * MrBean user) { String todayDate = new
	 * SimpleDateFormat("yyyyMMdd").format(new Date()); UploadData data = new
	 * UploadData(); if (data.getSyskey() == 0) {
	 * data.setCreatedDate(todayDate);
	 * data.setCreatedTime(data.getCreatedTime()); }
	 * data.setModifiedDate(todayDate);
	 * data.setModifiedTime(data.getModifiedTime()); if
	 * (!data.getUserId().equals("") && !data.getUserName().equals("")) {
	 * data.setUserId(data.getUserId()); data.setUserName(data.getUserName()); }
	 * else { data.setUserId(user.getUser().getUserId());
	 * data.setUserName(user.getUser().getUserName()); }
	 * data.setRecordStatus(1); data.setSyncStatus(1); data.setSyncBatch(0);
	 * data.setUserSyskey(0); data.setT1(d.getName());
	 * data.setT3(String.valueOf(d.getSerial())); data.setT4(d.getDesc());
	 * data.setN1(articleKey); return data; }
	 */
	/*
	 * public ArticleDataSet viewEditorArticle(String searchVal, MrBean user) {
	 * Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
	 * ArticleDataSet res = new ArticleDataSet(); try { long userKey =
	 * OPTDao.searchByID(user.getUser().getUserId(), conn); res =
	 * EduDao.viewArticle(searchVal, conn); ArticleData[] dataArr =
	 * res.getData(); for (ArticleData data : dataArr) {
	 * data.setN6(EduDao.searchLikeUser(data.getSyskey(), conn, user));
	 * data.setN7(userKey); } res.setData(dataArr); } catch (SQLException e) {
	 * e.printStackTrace(); } return res; }
	 */
	/////////////////////////////////////////////////////// MObile//////////////////////////////////////////

	/* click dilike edu */
	public ResultMobile clickDisLikeEdu(long id, String userSK, String type, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ResultMobile res = new ResultMobile();
		try {
			res = new EduMobileDao().updateDisLikeCountArticle(id, userSK, type, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	/* click like edu */
	public ResultMobile clickLikeEdu(long id, String userSK, String type, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ResultMobile res = new ResultMobile();
		try {
			res = new EduMobileDao().updateLikeCountArticle(id, userSK, type, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	/* click undilike edu */
	public ResultMobile UnDislikeEdu(long id, long userSK, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ResultMobile res = new ResultMobile();
		try {
			res = new EduMobileDao().UnDislikeArticle(id, userSK, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	/* click unlike edu */
	public ResultMobile UnlikeEdu(long id, long userSK, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ResultMobile res = new ResultMobile();
		try {
			res = new EduMobileDao().UnlikeArticle(id, userSK, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	/* save contetn detail education */
	public ArticleDataSet viewEdu(String searchVal, String userSK, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleDataSet res = new ArticleDataSet();
		try {
			long postkey = OPTDao.searchByPostID(searchVal, userSK, conn);
			res = new ArticleMobileDao().viewByKey(searchVal, postkey, "education", conn);
			ArticleData[] dataArr = res.getData();
			for (ArticleData data : dataArr) {
				data.setUploadedPhoto(new UploadMobileDao().search(String.valueOf(data.getSyskey()), "", conn).getData());
				data.setN6(new ArticleMobileDao().searchLikesUser(data.getSyskey(), Long.parseLong(userSK), conn, user));
				data.setN7(new ContentMenuDao().searchSaveContentOrNot(userSK, data.getSyskey(), conn));
				data.setN9(new ArticleMobileDao().searchDisLikeOrNot(data.getSyskey(), userSK, conn));
			}
			res.setData(dataArr);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

}
