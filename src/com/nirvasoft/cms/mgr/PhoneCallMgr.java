package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nirvasoft.cms.dao.AppHistoryDao;
import com.nirvasoft.cms.dao.ArticleDao;
import com.nirvasoft.cms.dao.PhoneCallDao;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.AppHistoryData;
import com.nirvasoft.cms.shared.AppHistoryDataSet;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.cms.shared.PhoneCallData;
import com.nirvasoft.cms.shared.PhoneCallDataSet;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.database.SysKeyMgr;

public class PhoneCallMgr {
	public static AppHistoryDataSet readData(String type, MrBean user) throws SQLException {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		AppHistoryDataSet res = new AppHistoryDataSet();
		res = AppHistoryDao.readData(type, conn);
		AppHistoryData[] dataArr = res.getData();
		for (AppHistoryData data : dataArr) {
			data.setT5(ArticleDao.ddMMyyyFormat(data.getT5()));

		}
		return res;

	}

	public static AppHistoryDataSet readDataByT7(String syskey, String ph, String sdate, String edate, MrBean user)
			throws SQLException {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		AppHistoryDataSet res = new AppHistoryDataSet();
		res = AppHistoryDao.readDataByT7(syskey, ph, sdate, edate, conn);
		AppHistoryData[] dataArr = res.getData();
		for (AppHistoryData data : dataArr) {
			data.setT7(ArticleDao.ddMMyyyFormat(data.getT7()));

		}
		return res;

	}

	public static AppHistoryDataSet readDataByTime(String syskey, String ph, String sdate, String edate, MrBean user)
			throws SQLException {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		AppHistoryDataSet res = new AppHistoryDataSet();
		res = AppHistoryDao.readDataByTime(syskey, ph, sdate, edate, conn);
		AppHistoryData[] dataArr = res.getData();
		for (AppHistoryData data : dataArr) {
			data.setT7(ArticleDao.ddMMyyyFormat(data.getT7()));

		}
		return res;

	}

	public static Resultb2b saveAppHistory(AppHistoryData data, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		long maxbatch = 0;
		long existKey = 0;
		long serialNo = 0;
		try {
			data = setStatusData(data, user);
			String t5 = data.getT5();
			String t6 = data.getT6();
			if (data.getSyskey() == 0) {
				long syskey = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
				data.setSyskey(syskey);
				existKey = AppHistoryDao.ExistUser(data, conn);
				if (existKey == 0) {

					maxbatch = 1;
					data.setN2(maxbatch);

					res = AppHistoryDao.insertApp(data, conn);// save Fmr014
					if (res.isState()) {
						long skey = SysKeyMgr.getSysKey(1, "syskey",
								ConnAdmin.getConn(user.getUser().getOrganizationID()));
						data.setSyskey(skey);
						data.setT9(data.getT7());
						data.setT10(data.getT8());// out time
						data.setT7(data.getT5());
						data.setT8(data.getT6());// last in time
						data.setT5(data.getT3());
						data.setT6(data.getT4());
						if (data.getT9().equals("") && data.getT10().equals("")) {
							data.setT3("Login");
							data.setT4("0");
						} else {
							data.setT3("Logout");
							data.setT4("-1");
						}
						data.setN3(syskey);// fmr14 syskey
						serialNo = AppHistoryDao.searchSerial(data, conn);
						data.setN4(serialNo);
						res = AppHistoryDao.insertAppHistoryView(data, conn);// save
																				// fmr015
					}

				} else {
					maxbatch = AppHistoryDao.searchCounts(data, conn) + 1;// n2
																			// of
																			// FMR014
					data.setT5(t5);
					data.setT6(t6);
					data.setN2(maxbatch);
					res = AppHistoryDao.updateLogin(data, conn);
					if (res.isState()) {
						data.setT9(data.getT7());
						data.setT10(data.getT8());
						data.setT7(t5);
						data.setT8(t6);
						data.setT5(data.getT3());
						data.setT6(data.getT4());
						data.setT3("Login");
						data.setT4("0");
						data.setN3(existKey);
						if (data.getT9().equals("") && data.getT10().equals("")) {// login

							long skey = SysKeyMgr.getSysKey(1, "syskey",
									ConnAdmin.getConn(user.getUser().getOrganizationID()));
							long batch = 1;
							data.setSyskey(skey);
							data.setN2(batch);
							serialNo = AppHistoryDao.searchSerial(data, conn);
							data.setN4(serialNo);
							res = AppHistoryDao.insertAppHistoryView(data, conn);// frm015
						} else {// logout
							long n3 = AppHistoryDao.isExistsysKey(data, conn);
							data.setN3(n3);
							serialNo = AppHistoryDao.searchSerial(data, conn);
							data.setN4(serialNo);
							long num = serialNo - 1;
							res = AppHistoryDao.updateLogoutTime(data, num, conn);// frm015
						}
					}
				}

			} else {

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public static Resultb2b saveTypeHistory(AppHistoryData data, String hour, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		long maxbatch = 0;
		long serialNo = 0;
		long num = 0;
		try {
			data = setStatusDatas(data, user);
			if (data.getSyskey() == 0) {
				long syskey = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
				data.setSyskey(syskey);
				maxbatch = 1;
				data.setN2(maxbatch);
				serialNo = AppHistoryDao.searchSerial(data, conn);
				data.setN4(serialNo);
				num = serialNo - 1;
				res = AppHistoryDao.insertTypeHistory(data, hour, num, conn);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static AppHistoryDataSet searchAppAnalysisList(PagerData p, String searchVal, String date, String status,
			String frmName, MrBean user) throws SQLException {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		AppHistoryDataSet res = new AppHistoryDataSet();
		res = AppHistoryDao.searchAppAnalysisList(p, searchVal, date, status, frmName, conn);
		AppHistoryData[] dataArr = res.getData();
		for (AppHistoryData data : dataArr) {
			data.setT7(ArticleDao.ddMMyyyFormat(data.getT7()));

		}
		return res;
	}

	// TDA
	public static AppHistoryDataSet searchApplicationHistoryViewList(PagerData p, String searchVal, String date,
			String status, MrBean user) throws SQLException {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		AppHistoryDataSet res = new AppHistoryDataSet();
		res = AppHistoryDao.searchApplicationHistoryViewList(p, searchVal, date, status, conn);
		AppHistoryData[] dataArr = res.getData();
		for (AppHistoryData data : dataArr) {
			data.setT5(ArticleDao.ddMMyyyFormat(data.getT5()));
		}
		return res;
	}

	public static AppHistoryDataSet searchAppList(PagerData p, String sdate, String edate, String searchVal,
			MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		AppHistoryDataSet res = new AppHistoryDataSet();
		try {
			res = AppHistoryDao.searchAppUserList(p, sdate, edate, searchVal, conn);
			AppHistoryData[] dataArr = res.getData();
			for (AppHistoryData data : dataArr) {
				data.setT3(ArticleDao.ddMMyyyFormat(data.getT3()));
				data.setT5(ArticleDao.ddMMyyyFormat(data.getT5()));
			}
			res.setData(dataArr);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	public static PhoneCallDataSet searchPhoneCallLists(PagerData p, String searchVal, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		PhoneCallDataSet res = new PhoneCallDataSet();
		try {
			res = PhoneCallDao.searchPhoneCallLists(p, searchVal, "article", conn);
			PhoneCallData[] dataArr = res.getData();
			for (PhoneCallData data : dataArr) {
				data.setT4(ArticleDao.ddMMyyyFormat(data.getT4()));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	public static AppHistoryDataSet searchUserAnalysisList(PagerData p, String searchVal, String date, String status,
			String frmName, MrBean user) throws SQLException {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		AppHistoryDataSet res = new AppHistoryDataSet();
		res = AppHistoryDao.searchAppAnalysisList(p, searchVal, date, status, frmName, conn);
		AppHistoryData[] dataArr = res.getData();
		for (AppHistoryData data : dataArr) {
			data.setT5(ArticleDao.ddMMyyyFormat(data.getT5()));
			data.setT7(ArticleDao.ddMMyyyFormat(data.getT7()));
		}
		return res;
	}

	public static PhoneCallData setData(PhoneCallData data, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
		}
		data.setModifiedDate(todayDate);

		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		return data;
	}

	public static AppHistoryData setDatas(AppHistoryData data, MrBean user) {
		long maxbatch = 0;
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
		}
		data.setModifiedDate(todayDate);
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		maxbatch = 1;
		if (data.getT4().equals("1")) {
			data.setT3("Weather");
			data.setT4(data.getT4());
			data.setN4(maxbatch);
		}
		if (data.getT4().equals("2")) {
			data.setT3("Price");
			data.setT4(data.getT4());
			data.setN5(maxbatch);
		}
		if (data.getT4().equals("3")) {
			data.setT3("News & Media");
			data.setT4(data.getT4());
			data.setN6(maxbatch);
		}
		if (data.getT4().equals("4")) {
			data.setT3("article");
			data.setT4(data.getT4());
			data.setN7(maxbatch);
		}
		if (data.getT4().equals("5")) {
			data.setT3("question");
			data.setT4(data.getT4());
			data.setN8(maxbatch);
		}
		if (data.getT4().equals("6")) {
			data.setT3("Call Center");
			data.setT4(data.getT4());
			data.setN9(maxbatch);
		}
		if (data.getT4().equals("7")) {
			data.setT3("Save Content");
			data.setT4(data.getT4());
			data.setN10(maxbatch);
		}
		if (data.getT4().equals("8")) {
			data.setT3("Profile");
			data.setT4(data.getT4());
			data.setN11(maxbatch);
		}
		if (data.getT4().equals("9")) {
			data.setT3("Video");
			data.setT4(data.getT4());
			data.setN12(maxbatch);
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		return data;
	}

	public static AppHistoryData setStatusData(AppHistoryData data, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
		}
		data.setModifiedDate(todayDate);
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		return data;
	}

	public static AppHistoryData setStatusDatas(AppHistoryData data, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
		}

		data.setModifiedDate(todayDate);
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		if (data.getT4().equals("1")) {
			data.setT3("Weather");
			data.setT4(data.getT4());

		}
		if (data.getT4().equals("2")) {
			data.setT3("Price");
			data.setT4(data.getT4());

		}
		if (data.getT4().equals("3")) {
			data.setT3("News & Media");
			data.setT4(data.getT4());

		}
		if (data.getT4().equals("4")) {
			data.setT3("article");
			data.setT4(data.getT4());

		}
		if (data.getT4().equals("5")) {
			data.setT3("question");
			data.setT4(data.getT4());

		}
		if (data.getT4().equals("6")) {
			data.setT3("Call Center");
			data.setT4(data.getT4());

		}
		if (data.getT4().equals("7")) {
			data.setT3("Save Content");
			data.setT4(data.getT4());

		}
		if (data.getT4().equals("8")) {
			data.setT3("Video");
			data.setT4(data.getT4());

		}
		if (data.getT4().equals("9")) {
			data.setT3("Profile");
			data.setT4(data.getT4());

		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		return data;
	}

	public static PhoneCallDataSet viewPhCallList(String searchVal, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		PhoneCallDataSet res = new PhoneCallDataSet();
		try {
			res = PhoneCallDao.view(searchVal, conn);
			PhoneCallData[] dataArr = res.getData();
			for (PhoneCallData data : dataArr) {
				data.setT5(ArticleDao.ddMMyyyFormat(data.getT5()));
			}
			res.setData(dataArr);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}
}
