package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.nirvasoft.cms.dao.PriceDao;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.DivisionComboDataSet;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.cms.shared.PriceData;
import com.nirvasoft.cms.shared.PriceDataSet;
import com.nirvasoft.cms.shared.PriceListDataSet;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.database.SysKeyMgr;

public class PriceMgr {

	public static Resultb2b deletePrice(Long key, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = PriceDao.delete(key, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static Resultb2b deletePriceBatch(PriceData p, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = PriceDao.deletePriceBatch(p, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	/*
	 * public static Result getImportDataSet(ArrayList<PriceData> mNewList,
	 * MrBean user) { Connection l_Conn =
	 * ConnAdmin.getConn(user.getUser().getOrganizationID());
	 * ArrayList<PriceData> l_ondutyList = new ArrayList<PriceData>(); Result
	 * res = new Result(); long sysKey = 0; int count = 0;
	 * 
	 * long maxbatch = PriceDao.getMaxBatch(l_Conn); for (int i = 0; i <
	 * mNewList.size(); i++) { if (!mNewList.get(i).isContainerror()) {
	 * l_ondutyList.add(mNewList.get(i)); } } if (l_ondutyList.size() > 0) { try
	 * { for (PriceData data : l_ondutyList) { PriceData l_info = new
	 * PriceData(); if (!data.isContainerror()) { sysKey = data.getSyskey(); if
	 * (sysKey == 0) { l_info.setT1(data.getT1());
	 * l_info.setT3(ServerUtil.changeMyanNumber(data.getT3()));
	 * l_info.setT2(data.getT2()); l_info.setT4(data.getT4());
	 * l_info.setT5(data.getT5()); l_info.setT6(data.getT6());
	 * l_info.setT7(data.getT7()); l_info.setT8(data.getT8());
	 * l_info.setT9(data.getT9()); l_info.setT10(data.getT10());
	 * l_info.setN1(maxbatch); l_info.setSyskey(SysKeyMgr.getSysKey(1,
	 * "syskey",ConnAdmin.getConn(user.getUser().getOrganizationID())));
	 * getObjectData(l_info, user); res = PriceDao.readDataExit(l_info, l_Conn);
	 * if (res.isState()) {
	 * l_info.setT3(ServerUtil.changeMyanNumber(data.getT3()));
	 * l_info.setSyskey(res.getKeyResult()); res = PriceDao.update(l_info,
	 * l_Conn);
	 * 
	 * } else { l_info.setT3(ServerUtil.changeMyanNumber(data.getT3())); res =
	 * PriceDao.insert(l_info, l_Conn); count++; } } if (!res.isState()) {
	 * res.setMsgDesc(res.getMsgCode()); break; } } }
	 * 
	 * } catch (Exception e) { res.setMsgDesc(res.getMsgCode());
	 * e.printStackTrace();
	 * 
	 * } finally { ServerUtil.closeConnection(l_Conn); } } else {
	 * res.setState(false); res.setMsgCode("No correct data for import!"); }
	 * res.setKeyResult(count); return res; }
	 */

	private static Resultb2b ExitDataPrice(String t1, String t2, String t3, String t10, Connection conn)
			throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = "select syskey,n1 from fmr009 where t1=N'" + t1 + "'and t2=N'" + t2 + "' and t3='" + t3
				+ "' and  T10=N'" + t10 + "'";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			res.setKeyResult(rs.getLong("syskey"));
			res.setN1(rs.getLong("n1"));
			res.setState(true);
		}
		return res;
	}

	public static DivisionComboDataSet getBatchList(MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		DivisionComboDataSet res = new DivisionComboDataSet();
		try {
			res = PriceDao.getBatchList(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static Resultb2b getImportDataSet(ArrayList<PriceData> mNewList, MrBean user) {
		Connection l_Conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		PriceData l_info = new PriceData();
		Resultb2b res = new Resultb2b();
		int count = 0;
		long maxbatch = PriceDao.getMaxBatch(l_Conn);
		try {
			for (int i = 0; i < mNewList.size(); i++) {
				String date = ServerUtil.changeMyanNumber(mNewList.get(i).getT3());
				res = ExitDataPrice(mNewList.get(i).getT1(), mNewList.get(i).getT2(), date, mNewList.get(i).getT10(),
						l_Conn);
				if (!res.isState()) {
					l_info = new PriceData();
					getObjectData(l_info, user);
					l_info.setT1(mNewList.get(i).getT1());
					l_info.setT3(ServerUtil.changeMyanNumber(mNewList.get(i).getT3()));
					l_info.setT2(mNewList.get(i).getT2());
					l_info.setT4(mNewList.get(i).getT4());
					l_info.setT5(mNewList.get(i).getT5());
					l_info.setT6(mNewList.get(i).getT6());
					l_info.setT7(mNewList.get(i).getT7());
					l_info.setT8(mNewList.get(i).getT8());
					l_info.setT9(mNewList.get(i).getT9());
					l_info.setT10(mNewList.get(i).getT10());
					l_info.setN1(maxbatch);
					l_info.setSyskey(
							SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID())));
					res = PriceDao.insert(l_info, l_Conn);
					count++;
				} else {
					l_info = new PriceData();
					getObjectData(l_info, user);
					l_info.setSyskey(res.getKeyResult());
					l_info.setT1(mNewList.get(i).getT1());
					l_info.setT3(ServerUtil.changeMyanNumber(mNewList.get(i).getT3()));
					l_info.setT2(mNewList.get(i).getT2());
					l_info.setT4(mNewList.get(i).getT4());
					l_info.setT5(mNewList.get(i).getT5());
					l_info.setT6(mNewList.get(i).getT6());
					l_info.setT7(mNewList.get(i).getT7());
					l_info.setT8(mNewList.get(i).getT8());
					l_info.setT9(mNewList.get(i).getT9());
					l_info.setT10(mNewList.get(i).getT10());
					l_info.setN1(res.getN1());
					res = PriceDao.update(l_info, l_Conn);
					count++;
				}
			}
		} catch (Exception e) {
			res.setMsgDesc(res.getMsgCode());
			e.printStackTrace();

		} finally {
			ServerUtil.closeConnection(l_Conn);
		}
		res.setKeyResult(count);
		return res;
	}

	/*
	 * public static PriceListDataSet getPriceListData(String state,String Date,
	 * MrBean user) { Connection conn =
	 * ConnAdmin.getConn(user.getUser().getOrganizationID()); PriceListDataSet
	 * res = new PriceListDataSet(); try { res =
	 * PriceDao.getPriceListData(state,Date, conn); } catch (SQLException e) {
	 * e.printStackTrace(); } return res; }
	 */

	public static void getObjectData(PriceData l_info, MrBean user) {
		l_info.setCreateddate(ServerUtil.datetoString());
		l_info.setModifieddate(ServerUtil.datetoString());
		l_info.setUserid(user.getUser().getUserId());
		l_info.setUsername(user.getUser().getUserName());
		l_info.setRecordStatus(1);
		l_info.setSyncStatus(0);
		l_info.setSyncBatch(0);
		l_info.setUsersyskey(user.getUser().getUsersyskey());
	}

	public static PriceListDataSet getPricelists(PagerData p, String searchVal, String type, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		PriceListDataSet res = new PriceListDataSet();
		try {
			res = PriceDao.getPricelists(p, searchVal, type, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
	/*
	 * public static PriceListDataSet getPriceListData(PagerData p, String
	 * searchVal, MrBean user) { Connection conn =
	 * ConnAdmin.getConn(user.getUser().getOrganizationID()); PriceListDataSet
	 * res = new PriceListDataSet(); try { res = PriceDao.getPriceListData(p,
	 * searchVal, conn); } catch (SQLException e) { e.printStackTrace(); }
	 * return res; }
	 */

	public static String[] getSheetNames(HttpServletRequest request, String filename, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArrayList<String> sheetNames = new ArrayList<String>();
		String UPLOAD_DIRECTORY = "";
		int l_ExtIndex = filename.lastIndexOf(".");
		String l_FileExt = filename.substring(l_ExtIndex);
		String appPath = request.getServletContext().getRealPath("");
		String contextPath = request.getContextPath();
		String s[] = appPath.split("\\.");
		UPLOAD_DIRECTORY = request.getRealPath("/") + "\\upload\\other\\";
		if (l_FileExt.equalsIgnoreCase(".xls"))
			sheetNames = PriceDao.getSheetNames(UPLOAD_DIRECTORY + filename, conn);
		String[] dataarry = new String[sheetNames.size()];
		dataarry = sheetNames.toArray(dataarry);
		return dataarry;
	}

	public static PriceDataSet previewExcelDataForPriceList(String mUploadFile, String mSheetNo, int m_ResultSize,
			int p_CurrentPage, MrBean user) {
		PriceDataSet l_GeneralDataSet = new PriceDataSet();
		Connection l_Conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			l_GeneralDataSet = PriceDao.previewExcelDataForPriceList(mUploadFile, mSheetNo, m_ResultSize, p_CurrentPage,
					l_Conn);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return l_GeneralDataSet;
	}

	public static PriceData readData(long key, MrBean user) {
		PriceData res = new PriceData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = PriceDao.read(key, conn);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	public static Resultb2b savePrice(PriceData data, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			conn.setAutoCommit(false);
			data = setStatusData(data, user);

			if (data.getSyskey() != 0) {
				res = PriceDao.update(data, conn);
			} else {
				long syskey = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
				data.setSyskey(syskey);
				long maxbatch = PriceDao.getMaxBatch(conn);
				data.setN1(maxbatch);
				res = PriceDao.insert(data, conn);
			}
			conn.commit();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	/*
	 * public static PriceListDataSet getPriceListDatas(PagerData p, String
	 * searchVal, MrBean user) { Connection conn =
	 * ConnAdmin.getConn(user.getUser().getOrganizationID()); PriceListDataSet
	 * res = new PriceListDataSet(); try { res = PriceDao.getPriceListDatas(p,
	 * searchVal, conn); } catch (SQLException e) { e.printStackTrace(); }
	 * return res; }
	 */

	public static PriceDataSet searchPriceLists(PagerData p, String batchno, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		PriceDataSet res = new PriceDataSet();
		try {
			res = PriceDao.searchPriceLists(p, batchno, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	public static PriceData setStatusData(PriceData data, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (data.getSyskey() == 0) {
			data.setCreateddate(todayDate);
		}

		data.setModifieddate(todayDate);
		if (!data.getUserid().equals("") && !data.getUsername().equals("")) {
			data.setUserid(data.getUserid());
			data.setUsername(data.getUsername());
		} else {
			data.setUserid(user.getUser().getUserId());
			data.setUsername(user.getUser().getUserName());
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUsersyskey(0);
		return data;
	}

}
