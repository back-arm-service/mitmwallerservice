package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.nirvasoft.cms.dao.ReportDao;
import com.nirvasoft.cms.shared.TempData;
import com.nirvasoft.cms.shared.WalletTransData;
import com.nirvasoft.cms.shared.WalletTransDataRequest;
import com.nirvasoft.cms.shared.WalletTransDataResponse;
import com.nirvasoft.rp.core.ccbs.data.db.DBTransactionMgr;
import com.nirvasoft.rp.core.util.StrUtil;
import com.nirvasoft.rp.dao.AccountDao;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.data.FeatureData;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.shared.AccountGLTransactionData;
import com.nirvasoft.rp.shared.DataResult;
import com.nirvasoft.rp.shared.UserDetailReportData;
import com.nirvasoft.rp.shared.UserReportData;
import com.nirvasoft.rp.shared.icbs.TransactionData;
import com.nirvasoft.rp.util.ServerUtil;

public class ReportMgr {
	public WalletTransDataResponse getReport(WalletTransDataRequest request) {
		WalletTransDataResponse response = null;

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			response = new ReportDao().getReport(request, conn);
		} catch (Exception e) {
			response = new WalletTransDataResponse();
			response.setMsgCode("0014");
			response.setMsgDesc("Server Error!");

			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

	public ArrayList<WalletTransData> getReportData(WalletTransDataRequest data) {
		ArrayList<WalletTransData> ret = new ArrayList<WalletTransData>();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			ret = new ReportDao().getReportData(data, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return ret;
	}

	public UserReportData getUserReportList1(UserReportData p,String download) {
//		ArrayList<UserDetailReportData> userdata = new ArrayList<UserDetailReportData>();
		UserReportData userdata=new UserReportData();
//		Result ret = new Result();
		Connection l_Conn = null;
		// String startTime = ServerUtil.getDateTime();
		String action = "";
		try {
			l_Conn = ConnAdmin.getConn("001", "");
			if (l_Conn != null) {
				userdata = new ReportDao().getUserReportList1(p,download, l_Conn);
				userdata.setState(true);
			} else {
				userdata.setState(false);
				userdata.setMsgDesc("Connection Refuse");
			}
		} catch (Exception e) {
			e.printStackTrace();
			userdata.setState(false);
			userdata.setMsgDesc(e.getMessage());
		} finally {
			ServerUtil.closeConnection(l_Conn);
		}
		if(download.equals("1")){
		if (userdata.isState()) {
//			if (userdata.size() != 0) {
				DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
				Date date = new Date();
				String param = (dateFormat.format(date));
				if (p.getFileType().equalsIgnoreCase("PDF")) {
					action = "Print";
					String fileName = "UserReport_" + param + ".pdf";
					// String fileName = "UserReport.pdf";
					boolean res = PDFMgr.printUserReport(userdata.getUsdata(), fileName);
					if (res) {
						userdata.setState(true);
						userdata.getStringResult().add(fileName);
					} else {
						userdata.setState(false);
						userdata.setMsgDesc("Can't write File");
					}
				} else if (p.getFileType().equalsIgnoreCase("EXCEL")) {
					// to write excel file
					action = "Download";
					String fileName = "UserReport_" + param + ".xls";
					// String fileName = "AuditLog_.xls";
					boolean res = ExcelFileMgr.exportUserReport(userdata.getUsdata(), fileName);
					if (res) {
						userdata.setState(true);
						userdata.getStringResult().add(fileName);
					} else {
						userdata.setState(false);
						userdata.setMsgDesc("Can't write File");
					}
				}
//			} else {
//				ret.setState(false);
//				ret.setMsgDesc("No Record Found");
//			}
		}
	}

		/*
		 * if(ret.isState()){ String endTime = ServerUtil.getDateTime();
		 * AuditLogMgr auditMgr = new AuditLogMgr(); AuditLogData auditData =
		 * auditMgr.prepareData("auditlog","Audit Log Report"
		 * ,action,startTime,endTime, "","0000",ret.getMsgDesc(),user);
		 * auditMgr.saveAuditLog(auditData, user); }
		 */
		return userdata;
	}

	public WalletTransDataResponse getWalletTopupList(WalletTransDataRequest p,String download) {
		ArrayList<WalletTransData> userdata = new ArrayList<WalletTransData>();
		
		Result ret = new Result();
		Connection l_Conn = null;
		WalletTransDataResponse response = new WalletTransDataResponse();
		// String startTime = ServerUtil.getDateTime();
		String action = "";
		try {
			l_Conn = ConnAdmin.getConn("001", "");
			if (l_Conn != null) {
				response = new ReportDao().getWalletTopupList(p,download, l_Conn);
				ret.setState(true);
			} else {
				response.setState(false);
				response.setMsgDesc("Connection Refuse");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setState(false);
			response.setMsgDesc(e.getMessage());
		} finally {
			ServerUtil.closeConnection(l_Conn);
		}
		if(download.equals("1")){
//		if (response.isState()) {
			if (response!= null) {
				DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
				Date date = new Date();
				String param = (dateFormat.format(date));
				if (p.getFileType().equalsIgnoreCase("PDF")) {
					action = "Print";
					String fileName = "", transtype = "";
					if (p.getTranstype().equals("4")) {
						fileName = "WalletTopup_" + param + ".pdf";
						transtype = "4";
					} else if (p.getTranstype().equals("1")) {
						fileName = "WalletTransfer_" + param + ".pdf";
						transtype = "1";
					}
					boolean res = PDFMgr.printWalletTopupReport(response.getWldata(), fileName, transtype);
					if (res) {
						response.setState(true);
						response.getStringResult().add(fileName);
					} else {
						response.setState(false);
						response.setMsgDesc("Can't write File");
					}
				} else if (p.getFileType().equalsIgnoreCase("EXCEL")) {
					// to write excel file
					action = "Download";
					String fileName = "", transtype = "";
						fileName = "WalletTopup_" + param + ".xls";
					
					boolean res = ExcelFileMgr.exportWalletTopupReport(response.getWldata(), fileName);
					if (res) {
						response.setState(true);
						response.getStringResult().add(fileName);
					} else {
						response.setState(false);
						response.setMsgDesc("Can't write File");
					}
				}
			} else {
				response.setState(false);
				response.setMsgDesc("No Record Found");
			}
		//}
		
		}

		/*
		 * if(ret.isState()){ String endTime = ServerUtil.getDateTime();
		 * AuditLogMgr auditMgr = new AuditLogMgr(); AuditLogData auditData =
		 * auditMgr.prepareData("auditlog","Audit Log Report"
		 * ,action,startTime,endTime, "","0000",ret.getMsgDesc(),user);
		 * auditMgr.saveAuditLog(auditData, user); }
		 */
		return response;
	}	
	
	public ArrayList<TempData> getWalletTransferReportJasperDownload(WalletTransDataRequest p) {
		ArrayList<TempData> userdata = new ArrayList<TempData>();
		Result ret = new Result();
		Connection l_Conn = null;
		try {
			l_Conn = ConnAdmin.getConn("001", "");
			if (l_Conn != null) {
				userdata = new ReportDao().getWalletTransferReportDownload(p, l_Conn);								
			} else {
				ret.setState(false);
				ret.setMsgDesc("Connection Refuse");
			}
		} catch (Exception e) {
			e.printStackTrace();
			ret.setState(false);
			ret.setMsgDesc(e.getMessage());
		} finally {
			ServerUtil.closeConnection(l_Conn);
		}
		return userdata;
	}
	public WalletTransDataResponse getWalletTransferReportJasper(WalletTransDataRequest p) {
		//ArrayList<TempData> userdata = new ArrayList<TempData>();
		WalletTransDataResponse userdata=new WalletTransDataResponse();
		Result ret = new Result();
		Connection l_Conn = null;
		try {
			l_Conn = ConnAdmin.getConn("001", "");
			if (l_Conn != null) {
				userdata = new ReportDao().getWalletTransferReport(p, l_Conn);								
			} else {
				userdata.setState(false);
				userdata.setMsgDesc("Connection Refuse");
			}
		} catch (Exception e) {
			e.printStackTrace();
			userdata.setState(false);
			userdata.setMsgDesc(e.getMessage());
		} finally {
			ServerUtil.closeConnection(l_Conn);
		}
		return userdata;
	}
	public ArrayList<TempData> getWalletTransferCountReportJasper(WalletTransDataRequest p) {
		ArrayList<TempData> userdata = new ArrayList<TempData>();
		Result ret = new Result();
		Connection l_Conn = null;
		try {
			l_Conn = ConnAdmin.getConn("001", "");
			if (l_Conn != null) {
				userdata = new ReportDao().getWalletTransferCountReport(p, l_Conn);								
			} else {
				ret.setState(false);
				ret.setMsgDesc("Connection Refuse");
			}
		} catch (Exception e) {
			e.printStackTrace();
			ret.setState(false);
			ret.setMsgDesc(e.getMessage());
		} finally {
			ServerUtil.closeConnection(l_Conn);
		}
		return userdata;
	}
	
	public WalletTransDataResponse getSettlementList(WalletTransDataRequest p,String download) {
		WalletTransDataResponse response = new WalletTransDataResponse();
		//ArrayList<WalletTransData> userdata = new ArrayList<WalletTransData>();
		//ArrayList<FeatureData> fdata = new ArrayList<FeatureData>();
		//Result ret = new Result();
		Connection conn2 = null;
		Connection conn1 = null;
		DAOManager daomgr = new DAOManager();
		String action = "";
		DataResult ret = new DataResult();
		boolean isdotrans= false;
		String l_BCode= "";
		AccountDao  l_AccDao  = new AccountDao();
		try {
			conn1 = daomgr.openConnection();
			conn2 = daomgr.openICBSConnection();
			
			if (conn2 != null) {
				//fdata = new ReportDao().getDataByID(conn1);
				
				response = new ReportDao().getSettlementList(p.getFromDate(), conn1,conn2);//,fdata					
				
				response.setState(true);
				l_BCode= l_AccDao.getBranchCode(conn2);
			} else {
				response.setState(false);
				response.setMsgDesc("Connection Refuse");
			}
		
			if(download.equals("1")){			
				if (response.isState()) {
					if (response.getWldata().length != 0 ) {
						/*if(! new ReportDao().checkExportLogs(p.getFromDate(), 1, l_BCode,conn2)){
							conn2.setAutoCommit(false);
							isdotrans = true;
							ret = prepareSettlementTrans(response.getWldata(),p.getUserID(),l_BCode,conn2);
							if(ret.getStatus()) {
								if(new ReportDao().saveExportLogs(p.getFromDate(), 1, l_BCode,
										(int) ret.getTransactionNumber(), conn2)) {
									if (p.getFileType().equalsIgnoreCase("EXCEL")) {
										// to write excel file
										action = "Download";
										String fileName = "";
										fileName = "WLSUM" + param + ".xls";
										boolean res = ExcelFileMgr.exportSettlementReport(response, fileName);
										if (res) {
											response.setState(true);	
											conn2.commit();
											response.getStringResult().add(fileName);
										} else {
											response.setState(false);		
											conn2.rollback();
											response.setMsgDesc("Can't write File");
										}
									}
											
								}else {
									conn2.rollback();
									response.setState(false);
									ret.setStatus(false);
									response.setMsgDesc("Save SettlementLog Fail!");
								}
							}else {
								conn2.rollback();
								response.setState(false);
								ret.setStatus(false);
								if(!ret.getDescription().equals(""))
									response.setMsgDesc(ret.getDescription());
								else
								response.setMsgDesc("Post Transaction Fail!");
							
							}
						}else {
							ret.setStatus(true);
							isdotrans = false;*/
							 if (p.getFileType().equalsIgnoreCase("EXCEL")) {
									// to write excel file
									action = "Download";
									String fileName = "";
									fileName = "WLSUM" + p.getFromDate() + ".xls";
									boolean res = ExcelFileMgr.exportSettlementReport(response, fileName);
									if (res) {
										response.setState(true);										
										response.getStringResult().add(fileName);
									} else {
										response.setState(false);										
										response.setMsgDesc("Can't write File");
									}
								}
						//}						
					} else {
						ret.setStatus(false);
						response.setState(false);
						response.setMsgDesc("No Record Found");
					}					
				}else {
					ret.setStatus(false);
					response.setState(false);
					response.setMsgDesc("No Record Found");
				}	
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setState(false);
			ret.setStatus(false);
			try {
				conn2.rollback();
			} catch (SQLException e1) {				
				e1.printStackTrace();
			}
			response.setMsgDesc(e.getMessage());			
		} finally {			
			ServerUtil.closeConnection(conn2);
			ServerUtil.closeConnection(conn1);
		}
		
		return response;
	}
	
	private DataResult prepareSettlementTrans(WalletTransData[] wallettrans,String puserID, String pBCode,String fromDate,Connection pConn) throws Exception {
		DataResult ret = new DataResult();
		com.nirvasoft.rp.shared.TransactionData l_TransData = new com.nirvasoft.rp.shared.TransactionData();
		ArrayList<com.nirvasoft.rp.shared.TransactionData> lstTransData = new ArrayList<com.nirvasoft.rp.shared.TransactionData>();
		ArrayList<AccountGLTransactionData> lstAccGLTransData = new ArrayList<AccountGLTransactionData>();
		AccountDao l_AccDao = new AccountDao();
	
		String l_CashInHand= l_AccDao.getCashGL(pConn);
		String l_TodayDate = StrUtil.formatDDMMYYYY2MIT(DBTransactionMgr.getCurrentDate());
		//String l_EffDate = DBTransactionMgr.getEffectiveTransDate
				//(l_TodayDate, pConn);
		String l_EffDate = DBTransactionMgr.getEffectiveTransDate
				(fromDate, pConn);
		if(wallettrans.length>0) {
			for(int i=0; i< wallettrans.length;i++){
				//Dr Account
				l_TransData = new com.nirvasoft.rp.shared.TransactionData();				
				l_TransData.setAccountNumber(wallettrans[i].getDrAccNumber());
				l_TransData.setAmount(Double.parseDouble(wallettrans[i].getAmount()));
				l_TransData.setDescription(wallettrans[i].getCrAccNumber()+ 
						", Settlement Nil Processing, "+wallettrans[i].getCurrencyCode()+", "+pBCode );
				l_TransData.setIsMobile(true);
				l_TransData.setReferenceNumber("");
				l_TransData.setCurrencyCode(wallettrans[i].getCurrencyCode());
				l_TransData.setProductGL("");
				l_TransData.setBranchCode(pBCode);
				l_TransData.setCashInHandGL(l_CashInHand);
				l_TransData.setPreviousBalance(0);
				l_TransData.setPreviousDate("19000101");
				l_TransData.setWorkstation("Mobile");
				l_TransData.setUserID(puserID);
				l_TransData.setAuthorizerID("Mobile");
				l_TransData.setCurrencyRate(1);
				l_TransData.setAmount(Double.parseDouble(wallettrans[i].getAmount()));
				l_TransData.setTransactionDate(l_TodayDate);
				l_TransData.setEffectiveDate(l_EffDate);
				l_TransData.setContraDate("19000101");
				l_TransData.setStatus(1);
				l_TransData.setRemark("Wallet Settlement");
				l_TransData.setTransactionType(705);
				l_TransData.setSubRef("");
				l_TransData.setAccRef("");
				lstTransData.add(l_TransData);
				
				//Cr Account
				l_TransData = new com.nirvasoft.rp.shared.TransactionData();
				l_TransData.setAccountNumber(wallettrans[i].getCrAccNumber());
				l_TransData.setAmount(Double.parseDouble(wallettrans[i].getAmount()));
				l_TransData.setDescription(wallettrans[i].getDrAccNumber()+ 
						", Settlement Nil Processing, "+wallettrans[i].getCurrencyCode()+", "+pBCode );
				l_TransData.setIsMobile(true);
				l_TransData.setReferenceNumber("");
				l_TransData.setCurrencyCode(wallettrans[i].getCurrencyCode());
				l_TransData.setProductGL("");
				l_TransData.setBranchCode(pBCode);
				l_TransData.setCashInHandGL(l_CashInHand);
				l_TransData.setPreviousBalance(0);
				l_TransData.setPreviousDate("19000101");
				l_TransData.setWorkstation("Mobile");
				l_TransData.setUserID(puserID);
				l_TransData.setAuthorizerID("Mobile");
				l_TransData.setCurrencyRate(1);
				l_TransData.setAmount(Double.parseDouble(wallettrans[i].getAmount()));
				l_TransData.setTransactionDate(l_TodayDate);
				l_TransData.setEffectiveDate(l_EffDate);
				l_TransData.setContraDate("19000101");
				l_TransData.setStatus(1);
				l_TransData.setRemark("Wallet Settlement");
				l_TransData.setTransactionType(205);
				l_TransData.setSubRef("");
				l_TransData.setAccRef("");
				lstTransData.add(l_TransData);
			}
			lstAccGLTransData = DBTransactionMgr.prepareAccountGLTransactions(lstTransData);
			DBTransactionMgr.prepareBaseCurrencys(lstTransData);
			ret = DBTransactionMgr.post(lstTransData, lstAccGLTransData, pConn);
		}
		
		return ret;
	}
	public WalletTransDataResponse gopostTransaction(WalletTransDataRequest p) {
		WalletTransDataResponse response = new WalletTransDataResponse();
		Connection conn2 = null;
		Connection conn1 = null;
		DAOManager daomgr = new DAOManager();
		DataResult ret = new DataResult();
		String l_BCode= "";
		AccountDao  l_AccDao  = new AccountDao();
		try {
			conn1 = daomgr.openConnection();
			conn2 = daomgr.openICBSConnection();			
			if (conn2 != null) {
				response = new ReportDao().getSettlementList(p.getFromDate(), conn1,conn2);//,fdata									
				response.setState(true);
				l_BCode= l_AccDao.getBranchCode(conn2);
			} else {
				response.setState(false);
				response.setMsgCode("0016");
				response.setMsgDesc("Connection Refuse");
			}			
				if (response.isState()) {
					if (response.getWldata().length != 0 ) {
						if(! new ReportDao().checkExportLogs(p.getFromDate(), 1, l_BCode,conn2)){
							conn2.setAutoCommit(false);
							ret = prepareSettlementTrans(response.getWldata(),p.getUserID(),l_BCode,p.getFromDate(),conn2);
							if(ret.getStatus()) {
								conn2.commit();
								if(new ReportDao().saveExportLogs(p.getFromDate(), 1, l_BCode,
										(int) ret.getTransactionNumber(), conn2)) {
									conn2.commit();
									response.setState(true);
									response.setMsgCode("0000");
									response.setMsgDesc("Post Transaction Successfully!");
									
								}else {
									conn2.rollback();
									response.setMsgCode("0016");
									response.setMsgDesc("Save SettlementLog Fail!");
								}
							}else {
								conn2.rollback();
								if(!ret.getDescription().equals("")){
									response.setMsgDesc(ret.getDescription());
									response.setMsgCode("0016");
								}else{
								response.setMsgCode("0016");
								response.setMsgDesc("Post Transaction Fail!");
								}
							}
						}else {
							response.setState(false);
							response.setMsgCode("0000");
							response.setMsgDesc("Already Posted Transaction");
						}						
					} else {
						response.setMsgCode("0016");
						response.setMsgDesc("No Record Found");
					}					
				}else {
					response.setMsgCode("0016");
					response.setMsgDesc("No Record Found");
				}	
		} catch (Exception e) {
			e.printStackTrace();
			response.setState(false);
			ret.setStatus(false);
			try {
				conn2.rollback();
			} catch (SQLException e1) {				
				e1.printStackTrace();
			}
			response.setMsgDesc(e.getMessage());			
		} finally {			
			ServerUtil.closeConnection(conn2);
			ServerUtil.closeConnection(conn1);
		}
		
		return response;
	}

}
