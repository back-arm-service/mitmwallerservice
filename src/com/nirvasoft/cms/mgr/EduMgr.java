package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nirvasoft.cms.dao.ArticleDao;
import com.nirvasoft.cms.dao.ArticleJunDao;
import com.nirvasoft.cms.dao.CommentDao;
import com.nirvasoft.cms.dao.ContentMenuDao;
import com.nirvasoft.cms.dao.EduDao;
import com.nirvasoft.cms.dao.OPTDao;
import com.nirvasoft.cms.dao.UploadDao;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.shared.ArticleDataSet;
import com.nirvasoft.cms.shared.CommentDataSet;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.cms.shared.PhotoUploadData;
import com.nirvasoft.cms.shared.UploadData;
import com.nirvasoft.cms.util.FontChangeUtil;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.database.SysKeyMgr;

public class EduMgr {
	public static Resultb2b clickDisLikeEdu(long id, String userSK, String type, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		Resultb2b res = new Resultb2b();
		try {
			res = EduDao.updateDisLikeCountArticle(id, userSK, type, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	public static Resultb2b clickLikeEdu(long id, String userSK, String type, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		Resultb2b res = new Resultb2b();
		try {
			res = EduDao.updateLikeCountArticle(id, userSK, type, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	public static Resultb2b clickUnlikeArticle(long id, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		Resultb2b res = new Resultb2b();
		try {
			res = EduDao.updateUnlikeCount(id, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static ArticleDataSet deleteComment(ArticleData data, MrBean user) {
		Resultb2b res = new Resultb2b();
		ArticleDataSet dataSet = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			conn.setAutoCommit(false);
			res = CommentDao.deleteAllByKey(data.getSyskey(), conn);
			res = CommentDao.delete(data.getSyskey(), conn);
			if (res.isState())
				res = EduDao.reduceCommentCount(data.getN1(), conn);
			if (res.isState()) {
				dataSet = CommentDao.search(String.valueOf(data.getN1()), conn);
				conn.commit();
			} else {
				conn.rollback();
			}
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dataSet;
	}

	public static Resultb2b deleteEdu(Long key, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = EduDao.delete(key, conn);
			if (res.isState()) {
				res = ArticleJunDao.deleteByUpdate(key, conn);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static ArticleDataSet getComments(String id, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleDataSet res = new ArticleDataSet();
		try {
			long userKey = OPTDao.searchByID(user.getUser().getUserId(), conn);
			res = CommentDao.search(id, conn);
			ArticleData[] dataArr = res.getData();
			for (ArticleData data : dataArr) {
				data.setN7(userKey);
				data.setUpload(UploadDao.search(String.valueOf(data.getSyskey()), "", conn).getUploads());
			}
			res.setData(dataArr);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static ArticleData readDataBySyskey(long key, MrBean user) {
		ArticleData res = new ArticleData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = EduDao.read(key, conn);
			res.setCropdata(new ArticleJunDao().search(String.valueOf(key), conn).getCrop());
			res.setUploadlist(UploadDao.searchUploadList(String.valueOf(res.getSyskey()), conn));
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	public static ArticleDataSet saveComment(ArticleData data, MrBean user) {
		Resultb2b res = new Resultb2b();
		ArticleDataSet dataSet = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			conn.setAutoCommit(false);
			data = setStatusData(data, user);
			if (data.getSyskey() == 0) {
				long syskey = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
				data.setSyskey(syskey);
				res = CommentDao.insert(data, conn);
				if (res.isState()) {
					for (int i = 0; i < data.getUpload().length; i++) {
						boolean flag = false;
						UploadData obj = setUploadData(data.getUpload()[i], syskey, user);
						obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
								ConnAdmin.getConn(user.getUser().getOrganizationID())));
						flag = UploadDao.insert(obj, conn);
						res.setState(flag);
						if (!flag)
							break;
					}
				}
				if (res.isState()) {
					if (data.getN3() != -1)
						res = EduDao.updateCommentCount(data.getN1(), conn);
				}

			} else {
				res = CommentDao.update(data, conn);
				if (res.isState()) {
					UploadDao.deleteByUpdate(data.getSyskey(), data.getT3(), conn);
					boolean flag = false;
					for (int i = 0; i < data.getUpload().length; i++) {
						UploadData obj = setUploadData(data.getUpload()[i], data.getSyskey(), user);
						obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
								ConnAdmin.getConn(user.getUser().getOrganizationID())));
						flag = UploadDao.insert(obj, conn);
						res.setState(flag);
						if (!flag)
							break;
					}
				}
			}
			if (res.isState()) {
				dataSet = CommentDao.search(String.valueOf(data.getN1()), conn);
				conn.commit();
			} else {
				conn.rollback();
			}
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dataSet;
	}

	public static ArticleDataSet searchEdu(PagerData p, String searchVal, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleDataSet res = new ArticleDataSet();
		try {
			res = EduDao.searchArticle(p, searchVal, "article", conn);
			ArticleData[] dataArr = res.getData();
			for (ArticleData data : dataArr) {
				data.setUpload(UploadDao.search(String.valueOf(data.getSyskey()), "", conn).getUploads());
			}
			res.setData(dataArr);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	public static ArticleDataSet searchEduLists(PagerData p, String croptype, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleDataSet res = new ArticleDataSet();
		try {
			res = EduDao.searchArticleLists(p, "article", croptype, conn);
			ArticleData[] dataArr = res.getData();
			for (ArticleData data : dataArr) {
				data.setModifiedDate(ArticleDao.ddMMyyyFormat(data.getModifiedDate()));
				data.setUpload(UploadDao.search(String.valueOf(data.getSyskey()), "", conn).getUploads());
			}
			res.setData(dataArr);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	public static ArticleDataSet searchLike(String type, long key, MrBean user) {
		ArticleDataSet res = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = EduDao.searchLikeEdu(type, key, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	public static ArticleDataSet searchLikeCount(String id, MrBean user) {
		ArticleDataSet res = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = EduDao.searchLikeCount(id, conn, user);
			ArticleData[] dataArr = res.getData();
			for (ArticleData data : dataArr) {
				data.setUpload(UploadDao.search(String.valueOf(data.getSyskey()), "", conn).getUploads());
				data.setVideoUpload(UploadDao.searchVideoList(String.valueOf(data.getSyskey()), conn).getVideoUpload());
			}
			res.setData(dataArr);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static CommentDataSet searchLikeUserList(String aSK, MrBean user) {
		CommentDataSet res = new CommentDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = EduDao.searchLikeUserList(aSK, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	// kndz
	public static UploadData setPhotoUploadData(PhotoUploadData d, long articleKey, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		UploadData data = new UploadData();
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(data.getCreatedTime());
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		data.setT1(d.getName());
		data.setT3(String.valueOf(d.getSerial()));
		data.setT4(d.getDesc());
		data.setN1(articleKey);
		return data;
	}

	public static ArticleData setStatusData(ArticleData data, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(data.getCreatedTime());
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		if (ServerUtil.isZawgyiEncoded(data.getT2())) {
			data.setT2(FontChangeUtil.zg2uni(data.getT2()));
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		return data;
	}

	public static UploadData setUploadData(String url, long articleKey, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		UploadData data = new UploadData();
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(data.getCreatedTime());
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		data.setT1(url);
		data.setN1(articleKey);
		return data;
	}

	public static UploadData setUploadVideoData(String url, long articleKey, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		UploadData data = new UploadData();
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(data.getCreatedTime());
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		data.setT2(url);
		data.setN2(articleKey);
		return data;
	}

	public static Resultb2b UnDislikeEdu(long id, long userSK, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		Resultb2b res = new Resultb2b();
		try {
			res = EduDao.UnDislikeArticle(id, userSK, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static Resultb2b UnlikeEdu(long id, long userSK, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		Resultb2b res = new Resultb2b();
		try {
			res = EduDao.UnlikeArticle(id, userSK, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static ArticleDataSet viewArticle(String searchVal, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleDataSet res = new ArticleDataSet();
		try {
			long userKey = OPTDao.searchByID(user.getUser().getUserId(), conn);
			res = EduDao.viewArticle(searchVal, conn);
			ArticleData[] dataArr = res.getData();
			for (ArticleData data : dataArr) {
				data.setUpload(UploadDao.search(String.valueOf(data.getSyskey()), "", conn).getUploads());
				data.setN6(EduDao.searchLikeUser(data.getSyskey(), conn, user));
				data.setN7(userKey);
			}
			res.setData(dataArr);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	public static ArticleDataSet viewEdu(String searchVal, String userSK, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleDataSet res = new ArticleDataSet();
		try {
			long postkey = OPTDao.searchByPostID(searchVal, userSK, conn);
			res = ArticleDao.viewByKey(searchVal, postkey, "education", conn);
			ArticleData[] dataArr = res.getData();
			for (ArticleData data : dataArr) {
				data.setUploadedPhoto(UploadDao.search(String.valueOf(data.getSyskey()), "", conn).getData());
				data.setN6(ArticleDao.searchLikesUser(data.getSyskey(), Long.parseLong(userSK), conn, user));
				data.setN7(ContentMenuDao.searchSaveContentOrNot(userSK, data.getSyskey(), conn));
				data.setN9(ArticleDao.searchDisLikeOrNot(data.getSyskey(), userSK, conn));
			}
			res.setData(dataArr);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

}
