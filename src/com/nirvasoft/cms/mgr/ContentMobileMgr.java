package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nirvasoft.cms.dao.ArticleMobileDao;
import com.nirvasoft.cms.dao.ContentMenuDao;
import com.nirvasoft.cms.dao.ContentMobileDao;
import com.nirvasoft.cms.dao.UploadMobileDao;
import com.nirvasoft.cms.framework.ResultMobile;
import com.nirvasoft.cms.shared.PagerMobileData;
import com.nirvasoft.database.SysKeyMgr;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.shared.ContentData;
import com.nirvasoft.cms.shared.ContentDataSet;
import com.nirvasoft.cms.shared.UserViewDataset;
import com.nirvasoft.cms.util.FontChangeUtil;
import com.nirvasoft.cms.util.ServerUtil;

public class ContentMobileMgr {

	private boolean ExitSaveContent(ContentData data, Connection conn) throws SQLException {
		boolean isExit = false;
		ResultSet rs;
		String sql = "SELECT * FROM FMR013 WHERE n1='" + data.getN1() + "' AND n2='" + data.getN2() + "' AND n3=1";
		PreparedStatement stat = conn.prepareStatement(sql);
		rs = stat.executeQuery();
		if (rs.next()) {
			isExit = true;
		}
		return isExit;
	}

	public UserViewDataset getContentWriterList(PagerMobileData p, MrBean user) throws SQLException {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		UserViewDataset res = new UserViewDataset();
		res = new ContentMobileDao().searchContentWriterList(p, conn);
		return res;
	}

	public ResultMobile saveContent(ContentData data, MrBean user) {
		ResultMobile res = new ResultMobile();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			data = setStatusData(data, user);
			if (data.getSyskey() == 0) {
				long syskey = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
				data.setSyskey(syskey);
				data.setT4(data.getT4());// post username
				if (!new ContentMobileDao().ExistPost(data, conn)) {// if exit update
																// save conent
					res = new ContentMobileDao().insert(data, conn);
				} else {
					res = new ContentMobileDao().update(data, conn);
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public ContentDataSet searchContent(PagerMobileData p, long userSK, String searchVal, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ContentDataSet res = new ContentDataSet();
		ArticleData ret = new ArticleData();
		try {
			res = new ContentMobileDao().searchContent(p, userSK, searchVal, conn);
			ContentData[] dataArr = res.getData();
			for (ContentData data : dataArr) {
				ret = new ArticleMobileDao().readByKey(data.getN2(), conn);
				data.setN7(new ContentMenuDao().searchSaveContentOrNot(Long.toString(userSK), data.getN2(), conn));
				data.setUploadedPhoto(new UploadMobileDao().search(String.valueOf(data.getN2()), "", conn).getData());
				if (ret.getT3().equalsIgnoreCase("Video")) {
					data.setUploadedPhoto(
							new UploadMobileDao().search(String.valueOf(data.getN2()), "Video", conn).getData());// photo
																											// and
																											// video
				}

				data.setT5(ret.getT1());
				data.setT6(ret.getT2());
				data.setT8(ret.getT8());
				data.setT10(ret.getT10());
			}
			res.setData(dataArr);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	public ContentData setStatusData(ContentData data, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
		}
		data.setModifiedDate(todayDate);
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		if (ServerUtil.isZawgyiEncoded(data.getT2())) {
			data.setT2(FontChangeUtil.zg2uni(data.getT2()));
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		return data;
	}

	public ResultMobile unsaveContent(ContentData data, MrBean user) {
		ResultMobile res = new ResultMobile();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			if (ExitSaveContent(data, conn)) {
				res = new ContentMobileDao().updatesavecontent(data, conn);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

}
