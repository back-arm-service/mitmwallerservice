package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nirvasoft.cms.dao.ButtonDataDao;
import com.nirvasoft.cms.dao.MenuDao;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.ButtonDataSet;
import com.nirvasoft.cms.shared.MenuData;
import com.nirvasoft.cms.shared.MenuRole;
import com.nirvasoft.cms.shared.MenuViewDataset;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.cms.shared.ValueCaptionDataSet;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.database.SysKeyMgr;

public class MenuDataMgr {
	public static Resultb2b deleteMenuData(long syskey, long n2, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = MenuDao.delete(syskey, n2, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static MenuViewDataset getAllMenuData(MrBean user) {
		MenuViewDataset res = new MenuViewDataset();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = MenuDao.getAllMenuData(user, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public static ButtonDataSet getButtonList(MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ButtonDataSet res = new ButtonDataSet();
		try {
			res = ButtonDataDao.getButtonList(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static ValueCaptionDataSet getmainmenulist(MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ValueCaptionDataSet res = new ValueCaptionDataSet();
		try {
			res = MenuDao.getmainmenulist(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static int getMenuCount(String searchVal, MrBean user) {

		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		int res = 0;
		try {
			res = MenuDao.getMenuCount(searchVal, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static MenuData initData(MenuData data, MrBean user, Connection con) {
		String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
		data.setUserId(user.getUser().getUserId());
		data.setUserName(user.getUser().getUserName());
		data.setModifiedDate(date);
		if (data.getSyskey() == 0) {
			data.setCreatedDate(date);
			data.setRecordStatus(1);
			data.setSyncBatch(0);
			data.setSyncStatus(1);
		}
		return data;
	}

	public static MenuRole initMenuRoleData(MenuRole mr, MenuData data, MrBean user, Connection con) {

		String date23 = new SimpleDateFormat("yyyyMMdd").format(new Date());
		mr.setUserId(user.getUser().getUserId());
		mr.setUserName(user.getUser().getUserName());
		mr.setModifiedDate(date23);
		mr.setN1(1);
		mr.setN2(data.getSyskey());
		if (mr.getSyskey() == 0) {
			mr.setCreatedDate(date23);
			mr.setRecordStatus(1);
			mr.setSyncBatch(0);
			mr.setSyncStatus(1);
		}
		return mr;
	}

	public static MenuData readDataBySyskey(long pKey, MrBean user) {
		MenuData res = new MenuData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = MenuDao.read(pKey, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	public static Resultb2b saveMenuData(MenuData data, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			long rolemenu = data.getSyskey();
			res = saveMenuData(data, user, conn);
			if (res.isState() && rolemenu == 0) {
				res = saveRoleMenuData(data, user, conn);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public static Resultb2b saveMenuData(MenuData data, MrBean user, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		data = initData(data, user, conn);
		if (data.getSyskey() == 0) {
			data.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID())));
			res = MenuDao.insert(data, conn);
		} else {
			res = MenuDao.update(data, conn);
		}
		if (res.isState()) {
		}
		return res;
	}

	public static Resultb2b saveRoleMenuData(MenuData data, MrBean user, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		MenuRole mr = new MenuRole();
		mr = initMenuRoleData(mr, data, user, conn);
		if (mr.getSyskey() == 0) {
			mr.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID())));
			res = MenuDao.insertMenuRole(mr, conn);
		} else {
			res = MenuDao.update(data, conn); /// need to edit
		}
		if (res.isState()) {

			res.getLongResult().add(data.getN2());
			res.getLongResult().add(data.getSyskey());
			long[] keyarry = new long[res.getLongResult().size()];

			for (int i = 0; i < res.getLongResult().size(); i++) {
				keyarry[i] = res.getLongResult().get(i);
			}
			res.setKey(keyarry);
		}
		return res;

	}

	public static MenuViewDataset searchMenubyValue(PagerData pager, String searchVal, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		MenuViewDataset res = new MenuViewDataset();
		try {
			res = MenuDao.searchMenu(pager, searchVal, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

}
