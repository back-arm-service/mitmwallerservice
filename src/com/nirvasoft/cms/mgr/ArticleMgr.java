package com.nirvasoft.cms.mgr;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nirvasoft.cms.dao.ArticleDao;
import com.nirvasoft.cms.dao.ArticleJunDao;
import com.nirvasoft.cms.dao.CommentDao;
import com.nirvasoft.cms.dao.ContentMenuDao;
import com.nirvasoft.cms.dao.OPTDao;
import com.nirvasoft.cms.dao.UploadDao;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.shared.ArticleDataSet;
import com.nirvasoft.cms.shared.ArticleJunData;
import com.nirvasoft.cms.shared.CommentDataSet;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.cms.shared.UploadData;
import com.nirvasoft.cms.util.FontChangeUtil;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.database.SysKeyMgr;
public class ArticleMgr {
	public static ArticleJunData initJUNData(ArticleJunData typeData, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (typeData.getSyskey() == 0) {
			typeData.setCreateddate(todayDate);
		}
		typeData.setModifieddate(todayDate);
		if (!typeData.getUserid().equals("") && !typeData.getUsername().equals("")) {
			typeData.setUserid(typeData.getUserid());
			typeData.setUsername(typeData.getUsername());
		} else {
			typeData.setUserid(user.getUser().getUserId());
			typeData.setUsername(user.getUser().getUserName());
		}
		typeData.setRecordStatus(1);
		typeData.setSyncStatus(1);
		typeData.setSyncBatch(0);
		typeData.setUsersyskey(0);
		return typeData;
	}

	public static ArticleData setStatusData(ArticleData data, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(data.getCreatedTime());
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		if (ServerUtil.isZawgyiEncoded(data.getT1())) {
			data.setT1(FontChangeUtil.zg2uni(data.getT1()));
		}
		if (ServerUtil.isZawgyiEncoded(data.getT2())) {
			data.setT2(FontChangeUtil.zg2uni(data.getT2()));
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		data.setVideoUpload(data.getVideoUpload());
		return data;
	}
	

	public static UploadData setUploadData(String url, long articleKey, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		UploadData data = new UploadData();
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(data.getCreatedTime());
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		data.setT1(url);
		data.setN1(articleKey);
		return data;
	}

	public static UploadData setUploadVideoData(String url, String filename, String imgurl, long articleKey,MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		UploadData data = new UploadData();
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(data.getCreatedTime());
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		data.setT1(filename);
		data.setT2(url);
		data.setN2(articleKey);
		return data;
	}

	
	public static ArticleDataSet searchVideoUserLists(PagerData p,String mobile, String searchVal, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleDataSet res = new ArticleDataSet();
		try {
			res = ArticleDao.searchVideoUserLists(p,mobile, searchVal, "Video", conn);
			ArticleData[] dataArr = res.getData();
			for (ArticleData data : dataArr) {
				data.setModifiedDate(ArticleDao.ddMMyyyFormat(data.getModifiedDate()));
				data.setVideoUpload(UploadDao.searchVideoList(String.valueOf(data.getSyskey()), conn).getVideoUpload());
			}
			res.setData(dataArr);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	public static ArticleData readDataBySyskey(long key, MrBean user) {
		ArticleData res = new ArticleData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = ArticleDao.read(key, conn);
			res.setCropdata(new ArticleJunDao().search(String.valueOf(key), conn).getCrop());
			res.setUpload(UploadDao.search(String.valueOf(res.getSyskey()),"", conn).getUploads());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static Resultb2b clickLikeArticle(long postsk, String userSK, String type, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		Resultb2b res = new Resultb2b();
		try {
			res = ArticleDao.updateLikeCountArticle(postsk, userSK, type, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}
	public static Resultb2b clickUnlikeArticle(long id, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		Resultb2b res = new Resultb2b();
		try {
			res = ArticleDao.updateUnlikeCount(id, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
	
	public static Resultb2b UnlikeArticle(long id, long userSK, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		Resultb2b res = new Resultb2b();
		try {
			res = ArticleDao.UnlikeArticle(id, userSK, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static Resultb2b clickDisLikeArticle(long id, String userSK, String type, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		Resultb2b res = new Resultb2b();
		try {
			res = ArticleDao.updateDisLikeCountArticle(id, userSK, type, conn, user);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}
	
	public static Resultb2b UnDislikeArticle(long id, long userSK, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		Resultb2b res = new Resultb2b();
		try {
			res = ArticleDao.UnDislikeArticle(id, userSK, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static ArticleDataSet saveComment(ArticleData data, MrBean user) {
		Resultb2b res = new Resultb2b();
		ArticleDataSet dataSet = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			data = setStatusData(data, user);
			if (data.getSyskey() == 0) {
				long syskey = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
				data.setSyskey(syskey);
				res = CommentDao.insert(data, conn);
				if (res.isState()) {
					for (int i = 0; i < data.getUpload().length; i++) {
						boolean flag = false;
						UploadData obj = setUploadData(data.getUpload()[i], syskey, user);
						obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",ConnAdmin.getConn(user.getUser().getOrganizationID())));
						flag = UploadDao.insert(obj, conn);
						res.setState(flag);
						if (!flag)
							break;
					}
				}
				if (res.isState()) {
					if (data.getN3() != -1)
						res = ArticleDao.updateCommentCount(data.getN1(), conn);
				}

			} else {
				res = CommentDao.update(data, conn);
				if (res.isState()) {
					UploadDao.deleteByUpdate(data.getSyskey(),data.getT3(), conn);
					boolean flag = false;
					for (int i = 0; i < data.getUpload().length; i++) {
						UploadData obj = setUploadData(data.getUpload()[i], data.getSyskey(), user);
						obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",ConnAdmin.getConn(user.getUser().getOrganizationID())));
						flag = UploadDao.insert(obj, conn);
						res.setState(flag);
						if (!flag)
							break;
					}
				}
			}
			if(res.isState()){
				dataSet = CommentDao.searchComment(String.valueOf(data.getN1()), conn);
			} 
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			ServerUtil.closeConnection(conn);
		}
		return dataSet;
	}

	public static ArticleDataSet getComments(String id, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleDataSet res = new ArticleDataSet();
		try {
			long userKey = OPTDao.searchByID(user.getUser().getUserId(), conn);
			res = CommentDao.search(id, conn);
			ArticleData[] dataArr = res.getData();
			for (ArticleData data : dataArr) {
				data.setN7(userKey);// usersk
				data.setUpload(UploadDao.search(String.valueOf(data.getSyskey()),"", conn).getUploads());
			}
			res.setData(dataArr);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static ArticleDataSet deleteComment(ArticleData data, MrBean user) {
		Resultb2b res = new Resultb2b();
		ArticleDataSet dataSet = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			conn.setAutoCommit(false);
			res = CommentDao.deleteAllByKey(data.getSyskey(), conn);
			res = CommentDao.delete(data.getSyskey(), conn);
			if (res.isState())
				res = ArticleDao.reduceCommentCount(data.getN1(), conn);
			if (res.isState()) {
				dataSet = CommentDao.search(String.valueOf(data.getN1()), conn);
				conn.commit();
			} else {
				conn.rollback();
			}
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dataSet;
	}

	public static ArticleDataSet viewArticle(String searchVal, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleDataSet res = new ArticleDataSet();
		try {
			long userKey = OPTDao.searchByID(user.getUser().getUserId(), conn);
			res = ArticleDao.viewArticle(searchVal, conn);
			ArticleData[] dataArr = res.getData();
			for (ArticleData data : dataArr) {
				data.setUpload(UploadDao.search(String.valueOf(data.getSyskey()),"", conn).getUploads());
				data.setN6(ArticleDao.searchLikeUser(data.getSyskey(), conn, user));
				data.setN7(userKey);
			}
			res.setData(dataArr);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static ArticleDataSet viewNews(String searchVal, String userSK, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleDataSet res = new ArticleDataSet();
		try {
			long postkey = OPTDao.searchByPostID(searchVal, userSK, conn);
			res = ArticleDao.viewByKey(searchVal, postkey, "News & Media", conn);
			ArticleData[] dataArr = res.getData();
			for (ArticleData data : dataArr) {
			   data.setUploadedPhoto(UploadDao.search(String.valueOf(data.getSyskey()),"", conn).getData());
				data.setN6(ArticleDao.searchLikesUser(data.getSyskey(), Long.parseLong(userSK), conn, user));
				data.setN7(ContentMenuDao.searchSaveContentOrNot(userSK,data.getSyskey(), conn));
				data.setN9(ArticleDao.searchDisLikeOrNot(data.getSyskey(), userSK, conn));
			}
			res.setData(dataArr);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	public static ArticleDataSet searchLike(String type, long key, MrBean user) {
		ArticleDataSet res = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = ArticleDao.searchLikeArticle(type, key, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	public static CommentDataSet searchLikeUserList(String aSK, MrBean user) {
		CommentDataSet res = new CommentDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = ArticleDao.searchLikeUserList(aSK, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}
	
	//convert canvas to image file for video file (WTZA)
/*	public static Boolean writeToFile(String data, String uploadedFileLocation) {
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] imgBytes;
		try {
			imgBytes = decoder.decodeBuffer(data);
			BufferedImage bufImg = ImageIO.read(new ByteArrayInputStream(imgBytes));
			File imgOutFile = new File(uploadedFileLocation);
			ImageIO.write(bufImg, "png", imgOutFile);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;

	}*/ 	
	public static Resultb2b clickLikeComment(long postkey, long comkey, long userSK, String type, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		Resultb2b res = new Resultb2b();
		try {
			res = CommentDao.updateLikeCountComment(postkey, comkey, userSK, type, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}
	
	/*clickUnLikeComment*/
	public static Resultb2b clickUnLikeComment(long postkey, long comkey, long userSK, String type, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		Resultb2b res = new Resultb2b();
		try {
			res = CommentDao.updateUnLikeCountComment(postkey, comkey, userSK, type, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
	
	/* searchQuesList */
	public static ArticleDataSet searchVideoList(PagerData p, long status, String statetype,MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleDataSet res = new ArticleDataSet();
		try {
			res = ArticleDao.searchVideoList(p, status, statetype,conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

}
