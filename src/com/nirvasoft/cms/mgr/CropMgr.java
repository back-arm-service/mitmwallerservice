package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nirvasoft.cms.dao.CropDao;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.AdvancedSearchData;
import com.nirvasoft.cms.shared.BrandComboDataSet;
import com.nirvasoft.cms.shared.CropData;
import com.nirvasoft.cms.shared.CropDataSet;
import com.nirvasoft.cms.shared.DivisionComboDataSet;
import com.nirvasoft.cms.util.ServerUtil;

public class CropMgr {

	public static Resultb2b deletCrop(Long key, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = CropDao.delete(key, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	// Crop Combo List
	public static DivisionComboDataSet getCropComboList(MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		DivisionComboDataSet res = new DivisionComboDataSet();
		try {
			res = CropDao.getCropComboList(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static BrandComboDataSet getMoonsoonCropList(MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		BrandComboDataSet res = new BrandComboDataSet();
		try {
			res = CropDao.getMoonsoonCropList(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static BrandComboDataSet getWinterCropList(MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		BrandComboDataSet res = new BrandComboDataSet();
		try {
			res = CropDao.getWinterCropList(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static CropData initData(CropData data, MrBean user) {
		String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (data.getSyskey() == 0) {
			data.setCreatedDate(date);
		} else {
			data.setCreatedDate(date);
		}
		data.setUserId(data.getT1());
		data.setUserName("User");
		data.setModifiedDate(date);
		data.setRecordStatus(1);
		data.setSyncBatch(0);
		data.setSyncStatus(1);

		return data;
	}

	/*
	 * public static DivisionComboDataSet getSenderList(MrBean user) {
	 * Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
	 * DivisionComboDataSet res = new DivisionComboDataSet(); try { res =
	 * CropDao.getSenderList(conn); } catch (SQLException e) {
	 * e.printStackTrace(); } return res; }
	 */

	public static CropData readDataBySyskey(long key, MrBean user) {
		CropData res = new CropData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = CropDao.read(key, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public static Resultb2b saveCropData(CropData data, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			if (data.getSyskey() == 0) {
				data.setSyskey(Long.parseLong(CropDao.getSyskey("FMR012", conn)));
				data = initData(data, user);
				if (data.getT3().equals("0")) {
					data.setN1(Long.parseLong(CropDao.getType(data.getT3(), conn)));
				} else if (data.getT3().equals("1")) {
					data.setN1(Long.parseLong(CropDao.getType(data.getT3(), conn)));
				}
				res = CropDao.insert(data, conn);
			} else {
				res = CropDao.update(data, conn);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public static CropDataSet searchCrop(AdvancedSearchData p, String searchVal, String sort, String type,
			MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		CropDataSet res = new CropDataSet();
		try {
			res = CropDao.search(p, searchVal, sort, type, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

}
