package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.nio.file.Files;

import sun.misc.BASE64Decoder;

import com.nirvasoft.cms.dao.ArticleDao;
import com.nirvasoft.cms.dao.ContentDao;
import com.nirvasoft.cms.dao.ContentMenuDao;
import com.nirvasoft.cms.dao.UploadDao;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Result;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.shared.ArticleJunData;
import com.nirvasoft.cms.shared.DivisionComboDataSet;
import com.nirvasoft.cms.shared.PhotoUploadData;
import com.nirvasoft.cms.shared.UploadData;
import com.nirvasoft.database.SysKeyMgr;

public class ContentMenuMgr {
	//////////////////////// CMS////////////////////
	public static Resultb2b saveContenMenu(ArticleData data, String filePath, MrBean user) {
		Resultb2b res = new Resultb2b();
		// ArticleJunData jundata = new ArticleJunData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			conn.setAutoCommit(false);
			data = setStatusData(data, user);
			if (data.getSyskey() == 0) {
				data.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID())));
				res = ArticleDao.insert(data, conn);
				if (res.isState()) {
					if (res.isState()) {
						for (int i = 0; i < data.getUploadlist().size(); i++) {
							boolean flag = false;
							UploadData obj = setPhotoUploadData(data.getUploadlist().get(i), data.getResizelist()[i],
									data.getSyskey(), user);
							obj.setModifiedTime(data.getModifiedTime());
							obj.setCreatedTime(data.getCreatedTime());
							obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
									ConnAdmin.getConn(user.getUser().getOrganizationID())));
							flag = UploadDao.insert(obj, conn);
							res.setState(flag);
						}
					}
				}
			} else {
				res = ArticleDao.update(data, conn);
				if (res.isState()) {
					// get_img_url
					// data.setUploadlist(getImgUrl(data.getUploadlist()));
					//
					if (isExistUpload(data.getSyskey(), conn)) {
						String sql = " DELETE FROM FMr006 WHERE N1 = '" + data.getSyskey() + "' ";
						PreparedStatement ps = conn.prepareStatement(sql);
						ps.executeUpdate();
					}

					boolean flag = false;
					for (int i = 0; i < data.getUploadlist().size(); i++) {
						UploadData obj = setPhotoUploadData(data.getUploadlist().get(i), data.getResizelist()[i],
								data.getSyskey(), user);
						obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
								ConnAdmin.getConn(user.getUser().getOrganizationID())));
						obj.setModifiedTime(data.getModifiedTime());
						obj.setCreatedTime(data.getCreatedTime());
						flag = UploadDao.insert(obj, conn);
						res.setState(flag);
					}

				}
			}
			if (res.isState()) {
				res.setKeyResult(data.getSyskey());
				res.getLongResult().add(data.getSyskey());
				conn.commit();
			} else
				conn.rollback();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return res;
	}

	public static ArticleData setStatusData(ArticleData data, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(data.getCreatedTime());
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());
		if (data.getSyskey() == 0) {
			data.setModifiedUserId("");
			data.setModifiedUserName("");

		} else {
			data.setModifiedUserId(data.getModifiedUserId());
			data.setModifiedUserName(data.getModifiedUserName());
		}
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		return data;
	}

	public static UploadData setPhotoUploadData(PhotoUploadData d, String resizefilename, long articleKey,
			MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		UploadData data = new UploadData();
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(data.getCreatedTime());
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		data.setT1(d.getName());// photo name
		data.setT3(String.valueOf(d.getSerial()));// photo serial no
		data.setT4(d.getDesc());// description
		data.setT5(d.getOrder());// description
		data.setT6(resizefilename);
		data.setT7(d.getUrl());

		data.setN1(articleKey);// fmr002 sk
		return data;
	}

	public static ArticleData readDataContentMenu(long key, MrBean user) {
		ArticleData res = new ArticleData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = ContentMenuDao.read(key, conn);
			res.setUploadlist(UploadDao.searchUploadList(String.valueOf(res.getSyskey()), conn));
			res.setResizelist(UploadDao.search(String.valueOf(res.getSyskey()), "", conn).getUploads());

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	public static DivisionComboDataSet getAcademicRef(MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		DivisionComboDataSet res = new DivisionComboDataSet();
		try {
			res = ContentMenuDao.getAcademicRef(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}


	private static boolean isExistUpload(long syskey, Connection conn) throws SQLException {
		boolean isExit = false;
		ResultSet rs = null;
		String sql = "Select * from fmr006 where (N1=" + syskey + " OR N2=" + syskey + ")";
		PreparedStatement ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();
		if (rs.next()) {
			isExit = true;
		}
		return isExit;
	}

	public static String convertBase64ToImage(ArticleData p, String imgurl) {

		String str = p.getT2();
		int a = str.indexOf("<img src=\"");
		String replaceString = "";
		if (a > 0) {
			int b = str.indexOf("alt"); // end index
			String exist = str.substring(a + 10, b - 2);
			String replace = imgurl + p.getUploadlist().get(0).getName();
			replaceString = str.replace(exist, replace);
			while (!exist.equalsIgnoreCase(replace)) {
				replaceString = str.replace(exist, replace);
				a = replaceString.indexOf("<img src=\""); // start index
				b = replaceString.indexOf(" alt="); // end index
				exist = replaceString.substring(a + 10, b - 1);
			}
			System.out.println("Str: " + replaceString);
		}
		return replaceString;
	}

	public static ArrayList<PhotoUploadData> checkUploadList(ArrayList<PhotoUploadData> uploadList, String str) {
		for (int i = 0; i < uploadList.size(); i++) {
			if (!str.contains(uploadList.get(i).getDesc())) {
				uploadList.remove(i);
			}
		}
		return uploadList;
	}

	/*
	 * public static ArrayList<PhotoUploadData>
	 * photoListLoopRemove(ArrayList<PhotoUploadData> uploadList) {
	 * ArrayList<Integer> count = new ArrayList<Integer>(); HashSet<Integer>
	 * hSetNumbers = new HashSet<Integer>(); for (int i = 0; i <
	 * uploadList.size(); i++) { String name = uploadList.get(i).getDesc(); for
	 * (int ii = i + 1; ii < uploadList.size(); ii++) { if
	 * (name.equalsIgnoreCase(uploadList.get(ii).getDesc())) {
	 * hSetNumbers.add(ii); } } } for (int strNumber : hSetNumbers)
	 * count.add(strNumber); Collections.sort(count,
	 * Collections.reverseOrder()); for (int j = 0; j < count.size(); j++) { int
	 * cnt = count.get(j); uploadList.remove(cnt - 1); } return uploadList; }
	 */

	/*
	 * public static int getUploadCount(String str, ArrayList<PhotoUploadData>
	 * uploadList) {
	 * 
	 * int uploadCount = 0; String exist = ""; int c = 0; String Arr[] = new
	 * String[100]; Arr = str.split(" alt=\""); c = Arr[1].indexOf(" width=\"");
	 * 
	 * exist = Arr[1].substring(0, c - 1);
	 * 
	 * for (int i = 0; i < uploadList.size(); i++) { if
	 * (uploadList.get(i).getDesc().equalsIgnoreCase(exist)) { uploadCount = i;
	 * } } return uploadCount; }
	 */

	/*
	 * public static ArrayList<PhotoUploadData> photoListRemove(String content,
	 * ArrayList<PhotoUploadData> uploadList) { ArrayList<Integer> count = new
	 * ArrayList<Integer>(); for (int i = 0; i < uploadList.size(); i++) {
	 * String desc = "alt=\"" + uploadList.get(i).getDesc() + "\""; if
	 * ((!content.contains(desc) &&
	 * !content.contains(uploadList.get(i).getName())) ||
	 * (content.contains(desc) &&
	 * content.contains(uploadList.get(i).getName()))) { count.add(i); } }
	 * 
	 * for (int j = count.size() - 1; j >= 0; j--) { int cnt = count.get(j);
	 * uploadList.remove(cnt); }
	 * 
	 * return uploadList; }
	 */

	///// atnnew/////////
	public static Result saveCMSMenu(ArticleData data, MrBean user) {
		Result res = new Result();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			conn.setAutoCommit(false);
			data = setStatusData(data, user);
			if (data.getSyskey() == 0) {
				long syskey = SysKeyMgr.getSysKey(1, "syskey", conn);
				data.setSyskey(syskey);
				res = ArticleDao.insertNew(data, conn);
				if (res.isState()) {
					if (data.getT3().equals("Video")) {
						if (data.getN10() == 1 || data.getN10() == 2) {
							for (int i = 0; i < data.getUploadlist().size(); i++) {
								boolean flag = false;
								UploadData obj = setPhotoUploadData(data.getUploadlist().get(i),
										data.getResizelist()[i], data.getSyskey(), user);
								obj.setModifiedTime(data.getModifiedTime());
								obj.setCreatedTime(data.getCreatedTime());
								obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
										ConnAdmin.getConn(user.getUser().getOrganizationID())));
								obj.setT8(data.getT5());
								flag = UploadDao.insert(obj, conn);
								res.setState(flag);
							}
						} else if (data.getN10() == 0) {
							for (int i = 0; i < data.getVideoUpload().length; i++) {
								boolean flag = false;
								UploadData obj = new UploadData();
								obj = setUploadVideoData(data.getVideoUpload()[i], data.getVideoUpload()[i], "image",
										data.getSyskey(), user);
								if (data.getUploadlist().size() > 0) {
									obj.setT1(data.getUploadlist().get(i).getName());
									obj.setT7(data.getUploadlist().get(i).getName());
								}
								obj.setModifiedTime(data.getModifiedTime());
								obj.setCreatedTime(data.getCreatedTime());
								obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey", conn));
								obj.setT5(data.getT5());// region--hmh
								flag = UploadDao.insert(obj, conn);
								res.setState(flag);
							}
						}
					} else {
						for (int i = 0; i < data.getUploadlist().size(); i++) {
							boolean flag = false;
							UploadData obj = setPhotoUploadData(data.getUploadlist().get(i), data.getResizelist()[i],
									data.getSyskey(), user);
							obj.setModifiedTime(data.getModifiedTime());
							obj.setCreatedTime(data.getCreatedTime());
							obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
									ConnAdmin.getConn(user.getUser().getOrganizationID())));
							obj.setT8(data.getT5());
							flag = UploadDao.insert(obj, conn);
							res.setState(flag);
						}
					}
				}
			} else {
				res = ArticleDao.updateNew(data, conn);
				if (res.isState()) {
					if (isExistUpload(data.getSyskey(), conn)) {
						String sql = " DELETE FROM FMR006 WHERE (N1 =" + data.getSyskey() + " OR N2 ="
								+ data.getSyskey() + ")";
						PreparedStatement ps = conn.prepareStatement(sql);
						ps.executeUpdate();
					}
					if (data.getT3().equals("Video")) {
						if (data.getN10() == 1 || data.getN10() == 2) {
							boolean flag = false;
							for (int i = 0; i < data.getUploadlist().size(); i++) {
								UploadData obj = setPhotoUploadData(data.getUploadlist().get(i),
										data.getResizelist()[i], data.getSyskey(), user);
								obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey", conn));
								obj.setModifiedTime(data.getModifiedTime());
								obj.setCreatedTime(data.getCreatedTime());
								obj.setT6(data.getT13());// resizefilename
								obj.setT5(data.getT5());// region--hmh
								flag = UploadDao.insert(obj, conn);
								res.setState(flag);
							}
						} else {
							for (int i = 0; i < data.getVideoUpload().length; i++) {
								boolean flag = false;
								UploadData obj = new UploadData();
								obj = setUploadVideoData(data.getVideoUpload()[i], data.getVideoUpload()[i], "image",
										data.getSyskey(), user);
								if (data.getUploadlist().size() > 0) {
									obj.setT1(data.getUploadlist().get(i).getName());
									obj.setT7(data.getUploadlist().get(i).getName());
								}
								obj.setModifiedTime(data.getModifiedTime());
								obj.setCreatedTime(data.getCreatedTime());
								// obj.setT6(data.getT13());//resizefilename
								obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey", conn));
								obj.setT5(data.getT5());// region--hmh
								flag = UploadDao.insert(obj, conn);
								res.setState(flag);
								res.setMsgDesc("Updated Successfully!");
							}
						}
					} else {
						for (int i = 0; i < data.getUploadlist().size(); i++) {
							boolean flag = false;
							UploadData obj = setPhotoUploadData(data.getUploadlist().get(i), data.getResizelist()[i],
									data.getSyskey(), user);
							obj.setModifiedTime(data.getModifiedTime());
							obj.setCreatedTime(data.getCreatedTime());
							obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
									ConnAdmin.getConn(user.getUser().getOrganizationID())));
							obj.setT8(data.getT5());
							flag = UploadDao.insert(obj, conn);
							res.setState(flag);
						}
					}
				}
			}
			if (res.isState()) {
				res.setKeyResult(data.getSyskey());
				res.getLongResult().add(data.getSyskey());
				conn.commit();
			} else
				conn.rollback();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return res;
	}

	public static UploadData setUploadVideoData(String url, String filename, String imgurl, long articleKey,
			MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		UploadData data = new UploadData();
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(data.getCreatedTime());
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		data.setT2(url);
		data.setN2(articleKey);
		return data;
	}

	public static ArticleData readDataBySyskey(long key, MrBean user) {
		ArticleData res = new ArticleData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {

			res = ArticleDao.readNew(key, conn);
			// res.setCropdata(new ArticleJunDao().search(String.valueOf(key),
			// conn).getCrop());

			if (res.getN10() == 1 || res.getN10() == 2) {
				// res.setUpload(UploadDao.search(String.valueOf(res.getSyskey()),
				// "Video", conn).getUploads());
				res.setUploadlist(UploadDao.searchUploadList(String.valueOf(res.getSyskey()), conn));
			} else {
				// res.setVideoUpload(UploadDao.searchVideoList(String.valueOf(res.getSyskey()),
				// conn).getVideoUpload());
				// res.setUpload(UploadDao.search(String.valueOf(res.getSyskey()),
				// "Video", conn).getUploads());
				res.setUploadlist(UploadDao.searchUploadList(String.valueOf(res.getSyskey()), conn));
				res.setResizelist(UploadDao.search(String.valueOf(res.getSyskey()),
				"", conn).getUploads());
				if (res.getT3().equalsIgnoreCase("Video")) {
					res.setResizelist(UploadDao.search(String.valueOf(res.getSyskey()), "Video", conn).getUploads());
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
	
	/* deleteQuestion */
	public static Result deleteContent(Long postsk, String type, MrBean user) {
		Result res = new Result();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = ArticleDao.delete(postsk, conn);
			if (res.isState()) {
				if (isUploadExit(postsk, type, conn))
					res = UploadDao.deleteByUpdate(postsk, type, conn);
			}
			if (res.isState()) {
				if (isExistLike(postsk, conn))
					res = ArticleDao.deleteByUpdate(postsk, conn);
			}
			if (res.isState()) {
				if (ContentDao.isPostExist(postsk, type, conn)) {
					res = ContentDao.delete(postsk, conn);
				}
			}
			if (res.isState()) {
				if (isExistComment(postsk, conn))
					res = ArticleDao.deleteByUpdateComment(postsk, conn);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return res;
	}
	private static boolean isUploadExit(long syskey, String type, Connection conn) throws SQLException {
		boolean isExit = false;
		ResultSet rs = null;
		String sql = "";
		if (!type.equalsIgnoreCase("Video")) {
			sql = "Select syskey from FMR006 where n1=?";
		} else {
			sql = "Select syskey from FMR006 where n2=?";
		}
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setLong(1, syskey);
		rs = ps.executeQuery();
		if (rs.next()) {
			isExit = true;
		}
		return isExit;
	}
	private static boolean isExistLike(long syskey, Connection conn) throws SQLException {
		boolean isExit = false;
		ResultSet rs = null;
		String sql = "Select * from FMR008 where n1='" + syskey + "'";
		PreparedStatement ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();
		if (rs.next()) {
			isExit = true;
		}
		return isExit;
	}
	/* isExistComment */
	private static boolean isExistComment(long syskey, Connection conn) throws SQLException {
		boolean isExit = false;
		ResultSet rs = null;
		String sql = "Select * from FMR003 where n1='" + syskey + "'";
		PreparedStatement ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();
		if (rs.next()) {
			isExit = true;
		}
		return isExit;
	}


}
