package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.nirvasoft.cms.dao.ContentMobileDao;
import com.nirvasoft.cms.dao.SetupMobileDao;
import com.nirvasoft.database.SysKeyMgr;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.shared.PagerMobileData;
import com.nirvasoft.cms.shared.ContentData;
import com.nirvasoft.cms.shared.SetupDataSet;
import com.nirvasoft.rp.framework.ConnAdmin;

public class SetupMobileMgr {

	// Menu Order For Mobile (TDA)
	public SetupDataSet getMenuOrderList(PagerMobileData data, MrBean user) {
		Connection conn = ConnAdmin.getConn("001", "");
		SetupDataSet res = new SetupDataSet();
		try {
			res = SetupMobileDao.getMenuOrderList(conn, data, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public void savefordownload(String version, String type, MrBean user) {
		ContentData data = new ContentData();
		Connection conn = ConnAdmin.getConn("001", "");
		try {
			String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
			String todaytime = new SimpleDateFormat("hh:mm a").format(Calendar.getInstance().getTime());
			data.setT3(todaytime);
			data.setCreatedDate(todayDate);
			data.setModifiedDate(todayDate);
			if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
				data.setUserId(data.getUserId());
				data.setUserName(data.getUserName());
			} else {
				data.setUserId(user.getUser().getUserId());
				data.setUserName(user.getUser().getUserName());
			}
			data.setRecordStatus(1);
			data.setSyncStatus(1);
			data.setSyncBatch(0);
			data.setUserSyskey(0);
			data.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn("001", "")));
			data.setT1(version);
			data.setT2(type);
			new ContentMobileDao().insertdownload(data, conn);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
