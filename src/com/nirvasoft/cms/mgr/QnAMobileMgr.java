package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.nirvasoft.cms.dao.ArticleMobileDao;
import com.nirvasoft.cms.dao.CommentDao;
import com.nirvasoft.cms.dao.CommentMobileDao;
import com.nirvasoft.cms.dao.ContentMenuDao;
import com.nirvasoft.cms.dao.ContentMobileDao;
import com.nirvasoft.cms.dao.UploadMobileDao;
import com.nirvasoft.database.SysKeyMgr;
import com.nirvasoft.cms.dao.ArticleDao;
import com.nirvasoft.cms.dao.ArticleJunDao;
import com.nirvasoft.cms.dao.ArticleJunMobileDao;
import com.nirvasoft.cms.dao.CropDao;
import com.nirvasoft.cms.dao.OPTDao;
import com.nirvasoft.cms.dao.UploadDao;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.ResultMobile;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.PagerMobileData;
import com.nirvasoft.cms.shared.ArticleComboData;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.shared.ArticleDataSet;
import com.nirvasoft.cms.shared.ArticleJunData;
import com.nirvasoft.cms.shared.CropData;
import com.nirvasoft.cms.shared.UploadData;
import com.nirvasoft.cms.util.FontChangeUtil;
import com.nirvasoft.cms.util.ServerUtil;

public class QnAMobileMgr {
	/* click dilike question */
	public ResultMobile clickDisLikeQuestion(long id, String userSK, String type, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ResultMobile res = new ResultMobile();
		try {
			res = new ArticleMobileDao().updateDisLikeCount(id, userSK, type, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	///////////////////////////////////////////////////// Mobile///////////////////////////////////////////////////////

	/* click like question */
	public ResultMobile clickLikeQuestion(long id, String userSK, String type, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ResultMobile res = new ResultMobile();
		try {
			res = new ArticleMobileDao().updateLikeCount(id, userSK, type, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public ResultMobile deleteComment(long syskey, MrBean user) {

		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ResultMobile res = new ResultMobile();
		try {
			res = new CommentMobileDao().deleteComment(syskey, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public ResultMobile deletePrivatePost(ArticleData p, MrBean user) {
		ResultMobile res = new ResultMobile();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = new ArticleMobileDao().delete(p.getSyskey(), conn);
			if (res.isState()) {
				if (isUploadExit(p.getSyskey(), conn))
					res = new UploadMobileDao().deleteByUpdate(p.getSyskey(), conn);
			}
			if (res.isState()) {
				res = new ArticleJunMobileDao().deleteByUpdate(p.getSyskey(), conn);
			}
			if (res.isState()) {
				if (isExistLike(p.getSyskey(), conn))
					res = new ArticleMobileDao().deleteByUpdate(p.getSyskey(), conn);
			}
			if (res.isState()) {
				if (new ContentMobileDao().isPostExist(p.getSyskey(), conn)) {
					res = new ContentMobileDao().delete(p.getSyskey(), conn);
				}
			}
			if (res.isState()) {
				if (isExistComment(p.getSyskey(), conn))
					res = new ArticleMobileDao().deleteByUpdateComment(p.getSyskey(), conn);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return res;
	}

	public ResultMobile deleteReplyComment(long syskey, MrBean user) {

		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ResultMobile res = new ResultMobile();
		try {
			res = new CommentMobileDao().deleteReplyComment(syskey, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	/* deletePrivatePost */
	/*
	 * public Result deletePrivatePost(ArticleData p, MrBean user) { Result res
	 * = new Result(); Connection conn =
	 * ConnAdmin.getConn(user.getUser().getOrganizationID()); try { res =
	 * ArticleDao.delete(p.getSyskey(), conn); if (res.isState()) { res =
	 * UploadDao.deleteByUpdate(p.getSyskey(), conn); } if (res.isState()) { res
	 * = ArticleJunDao.deleteByUpdate(p.getSyskey(), conn); } if (res.isState())
	 * { if (ContentDao.isPostExist(p.getSyskey(), p.getT3(), conn)) { res =
	 * ContentDao.delete(p.getSyskey(), conn); } } } catch (SQLException e) {
	 * e.printStackTrace(); } finally { if (conn != null) { try { if
	 * (!conn.isClosed()) conn.close(); } catch (SQLException e) {
	 * e.printStackTrace(); } } } return res; }
	 */

	/* get all comment */
	public ArticleDataSet getCommentmobile(String id, String usersk, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		long n3 = 0;
		ArticleDataSet res = new ArticleDataSet();
		try {
			n3 = new ArticleMobileDao().searchCommentCount(Long.parseLong(id), conn);
			res = new CommentMobileDao().searchComments(id, usersk, conn);
			ArticleData[] dataArr = res.getData();
			for (ArticleData data : dataArr) {
				data.setN4(n3);
				data.setUpload(new UploadMobileDao().search(String.valueOf(data.getSyskey()), "", conn).getUploads());
			}

			res.setData(dataArr);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
	//atn
	   public  ArticleDataSet getComment(String id,String sessionKey, String usersk, MrBean user) {
         Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
         long n3 = 0;
         ArticleDataSet res = new ArticleDataSet();
         try {
             new ArticleDao();
			n3 = ArticleDao.searchCommentCount(Long.parseLong(id), conn);
             res = new ArticleDao().searchComments(id, usersk, conn);
             ArticleData[] dataArr = res.getData();
             for (ArticleData data : dataArr) {
                 data.setN4(n3);
                 new UploadDao();
				data.setUpload(UploadDao.search(String.valueOf(data.getSyskey()),"", conn).getUploads());
                 data.setN11(new CommentDao().getReplyCount(data.getSyskey(), conn));
             }
             res.setData(dataArr);
             res.setSessionState(true);

         } catch (SQLException e) {
             e.printStackTrace();
         }
         return res;
     }

	/* get all comment */
	public ArticleDataSet getCommentReplymobile(String id, String usersk, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		long n3 = 0;
		ArticleDataSet res = new ArticleDataSet();
		try {
			n3 = new ArticleMobileDao().searchCommentReplyCount(Long.parseLong(id), conn);
			res = new CommentMobileDao().searchCommentsReply(id, usersk, conn);
			ArticleData[] dataArr = res.getData();
			for (ArticleData data : dataArr) {
				data.setN4(n3);
				data.setUpload(new UploadMobileDao().search(String.valueOf(data.getSyskey()), "", conn).getUploads());
			}

			res.setData(dataArr);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public ArticleJunData initJUNData(ArticleJunData typeData, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (typeData.getSyskey() == 0) {
			typeData.setCreateddate(todayDate);
		}
		typeData.setModifieddate(todayDate);
		if (!typeData.getUserid().equals("") && !typeData.getUsername().equals("")) {
			typeData.setUserid(typeData.getUserid());
			typeData.setUsername(typeData.getUsername());
		} else {
			typeData.setUserid(user.getUser().getUserId());
			typeData.setUsername(user.getUser().getUserName());
		}
		typeData.setRecordStatus(1);
		typeData.setSyncStatus(1);
		typeData.setSyncBatch(0);
		typeData.setUsersyskey(0);
		return typeData;
	}

	/* isExistComment */
	private boolean isExistComment(long syskey, Connection conn) throws SQLException {
		boolean isExit = false;
		ResultSet rs = null;
		String sql = "Select * from FMR003 where n1='" + syskey + "'";
		PreparedStatement ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();
		if (rs.next()) {
			isExit = true;
		}
		return isExit;
	}

	public Boolean isExistCommentLike(long n1, long n2, Connection conn) {
		String sql = "select * from fmr017 where n1=" + n1 + " and n2=" + n2;
		PreparedStatement ps;
		int count = 0;
		try {
			ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				count++;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (count > 0) {
			return true;
		} else {
			return false;
		}

	}

	private boolean isExistLike(long syskey, Connection conn) throws SQLException {
		boolean isExit = false;
		ResultSet rs = null;
		String sql = "Select * from FMR008 where n1='" + syskey + "'";
		PreparedStatement ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();
		if (rs.next()) {
			isExit = true;
		}
		return isExit;
	}

	private boolean isUploadExit(long syskey, Connection conn) throws SQLException {
		boolean isExit = false;
		ResultSet rs = null;
		String sql = "";
		sql = "Select * from FMR006 where n1='" + syskey + "'";
		PreparedStatement ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();
		if (rs.next()) {
			isExit = true;
		}
		return isExit;
	}

	/* PrivateQuestionList */
	public ArticleDataSet PrivatePostList(PagerMobileData p, String userSK, String type, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleDataSet res = new ArticleDataSet();
		try {
			res = new ArticleMobileDao().PrivatePostList(p, userSK, type, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	public ArticleDataSet readForCommentNoti(ArticleData data, MrBean user) {
		ArticleDataSet dataSet = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			dataSet = new CommentMobileDao().searchCommentNotification(String.valueOf(data.getN1()),
					String.valueOf(data.getN5()), conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dataSet;
	}

	/* TDA for Reply Amin Notification */
	public ArticleDataSet readForCommentNotiAdmin(ArticleData data, MrBean user) {
		ArticleDataSet dataSet = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			dataSet = new CommentMobileDao().searchCommentNotificationAdminNoti(String.valueOf(data.getN1()),
					String.valueOf(data.getN5()), conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dataSet;
	}

	public ArticleDataSet readForCommentReplyNoti(ArticleData data, MrBean user) {
		ArticleDataSet dataSet = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			dataSet = new CommentMobileDao().searchCommentReplyNotification(String.valueOf(data.getN1()),
					String.valueOf(data.getN5()), conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dataSet;
	}

	public ArticleDataSet readForCommentReplyNotiAdmin(ArticleData data, MrBean user) {
		ArticleDataSet dataSet = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			dataSet = new CommentMobileDao().searchCommentReplyNotificationAdminNoti(String.valueOf(data.getN1()),
					String.valueOf(data.getN5()), conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dataSet;
	}

	public ArticleData readID(long id, MrBean user) {
		ArticleData res = null;
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = new ArticleMobileDao().readID(id, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	/* save comment new */
	/*public ArticleDataSet saveComment(ArticleData data, MrBean user) {
		ResultMobile res = new ResultMobile();
		ArticleDataSet dataSet = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			// conn.setAutoCommit(false);
			data = setStatusData(data, user);
			if (data.getSyskey() == 0) {
				long syskey = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
				data.setSyskey(syskey);
				res = new CommentMobileDao().insert(data, conn);
				if (res.isState()) {
					for (int i = 0; i < data.getUpload().length; i++) {
						boolean flag = false;
						UploadData obj = setUploadData(data.getUpload()[i], syskey, user);
						obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
								ConnAdmin.getConn(user.getUser().getOrganizationID())));
						flag = new UploadMobileDao().insert(obj, conn);
						res.setState(flag);
						if (!flag)
							break;
					}
				}
				if (res.isState()) {
					if (data.getN3() != -1)
						res = new ArticleMobileDao().updateCommentCount(data.getN1(), conn);
				}
			} else {
				res = new CommentMobileDao().update(data, conn);
				if (res.isState()) {
					new UploadMobileDao().deleteByUpdate(data.getSyskey(), conn);
					boolean flag = false;
					for (int i = 0; i < data.getUpload().length; i++) {
						UploadData obj = setUploadData(data.getUpload()[i], data.getSyskey(), user);
						obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
								ConnAdmin.getConn(user.getUser().getOrganizationID())));
						flag = new UploadMobileDao().insert(obj, conn);
						res.setState(flag);
						if (!flag)
							break;
					}
				}
			}
			long n3 = 0;
			if (res.isState()) {
				// dataSet =
				// CommentDao.searchComment(String.valueOf(data.getN1()),conn);
				n3 = new ArticleMobileDao().searchCommentCount(data.getN1(), conn);
				dataSet = new CommentMobileDao().searchComments1(String.valueOf(data.getN1()), String.valueOf(data.getN5()),
						conn);
				// dataSet =
				// QnAMgr.getCommentmobile(String.valueOf(data.getN1()),
				// String.valueOf(data.getN5()), user);
				ArticleData[] dataArr = dataSet.getData();
				for (ArticleData data1 : dataArr) {
					data1.setN4(n3);
					data1.setUpload(new UploadMobileDao().search(String.valueOf(data1.getSyskey()), "", conn).getUploads());
				}

				dataSet.setData(dataArr);
				// conn.commit();
			} 
				 * else { conn.rollback(); }
				 
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dataSet;
	}*/
	//atn
	 public ArticleDataSet saveComment(ArticleData data, MrBean user) {
		 Resultb2b res = new Resultb2b();
	        ArticleDataSet dataSet = new ArticleDataSet();
	        Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
	        try {
	            // session
	            //if (new SessionIDDao().isSessionExpire(data.getN5(), data.getSessionKey(), conn)) {
	                // conn.setAutoCommit(false);
	                data = setStatusData(data, user);
	                if (data.getSyskey() == 0) {
	                    long syskey = SysKeyMgr.getSysKey(1, "syskey",
	                            ConnAdmin.getConn(user.getUser().getOrganizationID()));
	                    data.setSyskey(syskey);
	                    res = CommentDao.insert(data, conn);
	                    if (res.isState()) {
	                        if (data.getUpload().length > 0) {
	                            for (int i = 0; i < data.getUpload().length; i++) {
	                                boolean flag = false;
	                                UploadData obj = setUploadData(data.getUpload()[i], syskey, user);
	                                obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
	                                        ConnAdmin.getConn(user.getUser().getOrganizationID())));
	                                new UploadDao();
									flag = UploadDao.insert(obj, conn);
	                                res.setState(flag);
	                                if (!flag)
	                                    break;
	                            }
	                        }
	                        if (data.getN3() != -1) {
								new ArticleDao();
								res = ArticleDao.updateCommentCount(data.getN1(), conn);
							}
	 
	                    }
	                } else {	                    
						res = CommentDao.update(data, conn);
	                    if (res.isState()) {
	                        UploadDao.deleteByUpdateforVideo(data.getSyskey(), conn);
	                        boolean flag = false;
	                        for (int i = 0; i < data.getUpload().length; i++) {
	                            UploadData obj = setUploadData(data.getUpload()[i], data.getSyskey(), user);
	                            obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
	                                    ConnAdmin.getConn(user.getUser().getOrganizationID())));
	                            flag =UploadDao.insert(obj, conn);
	                            res.setState(flag);
	                            if (!flag)
	                                break;
	                        }
	                    }
	                }
	                long n3 = 0;
	                if (res.isState()) {
	                    // dataSet = CommentDao.searchComment(String.valueOf(data.getN1()),conn);
	                    n3 =ArticleDao.searchCommentCount(data.getN1(), conn);
	                    dataSet = new CommentDao().searchComments1(String.valueOf(data.getN1()),
	                            String.valueOf(data.getN5()), conn);
	                    ArticleData[] dataArr = dataSet.getData();
	                    for (ArticleData data1 : dataArr) {
	                        data1.setN4(n3);
	                        data1.setUpload(UploadDao.search(String.valueOf(data1.getSyskey()), "", conn).getUploads());
	                        data1.setN11(new CommentDao().getReplyCount(data1.getSyskey(), conn));
	                    }
	 
	                    dataSet.setData(dataArr);
	                    // conn.commit();
	                } 
	                conn.close();
	                res.setSessionState(true);
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	        return dataSet;
	    }

	public ArticleDataSet saveCommentLike(ArticleData data, MrBean user) throws SQLException {

		ArticleDataSet dataSet = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		if (!isExistCommentLike(data.getN1(), data.getN2(), conn)) {
			data = setStatusData(data, user);
			long syskey = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
			data.setSyskey(syskey);
			dataSet = new CommentMobileDao().saveCommentLike(data, conn);
		} else {
			dataSet = new CommentMobileDao().updateCommentLike(data, conn);
		}
		return dataSet;
	}

	/* save comment new */
	public ArticleDataSet saveCommentReply(ArticleData data, MrBean user) {
		ResultMobile res = new ResultMobile();
		ArticleDataSet dataSet = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			// conn.setAutoCommit(false);
			data = setStatusData(data, user);
			if (data.getSyskey() == 0) {
				long syskey = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
				data.setSyskey(syskey);
				res = new CommentMobileDao().insertCommentReply(data, conn);
				if (res.isState()) {
					for (int i = 0; i < data.getUpload().length; i++) {
						boolean flag = false;
						UploadData obj = setUploadData(data.getUpload()[i], syskey, user);
						obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
								ConnAdmin.getConn(user.getUser().getOrganizationID())));
						flag = new UploadMobileDao().insert(obj, conn);
						res.setState(flag);
						if (!flag)
							break;
					}
				}
				if (res.isState()) {
					if (data.getN3() != -1)
						res = new ArticleMobileDao().updateReplyCommentCount(data.getN1(), conn);
				}
			} else {
				res = new CommentMobileDao().updateCommentReply(data, conn);
				if (res.isState()) {
					new UploadMobileDao().deleteByUpdate(data.getSyskey(), conn);
					boolean flag = false;
					for (int i = 0; i < data.getUpload().length; i++) {
						UploadData obj = setUploadData(data.getUpload()[i], data.getSyskey(), user);
						obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
								ConnAdmin.getConn(user.getUser().getOrganizationID())));
						flag = new UploadMobileDao().insert(obj, conn);
						res.setState(flag);
						if (!flag)
							break;
					}
				}
			}
			if (res.isState()) {
				dataSet = new CommentMobileDao().searchCommentReply(String.valueOf(data.getN1()), conn);
				// conn.commit();
			} else {
				// conn.rollback();
			}
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dataSet;
	}

	/* save question */
	public ArticleData saveQuestions(ArticleData data, MrBean user) {
		ResultMobile res = new ResultMobile();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleData ret = new ArticleData();
		CropData cdata = new CropData();
		String t4 = data.getT4();
		try {
			data = setStatusData(data, user);
			if (data.getSyskey() == 0) {
				long syskey = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
				data.setSyskey(syskey);
				res = new ArticleMobileDao().insert(data, conn);
				res.setMsgCode(String.valueOf(syskey));
				if (res.isState()) {
					ArticleJunData jun = new ArticleJunData();
					cdata = CropDao.readSyskey(t4, conn);
					jun = initJUNData(jun, user);
					jun.setSyskey(
							SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID())));
					jun.setN1(data.getSyskey());// fmr002 sk
					jun.setT1(cdata.getT2());// crop name
					jun.setN2(cdata.getSyskey());// crop sk
					res = new ArticleJunMobileDao().insertJUN(jun, conn);
					ret = new QnAMobileMgr().readID(syskey, user);
					for (int i = 0; i < data.getUpload().length; i++) {
						boolean flag = false;
						UploadData obj = setUploadData(data.getUpload()[i], syskey, user);
						obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
								ConnAdmin.getConn(user.getUser().getOrganizationID())));
						obj.setModifiedTime(data.getModifiedTime());
						obj.setCreatedTime(data.getCreatedTime());
						flag = new UploadMobileDao().insert(obj, conn);
						res.setState(flag);
						if (flag) {
							ret = new QnAMobileMgr().readID(syskey, user);
						}
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ret;
	}

	public ArticleDataSet searchQuestionList(PagerMobileData p, String mobile, String firstRefresh, String searchVal,
			String userSK, String check, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleDataSet res = new ArticleDataSet();
		try {
			res = new ArticleMobileDao().searchQnA(p, mobile, firstRefresh, searchVal, "question", check, userSK, conn, user);
			/*
			 * ArticleData[] dataArr = res.getData(); for (ArticleData data :
			 * dataArr) { data.setN6(ArticleDao.searchLikeOrNot(data.getN1(),
			 * userSK, conn));
			 * data.setN7(ContentMenuDao.searchSaveContentOrNot(userSK,data.
			 * getN1(), conn));
			 * data.setN9(ArticleDao.searchDisLikeOrNot(data.getN1(), userSK,
			 * conn));
			 * data.setUploadedPhoto(UploadDao.search(String.valueOf(data.
			 * getSyskey()),"", conn).getData());
			 * data.setComData(CommentDao.searchLatest(data.getN1(), conn));
			 * data.setAnswer(CommentDao.searchAnswer(data.getN1(), "answer",
			 * conn).getAnsData());
			 * data.setVideoUpload(UploadDao.searchVideo(String.valueOf(data.
			 * getN1()), conn).getVideoUpload());
			 * data.setVideoUpload(UploadDao.searchVideo(String.valueOf(data.
			 * getSyskey()), conn).getVideoUpload());
			 * data.setSyskey(data.getN1()); } res.setData(dataArr);
			 */
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	public ArticleData setStatusData(ArticleData data, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		String todaytime = new SimpleDateFormat("hh:mm a").format(Calendar.getInstance().getTime());
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(todaytime);
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(todaytime);
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		/*
		 * if (ServerUtil.isZawgyiEncoded(data.getT2())) {
		 * data.setT2(FontChangeUtil.zg2uni(data.getT2())); }
		 */
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		data.setT4("");
		return data;
	}

	public UploadData setUploadData(String url, long articleKey, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		// String todaytime = new SimpleDateFormat("hh:mm
		// a").format(Calendar.getInstance().getTime());
		UploadData data = new UploadData();
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(data.getCreatedTime());
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		data.setT1(url);
		data.setN1(articleKey);
		return data;
	}

	public UploadData setUploadDataMobile(UploadData data, long articleKey, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		// String todaytime = new SimpleDateFormat("hh:mm
		// a").format(Calendar.getInstance().getTime());

		// UploadData data = new UploadData();
		if (data.getSyskey() == 0) {
			try {
				data.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID())));
				data.setCreatedDate(todayDate);
				data.setCreatedTime(data.getCreatedTime());
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		data.setT1(data.getT1());
		data.setT2(data.getT2());
		data.setN1(articleKey);
		return data;
	}

	/* click undilike question */
	public ResultMobile UnDislikeQuestion(long id, long userSK, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ResultMobile res = new ResultMobile();
		try {
			res = new ArticleMobileDao().UnDislikeQuestion(id, userSK, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	/* click unlike question */
	public ResultMobile UnlikeQuestion(long id, long userSK, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ResultMobile res = new ResultMobile();
		try {
			res = new ArticleMobileDao().UnlikeQuestion(id, userSK, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	/* UpdatePrivatepost */
	public ArticleData UpdatePrivatepost(ArticleData data, MrBean user) {
		ResultMobile res = new ResultMobile();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleData ret = new ArticleData();
		ArticleJunData ar = new ArticleJunData();
		try {
			conn.setAutoCommit(false);
			data = setStatusData(data, user);
			if (data.getSyskey() != 0) {
				res = new ArticleMobileDao().update(data, conn);
				if (res.isState()) {
					res = new ArticleJunMobileDao().deleteByUpdate(data.getSyskey(), conn);
					if (res.isState()) {
						ArticleComboData[] arr = data.getCrop();// crop
						ar = initJUNData(ar, user);
						for (int i = 0; i < arr.length; i++) {
							ar.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
									ConnAdmin.getConn(user.getUser().getOrganizationID())));
							ar.setN1(data.getSyskey());
							ar.setT1(arr[i].getName());
							ar.setN2(Long.parseLong(arr[i].getId()));
							res = new ArticleJunMobileDao().insertJUN(ar, conn);
						}
					}
					if (res.isState()) {
						new UploadMobileDao().deleteByUpdate(data.getSyskey(), conn);
						boolean flag = false;
						for (int i = 0; i < data.getUpload().length; i++) {
							UploadData obj = setUploadData(data.getUpload()[i], data.getSyskey(), user);
							obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
									ConnAdmin.getConn(user.getUser().getOrganizationID())));
							obj.setModifiedTime(data.getModifiedTime());
							obj.setCreatedTime(data.getCreatedTime());
							flag = new UploadMobileDao().insert(obj, conn);
							res.setState(flag);
						}
					}
				}
			}
			if (res.isState()) {
				conn.commit();
			} // else
				// conn.rollback();
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return ret;
	}

	/* save content question detail list */
	public ArticleDataSet viewQuestions(String searchVal, String userSK, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleDataSet res = new ArticleDataSet();
		try {
			long postkey = OPTDao.searchByPostID(searchVal, userSK, conn);
			res = new ArticleMobileDao().viewByKey(searchVal, postkey, "question", conn);
			ArticleData[] dataArr = res.getData();
			for (ArticleData data : dataArr) {
				data.setUploadedPhoto(new UploadMobileDao().search(String.valueOf(data.getSyskey()), "", conn).getData());
				data.setN6(new ArticleMobileDao().searchLikesUser(data.getSyskey(), Long.parseLong(userSK), conn, user));
				data.setN7(new ContentMenuDao().searchSaveContentOrNot(userSK, data.getSyskey(), conn));
				data.setN9(new ArticleMobileDao().searchDisLikeOrNot(data.getSyskey(), userSK, conn));
				data.setAnswer(new CommentMobileDao().searchAnswer(data.getSyskey(), "answer", conn).getAnsData());
			}
			res.setData(dataArr);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}
}
