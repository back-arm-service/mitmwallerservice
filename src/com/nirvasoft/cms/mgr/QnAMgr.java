package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nirvasoft.cms.dao.ArticleDao;
import com.nirvasoft.cms.dao.ArticleJunDao;
import com.nirvasoft.cms.dao.CommentDao;
import com.nirvasoft.cms.dao.ContentDao;
import com.nirvasoft.cms.dao.OPTDao;
import com.nirvasoft.cms.dao.UploadDao;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Result;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.ArticleComboData;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.shared.ArticleDataSet;
import com.nirvasoft.cms.shared.ArticleJunData;
import com.nirvasoft.cms.shared.DivisionComboDataSet;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.cms.shared.UploadData;
import com.nirvasoft.cms.shared.UploadDataSet;
import com.nirvasoft.cms.util.FontChangeUtil;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.database.SysKeyMgr;

public class QnAMgr {

	/* deleteQuestion */
	public static Result deleteQuestion(Long postsk, String type, MrBean user) {
		Result res = new Result();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = ArticleDao.delete(postsk, conn);
			if (res.isState()) {
				if (isUploadExit(postsk, type, conn))
					res = UploadDao.deleteByUpdate(postsk, type, conn);
			}
			if (res.isState()) {
				if (isExistLike(postsk, conn))
					res = ArticleDao.deleteByUpdate(postsk, conn);
			}
			if (res.isState()) {
				if (ContentDao.isPostExist(postsk, type, conn)) {
					res = ContentDao.delete(postsk, conn);
				}
			}
			if (res.isState()) {
				if (isExistComment(postsk, conn))
					res = ArticleDao.deleteByUpdateComment(postsk, conn);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return res;
	}

	/* isExistComment */
	private static boolean isExistComment(long syskey, Connection conn) throws SQLException {
		boolean isExit = false;
		ResultSet rs = null;
		String sql = "Select * from FMR003 where n1='" + syskey + "'";
		PreparedStatement ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();
		if (rs.next()) {
			isExit = true;
		}
		return isExit;
	}

	/* searchQuesList */
	public static ArticleDataSet searchQuesList(PagerData p, long status, String statetype,MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleDataSet res = new ArticleDataSet();
		try {
			res = ArticleDao.searchList(p, status, statetype,conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static ArticleData readDataBySyskey(long key, MrBean user) {
		ArticleData res = new ArticleData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = ArticleDao.read(key, conn);
			res.setCropdata(new ArticleJunDao().search(String.valueOf(key), conn).getCrop());
			if (res.getN10() == 1 || res.getN10() == 2) {
				res.setUpload(UploadDao.search(String.valueOf(res.getSyskey()), "Video", conn).getUploads());
				res.setUploadlist(UploadDao.searchUploadList(String.valueOf(res.getSyskey()), conn));
			} else {
				res.setVideoUpload(UploadDao.searchVideoList(String.valueOf(res.getSyskey()), conn).getVideoUpload());
				res.setUpload(UploadDao.search(String.valueOf(res.getSyskey()), "Video", conn).getUploads());
				res.setUploadlist(UploadDao.searchUploadList(String.valueOf(res.getSyskey()), conn));
				res.setResizelist(UploadDao.search(String.valueOf(res.getSyskey()), "", conn).getUploads());
				if(res.getT3().equalsIgnoreCase("Video")) {
					res.setResizelist(UploadDao.search(String.valueOf(res.getSyskey()), "Video", conn).getUploads());
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	/* getRoleStatusCount */
	public static long getRoleStatusCount(int status, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		long count = 0;
		return count = ArticleDao.getRoleStatusCount(status, conn);
	}

	public static ArticleData readByID(long id, MrBean user) {
		ArticleData res = null;
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = ArticleDao.readByID(id, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	public static ArticleData readID(long id, MrBean user) {
		ArticleData res = null;
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = ArticleDao.readID(id, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static ArticleData setStatusData(ArticleData data, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(data.getCreatedTime());
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		if (ServerUtil.isZawgyiEncoded(data.getT2())) {
			data.setT2(FontChangeUtil.zg2uni(data.getT2()));
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		data.setT4("");
		return data;
	}

	public static UploadData setUploadData(String url, long articleKey, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		UploadData data = new UploadData();
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(data.getCreatedTime());
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		data.setT1(url);
		data.setN1(articleKey);
		return data;
	}

	public static UploadData setUploadDataMobile(UploadData data, long articleKey, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (data.getSyskey() == 0) {
			try {
				data.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID())));
				data.setCreatedDate(todayDate);
				data.setCreatedTime(data.getCreatedTime());
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		data.setT1(data.getT1());
		data.setT2(data.getT2());
		data.setN1(articleKey);
		return data;
	}

	public static Resultb2b clickUnlikeQuestion(long id, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		Resultb2b res = new Resultb2b();
		try {
			res = ArticleDao.updateUnlikeCount(id, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static ArticleDataSet saveComment(ArticleData data, MrBean user) {
		Resultb2b res = new Resultb2b();
		ArticleDataSet dataSet = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			conn.setAutoCommit(false);
			data = setStatusData(data, user);
			if (data.getSyskey() == 0) {
				long syskey = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
				data.setSyskey(syskey);
				res = CommentDao.insert(data, conn);
				if (res.isState()) {
					for (int i = 0; i < data.getUpload().length; i++) {
						boolean flag = false;
						UploadData obj = setUploadData(data.getUpload()[i], syskey, user);
						obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
								ConnAdmin.getConn(user.getUser().getOrganizationID())));
						flag = UploadDao.insert(obj, conn);
						res.setState(flag);
						if (!flag)
							break;
					}
				}
				if (res.isState()) {
					if (data.getN3() != -1)
						res = ArticleDao.updateCommentCount(data.getN1(), conn);
				}
			} else {
				res = CommentDao.update(data, conn);
				if (res.isState()) {
					UploadDao.deleteByUpdate(data.getSyskey(), data.getT3(), conn);
					boolean flag = false;
					for (int i = 0; i < data.getUpload().length; i++) {
						UploadData obj = setUploadData(data.getUpload()[i], data.getSyskey(), user);
						obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
								ConnAdmin.getConn(user.getUser().getOrganizationID())));
						flag = UploadDao.insert(obj, conn);
						res.setState(flag);
						if (!flag)
							break;
					}
				}
			}
			if (res.isState()) {
				dataSet = CommentDao.searchComment(String.valueOf(data.getN1()), conn);
				conn.commit();
			} else {
				conn.rollback();
			}
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dataSet;
	}

	public static ArticleDataSet saveAns(ArticleData data, MrBean user) {
		Resultb2b res = new Resultb2b();
		ArticleDataSet dataSet = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			conn.setAutoCommit(false);
			if (data.getSyskey() != 0) {
				res = ArticleDao.saveAns(data, conn);
				if (res.isState()) {
				}
			}
			if (res.isState()) {
				conn.commit();
			} else {
				conn.rollback();
			}
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dataSet;
	}

	public static ArticleDataSet convertToAns(ArticleData data, MrBean user) {
		Resultb2b res = new Resultb2b();
		ArticleDataSet dataSet = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());

		try {
			conn.setAutoCommit(false);
			data = setStatusData(data, user);
			if (data.getSyskey() != 0) {
				res = CommentDao.convertToAns(data, conn);
				if (res.isState()) {
					res.setMsgDesc("convert successfull");
				}
			}
			if (res.isState()) {
				dataSet = CommentDao.search(String.valueOf(data.getN1()), conn);
				conn.commit();
			} else {
				conn.rollback();
			}
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dataSet;
	}

	public static ArticleDataSet deleteAns(ArticleData data, MrBean user) {
		Resultb2b res = new Resultb2b();
		ArticleDataSet dataSet = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			conn.setAutoCommit(false);
			res = CommentDao.deleteAllByKey(data.getSyskey(), conn);// FMR003
			res = CommentDao.delete(data.getSyskey(), conn);
			if (res.isState())
				res = ArticleDao.reduceCommentCount(data.getN1(), conn);// FMR002
			if (res.isState()) {
				dataSet = CommentDao.search(String.valueOf(data.getN1()), conn);// FMR003
				conn.commit();
			} else {
				conn.rollback();
			}
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dataSet;
	}

	// TDA for Reply Amin Notification
	public static ArticleDataSet readForCommentNotiAdmin(ArticleData data, MrBean user) {
		ArticleDataSet dataSet = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			dataSet = CommentDao.searchCommentNotificationAdminNoti(String.valueOf(data.getN1()),
					String.valueOf(data.getN5()), conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dataSet;
	}

	/////////////////////////////// CMS////////////////////////////

	public static DivisionComboDataSet getStatusList(long status, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		DivisionComboDataSet res = new DivisionComboDataSet();
		try {
			res = ArticleDao.getStatusList(status, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static UploadDataSet searchImage(String id, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		UploadDataSet res = new UploadDataSet();
		try {
			res = UploadDao.search(id, "", conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static Resultb2b saveQuestion(ArticleData data, MrBean user) {
		Resultb2b res = new Resultb2b();
		ArticleJunData ar = new ArticleJunData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			conn.setAutoCommit(false);
			data = setStatusData(data, user);
			if (data.getSyskey() == 0) {
				long syskey = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
				data.setSyskey(syskey);
				data.setCreatedTime(data.getCreatedTime());
				data.setModifiedTime(data.getModifiedTime());
				res = ArticleDao.insert(data, conn);
				res.setMsgCode(String.valueOf(syskey));
				if (res.isState()) {
					ArticleComboData[] arr = data.getCrop();// crop
					ar = initJUNData(ar, user);
					if (ar.getSyskey() == 0) {
						for (int i = 0; i < arr.length; i++) {
							ar.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
									ConnAdmin.getConn(user.getUser().getOrganizationID())));
							ar.setN1(data.getSyskey());
							ar.setT1(arr[i].getName());
							ar.setN2(Long.parseLong(arr[i].getId()));
							res = ArticleJunDao.insertJUN(ar, conn);
						}
					}

					for (int i = 0; i < data.getUpload().length; i++) {
						boolean flag = false;
						UploadData obj = setUploadData(data.getUpload()[i], syskey, user);
						obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
								ConnAdmin.getConn(user.getUser().getOrganizationID())));
						obj.setModifiedTime(data.getModifiedTime());
						obj.setCreatedTime(data.getCreatedTime());
						obj.setT6(data.getT13());// resizefilename
						flag = UploadDao.insert(obj, conn);
						res.setState(flag);
						if (flag) {

						} else {
							break;
						}
					}
				}
				res.setKeyResult(syskey);
			} else {
				res = ArticleDao.update(data, conn);
				if (res.isState()) {
					res = ArticleJunDao.deleteByUpdate(data.getSyskey(), conn);
					UploadDao.deleteByUpdate(data.getSyskey(), data.getT3(), conn);
					boolean flag = false;
					ArticleComboData[] arr = data.getCrop();// crop
					ar = initJUNData(ar, user);
					for (int i = 0; i < arr.length; i++) {
						ar.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
								ConnAdmin.getConn(user.getUser().getOrganizationID())));
						ar.setN1(data.getSyskey());
						ar.setT1(arr[i].getName());
						ar.setN2(Long.parseLong(arr[i].getId()));
						res = ArticleJunDao.insertJUN(ar, conn);
					}
					for (int i = 0; i < data.getUpload().length; i++) {
						UploadData obj = setUploadData(data.getUpload()[i], data.getSyskey(), user);
						obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
								ConnAdmin.getConn(user.getUser().getOrganizationID())));
						obj.setModifiedTime(data.getModifiedTime());
						obj.setCreatedTime(data.getCreatedTime());
						obj.setT6(data.getT13());// resizefilename
						flag = UploadDao.insert(obj, conn);
						res.setState(flag);
					}
				}
			}
			if (res.isState()) {
				res.setKeyResult(data.getSyskey());
				res.getLongResult().add(data.getSyskey());
				conn.commit();
			} else
				conn.rollback();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return res;
	}

	private static boolean isExistLike(long syskey, Connection conn) throws SQLException {
		boolean isExit = false;
		ResultSet rs = null;
		String sql = "Select * from FMR008 where n1='" + syskey + "'";
		PreparedStatement ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();
		if (rs.next()) {
			isExit = true;
		}
		return isExit;
	}

	private static boolean isUploadExit(long syskey, String type, Connection conn) throws SQLException {
		boolean isExit = false;
		ResultSet rs = null;
		String sql = "";
		if (!type.equalsIgnoreCase("Video")) {
			sql = "Select * from FMR006 where n1='" + syskey + "'";
		} else {
			sql = "Select * from FMR006 where n2='" + syskey + "'";
		}
		PreparedStatement ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();
		if (rs.next()) {
			isExit = true;
		}
		return isExit;
	}

	public static Resultb2b clickLikeQuestion(long id, String userSK, String type, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		Resultb2b res = new Resultb2b();
		try {
			res = ArticleDao.updateLikeCount(id, userSK, type, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	public static ArticleDataSet getComments(String id, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleDataSet res = new ArticleDataSet();
		try {
			long userKey = OPTDao.searchByID(user.getUser().getUserId(), conn);
			res = CommentDao.searchComment(id, conn);
			ArticleData[] dataArr = res.getData();
			for (ArticleData data : dataArr) {
				data.setN7(userKey);
				data.setUpload(UploadDao.search(String.valueOf(data.getSyskey()), "", conn).getUploads());
			}
			res.setData(dataArr);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static ArticleDataSet saveAnswer(ArticleData data, MrBean user) {
		Resultb2b res = new Resultb2b();
		ArticleDataSet dataSet = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			conn.setAutoCommit(false);
			data = setStatusData(data, user);
			if (data.getSyskey() == 0) {
				long syskey = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
				data.setSyskey(syskey);
				data.setT3("answer");
				res = CommentDao.insert(data, conn);
				if (res.isState()) {
					if (data.getN3() != -1)
						res = ArticleDao.updateCommentCount(data.getN1(), conn);
				}

			} else {
				res = CommentDao.update(data, conn);
			}
			if (res.isState()) {
				dataSet = CommentDao.searchAns(String.valueOf(data.getN1()), "answer", conn);
				conn.commit();
			} else {
				conn.rollback();
			}
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dataSet;
	}

	public static ArticleDataSet readForCommentNoti(ArticleData data, MrBean user) {
		ArticleDataSet dataSet = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			dataSet = CommentDao.searchCommentNotification(String.valueOf(data.getN1()), String.valueOf(data.getN5()),
					conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dataSet;
	}

	public static ArticleJunData initJUNData(ArticleJunData typeData, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (typeData.getSyskey() == 0) {
			typeData.setCreateddate(todayDate);
		}
		typeData.setModifieddate(todayDate);
		if (!typeData.getUserid().equals("") && !typeData.getUsername().equals("")) {
			typeData.setUserid(typeData.getUserid());
			typeData.setUsername(typeData.getUsername());
		} else {
			typeData.setUserid(user.getUser().getUserId());
			typeData.setUsername(user.getUser().getUserName());
		}
		typeData.setRecordStatus(1);
		typeData.setSyncStatus(1);
		typeData.setSyncBatch(0);
		typeData.setUsersyskey(0);
		return typeData;
	}

	public static ArticleDataSet deleteComment(ArticleData data, MrBean user) {
		Resultb2b res = new Resultb2b();
		long n3 = 0;
		ArticleDataSet dataSet = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			conn.setAutoCommit(false);
			n3 = ArticleDao.searchCommentCount(data.getN1(), conn);
			res = CommentDao.deleteAllByKey(data.getSyskey(), conn);// FMR003
			res = CommentDao.delete(data.getSyskey(), conn);
			if (res.isState()) {
				if (n3 > 0) {
					res = ArticleDao.reduceCommentCount(data.getN1(), conn);// FMR002
				}
			}
			if (res.isState()) {
				dataSet = CommentDao.searchComment(String.valueOf(data.getN1()), conn);// FMR003
				conn.commit();
			} else {
				conn.rollback();
			}
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dataSet;
	}

	public static ArticleDataSet viewQuestion(String searchVal, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleDataSet res = new ArticleDataSet();
		try {
			long userKey = OPTDao.searchByID(user.getUser().getUserId(), conn);
			res = ArticleDao.view(searchVal, "question", conn);
			ArticleData[] dataArr = res.getData();
			for (ArticleData data : dataArr) {
				data.setUpload(UploadDao.search(String.valueOf(data.getSyskey()), "", conn).getUploads());
				data.setN6(ArticleDao.searchLikeUser(data.getSyskey(), conn, user));
				data.setAnswer(CommentDao.searchAnswer(data.getSyskey(), "answer", conn).getAnsData());
				data.setN7(userKey);
			}
			res.setData(dataArr);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	public static ArticleDataSet searchLike(long key, MrBean user) {
		ArticleDataSet res = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = ArticleDao.searchLike(key, "question", conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static ArticleDataSet searchLikeCount(String id, MrBean user) {
		ArticleDataSet res = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = ArticleDao.searchLikeCount(id, conn, user);
			ArticleData[] dataArr = res.getData();
			for (ArticleData data : dataArr) {
				data.setUpload(UploadDao.search(String.valueOf(data.getSyskey()), "video", conn).getUploads());//atn "video"
				data.setVideoUpload(UploadDao.searchVideoList(String.valueOf(data.getSyskey()), conn).getVideoUpload());
				data.setUploadlist(UploadDao.searchUploadList(String.valueOf(data.getSyskey()), conn));//add atn
								
			}
			res.setData(dataArr);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static ArticleDataSet saveConvertAns(ArticleData data, MrBean user) {
		Resultb2b res = new Resultb2b();
		ArticleDataSet dataSet = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		Resultb2b result = new Resultb2b();
		try {
			conn.setAutoCommit(false);
			data = setStatusData(data, user);
			result = CommentDao.convertToAns(data, conn);
			if (result.isState()) {
				data.setSyskey(0);
				if (data.getSyskey() == 0) {
					long syskey = SysKeyMgr.getSysKey(1, "syskey",
							ConnAdmin.getConn(user.getUser().getOrganizationID()));
					data.setSyskey(syskey);
					data.setT3("answer");
					res = CommentDao.insertConvertAns(data, conn);
				}

			}
			if (res.isState()) {
				dataSet = CommentDao.searchComment(String.valueOf(data.getN1()), conn);
				conn.commit();
			} else {
				conn.rollback();
			}
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dataSet;
	}

	public static ArticleDataSet getMenuCount(long status, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleDataSet res = new ArticleDataSet();
		res = ArticleDao.getMenuCount(status, conn);
		return res;
	}
	
	public static boolean isUploadFileExit(String filename,MrBean user) throws SQLException {
        Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
        boolean isExit = false;
        String sql = "Select * from FMR006 where t1='" + filename + "'";
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            isExit = true;
        }
        return isExit;
    }
	
	  public static String deleteCorrect(String fileName, MrBean user){
	        String res="";
	        Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
	        res=UploadDao.deleteCorrect(fileName, conn);
	        return res;
	    }
}
