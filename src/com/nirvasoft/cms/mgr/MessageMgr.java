package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.cms.dao.MessageDao;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.shared.BrandComboDataSet;

public class MessageMgr {
	/*
	 * public static MessageDataSet getLatestNoti(PagerData p, long userSK, long
	 * state, String lg, String date,String noti, MrBean user) { Connection conn
	 * = ConnAdmin.getConn(user.getUser().getOrganizationID()); MessageDataSet
	 * res = new MessageDataSet(); try { res = MessageDao.getLatestNoti(p,
	 * userSK, state, lg, date, noti, conn); } catch (SQLException e) {
	 * e.printStackTrace(); } return res; }
	 */

	/*
	 * public static StateDataSet getStatelist(String language, MrBean user) {
	 * Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
	 * StateDataSet res = new StateDataSet(); try { res =
	 * MessageDao.getStatelist(language, conn); } catch (SQLException e) {
	 * e.printStackTrace(); } return res; }
	 */

	public static BrandComboDataSet getDistrictList(String divcode, MrBean user) {
		BrandComboDataSet res = new BrandComboDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = MessageDao.getDistrictList(divcode, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return res;
	}

	public static BrandComboDataSet getStateGPSList(String gps, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		BrandComboDataSet res = new BrandComboDataSet();
		try {
			res = MessageDao.getStateGPSList(gps, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static BrandComboDataSet getStateTypelist(MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		BrandComboDataSet res = new BrandComboDataSet();
		try {
			res = MessageDao.getStateTypeCombolist(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static BrandComboDataSet getTownshipList(String divcode, MrBean user) {
		BrandComboDataSet res = new BrandComboDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = MessageDao.getTownshipList(divcode, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return res;
	}

	public static BrandComboDataSet getVillageList(String divcode, MrBean user) {
		BrandComboDataSet res = new BrandComboDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = MessageDao.getVillageList(divcode, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return res;
	}
}
