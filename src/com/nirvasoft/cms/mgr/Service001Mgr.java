package com.nirvasoft.cms.mgr;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;

import com.nirvasoft.cms.dao.Service001Dao;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.ButtonCarryData;
import com.nirvasoft.cms.shared.PasswordData;
import com.nirvasoft.cms.shared.RoleData;
import com.nirvasoft.cms.shared.ValueCaptionData;
import com.nirvasoft.cms.util.ServerUtil;

public class Service001Mgr {

	public static Resultb2b changePassword(PasswordData data, MrBean user) {
		Connection con = ConnAdmin.getConn(user.getUser().getOrganizationID());
		Resultb2b res = new Resultb2b();
		String oldpass = ServerUtil.encryptPIN(data.getOldpass());
		String newpass = ServerUtil.encryptPIN(data.getNewpass());
		data.setOldpass(oldpass);
		data.setNewpass(newpass);
		res = Service001Dao.changePassword(user, con, data);
		return res;

	}

	public static ButtonCarryData[] getBtns(MrBean user, String id) {
		ArrayList<ButtonCarryData> datalist = new ArrayList<ButtonCarryData>();
		try {
			Connection con = ConnAdmin.getConn(user.getUser().getOrganizationID());
			String[] arr = Service001Dao.getAllLinks(con);
			for (int i = 0; i < arr.length; i++) {
				ButtonCarryData obj = getRoleBtn(con, id, arr[i]);
				datalist.add(obj);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		ButtonCarryData[] data = new ButtonCarryData[datalist.size()];
		data = datalist.toArray(data);
		return data;
	}

	public static String[] getMainMenu(MrBean user, String userid) {
		Connection con = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArrayList<String> arr = Service001Dao.getMainMenu(user, con, userid);
		String[] ary = new String[arr.size()];
		for (int i = 0; i < arr.size(); i++) {
			ary[i] = arr.get(i);
		}
		return ary;

	}

	/*
	 * public static long getRole(long syskey, MrBean user)throws
	 * JsonGenerationException, JsonMappingException, IOException { long n2 = 0;
	 * Connection con = ConnAdmin.getConn(user.getUser().getOrganizationID());
	 * try { n2 = Service001Dao.getRole(syskey, con); return n2; } catch
	 * (SQLException e) { e.printStackTrace(); } finally {
	 * ServerUtil.closeConnection(con); } return n2; }
	 */
	public static RoleData getRole(long syskey, MrBean user)
			throws JsonGenerationException, JsonMappingException, IOException {
		RoleData res = new RoleData();
		Connection con = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = Service001Dao.getRole(syskey, con);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(con);
		}
		return res;
	}

	public static ButtonCarryData getRoleBtn(Connection con, String id, String link) {
		ButtonCarryData dt = new ButtonCarryData();
		String[] temp = Service001Dao.getBtnKeys(con, id, link);
		if (temp != null && temp.length > 0) {
			String s = seperateBtnForRoles(temp);
			dt.setDesc(s);
			dt.setLink(link);
		}
		return dt;
	}

	public static ValueCaptionData[] getSubMenuItem(MrBean user, String userid, String parent) {
		Connection con = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArrayList<ValueCaptionData> arr = Service001Dao.getSubMenuItem(user, con, userid, parent);
		ValueCaptionData[] ary = new ValueCaptionData[arr.size()];
		for (int i = 0; i < arr.size(); i++) {
			ary[i] = arr.get(i);
		}
		return ary;

	}

	public static ValueCaptionData login(MrBean user)
			throws JsonGenerationException, JsonMappingException, IOException {
		ValueCaptionData st = new ValueCaptionData();
		Connection con = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			st = Service001Dao.login(user, con);
			return st;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(con);
		}
		return st;
	}

	public static String seperateBtnForRoles(String[] btns) {
		String s = "";
		ArrayList<String> list = new ArrayList<String>();
		for (int i = 0; i < btns.length; i++) {
			String[] temp = btns[i].split(",");
			for (int j = 0; j < temp.length; j++) {
				if (!list.contains(temp[j])) {
					list.add(temp[j]);
				}
			}
		}
		for (int k = 0; k < list.size(); k++) {
			if (!s.equals("")) {
				s += ",";
			}
			s += list.get(k);
		}
		return s;
	}

}
