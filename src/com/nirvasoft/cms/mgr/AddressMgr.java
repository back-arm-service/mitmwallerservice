package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.cms.dao.AddressDao;
import com.nirvasoft.cms.dao.WardDao;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.AddressDataList;
import com.nirvasoft.cms.shared.AddressRefData;
import com.nirvasoft.cms.shared.AddressRefDataSet;
import com.nirvasoft.cms.shared.AdvancedSearchData;
import com.nirvasoft.cms.shared.Ref1;
import com.nirvasoft.cms.shared.WardData;
import com.nirvasoft.cms.shared.WardDataSet;

public class AddressMgr {

	public static AddressDataList addressSetuplist(AddressDataList data, MrBean user) {
		AddressDataList ret = new AddressDataList();
		try {
			Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
			String searchText = data.getSearchText();
			int pageSize = data.getPageSize();
			int currentPage = data.getCurrentPage();
			ret = WardDao.addressSetuplist(searchText, pageSize, currentPage, conn);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	public static Resultb2b deletevillage(String wardcode, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = WardDao.deletevillage(wardcode, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static Resultb2b delettownship(String code, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = AddressDao.delete(code, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static Ref1[] getDistinctbyDiv(String division, MrBean user) {
		Ref1[] ref = null;
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			ref = AddressDao.getDistinctbyDiv(division, conn);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ref;
	}

	public static Ref1[] getDivision(MrBean user) {
		Ref1[] ref = null;
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			ref = AddressDao.getDivision(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ref;
	}

	public static Ref1[] getTownshipByDistinct(String distinct, MrBean user) {
		Ref1[] ref = null;
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			ref = AddressDao.getTownshipByDistinct(distinct, conn);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return ref;
	}

	// village
	public static WardData readBySyskey(String code, MrBean user) {
		WardData res = new WardData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = WardDao.read(code, conn);
			String statesub = res.getWardcode().substring(0, 2);
			String state = statesub + "000000";
			res.setDivision(state);
			String distsub = res.getCode().substring(0, 5);
			String dist = distsub + "000";
			res.setDistinc(dist);
			res.setTownship(res.getCode());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static AddressRefData readDataBySyskey(String code, MrBean user) {
		AddressRefData res = new AddressRefData();
		AddressRefData resdata = new AddressRefData();
		AddressRefData resdist = new AddressRefData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = AddressDao.read(code, conn);
			String statesub = res.getCode().substring(0, 2);
			String state = statesub + "000000";
			resdata = AddressDao.readstate(state, conn);
			res.setDivision(resdata.getCode());
			String distsub = res.getCode().substring(0, 5);
			String dist = distsub + "000";
			resdist = AddressDao.readstate(dist, conn);
			res.setDistric(resdist.getCode());
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	public static AddressRefData saveTownship(AddressRefData aData, MrBean user) {
		try {
			Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
			if (aData.getCode().equalsIgnoreCase("TBA")) {
				// insert
				String towncode = AddressDao.getNewDistCode(aData.getDistric(), conn);
				aData.setCode(towncode);
				boolean res = AddressDao.saveTownship(aData, conn);
				if (res) {
					aData.setErrorcode("0000");
					aData.setErrormessage("Saved Sucessfully");
				} else {
					aData.setErrorcode("0050");
					aData.setErrormessage("Save Fail");
				}
			} else {
				// update
				boolean res = AddressDao.updateTownship(aData, conn);
				if (res) {
					aData.setErrorcode("0001");
					aData.setErrormessage("Updated Sucessfully");
				} else {
					aData.setErrorcode("0051");
					aData.setErrormessage("Update Fail");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			aData.setErrorcode("0050");
			aData.setErrormessage(e.getMessage());
		}
		return aData;
	}

	public static WardData saveWard(WardData aData, MrBean user) {
		try {
			Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
			if (aData.getWardcode().equalsIgnoreCase("TBA")) {
				// insert
				String wardcode = AddressDao.getNewwardCode(aData.getTownship(), conn);
				aData.setWardcode(wardcode);
				aData.setSyskey(Long.parseLong(WardDao.getmaxSyskey("AddressExRef", conn)));
				boolean res = WardDao.saveWard(aData, conn);
				if (res) {
					aData.setErrorcode("0000");
					aData.setErrormessage("Saved Sucessfully");
				} else {
					aData.setErrorcode("0050");
					aData.setErrormessage("Save Fail");
				}
			} else {
				// update
				boolean res = WardDao.updateWard(aData, conn);
				if (res) {
					aData.setErrorcode("0001");
					aData.setErrormessage("Updated Sucessfully");
				} else {
					aData.setErrorcode("0051");
					aData.setErrormessage("Update Fail");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			aData.setErrorcode("0050");
			aData.setErrormessage(e.getMessage());
		}
		return aData;
	}

	public static AddressRefDataSet searchtownship(AdvancedSearchData p, String searchVal, String sort, String type,
			MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		AddressRefDataSet res = new AddressRefDataSet();
		try {
			res = AddressDao.search(p, searchVal, sort, type, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	public static WardDataSet searchVillage(AdvancedSearchData p, String searchVal, String sort, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		WardDataSet res = new WardDataSet();
		try {
			res = WardDao.search(p, searchVal, sort, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

}
