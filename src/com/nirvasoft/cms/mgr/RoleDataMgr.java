package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.nirvasoft.cms.dao.RoleDao;
import com.nirvasoft.cms.dao.RoleMenuDao;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.MenuRole;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.cms.shared.RoleData;
import com.nirvasoft.cms.shared.RoleDataset;
import com.nirvasoft.cms.shared.RoleMenuData;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.database.SysKeyMgr;

public class RoleDataMgr {

	public static Resultb2b deleteRoleData(long syskey, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());

		try {

			Resultb2b ret = RoleDao.deleteRoleMenu(syskey, conn);
			if (ret.isState()) {
				res = RoleDao.delete(syskey, conn);
				res.setMsgDesc("Deleted successfully!");
			} else
				res.setMsgDesc("Can't Delete!");

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static List<RoleData> getAllRoleData(MrBean user) {
		List<RoleData> res = new ArrayList<RoleData>();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = RoleDao.getAllRoleData(user, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return res;
	}

	public static int getRoleCount(String searchVal, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		int res = 0;
		try {
			res = RoleDao.getRoleCount(searchVal, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static RoleData initData(RoleData data, MrBean user) {
		String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
		data.setUserId(user.getUser().getUserId());
		data.setUserName(user.getUser().getUserName());
		data.setModifiedDate(date);
		data.setRecordStatus(1);
		data.setSyncBatch(0);
		data.setSyncStatus(1);
		data.setT3(new String("51")); // for Module

		if (data.getSyskey() == 0) {
			data.setCreatedDate(date);
		}

		return data;
	}

	public static Resultb2b insertRoleMenu(MrBean user, RoleData ur, Connection conn) {
		Resultb2b res = new Resultb2b();
		try {

			for (int i = 0; i < ur.getMenu().length; i++) {
				if (ur.getMenu()[i].isResult()) {
					res = new Resultb2b();
					MenuRole obj = new MenuRole();
					String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
					obj.setSyskey(
							SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID())));
					obj.setUserId(user.getUser().getUserId());
					obj.setUserName(user.getUser().getUserName());
					obj.setCreatedDate(date);
					obj.setModifiedDate(date);
					obj.setRecordStatus(1);
					obj.setN1(ur.getSyskey()); // row syskey
					obj.setN2(ur.getMenu()[i].getSyskey()); // menu syskey
					for (int num = 0; num < ur.getBtnarr().length; num++) {
						if (ur.getMenu()[i].getSyskey() == ur.getBtnarr()[num].getSyskey()) {
							obj.setT1(ur.getBtnarr()[num].getDesc());
						}
					}
					res = RoleDao.insertRoleMenu(obj, conn);

					for (int j = 0; j < ur.getMenu()[i].getChildmenus().length; j++) {
						if (ur.getMenu()[i].getChildmenus()[j].isResult()) {
							res = new Resultb2b();
							obj = new MenuRole();
							date = new SimpleDateFormat("yyyyMMdd").format(new Date());
							obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
									ConnAdmin.getConn(user.getUser().getOrganizationID())));
							obj.setUserId(user.getUser().getUserId());
							obj.setUserName(user.getUser().getUserName());
							obj.setCreatedDate(date);
							obj.setModifiedDate(date);
							obj.setRecordStatus(1);
							obj.setN1(ur.getSyskey()); // row syskey
							obj.setN2(ur.getMenu()[i].getChildmenus()[j].getSyskey()); // menu
							for (int num = 0; num < ur.getBtnarr().length; num++) {
								if (ur.getMenu()[i].getChildmenus()[j].getSyskey() == ur.getBtnarr()[num].getSyskey()) {
									obj.setT1(ur.getBtnarr()[num].getDesc());
								}
							}
							res = RoleDao.insertRoleMenu(obj, conn);
						}
					}

				} else {
					res.setMsgDesc("Please select Menu button");
				}
			}

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		return res;
	}

	// read role by syskey... ymk fixed here
	public static RoleData readRoleBySyskey(long pKey, MrBean user) {
		RoleData res = new RoleData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = RoleDao.readRole(pKey, conn);
			RoleMenuData[] dataarray;
			dataarray = RoleMenuDao.getRoleMenuList(pKey, res.getSyskey(), conn);
			res.setMenu(dataarray);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static Resultb2b saveRoleData(RoleData data, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = saveRoleData(data, user, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public static Resultb2b saveRoleData(RoleData data, MrBean user, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		Resultb2b res1 = new Resultb2b();
		data = initData(data, user);
		if (data.getSyskey() == 0) {
			data.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID())));
			res = RoleDao.insert(data, conn);// UVM009
			if (res.isState()) {
				res1 = insertRoleMenu(user, data, conn);
				if (res1.isState()) {
					res1.setMsgDesc("Saved Successfully!");
				} else {
					res1.setMsgDesc("Can't Save! Please select Menu. ");
				}
			} else {
				res1.setMsgDesc("Code already exist!");
			}

		} else {
			res = RoleDao.update(data, conn);

			if (res.isState()) {
				res1 = updateRoleMenu(user, data, conn);
				if (res1.isState()) {
					res1.setMsgDesc("Updated Successfully!");
				}
			} else {
				res1.setMsgDesc("Code already exist!");
			}

		}
		if (res1.isState()) {
			res1.setKeyResult(data.getSyskey());
		}
		return res1;
	}

	public static RoleDataset searchRolebyValue(PagerData pager, String searchVal, Long status, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		RoleDataset res = new RoleDataset();
		try {
			res = RoleDao.searchRolebyValue(pager, searchVal, status, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static Resultb2b updateRoleMenu(MrBean user, RoleData ur, Connection conn) {
		Resultb2b res = new Resultb2b();
		try {
			res = RoleDao.updateRoleMenu(ur, conn);
			if (res.isState()) {
				res = insertRoleMenu(user, ur, conn);
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		return res;
	}

}