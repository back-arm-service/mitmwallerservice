package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nirvasoft.cms.dao.ArticleMobileDao;
import com.nirvasoft.cms.dao.CommentMobileDao;
import com.nirvasoft.cms.dao.ContentMenuDao;
import com.nirvasoft.cms.dao.SetupMobileDao;
import com.nirvasoft.cms.dao.UploadMobileDao;
import com.nirvasoft.database.SysKeyMgr;
import com.nirvasoft.cms.dao.ArticleJunDao;
import com.nirvasoft.cms.dao.OPTDao;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.ResultMobile;
import com.nirvasoft.cms.shared.PagerMobileData;
import com.nirvasoft.cms.shared.RegisterData;
import com.nirvasoft.cms.shared.RegisterDataSet;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.shared.ArticleDataSet;
import com.nirvasoft.cms.shared.SetupDataSet;
import com.nirvasoft.cms.shared.UploadData;
import com.nirvasoft.cms.util.FontChangeUtil;
import com.nirvasoft.cms.util.ServerUtil;

public class ArticleMobileMgr {	

	//////////////////////////////////////////// MObile////////////////////////////////////////////

	/* clickDisLikeArticle */
	public ResultMobile clickDisLikeArticle(long id, String userSK, String type, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ResultMobile res = new ResultMobile();
		try {
			res = new ArticleMobileDao().updateDisLikeCountArticle(id, userSK, type, conn, user);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}	

	/* clickLikeArticle */
	public ResultMobile clickLikeArticle(long postsk, String userSK, String type, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ResultMobile res = new ResultMobile();
		try {
			res = new ArticleMobileDao().updateLikeCountArticle(postsk, userSK, type, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	public ResultMobile clickLikeComment(long postkey, long comkey, long userSK, String type, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ResultMobile res = new ResultMobile();
		try {
			res = new CommentMobileDao().updateLikeCountComment(postkey, comkey, userSK, type, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	/* clickUnLikeComment */
	public ResultMobile clickUnLikeComment(long postkey, long comkey, long userSK, String type, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ResultMobile res = new ResultMobile();
		try {
			res = new CommentMobileDao().updateUnLikeCountComment(postkey, comkey, userSK, type, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	// getArticleListByContentWriter
	public ArticleDataSet getArticleDataBySyskey(long syskey, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleDataSet res = new ArticleDataSet();
		try {
			res = new ArticleMobileDao().getArticleDataBySyskey(syskey, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	// getArticleListByContentWriter
	public SetupDataSet getArticleListByContentWriter(String usersk, String userid, PagerMobileData data, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		SetupDataSet res = new SetupDataSet();
		try {
			res = SetupMobileDao.getArticleListByContentWriter(usersk, userid, conn, data, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	/* TDA for share */
	public ArticleData readForShare(long postsk, String type, MrBean user) {
		ArticleData res = new ArticleData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = new ArticleMobileDao().readForShare(postsk, type, conn);
			res.setCropdata(new ArticleJunDao().search(String.valueOf(postsk), conn).getCrop());
			res.setUploadedPhoto(new UploadMobileDao().search(String.valueOf(postsk), type, conn).getData());
			res.setVideoUpload(new UploadMobileDao().searchVideo(String.valueOf(postsk), conn).getVideoUpload());
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	public ArticleDataSet readShare(long postsk, String type, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleDataSet res = new ArticleDataSet();
		res = new ArticleMobileDao().readShare(postsk, type, conn);
		ArticleData[] dataArr = res.getData();
		for (ArticleData data : dataArr) {
			try {
				data.setUploadedPhoto(new UploadMobileDao().search(String.valueOf(postsk), type, conn).getData());
				data.setVideoUpload(
						new UploadMobileDao().searchVideo(String.valueOf(data.getSyskey()), conn).getVideoUpload());
				data.setComData(new CommentMobileDao().searchcomment(data.getSyskey(), conn));
				data.setAnswer(new CommentMobileDao().searchAnswer(data.getSyskey(), "answer", conn).getAnsData());
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return res;

	}

	/* saveComment */
	public ArticleDataSet saveComment(ArticleData data, MrBean user) {
		ResultMobile res = new ResultMobile();
		ArticleDataSet dataSet = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			data = setStatusData(data, user);
			if (data.getSyskey() == 0) {
				long syskey = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
				data.setSyskey(syskey);
				res = new CommentMobileDao().insert(data, conn);
				if (res.isState()) {
					for (int i = 0; i < data.getUpload().length; i++) {
						boolean flag = false;
						UploadData obj = setUploadData(data.getUpload()[i], syskey, user);
						obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
								ConnAdmin.getConn(user.getUser().getOrganizationID())));
						flag = new UploadMobileDao().insert(obj, conn);
						res.setState(flag);
						if (!flag)
							break;
					}
				}
				if (res.isState()) {
					if (data.getN3() != -1)
						res = new ArticleMobileDao().updateCommentCount(data.getN1(), conn);
				}

			} else {
				res = new CommentMobileDao().update(data, conn);
				if (res.isState()) {
					new UploadMobileDao().deleteByUpdate(data.getSyskey(), conn);
					boolean flag = false;
					for (int i = 0; i < data.getUpload().length; i++) {
						UploadData obj = setUploadData(data.getUpload()[i], data.getSyskey(), user);
						obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
								ConnAdmin.getConn(user.getUser().getOrganizationID())));
						flag = new UploadMobileDao().insert(obj, conn);
						res.setState(flag);
						if (!flag)
							break;
					}
				}
			}
			if (res.isState()) {
				dataSet = new CommentMobileDao().searchComment(String.valueOf(data.getN1()), conn);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return dataSet;
	}

	/* searchArticleList */
	public ArticleDataSet searchArticleList(PagerMobileData p, String mobile, String firstRefresh, String searchVal,
			String userSK, String type, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleDataSet res = new ArticleDataSet();
		try {
			res = new ArticleMobileDao().searchArticles(p, mobile, firstRefresh, searchVal, type, userSK, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	// searchArticleListByContentWriter
	public SetupDataSet searchArticleListByContentWriter(String usersk, String userid, String startdate, String enddate,
			PagerMobileData data, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		SetupDataSet res = new SetupDataSet();
		try {
			res = SetupMobileDao.searchArticleListByContentWriter(usersk, userid, startdate, enddate, conn, data, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	/* searchVideo */
	public ArticleDataSet searchVideo(PagerMobileData p, String mobile, String firstRefresh, String searchVal, String userSK,
			String check, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleDataSet res = new ArticleDataSet();
		try {
			res = new ArticleMobileDao().searchVideo(p, mobile, firstRefresh, searchVal, "Video", check, userSK, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	/* setStatusData */
	public ArticleData setStatusData(ArticleData data, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(data.getCreatedTime());
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		if (ServerUtil.isZawgyiEncoded(data.getT1())) {
			data.setT1(FontChangeUtil.zg2uni(data.getT1()));
		}
		if (ServerUtil.isZawgyiEncoded(data.getT2())) {
			data.setT2(FontChangeUtil.zg2uni(data.getT2()));
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		data.setVideoUpload(data.getVideoUpload());
		return data;
	}

	/* setUploadData */
	public UploadData setUploadData(String url, long articleKey, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		UploadData data = new UploadData();
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(data.getCreatedTime());
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		data.setT1(url);
		data.setN1(articleKey);
		return data;
	}

	/* UnDislikeArticle */
	public ResultMobile UnDislikeArticle(long id, long userSK, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ResultMobile res = new ResultMobile();
		try {
			res = new ArticleMobileDao().UnDislikeArticle(id, userSK, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	/* UnlikeArticle */
	public ResultMobile UnlikeArticle(long id, long userSK, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ResultMobile res = new ResultMobile();
		try {
			res = new ArticleMobileDao().UnlikeArticle(id, userSK, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	/* viewNews */
	public ArticleDataSet viewNews(String searchVal, String userSK, String type, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleDataSet res = new ArticleDataSet();
		try {
			long postkey = OPTDao.searchByPostID(searchVal, userSK, conn);
			res = new ArticleMobileDao().viewByKey(searchVal, postkey, type, conn);
			ArticleData[] dataArr = res.getData();
			for (ArticleData data : dataArr) {
				data.setUploadedPhoto(new UploadMobileDao().search(String.valueOf(data.getSyskey()), "", conn).getData());
				data.setN6(new ArticleMobileDao().searchLikesUser(data.getSyskey(), Long.parseLong(userSK), conn, user));
				data.setN7(new ContentMenuDao().searchSaveContentOrNot(userSK, data.getSyskey(), conn));
				data.setN9(new ArticleMobileDao().searchDisLikeOrNot(data.getSyskey(), userSK, conn));
			}
			res.setData(dataArr);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}
	public RegisterDataSet getArticleLikePerson(long id, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		RegisterDataSet res = new RegisterDataSet();
		try {
			res = new ArticleMobileDao().getArticleLikePerson(id,conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}
	public RegisterDataSet getCommentLikePerson(long id, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		RegisterDataSet res = new RegisterDataSet();
		try {
			res = new ArticleMobileDao().getCommentLikePerson(id,conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

}
