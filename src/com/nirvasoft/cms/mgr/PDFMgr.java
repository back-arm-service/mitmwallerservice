package com.nirvasoft.cms.mgr;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfDestination;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import com.nirvasoft.cms.shared.WalletTransData;
import com.nirvasoft.rp.framework.ServerSession;
import com.nirvasoft.rp.shared.UserDetailReportData;

class HeaderFooter extends PdfPageEventHelper {

	BaseFont baseFont;
	private PdfTemplate totalPages;
	ArrayList<String> pageTitle;
	private String printBy = "";
	private String printType = "";
	int pagenumber;

	public ArrayList<String> getPageTitle() {
		return pageTitle;
	}

	public String getPrintBy() {
		return printBy;
	}

	public String getPrintType() {
		return printType;
	}

	@Override
	public void onChapter(PdfWriter writer, Document document, float paragraphPosition, Paragraph title) {
		pagenumber = 1;
	}

	@Override
	public void onCloseDocument(PdfWriter writer, Document document) {
		totalPages.beginText();
		totalPages.setFontAndSize(baseFont, 8f);
		totalPages.setTextMatrix(0, 0);
		totalPages.showText(String.valueOf(writer.getPageNumber() - 1));
		totalPages.endText();

	}

	@Override
	public void onEndPage(PdfWriter writer, Document document) {
		Font smallCell = FontFactory.getFont("/fonts/tahoma.ttf", 7, Font.NORMAL);
		smallCell.setColor(new BaseColor(141, 141, 141));
		Rectangle rect = writer.getBoxSize("art");
		/// new

		PdfContentByte cb = writer.getDirectContent();
		cb.saveState();
		String text = String.format("Page %s of ", writer.getPageNumber());
		float textSize = baseFont.getWidthPoint(text, 8f);

		cb.beginText();
		cb.setFontAndSize(baseFont, 8f);
		cb.setColorFill(new BaseColor(141, 141, 141));
		cb.setTextMatrix(rect.getLeft() + 20, rect.getBottom() - 10);
		cb.showText(text);
		cb.endText();
		cb.addTemplate(totalPages, rect.getLeft() + 20 + textSize, rect.getBottom() - 10);
		cb.restoreState();

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy   hh:mm:ss aa");

		ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER,
				new Phrase(sdf.format(new Date()), smallCell), rect.getRight() - 80, rect.getBottom() - 10, 0);

		/*
		 * ColumnText.showTextAligned(writer.getDirectContent(),Element.
		 * ALIGN_CENTER, new Phrase(printBy,smallCell), (rect.getLeft() +
		 * rect.getRight()) / 2, rect.getBottom() - 10, 0);
		 */

		ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase(printBy, smallCell),
				((rect.getLeft() + rect.getRight()) / 2) - 150, rect.getBottom() - 10, 0);
		ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase(printType, smallCell),
				((rect.getLeft() + rect.getRight()) / 2) + 80, rect.getBottom() - 10, 0);
	}

	/*
	 * private static Font titleFont = FontFactory.getFont("/fonts/tahoma.ttf",
	 * 12, Font.BOLD); private static Font smallCell =
	 * FontFactory.getFont("/fonts/tahoma.ttf", 8, Font.NORMAL);
	 */
	@Override
	public void onOpenDocument(PdfWriter writer, Document document) {
		totalPages = writer.getDirectContent().createTemplate(100, 100);
		totalPages.setBoundingBox(new Rectangle(-20, -20, 100, 100));
		try {
			baseFont = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, true);
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onStartPage(PdfWriter writer, Document document) {
		pagenumber++;
		Font titleFont = FontFactory.getFont("/fonts/tahoma.ttf", 12, Font.BOLD);
		Font titleFont1 = FontFactory.getFont("/fonts/tahoma.ttf", 11, Font.BOLD);
		Rectangle rect = writer.getBoxSize("art");
		int top = 32;
		for (int i = 0; i < pageTitle.size(); i++) {

			Paragraph titleParagraph = null;
			if (i == pageTitle.size() - 1)
				titleParagraph = new Paragraph(pageTitle.get(i), titleFont1);
			else
				titleParagraph = new Paragraph(pageTitle.get(i), titleFont);

			if (pageTitle.get(pageTitle.size() - 1).equalsIgnoreCase("Balance Confirmation")) {
				top += 17;
				ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase(titleParagraph),
						(rect.getLeft() + rect.getRight() - 20) / 3, rect.getTop() - top, 0);
			} else {
				top += 12;
				ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase(titleParagraph),
						(rect.getLeft() + rect.getRight()) / 2, rect.getTop() - top, 0);
			}
		}
	}

	public void setPageTitle(ArrayList<String> pageTitle) {
		this.pageTitle = pageTitle;
	}

	public void setPrintBy(String printBy) {
		this.printBy = printBy;
	}

	public void setPrintType(String printType) {
		this.printType = printType;
	}

}

// *****************************************************************************//
public class PDFMgr {
	static DecimalFormat df = new DecimalFormat("#,##0.00");
	static BaseColor borderCol = new BaseColor(141, 141, 141);

	private static PdfPTable addContentUserReport(UserDetailReportData[] dataList) throws DocumentException {
		Font header = FontFactory.getFont("/fonts/tahoma.ttf", 8, Font.BOLD);
		Font smallCell = FontFactory.getFont("/fonts/tahoma.ttf", 8, Font.NORMAL);
		PdfPTable table = new PdfPTable(8);
		table.setWidthPercentage(100);
		table.setWidths(new int[] { 50, 100, 170, 150, 100, 100, 100, 100 });

		BaseColor hCol = new BaseColor(217, 238, 244);
		PdfPCell c1 = new PdfPCell(new Phrase("No.", header));// 1
		c1.setBorderColor(borderCol);
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		c1.setMinimumHeight(20f);
		c1.setBackgroundColor(hCol);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("User ID", header));// 2
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		c1.setBackgroundColor(hCol);
		c1.setBorderColor(borderCol);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("User Name", header));// 3
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		c1.setBackgroundColor(hCol);
		c1.setBorderColor(borderCol);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("NRC", header));// 4
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		c1.setBackgroundColor(hCol);
		c1.setBorderColor(borderCol);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Phone No", header));// 5
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		c1.setBackgroundColor(hCol);
		c1.setBorderColor(borderCol);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Created Date", header));// 6
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		c1.setBackgroundColor(hCol);
		c1.setBorderColor(borderCol);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Modified Date", header));// 7
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		c1.setBackgroundColor(hCol);
		c1.setBorderColor(borderCol);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("User Status", header));// 8
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		c1.setBackgroundColor(hCol);
		c1.setBorderColor(borderCol);
		table.addCell(c1);

		table.setHeaderRows(1);

		for (int i = 0; i < dataList.length; i++) {

			c1 = new PdfPCell(new Phrase(String.valueOf(i + 1), smallCell));// 1
			c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c1.setMinimumHeight(17f);
			c1.setBorderColor(borderCol);
			table.addCell(c1);

			c1 = new PdfPCell(new Phrase(dataList[i].getUserID(), smallCell));// 2
			c1.setHorizontalAlignment(Element.ALIGN_LEFT);
			c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c1.setBorderColor(borderCol);
			table.addCell(c1);

			c1 = new PdfPCell(new Phrase(dataList[i].getUsername(), smallCell));// 3
			c1.setHorizontalAlignment(Element.ALIGN_LEFT);
			c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c1.setBorderColor(borderCol);
			table.addCell(c1);

			c1 = new PdfPCell(new Phrase(dataList[i].getNrc(), smallCell));// 4
			c1.setHorizontalAlignment(Element.ALIGN_LEFT);
			c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c1.setBorderColor(borderCol);
			table.addCell(c1);

			c1 = new PdfPCell(new Phrase(dataList[i].getPhoneno(), smallCell));// 5
			c1.setHorizontalAlignment(Element.ALIGN_LEFT);
			c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c1.setBorderColor(borderCol);
			table.addCell(c1);

			c1 = new PdfPCell(new Phrase(dataList[i].getCreateddate(), smallCell));// 6
			c1.setHorizontalAlignment(Element.ALIGN_LEFT);
			c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c1.setBorderColor(borderCol);
			table.addCell(c1);

			c1 = new PdfPCell(new Phrase(dataList[i].getModifieddate(), smallCell));// 7
			c1.setHorizontalAlignment(Element.ALIGN_LEFT);
			c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c1.setBorderColor(borderCol);
			table.addCell(c1);

			c1 = new PdfPCell(new Phrase(dataList[i].getUserstatus(), smallCell));// 8
			c1.setHorizontalAlignment(Element.ALIGN_LEFT);
			c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c1.setBorderColor(borderCol);
			table.addCell(c1);
		}

		return table;
	}

	private static PdfPTable addWalletTopupReport(WalletTransData[] dataList) throws DocumentException {
		Font header = FontFactory.getFont("/fonts/tahoma.ttf", 8, Font.BOLD);
		Font smallCell = FontFactory.getFont("/fonts/tahoma.ttf", 8, Font.NORMAL);
		PdfPTable table = new PdfPTable(9);
		table.setWidthPercentage(100);
		table.setWidths(new int[] { 70, 220, 220, 200, 200, 270, 180, 180,180 });

		BaseColor hCol = new BaseColor(217, 238, 244);

		PdfPCell c1 = new PdfPCell(new Phrase("No.", header));// 1
		c1.setBorderColor(borderCol);
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		c1.setMinimumHeight(20f);
		c1.setBackgroundColor(hCol);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("From Name", header));// 2
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		c1.setBackgroundColor(hCol);
		c1.setBorderColor(borderCol);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("To Name", header));// 3
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		c1.setBackgroundColor(hCol);
		c1.setBorderColor(borderCol);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("From Account", header));// 4
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		c1.setBackgroundColor(hCol);
		c1.setBorderColor(borderCol);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("To Account", header));// 5
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		c1.setBackgroundColor(hCol);
		c1.setBorderColor(borderCol);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Transfer Date/Time", header));// 6
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		c1.setBackgroundColor(hCol);
		c1.setBorderColor(borderCol);
		table.addCell(c1);

		/*
		 * c1 = new PdfPCell(new Phrase("Transfer Type", header));//7
		 * c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		 * c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		 * c1.setBackgroundColor(hCol); c1.setBorderColor(borderCol);
		 * table.addCell(c1);
		 */

		c1 = new PdfPCell(new Phrase("Transfer ID", header));// 8
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		c1.setBackgroundColor(hCol);
		c1.setBorderColor(borderCol);
		table.addCell(c1);

		/*c1 = new PdfPCell(new Phrase("Ref key", header));// 9
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		c1.setBackgroundColor(hCol);
		c1.setBorderColor(borderCol);
		table.addCell(c1);*/

		c1 = new PdfPCell(new Phrase("Amount", header));// 10
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		c1.setBackgroundColor(hCol);
		c1.setBorderColor(borderCol);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Commission Charges", header));// 11
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		c1.setBackgroundColor(hCol);
		c1.setBorderColor(borderCol);
		table.addCell(c1);

		/*c1 = new PdfPCell(new Phrase("Currency", header));// 12
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		c1.setBackgroundColor(hCol);
		c1.setBorderColor(borderCol);
		table.addCell(c1);*/

		table.setHeaderRows(1);

		for (int i = 0; i < dataList.length; i++) {

			c1 = new PdfPCell(new Phrase(String.valueOf(i + 1), smallCell));// 1
			c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c1.setMinimumHeight(17f);
			c1.setBorderColor(borderCol);
			table.addCell(c1);

			c1 = new PdfPCell(new Phrase(dataList[i].getT1(), smallCell));// 2
			c1.setHorizontalAlignment(Element.ALIGN_LEFT);
			c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c1.setBorderColor(borderCol);
			table.addCell(c1);

			c1 = new PdfPCell(new Phrase(dataList[i].getT2(), smallCell));// 3
			c1.setHorizontalAlignment(Element.ALIGN_LEFT);
			c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c1.setBorderColor(borderCol);
			table.addCell(c1);

			c1 = new PdfPCell(new Phrase(dataList[i].getFromAccount(), smallCell));// 4
			c1.setHorizontalAlignment(Element.ALIGN_LEFT);
			c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c1.setBorderColor(borderCol);
			table.addCell(c1);

			c1 = new PdfPCell(new Phrase(dataList[i].getToAccount(), smallCell));// 5
			c1.setHorizontalAlignment(Element.ALIGN_LEFT);
			c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c1.setBorderColor(borderCol);
			table.addCell(c1);

			c1 = new PdfPCell(new Phrase(dataList[i].getTransDate(), smallCell));// 6
			c1.setHorizontalAlignment(Element.ALIGN_LEFT);
			c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c1.setBorderColor(borderCol);
			table.addCell(c1);

			/*
			 * c1 = new PdfPCell(new Phrase(dataList.get(i).getTransType(),
			 * smallCell));//7 c1.setHorizontalAlignment(Element.ALIGN_LEFT);
			 * c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			 * c1.setBorderColor(borderCol); table.addCell(c1);
			 */

			c1 = new PdfPCell(new Phrase(dataList[i].getTransID(), smallCell));// 8
			c1.setHorizontalAlignment(Element.ALIGN_LEFT);
			c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c1.setBorderColor(borderCol);
			table.addCell(c1);

			/*c1 = new PdfPCell(new Phrase(dataList.get(i).getMbankingkey(), smallCell));// 9
			c1.setHorizontalAlignment(Element.ALIGN_LEFT);
			c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c1.setBorderColor(borderCol);
			table.addCell(c1);*/

			c1 = new PdfPCell(new Phrase(dataList[i].getAmount(), smallCell));// 10
			c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c1.setBorderColor(borderCol);
			table.addCell(c1);

			c1 = new PdfPCell(new Phrase(dataList[i].getCommissionCharges(), smallCell));// 11
			c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c1.setBorderColor(borderCol);
			table.addCell(c1);

			/*c1 = new PdfPCell(new Phrase(dataList.get(i).getCurrencyCode(), smallCell));// 12
			c1.setHorizontalAlignment(Element.ALIGN_LEFT);
			c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c1.setBorderColor(borderCol);
			table.addCell(c1);*/
		}

		return table;
	}

	public static boolean printUserReport(UserDetailReportData[] dataList, String fileName) {
		boolean ret = true;
		ArrayList<String> pageTitle = new ArrayList<String>();
		pageTitle.add("User Report");
		com.itextpdf.text.Document document = new Document(PageSize.A4, 10, 10, 70, 30);
		try {
			PdfWriter writer = PdfWriter.getInstance(document,
					new FileOutputStream(ServerSession.serverPath + "download\\pdf\\" + fileName));
			HeaderFooter event = new HeaderFooter();
			event.setPageTitle(pageTitle);
			writer.setBoxSize("art", new Rectangle(0, 20, PageSize.A4.getWidth(), PageSize.A4.getHeight()));
			writer.setPageEvent(event);
			// to change Zoom
			PdfDestination pdfDest = new PdfDestination(PdfDestination.XYZ, 0, document.getPageSize().getHeight(), 1f);
			document.open();
			PdfAction action = PdfAction.gotoLocalPage(1, pdfDest, writer);
			writer.setOpenAction(action);

			PdfPTable table = addContentUserReport(dataList); // Fill Data Table
			table.setSpacingBefore(20.0f);
			table.setSpacingAfter(20.0f);
			document.add(table);
		} catch (Exception e) {
			e.printStackTrace();
			ret = false;
		}
		document.close();
		return ret;
	}

	public static boolean printWalletTopupReport(WalletTransData[] dataList, String fileName, String transtype) {
		boolean ret = true;
		ArrayList<String> pageTitle = new ArrayList<String>();
		if (transtype.equals("1"))
			pageTitle.add("Wallet Transfer Report");
		else
			pageTitle.add("Wallet Topup Report");
		com.itextpdf.text.Document document = new Document(PageSize.A4, 10, 10, 70, 30);
		try {
			PdfWriter writer = PdfWriter.getInstance(document,
					new FileOutputStream(ServerSession.serverPath + "download\\pdf\\" + fileName));
			HeaderFooter event = new HeaderFooter();
			event.setPageTitle(pageTitle);
			writer.setBoxSize("art", new Rectangle(0, 20, PageSize.A4.getWidth(), PageSize.A4.getHeight()));
			writer.setPageEvent(event);
			// to change Zoom
			PdfDestination pdfDest = new PdfDestination(PdfDestination.XYZ, 0, document.getPageSize().getHeight(), 1f);
			document.open();
			PdfAction action = PdfAction.gotoLocalPage(1, pdfDest, writer);
			writer.setOpenAction(action);

			PdfPTable table = addWalletTopupReport(dataList); // Fill Data Table
			table.setSpacingBefore(20.0f);
			table.setSpacingAfter(20.0f);
			document.add(table);
		} catch (Exception e) {
			e.printStackTrace();
			ret = false;
		}
		document.close();
		return ret;
	}

}
