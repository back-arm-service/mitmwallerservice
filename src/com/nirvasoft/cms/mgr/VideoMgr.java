package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nirvasoft.cms.dao.ArticleDao;
import com.nirvasoft.cms.dao.ArticleJunDao;
import com.nirvasoft.cms.dao.CommentDao;
import com.nirvasoft.cms.dao.OPTDao;
import com.nirvasoft.cms.dao.UploadDao;
import com.nirvasoft.cms.dao.VideoDao;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.shared.ArticleDataSet;
import com.nirvasoft.cms.shared.ArticleJunData;
import com.nirvasoft.cms.shared.DocumentData;
import com.nirvasoft.cms.shared.DocumentDataSet;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.cms.shared.PhotoUploadData;
import com.nirvasoft.cms.shared.UploadData;
import com.nirvasoft.cms.shared.VideoData;
import com.nirvasoft.cms.shared.VideoDataSet;
import com.nirvasoft.cms.util.FontChangeUtil;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.database.SysKeyMgr;

public class VideoMgr {
	public static Resultb2b clickLikeVideo(long id, String userSK, String type, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		Resultb2b res = new Resultb2b();
		try {
			res = VideoDao.updateLikeCountVideo(id, userSK, type, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	/* atn */
	public static Resultb2b deleteDoc(DocumentData data) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn("");
		try {
			data = setStatus(data);
			res = ArticleDao.deleteDoc(data, conn);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return res;
	}

	/* atn */
	public static DocumentData DocBysyskey(long key, MrBean user) {
		DocumentData res = new DocumentData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = ArticleDao.readDoc(key, conn);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static VideoData readDataBySyskey(long key, MrBean user) {
		VideoData res = new VideoData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = VideoDao.read(key, conn);
			res.setCropdata(new ArticleJunDao().search(String.valueOf(key), conn).getCrop());
			if (res.getN10() == 1) {
				res.setUpload(UploadDao.search(String.valueOf(res.getSyskey()), "Video", conn).getUploads());
			} else {
				res.setVideoUpload(UploadDao.searchVideoList(String.valueOf(res.getSyskey()), conn).getVideoUpload());
				res.setUpload(UploadDao.search(String.valueOf(res.getSyskey()), "Video", conn).getUploads());
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static ArticleDataSet saveComment(ArticleData data, MrBean user) {
		Resultb2b res = new Resultb2b();
		ArticleDataSet dataSet = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			data = ArticleMgr.setStatusData(data, user);
			if (data.getSyskey() == 0) {
				long syskey = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
				data.setSyskey(syskey);
				res = CommentDao.insert(data, conn);
				if (res.isState()) {
					if (data.getN3() != -1)
						res = ArticleDao.updateCommentCountforVideo(data.getN1(), conn);
				}
			} else {
				res = CommentDao.update(data, conn);
				if (res.isState()) {
					UploadDao.deleteByUpdate(data.getSyskey(), data.getT3(), conn);
					boolean flag = false;
					for (int i = 0; i < data.getUpload().length; i++) {
						UploadData obj = setUploadData(data.getUpload()[i], data.getSyskey(), user);
						obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
								ConnAdmin.getConn(user.getUser().getOrganizationID())));
						flag = UploadDao.insert(obj, conn);
						res.setState(flag);
						if (!flag)
							break;
					}
				}
			}
			if (res.isState()) {
				dataSet = CommentDao.searchComment(String.valueOf(data.getN1()), conn);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return dataSet;
	}

	// TDA
	public static ArticleDataSet saveComments(ArticleData data, MrBean user) {
		Resultb2b res = new Resultb2b();
		ArticleDataSet dataSet = new ArticleDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			data = ArticleMgr.setStatusData(data, user);
			if (data.getSyskey() == 0) {
				long syskey = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
				data.setSyskey(syskey);
				res = CommentDao.insert(data, conn);
				if (res.isState()) {
					if (data.getN3() != -1)
						res = ArticleDao.updateCommentCountforVideo(data.getN1(), conn);
				}
			} else {
				res = CommentDao.update(data, conn);
				if (res.isState()) {
					UploadDao.deleteByUpdate(data.getSyskey(), data.getT3(), conn);
					boolean flag = false;
					for (int i = 0; i < data.getVideoUpload().length; i++) {
						UploadData obj = setUploadData(data.getUpload()[i], data.getSyskey(), user);
						obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
								ConnAdmin.getConn(user.getUser().getOrganizationID())));
						flag = UploadDao.insert(obj, conn);
						res.setState(flag);
						if (!flag)
							break;
					}
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return dataSet;
	}

	/* atn */
	public static Resultb2b savedocument(DocumentData data) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn("");
		try {
			conn.setAutoCommit(false);
			data = setStatus(data);
			if (data.getAutokey() == 0) {
				res = ArticleDao.insert(data, conn);

			} else {
				res = ArticleDao.update(data, conn);
			}
			if (res.isState()) {
				res.setKeyResult(data.getAutokey());
				res.getLongResult().add(data.getAutokey());
				// res.setMsgDesc("Updated Successfully!");
				conn.commit();
			} else
				conn.rollback();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return res;
	}

	public static Resultb2b saveVideo(ArticleData data, String filePath, MrBean user) {
		Resultb2b res = new Resultb2b();
		Resultb2b res1 = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleJunData ar = new ArticleJunData();
		try {
			conn.setAutoCommit(false);
			data = setStatusDatas(data, user);
			if (data.getSyskey() == 0) {
				long syskey = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
				data.setSyskey(syskey);
				res = ArticleDao.insert(data, conn);
				if (res.isState()) {
					if (data.getN10() == 1) {
						for (int i = 0; i < data.getUploadlist().size(); i++) {
							boolean flag = false;
							UploadData obj = setUploadDataImage(data.getUploadlist().get(i), syskey, user);
							obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
									ConnAdmin.getConn(user.getUser().getOrganizationID())));
							obj.setModifiedTime(data.getModifiedTime());
							obj.setCreatedTime(data.getCreatedTime());
							// obj.setT6(data.getT13());//resizefilename
							flag = UploadDao.insert(obj, conn);
							res.setState(flag);
							res.setMsgDesc("Inserted Successfully!");
							if (flag) {

							} else {
								break;
							}
						}

					} else {
						if (res.isState()) {
							for (int i = 0; i < data.getVideoUpload().length; i++) {
								boolean flag = false;
								UploadData obj = new UploadData();
								obj = setUploadVideoData(data.getVideoUpload()[i], data.getResizelist()[i], "image",
										syskey, user);
								obj.setModifiedTime(data.getModifiedTime());
								obj.setCreatedTime(data.getCreatedTime());
								// obj.setT6(data.getT13());//resizefilename
								obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
										ConnAdmin.getConn(user.getUser().getOrganizationID())));
								flag = UploadDao.insert(obj, conn);
								res.setState(flag);
								res.setMsgDesc("Inserted Successfully!");
							}
						}
					}

				}

			} else {
				res = ArticleDao.update(data, conn);
				if (res.isState()) {
					/*
					 * res = ArticleJunDao.deleteByUpdate(data.getSyskey(),
					 * conn); if(res.isState()){ res =
					 * UploadDao.deleteByUpdateforVideo(data.getSyskey(), conn);
					 * }
					 */
					res1 = UploadDao.deleteByUpdateforVideo(data.getSyskey(), conn);
					// if(res.isState()){
					if (data.getN10() == 1) {
						for (int i = 0; i < data.getUploadlist().size(); i++) {
							boolean flag = false;
							UploadData obj = setUploadDataImage(data.getUploadlist().get(i), data.getSyskey(), user);
							obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
									ConnAdmin.getConn(user.getUser().getOrganizationID())));
							obj.setModifiedTime(data.getModifiedTime());
							obj.setCreatedTime(data.getCreatedTime());
							obj.setT6(data.getT13());// resizefilename
							flag = UploadDao.insert(obj, conn);
							res.setState(flag);
							res.setMsgDesc("Updated Successfully!");
							if (flag) {

							} else {
								break;
							}
						}
					} else {
						for (int i = 0; i < data.getVideoUpload().length; i++) {
							boolean flag = false;
							UploadData obj = new UploadData();
							obj = setUploadVideoData(data.getVideoUpload()[i], data.getResizelist()[i], "image",
									data.getSyskey(), user);
							obj.setModifiedTime(data.getModifiedTime());
							obj.setCreatedTime(data.getCreatedTime());
							// obj.setT6(data.getT13());//resizefilename
							obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
									ConnAdmin.getConn(user.getUser().getOrganizationID())));
							flag = UploadDao.insert(obj, conn);
							res.setState(flag);
							res.setMsgDesc("Updated Successfully!");
						}
					}

					// }
				}
			}
			if (res.isState()) {
				res.setKeyResult(data.getSyskey());
				res.getLongResult().add(data.getSyskey());
				// res.setMsgDesc("Updated Successfully!");
				conn.commit();
			} else
				conn.rollback();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return res;
	}

	public static Resultb2b saveVideo1(ArticleData data, String filePath, MrBean user) {
		Resultb2b res = new Resultb2b();
		Resultb2b res1 = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		// ArticleJunData ar = new ArticleJunData();
		try {
			conn.setAutoCommit(false);
			data = setStatusDatas(data, user);
			if (data.getSyskey() == 0) {
				long syskey = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
				data.setSyskey(syskey);
				res = ArticleDao.insert(data, conn);
				if (res.isState()) {
					if (data.getN10() == 1 || data.getN10() == 2) {
						for (int i = 0; i < data.getUploadlist().size(); i++) {
							boolean flag = false;
							UploadData obj = setUploadDataImage(data.getUploadlist().get(i), syskey, user);
							obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
									ConnAdmin.getConn(user.getUser().getOrganizationID())));
							obj.setModifiedTime(data.getModifiedTime());
							obj.setCreatedTime(data.getCreatedTime());
							// obj.setT6(data.getT13());//resizefilename
							obj.setT5(data.getT5());// region--hmh
							flag = UploadDao.insert(obj, conn);
							res.setState(flag);
							res.setMsgDesc("Inserted Successfully!");
							if (flag) {

							} else {
								break;
							}
						}
					} else if (data.getN10() == 0) {

						/*
						 * for (int i = 0; i < data.getUploadlist().size(); i++)
						 * { boolean flag = false; UploadData obj =
						 * setUploadDataImage(data.getUploadlist().get(i),
						 * syskey, user); obj.setSyskey(SysKeyMgr.getSysKey(1,
						 * "syskey",ConnAdmin.getConn(user.getUser().
						 * getOrganizationID())));
						 * obj.setModifiedTime(data.getModifiedTime());
						 * obj.setCreatedTime(data.getCreatedTime()); //
						 * obj.setT6(data.getT13());//resizefilename flag =
						 * UploadDao.insert(obj, conn); res.setState(flag);
						 * res.setMsgDesc("Inserted Successfully!"); if (flag) {
						 * 
						 * } else { break; } }
						 */
						for (int i = 0; i < data.getVideoUpload().length; i++) {
							boolean flag = false;
							UploadData obj = new UploadData();
							obj = setUploadVideoData(data.getVideoUpload()[i], data.getVideoUpload()[i], "image",
									syskey, user);
							if (data.getUploadlist().size() > 0) {
								obj.setT1(data.getUploadlist().get(i).getName());
								obj.setT7(data.getUploadlist().get(i).getUrl());
							}
							obj.setModifiedTime(data.getModifiedTime());
							obj.setCreatedTime(data.getCreatedTime());
							// obj.setT6(data.getT13());//resizefilename
							obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
									ConnAdmin.getConn(user.getUser().getOrganizationID())));
							obj.setT5(data.getT5());// region--hmh
							flag = UploadDao.insert(obj, conn);
							res.setState(flag);
							res.setMsgDesc("Inserted Successfully!");
						}
					}

				}

			} else {
				res = ArticleDao.update(data, conn);
				if (res.isState()) {
					/*
					 * res = ArticleJunDao.deleteByUpdate(data.getSyskey(),
					 * conn); if(res.isState()){ res =
					 * UploadDao.deleteByUpdateforVideo(data.getSyskey(), conn);
					 * }
					 */
					res1 = UploadDao.deleteByUpdateforVideo(data.getSyskey(), conn);
					// if(res.isState()){
					if (data.getN10() == 1 || data.getN10() == 2) {
						for (int i = 0; i < data.getUploadlist().size(); i++) {
							boolean flag = false;
							UploadData obj = setUploadDataImage(data.getUploadlist().get(i), data.getSyskey(), user);
							obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
									ConnAdmin.getConn(user.getUser().getOrganizationID())));
							obj.setModifiedTime(data.getModifiedTime());
							obj.setCreatedTime(data.getCreatedTime());
							obj.setT6(data.getT13());// resizefilename
							obj.setT5(data.getT5());// region--hmh
							flag = UploadDao.insert(obj, conn);
							res.setState(flag);
							res.setMsgDesc("Updated Successfully!");
							if (flag) {

							} else {
								break;
							}
						}
					} else {
						for (int i = 0; i < data.getVideoUpload().length; i++) {
							boolean flag = false;
							UploadData obj = new UploadData();
							obj = setUploadVideoData(data.getVideoUpload()[i], data.getResizelist()[i], "image",
									data.getSyskey(), user);
							if (data.getUploadlist().size() > 0) {
								obj.setT1(data.getUploadlist().get(i).getName());
								obj.setT7(data.getUploadlist().get(i).getUrl());
							}
							obj.setModifiedTime(data.getModifiedTime());
							obj.setCreatedTime(data.getCreatedTime());
							// obj.setT6(data.getT13());//resizefilename
							obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
									ConnAdmin.getConn(user.getUser().getOrganizationID())));
							obj.setT5(data.getT5());// region--hmh
							flag = UploadDao.insert(obj, conn);
							res.setState(flag);
							res.setMsgDesc("Updated Successfully!");
						}
					}

					// }
				}
			}
			if (res.isState()) {
				res.setKeyResult(data.getSyskey());
				res.getLongResult().add(data.getSyskey());
				// res.setMsgDesc("Updated Successfully!");
				conn.commit();
			} else
				conn.rollback();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return res;
	}

	/* atn */
	public static DocumentDataSet searchDocumentList(PagerData p, String userid, String statustype, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		DocumentDataSet res = new DocumentDataSet();
		try {
			res = ArticleDao.searchDocumentList(p, userid, statustype, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static VideoDataSet searchLike(String type, long key, MrBean user) {
		VideoDataSet res = new VideoDataSet();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = VideoDao.searchLikeVideo(type, key, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	// atn
	public static DocumentData setStatus(DocumentData data) {
		// String todayDate = new SimpleDateFormat("yyyyMMdd").format(new
		// Date());
		String todayDate = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
		if (data.getAutokey() == 0) {
			data.setCreatedDate(todayDate);
		}
		data.setModifiedDate(todayDate);
		if (data.getAutokey() == 0) {
			data.setModifiedby("");
		} else {
			data.setModifiedby(data.getModifiedby());
		}
		/*
		 * if (ServerUtil.isZawgyiEncoded(data.getT1())) {
		 * data.setT1(FontChangeUtil.zg2uni(data.getT1()));
		 * 
		 * }
		 */
		data.setRecordStatus(1);
		return data;
	}

	/*
	 * public static Result deleteVideo(Long key, MrBean user) { Result res =
	 * new Result(); Connection conn =
	 * ConnAdmin.getConn(user.getUser().getOrganizationID()); try { res =
	 * VideoDao.delete(key, conn); if (res.isState()) { res =
	 * ArticleJunDao.deleteByUpdate(key, conn); } if (res.isState()) { res =
	 * UploadDao.deleteByUpdateforVideo(key, conn); } if (res.isState()) {
	 * if(isExistLike(key, conn)) res = ArticleDao.deleteByUpdate(key, conn); }
	 * } catch (SQLException e) { e.printStackTrace(); } return res; }
	 */

	/*
	 * private static boolean isExistLike(long syskey, Connection conn) throws
	 * SQLException { boolean isExit = false; ResultSet rs = null; String sql =
	 * "Select * from FMR008 where n1='" + syskey + "'"; PreparedStatement ps =
	 * conn.prepareStatement(sql); rs = ps.executeQuery(); if (rs.next()) {
	 * isExit = true; } return isExit; }
	 */

	public static VideoData setStatusData(VideoData data, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(data.getCreatedTime());
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		return data;
	}

	public static ArticleData setStatusDatas(ArticleData data, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(data.getCreatedTime());
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}

		if (ServerUtil.isZawgyiEncoded(data.getT1())) {
			data.setT1(FontChangeUtil.zg2uni(data.getT1()));

		}

		if (ServerUtil.isZawgyiEncoded(data.getT2())) {
			data.setT2(FontChangeUtil.zg2uni(data.getT2()));

		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);

		data.setVideoUpload(data.getVideoUpload());
		return data;
	}

	public static UploadData setUploadData(String url, long articleKey, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		UploadData data = new UploadData();
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(data.getCreatedTime());
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());

		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		data.setT2(url);
		data.setN2(articleKey);
		return data;
	}

	/* for image */
	public static UploadData setUploadDataImage(PhotoUploadData pdata, long articleKey, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		UploadData data = new UploadData();
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(data.getCreatedTime());
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());

		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		data.setT1(pdata.getName());
		data.setT7(pdata.getUrl());
		data.setN2(articleKey);
		return data;
	}

	/*
	 * public static ArticleDataSet searchvideoAdminvideo(PagerData p, String
	 * searchVal, MrBean user) { Connection conn =
	 * ConnAdmin.getConn(user.getUser().getOrganizationID()); ArticleDataSet res
	 * = new ArticleDataSet(); try { res = ArticleDao.searchArticle(p,
	 * searchVal,"Video", conn); ArticleData[] dataArr = res.getData(); for
	 * (ArticleData data : dataArr) {
	 * data.setModifiedDate(ArticleDao.ddMMyyyFormat(data.getModifiedDate()));
	 * data.setVideoUpload(UploadDao.searchAdminVideo(String.valueOf(data.
	 * getSyskey()), conn).getVideoUpload()); } res.setData(dataArr); } catch
	 * (SQLException e) { e.printStackTrace(); } return res;
	 * 
	 * }
	 */

	/*
	 * public static ArticleDataSet viewNews(String searchVal, String userSK,
	 * MrBean user) { Connection conn =
	 * ConnAdmin.getConn(user.getUser().getOrganizationID()); ArticleDataSet res
	 * = new ArticleDataSet(); try { long postkey =
	 * OPTDao.searchByPostID(searchVal, userSK, conn); res =
	 * ArticleDao.viewByKey(searchVal, postkey, "Video", conn); ArticleData[]
	 * dataArr = res.getData(); for (ArticleData data : dataArr) {
	 * //data.setUpload(UploadDao.searchImg(String.valueOf(data.getSyskey()),
	 * conn).getUploads());
	 * data.setUploadedPhoto(UploadDao.search(String.valueOf(data.getSyskey()),
	 * "Video", conn).getData());
	 * data.setVideoUpload(UploadDao.searchVideoList(String.valueOf(data.
	 * getSyskey()), conn).getVideoUpload());
	 * data.setN6(ArticleDao.searchLikesUser(data.getSyskey(),
	 * Long.parseLong(userSK), conn, user));
	 * data.setN7(ContentMenuDao.searchSaveContentOrNot(userSK,data.getSyskey(),
	 * conn)); data.setN9(ArticleDao.searchDisLikeOrNot(data.getSyskey(),
	 * userSK, conn)); } res.setData(dataArr); } catch (SQLException e) {
	 * e.printStackTrace(); } return res;
	 * 
	 * }
	 */

	public static UploadData setUploadVideoData(String url, String filename, String imgurl, long articleKey,
			MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		UploadData data = new UploadData();
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(data.getCreatedTime());
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		// data.setT1(filename);
		data.setT2(url);
		data.setN2(articleKey);
		return data;
	}

	// convert canvas to image file for video file (WTZA)
	/*
	 * public static Boolean writeToFile(String data, String
	 * uploadedFileLocation) { BASE64Decoder decoder = new BASE64Decoder();
	 * byte[] imgBytes; try { imgBytes = decoder.decodeBuffer(data);
	 * BufferedImage bufImg = ImageIO.read(new ByteArrayInputStream(imgBytes));
	 * File imgOutFile = new File(uploadedFileLocation); ImageIO.write(bufImg,
	 * "png", imgOutFile); return true; } catch (IOException e) {
	 * e.printStackTrace(); } return false;
	 * 
	 * }
	 */
	////////////////////////////////////////////// CMS////////////////////
	public static ArticleDataSet viewVideo(String searchVal, MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		ArticleDataSet res = new ArticleDataSet();
		try {
			long userKey = OPTDao.searchByID(user.getUser().getUserId(), conn);
			res = ArticleDao.viewArticle(searchVal, conn);
			ArticleData[] dataArr = res.getData();
			for (ArticleData data : dataArr) {
				data.setVideoUpload(
						UploadDao.searchAdminVideo(String.valueOf(data.getSyskey()), conn).getVideoUpload());
				data.setUpload(UploadDao.search(String.valueOf(data.getSyskey()), "video", conn).getUploads());
				data.setUploadlist(UploadDao.searchUploadList(String.valueOf(data.getSyskey()), conn));
				data.setN6(ArticleDao.searchLikeUser(data.getSyskey(), conn, user));
				data.setN7(userKey);
			}
			res.setData(dataArr);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

}
