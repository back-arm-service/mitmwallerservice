package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.nirvasoft.cms.dao.DocumentDao;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.data.DocumentResponseData;
import com.nirvasoft.rp.shared.DocumentData;
import com.nirvasoft.rp.shared.DocumentListResponseData;
import com.nirvasoft.rp.shared.DocumentRequest;
import com.nirvasoft.rp.shared.DocumentResponse;

public class DocumentMgr {

	public DocumentListResponseData getAllPdf() {
		DocumentListResponseData res = new DocumentListResponseData();
		DAOManager dao = new DAOManager();
		Connection conn = null;
		DocumentDao userDAO = new DocumentDao();
		List<DocumentResponseData> reslist = null;

		try {
			conn = dao.openConnection();
			reslist = userDAO.getAllPdf(conn);
		} catch (Exception e) {
			reslist = null;

			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				reslist = null;

				e.printStackTrace();
			}
		}

		if (reslist != null && reslist.size() != 0) {
			DocumentResponseData resArr[] = new DocumentResponseData[reslist.size()];

			for (int i = 0; i < reslist.size(); i++) {
				DocumentResponseData response = new DocumentResponseData();

				response.setAutokey(reslist.get(i).getAutokey());
				response.setCreateddate(reslist.get(i).getCreateddate());
				response.setModifieddate(reslist.get(i).getModifieddate());
				response.setFilename(reslist.get(i).getFilename());
				response.setUserid(reslist.get(i).getUserid());
				response.setStatus(reslist.get(i).getStatus());
				response.setRecordstatus(reslist.get(i).getRecordstatus());
				response.setFiletype(reslist.get(i).getFiletype());
				response.setRegion(reslist.get(i).getRegion());
				response.setFilepath(reslist.get(i).getFilepath());
				response.setPostedby(reslist.get(i).getPostedby());
				response.setModifiedby(reslist.get(i).getModifiedby());
				response.setFilesize(reslist.get(i).getFilesize());
				response.setPages(reslist.get(i).getPages());
				response.setTitle(reslist.get(i).getTitle());
				response.setT1(reslist.get(i).getT1());
				response.setT2(reslist.get(i).getT2());
				response.setT3(reslist.get(i).getT3());
				response.setT4(reslist.get(i).getT4());
				response.setT5(reslist.get(i).getT5());
				response.setN1(reslist.get(i).getN1());
				response.setN2(reslist.get(i).getN2());
				response.setN3(reslist.get(i).getN3());
				response.setN4(reslist.get(i).getN4());
				response.setN5(reslist.get(i).getN5());
				resArr[i] = response;
			}

			res.setCode("0000");
			res.setDesc("Getting Document List Success.");
			res.setResList(resArr);
		} else {
			res.setCode("0014");
			res.setDesc("No Document Found!");
		}

		return res;
	}

	public DocumentResponse getAllPdfByRegion(DocumentRequest req) {
		DocumentResponse res = new DocumentResponse();
		DAOManager dao = new DAOManager();
		Connection conn = null;
		DocumentDao daoDoc = new DocumentDao();
		List<DocumentData> pdfList = null;

		try {
			conn = dao.openConnection();
			pdfList = daoDoc.getAllPdfByRegion(req, conn);
		} catch (Exception e) {
			pdfList = null;

			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				pdfList = null;

				e.printStackTrace();
			}
		}

		if (pdfList != null && pdfList.size() != 0) {
			DocumentData pdfListArr[] = new DocumentData[pdfList.size()];

			for (int i = 0; i < pdfList.size(); i++) {
				DocumentData doc = new DocumentData();

				doc.setAutoKey(pdfList.get(i).getAutoKey());
				doc.setCreatedDate(pdfList.get(i).getCreatedDate());
				doc.setModifiedDate(pdfList.get(i).getModifiedDate());
				doc.setName(pdfList.get(i).getName());
				doc.setUserId(pdfList.get(i).getUserId());
				doc.setStatus(pdfList.get(i).getStatus());
				doc.setRecordStatus(pdfList.get(i).getRecordStatus());
				doc.setFileType(pdfList.get(i).getFileType());
				doc.setRegion(req.getRegion());
				doc.setLink(pdfList.get(i).getLink());
				doc.setPostedBy(pdfList.get(i).getPostedBy());
				doc.setModifiedBy(pdfList.get(i).getModifiedBy());
				doc.setFileSize(pdfList.get(i).getFileSize());
				doc.setPages(pdfList.get(i).getPages());
				doc.setTitle(pdfList.get(i).getTitle());
				doc.setKeyword(pdfList.get(i).getKeyword());
				doc.setT2(pdfList.get(i).getT2());
				doc.setT3(pdfList.get(i).getT3());
				doc.setT4(pdfList.get(i).getT4());
				doc.setT5(pdfList.get(i).getT5());
				doc.setN1(pdfList.get(i).getN1());
				doc.setN2(pdfList.get(i).getN2());
				doc.setN3(pdfList.get(i).getN3());
				doc.setN4(pdfList.get(i).getN4());
				doc.setN5(pdfList.get(i).getN5());

				pdfListArr[i] = doc;
			}

			res.setCode("0000");
			res.setDesc("Getting Document List Success.");
			res.setPdfList(pdfListArr);
		} else {
			res.setCode("0014");
			res.setDesc("No Document Found!");
		}

		return res;
	}

	public DocumentResponse getKeyword(DocumentRequest req) {
		DocumentResponse res = new DocumentResponse();
		DAOManager dao = new DAOManager();
		Connection conn = null;
		DocumentDao daoDoc = new DocumentDao();
		List<DocumentData> pdfList = null;

		try {
			conn = dao.openConnection();
			pdfList = daoDoc.getKeyword(req, conn);
		} catch (Exception e) {
			pdfList = null;

			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				pdfList = null;

				e.printStackTrace();
			}
		}

		if (pdfList != null && pdfList.size() != 0) {
			DocumentData pdfListArr[] = new DocumentData[pdfList.size()];

			for (int i = 0; i < pdfList.size(); i++) {
				DocumentData doc = new DocumentData();

				doc.setAutoKey(pdfList.get(i).getAutoKey());
				doc.setCreatedDate(pdfList.get(i).getCreatedDate());
				doc.setModifiedDate(pdfList.get(i).getModifiedDate());
				doc.setName(pdfList.get(i).getName());
				doc.setUserId(pdfList.get(i).getUserId());
				doc.setStatus(pdfList.get(i).getStatus());
				doc.setRecordStatus(pdfList.get(i).getRecordStatus());
				doc.setFileType(pdfList.get(i).getFileType());
				doc.setRegion(req.getRegion());
				doc.setLink(pdfList.get(i).getLink());
				doc.setPostedBy(pdfList.get(i).getPostedBy());
				doc.setModifiedBy(pdfList.get(i).getModifiedBy());
				doc.setFileSize(pdfList.get(i).getFileSize());
				doc.setPages(pdfList.get(i).getPages());
				doc.setTitle(pdfList.get(i).getTitle());
				doc.setKeyword(pdfList.get(i).getKeyword());
				doc.setT2(pdfList.get(i).getT2());
				doc.setT3(pdfList.get(i).getT3());
				doc.setT4(pdfList.get(i).getT4());
				doc.setT5(pdfList.get(i).getT5());
				doc.setN1(pdfList.get(i).getN1());
				doc.setN2(pdfList.get(i).getN2());
				doc.setN3(pdfList.get(i).getN3());
				doc.setN4(pdfList.get(i).getN4());
				doc.setN5(pdfList.get(i).getN5());

				pdfListArr[i] = doc;
			}

			res.setCode("0000");
			res.setDesc("Getting Document List Success.");
			res.setPdfList(pdfListArr);
		} else {
			res.setCode("0014");
			res.setDesc("No Document Found!");
		}

		return res;
	}

	public DocumentResponse getPdfByTypeAndRegion(DocumentRequest req) {
		DocumentResponse res = new DocumentResponse();
		DAOManager dao = new DAOManager();
		Connection conn = null;
		DocumentDao daoDoc = new DocumentDao();
		List<DocumentData> pdfList = null;

		try {
			conn = dao.openConnection();
			pdfList = daoDoc.getPdfByTypeAndRegion(req, conn);
		} catch (Exception e) {
			pdfList = null;

			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				pdfList = null;

				e.printStackTrace();
			}
		}

		if (pdfList != null && pdfList.size() != 0) {
			DocumentData pdfListArr[] = new DocumentData[pdfList.size()];

			for (int i = 0; i < pdfList.size(); i++) {
				DocumentData doc = new DocumentData();

				doc.setAutoKey(pdfList.get(i).getAutoKey());
				doc.setCreatedDate(pdfList.get(i).getCreatedDate());
				doc.setModifiedDate(pdfList.get(i).getModifiedDate());
				doc.setName(pdfList.get(i).getName());
				doc.setUserId(pdfList.get(i).getUserId());
				doc.setStatus(pdfList.get(i).getStatus());
				doc.setRecordStatus(pdfList.get(i).getRecordStatus());
				doc.setFileType(pdfList.get(i).getFileType());
				doc.setRegion(req.getRegion());
				doc.setLink(pdfList.get(i).getLink());
				doc.setPostedBy(pdfList.get(i).getPostedBy());
				doc.setModifiedBy(pdfList.get(i).getModifiedBy());
				doc.setFileSize(pdfList.get(i).getFileSize());
				doc.setPages(pdfList.get(i).getPages());
				doc.setTitle(pdfList.get(i).getTitle());
				doc.setKeyword(pdfList.get(i).getKeyword());
				doc.setT2(pdfList.get(i).getT2());
				doc.setT3(pdfList.get(i).getT3());
				doc.setT4(pdfList.get(i).getT4());
				doc.setT5(pdfList.get(i).getT5());
				doc.setN1(pdfList.get(i).getN1());
				doc.setN2(pdfList.get(i).getN2());
				doc.setN3(pdfList.get(i).getN3());
				doc.setN4(pdfList.get(i).getN4());
				doc.setN5(pdfList.get(i).getN5());

				pdfListArr[i] = doc;
			}

			res.setCode("0000");
			res.setDesc("Getting Document List Success.");
			res.setPdfList(pdfListArr);
		} else {
			res.setCode("0014");
			res.setDesc("No Document Found!");
		}

		return res;
	}

}
