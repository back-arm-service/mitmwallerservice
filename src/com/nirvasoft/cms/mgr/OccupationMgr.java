package com.nirvasoft.cms.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nirvasoft.cms.dao.OccupationDao;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.AdvancedSearchData;
import com.nirvasoft.cms.shared.OccupationData;
import com.nirvasoft.cms.shared.OccupationDataSet;
import com.nirvasoft.cms.util.ServerUtil;

public class OccupationMgr {
	public static Resultb2b deletOccupation(Long key, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = OccupationDao.delete(key, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static OccupationData initData(OccupationData data, MrBean user) {
		String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (data.getSyskey() == 0) {
			data.setCreateddate(date);
		} else {
			data.setCreateddate(date);
		}
		data.setUserid(data.getT1());
		data.setUsername("User");
		data.setModifieddate(date);
		data.setRecordStatus(1);
		data.setSyncBatch(0);
		data.setSyncStatus(1);

		return data;
	}

	public static OccupationData readDataBySyskey(long key, MrBean user) {
		OccupationData res = new OccupationData();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			res = OccupationDao.read(key, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public static Resultb2b saveOccupationData(OccupationData data, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		try {
			if (data.getSyskey() == 0) {
				data.setSyskey(Long.parseLong(OccupationDao.getSyskey("OCCUPATION", conn)));
				data = initData(data, user);
				res = OccupationDao.insert(data, conn);
			} else {
				res = OccupationDao.update(data, conn);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public static OccupationDataSet searchOccupation(AdvancedSearchData p, String searchVal, String sort, String type,
			MrBean user) {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		OccupationDataSet res = new OccupationDataSet();
		try {
			res = OccupationDao.search(p, searchVal, sort, type, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

}
