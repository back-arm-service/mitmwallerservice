package com.nirvasoft.cms.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import com.nirvasoft.cms.shared.TempData;
import com.nirvasoft.cms.shared.WalletTransData;
import com.nirvasoft.rp.framework.ServerSession;
import com.nirvasoft.rp.shared.UserDetailReportData;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanArrayDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsAbstractExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;

public class CommonMgr {

	private static FileInputStream fin;

	public static void downloadFile(String folder, String fileName, HttpServletResponse response) {
		response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
		File file = new File(ServerSession.serverPath + folder + "/" + fileName);
		int length = (int) file.length();
		byte[] bytes = new byte[length];
		try {
			fin = new FileInputStream(file);
			fin.read(bytes);
			ServletOutputStream os = response.getOutputStream();
			os.write(bytes);
			os.flush();
			os.close();
			fin.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void downloadPDFFile(String type, String folder, String fileName, HttpServletResponse response) {
		/*if (type.equals("pdf")) {
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "inline; filename=" + fileName);
		} else*/
			response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
		File file = new File(ServerSession.serverPath + folder + "/" + fileName);
		int length = (int) file.length();
		byte[] bytes = new byte[length];
		try {
			fin = new FileInputStream(file);
			fin.read(bytes);
			ServletOutputStream os = response.getOutputStream();
			os.write(bytes);
			os.flush();
			fin.close();
			if(file.canWrite()){
				file.delete();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void exportExcel(JasperPrint jPrint, HttpServletResponse response, String fileName)
			throws IOException, JRException {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

		JRXlsExporter excelExporter = new JRXlsExporter();

		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment;filename=" + fileName);

		excelExporter.setParameter(JRExporterParameter.JASPER_PRINT, jPrint);
		excelExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
		excelExporter.setParameter(JRXlsAbstractExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
		excelExporter.setParameter(JRXlsAbstractExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);
		excelExporter.setParameter(JRXlsAbstractExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
		excelExporter.setParameter(JRXlsAbstractExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
		excelExporter.setParameter(JRXlsAbstractExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
		excelExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, fileName);
		excelExporter.exportReport();

		ServletOutputStream servletOutputStream = response.getOutputStream();
		servletOutputStream.write(byteArrayOutputStream.toByteArray());
		servletOutputStream.flush();
	}

	public static void exportPdf(JasperPrint jPrint, HttpServletResponse response) throws JRException, IOException {
		JRPdfExporter pdfExporter = new JRPdfExporter();
		response.setContentType("application/pdf");
		pdfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jPrint);
		pdfExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
		pdfExporter.exportReport();
	}

	public static JasperPrint getJrPrint(HashMap<String, Object> params, String fileName, List<? extends Object> list)
			throws JRException {
		JRBeanCollectionDataSource l_dataSource = new JRBeanCollectionDataSource(list);
		JasperReport jasperRpt = JasperCompileManager.compileReport(fileName);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperRpt, params, l_dataSource);
		return jasperPrint;
	}

	public static JasperPrint MerchantTransferReport(String pRptCode, ArrayList<TempData> l_TempList,
			ArrayList<TempData> lstTmpData,String uPLOAD_DIRECTORY, String fONT_DIRECTORY, String iMAGE_DIRECTORY) {
		JasperPrint l_jPrint = new JasperPrint();
		JasperReport l_jsRpt = null;
		String l_rptFile = "";
		try {

			l_rptFile = uPLOAD_DIRECTORY + pRptCode + ".jrxml";
			JRBeanArrayDataSource l_dataSource = new JRBeanArrayDataSource(lstTmpData.toArray());
			JasperReport jasperSubReport = JasperCompileManager.compileReport(uPLOAD_DIRECTORY + "rptWallet_SubReport.jrxml");

			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("TypeList", l_TempList);
			params.put("SUBREPORT_DIR",uPLOAD_DIRECTORY);
			params.put("MFIlogo", iMAGE_DIRECTORY);
			params.put("subrpt", jasperSubReport);
			l_jsRpt = JasperCompileManager.compileReport(l_rptFile);
			l_jPrint = JasperFillManager.fillReport(l_jsRpt, params, l_dataSource);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return l_jPrint;
	}

	public static JasperPrint printUserReport(String pRptCode, ArrayList<UserDetailReportData> wldata,
			String uPLOAD_DIRECTORY, String fONT_DIRECTORY, String iMAGE_DIRECTORY) {
		JasperPrint l_jPrint = new JasperPrint();
		JasperReport l_jsRpt = null;
		String l_rptFile = "";
		try {

			l_rptFile = uPLOAD_DIRECTORY + pRptCode + ".jrxml";
			JRBeanArrayDataSource l_dataSource = new JRBeanArrayDataSource(wldata.toArray());
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("MFIlogo", iMAGE_DIRECTORY);
			l_jsRpt = JasperCompileManager.compileReport(l_rptFile);
			l_jPrint = JasperFillManager.fillReport(l_jsRpt, params, l_dataSource);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return l_jPrint;
	}
}
