package com.nirvasoft.cms.util;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import com.nirvasoft.cms.dao.AppHistoryDao;
import com.nirvasoft.cms.dao.ArticleDao;
import com.nirvasoft.cms.dao.RegisterDao;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.framework.ServerSession;
import com.nirvasoft.cms.shared.AppHistoryData;
import com.nirvasoft.cms.shared.AppHistoryDataSet;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.cms.shared.RegisterData;
import com.nirvasoft.cms.shared.RegisterDataSet;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class WriteExcel {
	private static WritableCellFormat timesBoldUnderline;
	private static WritableCellFormat times;

	private static void addCaption(WritableSheet sheet, int column, int row, String s)
			throws RowsExceededException, WriteException {
		Label label;
		label = new Label(column, row, s, timesBoldUnderline);
		sheet.addCell(label);
	}
	/*
	 * private static void addNumberContentwithFormat(WritableSheet sheet, int
	 * column, int row, double s, WritableCellFormat format) throws
	 * RowsExceededException, WriteException {
	 * 
	 * jxl.write.Number value = new jxl.write.Number(column, row, s, format);
	 * sheet.addCell(value); sheet.setColumnView(column, 7);// to adjust col
	 * width for 12 char }
	 */

	private static void addContent(WritableSheet sheet, int col, int row, String data)
			throws RowsExceededException, WriteException {
		Label label;
		label = new Label(col, row, data);
		sheet.addCell(label);
	}

	private static void createAppanalysisContent(WritableSheet sheet, List<AppHistoryData> aCollinfo,
			AppHistoryData[] count, int aType) {
		try {
			for (int i = 0; i < aCollinfo.size(); i++) {
				int rowcount = sheet.getRows();
				addContent(sheet, 0, rowcount, ArticleDao.ddMMyyyFormat(aCollinfo.get(i).getT7()));
				addContent(sheet, 1, rowcount, String.valueOf(aCollinfo.get(i).getT1()));
				addContent(sheet, 2, rowcount, String.valueOf(aCollinfo.get(i).getN1()));
				addContent(sheet, 3, rowcount, String.valueOf(aCollinfo.get(i).getN2()));
				addContent(sheet, 4, rowcount, String.valueOf(aCollinfo.get(i).getN3()));
				addContent(sheet, 5, rowcount, String.valueOf(aCollinfo.get(i).getN4()));
				addContent(sheet, 6, rowcount, String.valueOf(aCollinfo.get(i).getN5()));
				addContent(sheet, 7, rowcount, String.valueOf(aCollinfo.get(i).getN6()));
				addContent(sheet, 8, rowcount, String.valueOf(aCollinfo.get(i).getN10()));
				addContent(sheet, 9, rowcount, String.valueOf(aCollinfo.get(i).getN7()));
				addContent(sheet, 10, rowcount, String.valueOf(aCollinfo.get(i).getN8()));
			}
			for (int i = 0; i < count.length; i++) {
				int rowcount = sheet.getRows();
				addContent(sheet, 0, rowcount, "Total Count");
				addContent(sheet, 1, rowcount, "-");

				if (count[i].getT1().equals("")) {
					addContent(sheet, 2, rowcount, "0");
				} else {
					addContent(sheet, 2, rowcount, count[i].getT1());
				}
				if (count[i].getT2().equals("")) {
					addContent(sheet, 3, rowcount, "0");
				} else {
					addContent(sheet, 3, rowcount, count[i].getT2());
				}
				if (count[i].getT3().equals("")) {
					addContent(sheet, 4, rowcount, "0");
				} else {
					addContent(sheet, 4, rowcount, count[i].getT3());
				}
				if (count[i].getT4().equals("")) {
					addContent(sheet, 5, rowcount, "0");
				} else {
					addContent(sheet, 5, rowcount, count[i].getT4());
				}
				if (count[i].getT5().equals("")) {
					addContent(sheet, 6, rowcount, "0");
				} else {
					addContent(sheet, 6, rowcount, count[i].getT5());
				}
				if (count[i].getT6().equals("")) {
					addContent(sheet, 7, rowcount, "0");
				} else {
					addContent(sheet, 7, rowcount, count[i].getT6());
				}
				if (count[i].getT11().equals("")) {
					addContent(sheet, 8, rowcount, "0");
				} else {
					addContent(sheet, 8, rowcount, count[i].getT11());
				}
				if (count[i].getT10().equals("")) {
					addContent(sheet, 9, rowcount, "0");
				} else {
					addContent(sheet, 9, rowcount, count[i].getT10());
				}

				if (count[i].getT8().equals("")) {
					addContent(sheet, 10, rowcount, "0");
				} else {
					addContent(sheet, 10, rowcount, count[i].getT8());
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void createHeader(WritableSheet asheet, int Type) throws WriteException {
		WritableFont times10pt = new WritableFont(WritableFont.createFont("Myanmar3"), 10);
		times = new WritableCellFormat(times10pt);
		times.setWrap(true);
		WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD, false);
		timesBoldUnderline = new WritableCellFormat(times10ptBoldUnderline);
		timesBoldUnderline.setWrap(true);
		CellView cv = new CellView();
		cv.setFormat(times);
		cv.setFormat(timesBoldUnderline);
		int i = 0;
		if (Type == 1) { //
			addCaption(asheet, i++, 0, "Date.");
			addCaption(asheet, i++, 0, "Phone No");
			addCaption(asheet, i++, 0, "Login");
			addCaption(asheet, i++, 0, "Weather");
			addCaption(asheet, i++, 0, "Question");
			addCaption(asheet, i++, 0, "Price");
			addCaption(asheet, i++, 0, "Education");
			addCaption(asheet, i++, 0, "News & Media");
			addCaption(asheet, i++, 0, "Call Center");
			addCaption(asheet, i++, 0, "Video");
			addCaption(asheet, i++, 0, "Save Content");
		}
		if (Type == 2) { //
			addCaption(asheet, i++, 0, "Registration Mobile");
			addCaption(asheet, i++, 0, "Name");
			addCaption(asheet, i++, 0, "Mobile");
			addCaption(asheet, i++, 0, "Registration Date");
			addCaption(asheet, i++, 0, "Gender");
			addCaption(asheet, i++, 0, "Occupation");
			addCaption(asheet, i++, 0, "State");
			addCaption(asheet, i++, 0, "Township");
			/*
			 * addCaption(asheet, i++, 0, "Village"); addCaption(asheet, i++, 0,
			 * "Monsoon Crop"); addCaption(asheet, i++, 0, "Number of Acres");
			 * addCaption(asheet, i++, 0, "Winter Corp"); addCaption(asheet,
			 * i++, 0, "Number of Acres"); addCaption(asheet, i++, 0,
			 * "Agrochemical Brand"); addCaption(asheet, i++, 0,
			 * "Fertilizer Brand");
			 */
			addCaption(asheet, i++, 0, "Mobile Serial No");
		}
	}

	private static void createHeader_Status(WritableSheet asheet, int Type) throws WriteException {
		WritableFont times10pt = new WritableFont(WritableFont.createFont("Myanmar3"), 10);
		times = new WritableCellFormat(times10pt);
		times.setWrap(true);
		WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.createFont("Myanmar3"), 10,
				WritableFont.BOLD, false);
		timesBoldUnderline = new WritableCellFormat(times10ptBoldUnderline);
		timesBoldUnderline.setWrap(true);
		CellView cv = new CellView();
		cv.setFormat(times);
		cv.setFormat(timesBoldUnderline);
		// NumberFormat decimalNo = new NumberFormat("#,##0.00"); //04.07.2016
		// AMMS
		// WritableCellFormat numberFormat = new WritableCellFormat(decimalNo);
		int i = 0;
		if (Type == 1) {
			addCaption(asheet, i++, 0, "á€žá€®á€¸á€�?á€¾á€¶á€¡á€™á€Šá€º");
			addCaption(asheet, i++, 0, "á€�á€­á€¯á€„á€ºá€¸");
			addCaption(asheet, i++, 0, "á€™á€¼á€­á€¯á€·á€�?á€šá€º");
			addCaption(asheet, i++, 0, "â€‹á€�?á€±á€·á€…á€½á€²");
			addCaption(asheet, i++, 0,
					"â€‹á€…á€»á€±á€¸á€�?á€¾á€¯á€�?á€ºá€¸");
			addCaption(asheet, i++, 0, "á€™á€¼á€�?á€ºá€™á€¬á€€á€»á€•á€º");
			addCaption(asheet, i++, 0, "á€¡á€›á€±á€¡á€�á€½á€€á€º");
			addCaption(asheet, i++, 0, "â€‹á€›á€±á€�á€½á€€á€ºá€•á€¯á€¶");

		}
	}

	private static void createRegcontent(WritableSheet sheet, List<RegisterData> aCollinfo, int aType) {
		try {
			for (int i = 0; i < aCollinfo.size(); i++) {
				int rowcount = sheet.getRows();
				addContent(sheet, 0, rowcount, String.valueOf(aCollinfo.get(i).getT1()));
				addContent(sheet, 1, rowcount, String.valueOf(aCollinfo.get(i).getT3()));
				addContent(sheet, 2, rowcount, String.valueOf(aCollinfo.get(i).getT23()));
				addContent(sheet, 3, rowcount, ArticleDao.ddMMyyyFormat(aCollinfo.get(i).getModifieddate()));
				if (aCollinfo.get(i).getT5().equals("0")) {
					addContent(sheet, 4, rowcount, "Male");
				} else {
					addContent(sheet, 4, rowcount, "Female");
				}
				addContent(sheet, 5, rowcount, String.valueOf(aCollinfo.get(i).getT6()));
				addContent(sheet, 6, rowcount, String.valueOf(aCollinfo.get(i).getT7()));
				addContent(sheet, 7, rowcount, String.valueOf(aCollinfo.get(i).getT8()));
				addContent(sheet, 8, rowcount, String.valueOf(aCollinfo.get(i).getT22()));
				addContent(sheet, 9, rowcount, String.valueOf(aCollinfo.get(i).getT9()));
				addContent(sheet, 10, rowcount, String.valueOf(aCollinfo.get(i).getT10()));
				addContent(sheet, 11, rowcount, String.valueOf(aCollinfo.get(i).getT11()));
				addContent(sheet, 12, rowcount, String.valueOf(aCollinfo.get(i).getT12()));
				addContent(sheet, 13, rowcount, String.valueOf(aCollinfo.get(i).getT18()));
				addContent(sheet, 14, rowcount, String.valueOf(aCollinfo.get(i).getT19()));
				addContent(sheet, 15, rowcount, String.valueOf(aCollinfo.get(i).getT58()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Resultb2b writeExcelAppExport(String folder, String fileName, String searchVal, String fromdate,
			String todate, MrBean user) throws SQLException {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		AppHistoryDataSet ret = new AppHistoryDataSet();
		Resultb2b res = new Resultb2b();
		PagerData pdata = new PagerData();
		String filePath = ServerSession.serverPath + folder;
		File dir = new File(filePath);
		if (!dir.exists())
			dir.mkdirs();
		File l_file = new File(ServerSession.serverPath + folder + "/" + fileName);
		if (l_file.exists()) {
			l_file.delete();
		}
		try {
			WorkbookSettings wbSettings = new WorkbookSettings();
			wbSettings.setLocale(new Locale("en", "EN"));
			WritableWorkbook workbook = Workbook.createWorkbook(l_file, wbSettings);
			workbook.createSheet("List", 0);
			WritableSheet excelSheet = workbook.getSheet(0);
			createHeader(excelSheet, 1);
			ret = AppHistoryDao.searchAppAnalysisList(pdata, "", fromdate, todate, "app", conn);
			List<AppHistoryData> dataList = new ArrayList<AppHistoryData>(Arrays.asList(ret.getData()));
			createAppanalysisContent(excelSheet, dataList, ret.getCount(), 1);
			workbook.write();
			workbook.close();
			res.setState(true);
		} catch (WriteException e) {
			res.setState(false);
		} catch (IOException e) {
			res.setState(false);
		}
		return res;
	}

	public static Resultb2b writeExcelRegExport(String folder, String fileName, String searchVal, MrBean user)
			throws SQLException {
		Connection conn = ConnAdmin.getConn(user.getUser().getOrganizationID());
		RegisterDataSet ret = new RegisterDataSet();
		Resultb2b res = new Resultb2b();
		// PagerData pdata=new PagerData();
		String filePath = ServerSession.serverPath + folder;
		File dir = new File(filePath);
		if (!dir.exists())
			dir.mkdirs();
		File l_file = new File(ServerSession.serverPath + folder + "/" + fileName);
		if (l_file.exists()) {
			l_file.delete();
		}
		try {
			WorkbookSettings wbSettings = new WorkbookSettings();
			wbSettings.setLocale(new Locale("en", "EN"));
			WritableWorkbook workbook = Workbook.createWorkbook(l_file, wbSettings);
			workbook.createSheet("List", 0);
			WritableSheet excelSheet = workbook.getSheet(0);
			createHeader(excelSheet, 2);
			ret = RegisterDao.ExporReg(searchVal, "article", conn);
			List<RegisterData> dataList = new ArrayList<RegisterData>(Arrays.asList(ret.getData()));
			createRegcontent(excelSheet, dataList, 1);
			workbook.write();
			workbook.close();
			res.setState(true);
		} catch (WriteException e) {
			res.setState(false);
		} catch (IOException e) {
			res.setState(false);
		}
		return res;
	}

	public static Resultb2b writePriceDownloadTemplate(String folder, String pFilename, MrBean user)
			throws SQLException {
		Resultb2b res = new Resultb2b();
		try {
			String filePath = ServerSession.serverPath + folder;
			File dir = new File(filePath);
			if (!dir.exists())
				dir.mkdirs();

			File l_file = new File(ServerSession.serverPath + folder + "/" + pFilename);
			if (l_file.exists()) {
				l_file.delete();
			}
			WorkbookSettings wbSettings = new WorkbookSettings();
			wbSettings.setLocale(new Locale("en", "EN"));
			WritableWorkbook workbook = Workbook.createWorkbook(l_file, wbSettings);
			workbook.createSheet("List", 0);
			WritableSheet excelSheet1 = workbook.getSheet(0);
			createHeader_Status(excelSheet1, 1);
			workbook.write();
			workbook.close();
			res.setState(true);
		} catch (WriteException e) {
			res.setState(false);
			e.printStackTrace();
		} catch (IOException e) {
			res.setState(false);
			e.printStackTrace();
		}
		return res;
	}

}