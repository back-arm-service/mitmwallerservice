package com.nirvasoft.mfi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.nirvasoft.mfi.users.data.KeyData;
import com.nirvasoft.rp.framework.Result;
public class KeyGenerateDao {
	public KeyData getKeyData(String objectName,int keyDataType, Connection conn) throws SQLException {
		KeyData keydata=new KeyData();
		String query1 = "Select KeyTableSysKey,FK_MFIOrgSysKey,ObjectName,Prefix,NoOfDigit,Surfix,SequenceNo,KeyDataType,status from [KeyTable] where ObjectName=? and KeyDataType=? and status= ?";
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i=1;
		stmt2.setString(i++, objectName);
		stmt2.setInt(i++, keyDataType);
		stmt2.setInt(i++, 1);
		
		ResultSet keytable = stmt2.executeQuery();
		if (keytable.next()) {
			keydata.setKeyTableSysKey(keytable.getLong("KeyTableSysKey"));
			keydata.setfK_MFIOrgSysKey(keytable.getLong("FK_MFIOrgSysKey"));
			keydata.setObjectName(keytable.getString("ObjectName"));
			keydata.setPrefix(keytable.getString("Prefix"));
			keydata.setNoOfDigit(keytable.getInt("NoOfDigit"));
			keydata.setSrfix(keytable.getString("Surfix"));
			keydata.setSequenceNo(keytable.getInt("SequenceNo"));
			keydata.setKeyDataType(keytable.getInt("KeyDataType"));
			keydata.setStatus(keytable.getInt("status"));
		
		}
	
		return keydata;
	}
	
	public Result UpdateSeqNo( Connection conn,int seqno,long keyTableSysKey) throws SQLException{
		Result res = new Result();
		
		String logoutquery = "Update [KeyTable] set SequenceNo = ? WHERE KeyTableSysKey = ?";
		PreparedStatement ps = conn.prepareStatement(logoutquery);
		int i = 1;
		ps.setInt(i++,seqno);
		ps.setLong(i++,keyTableSysKey);
		
		int result = ps.executeUpdate();
		if(result > 0)
		{
			res.setState(true);
			res.setMsgCode("0000");
			res.setMsgDesc("Success");
		}
		else
		{
			res.setState(false);
			res.setMsgCode("0014");
			res.setMsgDesc("Failed");
		}
		
		return res;
	}

}
