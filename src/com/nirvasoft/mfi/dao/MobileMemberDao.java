package com.nirvasoft.mfi.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import com.nirvasoft.mfi.mgr.KeyGenarateMgr;
import com.nirvasoft.mfi.users.data.MobileMemberData;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.util.ServerGlobal;
public class MobileMemberDao {
	public static MobileMemberData getMemberDataBysyskey(Connection conn, String syskey)
			throws SQLException, ParseException {
		MobileMemberData data = new MobileMemberData();

		String query1 = "select syskey,PK_MemberId,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,T15,"
				+ "N1,N2,N3,M1,T16,T17,T920,T921,T922,T923,T924,T925,T926,T927,T928,"
				+ "T930,T931,T932,T933,T934,T950,T951,T952,status from Member" + " where syskey=?";
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setString(i++, syskey);

		ResultSet res = stmt2.executeQuery();
		if (res.next()) {

			data.setSyskey(res.getLong("syskey"));
			data.setMember_id(res.getString("PK_MemberId"));
			data.setReference_memberID(res.getString("T2"));
			data.setMember_name(res.getString("T3"));
			data.setNrc(res.getString("T4"));
			data.setNrc2(res.getString("T5"));
			data.setRegistered_mobileNo(res.getString("T6"));
			data.setDeviceID(res.getString("T7"));
			data.setPassword(res.getString("T8"));
			String imagepath = "upload/image/userProfile/";

			data.setFront_nrc_image_path(imagepath + res.getString("T9"));
			data.setBack_nrc_image_path(imagepath +res.getString("T10"));
			// data.setDob(res.getString("T11"));
			String date = res.getString("T11");// +"-"+res.getString("T11").substring(4,
												// 2);//+"-"+res.getString("T11").substring(6,
												// 2);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			Date d1 = sdf.parse(date);
			sdf.applyPattern("yyyy-MM-dd");
			data.setDob(sdf.format(d1));

			data.setCompany_name(res.getString("T12"));
			data.setCompany_ph_no(res.getString("T13"));
			data.setDepartment(res.getString("T14"));
			data.setCurrent_position(res.getString("T15"));

			data.setYears_of_service(res.getInt("N1"));
			data.setMonthly_basic_income(res.getInt("N2"));
			data.setCreditForOhters(res.getString("N3"));
			data.setCreditAmount(res.getInt("M1"));

			data.setLastlogin(res.getString("T16"));
			data.setLastlogout(res.getString("T17"));
			data.setCurrent_address_region(res.getString("T920"));
			data.setCurrent_address_township(res.getString("T921"));
			data.setCurrent_address_street(res.getString("T922"));
			data.setCurrent_address_houseno(res.getString("T923"));
			data.setCurrent_address_postal_code(res.getString("T924"));
			data.setEmail(res.getString("T925"));
			data.setMobile(res.getString("T926"));
			data.setLandline_phone(res.getString("T927"));
			data.setFax(res.getString("T928"));

			data.setPermanent_address_region(res.getString("t930"));
			data.setPermanent_address_township(res.getString("t931"));
			data.setPermanent_address_street(res.getString("t932"));
			data.setPermanent_address_houseno(res.getString("t933"));
			data.setPermanent_address_postal_code(res.getString("t934"));
			data.setStatus(res.getInt("status"));
			
			ServerGlobal.setTotalbitstatus(res.getInt("status"));
		}

		return data;
	}

	public static ArrayList<MobileMemberData> getGuarantorDataByApplicantsyskey(Connection conn, String appsyskey)
			throws SQLException, ParseException {

		ArrayList<MobileMemberData> datalist = new ArrayList<MobileMemberData>();

		String query1 = "select syskey,PK_MemberId,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,T15,"
				+ "N1,N2,N3,M1,T16,T17,T920,T921,T922,T923,T924,T925,T926,T927,T928,"
				+ "T930,T931,T932,T933,T934,T950,T951,T952,status from Member"
				+ " where syskey in(select FK_MemberGuarantorSysKey from Loan where FK_MemberSysKey=?  ) ";
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setString(i++, appsyskey);

		ResultSet res = stmt2.executeQuery();
		while (res.next()) {
			MobileMemberData data = new MobileMemberData();
			data.setSyskey(res.getLong("syskey"));
			data.setMember_id(res.getString("PK_MemberId"));
			data.setReference_memberID(res.getString("T2"));
			data.setMember_name(res.getString("T3"));
			data.setNrc(res.getString("T4"));
			data.setNrc2(res.getString("T5"));
			data.setRegistered_mobileNo(res.getString("T6"));
			data.setDeviceID(res.getString("T7"));
			data.setPassword(res.getString("T8"));
			String imagepath = "upload/image/userProfile/";

			data.setFront_nrc_image_path(imagepath + res.getString("T9"));			
			data.setBack_nrc_image_path(imagepath + res.getString("T10"));
			// data.setDob(res.getString("T11"));
			String date = res.getString("T11");// +"-"+res.getString("T11").substring(4,
												// 2);//+"-"+res.getString("T11").substring(6,
												// 2);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			Date d1 = sdf.parse(date);
			sdf.applyPattern("yyyy-MM-dd");
			data.setDob(sdf.format(d1));
			data.setCompany_name(res.getString("T12"));
			data.setCompany_ph_no(res.getString("T13"));
			data.setDepartment(res.getString("T14"));
			data.setCurrent_position(res.getString("T15"));

			data.setYears_of_service(res.getInt("N1"));
			data.setMonthly_basic_income(res.getInt("N2"));
			data.setCreditForOhters(res.getString("N3"));
			data.setCreditAmount(res.getInt("M1"));

			data.setLastlogin(res.getString("T16"));
			data.setLastlogout(res.getString("T17"));
			data.setCurrent_address_region(res.getString("T920"));
			data.setCurrent_address_township(res.getString("T921"));
			data.setCurrent_address_street(res.getString("T922"));
			data.setCurrent_address_houseno(res.getString("T923"));
			data.setCurrent_address_postal_code(res.getString("T924"));
			data.setEmail(res.getString("T925"));
			data.setMobile(res.getString("T926"));
			data.setLandline_phone(res.getString("T927"));
			data.setFax(res.getString("T928"));

			data.setPermanent_address_region(res.getString("t930"));
			data.setPermanent_address_township(res.getString("t931"));
			data.setPermanent_address_street(res.getString("t932"));
			data.setPermanent_address_houseno(res.getString("t933"));
			data.setPermanent_address_postal_code(res.getString("t934"));
			data.setStatus(res.getInt("status"));

			datalist.add(data);
		}

		return datalist;
	}
	public static MobileMemberData getGuarantorDataByLoansyskey(Connection conn, String loansyskey)
			throws SQLException, ParseException {
		MobileMemberData data = new MobileMemberData();
		String query1 = "select syskey,PK_MemberId,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,T15,"
				+ "N1,N2,N3,M1,T16,T17,T920,T921,T922,T923,T924,T925,T926,T927,T928,"
				+ "T930,T931,T932,T933,T934,T950,T951,T952,status from Member"
				+ " where syskey in(select FK_MemberGuarantorSysKey from Loan where LoanSysKey=?  ) ";
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setString(i++, loansyskey);

		ResultSet res = stmt2.executeQuery();
		if (res.next()) {

			data.setSyskey(res.getLong("syskey"));
			data.setMember_id(res.getString("PK_MemberId"));
			data.setReference_memberID(res.getString("T2"));
			data.setMember_name(res.getString("T3"));
			data.setNrc(res.getString("T4"));
			data.setNrc2(res.getString("T5"));
			data.setRegistered_mobileNo(res.getString("T6"));
			data.setDeviceID(res.getString("T7"));
			data.setPassword(res.getString("T8"));
			String imagepath = "upload/image/userProfile/";

			data.setFront_nrc_image_path(imagepath + res.getString("T9"));			
			data.setBack_nrc_image_path(imagepath + res.getString("T10"));
			// data.setDob(res.getString("T11"));
			String date = res.getString("T11");// +"-"+res.getString("T11").substring(4,
												// 2);//+"-"+res.getString("T11").substring(6,
												// 2);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			Date d1 = sdf.parse(date);
			sdf.applyPattern("yyyy-MM-dd");
			data.setDob(sdf.format(d1));
			data.setCompany_name(res.getString("T12"));
			data.setCompany_ph_no(res.getString("T13"));
			data.setDepartment(res.getString("T14"));
			data.setCurrent_position(res.getString("T15"));

			data.setYears_of_service(res.getInt("N1"));
			data.setMonthly_basic_income(res.getInt("N2"));
			data.setCreditForOhters(res.getString("N3"));
			data.setCreditAmount(res.getInt("M1"));

			data.setLastlogin(res.getString("T16"));
			data.setLastlogout(res.getString("T17"));
			data.setCurrent_address_region(res.getString("T920"));
			data.setCurrent_address_township(res.getString("T921"));
			data.setCurrent_address_street(res.getString("T922"));
			data.setCurrent_address_houseno(res.getString("T923"));
			data.setCurrent_address_postal_code(res.getString("T924"));
			data.setEmail(res.getString("T925"));
			data.setMobile(res.getString("T926"));
			data.setLandline_phone(res.getString("T927"));
			data.setFax(res.getString("T928"));

			data.setPermanent_address_region(res.getString("t930"));
			data.setPermanent_address_township(res.getString("t931"));
			data.setPermanent_address_street(res.getString("t932"));
			data.setPermanent_address_houseno(res.getString("t933"));
			data.setPermanent_address_postal_code(res.getString("t934"));
			data.setStatus(res.getInt("status"));
			ServerGlobal.setTotalbitstatus(res.getInt("status"));
		}

		return data;
	}
	public static String getRegionBySyskey(Connection conn, String syskey) throws SQLException, ParseException {
		String query1 = "select T920 from Member  where syskey=?";
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setString(i++, syskey);
		String region = "";

		ResultSet res = stmt2.executeQuery();
		if (res.next()) {

			region = res.getString("T920");

		}

		return region;
	}
	public static boolean isbitOn(int bitposition, int totalval) {
		boolean ret = false;
		if ((bitposition & totalval) == bitposition)
			return true;
		return ret;

	}
	
	public static boolean isLoanmemberOn() {
		return isbitOn((int) Math.pow(2, ServerGlobal.getLoanmemberPos()), ServerGlobal.getTotalbitstatus());
	}
	public static boolean isGuarantorOn() {
		return isbitOn((int) Math.pow(2, ServerGlobal.getGuarantorPos()), ServerGlobal.getTotalbitstatus());
	}
	public static Result checkMemberBySyskey(long syskey, Connection conn) throws SQLException {
		Result res = new Result();
		String phno = "";
		String query1 = "Select T6,status,syskey from [member] where syskey= ?";
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		stmt2.setLong(1, syskey);

		ResultSet resph = stmt2.executeQuery();
		while (resph.next()) {
			phno = resph.getString("T6");
			res.setSyskey(resph.getLong("syskey"));
			res.setStatus(resph.getInt("status"));
		}
		if (phno.equalsIgnoreCase("")) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		return res;
	}
	public static Result updateGrarantor(MobileMemberData obj, Connection conn) throws SQLException {
		Result res = new Result();

		try {
			int pretotal = 0;

			obj.setModified_date(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));

			String sql = "UPDATE [member] SET t3=?,t4=?,t5=?,t8=?,t9=?,t10=?,t11=?,t12=?,t13=?,t14=?,t15=?"
					+ ",n1=?,n2=?,n3=?,m1=?,t920=?,t921=?,t922=?,t923=?,t924=?,t925=?,t926=?,t927=?,t928=?,t930=?,t931=?,"
					+ "t932=?,t933=?,t934=?,t951=?,t952=?,status=? where syskey=? ";

			PreparedStatement stmt = conn.prepareStatement(sql);

			int i = 1;
			stmt.setString(i++, obj.getMember_name());
			stmt.setString(i++, obj.getNrc());
			stmt.setString(i++, obj.getNrc2());
			stmt.setString(i++, obj.getPassword());
			stmt.setString(i++, obj.getFront_nrc_image_path());
			stmt.setString(i++, obj.getBack_nrc_image_path());
			stmt.setString(i++, obj.getDob());
			stmt.setString(i++, obj.getCompany_name());
			stmt.setString(i++, obj.getCompany_ph_no());
			stmt.setString(i++, obj.getDepartment());
			stmt.setString(i++, obj.getCurrent_position());
			stmt.setInt(i++, obj.getYears_of_service());
			stmt.setDouble(i++, obj.getMonthly_basic_income());
			stmt.setString(i++, obj.getCreditForOhters());
			stmt.setDouble(i++, obj.getCreditAmount());

			stmt.setString(i++, obj.getCurrent_address_region());
			stmt.setString(i++, obj.getCurrent_address_township());
			stmt.setString(i++, obj.getCurrent_address_street());
			stmt.setString(i++, obj.getCurrent_address_houseno());
			stmt.setString(i++, obj.getCurrent_address_postal_code());
			stmt.setString(i++, obj.getEmail());
			stmt.setString(i++, obj.getMobile());
			stmt.setString(i++, obj.getLandline_phone());
			stmt.setString(i++, obj.getFax());
			stmt.setString(i++, obj.getPermanent_address_region());
			stmt.setString(i++, obj.getPermanent_address_township());
			stmt.setString(i++, obj.getPermanent_address_street());
			stmt.setString(i++, obj.getPermanent_address_houseno());
			stmt.setString(i++, obj.getPermanent_address_postal_code());

			stmt.setString(i++, obj.getModified_date());
			stmt.setString(i++, obj.getModifiedby());
			stmt.setInt(i++, obj.getStatus());
			stmt.setLong(i++, obj.getSyskey());

			int rs = stmt.executeUpdate();

			if (rs > 0) {
				//ServerGlobal.setTotalbitstatus(pretotal);
				res.setState(true);
				res.setMsgDesc("Saved Successfully");
				res.setMsgCode("0000");

			} else {
				res.setState(false);
				res.setMsgDesc("Cannot Save");
				res.setMsgCode("0014");
			}

		}

		catch (Exception e) {
			res.setMsgDesc("Cannot Save");
			res.setMsgCode("0014");
			e.printStackTrace();
		}

		return res;
	}
	public static Result insertGrarantor(MobileMemberData obj, Connection conn) throws SQLException {
		Result res = new Result();

		try {
			int pretotal = 0;

			pretotal = OnBitTotalNewValue(ServerGlobal.getAnonymousPos(), pretotal);
			pretotal = OnBitTotalNewValue(ServerGlobal.getGuarantorPos(), pretotal);
			pretotal = OnBitTotalNewValue(ServerGlobal.getActivePos(), pretotal);
			obj.setRegistration_date(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
			obj.setModified_date(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
			obj.setStatus(pretotal);
			obj.setMember_id(getNewMemberID(conn));

			String sql = "insert into [member] (syskey,PK_MemberId,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15,n1,n2,n3,m1,t16,t17,t920,t921,t922,t923,t924,t925,t926,t927,t928,t930,t931,t932,t933,t934,t950,t951,t952,status) "
					+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement stmt = conn.prepareStatement(sql);

			int i = 1;
			stmt.setLong(i++, obj.getSyskey());
			stmt.setString(i++, obj.getMember_id());
			stmt.setString(i++, obj.getReference_memberID());
			stmt.setString(i++, obj.getMember_name());
			stmt.setString(i++, obj.getNrc());
			stmt.setString(i++, obj.getNrc2());
			stmt.setString(i++, obj.getRegistered_mobileNo());
			stmt.setString(i++, obj.getDeviceID());
			stmt.setString(i++, obj.getPassword());
			stmt.setString(i++, obj.getFront_nrc_image_path());
			stmt.setString(i++, obj.getBack_nrc_image_path());
			stmt.setString(i++, obj.getDob());
			stmt.setString(i++, obj.getCompany_name());
			stmt.setString(i++, obj.getCompany_ph_no());
			stmt.setString(i++, obj.getDepartment());
			stmt.setString(i++, obj.getCurrent_position());
			stmt.setInt(i++, obj.getYears_of_service());
			stmt.setDouble(i++, obj.getMonthly_basic_income());
			stmt.setString(i++, obj.getCreditForOhters());
			stmt.setDouble(i++, obj.getCreditAmount());
			stmt.setString(i++, obj.getLastlogin());
			stmt.setString(i++, obj.getLastlogout());
			stmt.setString(i++, obj.getCurrent_address_region());
			stmt.setString(i++, obj.getCurrent_address_township());
			stmt.setString(i++, obj.getCurrent_address_street());
			stmt.setString(i++, obj.getCurrent_address_houseno());
			stmt.setString(i++, obj.getCurrent_address_postal_code());
			stmt.setString(i++, obj.getEmail());
			stmt.setString(i++, obj.getMobile());
			stmt.setString(i++, obj.getLandline_phone());
			stmt.setString(i++, obj.getFax());
			stmt.setString(i++, obj.getPermanent_address_region());
			stmt.setString(i++, obj.getPermanent_address_township());
			stmt.setString(i++, obj.getPermanent_address_street());
			stmt.setString(i++, obj.getPermanent_address_houseno());
			stmt.setString(i++, obj.getPermanent_address_postal_code());
			stmt.setString(i++, obj.getRegistration_date());
			stmt.setString(i++, obj.getModified_date());
			stmt.setString(i++, obj.getModifiedby());
			stmt.setInt(i++, obj.getStatus());

			int rs = stmt.executeUpdate();

			if (rs > 0) {
				//ServerGlobal.setTotalbitstatus(pretotal);
				res.setState(true);
				res.setMsgDesc("Saved Successfully");
				res.setMsgCode("0000");

			} else {
				res.setState(false);
				res.setMsgDesc("Cannot Save");
				res.setMsgCode("0014");
			}

		}

		catch (Exception e) {
			res.setMsgDesc("Cannot Save");
			res.setMsgCode("0014");
			e.printStackTrace();
		}

		return res;
	}
	public static int OnBitTotalNewValue(int bitpos, int previousTotalBitVal) {
		int retval = 0;
		retval = previousTotalBitVal + (int) (Math.pow(2, bitpos));
		return retval;
	}
	public static String getNewMemberID(Connection aConn) {
		String ret = new String();

		Result res = new Result();
		res = KeyGenarateMgr.getKeyDataMgr("Member", 1);
		ret = res.getKeyString();
		return ret;
	}
	public static Result updateApplicant(MobileMemberData obj, Connection conn) throws SQLException {
		Result res = new Result();

		try {
			int pretotal = 0;
			if (!isLoanmemberOn()) {
				pretotal = OnBitTotalNewValue(ServerGlobal.getLoanmemberPos(), ServerGlobal.getTotalbitstatus());
			} else {
				pretotal = ServerGlobal.getTotalbitstatus();
			}

			obj.setModified_date(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
			obj.setStatus(pretotal);

			String sql = "UPDATE [member] SET t3=?,t4=?,t5=?,t8=?,t9=?,t10=?,t11=?,t12=?,t13=?,t14=?,t15=?"
					+ ",n1=?,n2=?,n3=?,m1=?,t920=?,t921=?,t922=?,t923=?,t924=?,t925=?,t926=?,t927=?,t928=?,t930=?,t931=?,"
					+ "t932=?,t933=?,t934=?,t951=?,t952=?,status=? where syskey=? ";

			PreparedStatement stmt = conn.prepareStatement(sql);

			int i = 1;
			stmt.setString(i++, obj.getMember_name());
			stmt.setString(i++, obj.getNrc());
			stmt.setString(i++, obj.getNrc2());
			stmt.setString(i++, obj.getPassword());
			stmt.setString(i++, obj.getFront_nrc_image_path());
			stmt.setString(i++, obj.getBack_nrc_image_path());
			stmt.setString(i++, obj.getDob());
			stmt.setString(i++, obj.getCompany_name());
			stmt.setString(i++, obj.getCompany_ph_no());
			stmt.setString(i++, obj.getDepartment());
			stmt.setString(i++, obj.getCurrent_position());
			stmt.setInt(i++, obj.getYears_of_service());
			stmt.setDouble(i++, obj.getMonthly_basic_income());
			stmt.setString(i++, obj.getCreditForOhters());
			stmt.setDouble(i++, obj.getCreditAmount());

			stmt.setString(i++, obj.getCurrent_address_region());
			stmt.setString(i++, obj.getCurrent_address_township());
			stmt.setString(i++, obj.getCurrent_address_street());
			stmt.setString(i++, obj.getCurrent_address_houseno());
			stmt.setString(i++, obj.getCurrent_address_postal_code());
			stmt.setString(i++, obj.getEmail());
			stmt.setString(i++, obj.getMobile());
			stmt.setString(i++, obj.getLandline_phone());
			stmt.setString(i++, obj.getFax());
			stmt.setString(i++, obj.getPermanent_address_region());
			stmt.setString(i++, obj.getPermanent_address_township());
			stmt.setString(i++, obj.getPermanent_address_street());
			stmt.setString(i++, obj.getPermanent_address_houseno());
			stmt.setString(i++, obj.getPermanent_address_postal_code());

			stmt.setString(i++, obj.getModified_date());
			stmt.setString(i++, obj.getModifiedby());
			stmt.setInt(i++, obj.getStatus());
			stmt.setLong(i++, obj.getSyskey());

			int rs = stmt.executeUpdate();

			if (rs > 0) {
				ServerGlobal.setTotalbitstatus(pretotal);
				res.setState(true);
				res.setMsgDesc("Saved Successfully");
				res.setMsgCode("0000");

			} else {
				res.setState(false);
				res.setMsgDesc("Cannot Save");
				res.setMsgCode("0014");
			}

		}

		catch (Exception e) {
			res.setMsgDesc("Cannot Save");
			res.setMsgCode("0014");
			e.printStackTrace();
		}

		return res;
	}
	public Result checkPhoneNo(String phNo, Connection conn) throws SQLException {
		Result res = new Result();
		String phno = "";
		String query1 = "Select T6,status,syskey from [member] where t6= ?";
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		stmt2.setString(1, phNo);

		ResultSet resph = stmt2.executeQuery();
		while (resph.next()) {
			phno = resph.getString("T6");
			res.setSyskey(resph.getLong("syskey"));
			res.setStatus(resph.getInt("status"));
		}
		if (phno.equalsIgnoreCase("")) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		return res;
	}
	public static boolean isActiveOn() {
		return isbitOn((int) Math.pow(2, ServerGlobal.getActivePos()), ServerGlobal.getTotalbitstatus());
	}
	public boolean isRegisteredOn() {
		return isbitOn((int) Math.pow(2, ServerGlobal.getRegisteredPos()), ServerGlobal.getTotalbitstatus());
	}
	public boolean isSuspendedOn() {
		return isbitOn((int) Math.pow(2, ServerGlobal.getSuspendedPos()), ServerGlobal.getTotalbitstatus());
	}
	public Result UpdateStatuswithdeviceid(long syskey, Connection conn, int status, String deviceid)
			throws SQLException {
		Result res = new Result();
		String date = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
		String logoutquery = "Update [member] set Status = ?,T951 = ? ,T7=? WHERE syskey = ?";
		PreparedStatement ps = conn.prepareStatement(logoutquery);
		int i = 1;
		ps.setInt(i++, status);
		ps.setString(i++, date);
		ps.setString(i++, deviceid);
		ps.setLong(i++, syskey);
		int result = ps.executeUpdate();
		if (result > 0) {
			res.setState(true);
			res.setMsgCode("0000");
			res.setMsgDesc("Success");
		} else {
			res.setState(false);
			res.setMsgCode("0014");
			res.setMsgDesc("Failed");
		}

		return res;
	}
	public static Result insert(MobileMemberData obj, Connection conn) throws SQLException {
		Result res = new Result();

		if (!isPhDuplicate(obj, conn)) {

			try {
				int pretotal = 0;

				pretotal = OnBitTotalNewValue(ServerGlobal.getAnonymousPos(), pretotal);
				
				//todelete
				 pretotal=OnBitTotalNewValue(ServerGlobal.getRegisteredPos(),
				 pretotal);
				//todelete
				pretotal = OnBitTotalNewValue(ServerGlobal.getActivePos(), pretotal);
				obj.setRegistration_date(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
				obj.setModified_date(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
				obj.setStatus(pretotal);
				obj.setMember_id(getNewMemberID(conn));

				String sql = "insert into [member] (syskey,PK_MemberId,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15,n1,n2,t16,t17,t920,t921,t922,t923,t924,t925,t926,t927,t928,t930,t931,t932,t933,t934,t950,t951,t952,status) "
						+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				PreparedStatement stmt = conn.prepareStatement(sql);

				int i = 1;
				stmt.setLong(i++, obj.getSyskey());
				stmt.setString(i++, obj.getMember_id());
				stmt.setString(i++, obj.getReference_memberID());
				stmt.setString(i++, obj.getMember_name());
				stmt.setString(i++, obj.getNrc());
				stmt.setString(i++, obj.getNrc2());
				stmt.setString(i++, obj.getRegistered_mobileNo());
				stmt.setString(i++, obj.getDeviceID());
				stmt.setString(i++, obj.getPassword());
				stmt.setString(i++, obj.getFront_nrc_image_path());
				stmt.setString(i++, obj.getBack_nrc_image_path());
				stmt.setString(i++, obj.getDob());
				stmt.setString(i++, obj.getCompany_name());
				stmt.setString(i++, obj.getCompany_ph_no());
				stmt.setString(i++, obj.getDepartment());
				stmt.setString(i++, obj.getCurrent_position());
				stmt.setInt(i++, obj.getYears_of_service());
				stmt.setDouble(i++, obj.getMonthly_basic_income());
				stmt.setString(i++, obj.getLastlogin());
				stmt.setString(i++, obj.getLastlogout());
				stmt.setString(i++, obj.getCurrent_address_region());
				stmt.setString(i++, obj.getCurrent_address_township());
				stmt.setString(i++, obj.getCurrent_address_street());
				stmt.setString(i++, obj.getCurrent_address_houseno());
				stmt.setString(i++, obj.getCurrent_address_postal_code());
				stmt.setString(i++, obj.getEmail());
				stmt.setString(i++, obj.getMobile());
				stmt.setString(i++, obj.getLandline_phone());
				stmt.setString(i++, obj.getFax());
				stmt.setString(i++, obj.getPermanent_address_region());
				stmt.setString(i++, obj.getPermanent_address_township());
				stmt.setString(i++, obj.getPermanent_address_street());
				stmt.setString(i++, obj.getPermanent_address_houseno());
				stmt.setString(i++, obj.getPermanent_address_postal_code());
				stmt.setString(i++, obj.getRegistration_date());
				stmt.setString(i++, obj.getModified_date());
				stmt.setString(i++, obj.getModifiedby());
				stmt.setInt(i++, obj.getStatus());

				int rs = stmt.executeUpdate();

				if (rs > 0) {
					ServerGlobal.setTotalbitstatus(pretotal);
					res.setState(true);
					res.setMsgDesc("Saved Successfully");

				} else {
					res.setState(false);
					res.setMsgDesc("Cannot Save");
				}

			}

			catch (Exception e) {
				res.setMsgDesc("Cannot Save");
				e.printStackTrace();
			}

		} else {
			res.setMsgDesc("Phone No. already exist");
		}

		return res;
	}
	public static boolean isPhDuplicate(MobileMemberData obj, Connection conn) throws SQLException {

		String sql = "Select PK_MemberId from [member] Where T6 = ? and syskey <> ?";
		PreparedStatement psmt = conn.prepareStatement(sql);
		psmt.setString(1, obj.getRegistered_mobileNo());
		psmt.setLong(2, obj.getSyskey());
		ResultSet res = psmt.executeQuery();

		if (res.next()) {
			return true;
		} else {
			return false;
		}

	}
	public static boolean isLoanmemberOn(int totalBitStatus) {
		return isbitOn((int) Math.pow(2, ServerGlobal.getLoanmemberPos()), totalBitStatus);
	}
	public static boolean isGuarantorOn(int totalBitStatus) {
		return isbitOn((int) Math.pow(2, ServerGlobal.getGuarantorPos()), totalBitStatus);
	}
	public Result getPhoneNo(long lsyskey, Connection conn) throws SQLException {
        Result res = new Result();
        String query1 = "select m.t6 As Ph from loan l inner join member m on l.FK_MemberSysKey=m.syskey where l.LoanSysKey= ?";
        PreparedStatement stmt2 = conn.prepareStatement(query1);
        stmt2.setLong(1, lsyskey);

 

        ResultSet resph = stmt2.executeQuery();
        if(resph.next()) {
            res.setPhNo(resph.getString("Ph"));
        }
        return res;
    }
}
