package com.nirvasoft.mfi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.nirvasoft.mfi.mgr.KeyGenarateMgr;
import com.nirvasoft.mfi.users.data.StaffLoan;
import com.nirvasoft.mfi.users.data.StaffLoanApplicantImpl;
import com.nirvasoft.mfi.users.data.StaffLoanAppliedLoanListInterface;
import com.nirvasoft.mfi.users.data.StaffLoanDataImpl;
import com.nirvasoft.mfi.users.data.StaffLoanDataInterface;
import com.nirvasoft.mfi.users.data.StaffLoanGuarantorImpl;
import com.nirvasoft.mfi.users.data.StaffLoanInterface;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ServerGlobal;

public class StaffLoanDao {
	
	public static Result insertAppliedLoanList(StaffLoanAppliedLoanListInterface obj, Connection conn){
		Result res = new Result();

		try {
			obj.setRegistrationDate(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
			
			obj.setModifiedDate(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
			obj.setStatus(1);				
			obj.setAppliedLoanListID(getAppliedLoanID(conn));

			String sql ="insert into AppliedLoanList(AppliedLoanListSysKey,PK_AppliedLoanListId,FK_OutletProductLinkSysKey,T950,T951,T952,status) values (?,?,?,?,?,?,?)";
			PreparedStatement stmt = conn.prepareStatement(sql);

			int i = 1;
			stmt.setLong(i++, obj.getAppliedLoanListSyskey());
			stmt.setString(i++, obj.getAppliedLoanListID());	
			
			stmt.setLong(i++, obj.getOutletLoanTypeSyskey());
			stmt.setString(i++, obj.getRegistrationDate());
			stmt.setString(i++, obj.getModifiedDate());
			stmt.setString(i++, obj.getModifiedBy());
			stmt.setInt(i++, obj.getStatus());
			

			int rs = stmt.executeUpdate();

			if (rs > 0) {				
				res.setState(true);
				res.setMsgDesc("Saved Successfully");
				res.setMsgCode("0000");

			} else {
				res.setState(false);
				res.setMsgDesc("Cannot Save");
				res.setMsgCode("0014");
			}

		}catch (SQLException e) {
			res.setMsgDesc(e.toString());
			res.setMsgCode("0014");
			
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());				
				l_err.add("insertAppliedLoanList Function in StaffLoanDao.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}

		catch (Exception e) {
			res.setMsgDesc(e.toString());
			res.setMsgCode("0014");
			
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());				
				l_err.add("Applied Loan List Function in StaffLoanDao.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}

		return res;
	}
	public static String getAppliedLoanID(Connection aConn) {
		String ret = new String();

		Result res = new Result();
		res = KeyGenarateMgr.getKeyDataMgr("AppliedLoanList", 1);
		ret = res.getKeyString();
		return ret;
	}
	public StaffLoanInterface getLoanListByApplicationSyskeyandLoanSyskey(Connection conn, String syskey,
			String loansyskey) throws SQLException, ParseException {
		StaffLoanInterface appdata = new StaffLoan();

		String query1 = "Select distinct  l.[LoanSysKey], l.[PK_LoanId], l.[FK_MemberSysKey], "
						+"  l.[T1], l.[T10], l.[T11], l.[T34], l.[T35], l.[M4], l.[T70], l.[N2], l.[M2], l.[M3], l.[M5], l.[T68], l.[T69]"
						+",p.T2 ,l.status as LoanStatus , m.syskey ,m.PK_MemberId,m.t3 ,m.t4 ,m.t9 as frontnrc,m.t10 as backnrc,"
						+" m.T23,m.T24,l.T68,l.T69,l.M5 as basicincome,m.t920,m.t921 "
						+" , m.t922,m.t923,m.t924,m.t926,m.status as memberstatus,r.T1 as regval,t.T1 as townval"
						+" From Member m "
						+" inner join Loan L on m.syskey=l.FK_MemberSyskey"
						+" inner join LoanGuarantorList g on g.FK_LoanSysKey=l.[LoanSysKey]" 
						+" inner join AppliedLoanList al on al.AppliedLoanListSysKey=g.FK_AppliedLoanListSysKey "
						+" inner join OutletProductLink op on op.OutletProductLinkSysKey=al.FK_OutletProductLinkSysKey "
						+" inner join Product p on p.ProductSysKey=op.FK_ProductSysKey "
						+" inner join RegionReference r on r.RegionReferenceSysKey=m.t920"
						+"  inner join TownshipReference t on t.TownshipReferenceSysKey=m.t921"
						+" where  g.status=? and al.status=? and op.status=? and p.status=? and l.FK_MemberSysKey=? and l.LoanSysKey=?";

		
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		
		int i = 1;
		
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);		
		stmt2.setString(i++, syskey);
		stmt2.setString(i++, loansyskey);
		

		ResultSet res = stmt2.executeQuery();
		
		if (res.next()) {
			if(LoanDao.isbitOn(ServerGlobal.getLoanActivePos(), res.getInt("LoanStatus")))
			{
			
			StaffLoanDataImpl data=new StaffLoanDataImpl();
			data.setLoanSyskey(res.getLong("LoanSysKey"));
			data.setLoanID(res.getString("PK_LoanId"));
			
			data.setApplicantSyskey(res.getLong("FK_MemberSysKey"));
			
			String date = res.getString("T1");
			if (!date.equals("")) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
				Date d1 = sdf.parse(date);
				sdf.applyPattern("yyyy-MM-dd HH:mm:ss");
				data.setAppliedDate(sdf.format(d1));
			}			
					
			
			String imagepath = ServerGlobal.getStaffLoanImagePath().trim();
			
			data.setFrontFamilyRegImagePath(imagepath + res.getString("T10"));
			data.setBackFamilyRegImagePath(imagepath + res.getString("T11"));
			data.setStaffIDCardImagePath(imagepath + res.getString("T34"));
			data.setDepartmentChiefRecommendation(imagepath + res.getString("T35"));
			data.setLoanAmount(res.getDouble("M4"));
			data.setLoanAmountInWords(res.getString("T70"));

			
			data.setLoanTerm(res.getString("N2"));
			data.setBankInterestRate(res.getDouble("M2"));
			data.setServiceCharges(res.getDouble("M3"));
			data.setMonthlyBasicIncome(res.getDouble("M5"));
			data.setCurrentPosition(res.getString("T68"));
			data.setDepartment(res.getString("T69"));
			data.setLoanType(res.getString("T2"));
			
			data.setStatus(res.getInt("LoanStatus"));
			
			appdata.setStaffLoanData(data);
			
			
			StaffLoanApplicantImpl mdata = new StaffLoanApplicantImpl();
			mdata.setStaffSyskey(res.getLong("syskey"));
			mdata.setStaffID(res.getString("PK_MemberId"));
			
			mdata.setStaffName(res.getString("T3"));
			mdata.setStaffNrc(res.getString("T4"));			
			

			mdata.setFrontNRCImagePath(imagepath + res.getString("frontnrc"));
			mdata.setBackNRCImagePath(imagepath + res.getString("backnrc"));
			
			mdata.setPhotoPath(imagepath + res.getString("T23"));
			mdata.setPhotoPath1(imagepath + res.getString("T24"));
			
			mdata.setDepartment(res.getString("T68"));
			mdata.setCurrentPosition(res.getString("T69"));

			mdata.setMonthlyBasicIncome(res.getInt("basicincome"));
			
			mdata.setCurrentAddressRegion(res.getString("T920"));
			mdata.setCurrentAddressTownship(res.getString("T921"));
			mdata.setCurrentAddressStreet(res.getString("T922"));
			mdata.setCurrentAddressHouseNo(res.getString("T923"));
			mdata.setCurrentAddressPostalCode(res.getString("T924"));
			
			mdata.setMobile(res.getString("T926"));
			
			mdata.setStatus(res.getInt("memberstatus"));
			mdata.setRegionValue(res.getString("regval"));
			mdata.setTownshipValue(res.getString("townval"));
			appdata.setStaffLoanApplicant(mdata);
			
			ArrayList<StaffLoanGuarantorImpl> gualist=StaffLoanMemberDao.getGuarantorDataByLoanSyskey(conn, String.valueOf(data.getLoanSyskey()).trim());
			appdata.setStaffLoanGuarantor1( gualist.get(0));
			appdata.setStaffLoanGuarantor2( gualist.get(1));			
			
			
			}
		}
		

		return appdata;
	}
	public ArrayList<StaffLoanInterface> getLoanListByApplicationSyskeyandLoanStatus(Connection conn, String syskey,
			String loanstatus) throws SQLException, ParseException {
		ArrayList<StaffLoanInterface> loanlist = new ArrayList<StaffLoanInterface>();

		String query1 = "Select distinct  l.[LoanSysKey], l.[PK_LoanId], l.[FK_MemberSysKey], "
						+"  l.[T1], l.[T10], l.[T11], l.[T34], l.[T35], l.[M4], l.[T70], l.[N2], l.[M2], l.[M3], l.[M5], l.[T68], l.[T69]"
						+",p.T2 ,l.status as LoanStatus , m.syskey ,m.PK_MemberId,m.t3 ,m.t4 ,m.t9 as frontnrc,m.t10 as backnrc,"
						+" m.T23,m.T24,m.t14,m.t15,m.n2 as basicincome,m.t920,m.t921 "
						+" , m.t922,m.t923,m.t924,m.t926,m.status as memberstatus"
						+" From Member m "
						+" inner join Loan L on m.syskey=l.FK_MemberSyskey"
						+" inner join LoanGuarantorList g on g.FK_LoanSysKey=l.[LoanSysKey]" 
						+" inner join AppliedLoanList al on al.AppliedLoanListSysKey=g.FK_AppliedLoanListSysKey "
						+" inner join OutletProductLink op on op.OutletProductLinkSysKey=al.FK_OutletProductLinkSysKey "
						+" inner join Product p on p.ProductSysKey=op.FK_ProductSysKey "
						+" where  g.status=? and al.status=? and op.status=? and p.status=? and l.FK_MemberSysKey=?";

		if (!loanstatus.equals("") && !loanstatus.equals("")) {
			query1 += "  and l.status=?";
		}
		
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		
		int i = 1;
		
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);		
		stmt2.setString(i++, syskey);
		if (!loanstatus.equals("") && !loanstatus.equals("")) {
			stmt2.setString(i++, loanstatus);
		}

		ResultSet res = stmt2.executeQuery();
		
		while (res.next()) {
			if(LoanDao.isbitOn(ServerGlobal.getLoanActivePos(), res.getInt("LoanStatus")))
			{
			StaffLoanInterface appdata = new StaffLoan();
			StaffLoanDataImpl data=new StaffLoanDataImpl();
			data.setLoanSyskey(res.getLong("LoanSysKey"));
			data.setLoanID(res.getString("PK_LoanId"));
			
			data.setApplicantSyskey(res.getLong("FK_MemberSysKey"));
			
			String date = res.getString("T1");
			if (!date.equals("")) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
				Date d1 = sdf.parse(date);
				sdf.applyPattern("yyyy-MM-dd HH:mm:ss");
				data.setAppliedDate(sdf.format(d1));
			}			
					
			
			String imagepath = ServerGlobal.getStaffLoanImagePath().trim();
			
			data.setFrontFamilyRegImagePath(imagepath + res.getString("T10"));
			data.setBackFamilyRegImagePath(imagepath + res.getString("T11"));
			data.setStaffIDCardImagePath(imagepath + res.getString("T34"));
			data.setDepartmentChiefRecommendation(imagepath + res.getString("T35"));
			data.setLoanAmount(res.getDouble("M4"));
			data.setLoanAmountInWords(res.getString("T70"));

			
			data.setLoanTerm(res.getString("N2"));
			data.setBankInterestRate(res.getDouble("M2"));
			data.setServiceCharges(res.getDouble("M3"));
			data.setMonthlyBasicIncome(res.getDouble("M5"));
			data.setCurrentPosition(res.getString("T68"));
			data.setDepartment(res.getString("T69"));
			data.setLoanType(res.getString("T2"));
			
			data.setStatus(res.getInt("LoanStatus"));
			
			appdata.setStaffLoanData(data);
			
			
			StaffLoanApplicantImpl mdata = new StaffLoanApplicantImpl();
			mdata.setStaffSyskey(res.getLong("syskey"));
			mdata.setStaffID(res.getString("PK_MemberId"));
			
			mdata.setStaffName(res.getString("T3"));
			mdata.setStaffNrc(res.getString("T4"));			
			

			mdata.setFrontNRCImagePath(imagepath + res.getString("frontnrc"));
			mdata.setBackNRCImagePath(imagepath + res.getString("backnrc"));
			
			mdata.setPhotoPath(imagepath + res.getString("T23"));
			mdata.setPhotoPath1(imagepath + res.getString("T24"));
			
			mdata.setDepartment(res.getString("T14"));
			mdata.setCurrentPosition(res.getString("T15"));

			mdata.setMonthlyBasicIncome(res.getInt("basicincome"));
			
			mdata.setCurrentAddressRegion(res.getString("T920"));
			mdata.setCurrentAddressTownship(res.getString("T921"));
			mdata.setCurrentAddressStreet(res.getString("T922"));
			mdata.setCurrentAddressHouseNo(res.getString("T923"));
			mdata.setCurrentAddressPostalCode(res.getString("T924"));
			
			mdata.setMobile(res.getString("T926"));
			
			mdata.setStatus(res.getInt("memberstatus"));
			appdata.setStaffLoanApplicant(mdata);
			
			ArrayList<StaffLoanGuarantorImpl> gualist=StaffLoanMemberDao.getGuarantorDataByLoanSyskey(conn, String.valueOf(data.getLoanSyskey()).trim());
			appdata.setStaffLoanGuarantor1( gualist.get(0));
			appdata.setStaffLoanGuarantor2( gualist.get(1));			
			
			loanlist.add(appdata);
			}
		}
		

		return loanlist;
	}
	public static Result updateLoanStatus(StaffLoanDataInterface obj, Connection conn) {
		Result res = new Result();

		try {			

			obj.setModifiedDate(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
			

			String sql = "UPDATE [Loan] SET status=?,T951=? where LoanSyskey=? ";

			PreparedStatement stmt = conn.prepareStatement(sql);

			int i = 1;
			
			stmt.setInt(i++, obj.getStatus());
			stmt.setString(i++, obj.getModifiedDate());
			stmt.setLong(i++, obj.getLoanSyskey());
			int rs = stmt.executeUpdate();

			if (rs > 0) {			
				res.setState(true);
				res.setMsgDesc("Saved Successfully");
				res.setMsgCode("0000");
				

			} else {
				res.setState(false);
				res.setMsgDesc("Cannot Save");
				res.setMsgCode("0014");
			}

		}
		catch (SQLException e) {
			res.setMsgDesc(e.toString());
			res.setMsgCode("0014");
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				
				l_err.add("updateLoanStatus Function StaffLoanDao.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}
		catch (Exception e) {
			res.setMsgDesc(e.toString());
			res.setMsgCode("0014");
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				
				l_err.add("updateLoanStatus Function StaffLoanDao.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}

		return res;
	}
	public static Result insertLone(StaffLoanDataInterface obj, Connection conn) {
		Result res = new Result();

		try {

			int pretotal = 0;

			pretotal = LoanDao.OnBitLoanTotalNewValue(ServerGlobal.getLoanActivePos(), pretotal);

			//pretotal = LoanDao.OnBitLoanTotalNewValue(ServerGlobal.getLoanInprocessPos(), pretotal);
			String nowdate=new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
			//obj.setAppliedDate(nowdate);
			obj.setRegistrationDate(nowdate);
			obj.setModifiedDate(nowdate);
			obj.setStatus(pretotal);
			obj.setLoanID(getLoanID(conn));
			

			String sql = "insert into  [loan]([LoanSysKey],[PK_LoanId],[FK_MemberSysKey],[FK_MemberGuarantorSysKey],[FK_PaymentTearmSysKey],"
						+"[N1],[T9],[T1],[T10],[T11],[T34],[T35],[M4],[T70],[N2],[M2],[M3],[M5],[T68],[T69],[T950],[T951],[T952],[drvTotalPrice],[drvNetPrice],[status])"
						+" values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement stmt = conn.prepareStatement(sql);
			int i = 1;
			stmt.setLong(i++, obj.getLoanSyskey());
			stmt.setString(i++, obj.getLoanID());
			stmt.setLong(i++, obj.getApplicantSyskey());
			stmt.setLong(i++, 0);
			stmt.setLong(i++, 0);
			stmt.setInt(i++, 0);
			stmt.setString(i++, "12");
			stmt.setString(i++, obj.getAppliedDate());
			stmt.setString(i++, obj.getFrontFamilyRegImagePath());
			stmt.setString(i++, obj.getBackFamilyRegImagePath());
			stmt.setString(i++, obj.getStaffIDCardImagePath());
			stmt.setString(i++, obj.getDepartmentChiefRecommendation());
			stmt.setDouble(i++, obj.getLoanAmount());
			stmt.setString(i++, obj.getLoanAmountInWords());
			stmt.setString(i++, obj.getLoanTerm());
			stmt.setDouble(i++, obj.getBankInterestRate());
			stmt.setDouble(i++, obj.getServiceCharges());
			stmt.setDouble(i++, obj.getMonthlyBasicIncome());
			stmt.setString(i++, obj.getCurrentPosition());
			stmt.setString(i++, obj.getDepartment());
			stmt.setString(i++, obj.getRegistrationDate());
			stmt.setString(i++, obj.getModifiedDate());
			stmt.setString(i++, obj.getModifiedBy());
			stmt.setDouble(i++, obj.getLoanAmount());
			stmt.setDouble(i++, obj.getLoanAmount());
			
			stmt.setInt(i++, 3);	

			int rs = stmt.executeUpdate();

			if (rs > 0) {
				res.setSyskey(obj.getLoanSyskey());
				res.setState(true);
				res.setMsgDesc("Saved Successfully");
				res.setMsgCode("0000");

			} else {
				res.setState(false);
				res.setMsgDesc("Cannot Save");
				res.setMsgCode("0014");
			}

		}catch (SQLException e) {
			res.setMsgDesc(e.toString());
			res.setMsgCode("0014");
			
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());				
				l_err.add("insertLone Function in StaffLoanDao.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}

		catch (Exception e) {
			res.setMsgDesc(e.toString());
			res.setMsgCode("0014");
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());				
				l_err.add("insertLone Function in StaffLoanDao.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}

		return res;
	}
	public static String getLoanID(Connection aConn) {
		String ret = new String();

		Result res = new Result();
		res = KeyGenarateMgr.getKeyDataMgr("Loan", 1);
		ret = res.getKeyString();
		return ret;
	}
}
