package com.nirvasoft.mfi.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.mfi.users.data.TownshipData;

public class TownshipReferenceDao {
	public ArrayList<TownshipData> getAllTownshipByRegion(Connection conn, long resionsyskey) throws SQLException {
		ArrayList<TownshipData> townshipList = new ArrayList<TownshipData>();

		String query1 = "Select TownshipReferenceSysKey,PK_TownshipSysKey,FK_RegionSysKey,T1,T950,T951,T952,status from [TownshipReference] where  status= ? and FK_RegionSysKey=?";
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setInt(i++, 1);
		stmt2.setLong(i++, resionsyskey);

		ResultSet res = stmt2.executeQuery();
		while (res.next()) {
			TownshipData townshipData = new TownshipData();
			townshipData.setTownshipRefSyskey(res.getLong("TownshipReferenceSysKey"));
			townshipData.setTownshipId(res.getString("PK_TownshipSysKey"));
			townshipData.setRegionRefSyskey(res.getLong("FK_RegionSysKey"));

			townshipData.setDescription(res.getString("T1"));
			townshipData.setRegistrationDate(res.getString("T950"));
			townshipData.setModifiedDate(res.getString("T951"));
			townshipData.setModifiedBy(res.getString("T952"));
			townshipData.setStatus(res.getInt("status"));
			townshipList.add(townshipData);
		}

		return townshipList;
	}

}
