package com.nirvasoft.mfi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.nirvasoft.mfi.framework.Result;
import com.nirvasoft.mfi.mgr.KeyGenarateMgr;
import com.nirvasoft.mfi.users.data.PaymentInstallmentData;
import com.nirvasoft.mfi.users.data.StaffLoanPaymentInstallmentData;
import com.nirvasoft.mfi.users.data.StaffLoanPaymentInstallmentInterface;


public class PaymentInstallmentDao {

//	public static Result insertPaymentInstallment(PaymentInstallmentData obj, Connection conn) throws SQLException {
//		Result res = new Result();
//
//		try {
//
//			obj.setRegistration_date(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
//			obj.setModified_date(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
//			obj.setStatus(1);
//			obj.setPaymentInstallmentId(getPaymentInstallmentID(conn));
//
//			String sql = "insert into  [mfi_mobile].[PaymentInstallment](PaymentInstallmentSysKey,PK_PaymentInstallmentId,FK_LoanSysKey,N1,N2,N3,T1,N4,T2,T3,T950,T951,T952,status)"
//					+ " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
//			PreparedStatement stmt = conn.prepareStatement(sql);
//
//			int i = 1;
//			stmt.setLong(i++, obj.getPaymentInstallmentSysKey());
//			stmt.setString(i++, obj.getPaymentInstallmentId());
//			stmt.setLong(i++, obj.getLoansyskey());
//			stmt.setString(i++, obj.getPaymentTerm());
//			stmt.setInt(i++, obj.getPeroid());
//			stmt.setDouble(i++, obj.getMonthlypayment());
//			stmt.setString(i++, obj.getPaydate());
//			stmt.setInt(i++, obj.getPaymenttype());
//			stmt.setString(i++, obj.getMembername());
//			stmt.setString(i++, obj.getNrc());
//			stmt.setString(i++, obj.getRegistration_date());
//			stmt.setString(i++, obj.getModified_date());
//			stmt.setString(i++, obj.getModifiedby());
//			stmt.setInt(i++, obj.getStatus());
//
//			int rs = stmt.executeUpdate();
//
//			if (rs > 0) {
//				res.setState(true);
//				res.setMsgDesc("Saved Successfully");
//				res.setMsgCode("0000");
//
//			} else {
//				res.setState(false);
//				res.setMsgDesc("Cannot Save");
//				res.setMsgCode("0014");
//			}
//
//		}
//
//		catch (Exception e) {
//			res.setMsgDesc("Cannot Save");
//			res.setMsgCode("0014");
//			e.printStackTrace();
//		}
//
//		return res;
//	}

//	public static String getPaymentInstallmentID(Connection aConn) {
//		String ret = new String();
//
//		Result res = new Result();
//		res = KeyGenarateMgr.getKeyDataMgr("PaymentInstallment", 1);
//		ret = res.getKeyString();
//		return ret;
//	}

	public static boolean isPaymentPeriodByLoanidExist(PaymentInstallmentData obj, Connection conn)
			throws SQLException {

		String sql = "select PaymentInstallmentSysKey from PaymentInstallment where status<>? and FK_LoanSysKey=? and N2=?  ";
		PreparedStatement psmt = conn.prepareStatement(sql);
		int i = 1;

		psmt.setInt(i++, 4);
		psmt.setLong(i++, obj.getLoansyskey());
		psmt.setInt(i++, obj.getPeroid());
		ResultSet res = psmt.executeQuery();

		if (res.next()) {
			return true;
		} else {
			return false;
		}

	}

	public static boolean isPayDateByLoanidExist(PaymentInstallmentData obj, Connection conn) throws SQLException {

		String sql = "select PaymentInstallmentSysKey from PaymentInstallment where status<>? and FK_LoanSysKey=? and  left(convert(datetime,SUBSTRING(T1,1,8),112),6)  = left(convert(datetime,SUBSTRING(?,1,8),112) ,6) ";
		PreparedStatement psmt = conn.prepareStatement(sql);
		int i = 1;

		psmt.setInt(i++, 4);
		psmt.setLong(i++, obj.getLoansyskey());
		psmt.setString(i++, obj.getPaydate());
		ResultSet res = psmt.executeQuery();

		if (res.next()) {
			return true;
		} else {
			return false;
		}

	}

	public static ArrayList<PaymentInstallmentData> getPaymentInstallmentDataByLoanSyskey(Connection conn,
			String loansyskey) throws SQLException, ParseException {
		ArrayList<PaymentInstallmentData> datalist = new ArrayList<PaymentInstallmentData>();

		String query1 = "select p.PaymentInstallmentSysKey,p.PK_PaymentInstallmentId,p.FK_LoanSysKey,"
				+ " p.N1 as paymentterm,p.N2 as period,p.N3 as monthlyamount ,p.T1 as paydate,"
				+ " p.N4 as paymenttype,p.T2 as membername,p.T3 as nrc,p.T950,p.T951,p.T952,p.status,l.PK_LoanId"
				+ " from PaymentInstallment p inner join Loan as l on p.FK_LoanSysKey =l.LoanSysKey"
				+ " where p.status=? and FK_LoanSysKey=?  ";
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setInt(i++, 1);
		stmt2.setString(i++, loansyskey);
		ResultSet res = stmt2.executeQuery();
		while (res.next()) {
			PaymentInstallmentData data = new PaymentInstallmentData();
			data.setPaymentInstallmentSysKey(res.getLong("PaymentInstallmentSysKey"));
			data.setPaymentInstallmentId(res.getString("PK_PaymentInstallmentId"));
			data.setLoansyskey(res.getLong("FK_LoanSysKey"));
			data.setLoanId(res.getString("PK_LoanId"));
			data.setPaymentTerm(res.getString("paymentterm"));
			data.setPeroid(res.getInt("period"));
			data.setMonthlyamount(res.getDouble("monthlyamount"));
			String date1 = res.getString("paydate");
			if (!date1.equals("")) {
				SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMddHHmmss");
				Date d11 = sdf1.parse(date1);
				sdf1.applyPattern("yyyy-MM-dd");

				data.setPaydate(sdf1.format(d11));
			}
			data.setPaymenttype(res.getInt("paymenttype"));
			data.setMembername(res.getString("membername"));
			data.setNrc(res.getString("nrc"));
			data.setStatus(res.getInt("status"));
			datalist.add(data);

		}

		return datalist;
	}

	public static ArrayList<PaymentInstallmentData> getLoanDataByPaymentInProgress(Connection l_Conn)
			throws SQLException {

		ArrayList<PaymentInstallmentData> datalist = new ArrayList<PaymentInstallmentData>();
		String l_Query = "select LoanSysKey,PK_LoanId" + " from Loan where status=31 ORDER BY PK_LoanId";
		PreparedStatement pstmt = l_Conn.prepareStatement(l_Query);

		ResultSet res = pstmt.executeQuery();

		while (res.next()) {
			PaymentInstallmentData data = new PaymentInstallmentData();

			data.setLoansyskey(res.getLong("LoanSysKey"));
			data.setLoanId(res.getString("PK_LoanId"));

			datalist.add(data);
		}

		return datalist;
	}
	//to access data from MRHF table
	public static String getReferenceIdForMRHF(Connection l_Conn,String loansyskey)
			throws SQLException {

		String refid="";
		String l_Query = "select AppID from MobileDataImport WHERE LoanSysKey=? and RecordStatus <> 4";
		PreparedStatement pstmt = l_Conn.prepareStatement(l_Query);
		int i = 1;		
		pstmt.setString(i++, loansyskey);
		ResultSet res = pstmt.executeQuery();

		if (res.next()) {
			refid=res.getString("AppID");
		}

		return refid;
	}

	public static ArrayList<PaymentInstallmentData> getPaymentScheduleByLoanSyskey(Connection conn,
			String ref1, String ref2) throws SQLException, ParseException {
		ArrayList<PaymentInstallmentData> datalist = new ArrayList<PaymentInstallmentData>();

		String query1 = "select syskey,Ref1,Ref2,Month as period,Payment as monthlyamount,"
						+ "Interest,Principal,N7 as closingbalance,N6 AS OpeningBalance,T2 as duedate,n10 as outstandingrepayment"
						+ " From LoanRepaySchedule where Ref1=? and Ref2=?";
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;		
		stmt2.setString(i++, ref1);
		stmt2.setString(i++, ref2);
		ResultSet res = stmt2.executeQuery();
		while (res.next()) {
			PaymentInstallmentData data = new PaymentInstallmentData();
			data.setPaymentInstallmentSysKey(res.getLong("syskey"));
			data.setRef1(res.getString("Ref1"));
			data.setRef2(res.getString("Ref2"));	
			
			data.setPeroid(res.getInt("period"));
			data.setMonthlyamount(res.getDouble("monthlyamount"));
			data.setInterest(res.getDouble("Interest"));
			data.setPrincipal(res.getDouble("Principal"));
			data.setClosingbalance(res.getDouble("closingbalance"));
			data.setOpeningbalance(res.getDouble("OpeningBalance"));
			String date1 = res.getString("duedate");
			if (!date1.equals("")) {
				SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
				Date d11 = sdf1.parse(date1);
				sdf1.applyPattern("dd-MMM-yyyy");

				data.setDuedate(sdf1.format(d11));
			}
			data.setOutstandingrepayment(res.getDouble("outstandingrepayment"));
			
			datalist.add(data);

		}

		return datalist;
	}

	public static ArrayList<StaffLoanPaymentInstallmentInterface> getStaffLoanPaymentScheduleByLoanSyskey(Connection conn,
			String loansyskey) throws SQLException, ParseException {
		ArrayList<StaffLoanPaymentInstallmentInterface> datalist = new ArrayList<StaffLoanPaymentInstallmentInterface>();

		String query1 = "select BatchNo,SerialNo,RepayDate,RepayAmount,InterestAmount,PrincipleAmount,"
				+ "MonthlyRepayment,ActualRepayDate,Status,TransRef,N1,N3"
				+ " from StaffLoanRepayment where TransRef=?";
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;		
		stmt2.setString(i++, loansyskey);
		ResultSet res = stmt2.executeQuery();
		while (res.next()) {
			StaffLoanPaymentInstallmentInterface data = new StaffLoanPaymentInstallmentData();
			data.setBatchNo(res.getInt("BatchNo"));
			data.setSerialNo(res.getString("SerialNo"));			
			
			data.setRepayAmount(res.getInt("RepayAmount"));
			data.setInterestAmount(res.getDouble("InterestAmount"));
			data.setPricipleAmount(res.getDouble("PrincipleAmount"));
			data.setMonthlyRepayment(res.getDouble("MonthlyRepayment"));
			data.setStatus(res.getInt("Status"));
			data.setMonth(res.getInt("N1"));
			data.setClosingBalance(res.getDouble("N3"));
			
			
			datalist.add(data);

		}

		return datalist;
	}

}
