package com.nirvasoft.mfi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import com.nirvasoft.mfi.users.data.ShopListData;

public class ShopListDao {

	public ArrayList<ShopListData> getShopList(Connection conn) throws SQLException {
		ArrayList<ShopListData> shoplist = new ArrayList<ShopListData>();

		String query1 = "SELECT  MFIOrganizationSysKey,MFIOrgDealerLinkSysKey,DealerSysKey, OutletSysKey,PK_MFIOrganizationId,PK_DealerId,PK_OutletId,"
				+ " dealerstatus,  orgstatus,   outstaus, orgdealerstatus,"
				+ " T2,T3,T4, T5,T6,T7, T8,T9  , T920, T921, T922,T923,"
				+ " T924, T925, T926,T927,T928,T950, T951, T952,region ,township,T10 "
				+ " from VI_OrgDealerOutlet where orgstatus=? and dealerstatus=? and outstaus=? and orgdealerstatus=?";
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);

		ResultSet res = stmt2.executeQuery();
		while (res.next()) {
			ShopListData shoplistData = new ShopListData();
			shoplistData.setOrganizationSyskey(res.getLong("MFIOrganizationSysKey"));
			shoplistData.setOrgDearLinkSyskey(res.getLong("MFIOrgDealerLinkSysKey"));
			shoplistData.setDealerSyskey(res.getLong("DealerSysKey"));
			shoplistData.setOutletSyskey(res.getLong("OutletSysKey"));
			shoplistData.setOrganizationId(res.getString("PK_MFIOrganizationId"));
			shoplistData.setDealerid(res.getString("PK_DealerId"));
			shoplistData.setOutletId(res.getString("PK_OutletId"));
			shoplistData.setContactName(res.getString("T2"));
			shoplistData.setContactPhone(res.getString("T3"));
			shoplistData.setContactEmail(res.getString("T4"));
			shoplistData.setContactPosition(res.getString("T5"));
			shoplistData.setContactDept(res.getString("T6"));
			shoplistData.setLongitude(res.getString("T7"));
			shoplistData.setLatitude(res.getString("T8"));
			shoplistData.setOutletName(res.getString("T9"));
			shoplistData.setOutletCurrentAddressRegion(res.getString("region"));
			shoplistData.setOutletCurrentAddressTownship(res.getString("township"));
			shoplistData.setOutletCurrentAddressStreet(res.getString("T922"));
			shoplistData.setOutletCurrentAddressHomeNo(res.getString("T923"));
			shoplistData.setEmail(res.getString("T925"));
			shoplistData.setMobile(res.getString("T926"));
			shoplistData.setLandlinePhone(res.getString("T927"));
			shoplistData.setFax(res.getString("T928"));
			String outletname = "outlet/image/";

			shoplistData.setOutletImagePath(outletname + res.getString("T10"));
			shoplist.add(shoplistData);
		}

		return shoplist;
	}

	public ArrayList<ShopListData> getShopListByRegTownship1(Connection conn, String region, String township)
			throws SQLException {
		ArrayList<ShopListData> shoplist = new ArrayList<ShopListData>();

		String query1 = "SELECT  MFIOrganizationSysKey,MFIOrgDealerLinkSysKey,DealerSysKey, OutletSysKey,PK_MFIOrganizationId,PK_DealerId,PK_OutletId,"
				+ " dealerstatus,  orgstatus,   outstaus, orgdealerstatus,"
				+ " T2,T3,T4, T5,T6,T7, T8,T9  , T920, T921, T922,T923,"
				+ " T924, T925, T926,T927,T928,T950, T951, T952,region ,township,T10 "
				+ " from VI_OrgDealerOutlet where orgstatus=? and dealerstatus=? and outstaus=? and orgdealerstatus=? and  T920=? and T921=?";
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setString(i++, region);
		stmt2.setString(i++, township);

		ResultSet res = stmt2.executeQuery();
		while (res.next()) {
			ShopListData shoplistData = new ShopListData();
			shoplistData.setOrganizationSyskey(res.getLong("MFIOrganizationSysKey"));
			shoplistData.setOrgDearLinkSyskey(res.getLong("MFIOrgDealerLinkSysKey"));
			shoplistData.setDealerSyskey(res.getLong("DealerSysKey"));
			shoplistData.setOutletSyskey(res.getLong("OutletSysKey"));
			shoplistData.setOrganizationId(res.getString("PK_MFIOrganizationId"));
			shoplistData.setDealerid(res.getString("PK_DealerId"));
			shoplistData.setOutletId(res.getString("PK_OutletId"));
			shoplistData.setContactName(res.getString("T2"));
			shoplistData.setContactPhone(res.getString("T3"));
			shoplistData.setContactEmail(res.getString("T4"));
			shoplistData.setContactPosition(res.getString("T5"));
			shoplistData.setContactDept(res.getString("T6"));
			shoplistData.setLongitude(res.getString("T7"));
			shoplistData.setLatitude(res.getString("T8"));
			shoplistData.setOutletName(res.getString("T9"));
			shoplistData.setOutletCurrentAddressRegion(res.getString("region"));
			shoplistData.setOutletCurrentAddressTownship(res.getString("township"));
			shoplistData.setOutletCurrentAddressStreet(res.getString("T922"));
			shoplistData.setOutletCurrentAddressHomeNo(res.getString("T923"));
			shoplistData.setEmail(res.getString("T925"));
			shoplistData.setMobile(res.getString("T926"));
			shoplistData.setLandlinePhone(res.getString("T927"));
			shoplistData.setFax(res.getString("T928"));
			String outletname = "outlet/image/";

			shoplistData.setOutletImagePath(outletname + res.getString("T10"));
			shoplist.add(shoplistData);
		}

		return shoplist;
	}

	public ArrayList<ShopListData> getShopListByRegTownship(Connection conn, String region, String township)
			throws SQLException {
		ArrayList<ShopListData> shoplist = new ArrayList<ShopListData>();

		String query1 = "SELECT  MFIOrganizationSysKey,MFIOrgDealerLinkSysKey,DealerSysKey, OutletSysKey,PK_MFIOrganizationId,PK_DealerId,PK_OutletId,"
				+ " dealerstatus,  orgstatus,   outstaus, orgdealerstatus,"
				+ " T2,T3,T4, T5,T6,T7, T8,T9  , T920, T921, T922,T923,"
				+ " T924, T925, T926,T927,T928,T950, T951, T952,region ,township,T10"
				+ " from VI_OrgDealerOutlet where orgstatus=? and dealerstatus=? and outstaus=? and orgdealerstatus=?  ";
		if (region != "" && region != null && region != "") {
			query1 += " and  T920=?";
		}
		if (township != "" && township != null && township != "") {
			query1 += " and  T921=?";
		}
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		if (region != "" && region != null && region != "") {
			stmt2.setString(i++, region);
		}
		if (township != "" && township != null && township != "") {
			stmt2.setString(i++, township);
		}

		ResultSet res = stmt2.executeQuery();
		while (res.next()) {
			ShopListData shoplistData = new ShopListData();
			shoplistData.setOrganizationSyskey(res.getLong("MFIOrganizationSysKey"));
			shoplistData.setOrgDearLinkSyskey(res.getLong("MFIOrgDealerLinkSysKey"));
			shoplistData.setDealerSyskey(res.getLong("DealerSysKey"));
			shoplistData.setOutletSyskey(res.getLong("OutletSysKey"));
			shoplistData.setOrganizationId(res.getString("PK_MFIOrganizationId"));
			shoplistData.setDealerid(res.getString("PK_DealerId"));
			shoplistData.setOutletId(res.getString("PK_OutletId"));
			shoplistData.setContactName(res.getString("T2"));
			shoplistData.setContactPhone(res.getString("T3"));
			shoplistData.setContactEmail(res.getString("T4"));
			shoplistData.setContactPosition(res.getString("T5"));
			shoplistData.setContactDept(res.getString("T6"));
			shoplistData.setLongitude(res.getString("T7"));
			shoplistData.setLatitude(res.getString("T8"));
			shoplistData.setOutletName(res.getString("T9"));
			shoplistData.setOutletCurrentAddressRegion(res.getString("region"));
			shoplistData.setOutletCurrentAddressTownship(res.getString("township"));
			shoplistData.setOutletCurrentAddressStreet(res.getString("T922"));
			shoplistData.setOutletCurrentAddressHomeNo(res.getString("T923"));
			shoplistData.setEmail(res.getString("T925"));
			shoplistData.setMobile(res.getString("T926"));
			shoplistData.setLandlinePhone(res.getString("T927"));
			shoplistData.setFax(res.getString("T928"));
			String outletname = "outlet/image/";

			shoplistData.setOutletImagePath(outletname + res.getString("T10"));

			shoplist.add(shoplistData);
		}

		return shoplist;
	}

	public ArrayList<ShopListData> getShopListByApplicantSyskey(Connection conn, String syskey) throws SQLException {
		String region = "";
		try {
			region = MobileMemberDao.getRegionBySyskey(conn, syskey);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ArrayList<ShopListData> shoplist = new ArrayList<ShopListData>();

		String query1 = "SELECT  MFIOrganizationSysKey,MFIOrgDealerLinkSysKey,DealerSysKey, OutletSysKey,PK_MFIOrganizationId,PK_DealerId,PK_OutletId,"
				+ " dealerstatus,  orgstatus,   outstaus, orgdealerstatus,"
				+ " T2,T3,T4, T5,T6,T7, T8,T9  , T920, T921, T922,T923,"
				+ " T924, T925, T926,T927,T928,T950, T951, T952,region ,township,T10"
				+ " from VI_OrgDealerOutlet where orgstatus=? and dealerstatus=? and outstaus=? and orgdealerstatus=? ";
		if (!region.equals("")) {
			query1 += " and  T920 =" + region;// and T921=(select T921 from
												// mfi_mobile.Member where
												// syskey=?)";
		}
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);

		// stmt2.setString(i++,syskey);

		ResultSet res = stmt2.executeQuery();
		while (res.next()) {
			ShopListData shoplistData = new ShopListData();
			shoplistData.setOrganizationSyskey(res.getLong("MFIOrganizationSysKey"));
			shoplistData.setOrgDearLinkSyskey(res.getLong("MFIOrgDealerLinkSysKey"));
			shoplistData.setDealerSyskey(res.getLong("DealerSysKey"));
			shoplistData.setOutletSyskey(res.getLong("OutletSysKey"));
			shoplistData.setOrganizationId(res.getString("PK_MFIOrganizationId"));
			shoplistData.setDealerid(res.getString("PK_DealerId"));
			shoplistData.setOutletId(res.getString("PK_OutletId"));
			shoplistData.setContactName(res.getString("T2"));
			shoplistData.setContactPhone(res.getString("T3"));
			shoplistData.setContactEmail(res.getString("T4"));
			shoplistData.setContactPosition(res.getString("T5"));
			shoplistData.setContactDept(res.getString("T6"));
			shoplistData.setLongitude(res.getString("T7"));
			shoplistData.setLatitude(res.getString("T8"));
			shoplistData.setOutletName(res.getString("T9"));
			shoplistData.setOutletCurrentAddressRegion(res.getString("region"));
			shoplistData.setOutletCurrentAddressTownship(res.getString("township"));
			shoplistData.setOutletCurrentAddressStreet(res.getString("T922"));
			shoplistData.setOutletCurrentAddressHomeNo(res.getString("T923"));
			shoplistData.setEmail(res.getString("T925"));
			shoplistData.setMobile(res.getString("T926"));
			shoplistData.setLandlinePhone(res.getString("T927"));
			shoplistData.setFax(res.getString("T928"));
			String outletname = "outlet/image/";

			shoplistData.setOutletImagePath(outletname + res.getString("T10"));
			shoplist.add(shoplistData);
		}

		return shoplist;
	}

	public ShopListData getShopListByLoanSyskey(Connection conn, String syskey) throws SQLException {

		String query1 = "SELECT  MFIOrganizationSysKey,MFIOrgDealerLinkSysKey,DealerSysKey, OutletSysKey,PK_MFIOrganizationId,PK_DealerId,PK_OutletId,"
				+ "dealerstatus,  orgstatus,   outstaus, orgdealerstatus, "
				+ "T2,T3,T4, T5,T6,T7, T8,T9  , T920, T921, T922,T923,"
				+ "T924, T925, T926,T927,T928,T950, T951, T952,region ,township ,T10 "
				+ " from VI_OrgDealerOutlet where orgstatus=? and dealerstatus=? and outstaus=? and orgdealerstatus=?"
				+ "and OutletSysKey ="
				+ "(SELECT OutletSysKey FROM VI_OutletProductPricePromotion WHERE OutletProductLinkSysKey"
				+ " IN(select FK_OutLetProductSysKey from AppliedProductList WHERE FK_LoanSysKey=?) GROUP BY OutletSysKey)";

		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setString(i++, syskey);

		ShopListData shoplistData = new ShopListData();
		ResultSet res = stmt2.executeQuery();
		if (res.next()) {

			shoplistData.setOrganizationSyskey(res.getLong("MFIOrganizationSysKey"));
			shoplistData.setOrgDearLinkSyskey(res.getLong("MFIOrgDealerLinkSysKey"));
			shoplistData.setDealerSyskey(res.getLong("DealerSysKey"));
			shoplistData.setOutletSyskey(res.getLong("OutletSysKey"));
			shoplistData.setOrganizationId(res.getString("PK_MFIOrganizationId"));
			shoplistData.setDealerid(res.getString("PK_DealerId"));
			shoplistData.setOutletId(res.getString("PK_OutletId"));
			shoplistData.setContactName(res.getString("T2"));
			shoplistData.setContactPhone(res.getString("T3"));
			shoplistData.setContactEmail(res.getString("T4"));
			shoplistData.setContactPosition(res.getString("T5"));
			shoplistData.setContactDept(res.getString("T6"));
			shoplistData.setLongitude(res.getString("T7"));
			shoplistData.setLatitude(res.getString("T8"));
			shoplistData.setOutletName(res.getString("T9"));
			shoplistData.setOutletCurrentAddressRegion(res.getString("region"));
			shoplistData.setOutletCurrentAddressTownship(res.getString("township"));
			shoplistData.setOutletCurrentAddressStreet(res.getString("T922"));
			shoplistData.setOutletCurrentAddressHomeNo(res.getString("T923"));
			shoplistData.setEmail(res.getString("T925"));
			shoplistData.setMobile(res.getString("T926"));
			shoplistData.setLandlinePhone(res.getString("T927"));
			shoplistData.setFax(res.getString("T928"));
			String outletname = "outlet/image/";

			shoplistData.setOutletImagePath(outletname + res.getString("T10"));
		}

		return shoplistData;
	}
	
	

	public ArrayList<ShopListData> getShopListByRegionSyskey(Connection conn, String regionsyskey) throws SQLException {
			

		ArrayList<ShopListData> shoplist = new ArrayList<ShopListData>();

		String query1 = "SELECT  MFIOrganizationSysKey,MFIOrgDealerLinkSysKey,DealerSysKey, OutletSysKey,PK_MFIOrganizationId,PK_DealerId,PK_OutletId,"
				+ " dealerstatus,  orgstatus,   outstaus, orgdealerstatus,"
				+ " T2,T3,T4, T5,T6,T7, T8,T9  , T920, T921, T922,T923,"
				+ " T924, T925, T926,T927,T928,T950, T951, T952,region ,township ,T10"
				+ " from VI_OrgDealerOutlet where orgstatus=? and dealerstatus=? and outstaus=? and orgdealerstatus=? ";
		query1 += " and  T920 =" + regionsyskey;
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);

		// stmt2.setString(i++,syskey);

		ResultSet res = stmt2.executeQuery();
		while (res.next()) {
			ShopListData shoplistData = new ShopListData();
			shoplistData.setOrganizationSyskey(res.getLong("MFIOrganizationSysKey"));
			shoplistData.setOrgDearLinkSyskey(res.getLong("MFIOrgDealerLinkSysKey"));
			shoplistData.setDealerSyskey(res.getLong("DealerSysKey"));
			shoplistData.setOutletSyskey(res.getLong("OutletSysKey"));
			shoplistData.setOrganizationId(res.getString("PK_MFIOrganizationId"));
			shoplistData.setDealerid(res.getString("PK_DealerId"));
			shoplistData.setOutletId(res.getString("PK_OutletId"));
			shoplistData.setContactName(res.getString("T2"));
			shoplistData.setContactPhone(res.getString("T3"));
			shoplistData.setContactEmail(res.getString("T4"));
			shoplistData.setContactPosition(res.getString("T5"));
			shoplistData.setContactDept(res.getString("T6"));
			shoplistData.setLongitude(res.getString("T7"));
			shoplistData.setLatitude(res.getString("T8"));
			shoplistData.setOutletName(res.getString("T9"));
			shoplistData.setOutletCurrentAddressRegion(res.getString("region"));
			shoplistData.setOutletCurrentAddressTownship(res.getString("township"));
			shoplistData.setOutletCurrentAddressStreet(res.getString("T922"));
			shoplistData.setOutletCurrentAddressHomeNo(res.getString("T923"));
			shoplistData.setEmail(res.getString("T925"));
			shoplistData.setMobile(res.getString("T926"));
			shoplistData.setLandlinePhone(res.getString("T927"));
			shoplistData.setFax(res.getString("T928"));
			String outletname = "outlet/image/";

			shoplistData.setOutletImagePath(outletname + res.getString("T10"));
			shoplist.add(shoplistData);
		}

		return shoplist;
	}
	
	

	public ArrayList<ShopListData> getShopListByRegionSyskeyandSearchData(Connection conn, String regionsyskey,String searchdata) throws SQLException {
			

		ArrayList<ShopListData> shoplist = new ArrayList<ShopListData>();

		String query1 = "SELECT  MFIOrganizationSysKey,MFIOrgDealerLinkSysKey,DealerSysKey, OutletSysKey,PK_MFIOrganizationId,PK_DealerId,PK_OutletId,"
				+ " dealerstatus,  orgstatus,   outstaus, orgdealerstatus,"
				+ " T2,T3,T4, T5,T6,T7, T8,T9  , T920, T921, T922,T923,"
				+ " T924, T925, T926,T927,T928,T950, T951, T952,region ,township,T10 "
				+ " from VI_OrgDealerOutlet where orgstatus=? and dealerstatus=? and outstaus=? and orgdealerstatus=? ";
		if(regionsyskey!="" && regionsyskey!="0" && regionsyskey!=null)
		{
			query1 += " and  T920 =" + regionsyskey;
		}
		
		if(searchdata!="" && searchdata!=" " && searchdata!=null)
		{
			query1 += " and (OutletSysKey IN (select OutletSysKey from VI_OutletProductPricePromotion where ProductDescription like '%"+searchdata+"%') OR T9 LIKE '%"+searchdata+"%')";
		}
		
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);

		// stmt2.setString(i++,syskey);

		ResultSet res = stmt2.executeQuery();
		while (res.next()) {
			ShopListData shoplistData = new ShopListData();
			shoplistData.setOrganizationSyskey(res.getLong("MFIOrganizationSysKey"));
			shoplistData.setOrgDearLinkSyskey(res.getLong("MFIOrgDealerLinkSysKey"));
			shoplistData.setDealerSyskey(res.getLong("DealerSysKey"));
			shoplistData.setOutletSyskey(res.getLong("OutletSysKey"));
			shoplistData.setOrganizationId(res.getString("PK_MFIOrganizationId"));
			shoplistData.setDealerid(res.getString("PK_DealerId"));
			shoplistData.setOutletId(res.getString("PK_OutletId"));
			shoplistData.setContactName(res.getString("T2"));
			shoplistData.setContactPhone(res.getString("T3"));
			shoplistData.setContactEmail(res.getString("T4"));
			shoplistData.setContactPosition(res.getString("T5"));
			shoplistData.setContactDept(res.getString("T6"));
			shoplistData.setLongitude(res.getString("T7"));
			shoplistData.setLatitude(res.getString("T8"));
			shoplistData.setOutletName(res.getString("T9"));
			shoplistData.setOutletCurrentAddressRegion(res.getString("region"));
			shoplistData.setOutletCurrentAddressTownship(res.getString("township"));
			shoplistData.setOutletCurrentAddressStreet(res.getString("T922"));
			shoplistData.setOutletCurrentAddressHomeNo(res.getString("T923"));
			shoplistData.setEmail(res.getString("T925"));
			shoplistData.setMobile(res.getString("T926"));
			shoplistData.setLandlinePhone(res.getString("T927"));
			shoplistData.setFax(res.getString("T928"));
			String outletname = "outlet/image/";

			shoplistData.setOutletImagePath(outletname + res.getString("T10"));
			shoplist.add(shoplistData);
		}

		return shoplist;
	}
}
