package com.nirvasoft.mfi.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.nirvasoft.mfi.users.data.ApplicationFormData;
import com.nirvasoft.mfi.users.data.LoanData;
import com.nirvasoft.mfi.users.data.MobileMemberData;
public class ApplicationFormDao {
	public ArrayList<ApplicationFormData> getLoanListByApplicationSyskeyandLoanStatus(Connection conn, String syskey,
			String loanstatus) throws SQLException, ParseException {
		ArrayList<ApplicationFormData> applicationlist = new ArrayList<ApplicationFormData>();

		String query1 = "select FK_MemberSysKey, FK_MemberGuarantorSysKey, PK_LoanId,LoanSysKey,"
				+ "ReferenceMemberId,MemberName, " + "NRC,  NRC2, RegisteredMobileNo,"
				+ "GuaMember_1Name, GuaNRC, RegistrationType,  PaymentTerm, "
				+ "LoanAppliedDate, Loanconfirmdate,ConfirmedBy,"
				+ "loanstatus,MonthlyAmount,drvTotalPrice,drvNetPrice,loanstatus "
				+ " from VI_MemberLoanAppliedProduct  where FK_MemberSysKey=?";

		if (!loanstatus.equals("") && !loanstatus.equals("")) {
			query1 += "  and loanstatus=?";
		}
		query1 += " GROUP BY FK_MemberSysKey, FK_MemberGuarantorSysKey, PK_LoanId,LoanSysKey,"
				+ "ReferenceMemberId,MemberName, " + "NRC,  NRC2, RegisteredMobileNo,"
				+ "GuaMember_1Name, GuaNRC, RegistrationType,  PaymentTerm, "
				+ "LoanAppliedDate, Loanconfirmdate,ConfirmedBy, "
				+ "loanstatus,MonthlyAmount,drvTotalPrice,drvNetPrice,loanstatus ";
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;

		stmt2.setString(i++, syskey);
		if (!loanstatus.equals("") && !loanstatus.equals("")) {
			stmt2.setString(i++, loanstatus);
		}

		ResultSet res = stmt2.executeQuery();
		
		while (res.next()) {
			ApplicationFormData appdata = new ApplicationFormData();
			MobileMemberData mdata = new MobileMemberData();
			mdata.setSyskey(res.getLong("FK_MemberSysKey"));
			mdata.setReference_memberID(res.getString("ReferenceMemberId"));
			mdata.setMember_name(res.getString("MemberName"));
			mdata.setNrc(res.getString("NRC"));
			mdata.setNrc2(res.getString("NRC2"));
			mdata.setRegistered_mobileNo(res.getString("RegisteredMobileNo"));
			appdata.setApplicantdata(mdata);

			MobileMemberData gdata = new MobileMemberData();
			gdata.setSyskey(res.getLong("FK_MemberGuarantorSysKey"));
			gdata.setMember_name(res.getString("GuaMember_1Name"));
			gdata.setNrc(res.getString("GuaNRC"));
			appdata.setGuarantordata(gdata);

			LoanData ldata = new LoanData();
			ldata.setLoanid(res.getString("PK_LoanId"));
			ldata.setLoanSyskey(res.getLong("LoanSysKey"));
			ldata.setPaymentTerm(res.getString("PaymentTerm"));

			String date = res.getString("LoanAppliedDate");// +"-"+res.getString("T11").substring(4,
															// 2);//+"-"+res.getString("T11").substring(6,
															// 2);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
			Date d1 = sdf.parse(date);
			sdf.applyPattern("yyyy-MM-dd HH:mm:ss");
			ldata.setLoanAppliedDate(sdf.format(d1));

			String date1 = res.getString("Loanconfirmdate");// +"-"+res.getString("T11").substring(4,
															// 2);//+"-"+res.getString("T11").substring(6,
															// 2);
			if (!date1.equals("")) {
				SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMddHHmmssSSS");
				Date d11 = sdf1.parse(date1);
				sdf1.applyPattern("yyyy-MM-dd HH:mm:ss");
				ldata.setLoanConfirmDate(sdf.format(d11));

			}
			ldata.setTotalAmount(res.getInt("drvTotalPrice"));
			ldata.setNetAmount(res.getInt("drvNetPrice"));

			ldata.setConfirmedBy(res.getString("ConfirmedBy"));
			ldata.setMonthlyAmount(res.getInt("MonthlyAmount"));
			ldata.setStatus(res.getInt("loanstatus"));
			appdata.setLoandata(ldata);
			applicationlist.add(appdata);
		}
		

		return applicationlist;
	}
	public ArrayList<ApplicationFormData> getLoanListByApplicationSyskeyandRepaymentStatus(Connection conn, String syskey,
			String loanstatus) throws SQLException, ParseException {
		ArrayList<ApplicationFormData> applicationlist = new ArrayList<ApplicationFormData>();

		String query1 = "select FK_MemberSysKey, FK_MemberGuarantorSysKey, PK_LoanId,LoanSysKey,"
				+ "ReferenceMemberId,MemberName, " + "NRC,  NRC2, RegisteredMobileNo,"
				+ "GuaMember_1Name, GuaNRC, RegistrationType,  PaymentTerm, "
				+ "LoanAppliedDate, Loanconfirmdate,ConfirmedBy,"
				+ "loanstatus,MonthlyAmount,drvTotalPrice,drvNetPrice,loanstatus "
				+ " from VI_MemberLoanAppliedProduct  where FK_MemberSysKey=?";

		if (!loanstatus.equals("0") && !loanstatus.equals("") && !loanstatus.equals("")) {
			query1 += "  and loanstatus=?";
		}else
		{
			query1 += "  and loanstatus =7";
		}
		query1 += " GROUP BY FK_MemberSysKey, FK_MemberGuarantorSysKey, PK_LoanId,LoanSysKey,"
				+ "ReferenceMemberId,MemberName, " + "NRC,  NRC2, RegisteredMobileNo,"
				+ "GuaMember_1Name, GuaNRC, RegistrationType,  PaymentTerm, "
				+ "LoanAppliedDate, Loanconfirmdate,ConfirmedBy, "
				+ "loanstatus,MonthlyAmount,drvTotalPrice,drvNetPrice,loanstatus ";
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;

		stmt2.setString(i++, syskey);
		
		if (!loanstatus.equals("0") && !loanstatus.equals("") && !loanstatus.equals("")) {
			stmt2.setString(i++, loanstatus);
		}

		ResultSet res = stmt2.executeQuery();
		
		while (res.next()) {
			ApplicationFormData appdata = new ApplicationFormData();
			MobileMemberData mdata = new MobileMemberData();
			mdata.setSyskey(res.getLong("FK_MemberSysKey"));
			mdata.setReference_memberID(res.getString("ReferenceMemberId"));
			mdata.setMember_name(res.getString("MemberName"));
			mdata.setNrc(res.getString("NRC"));
			mdata.setNrc2(res.getString("NRC2"));
			mdata.setRegistered_mobileNo(res.getString("RegisteredMobileNo"));
			appdata.setApplicantdata(mdata);

			MobileMemberData gdata = new MobileMemberData();
			gdata.setSyskey(res.getLong("FK_MemberGuarantorSysKey"));
			gdata.setMember_name(res.getString("GuaMember_1Name"));
			gdata.setNrc(res.getString("GuaNRC"));
			appdata.setGuarantordata(gdata);

			LoanData ldata = new LoanData();
			ldata.setLoanid(res.getString("PK_LoanId"));
			ldata.setLoanSyskey(res.getLong("LoanSysKey"));
			ldata.setPaymentTerm(res.getString("PaymentTerm"));

			String date = res.getString("LoanAppliedDate");// +"-"+res.getString("T11").substring(4,
															// 2);//+"-"+res.getString("T11").substring(6,
															// 2);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
			Date d1 = sdf.parse(date);
			sdf.applyPattern("yyyy-MM-dd HH:mm:ss");
			ldata.setLoanAppliedDate(sdf.format(d1));

			String date1 = res.getString("Loanconfirmdate");// +"-"+res.getString("T11").substring(4,
															// 2);//+"-"+res.getString("T11").substring(6,
															// 2);
			if (!date1.equals("")) {
				SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMddHHmmssSSS");
				Date d11 = sdf1.parse(date1);
				sdf1.applyPattern("yyyy-MM-dd HH:mm:ss");
				ldata.setLoanConfirmDate(sdf.format(d11));

			}
			ldata.setTotalAmount(res.getInt("drvTotalPrice"));
			ldata.setNetAmount(res.getInt("drvNetPrice"));

			ldata.setConfirmedBy(res.getString("ConfirmedBy"));
			ldata.setMonthlyAmount(res.getInt("MonthlyAmount"));
			ldata.setStatus(res.getInt("loanstatus"));
			appdata.setLoandata(ldata);
			applicationlist.add(appdata);
		}
		

		return applicationlist;
	}
}
