package com.nirvasoft.mfi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;

import com.nirvasoft.mfi.users.data.BankChargesImpl;
import com.nirvasoft.mfi.users.data.BankChargesInterface;
import com.nirvasoft.rp.util.GeneralUtil;

public class BankChargesListDao {

	@Context
	HttpServletRequest request;

	@Context
	private HttpServletResponse response;

	public ArrayList<BankChargesInterface> getBankChargesList(Connection conn,String outletLoanTypeSyskey)
			throws SQLException {
		String[] types=GeneralUtil.readLoanType();
		boolean start=true;
		ArrayList<BankChargesInterface> BankCharges = new ArrayList<BankChargesInterface>();



//		String query1 = "select distinct  bc.FK_ProductSysKey,bc.N1,bc.N2,bc.N3,bc.N4 "
//						+" from mfi_mobile.BankCharges bc "
//						+" inner join mfi_mobile.OutletProductLink opl on bc.FK_ProductSysKey=opl.FK_ProductSysKey "
//						+" where opl.OutletProductLinkSysKey=? and opl.status=? and bc.status=?";
		String query1 = "WITH ViewData AS ( "
						+" SELECT *, RN = ROW_NUMBER() OVER(PARTITION BY N1 ORDER BY N1) "
						+" FROM (  select bc.BankChargeSysKey,bc.PK_BankChargeId,bc.FK_ProductSysKey,bc.N1,bc.N2,bc.N3,bc.N4,bc.N5,bc.N6,bc.N7 "
						+" from BankCharges bc "
						+" inner join OutletProductLink opl on bc.FK_ProductSysKey=opl.FK_ProductSysKey "
						+" where opl.OutletProductLinkSysKey=? and opl.status=? and bc.status=? "
						+" )A "
						+" ) "
						+" SELECT FK_ProductSysKey,N1,N2,N3,N4, "
						+" [Min1] = MAX(CASE WHEN RN = 1 THEN N5 ELSE 0.00 END), "
						+" [Max1] = MAX(CASE WHEN RN = 1 THEN N6 ELSE 0.00 END), "
						+" [Ser1] = MAX(CASE WHEN RN = 1 THEN N7 ELSE 0.00 END), "
						+" [Min2] = MAX(CASE WHEN RN = 2 THEN N5 ELSE 0.00 END), "
						+" [Max2] = MAX(CASE WHEN RN = 2 THEN N6 ELSE 0.00 END), "
						+" [Ser2] = MAX(CASE WHEN RN = 2 THEN N7 ELSE 0.00 END) "
						+" FROM ViewData "
						+" GROUP BY FK_ProductSysKey,N1,N2,N3,N4";
	

		
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setString(i++, outletLoanTypeSyskey);
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		
		ResultSet res = stmt2.executeQuery();
		while (res.next()) {
			BankChargesInterface bcData = new BankChargesImpl();
//			bcData.setBankChargesSyskey(res.getLong("BankChargeSysKey"));
//			bcData.setBankChargesId(res.getString("PK_BankChargeId"));
			bcData.setProductSyskey(res.getLong("FK_ProductSysKey"));			
			bcData.setLoanTermInYear(res.getInt("N1"));
			bcData.setBankInterestRate(res.getDouble("N2"));
			bcData.setServiceChargesRate(res.getDouble("N3"));
			bcData.setChargeType(res.getInt("N4"));
			//bcData.setMinLoanAmount(res.getDouble("N5"));
			//bcData.setMaxLoanAmount(res.getDouble("N6"));
			//bcData.setServiceAmount(res.getDouble("N7"));
			
			bcData.setMinLoanAmount(res.getDouble("Min1"));
			bcData.setMaxLoanAmount(res.getDouble("Max1"));
			bcData.setServiceAmount(res.getDouble("Ser1"));
			
			bcData.setMinLoanAmount1(res.getDouble("Min2"));
			bcData.setMaxLoanAmount1(res.getDouble("Max2"));
			bcData.setServiceAmount1(res.getDouble("Ser2"));
			

			BankCharges.add(bcData);
		}

		return BankCharges;
	}

}
