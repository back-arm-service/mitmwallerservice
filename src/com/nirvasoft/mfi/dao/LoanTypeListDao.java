package com.nirvasoft.mfi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;

import com.nirvasoft.mfi.users.data.LoanTypeImpl;
import com.nirvasoft.mfi.users.data.LoanTypeInterface;
import com.nirvasoft.rp.util.GeneralUtil;

public class LoanTypeListDao {

	@Context
	HttpServletRequest request;

	@Context
	private HttpServletResponse response;

	public ArrayList<LoanTypeInterface> getLoanTypeList(Connection conn,String orgid)
			throws SQLException {
		String[] types=GeneralUtil.readLoanType();
		boolean start=true;
		ArrayList<LoanTypeInterface> loanTypeList = new ArrayList<LoanTypeInterface>();

		String query1 = "select ProductSysKey,ol.OutletProductLinkSysKey,PK_ProductCode,p.T2 as LoanTypeDesc,p.N3 as LoanType,p.N2 as MaxLoanTermInYear"
						+" from Product p"
						+" inner join OutletProductLink ol on p.ProductSysKey=ol.FK_ProductSysKey"
						+" inner join VI_OrgDealerOutlet odo on odo.OutletSysKey=ol.FK_OutletSysKey"
						+" where p.status=? and ol.status=? and odo.PK_MFIOrganizationId=?";
		if(!types.equals(null))
		{
		
		if(types.length>0)
		{
			query1 +=" and ( ";
			
			for(int i=0;i<types.length;i++)
			{
				if(!start)
				{
					query1 +=" or ";
				}
				query1 +=" n3= "+types[i];
				start=false;
			}
			
			query1 +=" ) ";
		}
		}
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setString(i++, orgid);
		
		ResultSet res = stmt2.executeQuery();
		while (res.next()) {
			LoanTypeInterface productlistData = new LoanTypeImpl();
			productlistData.setLoanTypeSyskey(res.getLong("ProductSysKey"));
			productlistData.setOutletLoanTypeLinkSyskey(res.getLong("OutletProductLinkSysKey"));
			productlistData.setLoanTypeCode(res.getString("PK_ProductCode"));			
			productlistData.setLoanTypeDesc(res.getString("LoanTypeDesc"));
			productlistData.setLoanType(res.getInt("LoanType"));			
			productlistData.setMaxLoanTermInYear(res.getInt("MaxLoanTermInYear"));
			productlistData.setSelected(true);
			loanTypeList.add(productlistData);
		}

		return loanTypeList;
	}

}
