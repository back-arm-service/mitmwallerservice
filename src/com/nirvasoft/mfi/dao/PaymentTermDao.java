package com.nirvasoft.mfi.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.mfi.users.data.PaymentTermData;
public class PaymentTermDao {
	public ArrayList<PaymentTermData> getPaymentByOrgSyskey(Connection conn, String orgsyskey) throws SQLException {
		ArrayList<PaymentTermData> paymentTermList = new ArrayList<PaymentTermData>();

		String query1 = "select PaymentTermSysKey,PK_PaymentTermCode,FK_MFIOrganizationSysKey,N1,M1,N2,T950,T951,T952,status"
				+ " from PaymentTerm where status=? and FK_MFIOrganizationSysKey=?";

		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setInt(i++, 1);
		stmt2.setString(i++, orgsyskey);

		ResultSet res = stmt2.executeQuery();
		while (res.next()) {
			PaymentTermData paymentTermData = new PaymentTermData();
			paymentTermData.setPaymentTermSysKey(res.getLong("PaymentTermSysKey"));
			paymentTermData.setPaymentTermCode(res.getString("PK_PaymentTermCode"));
			paymentTermData.setOrganizationSysKey(res.getLong("FK_MFIOrganizationSysKey"));
			paymentTermData.setPeriodMonth(res.getInt("N1"));
			paymentTermData.setAmountLimit(res.getInt("M1"));
			paymentTermData.setItemLimit(res.getInt("N2"));
			paymentTermData.setRegistrationDate(res.getString("T950"));
			paymentTermData.setModifiedDate(res.getString("T951"));
			paymentTermData.setModifiedBy(res.getString("T952"));
			paymentTermData.setStatus(res.getInt("status"));

			paymentTermList.add(paymentTermData);
		}

		return paymentTermList;
	}

	public static int getPaymentTermByPaymentTermSyskey(Connection conn, String paymenttermsyskey) throws SQLException {

		String query1 = "select N1" + " from PaymentTerm where PaymentTermSysKey=?";

		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setString(i++, paymenttermsyskey);
		int paymentterm = 0;

		ResultSet res = stmt2.executeQuery();
		if (res.next()) {
			paymentterm = res.getInt("N1");

		}

		return paymentterm;
	}
	public PaymentTermData getPaymentByPaymentTermSyskey(Connection conn, String paymenttermsyskey)
			throws SQLException {
		PaymentTermData paymentTermData = new PaymentTermData();

		String query1 = "select PaymentTermSysKey,PK_PaymentTermCode,FK_MFIOrganizationSysKey,N1,M1,N2,T950,T951,T952,status"
				+ " from PaymentTerm where status=? and PaymentTermSysKey=?";

		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setInt(i++, 1);
		stmt2.setString(i++, paymenttermsyskey);

		ResultSet res = stmt2.executeQuery();
		if (res.next()) {

			paymentTermData.setPaymentTermSysKey(res.getLong("PaymentTermSysKey"));
			paymentTermData.setPaymentTermCode(res.getString("PK_PaymentTermCode"));
			paymentTermData.setOrganizationSysKey(res.getLong("FK_MFIOrganizationSysKey"));
			paymentTermData.setPeriodMonth(res.getInt("N1"));
			paymentTermData.setAmountLimit(res.getInt("M1"));
			paymentTermData.setItemLimit(res.getInt("N2"));
			paymentTermData.setRegistrationDate(res.getString("T950"));
			paymentTermData.setModifiedDate(res.getString("T951"));
			paymentTermData.setModifiedBy(res.getString("T952"));
			paymentTermData.setStatus(res.getInt("status"));

		}

		return paymentTermData;
	}

}
