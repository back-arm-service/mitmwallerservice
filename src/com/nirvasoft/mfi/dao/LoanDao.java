package com.nirvasoft.mfi.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nirvasoft.mfi.mgr.KeyGenarateMgr;
import com.nirvasoft.mfi.users.data.LoanData;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.util.ServerGlobal;
public class LoanDao {
	public static LoanData getLoanDataByLoansyskey(Connection conn, String loansyskey)
			throws SQLException, ParseException {
		LoanData data = new LoanData();
		String query1 = "select [LoanSysKey],[PK_LoanId],[FK_MemberSysKey],[FK_MemberGuarantorSysKey],FK_PaymentTearmSysKey,"
				+ "[N1],[T9],[T1],[T2],[T3],[T4],[T5],[T6],[T7],[M1],[T950],[T951],[T952],[status],[drvTotalPrice],[drvNetPrice]"
				+ " from Loan where LoanSysKey=?";
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setString(i++, loansyskey);
		ResultSet res = stmt2.executeQuery();
		if (res.next()) {

			data.setLoanSyskey(res.getLong("LoanSysKey"));
			data.setLoanid(res.getString("PK_LoanId"));
			data.setMemberSyskey(res.getLong("FK_MemberSysKey"));
			data.setGuarantorSyskey(res.getLong("FK_MemberGuarantorSysKey"));
			data.setPaymentTermSyskey(res.getLong("FK_PaymentTearmSysKey"));
			data.setRegistrationType(res.getInt("N1"));
			data.setPaymentTerm(res.getString("T9"));

			String date = res.getString("T1");
			if (!date.equals("")) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
				Date d1 = sdf.parse(date);
				sdf.applyPattern("yyyy-MM-dd HH:mm:ss");
				data.setLoanAppliedDate(sdf.format(d1));
			}
			String date1 = res.getString("T2");
			if (!date1.equals("")) {
				SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMddHHmmssSSS");
				Date d11 = sdf1.parse(date1);
				sdf1.applyPattern("yyyy-MM-dd HH:mm:ss");
				data.setLoanConfirmDate(sdf1.format(d11));
			}

			data.setConfirmedBy(res.getString("T3"));
			data.setHouseholdRegistraionFileLocation(res.getString("T4"));

			data.setHouseholdRegistrationImageFile(res.getString("T5"));
			data.setPoliceCertificateFIleLocation(res.getString("T6"));
			data.setEmploymentLetterFileLocation(res.getString("T7"));
			data.setMonthlyAmount(res.getInt("M1"));
			data.setTotalAmount(res.getInt("drvTotalPrice"));
			data.setNetAmount(res.getInt("drvNetPrice"));
			data.setStatus(res.getInt("status"));
			ServerGlobal.setLoantotalbitsstatus(res.getInt("status"));
		}

		return data;
	}
	public static Result insertLone(LoanData obj, Connection conn) throws SQLException {
		Result res = new Result();

		try {

			int pretotal = 0;

			pretotal = OnBitLoanTotalNewValue(ServerGlobal.getLoanActivePos(), pretotal);

			pretotal = OnBitLoanTotalNewValue(ServerGlobal.getLoanInprocessPos(), pretotal);

			obj.setLoanAppliedDate(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
			obj.setRegiatrationDate(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
			obj.setModifiedDate(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
			obj.setStatus(pretotal);
			obj.setLoanid(getLoanID(conn));
			obj.setPaymentTerm(String.valueOf(PaymentTermDao.getPaymentTermByPaymentTermSyskey(conn,
					String.valueOf(obj.getPaymentTermSyskey()))));

			String sql = "insert into  [loan]([LoanSysKey],[PK_LoanId],[FK_MemberSysKey],[FK_MemberGuarantorSysKey],FK_PaymentTearmSysKey,"
					+ "[N1],[T9],[T1],[T2],[T3],[T4],[T5],[T6],[T7],[M1],[T950],[T951],[T952],[status],[drvTotalPrice],[drvNetPrice])"
					+ "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement stmt = conn.prepareStatement(sql);

			int i = 1;
			stmt.setLong(i++, obj.getLoanSyskey());
			stmt.setString(i++, obj.getLoanid());
			stmt.setLong(i++, obj.getMemberSyskey());
			stmt.setLong(i++, obj.getGuarantorSyskey());
			stmt.setLong(i++, obj.getPaymentTermSyskey());
			stmt.setInt(i++, obj.getRegistrationType());
			stmt.setString(i++, obj.getPaymentTerm());
			stmt.setString(i++, obj.getLoanAppliedDate());
			stmt.setString(i++, obj.getLoanConfirmDate());
			stmt.setString(i++, obj.getConfirmedBy());
			stmt.setString(i++, obj.getHouseholdRegistraionFileLocation());
			stmt.setString(i++, obj.getHouseholdRegistrationImageFile());
			stmt.setString(i++, obj.getPoliceCertificateFIleLocation());
			stmt.setString(i++, obj.getEmploymentLetterFileLocation());
			stmt.setDouble(i++, obj.getMonthlyAmount());
			stmt.setString(i++, obj.getRegiatrationDate());
			stmt.setString(i++, obj.getModifiedDate());
			stmt.setString(i++, obj.getModifiedBy());
			stmt.setInt(i++, obj.getStatus());
			stmt.setDouble(i++, obj.getTotalAmount());
			stmt.setDouble(i++, obj.getNetAmount());

			int rs = stmt.executeUpdate();

			if (rs > 0) {
				res.setState(true);
				res.setMsgDesc("Saved Successfully");
				res.setMsgCode("0000");

			} else {
				res.setState(false);
				res.setMsgDesc("Cannot Save");
				res.setMsgCode("0014");
			}

		}

		catch (Exception e) {
			res.setMsgDesc("Cannot Save");
			res.setMsgCode("0014");
			e.printStackTrace();
		}

		return res;
	}
	public static String getLoanID(Connection aConn) {
		String ret = new String();

		Result res = new Result();
		res = KeyGenarateMgr.getKeyDataMgr("Loan", 1);
		ret = res.getKeyString();
		return ret;
	}
	public static int OnBitLoanTotalNewValue(int bitpos, int previousTotalBitVal) {
		int retval = 0;
		retval = previousTotalBitVal + (int) (Math.pow(2, bitpos));
		return retval;
	}

	public int OffBitLoanTotalNewValue(int bitpos, int previousTotalBitVal) {
		int retval = 0;
		retval = previousTotalBitVal - (int) (Math.pow(2, bitpos));
		return retval;
	}
	public static boolean isbitOn(int bitposition, int totalval) {
		boolean ret = false;
		if ((bitposition & totalval) == bitposition)
			return true;
		return ret;

	}
}
