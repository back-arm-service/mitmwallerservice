package com.nirvasoft.mfi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nirvasoft.mfi.users.data.OrgMemberLinkData;
import com.nirvasoft.rp.framework.Result;

public class OrganizationDao {

	public static long getOrganizationID(Connection conn) throws SQLException {

		long orgsyskey = 0;
		String query1 = "Select MFIOrganizationSysKey from [MFIOrganization] where status=?";
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		stmt2.setString(1, "1");

		ResultSet resph = stmt2.executeQuery();
		if (resph.next()) {
			orgsyskey = resph.getLong("MFIOrganizationSysKey");
		}

		return orgsyskey;
	}
	public static Result insert(OrgMemberLinkData obj, Connection conn) throws SQLException {
		Result res = new Result();

		try {
			obj.setRegistration_date(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
			obj.setModified_date(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));

			String sql = "insert into [OrgMemberLink] (syskey,FKMember,FKOrg,t950,t951,t952,status) "
					+ "values(?,?,?,?,?,?,?)";
			PreparedStatement stmt = conn.prepareStatement(sql);

			int i = 1;
			stmt.setLong(i++, obj.getSyskey());
			stmt.setLong(i++, obj.getFk_memberid());
			stmt.setLong(i++, obj.getFk_orgid());
			stmt.setString(i++, obj.getRegistration_date());
			stmt.setString(i++, obj.getModified_date());
			stmt.setString(i++, obj.getModifiedby());
			stmt.setInt(i++, obj.getStatus());

			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				res.setMsgDesc("Saved Successfully");

			} else {
				res.setState(false);
				res.setMsgDesc("Cannot Save");
			}

		}

		catch (Exception e) {
			res.setMsgDesc("Cannot Save");
			e.printStackTrace();
		}

		return res;
	}

}