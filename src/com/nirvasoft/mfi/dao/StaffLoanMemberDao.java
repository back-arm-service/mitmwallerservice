package com.nirvasoft.mfi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.nirvasoft.mfi.mgr.KeyGenarateMgr;
import com.nirvasoft.mfi.users.data.StaffLoanApplicantImpl;
import com.nirvasoft.mfi.users.data.StaffLoanApplicantInterface;
import com.nirvasoft.mfi.users.data.StaffLoanGuarantorImpl;
import com.nirvasoft.mfi.users.data.StaffLoanGuarantorInterface;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ServerGlobal;

public class StaffLoanMemberDao {

	public static Result updateApplicant(StaffLoanApplicantInterface obj, Connection conn,int status) {
		Result res = new Result();

		try {
			int pretotal = 0;
			if (!MobileMemberDao.isLoanmemberOn(status)) {
				pretotal = MobileMemberDao.OnBitTotalNewValue(ServerGlobal.getLoanmemberPos(), status);
			} else {
				pretotal = status;
			}

			obj.setModifiedDate(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
			obj.setStatus(pretotal);

			String sql = "UPDATE [member] SET t3=?,t4=?,t9=?,t10=?,T23=?,T24=?,t14=?,t15=?"
						 +",n2=?,t920=?,t921=?,t922=?,t923=?,t924=?,t926=?,t951=?,t952=?,status=? where syskey=? ";

			PreparedStatement stmt = conn.prepareStatement(sql);

			int i = 1;	
			stmt.setString(i++, obj.getStaffName());
			stmt.setString(i++, obj.getStaffNrc());
			stmt.setString(i++, obj.getFrontNRCImagePath());
			stmt.setString(i++, obj.getBackNRCImagePath());
			stmt.setString(i++, obj.getPhotoPath());
			stmt.setString(i++, obj.getPhotoPath1());
			stmt.setString(i++, obj.getDepartment());
			stmt.setString(i++, obj.getCurrentPosition());
			stmt.setDouble(i++, obj.getMonthlyBasicIncome());		
		
			stmt.setString(i++, obj.getCurrentAddressRegion());
			stmt.setString(i++, obj.getCurrentAddressTownship());
			stmt.setString(i++, obj.getCurrentAddressStreet());
			stmt.setString(i++, obj.getCurrentAddressHouseNo());
			stmt.setString(i++, obj.getCurrentAddressPostalCode());			
			stmt.setString(i++, obj.getMobile());
			

			stmt.setString(i++, obj.getModifiedDate());
			stmt.setString(i++, obj.getModifiedBy());
			stmt.setInt(i++, obj.getStatus());
			stmt.setLong(i++, obj.getStaffSyskey());

			int rs = stmt.executeUpdate();

			if (rs > 0) {				
				res.setState(true);
				res.setMsgDesc("Saved Successfully");
				res.setMsgCode("0000");

			} else {
				res.setState(false);
				res.setMsgDesc("Cannot Save");
				res.setMsgCode("0014");
			}

		}catch (SQLException e) {
			res.setMsgDesc(e.toString());
			res.setMsgCode("0014");
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("updateApplicant Function in StaffLoanMemberDao.... : " +obj.getMobile());
				l_err.add("updateApplicant Function in StaffLoanMemberDao.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}

		catch (Exception e) {
			res.setMsgDesc(e.toString());
			res.setMsgCode("0014");
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("updateApplicant Function in StaffLoanMemberDao.... : " +obj.getMobile());
				l_err.add("updateApplicant Function in StaffLoanMemberDao.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}

		return res;
	}

	public static Result updateGrarantor(StaffLoanGuarantorInterface obj, Connection conn) {
		Result res = new Result();

		try {			

			obj.setModifiedDate(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
			

			String sql = "UPDATE [member] SET t3=?,t4=?,t9=?,t10=?,t14=?,t15=?"
					+ ",t920=?,t921=?,t922=?,t923=?,t924=?,t926=?,"
					+ "t951=?,t952=?,status=? where syskey=? ";

			PreparedStatement stmt = conn.prepareStatement(sql);

			int i = 1;
			stmt.setString(i++, obj.getGuarantorName());
			stmt.setString(i++, obj.getGuarantorNrc());
			stmt.setString(i++, obj.getFrontNRCImagePath());
			stmt.setString(i++, obj.getBackNRCImagePath());
			stmt.setString(i++, obj.getDepartment());
			stmt.setString(i++, obj.getCurrentPosition());
			
			stmt.setString(i++, obj.getCurrentAddressRegion());
			stmt.setString(i++, obj.getCurrentAddressTownship());
			stmt.setString(i++, obj.getCurrentAddressStreet());
			stmt.setString(i++, obj.getCurrentAddressHouseNo());
			stmt.setString(i++, obj.getCurrentAddressPostalCode());
			stmt.setString(i++, obj.getMobile());
			
			stmt.setString(i++, obj.getModifiedDate());
			stmt.setString(i++, obj.getModifiedBy());
			stmt.setInt(i++, obj.getStatus());
			stmt.setLong(i++, obj.getGuarantorSyskey());
			int rs = stmt.executeUpdate();

			if (rs > 0) {
				obj.setApplicantSyskey(obj.getGuarantorSyskey());
				res=insertGrarantorLoanList(obj,conn);
				if(res.isState())
				{
				res.setState(true);
				res.setMsgDesc("Saved Successfully");
				res.setMsgCode("0000");
				}else
				{
					res.setState(false);
					res.setMsgDesc("Cannot Save");
					res.setMsgCode("0014");
				}

			} else {
				res.setState(false);
				res.setMsgDesc("Cannot Save");
				res.setMsgCode("0014");
			}

		}
		catch (SQLException e) {
			res.setMsgDesc(e.toString());
			res.setMsgCode("0014");
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Update Guarantor Function in StaffLoanMemberDao.... : " +obj.getGuarantorName());
				l_err.add("Update Guarantor Function in StaffLoanMemberDao.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}
		catch (Exception e) {
			res.setMsgDesc(e.toString());
			res.setMsgCode("0014");
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Update Guarantor Function in StaffLoanMemberDao.... : " +obj.getGuarantorName());
				l_err.add("Update Guarantor Function in StaffLoanMemberDao.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}

		return res;
	}
	
	public static Result insertGrarantor(StaffLoanGuarantorInterface obj, Connection conn){
		Result res = new Result();

		try {
			int pretotal = 0;

			pretotal = MobileMemberDao.OnBitTotalNewValue(ServerGlobal.getAnonymousPos(), pretotal);
			pretotal = MobileMemberDao.OnBitTotalNewValue(ServerGlobal.getGuarantorPos(), pretotal);
			pretotal = MobileMemberDao.OnBitTotalNewValue(ServerGlobal.getActivePos(), pretotal);
			String nowdate=new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());			
			obj.setRegistrationDate(nowdate);
			obj.setModifiedDate(nowdate);
			obj.setStatus(pretotal);
			obj.setGuarantorID(MobileMemberDao.getNewMemberID(conn));
			
			
			String sql = "insert into [member] (syskey,PK_MemberId, T3,T4,T14,T15,T920,T921,T922,T923,T924,T926,T9,T10,T950,T951,T952,status)" 
							+" values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement stmt = conn.prepareStatement(sql);

			int i = 1;
			
			stmt.setLong(i++, obj.getGuarantorSyskey());
			stmt.setString(i++, obj.getGuarantorID());
			stmt.setString(i++, obj.getGuarantorName());
			stmt.setString(i++, obj.getGuarantorNrc());
					
			stmt.setString(i++, obj.getDepartment());
			stmt.setString(i++, obj.getCurrentPosition());
			
			stmt.setString(i++, obj.getCurrentAddressRegion());
			stmt.setString(i++, obj.getCurrentAddressTownship());
			stmt.setString(i++, obj.getCurrentAddressStreet());
			stmt.setString(i++, obj.getCurrentAddressHouseNo());
			stmt.setString(i++, obj.getCurrentAddressPostalCode());
			stmt.setString(i++, obj.getMobile());
			stmt.setString(i++, obj.getFrontNRCImagePath());
			stmt.setString(i++, obj.getBackNRCImagePath());
			
			stmt.setString(i++, obj.getRegistrationDate());
			stmt.setString(i++, obj.getModifiedDate());
			stmt.setString(i++, obj.getModifiedBy());
			stmt.setInt(i++, obj.getStatus());

			int rs = stmt.executeUpdate();

			if (rs > 0) {	
				obj.setApplicantSyskey(obj.getGuarantorSyskey());
				res=insertGrarantorLoanList(obj,conn);
				if(res.isState())
				{
				res.setState(true);
				res.setMsgDesc("Saved Successfully");
				res.setMsgCode("0000");
				}else
				{
					res.setState(false);
					res.setMsgDesc("Cannot Save");
					res.setMsgCode("0014");
				}

			} else {
				res.setState(false);
				res.setMsgDesc("Cannot Save");
				res.setMsgCode("0014");
			}

		}catch (SQLException e) {
			res.setMsgDesc(e.toString());
			res.setMsgCode("0014");
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Insert Guarantor Function in StaffLoanMemberDao.... : " +obj.getGuarantorName());
				l_err.add("Insert Guarantor Function in StaffLoanMemberDao.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}
		catch (Exception e) {
			res.setMsgDesc(e.toString());
			res.setMsgCode("0014");
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Insert Guarantor Function in StaffLoanMemberDao.... : " +obj.getGuarantorName());
				l_err.add("Insert Guarantor Function in StaffLoanMemberDao.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}

		return res;
	}
	

	public static Result insertGrarantorLoanList(StaffLoanGuarantorInterface obj, Connection conn){
		Result res = new Result();

		try {
			
			String nowdate=new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());			
			obj.setRegistrationDate(nowdate);
			obj.setModifiedDate(nowdate);
			obj.setStatus(1);
			obj.setGuarantorID(getNewGuarantorListID(conn));
			Result keyres = new Result();
			keyres = KeyGenarateMgr.getKeyDataMgr("MFIMobile", 2);
			obj.setGuarantorSyskey(Long.parseLong(keyres.getKeyString()));

			String sql = "insert into [LoanGuarantorList](LoanGuarantorListSysKey,PK_LoanGuarantorListId, FK_MemberSysKey,FK_AppliedLoanListSysKey,"
						+" FK_LoanSysKey,T1,T2,T4,T920,T921,T922,T923,T924,T926,T950,T951,T952,status) "
						+" values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement stmt = conn.prepareStatement(sql);

			int i = 1;
			stmt.setLong(i++, obj.getGuarantorSyskey());
			stmt.setString(i++, obj.getGuarantorID());
			stmt.setLong(i++, obj.getApplicantSyskey());
			stmt.setLong(i++, obj.getAppliedLoanListSyskey());
			stmt.setLong(i++, obj.getLoanSyskey());
			stmt.setString(i++, obj.getCurrentPosition());
			stmt.setString(i++, obj.getDepartment());
			stmt.setString(i++, obj.getGuarantorPDFImagePath());

			stmt.setString(i++, obj.getCurrentAddressRegion());
			stmt.setString(i++, obj.getCurrentAddressTownship());
			stmt.setString(i++, obj.getCurrentAddressStreet());
			stmt.setString(i++, obj.getCurrentAddressHouseNo());
			stmt.setString(i++, obj.getCurrentAddressPostalCode());	
			stmt.setString(i++, obj.getMobile());	
			
			stmt.setString(i++, obj.getRegistrationDate());
			stmt.setString(i++, obj.getModifiedDate());
			stmt.setString(i++, obj.getModifiedBy());
			stmt.setInt(i++, obj.getStatus());

			int rs = stmt.executeUpdate();

			if (rs > 0) {
				// ServerGlobal.setTotalbitstatus(pretotal);
				res.setState(true);
				res.setMsgDesc("Saved Successfully");
				res.setMsgCode("0000");

			} else {
				res.setState(false);
				res.setMsgDesc("Cannot Save");
				res.setMsgCode("0014");
			}

		}catch (SQLException e) {
			res.setMsgDesc(e.toString());
			res.setMsgCode("0014");
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("insertGrarantorLoanList Function in StaffLoanMemberDao.... : " +obj.getGuarantorName());
				l_err.add("insertGrarantorLoanList Function in StaffLoanMemberDao.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}
		catch (Exception e) {
			res.setMsgDesc(e.toString());
			res.setMsgCode("0014");
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("insertGrarantorLoanList Function in StaffLoanMemberDao.... : " +obj.getGuarantorName());
				l_err.add("insertGrarantorLoanList Function in StaffLoanMemberDao.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}

		return res;
	}
	
	public static Result updateGrarantorLoanList(StaffLoanGuarantorInterface obj, Connection conn) {
		Result res = new Result();

		try {			

			obj.setModifiedDate(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
			

			String sql = "UPDATE [LoanGuarantorList] SET t3=?,T951=? where FK_LoanSysKey=? ";

			PreparedStatement stmt = conn.prepareStatement(sql);

			int i = 1;
			
			stmt.setString(i++, obj.getGuarantorSignImagePath());
			stmt.setString(i++, obj.getModifiedDate());
			stmt.setLong(i++, obj.getLoanSyskey());
			int rs = stmt.executeUpdate();

			if (rs > 0) {
				res.setMsgDesc("Saved Successfully");
				res.setMsgCode("0000");

			} else {
				res.setState(false);
				res.setMsgDesc("Cannot Save");
				res.setMsgCode("0014");
			}

		}
		catch (SQLException e) {
			res.setMsgDesc(e.toString());
			res.setMsgCode("0014");
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("updateGrarantorLoanList Function in StaffLoanMemberDao.... : " +obj.getGuarantorName());
				l_err.add("updateGrarantorLoanList Function in StaffLoanMemberDao.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}
		catch (Exception e) {
			res.setMsgDesc(e.toString());
			res.setMsgCode("0014");
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("updateGrarantorLoanList Function in StaffLoanMemberDao.... : " +obj.getGuarantorName());
				l_err.add("updateGrarantorLoanList Function in StaffLoanMemberDao.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}

		return res;
	}
	
	public static String getNewGuarantorListID(Connection aConn) {
		String ret = new String();

		Result res = new Result();
		res = KeyGenarateMgr.getKeyDataMgr("GuarantorList", 1);
		ret = res.getKeyString();
		return ret;
	}
	

	public static StaffLoanApplicantInterface getStaffLoanApplicantBysyskey(Connection conn, String syskey)
			throws SQLException, ParseException {
		StaffLoanApplicantInterface data = new StaffLoanApplicantImpl();

		String query1 = "select syskey,PK_MemberId,t3,t4,t9,t10,T23,T24,t14,t15,n2,t920,t921,"
						+ " t922,t923,t924,t926,t951,t952,status from Member  "
						+ " where syskey=?";
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setString(i++, syskey);

		ResultSet res = stmt2.executeQuery();
		if (res.next()) {

			data.setStaffSyskey(res.getLong("syskey"));
			data.setStaffID(res.getString("PK_MemberId"));
			
			data.setStaffName(res.getString("T3"));
			data.setStaffNrc(res.getString("T4"));
			
			String imagepath = ServerGlobal.getStaffLoanImagePath().trim();

			data.setFrontNRCImagePath(imagepath + res.getString("T9"));
			data.setBackNRCImagePath(imagepath + res.getString("T10"));
			
			data.setPhotoPath(imagepath + res.getString("T23"));
			
			if(res.getString("T24").indexOf(".")>0){			
			data.setPhotoPath1(imagepath + res.getString("T24"));
			}
			
			data.setDepartment(res.getString("T14"));
			data.setCurrentPosition(res.getString("T15"));

			data.setMonthlyBasicIncome(res.getInt("N2"));
			
			data.setCurrentAddressRegion(res.getString("T920"));
			data.setCurrentAddressTownship(res.getString("T921"));
			data.setCurrentAddressStreet(res.getString("T922"));
			data.setCurrentAddressHouseNo(res.getString("T923"));
			data.setCurrentAddressPostalCode(res.getString("T924"));
			
			data.setMobile(res.getString("T926"));
			
			data.setStatus(res.getInt("status"));

		}

		return data;
	}
	

	public static ArrayList<StaffLoanGuarantorInterface> getGuarantorDataByApplicantsyskey(Connection conn, String appsyskey)
			throws SQLException, ParseException {

		ArrayList<StaffLoanGuarantorInterface> datalist = new ArrayList<StaffLoanGuarantorInterface>();

//		String query1 = "select m.syskey,m.PK_MemberId, m.T3,m.T4,m.T14,m.T15,m.T920,m.T921,m.T922,m.T923,m.T924,m.T926,m.T9,m.T10,m.T950,m.T951,m.T952,m.status "
//						+" from mfi_mobile.Member "
//						+" where syskey in ( "
//						+" select FK_MemberSysKey from mfi_mobile.LoanGuarantorList where status=? and  FK_LoanSysKey in ( "
//						+" select LoanSysKey from mfi_mobile.Loan where FK_MemberSysKey=?)) ";
		
		String query1 = "select distinct m.syskey,m.PK_MemberId, m.T3,m.T4,m.T14,m.T15,m.T920,m.T921,m.T922,m.T923,m.T924,m.T926,m.T9,m.T10,m.T950,m.T951,m.T952,m.status " 
						+" from Member m "
						 +" inner join LoanGuarantorList lg on m.syskey=lg.FK_MemberSysKey "
						 +" inner join Loan l on lg.FK_LoanSysKey=l.LoanSysKey "
						 +" where   lg.status=? and l.FK_MemberSysKey=?";
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setInt(i++, 1);
		stmt2.setString(i++, appsyskey);

		ResultSet res = stmt2.executeQuery();
		while (res.next()) {
			StaffLoanGuarantorInterface data = new StaffLoanGuarantorImpl();
			data.setGuarantorSyskey(res.getLong("syskey"));
			data.setGuarantorID(res.getString("PK_MemberId"));
			
			data.setGuarantorName(res.getString("T3"));
			data.setGuarantorNrc(res.getString("T4"));			
			
			String imagepath = ServerGlobal.getStaffLoanImagePath().trim();
			
			data.setFrontNRCImagePath(imagepath + res.getString("T9"));
			data.setBackNRCImagePath(imagepath + res.getString("T10"));
			
			data.setDepartment(res.getString("T14"));
			data.setCurrentPosition(res.getString("T15"));

			
			data.setCurrentAddressRegion(res.getString("T920"));
			data.setCurrentAddressTownship(res.getString("T921"));
			data.setCurrentAddressStreet(res.getString("T922"));
			data.setCurrentAddressHouseNo(res.getString("T923"));
			data.setCurrentAddressPostalCode(res.getString("T924"));
			
			data.setMobile(res.getString("T926"));
			
			data.setStatus(res.getInt("status"));

			datalist.add(data);
		}

		return datalist;
	}

	public static StaffLoanGuarantorInterface getGuarantorDataBysyskey(Connection conn, String syskey)
			throws SQLException, ParseException {
		StaffLoanGuarantorInterface data = new StaffLoanGuarantorImpl();

		String query1 =  "select m.syskey,m.PK_MemberId, m.T3,m.T4,m.T14,m.T15,m.T920,m.T921,m.T922,m.T923,m.T924,m.T926,T9,T10,m.status" 
						+" from Member m "
						+" inner join LoanGuarantorList GL on m.syskey=gl.FK_MemberSysKey "
						+" where syskey=?";
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setString(i++, syskey);

		ResultSet res = stmt2.executeQuery();
		if (res.next()) {

			data.setGuarantorSyskey(res.getLong("syskey"));
			data.setGuarantorID(res.getString("PK_MemberId"));
			
			data.setGuarantorName(res.getString("T3"));
			data.setGuarantorNrc(res.getString("T4"));			
			
			String imagepath = ServerGlobal.getStaffLoanImagePath().trim();
			
			data.setFrontNRCImagePath(imagepath + res.getString("T9"));
			data.setBackNRCImagePath(imagepath + res.getString("T10"));
			
			data.setDepartment(res.getString("T14"));
			data.setCurrentPosition(res.getString("T15"));

			
			data.setCurrentAddressRegion(res.getString("T920"));
			data.setCurrentAddressTownship(res.getString("T921"));
			data.setCurrentAddressStreet(res.getString("T922"));
			data.setCurrentAddressHouseNo(res.getString("T923"));
			data.setCurrentAddressPostalCode(res.getString("T924"));
			
			data.setMobile(res.getString("T926"));
			
			data.setStatus(res.getInt("status"));
			//ServerGlobal.setTotalbitstatus(res.getInt("status"));
		}

		return data;
	}
	


	public static ArrayList<StaffLoanGuarantorImpl> getGuarantorDataByLoanSyskey(Connection conn, String loansyskey)
			throws SQLException, ParseException {

		ArrayList<StaffLoanGuarantorImpl> datalist = new ArrayList<StaffLoanGuarantorImpl>();

//		String query1 = "select m.syskey,m.PK_MemberId, m.T3,m.T4,m.T14,m.T15,m.T920,m.T921,m.T922,m.T923,m.T924,m.T926,m.T9,m.T10,m.T950,m.T951,m.T952,m.status "
//						+" from mfi_mobile.Member "
//						+" where syskey in ( "
//						+" select FK_MemberSysKey from mfi_mobile.LoanGuarantorList where status=? and  FK_LoanSysKey =?) ";
		String query1 = "select m.syskey,m.PK_MemberId, m.T3,m.T4,lg.T1 as position,lg.T2 as dept,lg.T920,lg.T921,lg.T922,lg.T923,lg.T924,lg.T926,m.T9,m.T10,m.T950,m.T951,m.T952,m.status ,r.T1 as regval,t.T1 as townval"
						+" from Member m "
						+" inner join LoanGuarantorList lg on m.syskey=lg.FK_MemberSysKey "
						+" inner join RegionReference r on r.RegionReferenceSysKey=m.t920"
						+" inner join TownshipReference t on t.TownshipReferenceSysKey=m.t921"
						+"  where lg.status=? and  lg.FK_LoanSysKey =?";
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setInt(i++, 1);
		stmt2.setString(i++, loansyskey);

		ResultSet res = stmt2.executeQuery();
		while (res.next()) {
			StaffLoanGuarantorImpl data = new StaffLoanGuarantorImpl();
			data.setGuarantorSyskey(res.getLong("syskey"));
			data.setGuarantorID(res.getString("PK_MemberId"));
			
			data.setGuarantorName(res.getString("T3"));
			data.setGuarantorNrc(res.getString("T4"));			
			
			String imagepath = ServerGlobal.getStaffLoanImagePath().trim();
			
			data.setFrontNRCImagePath(imagepath + res.getString("T9"));
			data.setBackNRCImagePath(imagepath + res.getString("T10"));
			
			data.setDepartment(res.getString("dept"));
			data.setCurrentPosition(res.getString("position"));

			
			data.setCurrentAddressRegion(res.getString("T920"));
			data.setCurrentAddressTownship(res.getString("T921"));
			data.setCurrentAddressStreet(res.getString("T922"));
			data.setCurrentAddressHouseNo(res.getString("T923"));
			data.setCurrentAddressPostalCode(res.getString("T924"));
			data.setRegionValue(res.getString("regval"));
			data.setTownshipValue(res.getString("townval"));
			
			data.setMobile(res.getString("T926"));
			
			data.setStatus(res.getInt("status"));

			datalist.add(data);
		}

		return datalist;
	}
}