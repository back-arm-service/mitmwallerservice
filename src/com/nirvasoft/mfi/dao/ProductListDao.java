package com.nirvasoft.mfi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;

import com.nirvasoft.mfi.mgr.KeyGenarateMgr;
import com.nirvasoft.mfi.users.data.ProductData;
import com.nirvasoft.mfi.users.data.ProductListData;
import com.nirvasoft.rp.framework.Result;

public class ProductListDao {

	@Context
	HttpServletRequest request;

	@Context
	private HttpServletResponse response;

	public ArrayList<ProductListData> getProductListByOutletSyskey(Connection conn, String outletsyskey)
			throws SQLException {
		ArrayList<ProductListData> productlist = new ArrayList<ProductListData>();

		String query1 = "select   OutletSysKey,OutletProductLinkSysKey, ProductSysKey, ProductPriceSysKey, PromotionSysKey, "
				+ "PK_OutletId, PK_ProductCode,ProductDescription, Category, ProductUnit, Manufacturer, "
				+ "Supplier, Model, SerialNo,ProductImagePath,ProductInfoFile, "
				+ "PK_PriceCode,  PriceUnit, price, PK_PromotionCode, Promotiondescription,"
				+ "startdate,enddate, promotiontype,  PromotionAmount, InterestRate, "
				+ "outletstatus,  productstatus, pricestatus, promotionstatus"
				+ " from VI_OutletProductPricePromotion where outletstatus=? and productstatus=? and pricestatus=? and (promotionstatus=? or promotionstatus IS NULL)  and  OutletSysKey=?";

		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setString(i++, outletsyskey);

		ResultSet res = stmt2.executeQuery();
		while (res.next()) {
			ProductListData productlistData = new ProductListData();
			productlistData.setOutletsyskey(res.getLong("OutletSysKey"));
			productlistData.setOutletProductLinksyskey(res.getLong("OutletProductLinkSysKey"));
			productlistData.setProductSyskey(res.getLong("ProductSysKey"));
			productlistData.setProductPricesyskey(res.getLong("ProductPriceSysKey"));
			productlistData.setPromotionsyskey(res.getLong("PromotionSysKey"));
			productlistData.setOutletid(res.getString("PK_OutletId"));
			productlistData.setProductCode(res.getString("PK_ProductCode"));
			productlistData.setProductdescription(res.getString("ProductDescription"));
			productlistData.setCategory(res.getString("Category"));
			productlistData.setProductunit(res.getString("ProductUnit"));
			productlistData.setManufacture(res.getString("Manufacturer"));
			productlistData.setSupplier(res.getString("Supplier"));
			productlistData.setModel(res.getString("Model"));
			productlistData.setSerialno(res.getString("SerialNo"));
			String orgname = "product/image/";

			productlistData.setProductImagePath(orgname + res.getString("ProductImagePath"));
			productlistData.setProductInfoFile(res.getString("ProductInfoFile"));
			productlistData.setPricecode(res.getString("PK_PriceCode"));
			productlistData.setPriceunit(res.getString("PriceUnit"));
			productlistData.setProductprice(res.getInt("price"));
			productlistData.setPromotioncode(res.getString("PK_PromotionCode"));
			productlistData.setPromotiondescription(res.getString("Promotiondescription"));
			productlistData.setStartdate(res.getString("startdate"));
			productlistData.setEnddate(res.getString("enddate"));
			productlistData.setPromotiontype(res.getInt("promotiontype"));
			productlistData.setPromotionamount(res.getInt("PromotionAmount"));
			productlistData.setInterestRate(res.getInt("InterestRate"));
			productlistData.setOutletstatus(res.getInt("outletstatus"));
			productlistData.setProductstatus(res.getInt("productstatus"));
			productlistData.setPricestatus(res.getInt("pricestatus"));
			productlistData.setPromotionstatus(res.getInt("promotionstatus"));

			productlist.add(productlistData);
		}

		return productlist;
	}

	public ProductListData getProductDataByOutletProductLinkSyskey(Connection conn, String outletproductLinksyskey)
			throws SQLException {

		String query1 = "select   OutletSysKey,OutletProductLinkSysKey, ProductSysKey, ProductPriceSysKey, PromotionSysKey, "
				+ "PK_OutletId, PK_ProductCode,ProductDescription, Category, ProductUnit, Manufacturer, "
				+ "Supplier, Model, SerialNo,ProductImagePath,ProductInfoFile, "
				+ "PK_PriceCode,  PriceUnit, price, PK_PromotionCode, Promotiondescription,"
				+ "startdate,enddate, promotiontype,  PromotionAmount, InterestRate, "
				+ "outletstatus,  productstatus, pricestatus, promotionstatus"
				+ " from VI_OutletProductPricePromotion where outletstatus=? and productstatus=? and pricestatus=? and (promotionstatus=? or promotionstatus IS NULL)  and  OutletProductLinkSysKey=?";

		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setString(i++, outletproductLinksyskey);

		ProductListData productlistData = new ProductListData();
		ResultSet res = stmt2.executeQuery();
		if (res.next()) {

			productlistData.setOutletsyskey(res.getLong("OutletSysKey"));
			productlistData.setOutletProductLinksyskey(res.getLong("OutletProductLinkSysKey"));
			productlistData.setProductSyskey(res.getLong("ProductSysKey"));
			productlistData.setProductPricesyskey(res.getLong("ProductPriceSysKey"));
			productlistData.setPromotionsyskey(res.getLong("PromotionSysKey"));
			productlistData.setOutletid(res.getString("PK_OutletId"));
			productlistData.setProductCode(res.getString("PK_ProductCode"));
			productlistData.setProductdescription(res.getString("ProductDescription"));
			productlistData.setCategory(res.getString("Category"));
			productlistData.setProductunit(res.getString("ProductUnit"));
			productlistData.setManufacture(res.getString("Manufacturer"));
			productlistData.setSupplier(res.getString("Supplier"));
			productlistData.setModel(res.getString("Model"));
			productlistData.setSerialno(res.getString("SerialNo"));
			productlistData.setProductImagePath(res.getString("ProductImagePath"));
			productlistData.setProductInfoFile(res.getString("ProductInfoFile"));
			productlistData.setPricecode(res.getString("PK_PriceCode"));
			productlistData.setPriceunit(res.getString("PriceUnit"));
			productlistData.setProductprice(res.getInt("price"));
			productlistData.setPromotioncode(res.getString("PK_PromotionCode"));
			productlistData.setPromotiondescription(res.getString("Promotiondescription"));
			productlistData.setStartdate(res.getString("startdate"));
			productlistData.setEnddate(res.getString("enddate"));
			productlistData.setPromotiontype(res.getInt("promotiontype"));
			productlistData.setPromotionamount(res.getInt("PromotionAmount"));
			productlistData.setInterestRate(res.getDouble("InterestRate"));
			productlistData.setOutletstatus(res.getInt("outletstatus"));
			productlistData.setProductstatus(res.getInt("productstatus"));
			productlistData.setPricestatus(res.getInt("pricestatus"));
			productlistData.setPromotionstatus(res.getInt("promotionstatus"));

		}

		return productlistData;
	}

	public ArrayList<ProductListData> getProductDataByLoanSyskey(Connection conn, String loansyskey)
			throws SQLException {
		ArrayList<ProductListData> productlist = new ArrayList<ProductListData>();

		// String query1 = "select OutletSysKey,OutletProductLinkSysKey,
		// ProductSysKey, ProductPriceSysKey, PromotionSysKey, "
		// + "PK_OutletId, PK_ProductCode,ProductDescription, Category,
		// ProductUnit, Manufacturer,"
		// + "Supplier, Model, SerialNo,ProductImagePath,ProductInfoFile, "
		// + "PK_PriceCode, PriceUnit, price, PK_PromotionCode,
		// Promotiondescription,"
		// + "startdate,enddate, promotiontype, PromotionAmount, InterestRate, "
		// + "outletstatus, productstatus, pricestatus, promotionstatus"
		// + " from mfi_mobile.VI_OutletProductPricePromotion "
		// + " where outletstatus=? and productstatus=? and pricestatus=? and
		// (promotionstatus=? or promotionstatus IS NULL)"
		// + " and OutletProductLinkSysKey IN(select FK_OutLetProductSysKey from
		// mfi_mobile.AppliedProductList WHERE FK_LoanSysKey=?)";
		String query1 = "select   OutletSysKey,OutletProductLinkSysKey, ProductSysKey, ProductPriceSysKey, PromotionSysKey,"
				+ " PK_OutletId, PK_ProductCode,ProductDescription, Category, ProductUnit, Manufacturer,"
				+ " Supplier, Model, SerialNo,ProductImagePath,ProductInfoFile, "
				+ " PK_PriceCode,  PriceUnit, price, PK_PromotionCode, Promotiondescription,"
				+ " startdate,enddate, promotiontype,  PromotionAmount, InterestRate, "
				+ " outletstatus,  productstatus, pricestatus, promotionstatus"
				+ " from VI_OutletProductPricePromotion inner join  AppliedProductList on FK_OutLetProductSysKey=OutletProductLinkSysKey"
				+ " where outletstatus=? and productstatus=? and pricestatus=? and (promotionstatus=? or promotionstatus IS NULL)"
				+ " and  FK_LoanSysKey=?";

		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setInt(i++, 1);
		stmt2.setString(i++, loansyskey);

		ResultSet res = stmt2.executeQuery();
		while (res.next()) {
			ProductListData productlistData = new ProductListData();
			productlistData.setOutletsyskey(res.getLong("OutletSysKey"));
			productlistData.setOutletProductLinksyskey(res.getLong("OutletProductLinkSysKey"));
			productlistData.setProductSyskey(res.getLong("ProductSysKey"));
			productlistData.setProductPricesyskey(res.getLong("ProductPriceSysKey"));
			productlistData.setPromotionsyskey(res.getLong("PromotionSysKey"));
			productlistData.setOutletid(res.getString("PK_OutletId"));
			productlistData.setProductCode(res.getString("PK_ProductCode"));
			productlistData.setProductdescription(res.getString("ProductDescription"));
			productlistData.setCategory(res.getString("Category"));
			productlistData.setProductunit(res.getString("ProductUnit"));
			productlistData.setManufacture(res.getString("Manufacturer"));
			productlistData.setSupplier(res.getString("Supplier"));
			productlistData.setModel(res.getString("Model"));
			productlistData.setSerialno(res.getString("SerialNo"));
			productlistData.setProductImagePath(res.getString("ProductImagePath"));
			productlistData.setProductInfoFile(res.getString("ProductInfoFile"));
			productlistData.setPricecode(res.getString("PK_PriceCode"));
			productlistData.setPriceunit(res.getString("PriceUnit"));
			productlistData.setProductprice(res.getInt("price"));
			productlistData.setPromotioncode(res.getString("PK_PromotionCode"));
			productlistData.setPromotiondescription(res.getString("Promotiondescription"));
			productlistData.setStartdate(res.getString("startdate"));
			productlistData.setEnddate(res.getString("enddate"));
			productlistData.setPromotiontype(res.getInt("promotiontype"));
			productlistData.setPromotionamount(res.getInt("PromotionAmount"));
			productlistData.setInterestRate(res.getInt("InterestRate"));
			productlistData.setOutletstatus(res.getInt("outletstatus"));
			productlistData.setProductstatus(res.getInt("productstatus"));
			productlistData.setPricestatus(res.getInt("pricestatus"));
			productlistData.setPromotionstatus(res.getInt("promotionstatus"));

			productlist.add(productlistData);
		}

		return productlist;
	}

	public static Result insertProduct(ProductData obj, Connection conn) throws SQLException {
		Result res = new Result();

		try {

			obj.setRegistrationDate(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
			obj.setModifiedDate(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
			obj.setStatus(1);
			obj.setProductCode(getNewProductCode(conn));

			String sql = "insert into [Product] (ProductSysKey,PK_ProductCode,T2,T3,T4,T5,T6,T7,T8,T9,T10,T950,T951,T952,status) "
					+ " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement stmt = conn.prepareStatement(sql);

			int i = 1;
			stmt.setLong(i++, obj.getProductSyskey());
			stmt.setString(i++, obj.getProductCode());
			
			stmt.setString(i++, obj.getProductdescription());
			stmt.setString(i++, obj.getCategory());
			stmt.setString(i++, obj.getProductunit());
			stmt.setString(i++, obj.getManufacture());
			stmt.setString(i++, obj.getSupplier());
			stmt.setString(i++, obj.getModel());

			stmt.setString(i++, obj.getSerialno());
			stmt.setString(i++, obj.getProductImagePath());
			stmt.setString(i++, obj.getProductInfoFile());
			stmt.setString(i++, obj.getRegistrationDate());
			stmt.setString(i++, obj.getModifiedDate());
			stmt.setString(i++, obj.getModifiedBy());
			stmt.setInt(i++, obj.getStatus());

			int rs = stmt.executeUpdate();

			if (rs > 0) {
				res.setState(true);
				res.setMsgDesc("Saved Successfully");
				res.setMsgCode("0000");

			} else {
				res.setState(false);
				res.setMsgDesc("Cannot Save");
				res.setMsgCode("0014");
			}

		}

		catch (Exception e) {
			res.setState(false);
			res.setMsgDesc(e.toString());
			res.setMsgCode("0014");
			e.printStackTrace();
		}

		return res;
	}

	public static String getNewProductCode(Connection aConn) {
		String ret = new String();

		Result res = new Result();
		res = KeyGenarateMgr.getKeyDataMgr("Product", 1);
		ret = res.getKeyString();
		return ret;
	}

	public static boolean isProductRefIdExist(ProductData obj, Connection conn) throws SQLException {

		String sql = "select ProductSysKey From Product where PK_ProductCode = ? and status <> ?";
		PreparedStatement psmt = conn.prepareStatement(sql);
		int i=1;
		psmt.setString(i++, obj.getProductCode().trim());
		psmt.setInt(i++, 4);
		ResultSet res = psmt.executeQuery();

		if (res.next()) {
			return true;
		} else {
			return false;
		}

	}

	public static Result updateProductByProductRefId(ProductData obj, Connection conn) throws SQLException {
		Result res = new Result();
		obj.setModifiedDate(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
		try {

			String sql = "UPDATE [Product] SET t2=?t3=?,t4=?,t5=?,t6=?,t7=?,t8=?,t9=?,t10=?"
					+ "t951=?,t952=? where PK_ProductCode = ?";

			PreparedStatement stmt = conn.prepareStatement(sql);

			int i = 1;
			stmt.setString(i++, obj.getProductdescription());
			stmt.setString(i++, obj.getCategory());
			stmt.setString(i++, obj.getProductunit());
			stmt.setString(i++, obj.getManufacture());
			stmt.setString(i++, obj.getSupplier());
			stmt.setString(i++, obj.getModel());
			stmt.setString(i++, obj.getSerialno());
			stmt.setString(i++, obj.getProductImagePath());
			stmt.setString(i++, obj.getProductInfoFile());
			stmt.setString(i++, obj.getRegistrationDate());
			stmt.setString(i++, obj.getModifiedDate());
			stmt.setString(i++, obj.getModifiedBy());
			stmt.setString(i++, obj.getProductCode());

			int rs = stmt.executeUpdate();

			if (rs > 0) {
				res.setState(true);
				res.setMsgDesc("Update Successfully");
				res.setMsgCode("0000");

			} else {
				res.setState(false);
				res.setMsgDesc("Cannot Save");
				res.setMsgCode("0014");
			}

		}

		catch (Exception e) {
			res.setMsgDesc(e.toString());
			res.setMsgCode("0014");
			e.printStackTrace();
		}

		return res;
	}
	

	public static long getProductSyskeyByProductCode(String prodcutcode, Connection conn) throws SQLException {

		String sql = "select ProductSysKey from Product where PK_ProductCode=? and status <> ?";
		PreparedStatement psmt = conn.prepareStatement(sql);
		int i=1;
		psmt.setString(i++, prodcutcode.trim());
		psmt.setInt(i++, 4);
		ResultSet res = psmt.executeQuery();

		if (res.next()) {
			return res.getLong("ProductSysKey");
		} else {
			return 0;
		}

	}
}
