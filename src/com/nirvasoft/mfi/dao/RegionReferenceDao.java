package com.nirvasoft.mfi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.mfi.users.data.RegionData;

public class RegionReferenceDao {
	public ArrayList<RegionData> getAllRegion(Connection conn) throws SQLException {
		ArrayList<RegionData> regionList = new ArrayList<RegionData>();

		String query1 = "Select RegionReferenceSysKey,PK_RegionId,T1,T950,T951,T952,status from [RegionReference] where  status= ?";
		PreparedStatement stmt2 = conn.prepareStatement(query1);
		int i = 1;
		stmt2.setInt(i, 1);

		ResultSet regtable = stmt2.executeQuery();
		while (regtable.next()) {
			RegionData regionData = new RegionData();
			regionData.setRegionRefSyskey(regtable.getLong("RegionReferenceSysKey"));
			regionData.setRegionId(regtable.getString("PK_RegionId"));
			regionData.setDescription(regtable.getString("T1"));
			regionData.setRegistrationDate(regtable.getString("T950"));
			regionData.setModifiedDate(regtable.getString("T951"));
			regionData.setModifiedBy(regtable.getString("T952"));
			regionData.setStatus(regtable.getInt("status"));
			regionList.add(regionData);
		}

		return regionList;
	}
}
