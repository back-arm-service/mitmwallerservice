package com.nirvasoft.mfi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nirvasoft.mfi.mgr.KeyGenarateMgr;
import com.nirvasoft.mfi.users.data.ApplyProductData;
import com.nirvasoft.rp.framework.Result;
public class AppliedProductListDao {


	public static Result insertAppliedProductList(ApplyProductData obj, Connection conn) throws SQLException {
		Result res = new Result();

		try {
			obj.setRegistrationDate(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
			
			obj.setModifiedDate(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
			obj.setStatus(1);				
			obj.setAppliedProductListId(getAppliedProductListID(conn));

			String sql ="insert into AppliedProductList(AppliedProductListSysKey,PK_AppliedProductListID,FK_LoanSysKey,FK_OutLetProductSysKey,T950,T951,T952,status,drvProductPrice) values(?,?,?,?,?,?,?,?,?)";
			PreparedStatement stmt = conn.prepareStatement(sql);

			int i = 1;
			stmt.setLong(i++, obj.getAppliedProductListSyskey());
			stmt.setString(i++, obj.getAppliedProductListId());		
			stmt.setLong(i++, obj.getLoanSyskey());
			stmt.setLong(i++, obj.getOutletProductSyskey());
			stmt.setString(i++, obj.getRegistrationDate());
			stmt.setString(i++, obj.getModifiedDate());
			stmt.setString(i++, obj.getModifiedBy());
			stmt.setInt(i++, obj.getStatus());
			stmt.setDouble(i++, obj.getProductprice());

			int rs = stmt.executeUpdate();

			if (rs > 0) {				
				res.setState(true);
				res.setMsgDesc("Saved Successfully");
				res.setMsgCode("0000");

			} else {
				res.setState(false);
				res.setMsgDesc("Cannot Save");
				res.setMsgCode("0014");
			}

		}

		catch (Exception e) {
			res.setMsgDesc("Cannot Save");
			res.setMsgCode("0014");
			e.printStackTrace();
		}

		return res;
	}
	
	 public static String getAppliedProductListID(Connection aConn) {
	 		String ret = new String();

	 		Result res = new Result();
			res=KeyGenarateMgr.getKeyDataMgr("AppliedProductList",1);
			ret=res.getKeyString();
	 		return ret;
	 	}


}
