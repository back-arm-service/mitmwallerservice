package com.nirvasoft.mfi.mgr;

import java.sql.Connection;

import com.nirvasoft.mfi.dao.RegionReferenceDao;
import com.nirvasoft.mfi.users.data.RegionData;
import com.nirvasoft.mfi.users.data.RegionDataArray;
import com.nirvasoft.mfi.users.data.RegionDataList;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.util.ServerUtil;

public class RegionReferenceMgr {
	public static RegionDataArray getRegionRefList() {
		RegionDataArray arr = new RegionDataArray();
		RegionDataList rlist = new RegionDataList();
		RegionReferenceDao regdao = new RegionReferenceDao();

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			rlist.setData(regdao.getAllRegion(conn));

			RegionData[] dataarry = new RegionData[rlist.getData().size()];

			for (int i = 0; i < rlist.getData().size(); i++) {
				dataarry[i] = rlist.getData().get(i);
			}

			arr.setData(dataarry);
			arr.setMsgcode("0000");
			arr.setMsgDesc("Successfully");

		} catch (Exception e) {
			arr.setMsgcode("0014");
			arr.setMsgDesc(e.toString());
		} finally {

			ServerUtil.closeConnection(conn);

		}

		return arr;
	}
}
