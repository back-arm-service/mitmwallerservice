package com.nirvasoft.mfi.mgr;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.mfi.dao.MobileMemberDao;
import com.nirvasoft.mfi.dao.OrganizationDao;
import com.nirvasoft.mfi.users.data.MobileMemberData;
import com.nirvasoft.mfi.users.data.MobileMemberDataArray;
import com.nirvasoft.mfi.users.data.MobileMemberDataList;
import com.nirvasoft.mfi.users.data.OrgMemberLinkData;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.util.GeneralUtility;
import com.nirvasoft.rp.util.ServerGlobal;
import com.nirvasoft.rp.util.ServerUtil;
public class MobileMemberDataMgr {	
	public static MobileMemberData getMemberBysyskey(String syskey) {
		MobileMemberData res = new MobileMemberData();
		MobileMemberData resp = new MobileMemberData();
		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			resp = MobileMemberDao.getMemberDataBysyskey(conn, syskey);
			if (MobileMemberDao.isLoanmemberOn()) {
				res = resp;
			}
			res.setMsgCode("0000");
			res.setMsgDesc("Get List Successful.");

		} catch (Exception e) {
			res.setMsgCode("0014");
			res.setMsgDesc(e.toString());

		} finally {

			ServerUtil.closeConnection(conn);

		}

		return res;
	}	
	public static MobileMemberDataArray getGuarantorBysyskey(String syskey) {
		MobileMemberDataArray res = new MobileMemberDataArray();
		MobileMemberDataList resp = new MobileMemberDataList();

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			resp.setData(MobileMemberDao.getGuarantorDataByApplicantsyskey(conn, syskey));

			MobileMemberData[] dataarry = new MobileMemberData[resp.getData().size()];

			for (int i = 0; i < resp.getData().size(); i++) {
				dataarry[i] = resp.getData().get(i);
			}

			res.setData(dataarry);
			res.setMsgcode("0000");
			res.setMsgDesc("Get List Successful.");
		} catch (Exception e) {
			res.setMsgcode("0014");
			res.setMsgDesc(e.toString());

		} finally {

			ServerUtil.closeConnection(conn);

		}

		return res;
	}
	public static MobileMemberData getGuarantorDataByLoansyskey(String loansyskey) {
		MobileMemberData res = new MobileMemberData();
		MobileMemberData resp = new MobileMemberData();
		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			resp = MobileMemberDao.getGuarantorDataByLoansyskey(conn, loansyskey);
			if (MobileMemberDao.isGuarantorOn()) {
				res = resp;
			}
			res.setMsgCode("0000");
			res.setMsgDesc("Get List Successful.");

		} catch (Exception e) {

		} finally {

			ServerUtil.closeConnection(conn);

		}

		return res;
	}
	public static Result updateApplicant(MobileMemberData data, Connection conn) throws SQLException {
		Result res = new Result();
		// res = normalizePhoneNoformat(data.getRegistered_mobileNo());
		// if (res.isState() == false) {
		// res.setMsgCode("0014");
		// res.setMsgDesc("Phone No. is incorrect!");
		// } else {

		res = MobileMemberDao.updateApplicant(data, conn);

		// } // end of phoneno success

		return res;
	}
	public static Result saveGrarantor(MobileMemberData data, Connection conn) throws SQLException {
		Result res = new Result();
		// res = normalizePhoneNoformat(data.getRegistered_mobileNo());
		// if (res.isState() == false) {
		// res.setMsgCode("0014");
		// res.setMsgDesc("Phone No. is incorrect!");
		// } else {
		if (data.getSyskey() > 0) {
			res = MobileMemberDao.updateGrarantor(data, conn);
			res.setSyskey(data.getSyskey());
		} else {
			Result keyres = new Result();
			keyres = KeyGenarateMgr.getKeyDataMgr("MFIMobile", 2);
			data.setSyskey(Long.parseLong(keyres.getKeyString()));

			res = MobileMemberDao.insertGrarantor(data, conn);
			res.setSyskey(data.getSyskey());
		}
		// } // end of phoneno success

		return res;
	}
	public static MobileMemberData saveMobileMember(MobileMemberData data) {
		Result res = new Result();
		MobileMemberDao u_dao = new MobileMemberDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			if (conn.equals("null") || conn.equals("")) {
				data.setState(false);
				data.setMsgDesc("Connection Fail");
			} else {
				//res = GeneralUtility.normalizePhoneNoformat(data.getRegistered_mobileNo());
				/*if (res.isState() == false) {
					data.setMsgCode("0014");
					data.setMsgDesc("Phone No. is incorrect!");
				} else {*/
					res = u_dao.checkPhoneNo(data.getRegistered_mobileNo(), conn);
					ServerGlobal.setTotalbitstatus(res.getStatus());
					if (res.isState() == false) {
						data.setSyskey(res.getSyskey());
						if (MobileMemberDao.isActiveOn()) {
							/*if (u_dao.isRegisteredOn()) {
								data.setMsgCode("0014");
								data.setMsgDesc("Registered Phone Number already exist");
								return data;
							} else */if (u_dao.isSuspendedOn()) {
								data.setMsgCode("0014");
								data.setMsgDesc(
										"Registered Phone Number is suspended.Please Wait until accepting by system");
								return data;
							} else if (MobileMemberDao.isGuarantorOn()) {
								ServerGlobal.setTotalbitstatus(MobileMemberDao.OnBitTotalNewValue(
										ServerGlobal.getRegisteredPos(), ServerGlobal.getTotalbitstatus()));

								u_dao.UpdateStatuswithdeviceid(res.getSyskey(), conn, ServerGlobal.getTotalbitstatus(),
										data.getDeviceID());
								data.setMsgDesc("Registation is Successful");
								data.setMsgCode("0000");
								data.setSyskey(data.getSyskey());
								data.setState(true);
								//data.setOTPcode(true);//original
								data.setOTPcode(false);
								return data;
							}

						} else {
							data.setMsgCode("0014");
							data.setMsgDesc("Registered Phone Number is not accepted from system");
							return data;
						}
					}

					ServerUtil.closeConnection(conn);

					conn = ConnAdmin.getConn("001", "");

					Result keyres = new Result();
					if (res.isState() == false) {
						data.setSyskey(res.getSyskey());
						data.setMsgDesc("Phone no is successsful.");
						data.setMsgCode("0000");
						data.setSyskey(data.getSyskey());
						data.setState(res.isState());
						data.setOTPcode(false);
					} else {
						conn.setAutoCommit(false);
						keyres = KeyGenarateMgr.getKeyDataMgr("MFIMobile", 2);
						data.setSyskey(Long.parseLong(keyres.getKeyString()));

						res = u_dao.checkPhoneNo(data.getRegistered_mobileNo(), conn);
						res = MobileMemberDao.insert(data, conn);
						if (res.isState()) {
							OrgMemberLinkData orgmemlink = new OrgMemberLinkData();
							keyres = KeyGenarateMgr.getKeyDataMgr("MFIMobile", 2);
							orgmemlink.setSyskey(Long.parseLong(keyres.getKeyString()));
							orgmemlink.setFk_memberid(data.getSyskey());
							OrganizationDao odao = new OrganizationDao();
							long fk_orgid = odao.getOrganizationID(conn);
							orgmemlink.setFk_orgid(fk_orgid);
							Result result = odao.insert(orgmemlink, conn);
							if (result.isState()) {
								conn.setAutoCommit(true);
								data.setMsgDesc(res.getMsgDesc());
								data.setMsgCode("0000");
								data.setSyskey(data.getSyskey());
								data.setState(res.isState());
								//data.setOTPcode(true);original
								data.setOTPcode(false);
							} else {
								data.setMsgDesc("Saving Fail");
								data.setMsgCode("0014");

								data.setState(false);
								data.setOTPcode(false);
							}

						}
					}

				//} // end of phoneno success
			} // end of connection success

		} catch (SQLException e) {
			data.setState(false);
			data.setMsgDesc("Connection Fail");
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return data;
	}
	public static MobileMemberData getGuarantorDataByGuarantorsyskey(String guarantorsyskey) {
		MobileMemberData res = new MobileMemberData();
		MobileMemberData resp = new MobileMemberData();
		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			resp = MobileMemberDao.getMemberDataBysyskey(conn, guarantorsyskey);
			if (MobileMemberDao.isGuarantorOn()) {
				res = resp;
			}
			res.setMsgCode("0000");
			res.setMsgDesc("Get List Successful.");

		} catch (Exception e) {
			res.setMsgCode("0014");
			res.setMsgDesc(e.toString());

		} finally {

			ServerUtil.closeConnection(conn);

		}

		return res;
	}
}
