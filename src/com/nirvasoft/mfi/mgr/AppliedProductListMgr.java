package com.nirvasoft.mfi.mgr;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.mfi.dao.AppliedProductListDao;
import com.nirvasoft.mfi.users.data.ApplyProductData;
import com.nirvasoft.mfi.users.data.ApplyProductDataArray;
import com.nirvasoft.rp.framework.Result;

public class AppliedProductListMgr {
	public static Result saveAppliedProductListData(ApplyProductDataArray datas, Connection conn, long loansyskey)
			throws SQLException {
		Result res = new Result();
		for (int i = 0; i < datas.getData().length; i++) {
			ApplyProductData data = new ApplyProductData();
			data = datas.getData()[i];
			Result keyres = new Result();
			keyres = KeyGenarateMgr.getKeyDataMgr("MFIMobile", 2);
			data.setAppliedProductListSyskey(Long.parseLong(keyres.getKeyString()));
			data.setLoanSyskey(loansyskey);

			res = AppliedProductListDao.insertAppliedProductList(data, conn);
			res.setSyskey(loansyskey);			
		}

		return res;
	}
}
