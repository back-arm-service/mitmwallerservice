package com.nirvasoft.mfi.mgr;

import java.sql.Connection;

import com.nirvasoft.mfi.dao.KeyGenerateDao;
import com.nirvasoft.mfi.users.data.KeyData;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.util.ServerUtil;
public class KeyGenarateMgr {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Result res = new Result();
		res = KeyGenarateMgr.getKeyDataMgr("MFIMobile", 2);
	}

	public static Result getKeyDataMgr(String objectname, int datatype) {
		Result res = new Result();
		KeyData kdata = new KeyData();
		KeyGenerateDao keydao = new KeyGenerateDao();
		String data = "";

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			kdata = keydao.getKeyData(objectname, datatype, conn);
			if (datatype == 1) {
				data = leadZeros("" + kdata.getSequenceNo(), kdata.getNoOfDigit(), kdata.getPrefix());

			} else if (datatype == 2) {
				data = String.valueOf(kdata.getSequenceNo());
			}

			res.setKeyString(data);
			Result result = new Result();
			result = keydao.UpdateSeqNo(conn, kdata.getSequenceNo() + 1, kdata.getKeyTableSysKey());
			res.setMsgCode(result.getMsgCode());
			res.setMsgDesc(result.getMsgDesc());

		} catch (Exception e) {
			res.setMsgCode("0014");
			res.setMsgDesc(e.getMessage());
		} finally {

			ServerUtil.closeConnection(conn);

		}

		return res;
	}

	public static String leadZeros(String p, int size, String laeadChar) {
		String ret = p;
		for (int i = p.length(); i < size; i++) {
			ret = "0" + ret;
		}
		ret = laeadChar + ret;
		return ret;
	}

}
