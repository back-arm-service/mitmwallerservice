package com.nirvasoft.mfi.mgr;

import java.sql.Connection;

import com.nirvasoft.mfi.dao.TownshipReferenceDao;
import com.nirvasoft.mfi.users.data.TownshipData;
import com.nirvasoft.mfi.users.data.TownshipDataArray;
import com.nirvasoft.mfi.users.data.TownshipDataList;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.util.ServerUtil;

public class TownshipReferenceMgr {
	public static TownshipDataArray getTownshipRefListByRegion(long regionsyskey) {

		TownshipDataArray ret = new TownshipDataArray();
		TownshipDataList rlist = new TownshipDataList();
		TownshipReferenceDao regdao = new TownshipReferenceDao();

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			rlist.setData(regdao.getAllTownshipByRegion(conn, regionsyskey));

			TownshipData[] dataarry = new TownshipData[rlist.getData().size()];

			for (int i = 0; i < rlist.getData().size(); i++) {
				dataarry[i] = rlist.getData().get(i);
			}

			ret.setData(dataarry);
			ret.setMsgcode("0000");
			ret.setMsgDesc("Successfully");

		} catch (Exception e) {
			ret.setMsgcode("0014");
			ret.setMsgDesc(e.toString());
		} finally {

			ServerUtil.closeConnection(conn);

		}

		return ret;
	}
}
