package com.nirvasoft.mfi.mgr;

import java.sql.Connection;

import com.nirvasoft.mfi.dao.PaymentTermDao;
import com.nirvasoft.mfi.users.data.PaymentTermData;
import com.nirvasoft.mfi.users.data.PaymentTermDataArray;
import com.nirvasoft.mfi.users.data.PaymentTermDataList;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.util.ServerUtil;

public class PaymentTermMgr {
	public static PaymentTermDataArray getPaymentByOrgSyskey(String syskey) {
		PaymentTermDataArray ret = new PaymentTermDataArray();
		PaymentTermDataList plist = new PaymentTermDataList();
		PaymentTermDao pdao = new PaymentTermDao();
		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			plist.setData(pdao.getPaymentByOrgSyskey(conn, syskey));

			PaymentTermData[] dataarry = new PaymentTermData[plist.getData().size()];

			for (int i = 0; i < plist.getData().size(); i++) {
				dataarry[i] = plist.getData().get(i);
			}

			ret.setData(dataarry);
			ret.setMsgcode("0000");
			ret.setMsgDesc("Successfully.");

		} catch (Exception e) {
			ret.setMsgcode("0014");
			ret.setMsgDesc(e.toString());
		} finally {

			ServerUtil.closeConnection(conn);

		}

		return ret;
	}
	public static PaymentTermData getPaymentByPaymentTermSyskey(String syskey) {
		PaymentTermData data = new PaymentTermData();
		PaymentTermDao pdao = new PaymentTermDao();
		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			data = pdao.getPaymentByPaymentTermSyskey(conn, syskey);
			data.setMsgCode("0000");
			data.setMsgDesc("Successfully.");
		} catch (Exception e) {
			data.setMsgCode("0014");
			data.setMsgDesc(e.toString());
		} finally {

			ServerUtil.closeConnection(conn);

		}

		return data;
	}
}
