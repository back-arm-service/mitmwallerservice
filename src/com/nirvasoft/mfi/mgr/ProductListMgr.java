package com.nirvasoft.mfi.mgr;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.mfi.dao.ProductListDao;
import com.nirvasoft.mfi.users.data.ProductData;
import com.nirvasoft.mfi.users.data.ProductDataArray;
import com.nirvasoft.mfi.users.data.ProductListData;
import com.nirvasoft.mfi.users.data.ProductListDataArray;
import com.nirvasoft.mfi.users.data.ProductListDataList;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.util.ServerUtil;

public class ProductListMgr {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public static ProductListDataArray getProductListByOutletSyskey(String syskey) {
		ProductListDataArray ret = new ProductListDataArray();
		ProductListDataList plist = new ProductListDataList();
		ProductListDao pdao = new ProductListDao();

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			plist.setData(pdao.getProductListByOutletSyskey(conn, syskey));

			ProductListData[] dataarry = new ProductListData[plist.getData().size()];

			for (int i = 0; i < plist.getData().size(); i++) {
				dataarry[i] = plist.getData().get(i);
			}

			ret.setData(dataarry);
			ret.setMsgcode("0000");
			ret.setMsgDesc("Successfully.");

		} catch (Exception e) {
			System.out.println(e.toString());
			ret.setMsgcode("0014");
			ret.setMsgDesc(e.toString());
		} finally {

			ServerUtil.closeConnection(conn);

		}

		return ret;
	}

	public static ProductListData getProductDataByOutletProductLinkSyskey(String syskey) {

		ProductListData pdata = new ProductListData();
		ProductListDao pdao = new ProductListDao();

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			pdata = pdao.getProductDataByOutletProductLinkSyskey(conn, syskey);

		} catch (Exception e) {

		} finally {

			ServerUtil.closeConnection(conn);

		}

		return pdata;
	}

	public static ProductListDataArray getProductListByLoanSyskey(String loansyskey) {
		ProductListDataArray ret = new ProductListDataArray();
		ProductListDataList plist = new ProductListDataList();
		ProductListDao pdao = new ProductListDao();

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			plist.setData(pdao.getProductDataByLoanSyskey(conn, loansyskey));

			ProductListData[] dataarry = new ProductListData[plist.getData().size()];

			for (int i = 0; i < plist.getData().size(); i++) {
				dataarry[i] = plist.getData().get(i);
			}

			ret.setData(dataarry);

		} catch (Exception e) {

		} finally {

			ServerUtil.closeConnection(conn);

		}

		return ret;
	}

	public Result CheckProductData(ProductData ddata) throws NumberFormatException, SQLException {
		Result res = new Result();
		res.setState(true);
		if ((ddata.getProductdescription().equals("")) || (ddata.getProductdescription().equals(null))) {

			res.setState(false);
			res.setMsgCode("0014");
			res.setMsgDesc("Product Description is incorrect!");
			return res;
		}

		return res;
	}


}
