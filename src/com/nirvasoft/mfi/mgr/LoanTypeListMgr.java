package com.nirvasoft.mfi.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.mfi.dao.LoanTypeListDao;
import com.nirvasoft.mfi.users.data.LoanTypeArrayImpl;
import com.nirvasoft.mfi.users.data.LoanTypeArrayInterface;
import com.nirvasoft.mfi.users.data.LoanTypeDataList;
import com.nirvasoft.mfi.users.data.LoanTypeImpl;
import com.nirvasoft.mfi.users.data.LoanTypeInterface;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ServerGlobal;
import com.nirvasoft.rp.util.ServerUtil;
public class LoanTypeListMgr {
	public static LoanTypeArrayInterface getLoanTypeList(String orgid) {
		LoanTypeArrayInterface ret = new LoanTypeArrayImpl();
		LoanTypeDataList plist = new LoanTypeDataList();
		LoanTypeListDao pdao = new LoanTypeListDao();

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			plist.setData(pdao.getLoanTypeList(conn,orgid));			
			LoanTypeInterface[] dataarry = new LoanTypeImpl[plist.getData().size()];

			for (int i = 0; i < plist.getData().size(); i++) {
				dataarry[i] = plist.getData().get(i);
			}

			ret.setData(dataarry);
			ret.setMsgCode("0000");
			ret.setMsgDesc("Get List Successfully.");

		}catch (SQLException e) {
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Loan Type List : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
			ret.setMsgCode("0014");
			ret.setMsgDesc(e.toString());
		}
		catch (Exception e) {
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Loan Type List : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
			ret.setMsgCode("0014");
			ret.setMsgDesc(e.toString());
		} finally {

			ServerUtil.closeConnection(conn);

		}

		return ret;
	}

}
