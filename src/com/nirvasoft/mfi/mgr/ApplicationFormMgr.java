package com.nirvasoft.mfi.mgr;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.nirvasoft.mfi.dao.ApplicationFormDao;
import com.nirvasoft.mfi.dao.MobileMemberDao;
import com.nirvasoft.mfi.users.data.ApplicationFormData;
import com.nirvasoft.mfi.users.data.ApplicationFormDataArray;
import com.nirvasoft.mfi.users.data.ApplicationFormDataList;
import com.nirvasoft.mfi.users.data.ApplyProductData;
import com.nirvasoft.mfi.users.data.MfiRequest;
import com.nirvasoft.mfi.users.data.MobileMemberData;
import com.nirvasoft.mfi.users.data.PaymentTermData;
import com.nirvasoft.mfi.users.data.ProductListData;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.shared.GoPaymentResponse;
import com.nirvasoft.rp.shared.ResultData;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.GeneralUtility;
import com.nirvasoft.rp.util.ServerGlobal;
import com.nirvasoft.rp.util.ServerUtil;

public class ApplicationFormMgr {
	public static ApplicationFormDataArray getLoanListBySyskeyandLoanstatus(String syskey, String loanstatus) {

		ApplicationFormDataArray ret = new ApplicationFormDataArray();
		ApplicationFormDataList alist = new ApplicationFormDataList();
		ApplicationFormDao appdao = new ApplicationFormDao();

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			alist.setData(appdao.getLoanListByApplicationSyskeyandLoanStatus(conn, syskey, loanstatus));

			ApplicationFormData[] dataarry = new ApplicationFormData[alist.getData().size()];

			for (int i = 0; i < alist.getData().size(); i++) {
				dataarry[i] = alist.getData().get(i);
			}

			ret.setData(dataarry);
			ret.setMsgCode("0000");
			ret.setMsgDesc("Successfully");

		} catch (Exception e) {
			System.out.println(e.toString());
			ret.setMsgCode("0014");
			ret.setMsgDesc(e.toString());
		} finally {

			ServerUtil.closeConnection(conn);

		}

		return ret;
	}
	public static ApplicationFormDataArray getLoanDataByApplicantandLoanSyskey(String appsyskey, String loansyskey) {
		ApplicationFormDataArray arr = new ApplicationFormDataArray();//
		ApplicationFormDataList alist = new ApplicationFormDataList();//
		ArrayList<ApplicationFormData> applicationlist = new ArrayList<ApplicationFormData>();//
		ApplicationFormData application = new ApplicationFormData();
		application.setApplicantdata(MobileMemberDataMgr.getMemberBysyskey(appsyskey));
		application.setGuarantordata(MobileMemberDataMgr.getGuarantorDataByLoansyskey(loansyskey));
		application.setLoandata(LoanMgr.getLoanDataByLoansyskey(loansyskey));
		application.setShoplistData(ShopListMgr.getShopListByLoanSyskey(loansyskey));
		application.setProductListData(ProductListMgr.getProductListByLoanSyskey(loansyskey));

		applicationlist.add(application);
		alist.setData(applicationlist);
		ApplicationFormData[] dataarry = new ApplicationFormData[alist.getData().size()];

		for (int i = 0; i < alist.getData().size(); i++) {
			dataarry[i] = alist.getData().get(i);
		}

		arr.setData(dataarry);
		return arr;
	}
	public static ApplicationFormDataArray getLoanListBySyskeyandRepaymentstatus(String syskey, String loanstatus) {

		ApplicationFormDataArray ret = new ApplicationFormDataArray();
		ApplicationFormDataList alist = new ApplicationFormDataList();
		ApplicationFormDao appdao = new ApplicationFormDao();

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			alist.setData(appdao.getLoanListByApplicationSyskeyandRepaymentStatus(conn, syskey, loanstatus));

			ApplicationFormData[] dataarry = new ApplicationFormData[alist.getData().size()];

			for (int i = 0; i < alist.getData().size(); i++) {
				dataarry[i] = alist.getData().get(i);
			}

			ret.setData(dataarry);
			ret.setMsgCode("0000");
			ret.setMsgDesc("Successfully");

		} catch (Exception e) {
			System.out.println(e.toString());
			ret.setMsgCode("0000");
			ret.setMsgDesc(e.toString());
		} finally {

			ServerUtil.closeConnection(conn);

		}

		return ret;
	}

	public static Result saveApplicantData(ApplicationFormData data) {
		Result res = new Result();

		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			if (conn.equals("null") || conn.equals("")) {
				data.setMsgcode("0014");
				data.setMsgDesc("Connection Fail");
			} else {
				conn.setAutoCommit(false);
				MobileMemberData mdata = new MobileMemberData();
				mdata.setRegistered_mobileNo(data.getApplicantdata().getMobile());
				mdata = MobileMemberDataMgr.saveMobileMember(mdata);
				data.getApplicantdata().setSyskey(mdata.getSyskey());
				if (mdata.getMsgCode().equals("0000")) {					
					res = MobileMemberDao.checkMemberBySyskey(data.getApplicantdata().getSyskey(), conn);
					ServerGlobal.setTotalbitstatus(res.getStatus());
					if (!res.isState()) {

						res = MobileMemberDataMgr.updateApplicant(data.getApplicantdata(), conn);

						if (res.getMsgCode().equals("0000")) {
							//res.setMemberSyskey(mdata.getSyskey());
							res = MobileMemberDataMgr.saveGrarantor(data.getGuarantordata(), conn);
							if (res.getMsgCode().equals("0000")) {
								data.getLoandata().setMemberSyskey(data.getApplicantdata().getSyskey());
								data.getLoandata().setGuarantorSyskey(res.getSyskey());
								res = LoanMgr.saveLoanData(data.getLoandata(), conn);
								if (res.getMsgCode().equals("0000")) {
									res = AppliedProductListMgr.saveAppliedProductListData(data.getApplyProductList(),
											conn, res.getSyskey());
									if (res.getMsgCode().equals("0000")) {
										res.setMemberSyskey(data.getApplicantdata().getSyskey());//ndh
										/*ResultData mfires = new ResultData();
										res.setMemberSyskey(data.getLoandata().getMemberSyskey());
										MfiRequest mfirequest = new MfiRequest();
										mfirequest.setT1(String.valueOf(res.getSyskey()));
										mfirequest.setT2(data.getApplicantdata().getMobile());
										mfirequest.setT3(data.getApplicantdata().getMember_name());
										mfirequest.setT4("81");*/
										//mfires = MFICallAPI(mfirequest);
										//if (mfires.getMsgCode().equals("0000")) {
											conn.setAutoCommit(true);
										//}
									}
								}
							}
						}
					}
				} // end of save loan

			} // end of connection success

		} catch (SQLException e) {
			res.setMsgCode("0014");
			res.setMsgDesc("Connection Fail");
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}
	public static ResultData MFICallAPI(MfiRequest mfireq) {
		ResultData res = new ResultData();
		GeneralUtil.readMFIServiceSetting();
		String aUrl = ServerGlobal.getMfiURL();	
        int responseCode = -1;
        String responseJSONBody = "";
        try {
        	aUrl = aUrl.replace("<parameter1>", mfireq.getT1());
        	aUrl = aUrl.replace("<parameter2>", mfireq.getT2());
        	aUrl = aUrl.replace("<parameter3>", mfireq.getT3());
        	aUrl = aUrl.replace("<parameter4>", mfireq.getT4());
            URL url = new URL(aUrl);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setConnectTimeout(30000);
            httpURLConnection.setReadTimeout(30000);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestMethod("POST");
            responseCode = httpURLConnection.getResponseCode();
            responseJSONBody = convertStreamToString(httpURLConnection.getInputStream());
            
            if (responseCode == 200 && responseJSONBody != null && !responseJSONBody.trim().equals("")) {
                JSONParser jsonParser = new JSONParser();
                Object obj = jsonParser.parse(responseJSONBody);
                JSONObject jsonObj = (JSONObject) obj;
                res.setMsgCode((String) jsonObj.get("msgCode"));
            } else {
                res.setMsgCode("0014");
                res.setMsgDesc("Call MFI Service Response Fail");
                return res;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            res.setMsgCode("0014");
            res.setMsgDesc("Call MFI Service Connection Fail");
            //res.setError(ex.getMessage());
            return res;
        }
        return res;
    }
	 private static String convertStreamToString(InputStream inputStream) throws Exception {
	        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
	        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	        StringBuilder stringBuilder = new StringBuilder();
	        String line = null;
	        while ((line = bufferedReader.readLine()) != null) {
	            stringBuilder.append(line);
	        }
	        return stringBuilder.toString();
	    }
	public static ApplicationFormData getMonthlyAmountByTerm(ApplicationFormData data) throws SQLException {

		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			if (conn.equals("null") || conn.equals("")) {
				data.setMsgcode("0014");
				data.setMsgDesc("Connection Fail");
			} else {
				// get Period month
				PaymentTermData pdata = PaymentTermMgr
						.getPaymentByPaymentTermSyskey(Long.toString(data.getLoandata().getPaymentTermSyskey()));
				int periodmonth = 0;
				int promotionamount = 0;
				double interestrate = 0;
				double price = 0;
				int permonthamount = 0;
				int permonthamt = 0;
				int totalamount = 0;
				periodmonth = pdata.getPeriodMonth();

				// get Promotion amount and interest rate
				for (int i = 0; i < data.getApplyProductList().getData().length; i++) {
					ApplyProductData darray = data.getApplyProductList().getData()[i];
					ProductListData productdata = ProductListMgr
							.getProductDataByOutletProductLinkSyskey(Long.toString(darray.getOutletProductSyskey()));
					price = productdata.getPrice();
					promotionamount += productdata.getPromotionamount();
					interestrate = productdata.getInterestRate();
					totalamount += price;

					permonthamount += GeneralUtility.roundUpNumberByUsingMultipleValue(
							((price * (interestrate / 100) * periodmonth) + price) / periodmonth, 100);

				}

				data.getLoandata().setTotalAmount(totalamount);
				data.getLoandata().setMonthlyAmount(permonthamount);
				data.getLoandata().setPromotionamount(promotionamount);
				data.getLoandata().setNetAmount(totalamount - promotionamount);
				data.setMsgcode("0000");
				data.setMsgDesc("Caculation Successful.");

			}

		} finally {
			ServerUtil.closeConnection(conn);
		}
		return data;
	}
}
