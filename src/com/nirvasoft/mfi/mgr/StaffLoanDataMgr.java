package com.nirvasoft.mfi.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import com.nirvasoft.mfi.dao.LoanDao;
import com.nirvasoft.mfi.dao.MobileMemberDao;
import com.nirvasoft.mfi.dao.StaffLoanDao;
import com.nirvasoft.mfi.dao.StaffLoanMemberDao;
import com.nirvasoft.mfi.users.data.StaffLoanApplicantImpl;
import com.nirvasoft.mfi.users.data.StaffLoanApplicantInterface;
import com.nirvasoft.mfi.users.data.StaffLoanAppliedLoanListInterface;
import com.nirvasoft.mfi.users.data.StaffLoanDataInterface;
import com.nirvasoft.mfi.users.data.StaffLoanGuarantorDataArray;
import com.nirvasoft.mfi.users.data.StaffLoanGuarantorDataList;
import com.nirvasoft.mfi.users.data.StaffLoanGuarantorImpl;
import com.nirvasoft.mfi.users.data.StaffLoanGuarantorInterface;
import com.nirvasoft.mfi.users.data.StaffLoanInterface;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ServerGlobal;
import com.nirvasoft.rp.util.ServerUtil;

public class StaffLoanDataMgr {
	public static Result saveAppliedLoanListData(StaffLoanAppliedLoanListInterface data, Connection conn, long loansyskey)
	{
		Result res = new Result();

			try {
				res = StaffLoanDao.insertAppliedLoanList(data, conn);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				res.setMsgDesc("Cannot Save");
				res.setMsgCode("0014");
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("");
					l_err.add("=============================================================================");
					l_err.add("Time :" + GeneralUtil.getTime());			
					l_err.add("saveAppliedLoanListData Function in StaffLoanDataMgr.... : " +e.toString());
					l_err.add("---------------------------------------------------------------------------");
					GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
				}
			}
		

		return res;
	}
	public static Result updateApplicant(StaffLoanApplicantInterface data, Connection conn,int status) {
		Result res = new Result();
	

		try {
			res = StaffLoanMemberDao.updateApplicant(data, conn,status);
			
		} 
		catch (Exception e) {
			res.setMsgDesc("Cannot Save");
			res.setMsgCode("0014");
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("updateApplicant Function in StaffLoanDataMgr.... : " +data.getMobile());
				l_err.add("updateApplicant Function in StaffLoanDataMgr.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}
		
		return res;
	}
	public static Result saveGrarantor(StaffLoanGuarantorInterface data, Connection conn){
		Result res = new Result();
		
		if (data.getGuarantorSyskey() > 0) {

			try {
				res = StaffLoanMemberDao.updateGrarantor(data, conn);				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				res.setMsgDesc("Cannot Save");
				res.setMsgCode("0014");
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("");
					l_err.add("=============================================================================");
					l_err.add("Time :" + GeneralUtil.getTime());			
					l_err.add("Save Guarantor Function in StaffLoanDataMgr.... : " +e.toString());
					l_err.add("---------------------------------------------------------------------------");
					GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
				}
			}
			
		} else {
			
			try {
				Result keyres = new Result();
				keyres = KeyGenarateMgr.getKeyDataMgr("MFIMobile", 2);
				data.setGuarantorSyskey(Long.parseLong(keyres.getKeyString()));
				
				res = StaffLoanMemberDao.insertGrarantor(data, conn);
				res.setgSyskey(data.getGuarantorSyskey());				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				res.setMsgDesc("Cannot Save");
				res.setMsgCode("0014");
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("");
					l_err.add("=============================================================================");
					l_err.add("Time :" + GeneralUtil.getTime());			
					l_err.add("Insert Guarantor Function in StaffLoanDataMgr.... : " +e.toString());
					l_err.add("---------------------------------------------------------------------------");
					GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
				}
			}
			
		}
		
		return res;
	}
	
	public static StaffLoanApplicantInterface getStaffLoanApplicantBysyskey(String syskey) {
		StaffLoanApplicantInterface res = new StaffLoanApplicantImpl();
		StaffLoanApplicantInterface resp = new StaffLoanApplicantImpl();
		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			resp = StaffLoanMemberDao.getStaffLoanApplicantBysyskey(conn, syskey);
			if (MobileMemberDao.isLoanmemberOn(resp.getStatus())) {
				res = resp;
			}
			res.setMsgCode("0000");
			res.setMsgDesc("Get List Successful.");

		} catch (SQLException e) {
			res.setMsgCode("0014");
			res.setMsgDesc(e.toString());
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("getStaffLoanApplicantBysyskey Function in StaffLoanDataMgr.... : " +syskey);
				l_err.add("getStaffLoanApplicantBysyskey.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}
		catch (ParseException e) {
			res.setMsgCode("0014");
			res.setMsgDesc(e.toString());
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("getStaffLoanApplicantBysyskey.... : " +syskey);
				l_err.add("getStaffLoanApplicantBysyskey.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}
		catch (Exception e) {
			res.setMsgCode("0014");
			res.setMsgDesc(e.toString());
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("getStaffLoanApplicantBysyskey.... : " +syskey);
				l_err.add("getStaffLoanApplicantBysyskey.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		} finally {

			ServerUtil.closeConnection(conn);

		}

		return res;
	}
	public static StaffLoanGuarantorDataArray getGuarantorBysyskey(String syskey) {
		StaffLoanGuarantorDataArray res = new StaffLoanGuarantorDataArray();
		StaffLoanGuarantorDataList resp = new StaffLoanGuarantorDataList();

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			resp.setData(StaffLoanMemberDao.getGuarantorDataByApplicantsyskey(conn, syskey));

			StaffLoanGuarantorInterface[] dataarry = new StaffLoanGuarantorInterface[resp.getData().size()];

			for (int i = 0; i < resp.getData().size(); i++) {
				dataarry[i] = resp.getData().get(i);
			}

			res.setData(dataarry);
			res.setMsgcode("0000");
			res.setMsgDesc("Get List Successful.");
		}  catch (SQLException e) {
			res.setMsgcode("0014");
			res.setMsgDesc(e.toString());
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("getGuarantorBysyskey Function in StaffLoanDataMgr .... : " +syskey);
				l_err.add("getGuarantorBysyskey Function in StaffLoanDataMgr.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}
		catch (ParseException e) {
			res.setMsgcode("0014");
			res.setMsgDesc(e.toString());
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("getGuarantorBysyskey Function in StaffLoanDataMgr.... : " +syskey);
				l_err.add("getGuarantorBysyskey Function in StaffLoanDataMgr.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}
		catch (Exception e) {
			res.setMsgcode("0014");
			res.setMsgDesc(e.toString());
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("getGuarantorBysyskey Function in StaffLoanDataMgr.... : " +syskey);
				l_err.add("getGuarantorBysyskey Function in StaffLoanDataMgr.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		} finally {

			ServerUtil.closeConnection(conn);

		}

		return res;
	}
	public static StaffLoanGuarantorInterface getGuarantorDataByGuarantorsyskey(String guarantorsyskey) {
		StaffLoanGuarantorInterface res = new StaffLoanGuarantorImpl();
		StaffLoanGuarantorInterface resp = new StaffLoanGuarantorImpl();
		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			resp = StaffLoanMemberDao.getGuarantorDataBysyskey(conn, guarantorsyskey);
			if (MobileMemberDao.isGuarantorOn(resp.getStatus())) {
				res = resp;
			}
			res.setMsgCode("0000");
			res.setMsgDesc("Get List Successful.");

		} catch (SQLException e) {
			res.setMsgCode("0014");
			res.setMsgDesc(e.toString());
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("getGuarantorDataByGuarantorsyskey Function in StaffLoanDataMgr .... : " +guarantorsyskey);
				l_err.add("getGuarantorDataByGuarantorsyskey Function in StaffLoanDataMgr.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}
		catch (ParseException e) {
			res.setMsgCode("0014");
			res.setMsgDesc(e.toString());
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("getGuarantorDataByGuarantorsyskey Function in StaffLoanDataMgr.... : " +guarantorsyskey);
				l_err.add("getGuarantorDataByGuarantorsyskey Function in StaffLoanDataMgr.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}
		catch (Exception e) {
			res.setMsgCode("0014");
			res.setMsgDesc(e.toString());
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("getGuarantorDataByGuarantorsyskey Function in StaffLoanDataMgr.... : " +guarantorsyskey);
				l_err.add("getGuarantorDataByGuarantorsyskey Function in StaffLoanDataMgr.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}  finally {

			ServerUtil.closeConnection(conn);

		}

		return res;
	}
	public static Result updateGuarantorSign(StaffLoanInterface data) {
		Result res = new Result();
	
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			if (conn.equals("null") || conn.equals("")) {
				res.setMsgCode("0014");
				res.setMsgDesc("Connection Fail");
			} else {
				conn.setAutoCommit(false);
				data.getStaffLoanGuarantor1().setLoanSyskey(data.getStaffLoanData().getLoanSyskey());
				data.getStaffLoanGuarantor2().setLoanSyskey(data.getStaffLoanData().getLoanSyskey());
				res = StaffLoanMemberDao.updateGrarantorLoanList(data.getStaffLoanGuarantor1(), conn);
				if(res.getMsgCode().equals("0000"))
				{
					res = StaffLoanMemberDao.updateGrarantorLoanList(data.getStaffLoanGuarantor2(), conn);
					if(res.getMsgCode().equals("0000"))
					{
						 data.getStaffLoanData().setStatus( LoanDao.OnBitLoanTotalNewValue(ServerGlobal.getLoanInprocessPos(), data.getStaffLoanData().getStatus()));
						updateLoanStatus(data.getStaffLoanData(),conn);
						if(res.getMsgCode().equals("0000"))
						{
							
						}
						conn.setAutoCommit(true);
					}
				}
			}
			
		} 
		catch (Exception e) {
			res.setMsgDesc("Cannot Save");
			res.setMsgCode("0014");
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				
				l_err.add("updateGuarantorSign Function in StaffLoanDataMgr.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}
		finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}
	public static Result updateLoanStatus(StaffLoanDataInterface loan, Connection conn) {
		Result res = new Result();
	

		try {
			res = StaffLoanDao.updateLoanStatus(loan, conn);
			
		} 
		catch (Exception e) {
			res.setMsgDesc("Cannot Save");
			res.setMsgCode("0014");
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				
				l_err.add("updateLoanStatus Function in StaffLoanDataMgr.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}
		
		return res;
	}
	public static Result saveLoanData(StaffLoanDataInterface data, Connection conn) {
		Result res = new Result();
		
		res.setSyskey(data.getLoanSyskey());

		try {
			res = StaffLoanDao.insertLone(data, conn);
			
		} 
		catch (Exception e) {
			res.setMsgDesc("Cannot Save");
			res.setMsgCode("0014");
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());			
				l_err.add("saveLoanData Function in StaffLoanDataMgr.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}
		
		return res;
	}
	
}
