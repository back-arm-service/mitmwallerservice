package com.nirvasoft.mfi.mgr;

import java.sql.Connection;

import com.nirvasoft.mfi.dao.ShopListDao;
import com.nirvasoft.mfi.users.data.ShopListData;
import com.nirvasoft.mfi.users.data.ShopListDataArray;
import com.nirvasoft.mfi.users.data.ShopListDataList;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.util.ServerUtil;

public class ShopListMgr {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ShopListDataArray list = getShopList();
		for (int i = 0; i < list.getData().length; i++) {

			System.out.println(list.getData()[i].toString());

		}
	}

	public static ShopListDataArray getShopList() {
		ShopListDataArray ret = new ShopListDataArray();
		ShopListDataList slist = new ShopListDataList();
		ShopListDao sdao = new ShopListDao();

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			slist.setData(sdao.getShopList(conn));

			ShopListData[] dataarry = new ShopListData[slist.getData().size()];

			for (int i = 0; i < slist.getData().size(); i++) {
				dataarry[i] = slist.getData().get(i);
			}

			ret.setData(dataarry);
			ret.setMsgcode("0000");
			ret.setMsgDesc("Successfully.");

		} catch (Exception e) {
			ret.setMsgcode("0014");
			ret.setMsgDesc(e.toString());
		} finally {

			ServerUtil.closeConnection(conn);

		}

		return ret;
	}

	public static ShopListDataArray getShopListByRegionTownship(String region, String township) {
		ShopListDataArray ret = new ShopListDataArray();
		ShopListDataList slist = new ShopListDataList();
		ShopListDao sdao = new ShopListDao();

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			slist.setData(sdao.getShopListByRegTownship(conn, region, township));

			ShopListData[] dataarry = new ShopListData[slist.getData().size()];

			for (int i = 0; i < slist.getData().size(); i++) {
				dataarry[i] = slist.getData().get(i);
			}

			ret.setData(dataarry);
			ret.setMsgcode("0000");
			ret.setMsgDesc("Successfully.");

		} catch (Exception e) {
			ret.setMsgcode("0014");
			ret.setMsgDesc(e.toString());
		} finally {

			ServerUtil.closeConnection(conn);

		}

		return ret;
	}

	public static ShopListDataArray getShopListByApplicantSyskey(String syskey) {
		ShopListDataArray ret = new ShopListDataArray();
		ShopListDataList slist = new ShopListDataList();
		ShopListDao sdao = new ShopListDao();

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			slist.setData(sdao.getShopListByApplicantSyskey(conn, syskey));

			ShopListData[] dataarry = new ShopListData[slist.getData().size()];

			for (int i = 0; i < slist.getData().size(); i++) {
				dataarry[i] = slist.getData().get(i);
			}

			ret.setData(dataarry);
			ret.setMsgcode("0000");
			ret.setMsgDesc("Successfully.");

		} catch (Exception e) {
			ret.setMsgcode("0014");
			ret.setMsgDesc(e.toString());
		} finally {

			ServerUtil.closeConnection(conn);

		}

		return ret;
	}

	public static ShopListData getShopListByLoanSyskey(String loansyskey) {
		ShopListData ret = new ShopListData();
		ShopListDao sdao = new ShopListDao();

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			ret = sdao.getShopListByLoanSyskey(conn, loansyskey);
		} catch (Exception e) {

		} finally {

			ServerUtil.closeConnection(conn);

		}

		return ret;
	}
	

	public static ShopListDataArray getShopListByRegionSyskey(String regionsyskey) {
		ShopListDataArray ret = new ShopListDataArray();
		ShopListDataList slist = new ShopListDataList();
		ShopListDao sdao = new ShopListDao();

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			slist.setData(sdao.getShopListByRegionSyskey(conn, regionsyskey));

			ShopListData[] dataarry = new ShopListData[slist.getData().size()];

			for (int i = 0; i < slist.getData().size(); i++) {
				dataarry[i] = slist.getData().get(i);
			}

			ret.setData(dataarry);
			ret.setMsgcode("0000");
			ret.setMsgDesc("Successfully.");

		} catch (Exception e) {
			ret.setMsgcode("0014");
			ret.setMsgDesc(e.toString());
		} finally {

			ServerUtil.closeConnection(conn);

		}

		return ret;
	}
	public static ShopListDataArray getShopListByRegionSyskeyandSearchData(String regionsyskey,String searchdata) {
		ShopListDataArray ret = new ShopListDataArray();
		ShopListDataList slist = new ShopListDataList();
		ShopListDao sdao = new ShopListDao();

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			slist.setData(sdao.getShopListByRegionSyskeyandSearchData(conn, regionsyskey,searchdata));

			ShopListData[] dataarry = new ShopListData[slist.getData().size()];

			for (int i = 0; i < slist.getData().size(); i++) {
				dataarry[i] = slist.getData().get(i);
			}

			ret.setData(dataarry);
			if(dataarry.length>0)
			{
			ret.setMsgcode("0000");
			ret.setMsgDesc("Successfully.");
			}
			else
			{
				ret.setMsgcode("0014");
				ret.setMsgDesc("There is no Record.");
			}

		} catch (Exception e) {
			ret.setMsgcode("0014");
			ret.setMsgDesc(e.toString());
		} finally {

			ServerUtil.closeConnection(conn);

		}

		return ret;
	}
}
