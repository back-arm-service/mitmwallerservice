package com.nirvasoft.mfi.mgr;

import java.sql.Connection;
import com.nirvasoft.mfi.dao.OrganizationDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.util.ServerUtil;
public class OrganizationMgr {

	public static Long getOrganizationSyskey() {
		OrganizationDao odao = new OrganizationDao();

		Connection conn = null;
		long orgsyskey = 0;

		try {
			conn = ConnAdmin.getConn("001", "");
			orgsyskey = odao.getOrganizationID(conn);

		} catch (Exception e) {

		} finally {

			ServerUtil.closeConnection(conn);

		}

		return orgsyskey;
	}

}
