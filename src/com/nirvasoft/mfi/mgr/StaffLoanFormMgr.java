package com.nirvasoft.mfi.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import com.nirvasoft.mfi.dao.MobileMemberDao;
import com.nirvasoft.mfi.dao.StaffLoanDao;
import com.nirvasoft.mfi.users.data.ApplicationFormData;
import com.nirvasoft.mfi.users.data.ApplyProductData;
import com.nirvasoft.mfi.users.data.MobileMemberData;
import com.nirvasoft.mfi.users.data.PaymentTermData;
import com.nirvasoft.mfi.users.data.ProductListData;
import com.nirvasoft.mfi.users.data.StaffLoan;
import com.nirvasoft.mfi.users.data.StaffLoanDataArray;
import com.nirvasoft.mfi.users.data.StaffLoanDataList;
import com.nirvasoft.mfi.users.data.StaffLoanInterface;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.GeneralUtility;
import com.nirvasoft.rp.util.ServerGlobal;
import com.nirvasoft.rp.util.ServerUtil;

public class StaffLoanFormMgr {
	public static StaffLoanInterface getLoanListBySyskeyandLoanSyskey(String syskey, String loansyskey) {

		StaffLoanInterface ret = new StaffLoan();		
		StaffLoanDao appdao = new StaffLoanDao();

		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			ret=appdao.getLoanListByApplicationSyskeyandLoanSyskey(conn, syskey, loansyskey);

			
			ret.setMsgCode("0000");
			ret.setMsgDesc("Get List Successful.");

		} catch (SQLException e) {
			ret.setMsgCode("0014");
			ret.setMsgDesc("Connection Fail");
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("getLoanListBySyskeyandLoanSyskey Function.... : " +syskey);
				l_err.add("getLoanListBySyskeyandLoanSyskey Function.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		} catch (ParseException e) {
			ret.setMsgCode("0014");
			ret.setMsgDesc("Connection Fail");
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("getLoanListBySyskeyandLoanSyskey Function.... : " +syskey);
				l_err.add("getLoanListBySyskeyandLoanSyskey Function.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}
		
		catch (Exception e) {
			ret.setMsgCode("0014");
			ret.setMsgDesc("Connection Fail");
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("getLoanListBySyskeyandLoanSyskey Function.... : " +syskey);
				l_err.add("getLoanListBySyskeyandLoanSyskey Function.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}finally {

			ServerUtil.closeConnection(conn);

		}

		return ret;
	}
	public static Result saveStaffLoanData(StaffLoanInterface data) {
		Result res = new Result();

		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			if (conn.equals("null") || conn.equals("")) {
				res.setMsgCode("0014");
				res.setMsgDesc("Connection Fail");
			} else {
				conn.setAutoCommit(false);
				
				MobileMemberData mdata = new MobileMemberData();
				mdata.setRegistered_mobileNo(data.getStaffLoanApplicant().getMobile());
				mdata = MobileMemberDataMgr.saveMobileMember(mdata);
				data.getStaffLoanApplicant().setStaffSyskey(mdata.getSyskey());
				if (mdata.getMsgCode().equals("0000")) {
				
				res = MobileMemberDao.checkMemberBySyskey(data.getStaffLoanApplicant().getStaffSyskey(), conn);
				
				if (!res.isState()) {

					res = StaffLoanDataMgr.updateApplicant(data.getStaffLoanApplicant(), conn,res.getStatus());

					if (res.getMsgCode().equals("0000")) {
						
						data.getStaffLoanData().setCurrentPosition(data.getStaffLoanApplicant().getCurrentPosition());
						data.getStaffLoanData().setDepartment(data.getStaffLoanApplicant().getDepartment());
						data.getStaffLoanData().setMonthlyBasicIncome(data.getStaffLoanApplicant().getMonthlyBasicIncome());
						data.getStaffLoanData().setApplicantSyskey(data.getStaffLoanApplicant().getStaffSyskey());
						Result keyres = new Result();
						keyres = KeyGenarateMgr.getKeyDataMgr("MFIMobile", 2);
						data.getStaffLoanData().setLoanSyskey(Long.parseLong(keyres.getKeyString()));
						
						res = StaffLoanDataMgr.saveLoanData(data.getStaffLoanData(), conn);
						long lSyskey = res.getSyskey();
						if (res.getMsgCode().equals("0000")) {
							keyres = new Result();
							keyres = KeyGenarateMgr.getKeyDataMgr("MFIMobile", 2);
							data.getStaffAppliedLoanData().setAppliedLoanListSyskey(Long.parseLong(keyres.getKeyString()));	
							res = StaffLoanDataMgr.saveAppliedLoanListData(data.getStaffAppliedLoanData(), conn,res.getSyskey());								
							if (res.getMsgCode().equals("0000")) {
								//data.getStaffLoanGuarantor1().setApplicantSyskey(data.getStaffLoanApplicant().getStaffSyskey());
								data.getStaffLoanGuarantor1().setAppliedLoanListSyskey(data.getStaffAppliedLoanData().getAppliedLoanListSyskey());
								data.getStaffLoanGuarantor1().setLoanSyskey(data.getStaffLoanData().getLoanSyskey());
								res = StaffLoanDataMgr.saveGrarantor(data.getStaffLoanGuarantor1(), conn);	
								if (res.getMsgCode().equals("0000")) {
									res.setSyskey(lSyskey);
									res.setMemberSyskey(data.getStaffLoanApplicant().getStaffSyskey());
									conn.setAutoCommit(true);
									}
								//if (res.getMsgCode().equals("0000")) {
									//data.getStaffLoanGuarantor2().setApplicantSyskey(data.getStaffLoanApplicant().getStaffSyskey());
									//data.getStaffLoanGuarantor2().setAppliedLoanListSyskey(data.getStaffAppliedLoanData().getAppliedLoanListSyskey());
									//data.getStaffLoanGuarantor2().setLoanSyskey(data.getStaffLoanData().getLoanSyskey());
																		
									
//									res = StaffLoanDataMgr.saveGrarantor(data.getStaffLoanGuarantor2(), conn);
//									
//									if (res.getMsgCode().equals("0000")) {
//										conn.setAutoCommit(true);
//										
//										
//										}
								//}


							}

						}

					}
				}
				else
				{
					res.setMsgCode("0014");
					res.setMsgDesc("Invalid Applicant Syskey");
				}
				} // end of save loan

			} // end of connection success

		} catch (SQLException e) {
			res.setMsgCode("0014");
			res.setMsgDesc("Connection Fail");
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Apply Loan Save Function.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		} catch (Exception e) {
			res.setMsgCode("0014");
			res.setMsgDesc("Connection Fail");
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Apply Loan Save Function.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}
		finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}
	
	public static StaffLoanDataArray getLoanListBySyskeyandLoanstatus(String syskey, String loanstatus) {

		StaffLoanDataArray ret = new StaffLoanDataArray();
		StaffLoanDataList alist = new StaffLoanDataList();
		StaffLoanDao appdao = new StaffLoanDao();

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			alist.setData(appdao.getLoanListByApplicationSyskeyandLoanStatus(conn, syskey, loanstatus));

			StaffLoanInterface[] dataarry = new StaffLoanInterface[alist.getData().size()];

			for (int i = 0; i < alist.getData().size(); i++) {
				dataarry[i] = alist.getData().get(i);
			}

			ret.setData(dataarry);
			ret.setMsgcode("0000");
			ret.setMsgDesc("Get List Successful.");

		} catch (SQLException e) {
			ret.setMsgcode("0014");
			ret.setMsgDesc("Connection Fail");
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("getLoanListBySyskeyandLoanstatus Function.... : " +syskey);
				l_err.add("getLoanListBySyskeyandLoanstatus Function.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		} catch (ParseException e) {
			ret.setMsgcode("0014");
			ret.setMsgDesc("Connection Fail");
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("getLoanListBySyskeyandLoanstatus Function.... : " +syskey);
				l_err.add("getLoanListBySyskeyandLoanstatus Function.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}
		
		catch (Exception e) {
			ret.setMsgcode("0014");
			ret.setMsgDesc("Connection Fail");
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("getLoanListBySyskeyandLoanstatus Function.... : " +syskey);
				l_err.add("getLoanListBySyskeyandLoanstatus Function.... : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}finally {

			ServerUtil.closeConnection(conn);

		}

		return ret;
	}
	/*public static StaffLoan getMonthlyAmountByTerm(StaffLoan data) throws SQLException {

		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			if (conn.equals("null") || conn.equals("")) {
				data.setMsgCode("0014");
				data.setMsgDesc("Connection Fail");
			} else {
				// get Period month
				PaymentTermData pdata = PaymentTermMgr
						.getPaymentByPaymentTermSyskey(Long.toString(data.getStaffLoanData().getLoanTermSyskey()));
				int periodmonth = 0;
				int promotionamount = 0;
				double interestrate = 0;
				double price = 0;
				int permonthamount = 0;
				int permonthamt = 0;
				int totalamount = 0;
				periodmonth = pdata.getPeriodMonth();

				// get Promotion amount and interest rate
				for (int i = 0; i < data.getApplyProductList().getData().length; i++) {
					ApplyProductData darray = data.getApplyProductList().getData()[i];
					ProductListData productdata = ProductListMgr
							.getProductDataByOutletProductLinkSyskey(Long.toString(darray.getOutletProductSyskey()));
					price = productdata.getPrice();
					promotionamount += productdata.getPromotionamount();
					interestrate = productdata.getInterestRate();
					totalamount += price;

					permonthamount += GeneralUtility.roundUpNumberByUsingMultipleValue(
							((price * (interestrate / 100) * periodmonth) + price) / periodmonth, 100);

				}

				data.getLoandata().setTotalAmount(totalamount);
				data.getLoandata().setMonthlyAmount(permonthamount);
				data.getLoandata().setPromotionamount(promotionamount);
				data.getLoandata().setNetAmount(totalamount - promotionamount);
				data.setMsgcode("0000");
				data.setMsgDesc("Caculation Successful.");

			}

		} finally {
			ServerUtil.closeConnection(conn);
		}
		return data;
	}*/
}
