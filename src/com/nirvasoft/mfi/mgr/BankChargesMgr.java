package com.nirvasoft.mfi.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.mfi.dao.BankChargesListDao;
import com.nirvasoft.mfi.users.data.BankChargesArrayImpl;
import com.nirvasoft.mfi.users.data.BankChargesArrayInterface;
import com.nirvasoft.mfi.users.data.BankChargesDataList;
import com.nirvasoft.mfi.users.data.BankChargesImpl;
import com.nirvasoft.mfi.users.data.BankChargesInterface;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ServerGlobal;
import com.nirvasoft.rp.util.ServerUtil;

public class BankChargesMgr {

	public static BankChargesArrayInterface getBankChargesList(String outletLoanTypeSyskey) {
		BankChargesArrayInterface ret = new BankChargesArrayImpl();
		BankChargesDataList blist = new BankChargesDataList();
		BankChargesListDao bdao = new BankChargesListDao();

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			blist.setData(bdao.getBankChargesList(conn,outletLoanTypeSyskey));			
			BankChargesInterface[] dataarry = new BankChargesImpl[blist.getData().size()];

			for (int i = 0; i < blist.getData().size(); i++) {
				dataarry[i] = blist.getData().get(i);
			}

			ret.setData(dataarry);
			ret.setMsgCode("0000");
			ret.setMsgDesc("Get List Successfully.");

		}catch (SQLException e) {
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Bank Charges List : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
			ret.setMsgCode("0014");
			ret.setMsgDesc(e.toString());
		}
		catch (Exception e) {
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Bank Charges List : " +e.toString());
				l_err.add("---------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
			ret.setMsgCode("0014");
			ret.setMsgDesc(e.toString());
		} finally {

			ServerUtil.closeConnection(conn);

		}

		return ret;
	}

}
