package com.nirvasoft.mfi.mgr;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.mfi.dao.LoanDao;
import com.nirvasoft.mfi.users.data.LoanData;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.util.ServerUtil;

public class LoanMgr {
	public static LoanData getLoanDataByLoansyskey(String loansyskey) {

		LoanData res = new LoanData();

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			res = LoanDao.getLoanDataByLoansyskey(conn, loansyskey);

		} catch (Exception e) {

		} finally {

			ServerUtil.closeConnection(conn);

		}

		return res;
	}
	public static Result saveLoanData(LoanData data, Connection conn) throws SQLException {
		Result res = new Result();

		Result keyres = new Result();
		keyres = KeyGenarateMgr.getKeyDataMgr("MFIMobile", 2);
		data.setLoanSyskey(Long.parseLong(keyres.getKeyString()));

		res = LoanDao.insertLone(data, conn);
		res.setSyskey(data.getLoanSyskey());
		

		return res;
	}
}
