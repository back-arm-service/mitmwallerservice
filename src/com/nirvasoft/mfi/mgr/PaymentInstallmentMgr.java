package com.nirvasoft.mfi.mgr;

import java.sql.Connection;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import com.nirvasoft.mfi.dao.PaymentInstallmentDao;
import com.nirvasoft.mfi.users.data.PaymentInstallmentData;
import com.nirvasoft.mfi.users.data.PaymentInstallmentDataArray;
import com.nirvasoft.mfi.users.data.PaymentInstallmentDataList;
import com.nirvasoft.mfi.users.data.StaffLoanPaymentInstallmentArray;
import com.nirvasoft.mfi.users.data.StaffLoanPaymentInstallmentDataList;
import com.nirvasoft.mfi.users.data.StaffLoanPaymentInstallmentInterface;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.util.ServerUtil;

public class PaymentInstallmentMgr {
	
	public static PaymentInstallmentDataArray getLoanListBySyskeyandRepaymentstatus(String loansyskey) {

		PaymentInstallmentDataArray ret = new PaymentInstallmentDataArray();
		PaymentInstallmentDataList alist = new PaymentInstallmentDataList();

		Result result = getReferenceIdFormMRHFTable(loansyskey);
		if (result.getMsgCode().equals("0000") && (!result.getKeyString().equals(""))) {
			Connection conn = null;

			try {
				conn = ConnAdmin.getMRHFConn("001", "");
				String[] ref = result.getKeyString().split("-");
                String ref1 = ref[0];
                String ref2 = ref[1] + "-" + ref[2];
				alist.setData(PaymentInstallmentDao.getPaymentScheduleByLoanSyskey(conn, ref1, ref2));
				double totalAmount=0;
				double repayttotal=0;
				double outstandingtotal=0;
				PaymentInstallmentData[] dataarry = new PaymentInstallmentData[alist.getData().size()];
				

				for (int i = 0; i < alist.getData().size(); i++) {
					dataarry[i] = alist.getData().get(i);
					
					if(i==0)
					{
						totalAmount=dataarry[0].getOpeningbalance();
					}
					outstandingtotal+=dataarry[i].getOutstandingrepayment();
				}
				repayttotal=totalAmount-outstandingtotal;
				
				ret.setTotalAmount(totalAmount);
				ret.setOutstandingAmount(outstandingtotal);
				ret.setRepaymentAmount(repayttotal);

				ret.setData(dataarry);
				ret.setMsgcode("0000");
				ret.setMsgDesc("Successfully");

			} catch (Exception e) {
				System.out.println(e.toString());
				ret.setMsgcode("0014");
				ret.setMsgDesc(e.toString());
			} finally {

				ServerUtil.closeConnection(conn);

			}
		} else {
			if(result.getKeyString().equals(""))
			{
				ret.setMsgcode("0014");
				ret.setMsgDesc("There is no AppID in MobileDataImport table.");
			}else
			{
			ret.setMsgcode("0014");
			ret.setMsgDesc(result.getMsgDesc());
			}
		}

		return ret;
	}
	public static Result getReferenceIdFormMRHFTable(String loansyskey) {

		Result refId = new Result();
		Connection conn = null;

		try {
			conn = ConnAdmin.getMRHFConn("001", "");
			refId.setKeyString(PaymentInstallmentDao.getReferenceIdForMRHF(conn, loansyskey));

			refId.setMsgCode("0000");
			refId.setMsgDesc("Successfully");

		} catch (Exception e) {
			System.out.println(e.toString());
			refId.setMsgCode("0014");
			refId.setMsgDesc(e.toString());
		} finally {

			ServerUtil.closeConnection(conn);

		}

		return refId;
	}
	public static StaffLoanPaymentInstallmentArray getStaffLoanRepaymentSchedule(String loansyskey) {

		StaffLoanPaymentInstallmentArray ret = new StaffLoanPaymentInstallmentArray();
		StaffLoanPaymentInstallmentDataList alist = new StaffLoanPaymentInstallmentDataList();

		
			Connection conn = null;

			try {
				conn = ConnAdmin.getConn("001", "");
				
				alist.setData(PaymentInstallmentDao.getStaffLoanPaymentScheduleByLoanSyskey(conn, loansyskey));
				double totalAmount=0;
				double repayttotal=0;
				double outstandingtotal=0;
				StaffLoanPaymentInstallmentInterface[] dataarry = new StaffLoanPaymentInstallmentInterface[alist.getData().size()];

				for (int i = 0; i < alist.getData().size(); i++) {
					dataarry[i] = alist.getData().get(i);
					totalAmount+=dataarry[i].getMonthlyRepayment();
					if(dataarry[i].getStatus()==1)
					{
					outstandingtotal+=dataarry[i].getMonthlyRepayment();
					}
				}
				repayttotal=totalAmount-outstandingtotal;
				
				NumberFormat formatter = new DecimalFormat("#0.00");     
				formatter.format(4.0);
				
				ret.setTotalAmount(Double.parseDouble( formatter.format(totalAmount)));
				ret.setOutstandingAmount(Double.parseDouble( formatter.format(outstandingtotal)));
				ret.setRepaymentAmount(Double.parseDouble( formatter.format(repayttotal)));

				ret.setData(dataarry);
				ret.setMsgcode("0000");
				ret.setMsgDesc("Successfully");

			} catch (Exception e) {
				System.out.println(e.toString());
				ret.setMsgcode("0014");
				ret.setMsgDesc(e.toString());
			} finally {

				ServerUtil.closeConnection(conn);

			}
		

		return ret;
	}
}
