package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public interface StaffLoanDataInterface {
	public void setLoanAmount (double loanAmount);
	public double getLoanAmount ();
	public void setLoanAmountInWords (String loanAmountInWords);
	public String getLoanAmountInWords ();
	public void setTotalAmount (double totalAmount);
	public double getTotalAmount ();
	public void setMonthlyAmount (double monthlyAmount);
	public double getMonthlyAmount ();
	public void setDepartmentChiefRecommendation (String recommendation);
	public String getDepartmentChiefRecommendation ();
	public void setStaffIDCardImagePath (String idCard);
	public String getStaffIDCardImagePath ();
	public void setFrontFamilyRegImagePath (String frontFamilyReg);
	public String getFrontFamilyRegImagePath ();
	public void setBackFamilyRegImagePath (String backFamilyReg);
	public String getBackFamilyRegImagePath ();
	
	public void setLoanSyskey (long syskey);
	public long getLoanSyskey ();
	public void setLoanID (String ID);
	public String getLoanID ();
	public void setApplicantSyskey (long syskey);
	public long getApplicantSyskey ();
	public void setLoanTermSyskey (long syskey);
	public long getLoanTermSyskey ();
	public void setLoanTerm (String term);
	public String getLoanTerm();
	public void setAppliedDate(String appliedDate);
	public String getAppliedDate();
	public void setRegistrationDate(String regDate);
	public String getRegistrationDate();
	public void setModifiedDate(String modifiedDate);
	public String getModifiedDate();
	public void setModifiedBy(String modifiedBy);
	public String getModifiedBy();
	public void setStatus(int status);
	public int getStatus();
	public void setPaymentTermSyskey(String paymentTermSyskey);
	public void getPaymentTermSyskey();
	
	//for derived field
	public void setCurrentPosition (String currentPosition);
	public String getCurrentPosition ();
	public void setMonthlyBasicIncome (double monthlyBasicIncome);
	public double getMonthlyBasicIncome ();
	public void setDepartment (String department);
	public String getDepartment ();
	public void setBankInterestRate (double bankInterestRate);
	public double getBankInterestRate();
	public void setServiceCharges (double serviceCharges);
	public double getServiceCharges();
	public void setLoanType (String loanType);
	public String getLoanType();
	//
	public void setMsgCode (String code);
	public String getMsgCode ();
	public void setMsgDesc (String desc);
	public String getMsgDesc ();
	
	//
	public void setAppSignImagePath (String appSignImagePath);
	public String getAppSignImagePath();
	
	
	
	
	
}
