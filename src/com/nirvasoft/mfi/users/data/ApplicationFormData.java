package com.nirvasoft.mfi.users.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ApplicationFormData {
	private LoanData loandata;
	private MobileMemberData applicantdata;
	private MobileMemberData guarantordata;

	private ApplyProductDataArray applyProductList;
	
	private ShopListData shoplistData;
	private ProductListDataArray productListData;
	
	private ShopListDataArray shopDataList;
	
	private ImageParseDataArray imagealldata;
	
	public ShopListDataArray getShopDataList() {
		return shopDataList;
	}
	public void setShopDataList(ShopListDataArray shopDataList) {
		this.shopDataList = shopDataList;
	}
	public ShopListData getShoplistData() {
		return shoplistData;
	}
	public void setShoplistData(ShopListData shoplistData) {
		this.shoplistData = shoplistData;
	}
	public ProductListDataArray getProductListData() {
		return productListData;
	}
	public void setProductListData(ProductListDataArray productListData) {
		this.productListData = productListData;
	}
	private String msgcode="";
	private String msgDesc="";
	public LoanData getLoandata() {
		return loandata;
	}
	public void setLoandata(LoanData loandata) {
		this.loandata = loandata;
	}
	public MobileMemberData getApplicantdata() {
		return applicantdata;
	}
	public void setApplicantdata(MobileMemberData applicantdata) {
		this.applicantdata = applicantdata;
	}
	public MobileMemberData getGuarantordata() {
		return guarantordata;
	}
	public void setGuarantordata(MobileMemberData guarantordata) {
		this.guarantordata = guarantordata;
	}

	public String getMsgcode() {
		return msgcode;
	}
	public void setMsgcode(String msgcode) {
		this.msgcode = msgcode;
	}
	public String getMsgDesc() {
		return msgDesc;
	}
	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}
	public ApplyProductDataArray getApplyProductList() {
		return applyProductList;
	}
	public void setApplyProductList(ApplyProductDataArray applyProductList) {
		this.applyProductList = applyProductList;
	}
	public ImageParseDataArray getImagealldata() {
		return imagealldata;
	}
	public void setImagealldata(ImageParseDataArray imagealldata) {
		this.imagealldata = imagealldata;
	}

}
