package com.nirvasoft.mfi.users.data;

import java.util.ArrayList;

public class ProductListDataList {
	private ArrayList<ProductListData> data;
	private String searchText;
	private int totalCount;
	private int currentPage;
	private int pageSize;

	public ArrayList<ProductListData> getData() {
		return data;
	}

	public void setData(ArrayList<ProductListData> data) {
		this.data = data;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

}
