package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public interface LoanTypeInterface {
	public void setLoanTypeSyskey (long syskey);
	public long getLoanTypeSyskey ();
	public void setLoanTypeCode (String code);
	public String getLoanTypeCode ();
	public void setLoanTypeDesc(String desc);
	public String getLoanTypeDesc();
	
	public void setLoanType (int ltype);
	public int getLoanType();
	public void setMaxLoanTermInYear (int maxterm);
	public int getMaxLoanTermInYear();
	public void setOutletLoanTypeLinkSyskey (long outLoanSyskey);
	public long getOutletLoanTypeLinkSyskey ();
	
	public void setSelected(boolean select);
	public boolean getSelected();

	
}
