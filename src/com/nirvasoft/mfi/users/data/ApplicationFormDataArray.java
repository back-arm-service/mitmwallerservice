package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ApplicationFormDataArray {
	private ApplicationFormData[] data;
	private String msgCode;
	private String msgDesc;
	 public String getMsgCode() {
		return msgCode;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public ApplicationFormDataArray(){
	    	super();
	    	setData(new ApplicationFormData[1]);    	
	    	
	    }

		public ApplicationFormData[] getData() {
			return data;
		}

		public void setData(ApplicationFormData[] data) {
			this.data = data;
		}
}
