package com.nirvasoft.mfi.users.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MobileMemberData {
	private long syskey;
	private String member_id="";
	private String reference_memberID="";
	private String member_name="";
	private String nrc="";
	private String nrc2="";
	private String registered_mobileNo="";
	private String deviceID="";
	private String password="";
	private String front_nrc_image_path="";
	private String back_nrc_image_path="";
	private String dob="";
	private String company_name="";
	private String company_ph_no="";
	private String department="";
	private String current_position="";
	private int years_of_service=0;
	private double monthly_basic_income=0;
	private String creditForOhters="";
	private double creditAmount=0;
	private String lastlogin="";
	private String lastlogout="";
	private String current_address_region="";
	private String current_address_township="";
	private String current_address_street="";
	private String current_address_houseno="";
	private String current_address_postal_code="";
	private String email="";
	private String mobile="";
	private String landline_phone="";
	private String fax="";
	private String permanent_address_region="";
	private String permanent_address_township="";
	private String permanent_address_street="";
	private String permanent_address_houseno="";
	private String permanent_address_postal_code="";
	private String registration_date="";
	private String modified_date="";
	private String modifiedby="";	
	private int status=-1;
	private boolean isOTPcode=false;
	private String msgCode = "";
	private String msgDesc = "";
	private boolean state = false;
	private int userType=0;
	
	

	public long getSyskey() {
		return syskey;
	}
	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}
	public String getMember_id() {
		return member_id;
	}
	public void setMember_id(String member_id) {
		this.member_id = member_id;
	}
	
	public String getModified_date() {
		return modified_date;
	}
	public void setModified_date(String modified_date) {
		this.modified_date = modified_date;
	}
	public String getLastlogin() {
		return lastlogin;
	}
	public void setLastlogin(String lastlogin) {
		this.lastlogin = lastlogin;
	}
	public String getLastlogout() {
		return lastlogout;
	}
	public void setLastlogout(String lastlogout) {
		this.lastlogout = lastlogout;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public boolean isOTPcode() {
		return isOTPcode;
	}
	public void setOTPcode(boolean isOTPcode) {
		this.isOTPcode = isOTPcode;
	}
	public String getMsgCode() {
		return msgCode;
	}
	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}
	public String getMsgDesc() {
		return msgDesc;
	}
	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}
	public boolean isState() {
		return state;
	}
	public void setState(boolean state) {
		this.state = state;
	}
	public String getReference_memberID() {
		return reference_memberID;
	}
	public void setReference_memberID(String reference_memberID) {
		this.reference_memberID = reference_memberID;
	}
	public String getMember_name() {
		return member_name;
	}
	public void setMember_name(String member_name) {
		this.member_name = member_name;
	}
	public String getNrc() {
		return nrc;
	}
	public void setNrc(String nrc) {
		this.nrc = nrc;
	}
	public String getNrc2() {
		return nrc2;
	}
	public void setNrc2(String nrc2) {
		this.nrc2 = nrc2;
	}
	public String getRegistered_mobileNo() {
		return registered_mobileNo;
	}
	public void setRegistered_mobileNo(String registered_mobileNo) {
		this.registered_mobileNo = registered_mobileNo;
	}
	public String getDeviceID() {
		return deviceID;
	}
	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFront_nrc_image_path() {
		return front_nrc_image_path;
	}
	public void setFront_nrc_image_path(String front_nrc_image_path) {
		this.front_nrc_image_path = front_nrc_image_path;
	}
	public String getBack_nrc_image_path() {
		return back_nrc_image_path;
	}
	public void setBack_nrc_image_path(String back_nrc_image_path) {
		this.back_nrc_image_path = back_nrc_image_path;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getCompany_name() {
		return company_name;
	}
	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}
	public String getCompany_ph_no() {
		return company_ph_no;
	}
	public void setCompany_ph_no(String company_ph_no) {
		this.company_ph_no = company_ph_no;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getCurrent_position() {
		return current_position;
	}
	public void setCurrent_position(String current_position) {
		this.current_position = current_position;
	}
	public int getYears_of_service() {
		return years_of_service;
	}
	public void setYears_of_service(int years_of_service) {
		this.years_of_service = years_of_service;
	}
	public double getMonthly_basic_income() {
		return monthly_basic_income;
	}
	public void setMonthly_basic_income(double monthly_basic_income) {
		this.monthly_basic_income = monthly_basic_income;
	}
	public String getCurrent_address_region() {
		return current_address_region;
	}
	public void setCurrent_address_region(String current_address_region) {
		this.current_address_region = current_address_region;
	}
	public String getCurrent_address_township() {
		return current_address_township;
	}
	public void setCurrent_address_township(String current_address_township) {
		this.current_address_township = current_address_township;
	}
	public String getCurrent_address_street() {
		return current_address_street;
	}
	public void setCurrent_address_street(String current_address_street) {
		this.current_address_street = current_address_street;
	}
	public String getCurrent_address_houseno() {
		return current_address_houseno;
	}
	public void setCurrent_address_houseno(String current_address_houseno) {
		this.current_address_houseno = current_address_houseno;
	}
	public String getCurrent_address_postal_code() {
		return current_address_postal_code;
	}
	public void setCurrent_address_postal_code(String current_address_postal_code) {
		this.current_address_postal_code = current_address_postal_code;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getLandline_phone() {
		return landline_phone;
	}
	public void setLandline_phone(String landline_phone) {
		this.landline_phone = landline_phone;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getPermanent_address_region() {
		return permanent_address_region;
	}
	public void setPermanent_address_region(String permanent_address_region) {
		this.permanent_address_region = permanent_address_region;
	}
	public String getPermanent_address_township() {
		return permanent_address_township;
	}
	public void setPermanent_address_township(String permanent_address_township) {
		this.permanent_address_township = permanent_address_township;
	}
	public String getPermanent_address_street() {
		return permanent_address_street;
	}
	public void setPermanent_address_street(String permanent_address_street) {
		this.permanent_address_street = permanent_address_street;
	}
	public String getPermanent_address_houseno() {
		return permanent_address_houseno;
	}
	public void setPermanent_address_houseno(String permanent_address_houseno) {
		this.permanent_address_houseno = permanent_address_houseno;
	}
	public String getPermanent_address_postal_code() {
		return permanent_address_postal_code;
	}
	public void setPermanent_address_postal_code(String permanent_address_postal_code) {
		this.permanent_address_postal_code = permanent_address_postal_code;
	}
	public String getRegistration_date() {
		return registration_date;
	}
	public void setRegistration_date(String registration_date) {
		this.registration_date = registration_date;
	}
	public String getModifiedby() {
		return modifiedby;
	}
	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}
	public String getCreditForOhters() {
		return creditForOhters;
	}
	public void setCreditForOhters(String creditForOhters) {
		this.creditForOhters = creditForOhters;
	}
	public double getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(double creditAmount) {
		this.creditAmount = creditAmount;
	}
	public int getUserType() {
		return userType;
	}
	public void setUserType(int userType) {
		this.userType = userType;
	}
	
}