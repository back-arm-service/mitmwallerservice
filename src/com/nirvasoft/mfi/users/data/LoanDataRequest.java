package com.nirvasoft.mfi.users.data;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LoanDataRequest {
	
	private String fromDate="";
	private String toDate="";

	private int loanstatusposition=0;
	private int loanstatus=0;
	private int status=0;
	private int pageSize=0;
	private int currentPage=0;
	private String sessionId="";
	private String msgCode="";
	private String msgDesc="";
	private ArrayList<LoanData> data;
	public ArrayList<LoanData> getData() {
		return data;
	}
	public void setData(ArrayList<LoanData> data) {
		this.data = data;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public int getLoanstatusposition() {
		return loanstatusposition;
	}
	public void setLoanstatusposition(int loanstatusposition) {
		this.loanstatusposition = loanstatusposition;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getMsgCode() {
		return msgCode;
	}
	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}
	public String getMsgDesc() {
		return msgDesc;
	}
	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getLoanstatus() {
		return loanstatus;
	}
	public void setLoanstatus(int loanstatus) {
		this.loanstatus = loanstatus;
	}
	
	
	

}
