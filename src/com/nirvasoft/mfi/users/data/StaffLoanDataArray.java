package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class StaffLoanDataArray {
	private StaffLoanInterface[] data;
	private String msgcode="";
	private String msgDesc="";

	public String getMsgcode() {
		return msgcode;
	}

	public void setMsgcode(String msgcode) {
		this.msgcode = msgcode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public StaffLoanInterface[] getData() {
		return data;
	}

	public void setData(StaffLoanInterface[] data) {
		this.data = data;
	}

}
