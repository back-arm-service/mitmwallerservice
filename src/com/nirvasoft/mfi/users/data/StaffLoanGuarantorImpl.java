package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

import com.nirvasoft.mfi.users.data.MobileMemberData;

@XmlRootElement
public class StaffLoanGuarantorImpl implements StaffLoanGuarantorInterface{
	private long applicantSyskey=0;
	private long loanSyskey=0;
	private long appliedLoanListSyskey=0;
	private String regvalue="";
	private String townvalue="";
	private String guaSignImagePath="";
	private String guaPDFImagePath="";
	MobileMemberData memberdata=new MobileMemberData();

	
	
	@Override
	public void setGuarantorName(String guarnatorname) {
		memberdata.setMember_name(guarnatorname);
	}

	@Override
	public String getGuarantorName() {
		return memberdata.getMember_name();
	}

	@Override
	public void setGuarantorNrc(String guarantorNrc) {
		memberdata.setNrc(guarantorNrc);
	}

	@Override
	public String getGuarantorNrc() {
		return memberdata.getNrc();
	}

	@Override
	public void setCurrentAddressRegion(String region) {
		memberdata.setCurrent_address_region(region);
	}

	@Override
	public String getCurrentAddressRegion() {
		return memberdata.getCurrent_address_region();
	}

	@Override
	public void setCurrentAddressTownship(String township) {
		memberdata.setCurrent_address_township(township);
	}

	@Override
	public String getCurrentAddressTownship() {
		return memberdata.getCurrent_address_township();
	}

	@Override
	public void setCurrentAddressStreet(String street) {
		memberdata.setCurrent_address_street(street);
	}

	@Override
	public String getCurrentAddressStreet() {
		return memberdata.getCurrent_address_street();
	}

	@Override
	public void setCurrentAddressHouseNo(String houseNo) {
		memberdata.setCurrent_address_houseno(houseNo);
	}

	@Override
	public String getCurrentAddressHouseNo() {
		return memberdata.getCurrent_address_houseno();
	}

	@Override
	public void setCurrentAddressPostalCode(String postalCode) {
		memberdata.setCurrent_address_postal_code(postalCode);
	}

	@Override
	public String getCurrentAddressPostalCode() {
		return memberdata.getCurrent_address_postal_code();
	}

	@Override
	public void setMobile(String mobile) {
		memberdata.setMobile(mobile);
	}

	@Override
	public String getMobile() {
		return memberdata.getMobile();
	}

	@Override
	public void setCurrentPosition(String currentPosition) {
		memberdata.setCurrent_position(currentPosition);
	}

	@Override
	public String getCurrentPosition() {
		return memberdata.getCurrent_position();
	}

	@Override
	public void setDepartment(String department) {
		memberdata.setDepartment(department);
	}

	@Override
	public String getDepartment() {
		return memberdata.getDepartment();
	}

	@Override
	public void setFrontNRCImagePath(String frontNrcImagePath) {
		memberdata.setFront_nrc_image_path(frontNrcImagePath);
	}

	@Override
	public String getFrontNRCImagePath() {		
		return memberdata.getFront_nrc_image_path();
	}

	@Override
	public void setBackNRCImagePath(String backNrcImagePath) {
		memberdata.setBack_nrc_image_path(backNrcImagePath);
	}

	@Override
	public String getBackNRCImagePath() {
		return memberdata.getBack_nrc_image_path();
	}

	@Override
	public void setMsgCode(String code) {
		memberdata.setMsgCode(code);
	}

	@Override
	public String getMsgCode() {
		return memberdata.getMsgCode();
	}

	@Override
	public void setMsgDesc(String desc) {
		memberdata.setMsgDesc(desc);
	}

	@Override
	public String getMsgDesc() {
		return memberdata.getMsgDesc();
	}

	@Override
	public void setGuarantorSyskey(long syskey) {
		memberdata.setSyskey(syskey);
	}

	@Override
	public long getGuarantorSyskey() {
		return memberdata.getSyskey();
	}

	@Override
	public void setGuarantorID(String id) {
		memberdata.setMember_id(id);
	}

	@Override
	public String getGuarantorID() {
		return memberdata.getMember_id();
	}

	@Override
	public void setApplicantSyskey(long syskey) {
		this.applicantSyskey=syskey;
	}

	@Override
	public long getApplicantSyskey() {
		return this.applicantSyskey;
	}

	@Override
	public void setLoanSyskey(long syskey) {
		this.loanSyskey=syskey;
	}

	@Override
	public long getLoanSyskey() {
		return this.loanSyskey;
	}

	@Override
	public void setAppliedLoanListSyskey(long syskey) {
		this.appliedLoanListSyskey=syskey;
	}

	@Override
	public long getAppliedLoanListSyskey() {
		return this.appliedLoanListSyskey;
	}

	@Override
	public void setRegistrationDate(String regDate) {
		// TODO Auto-generated method stub
		memberdata.setRegistration_date(regDate);
	}

	@Override
	public String getRegistrationDate() {
		// TODO Auto-generated method stub
		return memberdata.getRegistration_date();
	}

	@Override
	public void setModifiedDate(String modifiedDate) {
		// TODO Auto-generated method stub
		memberdata.setModified_date(modifiedDate);
	}

	@Override
	public String getModifiedDate() {
		// TODO Auto-generated method stub
		return memberdata.getModified_date();
	}

	@Override
	public void setModifiedBy(String modifiedBy) {
		// TODO Auto-generated method stub
		memberdata.setModifiedby(modifiedBy);
	}

	@Override
	public String getModifiedBy() {
		// TODO Auto-generated method stub
		return memberdata.getModifiedby();
	}

	@Override
	public void setStatus(int status) {
		// TODO Auto-generated method stub
		memberdata.setStatus(status);
	}

	@Override
	public int getStatus() {
		// TODO Auto-generated method stub
		return memberdata.getStatus();
	}

	@Override
	public void setRegionValue(String region) {
		// TODO Auto-generated method stub
		this.regvalue=region;
	}

	@Override
	public String getRegionValue() {
		// TODO Auto-generated method stub
		return this.regvalue;
	}

	@Override
	public void setTownshipValue(String township) {
		// TODO Auto-generated method stub
		this.townvalue=township;
	}

	@Override
	public String getTownshipValue() {
		// TODO Auto-generated method stub
		return this.townvalue;
	}

	@Override
	public void setGuarantorSignImagePath(String guarantorSignPath) {
		// TODO Auto-generated method stub
		this.guaSignImagePath=guarantorSignPath;
	}

	@Override
	public String getGuarantorSignImagePath() {
		// TODO Auto-generated method stub
		return this.guaSignImagePath;
	}

	@Override
	public void setGuarantorPDFImagePath(String guarantorPDFPath) {
		// TODO Auto-generated method stub
		this.guaPDFImagePath=guarantorPDFPath;
	}

	@Override
	public String getGuarantorPDFImagePath() {
		// TODO Auto-generated method stub
		return this.guaPDFImagePath;
	}


}
