package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LoanTypeArrayImpl implements LoanTypeArrayInterface {
	private LoanTypeInterface[] data;
	private String msgCode = "";
	private String msgDesc = "";
	
	@Override
	public void setMsgCode(String code) {
		// TODO Auto-generated method stub
		this.msgCode=code;
	}

	@Override
	public String getMsgCode() {
		// TODO Auto-generated method stub
		return this.msgCode;
	}

	@Override
	public void setMsgDesc(String desc) {
		// TODO Auto-generated method stub
		this.msgDesc=desc;
	}

	@Override
	public String getMsgDesc() {
		// TODO Auto-generated method stub
		return this.msgDesc;
	}

	@Override
	public void setData(LoanTypeInterface[] data) {
		// TODO Auto-generated method stub
		this.data=data;
	}

	@Override
	public LoanTypeInterface[] getData() {
		// TODO Auto-generated method stub
		return this.data;
	}

}
