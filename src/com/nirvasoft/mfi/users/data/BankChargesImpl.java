package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BankChargesImpl implements BankChargesInterface{
	
	private long syskey=0;
	private String id="";
	private long productSyskey=0;
	private int loanTermInYear=0;
	private double bankInterestRate=0;
	private double serviceChargesRate=0;
	private int chargeType=0;
	private double minAmt=0;
	private double maxAmt=0;
	private double serviceChargesAmt=0;
	private double minAmt1=0;
	private double maxAmt1=0;
	private double serviceChargesAmt1=0;
	private String regDate="";
	private String modDate="";
	private String modBy="";
	private int status=0;
	

	@Override
	public void setBankChargesSyskey(long syskey) {
		// TODO Auto-generated method stub
		this.syskey=syskey;
	}

	@Override
	public long getBankChargesSyskey() {
		// TODO Auto-generated method stub
		return this.syskey;
	}

	@Override
	public void setBankChargesId(String id) {
		// TODO Auto-generated method stub
		this.id=id;
	}

	@Override
	public String getBankChargesId() {
		// TODO Auto-generated method stub
		return this.id;
	}

	@Override
	public void setProductSyskey(long syskey) {
		// TODO Auto-generated method stub
		this.productSyskey=syskey;
	}

	@Override
	public long getProductSyskey() {
		// TODO Auto-generated method stub
		return this.productSyskey;
	}

	@Override
	public void setLoanTermInYear(int loantermyear) {
		// TODO Auto-generated method stub
		this.loanTermInYear=loantermyear;
	}

	@Override
	public int getLoanTermInYear() {
		// TODO Auto-generated method stub
		return this.loanTermInYear;
	}

	@Override
	public void setBankInterestRate(double interestRate) {
		// TODO Auto-generated method stub
		this.bankInterestRate=interestRate;
	}

	@Override
	public double getBankInterestRate() {
		// TODO Auto-generated method stub
		return this.bankInterestRate;
	}

	@Override
	public void setServiceChargesRate(double serviceChargeRate) {
		// TODO Auto-generated method stub
		this.serviceChargesRate=serviceChargeRate;
	}

	@Override
	public double getServiceChargesRate() {
		// TODO Auto-generated method stub
		return this.serviceChargesRate;
	}

	@Override
	public void setChargeType(int chargeType) {
		// TODO Auto-generated method stub
		this.chargeType=chargeType;
	}

	@Override
	public int getChargeType() {
		// TODO Auto-generated method stub
		return this.chargeType;
	}

	@Override
	public void setMinLoanAmount(double minAmt) {
		// TODO Auto-generated method stub
		this.minAmt=minAmt;
	}

	@Override
	public double getMinLoanAmount() {
		// TODO Auto-generated method stub
		return this.minAmt;
	}

	@Override
	public void setMaxLoanAmount(double maxAmt) {
		// TODO Auto-generated method stub
		this.maxAmt=maxAmt;
	}

	@Override
	public double getMaxLoanAmount() {
		// TODO Auto-generated method stub
		return this.maxAmt;
	}

	@Override
	public void setServiceAmount(double serviceAmt) {
		// TODO Auto-generated method stub
		this.serviceChargesAmt=serviceAmt;
	}

	@Override
	public double getServiceAmount() {
		// TODO Auto-generated method stub
		return this.serviceChargesAmt;
	}

	@Override
	public void setRegistrationDate(String regDate) {
		// TODO Auto-generated method stub
		this.regDate=regDate;
	}

	@Override
	public String getRegistrationDate() {
		// TODO Auto-generated method stub
		return this.regDate;
	}

	@Override
	public void setModifiedDate(String modDate) {
		// TODO Auto-generated method stub
		this.modDate=modDate;
	}

	@Override
	public String getModifiedDate() {
		// TODO Auto-generated method stub
		return this.modDate;
	}

	@Override
	public void setModifiedBy(String modBy) {
		// TODO Auto-generated method stub
		this.modBy=modBy;
	}

	@Override
	public String getModifiedBy() {
		// TODO Auto-generated method stub
		return this.modBy;
	}

	@Override
	public void setStatus(int status) {
		// TODO Auto-generated method stub
		this.status=status;
	}

	@Override
	public int getStatus() {
		// TODO Auto-generated method stub
		return this.status;
	}

	@Override
	public void setMinLoanAmount1(double minAmt) {
		// TODO Auto-generated method stub
		this.minAmt1=minAmt;
	}

	@Override
	public double getMinLoanAmount1() {
		// TODO Auto-generated method stub
		return this.minAmt1;
	}

	@Override
	public void setMaxLoanAmount1(double maxAmt) {
		// TODO Auto-generated method stub
		this.maxAmt1=maxAmt;
	}

	@Override
	public double getMaxLoanAmount1() {
		// TODO Auto-generated method stub
		return this.maxAmt1;
	}

	@Override
	public void setServiceAmount1(double serviceAmt) {
		// TODO Auto-generated method stub
		this.serviceChargesAmt1=serviceAmt;
	}

	@Override
	public double getServiceAmount1() {
		// TODO Auto-generated method stub
		return this.serviceChargesAmt1;
	}

}
