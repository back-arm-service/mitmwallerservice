package com.nirvasoft.mfi.users.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ProductData {
	private long productSyskey;
	private String productCode;
	private String ProductRefId;
	private String productdescription;
	private String category;
	private String productunit;
	private String manufacture;
	private String supplier;
	private String model;
	private String serialno;
	private String productImagePath;
	private String productInfoFile;
	private String registrationDate;
	private String modifiedDate;
	private String modifiedBy;
	private int status;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getProductSyskey() {
		return productSyskey;
	}

	public void setProductSyskey(long productSyskey) {
		this.productSyskey = productSyskey;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductRefId() {
		return ProductRefId;
	}

	public void setProductRefId(String productRefId) {
		ProductRefId = productRefId;
	}

	public String getProductdescription() {
		return productdescription;
	}

	public void setProductdescription(String productdescription) {
		this.productdescription = productdescription;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getProductunit() {
		return productunit;
	}

	public void setProductunit(String productunit) {
		this.productunit = productunit;
	}

	public String getManufacture() {
		return manufacture;
	}

	public void setManufacture(String manufacture) {
		this.manufacture = manufacture;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getSerialno() {
		return serialno;
	}

	public void setSerialno(String serialno) {
		this.serialno = serialno;
	}

	public String getProductImagePath() {
		return productImagePath;
	}

	public void setProductImagePath(String productImagePath) {
		this.productImagePath = productImagePath;
	}

	public String getProductInfoFile() {
		return productInfoFile;
	}

	public void setProductInfoFile(String productInfoFile) {
		this.productInfoFile = productInfoFile;
	}

	public String getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	
}
