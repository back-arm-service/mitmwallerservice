package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TownshipDataArray {
	private TownshipData[] data;
	private String msgcode="";
	private String msgDesc="";
    public String getMsgcode() {
		return msgcode;
	}

	public void setMsgcode(String msgcode) {
		this.msgcode = msgcode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public TownshipDataArray(){
    	super();
    	setData(new TownshipData[1]);    	
    	
    }

	public TownshipData[] getData() {
		return data;
	}

	public void setData(TownshipData[] data) {
		this.data = data;
	}

}
