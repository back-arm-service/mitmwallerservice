package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ShopListDataArray {
private ShopListData[] data;
private ShopListData[] shopdata;
public ShopListData[] getShopdata() {
	return shopdata;
}

public void setShopdata(ShopListData[] shopdata) {
	this.shopdata = shopdata;
}

private String msgcode="";
private String msgDesc="";
    public String getMsgcode() {
	return msgcode;
}

public void setMsgcode(String msgcode) {
	this.msgcode = msgcode;
}

public String getMsgDesc() {
	return msgDesc;
}

public void setMsgDesc(String msgDesc) {
	this.msgDesc = msgDesc;
}

	public ShopListDataArray(){
    	super();
    	setData(new ShopListData[1]);    	
    	
    }

	public ShopListData[] getData() {
		return data;
	}

	public void setData(ShopListData[] data) {
		this.data = data;
	}


}
