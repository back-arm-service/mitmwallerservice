package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ShopListData {
	private long organizationSyskey;
	private String organizationId;
	private long dealerSyskey;
	private String dealerid;
	private long orgDearLinkSyskey;
	private String orgDearLinkId;
	private long outletSyskey;
	private String outletId;
	private String contactName;
	private String contactPhone;
	private String contactEmail;
	private String contactPosition;
	private String contactDept;
	private String outletCurrentAddressRegion;
	private String outletCurrentAddressTownship;
	private String outletCurrentAddressStreet;
	private String outletCurrentAddressHomeNo;
	private String outletCurrentAddressPostalCode;
	private String email;
	private String mobile;
	private String landlinePhone;
	private String fax;
	private String longitude;
	private String latitude;
	private String outletName;
	private String outletImagePath="";
	
	public long getOrganizationSyskey() {
		return organizationSyskey;
	}
	public void setOrganizationSyskey(long organizationSyskey) {
		this.organizationSyskey = organizationSyskey;
	}
	public String getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}
	public long getDealerSyskey() {
		return dealerSyskey;
	}
	public void setDealerSyskey(long dealerSyskey) {
		this.dealerSyskey = dealerSyskey;
	}
	public String getDealerid() {
		return dealerid;
	}
	public void setDealerid(String dealerid) {
		this.dealerid = dealerid;
	}
	public long getOrgDearLinkSyskey() {
		return orgDearLinkSyskey;
	}
	public void setOrgDearLinkSyskey(long orgDearLinkSyskey) {
		this.orgDearLinkSyskey = orgDearLinkSyskey;
	}
	public String getOrgDearLinkId() {
		return orgDearLinkId;
	}
	public void setOrgDearLinkId(String orgDearLinkId) {
		this.orgDearLinkId = orgDearLinkId;
	}
	public long getOutletSyskey() {
		return outletSyskey;
	}
	public void setOutletSyskey(long outletSyskey) {
		this.outletSyskey = outletSyskey;
	}
	public String getOutletId() {
		return outletId;
	}
	public void setOutletId(String outletId) {
		this.outletId = outletId;
	}
	public String getOutletCurrentAddressRegion() {
		return outletCurrentAddressRegion;
	}
	public void setOutletCurrentAddressRegion(String outletCurrentAddressRegion) {
		this.outletCurrentAddressRegion = outletCurrentAddressRegion;
	}
	public String getOutletCurrentAddressTownship() {
		return outletCurrentAddressTownship;
	}
	public void setOutletCurrentAddressTownship(String outletCurrentAddressTownship) {
		this.outletCurrentAddressTownship = outletCurrentAddressTownship;
	}
	public String getOutletCurrentAddressStreet() {
		return outletCurrentAddressStreet;
	}
	public void setOutletCurrentAddressStreet(String outletCurrentAddressStreet) {
		this.outletCurrentAddressStreet = outletCurrentAddressStreet;
	}
	public String getOutletCurrentAddressHomeNo() {
		return outletCurrentAddressHomeNo;
	}
	public void setOutletCurrentAddressHomeNo(String outletCurrentAddressHomeNo) {
		this.outletCurrentAddressHomeNo = outletCurrentAddressHomeNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getLandlinePhone() {
		return landlinePhone;
	}
	public void setLandlinePhone(String landlinePhone) {
		this.landlinePhone = landlinePhone;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getContactPhone() {
		return contactPhone;
	}
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}
	public String getContactEmail() {
		return contactEmail;
	}
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	public String getContactPosition() {
		return contactPosition;
	}
	public void setContactPosition(String contactPosition) {
		this.contactPosition = contactPosition;
	}
	public String getContactDept() {
		return contactDept;
	}
	public void setContactDept(String contactDept) {
		this.contactDept = contactDept;
	}
	public String getOutletName() {
		return outletName;
	}
	public void setOutletName(String outletName) {
		this.outletName = outletName;
	}
	public String getOutletCurrentAddressPostalCode() {
		return outletCurrentAddressPostalCode;
	}
	public void setOutletCurrentAddressPostalCode(String outletCurrentAddressPostalCode) {
		this.outletCurrentAddressPostalCode = outletCurrentAddressPostalCode;
	}
	public String getOutletImagePath() {
		return outletImagePath;
	}
	public void setOutletImagePath(String outletImagePath) {
		this.outletImagePath = outletImagePath;
	}
	

}
