package com.nirvasoft.mfi.users.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public interface BankChargesInterface {
	
	public void setBankChargesSyskey (long syskey);
	public long getBankChargesSyskey ();
	public void setBankChargesId (String id);
	public String getBankChargesId ();
	public void setProductSyskey (long syskey);
	public long getProductSyskey ();
	public void setLoanTermInYear (int loantermyear);
	public int getLoanTermInYear ();
	public void setBankInterestRate (double interestRate);
	public double getBankInterestRate ();
	public void setServiceChargesRate (double serviceChargeRate);
	public double getServiceChargesRate ();
	public void setChargeType (int chargeType);
	public int getChargeType ();
	public void setMinLoanAmount (double minAmt);
	public double getMinLoanAmount();
	public void setMaxLoanAmount (double maxAmt);
	public double getMaxLoanAmount ();
	public void setServiceAmount (double serviceAmt);
	public double getServiceAmount ();
	public void setRegistrationDate (String regDate);
	public String getRegistrationDate ();
	public void setModifiedDate (String modDate);
	public String getModifiedDate();
	public void setModifiedBy(String modBy);
	public String getModifiedBy();
	public void setStatus (int status);
	public int getStatus();
	
	public void setMinLoanAmount1 (double minAmt);
	public double getMinLoanAmount1();
	public void setMaxLoanAmount1 (double maxAmt);
	public double getMaxLoanAmount1 ();
	public void setServiceAmount1 (double serviceAmt);
	public double getServiceAmount1 ();

}
