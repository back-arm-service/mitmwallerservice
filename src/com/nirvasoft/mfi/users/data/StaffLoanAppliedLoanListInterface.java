package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public interface StaffLoanAppliedLoanListInterface {
	
	public void setAppliedLoanListSyskey (long syskey);
	public long getAppliedLoanListSyskey ();
	public void setAppliedLoanListID (String id);
	public String getAppliedLoanListID ();
	public void setOutletLoanTypeSyskey (long syskey);
	public long getOutletLoanTypeSyskey ();
	public void setRegistrationDate(String regDate);
	public String getRegistrationDate();
	public void setModifiedDate(String modifiedDate);
	public String getModifiedDate();
	public void setModifiedBy(String modifiedBy);
	public String getModifiedBy();
	public void setStatus(int status);
	public int getStatus();
}
