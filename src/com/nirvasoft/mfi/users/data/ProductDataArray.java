package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ProductDataArray {
private ProductData[] data;
private String msgcode="";
private String msgDesc="";
    public String getMsgcode() {
	return msgcode;
}

public void setMsgcode(String msgcode) {
	this.msgcode = msgcode;
}

public String getMsgDesc() {
	return msgDesc;
}

public void setMsgDesc(String msgDesc) {
	this.msgDesc = msgDesc;
}

	public ProductDataArray(){
    	super();
    	setData(new ProductData[1]);    	
    	
    }

	public ProductData[] getData() {
		return data;
	}

	public void setData(ProductData[] data) {
		this.data = data;
	}

}
