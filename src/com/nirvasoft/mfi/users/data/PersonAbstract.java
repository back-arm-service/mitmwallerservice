package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public abstract class PersonAbstract {

	protected String fatherName;
	protected int age;
	protected String nationality;
	protected String gender;
	protected String maritalStatus;
	protected String qualification;
	protected String occupation;
	protected String jobtitle;
	protected String nameOfEmployeer;
	protected String office_address_region="";
	protected String office_address_township="";
	protected String office_address_street="";
	protected String office_address_houseno="";
	protected String office_address_postal_code="";
	protected String office_email="";
	protected double netmonthlyincome=0;
	protected double otherincome=0;
	protected String photopath="";
	protected String photopath1="";
	protected String regvalue="";
	protected String townvalue="";
	protected String relationtoapp="";
	
	protected long applicantSyskey=0;
	protected long loanSyskey=0;
	protected String id="";
	protected long syskey=0;
	
	protected String registrationdate="";
	protected String modifieddate="";
	protected String modifiedby="";
	protected int status=0;
	

}
