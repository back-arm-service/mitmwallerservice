package com.nirvasoft.mfi.users.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class StaffLoanPaymentInstallmentArray{
	private StaffLoanPaymentInstallmentInterface[] data;
	private double totalAmount=0;
	private double outstandingAmount=0;
	private double repaymentAmount=0;
	private String msgcode = "";
	private String msgDesc = "";
	public StaffLoanPaymentInstallmentInterface[] getData() {
		return data;
	}
	public void setData(StaffLoanPaymentInstallmentInterface[] data) {
		this.data = data;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public double getOutstandingAmount() {
		return outstandingAmount;
	}
	public void setOutstandingAmount(double outstandingAmount) {
		this.outstandingAmount = outstandingAmount;
	}
	public double getRepaymentAmount() {
		return repaymentAmount;
	}
	public void setRepaymentAmount(double repaymentAmount) {
		this.repaymentAmount = repaymentAmount;
	}
	public String getMsgcode() {
		return msgcode;
	}
	public void setMsgcode(String msgcode) {
		this.msgcode = msgcode;
	}
	public String getMsgDesc() {
		return msgDesc;
	}
	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

}
