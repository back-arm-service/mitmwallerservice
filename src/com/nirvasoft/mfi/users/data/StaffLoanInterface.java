package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

import com.nirvasoft.mfi.users.data.ImageParseDataArray;

@XmlRootElement
public interface StaffLoanInterface {
	
	public void setStaffLoanApplicant (StaffLoanApplicantImpl applicant);
	public StaffLoanApplicantInterface getStaffLoanApplicant ();
	public void setStaffLoanGuarantor1 (StaffLoanGuarantorImpl guarantor);
	public StaffLoanGuarantorInterface getStaffLoanGuarantor1 ();
	public void setStaffLoanGuarantor2 (StaffLoanGuarantorImpl guarantor);
	public StaffLoanGuarantorInterface getStaffLoanGuarantor2 ();
	public void setStaffLoanData (StaffLoanDataImpl loan);
	public StaffLoanDataInterface getStaffLoanData ();
	public void setStaffAppliedLoanData (StaffLoanAppliedLoanListImpl appliedloan);
	public StaffLoanAppliedLoanListInterface getStaffAppliedLoanData ();

	public void setMsgCode (String code);
	public String getMsgCode ();
	public void setMsgDesc (String desc);
	public String getMsgDesc ();
	
	public ImageParseDataArray getImagealldata();
	public void setImagealldata(ImageParseDataArray imagealldata);
}
