package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ApplyProductData {
	private long appliedProductListSyskey;
	private String appliedProductListId;
	private long loanSyskey;
	private long outletProductSyskey;
	private double productprice=0;
	private String productdescription;
	private String category;
	private String productImagePath;

	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getProductImagePath() {
		return productImagePath;
	}
	public void setProductImagePath(String productImagePath) {
		this.productImagePath = productImagePath;
	}
	public String getProductdescription() {
		return productdescription;
	}
	public void setProductdescription(String productdescription) {
		this.productdescription = productdescription;
	}
	public double getProductprice() {
		return productprice;
	}
	public void setProductprice(double productprice) {
		this.productprice = productprice;
	}
	private String registrationDate="";
	private String modifiedDate="";
	private String modifiedBy="";
	private int status=0;
	private String msgCode="";
	private String msgDesc="";
	public long getAppliedProductListSyskey() {
		return appliedProductListSyskey;
	}
	public void setAppliedProductListSyskey(long appliedProductListSyskey) {
		this.appliedProductListSyskey = appliedProductListSyskey;
	}
	public String getAppliedProductListId() {
		return appliedProductListId;
	}
	public void setAppliedProductListId(String appliedProductListId) {
		this.appliedProductListId = appliedProductListId;
	}
	public long getLoanSyskey() {
		return loanSyskey;
	}
	public void setLoanSyskey(long loanSyskey) {
		this.loanSyskey = loanSyskey;
	}
	public long getOutletProductSyskey() {
		return outletProductSyskey;
	}
	public void setOutletProductSyskey(long outletProductSyskey) {
		this.outletProductSyskey = outletProductSyskey;
	}
	public String getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}
	public String getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getMsgCode() {
		return msgCode;
	}
	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}
	public String getMsgDesc() {
		return msgDesc;
	}
	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	} 

}
