package com.nirvasoft.mfi.users.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public interface BankChargesArrayInterface {
	public void setData (BankChargesInterface[] data);
	public BankChargesInterface[] getData();
	public void setMsgCode (String code);
	public String getMsgCode ();
	public void setMsgDesc (String desc);
	public String getMsgDesc ();
}
