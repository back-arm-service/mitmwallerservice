package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ApplyProductDataArray {
	private ApplyProductData[] data;
	 public ApplyProductDataArray(){
	    	super();
	    	setData(new ApplyProductData[1]);    	
	    	
	    }

		public ApplyProductData[] getData() {
			return data;
		}

		public void setData(ApplyProductData[] data) {
			this.data = data;
		}
}
