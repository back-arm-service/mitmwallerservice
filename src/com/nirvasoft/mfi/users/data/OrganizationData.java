package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OrganizationData {
	private long syskey;
	private String organization_id="";
	private String organization_name="";
	private String contact_person_name="";
	private String contact_person_mobile_no="";
	private String contact_person_position="";
	
	private String current_address_region="";
	private String current_address_township="";
	private String current_address_street="";
	private String current_address_houseno="";
	private String current_address_postal_code="";
	private String email="";
	private String mobile="";
	private String landline_phone="";
	private String fax="";
	private String registration_date="";
	private String modified_date="";
	private String modifiedby="";	
	private int status;
	private String msgcode="";
	private String msgDesc="";
	
	public String getMsgcode() {
		return msgcode;
	}
	public void setMsgcode(String msgcode) {
		this.msgcode = msgcode;
	}
	public String getMsgDesc() {
		return msgDesc;
	}
	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}
	
	public long getSyskey() {
		return syskey;
	}
	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}
	public String getOrganization_id() {
		return organization_id;
	}
	public void setOrganization_id(String organization_id) {
		this.organization_id = organization_id;
	}
	public String getOrganization_name() {
		return organization_name;
	}
	public void setOrganization_name(String organization_name) {
		this.organization_name = organization_name;
	}
	public String getContact_person_name() {
		return contact_person_name;
	}
	public void setContact_person_name(String contact_person_name) {
		this.contact_person_name = contact_person_name;
	}
	public String getContact_person_mobile_no() {
		return contact_person_mobile_no;
	}
	public void setContact_person_mobile_no(String contact_person_mobile_no) {
		this.contact_person_mobile_no = contact_person_mobile_no;
	}
	public String getContact_person_position() {
		return contact_person_position;
	}
	public void setContact_person_position(String contact_person_position) {
		this.contact_person_position = contact_person_position;
	}
	public String getCurrent_address_region() {
		return current_address_region;
	}
	public void setCurrent_address_region(String current_address_region) {
		this.current_address_region = current_address_region;
	}
	public String getCurrent_address_township() {
		return current_address_township;
	}
	public void setCurrent_address_township(String current_address_township) {
		this.current_address_township = current_address_township;
	}
	public String getCurrent_address_street() {
		return current_address_street;
	}
	public void setCurrent_address_street(String current_address_street) {
		this.current_address_street = current_address_street;
	}
	public String getCurrent_address_houseno() {
		return current_address_houseno;
	}
	public void setCurrent_address_houseno(String current_address_houseno) {
		this.current_address_houseno = current_address_houseno;
	}
	public String getCurrent_address_postal_code() {
		return current_address_postal_code;
	}
	public void setCurrent_address_postal_code(String current_address_postal_code) {
		this.current_address_postal_code = current_address_postal_code;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getLandline_phone() {
		return landline_phone;
	}
	public void setLandline_phone(String landline_phone) {
		this.landline_phone = landline_phone;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getRegistration_date() {
		return registration_date;
	}
	public void setRegistration_date(String registration_date) {
		this.registration_date = registration_date;
	}
	public String getModified_date() {
		return modified_date;
	}
	public void setModified_date(String modified_date) {
		this.modified_date = modified_date;
	}
	public String getModifiedby() {
		return modifiedby;
	}
	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}
