package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PaymentTermData {
	private long paymentTermSysKey;
	private String paymentTermCode;
	private long organizationSysKey;
	private int periodMonth;
	private int amountLimit;
	private int itemLimit;
	private String registrationDate;
	private String modifiedDate;
	private String modifiedBy;
	private int status;
	private String msgCode;
	private String msgDesc;
	public String getMsgDesc() {
		return msgDesc;
	}
	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}
	public String getMsgCode() {
		return msgCode;
	}
	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}
	
	
	
	public long getPaymentTermSysKey() {
		return paymentTermSysKey;
	}
	public void setPaymentTermSysKey(long paymentTermSysKey) {
		this.paymentTermSysKey = paymentTermSysKey;
	}
	public String getPaymentTermCode() {
		return paymentTermCode;
	}
	public void setPaymentTermCode(String paymentTermCode) {
		this.paymentTermCode = paymentTermCode;
	}
	public long getOrganizationSysKey() {
		return organizationSysKey;
	}
	public void setOrganizationSysKey(long organizationSysKey) {
		this.organizationSysKey = organizationSysKey;
	}
	public int getPeriodMonth() {
		return periodMonth;
	}
	public void setPeriodMonth(int periodMonth) {
		this.periodMonth = periodMonth;
	}
	public int getAmountLimit() {
		return amountLimit;
	}
	public void setAmountLimit(int amountLimit) {
		this.amountLimit = amountLimit;
	}
	public int getItemLimit() {
		return itemLimit;
	}
	public void setItemLimit(int itemLimit) {
		this.itemLimit = itemLimit;
	}
	public String getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}
	public String getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	

}
