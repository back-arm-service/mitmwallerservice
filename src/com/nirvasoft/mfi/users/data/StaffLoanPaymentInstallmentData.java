package com.nirvasoft.mfi.users.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class StaffLoanPaymentInstallmentData implements StaffLoanPaymentInstallmentInterface{
	private int batchNo;
	private String serialNo;
	private String RepayDate;
	private double repayAmount;
	private double interestAmount;
	private double pricipleAmount;
	private double monthlyRepayment;
	private int status;
	private long transRef;
	private int month;
	private double closingBalance;
	@Override
	public int getBatchNo() {
		// TODO Auto-generated method stub
		return this.batchNo;
	}
	@Override
	public void setBatchNo(int batchNo) {
		// TODO Auto-generated method stub
		this.batchNo=batchNo;
	}
	@Override
	public String getSerialNo() {
		// TODO Auto-generated method stub
		return this.serialNo;
	}
	@Override
	public void setSerialNo(String serialNo) {
		// TODO Auto-generated method stub
		this.serialNo=serialNo;
	}
	@Override
	public String getRepayDate() {
		// TODO Auto-generated method stub
		return this.RepayDate;
	}
	@Override
	public void setRepayDate(String repayDate) {
		// TODO Auto-generated method stub
		this.RepayDate=repayDate;
	}
	@Override
	public double getRepayAmount() {
		// TODO Auto-generated method stub
		return this.repayAmount;
	}
	@Override
	public void setRepayAmount(double repayAmount) {
		// TODO Auto-generated method stub
		this.repayAmount=repayAmount;
	}
	@Override
	public double getInterestAmount() {
		// TODO Auto-generated method stub
		return this.interestAmount;
	}
	@Override
	public void setInterestAmount(double interestAmount) {
		// TODO Auto-generated method stub
		this.interestAmount=interestAmount;
	}
	@Override
	public double getPricipleAmount() {
		// TODO Auto-generated method stub
		return this.pricipleAmount;
	}
	@Override
	public void setPricipleAmount(double pricipleAmount) {
		// TODO Auto-generated method stub
		this.pricipleAmount=pricipleAmount;
	}
	@Override
	public double getMonthlyRepayment() {
		// TODO Auto-generated method stub
		return this.monthlyRepayment;
	}
	@Override
	public void setMonthlyRepayment(double monthlyRepayment) {
		// TODO Auto-generated method stub
		this.monthlyRepayment=monthlyRepayment;
	}
	@Override
	public int getStatus() {
		// TODO Auto-generated method stub
		return this.status;
	}
	@Override
	public void setStatus(int status) {
		// TODO Auto-generated method stub
		this.status=status;
	}
	@Override
	public long getTransRef() {
		// TODO Auto-generated method stub
		return this.transRef;
	}
	@Override
	public void setTransRef(long transRef) {
		// TODO Auto-generated method stub
		this.transRef=transRef;
	}
	@Override
	public int getMonth() {
		// TODO Auto-generated method stub
		return this.month;
	}
	@Override
	public void setMonth(int month) {
		// TODO Auto-generated method stub
		this.month=month;
	}
	@Override
	public double getClosingBalance() {
		// TODO Auto-generated method stub
		return this.closingBalance;
	}
	@Override
	public void setClosingBalance(double closingBalance) {
		// TODO Auto-generated method stub
		this.closingBalance=closingBalance;
	}
	
	
	
}