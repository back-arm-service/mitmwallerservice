package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public interface StaffLoanGuarantorInterface {
	public void setGuarantorSyskey (long syskey);
	public long getGuarantorSyskey ();
	public void setGuarantorID (String id);
	public String getGuarantorID ();
	public void setGuarantorName (String guarnatorname);
	public String getGuarantorName ();
	public void setGuarantorNrc (String guarantorNrc);
	public String getGuarantorNrc ();
	public void setCurrentAddressRegion (String region);
	public String getCurrentAddressRegion ();
	public void setCurrentAddressTownship (String township);
	public String getCurrentAddressTownship ();
	public void setCurrentAddressStreet (String street);
	public String getCurrentAddressStreet ();
	public void setCurrentAddressHouseNo (String houseNo);
	public String getCurrentAddressHouseNo ();
	public void setCurrentAddressPostalCode (String postalCode);
	public String getCurrentAddressPostalCode ();	
	public void setMobile (String mobile);
	public String getMobile ();
	public void setCurrentPosition (String currentPosition);
	public String getCurrentPosition ();
	public void setDepartment (String department);
	public String getDepartment ();	
	public void setFrontNRCImagePath (String frontNrcImagePath);
	public String getFrontNRCImagePath ();
	public void setBackNRCImagePath (String backNrcImagePath);
	public String getBackNRCImagePath ();
	

	public void setApplicantSyskey(long syskey);
	public long getApplicantSyskey ();
	public void setLoanSyskey(long syskey);
	public long getLoanSyskey ();
	public void setAppliedLoanListSyskey(long syskey);
	public long getAppliedLoanListSyskey ();
	
	public void setRegistrationDate(String regDate);
	public String getRegistrationDate();
	public void setModifiedDate(String modifiedDate);
	public String getModifiedDate();
	public void setModifiedBy(String modifiedBy);
	public String getModifiedBy();
	public void setStatus(int status);
	public int getStatus();
	
	public void setMsgCode (String code);
	public String getMsgCode ();
	public void setMsgDesc (String desc);
	public String getMsgDesc ();
	
	public void setRegionValue (String region);
	public String getRegionValue ();
	
	public void setTownshipValue (String township);
	public String getTownshipValue();
	
	public void setGuarantorSignImagePath (String guarantorSignPath);
	public String getGuarantorSignImagePath();
	
	public void setGuarantorPDFImagePath (String guarantorPDFPath);
	public String getGuarantorPDFImagePath();
}
