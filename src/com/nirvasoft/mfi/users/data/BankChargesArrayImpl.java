package com.nirvasoft.mfi.users.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BankChargesArrayImpl implements BankChargesArrayInterface{

	private BankChargesInterface[] data;
	private String msgCode = "";
	private String msgDesc = "";
	
	@Override
	public void setMsgCode(String code) {
		// TODO Auto-generated method stub
		this.msgCode=code;
	}

	@Override
	public String getMsgCode() {
		// TODO Auto-generated method stub
		return this.msgCode;
	}

	@Override
	public void setMsgDesc(String desc) {
		// TODO Auto-generated method stub
		this.msgDesc=desc;
	}

	@Override
	public String getMsgDesc() {
		// TODO Auto-generated method stub
		return this.msgDesc;
	}


	@Override
	public BankChargesInterface[] getData() {
		// TODO Auto-generated method stub
		return this.data;
	}

	@Override
	public void setData(BankChargesInterface[] data) {
		// TODO Auto-generated method stub
		this.data=data;
	}


}
