package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

import com.nirvasoft.mfi.users.data.ApplyProductData;

@XmlRootElement
public class StaffLoanAppliedLoanListImpl implements StaffLoanAppliedLoanListInterface{

	ApplyProductData applyLoan=new ApplyProductData();

	@Override
	public void setAppliedLoanListSyskey(long syskey) {
		applyLoan.setAppliedProductListSyskey(syskey);
	}

	@Override
	public long getAppliedLoanListSyskey() {
		return applyLoan.getAppliedProductListSyskey();
	}

	@Override
	public void setAppliedLoanListID(String id) {
		applyLoan.setAppliedProductListId(id);
	}

	@Override
	public String getAppliedLoanListID() {
		return applyLoan.getAppliedProductListId();
	}

	@Override
	public void setRegistrationDate(String regDate) {
		// TODO Auto-generated method stub
		applyLoan.setRegistrationDate(regDate);
	}

	@Override
	public String getRegistrationDate() {
		// TODO Auto-generated method stub
		return applyLoan.getRegistrationDate();
	}

	@Override
	public void setModifiedDate(String modifiedDate) {
		// TODO Auto-generated method stub
		applyLoan.setModifiedDate(modifiedDate);
	}

	@Override
	public String getModifiedDate() {
		// TODO Auto-generated method stub
		return applyLoan.getModifiedDate();
	}

	@Override
	public void setModifiedBy(String modifiedBy) {
		// TODO Auto-generated method stub
		applyLoan.setModifiedBy(modifiedBy);
	}

	@Override
	public String getModifiedBy() {
		// TODO Auto-generated method stub
		return applyLoan.getModifiedBy();
	}

	@Override
	public void setStatus(int status) {
		// TODO Auto-generated method stub
		applyLoan.setStatus(status);
	}

	@Override
	public int getStatus() {
		// TODO Auto-generated method stub
		return applyLoan.getStatus();
	}

	@Override
	public void setOutletLoanTypeSyskey(long syskey) {
		// TODO Auto-generated method stub
		applyLoan.setOutletProductSyskey(syskey);
	}

	@Override
	public long getOutletLoanTypeSyskey() {
		// TODO Auto-generated method stub
		return applyLoan.getOutletProductSyskey();
	}
	

}
