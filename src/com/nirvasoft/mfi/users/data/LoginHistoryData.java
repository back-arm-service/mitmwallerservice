package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LoginHistoryData {
	private long loginhistorysyskey=0;
	private MobileMemberData mdata;
	private int logintype=0;
	private String logindatetime="";
	private String regiatrationDate="";
	private String modifiedDate="";
	private String modifiedBy="";
	private int status=0;
	private String msgcode="";
	private String msgDesc="";
	private String sessionId="";
	
	public long getLoginhistorysyskey() {
		return loginhistorysyskey;
	}
	public void setLoginhistorysyskey(long loginhistorysyskey) {
		this.loginhistorysyskey = loginhistorysyskey;
	}
	public MobileMemberData getMdata() {
		return mdata;
	}
	public void setMdata(MobileMemberData mdata) {
		this.mdata = mdata;
	}
	public int getLogintype() {
		return logintype;
	}
	public void setLogintype(int logintype) {
		this.logintype = logintype;
	}
	public String getLogindatetime() {
		return logindatetime;
	}
	public void setLogindatetime(String logindatetime) {
		this.logindatetime = logindatetime;
	}
	public String getRegiatrationDate() {
		return regiatrationDate;
	}
	public void setRegiatrationDate(String regiatrationDate) {
		this.regiatrationDate = regiatrationDate;
	}
	public String getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getMsgcode() {
		return msgcode;
	}
	public void setMsgcode(String msgcode) {
		this.msgcode = msgcode;
	}
	public String getMsgDesc() {
		return msgDesc;
	}
	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}


}
