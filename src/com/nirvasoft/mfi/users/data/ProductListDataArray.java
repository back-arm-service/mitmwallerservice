package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ProductListDataArray {
private ProductListData[] data;
private String msgcode="";
private String msgDesc="";
    public String getMsgcode() {
	return msgcode;
}

public void setMsgcode(String msgcode) {
	this.msgcode = msgcode;
}

public String getMsgDesc() {
	return msgDesc;
}

public void setMsgDesc(String msgDesc) {
	this.msgDesc = msgDesc;
}

	public ProductListDataArray(){
    	super();
    	setData(new ProductListData[1]);    	
    	
    }

	public ProductListData[] getData() {
		return data;
	}

	public void setData(ProductListData[] data) {
		this.data = data;
	}

}
