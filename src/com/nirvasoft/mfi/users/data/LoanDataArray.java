package com.nirvasoft.mfi.users.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LoanDataArray {
	private LoanData[] data;
	private String msgcode = "";
	private String msgDesc = "";

	public String getMsgcode() {
		return msgcode;
	}

	public void setMsgcode(String msgcode) {
		this.msgcode = msgcode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public LoanData[] getData() {
		return data;
	}

	public void setData(LoanData[] data) {
		this.data = data;
	}

}
