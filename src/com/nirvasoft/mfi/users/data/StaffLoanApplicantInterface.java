package com.nirvasoft.mfi.users.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public interface StaffLoanApplicantInterface {

	public void setStaffSyskey (long name);
	public long getStaffSyskey ();
	public void setStaffID (String ID);
	public String getStaffID ();
	public void setStaffName (String name);
	public String getStaffName ();
	public void setStaffNrc (String nrc);
	public String getStaffNrc ();
	public void setCurrentAddressRegion (String region);
	public String getCurrentAddressRegion ();
	public void setCurrentAddressTownship (String township);
	public String getCurrentAddressTownship ();
	public void setCurrentAddressStreet (String street);
	public String getCurrentAddressStreet ();
	public void setCurrentAddressHouseNo (String houseNo);
	public String getCurrentAddressHouseNo ();
	public void setCurrentAddressPostalCode (String postalCode);
	public String getCurrentAddressPostalCode ();	
	public void setMobile (String mobile);
	public String getMobile ();
	public void setCurrentPosition (String currentPosition);
	public String getCurrentPosition ();
	public void setMonthlyBasicIncome (double monthlyBasicIncome);
	public double getMonthlyBasicIncome ();
	public void setDepartment (String department);
	public String getDepartment ();
	public void setPhotoPath (String photoPath);
	public String getPhotoPath ();
	public void setPhotoPath1 (String photoPath1);
	public String getPhotoPath1 ();
	public void setFrontNRCImagePath(String frontNrcImagePath);
	public String getFrontNRCImagePath();
	public void setBackNRCImagePath (String backNrcImagePath);
	public String getBackNRCImagePath();
	
	public void setModifiedDate(String modifiedDate);
	public String getModifiedDate();
	public void setModifiedBy(String modifiedBy);
	public String getModifiedBy();
	public void setStatus(int status);
	public int getStatus();
	
	public void setMsgCode (String code);
	public String getMsgCode ();
	public void setMsgDesc (String desc);
	public String getMsgDesc ();
	
	public void setRegionValue (String region);
	public String getRegionValue ();
	
	public void setTownshipValue (String township);
	public String getTownshipValue();
	
}
