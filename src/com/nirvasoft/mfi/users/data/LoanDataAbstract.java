package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public abstract class LoanDataAbstract {
	protected double loanAmount=0;
	protected String loanAmountInWords="";
	protected String deptChiefRecomm="";
	protected String deptCardIDImage="";
	protected String frontFamilyRegImage="";
	protected String backFamilyRegImage="";
	protected String currentPosition="";
	protected String department="";
	protected double monthlyBasicIncome=0;
	protected double bankInterestRate=0;
	protected double serviceCharges=0;
	protected String loanType="";
	
	protected String nameOfEmployeer;
	protected String office_address_region="";
	protected String office_address_township="";
	protected String office_address_street="";
	protected String office_address_houseno="";
	protected String office_address_postal_code="";
}
