package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public interface LoanTypeArrayInterface {
	public void setData (LoanTypeInterface[] data);
	public LoanTypeInterface[] getData();
	public void setMsgCode (String code);
	public String getMsgCode ();
	public void setMsgDesc (String desc);
	public String getMsgDesc ();
}
