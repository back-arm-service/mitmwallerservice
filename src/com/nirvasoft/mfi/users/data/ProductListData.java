package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ProductListData {
	private long productSyskey;
	private String productCode;
	private String ProductRefId;	
	private String productdescription;	
	private String category;
	private String productunit;
	private String manufacture;
	private String supplier;
	private String model;
	private String serialno;
	private String productImagePath;
	private String productInfoFile;
	private int productstatus;
	private long productPricesyskey;
	private String pricecode;
	private String priceunit;
	private double productprice;
	private long promotionsyskey;
	private String promotioncode;
	private String promotiondescription;
	private String startdate;
	private String enddate;
	private int promotiontype;
	private double promotionamount;
	private double interestRate;
	private int promotionstatus;
	private long outletProductLinksyskey;
	private long outletsyskey;
	private String outletid;
	private int outletstatus;
	private int pricestatus;
	public String getProductdescription() {
		return productdescription;
	}
	public void setProductdescription(String productdescription) {
		this.productdescription = productdescription;
	}
	public String getProductunit() {
		return productunit;
	}
	public void setProductunit(String productunit) {
		this.productunit = productunit;
	}
	public double getProductprice() {
		return productprice;
	}
	public void setProductprice(double productprice) {
		this.productprice = productprice;
	}
	public int getPromotiontype() {
		return promotiontype;
	}
	public void setPromotiontype(int promotiontype) {
		this.promotiontype = promotiontype;
	}
	public long getOutletsyskey() {
		return outletsyskey;
	}
	public void setOutletsyskey(long outletsyskey) {
		this.outletsyskey = outletsyskey;
	}
	public String getOutletid() {
		return outletid;
	}
	public void setOutletid(String outletid) {
		this.outletid = outletid;
	}
	public int getOutletstatus() {
		return outletstatus;
	}
	public void setOutletstatus(int outletstatus) {
		this.outletstatus = outletstatus;
	}
	public int getPricestatus() {
		return pricestatus;
	}
	public void setPricestatus(int pricestatus) {
		this.pricestatus = pricestatus;
	}
	
	public long getProductSyskey() {
		return productSyskey;
	}
	public void setProductSyskey(long productSyskey) {
		this.productSyskey = productSyskey;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
	public String getManufacture() {
		return manufacture;
	}
	public void setManufacture(String manufacture) {
		this.manufacture = manufacture;
	}
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getSerialno() {
		return serialno;
	}
	public void setSerialno(String serialno) {
		this.serialno = serialno;
	}
	public String getProductImagePath() {
		return productImagePath;
	}
	public void setProductImagePath(String productImagePath) {
		this.productImagePath = productImagePath;
	}
	public String getProductInfoFile() {
		return productInfoFile;
	}
	public void setProductInfoFile(String productInfoFile) {
		this.productInfoFile = productInfoFile;
	}
	public int getProductstatus() {
		return productstatus;
	}
	public void setProductstatus(int productstatus) {
		this.productstatus = productstatus;
	}
	public long getProductPricesyskey() {
		return productPricesyskey;
	}
	public void setProductPricesyskey(long productPricesyskey) {
		this.productPricesyskey = productPricesyskey;
	}
	public String getPricecode() {
		return pricecode;
	}
	public void setPricecode(String pricecode) {
		this.pricecode = pricecode;
	}
	public String getPriceunit() {
		return priceunit;
	}
	public void setPriceunit(String priceunit) {
		this.priceunit = priceunit;
	}
	public double getPrice() {
		return productprice;
	}
	public void setPrice(double price) {
		this.productprice = price;
	}
	public long getPromotionsyskey() {
		return promotionsyskey;
	}
	public void setPromotionsyskey(long promotionsyskey) {
		this.promotionsyskey = promotionsyskey;
	}
	public String getPromotioncode() {
		return promotioncode;
	}
	public void setPromotioncode(String promotioncode) {
		this.promotioncode = promotioncode;
	}
	public String getPromotiondescription() {
		return promotiondescription;
	}
	public void setPromotiondescription(String promotiondescription) {
		this.promotiondescription = promotiondescription;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	
	public double getPromotionamount() {
		return promotionamount;
	}
	public void setPromotionamount(double promotionamount) {
		this.promotionamount = promotionamount;
	}
	public double getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}
	public int getPromotionstatus() {
		return promotionstatus;
	}
	public void setPromotionstatus(int promotionstatus) {
		this.promotionstatus = promotionstatus;
	}
	public long getOutletProductLinksyskey() {
		return outletProductLinksyskey;
	}
	public void setOutletProductLinksyskey(long outletProductLinksyskey) {
		this.outletProductLinksyskey = outletProductLinksyskey;
	}
	public String getProductRefId() {
		return ProductRefId;
	}
	public void setProductRefId(String productRefId) {
		ProductRefId = productRefId;
	}
	
}
