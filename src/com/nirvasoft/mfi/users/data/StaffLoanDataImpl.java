package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;
import com.nirvasoft.mfi.users.data.LoanData;

@XmlRootElement
public class StaffLoanDataImpl extends LoanDataAbstract implements StaffLoanDataInterface{


	
	
	LoanData loandata=new LoanData();
	@Override
	public void setLoanAmount(double loanAmt) {
		this.loanAmount=loanAmt;
	}

	@Override
	public double getLoanAmount() {
		return this.loanAmount;
	}

	@Override
	public void setLoanAmountInWords(String loanAmtInWords) {
		this.loanAmountInWords=loanAmtInWords;
	}

	@Override
	public String getLoanAmountInWords() {
		return this.loanAmountInWords;
	}

	@Override
	public void setTotalAmount(double totalAmount) {
		loandata.setTotalAmount(totalAmount);
	}

	@Override
	public double getTotalAmount() {
		return loandata.getTotalAmount();
	}

	@Override
	public void setMonthlyAmount(double monthlyAmount) {
		loandata.setMonthlyAmount(monthlyAmount);
	}

	@Override
	public double getMonthlyAmount() {
		return loandata.getMonthlyAmount();
	}

	@Override
	public void setDepartmentChiefRecommendation(String recommendation) {
		this.deptChiefRecomm=recommendation;
	}

	@Override
	public String getDepartmentChiefRecommendation() {
		return this.deptChiefRecomm;
	}

	@Override
	public void setStaffIDCardImagePath(String idCard) {
		this.deptCardIDImage=idCard;
	}

	@Override
	public String getStaffIDCardImagePath() {
		return this.deptCardIDImage;
	}

	@Override
	public void setCurrentPosition(String currentPosition) {
		this.currentPosition=currentPosition;
	}

	@Override
	public String getCurrentPosition() {
		return this.currentPosition;
	}

	@Override
	public void setMonthlyBasicIncome(double monthlyBasicIncome) {
		this.monthlyBasicIncome=monthlyBasicIncome;
	}

	@Override
	public double getMonthlyBasicIncome() {
		return this.monthlyBasicIncome;
	}

	@Override
	public void setDepartment(String department) {
		this.department=department;
	}

	@Override
	public String getDepartment() {
		return this.department;
	}

	@Override
	public void setMsgCode(String code) {
		loandata.setMsgcode(code);
	}

	@Override
	public String getMsgCode() {
		return loandata.getMsgcode();
	}

	@Override
	public void setMsgDesc(String desc) {
		loandata.setMsgDesc(desc);
	}

	@Override
	public String getMsgDesc() {
		return loandata.getMsgDesc();
	}

	@Override
	public void setLoanSyskey(long syskey) {
		loandata.setLoanSyskey(syskey);
	}

	@Override
	public long getLoanSyskey() {
		return loandata.getLoanSyskey();
	}

	@Override
	public void setApplicantSyskey(long syskey) {
		loandata.setMemberSyskey(syskey);
	}

	@Override
	public long getApplicantSyskey() {
		return loandata.getMemberSyskey();
	}


	@Override
	public void setFrontFamilyRegImagePath(String frontFamilyReg) {
		this.frontFamilyRegImage=frontFamilyReg;
	}

	@Override
	public String getFrontFamilyRegImagePath() {
		return this.frontFamilyRegImage;
	}

	@Override
	public void setBackFamilyRegImagePath(String backFamilyReg) {
		this.backFamilyRegImage=backFamilyReg;
	}

	@Override
	public String getBackFamilyRegImagePath() {
		// TODO Auto-generated method stub
		return this.backFamilyRegImage;
	}

	@Override
	public void setLoanTermSyskey(long syskey) {
		// TODO Auto-generated method stub
		loandata.setPaymentTermSyskey(syskey);
	}

	@Override
	public long getLoanTermSyskey() {
		// TODO Auto-generated method stub
		return loandata.getPaymentTermSyskey();
	}

	@Override
	public void setLoanTerm(String term) {
		// TODO Auto-generated method stub
		loandata.setPaymentTerm(term);
	}

	@Override
	public String getLoanTerm() {
		// TODO Auto-generated method stub
		return loandata.getPaymentTerm();
	}

	@Override
	public void setBankInterestRate(double bankInterestRate) {
		// TODO Auto-generated method stub
		this.bankInterestRate=bankInterestRate;
	}

	@Override
	public double getBankInterestRate() {
		// TODO Auto-generated method stub
		return this.bankInterestRate;
	}

	@Override
	public void setServiceCharges(double serviceCharges) {
		// TODO Auto-generated method stub
		this.serviceCharges=serviceCharges;
	}

	@Override
	public double getServiceCharges() {
		// TODO Auto-generated method stub
		return this.serviceCharges;
	}

	@Override
	public void setLoanID(String ID) {
		// TODO Auto-generated method stub
		loandata.setLoanid(ID);
	}

	@Override
	public String getLoanID() {
		// TODO Auto-generated method stub
		return loandata.getLoanid();
	}

	@Override
	public void setRegistrationDate(String regDate) {
		// TODO Auto-generated method stub
		loandata.setRegiatrationDate(regDate);
	}

	@Override
	public String getRegistrationDate() {
		// TODO Auto-generated method stub
		return loandata.getRegiatrationDate();
	}

	@Override
	public void setModifiedDate(String modifiedDate) {
		// TODO Auto-generated method stub
		loandata.setModifiedDate(modifiedDate);
	}

	@Override
	public String getModifiedDate() {
		// TODO Auto-generated method stub
		return loandata.getModifiedDate();
	}

	@Override
	public void setModifiedBy(String modifiedBy) {
		// TODO Auto-generated method stub
		loandata.setModifiedBy(modifiedBy);
	}

	@Override
	public String getModifiedBy() {
		// TODO Auto-generated method stub
		return loandata.getModifiedBy();
	}

	@Override
	public void setStatus(int status) {
		// TODO Auto-generated method stub
		loandata.setStatus(status);
	}

	@Override
	public int getStatus() {
		// TODO Auto-generated method stub
		return loandata.getStatus();
	}

	@Override
	public void setAppliedDate(String appliedDate) {
		// TODO Auto-generated method stub
		loandata.setLoanAppliedDate(appliedDate);
	}

	@Override
	public String getAppliedDate() {
		// TODO Auto-generated method stub
		return loandata.getLoanAppliedDate();
	}

	@Override
	public void setLoanType(String loanType) {
		// TODO Auto-generated method stub
		this.loanType=loanType;
	}

	@Override
	public String getLoanType() {
		// TODO Auto-generated method stub
		return this.loanType;
	}

	@Override
	public void setAppSignImagePath(String appSignImagePath) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getAppSignImagePath() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setPaymentTermSyskey(String paymentTermSyskey) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void getPaymentTermSyskey() {
		// TODO Auto-generated method stub
		
	}





}
