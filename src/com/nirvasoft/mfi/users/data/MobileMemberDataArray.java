package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MobileMemberDataArray {
	private MobileMemberData[] data;
	private String msgcode="";
	private String msgDesc="";

	public String getMsgcode() {
		return msgcode;
	}

	public void setMsgcode(String msgcode) {
		this.msgcode = msgcode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public MobileMemberData[] getData() {
		return data;
	}

	public void setData(MobileMemberData[] data) {
		this.data = data;
	}

}
