package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;


import java.util.ArrayList;
@XmlRootElement

public class PaymentInstallmentDataList {
	private ArrayList<PaymentInstallmentData> data;
	private String searchText;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private String msgCode = "";
	private String msgDesc = "";
	public ArrayList<PaymentInstallmentData> getData() {
		return data;
	}

	public void setData(ArrayList<PaymentInstallmentData> data) {
		this.data = data;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}
}
