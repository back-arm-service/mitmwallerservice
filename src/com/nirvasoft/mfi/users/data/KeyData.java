package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class KeyData {
	private long keyTableSysKey;
	private long fK_MFIOrgSysKey;
	private String objectName;
	private String prefix;
	private int noOfDigit;
	private String srfix;
	private int sequenceNo;
	private int keyDataType;
	private String registrationDate;
	private String modifiedDate;
	private String modifiedby;
	private int status;
	public long getKeyTableSysKey() {
		return keyTableSysKey;
	}
	public void setKeyTableSysKey(long keyTableSysKey) {
		this.keyTableSysKey = keyTableSysKey;
	}
	public long getfK_MFIOrgSysKey() {
		return fK_MFIOrgSysKey;
	}
	public void setfK_MFIOrgSysKey(long fK_MFIOrgSysKey) {
		this.fK_MFIOrgSysKey = fK_MFIOrgSysKey;
	}
	public String getObjectName() {
		return objectName;
	}
	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public int getNoOfDigit() {
		return noOfDigit;
	}
	public void setNoOfDigit(int noOfDigit) {
		this.noOfDigit = noOfDigit;
	}
	public String getSrfix() {
		return srfix;
	}
	public void setSrfix(String srfix) {
		this.srfix = srfix;
	}
	public int getSequenceNo() {
		return sequenceNo;
	}
	public void setSequenceNo(int sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	public int getKeyDataType() {
		return keyDataType;
	}
	public void setKeyDataType(int keyDataType) {
		this.keyDataType = keyDataType;
	}
	public String getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}
	public String getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getModifiedby() {
		return modifiedby;
	}
	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}
