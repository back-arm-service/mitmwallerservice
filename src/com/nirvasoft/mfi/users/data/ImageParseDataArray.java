package com.nirvasoft.mfi.users.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ImageParseDataArray {
	private ImageParseData[] imageparsedata;
	private String msgcode = "";
	private String msgDesc = "";

	public String getMsgcode() {
		return msgcode;
	}

	public void setMsgcode(String msgcode) {
		this.msgcode = msgcode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public ImageParseData[] getImageparsedata() {
		return imageparsedata;
	}

	public void setImageparsedata(ImageParseData[] imageparsedata) {
		this.imageparsedata = imageparsedata;
	}

	

}
