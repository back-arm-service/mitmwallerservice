package com.nirvasoft.mfi.users.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OrgMemberLinkData {
	private long syskey=-1;
	private long fk_memberid=-1;
	private long fk_orgid=-1;
	
	private String registration_date="";
	private String modified_date="";
	private String modifiedby="";	
	private int status=-1;
	private String msgCode = "";
	private String msgDesc = "";
	public long getSyskey() {
		return syskey;
	}
	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}
	
	public String getModified_date() {
		return modified_date;
	}
	public void setModified_date(String modified_date) {
		this.modified_date = modified_date;
	}
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getRegistration_date() {
		return registration_date;
	}
	public void setRegistration_date(String registration_date) {
		this.registration_date = registration_date;
	}
	public String getModifiedby() {
		return modifiedby;
	}
	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}
	
	public String getMsgCode() {
		return msgCode;
	}
	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}
	public String getMsgDesc() {
		return msgDesc;
	}
	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}
	public long getFk_memberid() {
		return fk_memberid;
	}
	public void setFk_memberid(long fk_memberid) {
		this.fk_memberid = fk_memberid;
	}
	public long getFk_orgid() {
		return fk_orgid;
	}
	public void setFk_orgid(long fk_orgid) {
		this.fk_orgid = fk_orgid;
	}
	
}