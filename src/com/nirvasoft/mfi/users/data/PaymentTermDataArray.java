package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PaymentTermDataArray {
	private PaymentTermData[] data;
	private String msgcode="";
	private String msgDesc="";
	    public String getMsgcode() {
		return msgcode;
	}

	public void setMsgcode(String msgcode) {
		this.msgcode = msgcode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

		public PaymentTermDataArray(){
	    	super();
	    	setData(new PaymentTermData[1]);    	
	    	
	    }

		public PaymentTermData[] getData() {
			return data;
		}

		public void setData(PaymentTermData[] data) {
			this.data = data;
		}

}
