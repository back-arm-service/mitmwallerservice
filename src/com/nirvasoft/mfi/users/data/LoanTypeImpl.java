package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LoanTypeImpl implements LoanTypeInterface{
	ProductListData pdata=new ProductListData();
	
	private int loanType=0;
	private int maxLoanTermInYear=0;
	private boolean selected=false;
	@Override
	public void setLoanTypeSyskey(long syskey) {
		// TODO Auto-generated method stub
		pdata.setProductSyskey(syskey);
	}

	@Override
	public long getLoanTypeSyskey() {
		// TODO Auto-generated method stub
		return pdata.getProductSyskey();
	}

	@Override
	public void setLoanTypeCode(String code) {
		// TODO Auto-generated method stub
		pdata.setProductCode(code);
	}

	@Override
	public String getLoanTypeCode() {
		// TODO Auto-generated method stub
		return pdata.getProductCode();
	}



	@Override
	public void setLoanType(int ltype) {
		// TODO Auto-generated method stub
		this.loanType=ltype;
	}

	@Override
	public int getLoanType() {
		// TODO Auto-generated method stub
		return this.loanType;
	}

	@Override
	public void setLoanTypeDesc(String desc) {
		// TODO Auto-generated method stub
		pdata.setProductdescription(desc);
	}

	@Override
	public String getLoanTypeDesc() {
		// TODO Auto-generated method stub
		return pdata.getProductdescription();
	}

	@Override
	public void setMaxLoanTermInYear(int maxterm) {
		// TODO Auto-generated method stub
		this.maxLoanTermInYear=maxterm;
	}

	@Override
	public int getMaxLoanTermInYear() {
		// TODO Auto-generated method stub
		return this.maxLoanTermInYear;
	}

	@Override
	public void setOutletLoanTypeLinkSyskey(long outLoanSyskey) {
		// TODO Auto-generated method stub
		pdata.setOutletProductLinksyskey(outLoanSyskey);
	}

	@Override
	public long getOutletLoanTypeLinkSyskey() {
		// TODO Auto-generated method stub
		return pdata.getOutletProductLinksyskey();
	}

	@Override
	public void setSelected(boolean select) {
		// TODO Auto-generated method stub
		this.selected=select;
	}

	@Override
	public boolean getSelected() {
		// TODO Auto-generated method stub
		return this.selected;
	}

}
