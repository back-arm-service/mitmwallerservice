package com.nirvasoft.mfi.users.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public interface StaffLoanPaymentInstallmentInterface {
	public int getBatchNo();
	public void setBatchNo(int batchNo) ;
	public String getSerialNo();
	public void setSerialNo(String serialNo);
	public String getRepayDate();
	public void setRepayDate(String repayDate);
	public double getRepayAmount() ;
	public void setRepayAmount(double repayAmount);
	public double getInterestAmount() ;
	public void setInterestAmount(double interestAmount);
	public double getPricipleAmount();
	public void setPricipleAmount(double pricipleAmount);
	public double getMonthlyRepayment();
	public void setMonthlyRepayment(double monthlyRepayment);
	public int getStatus() ;
	public void setStatus(int status);
	public long getTransRef();
	public void setTransRef(long transRef);
	public int getMonth();
	public void setMonth(int month);
	public double getClosingBalance();
	public void setClosingBalance(double closingBalance);
}
