package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TownshipData {
	private long townshipRefSyskey;
	private String townshipId;
	private long regionRefSyskey;
	private String Description;
	private String registrationDate;
	private String modifiedDate;
	private String modifiedBy;
	private int status;
	private String regionCode;
	public long getRegionRefSyskey() {
		return regionRefSyskey;
	}
	public void setRegionRefSyskey(long regionRefSyskey) {
		this.regionRefSyskey = regionRefSyskey;
	}

	public String getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}
	public String getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public long getTownshipRefSyskey() {
		return townshipRefSyskey;
	}
	public void setTownshipRefSyskey(long townshipRefSyskey) {
		this.townshipRefSyskey = townshipRefSyskey;
	}
	public String getTownshipId() {
		return townshipId;
	}
	public void setTownshipId(String townshipId) {
		this.townshipId = townshipId;
	}
	public String getRegionCode() {
		return regionCode;
	}
	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}

}
