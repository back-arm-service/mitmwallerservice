package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RegionDataArray {
	 private RegionData[] data;
		private String msgcode="";
		private String msgDesc="";
	    public String getMsgcode() {
			return msgcode;
		}

		public void setMsgcode(String msgcode) {
			this.msgcode = msgcode;
		}

		public String getMsgDesc() {
			return msgDesc;
		}

		public void setMsgDesc(String msgDesc) {
			this.msgDesc = msgDesc;
		}

		public RegionDataArray(){
	    	super();
	    	setData(new RegionData[1]);    	
	    	
	    }

		public RegionData[] getData() {
			return data;
		}

		public void setData(RegionData[] data) {
			this.data = data;
		}
}
