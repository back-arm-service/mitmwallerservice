package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LoanData {
	private long loanSyskey=0;
	private String loanid="";

	private long memberSyskey=0;
	private long GuarantorSyskey=0;
	private long paymentTermSyskey=0;
	private int registrationType=0;
	private String loanAppliedDate="";
	private String loanConfirmDate="";
	private String confirmedBy="";
	private String paymentTerm="";
	private double monthlyAmount;
	private String householdRegistraionFileLocation="";
	private String householdRegistrationImageFile="";
	private String policeCertificateFIleLocation="";
	private String employmentLetterFileLocation="";
	private String regiatrationDate="";
	private String modifiedDate="";
	private String modifiedBy="";
	private int status=0;
	private String msgcode="";
	private String msgDesc="";
	private double totalAmount=0;
	private double netAmount=0;
	private int loanstatusposition;
		
	public int getLoanstatusposition() {
		return loanstatusposition;
	}
	public void setLoanstatusposition(int loanstatusposition) {
		this.loanstatusposition = loanstatusposition;
	}
	public double getNetAmount() {
		return netAmount;
	}
	public void setNetAmount(double netAmount) {
		this.netAmount = netAmount;
	}
	private int promotionamount=0;
	public int getPromotionamount() {
		return promotionamount;
	}
	public void setPromotionamount(int promotionamount) {
		this.promotionamount = promotionamount;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public long getLoanSyskey() {
		return loanSyskey;
	}
	public void setLoanSyskey(long loanSyskey) {
		this.loanSyskey = loanSyskey;
	}
	public String getLoanid() {
		return loanid;
	}
	public void setLoanid(String loanid) {
		this.loanid = loanid;
	}
	
	public long getMemberSyskey() {
		return memberSyskey;
	}
	public void setMemberSyskey(long memberSyskey) {
		this.memberSyskey = memberSyskey;
	}
	public long getGuarantorSyskey() {
		return GuarantorSyskey;
	}
	public void setGuarantorSyskey(long guarantorSyskey) {
		GuarantorSyskey = guarantorSyskey;
	}
	public int getRegistrationType() {
		return registrationType;
	}
	public void setRegistrationType(int registrationType) {
		this.registrationType = registrationType;
	}
	public String getLoanAppliedDate() {
		return loanAppliedDate;
	}
	public void setLoanAppliedDate(String loanAppliedDate) {
		this.loanAppliedDate = loanAppliedDate;
	}
	public String getLoanConfirmDate() {
		return loanConfirmDate;
	}
	public void setLoanConfirmDate(String loanConfirmDate) {
		this.loanConfirmDate = loanConfirmDate;
	}
	public String getConfirmedBy() {
		return confirmedBy;
	}
	public void setConfirmedBy(String confirmedBy) {
		this.confirmedBy = confirmedBy;
	}
	public String getPaymentTerm() {
		return paymentTerm;
	}
	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}
	public double getMonthlyAmount() {
		return monthlyAmount;
	}
	public void setMonthlyAmount(double monthlyAmount) {
		this.monthlyAmount = monthlyAmount;
	}
	public String getHouseholdRegistraionFileLocation() {
		return householdRegistraionFileLocation;
	}
	public void setHouseholdRegistraionFileLocation(String householdRegistraionFileLocation) {
		this.householdRegistraionFileLocation = householdRegistraionFileLocation;
	}
	public String getHouseholdRegistrationImageFile() {
		return householdRegistrationImageFile;
	}
	public void setHouseholdRegistrationImageFile(String householdRegistrationImageFile) {
		this.householdRegistrationImageFile = householdRegistrationImageFile;
	}
	public String getPoliceCertificateFIleLocation() {
		return policeCertificateFIleLocation;
	}
	public void setPoliceCertificateFIleLocation(String policeCertificateFIleLocation) {
		this.policeCertificateFIleLocation = policeCertificateFIleLocation;
	}
	public String getEmploymentLetterFileLocation() {
		return employmentLetterFileLocation;
	}
	public void setEmploymentLetterFileLocation(String employmentLetterFileLocation) {
		this.employmentLetterFileLocation = employmentLetterFileLocation;
	}
	public String getRegiatrationDate() {
		return regiatrationDate;
	}
	public void setRegiatrationDate(String regiatrationDate) {
		this.regiatrationDate = regiatrationDate;
	}
	public String getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public long getPaymentTermSyskey() {
		return paymentTermSyskey;
	}
	public void setPaymentTermSyskey(long paymentTermSyskey) {
		this.paymentTermSyskey = paymentTermSyskey;
	}
	public String getMsgcode() {
		return msgcode;
	}
	public void setMsgcode(String msgcode) {
		this.msgcode = msgcode;
	}
	public String getMsgDesc() {
		return msgDesc;
	}
	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}
	

}
