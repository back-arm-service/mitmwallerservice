package com.nirvasoft.mfi.users.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PaymentInstallmentData {
	private long paymentInstallmentSysKey;
	private String paymentInstallmentId = "";
	private long loansyskey = 0;
	private String loanId="";
	private String paymentTerm = "";
	private int peroid = 0;
	private double monthlypayment=0 ;
	private String paydate = "";
	private int paymenttype =0;
	private String membername = "";
	private String nrc = "";
	private long paymenttermsyskey=0;
	private long membersyskey=0;
	private double monthlyamount=0;	
	private String registration_date = "";
	private String modified_date = "";
	private String modifiedby = "";
	private int status = -1;
	private String msgCode = "";
	private String msgDesc = "";
	private long guarantorsyskey=0;
	private String guarantorname="";
	private String guarantornrc="";
	private String loanAppliedDate="";
	private String sessionId="";
	private String Ref1;
	private String Ref2;
	private double interest;
	private double principal;
	private double closingbalance;
	private double openingbalance;
	private String duedate;
	private double outstandingrepayment;
	
	
	public long getPaymentInstallmentSysKey() {
		return paymentInstallmentSysKey;
	}
	public void setPaymentInstallmentSysKey(long paymentInstallmentSysKey) {
		this.paymentInstallmentSysKey = paymentInstallmentSysKey;
	}
	public String getPaymentInstallmentId() {
		return paymentInstallmentId;
	}
	public void setPaymentInstallmentId(String paymentInstallmentId) {
		this.paymentInstallmentId = paymentInstallmentId;
	}
	public long getLoansyskey() {
		return loansyskey;
	}
	public void setLoansyskey(long loansyskey) {
		this.loansyskey = loansyskey;
	}
	public String getPaymentTerm() {
		return paymentTerm;
	}
	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}
	public int getPeroid() {
		return peroid;
	}
	public void setPeroid(int peroid) {
		this.peroid = peroid;
	}
	public double getMonthlypayment() {
		return monthlypayment;
	}
	public void setMonthlypayment(double monthlypayment) {
		this.monthlypayment = monthlypayment;
	}
	public String getPaydate() {
		return paydate;
	}
	public void setPaydate(String paydate) {
		this.paydate = paydate;
	}
	public int getPaymenttype() {
		return paymenttype;
	}
	public void setPaymenttype(int paymenttype) {
		this.paymenttype = paymenttype;
	}
	public String getMembername() {
		return membername;
	}
	public void setMembername(String membername) {
		this.membername = membername;
	}
	public String getNrc() {
		return nrc;
	}
	public void setNrc(String nrc) {
		this.nrc = nrc;
	}
	public long getPaymenttermsyskey() {
		return paymenttermsyskey;
	}
	public void setPaymenttermsyskey(long paymenttermsyskey) {
		this.paymenttermsyskey = paymenttermsyskey;
	}
	public long getMembersyskey() {
		return membersyskey;
	}
	public void setMembersyskey(long membersyskey) {
		this.membersyskey = membersyskey;
	}
	public String getRegistration_date() {
		return registration_date;
	}
	public void setRegistration_date(String registration_date) {
		this.registration_date = registration_date;
	}
	public String getModified_date() {
		return modified_date;
	}
	public void setModified_date(String modified_date) {
		this.modified_date = modified_date;
	}
	public String getModifiedby() {
		return modifiedby;
	}
	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getMsgCode() {
		return msgCode;
	}
	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}
	public String getMsgDesc() {
		return msgDesc;
	}
	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}
	public String getLoanId() {
		return loanId;
	}
	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}
	public double getMonthlyamount() {
		return monthlyamount;
	}
	public void setMonthlyamount(double monthlyamount) {
		this.monthlyamount = monthlyamount;
	}
	public long getGuarantorsyskey() {
		return guarantorsyskey;
	}
	public void setGuarantorsyskey(long guarantorsyskey) {
		this.guarantorsyskey = guarantorsyskey;
	}
	public String getGuarantorname() {
		return guarantorname;
	}
	public void setGuarantorname(String guarantorname) {
		this.guarantorname = guarantorname;
	}
	public String getGuarantornrc() {
		return guarantornrc;
	}
	public void setGuarantornrc(String guarantornrc) {
		this.guarantornrc = guarantornrc;
	}
	public String getLoanAppliedDate() {
		return loanAppliedDate;
	}
	public void setLoanAppliedDate(String loanAppliedDate) {
		this.loanAppliedDate = loanAppliedDate;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getRef1() {
		return Ref1;
	}
	public void setRef1(String ref1) {
		Ref1 = ref1;
	}
	public String getRef2() {
		return Ref2;
	}
	public void setRef2(String ref2) {
		Ref2 = ref2;
	}
	public double getInterest() {
		return interest;
	}
	public void setInterest(double interest) {
		this.interest = interest;
	}
	public double getPrincipal() {
		return principal;
	}
	public void setPrincipal(double principal) {
		this.principal = principal;
	}
	public double getClosingbalance() {
		return closingbalance;
	}
	public void setClosingbalance(double closingbalance) {
		this.closingbalance = closingbalance;
	}

	public String getDuedate() {
		return duedate;
	}
	public void setDuedate(String duedate) {
		this.duedate = duedate;
	}

	public double getOutstandingrepayment() {
		return outstandingrepayment;
	}
	public void setOutstandingrepayment(double outstandingrepayment) {
		this.outstandingrepayment = outstandingrepayment;
	}
	public double getOpeningbalance() {
		return openingbalance;
	}
	public void setOpeningbalance(double openingbalance) {
		this.openingbalance = openingbalance;
	}


}