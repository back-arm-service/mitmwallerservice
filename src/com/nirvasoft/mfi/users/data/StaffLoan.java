package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

import com.nirvasoft.mfi.users.data.ImageParseDataArray;

@XmlRootElement
public class StaffLoan implements StaffLoanInterface{
	
	private StaffLoanApplicantInterface applicant=new StaffLoanApplicantImpl(); 
	private  StaffLoanGuarantorInterface guarantor1=new StaffLoanGuarantorImpl();
	private  StaffLoanGuarantorInterface guarantor2=new StaffLoanGuarantorImpl();
	private  StaffLoanDataInterface loandata=new StaffLoanDataImpl();
	private  StaffLoanAppliedLoanListInterface appliedloandata=new StaffLoanAppliedLoanListImpl();
	private ImageParseDataArray imagealldata;
	
	private String msgcode="";
	private String msgDesc="";
	@Override
	public void setStaffLoanApplicant(StaffLoanApplicantImpl applicant) {
		// TODO Auto-generated method stub
		this.applicant=applicant;
		
	}
	@Override
	public StaffLoanApplicantInterface getStaffLoanApplicant() {
		// TODO Auto-generated method stub
		return this.applicant;
	}
	@Override
	public void setStaffLoanGuarantor1(StaffLoanGuarantorImpl guarantor) {
		// TODO Auto-generated method stub
		this.guarantor1=guarantor;
	}
	@Override
	public StaffLoanGuarantorInterface getStaffLoanGuarantor1() {
		// TODO Auto-generated method stub
		return this.guarantor1;
	}
	@Override
	public void setStaffLoanGuarantor2(StaffLoanGuarantorImpl guarantor) {
		// TODO Auto-generated method stub
		this.guarantor2=guarantor;
	}
	@Override
	public StaffLoanGuarantorInterface getStaffLoanGuarantor2() {
		// TODO Auto-generated method stub
		return this.guarantor2;
	}
	@Override
	public void setStaffLoanData(StaffLoanDataImpl loan) {
		// TODO Auto-generated method stub
		this.loandata=loan;
	}
	@Override
	public StaffLoanDataInterface getStaffLoanData() {
		// TODO Auto-generated method stub
		return this.loandata;
	}
	@Override
	public void setStaffAppliedLoanData(StaffLoanAppliedLoanListImpl appliedloan) {
		// TODO Auto-generated method stub
		this.appliedloandata=appliedloan;
	}
	@Override
	public StaffLoanAppliedLoanListInterface getStaffAppliedLoanData() {
		// TODO Auto-generated method stub
		return this.appliedloandata;
	}
	@Override
	public void setMsgCode(String code) {
		// TODO Auto-generated method stub
		this.msgcode=code;
	}
	@Override
	public String getMsgCode() {
		// TODO Auto-generated method stub
		return this.msgcode;
	}
	@Override
	public void setMsgDesc(String desc) {
		// TODO Auto-generated method stub
		this.msgDesc=desc;
	}
	@Override
	public String getMsgDesc() {
		// TODO Auto-generated method stub
		return this.msgDesc;
	}
	@Override
	public ImageParseDataArray getImagealldata() {
		// TODO Auto-generated method stub
		return this.imagealldata;
	}
	@Override
	public void setImagealldata(ImageParseDataArray imagealldata) {
		// TODO Auto-generated method stub
		this.imagealldata=imagealldata;
	}




}
