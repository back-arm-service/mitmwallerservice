package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class StaffLoanApplicantImpl extends PersonAbstract implements StaffLoanApplicantInterface{
	MobileMemberData memberdata=new MobileMemberData();

	
	@Override
	public void setStaffName(String name) {
		memberdata.setMember_name(name);	
	}

	@Override
	public String getStaffName() {		
		return memberdata.getMember_name();
	}

	@Override
	public void setStaffNrc(String nrc) {
		memberdata.setNrc(nrc);
		
	}

	@Override
	public String getStaffNrc() {		
		return memberdata.getNrc();
	}

	@Override
	public void setCurrentAddressRegion(String region) {
		memberdata.setCurrent_address_region(region);		
	}

	@Override
	public String getCurrentAddressRegion() {		
		return memberdata.getCurrent_address_region();
	}

	@Override
	public void setCurrentAddressTownship(String township) {
		memberdata.setCurrent_address_township(township);
		
	}

	@Override
	public String getCurrentAddressTownship() {		
		return memberdata.getCurrent_address_township();
	}

	@Override
	public void setCurrentAddressStreet(String street) {
		memberdata.setCurrent_address_street(street);
		
	}

	@Override
	public String getCurrentAddressStreet() {		
		return memberdata.getCurrent_address_street();
	}

	@Override
	public void setCurrentAddressHouseNo(String houseNo) {
		memberdata.setCurrent_address_houseno(houseNo);		
	}

	@Override
	public String getCurrentAddressHouseNo() {		
		return memberdata.getCurrent_address_houseno();
	}

	@Override
	public void setCurrentAddressPostalCode(String postalCode) {
		memberdata.setCurrent_address_postal_code(postalCode);
		
	}

	@Override
	public String getCurrentAddressPostalCode() {		
		return memberdata.getCurrent_address_postal_code();
	}

	@Override
	public void setMobile(String mobile) {
		memberdata.setMobile(mobile);
	}

	@Override
	public String getMobile() {		
		return memberdata.getMobile();
	}

	@Override
	public void setCurrentPosition(String currentPosition) {
		memberdata.setCurrent_position(currentPosition);
	}

	@Override
	public String getCurrentPosition() {		
		return memberdata.getCurrent_position();
	}

	@Override
	public void setMonthlyBasicIncome(double monthlyBasicIncome) {
		memberdata.setMonthly_basic_income(monthlyBasicIncome);
	}

	@Override
	public double getMonthlyBasicIncome() {		
		return memberdata.getMonthly_basic_income();
	}

	@Override
	public void setDepartment(String department) {
		memberdata.setDepartment(department);
	}

	@Override
	public String getDepartment() {
		return memberdata.getDepartment();
	}

	@Override
	public void setPhotoPath(String photoPath) {
		this.photopath=photoPath;
	}

	@Override
	public String getPhotoPath() {
		return this.photopath;
	}

	@Override
	public void setFrontNRCImagePath(String frontNrcImagePath) {
		memberdata.setFront_nrc_image_path(frontNrcImagePath);
	}

	@Override
	public String getFrontNRCImagePath() {
		return memberdata.getFront_nrc_image_path();
	}

	@Override
	public void setBackNRCImagePath(String backNrcImagePath) {
		memberdata.setBack_nrc_image_path(backNrcImagePath);
	}

	@Override
	public String getBackNRCImagePath() {
		return memberdata.getBack_nrc_image_path();
	}

	@Override
	public void setMsgCode(String code) {
		memberdata.setMsgCode(code);
	}

	@Override
	public String getMsgCode() {
		return memberdata.getMsgCode();
	}

	@Override
	public void setMsgDesc(String desc) {
		memberdata.setMsgDesc(desc);
	}

	@Override
	public String getMsgDesc() {
		return memberdata.getMsgDesc();
	}

	@Override
	public void setStaffSyskey(long syskey) {
		memberdata.setSyskey(syskey);
	}

	@Override
	public long getStaffSyskey() {
		return memberdata.getSyskey();
	}

	@Override
	public void setStaffID(String ID) {
		memberdata.setMember_id(ID);
	}

	@Override
	public String getStaffID() {
		return memberdata.getMember_id();
	}

	@Override
	public void setModifiedDate(String modifiedDate) {
		// TODO Auto-generated method stub
		memberdata.setModified_date(modifiedDate);
	}

	@Override
	public String getModifiedDate() {
		// TODO Auto-generated method stub
		return memberdata.getModified_date();
	}

	@Override
	public void setStatus(int status) {
		// TODO Auto-generated method stub
		memberdata.setStatus(status);
	}

	@Override
	public int getStatus() {
		// TODO Auto-generated method stub
		return memberdata.getStatus();
	}

	@Override
	public void setPhotoPath1(String photoPath1) {
		// TODO Auto-generated method stub
		this.photopath1=photoPath1;
	}

	@Override
	public String getPhotoPath1() {
		// TODO Auto-generated method stub
		return this.photopath1;
	}

	@Override
	public void setModifiedBy(String modifiedBy) {
		// TODO Auto-generated method stub
		memberdata.setModifiedby(modifiedBy);
	}

	@Override
	public String getModifiedBy() {
		// TODO Auto-generated method stub
		return memberdata.getModifiedby();
	}

	@Override
	public void setRegionValue(String region) {
		// TODO Auto-generated method stub
		this.regvalue=region;
	}

	@Override
	public String getRegionValue() {
		// TODO Auto-generated method stub
		return this.regvalue;
	}

	@Override
	public void setTownshipValue(String township) {
		// TODO Auto-generated method stub
		this.townvalue=township;
	}

	@Override
	public String getTownshipValue() {
		// TODO Auto-generated method stub
		return this.townvalue;
	}


}
