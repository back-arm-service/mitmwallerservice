package com.nirvasoft.mfi.users.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LoginHistoryDataArray {
	private LoginHistoryData[] data;
	private String msgcode = "";
	private String msgDesc = "";

	public String getMsgcode() {
		return msgcode;
	}

	public void setMsgcode(String msgcode) {
		this.msgcode = msgcode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public LoginHistoryData[] getData() {
		return data;
	}

	public void setData(LoginHistoryData[] data) {
		this.data = data;
	}

}
