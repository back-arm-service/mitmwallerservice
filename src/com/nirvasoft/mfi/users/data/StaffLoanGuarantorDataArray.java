package com.nirvasoft.mfi.users.data;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class StaffLoanGuarantorDataArray {
	private StaffLoanGuarantorInterface[] data;
	private String msgcode="";
	private String msgDesc="";

	public String getMsgcode() {
		return msgcode;
	}

	public void setMsgcode(String msgcode) {
		this.msgcode = msgcode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public StaffLoanGuarantorInterface[] getData() {
		return data;
	}

	public void setData(StaffLoanGuarantorInterface[] data) {
		this.data = data;
	}

}
