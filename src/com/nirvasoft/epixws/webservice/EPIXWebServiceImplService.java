/**
 * EPIXWebServiceImplService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.epixws.webservice;

public interface EPIXWebServiceImplService extends javax.xml.rpc.Service {
    public java.lang.String getEPIXWebServiceImplPortAddress();

    public com.nirvasoft.epixws.webservice.EPIXWebService getEPIXWebServiceImplPort() throws javax.xml.rpc.ServiceException;

    public com.nirvasoft.epixws.webservice.EPIXWebService getEPIXWebServiceImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
