/**
 * GlInfoResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.epixws.webservice;

public class GlInfoResult  implements java.io.Serializable {
    private java.lang.String accNo;

    private java.lang.String code;

    private java.lang.String desc;

    private com.nirvasoft.epixws.webservice.GlInfo[] GLInfoArr;

    public GlInfoResult() {
    }

    public GlInfoResult(
           java.lang.String accNo,
           java.lang.String code,
           java.lang.String desc,
           com.nirvasoft.epixws.webservice.GlInfo[] GLInfoArr) {
           this.accNo = accNo;
           this.code = code;
           this.desc = desc;
           this.GLInfoArr = GLInfoArr;
    }


    /**
     * Gets the accNo value for this GlInfoResult.
     * 
     * @return accNo
     */
    public java.lang.String getAccNo() {
        return accNo;
    }


    /**
     * Sets the accNo value for this GlInfoResult.
     * 
     * @param accNo
     */
    public void setAccNo(java.lang.String accNo) {
        this.accNo = accNo;
    }


    /**
     * Gets the code value for this GlInfoResult.
     * 
     * @return code
     */
    public java.lang.String getCode() {
        return code;
    }


    /**
     * Sets the code value for this GlInfoResult.
     * 
     * @param code
     */
    public void setCode(java.lang.String code) {
        this.code = code;
    }


    /**
     * Gets the desc value for this GlInfoResult.
     * 
     * @return desc
     */
    public java.lang.String getDesc() {
        return desc;
    }


    /**
     * Sets the desc value for this GlInfoResult.
     * 
     * @param desc
     */
    public void setDesc(java.lang.String desc) {
        this.desc = desc;
    }


    /**
     * Gets the GLInfoArr value for this GlInfoResult.
     * 
     * @return GLInfoArr
     */
    public com.nirvasoft.epixws.webservice.GlInfo[] getGLInfoArr() {
        return GLInfoArr;
    }


    /**
     * Sets the GLInfoArr value for this GlInfoResult.
     * 
     * @param GLInfoArr
     */
    public void setGLInfoArr(com.nirvasoft.epixws.webservice.GlInfo[] GLInfoArr) {
        this.GLInfoArr = GLInfoArr;
    }

    public com.nirvasoft.epixws.webservice.GlInfo getGLInfoArr(int i) {
        return this.GLInfoArr[i];
    }

    public void setGLInfoArr(int i, com.nirvasoft.epixws.webservice.GlInfo _value) {
        this.GLInfoArr[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GlInfoResult)) return false;
        GlInfoResult other = (GlInfoResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.accNo==null && other.getAccNo()==null) || 
             (this.accNo!=null &&
              this.accNo.equals(other.getAccNo()))) &&
            ((this.code==null && other.getCode()==null) || 
             (this.code!=null &&
              this.code.equals(other.getCode()))) &&
            ((this.desc==null && other.getDesc()==null) || 
             (this.desc!=null &&
              this.desc.equals(other.getDesc()))) &&
            ((this.GLInfoArr==null && other.getGLInfoArr()==null) || 
             (this.GLInfoArr!=null &&
              java.util.Arrays.equals(this.GLInfoArr, other.getGLInfoArr())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAccNo() != null) {
            _hashCode += getAccNo().hashCode();
        }
        if (getCode() != null) {
            _hashCode += getCode().hashCode();
        }
        if (getDesc() != null) {
            _hashCode += getDesc().hashCode();
        }
        if (getGLInfoArr() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGLInfoArr());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGLInfoArr(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GlInfoResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "glInfoResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accNo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AccNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("desc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Desc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("GLInfoArr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "GLInfoArr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "glInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
