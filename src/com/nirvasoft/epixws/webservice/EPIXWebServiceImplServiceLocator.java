/**
 * EPIXWebServiceImplServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.epixws.webservice;

public class EPIXWebServiceImplServiceLocator extends org.apache.axis.client.Service implements com.nirvasoft.epixws.webservice.EPIXWebServiceImplService {

    public EPIXWebServiceImplServiceLocator() {
    }


    public EPIXWebServiceImplServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public EPIXWebServiceImplServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for EPIXWebServiceImplPort
    private java.lang.String EPIXWebServiceImplPort_address = "http://localhost:8081/EPIXWebService/epixwebservice";

    public java.lang.String getEPIXWebServiceImplPortAddress() {
        return EPIXWebServiceImplPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String EPIXWebServiceImplPortWSDDServiceName = "EPIXWebServiceImplPort";

    public java.lang.String getEPIXWebServiceImplPortWSDDServiceName() {
        return EPIXWebServiceImplPortWSDDServiceName;
    }

    public void setEPIXWebServiceImplPortWSDDServiceName(java.lang.String name) {
        EPIXWebServiceImplPortWSDDServiceName = name;
    }

    public com.nirvasoft.epixws.webservice.EPIXWebService getEPIXWebServiceImplPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(EPIXWebServiceImplPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getEPIXWebServiceImplPort(endpoint);
    }

    public com.nirvasoft.epixws.webservice.EPIXWebService getEPIXWebServiceImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.nirvasoft.epixws.webservice.EPIXWebServiceImplPortBindingStub _stub = new com.nirvasoft.epixws.webservice.EPIXWebServiceImplPortBindingStub(portAddress, this);
            _stub.setPortName(getEPIXWebServiceImplPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setEPIXWebServiceImplPortEndpointAddress(java.lang.String address) {
        EPIXWebServiceImplPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.nirvasoft.epixws.webservice.EPIXWebService.class.isAssignableFrom(serviceEndpointInterface)) {
                com.nirvasoft.epixws.webservice.EPIXWebServiceImplPortBindingStub _stub = new com.nirvasoft.epixws.webservice.EPIXWebServiceImplPortBindingStub(new java.net.URL(EPIXWebServiceImplPort_address), this);
                _stub.setPortName(getEPIXWebServiceImplPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("EPIXWebServiceImplPort".equals(inputPortName)) {
            return getEPIXWebServiceImplPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EPIXWebServiceImplService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EPIXWebServiceImplPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("EPIXWebServiceImplPort".equals(portName)) {
            setEPIXWebServiceImplPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
