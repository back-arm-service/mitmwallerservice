/**
 * AtmLocationData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.epixws.webservice;

public class AtmLocationData  implements java.io.Serializable {
    private java.lang.String idEntity;

    private java.lang.String bankCode;

    private java.lang.String branchCode;

    private java.lang.String branchPhone1;

    private java.lang.String branchPhone2;

    private java.lang.String servicesProvided1;

    private java.lang.String servicesProvided2;

    private java.lang.String servicesProvided3;

    private java.lang.String additionalDetails1;

    private java.lang.String additionalDetails2;

    private java.lang.String workTimings1;

    private java.lang.String workTimings2;

    private java.lang.String workDays1;

    private java.lang.String workDays2;

    private java.lang.String latitude;

    private java.lang.String longitude;

    private java.lang.String locationType;

    public AtmLocationData() {
    }

    public AtmLocationData(
           java.lang.String idEntity,
           java.lang.String bankCode,
           java.lang.String branchCode,
           java.lang.String branchPhone1,
           java.lang.String branchPhone2,
           java.lang.String servicesProvided1,
           java.lang.String servicesProvided2,
           java.lang.String servicesProvided3,
           java.lang.String additionalDetails1,
           java.lang.String additionalDetails2,
           java.lang.String workTimings1,
           java.lang.String workTimings2,
           java.lang.String workDays1,
           java.lang.String workDays2,
           java.lang.String latitude,
           java.lang.String longitude,
           java.lang.String locationType) {
           this.idEntity = idEntity;
           this.bankCode = bankCode;
           this.branchCode = branchCode;
           this.branchPhone1 = branchPhone1;
           this.branchPhone2 = branchPhone2;
           this.servicesProvided1 = servicesProvided1;
           this.servicesProvided2 = servicesProvided2;
           this.servicesProvided3 = servicesProvided3;
           this.additionalDetails1 = additionalDetails1;
           this.additionalDetails2 = additionalDetails2;
           this.workTimings1 = workTimings1;
           this.workTimings2 = workTimings2;
           this.workDays1 = workDays1;
           this.workDays2 = workDays2;
           this.latitude = latitude;
           this.longitude = longitude;
           this.locationType = locationType;
    }


    /**
     * Gets the idEntity value for this AtmLocationData.
     * 
     * @return idEntity
     */
    public java.lang.String getIdEntity() {
        return idEntity;
    }


    /**
     * Sets the idEntity value for this AtmLocationData.
     * 
     * @param idEntity
     */
    public void setIdEntity(java.lang.String idEntity) {
        this.idEntity = idEntity;
    }


    /**
     * Gets the bankCode value for this AtmLocationData.
     * 
     * @return bankCode
     */
    public java.lang.String getBankCode() {
        return bankCode;
    }


    /**
     * Sets the bankCode value for this AtmLocationData.
     * 
     * @param bankCode
     */
    public void setBankCode(java.lang.String bankCode) {
        this.bankCode = bankCode;
    }


    /**
     * Gets the branchCode value for this AtmLocationData.
     * 
     * @return branchCode
     */
    public java.lang.String getBranchCode() {
        return branchCode;
    }


    /**
     * Sets the branchCode value for this AtmLocationData.
     * 
     * @param branchCode
     */
    public void setBranchCode(java.lang.String branchCode) {
        this.branchCode = branchCode;
    }


    /**
     * Gets the branchPhone1 value for this AtmLocationData.
     * 
     * @return branchPhone1
     */
    public java.lang.String getBranchPhone1() {
        return branchPhone1;
    }


    /**
     * Sets the branchPhone1 value for this AtmLocationData.
     * 
     * @param branchPhone1
     */
    public void setBranchPhone1(java.lang.String branchPhone1) {
        this.branchPhone1 = branchPhone1;
    }


    /**
     * Gets the branchPhone2 value for this AtmLocationData.
     * 
     * @return branchPhone2
     */
    public java.lang.String getBranchPhone2() {
        return branchPhone2;
    }


    /**
     * Sets the branchPhone2 value for this AtmLocationData.
     * 
     * @param branchPhone2
     */
    public void setBranchPhone2(java.lang.String branchPhone2) {
        this.branchPhone2 = branchPhone2;
    }


    /**
     * Gets the servicesProvided1 value for this AtmLocationData.
     * 
     * @return servicesProvided1
     */
    public java.lang.String getServicesProvided1() {
        return servicesProvided1;
    }


    /**
     * Sets the servicesProvided1 value for this AtmLocationData.
     * 
     * @param servicesProvided1
     */
    public void setServicesProvided1(java.lang.String servicesProvided1) {
        this.servicesProvided1 = servicesProvided1;
    }


    /**
     * Gets the servicesProvided2 value for this AtmLocationData.
     * 
     * @return servicesProvided2
     */
    public java.lang.String getServicesProvided2() {
        return servicesProvided2;
    }


    /**
     * Sets the servicesProvided2 value for this AtmLocationData.
     * 
     * @param servicesProvided2
     */
    public void setServicesProvided2(java.lang.String servicesProvided2) {
        this.servicesProvided2 = servicesProvided2;
    }


    /**
     * Gets the servicesProvided3 value for this AtmLocationData.
     * 
     * @return servicesProvided3
     */
    public java.lang.String getServicesProvided3() {
        return servicesProvided3;
    }


    /**
     * Sets the servicesProvided3 value for this AtmLocationData.
     * 
     * @param servicesProvided3
     */
    public void setServicesProvided3(java.lang.String servicesProvided3) {
        this.servicesProvided3 = servicesProvided3;
    }


    /**
     * Gets the additionalDetails1 value for this AtmLocationData.
     * 
     * @return additionalDetails1
     */
    public java.lang.String getAdditionalDetails1() {
        return additionalDetails1;
    }


    /**
     * Sets the additionalDetails1 value for this AtmLocationData.
     * 
     * @param additionalDetails1
     */
    public void setAdditionalDetails1(java.lang.String additionalDetails1) {
        this.additionalDetails1 = additionalDetails1;
    }


    /**
     * Gets the additionalDetails2 value for this AtmLocationData.
     * 
     * @return additionalDetails2
     */
    public java.lang.String getAdditionalDetails2() {
        return additionalDetails2;
    }


    /**
     * Sets the additionalDetails2 value for this AtmLocationData.
     * 
     * @param additionalDetails2
     */
    public void setAdditionalDetails2(java.lang.String additionalDetails2) {
        this.additionalDetails2 = additionalDetails2;
    }


    /**
     * Gets the workTimings1 value for this AtmLocationData.
     * 
     * @return workTimings1
     */
    public java.lang.String getWorkTimings1() {
        return workTimings1;
    }


    /**
     * Sets the workTimings1 value for this AtmLocationData.
     * 
     * @param workTimings1
     */
    public void setWorkTimings1(java.lang.String workTimings1) {
        this.workTimings1 = workTimings1;
    }


    /**
     * Gets the workTimings2 value for this AtmLocationData.
     * 
     * @return workTimings2
     */
    public java.lang.String getWorkTimings2() {
        return workTimings2;
    }


    /**
     * Sets the workTimings2 value for this AtmLocationData.
     * 
     * @param workTimings2
     */
    public void setWorkTimings2(java.lang.String workTimings2) {
        this.workTimings2 = workTimings2;
    }


    /**
     * Gets the workDays1 value for this AtmLocationData.
     * 
     * @return workDays1
     */
    public java.lang.String getWorkDays1() {
        return workDays1;
    }


    /**
     * Sets the workDays1 value for this AtmLocationData.
     * 
     * @param workDays1
     */
    public void setWorkDays1(java.lang.String workDays1) {
        this.workDays1 = workDays1;
    }


    /**
     * Gets the workDays2 value for this AtmLocationData.
     * 
     * @return workDays2
     */
    public java.lang.String getWorkDays2() {
        return workDays2;
    }


    /**
     * Sets the workDays2 value for this AtmLocationData.
     * 
     * @param workDays2
     */
    public void setWorkDays2(java.lang.String workDays2) {
        this.workDays2 = workDays2;
    }


    /**
     * Gets the latitude value for this AtmLocationData.
     * 
     * @return latitude
     */
    public java.lang.String getLatitude() {
        return latitude;
    }


    /**
     * Sets the latitude value for this AtmLocationData.
     * 
     * @param latitude
     */
    public void setLatitude(java.lang.String latitude) {
        this.latitude = latitude;
    }


    /**
     * Gets the longitude value for this AtmLocationData.
     * 
     * @return longitude
     */
    public java.lang.String getLongitude() {
        return longitude;
    }


    /**
     * Sets the longitude value for this AtmLocationData.
     * 
     * @param longitude
     */
    public void setLongitude(java.lang.String longitude) {
        this.longitude = longitude;
    }


    /**
     * Gets the locationType value for this AtmLocationData.
     * 
     * @return locationType
     */
    public java.lang.String getLocationType() {
        return locationType;
    }


    /**
     * Sets the locationType value for this AtmLocationData.
     * 
     * @param locationType
     */
    public void setLocationType(java.lang.String locationType) {
        this.locationType = locationType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AtmLocationData)) return false;
        AtmLocationData other = (AtmLocationData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.idEntity==null && other.getIdEntity()==null) || 
             (this.idEntity!=null &&
              this.idEntity.equals(other.getIdEntity()))) &&
            ((this.bankCode==null && other.getBankCode()==null) || 
             (this.bankCode!=null &&
              this.bankCode.equals(other.getBankCode()))) &&
            ((this.branchCode==null && other.getBranchCode()==null) || 
             (this.branchCode!=null &&
              this.branchCode.equals(other.getBranchCode()))) &&
            ((this.branchPhone1==null && other.getBranchPhone1()==null) || 
             (this.branchPhone1!=null &&
              this.branchPhone1.equals(other.getBranchPhone1()))) &&
            ((this.branchPhone2==null && other.getBranchPhone2()==null) || 
             (this.branchPhone2!=null &&
              this.branchPhone2.equals(other.getBranchPhone2()))) &&
            ((this.servicesProvided1==null && other.getServicesProvided1()==null) || 
             (this.servicesProvided1!=null &&
              this.servicesProvided1.equals(other.getServicesProvided1()))) &&
            ((this.servicesProvided2==null && other.getServicesProvided2()==null) || 
             (this.servicesProvided2!=null &&
              this.servicesProvided2.equals(other.getServicesProvided2()))) &&
            ((this.servicesProvided3==null && other.getServicesProvided3()==null) || 
             (this.servicesProvided3!=null &&
              this.servicesProvided3.equals(other.getServicesProvided3()))) &&
            ((this.additionalDetails1==null && other.getAdditionalDetails1()==null) || 
             (this.additionalDetails1!=null &&
              this.additionalDetails1.equals(other.getAdditionalDetails1()))) &&
            ((this.additionalDetails2==null && other.getAdditionalDetails2()==null) || 
             (this.additionalDetails2!=null &&
              this.additionalDetails2.equals(other.getAdditionalDetails2()))) &&
            ((this.workTimings1==null && other.getWorkTimings1()==null) || 
             (this.workTimings1!=null &&
              this.workTimings1.equals(other.getWorkTimings1()))) &&
            ((this.workTimings2==null && other.getWorkTimings2()==null) || 
             (this.workTimings2!=null &&
              this.workTimings2.equals(other.getWorkTimings2()))) &&
            ((this.workDays1==null && other.getWorkDays1()==null) || 
             (this.workDays1!=null &&
              this.workDays1.equals(other.getWorkDays1()))) &&
            ((this.workDays2==null && other.getWorkDays2()==null) || 
             (this.workDays2!=null &&
              this.workDays2.equals(other.getWorkDays2()))) &&
            ((this.latitude==null && other.getLatitude()==null) || 
             (this.latitude!=null &&
              this.latitude.equals(other.getLatitude()))) &&
            ((this.longitude==null && other.getLongitude()==null) || 
             (this.longitude!=null &&
              this.longitude.equals(other.getLongitude()))) &&
            ((this.locationType==null && other.getLocationType()==null) || 
             (this.locationType!=null &&
              this.locationType.equals(other.getLocationType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdEntity() != null) {
            _hashCode += getIdEntity().hashCode();
        }
        if (getBankCode() != null) {
            _hashCode += getBankCode().hashCode();
        }
        if (getBranchCode() != null) {
            _hashCode += getBranchCode().hashCode();
        }
        if (getBranchPhone1() != null) {
            _hashCode += getBranchPhone1().hashCode();
        }
        if (getBranchPhone2() != null) {
            _hashCode += getBranchPhone2().hashCode();
        }
        if (getServicesProvided1() != null) {
            _hashCode += getServicesProvided1().hashCode();
        }
        if (getServicesProvided2() != null) {
            _hashCode += getServicesProvided2().hashCode();
        }
        if (getServicesProvided3() != null) {
            _hashCode += getServicesProvided3().hashCode();
        }
        if (getAdditionalDetails1() != null) {
            _hashCode += getAdditionalDetails1().hashCode();
        }
        if (getAdditionalDetails2() != null) {
            _hashCode += getAdditionalDetails2().hashCode();
        }
        if (getWorkTimings1() != null) {
            _hashCode += getWorkTimings1().hashCode();
        }
        if (getWorkTimings2() != null) {
            _hashCode += getWorkTimings2().hashCode();
        }
        if (getWorkDays1() != null) {
            _hashCode += getWorkDays1().hashCode();
        }
        if (getWorkDays2() != null) {
            _hashCode += getWorkDays2().hashCode();
        }
        if (getLatitude() != null) {
            _hashCode += getLatitude().hashCode();
        }
        if (getLongitude() != null) {
            _hashCode += getLongitude().hashCode();
        }
        if (getLocationType() != null) {
            _hashCode += getLocationType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AtmLocationData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "atmLocationData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idEntity");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IdEntity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bankCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BankCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("branchCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BranchCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("branchPhone1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BranchPhone1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("branchPhone2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BranchPhone2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("servicesProvided1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ServicesProvided1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("servicesProvided2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ServicesProvided2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("servicesProvided3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ServicesProvided3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("additionalDetails1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AdditionalDetails1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("additionalDetails2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AdditionalDetails2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workTimings1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "WorkTimings1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workTimings2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "WorkTimings2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workDays1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "WorkDays1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workDays2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "WorkDays2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("latitude");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Latitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("longitude");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Longitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("locationType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "LocationType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
