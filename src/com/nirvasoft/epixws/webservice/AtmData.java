/**
 * AtmData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.epixws.webservice;

public class AtmData  implements java.io.Serializable {
    private java.lang.String branchCode;

    private java.lang.String branchAddress1;

    private java.lang.String branchAddress2;

    private java.lang.String branchAddress3;

    private java.lang.String branchName;

    private java.lang.String latitude;

    private java.lang.String longitude;

    private java.lang.String locationType;

    private java.lang.String branchPhone1;

    private java.lang.String branchPhone2;

    private java.lang.String servicesProvided1;

    private java.lang.String servicesProvided2;

    private java.lang.String servicesProvided3;

    private java.lang.String workTimings1;

    private java.lang.String workTimings2;

    private java.lang.String workDays1;

    private java.lang.String workDays2;

    public AtmData() {
    }

    public AtmData(
           java.lang.String branchCode,
           java.lang.String branchAddress1,
           java.lang.String branchAddress2,
           java.lang.String branchAddress3,
           java.lang.String branchName,
           java.lang.String latitude,
           java.lang.String longitude,
           java.lang.String locationType,
           java.lang.String branchPhone1,
           java.lang.String branchPhone2,
           java.lang.String servicesProvided1,
           java.lang.String servicesProvided2,
           java.lang.String servicesProvided3,
           java.lang.String workTimings1,
           java.lang.String workTimings2,
           java.lang.String workDays1,
           java.lang.String workDays2) {
           this.branchCode = branchCode;
           this.branchAddress1 = branchAddress1;
           this.branchAddress2 = branchAddress2;
           this.branchAddress3 = branchAddress3;
           this.branchName = branchName;
           this.latitude = latitude;
           this.longitude = longitude;
           this.locationType = locationType;
           this.branchPhone1 = branchPhone1;
           this.branchPhone2 = branchPhone2;
           this.servicesProvided1 = servicesProvided1;
           this.servicesProvided2 = servicesProvided2;
           this.servicesProvided3 = servicesProvided3;
           this.workTimings1 = workTimings1;
           this.workTimings2 = workTimings2;
           this.workDays1 = workDays1;
           this.workDays2 = workDays2;
    }


    /**
     * Gets the branchCode value for this AtmData.
     * 
     * @return branchCode
     */
    public java.lang.String getBranchCode() {
        return branchCode;
    }


    /**
     * Sets the branchCode value for this AtmData.
     * 
     * @param branchCode
     */
    public void setBranchCode(java.lang.String branchCode) {
        this.branchCode = branchCode;
    }


    /**
     * Gets the branchAddress1 value for this AtmData.
     * 
     * @return branchAddress1
     */
    public java.lang.String getBranchAddress1() {
        return branchAddress1;
    }


    /**
     * Sets the branchAddress1 value for this AtmData.
     * 
     * @param branchAddress1
     */
    public void setBranchAddress1(java.lang.String branchAddress1) {
        this.branchAddress1 = branchAddress1;
    }


    /**
     * Gets the branchAddress2 value for this AtmData.
     * 
     * @return branchAddress2
     */
    public java.lang.String getBranchAddress2() {
        return branchAddress2;
    }


    /**
     * Sets the branchAddress2 value for this AtmData.
     * 
     * @param branchAddress2
     */
    public void setBranchAddress2(java.lang.String branchAddress2) {
        this.branchAddress2 = branchAddress2;
    }


    /**
     * Gets the branchAddress3 value for this AtmData.
     * 
     * @return branchAddress3
     */
    public java.lang.String getBranchAddress3() {
        return branchAddress3;
    }


    /**
     * Sets the branchAddress3 value for this AtmData.
     * 
     * @param branchAddress3
     */
    public void setBranchAddress3(java.lang.String branchAddress3) {
        this.branchAddress3 = branchAddress3;
    }


    /**
     * Gets the branchName value for this AtmData.
     * 
     * @return branchName
     */
    public java.lang.String getBranchName() {
        return branchName;
    }


    /**
     * Sets the branchName value for this AtmData.
     * 
     * @param branchName
     */
    public void setBranchName(java.lang.String branchName) {
        this.branchName = branchName;
    }


    /**
     * Gets the latitude value for this AtmData.
     * 
     * @return latitude
     */
    public java.lang.String getLatitude() {
        return latitude;
    }


    /**
     * Sets the latitude value for this AtmData.
     * 
     * @param latitude
     */
    public void setLatitude(java.lang.String latitude) {
        this.latitude = latitude;
    }


    /**
     * Gets the longitude value for this AtmData.
     * 
     * @return longitude
     */
    public java.lang.String getLongitude() {
        return longitude;
    }


    /**
     * Sets the longitude value for this AtmData.
     * 
     * @param longitude
     */
    public void setLongitude(java.lang.String longitude) {
        this.longitude = longitude;
    }


    /**
     * Gets the locationType value for this AtmData.
     * 
     * @return locationType
     */
    public java.lang.String getLocationType() {
        return locationType;
    }


    /**
     * Sets the locationType value for this AtmData.
     * 
     * @param locationType
     */
    public void setLocationType(java.lang.String locationType) {
        this.locationType = locationType;
    }


    /**
     * Gets the branchPhone1 value for this AtmData.
     * 
     * @return branchPhone1
     */
    public java.lang.String getBranchPhone1() {
        return branchPhone1;
    }


    /**
     * Sets the branchPhone1 value for this AtmData.
     * 
     * @param branchPhone1
     */
    public void setBranchPhone1(java.lang.String branchPhone1) {
        this.branchPhone1 = branchPhone1;
    }


    /**
     * Gets the branchPhone2 value for this AtmData.
     * 
     * @return branchPhone2
     */
    public java.lang.String getBranchPhone2() {
        return branchPhone2;
    }


    /**
     * Sets the branchPhone2 value for this AtmData.
     * 
     * @param branchPhone2
     */
    public void setBranchPhone2(java.lang.String branchPhone2) {
        this.branchPhone2 = branchPhone2;
    }


    /**
     * Gets the servicesProvided1 value for this AtmData.
     * 
     * @return servicesProvided1
     */
    public java.lang.String getServicesProvided1() {
        return servicesProvided1;
    }


    /**
     * Sets the servicesProvided1 value for this AtmData.
     * 
     * @param servicesProvided1
     */
    public void setServicesProvided1(java.lang.String servicesProvided1) {
        this.servicesProvided1 = servicesProvided1;
    }


    /**
     * Gets the servicesProvided2 value for this AtmData.
     * 
     * @return servicesProvided2
     */
    public java.lang.String getServicesProvided2() {
        return servicesProvided2;
    }


    /**
     * Sets the servicesProvided2 value for this AtmData.
     * 
     * @param servicesProvided2
     */
    public void setServicesProvided2(java.lang.String servicesProvided2) {
        this.servicesProvided2 = servicesProvided2;
    }


    /**
     * Gets the servicesProvided3 value for this AtmData.
     * 
     * @return servicesProvided3
     */
    public java.lang.String getServicesProvided3() {
        return servicesProvided3;
    }


    /**
     * Sets the servicesProvided3 value for this AtmData.
     * 
     * @param servicesProvided3
     */
    public void setServicesProvided3(java.lang.String servicesProvided3) {
        this.servicesProvided3 = servicesProvided3;
    }


    /**
     * Gets the workTimings1 value for this AtmData.
     * 
     * @return workTimings1
     */
    public java.lang.String getWorkTimings1() {
        return workTimings1;
    }


    /**
     * Sets the workTimings1 value for this AtmData.
     * 
     * @param workTimings1
     */
    public void setWorkTimings1(java.lang.String workTimings1) {
        this.workTimings1 = workTimings1;
    }


    /**
     * Gets the workTimings2 value for this AtmData.
     * 
     * @return workTimings2
     */
    public java.lang.String getWorkTimings2() {
        return workTimings2;
    }


    /**
     * Sets the workTimings2 value for this AtmData.
     * 
     * @param workTimings2
     */
    public void setWorkTimings2(java.lang.String workTimings2) {
        this.workTimings2 = workTimings2;
    }


    /**
     * Gets the workDays1 value for this AtmData.
     * 
     * @return workDays1
     */
    public java.lang.String getWorkDays1() {
        return workDays1;
    }


    /**
     * Sets the workDays1 value for this AtmData.
     * 
     * @param workDays1
     */
    public void setWorkDays1(java.lang.String workDays1) {
        this.workDays1 = workDays1;
    }


    /**
     * Gets the workDays2 value for this AtmData.
     * 
     * @return workDays2
     */
    public java.lang.String getWorkDays2() {
        return workDays2;
    }


    /**
     * Sets the workDays2 value for this AtmData.
     * 
     * @param workDays2
     */
    public void setWorkDays2(java.lang.String workDays2) {
        this.workDays2 = workDays2;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AtmData)) return false;
        AtmData other = (AtmData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.branchCode==null && other.getBranchCode()==null) || 
             (this.branchCode!=null &&
              this.branchCode.equals(other.getBranchCode()))) &&
            ((this.branchAddress1==null && other.getBranchAddress1()==null) || 
             (this.branchAddress1!=null &&
              this.branchAddress1.equals(other.getBranchAddress1()))) &&
            ((this.branchAddress2==null && other.getBranchAddress2()==null) || 
             (this.branchAddress2!=null &&
              this.branchAddress2.equals(other.getBranchAddress2()))) &&
            ((this.branchAddress3==null && other.getBranchAddress3()==null) || 
             (this.branchAddress3!=null &&
              this.branchAddress3.equals(other.getBranchAddress3()))) &&
            ((this.branchName==null && other.getBranchName()==null) || 
             (this.branchName!=null &&
              this.branchName.equals(other.getBranchName()))) &&
            ((this.latitude==null && other.getLatitude()==null) || 
             (this.latitude!=null &&
              this.latitude.equals(other.getLatitude()))) &&
            ((this.longitude==null && other.getLongitude()==null) || 
             (this.longitude!=null &&
              this.longitude.equals(other.getLongitude()))) &&
            ((this.locationType==null && other.getLocationType()==null) || 
             (this.locationType!=null &&
              this.locationType.equals(other.getLocationType()))) &&
            ((this.branchPhone1==null && other.getBranchPhone1()==null) || 
             (this.branchPhone1!=null &&
              this.branchPhone1.equals(other.getBranchPhone1()))) &&
            ((this.branchPhone2==null && other.getBranchPhone2()==null) || 
             (this.branchPhone2!=null &&
              this.branchPhone2.equals(other.getBranchPhone2()))) &&
            ((this.servicesProvided1==null && other.getServicesProvided1()==null) || 
             (this.servicesProvided1!=null &&
              this.servicesProvided1.equals(other.getServicesProvided1()))) &&
            ((this.servicesProvided2==null && other.getServicesProvided2()==null) || 
             (this.servicesProvided2!=null &&
              this.servicesProvided2.equals(other.getServicesProvided2()))) &&
            ((this.servicesProvided3==null && other.getServicesProvided3()==null) || 
             (this.servicesProvided3!=null &&
              this.servicesProvided3.equals(other.getServicesProvided3()))) &&
            ((this.workTimings1==null && other.getWorkTimings1()==null) || 
             (this.workTimings1!=null &&
              this.workTimings1.equals(other.getWorkTimings1()))) &&
            ((this.workTimings2==null && other.getWorkTimings2()==null) || 
             (this.workTimings2!=null &&
              this.workTimings2.equals(other.getWorkTimings2()))) &&
            ((this.workDays1==null && other.getWorkDays1()==null) || 
             (this.workDays1!=null &&
              this.workDays1.equals(other.getWorkDays1()))) &&
            ((this.workDays2==null && other.getWorkDays2()==null) || 
             (this.workDays2!=null &&
              this.workDays2.equals(other.getWorkDays2())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBranchCode() != null) {
            _hashCode += getBranchCode().hashCode();
        }
        if (getBranchAddress1() != null) {
            _hashCode += getBranchAddress1().hashCode();
        }
        if (getBranchAddress2() != null) {
            _hashCode += getBranchAddress2().hashCode();
        }
        if (getBranchAddress3() != null) {
            _hashCode += getBranchAddress3().hashCode();
        }
        if (getBranchName() != null) {
            _hashCode += getBranchName().hashCode();
        }
        if (getLatitude() != null) {
            _hashCode += getLatitude().hashCode();
        }
        if (getLongitude() != null) {
            _hashCode += getLongitude().hashCode();
        }
        if (getLocationType() != null) {
            _hashCode += getLocationType().hashCode();
        }
        if (getBranchPhone1() != null) {
            _hashCode += getBranchPhone1().hashCode();
        }
        if (getBranchPhone2() != null) {
            _hashCode += getBranchPhone2().hashCode();
        }
        if (getServicesProvided1() != null) {
            _hashCode += getServicesProvided1().hashCode();
        }
        if (getServicesProvided2() != null) {
            _hashCode += getServicesProvided2().hashCode();
        }
        if (getServicesProvided3() != null) {
            _hashCode += getServicesProvided3().hashCode();
        }
        if (getWorkTimings1() != null) {
            _hashCode += getWorkTimings1().hashCode();
        }
        if (getWorkTimings2() != null) {
            _hashCode += getWorkTimings2().hashCode();
        }
        if (getWorkDays1() != null) {
            _hashCode += getWorkDays1().hashCode();
        }
        if (getWorkDays2() != null) {
            _hashCode += getWorkDays2().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AtmData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "atmData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("branchCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BranchCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("branchAddress1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BranchAddress1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("branchAddress2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BranchAddress2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("branchAddress3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BranchAddress3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("branchName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BranchName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("latitude");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Latitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("longitude");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Longitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("locationType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "LocationType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("branchPhone1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BranchPhone1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("branchPhone2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BranchPhone2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("servicesProvided1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ServicesProvided1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("servicesProvided2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ServicesProvided2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("servicesProvided3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ServicesProvided3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workTimings1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "WorkTimings1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workTimings2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "WorkTimings2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workDays1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "WorkDays1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workDays2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "WorkDays2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
