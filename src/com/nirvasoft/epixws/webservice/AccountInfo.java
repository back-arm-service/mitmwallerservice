/**
 * AccountInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.epixws.webservice;

public class AccountInfo  implements java.io.Serializable {
    private java.lang.String depositAcc;

    private java.lang.String cardAcc;

    private java.lang.String CCY;

    private double avlBal;

    private java.lang.String accType;

    private java.lang.String accDesc;

    private java.lang.String accNRC;

    private java.lang.String accCustomerID;

    private java.lang.String accName;

    private java.lang.String accDOB;

    private java.lang.String type;

    public AccountInfo() {
    }

    public AccountInfo(
           java.lang.String depositAcc,
           java.lang.String cardAcc,
           java.lang.String CCY,
           double avlBal,
           java.lang.String accType,
           java.lang.String accDesc,
           java.lang.String accNRC,
           java.lang.String accCustomerID,
           java.lang.String accName,
           java.lang.String accDOB,
           java.lang.String type) {
           this.depositAcc = depositAcc;
           this.cardAcc = cardAcc;
           this.CCY = CCY;
           this.avlBal = avlBal;
           this.accType = accType;
           this.accDesc = accDesc;
           this.accNRC = accNRC;
           this.accCustomerID = accCustomerID;
           this.accName = accName;
           this.accDOB = accDOB;
           this.type = type;
    }


    /**
     * Gets the depositAcc value for this AccountInfo.
     * 
     * @return depositAcc
     */
    public java.lang.String getDepositAcc() {
        return depositAcc;
    }


    /**
     * Sets the depositAcc value for this AccountInfo.
     * 
     * @param depositAcc
     */
    public void setDepositAcc(java.lang.String depositAcc) {
        this.depositAcc = depositAcc;
    }


    /**
     * Gets the cardAcc value for this AccountInfo.
     * 
     * @return cardAcc
     */
    public java.lang.String getCardAcc() {
        return cardAcc;
    }


    /**
     * Sets the cardAcc value for this AccountInfo.
     * 
     * @param cardAcc
     */
    public void setCardAcc(java.lang.String cardAcc) {
        this.cardAcc = cardAcc;
    }


    /**
     * Gets the CCY value for this AccountInfo.
     * 
     * @return CCY
     */
    public java.lang.String getCCY() {
        return CCY;
    }


    /**
     * Sets the CCY value for this AccountInfo.
     * 
     * @param CCY
     */
    public void setCCY(java.lang.String CCY) {
        this.CCY = CCY;
    }


    /**
     * Gets the avlBal value for this AccountInfo.
     * 
     * @return avlBal
     */
    public double getAvlBal() {
        return avlBal;
    }


    /**
     * Sets the avlBal value for this AccountInfo.
     * 
     * @param avlBal
     */
    public void setAvlBal(double avlBal) {
        this.avlBal = avlBal;
    }


    /**
     * Gets the accType value for this AccountInfo.
     * 
     * @return accType
     */
    public java.lang.String getAccType() {
        return accType;
    }


    /**
     * Sets the accType value for this AccountInfo.
     * 
     * @param accType
     */
    public void setAccType(java.lang.String accType) {
        this.accType = accType;
    }


    /**
     * Gets the accDesc value for this AccountInfo.
     * 
     * @return accDesc
     */
    public java.lang.String getAccDesc() {
        return accDesc;
    }


    /**
     * Sets the accDesc value for this AccountInfo.
     * 
     * @param accDesc
     */
    public void setAccDesc(java.lang.String accDesc) {
        this.accDesc = accDesc;
    }


    /**
     * Gets the accNRC value for this AccountInfo.
     * 
     * @return accNRC
     */
    public java.lang.String getAccNRC() {
        return accNRC;
    }


    /**
     * Sets the accNRC value for this AccountInfo.
     * 
     * @param accNRC
     */
    public void setAccNRC(java.lang.String accNRC) {
        this.accNRC = accNRC;
    }


    /**
     * Gets the accCustomerID value for this AccountInfo.
     * 
     * @return accCustomerID
     */
    public java.lang.String getAccCustomerID() {
        return accCustomerID;
    }


    /**
     * Sets the accCustomerID value for this AccountInfo.
     * 
     * @param accCustomerID
     */
    public void setAccCustomerID(java.lang.String accCustomerID) {
        this.accCustomerID = accCustomerID;
    }


    /**
     * Gets the accName value for this AccountInfo.
     * 
     * @return accName
     */
    public java.lang.String getAccName() {
        return accName;
    }


    /**
     * Sets the accName value for this AccountInfo.
     * 
     * @param accName
     */
    public void setAccName(java.lang.String accName) {
        this.accName = accName;
    }


    /**
     * Gets the accDOB value for this AccountInfo.
     * 
     * @return accDOB
     */
    public java.lang.String getAccDOB() {
        return accDOB;
    }


    /**
     * Sets the accDOB value for this AccountInfo.
     * 
     * @param accDOB
     */
    public void setAccDOB(java.lang.String accDOB) {
        this.accDOB = accDOB;
    }


    /**
     * Gets the type value for this AccountInfo.
     * 
     * @return type
     */
    public java.lang.String getType() {
        return type;
    }


    /**
     * Sets the type value for this AccountInfo.
     * 
     * @param type
     */
    public void setType(java.lang.String type) {
        this.type = type;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AccountInfo)) return false;
        AccountInfo other = (AccountInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.depositAcc==null && other.getDepositAcc()==null) || 
             (this.depositAcc!=null &&
              this.depositAcc.equals(other.getDepositAcc()))) &&
            ((this.cardAcc==null && other.getCardAcc()==null) || 
             (this.cardAcc!=null &&
              this.cardAcc.equals(other.getCardAcc()))) &&
            ((this.CCY==null && other.getCCY()==null) || 
             (this.CCY!=null &&
              this.CCY.equals(other.getCCY()))) &&
            this.avlBal == other.getAvlBal() &&
            ((this.accType==null && other.getAccType()==null) || 
             (this.accType!=null &&
              this.accType.equals(other.getAccType()))) &&
            ((this.accDesc==null && other.getAccDesc()==null) || 
             (this.accDesc!=null &&
              this.accDesc.equals(other.getAccDesc()))) &&
            ((this.accNRC==null && other.getAccNRC()==null) || 
             (this.accNRC!=null &&
              this.accNRC.equals(other.getAccNRC()))) &&
            ((this.accCustomerID==null && other.getAccCustomerID()==null) || 
             (this.accCustomerID!=null &&
              this.accCustomerID.equals(other.getAccCustomerID()))) &&
            ((this.accName==null && other.getAccName()==null) || 
             (this.accName!=null &&
              this.accName.equals(other.getAccName()))) &&
            ((this.accDOB==null && other.getAccDOB()==null) || 
             (this.accDOB!=null &&
              this.accDOB.equals(other.getAccDOB()))) &&
            ((this.type==null && other.getType()==null) || 
             (this.type!=null &&
              this.type.equals(other.getType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDepositAcc() != null) {
            _hashCode += getDepositAcc().hashCode();
        }
        if (getCardAcc() != null) {
            _hashCode += getCardAcc().hashCode();
        }
        if (getCCY() != null) {
            _hashCode += getCCY().hashCode();
        }
        _hashCode += new Double(getAvlBal()).hashCode();
        if (getAccType() != null) {
            _hashCode += getAccType().hashCode();
        }
        if (getAccDesc() != null) {
            _hashCode += getAccDesc().hashCode();
        }
        if (getAccNRC() != null) {
            _hashCode += getAccNRC().hashCode();
        }
        if (getAccCustomerID() != null) {
            _hashCode += getAccCustomerID().hashCode();
        }
        if (getAccName() != null) {
            _hashCode += getAccName().hashCode();
        }
        if (getAccDOB() != null) {
            _hashCode += getAccDOB().hashCode();
        }
        if (getType() != null) {
            _hashCode += getType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AccountInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "accountInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("depositAcc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DepositAcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardAcc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CardAcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CCY");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CCY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avlBal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AvlBal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AccType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accDesc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AccDesc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accNRC");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AccNRC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accCustomerID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AccCustomerID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AccName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accDOB");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AccDOB"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("type");
        elemField.setXmlName(new javax.xml.namespace.QName("", "type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
