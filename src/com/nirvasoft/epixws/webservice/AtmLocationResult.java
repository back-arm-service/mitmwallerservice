/**
 * AtmLocationResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.epixws.webservice;

public class AtmLocationResult  implements java.io.Serializable {
    private java.lang.String code;

    private java.lang.String desc;

    private com.nirvasoft.epixws.webservice.AtmLocationData[] ATMLocation;

    private com.nirvasoft.epixws.webservice.AtmData[] ATM;

    public AtmLocationResult() {
    }

    public AtmLocationResult(
           java.lang.String code,
           java.lang.String desc,
           com.nirvasoft.epixws.webservice.AtmLocationData[] ATMLocation,
           com.nirvasoft.epixws.webservice.AtmData[] ATM) {
           this.code = code;
           this.desc = desc;
           this.ATMLocation = ATMLocation;
           this.ATM = ATM;
    }


    /**
     * Gets the code value for this AtmLocationResult.
     * 
     * @return code
     */
    public java.lang.String getCode() {
        return code;
    }


    /**
     * Sets the code value for this AtmLocationResult.
     * 
     * @param code
     */
    public void setCode(java.lang.String code) {
        this.code = code;
    }


    /**
     * Gets the desc value for this AtmLocationResult.
     * 
     * @return desc
     */
    public java.lang.String getDesc() {
        return desc;
    }


    /**
     * Sets the desc value for this AtmLocationResult.
     * 
     * @param desc
     */
    public void setDesc(java.lang.String desc) {
        this.desc = desc;
    }


    /**
     * Gets the ATMLocation value for this AtmLocationResult.
     * 
     * @return ATMLocation
     */
    public com.nirvasoft.epixws.webservice.AtmLocationData[] getATMLocation() {
        return ATMLocation;
    }


    /**
     * Sets the ATMLocation value for this AtmLocationResult.
     * 
     * @param ATMLocation
     */
    public void setATMLocation(com.nirvasoft.epixws.webservice.AtmLocationData[] ATMLocation) {
        this.ATMLocation = ATMLocation;
    }

    public com.nirvasoft.epixws.webservice.AtmLocationData getATMLocation(int i) {
        return this.ATMLocation[i];
    }

    public void setATMLocation(int i, com.nirvasoft.epixws.webservice.AtmLocationData _value) {
        this.ATMLocation[i] = _value;
    }


    /**
     * Gets the ATM value for this AtmLocationResult.
     * 
     * @return ATM
     */
    public com.nirvasoft.epixws.webservice.AtmData[] getATM() {
        return ATM;
    }


    /**
     * Sets the ATM value for this AtmLocationResult.
     * 
     * @param ATM
     */
    public void setATM(com.nirvasoft.epixws.webservice.AtmData[] ATM) {
        this.ATM = ATM;
    }

    public com.nirvasoft.epixws.webservice.AtmData getATM(int i) {
        return this.ATM[i];
    }

    public void setATM(int i, com.nirvasoft.epixws.webservice.AtmData _value) {
        this.ATM[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AtmLocationResult)) return false;
        AtmLocationResult other = (AtmLocationResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.code==null && other.getCode()==null) || 
             (this.code!=null &&
              this.code.equals(other.getCode()))) &&
            ((this.desc==null && other.getDesc()==null) || 
             (this.desc!=null &&
              this.desc.equals(other.getDesc()))) &&
            ((this.ATMLocation==null && other.getATMLocation()==null) || 
             (this.ATMLocation!=null &&
              java.util.Arrays.equals(this.ATMLocation, other.getATMLocation()))) &&
            ((this.ATM==null && other.getATM()==null) || 
             (this.ATM!=null &&
              java.util.Arrays.equals(this.ATM, other.getATM())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCode() != null) {
            _hashCode += getCode().hashCode();
        }
        if (getDesc() != null) {
            _hashCode += getDesc().hashCode();
        }
        if (getATMLocation() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getATMLocation());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getATMLocation(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getATM() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getATM());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getATM(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AtmLocationResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "atmLocationResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("desc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Desc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ATMLocation");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ATMLocation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "atmLocationData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ATM");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ATM"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "atmData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
