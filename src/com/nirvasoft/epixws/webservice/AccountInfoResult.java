/**
 * AccountInfoResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.epixws.webservice;

public class AccountInfoResult  implements java.io.Serializable {
    private java.lang.String accNumber;

    private java.lang.String name;

    private java.lang.String NRC;

    private java.lang.String code;

    private java.lang.String desc;

    private com.nirvasoft.epixws.webservice.AccInfo[] accInfoArr;

    public AccountInfoResult() {
    }

    public AccountInfoResult(
           java.lang.String accNumber,
           java.lang.String name,
           java.lang.String NRC,
           java.lang.String code,
           java.lang.String desc,
           com.nirvasoft.epixws.webservice.AccInfo[] accInfoArr) {
           this.accNumber = accNumber;
           this.name = name;
           this.NRC = NRC;
           this.code = code;
           this.desc = desc;
           this.accInfoArr = accInfoArr;
    }


    /**
     * Gets the accNumber value for this AccountInfoResult.
     * 
     * @return accNumber
     */
    public java.lang.String getAccNumber() {
        return accNumber;
    }


    /**
     * Sets the accNumber value for this AccountInfoResult.
     * 
     * @param accNumber
     */
    public void setAccNumber(java.lang.String accNumber) {
        this.accNumber = accNumber;
    }


    /**
     * Gets the name value for this AccountInfoResult.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this AccountInfoResult.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the NRC value for this AccountInfoResult.
     * 
     * @return NRC
     */
    public java.lang.String getNRC() {
        return NRC;
    }


    /**
     * Sets the NRC value for this AccountInfoResult.
     * 
     * @param NRC
     */
    public void setNRC(java.lang.String NRC) {
        this.NRC = NRC;
    }


    /**
     * Gets the code value for this AccountInfoResult.
     * 
     * @return code
     */
    public java.lang.String getCode() {
        return code;
    }


    /**
     * Sets the code value for this AccountInfoResult.
     * 
     * @param code
     */
    public void setCode(java.lang.String code) {
        this.code = code;
    }


    /**
     * Gets the desc value for this AccountInfoResult.
     * 
     * @return desc
     */
    public java.lang.String getDesc() {
        return desc;
    }


    /**
     * Sets the desc value for this AccountInfoResult.
     * 
     * @param desc
     */
    public void setDesc(java.lang.String desc) {
        this.desc = desc;
    }


    /**
     * Gets the accInfoArr value for this AccountInfoResult.
     * 
     * @return accInfoArr
     */
    public com.nirvasoft.epixws.webservice.AccInfo[] getAccInfoArr() {
        return accInfoArr;
    }


    /**
     * Sets the accInfoArr value for this AccountInfoResult.
     * 
     * @param accInfoArr
     */
    public void setAccInfoArr(com.nirvasoft.epixws.webservice.AccInfo[] accInfoArr) {
        this.accInfoArr = accInfoArr;
    }

    public com.nirvasoft.epixws.webservice.AccInfo getAccInfoArr(int i) {
        return this.accInfoArr[i];
    }

    public void setAccInfoArr(int i, com.nirvasoft.epixws.webservice.AccInfo _value) {
        this.accInfoArr[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AccountInfoResult)) return false;
        AccountInfoResult other = (AccountInfoResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.accNumber==null && other.getAccNumber()==null) || 
             (this.accNumber!=null &&
              this.accNumber.equals(other.getAccNumber()))) &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.NRC==null && other.getNRC()==null) || 
             (this.NRC!=null &&
              this.NRC.equals(other.getNRC()))) &&
            ((this.code==null && other.getCode()==null) || 
             (this.code!=null &&
              this.code.equals(other.getCode()))) &&
            ((this.desc==null && other.getDesc()==null) || 
             (this.desc!=null &&
              this.desc.equals(other.getDesc()))) &&
            ((this.accInfoArr==null && other.getAccInfoArr()==null) || 
             (this.accInfoArr!=null &&
              java.util.Arrays.equals(this.accInfoArr, other.getAccInfoArr())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAccNumber() != null) {
            _hashCode += getAccNumber().hashCode();
        }
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getNRC() != null) {
            _hashCode += getNRC().hashCode();
        }
        if (getCode() != null) {
            _hashCode += getCode().hashCode();
        }
        if (getDesc() != null) {
            _hashCode += getDesc().hashCode();
        }
        if (getAccInfoArr() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAccInfoArr());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAccInfoArr(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AccountInfoResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "accountInfoResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AccNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NRC");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NRC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("desc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Desc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accInfoArr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AccInfoArr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "accInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
