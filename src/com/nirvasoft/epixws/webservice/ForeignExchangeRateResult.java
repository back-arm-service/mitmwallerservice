/**
 * ForeignExchangeRateResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.epixws.webservice;

public class ForeignExchangeRateResult  implements java.io.Serializable {
    private java.lang.String code;

    private java.lang.String desc;

    private com.nirvasoft.epixws.webservice.ForeignExchangeRateData[] foreignExchangeRate;

    public ForeignExchangeRateResult() {
    }

    public ForeignExchangeRateResult(
           java.lang.String code,
           java.lang.String desc,
           com.nirvasoft.epixws.webservice.ForeignExchangeRateData[] foreignExchangeRate) {
           this.code = code;
           this.desc = desc;
           this.foreignExchangeRate = foreignExchangeRate;
    }


    /**
     * Gets the code value for this ForeignExchangeRateResult.
     * 
     * @return code
     */
    public java.lang.String getCode() {
        return code;
    }


    /**
     * Sets the code value for this ForeignExchangeRateResult.
     * 
     * @param code
     */
    public void setCode(java.lang.String code) {
        this.code = code;
    }


    /**
     * Gets the desc value for this ForeignExchangeRateResult.
     * 
     * @return desc
     */
    public java.lang.String getDesc() {
        return desc;
    }


    /**
     * Sets the desc value for this ForeignExchangeRateResult.
     * 
     * @param desc
     */
    public void setDesc(java.lang.String desc) {
        this.desc = desc;
    }


    /**
     * Gets the foreignExchangeRate value for this ForeignExchangeRateResult.
     * 
     * @return foreignExchangeRate
     */
    public com.nirvasoft.epixws.webservice.ForeignExchangeRateData[] getForeignExchangeRate() {
        return foreignExchangeRate;
    }


    /**
     * Sets the foreignExchangeRate value for this ForeignExchangeRateResult.
     * 
     * @param foreignExchangeRate
     */
    public void setForeignExchangeRate(com.nirvasoft.epixws.webservice.ForeignExchangeRateData[] foreignExchangeRate) {
        this.foreignExchangeRate = foreignExchangeRate;
    }

    public com.nirvasoft.epixws.webservice.ForeignExchangeRateData getForeignExchangeRate(int i) {
        return this.foreignExchangeRate[i];
    }

    public void setForeignExchangeRate(int i, com.nirvasoft.epixws.webservice.ForeignExchangeRateData _value) {
        this.foreignExchangeRate[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ForeignExchangeRateResult)) return false;
        ForeignExchangeRateResult other = (ForeignExchangeRateResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.code==null && other.getCode()==null) || 
             (this.code!=null &&
              this.code.equals(other.getCode()))) &&
            ((this.desc==null && other.getDesc()==null) || 
             (this.desc!=null &&
              this.desc.equals(other.getDesc()))) &&
            ((this.foreignExchangeRate==null && other.getForeignExchangeRate()==null) || 
             (this.foreignExchangeRate!=null &&
              java.util.Arrays.equals(this.foreignExchangeRate, other.getForeignExchangeRate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCode() != null) {
            _hashCode += getCode().hashCode();
        }
        if (getDesc() != null) {
            _hashCode += getDesc().hashCode();
        }
        if (getForeignExchangeRate() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getForeignExchangeRate());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getForeignExchangeRate(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ForeignExchangeRateResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "foreignExchangeRateResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("desc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Desc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("foreignExchangeRate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ForeignExchangeRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "foreignExchangeRateData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
