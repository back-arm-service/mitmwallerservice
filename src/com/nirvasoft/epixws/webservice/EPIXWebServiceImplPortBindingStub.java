/**
 * EPIXWebServiceImplPortBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.epixws.webservice;

public class EPIXWebServiceImplPortBindingStub extends org.apache.axis.client.Stub implements com.nirvasoft.epixws.webservice.EPIXWebService {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[20];
        _initOperationDesc1();
        _initOperationDesc2();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getAccountActivity");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "AccNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "CustomerNo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DurationType"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "FromDate"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "ToDate"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "CurrentPage"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "PageSize"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "accountActivityResult"));
        oper.setReturnClass(com.nirvasoft.epixws.webservice.AccountActivityResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"),
                      "com.nirvasoft.epixws.webservice.EpixWebServiceException",
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"), 
                      true
                     ));
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getCommissionByProductCode");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "XREF"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "ProductCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "TxnAcc"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "commissionInfoResult"));
        oper.setReturnClass(com.nirvasoft.epixws.webservice.CommissionInfoResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"),
                      "com.nirvasoft.epixws.webservice.EpixWebServiceException",
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"), 
                      true
                     ));
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getTransferAccountList");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "CustomerID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "custInfoResult"));
        oper.setReturnClass(com.nirvasoft.epixws.webservice.CustInfoResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"),
                      "com.nirvasoft.epixws.webservice.EpixWebServiceException",
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"), 
                      true
                     ));
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getForeignExchangeRates");
        oper.setReturnType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "foreignExchangeRateResult"));
        oper.setReturnClass(com.nirvasoft.epixws.webservice.ForeignExchangeRateResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"),
                      "com.nirvasoft.epixws.webservice.EpixWebServiceException",
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"), 
                      true
                     ));
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getAccountCustomerInfobyCifNrc");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "cif"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "nrc"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "custAccInfoResult"));
        oper.setReturnClass(com.nirvasoft.epixws.webservice.CustAccInfoResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"),
                      "com.nirvasoft.epixws.webservice.EpixWebServiceException",
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"), 
                      true
                     ));
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getAccountActivityDownload");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "AccNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "CustomerNo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "DurationType"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "FromDate"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "ToDate"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "accountActivityResult"));
        oper.setReturnClass(com.nirvasoft.epixws.webservice.AccountActivityResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"),
                      "com.nirvasoft.epixws.webservice.EpixWebServiceException",
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"), 
                      true
                     ));
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getAccountInfoByAcctNo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "AccountNo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "custInfoResult"));
        oper.setReturnClass(com.nirvasoft.epixws.webservice.CustInfoResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"),
                      "com.nirvasoft.epixws.webservice.EpixWebServiceException",
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"), 
                      true
                     ));
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getAvailableAmountByAcctNo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "AccountNo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "custInfoResult"));
        oper.setReturnClass(com.nirvasoft.epixws.webservice.CustInfoResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"),
                      "com.nirvasoft.epixws.webservice.EpixWebServiceException",
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"), 
                      true
                     ));
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getCustomerNameAndNrcByAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Account"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "customerInfo"));
        oper.setReturnClass(com.nirvasoft.epixws.webservice.CustomerInfo.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"),
                      "com.nirvasoft.epixws.webservice.EpixWebServiceException",
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"), 
                      true
                     ));
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getAccountCustomerInfo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Caption"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Value"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Searchway"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "custAccInfoResult"));
        oper.setReturnClass(com.nirvasoft.epixws.webservice.CustAccInfoResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"),
                      "com.nirvasoft.epixws.webservice.EpixWebServiceException",
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"), 
                      true
                     ));
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("enquiryCheque");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "AccountNo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "ChequeNo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "chequeEnquiryRes"));
        oper.setReturnClass(com.nirvasoft.epixws.webservice.ChequeEnquiryRes.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getBankInfo");
        oper.setReturnType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "bankInfoList"));
        oper.setReturnClass(com.nirvasoft.epixws.webservice.BankInfoList.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"),
                      "com.nirvasoft.epixws.webservice.EpixWebServiceException",
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"), 
                      true
                     ));
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getChequeAccArr");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "CustomerID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "chequeAccResult"));
        oper.setReturnClass(com.nirvasoft.epixws.webservice.ChequeAccResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"),
                      "com.nirvasoft.epixws.webservice.EpixWebServiceException",
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"), 
                      true
                     ));
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getGLData");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Account"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "customerInfo"));
        oper.setReturnClass(com.nirvasoft.epixws.webservice.CustomerInfo.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"),
                      "com.nirvasoft.epixws.webservice.EpixWebServiceException",
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"), 
                      true
                     ));
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getEnAccountInfo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Caption"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Value"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Searchway"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "accountInfoResult"));
        oper.setReturnClass(com.nirvasoft.epixws.webservice.AccountInfoResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"),
                      "com.nirvasoft.epixws.webservice.EpixWebServiceException",
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"), 
                      true
                     ));
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getATMLocation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Id"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "atmLocationResult"));
        oper.setReturnClass(com.nirvasoft.epixws.webservice.AtmLocationResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getAccountInfo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "CustomerID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "FixedDeposit"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "custInfoResult"));
        oper.setReturnClass(com.nirvasoft.epixws.webservice.CustInfoResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"),
                      "com.nirvasoft.epixws.webservice.EpixWebServiceException",
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"), 
                      true
                     ));
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("checkAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "AccountNo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "AccountName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "custInfoResult"));
        oper.setReturnClass(com.nirvasoft.epixws.webservice.CustInfoResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"),
                      "com.nirvasoft.epixws.webservice.EpixWebServiceException",
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"), 
                      true
                     ));
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getEnGLInfo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Caption"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Value"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "Searchway"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "glInfoResult"));
        oper.setReturnClass(com.nirvasoft.epixws.webservice.GlInfoResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"),
                      "com.nirvasoft.epixws.webservice.EpixWebServiceException",
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"), 
                      true
                     ));
        _operations[18] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getCutomerInfo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "CustomerID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "customerInfo"));
        oper.setReturnClass(com.nirvasoft.epixws.webservice.CustomerInfo.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"),
                      "com.nirvasoft.epixws.webservice.EpixWebServiceException",
                      new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException"), 
                      true
                     ));
        _operations[19] = oper;

    }

    public EPIXWebServiceImplPortBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public EPIXWebServiceImplPortBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public EPIXWebServiceImplPortBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "accInfo");
            cachedSerQNames.add(qName);
            cls = com.nirvasoft.epixws.webservice.AccInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "accountActivityData");
            cachedSerQNames.add(qName);
            cls = com.nirvasoft.epixws.webservice.AccountActivityData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "accountActivityResult");
            cachedSerQNames.add(qName);
            cls = com.nirvasoft.epixws.webservice.AccountActivityResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "accountInfo");
            cachedSerQNames.add(qName);
            cls = com.nirvasoft.epixws.webservice.AccountInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "accountInfoResult");
            cachedSerQNames.add(qName);
            cls = com.nirvasoft.epixws.webservice.AccountInfoResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "atmData");
            cachedSerQNames.add(qName);
            cls = com.nirvasoft.epixws.webservice.AtmData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "atmLocationData");
            cachedSerQNames.add(qName);
            cls = com.nirvasoft.epixws.webservice.AtmLocationData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "atmLocationResult");
            cachedSerQNames.add(qName);
            cls = com.nirvasoft.epixws.webservice.AtmLocationResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "bankInfo");
            cachedSerQNames.add(qName);
            cls = com.nirvasoft.epixws.webservice.BankInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "bankInfoList");
            cachedSerQNames.add(qName);
            cls = com.nirvasoft.epixws.webservice.BankInfoList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "chequeAccArr");
            cachedSerQNames.add(qName);
            cls = com.nirvasoft.epixws.webservice.ChequeAccArr.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "chequeAccResult");
            cachedSerQNames.add(qName);
            cls = com.nirvasoft.epixws.webservice.ChequeAccResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "chequeEnquiryRes");
            cachedSerQNames.add(qName);
            cls = com.nirvasoft.epixws.webservice.ChequeEnquiryRes.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "commissionInfoResult");
            cachedSerQNames.add(qName);
            cls = com.nirvasoft.epixws.webservice.CommissionInfoResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "custAccInfoResult");
            cachedSerQNames.add(qName);
            cls = com.nirvasoft.epixws.webservice.CustAccInfoResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "custInfoResult");
            cachedSerQNames.add(qName);
            cls = com.nirvasoft.epixws.webservice.CustInfoResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "customerInfo");
            cachedSerQNames.add(qName);
            cls = com.nirvasoft.epixws.webservice.CustomerInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "epixFault");
            cachedSerQNames.add(qName);
            cls = com.nirvasoft.epixws.webservice.EpixFault.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "EpixWebServiceException");
            cachedSerQNames.add(qName);
            cls = com.nirvasoft.epixws.webservice.EpixWebServiceException.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "foreignExchangeRateData");
            cachedSerQNames.add(qName);
            cls = com.nirvasoft.epixws.webservice.ForeignExchangeRateData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "foreignExchangeRateResult");
            cachedSerQNames.add(qName);
            cls = com.nirvasoft.epixws.webservice.ForeignExchangeRateResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "glInfo");
            cachedSerQNames.add(qName);
            cls = com.nirvasoft.epixws.webservice.GlInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "glInfoResult");
            cachedSerQNames.add(qName);
            cls = com.nirvasoft.epixws.webservice.GlInfoResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "iftmArcMaintData");
            cachedSerQNames.add(qName);
            cls = com.nirvasoft.epixws.webservice.IftmArcMaintData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.nirvasoft.epixws.webservice.AccountActivityResult getAccountActivity(java.lang.String accNumber, java.lang.String customerNo, java.lang.String durationType, java.lang.String fromDate, java.lang.String toDate, java.lang.String currentPage, java.lang.String pageSize) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "getAccountActivity"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {accNumber, customerNo, durationType, fromDate, toDate, currentPage, pageSize});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.nirvasoft.epixws.webservice.AccountActivityResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.nirvasoft.epixws.webservice.AccountActivityResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.nirvasoft.epixws.webservice.AccountActivityResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.nirvasoft.epixws.webservice.EpixWebServiceException) {
              throw (com.nirvasoft.epixws.webservice.EpixWebServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.nirvasoft.epixws.webservice.CommissionInfoResult getCommissionByProductCode(java.lang.String XREF, java.lang.String productCode, java.lang.String txnAcc) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "getCommissionByProductCode"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {XREF, productCode, txnAcc});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.nirvasoft.epixws.webservice.CommissionInfoResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.nirvasoft.epixws.webservice.CommissionInfoResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.nirvasoft.epixws.webservice.CommissionInfoResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.nirvasoft.epixws.webservice.EpixWebServiceException) {
              throw (com.nirvasoft.epixws.webservice.EpixWebServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.nirvasoft.epixws.webservice.CustInfoResult getTransferAccountList(java.lang.String customerID) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "getTransferAccountList"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {customerID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.nirvasoft.epixws.webservice.CustInfoResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.nirvasoft.epixws.webservice.CustInfoResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.nirvasoft.epixws.webservice.CustInfoResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.nirvasoft.epixws.webservice.EpixWebServiceException) {
              throw (com.nirvasoft.epixws.webservice.EpixWebServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.nirvasoft.epixws.webservice.ForeignExchangeRateResult getForeignExchangeRates() throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "getForeignExchangeRates"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.nirvasoft.epixws.webservice.ForeignExchangeRateResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.nirvasoft.epixws.webservice.ForeignExchangeRateResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.nirvasoft.epixws.webservice.ForeignExchangeRateResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.nirvasoft.epixws.webservice.EpixWebServiceException) {
              throw (com.nirvasoft.epixws.webservice.EpixWebServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.nirvasoft.epixws.webservice.CustAccInfoResult getAccountCustomerInfobyCifNrc(java.lang.String cif, java.lang.String nrc) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "getAccountCustomerInfobyCifNrc"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {cif, nrc});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.nirvasoft.epixws.webservice.CustAccInfoResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.nirvasoft.epixws.webservice.CustAccInfoResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.nirvasoft.epixws.webservice.CustAccInfoResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.nirvasoft.epixws.webservice.EpixWebServiceException) {
              throw (com.nirvasoft.epixws.webservice.EpixWebServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.nirvasoft.epixws.webservice.AccountActivityResult getAccountActivityDownload(java.lang.String accNumber, java.lang.String customerNo, java.lang.String durationType, java.lang.String fromDate, java.lang.String toDate) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "getAccountActivityDownload"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {accNumber, customerNo, durationType, fromDate, toDate});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.nirvasoft.epixws.webservice.AccountActivityResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.nirvasoft.epixws.webservice.AccountActivityResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.nirvasoft.epixws.webservice.AccountActivityResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.nirvasoft.epixws.webservice.EpixWebServiceException) {
              throw (com.nirvasoft.epixws.webservice.EpixWebServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.nirvasoft.epixws.webservice.CustInfoResult getAccountInfoByAcctNo(java.lang.String accountNo) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "getAccountInfoByAcctNo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {accountNo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.nirvasoft.epixws.webservice.CustInfoResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.nirvasoft.epixws.webservice.CustInfoResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.nirvasoft.epixws.webservice.CustInfoResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.nirvasoft.epixws.webservice.EpixWebServiceException) {
              throw (com.nirvasoft.epixws.webservice.EpixWebServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.nirvasoft.epixws.webservice.CustInfoResult getAvailableAmountByAcctNo(java.lang.String accountNo) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "getAvailableAmountByAcctNo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {accountNo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.nirvasoft.epixws.webservice.CustInfoResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.nirvasoft.epixws.webservice.CustInfoResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.nirvasoft.epixws.webservice.CustInfoResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.nirvasoft.epixws.webservice.EpixWebServiceException) {
              throw (com.nirvasoft.epixws.webservice.EpixWebServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.nirvasoft.epixws.webservice.CustomerInfo getCustomerNameAndNrcByAccount(java.lang.String account) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "getCustomerNameAndNrcByAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {account});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.nirvasoft.epixws.webservice.CustomerInfo) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.nirvasoft.epixws.webservice.CustomerInfo) org.apache.axis.utils.JavaUtils.convert(_resp, com.nirvasoft.epixws.webservice.CustomerInfo.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.nirvasoft.epixws.webservice.EpixWebServiceException) {
              throw (com.nirvasoft.epixws.webservice.EpixWebServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.nirvasoft.epixws.webservice.CustAccInfoResult getAccountCustomerInfo(java.lang.String caption, java.lang.String value, java.lang.String searchway) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "getAccountCustomerInfo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {caption, value, searchway});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.nirvasoft.epixws.webservice.CustAccInfoResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.nirvasoft.epixws.webservice.CustAccInfoResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.nirvasoft.epixws.webservice.CustAccInfoResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.nirvasoft.epixws.webservice.EpixWebServiceException) {
              throw (com.nirvasoft.epixws.webservice.EpixWebServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.nirvasoft.epixws.webservice.ChequeEnquiryRes enquiryCheque(java.lang.String accountNo, java.lang.String chequeNo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "enquiryCheque"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {accountNo, chequeNo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.nirvasoft.epixws.webservice.ChequeEnquiryRes) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.nirvasoft.epixws.webservice.ChequeEnquiryRes) org.apache.axis.utils.JavaUtils.convert(_resp, com.nirvasoft.epixws.webservice.ChequeEnquiryRes.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.nirvasoft.epixws.webservice.BankInfoList getBankInfo() throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "getBankInfo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.nirvasoft.epixws.webservice.BankInfoList) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.nirvasoft.epixws.webservice.BankInfoList) org.apache.axis.utils.JavaUtils.convert(_resp, com.nirvasoft.epixws.webservice.BankInfoList.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.nirvasoft.epixws.webservice.EpixWebServiceException) {
              throw (com.nirvasoft.epixws.webservice.EpixWebServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.nirvasoft.epixws.webservice.ChequeAccResult getChequeAccArr(java.lang.String customerID) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "getChequeAccArr"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {customerID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.nirvasoft.epixws.webservice.ChequeAccResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.nirvasoft.epixws.webservice.ChequeAccResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.nirvasoft.epixws.webservice.ChequeAccResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.nirvasoft.epixws.webservice.EpixWebServiceException) {
              throw (com.nirvasoft.epixws.webservice.EpixWebServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.nirvasoft.epixws.webservice.CustomerInfo getGLData(java.lang.String account) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "getGLData"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {account});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.nirvasoft.epixws.webservice.CustomerInfo) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.nirvasoft.epixws.webservice.CustomerInfo) org.apache.axis.utils.JavaUtils.convert(_resp, com.nirvasoft.epixws.webservice.CustomerInfo.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.nirvasoft.epixws.webservice.EpixWebServiceException) {
              throw (com.nirvasoft.epixws.webservice.EpixWebServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.nirvasoft.epixws.webservice.AccountInfoResult getEnAccountInfo(java.lang.String caption, java.lang.String value, java.lang.String searchway) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "getEnAccountInfo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {caption, value, searchway});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.nirvasoft.epixws.webservice.AccountInfoResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.nirvasoft.epixws.webservice.AccountInfoResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.nirvasoft.epixws.webservice.AccountInfoResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.nirvasoft.epixws.webservice.EpixWebServiceException) {
              throw (com.nirvasoft.epixws.webservice.EpixWebServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.nirvasoft.epixws.webservice.AtmLocationResult getATMLocation(java.lang.String id) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "getATMLocation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {id});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.nirvasoft.epixws.webservice.AtmLocationResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.nirvasoft.epixws.webservice.AtmLocationResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.nirvasoft.epixws.webservice.AtmLocationResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.nirvasoft.epixws.webservice.CustInfoResult getAccountInfo(java.lang.String customerID, java.lang.String fixedDeposit) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "getAccountInfo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {customerID, fixedDeposit});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.nirvasoft.epixws.webservice.CustInfoResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.nirvasoft.epixws.webservice.CustInfoResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.nirvasoft.epixws.webservice.CustInfoResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.nirvasoft.epixws.webservice.EpixWebServiceException) {
              throw (com.nirvasoft.epixws.webservice.EpixWebServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.nirvasoft.epixws.webservice.CustInfoResult checkAccount(java.lang.String accountNo, java.lang.String accountName) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "checkAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {accountNo, accountName});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.nirvasoft.epixws.webservice.CustInfoResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.nirvasoft.epixws.webservice.CustInfoResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.nirvasoft.epixws.webservice.CustInfoResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.nirvasoft.epixws.webservice.EpixWebServiceException) {
              throw (com.nirvasoft.epixws.webservice.EpixWebServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.nirvasoft.epixws.webservice.GlInfoResult getEnGLInfo(java.lang.String caption, java.lang.String value, java.lang.String searchway) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "getEnGLInfo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {caption, value, searchway});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.nirvasoft.epixws.webservice.GlInfoResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.nirvasoft.epixws.webservice.GlInfoResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.nirvasoft.epixws.webservice.GlInfoResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.nirvasoft.epixws.webservice.EpixWebServiceException) {
              throw (com.nirvasoft.epixws.webservice.EpixWebServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.nirvasoft.epixws.webservice.CustomerInfo getCutomerInfo(java.lang.String customerID) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[19]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "getCutomerInfo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {customerID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.nirvasoft.epixws.webservice.CustomerInfo) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.nirvasoft.epixws.webservice.CustomerInfo) org.apache.axis.utils.JavaUtils.convert(_resp, com.nirvasoft.epixws.webservice.CustomerInfo.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.nirvasoft.epixws.webservice.EpixWebServiceException) {
              throw (com.nirvasoft.epixws.webservice.EpixWebServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

}
