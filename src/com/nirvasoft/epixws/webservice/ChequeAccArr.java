/**
 * ChequeAccArr.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.epixws.webservice;

public class ChequeAccArr  implements java.io.Serializable {
    private java.lang.String chequeAcc;

    private java.lang.String CCY;

    private double avlBal;

    public ChequeAccArr() {
    }

    public ChequeAccArr(
           java.lang.String chequeAcc,
           java.lang.String CCY,
           double avlBal) {
           this.chequeAcc = chequeAcc;
           this.CCY = CCY;
           this.avlBal = avlBal;
    }


    /**
     * Gets the chequeAcc value for this ChequeAccArr.
     * 
     * @return chequeAcc
     */
    public java.lang.String getChequeAcc() {
        return chequeAcc;
    }


    /**
     * Sets the chequeAcc value for this ChequeAccArr.
     * 
     * @param chequeAcc
     */
    public void setChequeAcc(java.lang.String chequeAcc) {
        this.chequeAcc = chequeAcc;
    }


    /**
     * Gets the CCY value for this ChequeAccArr.
     * 
     * @return CCY
     */
    public java.lang.String getCCY() {
        return CCY;
    }


    /**
     * Sets the CCY value for this ChequeAccArr.
     * 
     * @param CCY
     */
    public void setCCY(java.lang.String CCY) {
        this.CCY = CCY;
    }


    /**
     * Gets the avlBal value for this ChequeAccArr.
     * 
     * @return avlBal
     */
    public double getAvlBal() {
        return avlBal;
    }


    /**
     * Sets the avlBal value for this ChequeAccArr.
     * 
     * @param avlBal
     */
    public void setAvlBal(double avlBal) {
        this.avlBal = avlBal;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ChequeAccArr)) return false;
        ChequeAccArr other = (ChequeAccArr) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.chequeAcc==null && other.getChequeAcc()==null) || 
             (this.chequeAcc!=null &&
              this.chequeAcc.equals(other.getChequeAcc()))) &&
            ((this.CCY==null && other.getCCY()==null) || 
             (this.CCY!=null &&
              this.CCY.equals(other.getCCY()))) &&
            this.avlBal == other.getAvlBal();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getChequeAcc() != null) {
            _hashCode += getChequeAcc().hashCode();
        }
        if (getCCY() != null) {
            _hashCode += getCCY().hashCode();
        }
        _hashCode += new Double(getAvlBal()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ChequeAccArr.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "chequeAccArr"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chequeAcc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ChequeAcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CCY");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CCY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avlBal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AvlBal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
