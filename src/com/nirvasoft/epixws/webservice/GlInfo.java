/**
 * GlInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.epixws.webservice;

public class GlInfo  implements java.io.Serializable {
    private java.lang.String accNo;

    private java.lang.String accdesc;

    private java.lang.String subGroup;

    private java.lang.String subGroupDesc;

    private java.lang.String accTypeDescription;

    public GlInfo() {
    }

    public GlInfo(
           java.lang.String accNo,
           java.lang.String accdesc,
           java.lang.String subGroup,
           java.lang.String subGroupDesc,
           java.lang.String accTypeDescription) {
           this.accNo = accNo;
           this.accdesc = accdesc;
           this.subGroup = subGroup;
           this.subGroupDesc = subGroupDesc;
           this.accTypeDescription = accTypeDescription;
    }


    /**
     * Gets the accNo value for this GlInfo.
     * 
     * @return accNo
     */
    public java.lang.String getAccNo() {
        return accNo;
    }


    /**
     * Sets the accNo value for this GlInfo.
     * 
     * @param accNo
     */
    public void setAccNo(java.lang.String accNo) {
        this.accNo = accNo;
    }


    /**
     * Gets the accdesc value for this GlInfo.
     * 
     * @return accdesc
     */
    public java.lang.String getAccdesc() {
        return accdesc;
    }


    /**
     * Sets the accdesc value for this GlInfo.
     * 
     * @param accdesc
     */
    public void setAccdesc(java.lang.String accdesc) {
        this.accdesc = accdesc;
    }


    /**
     * Gets the subGroup value for this GlInfo.
     * 
     * @return subGroup
     */
    public java.lang.String getSubGroup() {
        return subGroup;
    }


    /**
     * Sets the subGroup value for this GlInfo.
     * 
     * @param subGroup
     */
    public void setSubGroup(java.lang.String subGroup) {
        this.subGroup = subGroup;
    }


    /**
     * Gets the subGroupDesc value for this GlInfo.
     * 
     * @return subGroupDesc
     */
    public java.lang.String getSubGroupDesc() {
        return subGroupDesc;
    }


    /**
     * Sets the subGroupDesc value for this GlInfo.
     * 
     * @param subGroupDesc
     */
    public void setSubGroupDesc(java.lang.String subGroupDesc) {
        this.subGroupDesc = subGroupDesc;
    }


    /**
     * Gets the accTypeDescription value for this GlInfo.
     * 
     * @return accTypeDescription
     */
    public java.lang.String getAccTypeDescription() {
        return accTypeDescription;
    }


    /**
     * Sets the accTypeDescription value for this GlInfo.
     * 
     * @param accTypeDescription
     */
    public void setAccTypeDescription(java.lang.String accTypeDescription) {
        this.accTypeDescription = accTypeDescription;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GlInfo)) return false;
        GlInfo other = (GlInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.accNo==null && other.getAccNo()==null) || 
             (this.accNo!=null &&
              this.accNo.equals(other.getAccNo()))) &&
            ((this.accdesc==null && other.getAccdesc()==null) || 
             (this.accdesc!=null &&
              this.accdesc.equals(other.getAccdesc()))) &&
            ((this.subGroup==null && other.getSubGroup()==null) || 
             (this.subGroup!=null &&
              this.subGroup.equals(other.getSubGroup()))) &&
            ((this.subGroupDesc==null && other.getSubGroupDesc()==null) || 
             (this.subGroupDesc!=null &&
              this.subGroupDesc.equals(other.getSubGroupDesc()))) &&
            ((this.accTypeDescription==null && other.getAccTypeDescription()==null) || 
             (this.accTypeDescription!=null &&
              this.accTypeDescription.equals(other.getAccTypeDescription())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAccNo() != null) {
            _hashCode += getAccNo().hashCode();
        }
        if (getAccdesc() != null) {
            _hashCode += getAccdesc().hashCode();
        }
        if (getSubGroup() != null) {
            _hashCode += getSubGroup().hashCode();
        }
        if (getSubGroupDesc() != null) {
            _hashCode += getSubGroupDesc().hashCode();
        }
        if (getAccTypeDescription() != null) {
            _hashCode += getAccTypeDescription().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GlInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "glInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accNo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AccNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accdesc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Accdesc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subGroup");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SubGroup"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subGroupDesc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SubGroupDesc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accTypeDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AccTypeDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
