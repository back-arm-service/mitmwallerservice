/**
 * EPIXWebService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.epixws.webservice;

public interface EPIXWebService extends java.rmi.Remote {
    public com.nirvasoft.epixws.webservice.AccountActivityResult getAccountActivity(java.lang.String accNumber, java.lang.String customerNo, java.lang.String durationType, java.lang.String fromDate, java.lang.String toDate, java.lang.String currentPage, java.lang.String pageSize) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException;
    public com.nirvasoft.epixws.webservice.CommissionInfoResult getCommissionByProductCode(java.lang.String XREF, java.lang.String productCode, java.lang.String txnAcc) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException;
    public com.nirvasoft.epixws.webservice.CustInfoResult getTransferAccountList(java.lang.String customerID) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException;
    public com.nirvasoft.epixws.webservice.ForeignExchangeRateResult getForeignExchangeRates() throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException;
    public com.nirvasoft.epixws.webservice.CustAccInfoResult getAccountCustomerInfobyCifNrc(java.lang.String cif, java.lang.String nrc) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException;
    public com.nirvasoft.epixws.webservice.AccountActivityResult getAccountActivityDownload(java.lang.String accNumber, java.lang.String customerNo, java.lang.String durationType, java.lang.String fromDate, java.lang.String toDate) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException;
    public com.nirvasoft.epixws.webservice.CustInfoResult getAccountInfoByAcctNo(java.lang.String accountNo) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException;
    public com.nirvasoft.epixws.webservice.CustInfoResult getAvailableAmountByAcctNo(java.lang.String accountNo) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException;
    public com.nirvasoft.epixws.webservice.CustomerInfo getCustomerNameAndNrcByAccount(java.lang.String account) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException;
    public com.nirvasoft.epixws.webservice.CustAccInfoResult getAccountCustomerInfo(java.lang.String caption, java.lang.String value, java.lang.String searchway) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException;
    public com.nirvasoft.epixws.webservice.ChequeEnquiryRes enquiryCheque(java.lang.String accountNo, java.lang.String chequeNo) throws java.rmi.RemoteException;
    public com.nirvasoft.epixws.webservice.BankInfoList getBankInfo() throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException;
    public com.nirvasoft.epixws.webservice.ChequeAccResult getChequeAccArr(java.lang.String customerID) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException;
    public com.nirvasoft.epixws.webservice.CustomerInfo getGLData(java.lang.String account) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException;
    public com.nirvasoft.epixws.webservice.AccountInfoResult getEnAccountInfo(java.lang.String caption, java.lang.String value, java.lang.String searchway) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException;
    public com.nirvasoft.epixws.webservice.AtmLocationResult getATMLocation(java.lang.String id) throws java.rmi.RemoteException;
    public com.nirvasoft.epixws.webservice.CustInfoResult getAccountInfo(java.lang.String customerID, java.lang.String fixedDeposit) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException;
    public com.nirvasoft.epixws.webservice.CustInfoResult checkAccount(java.lang.String accountNo, java.lang.String accountName) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException;
    public com.nirvasoft.epixws.webservice.GlInfoResult getEnGLInfo(java.lang.String caption, java.lang.String value, java.lang.String searchway) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException;
    public com.nirvasoft.epixws.webservice.CustomerInfo getCutomerInfo(java.lang.String customerID) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException;
}
