/**
 * ForeignExchangeRateData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.epixws.webservice;

public class ForeignExchangeRateData  implements java.io.Serializable {
    private java.lang.String CCY1;

    private java.lang.String CCY2;

    private java.lang.String systemDate;

    private double numBuyRate;

    private double numSellRate;

    private double numMidRate;

    private java.lang.String numQuotation;

    private java.lang.String txtRateType;

    private java.lang.String codMethod;

    private java.lang.String DTime;

    private java.lang.String slno;

    private double numChqBuyRate;

    private double numChqSellRate;

    private double numXFERSellRate;

    private double numXFERBuyRate;

    private java.lang.String namCURRFrom;

    private java.lang.String namCURRTo;

    public ForeignExchangeRateData() {
    }

    public ForeignExchangeRateData(
           java.lang.String CCY1,
           java.lang.String CCY2,
           java.lang.String systemDate,
           double numBuyRate,
           double numSellRate,
           double numMidRate,
           java.lang.String numQuotation,
           java.lang.String txtRateType,
           java.lang.String codMethod,
           java.lang.String DTime,
           java.lang.String slno,
           double numChqBuyRate,
           double numChqSellRate,
           double numXFERSellRate,
           double numXFERBuyRate,
           java.lang.String namCURRFrom,
           java.lang.String namCURRTo) {
           this.CCY1 = CCY1;
           this.CCY2 = CCY2;
           this.systemDate = systemDate;
           this.numBuyRate = numBuyRate;
           this.numSellRate = numSellRate;
           this.numMidRate = numMidRate;
           this.numQuotation = numQuotation;
           this.txtRateType = txtRateType;
           this.codMethod = codMethod;
           this.DTime = DTime;
           this.slno = slno;
           this.numChqBuyRate = numChqBuyRate;
           this.numChqSellRate = numChqSellRate;
           this.numXFERSellRate = numXFERSellRate;
           this.numXFERBuyRate = numXFERBuyRate;
           this.namCURRFrom = namCURRFrom;
           this.namCURRTo = namCURRTo;
    }


    /**
     * Gets the CCY1 value for this ForeignExchangeRateData.
     * 
     * @return CCY1
     */
    public java.lang.String getCCY1() {
        return CCY1;
    }


    /**
     * Sets the CCY1 value for this ForeignExchangeRateData.
     * 
     * @param CCY1
     */
    public void setCCY1(java.lang.String CCY1) {
        this.CCY1 = CCY1;
    }


    /**
     * Gets the CCY2 value for this ForeignExchangeRateData.
     * 
     * @return CCY2
     */
    public java.lang.String getCCY2() {
        return CCY2;
    }


    /**
     * Sets the CCY2 value for this ForeignExchangeRateData.
     * 
     * @param CCY2
     */
    public void setCCY2(java.lang.String CCY2) {
        this.CCY2 = CCY2;
    }


    /**
     * Gets the systemDate value for this ForeignExchangeRateData.
     * 
     * @return systemDate
     */
    public java.lang.String getSystemDate() {
        return systemDate;
    }


    /**
     * Sets the systemDate value for this ForeignExchangeRateData.
     * 
     * @param systemDate
     */
    public void setSystemDate(java.lang.String systemDate) {
        this.systemDate = systemDate;
    }


    /**
     * Gets the numBuyRate value for this ForeignExchangeRateData.
     * 
     * @return numBuyRate
     */
    public double getNumBuyRate() {
        return numBuyRate;
    }


    /**
     * Sets the numBuyRate value for this ForeignExchangeRateData.
     * 
     * @param numBuyRate
     */
    public void setNumBuyRate(double numBuyRate) {
        this.numBuyRate = numBuyRate;
    }


    /**
     * Gets the numSellRate value for this ForeignExchangeRateData.
     * 
     * @return numSellRate
     */
    public double getNumSellRate() {
        return numSellRate;
    }


    /**
     * Sets the numSellRate value for this ForeignExchangeRateData.
     * 
     * @param numSellRate
     */
    public void setNumSellRate(double numSellRate) {
        this.numSellRate = numSellRate;
    }


    /**
     * Gets the numMidRate value for this ForeignExchangeRateData.
     * 
     * @return numMidRate
     */
    public double getNumMidRate() {
        return numMidRate;
    }


    /**
     * Sets the numMidRate value for this ForeignExchangeRateData.
     * 
     * @param numMidRate
     */
    public void setNumMidRate(double numMidRate) {
        this.numMidRate = numMidRate;
    }


    /**
     * Gets the numQuotation value for this ForeignExchangeRateData.
     * 
     * @return numQuotation
     */
    public java.lang.String getNumQuotation() {
        return numQuotation;
    }


    /**
     * Sets the numQuotation value for this ForeignExchangeRateData.
     * 
     * @param numQuotation
     */
    public void setNumQuotation(java.lang.String numQuotation) {
        this.numQuotation = numQuotation;
    }


    /**
     * Gets the txtRateType value for this ForeignExchangeRateData.
     * 
     * @return txtRateType
     */
    public java.lang.String getTxtRateType() {
        return txtRateType;
    }


    /**
     * Sets the txtRateType value for this ForeignExchangeRateData.
     * 
     * @param txtRateType
     */
    public void setTxtRateType(java.lang.String txtRateType) {
        this.txtRateType = txtRateType;
    }


    /**
     * Gets the codMethod value for this ForeignExchangeRateData.
     * 
     * @return codMethod
     */
    public java.lang.String getCodMethod() {
        return codMethod;
    }


    /**
     * Sets the codMethod value for this ForeignExchangeRateData.
     * 
     * @param codMethod
     */
    public void setCodMethod(java.lang.String codMethod) {
        this.codMethod = codMethod;
    }


    /**
     * Gets the DTime value for this ForeignExchangeRateData.
     * 
     * @return DTime
     */
    public java.lang.String getDTime() {
        return DTime;
    }


    /**
     * Sets the DTime value for this ForeignExchangeRateData.
     * 
     * @param DTime
     */
    public void setDTime(java.lang.String DTime) {
        this.DTime = DTime;
    }


    /**
     * Gets the slno value for this ForeignExchangeRateData.
     * 
     * @return slno
     */
    public java.lang.String getSlno() {
        return slno;
    }


    /**
     * Sets the slno value for this ForeignExchangeRateData.
     * 
     * @param slno
     */
    public void setSlno(java.lang.String slno) {
        this.slno = slno;
    }


    /**
     * Gets the numChqBuyRate value for this ForeignExchangeRateData.
     * 
     * @return numChqBuyRate
     */
    public double getNumChqBuyRate() {
        return numChqBuyRate;
    }


    /**
     * Sets the numChqBuyRate value for this ForeignExchangeRateData.
     * 
     * @param numChqBuyRate
     */
    public void setNumChqBuyRate(double numChqBuyRate) {
        this.numChqBuyRate = numChqBuyRate;
    }


    /**
     * Gets the numChqSellRate value for this ForeignExchangeRateData.
     * 
     * @return numChqSellRate
     */
    public double getNumChqSellRate() {
        return numChqSellRate;
    }


    /**
     * Sets the numChqSellRate value for this ForeignExchangeRateData.
     * 
     * @param numChqSellRate
     */
    public void setNumChqSellRate(double numChqSellRate) {
        this.numChqSellRate = numChqSellRate;
    }


    /**
     * Gets the numXFERSellRate value for this ForeignExchangeRateData.
     * 
     * @return numXFERSellRate
     */
    public double getNumXFERSellRate() {
        return numXFERSellRate;
    }


    /**
     * Sets the numXFERSellRate value for this ForeignExchangeRateData.
     * 
     * @param numXFERSellRate
     */
    public void setNumXFERSellRate(double numXFERSellRate) {
        this.numXFERSellRate = numXFERSellRate;
    }


    /**
     * Gets the numXFERBuyRate value for this ForeignExchangeRateData.
     * 
     * @return numXFERBuyRate
     */
    public double getNumXFERBuyRate() {
        return numXFERBuyRate;
    }


    /**
     * Sets the numXFERBuyRate value for this ForeignExchangeRateData.
     * 
     * @param numXFERBuyRate
     */
    public void setNumXFERBuyRate(double numXFERBuyRate) {
        this.numXFERBuyRate = numXFERBuyRate;
    }


    /**
     * Gets the namCURRFrom value for this ForeignExchangeRateData.
     * 
     * @return namCURRFrom
     */
    public java.lang.String getNamCURRFrom() {
        return namCURRFrom;
    }


    /**
     * Sets the namCURRFrom value for this ForeignExchangeRateData.
     * 
     * @param namCURRFrom
     */
    public void setNamCURRFrom(java.lang.String namCURRFrom) {
        this.namCURRFrom = namCURRFrom;
    }


    /**
     * Gets the namCURRTo value for this ForeignExchangeRateData.
     * 
     * @return namCURRTo
     */
    public java.lang.String getNamCURRTo() {
        return namCURRTo;
    }


    /**
     * Sets the namCURRTo value for this ForeignExchangeRateData.
     * 
     * @param namCURRTo
     */
    public void setNamCURRTo(java.lang.String namCURRTo) {
        this.namCURRTo = namCURRTo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ForeignExchangeRateData)) return false;
        ForeignExchangeRateData other = (ForeignExchangeRateData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.CCY1==null && other.getCCY1()==null) || 
             (this.CCY1!=null &&
              this.CCY1.equals(other.getCCY1()))) &&
            ((this.CCY2==null && other.getCCY2()==null) || 
             (this.CCY2!=null &&
              this.CCY2.equals(other.getCCY2()))) &&
            ((this.systemDate==null && other.getSystemDate()==null) || 
             (this.systemDate!=null &&
              this.systemDate.equals(other.getSystemDate()))) &&
            this.numBuyRate == other.getNumBuyRate() &&
            this.numSellRate == other.getNumSellRate() &&
            this.numMidRate == other.getNumMidRate() &&
            ((this.numQuotation==null && other.getNumQuotation()==null) || 
             (this.numQuotation!=null &&
              this.numQuotation.equals(other.getNumQuotation()))) &&
            ((this.txtRateType==null && other.getTxtRateType()==null) || 
             (this.txtRateType!=null &&
              this.txtRateType.equals(other.getTxtRateType()))) &&
            ((this.codMethod==null && other.getCodMethod()==null) || 
             (this.codMethod!=null &&
              this.codMethod.equals(other.getCodMethod()))) &&
            ((this.DTime==null && other.getDTime()==null) || 
             (this.DTime!=null &&
              this.DTime.equals(other.getDTime()))) &&
            ((this.slno==null && other.getSlno()==null) || 
             (this.slno!=null &&
              this.slno.equals(other.getSlno()))) &&
            this.numChqBuyRate == other.getNumChqBuyRate() &&
            this.numChqSellRate == other.getNumChqSellRate() &&
            this.numXFERSellRate == other.getNumXFERSellRate() &&
            this.numXFERBuyRate == other.getNumXFERBuyRate() &&
            ((this.namCURRFrom==null && other.getNamCURRFrom()==null) || 
             (this.namCURRFrom!=null &&
              this.namCURRFrom.equals(other.getNamCURRFrom()))) &&
            ((this.namCURRTo==null && other.getNamCURRTo()==null) || 
             (this.namCURRTo!=null &&
              this.namCURRTo.equals(other.getNamCURRTo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCCY1() != null) {
            _hashCode += getCCY1().hashCode();
        }
        if (getCCY2() != null) {
            _hashCode += getCCY2().hashCode();
        }
        if (getSystemDate() != null) {
            _hashCode += getSystemDate().hashCode();
        }
        _hashCode += new Double(getNumBuyRate()).hashCode();
        _hashCode += new Double(getNumSellRate()).hashCode();
        _hashCode += new Double(getNumMidRate()).hashCode();
        if (getNumQuotation() != null) {
            _hashCode += getNumQuotation().hashCode();
        }
        if (getTxtRateType() != null) {
            _hashCode += getTxtRateType().hashCode();
        }
        if (getCodMethod() != null) {
            _hashCode += getCodMethod().hashCode();
        }
        if (getDTime() != null) {
            _hashCode += getDTime().hashCode();
        }
        if (getSlno() != null) {
            _hashCode += getSlno().hashCode();
        }
        _hashCode += new Double(getNumChqBuyRate()).hashCode();
        _hashCode += new Double(getNumChqSellRate()).hashCode();
        _hashCode += new Double(getNumXFERSellRate()).hashCode();
        _hashCode += new Double(getNumXFERBuyRate()).hashCode();
        if (getNamCURRFrom() != null) {
            _hashCode += getNamCURRFrom().hashCode();
        }
        if (getNamCURRTo() != null) {
            _hashCode += getNamCURRTo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ForeignExchangeRateData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "foreignExchangeRateData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CCY1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CCY1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CCY2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CCY2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SystemDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numBuyRate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NumBuyRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numSellRate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NumSellRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numMidRate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NumMidRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numQuotation");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NumQuotation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txtRateType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TxtRateType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMethod");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodMethod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DTime");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("slno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Slno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numChqBuyRate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NumChqBuyRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numChqSellRate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NumChqSellRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numXFERSellRate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NumXFERSellRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numXFERBuyRate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NumXFERBuyRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("namCURRFrom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NamCURRFrom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("namCURRTo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NamCURRTo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
