/**
 * IftmArcMaintData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.epixws.webservice;

public class IftmArcMaintData  implements java.io.Serializable {
    private java.lang.String accNumber;

    private java.lang.String codechgdesc;

    private java.lang.String codechgdesc1;

    private java.lang.String codechgtype;

    private java.lang.String codechgtype1;

    private java.lang.String codetranstype;

    private java.lang.String glcode1;

    private java.lang.String glcode2;

    private java.lang.String nettingcode;

    private java.lang.String nettingcode1;

    private java.lang.String txncode;

    private java.lang.String txncode1;

    public IftmArcMaintData() {
    }

    public IftmArcMaintData(
           java.lang.String accNumber,
           java.lang.String codechgdesc,
           java.lang.String codechgdesc1,
           java.lang.String codechgtype,
           java.lang.String codechgtype1,
           java.lang.String codetranstype,
           java.lang.String glcode1,
           java.lang.String glcode2,
           java.lang.String nettingcode,
           java.lang.String nettingcode1,
           java.lang.String txncode,
           java.lang.String txncode1) {
           this.accNumber = accNumber;
           this.codechgdesc = codechgdesc;
           this.codechgdesc1 = codechgdesc1;
           this.codechgtype = codechgtype;
           this.codechgtype1 = codechgtype1;
           this.codetranstype = codetranstype;
           this.glcode1 = glcode1;
           this.glcode2 = glcode2;
           this.nettingcode = nettingcode;
           this.nettingcode1 = nettingcode1;
           this.txncode = txncode;
           this.txncode1 = txncode1;
    }


    /**
     * Gets the accNumber value for this IftmArcMaintData.
     * 
     * @return accNumber
     */
    public java.lang.String getAccNumber() {
        return accNumber;
    }


    /**
     * Sets the accNumber value for this IftmArcMaintData.
     * 
     * @param accNumber
     */
    public void setAccNumber(java.lang.String accNumber) {
        this.accNumber = accNumber;
    }


    /**
     * Gets the codechgdesc value for this IftmArcMaintData.
     * 
     * @return codechgdesc
     */
    public java.lang.String getCodechgdesc() {
        return codechgdesc;
    }


    /**
     * Sets the codechgdesc value for this IftmArcMaintData.
     * 
     * @param codechgdesc
     */
    public void setCodechgdesc(java.lang.String codechgdesc) {
        this.codechgdesc = codechgdesc;
    }


    /**
     * Gets the codechgdesc1 value for this IftmArcMaintData.
     * 
     * @return codechgdesc1
     */
    public java.lang.String getCodechgdesc1() {
        return codechgdesc1;
    }


    /**
     * Sets the codechgdesc1 value for this IftmArcMaintData.
     * 
     * @param codechgdesc1
     */
    public void setCodechgdesc1(java.lang.String codechgdesc1) {
        this.codechgdesc1 = codechgdesc1;
    }


    /**
     * Gets the codechgtype value for this IftmArcMaintData.
     * 
     * @return codechgtype
     */
    public java.lang.String getCodechgtype() {
        return codechgtype;
    }


    /**
     * Sets the codechgtype value for this IftmArcMaintData.
     * 
     * @param codechgtype
     */
    public void setCodechgtype(java.lang.String codechgtype) {
        this.codechgtype = codechgtype;
    }


    /**
     * Gets the codechgtype1 value for this IftmArcMaintData.
     * 
     * @return codechgtype1
     */
    public java.lang.String getCodechgtype1() {
        return codechgtype1;
    }


    /**
     * Sets the codechgtype1 value for this IftmArcMaintData.
     * 
     * @param codechgtype1
     */
    public void setCodechgtype1(java.lang.String codechgtype1) {
        this.codechgtype1 = codechgtype1;
    }


    /**
     * Gets the codetranstype value for this IftmArcMaintData.
     * 
     * @return codetranstype
     */
    public java.lang.String getCodetranstype() {
        return codetranstype;
    }


    /**
     * Sets the codetranstype value for this IftmArcMaintData.
     * 
     * @param codetranstype
     */
    public void setCodetranstype(java.lang.String codetranstype) {
        this.codetranstype = codetranstype;
    }


    /**
     * Gets the glcode1 value for this IftmArcMaintData.
     * 
     * @return glcode1
     */
    public java.lang.String getGlcode1() {
        return glcode1;
    }


    /**
     * Sets the glcode1 value for this IftmArcMaintData.
     * 
     * @param glcode1
     */
    public void setGlcode1(java.lang.String glcode1) {
        this.glcode1 = glcode1;
    }


    /**
     * Gets the glcode2 value for this IftmArcMaintData.
     * 
     * @return glcode2
     */
    public java.lang.String getGlcode2() {
        return glcode2;
    }


    /**
     * Sets the glcode2 value for this IftmArcMaintData.
     * 
     * @param glcode2
     */
    public void setGlcode2(java.lang.String glcode2) {
        this.glcode2 = glcode2;
    }


    /**
     * Gets the nettingcode value for this IftmArcMaintData.
     * 
     * @return nettingcode
     */
    public java.lang.String getNettingcode() {
        return nettingcode;
    }


    /**
     * Sets the nettingcode value for this IftmArcMaintData.
     * 
     * @param nettingcode
     */
    public void setNettingcode(java.lang.String nettingcode) {
        this.nettingcode = nettingcode;
    }


    /**
     * Gets the nettingcode1 value for this IftmArcMaintData.
     * 
     * @return nettingcode1
     */
    public java.lang.String getNettingcode1() {
        return nettingcode1;
    }


    /**
     * Sets the nettingcode1 value for this IftmArcMaintData.
     * 
     * @param nettingcode1
     */
    public void setNettingcode1(java.lang.String nettingcode1) {
        this.nettingcode1 = nettingcode1;
    }


    /**
     * Gets the txncode value for this IftmArcMaintData.
     * 
     * @return txncode
     */
    public java.lang.String getTxncode() {
        return txncode;
    }


    /**
     * Sets the txncode value for this IftmArcMaintData.
     * 
     * @param txncode
     */
    public void setTxncode(java.lang.String txncode) {
        this.txncode = txncode;
    }


    /**
     * Gets the txncode1 value for this IftmArcMaintData.
     * 
     * @return txncode1
     */
    public java.lang.String getTxncode1() {
        return txncode1;
    }


    /**
     * Sets the txncode1 value for this IftmArcMaintData.
     * 
     * @param txncode1
     */
    public void setTxncode1(java.lang.String txncode1) {
        this.txncode1 = txncode1;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof IftmArcMaintData)) return false;
        IftmArcMaintData other = (IftmArcMaintData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.accNumber==null && other.getAccNumber()==null) || 
             (this.accNumber!=null &&
              this.accNumber.equals(other.getAccNumber()))) &&
            ((this.codechgdesc==null && other.getCodechgdesc()==null) || 
             (this.codechgdesc!=null &&
              this.codechgdesc.equals(other.getCodechgdesc()))) &&
            ((this.codechgdesc1==null && other.getCodechgdesc1()==null) || 
             (this.codechgdesc1!=null &&
              this.codechgdesc1.equals(other.getCodechgdesc1()))) &&
            ((this.codechgtype==null && other.getCodechgtype()==null) || 
             (this.codechgtype!=null &&
              this.codechgtype.equals(other.getCodechgtype()))) &&
            ((this.codechgtype1==null && other.getCodechgtype1()==null) || 
             (this.codechgtype1!=null &&
              this.codechgtype1.equals(other.getCodechgtype1()))) &&
            ((this.codetranstype==null && other.getCodetranstype()==null) || 
             (this.codetranstype!=null &&
              this.codetranstype.equals(other.getCodetranstype()))) &&
            ((this.glcode1==null && other.getGlcode1()==null) || 
             (this.glcode1!=null &&
              this.glcode1.equals(other.getGlcode1()))) &&
            ((this.glcode2==null && other.getGlcode2()==null) || 
             (this.glcode2!=null &&
              this.glcode2.equals(other.getGlcode2()))) &&
            ((this.nettingcode==null && other.getNettingcode()==null) || 
             (this.nettingcode!=null &&
              this.nettingcode.equals(other.getNettingcode()))) &&
            ((this.nettingcode1==null && other.getNettingcode1()==null) || 
             (this.nettingcode1!=null &&
              this.nettingcode1.equals(other.getNettingcode1()))) &&
            ((this.txncode==null && other.getTxncode()==null) || 
             (this.txncode!=null &&
              this.txncode.equals(other.getTxncode()))) &&
            ((this.txncode1==null && other.getTxncode1()==null) || 
             (this.txncode1!=null &&
              this.txncode1.equals(other.getTxncode1())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAccNumber() != null) {
            _hashCode += getAccNumber().hashCode();
        }
        if (getCodechgdesc() != null) {
            _hashCode += getCodechgdesc().hashCode();
        }
        if (getCodechgdesc1() != null) {
            _hashCode += getCodechgdesc1().hashCode();
        }
        if (getCodechgtype() != null) {
            _hashCode += getCodechgtype().hashCode();
        }
        if (getCodechgtype1() != null) {
            _hashCode += getCodechgtype1().hashCode();
        }
        if (getCodetranstype() != null) {
            _hashCode += getCodetranstype().hashCode();
        }
        if (getGlcode1() != null) {
            _hashCode += getGlcode1().hashCode();
        }
        if (getGlcode2() != null) {
            _hashCode += getGlcode2().hashCode();
        }
        if (getNettingcode() != null) {
            _hashCode += getNettingcode().hashCode();
        }
        if (getNettingcode1() != null) {
            _hashCode += getNettingcode1().hashCode();
        }
        if (getTxncode() != null) {
            _hashCode += getTxncode().hashCode();
        }
        if (getTxncode1() != null) {
            _hashCode += getTxncode1().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(IftmArcMaintData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "iftmArcMaintData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "accNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codechgdesc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codechgdesc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codechgdesc1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codechgdesc1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codechgtype");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codechgtype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codechgtype1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codechgtype1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codetranstype");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codetranstype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("glcode1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "glcode1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("glcode2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "glcode2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nettingcode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nettingcode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nettingcode1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nettingcode1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txncode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "txncode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txncode1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "txncode1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
