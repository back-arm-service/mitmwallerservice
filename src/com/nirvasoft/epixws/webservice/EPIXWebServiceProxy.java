package com.nirvasoft.epixws.webservice;

public class EPIXWebServiceProxy implements com.nirvasoft.epixws.webservice.EPIXWebService {
  private String _endpoint = null;
  private com.nirvasoft.epixws.webservice.EPIXWebService ePIXWebService = null;
  
  public EPIXWebServiceProxy() {
    _initEPIXWebServiceProxy();
  }
  
  public EPIXWebServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initEPIXWebServiceProxy();
  }
  
  private void _initEPIXWebServiceProxy() {
    try {
      ePIXWebService = (new com.nirvasoft.epixws.webservice.EPIXWebServiceImplServiceLocator()).getEPIXWebServiceImplPort();
      if (ePIXWebService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)ePIXWebService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)ePIXWebService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (ePIXWebService != null)
      ((javax.xml.rpc.Stub)ePIXWebService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.nirvasoft.epixws.webservice.EPIXWebService getEPIXWebService() {
    if (ePIXWebService == null)
      _initEPIXWebServiceProxy();
    return ePIXWebService;
  }
  
  public com.nirvasoft.epixws.webservice.AccountActivityResult getAccountActivity(java.lang.String accNumber, java.lang.String customerNo, java.lang.String durationType, java.lang.String fromDate, java.lang.String toDate, java.lang.String currentPage, java.lang.String pageSize) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException{
    if (ePIXWebService == null)
      _initEPIXWebServiceProxy();
    return ePIXWebService.getAccountActivity(accNumber, customerNo, durationType, fromDate, toDate, currentPage, pageSize);
  }
  
  public com.nirvasoft.epixws.webservice.CommissionInfoResult getCommissionByProductCode(java.lang.String XREF, java.lang.String productCode, java.lang.String txnAcc) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException{
    if (ePIXWebService == null)
      _initEPIXWebServiceProxy();
    return ePIXWebService.getCommissionByProductCode(XREF, productCode, txnAcc);
  }
  
  public com.nirvasoft.epixws.webservice.CustInfoResult getTransferAccountList(java.lang.String customerID) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException{
    if (ePIXWebService == null)
      _initEPIXWebServiceProxy();
    return ePIXWebService.getTransferAccountList(customerID);
  }
  
  public com.nirvasoft.epixws.webservice.ForeignExchangeRateResult getForeignExchangeRates() throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException{
    if (ePIXWebService == null)
      _initEPIXWebServiceProxy();
    return ePIXWebService.getForeignExchangeRates();
  }
  
  public com.nirvasoft.epixws.webservice.CustAccInfoResult getAccountCustomerInfobyCifNrc(java.lang.String cif, java.lang.String nrc) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException{
    if (ePIXWebService == null)
      _initEPIXWebServiceProxy();
    return ePIXWebService.getAccountCustomerInfobyCifNrc(cif, nrc);
  }
  
  public com.nirvasoft.epixws.webservice.AccountActivityResult getAccountActivityDownload(java.lang.String accNumber, java.lang.String customerNo, java.lang.String durationType, java.lang.String fromDate, java.lang.String toDate) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException{
    if (ePIXWebService == null)
      _initEPIXWebServiceProxy();
    return ePIXWebService.getAccountActivityDownload(accNumber, customerNo, durationType, fromDate, toDate);
  }
  
  public com.nirvasoft.epixws.webservice.CustInfoResult getAccountInfoByAcctNo(java.lang.String accountNo) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException{
    if (ePIXWebService == null)
      _initEPIXWebServiceProxy();
    return ePIXWebService.getAccountInfoByAcctNo(accountNo);
  }
  
  public com.nirvasoft.epixws.webservice.CustInfoResult getAvailableAmountByAcctNo(java.lang.String accountNo) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException{
    if (ePIXWebService == null)
      _initEPIXWebServiceProxy();
    return ePIXWebService.getAvailableAmountByAcctNo(accountNo);
  }
  
  public com.nirvasoft.epixws.webservice.CustomerInfo getCustomerNameAndNrcByAccount(java.lang.String account) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException{
    if (ePIXWebService == null)
      _initEPIXWebServiceProxy();
    return ePIXWebService.getCustomerNameAndNrcByAccount(account);
  }
  
  public com.nirvasoft.epixws.webservice.CustAccInfoResult getAccountCustomerInfo(java.lang.String caption, java.lang.String value, java.lang.String searchway) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException{
    if (ePIXWebService == null)
      _initEPIXWebServiceProxy();
    return ePIXWebService.getAccountCustomerInfo(caption, value, searchway);
  }
  
  public com.nirvasoft.epixws.webservice.ChequeEnquiryRes enquiryCheque(java.lang.String accountNo, java.lang.String chequeNo) throws java.rmi.RemoteException{
    if (ePIXWebService == null)
      _initEPIXWebServiceProxy();
    return ePIXWebService.enquiryCheque(accountNo, chequeNo);
  }
  
  public com.nirvasoft.epixws.webservice.BankInfoList getBankInfo() throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException{
    if (ePIXWebService == null)
      _initEPIXWebServiceProxy();
    return ePIXWebService.getBankInfo();
  }
  
  public com.nirvasoft.epixws.webservice.ChequeAccResult getChequeAccArr(java.lang.String customerID) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException{
    if (ePIXWebService == null)
      _initEPIXWebServiceProxy();
    return ePIXWebService.getChequeAccArr(customerID);
  }
  
  public com.nirvasoft.epixws.webservice.CustomerInfo getGLData(java.lang.String account) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException{
    if (ePIXWebService == null)
      _initEPIXWebServiceProxy();
    return ePIXWebService.getGLData(account);
  }
  
  public com.nirvasoft.epixws.webservice.AccountInfoResult getEnAccountInfo(java.lang.String caption, java.lang.String value, java.lang.String searchway) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException{
    if (ePIXWebService == null)
      _initEPIXWebServiceProxy();
    return ePIXWebService.getEnAccountInfo(caption, value, searchway);
  }
  
  public com.nirvasoft.epixws.webservice.AtmLocationResult getATMLocation(java.lang.String id) throws java.rmi.RemoteException{
    if (ePIXWebService == null)
      _initEPIXWebServiceProxy();
    return ePIXWebService.getATMLocation(id);
  }
  
  public com.nirvasoft.epixws.webservice.CustInfoResult getAccountInfo(java.lang.String customerID, java.lang.String fixedDeposit) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException{
    if (ePIXWebService == null)
      _initEPIXWebServiceProxy();
    return ePIXWebService.getAccountInfo(customerID, fixedDeposit);
  }
  
  public com.nirvasoft.epixws.webservice.CustInfoResult checkAccount(java.lang.String accountNo, java.lang.String accountName) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException{
    if (ePIXWebService == null)
      _initEPIXWebServiceProxy();
    return ePIXWebService.checkAccount(accountNo, accountName);
  }
  
  public com.nirvasoft.epixws.webservice.GlInfoResult getEnGLInfo(java.lang.String caption, java.lang.String value, java.lang.String searchway) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException{
    if (ePIXWebService == null)
      _initEPIXWebServiceProxy();
    return ePIXWebService.getEnGLInfo(caption, value, searchway);
  }
  
  public com.nirvasoft.epixws.webservice.CustomerInfo getCutomerInfo(java.lang.String customerID) throws java.rmi.RemoteException, com.nirvasoft.epixws.webservice.EpixWebServiceException{
    if (ePIXWebService == null)
      _initEPIXWebServiceProxy();
    return ePIXWebService.getCutomerInfo(customerID);
  }
  
  
}