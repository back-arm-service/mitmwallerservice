/**
 * CommissionInfoResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.epixws.webservice;

public class CommissionInfoResult  implements java.io.Serializable {
    private java.lang.String code;

    private java.lang.String desc;

    private java.lang.String xref;

    private java.lang.String productCode;

    private java.lang.String txnAcc;

    private com.nirvasoft.epixws.webservice.IftmArcMaintData[] commPCodeArr;

    public CommissionInfoResult() {
    }

    public CommissionInfoResult(
           java.lang.String code,
           java.lang.String desc,
           java.lang.String xref,
           java.lang.String productCode,
           java.lang.String txnAcc,
           com.nirvasoft.epixws.webservice.IftmArcMaintData[] commPCodeArr) {
           this.code = code;
           this.desc = desc;
           this.xref = xref;
           this.productCode = productCode;
           this.txnAcc = txnAcc;
           this.commPCodeArr = commPCodeArr;
    }


    /**
     * Gets the code value for this CommissionInfoResult.
     * 
     * @return code
     */
    public java.lang.String getCode() {
        return code;
    }


    /**
     * Sets the code value for this CommissionInfoResult.
     * 
     * @param code
     */
    public void setCode(java.lang.String code) {
        this.code = code;
    }


    /**
     * Gets the desc value for this CommissionInfoResult.
     * 
     * @return desc
     */
    public java.lang.String getDesc() {
        return desc;
    }


    /**
     * Sets the desc value for this CommissionInfoResult.
     * 
     * @param desc
     */
    public void setDesc(java.lang.String desc) {
        this.desc = desc;
    }


    /**
     * Gets the xref value for this CommissionInfoResult.
     * 
     * @return xref
     */
    public java.lang.String getXref() {
        return xref;
    }


    /**
     * Sets the xref value for this CommissionInfoResult.
     * 
     * @param xref
     */
    public void setXref(java.lang.String xref) {
        this.xref = xref;
    }


    /**
     * Gets the productCode value for this CommissionInfoResult.
     * 
     * @return productCode
     */
    public java.lang.String getProductCode() {
        return productCode;
    }


    /**
     * Sets the productCode value for this CommissionInfoResult.
     * 
     * @param productCode
     */
    public void setProductCode(java.lang.String productCode) {
        this.productCode = productCode;
    }


    /**
     * Gets the txnAcc value for this CommissionInfoResult.
     * 
     * @return txnAcc
     */
    public java.lang.String getTxnAcc() {
        return txnAcc;
    }


    /**
     * Sets the txnAcc value for this CommissionInfoResult.
     * 
     * @param txnAcc
     */
    public void setTxnAcc(java.lang.String txnAcc) {
        this.txnAcc = txnAcc;
    }


    /**
     * Gets the commPCodeArr value for this CommissionInfoResult.
     * 
     * @return commPCodeArr
     */
    public com.nirvasoft.epixws.webservice.IftmArcMaintData[] getCommPCodeArr() {
        return commPCodeArr;
    }


    /**
     * Sets the commPCodeArr value for this CommissionInfoResult.
     * 
     * @param commPCodeArr
     */
    public void setCommPCodeArr(com.nirvasoft.epixws.webservice.IftmArcMaintData[] commPCodeArr) {
        this.commPCodeArr = commPCodeArr;
    }

    public com.nirvasoft.epixws.webservice.IftmArcMaintData getCommPCodeArr(int i) {
        return this.commPCodeArr[i];
    }

    public void setCommPCodeArr(int i, com.nirvasoft.epixws.webservice.IftmArcMaintData _value) {
        this.commPCodeArr[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CommissionInfoResult)) return false;
        CommissionInfoResult other = (CommissionInfoResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.code==null && other.getCode()==null) || 
             (this.code!=null &&
              this.code.equals(other.getCode()))) &&
            ((this.desc==null && other.getDesc()==null) || 
             (this.desc!=null &&
              this.desc.equals(other.getDesc()))) &&
            ((this.xref==null && other.getXref()==null) || 
             (this.xref!=null &&
              this.xref.equals(other.getXref()))) &&
            ((this.productCode==null && other.getProductCode()==null) || 
             (this.productCode!=null &&
              this.productCode.equals(other.getProductCode()))) &&
            ((this.txnAcc==null && other.getTxnAcc()==null) || 
             (this.txnAcc!=null &&
              this.txnAcc.equals(other.getTxnAcc()))) &&
            ((this.commPCodeArr==null && other.getCommPCodeArr()==null) || 
             (this.commPCodeArr!=null &&
              java.util.Arrays.equals(this.commPCodeArr, other.getCommPCodeArr())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCode() != null) {
            _hashCode += getCode().hashCode();
        }
        if (getDesc() != null) {
            _hashCode += getDesc().hashCode();
        }
        if (getXref() != null) {
            _hashCode += getXref().hashCode();
        }
        if (getProductCode() != null) {
            _hashCode += getProductCode().hashCode();
        }
        if (getTxnAcc() != null) {
            _hashCode += getTxnAcc().hashCode();
        }
        if (getCommPCodeArr() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCommPCodeArr());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCommPCodeArr(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CommissionInfoResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "commissionInfoResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("desc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Desc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("xref");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Xref"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ProductCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txnAcc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TxnAcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commPCodeArr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CommPCodeArr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "iftmArcMaintData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
