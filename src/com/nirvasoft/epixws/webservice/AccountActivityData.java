/**
 * AccountActivityData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.epixws.webservice;

public class AccountActivityData  implements java.io.Serializable {
    private java.lang.String nbrAccount;

    private java.lang.String branchCode;

    private java.lang.String customerNo;

    private java.lang.String datValueDate;

    private java.lang.String txnDate;

    private java.lang.String postingDate;

    private java.lang.String codTxnCurr;

    private java.lang.String amountTag;

    private java.lang.String prodType;

    private java.lang.String namBranch;

    private double txnAmount;

    private java.lang.String coddrcr;

    private java.lang.String txtReferenchNo;

    private java.lang.String codTxnType;

    private java.lang.String description;

    private int srno;

    private java.lang.String txnCode;

    private java.lang.String swiftcode;

    private java.lang.String txnmodule;

    private java.lang.String userreferenceno;

    private java.lang.String virtualAcNo;

    private java.lang.String remitterName;

    private java.lang.String txnTypeDesc;

    public AccountActivityData() {
    }

    public AccountActivityData(
           java.lang.String nbrAccount,
           java.lang.String branchCode,
           java.lang.String customerNo,
           java.lang.String datValueDate,
           java.lang.String txnDate,
           java.lang.String postingDate,
           java.lang.String codTxnCurr,
           java.lang.String amountTag,
           java.lang.String prodType,
           java.lang.String namBranch,
           double txnAmount,
           java.lang.String coddrcr,
           java.lang.String txtReferenchNo,
           java.lang.String codTxnType,
           java.lang.String description,
           int srno,
           java.lang.String txnCode,
           java.lang.String swiftcode,
           java.lang.String txnmodule,
           java.lang.String userreferenceno,
           java.lang.String virtualAcNo,
           java.lang.String remitterName,
           java.lang.String txnTypeDesc) {
           this.nbrAccount = nbrAccount;
           this.branchCode = branchCode;
           this.customerNo = customerNo;
           this.datValueDate = datValueDate;
           this.txnDate = txnDate;
           this.postingDate = postingDate;
           this.codTxnCurr = codTxnCurr;
           this.amountTag = amountTag;
           this.prodType = prodType;
           this.namBranch = namBranch;
           this.txnAmount = txnAmount;
           this.coddrcr = coddrcr;
           this.txtReferenchNo = txtReferenchNo;
           this.codTxnType = codTxnType;
           this.description = description;
           this.srno = srno;
           this.txnCode = txnCode;
           this.swiftcode = swiftcode;
           this.txnmodule = txnmodule;
           this.userreferenceno = userreferenceno;
           this.virtualAcNo = virtualAcNo;
           this.remitterName = remitterName;
           this.txnTypeDesc = txnTypeDesc;
    }


    /**
     * Gets the nbrAccount value for this AccountActivityData.
     * 
     * @return nbrAccount
     */
    public java.lang.String getNbrAccount() {
        return nbrAccount;
    }


    /**
     * Sets the nbrAccount value for this AccountActivityData.
     * 
     * @param nbrAccount
     */
    public void setNbrAccount(java.lang.String nbrAccount) {
        this.nbrAccount = nbrAccount;
    }


    /**
     * Gets the branchCode value for this AccountActivityData.
     * 
     * @return branchCode
     */
    public java.lang.String getBranchCode() {
        return branchCode;
    }


    /**
     * Sets the branchCode value for this AccountActivityData.
     * 
     * @param branchCode
     */
    public void setBranchCode(java.lang.String branchCode) {
        this.branchCode = branchCode;
    }


    /**
     * Gets the customerNo value for this AccountActivityData.
     * 
     * @return customerNo
     */
    public java.lang.String getCustomerNo() {
        return customerNo;
    }


    /**
     * Sets the customerNo value for this AccountActivityData.
     * 
     * @param customerNo
     */
    public void setCustomerNo(java.lang.String customerNo) {
        this.customerNo = customerNo;
    }


    /**
     * Gets the datValueDate value for this AccountActivityData.
     * 
     * @return datValueDate
     */
    public java.lang.String getDatValueDate() {
        return datValueDate;
    }


    /**
     * Sets the datValueDate value for this AccountActivityData.
     * 
     * @param datValueDate
     */
    public void setDatValueDate(java.lang.String datValueDate) {
        this.datValueDate = datValueDate;
    }


    /**
     * Gets the txnDate value for this AccountActivityData.
     * 
     * @return txnDate
     */
    public java.lang.String getTxnDate() {
        return txnDate;
    }


    /**
     * Sets the txnDate value for this AccountActivityData.
     * 
     * @param txnDate
     */
    public void setTxnDate(java.lang.String txnDate) {
        this.txnDate = txnDate;
    }


    /**
     * Gets the postingDate value for this AccountActivityData.
     * 
     * @return postingDate
     */
    public java.lang.String getPostingDate() {
        return postingDate;
    }


    /**
     * Sets the postingDate value for this AccountActivityData.
     * 
     * @param postingDate
     */
    public void setPostingDate(java.lang.String postingDate) {
        this.postingDate = postingDate;
    }


    /**
     * Gets the codTxnCurr value for this AccountActivityData.
     * 
     * @return codTxnCurr
     */
    public java.lang.String getCodTxnCurr() {
        return codTxnCurr;
    }


    /**
     * Sets the codTxnCurr value for this AccountActivityData.
     * 
     * @param codTxnCurr
     */
    public void setCodTxnCurr(java.lang.String codTxnCurr) {
        this.codTxnCurr = codTxnCurr;
    }


    /**
     * Gets the amountTag value for this AccountActivityData.
     * 
     * @return amountTag
     */
    public java.lang.String getAmountTag() {
        return amountTag;
    }


    /**
     * Sets the amountTag value for this AccountActivityData.
     * 
     * @param amountTag
     */
    public void setAmountTag(java.lang.String amountTag) {
        this.amountTag = amountTag;
    }


    /**
     * Gets the prodType value for this AccountActivityData.
     * 
     * @return prodType
     */
    public java.lang.String getProdType() {
        return prodType;
    }


    /**
     * Sets the prodType value for this AccountActivityData.
     * 
     * @param prodType
     */
    public void setProdType(java.lang.String prodType) {
        this.prodType = prodType;
    }


    /**
     * Gets the namBranch value for this AccountActivityData.
     * 
     * @return namBranch
     */
    public java.lang.String getNamBranch() {
        return namBranch;
    }


    /**
     * Sets the namBranch value for this AccountActivityData.
     * 
     * @param namBranch
     */
    public void setNamBranch(java.lang.String namBranch) {
        this.namBranch = namBranch;
    }


    /**
     * Gets the txnAmount value for this AccountActivityData.
     * 
     * @return txnAmount
     */
    public double getTxnAmount() {
        return txnAmount;
    }


    /**
     * Sets the txnAmount value for this AccountActivityData.
     * 
     * @param txnAmount
     */
    public void setTxnAmount(double txnAmount) {
        this.txnAmount = txnAmount;
    }


    /**
     * Gets the coddrcr value for this AccountActivityData.
     * 
     * @return coddrcr
     */
    public java.lang.String getCoddrcr() {
        return coddrcr;
    }


    /**
     * Sets the coddrcr value for this AccountActivityData.
     * 
     * @param coddrcr
     */
    public void setCoddrcr(java.lang.String coddrcr) {
        this.coddrcr = coddrcr;
    }


    /**
     * Gets the txtReferenchNo value for this AccountActivityData.
     * 
     * @return txtReferenchNo
     */
    public java.lang.String getTxtReferenchNo() {
        return txtReferenchNo;
    }


    /**
     * Sets the txtReferenchNo value for this AccountActivityData.
     * 
     * @param txtReferenchNo
     */
    public void setTxtReferenchNo(java.lang.String txtReferenchNo) {
        this.txtReferenchNo = txtReferenchNo;
    }


    /**
     * Gets the codTxnType value for this AccountActivityData.
     * 
     * @return codTxnType
     */
    public java.lang.String getCodTxnType() {
        return codTxnType;
    }


    /**
     * Sets the codTxnType value for this AccountActivityData.
     * 
     * @param codTxnType
     */
    public void setCodTxnType(java.lang.String codTxnType) {
        this.codTxnType = codTxnType;
    }


    /**
     * Gets the description value for this AccountActivityData.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this AccountActivityData.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the srno value for this AccountActivityData.
     * 
     * @return srno
     */
    public int getSrno() {
        return srno;
    }


    /**
     * Sets the srno value for this AccountActivityData.
     * 
     * @param srno
     */
    public void setSrno(int srno) {
        this.srno = srno;
    }


    /**
     * Gets the txnCode value for this AccountActivityData.
     * 
     * @return txnCode
     */
    public java.lang.String getTxnCode() {
        return txnCode;
    }


    /**
     * Sets the txnCode value for this AccountActivityData.
     * 
     * @param txnCode
     */
    public void setTxnCode(java.lang.String txnCode) {
        this.txnCode = txnCode;
    }


    /**
     * Gets the swiftcode value for this AccountActivityData.
     * 
     * @return swiftcode
     */
    public java.lang.String getSwiftcode() {
        return swiftcode;
    }


    /**
     * Sets the swiftcode value for this AccountActivityData.
     * 
     * @param swiftcode
     */
    public void setSwiftcode(java.lang.String swiftcode) {
        this.swiftcode = swiftcode;
    }


    /**
     * Gets the txnmodule value for this AccountActivityData.
     * 
     * @return txnmodule
     */
    public java.lang.String getTxnmodule() {
        return txnmodule;
    }


    /**
     * Sets the txnmodule value for this AccountActivityData.
     * 
     * @param txnmodule
     */
    public void setTxnmodule(java.lang.String txnmodule) {
        this.txnmodule = txnmodule;
    }


    /**
     * Gets the userreferenceno value for this AccountActivityData.
     * 
     * @return userreferenceno
     */
    public java.lang.String getUserreferenceno() {
        return userreferenceno;
    }


    /**
     * Sets the userreferenceno value for this AccountActivityData.
     * 
     * @param userreferenceno
     */
    public void setUserreferenceno(java.lang.String userreferenceno) {
        this.userreferenceno = userreferenceno;
    }


    /**
     * Gets the virtualAcNo value for this AccountActivityData.
     * 
     * @return virtualAcNo
     */
    public java.lang.String getVirtualAcNo() {
        return virtualAcNo;
    }


    /**
     * Sets the virtualAcNo value for this AccountActivityData.
     * 
     * @param virtualAcNo
     */
    public void setVirtualAcNo(java.lang.String virtualAcNo) {
        this.virtualAcNo = virtualAcNo;
    }


    /**
     * Gets the remitterName value for this AccountActivityData.
     * 
     * @return remitterName
     */
    public java.lang.String getRemitterName() {
        return remitterName;
    }


    /**
     * Sets the remitterName value for this AccountActivityData.
     * 
     * @param remitterName
     */
    public void setRemitterName(java.lang.String remitterName) {
        this.remitterName = remitterName;
    }


    /**
     * Gets the txnTypeDesc value for this AccountActivityData.
     * 
     * @return txnTypeDesc
     */
    public java.lang.String getTxnTypeDesc() {
        return txnTypeDesc;
    }


    /**
     * Sets the txnTypeDesc value for this AccountActivityData.
     * 
     * @param txnTypeDesc
     */
    public void setTxnTypeDesc(java.lang.String txnTypeDesc) {
        this.txnTypeDesc = txnTypeDesc;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AccountActivityData)) return false;
        AccountActivityData other = (AccountActivityData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.nbrAccount==null && other.getNbrAccount()==null) || 
             (this.nbrAccount!=null &&
              this.nbrAccount.equals(other.getNbrAccount()))) &&
            ((this.branchCode==null && other.getBranchCode()==null) || 
             (this.branchCode!=null &&
              this.branchCode.equals(other.getBranchCode()))) &&
            ((this.customerNo==null && other.getCustomerNo()==null) || 
             (this.customerNo!=null &&
              this.customerNo.equals(other.getCustomerNo()))) &&
            ((this.datValueDate==null && other.getDatValueDate()==null) || 
             (this.datValueDate!=null &&
              this.datValueDate.equals(other.getDatValueDate()))) &&
            ((this.txnDate==null && other.getTxnDate()==null) || 
             (this.txnDate!=null &&
              this.txnDate.equals(other.getTxnDate()))) &&
            ((this.postingDate==null && other.getPostingDate()==null) || 
             (this.postingDate!=null &&
              this.postingDate.equals(other.getPostingDate()))) &&
            ((this.codTxnCurr==null && other.getCodTxnCurr()==null) || 
             (this.codTxnCurr!=null &&
              this.codTxnCurr.equals(other.getCodTxnCurr()))) &&
            ((this.amountTag==null && other.getAmountTag()==null) || 
             (this.amountTag!=null &&
              this.amountTag.equals(other.getAmountTag()))) &&
            ((this.prodType==null && other.getProdType()==null) || 
             (this.prodType!=null &&
              this.prodType.equals(other.getProdType()))) &&
            ((this.namBranch==null && other.getNamBranch()==null) || 
             (this.namBranch!=null &&
              this.namBranch.equals(other.getNamBranch()))) &&
            this.txnAmount == other.getTxnAmount() &&
            ((this.coddrcr==null && other.getCoddrcr()==null) || 
             (this.coddrcr!=null &&
              this.coddrcr.equals(other.getCoddrcr()))) &&
            ((this.txtReferenchNo==null && other.getTxtReferenchNo()==null) || 
             (this.txtReferenchNo!=null &&
              this.txtReferenchNo.equals(other.getTxtReferenchNo()))) &&
            ((this.codTxnType==null && other.getCodTxnType()==null) || 
             (this.codTxnType!=null &&
              this.codTxnType.equals(other.getCodTxnType()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            this.srno == other.getSrno() &&
            ((this.txnCode==null && other.getTxnCode()==null) || 
             (this.txnCode!=null &&
              this.txnCode.equals(other.getTxnCode()))) &&
            ((this.swiftcode==null && other.getSwiftcode()==null) || 
             (this.swiftcode!=null &&
              this.swiftcode.equals(other.getSwiftcode()))) &&
            ((this.txnmodule==null && other.getTxnmodule()==null) || 
             (this.txnmodule!=null &&
              this.txnmodule.equals(other.getTxnmodule()))) &&
            ((this.userreferenceno==null && other.getUserreferenceno()==null) || 
             (this.userreferenceno!=null &&
              this.userreferenceno.equals(other.getUserreferenceno()))) &&
            ((this.virtualAcNo==null && other.getVirtualAcNo()==null) || 
             (this.virtualAcNo!=null &&
              this.virtualAcNo.equals(other.getVirtualAcNo()))) &&
            ((this.remitterName==null && other.getRemitterName()==null) || 
             (this.remitterName!=null &&
              this.remitterName.equals(other.getRemitterName()))) &&
            ((this.txnTypeDesc==null && other.getTxnTypeDesc()==null) || 
             (this.txnTypeDesc!=null &&
              this.txnTypeDesc.equals(other.getTxnTypeDesc())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNbrAccount() != null) {
            _hashCode += getNbrAccount().hashCode();
        }
        if (getBranchCode() != null) {
            _hashCode += getBranchCode().hashCode();
        }
        if (getCustomerNo() != null) {
            _hashCode += getCustomerNo().hashCode();
        }
        if (getDatValueDate() != null) {
            _hashCode += getDatValueDate().hashCode();
        }
        if (getTxnDate() != null) {
            _hashCode += getTxnDate().hashCode();
        }
        if (getPostingDate() != null) {
            _hashCode += getPostingDate().hashCode();
        }
        if (getCodTxnCurr() != null) {
            _hashCode += getCodTxnCurr().hashCode();
        }
        if (getAmountTag() != null) {
            _hashCode += getAmountTag().hashCode();
        }
        if (getProdType() != null) {
            _hashCode += getProdType().hashCode();
        }
        if (getNamBranch() != null) {
            _hashCode += getNamBranch().hashCode();
        }
        _hashCode += new Double(getTxnAmount()).hashCode();
        if (getCoddrcr() != null) {
            _hashCode += getCoddrcr().hashCode();
        }
        if (getTxtReferenchNo() != null) {
            _hashCode += getTxtReferenchNo().hashCode();
        }
        if (getCodTxnType() != null) {
            _hashCode += getCodTxnType().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        _hashCode += getSrno();
        if (getTxnCode() != null) {
            _hashCode += getTxnCode().hashCode();
        }
        if (getSwiftcode() != null) {
            _hashCode += getSwiftcode().hashCode();
        }
        if (getTxnmodule() != null) {
            _hashCode += getTxnmodule().hashCode();
        }
        if (getUserreferenceno() != null) {
            _hashCode += getUserreferenceno().hashCode();
        }
        if (getVirtualAcNo() != null) {
            _hashCode += getVirtualAcNo().hashCode();
        }
        if (getRemitterName() != null) {
            _hashCode += getRemitterName().hashCode();
        }
        if (getTxnTypeDesc() != null) {
            _hashCode += getTxnTypeDesc().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AccountActivityData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "accountActivityData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nbrAccount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NbrAccount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("branchCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BranchCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerNo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CustomerNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datValueDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DatValueDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txnDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TxnDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("postingDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PostingDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTxnCurr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodTxnCurr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amountTag");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AmountTag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prodType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ProdType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("namBranch");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NamBranch"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txnAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TxnAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coddrcr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Coddrcr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txtReferenchNo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TxtReferenchNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTxnType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CodTxnType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("srno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Srno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txnCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TxnCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("swiftcode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Swiftcode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txnmodule");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Txnmodule"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userreferenceno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Userreferenceno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtualAcNo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VirtualAcNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("remitterName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RemitterName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txnTypeDesc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TxnTypeDesc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
