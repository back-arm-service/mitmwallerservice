/**
 * BankInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.epixws.webservice;

public class BankInfo  implements java.io.Serializable {
    private java.lang.String bankCode;

    private java.lang.String bankName;

    private java.lang.String branchCode;

    private java.lang.String bankAddress1;

    private java.lang.String bankAddress2;

    private java.lang.String bankAddress3;

    private java.lang.String bankAddress4;

    public BankInfo() {
    }

    public BankInfo(
           java.lang.String bankCode,
           java.lang.String bankName,
           java.lang.String branchCode,
           java.lang.String bankAddress1,
           java.lang.String bankAddress2,
           java.lang.String bankAddress3,
           java.lang.String bankAddress4) {
           this.bankCode = bankCode;
           this.bankName = bankName;
           this.branchCode = branchCode;
           this.bankAddress1 = bankAddress1;
           this.bankAddress2 = bankAddress2;
           this.bankAddress3 = bankAddress3;
           this.bankAddress4 = bankAddress4;
    }


    /**
     * Gets the bankCode value for this BankInfo.
     * 
     * @return bankCode
     */
    public java.lang.String getBankCode() {
        return bankCode;
    }


    /**
     * Sets the bankCode value for this BankInfo.
     * 
     * @param bankCode
     */
    public void setBankCode(java.lang.String bankCode) {
        this.bankCode = bankCode;
    }


    /**
     * Gets the bankName value for this BankInfo.
     * 
     * @return bankName
     */
    public java.lang.String getBankName() {
        return bankName;
    }


    /**
     * Sets the bankName value for this BankInfo.
     * 
     * @param bankName
     */
    public void setBankName(java.lang.String bankName) {
        this.bankName = bankName;
    }


    /**
     * Gets the branchCode value for this BankInfo.
     * 
     * @return branchCode
     */
    public java.lang.String getBranchCode() {
        return branchCode;
    }


    /**
     * Sets the branchCode value for this BankInfo.
     * 
     * @param branchCode
     */
    public void setBranchCode(java.lang.String branchCode) {
        this.branchCode = branchCode;
    }


    /**
     * Gets the bankAddress1 value for this BankInfo.
     * 
     * @return bankAddress1
     */
    public java.lang.String getBankAddress1() {
        return bankAddress1;
    }


    /**
     * Sets the bankAddress1 value for this BankInfo.
     * 
     * @param bankAddress1
     */
    public void setBankAddress1(java.lang.String bankAddress1) {
        this.bankAddress1 = bankAddress1;
    }


    /**
     * Gets the bankAddress2 value for this BankInfo.
     * 
     * @return bankAddress2
     */
    public java.lang.String getBankAddress2() {
        return bankAddress2;
    }


    /**
     * Sets the bankAddress2 value for this BankInfo.
     * 
     * @param bankAddress2
     */
    public void setBankAddress2(java.lang.String bankAddress2) {
        this.bankAddress2 = bankAddress2;
    }


    /**
     * Gets the bankAddress3 value for this BankInfo.
     * 
     * @return bankAddress3
     */
    public java.lang.String getBankAddress3() {
        return bankAddress3;
    }


    /**
     * Sets the bankAddress3 value for this BankInfo.
     * 
     * @param bankAddress3
     */
    public void setBankAddress3(java.lang.String bankAddress3) {
        this.bankAddress3 = bankAddress3;
    }


    /**
     * Gets the bankAddress4 value for this BankInfo.
     * 
     * @return bankAddress4
     */
    public java.lang.String getBankAddress4() {
        return bankAddress4;
    }


    /**
     * Sets the bankAddress4 value for this BankInfo.
     * 
     * @param bankAddress4
     */
    public void setBankAddress4(java.lang.String bankAddress4) {
        this.bankAddress4 = bankAddress4;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BankInfo)) return false;
        BankInfo other = (BankInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.bankCode==null && other.getBankCode()==null) || 
             (this.bankCode!=null &&
              this.bankCode.equals(other.getBankCode()))) &&
            ((this.bankName==null && other.getBankName()==null) || 
             (this.bankName!=null &&
              this.bankName.equals(other.getBankName()))) &&
            ((this.branchCode==null && other.getBranchCode()==null) || 
             (this.branchCode!=null &&
              this.branchCode.equals(other.getBranchCode()))) &&
            ((this.bankAddress1==null && other.getBankAddress1()==null) || 
             (this.bankAddress1!=null &&
              this.bankAddress1.equals(other.getBankAddress1()))) &&
            ((this.bankAddress2==null && other.getBankAddress2()==null) || 
             (this.bankAddress2!=null &&
              this.bankAddress2.equals(other.getBankAddress2()))) &&
            ((this.bankAddress3==null && other.getBankAddress3()==null) || 
             (this.bankAddress3!=null &&
              this.bankAddress3.equals(other.getBankAddress3()))) &&
            ((this.bankAddress4==null && other.getBankAddress4()==null) || 
             (this.bankAddress4!=null &&
              this.bankAddress4.equals(other.getBankAddress4())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBankCode() != null) {
            _hashCode += getBankCode().hashCode();
        }
        if (getBankName() != null) {
            _hashCode += getBankName().hashCode();
        }
        if (getBranchCode() != null) {
            _hashCode += getBranchCode().hashCode();
        }
        if (getBankAddress1() != null) {
            _hashCode += getBankAddress1().hashCode();
        }
        if (getBankAddress2() != null) {
            _hashCode += getBankAddress2().hashCode();
        }
        if (getBankAddress3() != null) {
            _hashCode += getBankAddress3().hashCode();
        }
        if (getBankAddress4() != null) {
            _hashCode += getBankAddress4().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BankInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "bankInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bankCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BankCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bankName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BankName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("branchCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BranchCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bankAddress1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BankAddress1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bankAddress2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BankAddress2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bankAddress3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BankAddress3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bankAddress4");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BankAddress4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
