/**
 * CustAccInfoResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.epixws.webservice;

public class CustAccInfoResult  implements java.io.Serializable {
    private java.lang.String customerId;

    private java.lang.String code;

    private java.lang.String desc;

    private java.lang.String name;

    private java.lang.String NRC;

    private java.lang.String DOB;

    private com.nirvasoft.epixws.webservice.AccountInfo[] accountInfoArr;

    public CustAccInfoResult() {
    }

    public CustAccInfoResult(
           java.lang.String customerId,
           java.lang.String code,
           java.lang.String desc,
           java.lang.String name,
           java.lang.String NRC,
           java.lang.String DOB,
           com.nirvasoft.epixws.webservice.AccountInfo[] accountInfoArr) {
           this.customerId = customerId;
           this.code = code;
           this.desc = desc;
           this.name = name;
           this.NRC = NRC;
           this.DOB = DOB;
           this.accountInfoArr = accountInfoArr;
    }


    /**
     * Gets the customerId value for this CustAccInfoResult.
     * 
     * @return customerId
     */
    public java.lang.String getCustomerId() {
        return customerId;
    }


    /**
     * Sets the customerId value for this CustAccInfoResult.
     * 
     * @param customerId
     */
    public void setCustomerId(java.lang.String customerId) {
        this.customerId = customerId;
    }


    /**
     * Gets the code value for this CustAccInfoResult.
     * 
     * @return code
     */
    public java.lang.String getCode() {
        return code;
    }


    /**
     * Sets the code value for this CustAccInfoResult.
     * 
     * @param code
     */
    public void setCode(java.lang.String code) {
        this.code = code;
    }


    /**
     * Gets the desc value for this CustAccInfoResult.
     * 
     * @return desc
     */
    public java.lang.String getDesc() {
        return desc;
    }


    /**
     * Sets the desc value for this CustAccInfoResult.
     * 
     * @param desc
     */
    public void setDesc(java.lang.String desc) {
        this.desc = desc;
    }


    /**
     * Gets the name value for this CustAccInfoResult.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this CustAccInfoResult.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the NRC value for this CustAccInfoResult.
     * 
     * @return NRC
     */
    public java.lang.String getNRC() {
        return NRC;
    }


    /**
     * Sets the NRC value for this CustAccInfoResult.
     * 
     * @param NRC
     */
    public void setNRC(java.lang.String NRC) {
        this.NRC = NRC;
    }


    /**
     * Gets the DOB value for this CustAccInfoResult.
     * 
     * @return DOB
     */
    public java.lang.String getDOB() {
        return DOB;
    }


    /**
     * Sets the DOB value for this CustAccInfoResult.
     * 
     * @param DOB
     */
    public void setDOB(java.lang.String DOB) {
        this.DOB = DOB;
    }


    /**
     * Gets the accountInfoArr value for this CustAccInfoResult.
     * 
     * @return accountInfoArr
     */
    public com.nirvasoft.epixws.webservice.AccountInfo[] getAccountInfoArr() {
        return accountInfoArr;
    }


    /**
     * Sets the accountInfoArr value for this CustAccInfoResult.
     * 
     * @param accountInfoArr
     */
    public void setAccountInfoArr(com.nirvasoft.epixws.webservice.AccountInfo[] accountInfoArr) {
        this.accountInfoArr = accountInfoArr;
    }

    public com.nirvasoft.epixws.webservice.AccountInfo getAccountInfoArr(int i) {
        return this.accountInfoArr[i];
    }

    public void setAccountInfoArr(int i, com.nirvasoft.epixws.webservice.AccountInfo _value) {
        this.accountInfoArr[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustAccInfoResult)) return false;
        CustAccInfoResult other = (CustAccInfoResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.customerId==null && other.getCustomerId()==null) || 
             (this.customerId!=null &&
              this.customerId.equals(other.getCustomerId()))) &&
            ((this.code==null && other.getCode()==null) || 
             (this.code!=null &&
              this.code.equals(other.getCode()))) &&
            ((this.desc==null && other.getDesc()==null) || 
             (this.desc!=null &&
              this.desc.equals(other.getDesc()))) &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.NRC==null && other.getNRC()==null) || 
             (this.NRC!=null &&
              this.NRC.equals(other.getNRC()))) &&
            ((this.DOB==null && other.getDOB()==null) || 
             (this.DOB!=null &&
              this.DOB.equals(other.getDOB()))) &&
            ((this.accountInfoArr==null && other.getAccountInfoArr()==null) || 
             (this.accountInfoArr!=null &&
              java.util.Arrays.equals(this.accountInfoArr, other.getAccountInfoArr())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCustomerId() != null) {
            _hashCode += getCustomerId().hashCode();
        }
        if (getCode() != null) {
            _hashCode += getCode().hashCode();
        }
        if (getDesc() != null) {
            _hashCode += getDesc().hashCode();
        }
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getNRC() != null) {
            _hashCode += getNRC().hashCode();
        }
        if (getDOB() != null) {
            _hashCode += getDOB().hashCode();
        }
        if (getAccountInfoArr() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAccountInfoArr());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAccountInfoArr(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustAccInfoResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "custAccInfoResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CustomerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("desc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Desc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NRC");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NRC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DOB");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DOB"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountInfoArr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AccountInfoArr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://webservice.epixws.nirvasoft.com/", "accountInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
