package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ReconciliationData {

	private String aFromDate;
	private String aToDate;
	private String kbzTransID;
	private String transID;
	private String status;
	private String select;
	private String tStatus;

	private int totalCount;
	private int currentPage;
	private int pageSize;

	public ReconciliationData() {
		clearProperty();
	}

	public void clearProperty() {
		this.aFromDate = "";
		this.aToDate = "";
		this.kbzTransID = "";
		this.transID = "";
		this.status = "";
		this.select = "";
		this.tStatus = "";
	}

	public String getaFromDate() {
		return aFromDate;
	}

	public String getaToDate() {
		return aToDate;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public String getKbzTransID() {
		return kbzTransID;
	}

	public int getPageSize() {
		return pageSize;
	}

	public String getSelect() {
		return select;
	}

	public String getStatus() {
		return status;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public String getTransID() {
		return transID;
	}

	public String gettStatus() {
		return tStatus;
	}

	public void setaFromDate(String aFromDate) {
		this.aFromDate = aFromDate;
	}

	public void setaToDate(String aToDate) {
		this.aToDate = aToDate;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setKbzTransID(String kbzTransID) {
		this.kbzTransID = kbzTransID;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setSelect(String select) {
		this.select = select;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public void setTransID(String transID) {
		this.transID = transID;
	}

	public void settStatus(String tStatus) {
		this.tStatus = tStatus;
	}

}
