package com.nirvasoft.rp.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

import com.nirvasoft.rp.framework.LOVDetails;

@XmlRootElement
public class StateListResponseData {

	private String code;
	private String desc;
	private LOVDetails[] stateList;

	public StateListResponseData() {
		this.clearProperty();
	}

	private void clearProperty() {
		this.code = "";
		this.desc = "";
		this.stateList = null;
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public LOVDetails[] getStateList() {
		return stateList;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setStateList(LOVDetails[] stateList) {
		this.stateList = stateList;
	}

	@Override
	public String toString() {
		return "StateListResponseData [code=" + code + ", desc=" + desc + ", stateList=" + Arrays.toString(stateList)
				+ "]";
	}

}
