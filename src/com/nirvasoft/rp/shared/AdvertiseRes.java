package com.nirvasoft.rp.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AdvertiseRes {

	private String code;
	private String desc;
	private Advertise[] dataList = null;

	public AdvertiseRes() {
		clearProperty();
	}

	private void clearProperty() {
		code = "";
		desc = "";
		dataList = null;
	}

	public String getCode() {
		return code;
	}

	public Advertise[] getDataList() {
		return dataList;
	}

	public String getDesc() {
		return desc;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDataList(Advertise[] dataList) {
		this.dataList = dataList;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public String toString() {
		return "{code=" + code + ", desc=" + desc + ", dataList=" + Arrays.toString(dataList) + "}";
	}

}
