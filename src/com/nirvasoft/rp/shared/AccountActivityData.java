
package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AccountActivityData {
	private String codTxnCurr;
	private String nbrAccount;
	private String txnDate;
	private String txnAmount;
	private String txtReferenceNo;
	private String txnTypeDesc;
	private String txnCode;
	private String subRef;
	private String accRef;
	private String drcr;
	private String remark;
	public String getDrcr() {
		return drcr;
	}

	public void setDrcr(String drcr) {
		this.drcr = drcr;
	}

	public String getAccRef() {
		return accRef;
	}

	public void setAccRef(String accRef) {
		this.accRef = accRef;
	}

	private String txnDateTime;

	public AccountActivityData() {
		clearProperty();
	}

	void clearProperty() {
		nbrAccount = "";
		txnDate = "";
		txnAmount = "";
		txtReferenceNo = "";
		txnTypeDesc = "";
		txnCode = "";
		subRef = "";
		accRef="";
		txnDateTime= "";
		remark= "";
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getTxnCode() {
		return txnCode;
	}

	public String getTxnDateTime() {
		return txnDateTime;
	}

	public void setTxnDateTime(String txnDateTime) {
		this.txnDateTime = txnDateTime;
	}

	public String getCodTxnCurr() {
		return codTxnCurr;
	}

	public String getNbrAccount() {
		return nbrAccount;
	}

	public String getSubRef() {
		return subRef;
	}

	public String getTxnAmount() {
		return txnAmount;
	}

	public String S() {
		return txnCode;
	}

	public String getTxnDate() {
		return txnDate;
	}

	public String getTxnTypeDesc() {
		return txnTypeDesc;
	}

	public String getTxtReferenceNo() {
		return txtReferenceNo;
	}

	public void setCodTxnCurr(String codTxnCurr) {
		this.codTxnCurr = codTxnCurr;
	}

	public void setNbrAccount(String nbrAccount) {
		this.nbrAccount = nbrAccount;
	}

	public void setSubRef(String subRef) {
		this.subRef = subRef;
	}

	public void setTxnAmount(String txnAmount) {
		this.txnAmount = txnAmount;
	}

	public void setTxnCode(String txnCode) {
		this.txnCode = txnCode;
	}

	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}

	public void setTxnTypeDesc(String txnTypeDesc) {
		this.txnTypeDesc = txnTypeDesc;
	}

	public void setTxtReferenceNo(String txtReferenceNo) {
		this.txtReferenceNo = txtReferenceNo;
	}

	@Override
	public String toString() {
		return "{codTxnCurr=" + codTxnCurr + ", nbrAccount=" + nbrAccount + ", txnDate=" + txnDate + ", txnAmount="
				+ txnAmount + ", txtReferenceNo=" + txtReferenceNo + ", txnTypeDesc=" + txnTypeDesc + ", txnCode="
				+ txnCode + ", remark = " + subRef + "}";
	}

}
