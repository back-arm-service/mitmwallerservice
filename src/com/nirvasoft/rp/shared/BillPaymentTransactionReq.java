package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BillPaymentTransactionReq {

	private String sessionID;
	private String userID;
	private String merchantID;
	private String acctNo;
	private String durationType;
	private String fromDate;
	private String toDate;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private int pageCount;
	private String status;

	public BillPaymentTransactionReq() {
		clearProperty();
	}

	private void clearProperty() {
		merchantID = "";
		userID = "";
		acctNo = "";
		sessionID = "";
		fromDate = "";
		toDate = "";
		durationType = "";

	}

	public String getAcctNo() {
		return acctNo;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public String getDurationType() {
		return durationType;
	}

	public String getFromDate() {
		return fromDate;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public int getPageCount() {
		return pageCount;
	}

	public int getPageSize() {
		return pageSize;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getStatus() {
		return status;
	}

	public String getToDate() {
		return toDate;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public String getUserID() {
		return userID;
	}

	public void setAcctNo(String accNumber) {
		this.acctNo = accNumber;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setDurationType(String durationType) {
		this.durationType = durationType;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	@Override
	public String toString() {
		return "{sessionID=" + sessionID + ", userID=" + userID + ", merchantID=" + merchantID + ", acctNo=" + acctNo
				+ ", durationType=" + durationType + ", fromDate=" + fromDate + ", toDate=" + toDate + ", totalCount="
				+ totalCount + ", currentPage=" + currentPage + ", pageSize=" + pageSize + ", pageCount=" + pageCount
				+ ", status=" + status + "}";
	}

}
