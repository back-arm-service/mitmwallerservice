package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BillPaymentTransaction {

	private String cardNo;
	private String transDate;
	private String bankRefNumber;
	private String amount;
	private String amountServiceCharges;
	private String amountTax;
	private String amountTotal;
	private String ccy;
	private String debitAccount;
	private String packageName;
	private String month;
	private String expiredDate;
	private String merchantName;
	private String resultCode;
	private String resultDesc;
	private String txnCode;

	public BillPaymentTransaction() {
		clearProperty();
	}

	private void clearProperty() {
		cardNo = "";
		transDate = "";
		bankRefNumber = "";
		amount = "0.00";
		ccy = "";
		debitAccount = "";
		packageName = "";
		month = "";
		expiredDate = "";
		merchantName = "";
		resultCode = "";
		resultDesc = "";
		amountServiceCharges = "0.00";
		amountTax = "0.00";
		amountTotal = "0.00";
		txnCode = "";
	}

	public String getAmount() {
		return amount;
	}

	public String getAmountServiceCharges() {
		return amountServiceCharges;
	}

	public String getAmountTax() {
		return amountTax;
	}

	public String getAmountTotal() {
		return amountTotal;
	}

	public String getBankRefNumber() {
		return bankRefNumber;
	}

	public String getCardNo() {
		return cardNo;
	}

	public String getCcy() {
		return ccy;
	}

	public String getDebitAccount() {
		return debitAccount;
	}

	public String getExpiredDate() {
		return expiredDate;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public String getMonth() {
		return month;
	}

	public String getPackageName() {
		return packageName;
	}

	public String getResultCode() {
		return resultCode;
	}

	public String getResultDesc() {
		return resultDesc;
	}

	public String getTransDate() {
		return transDate;
	}

	public String getTxnCode() {
		return txnCode;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public void setAmountServiceCharges(String amountServiceCharges) {
		this.amountServiceCharges = amountServiceCharges;
	}

	public void setAmountTax(String amountTax) {
		this.amountTax = amountTax;
	}

	public void setAmountTotal(String amountTotal) {
		this.amountTotal = amountTotal;
	}

	public void setBankRefNumber(String bankRefNumber) {
		this.bankRefNumber = bankRefNumber;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public void setDebitAccount(String debitAccount) {
		this.debitAccount = debitAccount;
	}

	public void setExpiredDate(String expiredDate) {
		this.expiredDate = expiredDate;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public void setResultCode(String resultcode) {
		this.resultCode = resultcode;
	}

	public void setResultDesc(String resultdesc) {
		this.resultDesc = resultdesc;
	}

	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}

	public void setTxnCode(String txnCode) {
		this.txnCode = txnCode;
	}

	@Override
	public String toString() {
		return "{cardNo=" + cardNo + ", transDate=" + transDate + ", bankRefNumber=" + bankRefNumber + ", amount="
				+ amount + ", amountServiceCharges=" + amountServiceCharges + ", amountTax=" + amountTax
				+ ", amountTotal=" + amountTotal + ", ccy=" + ccy + ", debitAccount=" + debitAccount + ", packageName="
				+ packageName + ", month=" + month + ", expiredDate=" + expiredDate + ", merchantName=" + merchantName
				+ ", resultCode=" + resultCode + ", resultDesc=" + resultDesc + ", txnCode=" + txnCode + "}";
	}

}
