package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ForceChangePwdResponse {
	private String code;
	private String desc;

	public ForceChangePwdResponse() {
		clearProperties();
		// TODO Auto-generated constructor stub
	}

	private void clearProperties() {
		// TODO Auto-generated method stub

		code = "";
		desc = "";
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public String toString() {
		return "{code=" + code + ", desc=" + desc + "}";
	}

}
