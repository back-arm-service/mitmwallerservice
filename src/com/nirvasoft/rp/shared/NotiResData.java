package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class NotiResData {
	private String notiMessage;
	private String notiType;

	public NotiResData() {
		clearProperty();
	}

	private void clearProperty() {
		notiMessage = "";
		notiType = "";
	}

	public String getNotiMessage() {
		return notiMessage;
	}

	public String getNotiType() {
		return notiType;
	}

	public void setNotiMessage(String notiMessage) {
		this.notiMessage = notiMessage;
	}

	public void setNotiType(String notiType) {
		this.notiType = notiType;
	}

}
