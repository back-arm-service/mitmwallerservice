package com.nirvasoft.rp.shared;

import java.text.DecimalFormat;

import com.nirvasoft.rp.util.ServerUtil;

public class TestClient {
	public static void main(String[] args) {
		TestClient data = new TestClient();
		System.out.println("AA");
		System.out.println(ServerUtil.decryptPIN("Smd9OVxEibYvj7A/hbNYWw=="));
		System.out.println("Result : " + data.roundTo2Decimals(1.117000));
	}

	double roundTo2Decimals(double val) {
		DecimalFormat df2 = new DecimalFormat("###.##");
		return Double.valueOf(df2.format(val));
	}

}
