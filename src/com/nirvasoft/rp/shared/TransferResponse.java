package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TransferResponse {

	private String code;
	private String desc;
	private String bankRefNumber;
	private String transactionDate;
	private String cardExpire;

	public TransferResponse() {
		clearProperty();
	}

	private void clearProperty() {
		code = "";
		desc = "";
		bankRefNumber = "";
		transactionDate = "";
		cardExpire = "";
	}

	public String getBankRefNumber() {
		return bankRefNumber;
	}

	public String getCardExpire() {
		return cardExpire;
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setBankRefNumber(String bankRefNumber) {
		this.bankRefNumber = bankRefNumber;
	}

	public void setCardExpire(String cardExpire) {
		this.cardExpire = cardExpire;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	@Override
	public String toString() {
		return "{ code=" + code + ", desc=" + desc + ", bankRefNumber=" + bankRefNumber + ", transactionDate="
				+ transactionDate + ", cardExpire=" + cardExpire + "}";
	}

}
