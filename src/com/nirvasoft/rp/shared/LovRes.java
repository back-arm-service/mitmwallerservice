package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LovRes {
	private String code;
	private String desc;
	private ReferenceListData[] dataList;

	public LovRes() {
		code = "";
		desc = "";
		dataList = null;
	}

	public String getCode() {
		return code;
	}

	public ReferenceListData[] getDataList() {
		return dataList;
	}

	public String getDesc() {
		return desc;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDataList(ReferenceListData[] dataList) {
		this.dataList = dataList;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
