package com.nirvasoft.rp.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MSigninResponseData {
	private String code;
	private String desc;
	private String sessionID;
	private String lastTimeLoginSuccess;
	private String lastTimeLoginFailed;

	private DepositAccData[] debitacc;

	public MSigninResponseData() {
		this.code = "";
		this.desc = "";
		this.sessionID = "";
		debitacc = null;
		this.lastTimeLoginSuccess = "";
		this.lastTimeLoginFailed = "";
	}

	public String getCode() {
		return code;
	}

	public DepositAccData[] getDebitAcc() {
		return debitacc;
	}

	public String getDesc() {
		return desc;
	}

	public String getLastTimeLoginFailed() {
		return lastTimeLoginFailed;
	}

	public String getLastTimeLoginSuccess() {
		return lastTimeLoginSuccess;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDebitAcc(DepositAccData[] debitacc) {
		this.debitacc = debitacc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setLastTimeLoginFailed(String lastTimeLoginFailed) {
		this.lastTimeLoginFailed = lastTimeLoginFailed;
	}

	public void setLastTimeLoginSuccess(String lastTimeLoginSuccess) {
		this.lastTimeLoginSuccess = lastTimeLoginSuccess;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	@Override
	public String toString() {
		return "{code=" + code + ", desc=" + desc + ", sessionID=" + sessionID + ", lastTimeLoginSuccess="
				+ lastTimeLoginSuccess + ", lastTimeLoginFailed=" + lastTimeLoginFailed + ", debitacc="
				+ Arrays.toString(debitacc) + "}";
	}

}
