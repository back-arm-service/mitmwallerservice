package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AccountActivityRequest {

	private String userID;
	private String sessionID;
	private String acctNo;
	private String customerNo;
	private String durationType;
	private String fromDate;
	private String toDate;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private int pageCount;

	public AccountActivityRequest() {
		clearProperty();
	}

	void clearProperty() {
		userID = "";
		sessionID = "";
		acctNo = "";
		customerNo = "";
		durationType = "";
		fromDate = "";
		toDate = "";
	}

	public String getAcctNo() {
		return acctNo;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public String getDurationType() {
		return durationType;
	}

	public String getFromDate() {
		return fromDate;
	}

	public int getPageCount() {
		return pageCount;
	}

	public int getPageSize() {
		return pageSize;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getToDate() {
		return toDate;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public String getUserID() {
		return userID;
	}

	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public void setDurationType(String durationType) {
		this.durationType = durationType;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	@Override
	public String toString() {
		return "{userid=" + userID + ", sessionID=" + sessionID + ", accNumber=" + acctNo + ", customerNo=" + customerNo
				+ ", durationType=" + durationType + ", fromDate=" + fromDate + ", toDate=" + toDate + ", totalCount="
				+ totalCount + ", currentPage=" + currentPage + ", pageSize=" + pageSize + ", pageCount=" + pageCount
				+ "}";
	}

}
