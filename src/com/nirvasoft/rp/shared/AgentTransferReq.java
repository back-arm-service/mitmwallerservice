package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AgentTransferReq {
	private String userID;
	private String sessionID;
	private String fromAccount;
	private String fromName;
	private String toAccount;
	private String toName;
	private String amount;
	private String bankCharges;
	private String narrative;
	private String refNo;
	private String draweeName;
	private String draweeNRC;
	private String draweePhone;
	private String payeeName;
	private String payeeNRC;
	private String payeePhone;
	private String field1;
	private String field2;
	private String sKey;

	public AgentTransferReq() {
		clearProperty();
	}

	void clearProperty() {
		userID = "";
		sessionID = "";
		fromAccount = "";
		fromName = "";
		toAccount = "";
		toName = "";
		amount = "0.00";
		bankCharges = "0.00";
		narrative = "";
		refNo = "";
		draweeName = "";
		draweeNRC = "";
		draweePhone = "";
		payeeName = "";
		payeeNRC = "";
		payeePhone = "";
		field1 = "";
		field2 = "";
		sKey = "";
	}

	public String getAmount() {
		return amount;
	}

	public String getBankCharges() {
		return bankCharges;
	}

	public String getDraweeName() {
		return draweeName;
	}

	public String getDraweeNRC() {
		return draweeNRC;
	}

	public String getDraweePhone() {
		return draweePhone;
	}

	public String getField1() {
		return field1;
	}

	public String getField2() {
		return field2;
	}

	public String getFromAccount() {
		return fromAccount;
	}

	public String getFromName() {
		return fromName;
	}

	public String getNarrative() {
		return narrative;
	}

	public String getPayeeName() {
		return payeeName;
	}

	public String getPayeeNRC() {
		return payeeNRC;
	}

	public String getPayeePhone() {
		return payeePhone;
	}

	public String getRefNo() {
		return refNo;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getsKey() {
		return sKey;
	}

	public String getToAccount() {
		return toAccount;
	}

	public String getToName() {
		return toName;
	}

	public String getUserID() {
		return userID;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public void setBankCharges(String bankCharges) {
		this.bankCharges = bankCharges;
	}

	public void setDraweeName(String draweeName) {
		this.draweeName = draweeName;
	}

	public void setDraweeNRC(String draweeNRC) {
		this.draweeNRC = draweeNRC;
	}

	public void setDraweePhone(String draweePhone) {
		this.draweePhone = draweePhone;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public void setFromAccount(String fromAccount) {
		this.fromAccount = fromAccount;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	public void setNarrative(String narrative) {
		this.narrative = narrative;
	}

	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}

	public void setPayeeNRC(String payeeNRC) {
		this.payeeNRC = payeeNRC;
	}

	public void setPayeePhone(String payeePhone) {
		this.payeePhone = payeePhone;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setsKey(String sKey) {
		this.sKey = sKey;
	}

	public void setToAccount(String toAccount) {
		this.toAccount = toAccount;
	}

	public void setToName(String toName) {
		this.toName = toName;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	@Override
	public String toString() {
		return "{userID=" + userID + ", sessionID=" + sessionID + ", fromAccount=" + fromAccount + ", fromName="
				+ fromName + ", toAccount=" + toAccount + ", toName=" + toName + ", amount=" + amount + ", bankCharges="
				+ bankCharges + ", narrative=" + narrative + ", refNo=" + refNo + ", draweeName=" + draweeName
				+ ", draweeNRC=" + draweeNRC + ", draweePhone=" + draweePhone + ", payeeName=" + payeeName
				+ ", payeeNRC=" + payeeNRC + ", payeePhone=" + payeePhone + ", field1=" + field1 + ", field2=" + field2
				+ ", sKey=" + sKey + "}";
	}

}
