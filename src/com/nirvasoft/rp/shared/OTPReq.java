
package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OTPReq {
	private String type;
	private String userID;
	private String sessionID;
	private String merchantID;
	private String sKey;
	private String flag;
	private String deviceID;

	public String getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public OTPReq() {
		clearProperties();
	}

	private void clearProperties() {
		this.type = "";
		this.userID = "";
		this.sessionID = "";
		this.merchantID = "";
		this.sKey = "";
	}

	public String getMerchantID() {
		return merchantID;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getsKey() {
		return sKey;
	}

	public String getType() {
		return type;
	}

	public String getUserID() {
		return userID;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setsKey(String sKey) {
		this.sKey = sKey;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	@Override
	public String toString() {
		return "{ type=" + type + ", userID=" + userID + ", sessionID=" + sessionID + ", merchantID = " + merchantID
				+ ",sKey = " + sKey + "}";
	}

}
