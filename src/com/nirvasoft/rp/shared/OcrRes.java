package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OcrRes {
	private String side;
	private String nrcID;
	private String issueDate;
	private String name;
	private String fatherName;
	private String birth;
	private String blReligion;
	private String height;
	private String bloodGroup;
	private String cl;
	private String cardID;
	private String nrcIDBack;
	private String profession;
	private String address;
	private String code;
	private String desc;

	public OcrRes() {
		clearProperty();
	}

	private void clearProperty() {
		side = "";
		nrcID = "";
		issueDate = "";
		name = "";
		fatherName = "";
		birth = "";
		blReligion = "";
		height = "";
		bloodGroup = "";
		cl = "";
		cardID = "";
		nrcIDBack = "";
		profession = "";
		address = "";
		code = "";
		desc = "";
	}

	public String getAddress() {
		return address;
	}

	public String getBirth() {
		return birth;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public String getBlReligion() {
		return blReligion;
	}

	public String getCardID() {
		return cardID;
	}

	public String getCl() {
		return cl;
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public String getFatherName() {
		return fatherName;
	}

	public String getHeight() {
		return height;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public String getName() {
		return name;
	}

	public String getNrcID() {
		return nrcID;
	}

	public String getNrcIDBack() {
		return nrcIDBack;
	}

	public String getProfession() {
		return profession;
	}

	public String getSide() {
		return side;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setBirth(String birth) {
		this.birth = birth;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public void setBlReligion(String blReligion) {
		this.blReligion = blReligion;
	}

	public void setCardID(String cardID) {
		this.cardID = cardID;
	}

	public void setCl(String cl) {
		this.cl = cl;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNrcID(String nrcID) {
		this.nrcID = nrcID;
	}

	public void setNrcIDBack(String nrcIDBack) {
		this.nrcIDBack = nrcIDBack;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public void setSide(String side) {
		this.side = side;
	}
}
