package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class WalletResponseData {
	private String messageCode = "";
	private String messageDesc = "";
	private String accountNo = "";
	private String sessionID = "";
	private String sKey = "";
	private String name = "";
	private String nrc = "";
	private String field1 = "";
	private String field2 = "";
	private String institutionCode = "";
	private String institutionName = "";
	private double balance = 0.00;
	private String t16;// user profile photo
	private String region;
	private String regionNumber;
	private String alreadyRegister;
	private String[] channels;
	private String type;
	private String uuid;
	private String userId;
	private long appSyskey;	

	public WalletResponseData() {
		clearProperty();
	}

	private void clearProperty() {
		this.messageCode = "";
		this.messageDesc = "";
		this.accountNo = "";
		this.sessionID = "";
		this.sKey = "";
		this.name = "";
		this.nrc = "";
		this.field1 = "";
		this.field2 = "";
		this.institutionCode = "";
		this.institutionName = "";
		this.balance = 0.00;
		this.t16 = "";
		this.region = "";
		this.alreadyRegister = "";
		this.regionNumber = "";
		this.channels = null;
		this.type = "";
		this.uuid = "";
		this.userId = "";
		this.appSyskey = 0;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public String getAlreadyRegister() {
		return alreadyRegister;
	}

	public double getBalance() {
		return balance;
	}

	public String[] getChannels() {
		return channels;
	}

	public String getField1() {
		return field1;
	}

	public String getField2() {
		return field2;
	}

	public String getInstitutionCode() {
		return institutionCode;
	}

	public String getInstitutionName() {
		return institutionName;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public String getMessageDesc() {
		return messageDesc;
	}

	public String getName() {
		return name;
	}

	public String getNrc() {
		return nrc;
	}

	public String getRegion() {
		return region;
	}

	public String getRegionNumber() {
		return regionNumber;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getsKey() {
		return sKey;
	}

	public String getT16() {
		return t16;
	}

	public String getType() {
		return type;
	}

	public String getUserId() {
		return userId;
	}

	public String getUuid() {
		return uuid;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public void setAlreadyRegister(String alreadyRegister) {
		this.alreadyRegister = alreadyRegister;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public void setChannels(String[] channels) {
		this.channels = channels;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}

	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public void setMessageDesc(String messageDesc) {
		this.messageDesc = messageDesc;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNrc(String nrc) {
		this.nrc = nrc;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public void setRegionNumber(String regionNumber) {
		this.regionNumber = regionNumber;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setsKey(String sKey) {
		this.sKey = sKey;
	}

	public void setT16(String t16) {
		this.t16 = t16;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public long getAppSyskey() {
		return appSyskey;
	}

	public void setAppSyskey(long appSyskey) {
		this.appSyskey = appSyskey;
	}

}
