package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CNPTransData {
	private String drAcc;
	private String crAcc;
	private String aFromDate;
	private String aToDate;
	private String select;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private String msgstatus;
	private String flexcubeId;
	private String xref;
	private double commission;
	private String merchantId;
	private String amount;
	private String accType;
	private String status;
	private String batchNo;
	private double grandTotal;
	private String autoKey;
	private String nrmsg;
	private String activebankname;
	private String custRefNo;
	private String trandate;
	private String sessionID;
	private String userID;
	private String rpTitle;
	private String merchantName;
	private String processingCode;
	private String bStatus;

	public CNPTransData() {
		clearProperty();
	}

	void clearProperty() {
		select = "";
		msgstatus = "";
		trandate = "";
		aFromDate = "";
		aToDate = "";
		accType = "";
		status = "";
		drAcc = "";
		crAcc = "";
		flexcubeId = "";
		xref = "";
		merchantId = "";
		grandTotal = 0.00;
		amount = "0.00";
		commission = 0.00;
		totalCount = 0;
		currentPage = 0;
		pageSize = 0;
		autoKey = "";
		batchNo = "";
		custRefNo = "";
	}

	public String getbStatus() {
		return bStatus;
	}

	public void setbStatus(String bStatus) {
		this.bStatus = bStatus;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getProcessingCode() {
		return processingCode;
	}

	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}

	public String getAccType() {
		return accType;
	}

	public String getActivebankname() {
		return activebankname;
	}

	public String getaFromDate() {
		return aFromDate;
	}

	public String getAmount() {
		return amount;
	}

	public String getaToDate() {
		return aToDate;
	}

	public String getAutoKey() {
		return autoKey;
	}

	public String getBatchNo() {
		return batchNo;
	}

	public double getCommission() {
		return commission;
	}

	public String getCrAcc() {
		return crAcc;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public String getCustRefNo() {
		return custRefNo;
	}

	public String getDrAcc() {
		return drAcc;
	}

	public String getFlexcubeId() {
		return flexcubeId;
	}

	public double getGrandTotal() {
		return grandTotal;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public String getMsgstatus() {
		return msgstatus;
	}

	public String getNrmsg() {
		return nrmsg;
	}

	public int getPageSize() {
		return pageSize;
	}

	public String getSelect() {
		return select;
	}

	public String getStatus() {
		return status;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public String getTrandate() {
		return trandate;
	}

	public String getXref() {
		return xref;
	}

	public void setAccType(String accType) {
		this.accType = accType;
	}

	public void setActivebankname(String activebankname) {
		this.activebankname = activebankname;
	}

	public void setaFromDate(String aFromDate) {
		this.aFromDate = aFromDate;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public void setaToDate(String aToDate) {
		this.aToDate = aToDate;
	}

	public void setAutoKey(String autoKey) {
		this.autoKey = autoKey;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public void setCommission(double commission) {
		this.commission = commission;
	}

	public void setCrAcc(String crAcc) {
		this.crAcc = crAcc;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setCustRefNo(String custRefNo) {
		this.custRefNo = custRefNo;
	}

	public void setDrAcc(String drAcc) {
		this.drAcc = drAcc;
	}

	public void setFlexcubeId(String flexcubeId) {
		this.flexcubeId = flexcubeId;
	}

	public void setGrandTotal(double grandTotal) {
		this.grandTotal = grandTotal;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public void setMsgstatus(String msgstatus) {
		this.msgstatus = msgstatus;
	}

	public void setNrmsg(String nrmsg) {
		this.nrmsg = nrmsg;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setSelect(String select) {
		this.select = select;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public void setTrandate(String trandate) {
		this.trandate = trandate;
	}

	public void setXref(String xref) {
		this.xref = xref;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getRpTitle() {
		return rpTitle;
	}

	public void setRpTitle(String rpTitle) {
		this.rpTitle = rpTitle;
	}

}
