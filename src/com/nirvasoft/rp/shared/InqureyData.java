package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class InqureyData {

	private String userID;
	private String bankAcc;
	private String sessionID;

	private String merchantID;
	private String processingCode;
	private String amount;
	private String refNumber; // cardID
	private String field1; // Did
	private String field2; // PackageID
	private String field3; // Duration
	private String field4; // tid
	private String field5; // accesstoken
	private String field6; // packagename
	private String amountServiceCharges;
	private String amountTax;
	private String amountTotal;
	private String field7;
	private String field8;
	private String field9;
	private String field10;
	private String paymentType;
	private String narrative;
	private String sKey;

	public InqureyData() {
		clearProperties();
	}

	private void clearProperties() {
		this.field1 = "";
		this.field2 = "";
		this.field3 = "";
		this.field4 = "";
		this.field5 = "";
		this.field6 = "";
		this.field7 = "";
		this.field8 = "";
		this.field9 = "";
		this.field10 = "";
		this.amountServiceCharges = "0.00";
		this.amountTax = "0.00";
		this.bankAcc = "";
		this.amountTotal = "0.00";
		this.userID = "";
		this.field10 = "";
		this.merchantID = "";
		this.processingCode = "";
		this.paymentType = "";
		this.narrative = "";
		this.sKey = "";
	}

	public String getAmount() {
		return amount;
	}

	public String getAmountServiceCharges() {
		return amountServiceCharges;
	}

	public String getAmountTax() {
		return amountTax;
	}

	public String getAmountTotal() {
		return amountTotal;
	}

	public String getBankAcc() {
		return bankAcc;
	}

	public String getField1() {
		return field1;
	}

	public String getField10() {
		return field10;
	}

	public String getField2() {
		return field2;
	}

	public String getField3() {
		return field3;
	}

	public String getField4() {
		return field4;
	}

	public String getField5() {
		return field5;
	}

	public String getField6() {
		return field6;
	}

	public String getField7() {
		return field7;
	}

	public String getField8() {
		return field8;
	}

	public String getField9() {
		return field9;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public String getNarrative() {
		return narrative;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public String getProcessingCode() {
		return processingCode;
	}

	public String getRefNumber() {
		return refNumber;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getsKey() {
		return sKey;
	}

	public String getUserID() {
		return userID;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public void setAmountServiceCharges(String amountServiceCharges) {
		this.amountServiceCharges = amountServiceCharges;
	}

	public void setAmountTax(String tax) {
		this.amountTax = tax;
	}

	public void setAmountTotal(String totalamount) {
		this.amountTotal = totalamount;
	}

	public void setBankAcc(String bankAcc) {
		this.bankAcc = bankAcc;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public void setField10(String field10) {
		this.field10 = field10;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public void setField3(String field3) {
		this.field3 = field3;
	}

	public void setField4(String field4) {
		this.field4 = field4;
	}

	public void setField5(String field5) {
		this.field5 = field5;
	}

	public void setField6(String field6) {
		this.field6 = field6;
	}

	public void setField7(String field7) {
		this.field7 = field7;
	}

	public void setField8(String field8) {
		this.field8 = field8;
	}

	public void setField9(String field9) {
		this.field9 = field9;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public void setNarrative(String narrative) {
		this.narrative = narrative;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}

	public void setRefNumber(String refnumber) {
		this.refNumber = refnumber;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setsKey(String sKey) {
		this.sKey = sKey;
	}

	public void setUserID(String userid) {
		this.userID = userid;
	}

	@Override
	public String toString() {
		return "{userID=" + userID + ", bankAcc=" + bankAcc + ", sessionID=" + sessionID + ", merchantID=" + merchantID
				+ ", processingCode=" + processingCode + ", amount=" + amount + ", refNumber=" + refNumber + ", field1="
				+ field1 + ", field2=" + field2 + ", field3=" + field3 + ", field4=" + field4 + ", field5=" + field5
				+ ", field6=" + field6 + ", amountServiceCharges=" + amountServiceCharges + ", amountTax=" + amountTax
				+ ", amountTotal=" + amountTotal + ", field7=" + field7 + ", field8=" + field8 + ", field9=" + field9
				+ ", field10=" + field10 + ", paymentType = " + paymentType + ", narrative = " + narrative + ", sKey = "
				+ sKey + "}";
	}

}
