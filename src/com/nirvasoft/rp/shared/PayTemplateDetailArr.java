package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PayTemplateDetailArr {

	private String payCaption;
	private String paydatatype;
	private String paydispayfield;
	private int lovKey;
	private String fieldId;

	public PayTemplateDetailArr() {
		clearProperty();
	}

	private void clearProperty() {
		payCaption = "";
		paydatatype = "";
		paydispayfield = "";
		lovKey = 0;
		fieldId = "";
	}

	public String getFieldId() {
		return fieldId;
	}

	public int getLovKey() {
		return lovKey;
	}

	public String getPayCaption() {
		return payCaption;
	}

	public String getPaydatatype() {
		return paydatatype;
	}

	public String getPaydispayfield() {
		return paydispayfield;
	}

	public void setFieldId(String fieldId) {
		this.fieldId = fieldId;
	}

	public void setLovKey(int lovKey) {
		this.lovKey = lovKey;
	}

	public void setPayCaption(String payCaption) {
		this.payCaption = payCaption;
	}

	public void setPaydatatype(String paydatatype) {
		this.paydatatype = paydatatype;
	}

	public void setPaydispayfield(String paydispayfield) {
		this.paydispayfield = paydispayfield;
	}

}
