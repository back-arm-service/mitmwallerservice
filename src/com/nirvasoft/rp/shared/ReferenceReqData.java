package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ReferenceReqData {
	private String userID;
	private String sessionID;
	private String processingCode;

	public ReferenceReqData() {
		clearProperty();
	}

	void clearProperty() {
		userID = "";
		sessionID = "";
		processingCode = "";
	}

	public String getProcessingCode() {
		return processingCode;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getUserID() {
		return userID;
	}

	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setUserID(String userid) {
		this.userID = userid;
	}

	@Override
	public String toString() {
		return "{userID=" + userID + ", sessionID=" + sessionID + ", processingCode= " + processingCode + "}";
	}

}
