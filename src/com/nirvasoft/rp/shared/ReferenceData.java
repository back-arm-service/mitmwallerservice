package com.nirvasoft.rp.shared;

import java.io.Serializable;

public class ReferenceData implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1529942856113254434L;
	private int pKey;
	private String pDescription;
	private String pCode;
	private String pRemark;
	private String pT1;
	private String pT2;

	private double pN1;

	public String getCode() {
		return pCode;
	}

	public String getDescription() {
		return pDescription;
	}

	public int getKey() {
		return pKey;
	}

	public double getN1() {
		return pN1;
	}

	public String getRemark() {
		return pRemark;
	}

	public String getT1() {
		return pT1;
	}

	public String getT2() {
		return pT2;
	}

	public void setCode(String p) {
		pCode = p;
	}

	public void setDescription(String p) {
		pDescription = p;
	}

	public void setKey(int p) {
		pKey = p;
	}

	public void setN1(double pCurUnit) {
		this.pN1 = pCurUnit;
	}

	public void setRemark(String p) {
		pRemark = p;
	}

	public void setT1(String pCurUnit) {
		this.pT1 = pCurUnit;
	}

	public void setT2(String pCurUnitDecimal) {
		this.pT2 = pCurUnitDecimal;
	}

}
