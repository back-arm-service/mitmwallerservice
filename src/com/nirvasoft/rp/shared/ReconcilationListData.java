package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ReconcilationListData {

	private int srno;
	private String transactionDateTime;
	private String kbzTransID;
	private String transID;
	private String transStatus;
	private String amount;
	private String remark;

	public ReconcilationListData() {
		clearProperty();
	}

	public void clearProperty() {
		this.srno = 0;
		this.transactionDateTime = "";
		this.kbzTransID = "";
		this.transID = "";
		this.transStatus = "";
		this.amount = "";
		this.remark = "";
	}

	public String getAmount() {
		return amount;
	}

	public String getKbzTransID() {
		return kbzTransID;
	}

	public String getRemark() {
		return remark;
	}

	public int getSrno() {
		return srno;
	}

	public String getTransactionDateTime() {
		return transactionDateTime;
	}

	public String getTransID() {
		return transID;
	}

	public String getTransStatus() {
		return transStatus;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public void setKbzTransID(String kbzTransID) {
		this.kbzTransID = kbzTransID;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public void setSrno(int srno) {
		this.srno = srno;
	}

	public void setTransactionDateTime(String transactionDateTime) {
		this.transactionDateTime = transactionDateTime;
	}

	public void setTransID(String transID) {
		this.transID = transID;
	}

	public void setTransStatus(String transStatus) {
		this.transStatus = transStatus;
	}

}
