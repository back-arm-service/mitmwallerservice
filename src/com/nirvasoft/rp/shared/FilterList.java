package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FilterList {
	private String org;
	private String division;
	private String district;

	public FilterList() {
		clearProperty();
	}

	private void clearProperty() {
		org = "";
		division = "";
		district = "";
	}

	public String getDistrict() {
		return district;
	}

	public String getDivision() {
		return division;
	}

	public String getOrg() {
		return org;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	@Override
	public String toString() {
		return "FilterList [org=" + org + ", division=" + division + ", district=" + district + "]";
	}

}
