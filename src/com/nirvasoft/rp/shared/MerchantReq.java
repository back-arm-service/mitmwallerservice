package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MerchantReq {
	private String userID;
	private String sessionID;
	private String paymentType;

	public MerchantReq() {
		clearProperty();
	}

	void clearProperty() {
		userID = "";
		sessionID = "";
		paymentType = "";
	}

	public String getPaymentType() {
		return paymentType;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getUserID() {
		return userID;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setUserID(String userid) {
		this.userID = userid;
	}

	@Override
	public String toString() {
		return "{userID=" + userID + ", sessionID=" + sessionID + " , paymentType = " + paymentType + "}";
	}

}
