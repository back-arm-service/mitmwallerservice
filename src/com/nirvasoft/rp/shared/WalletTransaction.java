package com.nirvasoft.rp.shared;

public class WalletTransaction {
	private long autoKey;
	private String createdDate;
	private String modifiedDate;
	private String userID;
	private String fromWallet;
	private String toWallet;
	private String fromOperator;
	private String toOperator;
	private String fromAccount;
	private String toAccount;
	private double amount;
	private double bankCharges;
	private double commissionCharges;
	private String transID;
	private String switchKey;
	private String refKey;
	private String transType;
	private int transStatus;
	private String code;
	private String desc;
	private String notiMessage;
	private String notiType;
	private int notiKey;
	private String transDate;
	private String merchantCode;//atn
	private String merchantID;//atn
	private String t1;
	private String t2;
	private String t3;
	private int n1;
	private int n2;
	private int n3;
	private String effectiveDate;
	private String transTime;
	private String toDescription;
	private String dueDate;
	private String otherStatus;
	private String pendltyaccount;
	private String pendltyamount;
	private String discountAmount;
	

	public WalletTransaction() {
		clearProperty();
	}

	private void clearProperty() {
		autoKey = 0;
		createdDate = "";
		modifiedDate = "";
		userID = "";
		fromWallet = "";
		toWallet = "";
		fromOperator = "";
		toOperator = "";
		fromAccount = "";
		toAccount = "";
		amount = 0;
		bankCharges = 0;
		commissionCharges = 0;
		transID = "";
		switchKey = "";
		refKey = "";
		transType = "";
		transStatus = 0;
		code = "";
		desc = "";
		notiMessage = "";
		notiType = "";
		notiKey = 0;
		transDate = "";
		merchantCode = "";
		merchantID = "";
		t1 = "";
		t2 = "";
		t3 = "";
		n1 = 0;
		n2 = 0;
		n3 = 0;
		effectiveDate= "";
		transTime= "";
		toDescription= "";
		dueDate= "";
		otherStatus= "";
		pendltyaccount= "";
		pendltyamount= "";
		discountAmount= "";
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getTransTime() {
		return transTime;
	}

	public void setTransTime(String transTime) {
		this.transTime = transTime;
	}

	public String getToDescription() {
		return toDescription;
	}

	public void setToDescription(String toDescription) {
		this.toDescription = toDescription;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getOtherStatus() {
		return otherStatus;
	}

	public void setOtherStatus(String otherStatus) {
		this.otherStatus = otherStatus;
	}

	public String getPendltyaccount() {
		return pendltyaccount;
	}

	public void setPendltyaccount(String pendltyaccount) {
		this.pendltyaccount = pendltyaccount;
	}

	public String getPendltyamount() {
		return pendltyamount;
	}

	public void setPendltyamount(String pendltyamount) {
		this.pendltyamount = pendltyamount;
	}

	public String getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(String discountAmount) {
		this.discountAmount = discountAmount;
	}

	public String getMerchantCode() {
		return merchantCode;
	}

	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public double getAmount() {
		return amount;
	}

	public long getAutoKey() {
		return autoKey;
	}

	public double getBankCharges() {
		return bankCharges;
	}

	public String getCode() {
		return code;
	}

	public double getCommissionCharges() {
		return commissionCharges;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public String getDesc() {
		return desc;
	}

	public String getFromAccount() {
		return fromAccount;
	}

	public String getFromOperator() {
		return fromOperator;
	}

	public String getFromWallet() {
		return fromWallet;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public int getN1() {
		return n1;
	}

	public int getN2() {
		return n2;
	}

	public int getN3() {
		return n3;
	}

	public int getNotiKey() {
		return notiKey;
	}

	public String getNotiMessage() {
		return notiMessage;
	}

	public String getNotiType() {
		return notiType;
	}

	public String getRefKey() {
		return refKey;
	}

	public String getSwitchKey() {
		return switchKey;
	}

	public String getT1() {
		return t1;
	}

	public String getT2() {
		return t2;
	}

	public String getT3() {
		return t3;
	}

	public String getToAccount() {
		return toAccount;
	}

	public String getToOperator() {
		return toOperator;
	}

	public String getToWallet() {
		return toWallet;
	}

	public String getTransDate() {
		return transDate;
	}

	public String getTransID() {
		return transID;
	}

	public int getTransStatus() {
		return transStatus;
	}

	public String getTransType() {
		return transType;
	}

	public String getUserID() {
		return userID;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void setAutoKey(long autoKey) {
		this.autoKey = autoKey;
	}

	public void setBankCharges(double bankCharges) {
		this.bankCharges = bankCharges;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCommissionCharges(double commissionCharges) {
		this.commissionCharges = commissionCharges;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setFromAccount(String fromAccount) {
		this.fromAccount = fromAccount;
	}

	public void setFromOperator(String fromOperator) {
		this.fromOperator = fromOperator;
	}

	public void setFromWallet(String fromWallet) {
		this.fromWallet = fromWallet;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public void setN2(int n2) {
		this.n2 = n2;
	}

	public void setN3(int n3) {
		this.n3 = n3;
	}

	public void setNotiKey(int notiKey) {
		this.notiKey = notiKey;
	}

	public void setNotiMessage(String notiMessage) {
		this.notiMessage = notiMessage;
	}

	public void setNotiType(String notiType) {
		this.notiType = notiType;
	}

	public void setRefKey(String refKey) {
		this.refKey = refKey;
	}

	public void setSwitchKey(String switchKey) {
		this.switchKey = switchKey;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public void setToAccount(String toAccount) {
		this.toAccount = toAccount;
	}

	public void setToOperator(String toOperator) {
		this.toOperator = toOperator;
	}

	public void setToWallet(String toWallet) {
		this.toWallet = toWallet;
	}

	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}

	public void setTransID(String transID) {
		this.transID = transID;
	}

	public void setTransStatus(int transStatus) {
		this.transStatus = transStatus;
	}

	public void setTransType(String transType) {
		this.transType = transType;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

}
