package com.nirvasoft.rp.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

import com.nirvasoft.rp.framework.LOVDetails;

@XmlRootElement
public class AccountTypeListResponseData {

	private String code;
	private String desc;
	private LOVDetails[] accountTypeList;

	public AccountTypeListResponseData() {
		this.clearProperty();
	}

	private void clearProperty() {
		this.code = "";
		this.desc = "";
		this.accountTypeList = null;
	}

	public LOVDetails[] getAccountTypeList() {
		return accountTypeList;
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public void setAccountTypeList(LOVDetails[] accountTypeList) {
		this.accountTypeList = accountTypeList;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public String toString() {
		return "AccountTypeListResponseData [code=" + code + ", desc=" + desc + ", accountTypeList="
				+ Arrays.toString(accountTypeList) + "]";
	}

}
