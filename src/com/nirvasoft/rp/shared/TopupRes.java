package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TopupRes {

	private String messageCode;
	private String messageDesc;
	private String bankRefNo;
	private String transDate;
	private String field1;
	private String field2;

	public TopupRes() {
		clearProperty();
	}

	private void clearProperty() {
		messageCode = "";
		messageDesc = "";
		bankRefNo = "";
		transDate = "";
		field1 = "";
		field2 = "";
	}

	public String getBankRefNo() {
		return bankRefNo;
	}

	public String getField1() {
		return field1;
	}

	public String getField2() {
		return field2;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public String getMessageDesc() {
		return messageDesc;
	}

	public String getTransDate() {
		return transDate;
	}

	public void setBankRefNo(String bankRefNo) {
		this.bankRefNo = bankRefNo;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public void setMessageDesc(String messageDesc) {
		this.messageDesc = messageDesc;
	}

	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}

	@Override
	public String toString() {
		return "TopupRes [messageCode=" + messageCode + ", messageDesc=" + messageDesc + ", bankRefNo=" + bankRefNo
				+ ", transDate=" + transDate + ", field1=" + field1 + ", field2=" + field2 + "]";
	}

}
