package com.nirvasoft.rp.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ChequeAccDataResult {
	private String code = "";
	private String desc = "";
	private ChequeAccData[] dataList = null;

	public ChequeAccDataResult() {
		clearProperties();
	}

	private void clearProperties() {
		this.code = "";
		this.desc = "";
		this.dataList = null;
	}

	public String getCode() {
		return code;
	}

	public ChequeAccData[] getDataList() {
		return dataList;
	}

	public String getDesc() {
		return desc;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDataList(ChequeAccData[] dataList) {
		this.dataList = dataList;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public String toString() {
		return "ChequeAccDataResult {code=" + code + ", desc=" + desc + ", dataList=" + Arrays.toString(dataList) + "}";
	}

}
