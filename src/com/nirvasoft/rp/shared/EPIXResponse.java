package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EPIXResponse {

	private String xref;
	private String code;
	private String desc;
	private String flexcubeid;

	private String param1;// phone no.
	private String param2;// email
	private String charges;//atn
	private String effectiveDate;
	private String comm1;
	private String comm2;

	public EPIXResponse() {
		clearProperty();
	}

	void clearProperty() {
		comm1= "";
		comm2= "";
		xref = "";
		code = "";
		desc = "";
		flexcubeid = "";
		param1 = "";
		param2 = "";
		effectiveDate = "";
	}

	public String getComm1() {
		return comm1;
	}

	public void setComm1(String comm1) {
		this.comm1 = comm1;
	}

	public String getComm2() {
		return comm2;
	}

	public void setComm2(String comm2) {
		this.comm2 = comm2;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getCharges() {
		return charges;
	}

	public void setCharges(String charges) {
		this.charges = charges;
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public String getFlexcubeid() {
		return flexcubeid;
	}

	public String getParam1() {
		return param1;
	}

	public String getParam2() {
		return param2;
	}

	public String getXref() {
		return xref;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setFlexcubeid(String flexcubeid) {
		this.flexcubeid = flexcubeid;
	}

	public void setParam1(String param1) {
		this.param1 = param1;
	}

	public void setParam2(String param2) {
		this.param2 = param2;
	}

	public void setXref(String xref) {
		this.xref = xref;
	}

}
