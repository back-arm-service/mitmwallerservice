package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ResponseBalance {

	private String code = "";
	private String desc = "";
	private String accountNo = "";
	private double balance = 0.00;

	public ResponseBalance() {
		clearProperty();
	}

	private void clearProperty() {
		code = "";
		desc = "";
		accountNo = "";
		balance = 0.00;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public double getBalance() {
		return balance;
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
