package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PackageDataResponse {

	private String code;
	private String desc;
	private PackageDataResult PackageDataResult;

	public PackageDataResponse() {
		clearProperty();
	}

	void clearProperty() {
		code = "";
		desc = "";
		PackageDataResult = new PackageDataResult();
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public PackageDataResult getResult() {
		return PackageDataResult;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setResult(PackageDataResult result) {
		this.PackageDataResult = result;
	}

	@Override
	public String toString() {
		return "{code=" + code + ", desc=" + desc + ", PackageDataResult=" + PackageDataResult + "}";
	}
}
