package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ForceChangePwdReq {
	private String userID;
	private String sessionID;
	private String oldPassword;
	private String newPassword;

	public ForceChangePwdReq() {
		clearProperties();
		// TODO Auto-generated constructor stub
	}

	private void clearProperties() {
		// TODO Auto-generated method stub
		oldPassword = "";
		newPassword = "";
		userID = "";
		sessionID = "";
	}

	public String getNewPassword() {
		return newPassword;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getUserID() {
		return userID;
	}

	public void setNewPassword(String newpassword) {
		this.newPassword = newpassword;
	}

	public void setOldPassword(String password) {
		this.oldPassword = password;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setUserID(String userid) {
		this.userID = userid;
	}

	@Override
	public String toString() {
		return "{userID=" + userID + ", sessionID=" + sessionID + ", oldPassword=" + oldPassword + ", newPassword="
				+ newPassword + "}";
	}

}
