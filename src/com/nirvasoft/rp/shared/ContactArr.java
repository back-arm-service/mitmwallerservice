package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ContactArr {
	private String code;
	private String desc;
	private Contact[] dataList;

	public ContactArr() {
		code = "";
		desc = "";
		dataList = null;
	}

	public String getCode() {
		return code;
	}

	public Contact[] getDataList() {
		return dataList;
	}

	public String getDesc() {
		return desc;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDataList(Contact[] dataList) {
		this.dataList = dataList;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
