package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ChangePwdRes {

	private String userID;
	private String sessionID;
	private String otpCode;
	private String rKey;
	private String sKey;
	private String oldPassword;
	private String newPassword;

	void clearProperty() {
		userID = "";
		otpCode = "";
		sessionID = "";
		oldPassword = "";
		newPassword = "";
		rKey = "";
		sKey = "";
	}

	public String getNewPassword() {
		return newPassword;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public String getOtpCode() {
		return otpCode;
	}

	public String getrKey() {
		return rKey;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getsKey() {
		return sKey;
	}

	public String getUserID() {
		return userID;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public void setOtpCode(String otpCode) {
		this.otpCode = otpCode;
	}

	public void setrKey(String rKey) {
		this.rKey = rKey;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setsKey(String sKey) {
		this.sKey = sKey;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	@Override
	public String toString() {
		return "{userID=" + userID + ", sessionID=" + sessionID + ", otpCode=" + otpCode + ", rKey=" + rKey
				+ ", oldPassword=" + oldPassword + ", newPassword=" + newPassword + ", sKey=" + sKey + "}";
	}

}
