package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserDetailReportList {

	private UserDetailReportData[] data;
	private PayTemplateDetailArr[] detailData;
	private int totalCount;
	private int currentPage;
	private int pageSize;

	private String msgCode = "";
	private String msgDesc = "";

	public UserDetailReportList() {
		clearProperty();
	}

	private void clearProperty() {
		data = null;
		detailData = null;
		totalCount = 0;
		currentPage = 0;
		pageSize = 0;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public UserDetailReportData[] getData() {
		return data;
	}

	public PayTemplateDetailArr[] getDetailData() {
		return detailData;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setData(UserDetailReportData[] data) {
		this.data = data;
	}

	public void setDetailData(PayTemplateDetailArr[] detailData) {
		this.detailData = detailData;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

}
