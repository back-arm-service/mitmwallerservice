package com.nirvasoft.rp.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ForeignExchangeRateResult {

	private String code;
	private String desc;
	private ForeignExchangeRateData[] data = null;

	public ForeignExchangeRateResult() {
		clearProperty();
	}

	void clearProperty() {
		code = "";
		desc = "";
	}

	public String getCode() {
		return code;
	}

	public ForeignExchangeRateData[] getData() {
		return data;
	}

	public String getDesc() {
		return desc;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setData(ForeignExchangeRateData[] data) {
		this.data = data;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public String toString() {
		return "{code=" + code + ", desc=" + desc + ", data=" + Arrays.toString(data) + "}";
	}

}
