package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CommonData {

	private long syskey;
	private long autokey;
	private String createdDate;
	private String createdTime;
	private String modifiedDate;
	private String modifiedTime;
	private String userId;
	private String userName;
	private int recordStatus;
	private int syncStatus;
	private long syncBatch;
	private long userSyskey;
	private String t1;
	private String t2;
	private String t3;
	private String t4;
	private String t5;
	private String t6;
	private String t7;
	private String t8;
	private String t9;
	private String t10;
	private String t11;
	private String t12;
	private String t13;
	private String t14;
	private String t15;
	private String t16;
	private String t17;
	private String t18;
	private String t19;
	private String t20;
	private long n1;
	private long n2;
	private long n3;
	private long n4;
	private long n5;
	private long n6;
	private long n7;
	private long n8;
	private long n9;
	private long n10;
	private long n11;
	private long n12;
	private long n13;
	private long n14;
	private long n15;
	private long n16;
	private long n17;
	private long n18;
	private long n19;
	private long n20;
	private String upload[];
	private String reply[];
	private CommonData[] replyData;
	private String msg;
	private String count;
	private String channelheader;
	private String channelName;
	private int isGroup;
	private String cuserSyskey[];
	private String alluser[];
	private long channelkey;
	private CommonData[] person;

	public CommonData() {
		super();
		clearProperties();
	}

	protected void clearProperties() {
		this.syskey = 0L;
		this.autokey = 0L;
		this.createdDate = "";
		this.createdTime = "";
		this.modifiedDate = "";
		this.modifiedTime = "";
		this.userId = "";
		this.userName = "";
		this.recordStatus = 0;
		this.syncStatus = 0;
		this.syncBatch = 0L;
		this.userSyskey = 0L;
		this.t1 = "";
		this.t2 = "";
		this.t3 = "";
		this.t4 = "";
		this.t5 = "";
		this.t6 = "";
		this.t7 = "";
		this.t8 = "";
		this.t9 = "";
		this.t10 = "";
		this.t11 = "";
		this.t12 = "";
		this.t13 = "";
		this.t14 = "";
		this.t15 = "";
		this.t16 = "";
		this.t17 = "";
		this.t18 = "";
		this.t19 = "";
		this.t20 = "";
		this.n1 = 0;
		this.n2 = 0;
		this.n3 = 0;
		this.n4 = 0;
		this.n5 = 0;
		this.n6 = 0;
		this.n7 = 0;
		this.n8 = 0;
		this.n9 = 0;
		this.n10 = 0;
		this.n11 = 0;
		this.n12 = 0;
		this.n13 = 0;
		this.n14 = 0;
		this.n15 = 0;
		this.n16 = 0;
		this.n17 = 0;
		this.n18 = 0;
		this.n19 = 0;
		this.n20 = 0;
		this.upload = new String[0];
		this.reply = new String[0];
		this.msg = "";
		this.count = "";
		this.cuserSyskey = new String[0];
		this.alluser = new String[0];
		this.person = new CommonData[0];

	}

	public String[] getAlluser() {
		return alluser;
	}

	public long getAutokey() {
		return autokey;
	}

	public String getChannelheader() {
		return channelheader;
	}

	public long getChannelkey() {
		return channelkey;
	}

	public String getChannelName() {
		return channelName;
	}

	public String getCount() {
		return count;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public String getCreatedTime() {
		return createdTime;
	}

	public String[] getCuserSyskey() {
		return cuserSyskey;
	}

	public int getIsGroup() {
		return isGroup;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public String getModifiedTime() {
		return modifiedTime;
	}

	public String getMsg() {
		return msg;
	}

	public long getN1() {
		return n1;
	}

	public long getN10() {
		return n10;
	}

	public long getN11() {
		return n11;
	}

	public long getN12() {
		return n12;
	}

	public long getN13() {
		return n13;
	}

	public long getN14() {
		return n14;
	}

	public long getN15() {
		return n15;
	}

	public long getN16() {
		return n16;
	}

	public long getN17() {
		return n17;
	}

	public long getN18() {
		return n18;
	}

	public long getN19() {
		return n19;
	}

	public long getN2() {
		return n2;
	}

	public long getN20() {
		return n20;
	}

	public long getN3() {
		return n3;
	}

	public long getN4() {
		return n4;
	}

	public long getN5() {
		return n5;
	}

	public long getN6() {
		return n6;
	}

	public long getN7() {
		return n7;
	}

	public long getN8() {
		return n8;
	}

	public long getN9() {
		return n9;
	}

	public CommonData[] getPerson() {
		return person;
	}

	public int getRecordStatus() {
		return recordStatus;
	}

	public String[] getReply() {
		return reply;
	}

	public CommonData[] getReplyData() {
		return replyData;
	}

	public long getSyncBatch() {
		return syncBatch;
	}

	public int getSyncStatus() {
		return syncStatus;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getT1() {
		return t1;
	}

	public String getT10() {
		return t10;
	}

	public String getT11() {
		return t11;
	}

	public String getT12() {
		return t12;
	}

	public String getT13() {
		return t13;
	}

	public String getT14() {
		return t14;
	}

	public String getT15() {
		return t15;
	}

	public String getT16() {
		return t16;
	}

	public String getT17() {
		return t17;
	}

	public String getT18() {
		return t18;
	}

	public String getT19() {
		return t19;
	}

	public String getT2() {
		return t2;
	}

	public String getT20() {
		return t20;
	}

	public String getT3() {
		return t3;
	}

	public String getT4() {
		return t4;
	}

	public String getT5() {
		return t5;
	}

	public String getT6() {
		return t6;
	}

	public String getT7() {
		return t7;
	}

	public String getT8() {
		return t8;
	}

	public String getT9() {
		return t9;
	}

	public String[] getUpload() {
		return upload;
	}

	public String getUserId() {
		return userId;
	}

	public String getUserName() {
		return userName;
	}

	public long getUserSyskey() {
		return userSyskey;
	}

	public void setAlluser(String[] alluser) {
		this.alluser = alluser;
	}

	public void setAutokey(long autokey) {
		this.autokey = autokey;
	}

	public void setChannelheader(String channelheader) {
		this.channelheader = channelheader;
	}

	public void setChannelkey(long channelkey) {
		this.channelkey = channelkey;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	public void setCuserSyskey(String[] cuserSyskey) {
		this.cuserSyskey = cuserSyskey;
	}

	public void setIsGroup(int isGroup) {
		this.isGroup = isGroup;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public void setModifiedTime(String modifiedTime) {
		this.modifiedTime = modifiedTime;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public void setN1(long n1) {
		this.n1 = n1;
	}

	public void setN10(long n10) {
		this.n10 = n10;
	}

	public void setN11(long n11) {
		this.n11 = n11;
	}

	public void setN12(long n12) {
		this.n12 = n12;
	}

	public void setN13(long n13) {
		this.n13 = n13;
	}

	public void setN14(long n14) {
		this.n14 = n14;
	}

	public void setN15(long n15) {
		this.n15 = n15;
	}

	public void setN16(long n16) {
		this.n16 = n16;
	}

	public void setN17(long n17) {
		this.n17 = n17;
	}

	public void setN18(long n18) {
		this.n18 = n18;
	}

	public void setN19(long n19) {
		this.n19 = n19;
	}

	public void setN2(long n2) {
		this.n2 = n2;
	}

	public void setN20(long n20) {
		this.n20 = n20;
	}

	public void setN3(long n3) {
		this.n3 = n3;
	}

	public void setN4(long n4) {
		this.n4 = n4;
	}

	public void setN5(long n5) {
		this.n5 = n5;
	}

	public void setN6(long n6) {
		this.n6 = n6;
	}

	public void setN7(long n7) {
		this.n7 = n7;
	}

	public void setN8(long n8) {
		this.n8 = n8;
	}

	public void setN9(long n9) {
		this.n9 = n9;
	}

	public void setPerson(CommonData[] person) {
		this.person = person;
	}

	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}

	public void setReply(String[] reply) {
		this.reply = reply;
	}

	public void setReplyData(CommonData[] replyData) {
		this.replyData = replyData;
	}

	public void setSyncBatch(long syncBatch) {
		this.syncBatch = syncBatch;
	}

	public void setSyncStatus(int syncStatus) {
		this.syncStatus = syncStatus;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT10(String t10) {
		this.t10 = t10;
	}

	public void setT11(String t11) {
		this.t11 = t11;
	}

	public void setT12(String t12) {
		this.t12 = t12;
	}

	public void setT13(String t13) {
		this.t13 = t13;
	}

	public void setT14(String t14) {
		this.t14 = t14;
	}

	public void setT15(String t15) {
		this.t15 = t15;
	}

	public void setT16(String t16) {
		this.t16 = t16;
	}

	public void setT17(String t17) {
		this.t17 = t17;
	}

	public void setT18(String t18) {
		this.t18 = t18;
	}

	public void setT19(String t19) {
		this.t19 = t19;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setT20(String t20) {
		this.t20 = t20;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public void setT4(String t4) {
		this.t4 = t4;
	}

	public void setT5(String t5) {
		this.t5 = t5;
	}

	public void setT6(String t6) {
		this.t6 = t6;
	}

	public void setT7(String t7) {
		this.t7 = t7;
	}

	public void setT8(String t8) {
		this.t8 = t8;
	}

	public void setT9(String t9) {
		this.t9 = t9;
	}

	public void setUpload(String[] upload) {
		this.upload = upload;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setUserSyskey(long userSyskey) {
		this.userSyskey = userSyskey;
	}

}
