package com.nirvasoft.rp.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BillPaymentTransactionRes {

	private String code;
	private String desc;

	private String userID;
	private String merchantID;
	private String acctNo;

	private String durationType;
	private String fromDate;
	private String toDate;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private int pageCount;

	private BillPaymentTransaction[] billResults = null;

	public BillPaymentTransactionRes() {
		clearProperty();
	}

	private void clearProperty() {
		code = "";
		desc = "";
		merchantID = "";
		acctNo = "";
		userID = "";
		fromDate = "";
		toDate = "";
		durationType = "";

	}

	public String getAcctNo() {
		return acctNo;
	}

	public BillPaymentTransaction[] getBillResults() {
		return billResults;
	}

	public String getCode() {
		return code;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public String getDesc() {
		return desc;
	}

	public String getDurationType() {
		return durationType;
	}

	public String getFromDate() {
		return fromDate;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public int getPageCount() {
		return pageCount;
	}

	public int getPageSize() {
		return pageSize;
	}

	public String getToDate() {
		return toDate;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public String getUserID() {
		return userID;
	}

	public void setAcctNo(String accNumber) {
		this.acctNo = accNumber;
	}

	public void setBillResults(BillPaymentTransaction[] billResults) {
		this.billResults = billResults;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setDurationType(String durationType) {
		this.durationType = durationType;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public void setUserID(String userid) {
		this.userID = userid;
	}

	@Override
	public String toString() {
		return "{code=" + code + ", desc=" + desc + ", userID=" + userID + ", merchantID=" + merchantID + ", acctNo="
				+ acctNo + ", durationType=" + durationType + ", fromDate=" + fromDate + ", toDate=" + toDate
				+ ", totalCount=" + totalCount + ", currentPage=" + currentPage + ", pageSize=" + pageSize
				+ ", pageCount=" + pageCount + ", billResults=" + Arrays.toString(billResults) + "}";
	}

}
