package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class VersionRes {
	private String code = "";
	private String desc = "";
	private String version = "";
	private String versionTitle = "";
	private int versionStatus = 0;
	private String versionDesc = "";
	private String notiTitle = "";
	private String notiDesc = "";

	public VersionRes() {
		clearProperty();
	}

	private void clearProperty() {
		code = "";
		desc = "";
		versionStatus = 0;
		versionDesc = "";
		version = "";
		versionTitle = "";
		notiTitle = "";
		notiDesc = "";
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public String getNotiDesc() {
		return notiDesc;
	}

	public String getNotiTitle() {
		return notiTitle;
	}

	public String getVersion() {
		return version;
	}

	public String getVersionDesc() {
		return versionDesc;
	}

	public int getVersionStatus() {
		return versionStatus;
	}

	public String getVersionTitle() {
		return versionTitle;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setNotiDesc(String notiDesc) {
		this.notiDesc = notiDesc;
	}

	public void setNotiTitle(String notiTitle) {
		this.notiTitle = notiTitle;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setVersionDesc(String versionDesc) {
		this.versionDesc = versionDesc;
	}

	public void setVersionStatus(int versionStatus) {
		this.versionStatus = versionStatus;
	}

	public void setVersionTitle(String versionTitle) {
		this.versionTitle = versionTitle;
	}

	@Override
	public String toString() {
		return "{code=" + code + ", desc = " + desc + ", version = " + version + ", versionTitle = " + versionTitle
				+ ", versionStatus = " + versionStatus + ", versionDesc = " + versionDesc + "}";
	}

}
