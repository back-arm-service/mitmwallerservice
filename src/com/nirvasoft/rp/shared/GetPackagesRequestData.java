package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class GetPackagesRequestData {
	private String sessionID;
	private String userID;
	private String merchantID;
	private String processingCode;

	public GetPackagesRequestData() {
		clearProperties();
	}

	private void clearProperties() {
		this.sessionID = "";
		this.userID = "";
		this.merchantID = "";
		this.processingCode = "";
	}

	public String getmerchantID() {
		return merchantID;
	}

	public String getProcessingCode() {
		return processingCode;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getUserID() {
		return userID;
	}

	public void setmerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setUserID(String userid) {
		this.userID = userid;
	}

	@Override
	public String toString() {
		return "{sessionID=" + sessionID + ", userID=" + userID + ", merchantID=" + merchantID + ", processingCode="
				+ processingCode + "}";
	}

}
