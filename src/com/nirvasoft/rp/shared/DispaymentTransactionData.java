package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DispaymentTransactionData {
	private long syskey;
	private String transactiondate;
	private String tranrefnumber;
	private String activebranch;
	private String debitaccount;
	private String debitbranch;
	private String collectionaccount;
	private String branch;
	private double amount;
	private double commcharges;
	private double commucharges;
	private double totalamount;
	private double t15, t18;
	private String n2, n3, n4;
	private String amountst;
	private String totalamountst;
	private String commchargest;
	private String commuchargest;
	private String did;
	private String customername;
	private String description;

	private String customerCode;

	private String customerNumber;
	private String fileUploadDateTime;

	private String chequeNo;
	private String currencyCode;
	private String t7;
	private String t8;
	private String t10;
	private String t11;
	private String t12;
	private String t13;
	private String t16;
	private String t17;

	private String t19;
	private String t20;
	private String t21;
	private String t22;
	private String t23;
	private String t24;
	private String t25;
	private int n1;
	private int n7;
	private int n8;
	private int n9;
	private int n17;
	private int n18;
	private int n19;
	private int n20;
	private int n10;

	private String distributorName;
	private String distributorAddress;
	private String messageCode;
	private String messageDesc;

	public DispaymentTransactionData() {
		clearProperty();
	}

	private void clearProperty() {
		messageCode = "";
		messageDesc = "";
		transactiondate = "";
		tranrefnumber = "";
		activebranch = "";
		debitaccount = "";
		debitbranch = "";
		collectionaccount = "";
		branch = "";
		amount = 0.0;
		commcharges = 0.0;
		commucharges = 0.0;
		totalamount = 0.0;
		t18 = 0.0;
		t15 = 0.0;

		n2 = "";
		n3 = "";
		n4 = "";
		amountst = "";
		totalamountst = "";
		commchargest = "";
		commuchargest = "";
		did = "";
		customername = "";
		description = "";

		customerCode = "";

		customerNumber = "";
		fileUploadDateTime = "";

		chequeNo = "";
		currencyCode = "";
		t7 = "";
		t8 = "";
		t10 = "";
		t11 = "";
		t12 = "";
		t13 = "";
		t16 = "";
		t17 = "";

		t19 = "";
		t20 = "";
		t21 = "";
		t22 = "";
		t23 = "";
		t24 = "";
		t25 = "";
		n1 = 0;
		n7 = 0;
		n8 = 0;
		n9 = 0;
		n17 = 0;
		n18 = 0;
		n19 = 0;
		n20 = 0;
		n10 = 0;

		distributorName = "";
		distributorAddress = "";

	}

	public String getActivebranch() {
		return activebranch;
	}

	public Double getAmount() {
		return amount;
	}

	public String getAmountst() {
		return amountst;
	}

	public String getBranch() {
		return branch;
	}

	public String getChequeNo() {
		return chequeNo;
	}

	public String getCollectionaccount() {
		return collectionaccount;
	}

	public Double getCommcharges() {
		return commcharges;
	}

	public String getCommchargest() {
		return commchargest;
	}

	public Double getCommucharges() {
		return commucharges;
	}

	public String getCommuchargest() {
		return commuchargest;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public String getCustomername() {
		return customername;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public String getDebitaccount() {
		return debitaccount;
	}

	public String getDebitbranch() {
		return debitbranch;
	}

	public String getDescription() {
		return description;
	}

	public String getDid() {
		return did;
	}

	public String getDistributorAddress() {
		return distributorAddress;
	}

	public String getDistributorName() {
		return distributorName;
	}

	public String getFileUploadDateTime() {
		return fileUploadDateTime;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public String getMessageDesc() {
		return messageDesc;
	}

	public int getN1() {
		return n1;
	}

	public int getN10() {
		return n10;
	}

	public int getN17() {
		return n17;
	}

	public int getN18() {
		return n18;
	}

	public int getN19() {
		return n19;
	}

	public String getN2() {
		return n2;
	}

	public int getN20() {
		return n20;
	}

	public String getN3() {
		return n3;
	}

	public String getN4() {
		return n4;
	}

	public int getN7() {
		return n7;
	}

	public int getN8() {
		return n8;
	}

	public int getN9() {
		return n9;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getT10() {
		return t10;
	}

	public String getT11() {
		return t11;
	}

	public String getT12() {
		return t12;
	}

	public String getT13() {
		return t13;
	}

	public Double getT15() {
		return t15;
	}

	public String getT16() {
		return t16;
	}

	public String getT17() {
		return t17;
	}

	public Double getT18() {
		return t18;
	}

	public String getT19() {
		return t19;
	}

	public String getT20() {
		return t20;
	}

	public String getT21() {
		return t21;
	}

	public String getT22() {
		return t22;
	}

	public String getT23() {
		return t23;
	}

	public String getT24() {
		return t24;
	}

	public String getT25() {
		return t25;
	}

	public String getT7() {
		return t7;
	}

	public String getT8() {
		return t8;
	}

	public Double getTotalamount() {
		return totalamount;
	}

	public String getTotalamountst() {
		return totalamountst;
	}

	public String getTranrefnumber() {
		return tranrefnumber;
	}

	public String getTransactiondate() {
		return transactiondate;
	}

	public void setActivebranch(String activebranch) {
		this.activebranch = activebranch;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public void setAmountst(String amountst) {
		this.amountst = amountst;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}

	public void setCollectionaccount(String collectionaccount) {
		this.collectionaccount = collectionaccount;
	}

	public void setCommcharges(double commcharges) {
		this.commcharges = commcharges;
	}

	public void setCommcharges(Double commcharges) {
		this.commcharges = commcharges;
	}

	public void setCommchargest(String commchargest) {
		this.commchargest = commchargest;
	}

	public void setCommucharges(double commucharges) {
		this.commucharges = commucharges;
	}

	public void setCommucharges(Double commucharges) {
		this.commucharges = commucharges;
	}

	public void setCommuchargest(String commuchargest) {
		this.commuchargest = commuchargest;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public void setCustomername(String customername) {
		this.customername = customername;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public void setDebitaccount(String debitaccount) {
		this.debitaccount = debitaccount;
	}

	public void setDebitbranch(String debitbranch) {
		this.debitbranch = debitbranch;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDid(String did) {
		this.did = did;
	}

	public void setDistributorAddress(String distributorAddress) {
		this.distributorAddress = distributorAddress;
	}

	public void setDistributorName(String distributorName) {
		this.distributorName = distributorName;
	}

	public void setFileUploadDateTime(String fileUploadDateTime) {
		this.fileUploadDateTime = fileUploadDateTime;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public void setMessageDesc(String messageDesc) {
		this.messageDesc = messageDesc;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public void setN10(int n10) {
		this.n10 = n10;
	}

	public void setN17(int n17) {
		this.n17 = n17;
	}

	public void setN18(int n18) {
		this.n18 = n18;
	}

	public void setN19(int n19) {
		this.n19 = n19;
	}

	public void setN2(String n2) {
		this.n2 = n2;
	}

	public void setN20(int n20) {
		this.n20 = n20;
	}

	public void setN3(String n3) {
		this.n3 = n3;
	}

	public void setN4(String n4) {
		this.n4 = n4;
	}

	public void setN7(int n7) {
		this.n7 = n7;
	}

	public void setN8(int n8) {
		this.n8 = n8;
	}

	public void setN9(int n9) {
		this.n9 = n9;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setT10(String t10) {
		this.t10 = t10;
	}

	public void setT11(String t11) {
		this.t11 = t11;
	}

	public void setT12(String t12) {
		this.t12 = t12;
	}

	public void setT13(String t13) {
		this.t13 = t13;
	}

	public void setT15(double t15) {
		this.t15 = t15;
	}

	public void setT15(Double t15) {
		this.t15 = t15;
	}

	public void setT16(String t16) {
		this.t16 = t16;
	}

	public void setT17(String t17) {
		this.t17 = t17;
	}

	public void setT18(double t18) {
		this.t18 = t18;
	}

	public void setT18(Double t18) {
		this.t18 = t18;
	}

	public void setT19(String t19) {
		this.t19 = t19;
	}

	public void setT20(String t20) {
		this.t20 = t20;
	}

	public void setT21(String t21) {
		this.t21 = t21;
	}

	public void setT22(String t22) {
		this.t22 = t22;
	}

	public void setT23(String t23) {
		this.t23 = t23;
	}

	public void setT24(String t24) {
		this.t24 = t24;
	}

	public void setT25(String t25) {
		this.t25 = t25;
	}

	public void setT7(String t7) {
		this.t7 = t7;
	}

	public void setT8(String t8) {
		this.t8 = t8;
	}

	public void setTotalamount(double totalamount) {
		this.totalamount = totalamount;
	}

	public void setTotalamount(Double totalamount) {
		this.totalamount = totalamount;
	}

	public void setTotalamountst(String totalamountst) {
		this.totalamountst = totalamountst;
	}

	public void setTranrefnumber(String tranrefnumber) {
		this.tranrefnumber = tranrefnumber;
	}

	public void setTransactiondate(String transactiondate) {
		this.transactiondate = transactiondate;
	}

}
