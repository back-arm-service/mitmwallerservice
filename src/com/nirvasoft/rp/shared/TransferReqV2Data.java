package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TransferReqV2Data {

	private String apikey;
	private String transID;
	private String mpokey;
	private String refKey;
	private String fromoperator;
	private String fromaccount;
	private String fromwallet;
	private String fromname;
	private String tooperator;
	private String toaccount;
	private String towallet;
	private String toname;
	private double amount;
	private double comm1;
	private double comm2;
	private double comm3;
	private String narration;
	private int status;
	private String fromrescode;
	private String fromresdesc;
	private String torescode;
	private String toresdesc;
	private String fromreqdatetime;
	private String fromresdatetime;
	private String toreqdatetime;
	private String toresdatetime;
	private String t1;
	private String t2;
	private String t3;
	private String t4;
	private String t5;
	private int n1;
	private int n2;
	private int n3;
	private int n4;
	private int n5;

	public TransferReqV2Data() {
		clearproperty();
	}

	void clearproperty() {
		apikey = "";
		transID = "";
		mpokey = "";
		refKey = "";
		fromoperator = "";
		fromaccount = "";
		fromwallet = "";
		fromname = "";
		tooperator = "";
		toaccount = "";
		towallet = "";
		toname = "";
		amount = 0.00;
		comm1 = 0.00;
		comm2 = 0.00;
		comm3 = 0.00;
		narration = "";
		status = -9;
		fromrescode = "";
		fromresdesc = "";
		torescode = "";
		toresdesc = "";
		fromreqdatetime = "";
		fromresdatetime = "";
		toreqdatetime = "";
		toresdatetime = "";
		t1 = "";
		t2 = "";
		t3 = "";
		t4 = "";
		t5 = "";
		n1 = 0;
		n2 = 0;
		n3 = 0;
		n4 = 0;
		n5 = 0;
	}

	public double getAmount() {
		return amount;
	}

	public String getApikey() {
		return apikey;
	}

	public double getComm1() {
		return comm1;
	}

	public double getComm2() {
		return comm2;
	}

	public double getComm3() {
		return comm3;
	}

	public String getFromaccount() {
		return fromaccount;
	}

	public String getFromcode() {
		return fromrescode;
	}

	public String getFromdesc() {
		return fromresdesc;
	}

	public String getFromname() {
		return fromname;
	}

	public String getFromoperator() {
		return fromoperator;
	}

	public String getFromreqdatetime() {
		return fromreqdatetime;
	}

	public String getFromrescode() {
		return fromrescode;
	}

	public String getFromresdatetime() {
		return fromresdatetime;
	}

	public String getFromresdesc() {
		return fromresdesc;
	}

	public String getFromwallet() {
		return fromwallet;
	}

	public String getMpokey() {
		return mpokey;
	}

	public int getN1() {
		return n1;
	}

	public int getN2() {
		return n2;
	}

	public int getN3() {
		return n3;
	}

	public int getN4() {
		return n4;
	}

	public int getN5() {
		return n5;
	}

	public String getNarration() {
		return narration;
	}

	public String getRefKey() {
		return refKey;
	}

	public int getStatus() {
		return status;
	}

	public String getT1() {
		return t1;
	}

	public String getT2() {
		return t2;
	}

	public String getT3() {
		return t3;
	}

	public String getT4() {
		return t4;
	}

	public String getT5() {
		return t5;
	}

	public String getToaccount() {
		return toaccount;
	}

	public String getTocode() {
		return torescode;
	}

	public String getTodesc() {
		return toresdesc;
	}

	public String getToname() {
		return toname;
	}

	public String getTooperator() {
		return tooperator;
	}

	public String getToreqdatetime() {
		return toreqdatetime;
	}

	public String getTorescode() {
		return torescode;
	}

	public String getToresdatetime() {
		return toresdatetime;
	}

	public String getToresdesc() {
		return toresdesc;
	}

	public String getTowallet() {
		return towallet;
	}

	public String getTransID() {
		return transID;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public void setComm1(double comm1) {
		this.comm1 = comm1;
	}

	public void setComm2(double comm2) {
		this.comm2 = comm2;
	}

	public void setComm3(double comm3) {
		this.comm3 = comm3;
	}

	public void setFromaccount(String fromaccount) {
		this.fromaccount = fromaccount;
	}

	public void setFromcode(String fromcode) {
		this.fromrescode = fromcode;
	}

	public void setFromdesc(String fromdesc) {
		this.fromresdesc = fromdesc;
	}

	public void setFromname(String fromname) {
		this.fromname = fromname;
	}

	public void setFromoperator(String fromoperator) {
		this.fromoperator = fromoperator;
	}

	public void setFromreqdatetime(String fromreqdatetime) {
		this.fromreqdatetime = fromreqdatetime;
	}

	public void setFromrescode(String fromrescode) {
		this.fromrescode = fromrescode;
	}

	public void setFromresdatetime(String fromresdatetime) {
		this.fromresdatetime = fromresdatetime;
	}

	public void setFromresdesc(String fromresdesc) {
		this.fromresdesc = fromresdesc;
	}

	public void setFromwallet(String fromwallet) {
		this.fromwallet = fromwallet;
	}

	public void setMpokey(String mpokey) {
		this.mpokey = mpokey;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public void setN2(int n2) {
		this.n2 = n2;
	}

	public void setN3(int n3) {
		this.n3 = n3;
	}

	public void setN4(int n4) {
		this.n4 = n4;
	}

	public void setN5(int n5) {
		this.n5 = n5;
	}

	public void setNarration(String narration) {
		this.narration = narration;
	}

	public void setRefKey(String refkey) {
		this.refKey = refkey;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public void setT4(String t4) {
		this.t4 = t4;
	}

	public void setT5(String t5) {
		this.t5 = t5;
	}

	public void setToaccount(String toaccount) {
		this.toaccount = toaccount;
	}

	public void setTocode(String tocode) {
		this.torescode = tocode;
	}

	public void setTodesc(String todesc) {
		this.toresdesc = todesc;
	}

	public void setToname(String toname) {
		this.toname = toname;
	}

	public void setTooperator(String tooperator) {
		this.tooperator = tooperator;
	}

	public void setToreqdatetime(String toreqdatetime) {
		this.toreqdatetime = toreqdatetime;
	}

	public void setTorescode(String torescode) {
		this.torescode = torescode;
	}

	public void setToresdatetime(String toresdatetime) {
		this.toresdatetime = toresdatetime;
	}

	public void setToresdesc(String toresdesc) {
		this.toresdesc = toresdesc;
	}

	public void setTowallet(String towallet) {
		this.towallet = towallet;
	}

	public void setTransID(String transid) {
		this.transID = transid;
	}

}
