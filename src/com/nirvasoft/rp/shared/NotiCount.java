package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class NotiCount {
	private int noticount;
	
	public NotiCount() {
		clearProperty();
	}

	private void clearProperty() {
		this.noticount = 0;
		
	}

	public int getNoticount() {
		return noticount;
	}

	public void setNoticount(int noticount) {
		this.noticount = noticount;
	}
	
}
