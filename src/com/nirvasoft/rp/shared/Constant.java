package com.nirvasoft.rp.shared;

public class Constant {

	// Flexcube
	public static final String UsedChq = "U";
	public static final String NotUsedChq = "N";
	public static final String StoppedChq = "S";
	public static final String RejectChq = "R";
	public static final String CancelChq = "C";

	public static final String UsedChqDesc = "Used";
	public static final String NotUsedChqDesc = "Not Used";
	public static final String StoppedChqDesc = "Stopped";
	public static final String RejectChqDesc = "Reject";
	public static final String CancelChqDesc = "Cancel";
	public static final String envVar = "mBanking360DC";
	public static final String appName1 = "Operator A";
	public static final String appCode1 = "000700";
	public static final String appName2 = "Operator B";
	public static final String appCode2 = "000600";
}
