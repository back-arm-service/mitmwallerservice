package com.nirvasoft.rp.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ATMLocationDataResult {

	private String code;
	private String desc;
	private ATMLocation[] data = null;
	private String id;

	public ATMLocationDataResult() {
		this.clearProperty();
	}

	private void clearProperty() {
		code = "";
		desc = "";
		id = "";
	}

	public String getCode() {
		return code;
	}

	public ATMLocation[] getData() {
		return data;
	}

	public String getDesc() {
		return desc;
	}

	public String getId() {
		return id;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setData(ATMLocation[] data) {
		this.data = data;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "ATMLocationDataResult [code=" + code + ", desc=" + desc + ", data=" + Arrays.toString(getData())
				+ ", id=" + id + "]";
	}

}
