package com.nirvasoft.rp.shared;

public class ResultData {

	private String keyResult;
	private String msgCode;
	private String msgDesc;
	private boolean state;

	public ResultData() {
		clearProperty();
	}

	private void clearProperty() {
		this.keyResult = "";
		this.msgCode = "";
		this.msgDesc= "";
		this.state = false;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public String getKeyResult() {
		return keyResult;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public boolean isState() {
		return state;
	}

	public void setKeyResult(String keyResult) {
		this.keyResult = keyResult;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setState(boolean state) {
		this.state = state;
	}

}
