package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class WalletRequestData {
	private String userID;
	private String name;
	private String nrc;
	private String sessionID;
	private String sKey;
	private String institutionCode;
	private String field1;
	private String field2;
	private String region;
	private String phoneType;
	private String alreadyRegister;
	private String password;
	private String deviceId;
	private String appVersion;
	private String regionNumber;
	private String fcmToken;
//	private String t8;
//	public String getT8() {
//		return t8;
//	}
//
//	public void setT8(String t8) {
//		this.t8 = t8;
//	}

	public String getFcmToken() {
		return fcmToken;
	}

	public void setFcmToken(String fcmToken) {
		this.fcmToken = fcmToken;
	}

	private String iv;
	private String dm;
	private String salt;
	

	public WalletRequestData() {
		clearProperty();
	}

	private void clearProperty() {
		this.userID = "";
		this.name = "";
		this.nrc = "";
		this.field1 = "";
		this.field2 = "";
		this.sessionID = "";
		this.sKey = "";
		this.institutionCode = "";
		this.region = "";
		this.phoneType = "";
		this.alreadyRegister = "";
		this.password = "";
		this.deviceId = "";
		this.appVersion = "";
		this.regionNumber = "";
		this.iv = "";
		this.dm = "";
		this.salt = "";
	}

	public String getField1() {
		return field1;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public String getAlreadyRegister() {
		return alreadyRegister;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public String getDm() {
		return dm;
	}

	public String getField2() {
		return field2;
	}

	public String getInstitutionCode() {
		return institutionCode;
	}

	public String getIv() {
		return iv;
	}

	public String getName() {
		return name;
	}

	public String getNrc() {
		return nrc;
	}

	public String getPassword() {
		return password;
	}

	public String getPhoneType() {
		return phoneType;
	}

	public String getRegion() {
		return region;
	}

	public String getRegionNumber() {
		return regionNumber;
	}

	public String getSalt() {
		return salt;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getsKey() {
		return sKey;
	}

	public String getUserID() {
		return userID;
	}

	public void setAlreadyRegister(String alreadyRegister) {
		this.alreadyRegister = alreadyRegister;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public void setDm(String dm) {
		this.dm = dm;
	}


	public void setField2(String field2) {
		this.field2 = field2;
	}

	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}

	public void setIv(String iv) {
		this.iv = iv;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNrc(String nrc) {
		this.nrc = nrc;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public void setRegionNumber(String regionNumber) {
		this.regionNumber = regionNumber;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setsKey(String sKey) {
		this.sKey = sKey;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

}
