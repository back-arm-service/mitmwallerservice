package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class InstallationGetOTPReqData {
	private String phoneno;

	public InstallationGetOTPReqData() {
		clearProperty();
	}

	private void clearProperty() {
		this.phoneno = "";
	}

	public String getPhoneno() {
		return phoneno;
	}

	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}

	@Override
	public String toString() {
		return "{phoneno=" + phoneno + "}";
	}

}
