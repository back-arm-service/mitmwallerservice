package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class LOVSetupAllData {

	private String lovNo;
	private long sysKey;
	private String createdDate;
	private String modiDate;
	private String userID;
	private String recStatus;
	private LOVSetupDetailData[] data;
	private String lovDesc2;
	private String lovType;
	private String messageCode;
	private String messageDesc;
	private String sessionID;
	private String User1;

	void clearProperty() {

		lovNo = "";
		sysKey = 0;
		createdDate = "";
		modiDate = "";
		userID = "";
		recStatus = "";
		data = null;
		sessionID = "";
		lovDesc2 = "";
		lovType = "";
		messageCode = "";
		messageDesc = "";
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public LOVSetupDetailData[] getData() {
		return data;
	}

	public String getLovDesc2() {
		return lovDesc2;
	}

	public String getLovNo() {
		return lovNo;
	}

	public String getLovType() {
		return lovType;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public String getMessageDesc() {
		return messageDesc;
	}

	public String getModiDate() {
		return modiDate;
	}

	public String getRecStatus() {
		return recStatus;
	}

	public String getSessionID() {
		return sessionID;
	}

	public long getSysKey() {
		return sysKey;
	}

	public String getUser1() {
		return User1;
	}

	public String getUserID() {
		return userID;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public void setData(LOVSetupDetailData[] data) {
		this.data = data;
	}

	public void setLovDesc2(String lovDesc2) {
		this.lovDesc2 = lovDesc2;
	}

	public void setLovNo(String lovNo) {
		this.lovNo = lovNo;
	}

	public void setLovType(String lovType) {
		this.lovType = lovType;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public void setMessageDesc(String messageDesc) {
		this.messageDesc = messageDesc;
	}

	public void setModiDate(String modiDate) {
		this.modiDate = modiDate;
	}

	public void setRecStatus(String recStatus) {
		this.recStatus = recStatus;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setSysKey(long sysKey) {
		this.sysKey = sysKey;
	}

	public void setUser1(String user1) {
		User1 = user1;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

}
