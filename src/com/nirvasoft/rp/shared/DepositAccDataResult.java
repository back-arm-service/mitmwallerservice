package com.nirvasoft.rp.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DepositAccDataResult {

	private String code = "";
	private String desc = "";
	private DepositAccData[] dataList = null;

	public DepositAccDataResult() {
		clearProperty();
	}

	void clearProperty() {
		code = "";
		desc = "";
		dataList = null;
	}

	public String getCode() {
		return code;
	}

	public DepositAccData[] getDataList() {
		return dataList;
	}

	public String getDesc() {
		return desc;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDataList(DepositAccData[] datalist) {
		this.dataList = datalist;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public String toString() {
		return "{code=" + code + ", desc=" + desc + ", dataList=" + Arrays.toString(dataList) + "}";
	}

}
