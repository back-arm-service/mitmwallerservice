package com.nirvasoft.rp.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationDataSet {
	private String sessionID;
	private String userID;
	private String msgCode = "";
	private String msgDesc = "";
	private String searchText;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private NotiData[] data;

	public NotificationDataSet() {
		clearProperty();
	}

	private void clearProperty() {
		sessionID = "";
		userID = "";
		msgCode = "";
		msgDesc = "";
		searchText = "";
		totalCount = 0;
		currentPage = 0;
		pageSize = 0;
		data = null;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public int getPageSize() {
		return pageSize;
	}

	public String getSearchText() {
		return searchText;
	}

	public String getSessionID() {
		return sessionID;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public String getUserID() {
		return userID;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public NotiData[] getData() {
		return data;
	}

	public void setData(NotiData[] data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "NotificationDataSet [sessionID=" + sessionID + ", userID=" + userID + ", msgCode=" + msgCode
				+ ", msgDesc=" + msgDesc + ", searchText=" + searchText + ", totalCount=" + totalCount
				+ ", currentPage=" + currentPage + ", pageSize=" + pageSize + ", data=" + Arrays.toString(data) + "]";
	}

}