package com.nirvasoft.rp.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class NotiListResponse {

	private String code;
	private String desc;
	private String error;
	private NotiListData[] data;

	public NotiListResponse() {
		clearProperty();
	}

	public void clearProperty() {
		this.code = "";
		this.desc = "";
		this.error = "";
		this.data = null;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public NotiListData[] getData() {
		return data;
	}

	public void setData(NotiListData[] data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "NotiListResponse [code=" + code + ", desc=" + desc + ", error=" + error + ", data="
				+ Arrays.toString(data) + "]";
	}

}
