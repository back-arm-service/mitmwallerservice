package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FilterData {
	private String pItemid;
	private String pCaption;
	private String pFieldname;
	private String pDatatype;
	private String pCondition;
	private String pT1;
	private String pT2;

	public FilterData() {
		ClearProperty();
	}

	private void ClearProperty() {
		pItemid = "";
		pCaption = "";
		pFieldname = "";
		pDatatype = "";
		pCondition = "";
		pT1 = "";
		pT2 = "";
	}

	public String getCaption() {
		return pCaption;
	}

	public String getCondition() {
		return pCondition;
	}

	public String getDatatype() {
		return pDatatype;
	}

	public String getFieldname() {
		return pFieldname;
	}

	public String getItemid() {
		return pItemid;
	}

	public String getT1() {
		return pT1;
	}

	public String getT2() {
		return pT2;
	}

	public void setCaption(String p) {
		pCaption = p;
	}

	public void setCondition(String p) {
		pCondition = p;
	}

	public void setDatatype(String p) {
		pDatatype = p;
	}

	public void setFieldname(String p) {
		pFieldname = p;
	}

	public void setItemid(String p) {
		pItemid = p;
	}

	public void setT1(String p) {
		pT1 = p;
	}

	public void setT2(String p) {
		pT2 = p;
	}
}