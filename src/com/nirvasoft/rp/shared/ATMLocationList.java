package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ATMLocationList {

	private ATMLocation[] data;

	private int totalCount;

	private int currentPage;
	private int pageSize;
	private String searchText;
	private String msgCode = "";
	private String msgDesc = "";

	private String sessionID = "";

	public ATMLocationList() {
		data = null;
		totalCount = 0;
		currentPage = 0;
		pageSize = 0;
		searchText = "";
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public ATMLocation[] getData() {
		return data;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public int getPageSize() {
		return pageSize;
	}

	public String getSearchText() {
		return searchText;
	}

	public String getSessionID() {
		return sessionID;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setData(ATMLocation[] data) {
		this.data = data;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

}
