package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ChequeEnquiry {

	private String userID;
	private String sessionID;
	private String accountNo;
	private String chequeNo;
	private String sKey;

	public ChequeEnquiry() {
		clearProperty();
	}

	private void clearProperty() {
		userID = "";
		sessionID = "";
		accountNo = "";
		chequeNo = "";
		sKey = "";
	}

	public String getAccountNo() {
		return accountNo;
	}

	public String getChequeNo() {
		return chequeNo;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getsKey() {
		return sKey;
	}

	public String getUserID() {
		return userID;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setsKey(String sKey) {
		this.sKey = sKey;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	@Override
	public String toString() {
		return "{userID = " + userID + ", sessionID = " + sessionID + ", accountNo = " + accountNo + ", chequeNo = "
				+ chequeNo + ", sKey = " + sKey + "}";
	}

}
