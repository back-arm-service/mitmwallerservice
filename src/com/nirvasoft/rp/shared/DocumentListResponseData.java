package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

import com.nirvasoft.rp.data.DocumentResponseData;

@XmlRootElement
public class DocumentListResponseData {

	private String code;
	private String desc;
	private DocumentResponseData[] resList;

	public DocumentListResponseData() {
		this.clearProperty();
	}

	private void clearProperty() {
		this.code = "";
		this.desc = "";
		this.resList = null;
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public DocumentResponseData[] getResList() {
		return resList;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setResList(DocumentResponseData[] resList) {
		this.resList = resList;
	}

}
