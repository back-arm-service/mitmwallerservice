package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MobileRequestData {
	private String acctNo = "";
	private String sessionID = "";
	private String userID = "";

	public MobileRequestData() {
		clearProperty();
	}

	void clearProperty() {
		acctNo = "";
		sessionID = "";
		userID = "";

	}

	public String getAcctNo() {
		return acctNo;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getUserID() {
		return userID;
	}

	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	@Override
	public String toString() {
		return "{acctNo=" + acctNo + ", sessionID=" + sessionID + ", userID=" + userID + "}";
	}

}
