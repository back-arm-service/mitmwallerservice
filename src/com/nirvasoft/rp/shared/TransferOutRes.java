package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransferOutRes {
	private String code;
	private String desc;
	private String bankRefNo;
	private String transDate;
	private String field1;
	private String field2;
	private CardData[] cadData;

	public TransferOutRes() {
		clearProperty();
	}

	private void clearProperty() {
		code = "";
		desc = "";
		bankRefNo = "";
		transDate = "";
		field1 = "";
		field2 = "";
		cadData = null;
	}

	public CardData[] getCadData() {
		return cadData;
	}

	public void setCadData(CardData[] cadData) {
		this.cadData = cadData;
	}

	public String getBankRefNo() {
		return bankRefNo;
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public String getField1() {
		return field1;
	}

	public String getField2() {
		return field2;
	}

	public String getTransDate() {
		return transDate;
	}

	public void setBankRefNo(String bankRefNo) {
		this.bankRefNo = bankRefNo;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}

}
