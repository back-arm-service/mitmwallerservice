package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OTPRes {
	private String rKey;
	private String code;
	private String desc;
	private String sKey;
	private String otpCode;
	private String otpMessage;
	private String sessionID;
	public OTPRes() {
		clearProperties();
	}

	private void clearProperties() {
		this.rKey = "";
		this.code = "";
		this.desc = "";
		this.sKey = "";
		this.otpCode = "";
		this.otpMessage = "";
		this.sessionID = "";
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public String getOtpCode() {
		return otpCode;
	}

	public String getOtpMessage() {
		return otpMessage;
	}

	public String getrKey() {
		return rKey;
	}

	public String getsKey() {
		return sKey;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setOtpCode(String otpCode) {
		this.otpCode = otpCode;
	}

	public void setOtpMessage(String otpMessage) {
		this.otpMessage = otpMessage;
	}

	public void setrKey(String rKey) {
		this.rKey = rKey;
	}

	public void setsKey(String sKey) {
		this.sKey = sKey;
	}
	
	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	@Override
	public String toString() {
		return "{rKey=" + rKey + ", code=" + code + ", desc=" + desc + ", sKey=" + sKey + "}";
	}

}
