package com.nirvasoft.rp.shared;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

import com.nirvasoft.cms.shared.DocumentData;

@XmlRootElement
public class DocumentDataset implements Serializable {
	private int pPageNo;
	private int pPageSize;
	private int pTotalCount;

	private String pUserID;
	private String pUserName;

	private ArrayList<DocumentData> pDataList;

	public DocumentDataset() {
		ClearProperty();
	}

	private void ClearProperty() {
		pPageNo = 0;
		pPageSize = 0;
		pTotalCount = 0;

		pUserID = "";
		pUserName = "";
	}

	public ArrayList<DocumentData> getDataList() {
		return pDataList;
	}

	public int getPageNo() {
		return pPageNo;
	}

	public int getPageSize() {
		return pPageSize;
	}

	public int getTotalCount() {
		return pTotalCount;
	}

	public String getUserID() {
		return pUserID;
	}

	public String getUserName() {
		return pUserName;
	}

	public void setDataList(ArrayList<DocumentData> p) {
		this.pDataList = p;
	}

	public void setPageNo(int p) {
		pPageNo = p;
	}

	public void setPageSize(int p) {
		pPageSize = p;
	}

	public void setTotalCount(int p) {
		pTotalCount = p;
	}

	public void setUserID(String p) {
		pUserID = p;
	}

	public void setUserName(String p) {
		pUserName = p;
	}
}