package com.nirvasoft.rp.shared;

public class ImageData {

	private String syskey;
	private String createdDate;
	private String userid;
	private String imageName;
	private String latitude;
	private String longitude;
	private String location;
	private String imagePath;
	private String t1;
	private String t2;
	private String t3;
	private String t4;
	private String t5;
	private String n1;
	private String n2;
	private String n3;
	private String n4;
	private String n5;

	public ImageData() {
		clearProperty();
	}

	private void clearProperty() {
		this.syskey = "";
		this.createdDate = "";
		this.userid = "";
		this.imageName = "";
		this.latitude = "";
		this.longitude = "";
		this.location = "";
		this.imagePath = "";
		this.t1 = "";
		this.t2 = "";
		this.t3 = "";
		this.t4 = "";
		this.t5 = "";
		this.n1 = "";
		this.n2 = "";
		this.n3 = "";
		this.n4 = "";
		this.n5 = "";
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public String getImageName() {
		return imageName;
	}

	public String getImagePath() {
		return imagePath;
	}

	public String getLatitude() {
		return latitude;
	}

	public String getLocation() {
		return location;
	}

	public String getLongitude() {
		return longitude;
	}

	public String getN1() {
		return n1;
	}

	public String getN2() {
		return n2;
	}

	public String getN3() {
		return n3;
	}

	public String getN4() {
		return n4;
	}

	public String getN5() {
		return n5;
	}

	public String getSyskey() {
		return syskey;
	}

	public String getT1() {
		return t1;
	}

	public String getT2() {
		return t2;
	}

	public String getT3() {
		return t3;
	}

	public String getT4() {
		return t4;
	}

	public String getT5() {
		return t5;
	}

	public String getUserid() {
		return userid;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public void setN1(String n1) {
		this.n1 = n1;
	}

	public void setN2(String n2) {
		this.n2 = n2;
	}

	public void setN3(String n3) {
		this.n3 = n3;
	}

	public void setN4(String n4) {
		this.n4 = n4;
	}

	public void setN5(String n5) {
		this.n5 = n5;
	}

	public void setSyskey(String syskey) {
		this.syskey = syskey;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public void setT4(String t4) {
		this.t4 = t4;
	}

	public void setT5(String t5) {
		this.t5 = t5;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

}
