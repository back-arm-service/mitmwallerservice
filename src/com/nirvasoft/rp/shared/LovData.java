package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LovData {
	private String value;
	private String name;
	private ReferenceListData[] dataList = null;

	public LovData() {
		clearProperty();
	}

	private void clearProperty() {
		dataList = null;
		value = "";
		name = "";
	}

	public ReferenceListData[] getDataList() {
		return dataList;
	}

	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}

	public void setDataList(ReferenceListData[] dataList) {
		this.dataList = dataList;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
