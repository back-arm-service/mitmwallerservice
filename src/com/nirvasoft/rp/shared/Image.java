package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Image {
	private String image;

	public Image() {
		clearProperty();
	}

	private void clearProperty() {
		image = "";
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

}
