package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class mobileuserobj {
	private String userID;
	private String sessionID;
	private long syskey;
	private String userstatus;

	public mobileuserobj() {
		clearProperty();
	}

	void clearProperty() {
		userID = "";
		sessionID = "";
	}

	public String getSessionID() {
		return sessionID;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getUserID() {
		return userID;
	}

	public String getUserstatus() {
		return userstatus;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setUserID(String userid) {
		this.userID = userid;
	}

	public void setUserstatus(String userstatus) {
		this.userstatus = userstatus;
	}

	@Override
	public String toString() {
		return "{userID=" + userID + ", sessionID=" + sessionID + "}";
	}

}
