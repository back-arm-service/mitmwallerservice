package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MerchantListData {
	private String MerchantID;
	private String MerchantName;
	private String ProcessingCode;
	private String ColorCode;
	private String Logo;
	private String accountNo;

	public MerchantListData() {
		clearProperty();
	}

	void clearProperty() {
		ProcessingCode = "";
		MerchantID = "";
		MerchantName = "";
		ColorCode = "";
		Logo = "";
		accountNo = "";
	}

	public String getAccountNo() {
		return accountNo;
	}

	public String getColorCode() {
		return ColorCode;
	}

	public String getLogo() {
		return Logo;
	}

	public String getMerchantID() {
		return MerchantID;
	}

	public String getMerchantName() {
		return MerchantName;
	}

	public String getProcessingCode() {
		return ProcessingCode;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public void setColorCode(String colorCode) {
		ColorCode = colorCode;
	}

	public void setLogo(String logo) {
		Logo = logo;
	}

	public void setMerchantID(String merchantID) {
		MerchantID = merchantID;
	}

	public void setMerchantName(String merchantName) {
		MerchantName = merchantName;
	}

	public void setProcessingCode(String processingCode) {
		ProcessingCode = processingCode;
	}

	@Override
	public String toString() {
		return "{MerchantID=" + MerchantID + ", MerchantName=" + MerchantName + ", ProcessingCode=" + ProcessingCode
				+ ", ColorCode=" + ColorCode + ", Logo=" + Logo + "}";
	}

}
