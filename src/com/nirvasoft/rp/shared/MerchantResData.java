package com.nirvasoft.rp.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MerchantResData {

	private String code;
	private String desc;
	private MerchantListData[] merchantList = null;

	public MerchantResData() {
		clearProperty();
	}

	private void clearProperty() {
		code = "";
		desc = "";
		merchantList = null;
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public MerchantListData[] getMerchantList() {
		return merchantList;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setMerchantList(MerchantListData[] merchantlist) {
		this.merchantList = merchantlist;
	}

	@Override
	public String toString() {
		return "{code=" + code + ", desc=" + desc + ", merchantList=" + Arrays.toString(merchantList) + "}";
	}

}
