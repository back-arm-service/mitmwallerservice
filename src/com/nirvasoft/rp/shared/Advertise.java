package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Advertise {
	private String link;
	private String logo;

	public Advertise() {
		clearProperty();
	}

	private void clearProperty() {
		link = "";
		logo = "";
	}

	public String getLink() {
		return link;
	}

	public String getLogo() {
		return logo;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

}
