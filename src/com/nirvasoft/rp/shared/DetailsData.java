package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DetailsData {
	private long sysKey;
	private long hkey;
	private int srno;
	private String code;
	private String description;
	private double price;
	private double amount;
	private int quantity;

	public DetailsData() {
		clearProperty();
	}

	private void clearProperty() {

	}

	public double getAmount() {
		return amount;
	}

	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public long getHkey() {
		return hkey;
	}

	public double getPrice() {
		return price;
	}

	public int getQuantity() {
		return quantity;
	}

	public int getSrno() {
		return srno;
	}

	public long getSysKey() {
		return sysKey;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setHkey(long hkey) {
		this.hkey = hkey;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public void setSrno(int srno) {
		this.srno = srno;
	}

	public void setSysKey(long sysKey) {
		this.sysKey = sysKey;
	}

}
