package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DepositAccData {

	private String depositAcc;
	private String ccy;
	private String avlBal;
	private String accType;

	public String getAccType() {
		return accType;
	}

	public String getAvlBal() {
		return avlBal;
	}

	public String getCcy() {
		return ccy;
	}

	public String getDepositAcc() {
		return depositAcc;
	}

	public void setAccType(String acctype) {
		this.accType = acctype;
	}

	public void setAvlBal(String avlbal) {
		this.avlBal = avlbal;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public void setDepositAcc(String depositacc) {
		this.depositAcc = depositacc;
	}

	@Override
	public String toString() {
		return "{depositAcc=" + depositAcc + ", ccy=" + ccy + ", avlBal=" + avlBal + ", accType=" + accType + "}";
	}

}
