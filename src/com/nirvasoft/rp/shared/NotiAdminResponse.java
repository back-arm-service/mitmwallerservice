package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotiAdminResponse {

	private String code = "";
	private String desc = "";
	private String exception = "";
	private String autokey = "";

	public NotiAdminResponse() {
		clearProperties();
	}

	private void clearProperties() {
		this.code = "";
		this.desc = "";
		this.exception = "";
		this.autokey = "";
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public String getAutokey() {
		return autokey;
	}

	public void setAutokey(String autokey) {
		this.autokey = autokey;
	}

	@Override
	public String toString() {
		return "NotiAdminResponse [code=" + code + ", desc=" + desc + ", exception=" + exception + ", autokey="
				+ autokey + "]";
	}

}
