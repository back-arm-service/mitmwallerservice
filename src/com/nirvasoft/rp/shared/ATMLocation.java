package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ATMLocation {
	private String userid = "";
	private String username = "";
	private String msgCode = "";
	private String msgDesc = "";
	private String sessionID = "";
	private long autokey;
	private String createdDate;
	private String modifiedDate;
	private String latitude;
	private String longitude;
	private String address;
	private String phone1;
	private String phone2;
	private String branchCode;
	private String name;
	private String locationType;
	private String services;
	private String workingHour;
	private String t1;
	private String t2;
	private String t3;
	private String t4;
	private String t5;
	private String t6;
	private String t7;

	public ATMLocation() {
		this.clearProperty();
	}

	private void clearProperty() {
		autokey = 0;
		createdDate = "";
		modifiedDate = "";
		latitude = "";
		longitude = "";
		address = "";
		phone1 = "";
		phone2 = "";
		branchCode = "";
		name = "";
		locationType = "";
		services = "";
		workingHour = "";
		t1 = "";
		t2 = "";
		t3 = "";
		t4 = "";
		t5 = "";
		t6 = "";
		t7 = "";
	}

	public String getAddress() {
		return address;
	}

	public long getAutokey() {
		return autokey;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public String getLatitude() {
		return latitude;
	}

	public String getLocationType() {
		return locationType;
	}

	public String getLongitude() {
		return longitude;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public String getName() {
		return name;
	}

	public String getPhone1() {
		return phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public String getServices() {
		return services;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getT1() {
		return t1;
	}

	public String getT2() {
		return t2;
	}

	public String getT3() {
		return t3;
	}

	public String getT4() {
		return t4;
	}

	public String getT5() {
		return t5;
	}

	public String getT6() {
		return t6;
	}

	public String getT7() {
		return t7;
	}

	public String getUserid() {
		return userid;
	}

	public String getUsername() {
		return username;
	}

	public String getWorkingHour() {
		return workingHour;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setAutokey(long autokey) {
		this.autokey = autokey;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public void setServices(String services) {
		this.services = services;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public void setT4(String t4) {
		this.t4 = t4;
	}

	public void setT5(String t5) {
		this.t5 = t5;
	}

	public void setT6(String t6) {
		this.t6 = t6;
	}

	public void setT7(String t7) {
		this.t7 = t7;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setWorkingHour(String workingHour) {
		this.workingHour = workingHour;
	}

}
