package com.nirvasoft.rp.shared;

public class NotiSession {
	private long autoKey;
	private String createdDate;
	private String userID;
	private String notiMessage;
	private String notiType;
	private long notiKey;
	private String operator;
	private int count;
	private int status;
	private int size;
	private int batchNo;
	private String t1;
	private String t2;
	private String t3;
	private int n1;
	private int n2;
	private int n3;

	public NotiSession() {
		clearProperty();
	}

	private void clearProperty() {
		autoKey = 0;
		createdDate = "";
		userID = "";
		operator = "";
		count = 0;
		status = 0;
		size = 0;
		notiMessage = "";
		notiType = "";
		notiKey = 0;
		batchNo = 0;
		t1 = "";
		t2 = "";
		t3 = "";
		n1 = 0;
		n2 = 0;
		n3 = 0;
	}

	public long getAutoKey() {
		return autoKey;
	}

	public int getBatchNo() {
		return batchNo;
	}

	public int getCount() {
		return count;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public int getN1() {
		return n1;
	}

	public int getN2() {
		return n2;
	}

	public int getN3() {
		return n3;
	}

	public long getNotiKey() {
		return notiKey;
	}

	public String getNotiMessage() {
		return notiMessage;
	}

	public String getNotiType() {
		return notiType;
	}

	public String getOperator() {
		return operator;
	}

	public int getSize() {
		return size;
	}

	public int getStatus() {
		return status;
	}

	public String getT1() {
		return t1;
	}

	public String getT2() {
		return t2;
	}

	public String getT3() {
		return t3;
	}

	public String getUserID() {
		return userID;
	}

	public void setAutoKey(long autoKey) {
		this.autoKey = autoKey;
	}

	public void setBatchNo(int batchNo) {
		this.batchNo = batchNo;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public void setN2(int n2) {
		this.n2 = n2;
	}

	public void setN3(int n3) {
		this.n3 = n3;
	}

	public void setNotiKey(long notiKey) {
		this.notiKey = notiKey;
	}

	public void setNotiMessage(String notiMessage) {
		this.notiMessage = notiMessage;
	}

	public void setNotiType(String notiType) {
		this.notiType = notiType;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

}
