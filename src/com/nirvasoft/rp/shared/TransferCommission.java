package com.nirvasoft.rp.shared;

public class TransferCommission {
	private String chgCode;
	private double chgRate;
	private double chgAmount;
	private double minChgAmount;

	public TransferCommission() {
		clearProperty();
	}

	private void clearProperty() {
		chgCode = "";
		chgRate = 0.00;
		chgAmount = 0.00;
		minChgAmount = 0.00;
	}

	public double getChgAmount() {
		return chgAmount;
	}

	public String getChgCode() {
		return chgCode;
	}

	public double getChgRate() {
		return chgRate;
	}

	public double getMinChgAmount() {
		return minChgAmount;
	}

	public void setChgAmount(double chgAmount) {
		this.chgAmount = chgAmount;
	}

	public void setChgCode(String chgCode) {
		this.chgCode = chgCode;
	}

	public void setChgRate(double chgRate) {
		this.chgRate = chgRate;
	}

	public void setMinChgAmount(double minChgAmount) {
		this.minChgAmount = minChgAmount;
	}

}
