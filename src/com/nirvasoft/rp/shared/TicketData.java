package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TicketData {

	private String messageCode;
	private String messageDesc;
	private String syskey;
	private String autokey;
	private String createddate;
	private String modifieddate;
	private String userid;
	private String date;
	private String time;
	private String shootdate;
	private String shoottime;
	private String T1;
	private String T2;
	private String T3;
	private String T4;
	private String T5;
	private String T6;
	private String T7;
	private String T8;
	private String T9;
	private String T10;
	private String T11;
	private String T12;
	private String T13;
	private String T14;
	private String T15;
	private String T16;
	private String T17;
	private String T18;
	private String T19;
	private String T20;
	private String n1;
	private String n2;
	private String n3;
	private String n4;
	private String n5;
	private String n6;
	private String n7;
	private String n8;
	private String n9;
	private String n10;

	public TicketData() {
		clearProperty();
	}

	private void clearProperty() {
		this.messageCode = "";
		this.messageDesc = "";
		this.syskey = "";
		this.autokey = "";
		this.createddate = "";
		this.modifieddate = "";
		this.userid = "";
		this.date = "";
		this.time = "";
		this.shootdate = "";
		this.shoottime = "";
		this.T1 = "";
		this.T2 = "";
		this.T3 = "";
		this.T4 = "";
		this.T5 = "";
		this.T6 = "";
		this.T7 = "";
		this.T8 = "";
		this.T9 = "";
		this.T10 = "";
		this.T11 = "";
		this.T12 = "";
		this.T13 = "";
		this.T14 = "";
		this.T15 = "";
		this.T16 = "";
		this.T17 = "";
		this.T18 = "";
		this.T19 = "";
		this.T20 = "";
		this.n1 = "0";
		this.n2 = "0";
		this.n3 = "0";
		this.n4 = "0";
		this.n5 = "0";
		this.n6 = "0";
		this.n7 = "0";
		this.n8 = "0";
		this.n9 = "0";
		this.n10 = "0";
	}

	public String getAutokey() {
		return autokey;
	}

	public String getCreateddate() {
		return createddate;
	}

	public String getDate() {
		return date;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public String getMessageDesc() {
		return messageDesc;
	}

	public String getModifieddate() {
		return modifieddate;
	}

	public String getN1() {
		return n1;
	}

	public String getN10() {
		return n10;
	}

	public String getN2() {
		return n2;
	}

	public String getN3() {
		return n3;
	}

	public String getN4() {
		return n4;
	}

	public String getN5() {
		return n5;
	}

	public String getN6() {
		return n6;
	}

	public String getN7() {
		return n7;
	}

	public String getN8() {
		return n8;
	}

	public String getN9() {
		return n9;
	}

	public String getShootdate() {
		return shootdate;
	}

	public String getShoottime() {
		return shoottime;
	}

	public String getSyskey() {
		return syskey;
	}

	public String getT1() {
		return T1;
	}

	public String getT10() {
		return T10;
	}

	public String getT11() {
		return T11;
	}

	public String getT12() {
		return T12;
	}

	public String getT13() {
		return T13;
	}

	public String getT14() {
		return T14;
	}

	public String getT15() {
		return T15;
	}

	public String getT16() {
		return T16;
	}

	public String getT17() {
		return T17;
	}

	public String getT18() {
		return T18;
	}

	public String getT19() {
		return T19;
	}

	public String getT2() {
		return T2;
	}

	public String getT20() {
		return T20;
	}

	public String getT3() {
		return T3;
	}

	public String getT4() {
		return T4;
	}

	public String getT5() {
		return T5;
	}

	public String getT6() {
		return T6;
	}

	public String getT7() {
		return T7;
	}

	public String getT8() {
		return T8;
	}

	public String getT9() {
		return T9;
	}

	public String getTime() {
		return time;
	}

	public String getUserid() {
		return userid;
	}

	public void setAutokey(String autokey) {
		this.autokey = autokey;
	}

	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public void setMessageDesc(String messageDesc) {
		this.messageDesc = messageDesc;
	}

	public void setModifieddate(String modifieddate) {
		this.modifieddate = modifieddate;
	}

	public void setN1(String n1) {
		this.n1 = n1;
	}

	public void setN10(String n10) {
		this.n10 = n10;
	}

	public void setN2(String n2) {
		this.n2 = n2;
	}

	public void setN3(String n3) {
		this.n3 = n3;
	}

	public void setN4(String n4) {
		this.n4 = n4;
	}

	public void setN5(String n5) {
		this.n5 = n5;
	}

	public void setN6(String n6) {
		this.n6 = n6;
	}

	public void setN7(String n7) {
		this.n7 = n7;
	}

	public void setN8(String n8) {
		this.n8 = n8;
	}

	public void setN9(String n9) {
		this.n9 = n9;
	}

	public void setShootdate(String shootdate) {
		this.shootdate = shootdate;
	}

	public void setShoottime(String shoottime) {
		this.shoottime = shoottime;
	}

	public void setSyskey(String syskey) {
		this.syskey = syskey;
	}

	public void setT1(String t1) {
		T1 = t1;
	}

	public void setT10(String t10) {
		T10 = t10;
	}

	public void setT11(String t11) {
		T11 = t11;
	}

	public void setT12(String t12) {
		T12 = t12;
	}

	public void setT13(String t13) {
		T13 = t13;
	}

	public void setT14(String t14) {
		T14 = t14;
	}

	public void setT15(String t15) {
		T15 = t15;
	}

	public void setT16(String t16) {
		T16 = t16;
	}

	public void setT17(String t17) {
		T17 = t17;
	}

	public void setT18(String t18) {
		T18 = t18;
	}

	public void setT19(String t19) {
		T19 = t19;
	}

	public void setT2(String t2) {
		T2 = t2;
	}

	public void setT20(String t20) {
		T20 = t20;
	}

	public void setT3(String t3) {
		T3 = t3;
	}

	public void setT4(String t4) {
		T4 = t4;
	}

	public void setT5(String t5) {
		T5 = t5;
	}

	public void setT6(String t6) {
		T6 = t6;
	}

	public void setT7(String t7) {
		T7 = t7;
	}

	public void setT8(String t8) {
		T8 = t8;
	}

	public void setT9(String t9) {
		T9 = t9;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

}
