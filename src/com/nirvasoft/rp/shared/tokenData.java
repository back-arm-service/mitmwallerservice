package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class tokenData {
	public String userid;
	public String password;
	public String expiredDate;
	public String token;
	public tokenData() {
		clearProperty();
	}

	private void clearProperty() {
		this.userid = "";
		this.password = "";
		this.expiredDate ="";
		this.token = "";
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(String expiredDate) {
		this.expiredDate = expiredDate;
	}

	@Override
	public String toString() {
		return "tokenData [userid=" + userid + ", password=" + password + ", expiredDate=" + expiredDate + "]";
	}

}
