package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class NewAccountResponseData {

	private String code;
	private String desc;

	public NewAccountResponseData() {
		clearProperty();
	}

	private void clearProperty() {
		code = "";
		desc = "";
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public String toString() {
		return "NewAccountResponseData [code=" + code + ", desc=" + desc + "]";
	}

}
