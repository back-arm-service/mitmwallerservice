package com.nirvasoft.rp.shared;

import java.io.Serializable;

public class CardAccountMappingData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3282810770539607818L;

	private String pCardAccountNumber;
	private String pCustomerName;
	private String pStatusDesc;
	private String pAccountType;
	private String pCurrency;
	private String pAccountNumber;
	private String pSetDefaultAccount;
	private int pStatus;

	public CardAccountMappingData() {
		clearProperty();
	}

	private void clearProperty() {
		pCardAccountNumber = "";
		pCustomerName = "";
		pStatusDesc = "";
		pAccountType = "";
		pCurrency = "";
		pAccountNumber = "";
		pSetDefaultAccount = "";
		pStatus = 0;
	}

	public String getAccountNumber() {
		return pAccountNumber;
	}

	public String getAccountType() {
		return pAccountType;
	}

	public String getCardAccountNumber() {
		return pCardAccountNumber;
	}

	public String getCurrency() {
		return pCurrency;
	}

	public String getCustomerName() {
		return pCustomerName;
	}

	public String getSetDefaultAccount() {
		return pSetDefaultAccount;
	}

	public int getStatus() {
		return pStatus;
	}

	public String getStatusDesc() {
		return pStatusDesc;
	}

	public void setAccountNumber(String pAccountNumber) {
		this.pAccountNumber = pAccountNumber;
	}

	public void setAccountType(String pAccountType) {
		this.pAccountType = pAccountType;
	}

	public void setCardAccountNumber(String pCardAccountNumber) {
		this.pCardAccountNumber = pCardAccountNumber;
	}

	public void setCurrency(String pCurrency) {
		this.pCurrency = pCurrency;
	}

	public void setCustomerName(String pCustomerName) {
		this.pCustomerName = pCustomerName;
	}

	public void setSetDefaultAccount(String pSetDefaultAccout) {
		this.pSetDefaultAccount = pSetDefaultAccout;
	}

	public void setStatus(int pStatus) {
		this.pStatus = pStatus;
	}

	public void setStatusDesc(String pStatus) {
		this.pStatusDesc = pStatus;
	}

}
