package com.nirvasoft.rp.shared;

import java.io.Serializable;

import com.nirvasoft.rp.shared.icbs.Enum;

public class ReferenceAccountsCriteriaData implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int FunctionID = 9999;
	public String Description = "";
	public String GLCode = "";
	public String GLAccNumber = "";

	public Enum.NumFilterOperator FunctionIDOperator = Enum.NumFilterOperator.Equal;
	public Enum.TxTFilterOperator DescriptionOperator = Enum.TxTFilterOperator.BeginWith;
	public Enum.TxTFilterOperator GLCodeOperator = Enum.TxTFilterOperator.BeginWith;
	public Enum.TxTFilterOperator GLAccNumberOperator = Enum.TxTFilterOperator.BeginWith;

}
