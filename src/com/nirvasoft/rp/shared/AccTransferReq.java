
package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;
 
@XmlRootElement
public class AccTransferReq {
	
	private String hashValue;
	private String token;
	private String senderCode;
	private String receiverCode;
	private String merchantCode;	
	private String fromName;
	private String toName;
	private String amount;
	private String prevBalance;
	private String narrative;//ndh --for reference
	//private String userID;
	//private String sessionID;
	private String fromAccount;
	private String toAccount;
	private String agentAccount;
	//private String merchantID;
	//private String amount;
	private String bankCharges;
	//private String narrative;
	private String refNo;
	private double comm;
	private String iv;
	private String dm;
	private String salt;
	private String password;
	private String appType;
	private String currentAmount;
	private String voucherType;
	private String packageName;
	private String subscriptionno;
	private String packagetype;
	private String cardNo;
	private String merchantID;
	private String moviecode;
	private String moviename;
	private String startdate;
	private String enddate;
	private String usage_service_catalog_identifier__id;
	//private String  fromInstitutionCode;
	//private String toInstitutionCode;
	//private String trprevBalance;
	private String taxDesc;
	private String belateday;
	private String billId;
	private String cusName;
	private String deptName;
	private String dueDate;
	private String penalty;
	private String vendorCode;	
	private String apkType;
	private String loanSyskey;

	public String getLoanSyskey() {
		return loanSyskey;
	}

	public void setLoanSyskey(String loanSyskey) {
		this.loanSyskey = loanSyskey;
	}

	public String getTaxDesc() {
		return taxDesc;
	}

	public void setTaxDesc(String taxDesc) {
		this.taxDesc = taxDesc;
	}

	public String getBelateday() {
		return belateday;
	}

	public void setBelateday(String belateday) {
		this.belateday = belateday;
	}

	public String getBillId() {
		return billId;
	}

	public void setBillId(String billId) {
		this.billId = billId;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getPenalty() {
		return penalty;
	}

	public void setPenalty(String penalty) {
		this.penalty = penalty;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getAppType() {
		return appType;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getBankCharges() {
		return bankCharges;
	}

	public void setBankCharges(String bankCharges) {
		this.bankCharges = bankCharges;
	}

	public String getCurrentAmount() {
		return currentAmount;
	}

	public void setCurrentAmount(String currentAmount) {
		this.currentAmount = currentAmount;
	}

	public String getVoucherType() {
		return voucherType;
	}

	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getSubscriptionno() {
		return subscriptionno;
	}

	public void setSubscriptionno(String subscriptionno) {
		this.subscriptionno = subscriptionno;
	}

	public String getPackagetype() {
		return packagetype;
	}

	public void setPackagetype(String packagetype) {
		this.packagetype = packagetype;
	}

	public void setAppType(String appType) {
		this.appType = appType;
	}

	public AccTransferReq() {
		clearProperty();
	}

	void clearProperty() {
		hashValue="";
		token ="";
		senderCode="";
		receiverCode="";
		merchantCode="";
		fromName="";
		toName="";
		amount="0.00";
		prevBalance="0.00";
		fromAccount="";
		toAccount="";
		agentAccount="";
		comm = 0.00;
		refNo="";
		currentAmount="";
		voucherType="";
		packageName="";
		subscriptionno="";
		packagetype="";
		merchantID="";
		moviecode = "";
		startdate = "";
		enddate = "";
		usage_service_catalog_identifier__id = "";
		password = "";
		appType = "";
		dm = "";
		iv = "";
		moviename = "";
		narrative = "";
		bankCharges = "";
		apkType = "";
		penalty = "";
	}
	
	public String getApkType() {
		return apkType;
	}

	public void setApkType(String apkType) {
		this.apkType = apkType;
	}

	public String getMoviecode() {
		return moviecode;
	}

	public void setMoviecode(String moviecode) {
		this.moviecode = moviecode;
	}

	public String getMoviename() {
		return moviename;
	}

	public void setMoviename(String moviename) {
		this.moviename = moviename;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getEnddate() {
		return enddate;
	}

	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}

	public String getUsage_service_catalog_identifier__id() {
		return usage_service_catalog_identifier__id;
	}

	public void setUsage_service_catalog_identifier__id(String usage_service_catalog_identifier__id) {
		this.usage_service_catalog_identifier__id = usage_service_catalog_identifier__id;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public String getIv() {
		return iv;
	}

	public void setIv(String iv) {
		this.iv = iv;
	}

	public String getDm() {
		return dm;
	}

	public void setDm(String dm) {
		this.dm = dm;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNarrative() {
		return narrative;
	}

	public void setNarrative(String narrative) {
		this.narrative = narrative;
	}
	
//OLD
	/*public double getComm() {
		return comm;
	}

	public void setComm(double comm) {
		this.comm = comm;
	}

	public String getWalletID() {
		return walletID;
	}

	public void setWalletID(String walletID) {
		this.walletID = walletID;
	}

	public String getReceiveID() {
		return receiveID;
	}

	public void setReceiveID(String receiveID) {
		this.receiveID = receiveID;
	}

	public String getPayeeID() {
		return payeeID;
	}

	public void setPayeeID(String payeeID) {
		this.payeeID = payeeID;
	}

	public String getTrprevBalance() {
		return trprevBalance;
	}

	public void setTrprevBalance(String trprevBalance) {
		this.trprevBalance = trprevBalance;
	}

	public String getFromInstitutionCode() {
		return fromInstitutionCode;
	}

	public void setFromInstitutionCode(String fromInstitutionCode) {
		this.fromInstitutionCode = fromInstitutionCode;
	}

	public String getToInstitutionCode() {
		return toInstitutionCode;
	}

	public void setToInstitutionCode(String toInstitutionCode) {
		this.toInstitutionCode = toInstitutionCode;
	}*/

	public String getHashValue() {
		return hashValue;
	}

	public void setHashValue(String hashValue) {
		this.hashValue = hashValue;
	}
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getSenderCode() {
		return senderCode;
	}

	public void setSenderCode(String senderCode) {
		this.senderCode = senderCode;
	}

	public String getReceiverCode() {
		return receiverCode;
	}

	public void setReceiverCode(String receiverCode) {
		this.receiverCode = receiverCode;
	}


	public String getMerchantCode() {
		return merchantCode;
	}

	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	public String getFromName() {
		return fromName;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	public String getToName() {
		return toName;
	}

	public void setToName(String toName) {
		this.toName = toName;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getPrevBalance() {
		return prevBalance;
	}

	public void setPrevBalance(String prevBalance) {
		this.prevBalance = prevBalance;
	}

	public String getFromAccount() {
		return fromAccount;
	}

	public void setFromAccount(String fromAccount) {
		this.fromAccount = fromAccount;
	}

	public String getToAccount() {
		return toAccount;
	}

	public void setToAccount(String toAccount) {
		this.toAccount = toAccount;
	}

	public double getComm() {
		return comm;
	}

	public void setComm(double comm) {
		this.comm = comm;
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public String getAgentAccount() {
		return agentAccount;
	}

	public void setAgentAccount(String agentAccount) {
		this.agentAccount = agentAccount;
	}

	@Override
	public String toString() {
		return "AccTransferReq [token=" + token + ", senderCode=" + senderCode + ", receiverCode=" + receiverCode
				+ ", merchantCode=" + merchantCode + ", fromName=" + fromName + ", toName=" + toName + ", amount="
				+ amount + ", prevBalance=" + prevBalance + ", fromAccount=" + fromAccount + ", toAccount=" + toAccount
				+ ", comm=" + comm + ", refNo=" + refNo + "]";
	}

}
