package com.nirvasoft.rp.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CommonDataSet {

	private CommonData[] data1;
	private String[] replyData;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private boolean state;
	private String Role;
	private String Msg;

	public CommonDataSet() {
		data1 = new CommonData[0];
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public CommonData[] getData1() {
		return data1;
	}

	public String getMsg() {
		return Msg;
	}

	public int getPageSize() {
		return pageSize;
	}

	public String[] getReplyData() {
		return replyData;
	}

	public String getRole() {
		return Role;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public boolean isState() {
		return state;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setData1(CommonData[] data1) {
		this.data1 = data1;
	}

	public void setMsg(String msg) {
		Msg = msg;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setReplyData(String[] replyData) {
		this.replyData = replyData;
	}

	public void setRole(String role) {
		Role = role;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	@Override
	public String toString() {
		return "CommonDataSet [data1=" + Arrays.toString(data1) + ", replyData=" + Arrays.toString(replyData)
				+ ", totalCount=" + totalCount + ", currentPage=" + currentPage + ", pageSize=" + pageSize + ", state="
				+ state + ", Role=" + Role + ", Msg=" + Msg + "]";
	}

}
