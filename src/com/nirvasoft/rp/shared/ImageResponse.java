package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ImageResponse {

	private String code;
	private String desc;
	private ImageData[] imageList;

	public ImageResponse() {
		this.clearProperty();
	}

	private void clearProperty() {
		this.code = "";
		this.desc = "";
		this.imageList = null;
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public ImageData[] getImageList() {
		return imageList;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setImageList(ImageData[] imageList) {
		this.imageList = imageList;
	}

}
