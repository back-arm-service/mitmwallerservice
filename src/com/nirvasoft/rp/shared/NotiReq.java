package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class NotiReq {
	private String userID;
	private String sessionID;
	private String operator;
	private String notiType;
	private String currentPage;
	private String pageSize;
	private String sKey;

	private NotiReq() {
		clearProperty();
	}

	private void clearProperty() {
		userID = "";
		sessionID = "";
		operator = "";
		notiType = "";
		currentPage = "";
		pageSize = "";
		sKey = "";
	}

	public String getCurrentPage() {
		return currentPage;
	}

	public String getNotiType() {
		return notiType;
	}

	public String getOperator() {
		return operator;
	}

	public String getPageSize() {
		return pageSize;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getsKey() {
		return sKey;
	}

	public String getUserID() {
		return userID;
	}

	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}

	public void setNotiType(String notiType) {
		this.notiType = notiType;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setsKey(String sKey) {
		this.sKey = sKey;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

}
