package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MerchantData {
	private String aMerchantID;
	private String aFromDate;
	private String aToDate;
	private String debitaccount;
	private String collectionaccount;
	private String did;
	private String customername;
	private String description;
	private String initiatedby;
	private String transrefno;
	private String processingCode;
	private String proNotiCode;// for Notification
	private String proDisCode;// for Distributor use/ unuse
	private String status;
	private PayTemplateDetail[] templatedata;

	private int totalCount;
	private int currentPage;
	private int pageSize;

	public MerchantData() {
		clearProperty();
	}

	private void clearProperty() {
		aMerchantID = "";
		aFromDate = "";
		aToDate = "";
		debitaccount = "";
		collectionaccount = "";
		did = "";
		customername = "";
		description = "";
		initiatedby = "";
		transrefno = "";
		processingCode = "";
		proNotiCode = "";// for Notification
		proDisCode = "";
		templatedata = null;
		totalCount = 0;
		currentPage = 0;
		pageSize = 0;
		status = "";
	}

	public String getaFromDate() {
		return aFromDate;
	}

	public String getaMerchantID() {
		return aMerchantID;
	}

	public String getaToDate() {
		return aToDate;
	}

	public String getcollectionaccount() {
		return collectionaccount;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public String getCustomername() {
		return customername;
	}

	public String getdebitaccount() {
		return debitaccount;
	}

	public String getDescription() {
		return description;
	}

	public String getDid() {
		return did;
	}

	public String getInitiatedby() {
		return initiatedby;
	}

	public int getPageSize() {
		return pageSize;
	}

	public String getProcessingCode() {
		return processingCode;
	}

	public String getProDisCode() {
		return proDisCode;
	}

	public String getProNotiCode() {
		return proNotiCode;
	}

	public String getStatus() {
		return status;
	}

	public PayTemplateDetail[] getTemplatedata() {
		return templatedata;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public String getTransrefno() {
		return transrefno;
	}

	public void setaFromDate(String aFromDate) {
		this.aFromDate = aFromDate;
	}

	public void setaMerchantID(String aMerchantID) {
		this.aMerchantID = aMerchantID;
	}

	public void setaToDate(String aToDate) {
		this.aToDate = aToDate;
	}

	public void setcollectionaccount(String aCollectionAccount) {
		this.collectionaccount = aCollectionAccount;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setCustomername(String customername) {
		this.customername = customername;
	}

	public void setdebitaccount(String aDebitAccount) {
		this.debitaccount = aDebitAccount;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDid(String did) {
		this.did = did;
	}

	public void setInitiatedby(String initiatedby) {
		this.initiatedby = initiatedby;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}

	public void setProDisCode(String proDisCode) {
		this.proDisCode = proDisCode;
	}

	public void setProNotiCode(String proNotiCode) {
		this.proNotiCode = proNotiCode;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setTemplatedata(PayTemplateDetail[] templatedata) {
		this.templatedata = templatedata;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public void setTransrefno(String transrefno) {
		this.transrefno = transrefno;
	}

}
