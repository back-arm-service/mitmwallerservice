package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class WalletCheckRes {
	private String code;
	private String desc;
	private String walletID;
	private String name;
	private String status;

	public WalletCheckRes() {
		code = "";
		desc = "";
		walletID = "";
		name = "";
		status = "";
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public String getName() {
		return name;
	}

	public String getStatus() {
		return status;
	}

	public String getWalletID() {
		return walletID;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setWalletID(String walletID) {
		this.walletID = walletID;
	}

}
