/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DisPaymentTransData {
	private long autokey;
	private String transtime;
	private String transdate;
	private String mobileuserid;
	private String fromAccount;
	private String toAccount;
	private String toBranch;
	private String fromBranch;
	private String flexrefno;
	private String customerCode;
	private String customerName;
	private String customerNumber;
	private String chequeNo;
	private String recmethod;
	private String currencycode;
	private double amount;
	private String fileuploaddatetime;
	private String t1;
	private String t2;
	private String t3;
	private String t4;
	private String t5;
	private String t6;// authorize datetime
	private String t7;
	private String t8;
	private String t9;
	private String t10;
	private String t11;
	private String t12;
	private String t13;
	private String t14;
	private String t15;
	private String t16;
	private String t17;
	private String t18;
	private String t19;
	private String t20;
	private String t25 = "";
	private int n1;
	private int n2;
	private int n3;
	private int n4;
	private int n5;
	private int n6;
	private int n7;
	private int n8;
	private int n9;
	private int n10;
	private int n11;
	private int n12;
	private int n13;
	private int n14;
	private int n15;
	private String sessionId;
	private int totalpage;
	private int flag;
	private String isUser;
	private String securitycode;
	private String CustID;
	private String BrUserID;
	private int IsVIP;
	private double totalamount;
	private String chargescitystatus = "";
	private String phoneno = "";
	private String message = "";

	private String auth_stat = "";
	private String record_stat = "";

	private String completedate = "";
	private boolean reversedstatus = false;

	private double commissionamt;
	private double communicationamt;

	private String GroupCode;

	private String AccountClass;

	private String backFlexcode;

	public DisPaymentTransData() {
		clearproperty();
	}

	private void clearproperty() {
		amount = 0;
		sessionId = "";
		autokey = 0;
		chequeNo = "";
		currencycode = "";
		customerCode = "";
		flexrefno = "";
		mobileuserid = "";
		fromAccount = "";
		toAccount = "";
		toBranch = "";
		fromBranch = "";
		customerName = "";
		customerNumber = "";
		fileuploaddatetime = "";
		n1 = 0;
		n2 = 0;
		n3 = 0;
		n4 = 0;
		n5 = 0;
		n6 = 0;
		n7 = 0;
		n8 = 0;
		n9 = 0;
		n10 = 0;
		n11 = 0;
		n12 = 0;
		n13 = 0;
		n14 = 0;
		n15 = 0;
		recmethod = "";
		transdate = "";
		transtime = "";
		t1 = "";
		t2 = "";
		t3 = "";
		t4 = "";
		t5 = "";
		t6 = "";
		t7 = "";
		t8 = "";
		t9 = "";
		t10 = "";
		t11 = "";
		t12 = "";
		t13 = "";
		t14 = "";
		t15 = "";
		t16 = "";
		t17 = "";
		t18 = "";
		t19 = "";
		t20 = "";
		auth_stat = "";
		record_stat = "";
		commissionamt = 0;
		communicationamt = 0;
		completedate = "";
	}

	public String getAccountClass() {
		return AccountClass;
	}

	public double getAmount() {
		return amount;
	}

	public String getAuth_stat() {
		return auth_stat;
	}

	public long getAutokey() {
		return autokey;
	}

	public String getBackFlexcode() {
		return backFlexcode;
	}

	/**
	 * @return the brUserID
	 */
	public String getBrUserID() {
		return BrUserID;
	}

	public String getChargescitystatus() {
		return chargescitystatus;
	}

	public String getChequeNo() {
		return chequeNo;
	}

	public double getCommissionamt() {
		return commissionamt;
	}

	public double getCommunicationamt() {
		return communicationamt;
	}

	public String getCompletedate() {
		return completedate;
	}

	public String getCurrencycode() {
		return currencycode;
	}

	public String getCustID() {
		return CustID;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public String getFileuploaddatetime() {
		return fileuploaddatetime;
	}

	public int getFlag() {
		return flag;
	}

	public String getFlexrefno() {
		return flexrefno;
	}

	public String getFromAccount() {
		return fromAccount;
	}

	public String getFromBranch() {
		return fromBranch;
	}

	public String getGroupCode() {
		return GroupCode;
	}

	public String getIsUser() {
		return isUser;
	}

	/**
	 * @return the isVIP
	 */
	public synchronized int getIsVIP() {
		return IsVIP;
	}

	public String getMessage() {
		return message;
	}

	public String getMobileuserid() {
		return mobileuserid;
	}

	public int getN1() {
		return n1;
	}

	public int getN10() {
		return n10;
	}

	public int getN11() {
		return n11;
	}

	public int getN12() {
		return n12;
	}

	public int getN13() {
		return n13;
	}

	public int getN14() {
		return n14;
	}

	public int getN15() {
		return n15;
	}

	public int getN2() {
		return n2;
	}

	public int getN3() {
		return n3;
	}

	public int getN4() {
		return n4;
	}

	public int getN5() {
		return n5;
	}

	public int getN6() {
		return n6;
	}

	public int getN7() {
		return n7;
	}

	public int getN8() {
		return n8;
	}

	public int getN9() {
		return n9;
	}

	public String getPhoneno() {
		return phoneno;
	}

	public String getRecmethod() {
		return recmethod;
	}

	public String getRecord_stat() {
		return record_stat;
	}

	public String getSecuritycode() {
		return securitycode;
	}

	public String getSessionId() {
		return sessionId;
	}

	public String getT1() {
		return t1;
	}

	public String getT10() {
		return t10;
	}

	public String getT11() {
		return t11;
	}

	public String getT12() {
		return t12;
	}

	public String getT13() {
		return t13;
	}

	public String getT14() {
		return t14;
	}

	public String getT15() {
		return t15;
	}

	public String getT16() {
		return t16;
	}

	public String getT17() {
		return t17;
	}

	public String getT18() {
		return t18;
	}

	public String getT19() {
		return t19;
	}

	public String getT2() {
		return t2;
	}

	public String getT20() {
		return t20;
	}

	public String getT25() {
		return t25;
	}

	public String getT3() {
		return t3;
	}

	public String getT4() {
		return t4;
	}

	public String getT5() {
		return t5;
	}

	public String getT6() {
		return t6;
	}

	public String getT7() {
		return t7;
	}

	public String getT8() {
		return t8;
	}

	public String getT9() {
		return t9;
	}

	public String getToAccount() {
		return toAccount;
	}

	public String getToBranch() {
		return toBranch;
	}

	public double getTotalamount() {
		return totalamount;
	}

	public int getTotalpage() {
		return totalpage;
	}

	public String getTransdate() {
		return transdate;
	}

	public String getTranstime() {
		return transtime;
	}

	public boolean isReversedstatus() {
		return reversedstatus;
	}

	public void setAccountClass(String accountClass) {
		AccountClass = accountClass;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void setAuth_stat(String auth_stat) {
		this.auth_stat = auth_stat;
	}

	public void setAutokey(long autokey) {
		this.autokey = autokey;
	}

	public void setBackFlexcode(String backFlexcode) {
		this.backFlexcode = backFlexcode;
	}

	/**
	 * @param brUserID
	 *            the brUserID to set
	 */
	public void setBrUserID(String brUserID) {
		BrUserID = brUserID;
	}

	public void setChargescitystatus(String chargescitystatus) {
		this.chargescitystatus = chargescitystatus;
	}

	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}

	public void setCommissionamt(double commissionamt) {
		this.commissionamt = commissionamt;
	}

	public void setCommunicationamt(double communicationamt) {
		this.communicationamt = communicationamt;
	}

	public void setCompletedate(String completedate) {
		this.completedate = completedate;
	}

	public void setCurrencycode(String currencycode) {
		this.currencycode = currencycode;
	}

	public void setCustID(String custID) {
		CustID = custID;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public void setFileuploaddatetime(String fileuploaddatetime) {
		this.fileuploaddatetime = fileuploaddatetime;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public void setFlexrefno(String flexrefno) {
		this.flexrefno = flexrefno;
	}

	public void setFromAccount(String fromAccount) {
		this.fromAccount = fromAccount;
	}

	public void setFromBranch(String fromBranch) {
		this.fromBranch = fromBranch;
	}

	public void setGroupCode(String groupCode) {
		GroupCode = groupCode;
	}

	public void setIsUser(String isUser) {
		this.isUser = isUser;
	}

	/**
	 * @param isVIP
	 *            the isVIP to set
	 */
	public synchronized void setIsVIP(int isVIP) {
		IsVIP = isVIP;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setMobileuserid(String mobileuserid) {
		this.mobileuserid = mobileuserid;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public void setN10(int n10) {
		this.n10 = n10;
	}

	public void setN11(int n11) {
		this.n11 = n11;
	}

	public void setN12(int n12) {
		this.n12 = n12;
	}

	public void setN13(int n13) {
		this.n13 = n13;
	}

	public void setN14(int n14) {
		this.n14 = n14;
	}

	public void setN15(int n15) {
		this.n15 = n15;
	}

	public void setN2(int n2) {
		this.n2 = n2;
	}

	public void setN3(int n3) {
		this.n3 = n3;
	}

	public void setN4(int n4) {
		this.n4 = n4;
	}

	public void setN5(int n5) {
		this.n5 = n5;
	}

	public void setN6(int n6) {
		this.n6 = n6;
	}

	public void setN7(int n7) {
		this.n7 = n7;
	}

	public void setN8(int n8) {
		this.n8 = n8;
	}

	public void setN9(int n9) {
		this.n9 = n9;
	}

	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}

	public void setRecmethod(String recmethod) {
		this.recmethod = recmethod;
	}

	public void setRecord_stat(String record_stat) {
		this.record_stat = record_stat;
	}

	public void setReversedstatus(boolean reversedstatus) {
		this.reversedstatus = reversedstatus;
	}

	public void setSecuritycode(String securitycode) {
		this.securitycode = securitycode;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT10(String t10) {
		this.t10 = t10;
	}

	public void setT11(String t11) {
		this.t11 = t11;
	}

	public void setT12(String t12) {
		this.t12 = t12;
	}

	public void setT13(String t13) {
		this.t13 = t13;
	}

	public void setT14(String t14) {
		this.t14 = t14;
	}

	public void setT15(String t15) {
		this.t15 = t15;
	}

	public void setT16(String t16) {
		this.t16 = t16;
	}

	public void setT17(String t17) {
		this.t17 = t17;
	}

	public void setT18(String t18) {
		this.t18 = t18;
	}

	public void setT19(String t19) {
		this.t19 = t19;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setT20(String t20) {
		this.t20 = t20;
	}

	public void setT25(String t25) {
		this.t25 = t25;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public void setT4(String t4) {
		this.t4 = t4;
	}

	public void setT5(String t5) {
		this.t5 = t5;
	}

	public void setT6(String t6) {
		this.t6 = t6;
	}

	public void setT7(String t7) {
		this.t7 = t7;
	}

	public void setT8(String t8) {
		this.t8 = t8;
	}

	public void setT9(String t9) {
		this.t9 = t9;
	}

	public void setToAccount(String toAccount) {
		this.toAccount = toAccount;
	}

	public void setToBranch(String toBranch) {
		this.toBranch = toBranch;
	}

	public void setTotalamount(double totalamount) {
		this.totalamount = totalamount;
	}

	public void setTotalpage(int totalpage) {
		this.totalpage = totalpage;
	}

	public void setTransdate(String transdate) {
		this.transdate = transdate;
	}

	public void setTranstime(String transtime) {
		this.transtime = transtime;
	}

}
