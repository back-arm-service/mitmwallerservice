package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ImageRequest {

	private String userID;

	public ImageRequest() {
		clearProperty();
	}

	private void clearProperty() {
		this.userID = "";
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

}
