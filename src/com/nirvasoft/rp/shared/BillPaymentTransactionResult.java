package com.nirvasoft.rp.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BillPaymentTransactionResult {

	private String code;
	private String desc;
	private String merchantID;
	private String account;
	private String userId;
	private String fromDate;
	private String toDate;
	private String durationType;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private int pageCount;
	private String status;

	private BillPaymentTransaction[] billResults = null;

	public String getAccount() {
		return account;
	}

	public BillPaymentTransaction[] getBillResults() {
		return billResults;
	}

	public String getCode() {
		return code;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public String getDesc() {
		return desc;
	}

	public String getDurationType() {
		return durationType;
	}

	public String getFromDate() {
		return fromDate;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public int getPageCount() {
		return pageCount;
	}

	public int getPageSize() {
		return pageSize;
	}

	public String getStatus() {
		return status;
	}

	public String getToDate() {
		return toDate;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public String getUserId() {
		return userId;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public void setBillResults(BillPaymentTransaction[] billResults) {
		this.billResults = billResults;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setDurationType(String durationType) {
		this.durationType = durationType;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "BillPaymentTransactionResult [code=" + code + ", desc=" + desc + ", merchantID=" + merchantID
				+ ", account=" + account + ", userId=" + userId + ", fromDate=" + fromDate + ", toDate=" + toDate
				+ ", durationType=" + durationType + ", totalCount=" + totalCount + ", currentPage=" + currentPage
				+ ", pageSize=" + pageSize + ", pageCount=" + pageCount + ", status=" + status + ", billResults="
				+ Arrays.toString(billResults) + "]";
	}

}
