package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
// @JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterData {

	private long syskey;
	private long autokey;
	private String createddate;
	private String modifieddate;
	private String userid;
	private String username;
	private int recordStatus;
	private int syncStatus;
	private long syncBatch;
	private long usersyskey;
	private String T1;
	private String T2;
	private String T3;
	private String T4;
	private String T5;
	private String T6;
	private String T7;
	private String T8;
	private String T9;
	private String T10;
	private String T11;
	private String T12;
	private String T13;
	private String T14;
	private String T15;
	private String T16;
	private String T17;
	private String T18;
	private String T19;
	private String T20;
	private int n1;
	private int n2;
	private int n3;
	private int n4;
	private int n5;
	private int n6;
	private int n7;
	private int n8;
	private int n9;
	private int n10;
	private int n11;
	private int n12;
	private int n13;
	private int n14;
	private int n15;
	private int n16;
	private int n17;
	private int n18;
	private int n19;
	private int n20;
	private ChatData[] chatData;
	private RegisterData[] person;
	private int select1;

	private String upload[];

	private String errMsg;

	public RegisterData() {
		clearProperties();

	}

	public void clearProperties() {
		this.syskey = 0;
		this.autokey = 0;
		this.createddate = "";
		this.modifieddate = "";
		this.userid = "";
		this.username = "";
		this.recordStatus = 0;
		this.syncStatus = 0;
		this.syncBatch = 0;
		this.usersyskey = 0;
		this.T1 = "";
		this.T2 = "";
		this.T3 = "";
		this.T4 = "";
		this.T5 = "";
		this.T6 = "";
		this.T7 = "";
		this.T8 = "";
		this.T9 = "";
		this.T10 = "";
		this.T11 = "";
		this.T12 = "";
		this.T13 = "";
		this.T14 = "";
		this.T15 = "";
		this.T16 = "";
		this.T17 = "";
		this.T18 = "";
		this.T19 = "";
		this.T20 = "";
		this.n1 = 0;
		this.n2 = 0;
		this.n3 = 0;
		this.n4 = 0;
		this.n5 = 0;
		this.n6 = 0;
		this.n7 = 0;
		this.n8 = 0;
		this.n9 = 0;
		this.n10 = 0;
		this.n11 = 0;
		this.n12 = 0;
		this.n13 = 0;
		this.n14 = 0;
		this.n15 = 0;
		this.n16 = 0;
		this.n17 = 0;
		this.n18 = 0;
		this.n19 = 0;
		this.n20 = 0;
		this.upload = new String[0];
		this.errMsg = "";
		this.select1 = 0;
		// this.chatData = new String[0];

	}

	public long getAutokey() {
		return autokey;
	}

	public ChatData[] getChatData() {
		return chatData;
	}

	public String getCreateddate() {
		return createddate;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public String getModifieddate() {
		return modifieddate;
	}

	public int getN1() {
		return n1;
	}

	public int getN10() {
		return n10;
	}

	public int getN11() {
		return n11;
	}

	public int getN12() {
		return n12;
	}

	public int getN13() {
		return n13;
	}

	public int getN14() {
		return n14;
	}

	public int getN15() {
		return n15;
	}

	public int getN16() {
		return n16;
	}

	public int getN17() {
		return n17;
	}

	public int getN18() {
		return n18;
	}

	public int getN19() {
		return n19;
	}

	public int getN2() {
		return n2;
	}

	public int getN20() {
		return n20;
	}

	public int getN3() {
		return n3;
	}

	public int getN4() {
		return n4;
	}

	public int getN5() {
		return n5;
	}

	public int getN6() {
		return n6;
	}

	public int getN7() {
		return n7;
	}

	public int getN8() {
		return n8;
	}

	public int getN9() {
		return n9;
	}

	public RegisterData[] getPerson() {
		return person;
	}

	public int getRecordStatus() {
		return recordStatus;
	}

	public int getSelect1() {
		return select1;
	}

	public long getSyncBatch() {
		return syncBatch;
	}

	public int getSyncStatus() {
		return syncStatus;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getT1() {
		return T1;
	}

	public String getT10() {
		return T10;
	}

	public String getT11() {
		return T11;
	}

	public String getT12() {
		return T12;
	}

	public String getT13() {
		return T13;
	}

	public String getT14() {
		return T14;
	}

	public String getT15() {
		return T15;
	}

	public String getT16() {
		return T16;
	}

	public String getT17() {
		return T17;
	}

	public String getT18() {
		return T18;
	}

	public String getT19() {
		return T19;
	}

	public String getT2() {
		return T2;
	}

	public String getT20() {
		return T20;
	}

	public String getT3() {
		return T3;
	}

	public String getT4() {
		return T4;
	}

	public String getT5() {
		return T5;
	}

	public String getT6() {
		return T6;
	}

	public String getT7() {
		return T7;
	}

	public String getT8() {
		return T8;
	}

	public String getT9() {
		return T9;
	}

	public String[] getUpload() {
		return upload;
	}

	public String getUserid() {
		return userid;
	}

	public String getUsername() {
		return username;
	}

	public long getUsersyskey() {
		return usersyskey;
	}

	public void setAutokey(long autokey) {
		this.autokey = autokey;
	}

	public void setChatData(ChatData[] chatData) {
		this.chatData = chatData;
	}

	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public void setModifieddate(String modifieddate) {
		this.modifieddate = modifieddate;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public void setN10(int n10) {
		this.n10 = n10;
	}

	public void setN11(int n11) {
		this.n11 = n11;
	}

	public void setN12(int n12) {
		this.n12 = n12;
	}

	public void setN13(int n13) {
		this.n13 = n13;
	}

	public void setN14(int n14) {
		this.n14 = n14;
	}

	public void setN15(int n15) {
		this.n15 = n15;
	}

	public void setN16(int n16) {
		this.n16 = n16;
	}

	public void setN17(int n17) {
		this.n17 = n17;
	}

	public void setN18(int n18) {
		this.n18 = n18;
	}

	public void setN19(int n19) {
		this.n19 = n19;
	}

	public void setN2(int n2) {
		this.n2 = n2;
	}

	public void setN20(int n20) {
		this.n20 = n20;
	}

	public void setN3(int n3) {
		this.n3 = n3;
	}

	public void setN4(int n4) {
		this.n4 = n4;
	}

	public void setN5(int n5) {
		this.n5 = n5;
	}

	public void setN6(int n6) {
		this.n6 = n6;
	}

	public void setN7(int n7) {
		this.n7 = n7;
	}

	public void setN8(int n8) {
		this.n8 = n8;
	}

	public void setN9(int n9) {
		this.n9 = n9;
	}

	public void setPerson(RegisterData[] person) {
		this.person = person;
	}

	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}

	public void setSelect1(int select1) {
		this.select1 = select1;
	}

	public void setSyncBatch(long syncBatch) {
		this.syncBatch = syncBatch;
	}

	public void setSyncStatus(int syncStatus) {
		this.syncStatus = syncStatus;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setT1(String t1) {
		T1 = t1;
	}

	public void setT10(String t10) {
		T10 = t10;
	}

	public void setT11(String t11) {
		T11 = t11;
	}

	public void setT12(String t12) {
		T12 = t12;
	}

	public void setT13(String t13) {
		T13 = t13;
	}

	public void setT14(String t14) {
		T14 = t14;
	}

	public void setT15(String t15) {
		T15 = t15;
	}

	public void setT16(String t16) {
		T16 = t16;
	}

	public void setT17(String t17) {
		T17 = t17;
	}

	public void setT18(String t18) {
		T18 = t18;
	}

	public void setT19(String t19) {
		T19 = t19;
	}

	public void setT2(String t2) {
		T2 = t2;
	}

	public void setT20(String t20) {
		T20 = t20;
	}

	public void setT3(String t3) {
		T3 = t3;
	}

	public void setT4(String t4) {
		T4 = t4;
	}

	public void setT5(String t5) {
		T5 = t5;
	}

	public void setT6(String t6) {
		T6 = t6;
	}

	public void setT7(String t7) {
		T7 = t7;
	}

	public void setT8(String t8) {
		T8 = t8;
	}

	public void setT9(String t9) {
		T9 = t9;
	}

	public void setUpload(String[] upload) {
		this.upload = upload;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setUsersyskey(long usersyskey) {
		this.usersyskey = usersyskey;
	}
}
