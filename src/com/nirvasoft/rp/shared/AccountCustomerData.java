package com.nirvasoft.rp.shared;

/* 
TUN THURA THET 2011 04 21
*/
import java.io.Serializable;

import com.nirvasoft.rp.shared.icbs.ICBSCustomerData;

public class AccountCustomerData implements Serializable {
	private static final long serialVersionUID = 7744840013072695732L;

	private String pAccNumber;

	private String pCustomerID;
	private int pAccountType;
	private int pRelationType;
	private ICBSCustomerData pCustomerData;

	public AccountCustomerData() {
		ClearProperty();
	}

	private void ClearProperty() {
		pAccNumber = "";
		pCustomerID = "";
		pAccountType = 0;
		pRelationType = 0;
	}

	public String getAccountNumber() {
		return pAccNumber;
	}

	public int getAccountType() {
		return pAccountType;
	}

	public ICBSCustomerData getCustomer() {
		return pCustomerData;
	}

	public String getCustomerID() {
		return pCustomerID;
	}

	public int getRelationType() {
		return pRelationType;
	}

	public void setAccountNumber(String p) {
		pAccNumber = p;
	}

	public void setAccountType(int p) {
		pAccountType = p;
	}

	public void setCustomer(ICBSCustomerData p) {
		pCustomerData = p;
	}

	public void setCustomerID(String p) {
		pCustomerID = p;
	}

	public void setRelationType(int p) {
		pRelationType = p;
	}
}
