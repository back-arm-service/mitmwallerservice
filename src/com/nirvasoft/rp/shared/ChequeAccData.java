package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ChequeAccData {
	private String chequeAcc;

	public ChequeAccData() {
		this.chequeAcc = "";
	}

	public String getChequeAcc() {
		return chequeAcc;
	}

	public void setChequeAcc(String chequeAcc) {
		this.chequeAcc = chequeAcc;
	}

	@Override
	public String toString() {
		return "{chequeAcc=" + chequeAcc + "}";
	}

}
