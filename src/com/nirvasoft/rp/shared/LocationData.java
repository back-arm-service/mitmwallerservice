package com.nirvasoft.rp.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LocationData {

	private String code;
	private String desc;
	private ATMLocationResData[] data = null;

	public LocationData() {
		clearProperty();
	}

	public void clearProperty() {
		this.code = "";
		this.desc = "";
		this.data = null;
	}

	public String getCode() {
		return code;
	}

	public ATMLocationResData[] getData() {
		return data;
	}

	public String getDesc() {
		return desc;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setData(ATMLocationResData[] data) {
		this.data = data;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public String toString() {
		return "LocationData [code=" + code + ", desc=" + desc + ", data=" + Arrays.toString(data) + "]";
	}

}
