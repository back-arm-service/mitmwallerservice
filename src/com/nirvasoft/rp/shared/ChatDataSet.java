package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
// @JsonIgnoreProperties(ignoreUnknown = true)
public class ChatDataSet {

	private ChatData[] data;
	private PagerData[] pdata;
	private String[] replyData;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private boolean state;
	private String Role;
	private long syskey;

	public ChatDataSet() {
		data = new ChatData[0];
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public ChatData[] getData() {
		return data;
	}

	public int getPageSize() {
		return pageSize;
	}

	public PagerData[] getPdata() {
		return pdata;
	}

	public String[] getReplyData() {
		return replyData;
	}

	public String getRole() {
		return Role;
	}

	public long getSyskey() {
		return syskey;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public boolean isState() {
		return state;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setData(ChatData[] data) {
		this.data = data;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setPdata(PagerData[] pdata) {
		this.pdata = pdata;
	}

	public void setReplyData(String[] replyData) {
		this.replyData = replyData;
	}

	public void setRole(String role) {
		Role = role;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

}
