package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Response {

	private String messageCode = "";
	private String messageDesc = "";
	private String accountNo = "";
	private String trnRefNo = "";

	public Response() {
		clearProperty();
	}

	private void clearProperty() {
		messageCode = "";
		messageDesc = "";
		accountNo = "";
		trnRefNo = "";
	}

	public String getAccountNo() {
		return accountNo;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public String getMessageDesc() {
		return messageDesc;
	}

	public String getTrnRefNo() {
		return trnRefNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public void setMessageDesc(String messageDesc) {
		this.messageDesc = messageDesc;
	}

	public void setTrnRefNo(String trnRefNo) {
		this.trnRefNo = trnRefNo;
	}

}
