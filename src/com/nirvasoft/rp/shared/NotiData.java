package com.nirvasoft.rp.shared;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotiData {
	private String sessionid;
	private String code;
	private String desc;
	private String autokey;
	private String createddatetime;
	private String modifieddatetime;
	private String title;
	private String description;
	private String toDescription;
	private String type;
	private String typedescription;
	private String date;
	private String time;
	private String userid;
	private String receiverID;
	private String username;
	private String lastuserid;
	private String lastusername;
	private String sendtofcmfirebase;
	private String responsecode;
	private String responsebody;
	private String t1="";
	private String t2;
	private String t3="";
	private String n1;
	private String n2;
	private String n3;
	private String modifiedhistory;
	private String notiType;
	private String serviceType;
	private String functionType;
	private String merchantID;
	private String amount;
	private String refNo;
	private String name;
	private String transactionType;
	private String merchantName;
	private String receiverName;
	
	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public NotiData() {
		clearProperties();
	}

	private void clearProperties() {
		sessionid = "";
		code = "";
		desc = "";

		autokey = "";
		createddatetime = "";
		modifieddatetime = "";
		title = "";
		description = "";
		type = "";
		typedescription = "";
		date = "";
		time = "";
		userid = "";
		receiverID="";
		username = "";
		lastuserid = "";
		lastusername = "";
		sendtofcmfirebase = "";
		responsecode = "";
		responsebody = "";
		t1 = "";
		t2 = "";
		t3 = "";
		n1 = "";
		n2 = "";
		n3 = "";
		modifiedhistory = "";
		serviceType="";
		functionType="";
		toDescription="";
		merchantID="";
		name="";
		refNo="";
		transactionType="";
		merchantName="";
		receiverName="";
	}
	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public String getReceiverID() {
		return receiverID;
	}

	public void setReceiverID(String receiverID) {
		this.receiverID = receiverID;
	}

	public String getToDescription() {
		return toDescription;
	}

	public void setToDescription(String toDescription) {
		this.toDescription = toDescription;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getFunctionType() {
		return functionType;
	}

	public void setFunctionType(String functionType) {
		this.functionType = functionType;
	}

	public String getSessionid() {
		return sessionid;
	}

	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getAutokey() {
		return autokey;
	}

	public void setAutokey(String autokey) {
		this.autokey = autokey;
	}

	public String getCreateddatetime() {
		return createddatetime;
	}

	public void setCreateddatetime(String createddatetime) {
		this.createddatetime = createddatetime;
	}

	public String getModifieddatetime() {
		return modifieddatetime;
	}

	public void setModifieddatetime(String modifieddatetime) {
		this.modifieddatetime = modifieddatetime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTypedescription() {
		return typedescription;
	}

	public void setTypedescription(String typedescription) {
		this.typedescription = typedescription;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getLastuserid() {
		return lastuserid;
	}

	public void setLastuserid(String lastuserid) {
		this.lastuserid = lastuserid;
	}

	public String getLastusername() {
		return lastusername;
	}

	public void setLastusername(String lastusername) {
		this.lastusername = lastusername;
	}

	public String getSendtofcmfirebase() {
		return sendtofcmfirebase;
	}

	public void setSendtofcmfirebase(String sendtofcmfirebase) {
		this.sendtofcmfirebase = sendtofcmfirebase;
	}

	public String getResponsecode() {
		return responsecode;
	}

	public void setResponsecode(String responsecode) {
		this.responsecode = responsecode;
	}

	public String getResponsebody() {
		return responsebody;
	}

	public void setResponsebody(String responsebody) {
		this.responsebody = responsebody;
	}

	public String getT1() {
		return t1;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public String getT2() {
		return t2;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public String getT3() {
		return t3;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public String getN1() {
		return n1;
	}

	public void setN1(String n1) {
		this.n1 = n1;
	}

	public String getN2() {
		return n2;
	}

	public void setN2(String n2) {
		this.n2 = n2;
	}

	public String getN3() {
		return n3;
	}

	public void setN3(String n3) {
		this.n3 = n3;
	}

	public String getModifiedhistory() {
		return modifiedhistory;
	}

	public void setModifiedhistory(String modifiedhistory) {
		this.modifiedhistory = modifiedhistory;
	}
	public String getNotiType() {
		return notiType;
	}

	public void setNotiType(String notiType) {
		this.notiType = notiType;
	}

	@Override
	public String toString() {
		return "NotiData [sessionid=" + sessionid + ", code=" + code + ", desc=" + desc + ", autokey=" + autokey
				+ ", createddatetime=" + createddatetime + ", modifieddatetime=" + modifieddatetime + ", title=" + title
				+ ", description=" + description + ", type=" + type + ", typedescription=" + typedescription + ", date="
				+ date + ", time=" + time + ", userid=" + userid + ", username=" + username + ", lastuserid="
				+ lastuserid + ", lastusername=" + lastusername + ", sendtofcmfirebase=" + sendtofcmfirebase
				+ ", responsecode=" + responsecode + ", responsebody=" + responsebody + ", t1=" + t1 + ", t2=" + t2
				+ ", t3=" + t3 + ", n1=" + n1 + ", n2=" + n2 + ", n3=" + n3 + ", modifiedhistory=" + modifiedhistory
				+ "]";
	}

}
