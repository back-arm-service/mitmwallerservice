package com.nirvasoft.rp.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RegisterDataSet {
	private RegisterData[] regdata;
	private int totalCount;
	private boolean state;

	public RegisterDataSet() {
		regdata = null;
	}

	public RegisterData[] getRegdata() {
		return regdata;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public boolean isState() {
		return state;
	}

	public void setRegdata(RegisterData[] regdata) {
		this.regdata = regdata;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	@Override
	public String toString() {
		return "RegisterDataSet [regdata=" + Arrays.toString(regdata) + ", totalCount=" + totalCount + ", state="
				+ state + "]";
	}

}
