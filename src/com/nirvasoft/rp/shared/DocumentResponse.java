package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DocumentResponse {

	private String code;
	private String desc;
	private DocumentData[] pdfList;

	public DocumentResponse() {
		this.clearProperty();
	}

	private void clearProperty() {
		this.code = "";
		this.desc = "";
		this.pdfList = null;
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public DocumentData[] getPdfList() {
		return pdfList;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setPdfList(DocumentData[] pdfList) {
		this.pdfList = pdfList;
	}

}
