package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LOVSetupDetailData {

	private long hKey;
	private String lov;
	private String lovCde;
	private String lovDesc1;
	private String lovDesc2;
	private double price;
	private String srno;

	public LOVSetupDetailData() {
		clearProperty();
	}

	void clearProperty() {
		hKey = 0;
		lov = "";
		lovCde = "";
		lovDesc1 = "";
		lovDesc2 = "";
		price = 0.0;
		srno = "1";
	}

	public long gethKey() {
		return hKey;
	}

	public String getLov() {
		return lov;
	}

	public String getLovCde() {
		return lovCde;
	}

	public String getLovDesc1() {
		return lovDesc1;
	}

	public String getLovDesc2() {
		return lovDesc2;
	}

	public double getPrice() {
		return price;
	}

	public String getSrno() {
		return srno;
	}

	public void sethKey(long hKey) {
		this.hKey = hKey;
	}

	public void setLov(String lov) {
		this.lov = lov;
	}

	public void setLovCde(String lovCde) {
		this.lovCde = lovCde;
	}

	public void setLovDesc1(String lovDesc1) {
		this.lovDesc1 = lovDesc1;
	}

	public void setLovDesc2(String lovDesc2) {
		this.lovDesc2 = lovDesc2;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public void setSrno(String srno) {
		this.srno = srno;
	}
}
