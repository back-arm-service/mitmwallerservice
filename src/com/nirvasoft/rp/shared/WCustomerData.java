package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class WCustomerData {
	private String CustomerId;
	private int CustomerType;
	private String Title;
	private String Name;
	private String AliasName;
	private int Sex;
	private String DateOfBirth;
	private String NrcNo;
	private String HouseNo;
	private String Street;
	private String Ward;
	private String Township;
	private String City;
	private String Division;
	private String Country;
	private String Phone;
	private String Email;
	private String Fax;
	private String PostalCode;
	private String Status;
	private String Occupation;
	private String FatherName;
	private String UniversalId;
	private int MStatus;
	private String OldNrcNo;
	private String M1;
	private String M2;
	private String T1;
	private String T2;
	private String T3;
	private String T4;
	private String T5;
	private String T6;
	private String BcNo;
	private int N1;
	private int N2;
	private int N3;
	private int N4;
	private int N5;
	private int N6;

	public WCustomerData() {
		ClearProperty();
	}

	private void ClearProperty() {
		CustomerId = "";
		CustomerType = 0;
		Title = "";
		Name = "";
		AliasName = "";
		Sex = 0;
		AliasName = "";
		NrcNo = "";
		HouseNo = "";
		Street = "";
		Ward = "";
		Township = "";
		City = "";
		Division = "";
		Country = "";
		Phone = "";
		Email = "";
		Fax = "";
		PostalCode = "";
		Status = "";
		Occupation = "";
		FatherName = "";
		UniversalId = "";
		MStatus = 0;
		OldNrcNo = "";
		M1 = "";
		M2 = "";
		T1 = "";
		T2 = "";
		T3 = "";
		T4 = "";
		T5 = "";
		T6 = "";
		BcNo = "";
		N1 = 0;
		N2 = 0;
		N3 = 0;
		N4 = 0;
		N5 = 0;
		N6 = 0;
	}

	public String getAliasName() {
		return AliasName;
	}

	public String getBcNo() {
		return BcNo;
	}

	public String getCity() {
		return City;
	}

	public String getCountry() {
		return Country;
	}

	public String getCustomerID() {
		return CustomerId;
	}

	public int getCustomerType() {
		return CustomerType;
	}

	public String getDivision() {
		return Division;
	}

	public String getDob() {
		return AliasName;
	}

	public String getEmail() {
		return Email;
	}

	public String getFatherName() {
		return FatherName;
	}

	public String getFax() {
		return Fax;
	}

	public String getHouseNo() {
		return HouseNo;
	}

	public String getM1() {
		return M1;
	}

	public String getM2() {
		return M2;
	}

	public int getmStatus() {
		return MStatus;
	}

	public int getN1() {
		return N1;
	}

	public int getN2() {
		return N2;
	}

	public int getN3() {
		return N3;
	}

	public int getN4() {
		return N4;
	}

	public int getN5() {
		return N5;
	}

	public int getN6() {
		return N6;
	}

	public String getName() {
		return Name;
	}

	public String getNrcNo() {
		return NrcNo;
	}

	public String getOccupation() {
		return Occupation;
	}

	public String getOldNrcNo() {
		return OldNrcNo;
	}

	public String getPhone() {
		return Phone;
	}

	public String getPostalCode() {
		return PostalCode;
	}

	public int getSex() {
		return Sex;
	}

	public String getStatus() {
		return Status;
	}

	public String getStreet() {
		return Street;
	}

	public String getT1() {
		return T1;
	}

	public String getT2() {
		return T2;
	}

	public String getT3() {
		return T3;
	}

	public String getT4() {
		return T4;
	}

	public String getT5() {
		return T5;
	}

	public String getT6() {
		return T6;
	}

	public String getTitle() {
		return Title;
	}

	public String getTownship() {
		return Township;
	}

	public String getUniversalId() {
		return UniversalId;
	}

	public String getWard() {
		return Ward;
	}

	public void setAliasName(String aliasName) {
		this.AliasName = aliasName;
	}

	public void setBcNo(String bcNo) {
		this.BcNo = bcNo;
	}

	public void setCity(String city) {
		this.City = city;
	}

	public void setCountry(String country) {
		this.Country = country;
	}

	public void setCustomerID(String customerID) {
		this.CustomerId = customerID;
	}

	public void setCustomerType(int customerType) {
		this.CustomerType = customerType;
	}

	public void setDivision(String division) {
		this.Division = division;
	}

	public void setDob(String dob) {
		this.AliasName = dob;
	}

	public void setEmail(String email) {
		this.Email = email;
	}

	public void setFatherName(String fatherName) {
		this.FatherName = fatherName;
	}

	public void setFax(String fax) {
		this.Fax = fax;
	}

	public void setHouseNo(String houseNo) {
		this.HouseNo = houseNo;
	}

	public void setM1(String m1) {
		this.M1 = m1;
	}

	public void setM2(String m2) {
		this.M2 = m2;
	}

	public void setmStatus(int mStatus) {
		this.MStatus = mStatus;
	}

	public void setN1(int n1) {
		this.N1 = n1;
	}

	public void setN2(int n2) {
		this.N2 = n2;
	}

	public void setN3(int n3) {
		this.N3 = n3;
	}

	public void setN4(int n4) {
		this.N4 = n4;
	}

	public void setN5(int n5) {
		this.N5 = n5;
	}

	public void setN6(int n6) {
		this.N6 = n6;
	}

	public void setName(String name) {
		this.Name = name;
	}

	public void setNrcNo(String nrcNo) {
		this.NrcNo = nrcNo;
	}

	public void setOccupation(String occupation) {
		this.Occupation = occupation;
	}

	public void setOldNrcNo(String oldNrcNo) {
		this.OldNrcNo = oldNrcNo;
	}

	public void setPhone(String phone) {
		this.Phone = phone;
	}

	public void setPostalCode(String postalCode) {
		this.PostalCode = postalCode;
	}

	public void setSex(int sex) {
		this.Sex = sex;
	}

	public void setStatus(String status) {
		this.Status = status;
	}

	public void setStreet(String street) {
		this.Street = street;
	}

	public void setT1(String t1) {
		this.T1 = t1;
	}

	public void setT2(String t2) {
		this.T2 = t2;
	}

	public void setT3(String t3) {
		this.T3 = t3;
	}

	public void setT4(String t4) {
		this.T4 = t4;
	}

	public void setT5(String t5) {
		this.T5 = t5;
	}

	public void setT6(String t6) {
		this.T6 = t6;
	}

	public void setTitle(String title) {
		this.Title = title;
	}

	public void setTownship(String township) {
		this.Township = township;
	}

	public void setUniversalId(String universalId) {
		this.UniversalId = universalId;
	}

	public void setWard(String ward) {
		this.Ward = ward;
	}
}
