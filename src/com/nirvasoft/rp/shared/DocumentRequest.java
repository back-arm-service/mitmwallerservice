package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DocumentRequest {

	private String type;
	private String region;
	private String keyword;

	public DocumentRequest() {
		clearProperty();
	}

	private void clearProperty() {
		this.type = "";
		this.region = "";
		this.keyword = "";
	}

	public String getKeyword() {
		return keyword;
	}

	public String getRegion() {
		return region;
	}

	public String getType() {
		return type;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public void setType(String type) {
		this.type = type;
	}

}
