
package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseData {

	private String code;
	private String desc;
	private int count;
	//private String t2;

	public int getCount() {
		return count;
	}

//	public String getT2() {
//		return t2;
//	}
//
//	public void setT2(String t2) {
//		this.t2 = t2;
//	}

	public void setCount(int count) {
		this.count = count;
	}

	public ResponseData() {
		clearProperty();
	}

	private void clearProperty() {
		code = "";
		desc = "";
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public String toString() {
		return "{code=" + code + ", desc=" + desc + "}";
	}

}
