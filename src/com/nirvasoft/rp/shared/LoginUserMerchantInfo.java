package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LoginUserMerchantInfo {
	private String loginUser;
	private String merchantId;
	private String merchantName;

	public LoginUserMerchantInfo() {
		clearProperty();
	}

	public void clearProperty() {
		loginUser = "";
		merchantId = "";
		merchantName = "";
	}

	public String getLoginUser() {
		return loginUser;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setLoginUser(String loginUser) {
		this.loginUser = loginUser;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

}
