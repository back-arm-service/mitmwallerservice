package com.nirvasoft.rp.shared;

import java.io.Serializable;

public class MessageCodeData implements Serializable {

	private static final long serialVersionUID = 6052296161313884400L;

	private String pValue;
	private String pDescription;

	public MessageCodeData() {
		clearProperty();
	}

	private void clearProperty() {
		pValue = "00";
		pDescription = "";
	}

	public String getDescription() {
		return pDescription;
	}

	public String getValue() {
		return pValue;
	}

	public void setDescription(String pDescription) {
		this.pDescription = pDescription;
	}

	public void setValue(String pValue) {
		this.pValue = pValue;
	}
}
