package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ChangePwdCheckOTPRequestData {

	private String userID;
	private String sessionID;
	private String otpCode;
	private String rKey;

	private String oldPassword;
	private String newPassword;

	void clearProperty() {
		userID = "";
		otpCode = "";
		sessionID = "";
		oldPassword = "";
		newPassword = "";
		rKey = "";
	}

	public String getNewPassword() {
		return newPassword;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public String getOtpCode() {
		return otpCode;
	}

	public String getrKey() {
		return rKey;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getUserID() {
		return userID;
	}

	public void setNewPassword(String newpassword) {
		this.newPassword = newpassword;
	}

	public void setOldPassword(String password) {
		this.oldPassword = password;
	}

	public void setOtpCode(String otpcode) {
		this.otpCode = otpcode;
	}

	public void setrKey(String rKey) {
		this.rKey = rKey;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setUserID(String userid) {
		this.userID = userid;
	}

	@Override
	public String toString() {
		return "{userID=" + userID + ", sessionID=" + sessionID + ", otpCode=" + otpCode + ", oldPassword="
				+ oldPassword + ", newPassword=" + newPassword + "}";
	}

}
