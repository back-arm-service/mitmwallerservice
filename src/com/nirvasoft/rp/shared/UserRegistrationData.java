package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserRegistrationData {
	private String userid;
	private String sessionid;
	private String Name;
	private String nrc;
	private String phoneno;
	private int status;
	private int regtype;

	private String t1;
	private String t2;
	private int n1;
	private int n2;

	public UserRegistrationData() {
		clearProperty();
	}

	private void clearProperty() {
		userid = "";
		sessionid = "";
		Name = "";
		nrc = "";
		phoneno = "";
		status = 0;
		regtype = 0;
		t1 = "";
		t2 = "";
		n1 = 0;
		n2 = 0;
	}

	public int getN1() {
		return n1;
	}

	public int getN2() {
		return n2;
	}

	public String getName() {
		return Name;
	}

	public String getNrc() {
		return nrc;
	}

	public String getPhoneno() {
		return phoneno;
	}

	public int getRegtype() {
		return regtype;
	}

	public String getSessionid() {
		return sessionid;
	}

	public int getStatus() {
		return status;
	}

	public String getT1() {
		return t1;
	}

	public String getT2() {
		return t2;
	}

	public String getUserid() {
		return userid;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public void setN2(int n2) {
		this.n2 = n2;
	}

	public void setName(String name) {
		Name = name;
	}

	public void setNrc(String nrc) {
		this.nrc = nrc;
	}

	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}

	public void setRegtype(int regtype) {
		this.regtype = regtype;
	}

	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

}
