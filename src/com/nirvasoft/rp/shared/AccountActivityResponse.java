package com.nirvasoft.rp.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AccountActivityResponse {

	private String code;
	private String desc;
	private String durationType;
	private String fromDate;
	private String toDate;
	private String acctNo;
	private String customerNo;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private int pageCount;
	private AccountActivityData[] data = null;

	public AccountActivityResponse() {
		clearProperty();
	}

	void clearProperty() {
		acctNo = "";
		customerNo = "";
		code = "";
		desc = "";
		durationType = "";
		fromDate = "";
		toDate = "";
	}

	public String getAcctNo() {
		return acctNo;
	}

	public String getCode() {
		return code;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public AccountActivityData[] getData() {
		return data;
	}

	public String getDesc() {
		return desc;
	}

	public String getDurationType() {
		return durationType;
	}

	public String getFromDate() {
		return fromDate;
	}

	public int getPageCount() {
		return pageCount;
	}

	public int getPageSize() {
		return pageSize;
	}

	public String getToDate() {
		return toDate;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public void setData(AccountActivityData[] data) {
		this.data = data;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setDurationType(String durationType) {
		this.durationType = durationType;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	@Override
	public String toString() {
		return "{code=" + code + ", desc=" + desc + ", durationType=" + durationType + ", fromDate=" + fromDate
				+ ", toDate=" + toDate + ", accNumber=" + acctNo + ", customerNo=" + customerNo + ", totalCount="
				+ totalCount + ", currentPage=" + currentPage + ", pageSize=" + pageSize + ", pageCount=" + pageCount
				+ ", data=" + Arrays.toString(data) + "}";
	}

}
