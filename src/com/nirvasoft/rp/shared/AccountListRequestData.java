package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AccountListRequestData {

	private String userID;
	private String sessionID;

	public AccountListRequestData() {
		clearProperty();
	}

	private void clearProperty() {
		this.userID = "";
		this.sessionID = "";
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getUserID() {
		return userID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	@Override
	public String toString() {
		return "AccountListRequestData [userID=" + userID + ", sessionID=" + sessionID + "]";
	}

}
