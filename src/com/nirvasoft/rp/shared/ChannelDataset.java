package com.nirvasoft.rp.shared;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

import com.nirvasoft.rp.data.ChannelData;

@XmlRootElement
public class ChannelDataset implements Serializable {

	private int pPageNo;
	private int pPageSize;
	private int ptotalCount;

	private String pUserID;
	private String pUserName;

	private ArrayList<ChannelData> pDataList;

	public ChannelDataset() {
		ClearProperty();
	}

	private void ClearProperty() {
		pPageNo = 0;
		pPageSize = 0;
		ptotalCount = 0;

		pUserID = "";
		pUserName = "";
	}

	public ArrayList<ChannelData> getDataList() {
		return pDataList;
	}

	public int getPageNo() {
		return pPageNo;
	}

	public int getPageSize() {
		return pPageSize;
	}

	public int getTotalCount() {
		return ptotalCount;
	}

	public String getUserID() {
		return pUserID;
	}

	public String getUserName() {
		return pUserName;
	}

	public void setDataList(ArrayList<ChannelData> p) {
		this.pDataList = p;
	}

	public void setPageNo(int p) {
		pPageNo = p;
	}

	public void setPageSize(int p) {
		pPageSize = p;
	}

	public void setTotalCount(int p) {
		ptotalCount = p;
	}

	public void setUserID(String p) {
		pUserID = p;
	}

	public void setUserName(String p) {
		pUserName = p;
	}
}