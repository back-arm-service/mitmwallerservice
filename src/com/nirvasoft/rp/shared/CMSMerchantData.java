package com.nirvasoft.rp.shared;

import com.nirvasoft.rp.data.FeatureData;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CMSMerchantData
{
  private String msgCode = "";
  private String msgDesc = "";
  private long sysKey;
  private String userName = "";
  private String userId = "";
  private String feature = "";
  private String branchCode = "";
  private String createdUserID = "";
  private String accountNumber = "";
  private String branchname = "";
  private String processingCode = "";
  private String CreatedDate = "";
  private String ModifiedDate = "";
  private String ModifiedUserId = "";
  private int recordStatus = 0;
  private String[] reference = null;
  private String order = "";
  //private FeatureData[] data = null;

  private String t1 = "";

  private String t2 = "";

  private String t3 = "";

  private String t4 = "";

  private String t5 = "";

  private String t6 = "";

  private String t7 = "";
  private String t8 = "";
  private String t9 = "";
  private String t10 = "";
  private String t11 = "";
  private String t12 = "";
  private String t13 = "";
  private String t14 = "";
  private String t15 = "";
  private int n1 = 0;
  private int n2 = 0;
  private int n3 = 0;
  private int n4 = 0;
  private int n5 = 0;
  private int n6 = 0;
  private int n7 = 0;
  private int n8 = 0;
  private int n9 = 0;
  private int n10 = 0;
  private double samecity = 0.0D;
  private double diffcity = 0.0D;
  private double rate = 0.0D;
  private double minimumamount = 0.0D;
  private int emailtype = 0;

  private String t16 = "";
  private String t17 = "";
  private String t18 = "";
  private String t19 = "";
  private String t20 = "";
  private String t21 = "";

  private String t22 = "";
  private String t23 = "";
  private String t24 = "";
  private String t25 = "";
  private int n11 = 0;
  private int n12 = 0;
  private int n13 = 0;
  private int n14 = 0;
  private int n15 = 0;
  private String chkAccount = "false";
  private String chkGL = "false";
  private String chk = "";
  private String sessionID = "";
  //private ArrayList<CMSMerchantAccRefData> mMerchantAccData = new ArrayList();
  private String p1 = "";

  public String getChk() {
	return chk;
}

public void setChk(String chk) {
	this.chk = chk;
}

public String getAccountNumber() {
    return this.accountNumber;
  }

  public String getBranchCode() {
    return this.branchCode;
  }

  public String getBranchname() {
    return this.branchname;
  }

  public String getChkAccount() {
    return this.chkAccount;
  }

  public String getChkGL() {
    return this.chkGL;
  }

  public String getCreatedDate() {
    return this.CreatedDate;
  }

  public String getCreatedUserID() {
    return this.createdUserID;
  }


  public double getDiffcity() {
    return this.diffcity;
  }

  public int getEmailtype() {
    return this.emailtype;
  }

  public String getFeature() {
    return this.feature;
  }

  public double getMinimumamount() {
    return this.minimumamount;
  }


  public String getModifiedDate() {
    return this.ModifiedDate;
  }

  public String getModifiedUserId() {
    return this.ModifiedUserId;
  }

  public String getMsgCode() {
    return this.msgCode;
  }

  public String getMsgDesc() {
    return this.msgDesc;
  }

  public int getN1() {
    return this.n1;
  }

  public int getN10() {
    return this.n10;
  }

  public int getN11() {
    return this.n11;
  }

  public int getN12() {
    return this.n12;
  }

  public int getN13() {
    return this.n13;
  }

  public int getN14() {
    return this.n14;
  }

  public int getN15() {
    return this.n15;
  }

  public int getN2() {
    return this.n2;
  }

  public int getN3() {
    return this.n3;
  }

  public int getN4() {
    return this.n4;
  }

  public int getN5() {
    return this.n5;
  }

  public int getN6() {
    return this.n6;
  }

  public int getN7() {
    return this.n7;
  }

  public int getN8() {
    return this.n8;
  }

  public int getN9() {
    return this.n9;
  }

  public String getOrder() {
    return this.order;
  }

  public String getP1() {
    return this.p1;
  }

  public String getProcessingCode() {
    return this.processingCode;
  }

  public double getRate() {
    return this.rate;
  }

  public int getRecordStatus() {
    return this.recordStatus;
  }

  public String[] getReference() {
    return this.reference;
  }

  public double getSamecity() {
    return this.samecity;
  }

  public String getSessionID() {
    return this.sessionID;
  }

  public long getSysKey() {
    return this.sysKey;
  }

  public String getT1() {
    return this.t1;
  }

  public String getT10() {
    return this.t10;
  }

  public String getT11() {
    return this.t11;
  }

  public String getT12() {
    return this.t12;
  }

  public String getT13() {
    return this.t13;
  }

  public String getT14() {
    return this.t14;
  }

  public String getT15() {
    return this.t15;
  }

  public String getT16() {
    return this.t16;
  }

  public String getT17() {
    return this.t17;
  }

  public String getT18() {
    return this.t18;
  }

  public String getT19() {
    return this.t19;
  }

  public String getT2() {
    return this.t2;
  }

  public String getT20() {
    return this.t20;
  }

  public String getT21() {
    return this.t21;
  }

  public String getT22() {
    return this.t22;
  }

  public String getT23() {
    return this.t23;
  }

  public String getT24() {
    return this.t24;
  }

  public String getT25() {
    return this.t25;
  }

  public String getT3() {
    return this.t3;
  }

  public String getT4() {
    return this.t4;
  }

  public String getT5() {
    return this.t5;
  }

  public String getT6() {
    return this.t6;
  }

  public String getT7() {
    return this.t7;
  }

  public String getT8() {
    return this.t8;
  }

  public String getT9() {
    return this.t9;
  }

  public String getUserId() {
    return this.userId;
  }

  public String getUserName() {
    return this.userName;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public void setBranchCode(String branchCode) {
    this.branchCode = branchCode;
  }

  public void setBranchname(String branchname) {
    this.branchname = branchname;
  }

  public void setChkAccount(String chkAccount) {
    this.chkAccount = chkAccount;
  }

  public void setChkGL(String chkGL) {
    this.chkGL = chkGL;
  }

  public void setCreatedDate(String createdDate) {
    this.CreatedDate = createdDate;
  }

  public void setCreatedUserID(String createdUserID) {
    this.createdUserID = createdUserID;
  }


  public void setDiffcity(double diffcity) {
    this.diffcity = diffcity;
  }

  public void setEmailtype(int emailtype) {
    this.emailtype = emailtype;
  }

  public void setFeature(String feature) {
    this.feature = feature;
  }

  public void setMinimumamount(double minimumamount) {
    this.minimumamount = minimumamount;
  }

  public void setModifiedDate(String modifiedDate) {
    this.ModifiedDate = modifiedDate;
  }

  public void setModifiedUserId(String modifiedUserId) {
    this.ModifiedUserId = modifiedUserId;
  }

  public void setMsgCode(String msgCode) {
    this.msgCode = msgCode;
  }

  public void setMsgDesc(String msgDesc) {
    this.msgDesc = msgDesc;
  }

  public void setN1(int n1) {
    this.n1 = n1;
  }

  public void setN10(int n10) {
    this.n10 = n10;
  }

  public void setN11(int n11) {
    this.n11 = n11;
  }

  public void setN12(int n12) {
    this.n12 = n12;
  }

  public void setN13(int n13) {
    this.n13 = n13;
  }

  public void setN14(int n14) {
    this.n14 = n14;
  }

  public void setN15(int n15) {
    this.n15 = n15;
  }

  public void setN2(int n2) {
    this.n2 = n2;
  }

  public void setN3(int n3) {
    this.n3 = n3;
  }

  public void setN4(int n4) {
    this.n4 = n4;
  }

  public void setN5(int n5) {
    this.n5 = n5;
  }

  public void setN6(int n6) {
    this.n6 = n6;
  }

  public void setN7(int n7) {
    this.n7 = n7;
  }

  public void setN8(int n8) {
    this.n8 = n8;
  }

  public void setN9(int n9) {
    this.n9 = n9;
  }

  public void setOrder(String order) {
    this.order = order;
  }

  public void setP1(String p1) {
    this.p1 = p1;
  }

  public void setProcessingCode(String processingCode) {
    this.processingCode = processingCode;
  }

  public void setRate(double rate) {
    this.rate = rate;
  }

  public void setRecordStatus(int recordStatus) {
    this.recordStatus = recordStatus;
  }

  public void setReference(String[] reference) {
    this.reference = reference;
  }

  public void setSamecity(double samecity) {
    this.samecity = samecity;
  }

  public void setSessionID(String sessionID) {
    this.sessionID = sessionID;
  }

  public void setSysKey(long sysKey) {
    this.sysKey = sysKey;
  }

  public void setT1(String t1) {
    this.t1 = t1;
  }

  public void setT10(String t10) {
    this.t10 = t10;
  }

  public void setT11(String t11) {
    this.t11 = t11;
  }

  public void setT12(String t12) {
    this.t12 = t12;
  }

  public void setT13(String t13) {
    this.t13 = t13;
  }

  public void setT14(String t14) {
    this.t14 = t14;
  }

  public void setT15(String t15) {
    this.t15 = t15;
  }

  public void setT16(String t16) {
    this.t16 = t16;
  }

  public void setT17(String t17) {
    this.t17 = t17;
  }

  public void setT18(String t18) {
    this.t18 = t18;
  }

  public void setT19(String t19) {
    this.t19 = t19;
  }

  public void setT2(String t2) {
    this.t2 = t2;
  }

  public void setT20(String t20) {
    this.t20 = t20;
  }

  public void setT21(String t21) {
    this.t21 = t21;
  }

  public void setT22(String t22) {
    this.t22 = t22;
  }

  public void setT23(String t23) {
    this.t23 = t23;
  }

  public void setT24(String t24) {
    this.t24 = t24;
  }

  public void setT25(String t25) {
    this.t25 = t25;
  }

  public void setT3(String t3) {
    this.t3 = t3;
  }

  public void setT4(String t4) {
    this.t4 = t4;
  }

  public void setT5(String t5) {
    this.t5 = t5;
  }

  public void setT6(String t6) {
    this.t6 = t6;
  }

  public void setT7(String t7) {
    this.t7 = t7;
  }

  public void setT8(String t8) {
    this.t8 = t8;
  }

  public void setT9(String t9) {
    this.t9 = t9;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }
}