package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class VersionReq {
	private String version;
	private String appCode;
	private String t1;
	private int n1;

	public VersionReq() {
		clearProperty();
	}

	private void clearProperty() {
		version = "";
		appCode = "";
		t1 = "";
		n1 = 0;
	}

	public String getAppCode() {
		return appCode;
	}

	public int getN1() {
		return n1;
	}

	public String getT1() {
		return t1;
	}

	public String getVersion() {
		return version;
	}

	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "{version=" + version + ", appCode = " + appCode + ", t1 = " + t1 + ", n1 = " + n1 + "}";
	}
}
