package com.nirvasoft.rp.shared;

public class NotiListData {

	private long autoKey;
	private String userID;
	private String userName;
	private String createdDate;
	private String date;
	private String time;
	private int type;
	private String title;
	private String description;
	private String t1;
	private String t2;
	private String t3;
	private int n1;
	private int n2;
	private int n3;

	private String code = "";
	private String desc = "";

	public NotiListData() {
		clearProperties();
	}

	private void clearProperties() {
		this.autoKey = 0;
		this.userID = "";
		this.userName = "";
		this.createdDate = "";
		this.date = "";
		this.type = 0;
		this.time = "";
		this.title = "";
		this.description = "";
		this.t1 = "";
		this.t2 = "";
		this.t3 = "";
		this.n1 = 0;
		this.n2 = 0;
		this.n3 = 0;

		this.code = "";
		this.desc = "";
	}

	public long getAutoKey() {
		return autoKey;
	}

	public void setAutoKey(long autoKey) {
		this.autoKey = autoKey;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getT1() {
		return t1;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public String getT2() {
		return t2;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public String getT3() {
		return t3;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public int getN1() {
		return n1;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public int getN2() {
		return n2;
	}

	public void setN2(int n2) {
		this.n2 = n2;
	}

	public int getN3() {
		return n3;
	}

	public void setN3(int n3) {
		this.n3 = n3;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public String toString() {
		return "NotiData [autoKey=" + autoKey + ", userID=" + userID + ", userName=" + userName + ", createdDate="
				+ createdDate + ", date=" + date + ", time=" + time + ", type=" + type + ", title=" + title
				+ ", description=" + description + ", t1=" + t1 + ", t2=" + t2 + ", t3=" + t3 + ", n1=" + n1 + ", n2="
				+ n2 + ", n3=" + n3 + ", code=" + code + ", desc=" + desc + "]";
	}

}
