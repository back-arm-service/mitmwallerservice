package com.nirvasoft.rp.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AccountActivityResult {

	private String code;
	private String desc;
	private String accNumber;
	private String customerNo;
	private String durationType;
	private String fromDate;
	private String toDate;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private int pageCount;
	private AccountActivityData[] data = null;

	public AccountActivityResult() {
		clearProperty();
	}

	void clearProperty() {
		code = "";
		desc = "";
		accNumber = "";
		customerNo = "";
	}

	public String getAccNumber() {
		return accNumber;
	}

	public String getCode() {
		return code;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public AccountActivityData[] getData() {
		return data;
	}

	public String getDesc() {
		return desc;
	}

	public String getDurationType() {
		return durationType;
	}

	public String getFromDate() {
		return fromDate;
	}

	public int getPageCount() {
		return pageCount;
	}

	public int getPageSize() {
		return pageSize;
	}

	public String getToDate() {
		return toDate;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setAccNumber(String accNumber) {
		this.accNumber = accNumber;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public void setData(AccountActivityData[] data) {
		this.data = data;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setDurationType(String durationType) {
		this.durationType = durationType;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	@Override
	public String toString() {
		return "AccountActivityResult [code=" + code + ", desc=" + desc + ", accNumber=" + accNumber + ", customerNo="
				+ customerNo + ", durationType=" + durationType + ", fromDate=" + fromDate + ", toDate=" + toDate
				+ ", totalCount=" + totalCount + ", currentPage=" + currentPage + ", pageSize=" + pageSize
				+ ", pageCount=" + pageCount + ", data=" + Arrays.toString(data) + "]";
	}

}
