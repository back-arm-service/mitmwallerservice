package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ActivityHistoryList {

	private String activity;

	public ActivityHistoryList() {
		clearProperty();
	}

	private void clearProperty() {
		this.activity = "";
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	@Override
	public String toString() {
		return "{activity=" + activity + "}";
	}

}
