package com.nirvasoft.rp.shared;

import java.io.Serializable;
import java.util.ArrayList;

import com.nirvasoft.rp.shared.icbs.AccountData;

public class DataResult implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2327728558305381390L;
	private String pExpDate = "";
	private String Vov="";
	private String vNo="";
	private boolean pStatus = false;
	private String pDescription = "";
	private String pCode = "";
	private String pData = "";
	private double pBalance;
	private long pTransactionNumber;
	private String pCashCardAccountNumber;
	private String pSecureCardAccountNumber;
	private ArrayList<TempData> plstTempData = new ArrayList<TempData>();
	private int pErrorFlag = 0;
	private long autoLinkTransRef;
	private String effectiveDate="";
	private boolean universalstatus=false;
	private boolean bcestatus = false;
	private long pAutoKey;
	private ArrayList<SignatureData> plstSigData = new ArrayList<SignatureData>();
	private String str=""; //mzcm
	private int transRef=0; //mzcm
	private String poSerial="";//mzcm
	private int recordStatus=0;//mzcm
	private String pRefNo="";//mzcm
	private double pTotalAmount;
	private double pCount;
	
	public long getAutoLinkTransRef() {
		return autoLinkTransRef;
	}
	public void setAutoLinkTransRef(long autoLinkTransRef) {
		this.autoLinkTransRef = autoLinkTransRef;
	}
	public boolean getStatus(){ return pStatus;}
	public void setStatus(boolean p){ pStatus=p;}
	public String getDescription(){ return pDescription;}
	public void setDescription(String p){ pDescription=p;}
	public String getCode(){ return pCode;}
	public void setCode(String p){ pCode=p;}
	public String getData(){ return pData;}
	public void setData(String p){ pData=p;}
	public double getBalance(){ return pBalance;}
	public void setBalance(double p){ pBalance=p;}

	public long getTransactionNumber(){ return pTransactionNumber;}
	public void setTransactionNumber(long p){ pTransactionNumber=p;}
	
	public String getCashCardAccountNumber(){ return pCashCardAccountNumber;}
	public void setCashCardAccountNumber(String p){ pCashCardAccountNumber=p;}
	
	public String getSecureCardAccountNumber(){ return pSecureCardAccountNumber;}
	public void setSecureCardAccountNumber(String p){ pSecureCardAccountNumber=p;}
	
	public ArrayList<TempData> getTempDataList() {	return plstTempData;}
	public void setTempDataList(ArrayList<TempData> plstTempData) {	this.plstTempData = plstTempData;	}
	
	public int getErrorFlag() {	return pErrorFlag;	}	
	public void setErrorFlag(int pErrorFlag) {	this.pErrorFlag = pErrorFlag;	}
	public String getVNo() {
		return vNo;
	}
	public void setVNo(String vNo) {
		this.vNo = vNo;
	}
	public String getVov() {
		return Vov;
	}
	public void setVov(String vov) {
		Vov = vov;
	}
	public String getExpDate() {
		return pExpDate;
	}
	public void setExpDate(String pExpDate) {
		this.pExpDate = pExpDate;
	}
	public boolean getUniversalstatus(){ return universalstatus;}
	public void setUniversalstatus(boolean p){ universalstatus=p;}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public boolean getBcestatus() {
		return bcestatus;
	}
	public void setBcestatus(boolean bcestatus) {
		this.bcestatus = bcestatus;
	}
	public long getpAutoKey() {
		return pAutoKey;
	}
	public void setpAutoKey(long pAutoKey) {
		this.pAutoKey = pAutoKey;
	}
	public ArrayList<SignatureData> getlistSigData() {
		return plstSigData;
	}
	public void setlistSigData(ArrayList<SignatureData> plstSigData) {
		this.plstSigData = plstSigData;
	}
	public String getStr() {
		return str;
	}
	public void setStr(String str) {
		this.str = str;
	}
	public String getpRefNo() {
		return pRefNo;
	}
	public void setpRefNo(String pRefNo) {
		this.pRefNo = pRefNo;
	}
	public int getTransRef() {
		return transRef;
	}
	public void setTransRef(int transRef) {
		this.transRef = transRef;
	}
	public int getRecordStatus() {
		return recordStatus;
	}
	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}
	public String getPoSerial() {
		return poSerial;
	}
	public void setPoSerial(String poSerial) {
		this.poSerial = poSerial;
	}
	
	//mmmyint 07.08.2018
	private ArrayList<AccountData> plstAccountData;
	public ArrayList<AccountData> getListAccountData() { 	return plstAccountData; }
	public void setListAccountData(ArrayList<AccountData> plstAccountData) { this.plstAccountData = plstAccountData; }
	
	public void setTotalAmount(double pTotalAmount) {
		this.pTotalAmount = pTotalAmount;
	}
	public double getTotalAmount() {
		return pTotalAmount;
	}
	
	public void setCount(double pCount) {
		this.pCount = pCount;
	}
	public double getCount() {
		return pCount;
	}
	@Override
	public String toString() {
		return "DataResult [pExpDate=" + pExpDate + ", Vov=" + Vov + ", vNo=" + vNo + ", pStatus=" + pStatus
				+ ", pDescription=" + pDescription + ", pCode=" + pCode + ", pData=" + pData + ", pBalance=" + pBalance
				+ ", pTransactionNumber=" + pTransactionNumber + ", pCashCardAccountNumber=" + pCashCardAccountNumber
				+ ", pSecureCardAccountNumber=" + pSecureCardAccountNumber + ", plstTempData=" + plstTempData
				+ ", pErrorFlag=" + pErrorFlag + ", autoLinkTransRef=" + autoLinkTransRef + ", effectiveDate="
				+ effectiveDate + ", universalstatus=" + universalstatus + ", bcestatus=" + bcestatus + ", pAutoKey="
				+ pAutoKey + ", plstSigData=" + plstSigData + ", str=" + str + ", transRef=" + transRef + ", poSerial="
				+ poSerial + ", recordStatus=" + recordStatus + ", pRefNo=" + pRefNo + ", pTotalAmount=" + pTotalAmount
				+ ", pCount=" + pCount + ", plstAccountData=" + plstAccountData + ", getAutoLinkTransRef()="
				+ getAutoLinkTransRef() + ", getStatus()=" + getStatus() + ", getDescription()=" + getDescription()
				+ ", getCode()=" + getCode() + ", getData()=" + getData() + ", getBalance()=" + getBalance()
				+ ", getTransactionNumber()=" + getTransactionNumber() + ", getCashCardAccountNumber()="
				+ getCashCardAccountNumber() + ", getSecureCardAccountNumber()=" + getSecureCardAccountNumber()
				+ ", getTempDataList()=" + getTempDataList() + ", getErrorFlag()=" + getErrorFlag() + ", getVNo()="
				+ getVNo() + ", getVov()=" + getVov() + ", getExpDate()=" + getExpDate() + ", getUniversalstatus()="
				+ getUniversalstatus() + ", getEffectiveDate()=" + getEffectiveDate() + ", getBcestatus()="
				+ getBcestatus() + ", getpAutoKey()=" + getpAutoKey() + ", getlistSigData()=" + getlistSigData()
				+ ", getStr()=" + getStr() + ", getpRefNo()=" + getpRefNo() + ", getTransRef()=" + getTransRef()
				+ ", getRecordStatus()=" + getRecordStatus() + ", getPoSerial()=" + getPoSerial()
				+ ", getListAccountData()=" + getListAccountData() + ", getTotalAmount()=" + getTotalAmount()
				+ ", getCount()=" + getCount() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

}
