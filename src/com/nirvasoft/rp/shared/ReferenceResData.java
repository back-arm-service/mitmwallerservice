package com.nirvasoft.rp.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ReferenceResData {

	private String code;
	private String desc;
	private LovData[] lovList = null;

	public ReferenceResData() {
		clearProperty();
	}

	private void clearProperty() {
		code = "";
		desc = "";
		lovList = null;
	}

	public String getCode() {
		return code;
	}

	public LovData[] getDataList() {
		return lovList;
	}

	public String getDesc() {
		return desc;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDataList(LovData[] lovList) {
		this.lovList = lovList;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public String toString() {
		return "{code=" + code + ", desc=" + desc + ", lovData=" + Arrays.toString(lovList) + "}";
	}

}
