package com.nirvasoft.rp.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PackageDataResult {

	private String accessToken = "";
	private PackageListData[] PackageListData = null;

	public PackageDataResult() {
		clearProperty();
	}

	void clearProperty() {
		accessToken = "";
		PackageListData = null;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public PackageListData[] getDataList() {
		return PackageListData;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public void setDataList(PackageListData[] datalist) {
		this.PackageListData = datalist;
	}

	@Override
	public String toString() {
		return "{accessToken=" + accessToken + ", PackageListData=" + Arrays.toString(PackageListData) + "}";
	}

}
