package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ATMLocationReqData {

	private String id;
	private String userID;
	private String sessionID;

	public ATMLocationReqData() {
		clearProperty();
	}

	private void clearProperty() {
		this.id = "";
		this.userID = "";
		this.sessionID = "";
	}

	public String getId() {
		return id;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getUserID() {
		return userID;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	@Override
	public String toString() {
		return "{id=" + id + ", userID=" + userID + ", sessionID=" + sessionID + "}";
	}

}
