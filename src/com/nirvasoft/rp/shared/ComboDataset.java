package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.nirvasoft.cms.shared.ComboData;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class ComboDataset {
	private ComboData[] data;
	private ComboData[] data1;

	public ComboDataset() {
		super();
		data = new ComboData[0];
		data1 = new ComboData[0];
	}

	public ComboData[] getData() {
		return data;
	}

	public ComboData[] getData1() {
		return data1;
	}

	public void setData(ComboData[] data) {
		this.data = data;
	}

	public void setData1(ComboData[] data1) {
		this.data1 = data1;
	}

}
