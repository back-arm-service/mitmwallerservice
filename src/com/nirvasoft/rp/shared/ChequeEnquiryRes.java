package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ChequeEnquiryRes {
	private String code;
	private String desc;
	private String status;

	public ChequeEnquiryRes() {
		clearProperty();
	}

	private void clearProperty() {
		code = "";
		desc = "";
		status = "";
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public String getStatus() {
		return status;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "{code=" + code + ", desc=" + desc + ", status = " + status + "}";
	}
}
