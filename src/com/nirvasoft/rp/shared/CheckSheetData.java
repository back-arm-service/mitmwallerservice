package com.nirvasoft.rp.shared;

import java.io.Serializable;

public class CheckSheetData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4852406040860841664L;

	private String pCheckSheetNo;
	private String pCheckSheetStatusInWord;
	private byte pCheckSheetStatus;
	private String pCheckSheetStart;
	private String pCheckSheetEnd;

	public CheckSheetData() {
		ClearProperty();
	}

	private void ClearProperty() {
		setCheckSheetNo("");
		setCheckSheetStatusInWord("");
		setCheckSheetStatus((byte) 0);
		setCheckSheetStart("");
		setCheckSheetEnd("");
	}

	public String getCheckSheetEnd() {
		return pCheckSheetEnd;
	}

	public String getCheckSheetNo() {
		return pCheckSheetNo;
	}

	public String getCheckSheetStart() {
		return pCheckSheetStart;
	}

	public byte getCheckSheetStatus() {
		return pCheckSheetStatus;
	}

	public String getCheckSheetStatusInWord() {
		return pCheckSheetStatusInWord;
	}

	public void setCheckSheetEnd(String pCheckSheetEnd) {
		this.pCheckSheetEnd = pCheckSheetEnd;
	}

	public void setCheckSheetNo(String pCheckSheetNo) {
		this.pCheckSheetNo = pCheckSheetNo;
	}

	public void setCheckSheetStart(String pCheckSheetStart) {
		this.pCheckSheetStart = pCheckSheetStart;
	}

	public void setCheckSheetStatus(byte pCheckSheetStatus) {
		this.pCheckSheetStatus = pCheckSheetStatus;
	}

	public void setCheckSheetStatusInWord(String pCheckSheetStatusInWord) {
		this.pCheckSheetStatusInWord = pCheckSheetStatusInWord;
	}

}
