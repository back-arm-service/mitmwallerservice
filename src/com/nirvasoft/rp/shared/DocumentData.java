package com.nirvasoft.rp.shared;

public class DocumentData {

	private String AutoKey;
	private String CreatedDate;
	private String ModifiedDate;
	private String name; // FileName
	private String UserId;
	private String Status;
	private String RecordStatus;
	private String FileType;
	private String Region;
	private String link; // FilePath
	private String PostedBy;
	private String ModifiedBy;
	private String FileSize;
	private String Pages;
	private String Title;
	private String keyword; // T1
	private String T2;
	private String T3;
	private String T4;
	private String T5;
	private String N1;
	private String N2;
	private String N3;
	private String N4;
	private String N5;

	public DocumentData() {
		clearProperty();
	}

	private void clearProperty() {
		this.AutoKey = "";
		this.CreatedDate = "";
		this.ModifiedDate = "";
		this.name = ""; // FileName
		this.UserId = "";
		this.Status = "";
		this.RecordStatus = "";
		this.FileType = "";
		this.Region = "";
		this.link = ""; // FilePath
		this.PostedBy = "";
		this.ModifiedBy = "";
		this.FileSize = "";
		this.Pages = "";
		this.Title = "";
		this.keyword = ""; // T1
		this.T2 = "";
		this.T3 = "";
		this.T4 = "";
		this.T5 = "";
		this.N1 = "";
		this.N2 = "";
		this.N3 = "";
		this.N4 = "";
		this.N5 = "";
	}

	public String getAutoKey() {
		return AutoKey;
	}

	public String getCreatedDate() {
		return CreatedDate;
	}

	public String getFileSize() {
		return FileSize;
	}

	public String getFileType() {
		return FileType;
	}

	public String getKeyword() {
		return keyword;
	}

	public String getLink() {
		return link;
	}

	public String getModifiedBy() {
		return ModifiedBy;
	}

	public String getModifiedDate() {
		return ModifiedDate;
	}

	public String getN1() {
		return N1;
	}

	public String getN2() {
		return N2;
	}

	public String getN3() {
		return N3;
	}

	public String getN4() {
		return N4;
	}

	public String getN5() {
		return N5;
	}

	public String getName() {
		return name;
	}

	public String getPages() {
		return Pages;
	}

	public String getPostedBy() {
		return PostedBy;
	}

	public String getRecordStatus() {
		return RecordStatus;
	}

	public String getRegion() {
		return Region;
	}

	public String getStatus() {
		return Status;
	}

	public String getT2() {
		return T2;
	}

	public String getT3() {
		return T3;
	}

	public String getT4() {
		return T4;
	}

	public String getT5() {
		return T5;
	}

	public String getTitle() {
		return Title;
	}

	public String getUserId() {
		return UserId;
	}

	public void setAutoKey(String autoKey) {
		AutoKey = autoKey;
	}

	public void setCreatedDate(String createdDate) {
		CreatedDate = createdDate;
	}

	public void setFileSize(String fileSize) {
		FileSize = fileSize;
	}

	public void setFileType(String fileType) {
		FileType = fileType;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public void setModifiedBy(String modifiedBy) {
		ModifiedBy = modifiedBy;
	}

	public void setModifiedDate(String modifiedDate) {
		ModifiedDate = modifiedDate;
	}

	public void setN1(String n1) {
		N1 = n1;
	}

	public void setN2(String n2) {
		N2 = n2;
	}

	public void setN3(String n3) {
		N3 = n3;
	}

	public void setN4(String n4) {
		N4 = n4;
	}

	public void setN5(String n5) {
		N5 = n5;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPages(String pages) {
		Pages = pages;
	}

	public void setPostedBy(String postedBy) {
		PostedBy = postedBy;
	}

	public void setRecordStatus(String recordStatus) {
		RecordStatus = recordStatus;
	}

	public void setRegion(String region) {
		Region = region;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public void setT2(String t2) {
		T2 = t2;
	}

	public void setT3(String t3) {
		T3 = t3;
	}

	public void setT4(String t4) {
		T4 = t4;
	}

	public void setT5(String t5) {
		T5 = t5;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public void setUserId(String userId) {
		UserId = userId;
	}

}
