package com.nirvasoft.rp.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PackageListData {

	private String id;
	private String name;
	private PackageRateData[] PackageRateData = null;

	public PackageListData() {
		clearProperty();
	}

	void clearProperty() {
		id = "";
		name = "";
		PackageRateData = null;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public PackageRateData[] getRateList() {
		return PackageRateData;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRateList(PackageRateData[] ratelist) {
		this.PackageRateData = ratelist;
	}

	@Override
	public String toString() {
		return "{id=" + id + ", name=" + name + ", PackageRateData=" + Arrays.toString(PackageRateData) + "}";
	}

}
