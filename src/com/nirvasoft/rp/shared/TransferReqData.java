package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TransferReqData {
	private String userID;
	private String sessionID;
	private String fromAccount;
	private String toAccount;
	private String merchantID;
	private double amount;
	private double bankCharges;
	private String narrative;
	private String refNo;
	private String field1;
	private String field2;

	public TransferReqData() {
		clearProperty();
	}

	private void clearProperty() {
		userID = "";
		sessionID = "";
		fromAccount = "";
		toAccount = "";
		amount = 0;
		bankCharges = 0;
		narrative = "";
		refNo = "";
		field1 = "";
		field2 = "";
		merchantID = "";
	}

	public double getAmount() {
		return amount;
	}

	public double getBankCharges() {
		return bankCharges;
	}

	public String getField1() {
		return field1;
	}

	public String getField2() {
		return field2;
	}

	public String getFromAccount() {
		return fromAccount;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public String getNarrative() {
		return narrative;
	}

	public String getRefNo() {
		return refNo;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getToAccount() {
		return toAccount;
	}

	public String getUserID() {
		return userID;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void setBankCharges(double bankCharges) {
		this.bankCharges = bankCharges;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public void setFromAccount(String fromAccount) {
		this.fromAccount = fromAccount;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public void setNarrative(String narrative) {
		this.narrative = narrative;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setToAccount(String toAccount) {
		this.toAccount = toAccount;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	@Override
	public String toString() {
		return "{userID=" + userID + ", sessionID=" + sessionID + ", fromAccount=" + fromAccount + "toAccount="
				+ toAccount + ", merchantID=" + merchantID + ",amount=" + amount + "" + "bankCharges=" + bankCharges
				+ ",Narrative=" + narrative + ",refNo=" + refNo + ",field1=" + field1 + ",field2=" + field2 + "}";
	}
}
