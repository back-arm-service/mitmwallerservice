package com.nirvasoft.rp.shared;

public class SyskeyRequestData {
	private String userID;
	private String sessionID;
	private String beneficiaryID;

	public SyskeyRequestData() {
		clearProperty();
	}

	private void clearProperty() {
		userID = "";
		sessionID = "";
		beneficiaryID = "";
	}

	public String getBeneficiaryID() {
		return beneficiaryID;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getUserID() {
		return userID;
	}

	public void setBeneficiaryID(String beneficiaryID) {
		this.beneficiaryID = beneficiaryID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}
}
