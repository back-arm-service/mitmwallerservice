package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PackageRateData {
	private String tid;
	private String pid;
	private String did;
	private String m;
	private String r;
	private String amountServiceCharges;
	private String amountTax;
	private String amountTotal;

	public PackageRateData() {
		clearProperty();
	}

	void clearProperty() {
		tid = "";
		did = "";
		pid = "";
		m = "";
		r = "";
		amountServiceCharges = "0.00";
		amountTax = "0.00";
		amountTotal = "0.00";
	}

	public String getAmountServiceCharges() {
		return amountServiceCharges;
	}

	public String getAmountTax() {
		return amountTax;
	}

	public String getAmountTotal() {
		return amountTotal;
	}

	public String getDid() {
		return did;
	}

	public String getM() {
		return m;
	}

	public String getPid() {
		return pid;
	}

	public String getR() {
		return r;
	}

	public String getTid() {
		return tid;
	}

	public void setAmountServiceCharges(String amountServiceCharges) {
		this.amountServiceCharges = amountServiceCharges;
	}

	public void setAmountTax(String amountTax) {
		this.amountTax = amountTax;
	}

	public void setAmountTotal(String amountTotal) {
		this.amountTotal = amountTotal;
	}

	public void setDid(String did) {
		this.did = did;
	}

	public void setM(String m) {
		this.m = m;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public void setR(String r) {
		this.r = r;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	@Override
	public String toString() {
		return "{tid=" + tid + ", pid=" + pid + ", did=" + did + ", m=" + m + ", r=" + r + ", amountServiceCharges="
				+ amountServiceCharges + ", amountTax=" + amountTax + ", amountTotal=" + amountTotal + "}";
	}

}
