package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DispaymentList {

	private DispaymentTransactionData[] data;
	private PayTemplateDetailArr[] detailData;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private String grandTotal;
	private String incomeAmt;
	private String deduAmt;

	public DispaymentList() {
		clearProperty();
	}

	private void clearProperty() {
		data = null;
		detailData = null;
		totalCount = 0;
		currentPage = 0;
		pageSize = 0;
		grandTotal = "";
		incomeAmt = "";
		deduAmt = "";
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public DispaymentTransactionData[] getData() {
		return data;
	}

	public String getDeduAmt() {
		return deduAmt;
	}

	public PayTemplateDetailArr[] getDetailData() {
		return detailData;
	}

	public String getGrandTotal() {
		return grandTotal;
	}

	public String getIncomeAmt() {
		return incomeAmt;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setData(DispaymentTransactionData[] data) {
		this.data = data;
	}

	public void setDeduAmt(String deduAmt) {
		this.deduAmt = deduAmt;
	}

	public void setDetailData(PayTemplateDetailArr[] detailData) {
		this.detailData = detailData;
	}

	public void setGrandTotal(String grandTotal) {
		this.grandTotal = grandTotal;
	}

	public void setIncomeAmt(String incomeAmt) {
		this.incomeAmt = incomeAmt;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

}
