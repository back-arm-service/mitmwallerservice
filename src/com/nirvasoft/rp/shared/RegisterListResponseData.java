package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

import com.nirvasoft.rp.data.RegisterResponseData;

@XmlRootElement
public class RegisterListResponseData {

	private String code;
	private String desc;
	private RegisterResponseData[] resList;
	
	public RegisterListResponseData() {
		
		this.clearProperty();
	}

	private void clearProperty() {
		this.code = "";
		this.desc = "";
		this.resList = null;
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public RegisterResponseData[] getResList() {
		return resList;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setResList(RegisterResponseData[] resList) {
		this.resList = resList;
	}

}
