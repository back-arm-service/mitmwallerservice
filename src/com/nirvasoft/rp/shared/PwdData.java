package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PwdData {

	private String password;
	private String newpassword;
	private String userid;
	private String sessionid;
	private String iv;
	private String dm;
	private String salt;

	public PwdData() {
		clearProperties();
	}

	private void clearProperties() {
		password = "";
		newpassword = "";
		userid = "";
		sessionid = "";
		this.iv = "";
		this.dm = "";
		this.salt = "";
	}

	public String getDm() {
		return dm;
	}

	public String getIv() {
		return iv;
	}

	public String getNewpassword() {
		return newpassword;
	}

	public String getPassword() {
		return password;
	}

	public String getSalt() {
		return salt;
	}

	public String getSessionid() {
		return sessionid;
	}

	public String getUserid() {
		return userid;
	}

	public void setDm(String dm) {
		this.dm = dm;
	}

	public void setIv(String iv) {
		this.iv = iv;
	}

	public void setNewpassword(String newpassword) {
		this.newpassword = newpassword;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

}
