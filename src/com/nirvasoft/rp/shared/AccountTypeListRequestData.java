package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AccountTypeListRequestData {

	private String userID;
	private String sessionID;

	public AccountTypeListRequestData() {
		clearProperty();
	}

	private void clearProperty() {
		this.userID = "";
		this.sessionID = "";
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getUserID() {
		return userID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	@Override
	public String toString() {
		return "AccountTypeListRequestData [userID=" + userID + ", sessionID=" + sessionID + "]";
	}

}
