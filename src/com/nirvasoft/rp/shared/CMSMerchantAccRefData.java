package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CMSMerchantAccRefData {

	private long syskey;
	private String createdDate = "";
	private String modifiedDate = "";
	private String createdUserID = "";
	private String modifiedUserID = "";
	private String accountNumber = "";
	private String userName = "";
	private String t1 = "";
	private String t2 = "";
	private String t3 = "";
	private String t4 = "";
	private String t5 = "";
	private String t6 = "";
	private String t7 = "";
	private String t8 = "";
	private String t9 = "";
	private String t10 = "";
	private int n1 = 0;
	private int n2 = 0;

	private int n3 = 0;
	private int n4 = 0;
	private int n5 = 0;
	private int n6 = 0;
	private int n7 = 0;
	private int n8 = 0;
	private int n9 = 0;
	private int n10 = 0;
	private String branchname = "";
	private String branchCode = "";

	private int recordstatus = 0;

	public String getAccountNumber() {
		return accountNumber;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public String getBranchname() {
		return branchname;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public String getCreatedUserID() {
		return createdUserID;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public String getModifiedUserID() {
		return modifiedUserID;
	}

	public int getN1() {
		return n1;
	}

	public int getN10() {
		return n10;
	}

	public int getN2() {
		return n2;
	}

	public int getN3() {
		return n3;
	}

	public int getN4() {
		return n4;
	}

	public int getN5() {
		return n5;
	}

	public int getN6() {
		return n6;
	}

	public int getN7() {
		return n7;
	}

	public int getN8() {
		return n8;
	}

	public int getN9() {
		return n9;
	}

	public int getRecordstatus() {
		return recordstatus;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getT1() {
		return t1;
	}

	public String getT10() {
		return t10;
	}

	public String getT2() {
		return t2;
	}

	public String getT3() {
		return t3;
	}

	public String getT4() {
		return t4;
	}

	public String getT5() {
		return t5;
	}

	public String getT6() {
		return t6;
	}

	public String getT7() {
		return t7;
	}

	public String getT8() {
		return t8;
	}

	public String getT9() {
		return t9;
	}

	public String getUserName() {
		return userName;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public void setBranchname(String branchname) {
		this.branchname = branchname;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public void setCreatedUserID(String createdUserID) {
		this.createdUserID = createdUserID;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public void setModifiedUserID(String modifiedUserID) {
		this.modifiedUserID = modifiedUserID;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public void setN10(int n10) {
		this.n10 = n10;
	}

	public void setN2(int n2) {
		this.n2 = n2;
	}

	public void setN3(int n3) {
		this.n3 = n3;
	}

	public void setN4(int n4) {
		this.n4 = n4;
	}

	public void setN5(int n5) {
		this.n5 = n5;
	}

	public void setN6(int n6) {
		this.n6 = n6;
	}

	public void setN7(int n7) {
		this.n7 = n7;
	}

	public void setN8(int n8) {
		this.n8 = n8;
	}

	public void setN9(int n9) {
		this.n9 = n9;
	}

	public void setRecordstatus(int recordstatus) {
		this.recordstatus = recordstatus;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT10(String t10) {
		this.t10 = t10;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public void setT4(String t4) {
		this.t4 = t4;
	}

	public void setT5(String t5) {
		this.t5 = t5;
	}

	public void setT6(String t6) {
		this.t6 = t6;
	}

	public void setT7(String t7) {
		this.t7 = t7;
	}

	public void setT8(String t8) {
		this.t8 = t8;
	}

	public void setT9(String t9) {
		this.t9 = t9;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}