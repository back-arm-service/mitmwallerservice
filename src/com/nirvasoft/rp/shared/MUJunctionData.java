package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MUJunctionData {

	private long syskey;
	private String merchantid;
	private long usersyskey;

	public MUJunctionData() {
		clearProperty();
	}

	void clearProperty() {
		syskey = 0;
		merchantid = "";
		usersyskey = 0;
	}

	public String getMerchantid() {
		return merchantid;
	}

	public long getSyskey() {
		return syskey;
	}

	public long getUsersyskey() {
		return usersyskey;
	}

	public void setMerchantid(String merchantid) {
		this.merchantid = merchantid;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setUsersyskey(long usersyskey) {
		this.usersyskey = usersyskey;
	}

}
