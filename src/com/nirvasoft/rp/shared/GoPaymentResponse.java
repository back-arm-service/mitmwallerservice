package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class GoPaymentResponse {

	private String code;
	private String desc;
	private String bankRefNumber;
	private String bankTaxRefNumber;
	private String transactionDate;
	private String cardExpire;
	private String bankCharges;
	private String otherResponseCode;
	private String otherResponseDesc;
	private String effectiveDate;
	private String otherStatus;
	private String penalty;
	private String actualDate;//ndh

	public GoPaymentResponse() {
		clearProperty();
	}

	private void clearProperty() {
		code = "";
		desc = "";
		bankRefNumber = "";
		bankTaxRefNumber = "";
		transactionDate = "";
		cardExpire = "";
		bankCharges = "";
		otherResponseCode = "";
		otherResponseDesc = "";
		effectiveDate = "";
		otherStatus ="";
		penalty ="";
		actualDate="";
	}

	public String getPenalty() {
		return penalty;
	}

	public void setPenalty(String penalty) {
		this.penalty = penalty;
	}

	public String getOtherStatus() {
		return otherStatus;
	}

	public void setOtherStatus(String otherStatus) {
		this.otherStatus = otherStatus;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getBankCharges() {
		return bankCharges;
	}

	public void setBankCharges(String bankCharges) {
		this.bankCharges = bankCharges;
	}

	public String getOtherResponseCode() {
		return otherResponseCode;
	}

	public void setOtherResponseCode(String otherResponseCode) {
		this.otherResponseCode = otherResponseCode;
	}

	public String getOtherResponseDesc() {
		return otherResponseDesc;
	}

	public void setOtherResponseDesc(String otherResponseDesc) {
		this.otherResponseDesc = otherResponseDesc;
	}

	public String getBankRefNumber() {
		return bankRefNumber;
	}

	public String getBankTaxRefNumber() {
		return bankTaxRefNumber;
	}

	public String getCardExpire() {
		return cardExpire;
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setBankRefNumber(String bankRefNumber) {
		this.bankRefNumber = bankRefNumber;
	}

	public void setBankTaxRefNumber(String bankTaxRefNumber) {
		this.bankTaxRefNumber = bankTaxRefNumber;
	}

	public void setCardExpire(String cardExpire) {
		this.cardExpire = cardExpire;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getActualDate() {
		return actualDate;
	}

	public void setActualDate(String actualDate) {
		this.actualDate = actualDate;
	}

	@Override
	public String toString() {
		return "{code=" + code + ", desc=" + desc + ", bankRefNumber=" + bankRefNumber + ", bankTaxRefNumber="
				+ bankTaxRefNumber + ", transactionDate=" + transactionDate + ", cardExpire=" + cardExpire + "}";
	}

}
