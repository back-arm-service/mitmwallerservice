package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ATMLocationResult {
	private String code;
	private String desc;
	private ATMLocationData[] data = null;

	private String id;

	public ATMLocationResult() {
		clearProperty();
	}

	private void clearProperty() {
		code = "";
		desc = "";
		id = "";
	}

	public String getCode() {
		return code;
	}

	public ATMLocationData[] getData() {
		return data;
	}

	public String getDesc() {
		return desc;
	}

	public String getId() {
		return id;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setData(ATMLocationData[] data) {
		this.data = data;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setId(String id) {
		this.id = id;
	}

}
