package com.nirvasoft.rp.shared;

import com.nirvasoft.rp.shared.icbs.Enum;

public class FunctionID {
	public static final int CashInHand = 0;
	public static final int RemitOutOnLine = 10000;
	public static final int RemitInOnLine = 10500;
	public static final int RemitOutOnlCom = 11000;
	public static final int RemitReceived = 250;
	public static final int CashPO = 2000;

	private String branchCode;
	private String currencyCode;
	private String Tenure;
	private Enum.FunctionID fIDType;
	private int poType;
	private String bankCode;
	private String pCommission;
	private int gcType = 0;
	private int hpType = 0;
	private String AccPType;
	private String glCode;
	private String caiType;
	private String saiType;

	public FunctionID() {
		clearProperty();
	}

	public FunctionID(String branchCode, String currencyCode, Enum.FunctionID fIDtType) {
		this.branchCode = branchCode;
		this.currencyCode = currencyCode;
		this.fIDType = fIDtType;
	}

	public void clearProperty() {
		branchCode = "";
		currencyCode = "";
		Tenure = "";
		poType = 0;
		bankCode = "";
		pCommission = "";
		gcType = 0;
		AccPType = "";
		glCode = "";
		caiType = "";
		saiType = "";
	}

	public String getAccPType() {
		return AccPType;
	}

	public String getBankCode() {
		return bankCode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public String getCAIType() {
		return caiType;
	}

	public String getCommission() {
		return pCommission;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public String getFunctionID() {
		String ret = "";
		if (fIDType == Enum.FunctionID.IBT) {
			ret = "IBT" + currencyCode + branchCode + AccPType;
		} else if (fIDType == Enum.FunctionID.IBTCom) {
			ret = "IBTCOM" + pCommission + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.POC) {
			ret = "POC" + poType + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.PO) {
			ret = "PO" + poType + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.FDI) {
			ret = "FDI" + Tenure + currencyCode;
		} else if (fIDType == Enum.FunctionID.FDA) {
			ret = "FDA" + Tenure + currencyCode;
		} else if (fIDType == Enum.FunctionID.FDS) {
			ret = "FDS" + Tenure + currencyCode;
		} else if (fIDType == Enum.FunctionID.OBR) {
			ret = "OBR" + currencyCode + bankCode;
		} else if (fIDType == Enum.FunctionID.OBRComm) {
			ret = "OBR" + currencyCode + pCommission + bankCode;
		} else if (fIDType == Enum.FunctionID.ODI) {
			ret = "ODI" + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.ODS) {
			ret = "ODS" + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.ODP) {
			ret = "ODP" + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.ADJ) {
			ret = "ADJ" + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.GCComm) {
			ret = "GCC" + currencyCode + poType;
		} else if (fIDType == Enum.FunctionID.GC) {
			ret = "GC" + currencyCode + poType;
		} else if (fIDType == Enum.FunctionID.CYP) {
			ret = "CYP" + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.CYPEQU) {
			ret = "CYPEQU" + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.RMT) {
			ret = "RMT" + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.HP) {
			ret = "HP" + currencyCode + branchCode + hpType;
		} else if (fIDType == Enum.FunctionID.HPINT) {
			ret = "HPINT" + currencyCode + branchCode + hpType;
		} else if (fIDType == Enum.FunctionID.HPComm) {
			ret = "HPComm" + currencyCode + branchCode + hpType;
		} else if (fIDType == Enum.FunctionID.HPMERLOC) {
			ret = "HPMERLOC" + currencyCode + branchCode + hpType;
		} else if (fIDType == Enum.FunctionID.HPPEN) {
			ret = "HPPEN" + currencyCode + branchCode + hpType;
		} else if (fIDType == Enum.FunctionID.HPMER) {
			ret = "HPMER" + currencyCode + branchCode + hpType;
		} else if (fIDType == Enum.FunctionID.HPSVC) {
			ret = "HPSVC" + currencyCode + branchCode + hpType;
		} else if (fIDType == Enum.FunctionID.LAI) {
			ret = "LAI" + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.LAS) {
			ret = "LAS" + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.LAP) {
			ret = "LAP" + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.ATMDEP) {
			ret = "ATMDEP" + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.POR) {
			ret = "POR" + currencyCode + poType + bankCode;
		} else if (fIDType == Enum.FunctionID.IBS) {
			ret = "IBS" + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.IBTCom) {
			ret = "IBTCOM" + pCommission + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.IBTCom3) {
			ret = "IBTCOM" + pCommission + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.CAI) {
			ret = "CAI" + caiType + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.CAA) {
			ret = "CAA" + caiType + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.ODI) {
			ret = "ODI" + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.ODS) {
			ret = "ODS" + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.ODC) {
			ret = "ODC" + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.ODP) {
			ret = "ODP" + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.ODNPL) {
			ret = "CLN" + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.CCI) {
			ret = "CCI" + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.CCS) {
			ret = "CCS" + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.CCC) {
			ret = "CCC" + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.CCP) {
			ret = "CCP" + currencyCode + branchCode;
		} else if (fIDType == Enum.FunctionID.MERCOM) {
			ret = "MERCOM" + currencyCode + branchCode;

		}
		return ret;
	}

	public int getGcType() {
		return gcType;
	}

	public String getGlCode() {
		return glCode;
	}

	public int getHPType() {
		return hpType;
	}

	public int getPoType() {
		return poType;
	}

	public String getSAIType() {
		return saiType;
	}

	public String getTenure() {
		return Tenure;
	}

	public Enum.FunctionID getType() {
		return fIDType;
	}

	public void setAccPType(String accPType) {
		AccPType = accPType;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public void setBranchCode(String p) {
		branchCode = p;
	}

	public void setCAIType(String caiType) {
		this.caiType = caiType;
	}

	public void setCommission(String pCommission) {
		this.pCommission = pCommission;
	}

	public void setCurrencyCode(String p) {
		currencyCode = p;
	}

	public void setGcType(int p) {
		gcType = p;
	}

	public void setGlCode(String glCode) {
		this.glCode = glCode;
	}

	public void setHPType(int p) {
		hpType = p;
	}

	public void setPoType(int p) {
		poType = p;
	}

	public void setSAIType(String saiType) {
		this.saiType = saiType;
	}

	public void setTenure(String tenure) {
		Tenure = tenure;
	}

	public void setType(Enum.FunctionID p) {
		fIDType = p;
	}

}
