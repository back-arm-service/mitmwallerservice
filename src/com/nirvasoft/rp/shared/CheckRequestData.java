package com.nirvasoft.rp.shared;

import java.io.Serializable;

public class CheckRequestData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2639246991151646372L;

	private String pRefNo = "";
	private String pAccNumber = "";
	private String pRemark = "";
	private String pRequestDate = "";
	private String pIssuedDate = "";
	private String pApprovedDate = "";
	private String pStartNo = "";
	private String pEndNo = "";
	private String pT9 = "";
	private String pT10 = "";

	private int pStatus = 0;
	private int pNoOfCheckBook = 0;
	private int pN3 = 0;
	private int pN4 = 0;
	private int pN5 = 0;

	private String pStatusInWord = "";

	public CheckRequestData() {
		clearProperty();
	}

	private void clearProperty() {
		pRefNo = "";
		pAccNumber = "";
		pRemark = "";
		pRequestDate = "";
		pIssuedDate = "";
		pApprovedDate = "";
		pStartNo = "";
		pEndNo = "";
		pT9 = "";
		pT10 = "";

		pStatus = 0;
		pNoOfCheckBook = 0;
		pN3 = 0;
		pN4 = 0;
		pN5 = 0;

		pStatusInWord = "";
	}

	public String getAccNumber() {
		return pAccNumber;
	}

	public String getApprovedDate() {
		return pApprovedDate;
	}

	public String getEndNo() {
		return pEndNo;
	}

	public String getIssuedDate() {
		return pIssuedDate;
	}

	public int getN3() {
		return pN3;
	}

	public int getN4() {
		return pN4;
	}

	public int getN5() {
		return pN5;
	}

	public int getNoOfCheckBook() {
		return pNoOfCheckBook;
	}

	public String getRefNo() {
		return pRefNo;
	}

	public String getRemark() {
		return pRemark;
	}

	public String getRequestDate() {
		return pRequestDate;
	}

	public String getStartNo() {
		return pStartNo;
	}

	public int getStatus() {
		return pStatus;
	}

	public String getStatusInWord() {
		return pStatusInWord;
	}

	public String getT10() {
		return pT10;
	}

	public String getT9() {
		return pT9;
	}

	public void setAccNumber(String pAccNumber) {
		this.pAccNumber = pAccNumber;
	}

	public void setApprovedDate(String pApprovedDate) {
		this.pApprovedDate = pApprovedDate;
	}

	public void setEndNo(String pEndNo) {
		this.pEndNo = pEndNo;
	}

	public void setIssuedDate(String pIssuedDate) {
		this.pIssuedDate = pIssuedDate;
	}

	public void setN3(int pN3) {
		this.pN3 = pN3;
	}

	public void setN4(int pN4) {
		this.pN4 = pN4;
	}

	public void setN5(int pN5) {
		this.pN5 = pN5;
	}

	public void setNoOfCheckBook(int pN2) {
		this.pNoOfCheckBook = pN2;
	}

	public void setRefNo(String pRefNo) {
		this.pRefNo = pRefNo;
	}

	public void setRemark(String pRemark) {
		this.pRemark = pRemark;
	}

	public void setRequestDate(String pRequestDate) {
		this.pRequestDate = pRequestDate;
	}

	public void setStartNo(String pStartNo) {
		this.pStartNo = pStartNo;
	}

	public void setStatus(int pStatus) {
		this.pStatus = pStatus;
	}

	public void setStatusInWord(String pStatusInWord) {
		this.pStatusInWord = pStatusInWord;
	}

	public void setT10(String pT10) {
		this.pT10 = pT10;
	}

	public void setT9(String pT9) {
		this.pT9 = pT9;
	}
}
