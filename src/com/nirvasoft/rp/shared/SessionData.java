package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SessionData {
	private String userID;
	private String sessionID;
	private String loginID;// atn for resetpassword
	private String lovDesc;
	private String t1;
	private String t2;

	public SessionData() {
		clearProperty();
	}

	void clearProperty() {
		userID = "";
		sessionID = "";
		lovDesc="";
		t1 = "";
		t2 = "";
	}

	public String getT1() {
		return t1;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public String getT2() {
		return t2;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public String getLovDesc() {
		return lovDesc;
	}

	public void setLovDesc(String lovDesc) {
		this.lovDesc = lovDesc;
	}

	public String getLoginID() {
		return loginID;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getUserID() {
		return userID;
	}

	public void setLoginID(String loginID) {
		this.loginID = loginID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setUserID(String userid) {
		this.userID = userid;
	}

	@Override
	public String toString() {
		return "{userID=" + userID + ", sessionID=" + sessionID + "}";
	}

}
