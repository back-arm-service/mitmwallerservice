package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserDetailReportData {
	private long syskey;
	private String userID;
	private String username;
	private String cif;
	private String nrc;
	private String phoneno;
	private String createddate;
	private String modifieddate;
	private String createdby;
	private String modifiedby;
	private String userstatus;
	private int recordStatus;
	private int n7;
	private String t10;// for wallet user
	private int n1;// from wallet user

	public UserDetailReportData() {
		clearProperty();
	}

	private void clearProperty() {

		userID = "";
		username = "";
		nrc = "";
		phoneno = "";
		createdby = "";
		modifiedby = "";
		createddate = "";
		modifieddate = "";
		userstatus = "";
		n7 = 0;
		recordStatus = 0;

	}

	public String getCif() {
		return cif;
	}

	public String getCreatedby() {
		return createdby;
	}

	public String getCreateddate() {
		return createddate;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public String getModifieddate() {
		return modifieddate;
	}

	public int getN1() {
		return n1;
	}

	public int getN7() {
		return n7;
	}

	public String getNrc() {
		return nrc;
	}

	public String getPhoneno() {
		return phoneno;
	}

	public int getRecordStatus() {
		return recordStatus;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getT10() {
		return t10;
	}

	public String getUserID() {
		return userID;
	}

	public String getUsername() {
		return username;
	}

	public String getUserstatus() {
		return userstatus;
	}

	public void setCif(String cif) {
		this.cif = cif;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	public void setModifieddate(String modifieddate) {
		this.modifieddate = modifieddate;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public void setN7(int n7) {
		this.n7 = n7;
	}

	public void setNrc(String nrc) {
		this.nrc = nrc;
	}

	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}

	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setT10(String t10) {
		this.t10 = t10;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setUserstatus(String userstatus) {
		this.userstatus = userstatus;
	}

}
