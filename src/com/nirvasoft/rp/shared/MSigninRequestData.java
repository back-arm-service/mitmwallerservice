package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MSigninRequestData {
	private String userID;
	private String password;
	private String deviceID;
	private String location;

	public MSigninRequestData() {
		clearProperties();
	}

	public void clearProperties() {
		this.userID = "";
		this.password = "";
		this.deviceID = "";
		this.location = "";
	}

	public String getDeviceID() {
		return deviceID;
	}

	public String getLocation() {
		return location;
	}

	public String getPassword() {
		return password;
	}

	public String getUserID() {
		return userID;
	}

	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	@Override
	public String toString() {
		return "{userID=" + userID + ", password=" + password + ", deviceID=" + deviceID + ", location=" + location
				+ "}";
	}

}
