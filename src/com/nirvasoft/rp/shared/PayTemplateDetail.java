package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PayTemplateDetail {

	private long sysKey;
	private long hkey;
	private String fieldID;
	private int fieldOrder;
	private String caption;
	private String dataType;
	private String disPayField;
	private String regExpKey;
	private Long lovKey;
	private int fieldShow;
	private int IsMandatory;
	private String fieldvalue;
	private LOVSetupDetailData[] lovData;

	public PayTemplateDetail() {
		clearProperty();
	}

	private void clearProperty() {
		sysKey = 0;
		hkey = 0;
		fieldID = "";
		fieldOrder = 0;
		caption = "";
		dataType = "";
		disPayField = "";
		regExpKey = "";
		lovKey = (long) 0.0;
		fieldShow = 0;
		IsMandatory = 0;
		fieldvalue = "";
		lovData = null;

	}

	public String getCaption() {
		return caption;
	}

	public String getDataType() {
		return dataType;
	}

	public String getDisPayField() {
		return disPayField;
	}

	public String getFieldID() {
		return fieldID;
	}

	public int getFieldOrder() {
		return fieldOrder;
	}

	public int getFieldShow() {
		return fieldShow;
	}

	public String getFieldvalue() {
		return fieldvalue;
	}

	public long getHkey() {
		return hkey;
	}

	public int getIsMandatory() {
		return IsMandatory;
	}

	public LOVSetupDetailData[] getLovData() {
		return lovData;
	}

	public Long getLovKey() {
		return lovKey;
	}

	public String getRegExpKey() {
		return regExpKey;
	}

	public long getSysKey() {
		return sysKey;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public void setDisPayField(String disPayField) {
		this.disPayField = disPayField;
	}

	public void setFieldID(String fieldID) {
		this.fieldID = fieldID;
	}

	public void setFieldOrder(int fieldOrder) {
		this.fieldOrder = fieldOrder;
	}

	public void setFieldShow(int fieldShow) {
		this.fieldShow = fieldShow;
	}

	public void setFieldvalue(String fieldvalue) {
		this.fieldvalue = fieldvalue;
	}

	public void setHkey(long hkey) {
		this.hkey = hkey;
	}

	public void setIsMandatory(int isMandatory) {
		IsMandatory = isMandatory;
	}

	public void setLovData(LOVSetupDetailData[] lovData) {
		this.lovData = lovData;
	}

	public void setLovKey(Long lovKey) {
		this.lovKey = lovKey;
	}

	public void setRegExpKey(String regExpKey) {
		this.regExpKey = regExpKey;
	}

	public void setSysKey(long sysKey) {
		this.sysKey = sysKey;
	}
}
