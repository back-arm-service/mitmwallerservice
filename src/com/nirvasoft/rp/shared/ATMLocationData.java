package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ATMLocationData {
	private String branchCode;

	private String branchaddress1;
	private String branchaddress2;
	private String branchaddress3;
	private String branchname;

	private String latitude;
	private String longitude;
	private String locationType;

	private String branchPhone1;
	private String branchPhone2;
	private String servicesProvided1;
	private String servicesProvided2;
	private String servicesProvided3;
	private String workTimings1;
	private String workTimings2;
	private String workDays1;
	private String workDays2;

	public ATMLocationData() {
		clearProperty();
	}

	private void clearProperty() {
		branchCode = "";
		branchaddress1 = "";
		branchaddress2 = "";
		branchaddress3 = "";
		branchname = "";
		branchPhone1 = "";
		branchPhone2 = "";
		servicesProvided1 = "";
		servicesProvided2 = "";
		servicesProvided3 = "";
		workTimings1 = "";
		workTimings2 = "";
		workDays1 = "";
		workDays2 = "";
		latitude = "";
		longitude = "";
		locationType = "";
	}

	public String getBranchaddress1() {
		return branchaddress1;
	}

	public String getBranchaddress2() {
		return branchaddress2;
	}

	public String getBranchaddress3() {
		return branchaddress3;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public String getBranchname() {
		return branchname;
	}

	public String getBranchPhone1() {
		return branchPhone1;
	}

	public String getBranchPhone2() {
		return branchPhone2;
	}

	public String getLatitude() {
		return latitude;
	}

	public String getLocationType() {
		return locationType;
	}

	public String getLongitude() {
		return longitude;
	}

	public String getServicesProvided1() {
		return servicesProvided1;
	}

	public String getServicesProvided2() {
		return servicesProvided2;
	}

	public String getServicesProvided3() {
		return servicesProvided3;
	}

	public String getWorkDays1() {
		return workDays1;
	}

	public String getWorkDays2() {
		return workDays2;
	}

	public String getWorkTimings1() {
		return workTimings1;
	}

	public String getWorkTimings2() {
		return workTimings2;
	}

	public void setBranchaddress1(String branchaddress1) {
		this.branchaddress1 = branchaddress1;
	}

	public void setBranchaddress2(String branchaddress2) {
		this.branchaddress2 = branchaddress2;
	}

	public void setBranchaddress3(String branchaddress3) {
		this.branchaddress3 = branchaddress3;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public void setBranchname(String branchname) {
		this.branchname = branchname;
	}

	public void setBranchPhone1(String branchPhone1) {
		this.branchPhone1 = branchPhone1;
	}

	public void setBranchPhone2(String branchPhone2) {
		this.branchPhone2 = branchPhone2;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public void setServicesProvided1(String servicesProvided1) {
		this.servicesProvided1 = servicesProvided1;
	}

	public void setServicesProvided2(String servicesProvided2) {
		this.servicesProvided2 = servicesProvided2;
	}

	public void setServicesProvided3(String servicesProvided3) {
		this.servicesProvided3 = servicesProvided3;
	}

	public void setWorkDays1(String workDays1) {
		this.workDays1 = workDays1;
	}

	public void setWorkDays2(String workDays2) {
		this.workDays2 = workDays2;
	}

	public void setWorkTimings1(String workTimings1) {
		this.workTimings1 = workTimings1;
	}

	public void setWorkTimings2(String workTimings2) {
		this.workTimings2 = workTimings2;
	}

}