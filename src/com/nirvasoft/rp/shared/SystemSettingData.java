package com.nirvasoft.rp.shared;

import java.io.Serializable;

public class SystemSettingData implements Serializable {
	private static final long serialVersionUID = -1922484382959144197L;

	private long psyskey;
	private long pautokey;
	private String pcreateddate;
	private String puserid;
	private int pSyncStatus;
	private long pSyncBatch;
	private String pt1;
	private String pt2;
	private String pt3;
	private String pt4;
	private int pn1;
	private int pn2;
	private int pn3;
	private int pn4;
	private int pn5;
	private int pn6;
	private int pn7;

	public SystemSettingData() {
		clearProperty();
	}

	private void clearProperty() {
		psyskey = 0;
		pautokey = 0;
		pcreateddate = "";
		puserid = "";
		pSyncStatus = 0;
		pSyncBatch = 0;
		pt1 = "";
		pt2 = "";
		pt3 = "";
		pt4 = "";
		pn1 = 0;
		pn2 = 0;
		pn3 = 0;
		pn4 = 0;
		pn5 = 0;
		pn6 = 0;
		pn7 = 0;
	}

	public long getautokey() {
		return pautokey;
	}

	public String getCreatedDate() {
		return pcreateddate;
	}

	public int getN1() {
		return pn1;
	}

	public int getN2() {
		return pn2;
	}

	public int getN3() {
		return pn3;
	}

	public int getN4() {
		return pn4;
	}

	public int getN5() {
		return pn5;
	}

	public int getN6() {
		return pn6;
	}

	public int getN7() {
		return pn7;
	}

	public long getSyncBatch() {
		return pSyncBatch;
	}

	public int getSyncStatus() {
		return pSyncStatus;
	}

	public long getsyskey() {
		return psyskey;
	}

	public String getT1() {
		return pt1;
	}

	public String getT2() {
		return pt2;
	}

	public String getT3() {
		return pt3;
	}

	public String getT4() {
		return pt4;
	}

	public String getUserId() {
		return puserid;
	}

	public void setautokey(long p) {
		pautokey = p;
	}

	public void setCreatedDate(String p) {
		pcreateddate = p;
	}

	public void setN1(int p) {
		pn1 = p;
	}

	public void setN2(int p) {
		pn2 = p;
	}

	public void setN3(int p) {
		pn3 = p;
	}

	public void setN4(int p) {
		pn4 = p;
	}

	public void setN5(int p) {
		pn5 = p;
	}

	public void setN6(int p) {
		pn6 = p;
	}

	public void setN7(int p) {
		pn7 = p;
	}

	public void setSyncBatch(long p) {
		pSyncBatch = p;
	}

	public void setSyncStatus(int p) {
		pSyncStatus = p;
	}

	public void setsyskey(long p) {
		psyskey = p;
	}

	public void setT1(String p) {
		pt1 = p;
	}

	public void setT2(String p) {
		pt2 = p;
	}

	public void setT3(String p) {
		pt3 = p;
	}

	public void setT4(String p) {
		pt4 = p;
	}

	public void setUserId(String p) {
		puserid = p;
	}

}
