package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ReferenceListData {
	private String name;
	private String value;
	private String price;
	private String bankCharges;
	private String merchantCharges;

	public ReferenceListData() {
		name = "";
		value = "";
		price = "";
		bankCharges = "";
		merchantCharges = "";
	}

	public String getBankCharges() {
		return bankCharges;
	}

	public String getMerchantCharges() {
		return merchantCharges;
	}

	public String getName() {
		return name;
	}

	public String getPrice() {
		return price;
	}

	public String getValue() {
		return value;
	}

	public void setBankCharges(String bankCharges) {
		this.bankCharges = bankCharges;
	}

	public void setMerchantCharges(String merchantCharges) {
		this.merchantCharges = merchantCharges;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "{name=" + name + ", value=" + value + ", price=" + price + ", bankCharges=" + bankCharges
				+ ", merchantCharges=" + merchantCharges + "}";
	}
}
