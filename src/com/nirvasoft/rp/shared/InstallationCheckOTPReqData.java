package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class InstallationCheckOTPReqData {
	private String phoneno;
	private String otpcode;

	public InstallationCheckOTPReqData() {
		clearProperty();
	}

	private void clearProperty() {
		this.phoneno = "";
		this.otpcode = "";
	}

	public String getOtpcode() {
		return otpcode;
	}

	public String getPhoneno() {
		return phoneno;
	}

	public void setOtpcode(String otpcode) {
		this.otpcode = otpcode;
	}

	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}

	@Override
	public String toString() {
		return "{phoneno=" + phoneno + ", otpcode=" + otpcode + "}";
	}

}
