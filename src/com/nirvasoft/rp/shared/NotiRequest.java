package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class NotiRequest {

	private String title;
	private String description;

	public NotiRequest() {
		clearProperty();
	}

	private void clearProperty() {
		title = "";
		description = "";
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "NotiRequest [title=" + title + ", description=" + description + "]";
	}

}
