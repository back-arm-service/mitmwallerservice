package com.nirvasoft.rp.shared.integration;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MessageData {
	private long syskey;
	private String createdDate;
	private String modifiedDate;
	private String userId;
	private String userName;
	private int recordStatus;
	private String toAddress;
	private String message;
	private String dateReceived;
	private String dateSent;
	private String retriesCount;
	private String serviceCode;
	private String status;
	private String responseMessage;
	private String deliveryStatus;
	private String t1;
	private String t2;
	private String t3;
	private String t4;
	private String t5;
	private int n1;
	private int n2;
	private int n3;
	private int n4;
	private int n5;

	public MessageData() {
		clearProperties();
	}

	private void clearProperties() {
		this.syskey = 0L;
		this.createdDate = "";
		this.modifiedDate = "";
		this.userId = "";
		this.userName = "";
		this.recordStatus = 0;
		this.toAddress = "";
		this.message = "";
		this.dateReceived = "";
		this.dateSent = "";
		this.retriesCount = "0";
		this.serviceCode = "";
		this.status = "0";
		this.responseMessage = "";
		this.deliveryStatus = "0";
		this.t1 = "";
		this.t2 = "";
		this.t3 = "";
		this.t4 = "";
		this.t5 = "";
		this.n1 = 0;
		this.n2 = 0;
		this.n3 = 0;
		this.n4 = 0;
		this.n5 = 0;

	}

	public String getCreatedDate() {
		return createdDate;
	}

	public String getDateReceived() {
		return dateReceived;
	}

	public String getDateSent() {
		return dateSent;
	}

	public String getDeliveryStatus() {
		return deliveryStatus;
	}

	public String getMessage() {
		return message;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public int getN1() {
		return n1;
	}

	public int getN2() {
		return n2;
	}

	public int getN3() {
		return n3;
	}

	public int getN4() {
		return n4;
	}

	public int getN5() {
		return n5;
	}

	public int getRecordStatus() {
		return recordStatus;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public String getRetriesCount() {
		return retriesCount;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public String getStatus() {
		return status;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getT1() {
		return t1;
	}

	public String getT2() {
		return t2;
	}

	public String getT3() {
		return t3;
	}

	public String getT4() {
		return t4;
	}

	public String getT5() {
		return t5;
	}

	public String getToAddress() {
		return toAddress;
	}

	public String getUserId() {
		return userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public void setDateReceived(String dateReceived) {
		this.dateReceived = dateReceived;
	}

	public void setDateSent(String dateSent) {
		this.dateSent = dateSent;
	}

	public void setDeliveryStatus(String deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public void setN2(int n2) {
		this.n2 = n2;
	}

	public void setN3(int n3) {
		this.n3 = n3;
	}

	public void setN4(int n4) {
		this.n4 = n4;
	}

	public void setN5(int n5) {
		this.n5 = n5;
	}

	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public void setRetriesCount(String retriesCount) {
		this.retriesCount = retriesCount;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public void setT4(String t4) {
		this.t4 = t4;
	}

	public void setT5(String t5) {
		this.t5 = t5;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
