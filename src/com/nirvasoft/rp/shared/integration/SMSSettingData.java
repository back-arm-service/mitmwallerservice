package com.nirvasoft.rp.shared.integration;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SMSSettingData {
	// "code": "", "desc": "", "id": 0, "syskey": 0, "serviceCode": "",
	// "serviceDesc": "",
	// "funCode": "", "funDesc": "", "site": "", "from": "", "to": "",
	// "fromMsg": "", "toMsg": "",
	// "message": "", "merchantID": "", "operatorType": "",
	// "active": "", "sessionID": "", "userID": ""
	private long syskey;
	private long id;
	private String serviceCode;
	private String serviceDesc;
	private String funCode;
	private String funDesc;
	private String from;
	private String to;
	private String fromMsg;
	private String toMsg;
	private String message;
	private String site;
	private String merchantID;
	private String operatorType;
	private String active;
	private String both;
	private String language;
	private String languageDesc;
	private String sessionID;
	private String userID;
	private String t1;
	private String t2;
	private String t3;
	private String t4;
	private String t5;
	private int n1;
	private int n2;
	private int n3;
	private int n4;
	private int n5;
	private String code;
	private String desc;

	public SMSSettingData() {
		clearProperty();
	}

	private void clearProperty() {
		syskey = 0L;
		code = "";
		desc = "";
		serviceCode = "";
		serviceDesc = "";
		funCode = "";
		funDesc = "";
		from = "";
		to = "";
		fromMsg = "";
		toMsg = "";
		merchantID = "";
		operatorType = "";
		active = "";
		both="";
		language="";
		languageDesc="";
		userID = "";
		sessionID = "";

	}
	public String getBoth() {
		return both;
	}

	public void setBoth(String both) {
		this.both = both;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getLanguageDesc() {
		return languageDesc;
	}

	public void setLanguageDesc(String languageDesc) {
		this.languageDesc = languageDesc;
	}

	public String getActive() {
		return active;
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public String getFrom() {
		return from;
	}

	public String getFromMsg() {
		return fromMsg;
	}

	public String getFunCode() {
		return funCode;
	}

	public String getFunDesc() {
		return funDesc;
	}

	public long getId() {
		return id;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public String getMessage() {
		return message;
	}

	public int getN1() {
		return n1;
	}

	public int getN2() {
		return n2;
	}

	public int getN3() {
		return n3;
	}

	public int getN4() {
		return n4;
	}

	public int getN5() {
		return n5;
	}

	public String getOperatorType() {
		return operatorType;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public String getServiceDesc() {
		return serviceDesc;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getSite() {
		return site;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getT1() {
		return t1;
	}

	public String getT2() {
		return t2;
	}

	public String getT3() {
		return t3;
	}

	public String getT4() {
		return t4;
	}

	public String getT5() {
		return t5;
	}

	public String getTo() {
		return to;
	}

	public String getToMsg() {
		return toMsg;
	}

	public String getUserID() {
		return userID;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public void setFromMsg(String fromMsg) {
		this.fromMsg = fromMsg;
	}

	public void setFunCode(String funCode) {
		this.funCode = funCode;
	}

	public void setFunDesc(String funDesc) {
		this.funDesc = funDesc;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public void setN2(int n2) {
		this.n2 = n2;
	}

	public void setN3(int n3) {
		this.n3 = n3;
	}

	public void setN4(int n4) {
		this.n4 = n4;
	}

	public void setN5(int n5) {
		this.n5 = n5;
	}

	public void setOperatorType(String operatorType) {
		this.operatorType = operatorType;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public void setServiceDesc(String serviceDesc) {
		this.serviceDesc = serviceDesc;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public void setT4(String t4) {
		this.t4 = t4;
	}

	public void setT5(String t5) {
		this.t5 = t5;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public void setToMsg(String toMsg) {
		this.toMsg = toMsg;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

}
