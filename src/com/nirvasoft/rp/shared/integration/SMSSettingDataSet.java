package com.nirvasoft.rp.shared.integration;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SMSSettingDataSet {
	private String msgCode;
	private String msgDesc;
	private SMSSettingData[] data = null;

	private String searchText;
	private int totalCount;
	private int currentPage;
	private int pageSize;

	public SMSSettingDataSet() {
		clearProperties();
	}

	private void clearProperties() {
		this.msgCode = "";
		this.msgDesc = "";
		this.data = null;
		searchText = "";
		totalCount = 0;
		currentPage = 0;
		pageSize = 0;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public SMSSettingData[] getData() {
		return data;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public int getPageSize() {
		return pageSize;
	}

	public String getSearchText() {
		return searchText;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setData(SMSSettingData[] data) {
		this.data = data;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	@Override
	public String toString() {
		return "SMSSettingDataset [msgCode=" + msgCode + ", msgDesc=" + msgDesc + ", data=" + Arrays.toString(data)
				+ ", searchText=" + searchText + ", totalCount=" + totalCount + ", currentPage=" + currentPage
				+ ", pageSize=" + pageSize + "]";
	}

}
