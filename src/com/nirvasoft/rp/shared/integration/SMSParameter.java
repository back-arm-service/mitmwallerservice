package com.nirvasoft.rp.shared.integration;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SMSParameter {
	private String transRef;
	private String amount;
	private String commAmount;
	private String fromAcc;
	private String toAcc;
	private String fromPhone;
	private String toPhone;
	private String fromName;
	private String toName;
	private String date;
	private String otp;
	private String password;
	private String status;
	private String loginID;

	public SMSParameter() {
		clearProperty();
	}

	private void clearProperty() {
		transRef = "";
		amount = "";
		commAmount = "";
		fromAcc = "";
		toAcc = "";
		fromPhone = "";
		toPhone = "";
		fromName = "";
		toName = "";
		date = "";
		otp = "";
		password = "";
		status = "";
		loginID = "";
	}

	public String getAmount() {
		return amount;
	}

	public String getCommAmount() {
		return commAmount;
	}

	public String getDate() {
		return date;
	}

	public String getFromAcc() {
		return fromAcc;
	}

	public String getFromName() {
		return fromName;
	}

	public String getFromPhone() {
		return fromPhone;
	}

	public String getLoginID() {
		return loginID;
	}

	public String getOtp() {
		return otp;
	}

	public String getPassword() {
		return password;
	}

	public String getStatus() {
		return status;
	}

	public String getToAcc() {
		return toAcc;
	}

	public String getToName() {
		return toName;
	}

	public String getToPhone() {
		return toPhone;
	}

	public String getTransRef() {
		return transRef;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public void setCommAmount(String commAmount) {
		this.commAmount = commAmount;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setFromAcc(String fromAcc) {
		this.fromAcc = fromAcc;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	public void setFromPhone(String fromPhone) {
		this.fromPhone = fromPhone;
	}

	public void setLoginID(String loginID) {
		this.loginID = loginID;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setToAcc(String toAcc) {
		this.toAcc = toAcc;
	}

	public void setToName(String toName) {
		this.toName = toName;
	}

	public void setToPhone(String toPhone) {
		this.toPhone = toPhone;
	}

	public void setTransRef(String transRef) {
		this.transRef = transRef;
	}

}
