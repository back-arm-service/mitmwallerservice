package com.nirvasoft.rp.shared.integration;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MerchantFeature {
	private long autoKey;
	private String merchantID;
	private String feature;
	private String accountNumber;
	private String branchCode;
	private String t1;
	private String t2;
	private int n1;
	private int n2;

	public MerchantFeature() {
		clearProperty();
	}

	private void clearProperty() {
		autoKey = 0;
		merchantID = "";
		feature = "";
		accountNumber = "";
		branchCode = "";
		t1 = "";
		t2 = "";
		n1 = 0;
		n2 = 0;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public long getAutoKey() {
		return autoKey;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public String getFeature() {
		return feature;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public int getN1() {
		return n1;
	}

	public int getN2() {
		return n2;
	}

	public String getT1() {
		return t1;
	}

	public String getT2() {
		return t2;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setAutoKey(long autoKey) {
		this.autoKey = autoKey;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public void setFeature(String feature) {
		this.feature = feature;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public void setN2(int n2) {
		this.n2 = n2;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

}
