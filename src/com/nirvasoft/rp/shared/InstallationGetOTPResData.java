package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class InstallationGetOTPResData {
	private String phoneno;
	private String code;
	private String desc;

	public InstallationGetOTPResData() {
		clearProperties();
	}

	private void clearProperties() {
		this.phoneno = "";
		this.code = "";
		this.desc = "";
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public String getPhoneno() {
		return phoneno;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}

	@Override
	public String toString() {
		return "{phoneno=" + phoneno + ", code=" + code + ", desc=" + desc + "}";
	}

}
