package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class InstallationCheckOTPResData {
	private String phoneno;
	private String lasttimelogin;
	private String code;
	private String desc;

	public InstallationCheckOTPResData() {
		clearProperties();
	}

	private void clearProperties() {
		this.phoneno = "";
		this.lasttimelogin = "";
		this.code = "";
		this.desc = "";
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public String getLasttimelogin() {
		return lasttimelogin;
	}

	public String getPhoneno() {
		return phoneno;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setLasttimelogin(String lasttimelogin) {
		this.lasttimelogin = lasttimelogin;
	}

	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}

	@Override
	public String toString() {
		return "{phoneno=" + phoneno + ", lasttimelogin=" + lasttimelogin + ", code=" + code + ", desc=" + desc + "}";
	}

}
