package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TransferResData {

	private String transID;
	private String refKey;
	private String code;
	private String desc;
	private String t1;// To Name
	private String t2;
	private String t3;

	public TransferResData() {
		clearproperty();
	}

	void clearproperty() {
		transID = "";
		refKey = "";
		code = "";
		desc = "";
		t1 = "";
		t2 = "";
		t3 = "";
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public String getRefKey() {
		return refKey;
	}

	public String getT1() {
		return t1;
	}

	public String getT2() {
		return t2;
	}

	public String getT3() {
		return t3;
	}

	public String getTransID() {
		return transID;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setRefKey(String refKey) {
		this.refKey = refKey;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public void setTransID(String transID) {
		this.transID = transID;
	}

}
