package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TransferInResData {

	private String mpokey;
	private String refkey;
	private String code;
	private String desc;

	public TransferInResData() {
		clearproperty();
	}

	void clearproperty() {
		mpokey = "";
		refkey = "";
		code = "";
		desc = "";
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public String getMpokey() {
		return mpokey;
	}

	public String getRefkey() {
		return refkey;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setMpokey(String mpokey) {
		this.mpokey = mpokey;
	}

	public void setRefkey(String refkey) {
		this.refkey = refkey;
	}

}
