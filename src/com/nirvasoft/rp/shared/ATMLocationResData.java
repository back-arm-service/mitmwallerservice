package com.nirvasoft.rp.shared;

import java.util.ArrayList;
import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ATMLocationResData {
	private String code;
	private String desc;
	private ATMLocation[] data = null;
	private ArrayList<ATMLocation> dataList = null;
	private String id;
	private String locationType;
	private String locationName;

	public ATMLocationResData() {
		clearProperties();
	}

	private void clearProperties() {
		this.id = "";
		this.code = "";
		this.desc = "";
		this.data = null;
		this.locationType = "";
		this.locationName = "";
		this.dataList = null;

	}

	public String getCode() {
		return code;
	}

	public ATMLocation[] getData() {
		return data;
	}

	public ArrayList<ATMLocation> getDataList() {
		return dataList;
	}

	public String getDesc() {
		return desc;
	}

	public String getId() {
		return id;
	}

	public String getLocationName() {
		return locationName;
	}

	public String getLocationType() {
		return locationType;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setData(ATMLocation[] data) {
		this.data = data;
	}

	public void setDataList(ArrayList<ATMLocation> dataList) {
		this.dataList = dataList;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}

	@Override
	public String toString() {
		return "ATMLocationResData [code=" + code + ", desc=" + desc + ", data=" + Arrays.toString(data) + ", dataList="
				+ dataList + ", id=" + id + ", locationType=" + locationType + ", locationName=" + locationName + "]";
	}

}
