package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CMSMerchantAccJunctionArr {
	private String merchantID;
	private CMSMerchantAccRefData[] data;
	private String messagecode;
	private String messagedesc;

	private String searchText;
	private int totalCount;
	private int currentPage;
	private int pageSize;

	public CMSMerchantAccJunctionArr() {
		clearProperty();
	}

	private void clearProperty() {
		messagecode = "";
		messagedesc = "";
		data = null;
		merchantID = "";
		searchText = "";
		totalCount = 0;
		currentPage = 0;
		pageSize = 0;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public CMSMerchantAccRefData[] getData() {
		return data;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public String getMessagecode() {
		return messagecode;
	}

	public String getMessagedesc() {
		return messagedesc;
	}

	public int getPageSize() {
		return pageSize;
	}

	public String getSearchText() {
		return searchText;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setData(CMSMerchantAccRefData[] data) {
		this.data = data;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public void setMessagecode(String messagecode) {
		this.messagecode = messagecode;
	}

	public void setMessagedesc(String messagedesc) {
		this.messagedesc = messagedesc;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

}
