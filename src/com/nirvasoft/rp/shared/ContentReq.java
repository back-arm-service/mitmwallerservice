package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ContentReq {
	private String type;
	private String syskey;

	public ContentReq() {
		clearProperty();
	}

	private void clearProperty() {
		this.type = "";
		this.syskey = "";
	}

	public String getSyskey() {
		return syskey;
	}

	public String getType() {
		return type;
	}

	public void setSyskey(String syskey) {
		this.syskey = syskey;
	}

	public void setType(String type) {
		this.type = type;
	}

}
