package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PayTemplateHeader {

	private long sysKey;
	private String merchantID;
	private String name;
	private String processingCode;
	private String proNotiCode;// for Notification
	private String proDisCode;// for Distributor use/ unuse

	// private String logo;
	private String detailOrder;

	private PayTemplateDetail[] data;
	private LOVSetupDetailData[] detailLov;
	private DisPaymentTransDetail[] detailData;

	public PayTemplateHeader() {
		clearProperty();
	}

	private void clearProperty() {
		sysKey = 0;
		merchantID = "";
		name = "";
		processingCode = "";
		proNotiCode = "";
		proDisCode = "";
		data = null;
		detailOrder = "";
		detailLov = null;
		detailData = null;
	}

	public PayTemplateDetail[] getData() {
		return data;
	}

	public DisPaymentTransDetail[] getDetailData() {
		return detailData;
	}

	public LOVSetupDetailData[] getDetailLov() {
		return detailLov;
	}

	public String getDetailOrder() {
		return detailOrder;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public String getName() {
		return name;
	}

	public String getProcessingCode() {
		return processingCode;
	}

	public String getProDisCode() {
		return proDisCode;
	}

	/*
	 * public String getLogo() { return logo; }
	 * 
	 * 
	 * 
	 * public void setLogo(String logo) { this.logo = logo; }
	 */

	public String getProNotiCode() {
		return proNotiCode;
	}

	public long getSysKey() {
		return sysKey;
	}

	public void setData(PayTemplateDetail[] data) {
		this.data = data;
	}

	public void setDetailData(DisPaymentTransDetail[] detailData) {
		this.detailData = detailData;
	}

	public void setDetailLov(LOVSetupDetailData[] detailLov) {
		this.detailLov = detailLov;
	}

	public void setDetailOrder(String detailOrder) {
		this.detailOrder = detailOrder;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}

	public void setProDisCode(String procDisCode) {
		this.proDisCode = procDisCode;
	}

	public void setProNotiCode(String proNotiCode) {
		this.proNotiCode = proNotiCode;
	}

	public void setSysKey(long sysKey) {
		this.sysKey = sysKey;
	}

}
