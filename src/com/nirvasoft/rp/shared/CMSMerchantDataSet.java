package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CMSMerchantDataSet {
	private CMSMerchantData[] data;
	private String searchText;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private String msgCode;
	private String msgDesc;
	public CMSMerchantDataSet() {
		clearProperty();
	}

	private void clearProperty() {
		searchText = "";
		totalCount = 0;
		currentPage = 0;
		pageSize = 0;
		msgCode="";
		msgDesc="";
	}

	public String getMsgCode() {
		return msgCode;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public CMSMerchantData[] getData() {
		return data;
	}

	public int getPageSize() {
		return pageSize;
	}

	public String getSearchText() {
		return searchText;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setData(CMSMerchantData[] data) {
		this.data = data;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

}
