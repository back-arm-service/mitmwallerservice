package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class NotiListRequest {

	private String id;
	private String userID;
	private String sessionID;

	public NotiListRequest() {
		clearProperty();
	}

	private void clearProperty() {
		this.id = "";
		this.userID = "";
		this.sessionID = "";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	@Override
	public String toString() {
		return "NotiRequest [id=" + id + ", userID=" + userID + ", sessionID=" + sessionID + "]";
	}

}
