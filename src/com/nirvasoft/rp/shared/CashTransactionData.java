package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CashTransactionData {

	private long id;
	private String mobileuserid;
	private double denoamount;
	private int drcr;
	private int trtype;
	private String trDate;
	private String trdatetime;
	private String t1;
	private String t2;
	private String t3;
	private String t4;
	private String t5;
	private String t6;
	private String t7;
	private String t8;
	private String t9;
	private String t10;
	private int n1;
	private int n2;
	private int n3;
	private int n4;
	private int n5;
	private float n6;
	private float n7;
	private float n8;
	private float n9;
	private float n10;

	private double debitAmount;
	private double creditAmount;
	private int totalSize;
	private String code;
	private String desc;

	public CashTransactionData() {
		denoamount = 0;
		drcr = 0;
		trtype = 0;
		trDate = "";
		trdatetime = "";
		id = 0;
		mobileuserid = "";
		n1 = 0;
		n10 = 0;
		n2 = 0;
		n3 = 0;
		n4 = 0;
		n5 = 0;
		n6 = 0;
		n7 = 0;
		n8 = 0;
		n9 = 0;
		t1 = "";
		t2 = "";
		t3 = "";
		t4 = "";
		t5 = "";
		t6 = "";
		t7 = "";
		t8 = "";
		t9 = "";
		t10 = "";
		debitAmount = 0;
		creditAmount = 0;
		totalSize = 0;
		code = "";
		desc = "";
	}

	public String getCode() {
		return code;
	}

	/**
	 * @return the creditAmount
	 */
	public double getCreditAmount() {
		return creditAmount;
	}

	/**
	 * @return the debitAmount
	 */
	public double getDebitAmount() {
		return debitAmount;
	}

	public double getDenoamount() {
		return denoamount;
	}

	public String getDesc() {
		return desc;
	}

	public int getDrcr() {
		return drcr;
	}

	public long getId() {
		return id;
	}

	public String getMobileuserid() {
		return mobileuserid;
	}

	public int getN1() {
		return n1;
	}

	public float getN10() {
		return n10;
	}

	public int getN2() {
		return n2;
	}

	public int getN3() {
		return n3;
	}

	public int getN4() {
		return n4;
	}

	public int getN5() {
		return n5;
	}

	public float getN6() {
		return n6;
	}

	public float getN7() {
		return n7;
	}

	public float getN8() {
		return n8;
	}

	public float getN9() {
		return n9;
	}

	public String getT1() {
		return t1;
	}

	public String getT10() {
		return t10;
	}

	public String getT2() {
		return t2;
	}

	public String getT3() {
		return t3;
	}

	public String getT4() {
		return t4;
	}

	public String getT5() {
		return t5;
	}

	public String getT6() {
		return t6;
	}

	public String getT7() {
		return t7;
	}

	public String getT8() {
		return t8;
	}

	public String getT9() {
		return t9;
	}

	/**
	 * @return the totolSize
	 */
	public int getTotalSize() {
		return totalSize;
	}

	/**
	 * @return the trDate
	 */
	public String getTrDate() {
		return trDate;
	}

	public String getTrdatetime() {
		return trdatetime;
	}

	public int getTrtype() {
		return trtype;
	}

	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @param creditAmount
	 *            the creditAmount to set
	 */
	public void setCreditAmount(double creditAmount) {
		this.creditAmount = creditAmount;
	}

	/**
	 * @param debitAmount
	 *            the debitAmount to set
	 */
	public void setDebitAmount(double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public void setDenoamount(double denoamount) {
		this.denoamount = denoamount;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setDrcr(int drcr) {
		this.drcr = drcr;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setMobileuserid(String mobileuserid) {
		this.mobileuserid = mobileuserid;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public void setN10(float n10) {
		this.n10 = n10;
	}

	public void setN2(int n2) {
		this.n2 = n2;
	}

	public void setN3(int n3) {
		this.n3 = n3;
	}

	public void setN4(int n4) {
		this.n4 = n4;
	}

	public void setN5(int n5) {
		this.n5 = n5;
	}

	public void setN6(float n6) {
		this.n6 = n6;
	}

	public void setN7(float n7) {
		this.n7 = n7;
	}

	public void setN8(float n8) {
		this.n8 = n8;
	}

	public void setN9(float n9) {
		this.n9 = n9;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT10(String t10) {
		this.t10 = t10;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public void setT4(String t4) {
		this.t4 = t4;
	}

	public void setT5(String t5) {
		this.t5 = t5;
	}

	public void setT6(String t6) {
		this.t6 = t6;
	}

	public void setT7(String t7) {
		this.t7 = t7;
	}

	public void setT8(String t8) {
		this.t8 = t8;
	}

	public void setT9(String t9) {
		this.t9 = t9;
	}

	/**
	 * @param totolSize
	 *            the totolSize to set
	 */
	public void setTotalSize(int totolSize) {
		this.totalSize = totolSize;
	}

	/**
	 * @param trDate
	 *            the trDate to set
	 */
	public void setTrDate(String trDate) {
		this.trDate = trDate;
	}

	public void setTrdatetime(String trdate) {
		this.trdatetime = trdate;
	}

	public void setTrtype(int trtype) {
		this.trtype = trtype;
	}

}
