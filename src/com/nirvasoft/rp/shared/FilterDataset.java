package com.nirvasoft.rp.shared;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FilterDataset implements Serializable {
	private int pPageNo;
	private int pPageSize;
	private String pSearchText;

	private int pFilterSource;

	private String pUserID;
	private String pUserName;

	private String pSessionID;
	private ArrayList<FilterData> pFilterList;

	public FilterDataset() {
		ClearProperty();
	}

	private void ClearProperty() {
		pPageNo = 0;
		pPageSize = 0;

		pUserID = "";
		pUserName = "";
		pSessionID = "";

		pSearchText = "";
		pFilterSource = 0; // 0 - CommonSearch, 1 - AdvancedSearch
	}

	public ArrayList<FilterData> getFilterList() {
		return pFilterList;
	}

	public int getFilterSource() {
		return pFilterSource;
	}

	public int getPageNo() {
		return pPageNo;
	}

	public int getPageSize() {
		return pPageSize;
	}

	public String getSearchText() {
		return pSearchText;
	}

	public String getSessionID() {
		return pSessionID;
	}

	public String getUserID() {
		return pUserID;
	}

	public String getUserName() {
		return pUserName;
	}

	public void setFilterList(ArrayList<FilterData> aFilterList) {
		this.pFilterList = aFilterList;
	}

	public void setFilterSource(int p) {
		pFilterSource = p;
	}

	public void setPageNo(int p) {
		pPageNo = p;
	}

	public void setPageSize(int p) {
		pPageSize = p;
	}

	public void setSearchText(String pSearchText) {
		this.pSearchText = pSearchText;
	}

	public void setSessionID(String p) {
		pSessionID = p;
	}

	public void setUserID(String p) {
		pUserID = p;
	}

	public void setUserName(String p) {
		pUserName = p;
	}
}