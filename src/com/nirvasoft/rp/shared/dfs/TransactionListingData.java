package com.nirvasoft.rp.shared.dfs;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TransactionListingData
{
	private int pTransNo;
	private String pTerminalID;
	private int pTransRef;
	private String pUserID;
	private String pAccountNumber;
	private String pEffectiveDate;
	private String glDescription;
	private String pDescription;
	private String pChequeNo;
	private String pCurrencyCode;
	private double pAmount;
	private double pPreviousBalance;
	private int pTransactionType;
	private String pRemark;
	private int pStatus;
	private String pSubRef;
	private int pSerialNo;
	private String pTransactionTime;
	private String pSupervisorID;
	private String pBranchCode;
	private String pBranchName;
	private String pTransSide;
	private String pAccRef;
	private String pTransactionType1;
	

	public int getTransNo() { return pTransNo; }
	public void setTransNo(int p) { pTransNo = p; }
	
	public String getTerminalID() { return pTerminalID; }
	public void setTerminalID(String p) { pTerminalID = p; }
	
	public int getTransRef() { return pTransRef; }
	public void setTransRef(int p) { pTransRef = p; }
	
	public String getUserID() { return pUserID; }
	public void setUserID(String p) { pUserID = p; }
	
	public String getAccountNumber() { return pAccountNumber; }
	public void setAccountNumber(String p) { pAccountNumber = p; }
	
	public String getEffectiveDate() { return pEffectiveDate; }
	public void setEffectiveDate(String p) { pEffectiveDate = p; }
	
	public String getDescription() { return pDescription; }
	public void setDescription(String p) { pDescription = p; }
	
	
	public String getGlDescription() {
		return glDescription;
	}
	public void setGlDescription(String glDescription) {
		this.glDescription = glDescription;
	}
	public String getChequeNo() { return pChequeNo; }
	public void setChequeNo(String p) { pChequeNo = p; }
	
	public String getCurrencyCode() { return pCurrencyCode; }
	public void setCurrencyCode(String p) { pCurrencyCode = p; }
	
	public double getAmount() { return pAmount; }
	public void setAmount(double p) { pAmount = p; }
	
	public double getPreviousBalance() { return pPreviousBalance; }
	public void setPreviousBalance(double p) { pPreviousBalance = p; }
	
	public int getTransactionType() { return pTransactionType; }
	public void setTransactionType(int p) { pTransactionType = p; }
	
	public String getRemark() { return pRemark; }
	public void setRemark(String p) { pRemark = p; }
	
	public int getStatus() { return pStatus; }
	public void setStatus(int p) { pStatus = p; }
	
	public String getSubRef() { return pSubRef; }
	public void setSubRef(String p) { pSubRef = p; }
	
	public int getSerialNo() { return pSerialNo; }
	public void setSerialNo(int p) { pSerialNo = p; }
	
	public String getTransactionTime() { return pTransactionTime; }
	public void setTransactionTime(String p) { pTransactionTime = p; }	

	public String getSupervisorID() { return pSupervisorID; }
	public void setSupervisorID(String p) { pSupervisorID = p; }

	public String getBranchCode() { return pBranchCode; }
	public void setBranchCode(String p) { pBranchCode = p; }

	public String getBranchName() { return pBranchName; }
	public void setBranchName(String p) { pBranchName = p; }

	public String getTransSide() { return pTransSide; }
	public void setTransSide(String p) { pTransSide = p; }

	public String getAccRef() { return pAccRef; }
	public void setAccRef(String p) { pAccRef = p; }

	public TransactionListingData()
	{
		ClearProperty();
	}

	private void ClearProperty()
	{
		pTransNo = 0;
		pTerminalID = "";
		pTransRef = 0;
		pUserID = "";
		pAccountNumber = "";
		pEffectiveDate = "";
		pDescription = "";
		glDescription = "";
		pChequeNo = "";
		pCurrencyCode = "";
		pAmount = 0;
		pPreviousBalance = 0;
		pTransactionType = 0;
		pRemark = "";
		pStatus = 0;
		pSubRef = "";
		pSerialNo = 0;
		pTransactionTime = "";
		pSupervisorID = "";
		pBranchCode = "";
		pTransSide = "";
		pAccRef = "";
	}
	public String getTransactionType1() {
		return pTransactionType1;
	}
	public void setTransactionType1(String pTransactionType1) {
		this.pTransactionType1 = pTransactionType1;
	}
}