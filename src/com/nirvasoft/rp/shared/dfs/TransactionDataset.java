package com.nirvasoft.rp.shared.dfs;
/* 
TUN THURA THET 2011 04 21
*/

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
public class TransactionDataset implements Serializable
{
	private static final long serialVersionUID = 1576098961058252874L;
	private ArrayList<TransactionData> pDataset;
	private TransactionData[] data;
	private int pTotalCount;
	private int pCurrentPage;
	private int pPageSize;
	private String pFields;
	private String pOrderBy;
	private String pactivestatus;
	
	private boolean pDrGlcheck;
	private boolean pCrGlcheck;
	
	public ArrayList<TransactionData>getDataset(){return pDataset;}
	public void setDataset(ArrayList<TransactionData> p){pDataset=p;}

	public int getTotalCount(){return pTotalCount;}
	public void setTotalCount(int p){pTotalCount=p;}

	public int getPageSize(){return pPageSize;}
	public void setPageSize(int p){pPageSize=p;}
	
	public int getCurrentPage(){return pCurrentPage;}
	public void setCurrentPage(int p){pCurrentPage=p;}
	
	public String getFields(){return pFields;}
	public void setFields(String p){pFields=p;}
	
	public String getOrderBy(){return pOrderBy;}
	public void setOrderBy(String p){pOrderBy=p;}
	
	public String getActiveStatus(){return pactivestatus;}
	public void setActiveStatus(String p){pactivestatus=p;}
	
	public TransactionData[] getData() { return data; }
	public void setData(TransactionData[] data) { this.data = data; }
	
	public boolean getDrGlcheck() { return pDrGlcheck; }
	public void setDrGlcheck(boolean p) { this.pDrGlcheck = p; }
	
	public boolean getCrGlcheck() { return pCrGlcheck; }
	public void setCrGlcheck(boolean p) { this.pCrGlcheck = p; }
}
