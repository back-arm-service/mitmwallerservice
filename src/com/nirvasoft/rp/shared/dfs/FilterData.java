package com.nirvasoft.rp.shared.dfs;

import java.util.ArrayList;


import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FilterData
{
	private String pItemid;
	private String pCaption;
	private String pFieldname;
	private String pDatatype;
	private String pCondition;
	private String pT1;
	private String pT2;

	public String getItemid() { return pItemid; }
	public void setItemid(String p) { pItemid = p; }

	public String getCaption() { return pCaption; }
	public void setCaption(String p) { pCaption = p; }

	public String getFieldname() { return pFieldname; }
	public void setFieldname(String p) { pFieldname = p; }

	public String getDatatype() { return pDatatype; }
	public void setDatatype(String p) { pDatatype = p; }

	public String getCondition() { return pCondition; }
	public void setCondition(String p) { pCondition = p; }

	public String getT1() { return pT1; }
	public void setT1(String p) { pT1 = p; }

	public String getT2() { return pT2; }
	public void setT2(String p) { pT2 = p; }

	public FilterData()
	{
		ClearProperty();
	}

	private void ClearProperty()
	{
		pItemid = "";
		pCaption = "";
		pFieldname = "";
		pDatatype = "";
		pCondition = "";
		pT1 = "";
		pT2 = "";
	}
}