package com.nirvasoft.rp.shared.dfs;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FilterDataset implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int pPageNo;
	private int pPageSize;
	private String pSearchText;
	private String loanType;
	private int loanSubType;
	private String productType;
	private String t1;
	
	private int pFilterSource;

	private String pUserID;
	private String pUserName;
	
	private String pSessionID;
	private ArrayList<FilterData> pFilterList;
	private ArrayList<String> pKeyList;
	private String typeDesc;
	private String branchCode;
	private int allLoanSubType;
	private String subtype;
	private String loancode;
	public int getAllLoanSubType() {
		return allLoanSubType;
	}
	public void setAllLoanSubType(int allLoanSubType) {
		this.allLoanSubType = allLoanSubType;
	}
	public int getPageSize() { return pPageSize; }
	public void setPageSize(int p) { pPageSize = p; }

	public int getPageNo() { return pPageNo; }
	public void setPageNo(int p) { pPageNo = p; }

	public int getFilterSource() { return pFilterSource; }
	public void setFilterSource(int p) { pFilterSource = p; }

	public String getSessionID() { return pSessionID; }
	public void setSessionID(String p) { pSessionID = p; }

	public String getUserID() { return pUserID; }
	public void setUserID(String p) { pUserID = p; }

	public String getUserName() { return pUserName; }
	public void setUserName(String p) { pUserName = p; }

	public String getSearchText() { return pSearchText; }
	public void setSearchText(String pSearchText) { this.pSearchText = pSearchText; }

	public ArrayList<FilterData> getFilterList() { return pFilterList; }
	public void setFilterList(ArrayList<FilterData> aFilterList) { this.pFilterList = aFilterList; }

	public FilterDataset()
	{
		ClearProperty();
	}
	
	private void ClearProperty()
	{
		pPageNo = 0;
		pPageSize = 0;

		pUserID = "";
		pUserName = "";
		pSessionID = "";

		pSearchText = "";
		pFilterSource = 0;			// 0 - CommonSearch, 1 - AdvancedSearch
		loanType = "";
		loanSubType = 0;
		branchCode = "";
		typeDesc = "";
		productType = "";
		allLoanSubType=0;
		t1 = "";
		subtype = "";
		loancode = "";
	}
	public ArrayList<String> getKeyList() {
		return pKeyList;
	}
	public void setKeyList(ArrayList<String> pKeyList) {
		this.pKeyList = pKeyList;
	}
	public String getLoanType() {
		return loanType;
	}
	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}
	public int getLoanSubType() {
		return loanSubType;
	}
	public void setLoanSubType(int loanSubType) {
		this.loanSubType = loanSubType;
	}
	
	public String getTypeDesc() {
		return typeDesc;
	}
	public void setTypeDesc(String typeDesc) {
		this.typeDesc = typeDesc;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getT1() {
		return t1;
	}
	public void setT1(String t1) {
		this.t1 = t1;
	}
	public String getSubtype() {
		return subtype;
	}
	public void setSubtype(String subtype) {
		this.subtype = subtype;
	}
	public String getLoancode() {
		return loancode;
	}
	public void setLoancode(String loancode) {
		this.loancode = loancode;
	}
	
}