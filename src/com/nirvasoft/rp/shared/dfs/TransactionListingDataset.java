package com.nirvasoft.rp.shared.dfs;

import java.util.ArrayList;
import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TransactionListingDataset implements Serializable
{
	private int pPageNo;
	private int pPageSize;
	private int pTotalRecords;
	
	private String pUserID;
	private String pUserName;

	private ArrayList<TransactionListingData> pDataList;

	public ArrayList<TransactionListingData> getDataList() { return pDataList; }
	public void setDataList(ArrayList<TransactionListingData> p) { this.pDataList = p; }

	public int getPageNo() { return pPageNo; }
	public void setPageNo(int p) { pPageNo = p; }

	public int getPageSize() { return pPageSize; }
	public void setPageSize(int p) { pPageSize = p; }

	public int getTotalRecords() { return pTotalRecords; }
	public void setTotalRecords(int p) { pTotalRecords = p; }

	public String getUserID() { return pUserID; }
	public void setUserID(String p) { pUserID = p; }

	public String getUserName() { return pUserName; }
	public void setUserName(String p) { pUserName = p; }

	public TransactionListingDataset()
	{
		ClearProperty();
	}
	
	private void ClearProperty()
	{
		pPageNo = 0;
		pPageSize = 0;
		pTotalRecords = 0;
		
		pUserID = "";
		pUserName = "";
	}
}