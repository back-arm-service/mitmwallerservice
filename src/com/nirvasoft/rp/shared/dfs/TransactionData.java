package com.nirvasoft.rp.shared.dfs;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

import com.nirvasoft.rp.shared.SharedUtil;

/* 
TUN THURA THET 2011 04 21
*/
@XmlRootElement
public class TransactionData{
	
	public TransactionData()
	{
		ClearProperty();
	}
	
	private long pTransactionNumber;
	private long pTransactionReference;
	private int pTransactionType;
	private String pTransactionDate;
	private String pTransactionTime;
	private String pEffectiveDate;
	private String pBranchCode;
	private String pAccountNumber;
	private String pReferenceNumber;  //check number
	private double pAmount;
	private int pTransactionCount;
	public String pRemark;
	
	public String pWorkstation;
	public int pSerial;
	public String pUserID;
	public String pAuthorizerID;
	public String pDescription;
	public String pCurrencyCode;	
	public double pCurrencyRate;
	public double pPrevBalance;
	public String pPreviousDate;
	public String pContraDate;
	public int pStatus;
	public String pAccRef;
	public String pSubRef;
	public int pSystemCode;
	public ArrayList<DenominationData> pDenorminations;
	private String pCashInHandGL;
	private String pProductGL;
	private double pBaseAmount;
	private boolean pIsByForceOD;
	private String pTrCurCode;
	private double pTrAmount;
	private double pTrPrevBalance;
	private double pN1;
	private double pN2;
	private double pN3;
	private String pT1;
	private String pT2;
	private String pT3;
	private String pFromBatchProcess;

	private double pDrAmt;
	private double pDrBaseAmt;
	private double pCrAmt;
	private double pCrBaseAmt;
	private String pVoucherSerialNo;
	private int pDenoStatus;
	private int pDenoSrNo;
	private String pRefNo;
	private boolean pIsMobile;
	private int pProcessType;
	private String pContraRef;//ODByForce form
	private String pType;
	ArrayList<String> lstUniEntryDate = new ArrayList<String>();
	private boolean checkbecAccount=false;
	private boolean checkbecCrAccount=false;
	public int pOfflineType;
	
	private String pT4; //------15/10/2017   tzw
	private String pT5;
	private String pT6;
	private String activestatus;
	
	//mmm
	private double baseDebit = 0;
	private double baseCredit = 0;
	private double baseOpening = 0;
	private double baseClosing = 0;
	private double opening = 0;
	private double closing = 0;
	private double debit = 0;
	private double credit = 0;
	private String accName="";
	
	private boolean pGlcheck;
	
	public boolean getGlcheck() { return pGlcheck; }
	public void setGlcheck(boolean p) { pGlcheck = p; }
	
	public ArrayList<DenominationData> getDenominations(){return pDenorminations;}
	public void setDenominations(ArrayList<DenominationData> p){pDenorminations =p;}
	
	public long getTransactionNumber(){return pTransactionNumber;}
	public void setTransactionNumber(long p){pTransactionNumber =p;}
	public long getTransactionReference(){return pTransactionReference;}
	public void setTransactionReference(long p){pTransactionReference =p;}
	public int getTransactionType(){return pTransactionType;}
	public void setTransactionType(int p){pTransactionType =p;}
	public String getTransactionDate(){return pTransactionDate;}
	public void setTransactionDate(String p){pTransactionDate =p;}
	public String getTransactionTime(){return pTransactionTime;}
	public void setTransactionTime(String p){pTransactionTime =p;}
	public String getEffectiveDate(){return pEffectiveDate;}
	public void setEffectiveDate(String p){pEffectiveDate =p;}
	public String getBranchCode(){return pBranchCode;}
	public void setBranchCode(String p){pBranchCode =p;}
	public String getAccountNumber(){return pAccountNumber;}
	public void setAccountNumber(String p){pAccountNumber =p;}
	public String getReferenceNumber(){return pReferenceNumber;}
	public void setReferenceNumber(String p){pReferenceNumber =p;}
	public double getAmount(){return pAmount;}
	public void setAmount(double p){pAmount =p;}
	public String getRemark(){return pRemark;}
	public void setRemark(String p){pRemark =p;}


	public int getStatus(){return pStatus;}
	public void setStatus(int p){pStatus =p;}
	
	public double getPreviousBalance(){return pPrevBalance;}
	public void setPreviousBalance(double p){pPrevBalance =p;}
	public double getCurrencyRate(){return pCurrencyRate;}
	public void setCurrencyRate(double p){pCurrencyRate =p;}
	
	public int getSerial(){return pSerial;}
	public void setSerial(int p){pSerial =p;}
	public int getSystemCode(){return pSystemCode;}
	public void setSystemCode(int p){pSystemCode =p;}
	
	public String getWorkstation(){return pWorkstation;}
	public void setWorkstation(String p){pWorkstation =p;}
	public String getUserID(){return pUserID;}
	public void setUserID(String p){pUserID =p;}
	public String getAuthorizerID(){return pAuthorizerID;}
	public void setAuthorizerID(String p){pAuthorizerID =p;}
	public String getDescription(){return pDescription;}
	public void setDescription(String p){pDescription =p;}
	public String getCurrencyCode(){return pCurrencyCode;}
	public void setCurrencyCode(String p){pCurrencyCode =p;}
	public String getContraDate(){return pContraDate;}
	public void setContraDate(String p){pContraDate =p;}
	public String getPreviousDate(){return pPreviousDate;}
	public void setPreviousDate(String p){pPreviousDate =p;}
	public String getAccRef(){return pAccRef;}
	public void setAccRef(String p){pAccRef =p;}
	public String getSubRef(){return pSubRef;}
	public void setSubRef(String p){pSubRef =p;}
	
	public String getCashInHandGL() {return pCashInHandGL;}
	public void setCashInHandGL(String p) {pCashInHandGL = p;}
	
	public String getProductGL() {return pProductGL;}
	public void setProductGL(String p) {pProductGL = p;}
	
	public double getBaseAmount() {return pBaseAmount;}
	public void setBaseAmount(double p) {pBaseAmount = p;}
	
	public boolean getIsByForceOD() {return pIsByForceOD;}
	public void setIsByForceOD(boolean pIsByForceOD) {this.pIsByForceOD = pIsByForceOD;}
	//hnw 
	private String pName = "";
	public String getName() { return pName; }
	public void setName(String Name) { this.pName = Name; }
	
	public String getRefNo() {
		return pRefNo;
	}
	public void setRefNo(String pRefNo) {
		this.pRefNo = pRefNo;
	}
	public int getDenoSrNo() {
		return pDenoSrNo;
	}
	public void setDenoSrNo(int pDenoSrNo) {
		this.pDenoSrNo = pDenoSrNo;
	}
	public String getTrCurCode() {
		return pTrCurCode;
	}
	public void setTrCurCode(String pTrCurCode) {
		this.pTrCurCode = pTrCurCode;
	}
	
	public String getpFromBatchProcess() {
		return pFromBatchProcess;
	}
	public void setpFromBatchProcess(String pFromBatchProcess) {
		this.pFromBatchProcess = pFromBatchProcess;
	}
	
	private void ClearProperty()
	{
		pTransactionReference=0;
		pTransactionType=0;
		pTransactionDate=SharedUtil.formatDDMMYYYY2MIT("01/01/1900");
		pTransactionTime=SharedUtil.formatDDMMYYYY2MIT("01/01/1900");
		pEffectiveDate=SharedUtil.formatDDMMYYYY2MIT("01/01/1900");
		pBranchCode="001";
		pAccountNumber="";
		pReferenceNumber="";  //check number
		pAmount=0;
		pRemark="";
		
		pWorkstation="001";
		pSerial=0;
		pUserID="";
		pAuthorizerID="";
		pDescription="";
		pCurrencyCode="MMK";
		pCurrencyRate=1;
		pPrevBalance=0;
		pPreviousDate=SharedUtil.formatDDMMYYYY2MIT("01/01/1900");;
		pContraDate=SharedUtil.formatDDMMYYYY2MIT("01/01/1900");;
		pStatus=0;
		pAccRef="";
		pSubRef="";
		pSystemCode=0;
		pCashInHandGL = "";
		pProductGL = "";
		pBaseAmount = 0;
		setIsByForceOD(false);
		setTrCurCode("");
		pTrAmount = 0;
		pTrPrevBalance = 0;
		pTransactionCount = 0;
		
		pN1 = 0;
		pN2 = 0;
		pN3 = 0;
		pT1 = "";
		pT2 = "";
		pT3 = "";
		
		pDrAmt = 0;
		pDrBaseAmt = 0;
		pCrAmt = 0;
		pCrBaseAmt = 0;		
		pVoucherSerialNo = "";
		pDenoStatus = -1;
		pDenoSrNo = 0;
		pRefNo = "";
		pIsMobile = false;
		pProcessType = 0;
		pContraRef = "";
		pType = "";
		pFromBatchProcess="";
		pOfflineType = 0;
		
		pT4 = "";      //------15/10/2017   tzw
		pT5 = "";
		pT6 = "";
		activestatus = "";
		
		pGlcheck = false;
	}
	public double getTrAmount() {
		return pTrAmount;
	}
	public void setTrAmount(double pTrAmount) {
		this.pTrAmount = pTrAmount;
	}
	public double getTrPrevBalance() {
		return pTrPrevBalance;
	}
	public void setTrPrevBalance(double pTrPrevBalance) {
		this.pTrPrevBalance = pTrPrevBalance;
	}
	public int getTransactionCount() {
		return pTransactionCount;
	}
	public void setTransactionCount(int transactionCount) {
		pTransactionCount = transactionCount;
	}
	public double getN1() {
		return pN1;
	}
	public void setN1(double pN1) {
		this.pN1 = pN1;
	}
	
	public double getDrAmt() {
		return pDrAmt;
	}
	public void setDrAmt(double pDrAmt) {
		this.pDrAmt = pDrAmt;
	}
	public double getDrBaseAmt() {
		return pDrBaseAmt;
	}
	public void setDrBaseAmt(double pDrBaseAmt) {
		this.pDrBaseAmt = pDrBaseAmt;
	}
	public double getCrAmt() {
		return pCrAmt;
	}
	public void setCrAmt(double pCrAmt) {
		this.pCrAmt = pCrAmt;
	}
	public double getCrBaseAmt() {
		return pCrBaseAmt;
	}
	public void setCrBaseAmt(double pCrBaseAmt) {
		this.pCrBaseAmt = pCrBaseAmt;
	}
	public String getVoucherSerialNo() {
		return pVoucherSerialNo;
	}
	public void setVoucherSerialNo(String pVoucherSerialNo) {
		this.pVoucherSerialNo = pVoucherSerialNo;
	}
	public int getDenoStatus() {
		return pDenoStatus;
	}
	public void setDenoStatus(int pDenoStatus) {
		this.pDenoStatus = pDenoStatus;
	}
	public double getN2() {
		return pN2;
	}
	public void setN2(double pN2) {
		this.pN2 = pN2;
	}
	public double getN3() {
		return pN3;
	}
	public void setN3(double pN3) {
		this.pN3 = pN3;
	}
	public String getT1() {
		return pT1;
	}
	public void setT1(String pT1) {
		this.pT1 = pT1;
	}
	public String getT2() {
		return pT2;
	}
	public void setT2(String pT2) {
		this.pT2 = pT2;
	}
	public String getT3() {
		return pT3;
	}
	public void setT3(String pT3) {
		this.pT3 = pT3;
	}
	public boolean getIsMobile() {	return pIsMobile;	}
	public void setIsMobile(boolean pIsMobile) {	this.pIsMobile = pIsMobile;	}
	
	public int getProcessType() {
		return pProcessType;
	}
	public void setProcessType(int pProcessType) {
		this.pProcessType = pProcessType;
	}
	public String getContraRef() {
		return pContraRef;
	}
	public void setContraRef(String pContraRef) {
		this.pContraRef = pContraRef;
	}
	public String getType() {
		return pType;
	}
	public void setType(String pType) {
		this.pType = pType;
	}	
	public ArrayList<String> getLstUniEntryDate() { return lstUniEntryDate; }

	public void setLstUniEntryDate(ArrayList<String> lstUniEntryDate) {	this.lstUniEntryDate = lstUniEntryDate;	}
	/*public boolean isCheckbecAccount() { 
		return checkbecAccount;
	}
	public void setCheckbecAccount(boolean checkbecAccount) {
		this.checkbecAccount = checkbecAccount;
	}*/
	
	public boolean isCheckbecCrAccount() {
		return checkbecCrAccount;
	}
	public boolean isCheckbecAccount() {
		return checkbecAccount;
	}
	public void setCheckbecAccount(boolean checkbecAccount) {
		this.checkbecAccount = checkbecAccount;
	}
	public void setCheckbecCrAccount(boolean checkbecCrAccount) {
		this.checkbecCrAccount = checkbecCrAccount;
	}
	public int getOfflineType() {
		return pOfflineType;
	}
	public void setOfflineType(int pOfflineType) {
		this.pOfflineType = pOfflineType;
	}
	
	public String getT4() {
		return pT4;
	}
	public void setT4(String pT4) {
		this.pT4 = pT4;
	}
	public String getT5() {
		return pT5;
	}
	public void setT5(String pT5) {
		this.pT5 = pT5;
	}
	public String getT6() {
		return pT6;
	}
	public void setT6(String pT6) {
		this.pT6 = pT6;
	}
	public String getActivestatus() {
		return activestatus;
	}
	public void setActivestatus(String activestatus) {
		this.activestatus = activestatus;
	}
	public double getClosing() {
		return closing;
	}
	public void setClosing(double closing) {
		this.closing = closing;
	}
	public double getBaseDebit() {
		return baseDebit;
	}
	public void setBaseDebit(double baseDebit) {
		this.baseDebit = baseDebit;
	}
	public double getBaseCredit() {
		return baseCredit;
	}
	public void setBaseCredit(double baseCredit) {
		this.baseCredit = baseCredit;
	}
	public double getBaseOpening() {
		return baseOpening;
	}
	public void setBaseOpening(double baseOpening) {
		this.baseOpening = baseOpening;
	}
	public double getBaseClosing() {
		return baseClosing;
	}
	public void setBaseClosing(double baseClosing) {
		this.baseClosing = baseClosing;
	}
	public double getOpening() {
		return opening;
	}
	public void setOpening(double opening) {
		this.opening = opening;
	}
	public double getDebit() {
		return debit;
	}
	public void setDebit(double debit) {
		this.debit = debit;
	}
	public double getCredit() {
		return credit;
	}
	public void setCredit(double credit) {
		this.credit = credit;
	}
	public String getAccName() {
		return accName;
	}
	public void setAccName(String accName) {
		this.accName = accName;
	}
	
	
}
