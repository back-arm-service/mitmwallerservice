package com.nirvasoft.rp.shared.dfs;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DenominationData implements Serializable {
	private static final long serialVersionUID = 8692255645383445244L;
	private double pSize;
	private long pQuantity;
	private short pDenoType; // For Refund Case
	private short pDenoMoneyType; // For Kyat, Pyar Case
	private String pCurrencyCode;
	private double pAmount;
	private double pBaseAmount;
	private String pDenoName;
	private String pReferenceModuleCode;
	private String pBranchCode;
	private int PreAccountTransactionRefNo;
	private int pDrCr;
	private double pTotalAmount;

	private String pUnitType;

	private long pTransNo;
	private int pDenoSrNo;
	private String pEntryDate;
	private double pK5000;
	private double pK1000;
	private double pK500;
	private double pK200;
	private double pK100;
	private double pK90;
	private double pK50;
	private double pK45;
	private double pK20;
	private double pK15;
	private double pK10;
	private double pK5;
	private double pK1;

	private double pC100;
	private double pC50;
	private double pC25;
	private double pC10;
	private double pC5;
	private double pC1;

	private double pP50;
	private double pP25;
	private double pP10;
	private double pP5;
	private double pP1;

	private String pRefNo;
	private double pSerialNo;
	private String pSlipNo;
	private String pAccountNumber;
	private String pT1;
	private String pTellerID;
	private String pCounterID;
	private String pTrpsNo;
	private String pUserID;
	private String pRemark;
	private String pUniqueNo;
	private int pDenoStatus = 0;
	private double pN9 = 0.00;
	private double pN10 = 0.00;
	private String pT11 = "";
	private String pT12 = "";
	private String pT13 = "";
	private String pT14 = "";
	private String pT15 = "";
	private int pStatus; // mmmyint 07.08.2018
	private boolean isGL;
	private String productFeature;
	ArrayList<String> lstUniEntryDate = new ArrayList<String>();

	public DenominationData() {
		clearProperty();
	}

	public double getpSize() {
		return pSize;
	}

	public void setpSize(double pSize) {
		this.pSize = pSize;
	}

	public long getpQuantity() {
		return pQuantity;
	}

	public void setpQuantity(long pQuantity) {
		this.pQuantity = pQuantity;
	}

	public short getpDenoType() {
		return pDenoType;
	}

	public void setpDenoType(short pDenoType) {
		this.pDenoType = pDenoType;
	}

	public short getpDenoMoneyType() {
		return pDenoMoneyType;
	}

	public void setpDenoMoneyType(short pDenoMoneyType) {
		this.pDenoMoneyType = pDenoMoneyType;
	}

	public String getpCurrencyCode() {
		return pCurrencyCode;
	}

	public void setpCurrencyCode(String pCurrencyCode) {
		this.pCurrencyCode = pCurrencyCode;
	}

	public double getpAmount() {
		return pAmount;
	}

	public void setpAmount(double pAmount) {
		this.pAmount = pAmount;
	}

	public double getpBaseAmount() {
		return pBaseAmount;
	}

	public void setpBaseAmount(double pBaseAmount) {
		this.pBaseAmount = pBaseAmount;
	}

	public String getpDenoName() {
		return pDenoName;
	}

	public void setpDenoName(String pDenoName) {
		this.pDenoName = pDenoName;
	}

	public String getpReferenceModuleCode() {
		return pReferenceModuleCode;
	}

	public void setpReferenceModuleCode(String pReferenceModuleCode) {
		this.pReferenceModuleCode = pReferenceModuleCode;
	}

	public String getpBranchCode() {
		return pBranchCode;
	}

	public void setpBranchCode(String pBranchCode) {
		this.pBranchCode = pBranchCode;
	}

	public int getpDrCr() {
		return pDrCr;
	}

	public void setpDrCr(int pDrCr) {
		this.pDrCr = pDrCr;
	}

	public double getpTotalAmount() {
		return pTotalAmount;
	}

	public void setpTotalAmount(double pTotalAmount) {
		this.pTotalAmount = pTotalAmount;
	}

	public String getpUnitType() {
		return pUnitType;
	}

	public void setpUnitType(String pUnitType) {
		this.pUnitType = pUnitType;
	}

	public long getpTransNo() {
		return pTransNo;
	}

	public void setpTransNo(long pTransNo) {
		this.pTransNo = pTransNo;
	}

	public int getpDenoSrNo() {
		return pDenoSrNo;
	}

	public void setpDenoSrNo(int pDenoSrNo) {
		this.pDenoSrNo = pDenoSrNo;
	}

	public String getpEntryDate() {
		return pEntryDate;
	}

	public void setpEntryDate(String pEntryDate) {
		this.pEntryDate = pEntryDate;
	}

	public double getpK5000() {
		return pK5000;
	}

	public void setpK5000(double pK5000) {
		this.pK5000 = pK5000;
	}

	public double getpK1000() {
		return pK1000;
	}

	public void setpK1000(double pK1000) {
		this.pK1000 = pK1000;
	}

	public double getpK500() {
		return pK500;
	}

	public void setpK500(double pK500) {
		this.pK500 = pK500;
	}

	public double getpK200() {
		return pK200;
	}

	public void setpK200(double pK200) {
		this.pK200 = pK200;
	}

	public double getpK100() {
		return pK100;
	}

	public void setpK100(double pK100) {
		this.pK100 = pK100;
	}

	public double getpK90() {
		return pK90;
	}

	public void setpK90(double pK90) {
		this.pK90 = pK90;
	}

	public double getpK50() {
		return pK50;
	}

	public void setpK50(double pK50) {
		this.pK50 = pK50;
	}

	public double getpK45() {
		return pK45;
	}

	public void setpK45(double pK45) {
		this.pK45 = pK45;
	}

	public double getpK20() {
		return pK20;
	}

	public void setpK20(double pK20) {
		this.pK20 = pK20;
	}

	public double getpK15() {
		return pK15;
	}

	public void setpK15(double pK15) {
		this.pK15 = pK15;
	}

	public double getpK10() {
		return pK10;
	}

	public void setpK10(double pK10) {
		this.pK10 = pK10;
	}

	public double getpK5() {
		return pK5;
	}

	public void setpK5(double pK5) {
		this.pK5 = pK5;
	}

	public double getpK1() {
		return pK1;
	}

	public void setpK1(double pK1) {
		this.pK1 = pK1;
	}

	public double getpC100() {
		return pC100;
	}

	public void setpC100(double pC100) {
		this.pC100 = pC100;
	}

	public double getpC50() {
		return pC50;
	}

	public void setpC50(double pC50) {
		this.pC50 = pC50;
	}

	public double getpC25() {
		return pC25;
	}

	public void setpC25(double pC25) {
		this.pC25 = pC25;
	}

	public double getpC10() {
		return pC10;
	}

	public void setpC10(double pC10) {
		this.pC10 = pC10;
	}

	public double getpC5() {
		return pC5;
	}

	public void setpC5(double pC5) {
		this.pC5 = pC5;
	}

	public double getpC1() {
		return pC1;
	}

	public void setpC1(double pC1) {
		this.pC1 = pC1;
	}

	public double getpP50() {
		return pP50;
	}

	public void setpP50(double pP50) {
		this.pP50 = pP50;
	}

	public double getpP25() {
		return pP25;
	}

	public void setpP25(double pP25) {
		this.pP25 = pP25;
	}

	public double getpP10() {
		return pP10;
	}

	public void setpP10(double pP10) {
		this.pP10 = pP10;
	}

	public double getpP5() {
		return pP5;
	}

	public void setpP5(double pP5) {
		this.pP5 = pP5;
	}

	public double getpP1() {
		return pP1;
	}

	public void setpP1(double pP1) {
		this.pP1 = pP1;
	}

	public String getpRefNo() {
		return pRefNo;
	}

	public void setpRefNo(String pRefNo) {
		this.pRefNo = pRefNo;
	}

	public double getpSerialNo() {
		return pSerialNo;
	}

	public void setpSerialNo(double pSerialNo) {
		this.pSerialNo = pSerialNo;
	}

	public String getpSlipNo() {
		return pSlipNo;
	}

	public void setpSlipNo(String pSlipNo) {
		this.pSlipNo = pSlipNo;
	}

	public String getpAccountNumber() {
		return pAccountNumber;
	}

	public void setpAccountNumber(String pAccountNumber) {
		this.pAccountNumber = pAccountNumber;
	}

	public String getpT1() {
		return pT1;
	}

	public void setpT1(String pT1) {
		this.pT1 = pT1;
	}

	public String getpTellerID() {
		return pTellerID;
	}

	public void setpTellerID(String pTellerID) {
		this.pTellerID = pTellerID;
	}

	public String getpCounterID() {
		return pCounterID;
	}

	public void setpCounterID(String pCounterID) {
		this.pCounterID = pCounterID;
	}

	public String getpTrpsNo() {
		return pTrpsNo;
	}

	public void setpTrpsNo(String pTrpsNo) {
		this.pTrpsNo = pTrpsNo;
	}

	public String getpUserID() {
		return pUserID;
	}

	public void setpUserID(String pUserID) {
		this.pUserID = pUserID;
	}

	public String getpRemark() {
		return pRemark;
	}

	public void setpRemark(String pRemark) {
		this.pRemark = pRemark;
	}

	public String getpUniqueNo() {
		return pUniqueNo;
	}

	public void setpUniqueNo(String pUniqueNo) {
		this.pUniqueNo = pUniqueNo;
	}

	public int getpDenoStatus() {
		return pDenoStatus;
	}

	public void setpDenoStatus(int pDenoStatus) {
		this.pDenoStatus = pDenoStatus;
	}

	public double getpN9() {
		return pN9;
	}

	public void setpN9(double pN9) {
		this.pN9 = pN9;
	}

	public double getpN10() {
		return pN10;
	}

	public void setpN10(double pN10) {
		this.pN10 = pN10;
	}

	public String getpT12() {
		return pT12;
	}

	public void setpT12(String pT12) {
		this.pT12 = pT12;
	}

	public String getpT13() {
		return pT13;
	}

	public void setpT13(String pT13) {
		this.pT13 = pT13;
	}

	public String getpT14() {
		return pT14;
	}

	public void setpT14(String pT14) {
		this.pT14 = pT14;
	}

	public String getpT15() {
		return pT15;
	}

	public void setpT15(String pT15) {
		this.pT15 = pT15;
	}

	public int getpStatus() {
		return pStatus;
	}

	public void setpStatus(int pStatus) {
		this.pStatus = pStatus;
	}

	public void setpT11(String pT11) {
		this.pT11 = pT11;
	}

	public String getUnitType() {
		return pUnitType;
	}

	public void setUnitType(String p) {
		this.pUnitType = p;
	}

	public void setSize(double p) {
		pSize = p;
	}

	public double getSize() {
		return pSize;
	}

	public void setQuantity(long p) {
		pQuantity = p;
	}

	public long getQuantity() {
		return pQuantity;
	}

	public void setCurrencyCode(String p) {
		pCurrencyCode = p;
	}

	public String getCurrencyCode() {
		return pCurrencyCode;
	}

	public void setDenoType(short p) {
		pDenoType = p;
	}

	public short getDenoType() {
		return pDenoType;
	}

	public void setDenoSrNo(int p) {
		pDenoSrNo = p;
	}

	public int getDenoSrNo() {
		return pDenoSrNo;
	}

	public void setTransNo(long p) {
		pTransNo = p;
	}

	public long getTransNo() {
		return pTransNo;
	}

	public void setEntryDate(String p) {
		pEntryDate = p;
	}

	public String getEntryDate() {
		return pEntryDate;
	}

	public void setRefNo(String p) {
		pRefNo = p;
	}

	public String getRefNo() {
		return pRefNo;
	}

	public void setSerialNo(double p) {
		pSerialNo = p;
	}

	public double getSerialNo() {
		return pSerialNo;
	}

	public void setAccountNumber(String p) {
		pAccountNumber = p;
	}

	public String getAccountNumber() {
		return pAccountNumber;
	}

	public void setSlipNo(String p) {
		pSlipNo = p;
	}

	public String getSlipNo() {
		return pSlipNo;
	}

	public void setT1(String p) {
		pT1 = p;
	}

	public String getT1() {
		return pT1;
	}

	public void setTellerID(String p) {
		pTellerID = p;
	}

	public String getTellerID() {
		return pTellerID;
	}

	public void setCounterID(String p) {
		pCounterID = p;
	}

	public String getCounterID() {
		return pCounterID;
	}

	public void setTrpsNo(String p) {
		pTrpsNo = p;
	}

	public String getTrpsNo() {
		return pTrpsNo;
	}

	public void setUserID(String p) {
		pUserID = p;
	}

	public String getUserID() {
		return pUserID;
	}

	public void setDenoMoneyType(short p) {
		pDenoMoneyType = p;
	}

	public short getDenoMoneyType() {
		return pDenoMoneyType;
	}

	public void setK5000(double p) {
		pK5000 = p;
	}

	public double getK5000() {
		return pK5000;
	}

	public void setK1000(double p) {
		pK1000 = p;
	}

	public double getK1000() {
		return pK1000;
	}

	public void setK500(double p) {
		pK500 = p;
	}

	public double getK500() {
		return pK500;
	}

	public void setK200(double p) {
		pK200 = p;
	}

	public double getK200() {
		return pK200;
	}

	public void setK100(double p) {
		pK100 = p;
	}

	public double getK100() {
		return pK100;
	}

	public void setK90(double p) {
		pK90 = p;
	}

	public double getK90() {
		return pK90;
	}

	public void setK50(double p) {
		pK50 = p;
	}

	public double getK50() {
		return pK50;
	}

	public void setK45(double p) {
		pK45 = p;
	}

	public double getK45() {
		return pK45;
	}

	public void setK20(double p) {
		pK20 = p;
	}

	public double getK20() {
		return pK20;
	}

	public void setK15(double p) {
		pK15 = p;
	}

	public double getK15() {
		return pK15;
	}

	public void setK10(double p) {
		pK10 = p;
	}

	public double getK10() {
		return pK10;
	}

	public void setK1(double p) {
		pK1 = p;
	}

	public double getK1() {
		return pK1;
	}

	public void setC100(double p) {
		pC100 = p;
	}

	public double getC100() {
		return pC100;
	}

	public void setC50(double p) {
		pC50 = p;
	}

	public double getC50() {
		return pC50;
	}

	public void setC25(double p) {
		pC25 = p;
	}

	public double getC25() {
		return pC25;
	}

	public void setC10(double p) {
		pC10 = p;
	}

	public double getC10() {
		return pC10;
	}

	public void setC5(double p) {
		pC5 = p;
	}

	public double getC5() {
		return pC5;
	}

	public void setC1(double p) {
		pC1 = p;
	}

	public double getC1() {
		return pC1;
	}

	public void setK5(double p) {
		pK5 = p;
	}

	public double getK5() {
		return pK5;
	}

	public void setP50(double p) {
		pP50 = p;
	}

	public double getP50() {
		return pP50;
	}

	public void setP25(double p) {
		pP25 = p;
	}

	public double getP25() {
		return pP25;
	}

	public void setP10(double p) {
		pP10 = p;
	}

	public double getP10() {
		return pP10;
	}

	public void setP5(double p) {
		pP5 = p;
	}

	public double getP5() {
		return pP5;
	}

	public void setP1(double p) {
		pP1 = p;
	}

	public double getP1() {
		return pP1;
	}

	public double getAmount() {
		return pAmount;
	}

	public void setAmount(double pAmount) {
		this.pAmount = pAmount;
	}

	public double getBaseAmount() {
		return pBaseAmount;
	}

	public void setBaseAmount(double pBaseAmount) {
		this.pBaseAmount = pBaseAmount;
	}

	public String getDenoName() {
		return pDenoName;
	}

	public void setDenoName(String pDenoName) {
		this.pDenoName = pDenoName;
	}

	public String getReferenceModuleCode() {
		return pReferenceModuleCode;
	}

	public void setReferenceModuleCode(String pReferenceModuleCode) {
		this.pReferenceModuleCode = pReferenceModuleCode;
	}

	public String getBranchCode() {
		return pBranchCode;
	}

	public void setBranchCode(String pBranchCode) {
		this.pBranchCode = pBranchCode;
	}

	public int getPreAccountTransactionRefNo() {
		return PreAccountTransactionRefNo;
	}

	public void setPreAccountTransactionRefNo(int preAccountTransactionRefNo) {
		PreAccountTransactionRefNo = preAccountTransactionRefNo;
	}

	public int getDrCr() {
		return pDrCr;
	}

	public void setDrCr(int pDrCr) {
		this.pDrCr = pDrCr;
	}

	public double getTotalAmount() {
		return pTotalAmount;
	}

	public void setTotalAmount(double pTotalAmount) {
		this.pTotalAmount = pTotalAmount;
	}

	public String getRemark() {
		return pRemark;
	}

	public void setRemark(String pRemark) {
		this.pRemark = pRemark;
	}

	public String getUniqueNo() {
		return pUniqueNo;
	}

	public void setUniqueNo(String uniqueNo) {
		pUniqueNo = uniqueNo;
	}

	public int getDenoStatus() {
		return pDenoStatus;
	}

	public void setDenoStatus(int pDenoStatus) {
		this.pDenoStatus = pDenoStatus;
	}

	public double getN9() {
		return pN9;
	}

	public void setN9(double pN9) {
		this.pN9 = pN9;
	}

	public double getN10() {
		return pN10;
	}

	public void setN10(double pN10) {
		this.pN10 = pN10;
	}

	public String getpT11() {
		return pT11;
	}

	public void setT11(String pT11) {
		this.pT11 = pT11;
	}

	public String getT12() {
		return pT12;
	}

	public void setT12(String pT12) {
		this.pT12 = pT12;
	}

	public String getT13() {
		return pT13;
	}

	public void setT13(String pT13) {
		this.pT13 = pT13;
	}

	public String getT14() {
		return pT14;
	}

	public void setT14(String pT14) {
		this.pT14 = pT14;
	}

	public String getT15() {
		return pT15;
	}

	public void setT15(String pT15) {
		this.pT15 = pT15;
	}

	void clearProperty() {
		pSize = 0;
		pQuantity = 0;
		pDenoType = 0;
		pDenoSrNo = 0;
		pEntryDate = "";

		pUnitType = "";

		pK5000 = 0;
		pK1000 = 0;
		pK500 = 0;
		pK200 = 0;
		pK100 = 0;
		pK90 = 0;
		pK50 = 0;
		pK45 = 0;
		pK20 = 0;
		pK15 = 0;
		pK10 = 0;
		pK5 = 0;
		pK1 = 0;

		pC100 = 0;
		pC50 = 0;
		pC25 = 0;
		pC10 = 0;
		pC5 = 0;
		pC1 = 0;

		pP50 = 0;
		pP25 = 0;
		pP10 = 0;
		pP5 = 0;
		pP1 = 0;

		pRefNo = "";
		pSerialNo = 0;
		pSlipNo = "";
		pAccountNumber = "";
		pT1 = "";
		pCounterID = "";
		pTellerID = "";
		pTrpsNo = "";
		pUserID = "";
		pDenoMoneyType = 0;
		pAmount = 0;
		pBaseAmount = 0;
		pDenoName = "";
		pBranchCode = "";
		pReferenceModuleCode = "";
		PreAccountTransactionRefNo = 0;
		pDrCr = 1;
		pTotalAmount = 0;
		pCurrencyCode = "";
		pRemark = "";
		pUniqueNo = "";
		pDenoStatus = 0;
		pN9 = 0.00;
		pN10 = 0.00;
		pT11 = "";
		pT12 = "";
		pT13 = "";
		pT14 = "";
		pT15 = "";
		lstUniEntryDate = new ArrayList<String>();
		pStatus = 1;
		isGL = false;
		productFeature = "";
	}

	public static String getCurrencySymbol(String currencyType, int denoMoneyType, int denoType) {
		String ret = "";
		switch (denoMoneyType) {
		case 0://
			if (currencyType.equals("MMK")) {
				ret = "K";
			} else if (currencyType.equals("USD")) {
				ret = "USD";
			} else if (currencyType.equals("SGD")) {
				ret = "SGD";
			} else if (currencyType.equals("FEC")) {
				ret = "FEC";
			} else if (currencyType.equals("EUR")) {
				ret = "EUR";
			}
			break;
		case 1: // Coin
			if (currencyType.equals("MMK")) {
				ret = "C";
			} else if (currencyType.equals("USD") || currencyType.equals("SGD")) {
				ret = "C";
			} else if (currencyType.equals("FEC")) {
				ret = "C"; // No Coin in FEC
			} else if (currencyType.equals("EUR")) {
				ret = "C";
			}
			break;
		case 2: // Cents and Pyas
			if (currencyType.equals("MMK")) {
				ret = "P";
			} else if (currencyType.equals("USD") || currencyType.equals("SGD")) {
				ret = "Cents";
			} else if (currencyType.equals("EUR")) {
				ret = "Cents";
			}
			break;
		default:
			break;
		}
		if (currencyType.equals("MMK")) {

		}
		return ret;
	}

	public static double getTotalDeno(ArrayList<DenominationData> pArl) {
		double ret = 0;
		for (int i = 0; i < pArl.size(); i++) {
			if (pArl.get(i).getDenoType() == 0) { // 0 for deno
				ret += pArl.get(i).getSize() * pArl.get(i).getQuantity();
			}
		}
		return ret;
	}

	public static double getTotalRefund(ArrayList<DenominationData> pArl) {
		double ret = 0;
		for (int i = 0; i < pArl.size(); i++) {
			if (pArl.get(i).getDenoType() == 1) { // 1 for refund
				ret += pArl.get(i).getSize() * pArl.get(i).getQuantity();
			}
		}
		return ret;
	}

	public static double getTotal(ArrayList<DenominationData> pArl) {
		return getTotalDeno(pArl) - getTotalRefund(pArl);
	}

	public ArrayList<String> getLstUniEntryDate() {
		return lstUniEntryDate;
	}

	public void setLstUniEntryDate(ArrayList<String> lstUniEntryDate) {
		this.lstUniEntryDate = lstUniEntryDate;
	}

	public int getStatus() {
		return pStatus;
	}

	public void setStatus(int pStatus) {
		this.pStatus = pStatus;
	}

	public boolean isGL() {
		return isGL;
	}

	public void setGL(boolean isGL) {
		this.isGL = isGL;
	}

	public String getProductFeature() {
		return productFeature;
	}

	public void setProductFeature(String productFeature) {
		this.productFeature = productFeature;
	}

}
