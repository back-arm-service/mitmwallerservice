package com.nirvasoft.rp.shared.dfs;

public class MultiTransferListingData {	
	
	private int transferNo;
	private String transDate;
	private String totalAmount;
	private String status;
	private String currencyCode;
	
	public MultiTransferListingData(){
		clearProperty();
		
	}
	private void clearProperty(){
		
	}
	public int getTransferNo() {
		return transferNo;
	}
	public void setTransferNo(int transferNo) {
		this.transferNo = transferNo;
	}
	public String getTransDate() {
		return transDate;
	}
	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	

}
