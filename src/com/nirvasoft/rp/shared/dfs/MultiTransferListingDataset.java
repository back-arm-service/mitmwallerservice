package com.nirvasoft.rp.shared.dfs;

import java.util.ArrayList;
import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import com.nirvasoft.rp.shared.MultiTransferData;

@XmlRootElement
public class MultiTransferListingDataset implements Serializable
{
	private int pPageNo;
	private int pPageSize;
	private int pTotalRecords;
	
	private String pUserID;
	private String pUserName;

	private ArrayList<MultiTransferData> pDataList;

	public ArrayList<MultiTransferData> getDataList() { return pDataList; }
	public void setDataList(ArrayList<MultiTransferData> p) { this.pDataList = p; }

	public int getPageNo() { return pPageNo; }
	public void setPageNo(int p) { pPageNo = p; }

	public int getPageSize() { return pPageSize; }
	public void setPageSize(int p) { pPageSize = p; }

	public int getTotalRecords() { return pTotalRecords; }
	public void setTotalRecords(int p) { pTotalRecords = p; }

	public String getUserID() { return pUserID; }
	public void setUserID(String p) { pUserID = p; }

	public String getUserName() { return pUserName; }
	public void setUserName(String p) { pUserName = p; }

	public MultiTransferListingDataset()
	{
		ClearProperty();
	}
	
	private void ClearProperty()
	{
		pPageNo = 0;
		pPageSize = 0;
		pTotalRecords = 0;
		
		pUserID = "";
		pUserName = "";
	}
}