package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransferOutReq {
	private String userID;
	private String sessionID;
	private String payeeID;
	private String beneficiaryID;
	private String fromInstitutionCode;
	private String toInstitutionCode;
	private String toAcc;
	private double amount;
	private double bankCharges;
	private double commissionCharges;
	private String remark;
	private String transferType;
	private String sKey;
	private String field1;
	private String field2;
	private String fromName;
	private String toName;
	private String tosKey;
	private String wType;
	private String billId;
	private String refNo;
	private String cusName;
	private String billAmount;
	private String ccyCode;
	private String dueDate;
	private String t1;
	private String t2;
	private String txnRef;
	private String penalty;
	private String txnDate;
	private String paidBy;
	private String vendorCode;
	private String deptName;
	private String taxDesc;
	private String iv;
	private String dm;
	private String salt;
	private String password;
	private String paymentType;
	private String merchantID;

	public TransferOutReq() {
		clearProperty();
	}

	private void clearProperty() {
		userID = "";
		sessionID = "";
		payeeID = "";
		beneficiaryID = "";
		fromInstitutionCode = "";
		toInstitutionCode = "";
		toAcc = "";
		amount = 0.00;
		bankCharges = 0.00;
		commissionCharges = 0.00;
		remark = "";
		transferType = "";
		sKey = "";
		field1 = "";
		field2 = "";
		fromName = "";
		toName = "";
		wType = "";
		tosKey = "";
		billId = "";
		refNo = "";
		cusName = "";
		billAmount = "";
		ccyCode = "";
		dueDate = "";
		t1 = "";
		t2 = "";
		txnRef = "";
		penalty = "";
		txnDate = "";
		paidBy = "";
		vendorCode = "";
		deptName = "";
		taxDesc = "";
		this.password = "";
		this.iv = "";
		this.dm = "";
		this.salt = "";
		this.paymentType = "";
		this.merchantID = "";
	}

	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public double getAmount() {
		return amount;
	}

	public double getBankCharges() {
		return bankCharges;
	}

	public String getBeneficiaryID() {
		return beneficiaryID;
	}

	public String getBillAmount() {
		return billAmount;
	}

	public String getBillId() {
		return billId;
	}

	public String getCcyCode() {
		return ccyCode;
	}

	public double getCommissionCharges() {
		return commissionCharges;
	}

	public String getCusName() {
		return cusName;
	}

	public String getDeptName() {
		return deptName;
	}

	public String getDm() {
		return dm;
	}

	public String getDueDate() {
		return dueDate;
	}

	public String getField1() {
		return field1;
	}

	public String getField2() {
		return field2;
	}

	public String getFromInstitutionCode() {
		return fromInstitutionCode;
	}

	public String getFromName() {
		return fromName;
	}

	public String getIv() {
		return iv;
	}

	public String getPaidBy() {
		return paidBy;
	}

	public String getPassword() {
		return password;
	}

	public String getPayeeID() {
		return payeeID;
	}

	public String getPenalty() {
		return penalty;
	}

	public String getRefNo() {
		return refNo;
	}

	public String getRemark() {
		return remark;
	}

	public String getSalt() {
		return salt;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getsKey() {
		return sKey;
	}

	public String getT1() {
		return t1;
	}

	public String getT2() {
		return t2;
	}

	public String getTaxDesc() {
		return taxDesc;
	}

	public String getToAcc() {
		return toAcc;
	}

	public String getToInstitutionCode() {
		return toInstitutionCode;
	}

	public String getToName() {
		return toName;
	}

	public String getTosKey() {
		return tosKey;
	}

	public String getTransferType() {
		return transferType;
	}

	public String getTxnDate() {
		return txnDate;
	}

	public String getTxnRef() {
		return txnRef;
	}

	public String getUserID() {
		return userID;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public String getwType() {
		return wType;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void setBankCharges(double bankCharges) {
		this.bankCharges = bankCharges;
	}

	public void setBeneficiaryID(String beneficiaryID) {
		this.beneficiaryID = beneficiaryID;
	}

	public void setBillAmount(String billAmount) {
		this.billAmount = billAmount;
	}

	public void setBillId(String billId) {
		this.billId = billId;
	}

	public void setCcyCode(String ccyCode) {
		this.ccyCode = ccyCode;
	}

	public void setCommissionCharges(double commissionCharges) {
		this.commissionCharges = commissionCharges;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public void setDm(String dm) {
		this.dm = dm;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public void setFromInstitutionCode(String fromInstitutionCode) {
		this.fromInstitutionCode = fromInstitutionCode;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	public void setIv(String iv) {
		this.iv = iv;
	}

	public void setPaidBy(String paidBy) {
		this.paidBy = paidBy;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPayeeID(String payeeID) {
		this.payeeID = payeeID;
	}

	public void setPenalty(String penalty) {
		this.penalty = penalty;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setsKey(String sKey) {
		this.sKey = sKey;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setTaxDesc(String taxDesc) {
		this.taxDesc = taxDesc;
	}

	public void setToAcc(String toAcc) {
		this.toAcc = toAcc;
	}

	public void setToInstitutionCode(String toInstitutionCode) {
		this.toInstitutionCode = toInstitutionCode;
	}

	public void setToName(String toName) {
		this.toName = toName;
	}

	public void setTosKey(String tosKey) {
		this.tosKey = tosKey;
	}

	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}

	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}

	public void setTxnRef(String txnRef) {
		this.txnRef = txnRef;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public void setwType(String wType) {
		this.wType = wType;
	}

	@Override
	public String toString() {
		return "TransferOutReq [userID=" + userID + ", sessionID=" + sessionID + ", payeeID=" + payeeID
				+ ", beneficiaryID=" + beneficiaryID + ", fromInstitutionCode=" + fromInstitutionCode
				+ ", toInstitutionCode=" + toInstitutionCode + ", toAcc=" + toAcc + ", amount=" + amount
				+ ", bankCharges=" + bankCharges + ", commissionCharges=" + commissionCharges + ", remark=" + remark
				+ ", transferType=" + transferType + ", sKey=" + sKey + ", field1=" + field1 + ", field2=" + field2
				+ ", fromName=" + fromName + ", toName=" + toName + ", tosKey=" + tosKey + ", wType=" + wType
				+ ", billId=" + billId + ", refNo=" + refNo + ", cusName=" + cusName + ", billAmount=" + billAmount
				+ ", ccyCode=" + ccyCode + ", dueDate=" + dueDate + ", t1=" + t1 + ", t2=" + t2 + ", txnRef=" + txnRef
				+ ", penalty=" + penalty + ", txnDate=" + txnDate + ", paidBy=" + paidBy + ", vendorCode=" + vendorCode
				+ ", deptName=" + deptName + ", taxDesc=" + taxDesc + ", iv=" + iv + ", dm=" + dm + ", salt=" + salt
				+ ", password=" + password + ", paymentType=" + paymentType + ", merchantID=" + merchantID + "]";
	}

}
