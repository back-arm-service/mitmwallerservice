package com.nirvasoft.rp.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

import com.nirvasoft.rp.data.EachAccount;

@XmlRootElement
public class AccountListResponseData {

	private String code;
	private String desc;
	private EachAccount[] accountList;

	public AccountListResponseData() {
		this.clearProperty();
	}

	private void clearProperty() {
		this.code = "";
		this.desc = "";
		this.accountList = null;
	}

	public EachAccount[] getAccountList() {
		return accountList;
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public void setAccountList(EachAccount[] accountList) {
		this.accountList = accountList;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public String toString() {
		return "AccountList [code=" + code + ", desc=" + desc + ", accountList=" + Arrays.toString(accountList) + "]";
	}

}
