package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TopupReq {
	private String userID;
	private String sessionID;
	private String toAcc;
	private String amount;
	private double amountdbl;
	private double bankCharges;
	private double commissionCharges;
	private String reference;
	private String transferType;
	private String sKey;
	private String field1;
	private String field2;

	public TopupReq() {
		clearProperty();
	}

	private void clearProperty() {
		userID = "";
		sessionID = "";
		toAcc = "";
		amount = "";
		amountdbl = 0.00;
		bankCharges = 0.00;
		commissionCharges = 0.00;
		reference = "";
		transferType = "";
		sKey = "";
		field1 = "";
		field2 = "";
	}

	public String getAmount() {
		return amount;
	}

	public double getAmountdbl() {
		return amountdbl;
	}

	public double getBankCharges() {
		return bankCharges;
	}

	public double getCommissionCharges() {
		return commissionCharges;
	}

	public String getField1() {
		return field1;
	}

	public String getField2() {
		return field2;
	}

	public String getReference() {
		return reference;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getsKey() {
		return sKey;
	}

	public String getToAcc() {
		return toAcc;
	}

	public String getTransferType() {
		return transferType;
	}

	public String getUserID() {
		return userID;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public void setAmountdbl(double amountdbl) {
		this.amountdbl = amountdbl;
	}

	public void setBankCharges(double bankCharges) {
		this.bankCharges = bankCharges;
	}

	public void setCommissionCharges(double commissionCharges) {
		this.commissionCharges = commissionCharges;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setsKey(String sKey) {
		this.sKey = sKey;
	}

	public void setToAcc(String toAcc) {
		this.toAcc = toAcc;
	}

	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	@Override
	public String toString() {
		return "TopupReq [userID=" + userID + ", sessionID=" + sessionID + ", toAcc=" + toAcc + ", amount=" + amount
				+ ", amountdbl=" + amountdbl + ", bankCharges=" + bankCharges + ", commissionCharges="
				+ commissionCharges + ", reference=" + reference + ", transferType=" + transferType + ", sKey=" + sKey
				+ ", field1=" + field1 + ", field2=" + field2 + "]";
	}

}
