package com.nirvasoft.rp.shared;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MobileLogoutResponseData {
	private String userID;
	private String sessionID;
	private String lastTimeLogin;
	private String lastTimeLogout;
	private String duration;
	private String code;
	private String desc;
	private ActivityHistoryList[] activityList;

	public MobileLogoutResponseData() {
		clearProperties();
	}

	private void clearProperties() {
		this.userID = "";
		this.sessionID = "";
		this.lastTimeLogin = "";
		this.lastTimeLogout = "";
		this.duration = "";
		this.code = "";
		this.desc = "";
		this.activityList = null;
	}

	public ActivityHistoryList[] getActivityList() {
		return activityList;
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public String getDuration() {
		return duration;
	}

	public String getLastTimeLogin() {
		return lastTimeLogin;
	}

	public String getLastTimeLogout() {
		return lastTimeLogout;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getUserID() {
		return userID;
	}

	public void setActivityList(ActivityHistoryList[] activityList) {
		this.activityList = activityList;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public void setLastTimeLogin(String lastTimeLogin) {
		this.lastTimeLogin = lastTimeLogin;
	}

	public void setLastTimeLogout(String lastTimeLogout) {
		this.lastTimeLogout = lastTimeLogout;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	@Override
	public String toString() {
		return "{userID=" + userID + ", sessionID=" + sessionID + ", lastTimeLogin=" + lastTimeLogin
				+ ", lastTimeLogout=" + lastTimeLogout + ", duration=" + duration + ", code=" + code + ", desc=" + desc
				+ ", activityList=" + Arrays.toString(activityList) + "}";
	}

}
