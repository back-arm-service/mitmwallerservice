package com.nirvasoft.rp.shared.icbs;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AgentEncashReqData {
	private String userID;
	private String sessionID;
	private String refno;
	private String field1;
	private String field2;

	public AgentEncashReqData() {
		clearProperties();
	}

	private void clearProperties() {
		this.userID = "";
		this.sessionID = "";
		this.refno = "";
		this.field1 = "";
		this.field2 = "";
	}

	public String getField1() {
		return field1;
	}

	public String getField2() {
		return field2;
	}

	public String getRefno() {
		return refno;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getUserID() {
		return userID;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public void setRefno(String refno) {
		this.refno = refno;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	@Override
	public String toString() {
		return "{userID=" + userID + ", sessionID=" + sessionID + ", refno=" + refno + ", field1=" + field1
				+ ", field2=" + field2 + "}";
	}
}
