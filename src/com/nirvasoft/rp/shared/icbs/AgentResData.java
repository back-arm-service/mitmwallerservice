package com.nirvasoft.rp.shared.icbs;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

import com.nirvasoft.rp.data.CommRateHeaderData;

@XmlRootElement
public class AgentResData {
	private String code;
	private String desc;
	private AgentData[] data = null;
	private CommRateHeaderData comData = null;
	private String id;
	private String fromAccount;
	private String fromName;

	public AgentResData() {
		clearProperties();
	}

	private void clearProperties() {
		this.id = "";
		this.code = "";
		this.desc = "";
		this.data = null;
		this.fromAccount = "";
		this.fromName = "";
		this.comData = null;
	}

	public String getCode() {
		return code;
	}

	public CommRateHeaderData getComData() {
		return comData;
	}

	public AgentData[] getData() {
		return data;
	}

	public String getDesc() {
		return desc;
	}

	public String getFromAccount() {
		return fromAccount;
	}

	public String getFromName() {
		return fromName;
	}

	public String getId() {
		return id;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setComData(CommRateHeaderData comData) {
		this.comData = comData;
	}

	public void setData(AgentData[] data) {
		this.data = data;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setFromAccount(String fromAccount) {
		this.fromAccount = fromAccount;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "{code=" + code + ", desc=" + desc + ", data=" + Arrays.toString(data) + ", id=" + id + ",fromAccount="
				+ fromAccount + ",fromName=" + fromName + ",comData=" + comData + "}";
	}
}
