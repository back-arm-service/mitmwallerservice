package com.nirvasoft.rp.shared.icbs;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AccountGLTransactionData {
	private long transNo;
	private String accNumber;
	private String GLCode1;
	private String GLCode2;
	private String GLCode3;
	private String GLCode4;
	private String GLCode5;
	private String baseCurCode;
	private String baseCurOperator;
	private double baseRate;
	private double baseAmount;
	private String mediumCurCode;
	private String mediumCurOperator;
	private double mediumRate;
	private double mediumAmount;
	private String trCurCode;
	private String trCurOperator;
	private double trRate;
	private double trAmount;
	private double trPrevBalance;
	private int n1;
	private int n2;
	private int n3;
	private String t1;
	private String t2;
	private String t3;

	public AccountGLTransactionData() {
		clearProperty();
	}

	private void clearProperty() {
		transNo = 0;
		accNumber = "";
		GLCode1 = "";
		GLCode2 = "";
		GLCode3 = "";
		GLCode4 = "";
		GLCode5 = "";
		baseCurCode = "";
		baseCurOperator = "";
		baseRate = 0;
		baseAmount = 0;
		mediumCurCode = "";
		mediumCurOperator = "";
		mediumRate = 0;
		mediumAmount = 0;
		trCurCode = "";
		trCurOperator = "";
		trRate = 0;
		trAmount = 0;
		trPrevBalance = 0;
		n1 = 0;
		n2 = 0;
		n3 = 0;
		t1 = "";
		t2 = "";
		t3 = "";
	}

	public String getAccNumber() {
		return accNumber;
	}

	public double getBaseAmount() {
		return baseAmount;
	}

	public String getBaseCurCode() {
		return baseCurCode;
	}

	public String getBaseCurOperator() {
		return baseCurOperator;
	}

	public double getBaseRate() {
		return baseRate;
	}

	public String getGLCode1() {
		return GLCode1;
	}

	public String getGLCode2() {
		return GLCode2;
	}

	public String getGLCode3() {
		return GLCode3;
	}

	public String getGLCode4() {
		return GLCode4;
	}

	public String getGLCode5() {
		return GLCode5;
	}

	public double getMediumAmount() {
		return mediumAmount;
	}

	public String getMediumCurCode() {
		return mediumCurCode;
	}

	public String getMediumCurOperator() {
		return mediumCurOperator;
	}

	public double getMediumRate() {
		return mediumRate;
	}

	public int getN1() {
		return n1;
	}

	public int getN2() {
		return n2;
	}

	public int getN3() {
		return n3;
	}

	public String getT1() {
		return t1;
	}

	public String getT2() {
		return t2;
	}

	public String getT3() {
		return t3;
	}

	public double getTrAmount() {
		return trAmount;
	}

	public long getTransNo() {
		return transNo;
	}

	public String getTrCurCode() {
		return trCurCode;
	}

	public String getTrCurOperator() {
		return trCurOperator;
	}

	public double getTrPrevBalance() {
		return trPrevBalance;
	}

	public double getTrRate() {
		return trRate;
	}

	public void setAccNumber(String accNumber) {
		this.accNumber = accNumber;
	}

	public void setBaseAmount(double baseAmount) {
		this.baseAmount = baseAmount;
	}

	public void setBaseCurCode(String baseCurCode) {
		this.baseCurCode = baseCurCode;
	}

	public void setBaseCurOperator(String baseCurOperator) {
		this.baseCurOperator = baseCurOperator;
	}

	public void setBaseRate(double baseRate) {
		this.baseRate = baseRate;
	}

	public void setGLCode1(String gLCode1) {
		this.GLCode1 = gLCode1;
	}

	public void setGLCode2(String gLCode2) {
		this.GLCode2 = gLCode2;
	}

	public void setGLCode3(String gLCode3) {
		this.GLCode3 = gLCode3;
	}

	public void setGLCode4(String gLCode4) {
		this.GLCode4 = gLCode4;
	}

	public void setGLCode5(String gLCode5) {
		this.GLCode5 = gLCode5;
	}

	public void setMediumAmount(double mediumAmount) {
		this.mediumAmount = mediumAmount;
	}

	public void setMediumCurCode(String mediumCurCode) {
		this.mediumCurCode = mediumCurCode;
	}

	public void setMediumCurOperator(String mediumCurOperator) {
		this.mediumCurOperator = mediumCurOperator;
	}

	public void setMediumRate(double mediumRate) {
		this.mediumRate = mediumRate;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public void setN2(int n2) {
		this.n2 = n2;
	}

	public void setN3(int n3) {
		this.n3 = n3;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public void setTrAmount(double trAmount) {
		this.trAmount = trAmount;
	}

	public void setTransNo(long transNo) {
		this.transNo = transNo;
	}

	public void setTrCurCode(String trCurCode) {
		this.trCurCode = trCurCode;
	}

	public void setTrCurOperator(String trCurOperator) {
		this.trCurOperator = trCurOperator;
	}

	public void setTrPrevBalance(double trPrevBalance) {
		this.trPrevBalance = trPrevBalance;
	}

	public void setTrRate(double trRate) {
		this.trRate = trRate;
	}

}
