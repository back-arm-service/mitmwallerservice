package com.nirvasoft.rp.shared.icbs;

import java.io.Serializable;

public class SMSTransferData extends ATMParameterData implements Serializable {
	private static final long serialVersionUID = -8583503791598502899L;
	private String pFromAccNumber;
	private String pToAccNumber;
	private String FromZone;
	private String ToZone;
	private String FromBranchCode;
	private String ToBranchCode;
	private String UserID;
	private double LimitAmount;
	private String fromName;
	private String toName;
	private String fromNrc;
	private String toNrc;
	private int transferKey;
	private String debitIBTKey;
	private String creditIBTKey;
	private boolean isAgent;
	private boolean isCreditAgent;
	private boolean isFinishCutOffTime;
	private String ReferenceNo;
	private boolean isSameAgent;
	private String customerID;
	private int onlineSerialNo;
	private String merchantID;
	private String remark;
	private String fromProductCode;
	private String toProductCode;
	private String fromCCY;
	private String toCCY;

	public SMSTransferData() {
		super.clearProperty();

		pFromAccNumber = "";
		pToAccNumber = "";
		FromZone = "";
		ToZone = "";
		FromBranchCode = "";
		ToBranchCode = "";
		UserID = "";
		LimitAmount = 0;
		fromName = "";
		toName = "";
		fromNrc = "";
		toNrc = "";
		transferKey = 0;
		debitIBTKey = "";
		creditIBTKey = "";
		isAgent = false;
		isCreditAgent = false;
		isFinishCutOffTime = false;
		ReferenceNo = "";
		isSameAgent = false;
		onlineSerialNo = 0;
		merchantID = "";
		remark = "";
		fromProductCode = "";
		toProductCode = "";
	}

	public String getFromCCY() {
		return fromCCY;
	}

	public void setFromCCY(String fromCCY) {
		this.fromCCY = fromCCY;
	}

	public String getToCCY() {
		return toCCY;
	}

	public void setToCCY(String toCCY) {
		this.toCCY = toCCY;
	}

	public String getCreditIBTKey() {
		return creditIBTKey;
	}

	public String getCustomerID() {
		return customerID;
	}

	public String getDebitIBTKey() {
		return debitIBTKey;
	}

	public String getFromAccNumber() {
		return pFromAccNumber;
	}

	public String getFromBranchCode() {
		return FromBranchCode;
	}

	public String getFromName() {
		return fromName;
	}

	public String getFromNrc() {
		return fromNrc;
	}

	public String getFromProductCode() {
		return fromProductCode;
	}

	public String getFromZone() {
		return FromZone;
	}

	public boolean getIsAgent() {
		return isAgent;
	}

	public boolean getIsCreditAgent() {
		return isCreditAgent;
	}

	public double getLimitAmount() {
		return LimitAmount;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public int getOnlineSerialNo() {
		return onlineSerialNo;
	}

	public String getReferenceNo() {
		return ReferenceNo;
	}

	public String getRemark() {
		return remark;
	}

	public String getToAccNumber() {
		return pToAccNumber;
	}

	public String getToBranchCode() {
		return ToBranchCode;
	}

	public String getToName() {
		return toName;
	}

	public String getToNrc() {
		return toNrc;
	}

	public String getToProductCode() {
		return toProductCode;
	}

	public String getToZone() {
		return ToZone;
	}

	public int getTransferKey() {
		return transferKey;
	}

	public String getUserID() {
		return UserID;
	}

	public boolean isFinishCutOffTime() {
		return isFinishCutOffTime;
	}

	public boolean isSameAgent() {
		return isSameAgent;
	}

	public void setCreditIBTKey(String creditIBTKey) {
		this.creditIBTKey = creditIBTKey;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public void setDebitIBTKey(String debitIBTKey) {
		this.debitIBTKey = debitIBTKey;
	}

	public void setFinishCutOffTime(boolean isFinishCutOffTime) {
		this.isFinishCutOffTime = isFinishCutOffTime;
	}

	public void setFromAccNumber(String pFromAccNumber) {
		this.pFromAccNumber = pFromAccNumber;
	}

	public void setFromBranchCode(String fromBranchCode) {
		FromBranchCode = fromBranchCode;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	public void setFromNrc(String fromNrc) {
		this.fromNrc = fromNrc;
	}

	public void setFromProductCode(String fromProductCode) {
		this.fromProductCode = fromProductCode;
	}

	public void setFromZone(String fromZone) {
		FromZone = fromZone;
	}

	public void setIsAgent(boolean isAgent) {
		this.isAgent = isAgent;
	}

	public void setIsCreditAgent(boolean isCreditAgent) {
		this.isCreditAgent = isCreditAgent;
	}

	public void setLimitAmount(double limitAmount) {
		LimitAmount = limitAmount;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public void setOnlineSerialNo(int onlineSerialNo) {
		this.onlineSerialNo = onlineSerialNo;
	}

	public void setReferenceNo(String referenceNo) {
		ReferenceNo = referenceNo;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public void setSameAgent(boolean isSameAgent) {
		this.isSameAgent = isSameAgent;
	}

	public void setToAccNumber(String pToAccNumber) {
		this.pToAccNumber = pToAccNumber;
	}

	public void setToBranchCode(String toBranchCode) {
		ToBranchCode = toBranchCode;
	}

	public void setToName(String toName) {
		this.toName = toName;
	}

	public void setToNrc(String toNrc) {
		this.toNrc = toNrc;
	}

	public void setToProductCode(String toProductCode) {
		this.toProductCode = toProductCode;
	}

	public void setToZone(String toZone) {
		ToZone = toZone;
	}

	public void setTransferKey(int transferKey) {
		this.transferKey = transferKey;
	}

	public void setUserID(String userID) {
		UserID = userID;
	}

}
