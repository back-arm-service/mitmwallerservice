package com.nirvasoft.rp.shared.icbs;

import java.util.ArrayList;

import com.nirvasoft.rp.shared.AccountCustomerData;

public class AccountData {

	public static int Status_New = 0;

	public static int Status_Active = 0;
	public static int Status_Suspend = 1;
	public static int Status_ClosePending = 2;
	public static int Status_Closed = 3;
	public static int Status_Dormant = 4;
	public static int Status_Lost = 5;
	public static int Status_Damage = 6;
	public static int Status_StoppedPayment = 7;
	private String pAccountNumber = "";

	private String pProduct = "";
	private String pType = "";
	private String pBranchCode = "";
	private int pZoneCode;
	private String pCurrencyCode = "";
	private double pCurRate;
	private double pOpeningBalance;
	private double pMinimumBalance;
	private String pOpeningDate = "";
	private double pCurrentBalance = 0;
	private String pClosingDate = "";
	private String pLastUpdate = "";
	private int pStatus;
	private String pLastTransDate = "";
	private String pDrawingType = "";
	private String pAccountName = "";
	private String pAccountNrc="";//ndh
	private String pSAccNo="";//ndh
	private boolean glStatus;
	

	public boolean isGlStatus() {
		return glStatus;
	}

	public void setGlStatus(boolean glStatus) {
		this.glStatus = glStatus;
	}

	public String getpSAccNo() {
		return pSAccNo;
	}

	public void setpSAccNo(String pSAccNo) {
		this.pSAccNo = pSAccNo;
	}

	public String getpAccountNrc() {
		return pAccountNrc;
	}

	public void setpAccountNrc(String pAccountNrc) {
		this.pAccountNrc = pAccountNrc;
	}

	private String pAccountDescription = "";
	private String pAccountID = "";
	private String pShortAccountCode = "";
	private String pPassbook = "";
	private String pCardNumber = "";
	private int pCardPriority;
	private double pCardLimit;
	private String pRemark = "";
	private String pCashInHandGL = "";
	private String pProductGL = "";
	// saving account
	private double IntToBePaid;

	// current accont
	private double OdLimit;

	private String OdExpDate = "";
	private double IntToBeCollected;
	private int BusinessType;
	private double Collateral;
	private String ApproveNo = "";
	private String ApproveId = "";
	private int SecurityType;
	private int IsOD;
	private String SanctionDate = "";
	private int ODStatus;
	private double TODLimit;
	private String TODExpDate = "";
	private String TODSanctionDate = "";
	private double SystemTOD;
	private int IsCommit;
	private double pAvailableBalance;
	// CLHeader
	private int LastTransNo;

	private int LastLineNo;
	private int LastPage;
	// PBHeader
	private int BeginTransNo;

	// PassBookHistory
	private String PBSerialNo = "";
	private String IssueDate = "";
	private int PBStatus;
	private int zone;
	private double pInterest;
	private double pInterestRate;
	private int pRef;
	private double pTenure;
	private ArrayList<AccBarData> pAccBarDatas;
	// public CreditCardlimitAmountData crditcardData = new
	// CreditCardlimitAmountData();
	// Card A/C
	private String pCardAccountNumber;

	// AccountRevaluate
	private double pBaseBalance = 0;

	private double pBaseBalanceWithCurrencyRate = 0;
	private double pDifferentBaseBalance = 0;
	ArrayList<PBFixedDepositAccountData> ppbFixedDeAcDataList;
	private PBFixedDepositAccountData pbFixedDeAcData;
	// Fixed Deposit ==> WHA
	private FixedDepositAccountData pFixedDepositAccountData;
	private ArrayList<AccountLinksData> pAccountLinkDatas;
	private boolean isExpire;
	private ArrayList<AccountCustomerData> pAccountCustomers;

	public AccountData() {
		ClearProperty();
	}

	private void ClearProperty() {
		pCurrencyCode = "MMK";
		pZoneCode = 0;
		pCurRate = 1;
		pStatus = 0;
		pAccountID = "";
		pAccountName = "";
		pDrawingType = "";
		pShortAccountCode = "";
		pRemark = "";
		IntToBePaid = 0;
		ApproveId = "";
		ApproveNo = "";
		LastTransNo = 0;
		LastLineNo = 0;
		LastPage = 0;
		PBSerialNo = "1";
		PBStatus = 1;
		pCashInHandGL = "";
		pProductGL = "";
		pCardAccountNumber = "";
		pAccountNumber = "";
		pBaseBalance = 0;
		pBaseBalanceWithCurrencyRate = 0;
		pDifferentBaseBalance = 0;

		isExpire = false;
	}

	public ArrayList<AccBarData> getAccountBarDatas() {
		return pAccBarDatas;
	}

	public ArrayList<AccountCustomerData> getAccountCustomers() {
		return pAccountCustomers;
	}

	public String getAccountDescription() {
		return pAccountDescription;
	}

	public String getAccountID() {
		return pAccountID;
	}

	public ArrayList<AccountLinksData> getAccountLinkDatas() {
		return pAccountLinkDatas;
	}

	public String getAccountName() {
		return pAccountName;
	}

	public String getAccountNumber() {
		return pAccountNumber;
	}

	public String getApproveID() {
		return ApproveId;
	}

	public String getApproveNo() {
		return ApproveNo;
	}

	public double getAvailableBalance() {
		return pAvailableBalance;
	}

	public double getBaseBalancce() {
		return pBaseBalance;
	}

	public double getBaseBalanceWithCurrencyRate() {
		return pBaseBalanceWithCurrencyRate;
	}

	public int getBeginTransNo() {
		return BeginTransNo;
	}

	public String getBranchCode() {
		return pBranchCode;
	}

	public int getBusinessType() {
		return BusinessType;
	}

	public String getCardAccountNumber() {
		return pCardAccountNumber;
	}

	public double getCardLimit() {
		return pCardLimit;
	}

	public String getCardNumber() {
		return pCardNumber;
	}

	public int getCardPriority() {
		return pCardPriority;
	}

	public String getCashInHandGL() {
		return pCashInHandGL;
	}

	public String getClosingDate() {
		return pClosingDate;
	}

	public double getCollateral() {
		return Collateral;
	}

	public String getCurrencyCode() {
		return pCurrencyCode;
	}

	public double getCurrencyRate() {
		return pCurRate;
	}

	public double getCurrentBalance() {
		return pCurrentBalance;
	}

	public double getDifferentBaseBalance() {
		return pDifferentBaseBalance;
	}

	public String getDrawingType() {
		return pDrawingType;
	}

	public FixedDepositAccountData getFixedDepositAccount() {
		return pFixedDepositAccountData;
	}

	public double getInterest() {
		return pInterest;
	}

	public double getInterestRate() {
		return pInterestRate;
	}

	public double getIntToBeCollected() {
		return IntToBeCollected;
	}

	public double getIntToBePaid() {
		return IntToBePaid;
	}

	public int getIsCommit() {
		return IsCommit;
	}

	public int getIsOD() {
		return IsOD;
	}

	public String getIssueDate() {
		return IssueDate;
	}

	public int getLastLineNo() {
		return LastLineNo;
	}

	public int getLastPage() {
		return LastPage;
	}

	public String getLastTransDate() {
		return pLastTransDate;
	}

	public int getLastTransNo() {
		return LastTransNo;
	}

	public String getLastUpdate() {
		return pLastUpdate;
	}

	public double getMinimumBalance() {
		return pMinimumBalance;
	}

	public String getOdExpDate() {
		return OdExpDate;
	}

	public double getOdLimit() {
		return OdLimit;
	}

	public int getODStatus() {
		return ODStatus;
	}

	public double getOpeningBalance() {
		return pOpeningBalance;
	}

	public String getOpeningDate() {
		return pOpeningDate;
	}

	public String getPassbook() {
		return pPassbook;
	}

	public PBFixedDepositAccountData getPBFixedDepAccountData() {
		return pbFixedDeAcData;
	}

	public String getPBSerialNo() {
		return PBSerialNo;
	}

	public int getPBStatus() {
		return PBStatus;
	}

	public String getProduct() {
		return pProduct;
	}

	public String getProductGL() {
		return pProductGL;
	}

	public int getRef() {
		return pRef;
	}

	public String getRemark() {
		return pRemark;
	}

	public String getSanctionDate() {
		return SanctionDate;
	}

	public int getSecurityType() {
		return SecurityType;
	}

	public String getShortAccountCode() {
		return pShortAccountCode;
	}

	public int getStatus() {
		return pStatus;
	}

	public double getSystemTOD() {
		return SystemTOD;
	}

	public double getTenure() {
		return pTenure;
	}

	public String getTODExpDate() {
		return TODExpDate;
	}

	public double getTODLimit() {
		return TODLimit;
	}

	public String getTODSanctionDate() {
		return TODSanctionDate;
	}

	public String getType() {
		return pType;
	}

	public int getZone() {
		return zone;
	}

	public int getZoneCode() {
		return pZoneCode;
	}

	/*
	 * public CreditCardlimitAmountData getCrditcardData() { return
	 * crditcardData; } public void setCrditcardData(CreditCardlimitAmountData
	 * crditcardData) { this.crditcardData = crditcardData; }
	 */
	public boolean isExpire() {
		return isExpire;
	}

	public void setAccountBarDatas(ArrayList<AccBarData> p) {
		pAccBarDatas = p;
	}

	public void setAccountCustomers(ArrayList<AccountCustomerData> p) {
		pAccountCustomers = p;
	}

	public void setAccountDescription(String p) {
		pAccountDescription = p;
	}

	public void setAccountID(String p) {
		pAccountID = p;
	}

	public void setAccountLinkDatas(ArrayList<AccountLinksData> pAccountLinkDatas) {
		this.pAccountLinkDatas = pAccountLinkDatas;
	}

	public void setAccountName(String p) {
		pAccountName = p;
	}

	public void setAccountNumber(String p) {
		pAccountNumber = p;
	}

	public void setApproveID(String p) {
		this.ApproveId = p;
	}

	public void setApproveNo(String p) {
		this.ApproveNo = p;
	}

	public void setAvailableBalance(double pAvailableBalance) {
		this.pAvailableBalance = pAvailableBalance;
	}

	public void setBaseBalance(double p) {
		pBaseBalance = p;
	}

	public void setBaseBalanceWithCurrencyRate(double p) {
		pBaseBalanceWithCurrencyRate = p;
	}

	public void setBeginTransNo(int p) {
		this.BeginTransNo = p;
	}

	public void setBranchCode(String p) {
		pBranchCode = p;
	}

	public void setBusinessType(int p) {
		this.BusinessType = p;
	}

	public void setCardAccountNumber(String pCardAccountNumber) {
		this.pCardAccountNumber = pCardAccountNumber;
	}

	public void setCardLimit(double p) {
		pCardLimit = p;
	}

	public void setCardNumber(String p) {
		pCardNumber = p;
	}

	public void setCardPriority(int p) {
		pCardPriority = p;
	}

	public void setCashInHandGL(String p) {
		pCashInHandGL = p;
	}

	public void setCheckIssue(ArrayList<CheckIssueData> pCheckIssue) {
	}

	public void setClosingDate(String p) {
		pClosingDate = p;
	}

	public void setCollateral(double p) {
		this.Collateral = p;
	}

	public void setCurrencyCode(String p) {
		pCurrencyCode = p;
	}

	public void setCurrencyRate(double p) {
		pCurRate = p;
	}

	public void setCurrentBalance(double p) {
		pCurrentBalance = p;
	}

	public void setDifferentBaseBalance(double p) {
		pDifferentBaseBalance = p;
	}

	public void setDrawingType(String p) {
		pDrawingType = p;
	}

	public void setExpire(boolean isExpire) {
		this.isExpire = isExpire;
	}

	public void setFixedDepositAccount(FixedDepositAccountData p) {
		pFixedDepositAccountData = p;
	}

	public void setInterest(double pInterest) {
		this.pInterest = pInterest;
	}

	public void setInterestRate(double pInterestRate) {
		this.pInterestRate = pInterestRate;
	}

	public void setIntToBeCollected(double p) {
		this.IntToBeCollected = p;
	}

	public void setIntToBePaid(double p) {
		this.IntToBePaid = p;
	}

	public void setIsCommit(int p) {
		this.IsCommit = p;
	}

	public void setIsOD(int p) {
		this.IsOD = p;
	}

	public void setIssueDate(String p) {
		this.IssueDate = p;
	}

	public void setLastLineNo(int p) {
		this.LastLineNo = p;
	}

	public void setLastPage(int p) {
		this.LastPage = p;
	}

	public void setLastTransDate(String p) {
		pLastTransDate = p;
	}

	public void setLastTransNo(int p) {
		this.LastTransNo = p;
	}

	public void setLastUpdate(String p) {
		pLastUpdate = p;
	}

	public void setMinimumBalance(double p) {
		pMinimumBalance = p;
	}

	public void setOdExpDate(String p) {
		this.OdExpDate = p;
	}

	public void setOdLimit(double p) {
		this.OdLimit = p;
	}

	public void setODStatus(int p) {
		this.ODStatus = p;
	}

	public void setOpeningBalance(double p) {
		pOpeningBalance = p;
	}

	public void setOpeningDate(String p) {
		pOpeningDate = p;
	}

	public void setPassbook(String p) {
		pPassbook = p;
	}

	public void setPBFixedDepAccountData(PBFixedDepositAccountData pbFixedDeAcData) {
		this.pbFixedDeAcData = pbFixedDeAcData;
	}

	public void setPBSerialNo(String p) {
		this.PBSerialNo = p;
	}

	public void setPBStatus(int p) {
		this.PBStatus = p;
	}

	public void setProduct(String p) {
		pProduct = p;
	}

	public void setProductGL(String p) {
		pProductGL = p;
	}

	public void setRef(int pRef) {
		this.pRef = pRef;
	}

	public void setRemark(String p) {
		pRemark = p;
	}

	public void setSanctionDate(String p) {
		this.SanctionDate = p;
	}

	public void setSecurityType(int p) {
		this.SecurityType = p;
	}

	public void setShortAccountCode(String p) {
		pShortAccountCode = p;
	}

	public void setStatus(int i) {
		pStatus = i;
	}

	public void setSystemTOD(double p) {
		this.SystemTOD = p;
	}

	public void setTenure(double pTenure) {
		this.pTenure = pTenure;
	}

	public void setTODExpDate(String p) {
		this.TODExpDate = p;
	}

	public void setTODLimit(double p) {
		this.TODLimit = p;
	}

	public void setTODSanctionDate(String p) {
		this.TODSanctionDate = p;
	}

	public void setType(String p) {
		pType = p;
	}

	public void setZone(int zone) {
		this.zone = zone;
	}

	public void setZoneCode(int p) {
		this.pZoneCode = p;
	}
}
