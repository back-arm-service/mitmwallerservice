package com.nirvasoft.rp.shared.icbs;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

import com.nirvasoft.rp.shared.AgentTransferReq;

@XmlRootElement
public class AgentEncashData {
	private String code;
	private String desc;
	private AgentTransferReq[] data = null;
	private String RefNo;
	private String fromid;
	private String toid;

	public AgentEncashData() {
		clearProperties();
	}

	private void clearProperties() {
		this.fromid = "";
		this.code = "";
		this.desc = "";
		this.data = null;
		this.RefNo = "";
		this.toid = "";
		this.data = null;
	}

	public String getCode() {
		return code;
	}

	public AgentTransferReq[] getData() {
		return data;
	}

	public String getDesc() {
		return desc;
	}

	public String getFromid() {
		return fromid;
	}

	public String getRefNo() {
		return RefNo;
	}

	public String getToid() {
		return toid;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setData(AgentTransferReq[] data) {
		this.data = data;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setFromid(String fromid) {
		this.fromid = fromid;
	}

	public void setRefNo(String refNo) {
		RefNo = refNo;
	}

	public void setToid(String toid) {
		this.toid = toid;
	}

	@Override
	public String toString() {
		return "{fromid=" + fromid + ", code=" + code + ", desc=" + desc + ", RefNo=" + RefNo + ", toid=" + toid
				+ ",data=" + Arrays.toString(data) + "}";
	}
}
