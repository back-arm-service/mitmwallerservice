package com.nirvasoft.rp.shared.icbs;

import java.io.Serializable;

public class DenoAndTrData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4235862923369471102L;
	private String pCounterID;
	private String pEntryDate;
	private String pSerialNo;
	private String pT1;
	private String pT2;
	private long pTransNo;
	private long pTransRef;

	public DenoAndTrData() {
		clearProperty();
	}

	private void clearProperty() {
		pCounterID = "";
		pEntryDate = "";
		pSerialNo = "";
		pT1 = "";
		pT2 = "";
		pTransNo = 0;
		pTransRef = 0;
	}

	public String getCounterID() {
		return pCounterID;
	}

	public String getEntryDate() {
		return pEntryDate;
	}

	public String getSerialNo() {
		return pSerialNo;
	}

	public String getT1() {
		return pT1;
	}

	public String getT2() {
		return pT2;
	}

	public long getTransNo() {
		return pTransNo;
	}

	public long getTransRef() {
		return pTransRef;
	}

	public void setCounterID(String p) {
		pCounterID = p;
	}

	public void setEntryDate(String p) {
		pEntryDate = p;
	}

	public void setSerialNo(String p) {
		pSerialNo = p;
	}

	public void setT1(String p) {
		pT1 = p;
	}

	public void setT2(String p) {
		pT2 = p;
	}

	public void setTransNo(long p) {
		pTransNo = p;
	}

	public void setTransRef(long p) {
		pTransRef = p;
	}

}
