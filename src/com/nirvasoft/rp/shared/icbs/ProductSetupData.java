package com.nirvasoft.rp.shared.icbs;

import java.io.Serializable;

public class ProductSetupData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4819975466369224082L;
	private String pProductId;
	private String pProductType;
	private double pMinBalance;
	private double pMinOpeningBalance;
	private double pMinWithdrawal;
	private int pMultipleOfp;
	private double pMultipleOf;
	private boolean pIsPassBook;
	private boolean pIsOpeningBalance;
	private boolean pIsOverdraftFacility;
	private boolean pIsReference;
	private String pProductName;

	public void clearProperty() {
		pProductId = "";
		pProductType = "";
		pMinBalance = 0;
		pMinOpeningBalance = 0;// numeric(18, 2) Unchecked
		pMinWithdrawal = 0;// numeric(18, 2) Unchecked
		pMultipleOfp = 0;// int Unchecked
		pMultipleOf = 0;// numeric(18, 2) Unchecked
		pIsPassBook = false; // bit Unchecked
		pIsOpeningBalance = false; // bit Unchecked
		pIsOverdraftFacility = false;// bit Unchecked
		pIsReference = false;// bit Unchecked
		pProductName = "";// varchar(100) Unchecked
	}

	public double getMinBalance() {
		return pMinBalance;
	}

	public double getMinOpeningBalance() {
		return pMinOpeningBalance;
	}

	public double getMinWithdrawal() {
		return pMinWithdrawal;
	}

	public double getMultipleOf() {
		return pMultipleOf;
	}

	public int getMultipleOfp() {
		return pMultipleOfp;
	}

	public String getProductId() {
		return pProductId;
	}

	public String getProductName() {
		return pProductName;
	}

	public String getProductType() {
		return pProductType;
	}

	public boolean isIsOpeningBalance() {
		return pIsOpeningBalance;
	}

	public boolean isIsOverdraftFacility() {
		return pIsOverdraftFacility;
	}

	public boolean isIsPassBook() {
		return pIsPassBook;
	}

	public boolean isIsReference() {
		return pIsReference;
	}

	public void setIsOpeningBalance(boolean pIsOpeningBalance) {
		this.pIsOpeningBalance = pIsOpeningBalance;
	}

	public void setIsOverdraftFacility(boolean pIsOverdraftFacility) {
		this.pIsOverdraftFacility = pIsOverdraftFacility;
	}

	public void setIsPassBook(boolean pIsPassBook) {
		this.pIsPassBook = pIsPassBook;
	}

	public void setIsReference(boolean pIsReference) {
		this.pIsReference = pIsReference;
	}

	public void setMinBalance(double pMinBalance) {
		this.pMinBalance = pMinBalance;
	}

	public void setMinOpeningBalance(double minOpeningBalance) {
		pMinOpeningBalance = minOpeningBalance;
	}

	public void setMinWithdrawal(double pMinWithdrawal) {
		this.pMinWithdrawal = pMinWithdrawal;
	}

	public void setMultipleOf(double pMultipleOf) {
		this.pMultipleOf = pMultipleOf;
	}

	public void setMultipleOfp(int multipleOfp) {
		pMultipleOfp = multipleOfp;
	}

	public void setProductId(String pProductId) {
		this.pProductId = pProductId;
	}

	public void setProductName(String pProductName) {
		this.pProductName = pProductName;
	}

	public void setProductType(String pProductType) {
		this.pProductType = pProductType;
	}

}
