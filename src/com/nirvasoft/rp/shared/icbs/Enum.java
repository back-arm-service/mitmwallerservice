package com.nirvasoft.rp.shared.icbs;

/* 
TUN THURA THET 2011 04 21
*/
public class Enum {
	public enum AppServerType {
		TextDB, OracleDB, MSSQLDB, MySQLDB, Jboss, GlassFish
	}

	public enum Currency {
		Kyats, // Kyats=0
		Dollars, // US$=1,S$=2
		FEC, // FEC=3
		EURO // EURO=4
	}

	public enum DataTypeCri {
		Equal, NotEqual, GreaterThan, GreaterThanEqual, LessThan, LessThanEqual, Contain, BeginWith, EndWith, Between, In
	}

	public enum DateFilterOperator {
		All, Equal, GreaterThan, LessThan, GreaterThanEqual, LessThanEqual, Between
	}

	public enum DenoType {
		Opening, Withdraw, Deposit, TransferFrom, TransferTo, Exchange
	}

	public enum FunctionID {
		CashInHand, IBSOtherBranch, IBSLocalBranch, IBSOtherBank, IBSLocalBank, Cash, RemitOutOnline, RemitInOnline, RemitOutOnlCom, IBT, IBTCom, IBTCom3, PO, POC, FDI, FDA, FDS, OBR, OBRComm, ODI, ODS, ODP, ADJ, GC, GCComm, CYP, CYPEQU, RMT, RMTComm, HP, HPINT, HPComm, HPMERLOC, HPPEN, HPMER, HPSVC, LAI, LAS, LAP, ATMDEP, POR, IBS, CAI, CAA, ODC, ODNPL, SAI, SAA, CLN, SPI, SPA, CDI, CDA, CCI, CCS, CCC, CCP, MERCOM
	}

	public enum MessageType {
		Info, Success, Warning, Error
	}

	public enum NumFilterOperator {
		Equal, GreaterThan, LessThan, GreaterThanEqual, LessThanEqual, Between
	}

	public enum ProductType {
		CurrentAccount, SavingsAccount, FixedDepositAccount, LoanAccount, R1, R2, CashCardAccount, R3, SecureCashCardAccount
	}

	public enum RemitDrawingTransType {
		NrcToNrc, NrcToAccNo, AccNoToAccNo, AccNoToNrc, GLToNrc, GLToAccNo
	}

	public enum SearchControlType {
		Auto, SuggestBox, ListBox, MultiCombo
	}

	public enum SearchOptionDataType {
		String, Number, DateTime, None
	}

	public enum StandardCurrency {
		MMK, USD, SGD, FEC, EUR
	}

	public enum SuggestType {
		None, Bank, AllBranch, Branch, CustomerType, AccountStatus, GCStatus, GCType, OthEncashStatus, OtherBank, OtherBranch, POType, FIStatus, SIType, IntType, RMTDrawingStatus, AODFStatus, HPStatus, CollateralInfoStatus, CollateralProductType, CollateralInformationStatus, CounterType, BarType, Currency, LocalBranch, CurCode, ComType, SMSRoleType, MJunctionStatus, IBTStatus
	}

	public enum TransCode {
		Deposit, Withdraw, TransferFrom, TransferTo, MultipleTrnasferFrom, MultipleTransferTo, ATMIBTTransferFrom, ATMIBTTransferTo, ScheduleCashDr, ScheduleCashCr, ScheduleTransferDr, ScheduleTransferCr, FixedDepositIntWdl// HMT
		, PoCashDr, PoCashCr, PoTransferDr, PoTransferCr // HMT
		, LoanIntCollectDr, LoanIntCollectCr, HPTrDr, HPTrCr, ODIntCollectDr, ODIntCollectCr, AccountLinkDr, AccountLinkCr // HMT
		, LoanLimitDr, LoanLimitCr, GcCashDr, GcCashCr, GcTransferDr, GcTransferCr, RemitTransferDr, RemitTransferCr, RemitCashDr, RemitCashCr, RemitInWardDr, RemitInWardCr, FixedInterestDr, FixedAutoRenewDr, FDWDLDr, FDWDLCr, FixedAutoRenewCr, FixedInterestCr, FDCashWDL, SettlementCashCr, SettlementCashDr, SettlementTrCr, SettlementTrDr, POSTrCr, POSTrDr, SettlementFeeCashCr, SettlementFeeCashDr, POSFeeTrCr, POSFeeTrDr // HMT
																																																																																																										// for
																																																																																																										// Settlement
		, OtherBankRemitOutCash, OtherBankRemitOutDr, OtherBankRemitOutCr, RevaluateFrom, RevaluateTo, RevaluateCashDr, RevaluateCashCr, AllowLoanLimitDr, AllowLoanLimitCr, LoanRepaymentDr, LoanRepaymentCr, CaIntDr, CaIntCr, CurrentIntWdlDr, CurrentIntWdlCr, CurrentIntCashDr, SavingsIntWdlDr, SavingsIntWdlCr, SavingsIntCashDr, SaIntDr, SaIntCr, ODDr, ODCr, ODDrNPL, ODCrNPL, ODSCrNPL
	}

	public enum TxTFilterOperator {
		Equal, Contain, BeginWith, EndWith, IN
	}
}
