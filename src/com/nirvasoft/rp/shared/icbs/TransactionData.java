package com.nirvasoft.rp.shared.icbs;

import java.io.Serializable;
import java.util.ArrayList;

import com.nirvasoft.rp.util.SharedUtil;

/* 
TUN THURA THET 2011 04 21
*/
public class TransactionData implements Serializable {
	private static final long serialVersionUID = -4515767800629231541L;

	private long pTransactionNumber;

	private long pTransactionReference;
	private int pTransactionType;
	private String pTransactionDate;
	private String pTransactionTime;
	private String pEffectiveDate;
	private String pBranchCode;
	private String pAccountNumber;
	private String pReferenceNumber; // check number
	private double pAmount;
	public String pRemark;
	private boolean chkInclusive;
	public String pWorkstation;
	public int pSerial;
	public String pUserID;
	public String pAuthorizerID;
	public String pDescription;
	public String pCurrencyCode;
	public double pCurrencyRate;
	public double pPrevBalance;
	public String pPreviousDate;
	public String pContraDate;
	public int pStatus;
	public String pAccRef;
	public String pSubRef;
	public int pSystemCode;
	public ArrayList<DenominationData> pDenorminations;
	private String pCashInHandGL;
	private String pProductGL;
	private double pBaseAmount;
	private String pRefNo;
	private boolean pIsByForceOD;
	private boolean pIsMobile;
	private boolean isComTransaction;
	// For Detail Trial
	private double pDebit = 0;

	private double pCredit = 0;
	private double pBaseDebit = 0;
	private double pBaseCredit = 0;
	private double pBaseOpening = 0;

	private double pBaseClosing = 0;
	private String pT1 = "";

	private String pVoucherSerialNo;

	private int chkowncr = 0;
	private double Comm;
	private int pProcessType = 0;

	public TransactionData() {
		ClearProperty();
	}

	public TransactionData(String AccNumber, String Description, double Amount, double Debit, double Credit,
			double BaseAmount, double BDebit, double BCredit, String CurrencyCode, String BranchCode) {
		this.pAccountNumber = AccNumber;
		this.pDescription = Description;
		this.pAmount = Amount;
		this.pDebit = Debit;
		this.pCredit = Credit;
		this.pBaseAmount = BaseAmount;
		this.pBaseDebit = BDebit;
		this.pBaseCredit = BCredit;
		this.pCurrencyCode = CurrencyCode;
		this.pBranchCode = BranchCode;
	}

	public TransactionData(String AccNumber, String AccDes, String SubGroup, String SubGroupDesc, double BaseDebit,
			double BaseCredit, double TrDebit, double TrCredit, double BaseOpening, double Amount, double BaseClosing) {
		this.pAccountNumber = AccNumber;
		this.pDescription = AccDes;
		this.pSubRef = SubGroup;
		this.pRemark = SubGroupDesc;
		this.pBaseDebit = BaseDebit;
		this.pBaseCredit = BaseCredit;
		this.pDebit = TrDebit;
		this.pCredit = TrCredit;
		this.pBaseOpening = BaseOpening;
		this.pAmount = Amount;
		this.pBaseClosing = BaseClosing;
	}

	private void ClearProperty() {
		pTransactionReference = 0;
		pTransactionType = 0;
		pTransactionDate = SharedUtil.formatDDMMYYYY2MIT("01/01/1900");
		pTransactionTime = SharedUtil.formatDDMMYYYY2MIT("01/01/1900");
		pEffectiveDate = SharedUtil.formatDDMMYYYY2MIT("01/01/1900");
		pBranchCode = "001";
		pAccountNumber = "";
		pReferenceNumber = ""; // check number
		pAmount = 0;
		pRemark = "";

		pWorkstation = "001";
		pSerial = 0;
		pUserID = "";
		pAuthorizerID = "";
		pDescription = "";
		pCurrencyCode = "MMK";
		pCurrencyRate = 1;
		pPrevBalance = 0;
		pPreviousDate = SharedUtil.formatDDMMYYYY2MIT("01/01/1900");
		;
		pContraDate = SharedUtil.formatDDMMYYYY2MIT("01/01/1900");
		;
		pStatus = 0;
		pAccRef = "";
		pSubRef = "";
		pSystemCode = 0;
		pCashInHandGL = "";
		pProductGL = "";
		pBaseAmount = 0;
		setIsByForceOD(false);
		pIsMobile = false;
		pRefNo = "";
		pVoucherSerialNo = "";
		pBaseOpening = 0;
		pBaseClosing = 0;
		pT1 = "";
		pDebit = 0;
		pCredit = 0;
		pBaseDebit = 0;
		pBaseCredit = 0;
		isComTransaction = false;
		Comm = 0;
		pProcessType = 0;
	}

	public String getAccountNumber() {
		return pAccountNumber;
	}

	public String getAccRef() {
		return pAccRef;
	}

	public double getAmount() {
		return pAmount;
	}

	public String getAuthorizerID() {
		return pAuthorizerID;
	}

	public double getBaseAmount() {
		return pBaseAmount;
	}

	public double getBaseClosing() {
		return pBaseClosing;
	}

	public double getBaseCredit() {
		return pBaseCredit;
	}

	public double getBaseDebit() {
		return pBaseDebit;
	}

	public double getBaseOpening() {
		return pBaseOpening;
	}

	public String getBranchCode() {
		return pBranchCode;
	}

	public String getCashInHandGL() {
		return pCashInHandGL;
	}

	public boolean getchkInclusive() {
		return chkInclusive;
	}

	public int getChkowncr() {
		return chkowncr;
	}

	public double getComm() {
		return Comm;
	}

	public String getContraDate() {
		return pContraDate;
	}

	public double getCredit() {
		return pCredit;
	}

	public String getCurrencyCode() {
		return pCurrencyCode;
	}

	public double getCurrencyRate() {
		return pCurrencyRate;
	}

	public double getDebit() {
		return pDebit;
	}

	public ArrayList<DenominationData> getDenominations() {
		return pDenorminations;
	}

	public String getDescription() {
		return pDescription;
	}

	public String getEffectiveDate() {
		return pEffectiveDate;
	}

	public boolean getIsByForceOD() {
		return pIsByForceOD;
	}

	public boolean getIsMobile() {
		return pIsMobile;
	}

	public double getPreviousBalance() {
		return pPrevBalance;
	}

	public String getPreviousDate() {
		return pPreviousDate;
	}

	public int getProcessType() {
		return pProcessType;
	}

	public String getProductGL() {
		return pProductGL;
	}

	public String getReferenceNumber() {
		return pReferenceNumber;
	}

	public String getRefNo() {
		return pRefNo;
	}

	public String getRemark() {
		return pRemark;
	}

	public int getSerial() {
		return pSerial;
	}

	public int getStatus() {
		return pStatus;
	}

	public String getSubRef() {
		return pSubRef;
	}

	public int getSystemCode() {
		return pSystemCode;
	}

	public String getT1() {
		return pT1;
	}

	public String getTransactionDate() {
		return pTransactionDate;
	}

	public long getTransactionNumber() {
		return pTransactionNumber;
	}

	public long getTransactionReference() {
		return pTransactionReference;
	}

	public String getTransactionTime() {
		return pTransactionTime;
	}

	public int getTransactionType() {
		return pTransactionType;
	}

	public String getUserID() {
		return pUserID;
	}

	public String getVoucherSerialNo() {
		return pVoucherSerialNo;
	}

	public String getWorkstation() {
		return pWorkstation;
	}

	public boolean isComTransaction() {
		return isComTransaction;
	}

	public void setAccountNumber(String p) {
		pAccountNumber = p;
	}

	public void setAccRef(String p) {
		pAccRef = p;
	}

	public void setAmount(double p) {
		pAmount = p;
	}

	public void setAuthorizerID(String p) {
		pAuthorizerID = p;
	}

	public void setBaseAmount(double p) {
		pBaseAmount = p;
	}

	public void setBaseClosing(double pBaseClosing) {
		this.pBaseClosing = pBaseClosing;
	}

	public void setBaseCredit(double pBaseCredit) {
		this.pBaseCredit = pBaseCredit;
	}

	public void setBaseDebit(double pBaseDebit) {
		this.pBaseDebit = pBaseDebit;
	}

	public void setBaseOpening(double pBaseOpening) {
		this.pBaseOpening = pBaseOpening;
	}

	public void setBranchCode(String p) {
		pBranchCode = p;
	}

	public void setCashInHandGL(String p) {
		pCashInHandGL = p;
	}

	public void setchkInclusive(boolean chkInclusive) {
		this.chkInclusive = chkInclusive;
	}

	public void setChkowncr(int chkowncr) {
		this.chkowncr = chkowncr;
	}

	public void setComm(double comm) {
		Comm = comm;
	}

	public void setComTransaction(boolean isComTransaction) {
		this.isComTransaction = isComTransaction;
	}

	public void setContraDate(String p) {
		pContraDate = p;
	}

	public void setCredit(double pCredit) {
		this.pCredit = pCredit;
	}

	public void setCurrencyCode(String p) {
		pCurrencyCode = p;
	}

	public void setCurrencyRate(double p) {
		pCurrencyRate = p;
	}

	public void setDebit(double pDebit) {
		this.pDebit = pDebit;
	}

	public void setDenominations(ArrayList<DenominationData> p) {
		pDenorminations = p;
	}

	public void setDescription(String p) {
		pDescription = p;
	}

	public void setEffectiveDate(String p) {
		pEffectiveDate = p;
	}

	public void setIsByForceOD(boolean pIsByForceOD) {
		this.pIsByForceOD = pIsByForceOD;
	}

	public void setIsMobile(boolean pIsMobile) {
		this.pIsMobile = pIsMobile;
	}

	public void setpRefNo(String pRefNo) {
		this.pRefNo = pRefNo;
	}

	public void setPreviousBalance(double p) {
		pPrevBalance = p;
	}

	public void setPreviousDate(String p) {
		pPreviousDate = p;
	}

	public void setProcessType(int pProcessType) {
		this.pProcessType = pProcessType;
	}

	public void setProductGL(String p) {
		pProductGL = p;
	}

	public void setReferenceNumber(String p) {
		pReferenceNumber = p;
	}

	public void setRemark(String p) {
		pRemark = p;
	}

	public void setSerial(int p) {
		pSerial = p;
	}

	public void setStatus(int p) {
		pStatus = p;
	}

	public void setSubRef(String p) {
		pSubRef = p;
	}

	public void setSystemCode(int p) {
		pSystemCode = p;
	}

	public void setT1(String pT1) {
		this.pT1 = pT1;
	}

	public void setTransactionDate(String p) {
		pTransactionDate = p;
	}

	public void setTransactionNumber(long p) {
		pTransactionNumber = p;
	}

	public void setTransactionReference(long p) {
		pTransactionReference = p;
	}

	public void setTransactionTime(String p) {
		pTransactionTime = p;
	}

	public void setTransactionType(int p) {
		pTransactionType = p;
	}

	public void setUserID(String p) {
		pUserID = p;
	}

	public void setVoucherSerialNo(String pVoucherSerialNo) {
		this.pVoucherSerialNo = pVoucherSerialNo;
	}

	public void setWorkstation(String p) {
		pWorkstation = p;
	}

}
