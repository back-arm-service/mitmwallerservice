package com.nirvasoft.rp.shared.icbs;

import java.io.Serializable;

public class AddressData implements Serializable {

	private static final long serialVersionUID = 6417036037645458132L;

	private String pHouseNo;

	private String pStreet;
	private String pWard;
	private String pTownship;
	private String pDistrict;
	private String pCity;
	private String pDivision;
	private String pCountry;
	private String pTel1;
	private String pTel2;
	private String pTel3;
	private String pEmail;
	private String pWebsite;
	private String pFacebook;
	private String pFax;
	private String pPostalCode;
	private String pStatus;

	public AddressData() {
		ClearProperty();
	}

	private void ClearProperty() {
		pHouseNo = "";
		pStreet = "";
		pWard = "";
		pTownship = "";
		pDistrict = "";
		pCity = "";
		pDivision = "";
		pCountry = "";
		pTel1 = "";
		pTel2 = "";
		pTel3 = "";
		pEmail = "";
		pWebsite = "";
		pFacebook = "";
		pFax = "";
		pPostalCode = "";
		pStatus = "";
	}

	public String getAddress() {
		String l_Address = "";
		l_Address += getHouseNo() + "";
		if (!getHouseNo().equals(""))
			l_Address += ",";
		l_Address += getStreet();
		if (!getStreet().equals(""))
			l_Address += ",";
		l_Address += getWard();
		if (!getWard().equals(""))
			l_Address += ",";
		l_Address += getTownship();
		if (!getTownship().equals(""))
			l_Address += ",";
		l_Address += getCity();
		if (!getCity().equals(""))
			l_Address += ",";
		l_Address += getDivision();
		if (!getDivision().equals(""))
			l_Address += ",";
		l_Address += getCountry();
		return l_Address;
	}

	public String getCity() {
		return pCity;
	}

	public String getCountry() {
		return pCountry;
	}

	public String getDistrict() {
		return pDistrict;
	}

	public String getDivision() {
		return pDivision;
	}

	public String getEmail() {
		return pEmail;
	}

	public String getFacebook() {
		return pFacebook;
	}

	public String getFax() {
		return pFax;
	}

	public String getHouseNo() {
		return pHouseNo;
	}

	public String getPostalCode() {
		return pPostalCode;
	}

	public String getStatus() {
		return pStatus;
	}

	public String getStreet() {
		return pStreet;
	}

	public String getTel1() {
		return pTel1;
	}

	public String getTel2() {
		return pTel2;
	}

	public String getTel3() {
		return pTel3;
	}

	public String getTels() {
		return pTel1 + (pTel2 == null || pTel2.equals("") ? "" : ("; " + pTel2))
				+ (pTel3 == null || pTel3.equals("") ? "" : ("; " + pTel3));
	}

	public String getTownship() {
		return pTownship;
	}

	public String getWard() {
		return pWard;
	}

	public String getWebsite() {
		return pWebsite;
	}

	public void setCity(String city) {
		pCity = city;
	}

	public void setCountry(String country) {
		pCountry = country;
	}

	public void setDistrict(String p) {
		pDistrict = p;
	}

	public void setDivision(String division) {
		pDivision = division;
	}

	public void setEmail(String p) {
		pEmail = p;
	}

	public void setFacebook(String p) {
		pFacebook = p;
	}

	public void setFax(String fax) {
		pFax = fax;
	}

	public void setHouseNo(String houseNo) {
		pHouseNo = houseNo;
	}

	public void setPostalCode(String postalCode) {
		pPostalCode = postalCode;
	}

	public void setStatus(String p) {
		pStatus = p;
	}

	public void setStreet(String street) {
		pStreet = street;
	}

	public void setTel1(String p) {
		pTel1 = p;
	}

	public void setTel2(String p) {
		pTel2 = p;
	}

	public void setTel3(String p) {
		pTel3 = p;
	}

	public void setTownship(String township) {
		pTownship = township;
	}

	public void setWard(String ward) {
		pWard = ward;
	}

	public void setWebsite(String p) {
		pWebsite = p;
	}
}