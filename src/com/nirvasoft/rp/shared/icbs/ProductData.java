package com.nirvasoft.rp.shared.icbs;

import java.io.Serializable;
import java.util.ArrayList;

public class ProductData implements Serializable {
	private static final long serialVersionUID = 5547754537939965583L;
	private String pProductID; // "CA"
	private String pProductOldID;
	private String pProductName; // "Current Account"
	private String pProductCode; // "01"

	// TXL Start
	private String pProductType; // CA or SA or FD etc.This is key for relation
									// between ProductSetup, ProductType and
									// ProductNumber tables.
	private String pInfoType;
	private int pStart;
	private int pLen;
	// TXL End

	// WHA
	private String pCurCode;
	// End of WHA

	private int key = 0;

	// Features
	private boolean pHasPassbook;
	private boolean pIsCard; // 1 to 1 Card Account eg. Cash Card, Cr Card
	private boolean pHasCheque;
	private boolean pHasCollateral;
	private boolean pHasHP;
	private long pProductFeatureType;
	private long pCollateralTable;
	private String pCashInHandGL;
	private String pProductGL;
	// End of Features

	private double pMinBalance;
	private double pMinOpeningBalance;
	private double pMinWithdrawal;
	private double pMultipleOfp;
	private double pMultipleOf;

	private int pProductStart;
	private int pProductLen;

	private int pBranchStart;
	private int pBranchLen;

	private int pCurrencyStart;
	private int pCurrencyLen;

	private int pTypeStart;
	private int pTypeLen;

	private int pSerialStart;
	private int pSerialLen;

	private int pCheckStart;
	private int pCheckLen;

	private int pOtherStart;
	private int pOtherLen;

	private int pReserveStart;
	private int pReserveLen;

	private String pProcessCode6;
	// HMT 2014 06 14
	private ArrayList<ProductSetupData> pProductSetupList = new ArrayList<ProductSetupData>();
	private String pAcccountTypeGL = "";
	private String pAccountTypeCashGL = "";
	private String pAccType = "";
	private boolean phasCertificate;

	public String getAcccountTypeGL() {
		return pAcccountTypeGL;
	}

	public String getAccountTypeCashGL() {
		return pAccountTypeCashGL;
	}

	public String getAccType() {
		return pAccType;
	}

	public int getBranchLen() {
		return pBranchLen;
	} // 3

	public int getBranchStart() {
		return pBranchStart;
	} // 3

	public String getCashInHandGL() {
		return pCashInHandGL;
	}

	public int getCheckLen() {
		return pCheckLen;
	} // 1

	public int getCheckStart() {
		return pCheckStart;
	} // 1

	public long getCollateralTable() {
		return pCollateralTable;
	}

	public String getCurCode() {
		return pCurCode;
	}

	public int getCurrencyLen() {
		return pCurrencyLen;
	} // 1

	public int getCurrencyStart() {
		return pCurrencyStart;
	} // 1

	public String getInfoType() {
		return pInfoType;
	}

	public int getKey() {
		return key;
	}

	public int getLen() {
		return pLen;
	}

	// TXL End
	public int getLength() {
		return pProductLen + pBranchLen + pTypeLen + pCurrencyLen + pReserveLen + pSerialLen + pCheckLen + pOtherLen;
	}

	public double getMinBalance() {
		return pMinBalance;
	}

	public double getMinOpeningBalance() {
		return pMinOpeningBalance;
	}

	public double getMinWithdrawal() {
		return pMinWithdrawal;
	}

	public double getMultipleOf() {
		return pMultipleOf;
	}

	public double getMultipleOfp() {
		return pMultipleOfp;
	}

	public int getOtherLen() {
		return pOtherLen;
	} // 0

	public int getOtherStart() {
		return pOtherStart;
	} // 0

	public String getpProductOldID() {
		return pProductOldID;
	}

	public String getProcessCode6() {
		return pProcessCode6;
	}

	public String getProductCode() {
		return pProductCode;
	}

	public long getProductFeatureType() {
		return pProductFeatureType;
	}

	public String getProductGL() {
		return pProductGL;
	}

	public String getProductID() {
		return pProductID;
	}

	public int getProductLen() {
		return pProductLen;
	} // 2

	public String getProductName() {
		return pProductName;
	}

	public ArrayList<ProductSetupData> getProductSetupList() {
		return pProductSetupList;
	}

	public int getProductStart() {
		return pProductStart;
	} // 2

	public String getProductType() {
		return pProductType;
	}

	public int getReserveLen() {
		return pReserveLen;
	} // 1

	public int getReserveStart() {
		return pReserveStart;
	} // 1

	public int getSerialLen() {
		return pSerialLen;
	} // 6

	public int getSerialStart() {
		return pSerialStart;
	} // 6

	public int getStart() {
		return pStart;
	}

	public int getTypeLen() {
		return pTypeLen;
	} // 2

	public int getTypeStart() {
		return pTypeStart;
	} // 2

	public boolean hasCertificate() {
		return phasCertificate;
	}

	public boolean hasCheque() {
		return pHasCheque;
	}

	public boolean hasCollateral() {
		return pHasCollateral;
	}

	public boolean hasHirePurchase() {
		return pHasHP;
	}

	public boolean hasPassbook() {
		return pHasPassbook;
	}

	public boolean isCard() {
		return pIsCard;
	}

	public void setAcccountTypeGL(String pAcccountTypeGL) {
		this.pAcccountTypeGL = pAcccountTypeGL;
	}

	public void setAccountTypeCashGL(String pAccountTypeCashGL) {
		this.pAccountTypeCashGL = pAccountTypeCashGL;
	}

	public void setAccType(String pAccType) {
		this.pAccType = pAccType;
	}

	public void setBranchLen(int p) {
		pBranchLen = p;
	} // 3

	public void setBranchStart(int p) {
		pBranchStart = p;
	} // 3

	public void setCashInHandGL(String p) {
		pCashInHandGL = p;
	}

	public void setCheckLen(int p) {
		pCheckLen = p;
	} // 16

	public void setCheckStart(int p) {
		pCheckStart = p;
	} // 16

	public void setCollateralTable(long p) {
		pCollateralTable = p;
	}

	public void setCurCode(String p) {
		this.pCurCode = p;
	}

	public void setCurrencyLen(int p) {
		pCurrencyLen = p;
	} // 8

	public void setCurrencyStart(int p) {
		pCurrencyStart = p;
	} // 8

	public void sethasCertificate(boolean phasCertificate) {
		this.phasCertificate = phasCertificate;
	}

	public void setHasCheque(boolean p) {
		pHasCheque = p;
	}

	public void setHasCollateral(boolean p) {
		pHasCollateral = p;
	}

	public void setHasHirePurchase(boolean p) {
		pHasHP = p;
	}

	public void setHasPassbook(boolean p) {
		pHasPassbook = p;
	}

	public void setInfoType(String p) {
		pInfoType = p;
	}

	public void setIsCard(boolean p) {
		pIsCard = p;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public void setLen(int p) {
		pLen = p;
	}

	public void setMinBalance(double p) {
		pMinBalance = p;
	}

	public void setMinOpeningBalance(double p) {
		pMinOpeningBalance = p;
	}

	public void setMinWithdrawal(double p) {
		pMinWithdrawal = p;
	}

	public void setMultipleOf(double p) {
		pMultipleOf = p;
	}

	public void setMultipleOfp(double p) {
		pMultipleOfp = p;
	}

	public void setOtherLen(int p) {
		pOtherLen = p;
	} // 0

	public void setOtherStart(int p) {
		pOtherStart = p;
	} // 0

	public void setpProductOldID(String pProductOldID) {
		this.pProductOldID = pProductOldID;
	}

	public void setProcessCode6(String pProcessCode6) {
		this.pProcessCode6 = pProcessCode6;
	}

	public void setProductCode(String p) {
		pProductCode = p;
	} // 01

	public void setProductFeatureType(long p) {
		pProductFeatureType = p;
	}

	public void setProductGL(String p) {
		pProductGL = p;
	}

	public void setProductID(String p) {
		pProductID = p;
	} // CA

	public void setProductLen(int p) {
		pProductLen = p;
	} // 1

	public void setProductName(String p) {
		pProductName = p;
	} // Current Account

	public void setProductSetupList(ArrayList<ProductSetupData> pProductSetupList) {
		this.pProductSetupList = pProductSetupList;
	}

	public void setProductStart(int p) {
		pProductStart = p;
	} // 1

	// TXL Start
	public void setProductType(String p) {
		pProductType = p;
	}

	public void setReserveLen(int p) {
		pReserveLen = p;
	} // 9

	public void setReserveStart(int p) {
		pReserveStart = p;
	} // 9

	public void setSerialLen(int p) {
		pSerialLen = p;
	} // 10

	public void setSerialStart(int p) {
		pSerialStart = p;
	} // 10

	public void setStart(int p) {
		pStart = p;
	}

	public void setTypeLen(int p) {
		pTypeLen = p;
	} // 6

	public void setTypeStart(int p) {
		pTypeStart = p;
	} // 6

}
