package com.nirvasoft.rp.shared.icbs;

import javax.xml.bind.annotation.XmlRootElement;

import com.nirvasoft.rp.data.BranchData;

@XmlRootElement
public class BranchSetupDataSet {
	private BranchData[] data;
	private String searchText;
	private int totalCount;
	private int currentPage;
	private int pageSize;

	public BranchSetupDataSet() {
		clearProperty();
	}

	private void clearProperty() {
		searchText = "";
		totalCount = 0;
		currentPage = 0;
		pageSize = 0;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public BranchData[] getData() {
		return data;
	}

	public int getPageSize() {
		return pageSize;
	}

	public String getSearchText() {
		return searchText;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setData(BranchData[] data) {
		this.data = data;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

}
