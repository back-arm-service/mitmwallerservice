
package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CheckOTPRequestData {
	private String userID;
	private String sessionID;
	private String otpCode;
	private String rKey;
	private String sKey;
	private String type;
	private String deviceID;
	private String fcmToken;

	public String getFcmToken() {
		return fcmToken;
	}

	public void setFcmToken(String fcmToken) {
		this.fcmToken = fcmToken;
	}

	public String getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}

	public CheckOTPRequestData() {
		clearProperties();
	}

	private void clearProperties() {
		this.userID = "";
		this.sessionID = "";
		rKey = "";
		this.otpCode = "";
		this.sKey = "";
		this.type = "";
	}

	public String getOtpCode() {
		return otpCode;
	}

	public String getrKey() {
		return rKey;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getsKey() {
		return sKey;
	}

	public String getType() {
		return type;
	}

	public String getUserID() {
		return userID;
	}

	public void setOtpCode(String otpCode) {
		this.otpCode = otpCode;
	}

	public void setrKey(String rKey) {
		this.rKey = rKey;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setsKey(String sKey) {
		this.sKey = sKey;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	@Override
	public String toString() {
		return "{userID=" + userID + ", sessionID=" + sessionID + ", otpCode=" + otpCode + ", rKey=" + rKey + ",sKey="
				+ sKey + ",type=" + type + "}";
	}

}
