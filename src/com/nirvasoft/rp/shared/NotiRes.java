package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class NotiRes {
	private String code;
	private String desc;
	private String notiCount;
	private NotiResData[] data;

	public NotiRes() {
		clearProperty();
	}

	private void clearProperty() {
		code = "";
		desc = "";
		notiCount = "";
		data = null;
	}

	public String getCode() {
		return code;
	}

	public NotiResData[] getData() {
		return data;
	}

	public String getDesc() {
		return desc;
	}

	public String getNotiCount() {
		return notiCount;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setData(NotiResData[] data) {
		this.data = data;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setNotiCount(String notiCount) {
		this.notiCount = notiCount;
	}

}
