package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class NewAccountRequestData {

	private String userID;
	private String sessionID;
	private String accNumber;
	private String accountType; // t2
	private int serialNo;

	public NewAccountRequestData() {
		clearProperty();
	}

	private void clearProperty() {
		userID = "";
		sessionID = "";
		accNumber = "";
		accountType = "";
		serialNo = 0;
	}

	public String getAccNumber() {
		return accNumber;
	}

	public String getAccountType() {
		return accountType;
	}

	public int getSerialNo() {
		return serialNo;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getUserID() {
		return userID;
	}

	public void setAccNumber(String accNumber) {
		this.accNumber = accNumber;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public void setSerialNo(int serialNo) {
		this.serialNo = serialNo;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setUserID(String userid) {
		this.userID = userid;
	}

	@Override
	public String toString() {
		return "NewAccountRequestData [userID=" + userID + ", sessionID=" + sessionID + ", accNumber=" + accNumber
				+ ", accountType=" + accountType + ", serialNo=" + serialNo + "]";
	}

}
