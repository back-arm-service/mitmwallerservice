package com.nirvasoft.rp.shared;

/* 
TUN THURA THET 2011 04 21
*/
import java.io.Serializable;
import java.util.ArrayList;

public class SystemData implements Serializable {

	private static final long serialVersionUID = 51525865987834667L;
	private String pNewLabel = "new*";
	private ArrayList<String> pSystemCodes;
	private ArrayList<String> pSuggests;
	private ArrayList<String> pTitles;
	private ArrayList<ReferenceData> pPersonTypes;
	private ArrayList<ReferenceData> pOrganizationTypes;
	private ArrayList<ReferenceData> pTownships;
	private ArrayList<ReferenceData> pDistricts;
	private ArrayList<ReferenceData> pDivisions;
	private ArrayList<ReferenceData> pCountries;
	private ArrayList<ReferenceData> pAccountTypes;
	private ArrayList<ReferenceData> pDrawingTypes;
	private ArrayList<ReferenceData> pCurrencyCodes;
	private ArrayList<ReferenceData> pBranchCodes;
	private ArrayList<ReferenceData> pAllBranchCodes;
	private ArrayList<ReferenceData> pStatusCodes;
	private ArrayList<ReferenceData> pACRelationTypes;
	private ArrayList<String> pICPrefixes;
	private ArrayList<String> pNationalities;
	private ArrayList<String> pRaces;
	private ArrayList<String> pReligions;
	private ArrayList<ProductData> pProducts;
	private ArrayList<ReferenceData> pUserLevels;
	private ArrayList<ReferenceData> pPBookStatus;
	private ArrayList<ReferenceData> pTitlesSex;
	private ArrayList<ReferenceData> pAddressRefByTownship;
	private ArrayList<ReferenceData> pButtons;
	ArrayList<ProductData> pProductType;
	private ArrayList<ReferenceData> pAccBarType;
	private ArrayList<String> pNrcs;

	private ArrayList<ReferenceData> pMJunctionStatus;

	// TXL Start
	private ArrayList<ProductData> pProductsType;
	private ArrayList<ProductData> pProductsNumber;
	private ArrayList<ProductData> pProductsConfiguration;
	private ArrayList<ReferenceData> pAccountStatus;
	private ArrayList<ReferenceData> pAccountFormat;
	private ArrayList<ProductData> pAccountSeparateNumber;
	private int logOffTime = 0;

	// TXL End
	private ArrayList<SystemSettingData> pSystemSettingDatas;
	// WHA
	private double pMaxATMTransAmount = 300000;

	// Currency
	private String pBaseCurCode;
	private double pBaseCurRate = 1;

	private String pBaseCurOperator = "/";
	private ArrayList<ProductData> pProductDatasCurrency;
	private ArrayList<ReferenceData> transactionDesc;

	public SystemData() {
		pBaseCurCode = "MMK";
		pBaseCurRate = 1;
		pBaseCurOperator = "/";
		pProductDatasCurrency = new ArrayList<ProductData>();
	}

	public ArrayList<ReferenceData> getAccBarType() {
		return pAccBarType;
	}

	public ArrayList<ReferenceData> getAccountFormat() {
		return pAccountFormat;
	}

	public ArrayList<ProductData> getAccountSeparateNumber() {
		return pAccountSeparateNumber;
	}

	public ArrayList<ReferenceData> getAccountStatus() {
		return pAccountStatus;
	}

	public ArrayList<ReferenceData> getAccountTypes() {
		return pAccountTypes;
	}

	public ArrayList<ReferenceData> getACRelationTypes() {
		return pACRelationTypes;
	}

	public ArrayList<ReferenceData> getAddressRefByTownship() {
		return pAddressRefByTownship;
	}

	public ArrayList<ReferenceData> getAllBranchCodes() {
		return pAllBranchCodes;
	}

	public double getATMMaxTransAmount() {
		return pMaxATMTransAmount;
	}

	public String getBaseCurCode() {
		return pBaseCurCode;
	}

	public String getBaseCurOperator() {
		return pBaseCurOperator;
	}

	public double getBaseCurRate() {
		return pBaseCurRate;
	}

	public ArrayList<ReferenceData> getBranchCodes() {
		return pBranchCodes;
	}

	public ArrayList<ReferenceData> getButtons() {
		return pButtons;
	}

	public ArrayList<ReferenceData> getCountries() {
		return pCountries;
	}

	public ArrayList<ReferenceData> getCurrencyCodes() {
		return pCurrencyCodes;
	}

	public ArrayList<ReferenceData> getDistricts() {
		return pDistricts;
	}

	public ArrayList<ReferenceData> getDivisions() {
		return pDivisions;
	}

	public ArrayList<ReferenceData> getDrawingTypes() {
		return pDrawingTypes;
	}

	public ArrayList<String> getICPrefixes() {
		return pICPrefixes;
	}

	public int getLogOffTime() {
		return this.logOffTime;
	}

	// WMMH Mobile junction status
	public ArrayList<ReferenceData> getMJunctionStatus() {
		return pMJunctionStatus;
	}

	public ArrayList<String> getNationalities() {
		return pNationalities;
	}

	public String getNewLabel() {
		return pNewLabel;
	}

	// wnnt
	public ArrayList<String> getNrcs() {
		return pNrcs;
	}

	public ArrayList<ReferenceData> getOrganizationTypes() {
		return pOrganizationTypes;
	}

	public ArrayList<ReferenceData> getPBookStatus() {
		return pPBookStatus;
	}

	public ArrayList<ReferenceData> getPersonTypes() {
		return pPersonTypes;
	}

	public ArrayList<ProductData> getProductDatasCurrency() {
		return pProductDatasCurrency;
	}

	public ArrayList<ProductData> getProducts() {
		return pProducts;
	}

	public ArrayList<ProductData> getProductsConfiguration() {
		return pProductsConfiguration;
	}

	public ArrayList<ProductData> getProductsNumber() {
		return pProductsNumber;
	}

	// TXL Start
	public ArrayList<ProductData> getProductsType() {
		return pProductsType;
	}

	public ArrayList<ProductData> getProductType() {
		return pProductType;
	}

	public ArrayList<String> getRaces() {
		return pRaces;
	}

	public ArrayList<String> getReligions() {
		return pReligions;
	}

	public ArrayList<ReferenceData> getStatusCodes() {
		return pStatusCodes;
	}

	public ArrayList<String> getSuggests() {
		return pSuggests;
	}

	public ArrayList<String> getSystemCodes() {
		return pSystemCodes;
	}

	public ArrayList<SystemSettingData> getSystemSettingDatas() {
		return pSystemSettingDatas;
	}// WHA ==> For SystemSettingData

	public ArrayList<String> getTitles() {
		return pTitles;
	}

	public ArrayList<ReferenceData> getTitlesSex() {
		return pTitlesSex;
	}

	public ArrayList<ReferenceData> getTownships() {
		return pTownships;
	}

	public ArrayList<ReferenceData> getTransactionDesc() {
		return this.transactionDesc;
	}

	public ArrayList<ReferenceData> getUserLevels() {
		return pUserLevels;
	}

	public void setAccBarType(ArrayList<ReferenceData> pAccBarType) {
		this.pAccBarType = pAccBarType;
	}

	public void setAccountFormat(ArrayList<ReferenceData> p) {
		pAccountFormat = p;
	}

	public void setAccountSeparateNumber(ArrayList<ProductData> p) {
		pAccountSeparateNumber = p;
	}

	public void setAccountStatus(ArrayList<ReferenceData> p) {
		pAccountStatus = p;
	}

	public void setAccountTypes(ArrayList<ReferenceData> p) {
		pAccountTypes = p;
	}

	public void setACRelationTypes(ArrayList<ReferenceData> p) {
		pACRelationTypes = p;
	}

	public void setAddressRefByTownship(ArrayList<ReferenceData> p) {
		pAddressRefByTownship = p;
	}

	public void setAllBranchCodes(ArrayList<ReferenceData> p) {
		pAllBranchCodes = p;
	}

	public void setATMMaxTransAmount(double p) {
		pMaxATMTransAmount = p;
	}

	public void setBaseCurCode(String pBaseCurCode) {
		this.pBaseCurCode = pBaseCurCode;
	}

	public void setBaseCurOperator(String p) {
		pBaseCurOperator = p;
	}

	public void setBaseCurRate(double p) {
		pBaseCurRate = p;
	}

	public void setBranchCodes(ArrayList<ReferenceData> p) {
		pBranchCodes = p;
	}

	public void setButtons(ArrayList<ReferenceData> p) {
		pButtons = p;
	}

	public void setCountries(ArrayList<ReferenceData> p) {
		pCountries = p;
	}

	public void setCurrencyCodes(ArrayList<ReferenceData> p) {
		pCurrencyCodes = p;
	}

	public void setDistricts(ArrayList<ReferenceData> p) {
		pDistricts = p;
	}

	public void setDivisions(ArrayList<ReferenceData> p) {
		pDivisions = p;
	}

	public void setDrawingTypes(ArrayList<ReferenceData> p) {
		pDrawingTypes = p;
	}

	public void setICPrefixes(ArrayList<String> p) {
		pICPrefixes = p;
	}

	public void setLogOffTime(int value) {
		if (value < 10) {
			this.logOffTime = 10;
		} else {
			this.logOffTime = value / 10;
		}
	}

	public void setMJunctionStatus(ArrayList<ReferenceData> p) {
		pMJunctionStatus = p;
	}

	public void setNationalities(ArrayList<String> p) {
		pNationalities = p;
	}

	public void setNrcs(ArrayList<String> p) {
		pNrcs = p;
	}

	public void setOrganizationTypes(ArrayList<ReferenceData> p) {
		pOrganizationTypes = p;
	}

	public void setPBookStatus(ArrayList<ReferenceData> p) {
		pPBookStatus = p;
	}

	// TXL End

	public void setPersonTypes(ArrayList<ReferenceData> p) {
		pPersonTypes = p;
	}

	public void setProductDatasCurrency(ArrayList<ProductData> p) {
		pProductDatasCurrency = p;
	}

	public void setProducts(ArrayList<ProductData> p) {
		pProducts = p;
	}

	public void setProductsConfiguration(ArrayList<ProductData> p) {
		pProductsConfiguration = p;
	}

	// 03 Jan 2012

	public void setProductsNumber(ArrayList<ProductData> p) {
		pProductsNumber = p;
	}

	public void setProductsType(ArrayList<ProductData> p) {
		pProductsType = p;
	}

	public void setProductType(ArrayList<ProductData> pProductType) {
		this.pProductType = pProductType;
	}

	public void setRaces(ArrayList<String> p) {
		pRaces = p;
	}

	public void setReligions(ArrayList<String> p) {
		pReligions = p;
	}

	public void setStatusCodes(ArrayList<ReferenceData> p) {
		pStatusCodes = p;
	}

	public void setSuggests(ArrayList<String> p) {
		pSuggests = p;
	}

	public void setSystemCodes(ArrayList<String> p) {
		pSystemCodes = p;
	}

	public void setSystemSettingDatas(ArrayList<SystemSettingData> p) {
		pSystemSettingDatas = p;
	}

	public void setTitles(ArrayList<String> p) {
		pTitles = p;
	}

	public void setTitlesSex(ArrayList<ReferenceData> p) {
		pTitlesSex = p;
	}

	public void setTownships(ArrayList<ReferenceData> p) {
		pTownships = p;
	}

	public void setTransactionDesc(ArrayList<ReferenceData> value) {
		this.transactionDesc = value;
	}

	public void setUserLevels(ArrayList<ReferenceData> p) {
		pUserLevels = p;
	}

}
