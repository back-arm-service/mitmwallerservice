package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ForeignExchangeRateData {

	private String ccy1;
	private String ccy2;
	private String systemDate;
	private String numBuyRate;
	private String numSellRate;
	private String numMidRate;
	private String numQuotation;
	private String txtRateType;
	private String codMethod;
	private String dTime;
	private String slno;
	private String numChqBuyRate;
	private String numChqSellRate;
	private String numXFERSellRate;
	private String numXFERBuyRate;
	private String namCURRFrom;
	private String namCURRTo;

	public ForeignExchangeRateData() {
		clearProperty();
	}

	void clearProperty() {
		ccy1 = "";
		ccy2 = "";
		systemDate = "";
		numBuyRate = "0.00";
		numSellRate = "0.00";
		numMidRate = "0.00";
		numQuotation = "";
		txtRateType = "";
		codMethod = "";
		dTime = "";
		slno = "";
		numChqBuyRate = "0.00";
		numChqSellRate = "0.00";
		numXFERSellRate = "0.00";
		numXFERBuyRate = "0.00";
		namCURRFrom = "";
		namCURRTo = "";
	}

	public String getCcy1() {
		return ccy1;
	}

	public String getCcy2() {
		return ccy2;
	}

	public String getCodMethod() {
		return codMethod;
	}

	public String getdTime() {
		return dTime;
	}

	public String getNamCURRFrom() {
		return namCURRFrom;
	}

	public String getNamCURRTo() {
		return namCURRTo;
	}

	public String getNumBuyRate() {
		return numBuyRate;
	}

	public String getNumChqBuyRate() {
		return numChqBuyRate;
	}

	public String getNumChqSellRate() {
		return numChqSellRate;
	}

	public String getNumMidRate() {
		return numMidRate;
	}

	public String getNumQuotation() {
		return numQuotation;
	}

	public String getNumSellRate() {
		return numSellRate;
	}

	public String getNumXFERBuyRate() {
		return numXFERBuyRate;
	}

	public String getNumXFERSellRate() {
		return numXFERSellRate;
	}

	public String getSlno() {
		return slno;
	}

	public String getSystemDate() {
		return systemDate;
	}

	public String getTxtRateType() {
		return txtRateType;
	}

	public void setCcy1(String ccy1) {
		this.ccy1 = ccy1;
	}

	public void setCcy2(String ccy2) {
		this.ccy2 = ccy2;
	}

	public void setCodMethod(String codMethod) {
		this.codMethod = codMethod;
	}

	public void setdTime(String dTime) {
		this.dTime = dTime;
	}

	public void setNamCURRFrom(String namCURRFrom) {
		this.namCURRFrom = namCURRFrom;
	}

	public void setNamCURRTo(String namCURRTo) {
		this.namCURRTo = namCURRTo;
	}

	public void setNumBuyRate(String numBuyRate) {
		this.numBuyRate = numBuyRate;
	}

	public void setNumChqBuyRate(String numChqBuyRate) {
		this.numChqBuyRate = numChqBuyRate;
	}

	public void setNumChqSellRate(String numChqSellRate) {
		this.numChqSellRate = numChqSellRate;
	}

	public void setNumMidRate(String numMidRate) {
		this.numMidRate = numMidRate;
	}

	public void setNumQuotation(String numQuotation) {
		this.numQuotation = numQuotation;
	}

	public void setNumSellRate(String numSellRate) {
		this.numSellRate = numSellRate;
	}

	public void setNumXFERBuyRate(String numXFERBuyRate) {
		this.numXFERBuyRate = numXFERBuyRate;
	}

	public void setNumXFERSellRate(String numXFERSellRate) {
		this.numXFERSellRate = numXFERSellRate;
	}

	public void setSlno(String slno) {
		this.slno = slno;
	}

	public void setSystemDate(String systemDate) {
		this.systemDate = systemDate;
	}

	public void setTxtRateType(String txtRateType) {
		this.txtRateType = txtRateType;
	}

	@Override
	public String toString() {
		return "{ccy1=" + ccy1 + ", ccy2=" + ccy2 + ", systemDate=" + systemDate + ", numBuyRate=" + numBuyRate
				+ ", numSellRate=" + numSellRate + ", numMidRate=" + numMidRate + ", numQuotation=" + numQuotation
				+ ", txtRateType=" + txtRateType + ", codMethod=" + codMethod + ", dTime=" + dTime + ", slno=" + slno
				+ ", numChqBuyRate=" + numChqBuyRate + ", numChqSellRate=" + numChqSellRate + ", numXFERSellRate="
				+ numXFERSellRate + ", numXFERBuyRate=" + numXFERBuyRate + ", namCURRFrom=" + namCURRFrom
				+ ", namCURRTo=" + namCURRTo + "}";
	}

}
