package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TransferConfirmResData {

	private String transid;
	private String code;
	private String desc;

	public TransferConfirmResData() {
		clearproperty();
	}

	void clearproperty() {
		transid = "";
		code = "";
		desc = "";
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public String getTransid() {
		return transid;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setTransid(String transid) {
		this.transid = transid;
	}

}
