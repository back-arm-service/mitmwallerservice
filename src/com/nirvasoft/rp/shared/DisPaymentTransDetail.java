package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DisPaymentTransDetail {
	private long sysKey;
	private long hkey;
	private int srno;
	private String code;
	private String description;
	private double price;
	private int qty;
	private double amount;

	public DisPaymentTransDetail() {
		clearProperty();
	}

	private void clearProperty() {
		sysKey = 0;
		hkey = 0;
		srno = 0;
		code = "";
		description = "";
		price = 0.0;
		qty = 0;
		amount = 0.0;
	}

	public double getAmount() {
		return amount;
	}

	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public long getHkey() {
		return hkey;
	}

	public double getPrice() {
		return price;
	}

	public int getQty() {
		return qty;
	}

	public int getSrno() {
		return srno;
	}

	public long getSysKey() {
		return sysKey;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setHkey(long hkey) {
		this.hkey = hkey;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public void setSrno(int srno) {
		this.srno = srno;
	}

	public void setSysKey(long sysKey) {
		this.sysKey = sysKey;
	}

}
