package com.nirvasoft.rp.shared;

public class WJunction {
	private long autoKey;
	private long hKey;
	private String userID;
	private String createdDate;
	private String accNumber;
	private String cusID;
	private int status;
	private int regType;
	private String t1;
	private String t2;
	private String t3;
	private int n1;
	private int n2;
	private int n3;

	public WJunction() {
		clearProperty();
	}

	private void clearProperty() {
		autoKey = 0;
		hKey = 0;
		createdDate = "";
		userID = "";
		cusID = "";
		accNumber = "";
		status = 0;
		regType = 0;
		t1 = "";
		t2 = "";
		t3 = "";
		n1 = 0;
		n2 = 0;
		n3 = 0;
	}

	public String getAccNumber() {
		return accNumber;
	}

	public long getAutoKey() {
		return autoKey;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public String getCusID() {
		return cusID;
	}

	public long gethKey() {
		return hKey;
	}

	public int getN1() {
		return n1;
	}

	public int getN2() {
		return n2;
	}

	public int getN3() {
		return n3;
	}

	public int getRegType() {
		return regType;
	}

	public int getStatus() {
		return status;
	}

	public String getT1() {
		return t1;
	}

	public String getT2() {
		return t2;
	}

	public String getT3() {
		return t3;
	}

	public String getUserID() {
		return userID;
	}

	public void setAccNumber(String accNumber) {
		this.accNumber = accNumber;
	}

	public void setAutoKey(long autoKey) {
		this.autoKey = autoKey;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public void setCusID(String cusID) {
		this.cusID = cusID;
	}

	public void sethKey(long hKey) {
		this.hKey = hKey;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public void setN2(int n2) {
		this.n2 = n2;
	}

	public void setN3(int n3) {
		this.n3 = n3;
	}

	public void setRegType(int regType) {
		this.regType = regType;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

}
