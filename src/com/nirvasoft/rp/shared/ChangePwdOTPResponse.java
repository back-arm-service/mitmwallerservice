package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ChangePwdOTPResponse {

	private String code;
	private String desc;

	public ChangePwdOTPResponse() {
		clearProperty();
	}

	void clearProperty() {
		code = "";
		desc = "";
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
