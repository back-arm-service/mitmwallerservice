package com.nirvasoft.rp.shared;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.nirvasoft.cms.shared.WalletTransData;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserReportData {
	private UserDetailReportData[] usdata;
	private String msgCode = "";
	private String msgDesc = "";
	private String userID = "";
	private String sessionID = "";
	private String aFromDate;
	private String aToDate;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private String select;
	private String msgstatus;
	private String username;
	private String nrc;
	private String phoneno;
	private String usertype;
	private String status;
	private String branchCode;
	private boolean state;
	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	private int click;
	private boolean alldate;
	private String fileType;
	private ArrayList<String> stringResult = new ArrayList<String>();
	public ArrayList<String> getStringResult() {
		return stringResult;
	}

	public void setStringResult(ArrayList<String> stringResult) {
		this.stringResult = stringResult;
	}

	public UserDetailReportData[] getUsdata() {
		return usdata;
	}

	public void setUsdata(UserDetailReportData[] usdata) {
		this.usdata = usdata;
	}

	public UserReportData() {
		clearProperty();
	}

	private void clearProperty() {
		aFromDate = "";
		aToDate = "";
		totalCount = 0;
		currentPage = 0;
		pageSize = 0;
		username = "";
		nrc = "";
		phoneno = "";
		status = "";
		usertype = "";
		branchCode = "";
		userID = "";
		click = 0;
		alldate = true;
		fileType= "";
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getaFromDate() {
		return aFromDate;
	}

	public String getaToDate() {
		return aToDate;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public int getClick() {
		return click;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public String getMsgstatus() {
		return msgstatus;
	}

	public String getNrc() {
		return nrc;
	}

	public int getPageSize() {
		return pageSize;
	}

	public String getPhoneno() {
		return phoneno;
	}

	public String getSelect() {
		return select;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getStatus() {
		return status;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public String getUserID() {
		return userID;
	}

	public String getUsername() {
		return username;
	}

	public String getUsertype() {
		return usertype;
	}

	public boolean isAlldate() {
		return alldate;
	}

	public void setaFromDate(String aFromDate) {
		this.aFromDate = aFromDate;
	}

	public void setAlldate(boolean alldate) {
		this.alldate = alldate;
	}

	public void setaToDate(String aToDate) {
		this.aToDate = aToDate;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public void setClick(int click) {
		this.click = click;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public void setMsgstatus(String msgstatus) {
		this.msgstatus = msgstatus;
	}

	public void setNrc(String nrc) {
		this.nrc = nrc;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}

	public void setSelect(String select) {
		this.select = select;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

}
