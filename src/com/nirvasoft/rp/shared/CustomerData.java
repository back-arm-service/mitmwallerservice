package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CustomerData {
	private String customerID;
	private String glDesp;
	private String name;
	private String nrc;
	private String accountNo;
	private String balance;
	private String message;
	private boolean chkAccount = false;
	private boolean chkGL = false;
	private boolean chkOther = false;
	private String msgCode = "";
	private String msgDesc = "";

	public CustomerData() {
		clearProperty();
	}

	void clearProperty() {
		msgCode = "";
		glDesp = "";
		msgDesc = "";
		customerID = "";
		name = "";
		nrc = "";
		accountNo = "";
		balance = "";
		message = "";
		chkAccount = false;
		chkGL = false;
		chkOther = false;
		glDesp = "";
	}

	public String getAccountNo() {
		return accountNo;
	}

	public String getBalance() {
		return balance;
	}

	public String getCustomerID() {
		return customerID;
	}

	public String getGlDesp() {
		return glDesp;
	}

	public String getMessage() {
		return message;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public String getName() {
		return name;
	}

	public String getNrc() {
		return nrc;
	}

	public boolean isChkAccount() {
		return chkAccount;
	}

	public boolean isChkGL() {
		return chkGL;
	}

	public boolean isChkOther() {
		return chkOther;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public void setChkAccount(boolean chkAccount) {
		this.chkAccount = chkAccount;
	}

	public void setChkGL(boolean chkGL) {
		this.chkGL = chkGL;
	}

	public void setChkOther(boolean chkOther) {
		this.chkOther = chkOther;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public void setGlDesp(String glDesp) {
		this.glDesp = glDesp;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}
	/*
	 * public boolean isAccount() { return isAccount; } public void
	 * setAccount(boolean isAccount) { this.isAccount = isAccount; } public
	 * boolean isGL() { return isGL; } public void setGL(boolean isGL) {
	 * this.isGL = isGL; } public boolean isOther() { return isOther; } public
	 * void setOther(boolean isOther) { this.isOther = isOther; }
	 */

	public void setName(String name) {
		this.name = name;
	}

	public void setNrc(String nrc) {
		this.nrc = nrc;
	}

	@Override
	public String toString() {
		return "CustomerData [customerID=" + customerID + ", name=" + name + ", nrc=" + nrc + ", accountNo=" + accountNo
				+ ", balance=" + balance + ", message=" + message + "]";
	}

}
