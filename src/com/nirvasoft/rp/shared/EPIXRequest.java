package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EPIXRequest {

	private String source;
	private String makerid;
	private String checkerid;
	private String xref;
	private String productcode;

	private String txnbrcode;
	private String txnacc;
	private String txnccy;
	private double txnamt;

	private String offsetbrcode;
	private String offsetacc;
	private String offsetccy;
	private double offsetamt;
	private String txndate;
	private String valuedate;
	private String narrative;
	private double rate;

	private String udf1fieldname;
	private String udf1fieldvalue;
	private String udf1filetype;

	private String udf2fieldname;
	private String udf2fieldvalue;
	private String udf2filetype;

	private double comm1;
	private double comm2;// tax
	private double comm3;
	private double comm4;
	private double comm5;
	private String param1;
	private String param2;

	public EPIXRequest() {
		clearProperty();
	}

	void clearProperty() {

		source = "";
		makerid = "";
		checkerid = "";
		xref = "";
		productcode = "";
		txnbrcode = "";
		txnacc = "";
		txnccy = "";
		txnamt = 0.00;

		offsetbrcode = "";
		offsetacc = "";
		offsetccy = "";
		offsetamt = 0.00;
		txndate = "";
		valuedate = "";
		narrative = "";
		rate = 0.00;

		udf1fieldname = "";
		udf1fieldvalue = "";
		udf1filetype = "";

		udf2fieldname = "";
		udf2fieldvalue = "";
		udf2filetype = "";

		comm1 = 0.00;
		comm2 = 0.00;
		comm3 = 0.00;
		comm4 = 0.00;
		comm5 = 0.00;
		param1 = "";
		param2 = "";
	}

	public String getCheckerid() {
		return checkerid;
	}

	public double getComm1() {
		return comm1;
	}

	public double getComm2() {
		return comm2;
	}

	public double getComm3() {
		return comm3;
	}

	public double getComm4() {
		return comm4;
	}

	public double getComm5() {
		return comm5;
	}

	public String getMakerid() {
		return makerid;
	}

	public String getNarrative() {
		return narrative;
	}

	public String getOffsetacc() {
		return offsetacc;
	}

	public double getOffsetamt() {
		return offsetamt;
	}

	public String getOffsetbrcode() {
		return offsetbrcode;
	}

	public String getOffsetccy() {
		return offsetccy;
	}

	public String getParam1() {
		return param1;
	}

	public String getParam2() {
		return param2;
	}

	public String getProductcode() {
		return productcode;
	}

	public double getRate() {
		return rate;
	}

	public String getSource() {
		return source;
	}

	public String getTxnacc() {
		return txnacc;
	}

	public double getTxnamt() {
		return txnamt;
	}

	public String getTxnbrcode() {
		return txnbrcode;
	}

	public String getTxnccy() {
		return txnccy;
	}

	public String getTxndate() {
		return txndate;
	}

	public String getUdf1fieldname() {
		return udf1fieldname;
	}

	public String getUdf1fieldvalue() {
		return udf1fieldvalue;
	}

	public String getUdf1filetype() {
		return udf1filetype;
	}

	public String getUdf2fieldname() {
		return udf2fieldname;
	}

	public String getUdf2fieldvalue() {
		return udf2fieldvalue;
	}

	public String getUdf2filetype() {
		return udf2filetype;
	}

	public String getValuedate() {
		return valuedate;
	}

	public String getXref() {
		return xref;
	}

	public void setCheckerid(String checkerid) {
		this.checkerid = checkerid;
	}

	public void setComm1(double comm1) {
		this.comm1 = comm1;
	}

	public void setComm2(double comm2) {
		this.comm2 = comm2;
	}

	public void setComm3(double comm3) {
		this.comm3 = comm3;
	}

	public void setComm4(double comm4) {
		this.comm4 = comm4;
	}

	public void setComm5(double comm5) {
		this.comm5 = comm5;
	}

	public void setMakerid(String makerid) {
		this.makerid = makerid;
	}

	public void setNarrative(String narrative) {
		this.narrative = narrative;
	}

	public void setOffsetacc(String offsetacc) {
		this.offsetacc = offsetacc;
	}

	public void setOffsetamt(double offsetamt) {
		this.offsetamt = offsetamt;
	}

	public void setOffsetbrcode(String offsetbrcode) {
		this.offsetbrcode = offsetbrcode;
	}

	public void setOffsetccy(String offsetccy) {
		this.offsetccy = offsetccy;
	}

	public void setParam1(String param1) {
		this.param1 = param1;
	}

	public void setParam2(String param2) {
		this.param2 = param2;
	}

	public void setProductcode(String productcode) {
		this.productcode = productcode;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public void setTxnacc(String txnacc) {
		this.txnacc = txnacc;
	}

	public void setTxnamt(double txnamt) {
		this.txnamt = txnamt;
	}

	public void setTxnbrcode(String txnbrcode) {
		this.txnbrcode = txnbrcode;
	}

	public void setTxnccy(String txnccy) {
		this.txnccy = txnccy;
	}

	public void setTxndate(String txndate) {
		this.txndate = txndate;
	}

	public void setUdf1fieldname(String udf1fieldname) {
		this.udf1fieldname = udf1fieldname;
	}

	public void setUdf1fieldvalue(String udf1fieldvalue) {
		this.udf1fieldvalue = udf1fieldvalue;
	}

	public void setUdf1filetype(String udf1filetype) {
		this.udf1filetype = udf1filetype;
	}

	public void setUdf2fieldname(String udf2fieldname) {
		this.udf2fieldname = udf2fieldname;
	}

	public void setUdf2fieldvalue(String udf2fieldvalue) {
		this.udf2fieldvalue = udf2fieldvalue;
	}

	public void setUdf2filetype(String udf3filetype) {
		this.udf2filetype = udf3filetype;
	}

	public void setValuedate(String valuedate) {
		this.valuedate = valuedate;
	}

	public void setXref(String xref) {
		this.xref = xref;
	}

}
