package com.nirvasoft.rp.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CAJunctionData {
	private String accNumber;
	private String customerId;
	private int accType;
	private int rType;

	public CAJunctionData() {
		clearProperty();
	}

	private void clearProperty() {
		accNumber = "";
		customerId = "";
		accType = 0;
		rType = 0;
	}

	public String getAccNumber() {
		return accNumber;
	}

	public int getAccType() {
		return accType;
	}

	public String getCustomerId() {
		return customerId;
	}

	public int getrType() {
		return rType;
	}

	public void setAccNumber(String accNumber) {
		this.accNumber = accNumber;
	}

	public void setAccType(int accType) {
		this.accType = accType;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public void setrType(int rType) {
		this.rType = rType;
	}

}
