package com.nirvasoft.rp.shared;

public class SyskeyResponseData {
	private String messageCode;
	private String messageDesc;
	private long sysKey;

	public SyskeyResponseData() {
		clearProperty();
	}

	private void clearProperty() {
		messageCode = "";
		messageDesc = "";
		sysKey = 0;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public String getMessageDesc() {
		return messageDesc;
	}

	public long getSysKey() {
		return sysKey;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public void setMessageDesc(String messageDesc) {
		this.messageDesc = messageDesc;
	}

	public void setSysKey(long sysKey) {
		this.sysKey = sysKey;
	}
}
