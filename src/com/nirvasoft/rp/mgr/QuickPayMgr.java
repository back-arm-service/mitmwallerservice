package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.rp.dao.CMSMerchantAccDao;
import com.nirvasoft.rp.dao.QuickPayDAO;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.shared.CMSMerchantAccRefData;
import com.nirvasoft.rp.shared.CMSMerchantData;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ServerGlobal;
import com.nirvasoft.rp.util.ServerUtil;

public class QuickPayMgr {

	public ArrayList<CMSMerchantAccRefData> getMerchantAccInfoList(String aMerchantID)
			throws ClassNotFoundException, SQLException {
		ArrayList<CMSMerchantAccRefData> datalist = new ArrayList<CMSMerchantAccRefData>();
		Connection conn = null;
		conn = ConnAdmin.getConn("001", "");
		try {
			datalist = CMSMerchantAccDao.read(aMerchantID, conn);
		} catch (Exception e) {
			e.printStackTrace();

			// Log
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Internal Server Error : ");
				l_err.add(e.getMessage());
				l_err.add("-----------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err);
			}

		} finally {
			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				// Log
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("");
					l_err.add("=============================================================================");
					l_err.add("Time :" + GeneralUtil.getTime());
					l_err.add("Internal Server Error : ");
					l_err.add(e.getMessage());
					l_err.add("-----------------------------------------------------------------------------");
					GeneralUtil.writeLog(l_err);
				}
			}
		}
		return datalist;
	}

	public CMSMerchantData getMerchantIDByPCode(String aPCode, String aMerchantID)
			throws ClassNotFoundException, SQLException {
		CMSMerchantData data = new CMSMerchantData();
		Connection conn = null;

		QuickPayDAO dao = new QuickPayDAO();
		try {
			conn = ConnAdmin.getConn();
			data = dao.getMerchantIDByProcessingCode(aPCode, aMerchantID, conn);
		} catch (Exception e) {
			e.printStackTrace();

			// Log
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Internal Server Error : ");
				l_err.add(e.getMessage());
				l_err.add("-----------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err);
			}

		} finally {
			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				// Log
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("");
					l_err.add("=============================================================================");
					l_err.add("Time :" + GeneralUtil.getTime());
					l_err.add("Internal Server Error : ");
					l_err.add(e.getMessage());
					l_err.add("-----------------------------------------------------------------------------");
					GeneralUtil.writeLog(l_err);
				}
			}
		}
		return data;
	}

	public CMSMerchantData getMerchantInfo(String aMerchantID) throws ClassNotFoundException, SQLException {
		CMSMerchantData data = new CMSMerchantData();
		Connection conn = null;
		conn = ConnAdmin.getConn();
		QuickPayDAO dao = new QuickPayDAO();
		try {
			data = dao.getMerchantInfo(aMerchantID, conn);
		} catch (Exception e) {
			e.printStackTrace();

			// Log
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Internal Server Error : ");
				l_err.add(e.getMessage());
				l_err.add("-----------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err);
			}

		} finally {
			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				// Log
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("");
					l_err.add("=============================================================================");
					l_err.add("Time :" + GeneralUtil.getTime());
					l_err.add("Internal Server Error : ");
					l_err.add(e.getMessage());
					l_err.add("-----------------------------------------------------------------------------");
					GeneralUtil.writeLog(l_err);
				}
			}
		}
		return data;
	}

	public String getToAccountByPCode(String aPCode) throws ClassNotFoundException, SQLException {
		String toaccount = "";
		Connection conn = null;
		conn = ConnAdmin.getConn("001", "");
		QuickPayDAO dao = new QuickPayDAO();
		try {
			toaccount = dao.getToAccountByProcessingCode(aPCode, conn);
		} catch (Exception e) {
			e.printStackTrace();

			// Log
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Internal Server Error : ");
				l_err.add(e.getMessage());
				l_err.add("-----------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err);
			}

		} finally {
			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				// Log
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("");
					l_err.add("=============================================================================");
					l_err.add("Time :" + GeneralUtil.getTime());
					l_err.add("Internal Server Error : ");
					l_err.add(e.getMessage());
					l_err.add("-----------------------------------------------------------------------------");
					GeneralUtil.writeLog(l_err);
				}
			}
		}
		return toaccount;
	}

	public Result IsDrCrAccountsExist(String mobileUserId, String drAcctNo, String crAcctNo, String aMerchantID)
			throws SQLException {
		Result ret = new Result();
		Connection conn = null;

		QuickPayDAO dao = new QuickPayDAO();
		try {
			conn = ConnAdmin.getConn("001", "");
			ret = dao.IsDrCrAccountsExist(mobileUserId, drAcctNo, crAcctNo, aMerchantID, conn);
		} catch (Exception e) {
			e.printStackTrace();

			// Log
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("Merchant Active");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Internal Server Error : ");
				l_err.add(e.getMessage());
				l_err.add("-----------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err);
			}

		} finally {
			ServerUtil.closeConnection(conn);
		}
		return ret;
	}

	public void updateSkyNetPlusRevStatus(int n2Status, String desc, String reversaldatetime, String aRSPName,
			String aCardExpire, String aResultText, int aResultCodeNo, String aFlexID)
			throws ClassNotFoundException, SQLException {
		Connection conn = null;
		QuickPayDAO dao = new QuickPayDAO();
		try {
			conn = ConnAdmin.getConn("001", "");
			dao.updateSkyNetPlusRevStatus(n2Status, desc, reversaldatetime, aRSPName, aCardExpire, aResultText,
					aResultCodeNo, aFlexID, conn);
		} catch (Exception e) {
			e.printStackTrace();

			// Log
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Internal Server Error : ");
				l_err.add(e.getMessage());
				l_err.add("-----------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err);
			}

		} finally {
			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				// Log
				if (ServerGlobal.isWriteLog()) {

					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("");
					l_err.add("=============================================================================");
					l_err.add("Time :" + GeneralUtil.getTime());
					l_err.add("Internal Server Error : ");
					l_err.add(e.getMessage());
					l_err.add("-----------------------------------------------------------------------------");
					GeneralUtil.writeLog(l_err);
				}
			}
		}
	}

	public void updateSkyNetStatus(String aRSPName, String aCardExpire, String aResultText, int aResultCodeNo,
			String aFlexID) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		QuickPayDAO dao = new QuickPayDAO();
		try {
			conn = ConnAdmin.getConn("001", "");
			dao.updateSkyNetStatus(aRSPName, aCardExpire, aResultText, aResultCodeNo, aFlexID, conn);
		} catch (Exception e) {
			e.printStackTrace();

			// Log
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Internal Server Error : ");
				l_err.add(e.getMessage());
				l_err.add("-----------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err);
			}

		} finally {
			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				// Log
				if (ServerGlobal.isWriteLog()) {

					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("");
					l_err.add("=============================================================================");
					l_err.add("Time :" + GeneralUtil.getTime());
					l_err.add("Internal Server Error : ");
					l_err.add(e.getMessage());
					l_err.add("-----------------------------------------------------------------------------");
					GeneralUtil.writeLog(l_err);
				}
			}
		}
	}

}
