package com.nirvasoft.rp.mgr;

import java.security.Security;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.nirvasoft.rp.data.QRData;
import com.nirvasoft.rp.data.QRDataResponse;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ServerGlobal;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import java.util.Date;

public class QRDataMgr {

	public static QRDataResponse decrypt(String encrypted) {
		GeneralUtil.readCCDAPIServiceSetting();
		String initVector, key,pUrl;
		QRDataResponse res = new QRDataResponse();
		initVector = ServerGlobal.getmGetCCDINITVECTOR();
		key = ServerGlobal.getmGetCCDKEY();
		pUrl = ServerGlobal.getmGetCCDPURL();
		String urlPaidUnpaid = ServerGlobal.getmGetCCDPaidUnPaidURL();
		try {
			Security.setProperty("crypto.policy", "unlimited");
			IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
			byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));
			String dataString = new String(original, "UTF-8");
			GeneralUtil.readDebugLogStatus();
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Decrypted Result in QR From Formula :" + dataString);
				l_err.add("-----------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\CCD\\log\\");
			}
			if (!(dataString.equalsIgnoreCase("") || dataString.equals(null))) {
				QRData data = updateData(dataString, pUrl, urlPaidUnpaid);
				QRData[] dataArr = new QRData[1];
				dataArr[0] = data;
				res.setData(dataArr);
				if (data.getStatusCode().equals("402")) {
					res.setCode("0000");
				} else {
					res.setCode("0014");
				}

				res.setDesc(data.getDesc());
				res.setStatusCode(data.getStatusCode());
			} else {
				res.setCode("0014");
				res.setDesc("fail");
			}
		} catch (Exception ex) {
			GeneralUtil.readDebugLogStatus();
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Decrypted Result in QR From Formula : Error");
				l_err.add("Decrypted Result in QR From Formula : " + ex);
				l_err.add("-----------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\CCD\\log\\");
			}
			
			ex.getMessage();
			res.setCode("0014");
			res.setDesc("Invalid QR Code.");
		}
		return res;
	}

	public static String encrypt(String value) {
		String initVector, key;
		initVector = ServerGlobal.getmGetCCDINITVECTOR();
		key = ServerGlobal.getmGetCCDKEY();
		try {

			IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));

			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");

			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

			byte[] encrypted = cipher.doFinal(value.getBytes());

			return Base64.encodeBase64String(encrypted);

		} catch (Exception ex) {

			ex.printStackTrace();

		}

		return null;

	}

	/*public static QRData updateData(String dataString, String url, String urlPaidUnpaid) {
		QRData data = new QRData();
		StringTokenizer strings = new StringTokenizer(dataString, "|");
		String billId = "";
		String refNo = "";
		String cusName = "";
		String billAmount = "";
		String ccyCode = "";
		String deptAcc = "";
		String dueDate = "";
		String deptName = "";
		String taxtDesc = "";
		String t1 = "";
		String t2 = "";
		String vendorCode = "";
		String penalty = "";
		
		while (strings.hasMoreElements()) {
			vendorCode = strings.nextToken();
			billId = strings.nextToken();
			refNo = strings.nextToken();
			cusName = strings.nextToken();
			billAmount = strings.nextToken();
			ccyCode = strings.nextToken();
			deptAcc = strings.nextToken();
			deptName = strings.nextToken();
			taxtDesc = strings.nextToken();
			t1 = strings.nextToken();
			t2 = strings.nextToken();
			dueDate = strings.nextToken();
		}
		if (billId.equalsIgnoreCase("") || billId.equals(null)) {
			billId = "";
		} else {
			data.setBillId(billId);
		}
		if (refNo.equalsIgnoreCase("") || refNo.equals(null)) {
			refNo = "";
		} else {
			data.setRefNo(refNo);
		}
		if (cusName.equalsIgnoreCase("") || cusName.equals(null)) {
			cusName = "";
		} else {
			data.setCusName(cusName);
		}
		if (billAmount.equalsIgnoreCase("") || billAmount.equals(null)) {
			billAmount = "";
		} else {
			data.setBillAmount(billAmount);
		}
		if (ccyCode.equalsIgnoreCase("") || ccyCode.equals(null)) {
			ccyCode = "";
		} else {
			data.setCcyCode(ccyCode);
		}
		if (deptAcc.equalsIgnoreCase("") || deptAcc.equals(null)) {
			deptAcc = "";
		} else {
			data.setDeptAcc(deptAcc);
		}

		if (deptName.equalsIgnoreCase("") || deptName.equals(null)) {
			deptName = "";
		} else {
			data.setDeptName(deptName);
		}
		if (taxtDesc.equalsIgnoreCase("") || taxtDesc.equals(null)) {
			taxtDesc = "";
		} else {
			data.setTaxDesc(taxtDesc);
		}

		if (t1.equalsIgnoreCase("") || t1.equals(null)) {
			t1 = "";
		} else {
			data.setT1(t1);
		}
		if (t2.equalsIgnoreCase("") || t2.equals(null)) {
			t2 = "";
		} else {
			data.setT2(t2);
		}
		if (dueDate.equalsIgnoreCase("") || dueDate.equals(null)) {
			dueDate = "";
		} else {
			data.setDueDate(dueDate);
		}
		if (vendorCode.equalsIgnoreCase("") || vendorCode.equals(null)) {
			vendorCode = "";
		} else {
			data.setVendorCode(vendorCode);
		}
		
		if (!(billId.equals("") && refNo.equals(""))) {
			data = getPenalty(data, url);
		}
		penalty = data.getPenalty();
		if (penalty.equals("") || penalty.equals(null)) {
			penalty = "0";
		} else {
			penalty = data.getPenalty();
		}
		if (!(billId.equals("") && refNo.equals(""))) {
			data = checkPaidOrUnPaid(data, urlPaidUnpaid);
		}

		String aBankComm = ConnAdmin.readConfig("BillBankComm");
		if(aBankComm.equalsIgnoreCase("")){
			aBankComm="0";
		}
		data.setBankCharges(aBankComm);
		data.setTotalAmount(String.valueOf(Double.parseDouble(data.getBillAmount()) + Double.parseDouble(aBankComm)
				+ Double.parseDouble(penalty)));
		return data;
	}*/
	public static QRData updateData(String dataString, String url, String urlPaidUnpaid) {
		QRData data = new QRData();
		StringTokenizer strings = new StringTokenizer(dataString, "|");
		String billId = "";
		String refNo = "";
		String cusName = "";
		String billAmount = "";
		String ccyCode = "";
		String deptAcc = "";
		String dueDate = "";
		String deptName = "";
		String taxtDesc = "";
		String t1 = "";
		String t2 = "";
		String vendorCode = "";
		String penalty = "";
		//MerchantDao dao = new MerchantDao();
		Connection conn = null;
		conn = ConnAdmin.getConn("001", "");

		while (strings.hasMoreElements()) {
			vendorCode = strings.nextToken();
			billId = strings.nextToken();
			refNo = strings.nextToken();
			cusName = strings.nextToken();
			billAmount = strings.nextToken();
			ccyCode = strings.nextToken();
			deptAcc = strings.nextToken();
			deptName = strings.nextToken();
			taxtDesc = strings.nextToken();
			t1 = strings.nextToken();
			t2 = strings.nextToken();
			dueDate = strings.nextToken();

		}
		if (billId == null || billId.equalsIgnoreCase("")) {
			billId = "";
		} else {
			data.setBillId(billId);
		}
		if (refNo == null || refNo.equalsIgnoreCase("")) {
			refNo = "";
		} else {
			data.setRefNo(refNo);
		}
		if (cusName == null || cusName.equalsIgnoreCase("")) {
			cusName = "";
		} else {
			data.setCusName(cusName);
		}
		if (billAmount == null || billAmount.equalsIgnoreCase("")) {
			billAmount = "";
		} else {
			data.setBillAmount(billAmount);
		}
		if (ccyCode == null || ccyCode.equalsIgnoreCase("")) {
			ccyCode = "";
		} else {
			data.setCcyCode(ccyCode);
		}
		if (deptAcc == null || deptAcc.equalsIgnoreCase("")) {
			deptAcc = "";
		} else {
			data.setDeptAcc(deptAcc);
		}

		if (deptName == null || deptName.equalsIgnoreCase("")) {
			deptName = "";
		} else {
			data.setDeptName(deptName);
		}
		if (taxtDesc == null || taxtDesc.equalsIgnoreCase("")) {
			taxtDesc = "";
		} else {
			data.setTaxDesc(taxtDesc);
		}

		if (t1 == null || t1.equalsIgnoreCase("")) {
			t1 = "";
		} else {
			data.setT1(t1);
		}
		if (t2 == null || t2.equalsIgnoreCase("")) {
			t2 = "";
		} else {
			data.setT2(t2);
		}
		if (dueDate == null || dueDate.equalsIgnoreCase("")) {
			dueDate = "";
		} else {
			data.setDueDate(dueDate);
		}
		if (vendorCode == null || vendorCode.equalsIgnoreCase("")) {
			vendorCode = "";
		} else {
			data.setVendorCode(vendorCode);
		}

		long diffInMillies = 0;

		if (!data.getDueDate().equals("")) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

			try {
				Date dueDateFromQR = sdf.parse(data.getDueDate());
				Date today = sdf.parse(sdf.format(new Date()));

				diffInMillies = dueDateFromQR.getTime() - today.getTime();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
				data.setCode("0014");
				data.setDesc("Invalid Due Date Format.");
				
				return data;
			}
		} else {
			diffInMillies = -1;
		}

		if (diffInMillies < 0) {
			if (!(billId.equals("") && refNo.equals(""))) {
				data = getPenalty(data, url);
			}
		}

		penalty = data.getPenalty();
		if (penalty.equals("") || penalty.equals(null)) {
			penalty = "0";
			data.setPenalty("0.00");
		} else {
			penalty = data.getPenalty();
		}
		
		if (!(billId.equals("") && refNo.equals(""))) {
			data = checkPaidOrUnPaid(data, urlPaidUnpaid);
		}
		String aBankComm = ConnAdmin.readConfig("BillBankComm");
		data.setBankCharges(aBankComm);
		data.setTotalAmount(String.valueOf(Double.parseDouble(data.getBillAmount()) + Double.parseDouble(aBankComm)
				+ Double.parseDouble(penalty)));
/*		try {
			String merchantID = dao.getMerchantID("100600", conn);
			data.setMerchantID(merchantID);
		} catch (SQLException e) {
			data.setMerchantID("");
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}*/	
	return data;
	}

	
	@SuppressWarnings({ "unchecked", "unused" })
	public static QRData getPenalty(QRData data, String url) {
		String status, message, lateDay, pAmount, amount, totalAmount = "";
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("----------------------------------Online-------------------------------------------");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("KEY :" + data.getBillId());
			l_err.add("QR Request Message : ");
			l_err.add(url);
			l_err.add("===================================================================================");
			GeneralUtil.writeLog(l_err, "\\CCD\\log\\");
		}
		Client client = Client.create();
		WebResource webResource = client.resource(url);
		ClientResponse response = null;
		JSONObject obj = new JSONObject();

		obj.put("bill_id", data.getBillId());
		obj.put("ref_no", data.getRefNo());

		System.out.println(obj.toString());
		try {
			response = webResource.type("application/json").post(ClientResponse.class, obj.toString());
			String jsonStr = response.getEntity(String.class);
			JSONParser parser = new JSONParser();
			Object object = parser.parse(jsonStr);
			JSONObject jsonObject = (JSONObject) object;
			// status = (String) jsonObject.get("Belated Days");
			// long day = (long) jsonObject.get("Belated_Days");
			if (Double.parseDouble(jsonObject.get("Penalty_Amount").toString()) > 0) {
				data.setPenalty(String.valueOf(Double.parseDouble(jsonObject.get("Penalty_Amount").toString())));
				data.setBillAmount(String.valueOf(Double.parseDouble(jsonObject.get("Amount").toString())));
				data.setTotalAmount(
						String.valueOf(Double.parseDouble(jsonObject.get("Total_amount_to_pay").toString())));
				data.setBelatedDays(String.valueOf((long) jsonObject.get("Belated_Days")));
				data.setPenaltyAccount(String.valueOf(jsonObject.get("Penalty_account")));
				data.setCode("0000");
				data.setDesc("Success");// You must not be late more than 60
										// days.We cut off water supply.
			} else {
				data.setCode("0014");
				data.setDesc("No Penalty");// You must not be late more than 60
											// days.We cut off water supply.
			}

			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("----------------------------------Online-------------------------------------------");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("KEY :" + data.getBillId());
				l_err.add("QR Response Message : ");
				l_err.add(jsonStr);
				l_err.add("===================================================================================");
				GeneralUtil.writeLog(l_err, "\\CCD\\log\\");
			}
		} catch (Exception ex) {
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("----------------------------------Online-------------------------------------------");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("KEY :" + data.getBillId());
				l_err.add("QR Response Message : " + "api calling");
				l_err.add(ex.getMessage());
				l_err.add("===================================================================================");
				GeneralUtil.writeLog(l_err, "\\CCD\\log\\");
			}
			data.setCode("0014");
			data.setDesc(ex.getMessage());
			return data;
		}
		return data;
	}
	
	@SuppressWarnings({ "unchecked", "unused" })
	public static QRData checkPaidOrUnPaid(QRData qrData, String url) {
		String status, message, lateDay, pAmount, amount, totalAmount = "";
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("----------------------------------Online-------------------------------------------");
			l_err.add("Time : " + GeneralUtil.getTime());
			l_err.add("KEY : " + qrData.getBillId() + ":" + qrData.getRefNo());
			l_err.add("QR CheckPaidOrUnPaid Request Message : ");
			l_err.add(url);
			l_err.add("===================================================================================");
			GeneralUtil.writeLog(l_err, "\\CCD\\log\\");
		}

		Client client = Client.create();
		WebResource webResource = client.resource(url);
		ClientResponse response = null;
		JSONObject obj = new JSONObject();
		obj.put("bill_id", qrData.getBillId());
		obj.put("ref_no", qrData.getRefNo());
		try {
			response = webResource.type("application/json").post(ClientResponse.class, obj.toString());
			String jsonStr = response.getEntity(String.class);
			int statusCode = response.getStatus();
			JSONParser parser = new JSONParser();
			Object object = parser.parse(jsonStr);
			JSONObject jsonObject = (JSONObject) object;

			qrData.setStatusCode(String.valueOf(statusCode));
			qrData.setDesc(jsonObject.get("message").toString());

			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("----------------------------------Online-------------------------------------------");
				l_err.add("Time : " + GeneralUtil.getTime());
				l_err.add("KEY : " + qrData.getBillId() + ":" + qrData.getRefNo());
				l_err.add("QR CheckPaidOrUnPaid Response Message : ");
				l_err.add(jsonStr);
				l_err.add("===================================================================================");
				GeneralUtil.writeLog(l_err, "\\CCD\\log\\");
			}
		} catch (Exception ex) {
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("----------------------------------Online-------------------------------------------");
				l_err.add("Time : " + GeneralUtil.getTime());
				l_err.add("KEY : " + qrData.getBillId() + ":" + qrData.getRefNo());
				l_err.add("QR CheckPaidOrUnPaid Response Message : Error while calling API");
				l_err.add(url);
				l_err.add(ex.getMessage());
				l_err.add("===================================================================================");
				GeneralUtil.writeLog(l_err, "\\CCD\\log\\");
			}
			qrData.setCode("0014");
			qrData.setDesc(ex.getMessage());
			return qrData;
		}

		return qrData;
	}

}
