
package com.nirvasoft.rp.mgr;
import java.sql.Connection;
import java.sql.SQLException;

import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.dao.WJunctionDao;
import com.nirvasoft.rp.dao.WalletTransactionDao;
import com.nirvasoft.rp.dao.icbs.AccountDao;
import com.nirvasoft.rp.mgr.icbs.DBSystemMgr;
import com.nirvasoft.rp.shared.Response;
import com.nirvasoft.rp.shared.TopupReq;
import com.nirvasoft.rp.shared.TopupRes;
import com.nirvasoft.rp.shared.TransferOutReq;
import com.nirvasoft.rp.shared.TransferOutRes;
import com.nirvasoft.rp.shared.WJunction;
import com.nirvasoft.rp.shared.icbs.ReferenceAccountsData;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ServerGlobal;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class WalletMgr {

	@SuppressWarnings("unchecked")
	public TransferOutRes goUtilityPaymentFromWallet(TransferOutReq req) throws Exception {
		TransferOutRes res = new TransferOutRes();
		GeneralUtil.readMBServiceSetting();
		String aUrl = ServerGlobal.getMbURL() + "service003/goUtilityPaymentFromWallet";
		ClientConfig cc = new DefaultClientConfig();
		cc.getClasses().add(JacksonJsonProvider.class);
		Client client = Client.create(cc);
		WebResource webResource = client.resource(aUrl);
		ClientResponse response = null;
		JSONObject obj = new JSONObject();
		obj.put("userID", req.getUserID());
		obj.put("sessionID", req.getSessionID());
		obj.put("fromAccount", "");
		obj.put("toAccount", req.getToAcc());
		obj.put("amount", req.getAmount());
		obj.put("totalAmount", req.getAmount());
		obj.put("field1", req.getField1());
		obj.put("field2", req.getField2());
		obj.put("refNo", req.getRefNo());
		obj.put("billId", req.getBillId());
		obj.put("cusName", req.getCusName());
		obj.put("ccyCode", req.getCcyCode());
		obj.put("t1", req.getT1());
		obj.put("t2", req.getT2());
		obj.put("dueDate", req.getDueDate());
		obj.put("deptName", req.getDeptName());
		obj.put("taxDesc", req.getTaxDesc());
		obj.put("vendorCode", req.getVendorCode());
		obj.put("paidBy", req.getPaidBy());
		obj.put("txnRef", req.getTxnRef());
		response = webResource.type("application/json").post(ClientResponse.class, obj);
		String jsonStr = response.getEntity(String.class);
		JSONParser parser = new JSONParser();
		Object obj1 = null;
		obj1 = parser.parse(jsonStr);
		JSONObject jsonObject = (JSONObject) obj1;
		res.setCode((String) jsonObject.get("code"));
		res.setDesc((String) jsonObject.get("desc"));
		res.setBankRefNo((String) jsonObject.get("bankRefNumber"));
		res.setTransDate((String) jsonObject.get("transactionDate"));
		return res;
	}
	
	@SuppressWarnings("unchecked")
	public TransferOutRes goWalletToAccount(TransferOutReq req) throws Exception {
		TransferOutRes res = new TransferOutRes();
		GeneralUtil.readMBServiceSetting();
		//String aUrl = ServerGlobal.getMbURL() + "service003/goWalletToAccount";
		String aUrl = ServerGlobal.getMbURL() + "serviceWalletApp/goWalletToAccount";
		ClientConfig cc = new DefaultClientConfig();
		cc.getClasses().add(JacksonJsonProvider.class);
		Client client = Client.create(cc);
		WebResource webResource = client.resource(aUrl);
		ClientResponse response = null;
		JSONObject obj = new JSONObject();
		obj.put("userID", req.getUserID());
		obj.put("sessionID", req.getSessionID());
		obj.put("fromAccount", "");
		obj.put("toAccount", req.getToAcc());
		obj.put("amount", req.getAmount());
		obj.put("totalAmount", req.getAmount());
		obj.put("field1", req.getField1());
		obj.put("field2", req.getField2());
		obj.put("refNo", req.getRefNo());
		obj.put("billId", req.getBillId());
		obj.put("cusName", req.getCusName());
		obj.put("ccyCode", req.getCcyCode());
		obj.put("t1", req.getT1());
		obj.put("t2", req.getT2());
		obj.put("dueDate", req.getDueDate());
		obj.put("deptName", req.getDeptName());
		obj.put("taxDesc", req.getTaxDesc());
		obj.put("vendorCode", req.getVendorCode());
		obj.put("paidBy", req.getPaidBy());
		obj.put("txnRef", req.getTxnRef());
		response = webResource.type("application/json").post(ClientResponse.class, obj);
		String jsonStr = response.getEntity(String.class);
		JSONParser parser = new JSONParser();
		Object obj1 = null;
		obj1 = parser.parse(jsonStr);
		JSONObject jsonObject = (JSONObject) obj1;
		res.setCode((String) jsonObject.get("code"));
		res.setDesc((String) jsonObject.get("desc"));
		res.setBankRefNo((String) jsonObject.get("bankRefNumber"));
		res.setTransDate((String) jsonObject.get("transactionDate"));
		return res;
	}

	public TransferOutRes paymentUtility(TransferOutReq request) {
		TransferOutRes response = new TransferOutRes();
		TransferMgr trmgr = new TransferMgr();
		DAOManager daomgr = new DAOManager();
		Response res = new Response();
		Connection conn1 = null;
		Connection conn2 = null;
		WJunction wJunction = new WJunction();
		AccountDao l_AccDao = new AccountDao();
		String wTransRefNo = "";
		String l_ComGL = "";
		try {
			conn1 = daomgr.openConnection();
			conn2 = daomgr.openICBSConnection();
			wJunction = new WJunctionDao().getDataByID(request.getUserID(), conn1);
			ReferenceAccountsData l_RefData = new ReferenceAccountsData();
			l_RefData = DBSystemMgr.getReferenceAccCode("WTR001");
			l_ComGL = l_RefData.getGLCode();
			if (l_AccDao.getBalanceByIDV2(wJunction.getAccNumber(), conn2) < request.getAmount()) {
				response.setCode("0014");
				response.setDesc("Incifficient balance!");
				return response;
			}
			res = trmgr.paymentUtility(wJunction.getAccNumber(), l_ComGL, request.getUserID(), request.getAmount(),
					request.getRefNo(), conn2);
			if (res.getMessageCode().equals("0000")) {
				response.setBankRefNo(res.getTrnRefNo());
				request.setBeneficiaryID(l_ComGL);
				new WalletTransactionDao()
						.save(new TransferMgr().updateWalletDataCr(request, res.getTrnRefNo(), "", "2"), conn1);
				wTransRefNo = response.getBankRefNo();
				request.setTxnRef(wTransRefNo);
				response = goUtilityPaymentFromWallet(request);
				if (response.getCode().equals("0000")) {
					new WalletTransactionDao().update(wTransRefNo, "", response.getBankRefNo(), request.getUserID(),
							"Utility Payment", conn1);
					response.setBankRefNo(wTransRefNo);
					response.setCode("0000");
					response.setDesc("Payment is successful");
				} else {
					// reverse transaction
					res = trmgr.reverseForPaymentUtility(l_ComGL, wJunction.getAccNumber(), request.getUserID(),
							request.getAmount(), request.getRefNo(), conn2);
					if (res.getMessageCode().equals("0000")) {
						TransferOutReq reqData = new TransferOutReq();
						reqData.setAmount(request.getAmount());
						reqData.setRefNo(request.getRefNo());
						reqData.setBeneficiaryID(request.getUserID());
						reqData.setPayeeID(l_ComGL);
						new WalletTransactionDao()
								.save(new TransferMgr().updateWalletDataCr(reqData, res.getTrnRefNo(), "", "2"), conn1);
					}
				}
			} else {
				response.setCode("0014");
				response.setDesc("Payment failed.");
			}
		} catch (Exception e) {
			// reverse transaction
			try {
				res = trmgr.reverseForPaymentUtility(l_ComGL, wJunction.getAccNumber(), request.getUserID(),
						request.getAmount(), request.getRefNo(), conn2);
				if (res.getMessageCode().equals("0000")) {
					TransferOutReq reqData = new TransferOutReq();
					reqData.setAmount(request.getAmount());
					reqData.setRefNo(request.getRefNo());
					reqData.setBeneficiaryID(request.getUserID());
					reqData.setPayeeID(l_ComGL);
					new WalletTransactionDao()
							.save(new TransferMgr().updateWalletDataCr(reqData, res.getTrnRefNo(), "", "4"), conn1);
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			response.setCode("0014");
			response.setDesc("Payment failed.");
			e.printStackTrace();
		} finally {
			com.nirvasoft.rp.util.ServerUtil.closeConnection(conn1);
			com.nirvasoft.rp.util.ServerUtil.closeConnection(conn2);
		}
		return response;
	}
	
	public TransferOutRes walletToAccount(TransferOutReq request) {
		TransferOutRes response = new TransferOutRes();
		TransferMgr trmgr = new TransferMgr();
		DAOManager daomgr = new DAOManager();
		Response res = new Response();
		Connection conn1 = null;
		Connection conn2 = null;
		WJunction wJunction = new WJunction();
		AccountDao l_AccDao = new AccountDao();
		String wTransRefNo = "";
		String l_ComGL = "";
		try {
			conn1 = daomgr.openConnection();
			conn2 = daomgr.openICBSConnection();
			wJunction = new WJunctionDao().getDataByID(request.getUserID(), conn1);
			ReferenceAccountsData l_RefData = new ReferenceAccountsData();
			l_RefData = DBSystemMgr.getReferenceAccCode("WTR001");
			l_ComGL = l_RefData.getGLCode();
			if (l_AccDao.getBalanceByIDV2(wJunction.getAccNumber(), conn2) < request.getAmount()) {
				response.setCode("0014");
				response.setDesc("Incifficient balance!");
				return response;
			}
			res = trmgr.walletToAccount(wJunction.getAccNumber(), l_ComGL, request.getUserID(), request.getAmount(),
					request.getRefNo(), conn2);
			if (res.getMessageCode().equals("0000")) {
				response.setBankRefNo(res.getTrnRefNo());
				request.setBeneficiaryID(l_ComGL);
				new WalletTransactionDao()
						.save(new TransferMgr().updateWalletDataCr(request, res.getTrnRefNo(), "", "2"), conn1);
				wTransRefNo = response.getBankRefNo();
				request.setTxnRef(wTransRefNo);
				response = goWalletToAccount(request);
				if (response.getCode().equals("0000")) {
					new WalletTransactionDao().update(wTransRefNo, "", response.getBankRefNo(), request.getUserID(),
							"Utility Payment", conn1);
					response.setBankRefNo(wTransRefNo);
					response.setCode("0000");
					response.setDesc("Wallet to Account Transfer is successful");
				} else {
					// reverse transaction
					res = trmgr.reverseForPaymentUtility(l_ComGL, wJunction.getAccNumber(), request.getUserID(),
							request.getAmount(), request.getRefNo(), conn2);
					if (res.getMessageCode().equals("0000")) {
						TransferOutReq reqData = new TransferOutReq();
						reqData.setAmount(request.getAmount());
						reqData.setRefNo(request.getRefNo());
						reqData.setBeneficiaryID(request.getUserID());
						reqData.setPayeeID(l_ComGL);
						new WalletTransactionDao()
								.save(new TransferMgr().updateWalletDataCr(reqData, res.getTrnRefNo(), "", "2"), conn1);
					}
				}
			} else {
				response.setCode("0014");
				response.setDesc("Payment failed.");
			}
		} catch (Exception e) {
			// reverse transaction
			try {
				res = trmgr.reverseForPaymentUtility(l_ComGL, wJunction.getAccNumber(), request.getUserID(),
						request.getAmount(), request.getRefNo(), conn2);
				if (res.getMessageCode().equals("0000")) {
					TransferOutReq reqData = new TransferOutReq();
					reqData.setAmount(request.getAmount());
					reqData.setRefNo(request.getRefNo());
					reqData.setBeneficiaryID(request.getUserID());
					reqData.setPayeeID(l_ComGL);
					new WalletTransactionDao()
							.save(new TransferMgr().updateWalletDataCr(reqData, res.getTrnRefNo(), "", "4"), conn1);
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			response.setCode("0014");
			response.setDesc("Payment failed.");
			e.printStackTrace();
		} finally {
			com.nirvasoft.rp.util.ServerUtil.closeConnection(conn1);
			com.nirvasoft.rp.util.ServerUtil.closeConnection(conn2);
		}
		return response;
	}

	public TopupRes topup(TopupReq request) {
		TopupRes response = new TopupRes();
		TransferMgr trmgr = new TransferMgr();
		DAOManager daomgr = new DAOManager();
		Response res = new Response();
		Connection conn = null;

		try {
			conn = daomgr.openICBSConnection();
			res = trmgr.transDeposit(request.getToAcc(), request.getUserID(), request.getAmountdbl(),
					request.getReference(), conn);

			if (res.getMessageCode().equals("0000")) {
				response.setMessageCode("0000");
				response.setMessageDesc("Topup successfully.");
			} else {
				response.setMessageCode("0014");
				response.setMessageDesc("Topup fail.");
			}
		} catch (Exception e) {
			response.setMessageCode("0014");
			response.setMessageDesc("Topup fail.");

			e.printStackTrace();
		} finally {
			com.nirvasoft.rp.util.ServerUtil.closeConnection(conn);
		}

		return response;
	}

	public TopupRes topupWallet(TopupReq request) {
		TopupRes response = new TopupRes();
		TransferMgr trmgr = new TransferMgr();
		DAOManager daomgr = new DAOManager();
		Response res = new Response();
		Connection conn1 = null;
		Connection conn2 = null;
		WJunction wJunction = new WJunction();
		try {
			conn1 = daomgr.openConnection();
			wJunction = new WJunctionDao().getDataByID(request.getUserID(), conn1);
			
			ReferenceAccountsData l_RefData = new ReferenceAccountsData();
			l_RefData = DBSystemMgr.getReferenceAccCode("WTR"+ServerGlobal.getmBranchCode());
			String l_ComGL = l_RefData.getGLCode();
			res = trmgr.topupWallet(l_ComGL, wJunction.getAccNumber(), request.getUserID(), request.getAmountdbl(),
					request.getReference(), conn2);
			if (res.getMessageCode().equals("0000")) {
				response.setBankRefNo(res.getTrnRefNo());
				response.setMessageCode("0000");
				response.setMessageDesc("Topup successfully.");
				// save in wallet transactino
				TransferOutReq reqData = new TransferOutReq();
				reqData.setAmount(Double.parseDouble(request.getAmount()));
				reqData.setRefNo(request.getReference());
				reqData.setBeneficiaryID(request.getUserID());
				reqData.setPayeeID(l_ComGL);
				new WalletTransactionDao()
						.save(new TransferMgr().updateWalletDataCr(reqData, res.getTrnRefNo(), "", "4"), conn1);
			} else {
				response.setMessageCode("0014");
				response.setMessageDesc("Topup failed.");
			}
		} catch (Exception e) {
			response.setMessageCode("0014");
			response.setMessageDesc("Topup failed.");

			e.printStackTrace();
		} finally {
			com.nirvasoft.rp.util.ServerUtil.closeConnection(conn1);
			com.nirvasoft.rp.util.ServerUtil.closeConnection(conn2);
		}

		return response;
	}
	
	@SuppressWarnings("unchecked")
	public TransferOutRes checkUserAcc(String req) throws ParseException {
		TransferOutRes res = new TransferOutRes();
		GeneralUtil.readMBServiceSetting();
		String aUrl = ServerGlobal.getMbURL() + "/";
		ClientConfig cc = new DefaultClientConfig();
		cc.getClasses().add(JacksonJsonProvider.class);
		Client client = Client.create(cc);
		WebResource webResource = client.resource(aUrl);
		ClientResponse response = null;
		JSONObject obj = new JSONObject();
		obj.put("req", req);
		response = webResource.type("application/json").post(ClientResponse.class, obj);
		String jsonStr = response.getEntity(String.class);
		JSONParser parser = new JSONParser();
		Object obj1 = null;
		obj1 = parser.parse(jsonStr);
		JSONObject jsonObject = (JSONObject) obj1;
		res.setCode((String) jsonObject.get("code"));
		res.setDesc((String) jsonObject.get("desc"));
		res.setField1((String) jsonObject.get("currentBalance"));
		res.setField2((String) jsonObject.get("ccy"));
		return res;
	}

}
