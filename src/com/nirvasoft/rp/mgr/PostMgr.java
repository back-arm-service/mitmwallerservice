package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;

import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.shared.PhotoUploadData;
import com.nirvasoft.cms.shared.UploadData;
import com.nirvasoft.database.SysKeyMgr;
import com.nirvasoft.rp.dao.PostDao;
import com.nirvasoft.rp.dao.UploadDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.util.Geneal;

public class PostMgr {
	public static int getUploadCount(String str, ArrayList<PhotoUploadData> uploadList) {

		int uploadCount = 0;
		String exist = "";
		int c = 0;
		String Arr[] = new String[100];
		Arr = str.split(" alt=\"");
		c = Arr[1].indexOf(" width=\"");

		exist = Arr[1].substring(0, c - 1);

		for (int i = 0; i < uploadList.size(); i++) {
			if (uploadList.get(i).getDesc().equalsIgnoreCase(exist)) {
				uploadCount = i;
			}
		}
		return uploadCount;
	}

	private static boolean isExistUpload(long syskey, Connection conn) throws SQLException {
		boolean isExit = false;
		ResultSet rs = null;
		String sql = "SELECT * FROM FMR006 WHERE N1='" + syskey + "'";
		PreparedStatement ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();
		if (rs.next()) {
			isExit = true;
		}
		return isExit;
	}

	public static ArrayList<PhotoUploadData> photoListLoopRemove(ArrayList<PhotoUploadData> uploadList) {
		ArrayList<Integer> count = new ArrayList<Integer>();
		HashSet<Integer> hSetNumbers = new HashSet<Integer>();
		for (int i = 0; i < uploadList.size(); i++) {
			String name = uploadList.get(i).getDesc();
			for (int ii = i + 1; ii < uploadList.size(); ii++) {
				if (name.equalsIgnoreCase(uploadList.get(ii).getDesc())) {
					hSetNumbers.add(ii);
				}
			}
		}
		// Collections.sort(count,Collections.reverseOrder());
		// Collections.reverseOrder();
		for (int strNumber : hSetNumbers)
			count.add(strNumber);
		Collections.sort(count, Collections.reverseOrder());
		// Collections.sort(count,Collections.reverseOrder());
		for (int j = 0; j < count.size(); j++) {
			int cnt = count.get(j);
			uploadList.remove(cnt - 1);
		}
		/*
		 * for(int j = count.size()-1 ; j >=0 ; j--) { int cnt = count.get(j);
		 * uploadList.remove(cnt-1); }
		 */
		return uploadList;
	}

	public static ArrayList<PhotoUploadData> photoListRemove(String content, ArrayList<PhotoUploadData> uploadList) {
		ArrayList<Integer> count = new ArrayList<Integer>();
		for (int i = 0; i < uploadList.size(); i++) {
			String desc = "alt=\"" + uploadList.get(i).getDesc() + "\"";
			if ((!content.contains(desc) && !content.contains(uploadList.get(i).getName()))
					|| (content.contains(desc) && content.contains(uploadList.get(i).getName()))) {
				count.add(i);
			}
		}

		for (int j = count.size() - 1; j >= 0; j--) {
			int cnt = count.get(j);
			uploadList.remove(cnt);
		}

		return uploadList;
	}

	public static Resultb2b savePost(ArticleData data) {
		Resultb2b res = new Resultb2b();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			conn.setAutoCommit(false);
			data = setStatusData(data);
			if (data.getN4() == 0) {
				data.setT3("news");
			}
			if (data.getN4() == 1) {
				data.setT3("article");
			}
			if (data.getN4() == 2) {
				data.setT3("business");
			}
			if (data.getN4() == 3) {
				data.setT3("interview");
			}
			if (data.getN4() == 4) {
				data.setT3("leadership");
			}
			if (data.getN4() == 5) {
				data.setT3("innovation");
			}
			if (data.getN4() == 6) {
				data.setT3("general");
			}
			if (data.getN4() == 7) {
				data.setT3("investment");
			}
			if (data.getN4() == 8) {
				data.setT3("user post");
			}
			// data.setT11(data.getDomain());
			if (data.getSyskey() == 0) {
				data.setSyskey(SysKeyMgr.getSysKey(1, "syskey", conn));
				res = PostDao.insert(data, conn);
				if (res.isState()) {
					for (int i = 0; i < data.getUploadlist().size(); i++) {
						boolean flag = false;
						UploadData obj = setPhotoUploadData(data.getUploadlist().get(i), data.getResizelist()[i],
								data.getSyskey());
						obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey", conn));
						flag = UploadDao.insert(obj, conn);
						res.setState(flag);
					}
				}
			} else {
				res = PostDao.update(data, conn);
				if (res.isState()) {
					if (isExistUpload(data.getSyskey(), conn)) {
						String sql = "DELETE FROM FMR006 WHERE N1 = '" + data.getSyskey() + "' ";
						PreparedStatement ps = conn.prepareStatement(sql);
						ps.executeUpdate();
					}

					boolean flag = false;
					for (int i = 0; i < data.getUploadlist().size(); i++) {
						UploadData obj = setPhotoUploadData(data.getUploadlist().get(i), data.getResizelist()[i],
								data.getSyskey());
						obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey", conn));
						flag = UploadDao.insert(obj, conn);
						res.setState(flag);
					}

				}
			}
			if (res.isState()) {
				res.setKeyResult(data.getSyskey());
				res.getLongResult().add(data.getSyskey());
				conn.commit();
			} else
				conn.rollback();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return res;
	}

	public static UploadData setPhotoUploadData(PhotoUploadData d, String resizefilename, long articleKey) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		Date today = new Date();
		UploadData data = new UploadData();
		data.setCreatedDate(todayDate);
		data.setCreatedTime(Geneal.getTimeAMPM(today));
		data.setModifiedDate(todayDate);
		data.setModifiedTime(Geneal.getTimeAMPM(today));

		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		data.setT1(d.getName());// photo name
		data.setT3(String.valueOf(d.getSerial()));// photo serial no
		data.setT4(d.getDesc());// description
		data.setT5(d.getOrder());// description
		data.setT6(resizefilename);
		data.setT7(d.getUrl());

		data.setN1(articleKey);// fmr002 sk
		return data;
	}

	public static ArticleData setStatusData(ArticleData data) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		Date today = new Date();
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(Geneal.getTimeAMPM(today));
			data.setModifiedDate(todayDate);
			data.setModifiedTime(Geneal.getTimeAMPM(today));
		} else {
			data.setModifiedUserId(data.getUserId());
			data.setModifiedUserName(data.getUserName());
			data.setModifiedDate(todayDate);
			data.setModifiedTime(Geneal.getTimeAMPM(today));
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		return data;
	}

}
