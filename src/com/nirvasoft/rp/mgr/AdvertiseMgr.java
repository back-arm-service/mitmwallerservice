package com.nirvasoft.rp.mgr;

import java.util.ArrayList;

import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.shared.Advertise;
import com.nirvasoft.rp.shared.AdvertiseRes;
import com.nirvasoft.rp.util.GeneralUtil;

public class AdvertiseMgr {
	public AdvertiseRes getAdLink() {
		AdvertiseRes ret = new AdvertiseRes();
		String adertise = ConnAdmin.readAdvertiseConfig("Advertise");
		if (adertise.equalsIgnoreCase("yes")) {
			ArrayList<Advertise> adList = new ArrayList<Advertise>();
			ArrayList<String> linkList = null;
			String imgDir = "";
			imgDir = ConnAdmin.readExternalUrlAdverise("IMAGEURL");
			linkList = GeneralUtil.readAdLink();
			if (linkList.size() > 0) {
				for (int i = 0; i < linkList.size(); i++) {
					Advertise ad = new Advertise();
					ad.setLink(linkList.get(i).split("_")[1]);
					ad.setLogo(imgDir + String.valueOf(i + 1) + ".jpg");
					adList.add(ad);
				}
			}
			if (adList.size() > 0) {
				Advertise[] adArr = new Advertise[adList.size()];
				for (int i = 0; i < adList.size(); i++) {
					adArr[i] = adList.get(i);
				}
				ret.setCode("0000");
				ret.setDesc("Success");
				ret.setDataList(adArr);
			} else {
				ret.setCode("0000");
				ret.setDesc("Record not found");
			}
		}
		return ret;
	}

}
