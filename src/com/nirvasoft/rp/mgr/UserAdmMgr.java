package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.rp.dao.PersonDao;
import com.nirvasoft.rp.dao.UserAdmDao;
import com.nirvasoft.rp.data.AgentData;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.shared.SessionData;
import com.nirvasoft.rp.users.data.PersonData;
import com.nirvasoft.rp.users.data.UserData;
import com.nirvasoft.rp.users.data.UserViewData;
import com.nirvasoft.rp.users.data.UserViewDataArr;
import com.nirvasoft.rp.users.data.UserViewDataset;
import com.nirvasoft.rp.util.ServerUtil;

public class UserAdmMgr {

	// activate/deactivate user & lock/unlock user start
	// get create user data
	public static UserData readUserProfileDataBySyskey(long pKey) {
		UserData res = new UserData();
		UserAdmDao userDao = new UserAdmDao();
		PersonDao p_Dao = new PersonDao();
		PersonData p_Data = new PersonData();
		AgentData a_Data = new AgentData();
		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			res = userDao.readUserProfileDataBySyskey(pKey, conn);

			/*
			 * if (res.getN3() == 1) { a_Data = p_Dao.getAgentData(res.getT1(),
			 * conn); if (a_Data.getN2() == 0) { res.setChkAccount("true"); }
			 * else if (a_Data.getN2() == 1) { res.setChkGL("true"); }
			 * res.setParentID(a_Data.getParentID());
			 * res.setAccountNumber(a_Data.getAccountNumber()); }
			 */

			/*
			 * UCJunction[] junarr; junarr = userDao.getCustIDByUID(res.getT1(),
			 * conn); res.setData(junarr); UserRoleViewData[] dataarray;
			 * dataarray = UserRoleViewDao.getUserRoleList(pKey, false, conn);
			 * long pvalue[] = UserRoleViewDao.getRoleResult(pKey, conn); String
			 * name = PersonDao.getUserName(pKey, conn); res.setName(name);
			 * p_Data = p_Dao.getCreateUserData(pKey, conn);
			 * res.setState(p_Data.getT3()); res.setCity(p_Data.getT4());
			 * res.setCustomerid(""); res.setUserrolelist(dataarray);
			 * res.setRolesyskey(pvalue);
			 */

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	public Result activatedeactivateUserData(long syskey, String status, String userId) {
		Result res = new Result();
		Connection conn = null;
		String msg = "";
		UserAdmDao u_dao = new UserAdmDao();
		try {
			conn = ConnAdmin.getConn("001", "");
			res = u_dao.activateDeactivateUser(syskey, conn, status, userId);

			if (res.isState()) {
				if (status.equalsIgnoreCase("Activate")) {
					msg = "Activate Successfully.";
				} else if (status.equalsIgnoreCase("Deactivate")) {
					msg = "Deactivate Successfully.";
				}
			} else {
				if (status.equalsIgnoreCase("Activate")) {
					msg = "Activate Failed.";
				} else if (status.equalsIgnoreCase("Deactivate")) {
					if (res.getMsgCode().equals("0014")) {
						msg = res.getMsgDesc();
					} else {
						msg = "Deactivate Failed.";
					}
				}
			}
			res.setMsgDesc(msg);
		} catch (SQLException e) {
			res.setState(false);
			res.setMsgDesc(e.getMessage());
		}

		return res;
	}

	public UserViewDataArr getAllUserData(String searchText, int pageSize, int currentPage, String operation,
			boolean master) {

		UserViewDataArr res = new UserViewDataArr();
		UserViewDataset dataSet = new UserViewDataset();
		UserAdmDao u_dao = new UserAdmDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			dataSet = u_dao.getAllUserData(searchText, conn, operation, master);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		int startPage = (currentPage - 1) * pageSize;
		int endPage = pageSize + startPage;
		res.setTotalCount(dataSet.getArlData().size());
		dataSet.setArlData(new ArrayList<UserViewData>(dataSet.getArlData().subList(startPage,
				(endPage > dataSet.getArlData().size()) ? dataSet.getArlData().size() : endPage)));
		UserViewData[] dataarry = new UserViewData[dataSet.getArlData().size()];
		/*
		 * if (dataarry.length == 1) { dataarry = new
		 * UserViewData[dataSet.getArlData().size() + 1]; dataarry[0] =
		 * dataSet.getArlData().get(0); //dataarry[1] = new UserViewData(); }
		 */

		for (int i = 0; i < dataSet.getArlData().size(); i++) {
			dataarry[i] = dataSet.getArlData().get(i);

		}
		res.setdata(dataarry);
		res.setSearchText(searchText);

		res.setCurrentPage(currentPage);
		res.setPageSize(pageSize);

		return res;
	}

	// reset password start
	public UserData getUserNameAndNrc(SessionData udata) {
		UserData data = new UserData();
		UserAdmDao u_dao = new UserAdmDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			data = u_dao.getUserNameAndNrc(udata, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return data;
	}

	public boolean isMasterUser(String userID) {
		UserAdmDao dao = new UserAdmDao();
		Connection con = ConnAdmin.getConn("001", "");
		Boolean res = false;
		try {
			res = dao.isMasterUser(userID, con);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(con);
		}
		return res;
	}

	public Result lockUnlockUserData(String userid, long syskey, String status) {
		Result res = new Result();
		Connection conn = null;
		String msg = "";
		UserAdmDao u_dao = new UserAdmDao();
		try {
			conn = ConnAdmin.getConn("001", "");
			res = u_dao.lockUnlockUser(userid, syskey, conn, status);
			if (res.isState()) {
				if (status.equalsIgnoreCase("Lock")) {
					msg = "Lock Successfully.";
				} else if (status.equalsIgnoreCase("Unlock")) {
					msg = "Unlock Successfully.";
				}
			} else {
				if (status.equalsIgnoreCase("Lock")) {
					if (res.getMsgCode().equals("0014")) {
						msg = res.getMsgDesc();
					} else {
						msg = "Lock Failed.";
					}
				} else if (status.equalsIgnoreCase("Unlock")) {
					msg = "Unlock Failed.";
				}
			}

			res.setMsgDesc(msg);
		} catch (SQLException e) {
			res.setState(false);
			res.setMsgDesc(e.getMessage());
		}

		return res;
	}
	// activate/deactivate user & lock/unlock user end

	public Result resetPasswordById(SessionData udata) {
		// TODO Auto-generated method stub
		Result res = new Result();
		UserAdmDao u_dao = new UserAdmDao();
		Connection conn = ConnAdmin.getConn("001", "");
		try {
			res = u_dao.resetPasswordById(udata, conn);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	// reset password end
}
