package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.rp.dao.CashTransactionDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.shared.CashTransactionData;
import com.nirvasoft.rp.util.ServerUtil;

public class CashTransactionMgr {	

	public boolean insertDenomination(CashTransactionData denoTrdata) {
		Connection aConn = null;
		boolean l_rs = false;
		try {
			aConn = ConnAdmin.getConn("001", "");
			l_rs = CashTransactionDao.insertDenomination(denoTrdata, aConn);
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			ServerUtil.closeConnection(aConn);
		}

		return l_rs;
	}
}
