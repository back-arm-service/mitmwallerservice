package com.nirvasoft.rp.mgr;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import javax.net.ssl.HttpsURLConnection;

import org.json.JSONException;
import org.json.JSONObject;

import com.nirvasoft.rp.dao.NotiDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.shared.NotiAdminResponse;
import com.nirvasoft.rp.shared.NotiData;
import com.nirvasoft.rp.shared.NotiRequest;
import com.nirvasoft.rp.shared.NotiResponse;
import com.nirvasoft.rp.shared.NotificationDataSet;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.util.ServerUtil;

public class NotiMgr {
	public static final String FCM_URL = "https://fcm.googleapis.com/fcm/send";
	//public static final String FCM_SERVER_API_KEY = "AAAA1Fqf0pI:APA91bE6bI4eAK0rvHlj6F-T-Lb-P6hKMtZtFpjV4Vh754MQTB_XSCVBDFadC7ZLbd2zqcW38lVjlhNiZ26to0nnaHLSUVtk--K_nBPUXZE6Q1TE_KwVDyW0h_9pgRZuIf-elfV3yFXk";
//	public static final String FCM_SERVER_API_KEY = "AAAAze5ZlOQ:APA91bEuYH_jJLkN6kEKLl-D_eWX5aIyzunflKWMN6tPv5ER4_XZHe7Arvl7J9cJWe-60nfTAIwXWxxJ1NgVQXETs0jObqWcjezSYqpK-UjwNGJFglmxXJabuuV9jGLBU_1ROAbjxS0S";
	public static final String FCM_TOPIC_NAME = "hello";
	
	public NotiAdminResponse CheckUserToken(NotiData req) throws SQLException{
		NotiAdminResponse res = new NotiAdminResponse();
		Connection aConn = null;
		String date;
		String time;
		long autokey = Long.parseLong(req.getAutokey());
		try {
			aConn = ConnAdmin.getConn("001", "");

		NotiDao dao=new NotiDao();
		res=dao.CheckUserToken(req,aConn);
		if(res.getCode()=="0000"){
			if (autokey == 0) {
				req.setCreateddatetime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				req.setModifieddatetime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				date= req.getCreateddatetime().substring(0, req.getCreateddatetime().indexOf(' '));
				time= req.getCreateddatetime().substring(req.getCreateddatetime().indexOf(' ')+1);
				req.setDate(date);
				req.setTime(time);
				req.setModifiedhistory("[" + req.getModifieddatetime() + "," + req.getUserid() + "]");
			long responseAutokey = 0;
			responseAutokey = new NotiDao().insert(req, aConn);

			if (responseAutokey > 0) {
				res.setCode("0000");
				res.setDesc("Saved successfully.");
				res.setAutokey(String.valueOf(responseAutokey));
			} else {
				res.setCode("0014");
				res.setDesc("Saved fail.");
			}
		}
		}
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return res;
		
	}
	public NotiAdminResponse selectUserToken(NotiData req) throws SQLException{
		NotiAdminResponse res = new NotiAdminResponse();
		String fcmToken;
		Connection aConn = null;
		String date;
		String time;
		long autokey = Long.parseLong(req.getAutokey());
		try {
			aConn = ConnAdmin.getConn("001", "");

		NotiDao dao=new NotiDao();
		res=dao.selectUserToken(req,aConn);
		if(res.getCode()=="0000"){
			fcmToken=res.getAutokey();
			if (autokey == 0) {
				req.setT1(fcmToken);
				req.setCreateddatetime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				req.setModifieddatetime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				date= req.getCreateddatetime().substring(0, req.getCreateddatetime().indexOf(' '));
				time= req.getCreateddatetime().substring(req.getCreateddatetime().indexOf(' ')+1);
				req.setDate(date);
				req.setTime(time);
				req.setModifiedhistory("[" + req.getModifieddatetime() + "," + req.getUserid() + "]");
			long responseAutokey = 0;
			responseAutokey = new NotiDao().insert(req, aConn);

			if (responseAutokey > 0) {
				res.setCode("0000");
				res.setDesc(fcmToken);
				res.setAutokey(String.valueOf(responseAutokey));
			} else {
				res.setCode("0014");
				res.setDesc("Saved fail.");
			}
		}
		}
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return res;
		
	}
	public NotiResponse callFcmFirebase(NotiData req) {
		NotiResponse response=new NotiResponse();
		Connection conn = null;
		String fcmServerKey;
		conn = ConnAdmin.getConn("001", "");
		fcmServerKey = ConnAdmin.readFCMConfig("ServerKey");
		int responseCode = -1;
		String responseBody = "";

		try {
			byte[] postData = getPostTokenData(req);

			URL url = new URL(FCM_URL);
			HttpsURLConnection httpURLConnection = (HttpsURLConnection) url.openConnection();
			// System.out.println("FCM URL : " + url);
			// set timeputs to 10 seconds
			httpURLConnection.setConnectTimeout(10000);
			httpURLConnection.setReadTimeout(10000);
			httpURLConnection.setDoInput(true);
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setUseCaches(false);
			httpURLConnection.setRequestMethod("POST");
			httpURLConnection.setRequestProperty("Content-Type",
					"application/json;application/x-www-form-urlencoded;charset=UTF-8");
			httpURLConnection.setRequestProperty("Content-Length", Integer.toString(postData.length));
//			httpURLConnection.setRequestProperty("Authorization", "key=" + FCM_SERVER_API_KEY);
			httpURLConnection.setRequestProperty("Authorization", "key=" + fcmServerKey);

			OutputStream out = httpURLConnection.getOutputStream();
			out.write(postData);
			out.close();
			responseCode = httpURLConnection.getResponseCode();

			if (responseCode == 200) { // success
				responseBody = convertStreamToString(httpURLConnection.getInputStream());
				response.setCode("0000");
				response.setDesc("Sent successfully.");
				response.setResponseCode(String.valueOf(responseCode));
				response.setResponseBody(responseBody);
			} else { // failure
				responseBody = convertStreamToString(httpURLConnection.getErrorStream());
				response.setCode("0014");
				response.setDesc("Sent fail.");
				response.setResponseCode(String.valueOf(responseCode));
				response.setResponseBody(responseBody);
			}
			if (response.getCode().equals("0000") && response.getResponseCode().equals("200")) {
				new NotiDao().updateFCMResponse("sent", response.getResponseCode(), response.getResponseBody(),
						req.getAutokey(), conn);
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();

			// System.out.println("FCM IOException : " + ioe.getMessage());

			response.setCode("0014");
			response.setDesc(ioe.getMessage());
			response.setResponseCode(String.valueOf(responseCode));
			response.setResponseBody(responseBody);
		} catch (Exception e) {
			e.printStackTrace();

			// System.out.println("FCM Exception : " + e.getMessage());

			response.setCode("0014");
			response.setDesc(e.getMessage());
			response.setResponseCode(String.valueOf(responseCode));
			response.setResponseBody(responseBody);
		}
		return response;
		
	}
	
	public NotiAdminResponse insertOrUpdateNoti(NotiData req) {
		NotiAdminResponse res = new NotiAdminResponse();
		Connection aConn = null;
		long autokey = Long.parseLong(req.getAutokey());

		try {
			aConn = ConnAdmin.getConn("001", "");

			if (autokey == 0) {
				req.setCreateddatetime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				req.setModifieddatetime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));

				req.setModifiedhistory("[" + req.getModifieddatetime() + "," + req.getUserid() + "]");

				long responseAutokey = 0;
				responseAutokey = new NotiDao().insert(req, aConn);

				if (responseAutokey > 0) {
					res.setCode("0000");
					res.setDesc("Saved successfully.");
					res.setAutokey(String.valueOf(responseAutokey));
				} else {
					res.setCode("0014");
					res.setDesc("Saved fail.");
				}
			} else {
				req.setModifieddatetime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));

				req.setLastuserid(req.getUserid());
				req.setLastusername(req.getUsername());

				String modifiedHistory = "";
				modifiedHistory = new NotiDao().getLastModifiedHistory(autokey, aConn);

				if (!modifiedHistory.equals("")) {
					req.setModifiedhistory(
							modifiedHistory + "[" + req.getModifieddatetime() + "," + req.getUserid() + "]");

					long responseInt = 0;
					responseInt = new NotiDao().update(req, aConn);

					if (responseInt > 0) {
						res.setCode("0000");
						res.setDesc("Updated successfully.");
						res.setAutokey(String.valueOf(req.getAutokey()));
					} else {
						res.setCode("0014");
						res.setDesc("Updated fail.");
						res.setAutokey(String.valueOf(req.getAutokey()));
					}
				} else {
					res.setCode("0014");
					res.setDesc("Updated fail.");
					res.setAutokey(String.valueOf(req.getAutokey()));
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();

			res.setCode("0014");
			if (autokey == 0) {
				res.setDesc("Saved fail.");
			} else {
				res.setDesc("Updated fail.");
				res.setAutokey(String.valueOf(req.getAutokey()));
			}
			res.setException(ex.getMessage());
		} finally {
			ServerUtil.closeConnection(aConn);
		}

		return res;
	}

	public void sendNoti(String date, String time) {
		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");

			NotiData res = new NotiData();
			res = new NotiDao().getNotiTitleDescription(date, time, conn);

			if (!res.getAutokey().equals("") && !res.getTitle().equals("") && !res.getDescription().equals("")) {
				// call AppService
				NotiResponse res2 = callAppService(res);

				// write log

				// update Response from AppService
				if (res2.getCode().equals("0000") && res2.getResponseCode().equals("200")) {
					new NotiDao().updateResponse("sent", res2.getResponseCode(), res2.getResponseBody(),
							res.getAutokey(), conn);
				}
			} else {
				// write log
			}
		} catch (Exception ex) {
			ex.printStackTrace();

			// write log
		} finally {
			ServerUtil.closeConnection(conn);
		}
	}

	private NotiResponse callAppService(NotiData data) {
		NotiResponse res = new NotiResponse();
		int responseCode = -1;
		String responseBody = "";
		String fcmServerKey;
	
		fcmServerKey = ConnAdmin.readFCMConfig("ServerKey");
		try {
			byte[] postData = getPostData(data);

			URL url = new URL(FCM_URL);
			HttpsURLConnection httpURLConnection = (HttpsURLConnection) url.openConnection();
			// System.out.println("FCM URL : " + url);
			// set timeputs to 10 seconds
			httpURLConnection.setConnectTimeout(10000);
			httpURLConnection.setReadTimeout(10000);
			httpURLConnection.setDoInput(true);
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setUseCaches(false);
			httpURLConnection.setRequestMethod("POST");
			httpURLConnection.setRequestProperty("Content-Type",
					"application/json;application/x-www-form-urlencoded;charset=UTF-8");
			httpURLConnection.setRequestProperty("Content-Length", Integer.toString(postData.length));
//			httpURLConnection.setRequestProperty("Authorization", "key=" + FCM_SERVER_API_KEY);
			httpURLConnection.setRequestProperty("Authorization", "key=" + fcmServerKey);

			OutputStream out = httpURLConnection.getOutputStream();
			out.write(postData);
			out.close();
			responseCode = httpURLConnection.getResponseCode();

			if (responseCode == 200) { // success
				responseBody = convertStreamToString(httpURLConnection.getInputStream());

				// System.out.println("FCM responseCode : =" + responseCode);
				// System.out.println("FCM responseBody : " + responseBody);

				res.setCode("0000");
				res.setDesc("Sent successfully.");
				res.setResponseCode(String.valueOf(responseCode));
				res.setResponseBody(responseBody);
			} else { // failure
				responseBody = convertStreamToString(httpURLConnection.getErrorStream());

				// System.out.println("FCM responseCode : =" + responseCode);
				// System.out.println("FCM responseBody : " + responseBody);

				res.setCode("0014");
				res.setDesc("Sent fail.");
				res.setResponseCode(String.valueOf(responseCode));
				res.setResponseBody(responseBody);
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();

			// System.out.println("FCM IOException : " + ioe.getMessage());

			res.setCode("0014");
			res.setDesc(ioe.getMessage());
			res.setResponseCode(String.valueOf(responseCode));
			res.setResponseBody(responseBody);
		} catch (Exception e) {
			e.printStackTrace();

			// System.out.println("FCM Exception : " + e.getMessage());

			res.setCode("0014");
			res.setDesc(e.getMessage());
			res.setResponseCode(String.valueOf(responseCode));
			res.setResponseBody(responseBody);
		}
		// System.out.println("FCM Response : " + res);
		return res;
	}

	private static byte[] getPostData(NotiData data) throws JSONException {
		HashMap<String, String> dataMap = new HashMap<>();
		dataMap.put("title", data.getTitle());
		dataMap.put("body", data.getDescription());
		JSONObject contentObj = new JSONObject(dataMap);
		JSONObject finalObj = new JSONObject();
		finalObj.put("notification", contentObj);
		finalObj.put("to", "/topics/" + FCM_TOPIC_NAME);
		// finalObj.put("priority", "high");
		// finalObj.put("content_available", true);
		// finalObj.put("mutable-content", true);
		// finalObj.put("collapse_key", "Updates Available");
		// System.out.println(finalObj.toString());
		return finalObj.toString().getBytes(Charset.forName("UTF-8"));
	}
	private static byte[] getPostTokenData(NotiData data) throws JSONException {
		HashMap<String, String> dataMap = new HashMap<>();
		dataMap.put("title", data.getTitle());
		dataMap.put("body", data.getDescription());
		JSONObject contentObj = new JSONObject(dataMap);
		JSONObject finalObj = new JSONObject();
		finalObj.put("notification", contentObj);
		finalObj.put("to",data.getT1());
		// finalObj.put("priority", "high");
		// finalObj.put("content_available", true);
		// finalObj.put("mutable-content", true);
		// finalObj.put("collapse_key", "Updates Available");
		// System.out.println(finalObj.toString());
		return finalObj.toString().getBytes(Charset.forName("UTF-8"));
	}
	public static String convertStreamToString(InputStream inStream) throws Exception {
		InputStreamReader inputStream = new InputStreamReader(inStream);
		BufferedReader bReader = new BufferedReader(inputStream);

		StringBuilder sb = new StringBuilder();
		String line = null;
		while ((line = bReader.readLine()) != null) {
			sb.append(line);
		}
		return sb.toString();
	}

	public NotificationDataSet getAllNoti(String searchText, int pageSize, int currentPage) {
		NotificationDataSet response = null;

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			response = new NotiDao().getAllNoti(searchText, pageSize, currentPage, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}
	public NotificationDataSet getAllTokenNoti(String userid,int pageSize, int currentPage) {
		NotificationDataSet response = null;

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			response = new NotiDao().getAllTokenNoti(userid,pageSize, currentPage, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}
	public ResponseData tokenNotiCount(String userid){
		int noticount=0;
		ResponseData  data=new ResponseData();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			data = new NotiDao().tokenNotiCount(userid,conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return 	data;
	}
	public ResponseData removeNotiCount(String userid){
		int noticount=0;
		ResponseData  data=new ResponseData();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			data = new NotiDao().removeNotiCount(userid,conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return 	data;
	}


	public NotiData deleteNoti(long autoKey) {
		NotiData response = null;
		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			response = new NotiDao().delete(autoKey, conn);
		} catch (Exception e) {
			e.printStackTrace();

			response = new NotiData();
			response.setCode("0014");
			response.setDesc("Deleted fail.");
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

}
