package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nirvasoft.cms.util.FontChangeUtil;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.dao.GenerateSerialDao;
import com.nirvasoft.rp.dao.TicketDao;
import com.nirvasoft.rp.shared.TicketData;
import com.nirvasoft.rp.util.Geneal;
import com.nirvasoft.rp.util.ServerUtil;

public class TicketMgr {

	public TicketData insertTicket(TicketData request) {
		TicketData response = null;
		DAOManager dao = new DAOManager();
		Connection conn = null;

		try {
			conn = dao.openConnection();
			/* Generate serial key */
			request.setT1(new GenerateSerialDao().getMaxSerail(request.getT6(), conn));

			/* Change User name zawgyi to uni */
			if (!ServerUtil.isUniEncoded(request.getT5())) {
				request.setT5(FontChangeUtil.zg2uni(request.getT5()));
			}

			/* Set Region */
			if (request.getT4().equals("1058") || request.getT4().equals("1059")) {
				request.setT2("YANGON");
				request.setT12("13000000");
			}
			if (request.getT4().equals("1061") || request.getT4().equals("1062")) {
				request.setT2("MANDALAY");
				request.setT12("10000000");
			}
			if (request.getT4().equals("34434")) {
				request.setT2("-");
				request.setT12("00000000");
			}

			/* Set Channel Name */
			/*
			 * if (request.getT4().equals("1054") ||
			 * request.getT4().equals("1060")) { request.setT15("General"); } if
			 * (request.getT4().equals("1058") ||
			 * request.getT4().equals("1061")) { request.setT15("Water Pipeline"
			 * ); } if (request.getT4().equals("1059") ||
			 * request.getT4().equals("1062")) { request.setT15(
			 * "Gabbage Collection"); }
			 */
			request.setT15(new TicketDao().getChannelNameByID(request.getT4(), conn));
			/* Set Status */
			request.setT10("New");

			/* Set Record Status */
			request.setN2("1");

			/* Set Created Date, Modified Date */
			String todaydate1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
			request.setCreateddate(todaydate1);
			request.setModifieddate(todaydate1);

			/* Set Shoot Date */
			String shootDate = request.getShootdate();
			String shootDay = shootDate.substring(shootDate.indexOf("-") + 1, shootDate.lastIndexOf("-"));
			if (shootDay.length() == 1) {
				shootDay = "0" + shootDay;
			}
			String shootMonth = Geneal.getShootMonth(shootDate.substring(0, shootDate.indexOf("-")));
			String shootYear = shootDate.substring(shootDate.lastIndexOf("-") + 1);
			shootDate = shootYear + "-" + shootMonth + "-" + shootDay;
			request.setShootdate(shootDate);

			/* Insert into database */
			response = new TicketDao().insertTicket(request, conn);
		} catch (Exception e) {
			response = new TicketData();
			response.setMessageCode("0014");
			response.setMessageDesc("Save failed.");

			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed()) {
						conn.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return response;
	}

}
