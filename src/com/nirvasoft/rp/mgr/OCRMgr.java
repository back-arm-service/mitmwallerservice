package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.rp.dao.OCRDao;
import com.nirvasoft.rp.data.OCRRequestData;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.util.Geneal;

public class OCRMgr {

	public String getToken() {
		String token = "";

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");

			token = new OCRDao().getToken(Geneal.datetoString(), conn);
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return token;
	}

	public String insertToken(String token) {
		String messageCode = "";

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");

			OCRRequestData req = new OCRRequestData();
			req.setCreatedDate(Geneal.datetoString());
			req.setToken(token);

			messageCode = new OCRDao().insertToken(req, conn);
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return messageCode;
	}

}
