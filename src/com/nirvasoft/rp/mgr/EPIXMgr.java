package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.rp.dao.EPIXWebServiceDAO;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ServerGlobal;

public class EPIXMgr {
	public boolean getAccByCustID(String fileName, String oId) {
		boolean res = false;
		EPIXWebServiceDAO chkdbsource = new EPIXWebServiceDAO();
		Connection cmsconn = null;

		try {
			cmsconn = ConnAdmin.getConn("001", "");
			res = chkdbsource.isUsedWebService(cmsconn);

		} catch (Exception e) {
			e.printStackTrace();

			// Log
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Message : ");
				l_err.add(e.getMessage());
				l_err.add("=============================================================================");
				GeneralUtil.writeLog(l_err);
			}

		} finally {
			try {
				if (cmsconn != null) {
					cmsconn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				// Log
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("");
					l_err.add("Time :" + GeneralUtil.getTime());
					l_err.add("Message : ");
					l_err.add(e.getMessage());
					l_err.add("=============================================================================");
					GeneralUtil.writeLog(l_err);
				}
			}
		}
		return res;
	}
}
