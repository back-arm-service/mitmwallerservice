package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;

import com.nirvasoft.rp.dao.PasswordPolicyDao;
import com.nirvasoft.rp.dao.PwdDao;
import com.nirvasoft.rp.data.PswPolicyData;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.ResultTwo;
import com.nirvasoft.rp.shared.ForceChangePwdReq;
import com.nirvasoft.rp.shared.ForceChangePwdResponse;
import com.nirvasoft.rp.shared.PwdData;
import com.nirvasoft.rp.util.ServerUtil;

public class PwdMgr {

	public ResultTwo changePwd(PwdData data) {
		ResultTwo ret = new ResultTwo();
		PwdDao t_DAO = new PwdDao();
		Connection l_Conn = null;

		try {
			l_Conn = ConnAdmin.getConn("001", "");
			ret = t_DAO.updatePwdData(data, l_Conn);
		} catch (SQLException e) {

			e.printStackTrace();
			ret.setMsgDesc(e.getMessage());
			ret.setState(false);

		} finally {

			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
				// l_DAO.deregisterDriver();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				ret.setMsgDesc(e.getMessage());
				ret.setState(false);
			}
		}
		return ret;
	}

	public boolean checkOldPassword(PwdData data) {
		boolean isPass = false;
		PwdDao t_DAO = new PwdDao();
		Connection l_Conn = null;

		try {
			l_Conn = ConnAdmin.getConn("001", "");
			isPass = t_DAO.checkOldPassword(data, l_Conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return isPass;
	}

	public ResultTwo checkPasswordPolicyPattern(String newPwd) {
		// TODO Auto-generated method stub
		PasswordPolicyDao t_DAO = new PasswordPolicyDao();
		ResultTwo ret = new ResultTwo();
		Connection l_Conn = null;

		try {
			l_Conn = ConnAdmin.getConn("001", "");
			ret = t_DAO.checkPasswordPolicyPattern(newPwd, l_Conn);
		} catch (SQLException e) {

			e.printStackTrace();
			ret.setMsgCode("0014");
			ret.setMsgDesc(e.getMessage());

		} finally {
			ServerUtil.closeConnection(l_Conn);
		}
		return ret;
	}

	/*
	 * public Result forcechangePassword(PwdData data) throws ParseException {
	 * // TODO Auto-generated method stub Result ret=new Result(); PwdDao
	 * t_DAO=new PwdDao(); Connection l_Conn = null;
	 * 
	 * try{ l_Conn = ConnAdmin.getConn("001", "");
	 * ret=t_DAO.forcechangePassword(data, l_Conn); }catch(SQLException e){
	 * 
	 * e.printStackTrace(); ret.setKeyst("1");//Error Message
	 * ret.setMsgDesc(e.getMessage());
	 * 
	 * } finally{
	 * 
	 * try { if(!l_Conn.isClosed()) l_Conn.close(); //l_DAO.deregisterDriver();
	 * } catch (SQLException e) { // TODO Auto-generated catch block
	 * e.printStackTrace();
	 * 
	 * ret.setKeyst("1");//Error Message ret.setMsgDesc(e.getMessage()); } }
	 * return ret; }
	 */

	public ForceChangePwdResponse forcechangePassword(ForceChangePwdReq data) throws ParseException {
		// TODO Auto-generated method stub
		ForceChangePwdResponse ret = new ForceChangePwdResponse();
		PwdDao t_DAO = new PwdDao();
		Connection l_Conn = null;

		try {
			l_Conn = ConnAdmin.getConn("001", "");
			ret = t_DAO.forcechangePassword(data, l_Conn);
		} catch (SQLException e) {

			e.printStackTrace();
			ret.setCode("0014");
			ret.setDesc(e.getMessage());

		} finally {

			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
				// l_DAO.deregisterDriver();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				ret.setCode("0014");
				ret.setDesc(e.getMessage());
			}
		}
		return ret;
	}

	public PswPolicyData readPswPolicy() {
		PswPolicyData data = new PswPolicyData();
		PwdDao dao = new PwdDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			data = dao.readPswPolicy(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return data;

	}

	public ResultTwo resetPassword(PwdData data) {
		ResultTwo ret = new ResultTwo();
		PwdDao t_DAO = new PwdDao();
		Connection l_Conn = null;

		try {
			l_Conn = ConnAdmin.getConn("001", "");
			ret = t_DAO.resetPassword(data, l_Conn);
		} catch (SQLException e) {

			e.printStackTrace();
			ret.setMsgDesc(e.getMessage());
			ret.setState(false);

		} finally {

			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
				// l_DAO.deregisterDriver();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				ret.setMsgDesc(e.getMessage());
				ret.setState(false);
			}
		}
		return ret;
	}
}
