package com.nirvasoft.rp.mgr.dfs;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.shared.dfs.FilterDataset;
import com.nirvasoft.rp.shared.dfs.TransactionListingDataset;
import com.nirvasoft.rp.dao.dfs.TransactionDAO;

public class DBApplicationDataMgr 
{
	
	// DBS Function
	//
	public TransactionListingDataset getTransactionListingData(FilterDataset aFilterDataset)
	{
		Connection l_Conn = null;
		
		TransactionDAO l_TransactionDAO = new TransactionDAO();
		
		TransactionListingDataset l_Dataset = new TransactionListingDataset();		
		DAOManager l_DAOMgr = new DAOManager();
		try
		{
			// Create connection
			l_Conn = l_DAOMgr.openICBSConnection();

			// Customer Listing Data
			l_Dataset = l_TransactionDAO.getTransactionListingData(aFilterDataset, l_Conn);
		}
		catch(Exception e)
		{
			// Print stack trace
			e.printStackTrace();
		}
		finally
		{
			try 
			{
				// Close connection
				if(!l_Conn.isClosed())	l_Conn.close();	
			} 
			catch (SQLException eSQL) 
			{
				eSQL.printStackTrace();
			}
		}

		return l_Dataset;
	}

	
	
}