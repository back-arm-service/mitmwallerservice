package com.nirvasoft.rp.mgr;

import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.rp.dao.UserDao;
import com.nirvasoft.rp.dao.integration.SMSSettingDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.mgr.integration.MITMessageService;
import com.nirvasoft.rp.shared.OTPRes;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.shared.integration.MessageData;
import com.nirvasoft.rp.shared.integration.SMSParameter;
import com.nirvasoft.rp.shared.integration.SMSSettingData;
import com.nirvasoft.rp.users.data.OTPReqData;
import com.nirvasoft.rp.util.ServerUtil;

public class SMSMgr {

	public ArrayList<SMSSettingData> checkMessageSetting(String service, String function, String merchantid,Connection pConn) {
		ArrayList<SMSSettingData> ret = new ArrayList<SMSSettingData>();
		SMSSettingDao dao = new SMSSettingDao();
		
		try {			
			ret = dao.checkMessageSetting(service, function, merchantid, pConn);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return ret;
	}

	public OTPRes checkOTPSetting(String service, String function, String merchantid, String sessionid, String userid) {
		OTPRes ret = new OTPRes();
		SMSSettingDao dao = new SMSSettingDao();
		Connection conn = null;

		String sKey = "";
		try {

			conn = ConnAdmin.getConn("", "");
			if (dao.checkOTPSetting(service, function, merchantid, conn)) {
				sKey = dao.saveSecuritySession(function, sessionid, userid, "1", conn);
				if (sKey.equals("")) {
					ret.setCode("0014");
					ret.setDesc("Server Error!");
				} else {
					ret.setCode("0000");
					ret.setDesc("Success");
					ret.setrKey("true");
					ret.setsKey(sKey);
				}
			} else {
				ret.setCode("0000");
				ret.setDesc("No OTP Service");
				ret.setrKey("false");
				sKey = dao.saveSecuritySession(function, sessionid, userid, "3", conn);
				if (sKey.equals("")) {
					ret.setCode("0014");
					ret.setDesc("Server Error!");
				} else {
					ret.setCode("0000");
					ret.setDesc("Success");
					ret.setsKey(sKey);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			ret.setCode("0014");
			ret.setDesc("Server Error!");
		} finally {
			try {
				if (!conn.isClosed()) {
					ServerUtil.closeConnection(conn);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return ret;
	}

	public OTPReqData getOTPCode(String userId, String phoneNo) {
		OTPReqData data = new OTPReqData();
		String otpcode = "";
		try {
			otpcode = MITMessageService.getOTP(userId.trim(), phoneNo.trim());
			if (otpcode.equals("000000")) {
				data.setCode("0014");
				data.setDesc("Invalid Ph No.");
				data.setotpcode(otpcode);
				data.setuserid(userId);
				data.setphno(phoneNo);
			} else if (!otpcode.equals("") || otpcode.equalsIgnoreCase("null")) {
				data.setCode("0000");
				data.setDesc("Success");
				data.setotpcode(otpcode);
				data.setuserid(userId);
				data.setphno(phoneNo);
			} else {
				data.setCode("0014");
				data.setDesc("Failed");
				data.setotpcode(otpcode);
				data.setuserid(userId);
			}

		} catch (GeneralSecurityException e) {
			e.printStackTrace();
			data.setCode("0014");
			data.setDesc("Failed");
			data.setotpcode(otpcode);
			data.setuserid(userId);
		} catch (Exception e) {
			e.printStackTrace();
			data.setCode("0014");
			data.setDesc("Failed");
			data.setotpcode(otpcode);
			data.setuserid(userId);
		}
		return data;
	}

	public String getPhoneNo(String userID) {
		String phone = "";
		Connection conn = null;
		UserDao userDao = new UserDao();
		try {
			conn = ConnAdmin.getConn("", "");
			phone = userDao.getPhoneNo(userID, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (!conn.isClosed()) {
					ServerUtil.closeConnection(conn);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return phone;
	}

	public String replaceMessage(String message, SMSParameter smsParam) {
		String ret = "";
		ret = message.replace("<TransRef>", smsParam.getTransRef());
		message = ret.replace("<Amount>", smsParam.getAmount());
		ret = message.replace("<CommAmount>", smsParam.getCommAmount());
		message = ret.replace("<FromAcc>", smsParam.getFromAcc());
		ret = message.replace("<ToAcc>", smsParam.getToAcc());
		message = ret.replace("<FromPhone>", smsParam.getFromPhone());
		ret = message.replace("<ToPhone>", smsParam.getToPhone());
		message = ret.replace("<FromName>", smsParam.getFromName());
		ret = message.replace("<ToName>", smsParam.getToName());
		message = ret.replace("<Date>", smsParam.getDate());
		ret = message.replace("<OTP>", smsParam.getOtp());
		message = ret.replace("<Password>", smsParam.getPassword());
		ret = message.replace("<Status>", smsParam.getStatus());
		return ret;
	}

	public ResponseData sendMessageService(ArrayList<SMSSettingData> smsList, SMSParameter smsParam, String fromPhone,
			String toPhone) {
		ResponseData ret = new ResponseData();
		try {
			for (int i = 0; i < smsList.size(); i++) {
				String message = "";
				String phone = "";
				if (smsList.get(i).getFrom().equalsIgnoreCase("1")) {
					message = smsList.get(i).getFromMsg();
					phone = fromPhone;
				} else if (smsList.get(i).getTo().equalsIgnoreCase("1")) {
					message = smsList.get(i).getToMsg();
					phone = toPhone;
				}
				message = replaceMessage(message, smsParam);
				String env = ConnAdmin.readConfig("SMSAggregator");
				if (env.equalsIgnoreCase("MIT")) {
					/*Response smsresponse = null;
					smsresponse = MITMessageService.sendSMSMessage(phone, message, "text");
					if (smsresponse.getRESULT() != null) {// PURE SUCCESS
						if (smsresponse.getRESULT().toLowerCase().contains("success")) {
							ret.setCode("0000");
							ret.setDesc("Success");
						} else {
							ret.setCode("0014");
							ret.setDesc("Unknown SMS Error");
						}
					} else {
						ret.setCode("0014");
						ret.setDesc(smsresponse.getERRORDESC());
					}*/
					// new version
					MessageData msgData = new MessageData();
					msgData = MITMessageService.sendSMSApi(phone, message);
					if(msgData.getStatus().equals("1")){
						ret.setCode("0000");
						ret.setDesc("Success");
					} else {
						ret.setCode("0014");
						ret.setDesc(msgData.getResponseMessage());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

}
