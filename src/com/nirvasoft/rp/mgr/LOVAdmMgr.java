package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.cms.framework.Result;
import com.nirvasoft.rp.dao.LOVAdmDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Lov3;
import com.nirvasoft.rp.shared.SessionData;
import com.nirvasoft.rp.util.ServerUtil;

public class LOVAdmMgr {
	public ArrayList<Result> getAllFeatures() {
		Connection conn = null;
		LOVAdmDao u_dao = new LOVAdmDao();
		ArrayList<Result> res = new ArrayList<Result>();

		try {
			conn = ConnAdmin.getConn("001", "");
			res = u_dao.getAllFeatures(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public Lov3 getChannelkey(SessionData data) {
		Lov3 response = null;

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			response = new LOVAdmDao().getChannelkey(data, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

	public ArrayList<Result> getCodeFromLovdeatils(String merchantID) {
		Connection l_Conn = null;
		ArrayList<Result> res = new ArrayList<Result>();

		try {
			l_Conn = ConnAdmin.getAnotherConn("ConncetionConfig.txt", "001");
			res = new LOVAdmDao().getCodeFromLovdeatils(merchantID, l_Conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	// SMS end
	public Lov3 getDomainType() {
		Lov3 response = null;

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			response = new LOVAdmDao().getDomainType(conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

	public Lov3 getFileType() {
		Lov3 response = null;

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			response = new LOVAdmDao().getFileType(conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

	// SMS start
	public Lov3 getMerchantIDListDetail(String userId) {

		Lov3 lov3 = new Lov3();
		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			lov3 = new LOVAdmDao().getMerchantIDListDetail(conn, userId);

		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();

			}
		}

		return lov3;

	}

	// Merchant
	public ArrayList<Result> getOrderList() {

		Connection conn = null;
		LOVAdmDao u_dao = new LOVAdmDao();
		ArrayList<Result> res = new ArrayList<Result>();

		try {
			conn = ConnAdmin.getConn("001", "");
			res = u_dao.getOrderList(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}

	public Lov3 getPOCUserType() {
		Lov3 response = null;

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			response = new LOVAdmDao().getPOCUserType(conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

	public Lov3 getTypes() {
		Lov3 lov3 = null;

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			lov3 = new LOVAdmDao().getAccountTransferTypes(conn);
		} catch (Exception e) {
			lov3 = new Lov3();
			lov3.setMsgCode("0014");
			lov3.setMsgDesc("Server Error!");

			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return lov3;
	}
	
	public Lov3 getLovType(String lovDesc) {
		Lov3 response = null;

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			response = new LOVAdmDao().getLovType(lovDesc,conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

}
