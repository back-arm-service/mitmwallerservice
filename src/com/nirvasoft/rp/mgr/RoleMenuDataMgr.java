package com.nirvasoft.rp.mgr;

import java.sql.Connection;

import com.nirvasoft.rp.dao.RoleMenuDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.users.data.RoleMenuData;

public class RoleMenuDataMgr {

	public static RoleMenuData[] getRoleMenuList() {

		RoleMenuData[] dataarray = null;
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			dataarray = RoleMenuDao.getRoleMenuList(conn);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dataarray;
	}

}
