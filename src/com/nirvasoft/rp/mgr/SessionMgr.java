
package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import com.nirvasoft.rp.dao.SessionDAO;
import com.nirvasoft.rp.dao.UserDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.mgr.integration.MITMessageService;
import com.nirvasoft.rp.shared.CheckOTPRequestData;
import com.nirvasoft.rp.shared.MSigninResponseData;
import com.nirvasoft.rp.shared.MobileLogoutResponseData;
import com.nirvasoft.rp.shared.OTPReq;
import com.nirvasoft.rp.shared.OTPRes;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.shared.SessionData;
import com.nirvasoft.rp.shared.WalletResponseData;
import com.nirvasoft.rp.shared.integration.MessageData;
import com.nirvasoft.rp.shared.integration.SMSSettingData;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ServerGlobal;
import com.nirvasoft.rp.util.ServerUtil;

public class SessionMgr {
	public MSigninResponseData checkLogin(String userId) {
		MSigninResponseData res = new MSigninResponseData();
		SessionDAO s_dao = new SessionDAO();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = s_dao.checkLogin(userId, conn);
		} catch (Exception e) {
			res.setCode("0014");
			res.setDesc(e.getMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				res.setCode("0014");
				res.setDesc(e.getMessage());
			}
		}
		return res;
	}
	public ResponseData checkDeviceId(String deviceID){
		ResponseData response=new ResponseData();
		SessionDAO s_dao = new SessionDAO();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			response = s_dao.checkDeviceId(deviceID, conn);
		} catch (Exception e) {
			response.setCode("0014");
			response.setDesc(e.getMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				response.setCode("0014");
				response.setDesc(e.getMessage());
			}
		}
		
		return response;
		
	}
	public ResponseData removeDeviceId(String deviceID){
		ResponseData response=new ResponseData();
		SessionDAO s_dao = new SessionDAO();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			response = s_dao.removeDeviceId(deviceID, conn);
		} catch (Exception e) {
			response.setCode("0014");
			response.setDesc(e.getMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				response.setCode("0014");
				response.setDesc(e.getMessage());
			}
		}
		
		return response;
		
	}
	public ResponseData updateActivityTime(String sessionId, String userID) {//ndh
		ResponseData res = new ResponseData();
		Connection conn = null;
		SessionDAO s_DAO = new SessionDAO();
		conn = ConnAdmin.getConn("001", "");
		try {
			res = s_DAO.updateActivityTime(sessionId, userID, conn);
			if(res.getCode().equalsIgnoreCase("0000")){
				if(s_DAO.checkSessionExpired(sessionId, userID, conn)){
					res.setCode("0016");
					res.setDesc("Your session has expired. Please signin again.");
				}
			}
		} catch (Exception e) {
			res.setCode("0014");
			res.setDesc(e.getMessage());
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}
	

	public ResponseData checkOtpCode(String sessionid, String userid, String rkey, String otpcode, String securityKey) {
		ResponseData response = new ResponseData();
		SessionDAO session_dao = new SessionDAO();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			response = session_dao.checkOTPCode(sessionid, userid, rkey, otpcode, conn);
			if (response.getCode().equalsIgnoreCase("0000")) {
				response.setCode("0000");
				response.setDesc("Success");
			} else {
				response.setCode("0014");
				response.setDesc("Failed");
			}
		} catch (Exception e) {
			response.setCode("0014");
			response.setDesc("Failed");
		}
		return response;
	}
	
	public ResponseData checkOtpCodeV2(String userid, String rkey, String otpcode, String securityKey) {
		ResponseData response = new ResponseData();
		SessionDAO session_dao = new SessionDAO();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			response = session_dao.checkOTPCodeV2(userid, rkey, otpcode, conn);			
		} catch (Exception e) {
			response.setCode("0014");
			response.setDesc("Failed");
		}
		return response;
	}
//	public ResponseData checkOtpCodeV2(String userid, String rkey, String otpcode, String securityKey,String deviceid,String t2) {
//		ResponseData response = new ResponseData();
//		SessionDAO session_dao = new SessionDAO();
//		Connection conn = null;
//		try {
//			conn = ConnAdmin.getConn("001", "");
//			response = session_dao.checkOTPCodeV2(userid, rkey, otpcode,deviceid,t2, conn);			
//		} catch (Exception e) {
//			response.setCode("0014");
//			response.setDesc("Failed");
//		}
//		return response;
//	}
	

	public ResponseData checkOtpCodeWallet(String sessionid, String userid, String rkey, String otpcode,
			String securityKey, String type) {
		ResponseData response = new ResponseData();
		SessionDAO session_dao = new SessionDAO();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			response = session_dao.checkOTPCode(sessionid, userid, rkey, otpcode, conn);
			if (response.getCode().equalsIgnoreCase("0000")) {
				response.setCode("0000");
				response.setDesc("Success");
			} else {
				response.setCode("0014");
				response.setDesc(response.getDesc());
			}
		} catch (Exception e) {
			response.setCode("0014");
			response.setDesc("Failed");
		}
		return response;
	}

	public Result checkOTPDuration(String sessionId) {
		Result res = new Result();
		SessionDAO session_dao = new SessionDAO();
		String getOTPTime = "";
		boolean timeflag = false;
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			getOTPTime = session_dao.getOTPGeneratedTime(conn, sessionId);
			if (getOTPTime.equalsIgnoreCase("") || getOTPTime.equalsIgnoreCase("null")) {
				res.setMsgCode("0000"); // first time getOTP request with new
										// sessionID
			} else {
				String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
				String dtime1 = getOTPTime.replace("-", "");// getotp datetime
				String dtime2 = date.replace("-", ""); // current datetime
				String otpsessionLimit = ConnAdmin.readExternalUrl("OTPSESSIONTIME");
				timeflag = session_dao.checkTimeStamp(dtime1, dtime2, otpsessionLimit);
				if (timeflag == true) {
					res.setMsgCode("0001");// OTP Request within 5 min
				} else {
					res.setMsgCode("0000");
				}
			}
		} catch (Exception e) {
			res.setMsgCode("0014");
			res.setMsgDesc(e.getMessage());
		}
		return res;
	}

	public Result checkSession(String sessionId, String userId) {
		Result response = new Result();
		SessionDAO s_dao = new SessionDAO();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			response = s_dao.checkSession(sessionId, userId, conn);
		} catch (Exception e) {
			e.printStackTrace();
			// Log
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Check Session ID Message : ");
				l_err.add(e.getMessage());
				l_err.add("=============================================================================");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
			response.setMsgCode("0014");
			response.setMsgDesc("Internal Error");

		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				// Log
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("");
					l_err.add("Time :" + GeneralUtil.getTime());
					l_err.add("Check Session ID Message : ");
					l_err.add(e.getMessage());
					l_err.add("=============================================================================");
					GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
				}
			}
		}
		return response;
	}

	public boolean checkSessionID(String sessionId, String userId) {
		boolean res = false;
		SessionDAO s_dao = new SessionDAO();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = s_dao.checkSessionID(sessionId, userId, conn);
		} catch (Exception e) {
			e.printStackTrace();
			// Log
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Check Session ID Message : ");
				l_err.add(e.getMessage());
				l_err.add("=============================================================================");
				GeneralUtil.writeLog(l_err);
			}
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				// Log
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("");
					l_err.add("Time :" + GeneralUtil.getTime());
					l_err.add("Check Session ID Message : ");
					l_err.add(e.getMessage());
					l_err.add("=============================================================================");
					GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
				}
			}
		}
		return res;
	}

	public String generateOTP() {
		String otpcode = "";
		String num_list = "0123456789";
		int length = 6;
		char ch;
		StringBuffer randStr = new StringBuffer();
		for (int i = 0; i < length; i++) {
			Random randomGenerator = new Random();
			int number = 0;
			number = randomGenerator.nextInt(num_list.length());
			ch = num_list.charAt(number);

			randStr.append(ch);
		}
		otpcode = String.valueOf(randStr);
		return otpcode;
	}

	public MobileLogoutResponseData getActivitiesHistory(SessionData data) {
		MobileLogoutResponseData res = new MobileLogoutResponseData();
		Connection conn = null;
		SessionDAO session_dao = new SessionDAO();
		try {
			conn = ConnAdmin.getConn("001", "");
			res = session_dao.getActivitiesHistory(data, conn);
		} catch (Exception e) {
			res.setCode("0014");
			res.setDesc(e.getMessage());
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return res;
	}

	public Result insertLogOutSessionID(String sessionId, String userId, String activity, String expired,
			String duration) {
		Result res = new Result();
		Connection conn = null;
		SessionDAO s_DAO = new SessionDAO();
		conn = ConnAdmin.getConn("001", "");
		try {
			res = s_DAO.insertLogOutSessionID(sessionId, userId, activity, expired, duration, conn);
		} catch (Exception e) {
			res.setState(false);
			res.setMsgDesc(e.getMessage());
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public Result insertSession(String userId) {
		Result res = new Result();
		Connection conn = null;
		SessionDAO s_DAO = new SessionDAO();
		conn = ConnAdmin.getConn("001", "");
		try {
			res = s_DAO.insertSession(userId, conn);
		} catch (Exception e) {
			res.setState(false);
			res.setMsgDesc(e.getMessage());
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public Result saveActivity(String sessionId, String userId, String activity, String expired, String duration) {
		Result res = new Result();
		Connection conn = null;
		SessionDAO s_DAO = new SessionDAO();
		conn = ConnAdmin.getConn("001", "");
		try {
			res = s_DAO.saveActivity(sessionId, userId, activity, expired, duration, conn);
		} catch (Exception e) {
			res.setState(false);
			res.setMsgDesc(e.getMessage());
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;

	}

	/*public OTPRes sendOTPCode(OTPReq request, int flag) {
		OTPRes response = new OTPRes();
		OTPRes result = new OTPRes();
		UserDao u_dao = new UserDao();
		SessionDAO session_dao = new SessionDAO();
		CheckOTPRequestData data = new CheckOTPRequestData();
		String phoneno = "";
		String otpcode = "";
		String otpMsg = "";
		Connection conn = null;
		ResponseData updateCountRes = new ResponseData();
		try {
			phoneno = request.getUserID();
			conn = ConnAdmin.getConn("001", "");
			String msg = "";
			ArrayList<SMSSettingData> smsList = new ArrayList<SMSSettingData>();
			SMSMgr smsMgr = new SMSMgr();
			smsList = smsMgr.checkMessageSetting("1", request.getType(), request.getMerchantID(),conn);
			if (smsList.size() > 0) {
				for (int i = 0; i < smsList.size(); i++) {
					if (smsList.get(i).getFrom().equalsIgnoreCase("1")) {
						msg = smsList.get(i).getFromMsg();
					}
				}
			}
			if (msg.equals("")) {
				response.setCode("0014");
				response.setDesc("There is no OTP message");
			}
			if (flag == 1) // within 5 min,same OTP will be generated
			{
				data = session_dao.getOTPCodeFromTbl(request.getSessionID(), conn);
				otpcode = data.getOtpCode();
				updateCountRes = session_dao.updateGetOTPCount(data.getrKey(), conn);// update
				result.setCode(updateCountRes.getCode());
			} else // new OTP will be generated
			{
				otpcode = generateOTP(); // generate OTP
				result = session_dao.insertOTPSession(request, phoneno, otpcode, conn);
			}
			if (!msg.equalsIgnoreCase("") && !msg.equalsIgnoreCase("null")) {
				otpMsg = msg.replace("<OTP>", otpcode);
				otpMsg = otpMsg.replace("<OTPCode>", otpcode);
			}
			if (result.getCode().equalsIgnoreCase("0000")) {
				String env = ConnAdmin.readConfig("SMSAggregator");
				if (env.equalsIgnoreCase("MIT")) {
					Response smsresponse = null;
					smsresponse = MITMessageService.sendSMSMessage(phoneno, otpMsg, "text");
					if (smsresponse.getRESULT() != null) {// PURE SUCCESS
						if (smsresponse.getRESULT().toLowerCase().contains("success")) {
							response.setCode("0000");
							response.setDesc("Success");

							if (flag == 0) {
								response.setrKey(result.getrKey());
							} else {
								response.setrKey(data.getrKey());
							}

						} else {
							response.setCode("0014");
							response.setDesc("Unknown SMS Error");
						}
					} else {
						response.setCode("0014");
						response.setDesc(smsresponse.getERRORDESC());
					}
					// new version
					MessageData msgData = new MessageData();
					//msgData = MITMessageService.sendSMSApi(phoneno, otpMsg);
					if(msgData.getStatus().equals("1")){
						response.setCode("0000");
						response.setDesc(msgData.getResponseMessage());
						if (flag == 0) {
							response.setrKey(result.getrKey());
						} else {
							response.setrKey(data.getrKey());
						}
					}else if(msgData.getStatus().equals("0")){
						response.setCode("0000");
						response.setDesc("Success");
						if (flag == 0) {
							response.setrKey(result.getrKey());
						} else {
							response.setrKey(data.getrKey());
						}
					}else {
						response.setCode("0014");
						response.setDesc(msgData.getResponseMessage());
					}
				}
				// Response log
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("Time :" + GeneralUtil.getTime());
					l_err.add("SMS Response Message.... : ");
					l_err.add("Error :" + response.getCode() + "," + response.getDesc());
					l_err.add("Result :" + response.getrKey());
					l_err.add("=============================================================================");
					GeneralUtil.writeLog(l_err, "\\SMS\\log\\");
				}
			} else {
				response.setCode("0014");
				response.setDesc("Insert Failed");
			}
		} catch (Exception e) {
			response.setCode("0014");
			response.setDesc("Failed");
		}
		return response;
	}*/
	public OTPRes sendOTPCode(OTPReq request, int flag) {
		OTPRes response = new OTPRes();
		OTPRes result = new OTPRes();
		UserDao u_dao = new UserDao();
		SessionDAO session_dao = new SessionDAO();
		CheckOTPRequestData data = new CheckOTPRequestData();
		String phoneno = "";
		String otpcode = "";
		String otpMsg = "";
		String fingerprint="0";
		Connection conn = null;
		ResponseData updateCountRes = new ResponseData();
		try {
			phoneno = request.getUserID();
			conn = ConnAdmin.getConn("001", "");
			String msg = "";
			ArrayList<SMSSettingData> smsList = new ArrayList<SMSSettingData>();
			SMSMgr smsMgr = new SMSMgr();
			smsList = smsMgr.checkMessageSetting("1", request.getType(), request.getMerchantID(),conn);
			if (smsList.size() > 0) {
				for (int i = 0; i < smsList.size(); i++) {
					if (smsList.get(i).getFrom().equalsIgnoreCase("1")) {
						msg = smsList.get(i).getFromMsg();
					}
				}
			}
			if (msg.equals("")) {
				response.setCode("0014");
				response.setDesc("There is no OTP message");
			}
			if (flag == 1) // within 5 min,same OTP will be generated
			{
				data = session_dao.getOTPCodeFromTbl(request.getSessionID(), conn);
				otpcode = data.getOtpCode();
				updateCountRes = session_dao.updateGetOTPCount(data.getrKey(), conn);// update
				result.setCode(updateCountRes.getCode());
			} else // new OTP will be generated
			{
				otpcode = generateOTP(); // generate OTP
				if (phoneno.equals("+959955795474") || phoneno.equals("+959770001680")|| phoneno.equals("+959777457947")
						|| phoneno.equals("+959444602278")|| phoneno.equals("+959794500759")) {

					otpcode = "123456";
				}
				result = session_dao.insertOTPSession(request.getType(), phoneno, otpcode,fingerprint,request.getDeviceID(),request.getFlag(),conn);
			}
			
			if (!msg.equalsIgnoreCase("") && !msg.equalsIgnoreCase("null")) {
				otpMsg = msg.replace("<OTP>", otpcode);
				otpMsg = otpMsg.replace("<OTPCode>", otpcode);
			}
			if (result.getCode().equalsIgnoreCase("0000")) {
				String env = ConnAdmin.readConfig("SMSAggregator");
				if (env.equalsIgnoreCase("MIT")) {
					/*Response smsresponse = null;
					smsresponse = MITMessageService.sendSMSMessage(phoneno, otpMsg, "text");
					if (smsresponse.getRESULT() != null) {// PURE SUCCESS
						if (smsresponse.getRESULT().toLowerCase().contains("success")) {
							response.setCode("0000");
							response.setDesc("Success");

							if (flag == 0) {
								response.setrKey(result.getrKey());
							} else {
								response.setrKey(data.getrKey());
							}

						} else {
							response.setCode("0014");
							response.setDesc("Unknown SMS Error");
						}
					} else {
						response.setCode("0014");
						response.setDesc(smsresponse.getERRORDESC());
					}*/
					// new version
					MessageData msgData = new MessageData();
					if(phoneno.equals("+959955795474") || phoneno.equals("+959770001680") || phoneno.equals("+959777457947")
							|| phoneno.equals("+959444602278")|| phoneno.equals("+959794500759")){
					
					}else  msgData = MITMessageService.sendSMSApi(phoneno, otpMsg);
					if(msgData.getStatus().equals("1")){
						response.setCode("0000");
						response.setDesc(msgData.getResponseMessage());
						if (flag == 0) {
							response.setrKey(result.getrKey());
						} else {
							response.setrKey(data.getrKey());
						}
					}else if(msgData.getStatus().equals("0")){
						response.setCode("0000");
						response.setDesc("Success");
						if (flag == 0) {
							response.setrKey(result.getrKey());
						} else {
							response.setrKey(data.getrKey());
						}
					}else {
						response.setCode("0014");
						response.setDesc(msgData.getResponseMessage());
					}
				}
				// Response log
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("Time :" + GeneralUtil.getTime());
					l_err.add("SMS Response Message.... : ");
					l_err.add("Error :" + response.getCode() + "," + response.getDesc());
					l_err.add("Result :" + response.getrKey());
					l_err.add("=============================================================================");
					GeneralUtil.writeLog(l_err, "\\SMS\\log\\");
				}
			} else {
				response.setCode("0014");
				response.setDesc("Insert Failed");
			}
		} catch (Exception e) {
			response.setCode("0014");
			response.setDesc("Failed");
		}
		return response;
	}

	public ResponseData updateActivityTime_Old(String sessionId, String userID) {
		ResponseData res = new ResponseData();
		Connection conn = null;
		SessionDAO s_DAO = new SessionDAO();
		conn = ConnAdmin.getConn();
		try {
			res = s_DAO.updateActivityTime_Old(sessionId, userID, conn);
		} catch (Exception e) {
			res.setCode("0014");
			res.setDesc(e.getMessage());
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}
	public ResponseData updateActivityTime(String sessionId, String userID,String formID) {
		ResponseData res = null;
		Connection conn = null;
		SessionDAO s_DAO = new SessionDAO();

		try {
			conn = ConnAdmin.getConn("001", "");
			res = s_DAO.updateActivityTime(sessionId, userID,formID, conn);
			/*if(s_DAO.checkSessionExpired(sessionId, userID, conn)){
				res.setCode("0016");
				res.setDesc("Your session has expired. Please signin again.");
			}*/
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return res;
	}

	public ResponseData updateSecuritySession(String sessionId, String userid, String securityKey, String status,
			String preStatus) {
		ResponseData res = new ResponseData();
		Connection conn = null;
		SessionDAO s_DAO = new SessionDAO();
		conn = ConnAdmin.getConn("001", "");
		boolean rt = false;
		try {
			rt = s_DAO.updateSecuritySession(securityKey, status, sessionId, userid, preStatus, conn);
			if (rt) {
				res.setCode("0000");
				res.setDesc("Success");
			} else {
				res.setCode("0014");
				res.setDesc("Fail");
			}
		} catch (Exception e) {
			res.setCode("0014");
			res.setDesc("Fail");
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public ResponseData validateactivity(String accountNo, String sessionId, String userId) {
		ResponseData response = new ResponseData();
		if (accountNo.equalsIgnoreCase("null") || accountNo.equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("Account No. is manadatory!");
		} else if (sessionId.equalsIgnoreCase("null") || sessionId.equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("Invalid Session ID !");
		} else if (userId.equalsIgnoreCase("null") || userId.equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("Invalid User ID !");
		} else {
			response.setCode("0000");
			response.setDesc("Success");
		}
		return response;
	}

	public ResponseData validatecheckOTPReq(String sessionId, String userId, String rkey, String otpCode) {
		ResponseData response = new ResponseData();
		if (sessionId.equalsIgnoreCase("null") || sessionId.equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("Session ID is mandatory");
		} else if (userId.equalsIgnoreCase("null") || userId.equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("User ID is mandatory");
		} else if (rkey.equalsIgnoreCase("null") || rkey.equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("Reference Key is mandatory");
		} else if (otpCode.equalsIgnoreCase("null") || otpCode.equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("OTP Code is mandatory");
		} else {
			response.setCode("0000");
			response.setDesc("Success");
		}
		return response;
	}
	
	public ResponseData validatecheckOTPReqV2(String sessionId, String userId, String rkey, String otpCode) {
		ResponseData response = new ResponseData();
		if (userId.equalsIgnoreCase("null") || userId.equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("User ID is mandatory");
		} else if (rkey.equalsIgnoreCase("null") || rkey.equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("Reference Key is mandatory");
		} else if (otpCode.equalsIgnoreCase("null") || otpCode.equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("OTP Code is mandatory");
		}
//		else if (deviceId.equalsIgnoreCase("null") || deviceId.equalsIgnoreCase("")) {
//			response.setCode("0014");
//			response.setDesc("Device ID is mandatory");
//		}
		else {
			response.setCode("0000");
			response.setDesc("Success");
		}
		return response;
	}

	public ResponseData validatecheckOTPReqW(String userId, String rkey, String otpCode) {
		ResponseData response = new ResponseData();
		if (userId.equalsIgnoreCase("null") || userId.equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("User ID is mandatory");
		} else if (rkey.equalsIgnoreCase("null") || rkey.equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("Reference Key is mandatory");
		} else if (otpCode.equalsIgnoreCase("null") || otpCode.equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("OTP Code is mandatory");
		} else {
			response.setCode("0000");
			response.setDesc("Success");
		}
		return response;
	}

	public ResponseData validateOTP(String otpcode, String rKey, String sessionId, String userId) {
		ResponseData response = new ResponseData();
		/*
		 * if(otpcode.equalsIgnoreCase("null") || otpcode.equalsIgnoreCase("")){
		 * response.setCode("0014"); response.setDesc("OTP Code is manadatory!"
		 * ); }else
		 */
		if (sessionId.equalsIgnoreCase("null") || sessionId.equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("Invalid Session ID !");
		} else if (userId.equalsIgnoreCase("null") || userId.equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("Invalid User ID !");
		} else {
			response.setCode("0000");
			response.setDesc("Success");
		}
		return response;
	}
	public OTPRes generateSessionID(String userid,String type,String otpcode,String deviceID,String flag){
		OTPRes res=new OTPRes();
		Connection conn = null;
		String fingerprint="1";
		SessionDAO s_DAO = new SessionDAO();
		try {
			conn = ConnAdmin.getConn("001", "");
			res = s_DAO.insertOTPSession(type,userid, otpcode,fingerprint,deviceID,flag, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
		
	}
	public ResponseData selectDeviceID(String userID){
		String userid=userID;
		ResponseData res=new ResponseData();
		SessionDAO s_DAO = new SessionDAO();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = s_DAO.selectDeviceID(userid, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		
		return res;
		
	}
	

public ResponseData updateT2(String userID,String DeviceID,String flag){
	ResponseData res=new ResponseData();
	SessionDAO s_DAO = new SessionDAO();
	Connection conn = null;
	try {
		conn = ConnAdmin.getConn("001", "");
		res = s_DAO.updateT2(userID,DeviceID,flag, conn);
	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		ServerUtil.closeConnection(conn);
	}
	
	return res;
	
}

}
