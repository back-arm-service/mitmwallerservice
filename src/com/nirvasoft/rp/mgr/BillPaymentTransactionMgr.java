package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;

import com.nirvasoft.rp.dao.BillPaymentTransactionDao;
import com.nirvasoft.rp.dao.SessionDAO;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.shared.BillPaymentTransactionReq;
import com.nirvasoft.rp.shared.BillPaymentTransactionRes;
import com.nirvasoft.rp.util.ServerUtil;

public class BillPaymentTransactionMgr {

	public BillPaymentTransactionRes getBillPaymentTansaction(BillPaymentTransactionReq req) {
		// TODO Auto-generated method stub
		BillPaymentTransactionRes ret = new BillPaymentTransactionRes();
		Connection conn = null;
		BillPaymentTransactionDao dao = new BillPaymentTransactionDao();
		try {
			conn = ConnAdmin.getConn("001", "");
			ret = dao.getBillPaymentTransList(req, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return ret;
	}

	public Result insertSessionID(String sessionId, String userId, String activity, String expired, String duration)
			throws ParseException {

		Result res = new Result();
		Connection conn = null;
		SessionDAO s_DAO = new SessionDAO();
		try {
			conn = ConnAdmin.getConn("001", "");
			if (conn.equals("null") || conn.equals("")) {
				res.setState(false);
				res.setMsgDesc("Connection Fail");
			} else {
				res = s_DAO.saveActivity(sessionId, userId, activity, expired, duration, conn);

			}
		} catch (SQLException e) {
			res.setState(false);
			res.setMsgDesc("Connection Fail");
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;

	}

}
