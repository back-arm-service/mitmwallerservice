package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.dao.FAQDao;
import com.nirvasoft.rp.data.FAQResponseData;
import com.nirvasoft.rp.data.FAQResponseDataArr;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ServerGlobal;

public class FAQMgr {

	public FAQResponseDataArr getFAQList() {
		FAQResponseDataArr faqData = new FAQResponseDataArr();
		DAOManager dao = new DAOManager();
		Connection conn = null;
		FAQDao userDAO = new FAQDao();
		List<FAQResponseData> faqList = null;

		try {
			conn = dao.openConnection();
			if (conn.equals("null") || conn.equals("")) {
				faqData.setCode("0014");
				faqData.setDesc("Connection Error.");
				return faqData;
			} else {
				faqList = userDAO.getFAQList(conn);
			}

		} catch (Exception e) {
			faqList = null;

			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("getFAQList in FAQMgr.... " + e.toString());
				l_err.add("-----------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
			}

		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				faqList = null;
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("");
					l_err.add("=============================================================================");
					l_err.add("Time :" + GeneralUtil.getTime());
					l_err.add("getFAQList in FAQMgr.... " + e.toString());
					l_err.add("-----------------------------------------------------------------------------");
					GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
				}
			}
		}

		if (faqList != null && faqList.size() != 0) {
			FAQResponseData faqArr[] = new FAQResponseData[faqList.size()];

			for (int i = 0; i < faqList.size(); i++) {

				faqArr[i] = faqList.get(i);
			}

			faqData.setCode("0000");
			faqData.setDesc("Getting FAQ List Success.");
			faqData.setFaqData(faqArr);
		} else {
			faqData.setCode("0014");
			faqData.setDesc("Getting FAQ List Fail.");
		}

		return faqData;
	}

}
