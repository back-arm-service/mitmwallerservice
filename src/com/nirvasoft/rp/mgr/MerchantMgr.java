package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.nirvasoft.cms.framework.Result;
import com.nirvasoft.database.SysKeyMgr;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.dao.MerchantDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.shared.CMSMerchantAccJunctionArr;
import com.nirvasoft.rp.shared.CMSMerchantData;
import com.nirvasoft.rp.shared.CMSMerchantDataSet;
import com.nirvasoft.rp.util.ServerUtil;

public class MerchantMgr {

	/*
	 * public AccountGLData checkAccGL(String account) { Connection l_Conn =
	 * null; MerchantDao m_dao = new MerchantDao(); AccountGLData aData = new
	 * AccountGLData();
	 * 
	 * try { l_Conn = ConnAdmin.getAnotherConn("ConncetionConfig.txt", "001");
	 * aData = m_dao.checkAccGL(account, l_Conn); } catch (SQLException e) {
	 * e.printStackTrace(); } return aData; }
	 */

	/*
	 * public Result checkAcctNoBrCode(String branchCode, String AcctNo) {
	 * 
	 * Result ret = new Result(); MerchantDao custdao = new MerchantDao();
	 * Connection l_Conn = null;
	 * 
	 * try { l_Conn = ConnAdmin.getAnotherConn("ConncetionConfig.txt", "001");
	 * ret = custdao.checkAcctNoBrCode(branchCode, AcctNo, l_Conn); } catch
	 * (SQLException e) {
	 * 
	 * e.printStackTrace();
	 * 
	 * } finally {
	 * 
	 * try { if (!l_Conn.isClosed()) l_Conn.close(); } catch (SQLException e) {
	 * // TODO Auto-generated catch block e.printStackTrace(); } }
	 * 
	 * return ret;
	 * 
	 * }
	 */

	/*
	 * public BranchCodeData checkBranchCode() { BranchCodeData aData = new
	 * BranchCodeData(); MerchantDao u_dao = new MerchantDao(); Connection con =
	 * ConnAdmin.getConn("001", ""); try { aData = u_dao.checkBranchCode(con);
	 * 
	 * } finally { ServerUtil.closeConnection(con); } return aData; }
	 */

	public Result deletebyMerchantID(String aUserID) {
		// TODO Auto-generated method stub
		Result res = new Result();
		Connection l_Conn = null;
		MerchantDao l_DAO = new MerchantDao();
		try {
			l_Conn = ConnAdmin.getAnotherConn("ConncetionConfig.txt", "001");

			if (l_DAO.deletebyMerchantID(aUserID, l_Conn)) {
				res.setState(true);
				res.setMsgCode("0002");
				res.setMsgDesc("Deleted Successfully.");
			} else {
				res.setState(false);
				res.setMsgCode("0052");
				res.setMsgDesc("Delete Failed.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			res.setState(false);
			res.setMsgCode("0014");
			res.setMsgDesc(e.getMessage());
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				res.setState(false);
				res.setMsgCode("0014");
				res.setMsgDesc(e.getMessage());
			}
		}

		return res;
	}
	public Result deleteCMSMerchantData(CMSMerchantData data) {

		Result ret = new Result();
		DAOManager l_DAO = new DAOManager();
		MerchantDao custdao = new MerchantDao();
		Connection l_Conn = null;
		try {

			l_Conn = ConnAdmin.getConn("001", "");
			ret = custdao.deleteCMSMerchantData(data, l_Conn);

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return ret;

	}

	public ArrayList<CMSMerchantData> getAllMerchant() {
		Connection l_Conn = null;
		MerchantDao m_dao = new MerchantDao();
		ArrayList<CMSMerchantData> res = new ArrayList<CMSMerchantData>();

		try {
			l_Conn = ConnAdmin.getAnotherConn("ConncetionConfig.txt", "001");
			res = m_dao.getAllMerchant(l_Conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public CMSMerchantDataSet getAllMerchantData(String searchText, int pageSize, int currentPage) {
		CMSMerchantDataSet res = new CMSMerchantDataSet();
		MerchantDao m_dao = new MerchantDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = m_dao.getAllMerchantData(searchText, pageSize, currentPage, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return res;
	}

	public CMSMerchantData getCMSMerchantDataByID(String acode) {

		CMSMerchantData ret = new CMSMerchantData();
		DAOManager l_DAO = new DAOManager();
		Connection l_Conn = null;

		try {

			l_Conn = ConnAdmin.getAnotherConn("ConncetionConfig.txt", "001");
			MerchantDao custdao = new MerchantDao();
			ret = custdao.getCMSMerchantDataByID(acode, l_Conn);

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return ret;
	}

	public CMSMerchantAccJunctionArr getDataByMerchantID(String id) throws SQLException {
		// TODO Auto-generated method stub
		CMSMerchantAccJunctionArr ret = new CMSMerchantAccJunctionArr();
		MerchantDao m_dao = new MerchantDao();
		Connection conn = null;

		try {

			conn = ConnAdmin.getAnotherConn("ConncetionConfig.txt", "001");
			ret = m_dao.getDataByMerchantID(id, conn);

		} finally {

			try {
				if (!conn.isClosed())
					conn.close();

			} catch (SQLException e) {

				e.printStackTrace();
			}
		}

		return ret;
	}

	public String getDate() {
		Date todaydate = new Date();
		DateFormat dateformat = new SimpleDateFormat("yyyyMMdd");
		String today = dateformat.format(todaydate);
		return today;
	}

	public CMSMerchantData getMerchantDataByID(String merchantId) {

		CMSMerchantData ret = new CMSMerchantData();
		MerchantDao m_dao = new MerchantDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			ret = m_dao.getMerchantDataByID(merchantId, conn);

		} catch (SQLException e) {

			e.printStackTrace();
		} finally {

			try {
				if (!conn.isClosed())
					conn.close();

			} catch (SQLException e) {

				e.printStackTrace();
			}
		}

		return ret;
	}

	public CMSMerchantData getMerchantAccount(String merchantType) throws Exception {

		CMSMerchantData ret =new CMSMerchantData();
		MerchantDao m_dao = new MerchantDao();
		Connection conn = null;
		try {			conn = ConnAdmin.getConn("001", "");
			ret = m_dao.getMerchantAccount(conn,merchantType);

		} catch (SQLException e) {

			e.printStackTrace();
		} finally {

			try {
				if (!conn.isClosed())
					conn.close();

			} catch (SQLException e) {

				e.printStackTrace();
			}
		}

		return ret;
	}

	// Merchant Account Mapping Start //
	public CMSMerchantAccJunctionArr getMerchantMappingList(String searchText, int pageSize, int currentPage) {
		CMSMerchantAccJunctionArr res = new CMSMerchantAccJunctionArr();
		MerchantDao m_dao = new MerchantDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getAnotherConn("ConncetionConfig.txt", "001");
			res = m_dao.getMerchantMappingList(searchText, pageSize, currentPage, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return res;
	}

	public Result saveCMSMerchantData(CMSMerchantData data) {
		Result ret = new Result();
		MerchantDao custdao = new MerchantDao();
		Connection l_Conn = null;
		try {
			l_Conn = ConnAdmin.getConn("001", "");

			data.setCreatedDate(getDate());
			data.setModifiedDate(getDate());
			if (data.getUserId().equals("")) {
				ret = custdao.saveCMSMerchantData(data, l_Conn);
				if (ret.isState()) {
					ret.setMsgDesc("Saved Successfully");

				} else {
					if (ret.getMsgCode().equals("0015")) {
						ret.setMsgDesc("Account No./GL already exists");
					} else if (ret.getMsgCode().equals("0017")) {
						ret.setMsgDesc("Invalid Branch Code");
					} else {

						ret.setMsgDesc("Saved Fail");
					}
				}
			} else {
				ret = custdao.updateCMSMerchantData(data, l_Conn);
				if (ret.isState()) {
					ret.setMsgDesc("Updated Successfully");
					ret.setMsgCode("0000");
				} else {

					ret.setMsgDesc(ret.getMsgDesc());
					ret.setMerchantID(data.getUserId());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return ret;

	}

	public CMSMerchantAccJunctionArr saveMerchantAccountJunction(CMSMerchantAccJunctionArr aData) {

		DAOManager l_DAO = new DAOManager();
		Connection l_Conn = null;
		MerchantDao m_Dao = new MerchantDao();
		String id = "";
		long Hkey = 0;
		try {
			l_Conn = ConnAdmin.getAnotherConn("ConncetionConfig.txt", "001");
			String merchantid = "";
			if (aData.getData().length > 0 && aData.getData() != null) {
				merchantid = aData.getData()[0].getT1();
			}
			if (merchantid.equals("")) {
				aData.setMessagecode("0014");
				aData.setMessagedesc("Merchant ID is blank.");
			} else {
				String result = m_Dao.saveMerchantAccountJunction(aData, merchantid, l_Conn);
				if (result.equals("01")) {
					aData.setMessagecode("0000");
					aData.setMessagedesc("Saved Successfully.");
				} else if (result.equals("00")) {
					aData.setMessagecode("0050");
					aData.setMessagedesc("Save Failed.");
				} else if (result.equals("11")) {
					aData.setMessagecode("0001");
					aData.setMessagedesc("Updated Successfully.");
				} else if (result.equals("10")) {
					aData.setMessagecode("0051");
					aData.setMessagedesc("Update Failed.");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			aData.setMessagedesc(e.getMessage());
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				aData.setMessagedesc(e.getMessage());
			}
		}
		return aData;
	}

	// Merchant Account Mapping End //
}
