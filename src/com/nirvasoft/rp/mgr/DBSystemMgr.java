package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.rp.core.ccbs.dao.CurrencyTableDao;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.dao.SystemSettingDAO;
import com.nirvasoft.rp.dao.icbs.ProductsDAO;
import com.nirvasoft.rp.dao.icbs.ReferenceAccountsDAO;
import com.nirvasoft.rp.mgr.icbs.DBProductMgr;
import com.nirvasoft.rp.shared.ProductData;
import com.nirvasoft.rp.shared.ReferenceData;
import com.nirvasoft.rp.shared.SystemData;
import com.nirvasoft.rp.shared.SystemSettingData;
import com.nirvasoft.rp.shared.icbs.ReferenceAccountsData;

public class DBSystemMgr {	
	public static ReferenceAccountsData getReferenceAccCode(String pCode) throws Exception {
		ReferenceAccountsData l_ReferenceAccountData = new ReferenceAccountsData();
		DAOManager myConnectionUtil = new DAOManager();
		Connection conn = myConnectionUtil.openICBSConnection();

		try {
			l_ReferenceAccountData = getReferenceAccCode(pCode, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null)
				if (!conn.isClosed())
					conn.close();
		}
		return l_ReferenceAccountData;
	}
	public static ReferenceAccountsData getReferenceAccCode(String pCode, Connection pConn) {
		ReferenceAccountsData object = new ReferenceAccountsData();
		ReferenceAccountsDAO l_DAO = new ReferenceAccountsDAO();
		try {
			if (l_DAO.getReferenceAccCode(pCode, pConn)) {
				object = l_DAO.getReferenceAccountsDataBean();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return object;
	}
	public static String getByForceCheck() throws Exception {
		String ret = "";
		DAOManager myConnectionUtil = new DAOManager();
		Connection conn = myConnectionUtil.openICBSConnection();
		try {
			ret = getByForceCheck(conn);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			if (conn != null)
				if (!conn.isClosed())
					conn.close();
		}
		return ret;
	}
	public static String getByForceCheck(Connection conn) {
		String ret = "";
		SystemSettingDAO l_DAO = new SystemSettingDAO();
		SystemSettingData l_Data = new SystemSettingData();
		try {
			l_Data.setT1("CHQ");
			l_Data = l_DAO.getSystemSettingData(l_Data, conn);
			ret = l_Data.getT4();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return ret;
	}
	public static ArrayList<ProductData> readProducts(Connection pConn) throws SQLException {
		ArrayList<ProductData> ret = new ArrayList<ProductData>();
		ProductsDAO l_ProductDAO = new ProductsDAO();
		try {
			if (l_ProductDAO.getProducts(pConn))
				ret = l_ProductDAO.getProductDataList();
			else
				ret = null;
			//

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ret;
	}

	public static SystemData readSystemData(Connection pConn) throws Exception {
		SystemData ret = new SystemData();
		ret.setProducts(readProducts(pConn));
		ret.setProductsType(DBProductMgr.getProductsType(pConn));
		ret.setCurrencyCodes(DBSystemMgr.readCurrencyCodes(pConn));
		ret.setProductsNumber(DBProductMgr.getProductsNumber(pConn));
		ret.setProductsConfiguration(DBProductMgr.getProductsConfiguration(pConn));
		ret.setSystemSettingDatas(getSystemSettingDatas(pConn));
		return ret;
	}
	
	public static ArrayList<SystemSettingData> getSystemSettingDatas(Connection pConn) throws SQLException {
		ArrayList<SystemSettingData> ret = new ArrayList<SystemSettingData>();
		SystemSettingDAO l_DAO = new SystemSettingDAO();
		try {
			ret = l_DAO.getSystemSettingDatas(pConn);
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return ret;
	}

	
	public static ArrayList<ReferenceData> readCurrencyCodes() throws SQLException{
		ArrayList<ReferenceData> ret  =  new ArrayList<ReferenceData>();
		DAOManager lConn=new DAOManager();
		Connection conn = lConn.CreateConnection();	
		ret = readCurrencyCodes(conn);
		conn.close();
		return ret;
	}

	public static ArrayList<ReferenceData> readCurrencyCodes(Connection pConn) throws SQLException{
		ArrayList<ReferenceData> ret  =  new ArrayList<ReferenceData>();
		CurrencyTableDao lCurDAO =new CurrencyTableDao();
		if (lCurDAO.getCurrencyCodes(pConn))
			ret = lCurDAO.getCurrencyDataList();
		else
			ret = null;
		return ret;		
	}
}
