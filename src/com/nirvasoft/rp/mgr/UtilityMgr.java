package com.nirvasoft.rp.mgr;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.net.ConnectException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.nirvasoft.rp.dao.UtilityDao;
import com.nirvasoft.rp.data.UtilityResponse;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.shared.AccTransferReq;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.shared.TransferOutReq;
import com.nirvasoft.rp.shared.TransferOutRes;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ServerGlobal;
import com.nirvasoft.rp.util.ServerUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class UtilityMgr {

	@SuppressWarnings("unchecked")
	public ResponseData callCCD(UtilityResponse utilityRes) throws ParseException {
		GeneralUtil.readCCDAPIServiceSetting();
		String url = ServerGlobal.getmGetCCDURL();
		String status, code, message;
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("----------------------------------Online-------------------------------------------");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("KEY :" + utilityRes.getBillId());
			l_err.add("Request Message : ");
			l_err.add(url);
			l_err.add("===================================================================================");
			GeneralUtil.writeLog(l_err, "\\CCD\\log\\");
		}
		ResponseData res = new ResponseData();
		Client client = Client.create();
		WebResource webResource = client.resource(url);
		ClientResponse response = null;
		JSONObject obj = new JSONObject();

		obj.put("bill_id", utilityRes.getBillId());
		obj.put("ref_no", utilityRes.getRefNo());
		obj.put("txn_ref", utilityRes.getTxnRef());
		obj.put("bill_amount", utilityRes.getBillAmount());
		obj.put("penalty_amount", "0.00");
		obj.put("commission_amount", "0.00");
		obj.put("discount_amount", "0.00");
		obj.put("total_amount", utilityRes.getBillAmount());
		obj.put("ccy_code", utilityRes.getCcyCode());
		obj.put("due_date", utilityRes.getDueDate());
		obj.put("txn_date", utilityRes.getTxnDate());
		obj.put("paid_by", utilityRes.getPaidBy());
		obj.put("additional_t1", "");
		obj.put("additional_t2", "");
		obj.put("additional_t3", "");
		obj.put("additional_n1", "");
		obj.put("additional_n2", "");
		obj.put("additional_n3", "");

		System.out.println(obj.toString());
		try {
			response = webResource.type("application/json").post(ClientResponse.class, obj.toString());
		} catch (Exception ex) {

			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("----------------------------------Online-------------------------------------------");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("KEY :" + utilityRes.getBillId());
				l_err.add("Response Message : " + "api calling");
				l_err.add(ex.getMessage());
				l_err.add("===================================================================================");
				GeneralUtil.writeLog(l_err, "\\CCD\\log\\");
			}
			return res;
		}
		String jsonStr = response.getEntity(String.class);
		JSONParser parser = new JSONParser();
		Object object = parser.parse(jsonStr);
		JSONObject jsonObject = (JSONObject) object;
		status = (String) jsonObject.get("message");

		if (status == null) {
			status = (String) jsonObject.get("message");
		}
		if (status.equals("Payment Success")) {
			message = "Post Successfully";
			code = "0000";
		} else {
			status = (String) jsonObject.get("message");
			message = (String) jsonObject.get("message");
			code = "014";
		}
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("----------------------------------Online-------------------------------------------");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("KEY :" + utilityRes.getBillId());
			l_err.add("Response Message : ");
			l_err.add(jsonStr);
			l_err.add("===================================================================================");
			GeneralUtil.writeLog(l_err, "\\CCD\\log\\");
		}
		res.setCode(code);
		res.setDesc(message);
		return res;
	}

	public UtilityResponse saveUtility(TransferOutReq reqData, TransferOutRes res) {
		Connection conn = null;
		conn = ConnAdmin.getConn("001", "");
		UtilityDao utilityDao = new UtilityDao();
		UtilityResponse response = new UtilityResponse();
		try {
			response = utilityDao.saveUtility(reqData, res, conn);
		} catch (Exception e) {
			response.setCode("0014");
			response.setDesc(e.getMessage());
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return response;
	}

	public UtilityResponse updateUtility(ResponseData resp, String transRef) {

		Connection conn = null;
		conn = ConnAdmin.getConn("001", "");
		UtilityDao utilityDao = new UtilityDao();
		UtilityResponse response = new UtilityResponse();
		try {
			response = utilityDao.updateUtility(resp, transRef, conn);
		} catch (Exception e) {
			response.setCode("0014");
			response.setDesc(e.getMessage());
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return response;

	}
	
	@SuppressWarnings("unchecked")
	public UtilityResponse callCCDAPI(AccTransferReq req,String refNo,String txnRef,String effictiveDate) throws Exception {
		UtilityResponse res = new UtilityResponse();
		JSONObject reqObj = new JSONObject();
		JSONObject responseObj = new JSONObject();

		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("Time : " + GeneralUtil.getTime());
			l_err.add("KEY : " + req.getBillId());
			l_err.add("callCCD : AppService Request");
			l_err.add("AppService Request Object : " + req);
			writeLog(l_err);
		}

		// read URL and settings from Config
		String urlString = ServerGlobal.getmGetCCDURLTrans();
		String connectTimeoutString = ServerGlobal.getmGetCCDConnectTimeout();
		String readTimeoutString = ServerGlobal.getmGetCCDReadTimeout();

		String status = "";
		String checkBillID = "";
		String checkRefNo = "";

		try {
			URL url = new URL(urlString);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();

			// set the request method and properties.
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			con.setRequestProperty("Accept", "application/json");

			// set Connection Timeout and Read Input Stream Timeout
			con.setConnectTimeout(Integer.parseInt(connectTimeoutString));
			con.setReadTimeout(Integer.parseInt(readTimeoutString));

			// set the auto redirection to true
			HttpURLConnection.setFollowRedirects(true);

			// set the doInput to true.
			// con.setDoInput(true);

			// set the doOutput to true.
			con.setDoOutput(true);

			// prepare Request Obj
			reqObj.put("bill_id", req.getBillId());
			reqObj.put("ref_no", refNo);
			reqObj.put("txn_ref", txnRef);
			reqObj.put("bill_amount", req.getCurrentAmount());
			if (req.getPenalty().equals(""))
				req.setPenalty("0");
			reqObj.put("penalty_amount", req.getPenalty());
			reqObj.put("commission_amount", req.getBankCharges());
			reqObj.put("discount_amount", "");
			reqObj.put("total_amount", req.getAmount());
			reqObj.put("ccy_code", "MMK");
			reqObj.put("due_date", req.getDueDate());
			reqObj.put("txn_date", GeneralUtil.getDateYYYYMMDDHHMMSS());
			reqObj.put("paid_by", "mWallet");
			reqObj.put("additional_t1", "");
			reqObj.put("additional_t2", "");
			reqObj.put("additional_t3", "");
			reqObj.put("additional_n1", "");
			reqObj.put("additional_n2", "");
			reqObj.put("additional_n3", "");
			reqObj.put("effective_date", effictiveDate);

			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("Time : " + GeneralUtil.getTime());
				l_err.add("KEY : " + req.getBillId());
				l_err.add("CallCCD : CCD Request");
				l_err.add("CallCCD Request URL : " + urlString);
				l_err.add("CallCCD Request Object : " + reqObj.toString());
				l_err.add("CallCCD Request Connect Timeout : " + connectTimeoutString);
				l_err.add("CallCCD Request Read Timeout : " + readTimeoutString);
				writeLog(l_err);
			}

			// send Request Obj
			OutputStream os = con.getOutputStream();
			byte[] outputBytes = reqObj.toString().getBytes("UTF-8");
			os.write(outputBytes);

			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("Time : " + GeneralUtil.getTime());
				l_err.add("KEY : " + req);
				l_err.add("CallCCD : CCD Response HTTP Status Code");
				l_err.add("CallCCD Request URL : " + urlString);
				l_err.add("CallCCD Request Object : " + reqObj.toString());
				l_err.add("CallCCD Request Connect Timeout : " + connectTimeoutString);
				l_err.add("CallCCD Request Read Timeout : " + readTimeoutString);
				l_err.add("AppService Request Object : " + req);
				l_err.add("CallCCD Response HTTP Status Code : " + con.getResponseCode());
				writeLog(l_err);
			}

			if (String.valueOf(con.getResponseCode()).equals("200")) {
				// get Response
				InputStream in = con.getInputStream();
				BufferedReader br = new BufferedReader(new InputStreamReader(in));
				StringBuffer response = new StringBuffer();
				String inputLine = "";
				while ((inputLine = br.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();

				res.setCcdresmsg(response.toString());

				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("Time : " + GeneralUtil.getTime());
					l_err.add("KEY : " + req.getBillId());
					l_err.add("CallCCD : CCD Response");
					l_err.add("CallCCD Request URL : " + urlString);
					l_err.add("CallCCD Request Object : " + reqObj);
					l_err.add("AppService Request Object : " + req);
					l_err.add("CallCCD Response HTTP Status Code : " + con.getResponseCode());
					l_err.add("CallCCD Response String : " + response.toString());
					writeLog(l_err);
				}

				JSONParser parser = new JSONParser();
				Object object = parser.parse(response.toString());
				responseObj = (JSONObject) object;
				status = (String) responseObj.get("message");

				if (status != null && (!status.equals("")) && status.equals("Payment Success")) {
					JSONObject checkObj = (JSONObject) responseObj.get("data");
					checkBillID = (String) checkObj.get("bill_id");
					checkRefNo = (String) checkObj.get("ref_no");
					if (checkBillID != null && (!checkBillID.equals("")) && checkBillID.equals(req.getBillId())
							&& checkRefNo != null && (!checkRefNo.equals(""))
							&& checkRefNo.equals(refNo)) {
						res.setCode("0000");
						res.setDesc("Payment is successful.");
						res.setStatus("Approved");
						res.setMessage(status);
					} else {
						res.setCode("0000");
						res.setStatus("Posted");
						res.setMessage(status);
						res.setDesc("Your transaction has been submitted for processing.");
					}
				} else {
					res.setCode("0000");
					res.setStatus("Posted");
					res.setMessage(status);
					res.setDesc("Your transaction has been submitted for processing.");
				}
			} else {
				res.setiCBStxnRef("");
				res.setCode("0000");
				res.setStatus("Posted");
				res.setMessage(status);
				res.setDesc("Your transaction has been submitted for processing.");
			}
		} catch (ConnectException ex) {
			ex.printStackTrace();

			res.setCode("0000");
			res.setStatus("Posted");
			res.setMessage(status);
			res.setDesc("Your transaction has been submitted for processing.");

			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("Time : " + GeneralUtil.getTime());
				l_err.add("KEY : " + req.getBillId());
				l_err.add("CallCCD : ");
				l_err.add("CallCCD Response Exception : " + ex.getMessage());
				l_err.add("CallCCD Request URL : " + urlString);
				l_err.add("CallCCD Request Object : " + reqObj);
				l_err.add("CallCCD Response Object : " + responseObj);
				l_err.add("AppService Request Object : " + req);
				l_err.add("AppService Response Object : " + res);
				writeLog(l_err);
			}

			return res;
		} catch (Exception ex) {
			ex.printStackTrace();

			res.setCode("0000");
			res.setStatus("Posted");
			res.setMessage(status);
			res.setDesc("Your transaction has been submitted for processing.");

			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("Time : " + GeneralUtil.getTime());
				l_err.add("KEY : " + req.getBillId());
				l_err.add("CallCCD : ");
				l_err.add("CallCCD Response Exception : " + ex.getMessage());
				l_err.add("CallCCD Request URL : " + urlString);
				l_err.add("CallCCD Request Object : " + reqObj);
				l_err.add("CallCCD Response Object : " + responseObj);
				l_err.add("AppService Request Object : " + req);
				l_err.add("AppService Response Object : " + res);
				writeLog(l_err);
			}

			return res;
		}

		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("Time : " + GeneralUtil.getTime());
			l_err.add("KEY : " + req.getBillId());
			l_err.add("CallCCD Request URL : " + urlString);
			l_err.add("CallCCD Request Object : " + reqObj);
			l_err.add("CallCCD Response Object : " + responseObj);
			l_err.add("AppService Request Object : " + req);
			l_err.add("AppService Response Object : " + res);
			writeLog(l_err);
		}

		return res;
	}
	private void writeLog(ArrayList<String> logDetailList) {
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("----------------------------------Online-------------------------------------------");
			for (String eachLine : logDetailList) {
				l_err.add(eachLine);
			}
			l_err.add("===================================================================================");
			GeneralUtil.writeLog(l_err, "\\CCD\\log\\");
		}
	}
}
