package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.rp.dao.UCJunctionDAO;
import com.nirvasoft.rp.dao.UserDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.shared.DepositAccDataResult;

public class UCJunctionMgr {

	public boolean checkUserAndAccount(String aUserID, String aFromAcc) {
		Connection conn = null;
		boolean result = false;
		UCJunctionDAO aUCdao = new UCJunctionDAO();
		try {
			conn = ConnAdmin.getConn("001", "");
			result = aUCdao.checkUserAndAccount(aUserID, aFromAcc, conn);

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				result = false;
			}
		}
		return result;
	}

	public Result getCusIDByLoginUser(String userID) {
		Connection conn = null;
		Result result = new Result();
		String customerid = "";
		ArrayList<String> customerIDList = new ArrayList<String>();
		try {
			conn = ConnAdmin.getConn("001", "");
			customerIDList = UCJunctionDAO.getCustomerID(userID, conn);
			if (customerIDList.size() >= 1) {
				if (customerIDList.size() == 1) {
					customerid += "'" + customerIDList.get(0).toString() + "'";
				} else {
					for (int i = 0; i < customerIDList.size() - 1; i++) {
						customerid += "'" + customerIDList.get(i).toString() + "',";
					}
					customerid += "'" + customerIDList.get(customerIDList.size() - 1).toString() + "'";
				}
				result.setState(true);
				result.setUserid(customerid);// Using Customer ID
			} else {
				result.setState(false);
				result.setMsgCode("0014");
				result.setMsgDesc("There is no Customer ID!");
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setState(false);
			result.setMsgCode("0014");
			result.setMsgDesc(e.getMessage());
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				result.setState(false);
				result.setMsgCode("0014");
				result.setMsgDesc(e.getMessage());
			}
		}
		return result;
	}

	public Result getCusIDByLoginUserV1(String userid) {
		Connection conn = null;
		String customerID = "";
		UserDao dao = new UserDao();
		Result result = new Result();
		try {
			conn = ConnAdmin.getConn("001", "");
			customerID = dao.getCustomerID(userid, conn);
			result.setState(true);
			result.setUserid(customerID);// Using Customer ID
		} catch (Exception e) {
			e.printStackTrace();
			result.setState(false);
			result.setMsgCode("0014");
			result.setMsgDesc(e.getMessage());
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				result.setState(false);
				result.setMsgCode("0014");
				result.setMsgDesc(e.getMessage());
			}
		}
		return result;
	}

	public Result saveUserAccJunction(String userID, String sessionId, String customerid, DepositAccDataResult aData) {
		Connection conn = null;
		Result result = new Result();
		boolean res = false;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = UCJunctionDAO.saveUAjunction(userID, sessionId, customerid, aData, conn);
			if (res) {
				result.setState(true);
				result.setUserid(customerid);// Using Customer ID
			} else {
				result.setState(false);
				result.setMsgCode("0014");
				result.setMsgDesc("Server Error!");
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setState(false);
			result.setMsgCode("0014");
			result.setMsgDesc(e.getMessage());
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				result.setState(false);
				result.setMsgCode("0014");
				result.setMsgDesc(e.getMessage());
			}
		}
		return result;
	}
}
