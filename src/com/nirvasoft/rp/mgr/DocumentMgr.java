package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.dao.DocumentDao;
import com.nirvasoft.rp.shared.DocumentData;
import com.nirvasoft.rp.shared.DocumentRequest;
import com.nirvasoft.rp.shared.DocumentResponse;

public class DocumentMgr {

	public DocumentResponse getAllPdfByRegion(DocumentRequest req) {
		DocumentResponse res = new DocumentResponse();
		DAOManager dao = new DAOManager();
		Connection conn = null;
		DocumentDao daoDoc = new DocumentDao();
		List<DocumentData> pdfList = null;

		try {
			conn = dao.openConnection();
			pdfList = daoDoc.getAllPdfByRegion(req, conn);
		} catch (Exception e) {
			pdfList = null;

			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				pdfList = null;

				e.printStackTrace();
			}
		}

		if (pdfList != null && pdfList.size() != 0) {
			DocumentData pdfListArr[] = new DocumentData[pdfList.size()];

			for (int i = 0; i < pdfList.size(); i++) {
				DocumentData doc = new DocumentData();

				doc.setAutoKey(pdfList.get(i).getAutoKey());
				doc.setCreatedDate(pdfList.get(i).getCreatedDate());
				doc.setModifiedDate(pdfList.get(i).getModifiedDate());
				doc.setName(pdfList.get(i).getName());
				doc.setUserId(pdfList.get(i).getUserId());
				doc.setStatus(pdfList.get(i).getStatus());
				doc.setRecordStatus(pdfList.get(i).getRecordStatus());
				doc.setFileType(pdfList.get(i).getFileType());
				doc.setRegion(req.getRegion());
				doc.setLink(pdfList.get(i).getLink());
				doc.setPostedBy(pdfList.get(i).getPostedBy());
				doc.setModifiedBy(pdfList.get(i).getModifiedBy());
				doc.setFileSize(pdfList.get(i).getFileSize());
				doc.setPages(pdfList.get(i).getPages());
				doc.setTitle(pdfList.get(i).getTitle());
				doc.setKeyword(pdfList.get(i).getKeyword());
				doc.setT2(pdfList.get(i).getT2());
				doc.setT3(pdfList.get(i).getT3());
				doc.setT4(pdfList.get(i).getT4());
				doc.setT5(pdfList.get(i).getT5());
				doc.setN1(pdfList.get(i).getN1());
				doc.setN2(pdfList.get(i).getN2());
				doc.setN3(pdfList.get(i).getN3());
				doc.setN4(pdfList.get(i).getN4());
				doc.setN5(pdfList.get(i).getN5());

				pdfListArr[i] = doc;
			}

			res.setCode("0000");
			res.setDesc("Getting All Pdf List Success.");
			res.setPdfList(pdfListArr);
		} else {
			res.setCode("0014");
			res.setDesc("No Pdf Found!");
		}

		return res;
	}

	public DocumentResponse getKeyword(DocumentRequest req) {
		DocumentResponse res = new DocumentResponse();
		DAOManager dao = new DAOManager();
		Connection conn = null;
		DocumentDao daoDoc = new DocumentDao();
		List<DocumentData> pdfList = null;

		try {
			conn = dao.openConnection();
			pdfList = daoDoc.getKeyword(req, conn);
		} catch (Exception e) {
			pdfList = null;

			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				pdfList = null;

				e.printStackTrace();
			}
		}

		if (pdfList != null && pdfList.size() != 0) {
			DocumentData pdfListArr[] = new DocumentData[pdfList.size()];

			for (int i = 0; i < pdfList.size(); i++) {
				DocumentData doc = new DocumentData();

				doc.setAutoKey(pdfList.get(i).getAutoKey());
				doc.setCreatedDate(pdfList.get(i).getCreatedDate());
				doc.setModifiedDate(pdfList.get(i).getModifiedDate());
				doc.setName(pdfList.get(i).getName());
				doc.setUserId(pdfList.get(i).getUserId());
				doc.setStatus(pdfList.get(i).getStatus());
				doc.setRecordStatus(pdfList.get(i).getRecordStatus());
				doc.setFileType(pdfList.get(i).getFileType());
				doc.setRegion(req.getRegion());
				doc.setLink(pdfList.get(i).getLink());
				doc.setPostedBy(pdfList.get(i).getPostedBy());
				doc.setModifiedBy(pdfList.get(i).getModifiedBy());
				doc.setFileSize(pdfList.get(i).getFileSize());
				doc.setPages(pdfList.get(i).getPages());
				doc.setTitle(pdfList.get(i).getTitle());
				doc.setKeyword(pdfList.get(i).getKeyword());
				doc.setT2(pdfList.get(i).getT2());
				doc.setT3(pdfList.get(i).getT3());
				doc.setT4(pdfList.get(i).getT4());
				doc.setT5(pdfList.get(i).getT5());
				doc.setN1(pdfList.get(i).getN1());
				doc.setN2(pdfList.get(i).getN2());
				doc.setN3(pdfList.get(i).getN3());
				doc.setN4(pdfList.get(i).getN4());
				doc.setN5(pdfList.get(i).getN5());

				pdfListArr[i] = doc;
			}

			res.setCode("0000");
			res.setDesc("Getting All Pdf List Success.");
			res.setPdfList(pdfListArr);
		} else {
			res.setCode("0014");
			res.setDesc("No Pdf Found!");
		}

		return res;
	}

	public DocumentResponse getPdfByTypeAndRegion(DocumentRequest req) {
		DocumentResponse res = new DocumentResponse();
		DAOManager dao = new DAOManager();
		Connection conn = null;
		DocumentDao daoDoc = new DocumentDao();
		List<DocumentData> pdfList = null;

		try {
			conn = dao.openConnection();
			pdfList = daoDoc.getPdfByTypeAndRegion(req, conn);
		} catch (Exception e) {
			pdfList = null;

			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				pdfList = null;

				e.printStackTrace();
			}
		}

		if (pdfList != null && pdfList.size() != 0) {
			DocumentData pdfListArr[] = new DocumentData[pdfList.size()];

			for (int i = 0; i < pdfList.size(); i++) {
				DocumentData doc = new DocumentData();

				doc.setAutoKey(pdfList.get(i).getAutoKey());
				doc.setCreatedDate(pdfList.get(i).getCreatedDate());
				doc.setModifiedDate(pdfList.get(i).getModifiedDate());
				doc.setName(pdfList.get(i).getName());
				doc.setUserId(pdfList.get(i).getUserId());
				doc.setStatus(pdfList.get(i).getStatus());
				doc.setRecordStatus(pdfList.get(i).getRecordStatus());
				doc.setFileType(pdfList.get(i).getFileType());
				doc.setRegion(req.getRegion());
				doc.setLink(pdfList.get(i).getLink());
				doc.setPostedBy(pdfList.get(i).getPostedBy());
				doc.setModifiedBy(pdfList.get(i).getModifiedBy());
				doc.setFileSize(pdfList.get(i).getFileSize());
				doc.setPages(pdfList.get(i).getPages());
				doc.setTitle(pdfList.get(i).getTitle());
				doc.setKeyword(pdfList.get(i).getKeyword());
				doc.setT2(pdfList.get(i).getT2());
				doc.setT3(pdfList.get(i).getT3());
				doc.setT4(pdfList.get(i).getT4());
				doc.setT5(pdfList.get(i).getT5());
				doc.setN1(pdfList.get(i).getN1());
				doc.setN2(pdfList.get(i).getN2());
				doc.setN3(pdfList.get(i).getN3());
				doc.setN4(pdfList.get(i).getN4());
				doc.setN5(pdfList.get(i).getN5());

				pdfListArr[i] = doc;
			}

			res.setCode("0000");
			res.setDesc("Getting Pdf List Success.");
			res.setPdfList(pdfListArr);
		} else {
			res.setCode("0014");
			res.setDesc("No Pdf Found!");
		}

		return res;
	}

}
