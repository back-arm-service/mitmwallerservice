package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.rp.dao.TokenDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.shared.tokenData;

public class TokenMgr {
	public boolean validToken(tokenData tdata){
		boolean status = false;
		Connection l_Conn = null;
		l_Conn =ConnAdmin.getConn();
		try {
			status =new TokenDao().validToken(tdata,l_Conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return status;
	}
}
