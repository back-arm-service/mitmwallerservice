package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.rp.dao.MCommRateMappingDao;
import com.nirvasoft.rp.data.CommRateHeaderData;
import com.nirvasoft.rp.data.CommRes;
import com.nirvasoft.rp.data.MCommRateMappingData;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.util.GeneralUtility;

public class MCommRateMappingMgr {

	public MCommRateMappingData getMCommMappingToCalculateChargewByMerId(String merId, String operator) {
		MCommRateMappingData ret = new MCommRateMappingData();
		MCommRateMappingDao mDao = new MCommRateMappingDao();
		CommRateMgr comMgr = new CommRateMgr();
		Connection conn = null;
		try {
			// Get Commrefence and Merchant mapping
			conn = ConnAdmin.getConn("001", "");
			ret = mDao.getMCommMappingdataByMerId(merId, operator, conn);
			// Get commision information detais from CommRateheader and
			// CommRatedetail tables
			if (!ret.getCommRef1().equals("-") && !ret.getCommRef1().equals("")) {
				CommRateHeaderData comm1 = comMgr.getComRatedataByComRefNo(ret.getCommRef1());
				ret.setCommObj1(comm1);
			}
			if (!ret.getCommRef2().equals("-") && !ret.getCommRef2().equals("")) {
				CommRateHeaderData comm2 = comMgr.getComRatedataByComRefNo(ret.getCommRef2());
				ret.setCommObj2(comm2);
			}
			if (!ret.getCommRef3().equals("-") && !ret.getCommRef3().equals("")) {
				CommRateHeaderData comm3 = comMgr.getComRatedataByComRefNo(ret.getCommRef3());
				ret.setCommObj3(comm3);
			}
			if (!ret.getCommRef4().equals("-") && !ret.getCommRef4().equals("")) {
				CommRateHeaderData comm4 = comMgr.getComRatedataByComRefNo(ret.getCommRef4());
				ret.setCommObj4(comm4);
			}
			if (!ret.getCommRef5().equals("-") && !ret.getCommRef5().equals("")) {
				CommRateHeaderData comm5 = comMgr.getComRatedataByComRefNo(ret.getCommRef5());
				ret.setCommObj5(comm5);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("e.getMessage() " + e.getMessage());
			ret.setCode("0014");
			ret.setDesc(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed()) {
						conn.close();
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					ret.setCode("0014");
					ret.setDesc(e.getMessage());
				}
			}

		}
		return ret;
	}
	public MCommRateMappingData getMCommMappingdataByMerId(String merId, String operator) {
		MCommRateMappingData ret = new MCommRateMappingData();
		MCommRateMappingDao mDao = new MCommRateMappingDao();
		CommRateMgr comMgr = new CommRateMgr();
		Connection conn = null;
		try {
			// Get Commrefence and Merchant mapping
			conn = ConnAdmin.getConn("001", "");
			ret = mDao.getMCommMappingdataByMerId(merId, operator, conn);
			// Get commision information detais from CommRateheader and
			// CommRatedetail tables
			if (!ret.getCommRef1().equals("-") && !ret.getCommRef1().equals("")) {
				CommRateHeaderData comm1 = comMgr.getComRatedataByComRefNo(ret.getCommRef1());
				ret.setCommObj1(comm1);
			}
			if (!ret.getCommRef2().equals("-") && !ret.getCommRef2().equals("")) {
				CommRateHeaderData comm2 = comMgr.getComRatedataByComRefNo(ret.getCommRef2());
				ret.setCommObj2(comm2);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("e.getMessage() " + e.getMessage());
			ret.setCode("0014");
			ret.setDesc(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed()) {
						conn.close();
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					ret.setCode("0014");
					ret.setDesc(e.getMessage());
				}
			}

		}
		return ret;
	}
	public CommRes getAgentComm(String merId, String operator,double amount) {
		CommRes ret = new CommRes();
		Connection conn = null;
		MCommRateMappingDao mDao = new MCommRateMappingDao();
		MCommRateMappingData AcommMapData = new MCommRateMappingData();
		try {
			conn = ConnAdmin.getConn("001", "");
			AcommMapData =getMCommMappingdataByMerId(merId, operator);
			ret.setCommessionAmt1(
					GeneralUtility.calculateIndividualCharges(AcommMapData.getCommObj1(), 3, amount, 0));
			ret.setCommessionAmt2(
					GeneralUtility.calculateIndividualCharges(AcommMapData.getCommObj2(), 3, amount, 0));
			ret.setCode("0000");
			ret.setDesc("Success");
		} catch (Exception e) {
			e.printStackTrace();
			ret.setCode("0014");
			ret.setDesc("Server Error!");
		}
		return ret;
	}
}
