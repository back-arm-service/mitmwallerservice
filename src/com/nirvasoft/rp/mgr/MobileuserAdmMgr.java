package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.cms.framework.Result;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.rp.dao.MobileuserAdmDao;
import com.nirvasoft.rp.data.MobileuserListingDataList;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.shared.FilterDataset;

public class MobileuserAdmMgr {

	public Result approvedbyuser(long syskey, String status) {
		Result res = new Result();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			conn.setAutoCommit(false);
			res = new MobileuserAdmDao().approvedbyuser(syskey, status, conn);
			if (res.isState()) {
				res.setKeyResult(syskey);
				res.getLongResult().add(syskey);
				conn.commit();
			} else
				conn.rollback();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			} finally {
				ServerUtil.closeConnection(conn);
			}
		}
		return res;
	}

	public MobileuserListingDataList getMobileuser(FilterDataset p) {
		MobileuserListingDataList res = new MobileuserListingDataList();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = new MobileuserAdmDao().getMobileuser(p, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

}
