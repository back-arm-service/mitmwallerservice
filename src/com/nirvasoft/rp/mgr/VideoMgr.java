package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nirvasoft.rp.dao.VideoDao;
import com.nirvasoft.rp.data.VideoListingDataList;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.shared.FilterDataset;
import com.nirvasoft.cms.dao.ArticleDao;
import com.nirvasoft.cms.dao.ArticleJunDao;
import com.nirvasoft.cms.dao.OPTDao;
import com.nirvasoft.cms.dao.UploadDao;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.shared.ArticleDataSet;
import com.nirvasoft.cms.shared.ArticleJunData;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.cms.shared.PhotoUploadData;
import com.nirvasoft.cms.shared.UploadData;
import com.nirvasoft.cms.shared.VideoDataSet;
import com.nirvasoft.cms.util.FontChangeUtil;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.database.SysKeyMgr;
import com.nirvasoft.rp.dao.VideoDao;

public class VideoMgr {
	/* searchQuesList */
	public ArticleDataSet searchVideoList(PagerData p, long status, String statetype, MrBean user) {
		ArticleDataSet res = new ArticleDataSet();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = new VideoDao().searchVideoList(p, status, statetype, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public ArticleData readDataBySyskey(long key, MrBean user) {
		ArticleData res = new ArticleData();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = new VideoDao().read(key, conn);
			res.setCropdata(new ArticleJunDao().search(String.valueOf(key), conn).getCrop());
			if (res.getN10() == 1 || res.getN10() == 2) {
				res.setUpload(UploadDao.search(String.valueOf(res.getSyskey()), "Video", conn).getUploads());
				res.setUploadlist(UploadDao.searchUploadList(String.valueOf(res.getSyskey()), conn));
			} else {
				res.setVideoUpload(UploadDao.searchVideoList(String.valueOf(res.getSyskey()), conn).getVideoUpload());
				res.setUpload(UploadDao.search(String.valueOf(res.getSyskey()), "Video", conn).getUploads());
				res.setUploadlist(UploadDao.searchUploadList(String.valueOf(res.getSyskey()), conn));
				res.setResizelist(UploadDao.search(String.valueOf(res.getSyskey()), "", conn).getUploads());
				if (res.getT3().equalsIgnoreCase("Video")) {
					res.setResizelist(UploadDao.search(String.valueOf(res.getSyskey()), "Video", conn).getUploads());
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static ArticleData setStatusDatas(ArticleData data, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(data.getCreatedTime());
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}

		if (ServerUtil.isZawgyiEncoded(data.getT1())) {
			data.setT1(FontChangeUtil.zg2uni(data.getT1()));

		}

		if (ServerUtil.isZawgyiEncoded(data.getT2())) {
			data.setT2(FontChangeUtil.zg2uni(data.getT2()));

		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);

		data.setVideoUpload(data.getVideoUpload());
		return data;
	}

	public Resultb2b saveVideo1(ArticleData data, String filePath, MrBean user) {
		Resultb2b res = new Resultb2b();
		Resultb2b res1 = new Resultb2b();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			conn.setAutoCommit(false);
			data = setStatusDatas(data, user);
			if (data.getSyskey() == 0) {
				long syskey = SysKeyMgr.getSysKey(1, "syskey", conn);
				data.setSyskey(syskey);
				res = ArticleDao.insert(data, conn);
				if (res.isState()) {
					if (data.getN10() == 1 || data.getN10() == 2) {
						for (int i = 0; i < data.getUploadlist().size(); i++) {
							boolean flag = false;
							UploadData obj = setUploadDataImage(data.getUploadlist().get(i), syskey, user);
							obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey", conn));
							obj.setModifiedTime(data.getModifiedTime());
							obj.setCreatedTime(data.getCreatedTime());
							obj.setT5(data.getT5());// region--hmh
							flag = UploadDao.insert(obj, conn);
							res.setState(flag);
							res.setMsgDesc("Inserted Successfully!");
							if (flag) {

							} else {
								break;
							}
						}
					} else if (data.getN10() == 0) {
						for (int i = 0; i < data.getVideoUpload().length; i++) {
							boolean flag = false;
							UploadData obj = new UploadData();
							obj = setUploadVideoData(data.getVideoUpload()[i], data.getVideoUpload()[i], "image",
									syskey, user);
							if (data.getUploadlist().size() > 0) {
								obj.setT1(data.getUploadlist().get(i).getName());
								obj.setT7(data.getUploadlist().get(i).getName());
							}
							obj.setModifiedTime(data.getModifiedTime());
							obj.setCreatedTime(data.getCreatedTime());
							obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey", conn));
							obj.setT5(data.getT5());// region--hmh
							flag = UploadDao.insert(obj, conn);
							res.setState(flag);
							res.setMsgDesc("Inserted Successfully!");
						}
					}

				}

			} else {
				res = ArticleDao.update(data, conn);
				if (res.isState()) {
					/*
					 * res = ArticleJunDao.deleteByUpdate(data.getSyskey(),
					 * conn); if(res.isState()){ res =
					 * UploadDao.deleteByUpdateforVideo(data.getSyskey(), conn);
					 * }
					 */
					res1 = UploadDao.deleteByUpdateforVideo(data.getSyskey(), conn);
					// if(res.isState()){
					if (data.getN10() == 1 || data.getN10() == 2) {
						for (int i = 0; i < data.getUploadlist().size(); i++) {
							boolean flag = false;
							UploadData obj = setUploadDataImage(data.getUploadlist().get(i), data.getSyskey(), user);
							obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey", conn));
							obj.setModifiedTime(data.getModifiedTime());
							obj.setCreatedTime(data.getCreatedTime());
							obj.setT6(data.getT13());// resizefilename
							obj.setT5(data.getT5());// region--hmh
							flag = UploadDao.insert(obj, conn);
							res.setState(flag);
							res.setMsgDesc("Updated Successfully!");
							if (flag) {

							} else {
								break;
							}
						}
					} else {
						for (int i = 0; i < data.getVideoUpload().length; i++) {
							boolean flag = false;
							UploadData obj = new UploadData();
							obj = setUploadVideoData(data.getVideoUpload()[i], data.getResizelist()[i], "image",
									data.getSyskey(), user);
							if (data.getUploadlist().size() > 0) {
								obj.setT1(data.getUploadlist().get(i).getName());
								obj.setT7(data.getUploadlist().get(i).getUrl());
							}
							obj.setModifiedTime(data.getModifiedTime());
							obj.setCreatedTime(data.getCreatedTime());
							// obj.setT6(data.getT13());//resizefilename
							obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey", conn));
							obj.setT5(data.getT5());// region--hmh
							flag = UploadDao.insert(obj, conn);
							res.setState(flag);
							res.setMsgDesc("Updated Successfully!");
						}
					}

					// }
				}
			}
			if (res.isState()) {
				res.setKeyResult(data.getSyskey());
				res.getLongResult().add(data.getSyskey());
				// res.setMsgDesc("Updated Successfully!");
				conn.commit();
			} else
				conn.rollback();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return res;
	}

	/* for image */
	public static UploadData setUploadDataImage(PhotoUploadData pdata, long articleKey, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		UploadData data = new UploadData();
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(data.getCreatedTime());
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());

		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		data.setT1(pdata.getName());
		data.setT7(pdata.getUrl());
		data.setN2(articleKey);
		return data;
	}

	public static UploadData setUploadVideoData(String url, String filename, String imgurl, long articleKey,
			MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		UploadData data = new UploadData();
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(data.getCreatedTime());
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		// data.setT1(filename);
		data.setT2(url);
		data.setN2(articleKey);
		return data;
	}

	public ArticleDataSet viewVideo(String searchVal, String userid) {
		ArticleDataSet res = new ArticleDataSet();
		VideoDao vDao = new VideoDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			//long userKey = vDao.searchByID(userid, conn);
			res = vDao.viewArticle(searchVal, conn);
			ArticleData[] dataArr = res.getData();
			for (ArticleData data : dataArr) {
				data.setVideoUpload(
						UploadDao.searchAdminVideo(String.valueOf(data.getSyskey()), conn).getVideoUpload());
				data.setUpload(UploadDao.search(String.valueOf(data.getSyskey()), "video", conn).getUploads());
				data.setUploadlist(UploadDao.searchUploadList(String.valueOf(data.getSyskey()), conn));
				data.setN6(vDao.searchLikeUser(data.getSyskey(), conn, userid));
				data.setN7(data.getSyskey());
			}
			res.setData(dataArr);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public VideoDataSet searchLike(String type, long key, MrBean user) {
		VideoDataSet res = new VideoDataSet();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = new VideoDao().searchLikeVideo(type, key, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	public Resultb2b clickLikeVideo(long id, String userSK, String type, MrBean user) {
		Resultb2b res = new Resultb2b();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = new VideoDao().updateLikeCountVideo(id, userSK, type, conn, user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;

	}	
	public VideoListingDataList searchVideolist(FilterDataset p) {
		VideoListingDataList res = new VideoListingDataList();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = new VideoDao().searchVideolist(p, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
/////atn////////
	public ArticleData readBySyskeyNew(long key) {
		ArticleData res = new ArticleData();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = new VideoDao().read(key, conn);
			//res.setCropdata(new ArticleJunDao().search(String.valueOf(key), conn).getCrop());
			if (res.getN10() == 1 || res.getN10() == 2) {
				res.setUpload(UploadDao.search(String.valueOf(res.getSyskey()), "Video", conn).getUploads());
				res.setUploadlist(UploadDao.searchUploadList(String.valueOf(res.getSyskey()), conn));
			} else {
				res.setVideoUpload(UploadDao.searchVideoList(String.valueOf(res.getSyskey()), conn).getVideoUpload());
				res.setUpload(UploadDao.search(String.valueOf(res.getSyskey()), "Video", conn).getUploads());
				res.setUploadlist(UploadDao.searchUploadList(String.valueOf(res.getSyskey()), conn));
				res.setResizelist(UploadDao.search(String.valueOf(res.getSyskey()), "", conn).getUploads());
				if (res.getT3().equalsIgnoreCase("Video")) {
					res.setResizelist(UploadDao.search(String.valueOf(res.getSyskey()), "Video", conn).getUploads());
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
	
}
