package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.rp.dao.RegionDAO;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.shared.ComboDataset;

public class RegionMgr {

	public static ComboDataset getStateListByRegion(String regionCode) {
		ComboDataset res = new ComboDataset();
		Connection l_Conn = null;
		try {
			l_Conn = ConnAdmin.getConn("001", "");
			res = RegionDAO.getStateListByRegion(regionCode, l_Conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return res;
	}

}
