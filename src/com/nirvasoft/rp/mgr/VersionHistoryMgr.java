package com.nirvasoft.rp.mgr;

import java.sql.Connection;

import com.nirvasoft.rp.dao.LOVDAO;
import com.nirvasoft.rp.dao.VersionHistoryDao;
import com.nirvasoft.rp.data.VersionHistoryData;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Lov3;
import com.nirvasoft.rp.shared.VersionHistoryDataSet;
import com.nirvasoft.rp.util.ServerUtil;

public class VersionHistoryMgr {

	public VersionHistoryData delete(long autoKey) {
		VersionHistoryData response = null;

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			response = new VersionHistoryDao().delete(autoKey, conn);
		} catch (Exception e) {
			response.setMsgCode("0014");
			response.setMsgDesc("Server Error!");
			response.setState(false);
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

	public VersionHistoryDataSet getAll(String searchText, int pageSize, int currentPage) {
		VersionHistoryDataSet response = null;

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			response = new VersionHistoryDao().getAll(searchText, pageSize, currentPage, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

	public Lov3 getAppCodes() {
		Lov3 response = null;

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			response = new LOVDAO().getAppCodesForVersionHistory(conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

	public VersionHistoryData getOneByAutoKey(long autoKey) {
		VersionHistoryData response = null;

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			response = new VersionHistoryDao().getOneByAutoKey(autoKey, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

	public Lov3 getStatusCodes() {
		Lov3 response = null;

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			response = new LOVDAO().getStatusCodesForVersionHistory(conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

	public VersionHistoryData save(VersionHistoryData request) {
		VersionHistoryData response = new VersionHistoryData();

		Connection conn = null;
		VersionHistoryDao vhDao = new VersionHistoryDao();

		try {
			conn = ConnAdmin.getConn("001", "");

			if (request.getAutokey().equals("")) {
				if (vhDao.isExistedByAppCodeAndVersion(request.getAppcode(), request.getVersion(), conn)) {
					response.setMsgCode("0014");
					response.setMsgDesc(
							"This Version History Already Existed. (This App Code And This Version PAIR Already Existed.)");
					response.setState(false);
				} else {
					String lastVersionKey = vhDao.getLastVersionKeyByAppCode(request.getAppcode(), conn);
					long versionKey = Long.parseLong(lastVersionKey) + 1;
					request.setVersionkey(versionKey + "");

					response = vhDao.save(request, conn);

					if (response.getMsgCode() == "0000") {
						response.setAutokey(vhDao.getAutoKeyByAppCodeAndVersionKey(request.getAppcode(),
								request.getVersionkey(), conn));
						response.setVersionkey(request.getVersionkey());
					}
				}
			} else {
				response = vhDao.update(request, conn);
				response.setAutokey(request.getAutokey());
				response.setVersionkey(request.getVersionkey());
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

}