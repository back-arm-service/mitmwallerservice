package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.shared.DivisionComboDataSet;
import com.nirvasoft.database.SysKeyMgr;
import com.nirvasoft.rp.dao.UserDao;
import com.nirvasoft.rp.data.CityStateComboDataSet;
import com.nirvasoft.rp.data.CityStateData;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.users.data.PersonData;
import com.nirvasoft.rp.users.data.UserData;
import com.nirvasoft.rp.users.data.UserViewData;
import com.nirvasoft.rp.users.data.UserViewDataArr;
import com.nirvasoft.rp.users.data.UserViewDataset;
import com.nirvasoft.rp.util.ServerUtil;

public class UserDataMgr {

	public static String checklength(String phNo) {
		String retphoneNo = "";
		if (phNo.length() == 9) {
			if (phNo.substring(1, 2).equalsIgnoreCase("4") || phNo.substring(1, 2).equalsIgnoreCase("3")) {
				retphoneNo = phNo.substring(1, phNo.length());
			} else {
				retphoneNo = phNo;
			}
		}
		if (phNo.length() == 10) {
			retphoneNo = phNo.substring(1, phNo.length());
		}
		return retphoneNo;

	}

	public static Result checkPhoneNolength(String phNo) {
		Result res = new Result();
		String countrycode = "959";
		String retphoneNo = "";

		if (phNo.length() >= 7 && phNo.length() <= 9) {
			retphoneNo = countrycode + phNo;
			res.setPhNo(retphoneNo);
			res.setState(true);
			res.setMsgCode("0000");
			res.setMsgDesc("Phone No. format is correct!");

		} else {
			res.setState(false);

		}

		return res;
	}
	// public Result saveUserProfile(UserData data) {
	// Result res = new Result();
	// Connection conn = null;
	// try {
	// conn = ConnAdmin.getConn("001", "");
	// if (conn.equals("null") || conn.equals("")) {
	// res.setState(false);
	// res.setMsgDesc("Connection Fail");
	// } else {
	// data.getPerson().setSyskey(UserDao.getPersonSyskey(data.getSyskey(),
	// conn));
	// if (data.getPerson().getSyskey() == 0) {
	// data.getPerson()
	// .setSyskey(
	// SysKeyMgr.getSysKey(1, "syskey",
	// ConnAdmin.getConn("001", "")));
	// data = initData(data,conn);
	// ServerUtil.closeConnection(conn);
	// conn = ConnAdmin.getConn("001", "");
	// res = PersonDao.insert(getPersonData(data), conn); // UVM012_A
	// } else {
	// data = initData(data,conn);
	// ServerUtil.closeConnection(conn);
	// conn = ConnAdmin.getConn("001", "");
	// res = PersonDao.updatePersonData(data, conn);
	//// res = PersonDao.update(getPersonData(data), conn);
	// }
	//
	// if (res.isState()) {
	// ServerUtil.closeConnection(conn);
	// conn = ConnAdmin.getConn("001", "");
	// if (data.getSyskey() == 0) {
	// data.setSyskey(SysKeyMgr.getSysKey(1, "syskey",
	// ConnAdmin.getConn("001", "")));
	//
	// data.setN4(data.getPerson().getSyskey());
	// data.setT2(ServerUtil.encryptPIN(UserDao.generatePassword()));
	// data.setN2(1);
	// res = UserDao.insert(true, data, conn); // UVM005_A
	// } else {
	// data.setN2(1);
	// data.setN4(data.getPerson().getSyskey());
	// res = UserDao.update(true, data, conn);
	// }
	//
	// }
	//
	// if (res.isState()) {
	//
	// res.setKeyResult(data.getSyskey());
	// res.getLongResult().add(data.getSyskey());
	// res.getLongResult().add(data.getPerson().getSyskey());
	//
	// }
	// }
	//
	// } catch (SQLException e) {
	// res.setState(false);
	// res.setMsgDesc("Connection Fail");
	// e.printStackTrace();
	// } finally {
	// ServerUtil.closeConnection(conn);
	// }
	// return res;
	// }

	public static Result deleteUserData(long syskey, String modifieduserId) {
		Result res = new Result();
		Connection conn = ConnAdmin.getConn("001", "");
		try {
			res = UserDao.delete(syskey, modifieduserId, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	// get Junction userlist
	public static UserViewDataArr getAllUCJunctionData(String searchText, int pageSize, int currentPage) {
		UserViewDataArr res = new UserViewDataArr();
		UserViewDataset dataSet = new UserViewDataset();
		Connection conn = ConnAdmin.getConn("001", "");
		try {
			dataSet = UserDao.getAllUCJunctionData(searchText, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		int startPage = (currentPage - 1) * pageSize;
		int endPage = pageSize + startPage;
		res.setTotalCount(dataSet.getArlData().size());
		dataSet.setArlData(new ArrayList<UserViewData>(dataSet.getArlData().subList(startPage,
				(endPage > dataSet.getArlData().size()) ? dataSet.getArlData().size() : endPage)));

		UserViewData[] dataarry = new UserViewData[dataSet.getArlData().size()];

		if (dataarry.length == 1) {
			dataarry = new UserViewData[dataSet.getArlData().size() + 1];
			dataarry[0] = dataSet.getArlData().get(0);
			dataarry[1] = new UserViewData();
		}

		for (int i = 0; i < dataSet.getArlData().size(); i++) {
			dataarry[i] = dataSet.getArlData().get(i);

		}

		res.setdata(dataarry);
		res.setSearchText(searchText);
		res.setCurrentPage(currentPage);
		res.setPageSize(pageSize);

		return res;
	}

	public static UserViewDataArr getAllUserCIFDataList(String searchText, int pageSize, int currentPage) {
		UserViewDataArr res = new UserViewDataArr();
		UserViewDataset dataSet = new UserViewDataset();
		Connection conn = ConnAdmin.getConn("001", "");
		try {
			dataSet = UserDao.getAllUserCIFDataList(searchText, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		int startPage = (currentPage - 1) * pageSize;
		int endPage = pageSize + startPage;
		res.setTotalCount(dataSet.getArlData().size());
		dataSet.setArlData(new ArrayList<UserViewData>(dataSet.getArlData().subList(startPage,
				(endPage > dataSet.getArlData().size()) ? dataSet.getArlData().size() : endPage)));
		UserViewData[] dataarry = new UserViewData[dataSet.getArlData().size()];

		if (dataarry.length == 1) {
			dataarry = new UserViewData[dataSet.getArlData().size() + 1];
			dataarry[0] = dataSet.getArlData().get(0);
			dataarry[1] = new UserViewData();
		}

		for (int i = 0; i < dataSet.getArlData().size(); i++) {
			dataarry[i] = dataSet.getArlData().get(i);

		}

		res.setdata(dataarry);
		res.setSearchText(searchText);
		res.setCurrentPage(currentPage);
		res.setPageSize(pageSize);

		return res;
	}

	public static PersonData getPersonData(UserData bdata) {

		PersonData adata = new PersonData();
		adata.setSyskey(bdata.getPerson().getSyskey());
		adata.setT1(bdata.getT1());
		adata.setT2(bdata.getName());
		adata.setT3(bdata.getState());
		adata.setT4(bdata.getCity());
		adata.setCreatedDate(bdata.getCreatedDate());
		adata.setModifiedDate(bdata.getModifiedDate());
		adata.setRecordStatus(bdata.getRecordStatus());
		adata.setSyncStatus(bdata.getSyncStatus());
		adata.setSyncBatch(bdata.getSyncBatch());
		adata.setUserId(bdata.getUserId());
		adata.setUserName(bdata.getUserName());
		return adata;
	}

	public static String getUserName(String userid, Connection con) throws SQLException {
		String st = "";
		UserDao u_dao = new UserDao();

		try {
			st = u_dao.getUserName(userid, con);

		} finally {
			ServerUtil.closeConnection(con);
		}
		return st;
	}

	public static UserData initData(UserData data, Connection con) {
		String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
		try {
			data.setUserName(getUserName(data.getUserId(), con));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (data.getSyskey() == 0) {
			data.setCreatedDate(date);
		}
		if (data.getCreatedDate().equals("")) {
			data.setCreatedDate(date);
		}

		data.setT2(ServerUtil.encryptPIN(data.getT2()));
		data.setModifiedDate(date);
		if (data.getRecordStatus() == 0) {
			data.setRecordStatus(1);
		} else {
			data.setRecordStatus(data.getRecordStatus());
		}

		data.setSyncBatch(0);
		data.setSyncStatus(1);

		return data;
	}

	/*
	 * public static UserData readBranchUserDataBySyskey(long pKey) { UserData
	 * res = new UserData(); Connection conn = ConnAdmin.getConn("001", ""); try
	 * { res = UserDao.readBranchUserDataBySyskey(pKey, conn);
	 * 
	 * UserRoleViewData[] dataarray; dataarray =
	 * UserRoleViewDao.getUserRoleList(pKey, conn); long pvalue[] =
	 * UserRoleViewDao.getRoleResult(pKey, conn); String name =
	 * PersonDao.getUserName(pKey, conn); res.setName(name);
	 * res.setUserrolelist(dataarray); res.setRolesyskey(pvalue);
	 * 
	 * } catch (SQLException e) { e.printStackTrace(); }
	 * 
	 * return res; }
	 */

	// get create user data
	/*
	 * public static UserData readUserProfileDataBySyskey(long pKey) { UserData
	 * res = new UserData(); UserDao userDao = new UserDao(); PersonDao p_Dao =
	 * new PersonDao(); PersonData p_Data = new PersonData(); Connection conn =
	 * null;
	 * 
	 * try { conn = ConnAdmin.getConn("001", ""); res =
	 * userDao.readUserProfileDataBySyskey(pKey, conn);
	 * 
	 * UserRoleViewData[] dataarray; dataarray =
	 * UserRoleViewDao.getUserRoleList(pKey, conn); long pvalue[] =
	 * UserRoleViewDao.getRoleResult(pKey, conn); String name =
	 * PersonDao.getUserName(pKey, conn); res.setName(name); p_Data =
	 * p_Dao.getCreateUserData(pKey, conn); res.setState(p_Data.getT3());
	 * res.setCity(p_Data.getT4()); res.setUserrolelist(dataarray);
	 * res.setRolesyskey(pvalue);
	 * 
	 * } catch (SQLException e) { e.printStackTrace(); }
	 * 
	 * return res; }
	 */

	/*
	 * public static Result saveBranchUserData(Userdata data) {
	 * 
	 * Result res = new Result(); Connection conn = null; try { conn =
	 * ConnAdmin.getConn("001", ""); if (conn.equals("null") || conn.equals(""))
	 * { res.setState(false); res.setMsgDesc("Connection Fail"); } else {
	 * data.getPerson().setSyskey(UserDao.getPersonSyskey(data.getSyskey(),
	 * conn));
	 * 
	 * if (data.getPerson().getSyskey() == 0) {
	 * data.getPerson().setSyskey(SysKeyMgr.getSysKey(1, "syskey",
	 * ConnAdmin.getConn("001", ""))); data = initData(data, conn);
	 * ServerUtil.closeConnection(conn); conn = ConnAdmin.getConn("001", "");
	 * res = PersonDao.insert(getPersonData(data), conn); } else { data =
	 * initData(data, conn); ServerUtil.closeConnection(conn); conn =
	 * ConnAdmin.getConn("001", ""); res = PersonDao.updatePersonData(data,
	 * conn); // res = PersonDao.update(getPersonData(data), conn); }
	 * 
	 * if (res.isState()) { ServerUtil.closeConnection(conn); conn =
	 * ConnAdmin.getConn("001", ""); if (data.getSyskey() == 0) {
	 * data.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn("001",
	 * "")));
	 * 
	 * data.setN4(data.getPerson().getSyskey()); data.setN2(2); res =
	 * UserDao.insert(false, data, conn); } else { data.setN2(2);
	 * data.setN4(data.getPerson().getSyskey()); res = UserDao.update(false,
	 * data, conn); }
	 * 
	 * }
	 * 
	 * if (res.isState()) {
	 * 
	 * res.setKeyResult(data.getSyskey());
	 * res.getLongResult().add(data.getSyskey());
	 * res.getLongResult().add(data.getPerson().getSyskey());
	 * 
	 * } } } catch (SQLException e) { res.setState(false); res.setMsgDesc(
	 * "Connection Fail"); e.printStackTrace(); } finally {
	 * ServerUtil.closeConnection(conn); } return res;
	 * 
	 * }
	 */

	public Result activatedeactivateUserData(long syskey, String status, String userId) {
		Result res = new Result();
		Connection conn = null;
		String msg = "";
		UserDao u_dao = new UserDao();
		try {
			conn = ConnAdmin.getConn("001", "");
			res = u_dao.activateDeactivateUser(syskey, conn, status, userId);

			if (res.isState()) {
				if (status.equalsIgnoreCase("Activate")) {
					msg = "Activate Successfully.";
				} else if (status.equalsIgnoreCase("Deactivate")) {
					msg = "Deactivate Successfully.";
				}
			}
			res.setMsgDesc(msg);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	// get User setup list
	public UserViewDataArr getAllUserData(String searchText, int pageSize, int currentPage, String operation) {

		UserViewDataArr res = new UserViewDataArr();
		UserViewDataset dataSet = new UserViewDataset();
		UserDao u_dao = new UserDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			dataSet = u_dao.getAllUserData(searchText, conn, operation);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		int startPage = (currentPage - 1) * pageSize;
		int endPage = pageSize + startPage;
		res.setTotalCount(dataSet.getArlData().size());
		dataSet.setArlData(new ArrayList<UserViewData>(dataSet.getArlData().subList(startPage,
				(endPage > dataSet.getArlData().size()) ? dataSet.getArlData().size() : endPage)));
		UserViewData[] dataarry = new UserViewData[dataSet.getArlData().size()];
		if (dataarry.length == 1) {
			dataarry = new UserViewData[dataSet.getArlData().size() + 1];
			dataarry[0] = dataSet.getArlData().get(0);
			dataarry[1] = new UserViewData();
		}

		for (int i = 0; i < dataSet.getArlData().size(); i++) {
			dataarry[i] = dataSet.getArlData().get(i);

		}
		res.setdata(dataarry);
		res.setSearchText(searchText);

		res.setCurrentPage(currentPage);
		res.setPageSize(pageSize);

		return res;
	}

	public CityStateComboDataSet getCitylist(String pstate) {
		Connection conn = null;
		UserDao u_dao = new UserDao();
		CityStateComboDataSet res = new CityStateComboDataSet();

		try {
			conn = ConnAdmin.getConn("001", "");
			res = u_dao.getCityCombolist(pstate, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public String getlastlogintimefail(String userid) {
		String lasttimelogin = "";
		UserDao u_dao = new UserDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			lasttimelogin = u_dao.getlastlogintimefail(userid, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return lasttimelogin;
	}

	public String getlasttimelogin(String userid) {
		String lasttimelogin = "";
		UserDao u_dao = new UserDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			lasttimelogin = u_dao.getlasttimelogin(userid, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return lasttimelogin;
	}

	public String getSessionID() {
		String sessionID = "";
		String prefix = "S";
		Long key = 0L;
		try {
			key = SysKeyMgr.getSysKey(1, "", ConnAdmin.getConn("001", ""));
			sessionID = prefix + key;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sessionID;
	}

	/*public ArrayList<CityStateData> getStateListByUserID(String userID, String type) {
		Connection conn = null;
		UserDao u_dao = new UserDao();
		ArrayList<CityStateData> res = new ArrayList<CityStateData>();

		try {
			conn = ConnAdmin.getConn("001", "");
			if (type.equals("1")) {
				res = u_dao.getStateListByUserIDWithAll(userID, conn);
			} else {
				res = u_dao.getStateListByUserID(userID, conn);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}*/

	// City Stae Combo //
	public CityStateComboDataSet getStateTypelist(MrBean user) {
		Connection conn = null;
		UserDao u_dao = new UserDao();
		CityStateComboDataSet res = new CityStateComboDataSet();

		try {
			conn = ConnAdmin.getConn(user.getUser().getOrganizationID(), "");
			res = u_dao.getStateTypeCombolist(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public ArrayList<CityStateData> getStateTypelistNew() {
		Connection conn = null;
		UserDao u_dao = new UserDao();
		ArrayList<CityStateData> res = new ArrayList<CityStateData>();

		try {
			conn = ConnAdmin.getConn("001", "");
			res = u_dao.getStateTypeCombolistNew(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public DivisionComboDataSet getStatusList(long status, MrBean user) {
		Connection conn = null;
		DivisionComboDataSet res = new DivisionComboDataSet();
		try {
			conn = ConnAdmin.getConn("001", "");
			res = new UserDao().getStatusList(status, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	// zmth ,atn>> close
	// Save User Profile
	/*
	 * public Result saveUserProfile(Userdata data) { Result res = new Result();
	 * Connection conn = null; try { conn = ConnAdmin.getConn("001", ""); if
	 * (conn.equals("null") || conn.equals("")) { res.setState(false);
	 * res.setMsgDesc("Connection Fail"); } else {
	 * 
	 * res = normalizePhoneNoformat(data.getT4()); if (res.isState() == false) {
	 * res.setMsgCode("0014"); res.setMsgDesc("Phone No. is incorrect!"); } else
	 * {
	 * 
	 * data.setT4(res.getPhNo()); // Phone No
	 * 
	 * data.getPerson().setSyskey(UserDao.getPersonSyskey(data.getSyskey(),
	 * conn)); if (data.getPerson().getSyskey() == 0) {
	 * data.getPerson().setSyskey(SysKeyMgr.getSysKey(1, "syskey",
	 * ConnAdmin.getConn("001", ""))); data.setT1(res.getPhNo()); data =
	 * initData(data, conn); ServerUtil.closeConnection(conn); conn =
	 * ConnAdmin.getConn("001", ""); res = PersonDao.insert(getPersonData(data),
	 * conn); // UVM012_A } else { data = initData(data, conn);
	 * ServerUtil.closeConnection(conn); conn = ConnAdmin.getConn("001", "");
	 * res = PersonDao.updatePersonData(data, conn); // res =
	 * PersonDao.update(getPersonData(data), conn); }
	 * 
	 * if (res.isState()) { ServerUtil.closeConnection(conn); conn =
	 * ConnAdmin.getConn("001", ""); if (data.getSyskey() == 0) {
	 * data.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn("001",
	 * "")));
	 * 
	 * data.setN4(data.getPerson().getSyskey());
	 * data.setT2(ServerUtil.encryptPIN(UserDao.generatePassword()));
	 * data.setN2(1);
	 * 
	 * res = UserDao.insert(true, data, conn); // UVM005_A } else {
	 * data.setN2(1); data.setN4(data.getPerson().getSyskey()); res =
	 * UserDao.update(true, data, conn); }
	 * 
	 * }
	 * 
	 * if (res.isState()) { res.setKeyResult(data.getSyskey());
	 * res.getLongResult().add(data.getSyskey());
	 * res.getLongResult().add(data.getPerson().getSyskey()); } } }
	 * 
	 * } catch (SQLException e) { res.setState(false); res.setMsgDesc(
	 * "Connection Fail"); e.printStackTrace(); } finally {
	 * ServerUtil.closeConnection(conn); } return res; }
	 */
	public UserData getUserbySyskey(long skskey) {
		UserData data = new UserData();
		UserDao u_dao = new UserDao();
		Connection conn = ConnAdmin.getConn("001", "");
		try {
			data = u_dao.getUserbySyskey(skskey, conn);
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return data;
	}

	public Result getUserId(String phNo) {

		Result res = new Result();
		UserDao user_dao = new UserDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			if (conn.equals("null") || conn.equals("")) {
				res.setState(false);
				res.setMsgDesc("Connection Fail");
			} else {
				res = user_dao.getUserId(phNo, conn);
			}
		} catch (Exception e) {
			e.printStackTrace();
			res.setState(false);
			res.setMsgDesc(e.getMessage());

		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public UserData getUserNameAndNrc(String userId) {
		UserData data = new UserData();
		UserDao u_dao = new UserDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			data = u_dao.getUserNameAndNrc(userId, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return data;
	}

	public Result lockUnlockUserData(String userid, long syskey, String status) {
		Result res = new Result();
		Connection conn = null;
		String msg = "";
		UserDao u_dao = new UserDao();
		try {
			conn = ConnAdmin.getConn("001", "");
			res = u_dao.lockUnlockUser(userid, syskey, conn, status);
			if (res.isState()) {
				if (status.equalsIgnoreCase("Lock")) {
					msg = "Lock Successfully.";
				} else if (status.equalsIgnoreCase("Unlock")) {
					msg = "Unlock Successfully.";
				}
			}

			res.setMsgDesc(msg);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	public Result markPrinted(String userId) {
		Result res = new Result();
		Connection conn = null;
		String msg = "";
		UserDao u_dao = new UserDao();
		try {
			conn = ConnAdmin.getConn("001", "");
			res = u_dao.markPrinted(userId, conn);
			if (res.isState()) {
				msg = "Print";
			} else {
				msg = "Printed already once";
			}
			res.setMsgDesc(msg);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	public Result normalizePhoneNoformat(String phoneNo) {
		String chkPhno = "";
		Result res = new Result();

		if (phoneNo.substring(0, 2).equalsIgnoreCase("09")) {
			chkPhno = phoneNo.substring(2, phoneNo.length());
			res = checkPhoneNolength(chkPhno);

		} else if (phoneNo.substring(0, 2).equalsIgnoreCase("95")) {

			if (phoneNo.substring(2, 3).equalsIgnoreCase("9")) {
				chkPhno = phoneNo.substring(3, phoneNo.length());
			} else if (phoneNo.substring(2, 4).equalsIgnoreCase("09")) {
				chkPhno = phoneNo.substring(4, phoneNo.length());
			} else {
				chkPhno = phoneNo.substring(2, phoneNo.length());
			}

			res = checkPhoneNolength(chkPhno);

		} else if (phoneNo.substring(0, 1).equalsIgnoreCase("9")) {
			chkPhno = checklength(phoneNo);
			res = checkPhoneNolength(chkPhno);
		} else {
			chkPhno = phoneNo;
			res = checkPhoneNolength(chkPhno);
		}

		return res;
	}

	public UserData readByUserID(String aUserID) {
		UserData res = new UserData();
		Connection conn = null;
		UserDao dao = new UserDao();
		try {
			conn = ConnAdmin.getConn("001", "");
			res = dao.readByUserID(aUserID, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	public Result resetPasswordById(String userId) throws SQLException {
		// TODO Auto-generated method stub
		Result res = new Result();
		UserDao u_dao = new UserDao();
		Connection conn = ConnAdmin.getConn("001", "");
		try {
			res = u_dao.resetPasswordById(userId, conn);
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}
	public UserViewDataArr showAccountList(String searchText, int pageSize, int currentPage, boolean master, String chk)
	  {
	    UserViewDataArr res = new UserViewDataArr();
	    UserDao u_dao = new UserDao();
	    Connection conn = null;
	    Connection l_ConniCBS = null;
	    try {
	      conn = ConnAdmin.getConn("001", "");
	      l_ConniCBS = ConnAdmin.getConn_iCBS("001", "");
	      if (chk.equals("1"))
	        res = u_dao.showAccountList(searchText, pageSize, currentPage, conn, master);
	      else res = u_dao.getGLList(searchText, pageSize, currentPage, l_ConniCBS, master); 
	    }
	    catch (SQLException e)
	    {
	      e.printStackTrace();
	    } finally {
	      ServerUtil.closeConnection(conn);
	    }

	    return res;
	  }

}
