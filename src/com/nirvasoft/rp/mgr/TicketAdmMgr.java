package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.cms.util.FontChangeUtil;
import com.nirvasoft.rp.dao.LOVDAO;
import com.nirvasoft.rp.dao.TicketAdmDao;
import com.nirvasoft.rp.data.TicketArr1;
import com.nirvasoft.rp.data.TicketData1;
import com.nirvasoft.rp.data.TicketListingDataList;
import com.nirvasoft.rp.data.TicketResponse;
import com.nirvasoft.rp.data.TicketResponseList;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Lov3;
import com.nirvasoft.rp.shared.FilterDataset;
import com.nirvasoft.rp.util.ServerUtil;

public class TicketAdmMgr {
	public TicketData1 deleteTicket(TicketResponse request) {
		TicketData1 response = null;

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			response = new TicketAdmDao().deleteTicket(request, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

	// acmy
	public TicketArr1 getAllTicket(TicketArr1 tdata, String ticketRegion) {
		TicketArr1 response = null;
		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			response = new TicketAdmDao().getAllTicket(tdata, ticketRegion, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

	// public TicketArr1 getTickets(TicketArr1 tdata, String tstatus, String
	// ticketRegion) {
	// TicketArr1 response = null;
	// Connection conn = null;
	//
	// try {
	// conn = ConnAdmin.getConn("001", "");
	// response = new TicketAdmDao().getTickets(tdata, tstatus, ticketRegion,
	// conn);
	// } catch (Exception e) {
	// e.printStackTrace();
	// } finally {
	// ServerUtil.closeConnection(conn);
	// }
	//
	// return response;
	// }

	public TicketResponseList getAllTicketList(String[] ticketList) {
		TicketResponseList response = null;

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			response = new TicketAdmDao().getAllTicketList(ticketList, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

	public TicketArr1 getAllTickets(TicketArr1 tdata, String ticketChannel) {
		TicketArr1 response = null;
		Connection conn = null;
		String searchText = "";
		try {
			conn = ConnAdmin.getConn("001", "");
			TicketData1[] ticketData = tdata.getTicketData();
			for (int i = 0; i < ticketData.length; i++) {
				searchText = ticketData[i].getT4();
			}
			response = new TicketAdmDao().getAllTickets(tdata, ticketChannel, searchText, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

	public TicketArr1 getAllUsertypes(TicketArr1 tdata, String ticketUsertype) {
		TicketArr1 response = null;
		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			response = new TicketAdmDao().getAllUsertypes(tdata, ticketUsertype, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

	public TicketData1 getOneTicket(String ticketNo) {
		TicketData1 response = null;

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			response = new TicketAdmDao().getOneTicket(ticketNo, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

	public TicketListingDataList getPOCTickets(FilterDataset aFilterDataset) {
		TicketListingDataList response = null;
		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			response = new TicketAdmDao().getPOCTickets(aFilterDataset, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

	public TicketListingDataList getTickets(FilterDataset aFilterDataset) {
		TicketListingDataList response = null;
		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			response = new TicketAdmDao().getTickets(aFilterDataset, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

	public Lov3 getTicketStatus() {
		Lov3 response = null;

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			response = new LOVDAO().getTicketStatus(conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

	public TicketResponse saveTicket(TicketResponse request) {
		TicketResponse response = null;

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");

			if (!ServerUtil.isUniEncoded(request.getSenderName())) {
				request.setSenderName(FontChangeUtil.zg2uni(request.getSenderName()));
			}

			if (!ServerUtil.isUniEncoded(request.getMessage())) {
				request.setMessage(FontChangeUtil.zg2uni(request.getMessage()));
			}

			if (!ServerUtil.isUniEncoded(request.getImage())) {
				request.setImage(FontChangeUtil.zg2uni(request.getImage()));
			}

			if (!ServerUtil.isUniEncoded(request.getT9())) {
				request.setT9(FontChangeUtil.zg2uni(request.getT9()));
			}

			if (!ServerUtil.isUniEncoded(request.getT5())) {
				request.setT5(FontChangeUtil.zg2uni(request.getT5()));
			}

			if (!ServerUtil.isUniEncoded(request.getT10())) {
				request.setT10(FontChangeUtil.zg2uni(request.getT10()));
			}

			response = new TicketAdmDao().saveTicket(request, conn);

			if (response.isState()) {
				String[] abc = new String[1];
				abc[0] = request.getTicketNo();

				// TicketResponse responseTwo = new
				// TicketDao().getOneTicket(abc, conn);

				// if (responseTwo.isState()) {
				// response.setAutoKey(responseTwo.getAutoKey());
				// }
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

	// atn
	public TicketData1 updateMyTicket(TicketData1 request) {
		TicketData1 response = null;
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			response = new TicketAdmDao().updatemyTicket(request, conn);
		} catch (SQLException e) {
			response = new TicketData1();
			response.setCode("0014");
			response.setDesc("Update failed.");
			response.setState(false);

			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return response;
	}

	public TicketResponse updateTicket(TicketResponse request) {
		TicketResponse response = null;

		Connection conn = null;

		try {
			conn = ConnAdmin.getConn("001", "");
			if (!ServerUtil.isUniEncoded(request.getSenderName())) {
				request.setSenderName(FontChangeUtil.zg2uni(request.getSenderName()));
			}

			if (!ServerUtil.isUniEncoded(request.getMessage())) {
				request.setMessage(FontChangeUtil.zg2uni(request.getMessage()));
			}

			if (!ServerUtil.isUniEncoded(request.getImage())) {
				request.setImage(FontChangeUtil.zg2uni(request.getImage()));
			}

			if (!ServerUtil.isUniEncoded(request.getT9())) {
				request.setT9(FontChangeUtil.zg2uni(request.getT9()));
			}

			if (!ServerUtil.isUniEncoded(request.getT5())) {
				request.setT5(FontChangeUtil.zg2uni(request.getT5()));
			}

			if (!ServerUtil.isUniEncoded(request.getT10())) {
				request.setT10(FontChangeUtil.zg2uni(request.getT10()));
			}

			response = new TicketAdmDao().updateTicket(request, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

}
