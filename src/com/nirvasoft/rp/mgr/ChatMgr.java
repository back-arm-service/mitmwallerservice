package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;

import com.nirvasoft.rp.dao.ChatDao;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.data.ChannelKeyRequest;
import com.nirvasoft.rp.data.ContactListReq;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.shared.Contact;
import com.nirvasoft.rp.shared.ContactArr;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.util.ServerUtil;
import com.sun.xml.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

public class ChatMgr {

	public ResponseData addContact(Contact req) {
		ResponseData res = new ResponseData();
		Connection conn = null;
		try {
			conn = new DAOManager().openConnection();
			res = new ChatDao().addContact(req, conn);
		} catch (Exception e) {
			res.setCode("0014");
			res.setDesc("Server Error!");
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public ContactArr getContact(String userID) {
		ContactArr res = new ContactArr();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = new ChatDao().getContact(userID, conn);
		} catch (Exception e) {
			e.printStackTrace();
			res.setCode("0014");
			res.setDesc("Server Error!");
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public ResponseData updateChannelKey(ChannelKeyRequest request) {
		ResponseData res = new ResponseData();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = new ChatDao().updateChannelKey(request.getChannelkey(), request.getSyskeyOne(), request.getPhoneTwo(),
					conn);

			if (res.getCode() == "0000") {
				res = new ChatDao().updateChannelKey(request.getChannelkey(), request.getSyskeyTwo(),
						request.getPhoneOne(), conn);
			}
		} catch (Exception e) {
			e.printStackTrace();
			res.setCode("0014");
			res.setDesc("Server Error!");
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;

	}

	public ContactArr addContactList(ContactListReq req) {
		ContactArr res = new ContactArr();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			for(int i=0;i<req.getData().length;i++){
				new ChatDao().addContact(req.getData()[i], conn);
			}
			res = new ChatDao().getContact(req.getUserID(), conn);
		} catch (Exception e) {
			e.printStackTrace();
			res.setCode("0014");
			res.setDesc("Server Error!");
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	public ContactArr getContactListByPhoneNo(ContactListReq req) {
		ContactArr res = new ContactArr();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			String phone="";
			for(int i=0;i<req.getData().length;i++){
				if(req.getData().length-1!=i){
					phone+="'"+req.getData()[i].getPhone()+"', ";
				}else{
					phone +="'"+req.getData()[i].getPhone()+"'";
				}
			}
			res=new ChatDao().getContactListByPhoneNo(req.getUserID(),phone, conn);
		} catch (Exception e) {
			e.printStackTrace();
			res.setCode("0014");
			res.setDesc("Server Error!");
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}
	
	public Contact checkPhoneNo(String  phoneNo) {
		Contact res = new Contact();
		Connection conn = null;
		ChatDao dao = new ChatDao();
		try {
			conn = ConnAdmin.getConn("001", "");
			res = dao.checkPhoneNo(phoneNo, conn);
			if(!res.getCode().equals("0000")){
				res.setCode("0014");
				res.setDesc("No data found!");
			}
		} catch (Exception e) {
			e.printStackTrace();
			res.setCode("0014");
			res.setDesc("Server Error!");
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}
}
