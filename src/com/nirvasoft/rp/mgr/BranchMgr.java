package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.database.SysKeyMgr;
import com.nirvasoft.fcpayment.core.data.fcchannel.CMSModuleConfigData;
import com.nirvasoft.rp.dao.BranchDao;
import com.nirvasoft.rp.data.BranchData;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.shared.icbs.BranchSetupDataSet;
import com.nirvasoft.rp.util.ServerUtil;

public class BranchMgr {

	public Result deleteBranchSetup(String branchCode) {
		Result res = new Result();
		Connection conn = null;
		BranchDao brDao = new BranchDao();
		try {
			conn = ConnAdmin.getConn("001", "");
			res = brDao.deleteBranchSetup(branchCode, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return res;
	}

	public ArrayList<BranchData> getAllBranch() {
		Connection l_Conn = null;
		BranchDao branch_dao = new BranchDao();
		ArrayList<BranchData> res = new ArrayList<BranchData>();

		try {
			l_Conn = ConnAdmin.getConn("001", "");
			res = branch_dao.getAllBranch(l_Conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public BranchSetupDataSet getAllBranchSetupData(String searchText, int pageSize, int currentPage) {
		BranchSetupDataSet res = new BranchSetupDataSet();
		BranchDao b_dao = new BranchDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = b_dao.getAllBranchSetupData(searchText, pageSize, currentPage, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return res;
	}

	public BranchData getBranchSetupDataByID(String id) {

		BranchData ret = new BranchData();
		BranchDao b_dao = new BranchDao();
		Connection conn = null;

		try {

			conn = ConnAdmin.getConn("001", "");
			ret = b_dao.getBranchSetupDataByID(id, conn);

		} catch (SQLException e) {

			e.printStackTrace();
		} finally {

			try {
				if (!conn.isClosed())
					conn.close();

			} catch (SQLException e) {

				e.printStackTrace();
			}
		}

		return ret;
	}

	public Result saveBranchSetup(BranchData data) {
		Result ret = new Result();
		BranchDao brDao = new BranchDao();
		Connection l_Conn = null;

		try {
			if (data.getSyskey() == 0) {
				l_Conn = ConnAdmin.getConn("001", "");
				data.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn("001", "")));
				ret = brDao.saveBranchSetup(data, l_Conn);

			} else {
				l_Conn = ConnAdmin.getConn("001", "");
				data.setSyskey(SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn("001", "")));
				ret = brDao.updateBranchSetup(data, l_Conn);
			}

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return ret;

	}
	//atn
	public CMSModuleConfigData getCMSModuleConfig(Connection pConn) throws SQLException {
		CMSModuleConfigData ret = new CMSModuleConfigData();
		BranchDao brDao = new BranchDao();		
		ret = brDao.getCMSModuleConfig(pConn);
		return ret;

	}

}
