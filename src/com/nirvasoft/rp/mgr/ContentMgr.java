package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;

import com.nirvasoft.cms.dao.ArticleDao;
import com.nirvasoft.cms.dao.ArticleJunDao;
import com.nirvasoft.cms.dao.UploadDao;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.shared.ArticleJunData;
import com.nirvasoft.cms.shared.DivisionComboDataSet;
import com.nirvasoft.cms.shared.PhotoUploadData;
import com.nirvasoft.cms.shared.UploadData;
import com.nirvasoft.database.SysKeyMgr;
import com.nirvasoft.rp.dao.ContentDao;
import com.nirvasoft.rp.dao.MerchantReportDao;
import com.nirvasoft.rp.data.ContentListingDataList;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.shared.DispaymentList;
import com.nirvasoft.rp.shared.FilterDataset;
import com.nirvasoft.rp.shared.PayTemplateDetail;
import com.nirvasoft.rp.util.Geneal;

public class ContentMgr {
	public static ArrayList<PhotoUploadData> checkUploadList(ArrayList<PhotoUploadData> uploadList, String str) {
		for (int i = 0; i < uploadList.size(); i++) {
			if (!str.contains(uploadList.get(i).getDesc())) {
				uploadList.remove(i);
			}
		}
		return uploadList;
	}

	/* searchSelfPost */
	/*
	 * public ContentListingDataList searchSelfPost(FilterDataset p) {
	 * ContentListingDataList res = new ContentListingDataList(); Connection
	 * conn = null; try { conn = ConnAdmin.getConn("001", ""); res = new
	 * ContentDao().searchSelfPost(p, conn); } catch (SQLException e) {
	 * e.printStackTrace(); } return res; }
	 */

	public static String convertBase64ToImage(ArticleData p, String imgurl) {

		String str = p.getT2();
		int a = str.indexOf("<img src=\"");
		String replaceString = "";
		if (a > 0) {
			int b = str.indexOf("alt"); // end index
			String exist = str.substring(a + 10, b - 2);
			String replace = imgurl + p.getUploadlist().get(0).getName();
			replaceString = str.replace(exist, replace);
			while (!exist.equalsIgnoreCase(replace)) {
				replaceString = str.replace(exist, replace);
				a = replaceString.indexOf("<img src=\""); // start index
				b = replaceString.indexOf(" alt="); // end index
				exist = replaceString.substring(a + 10, b - 1);
			}
			System.out.println("Str: " + replaceString);
		}
		return replaceString;
	}

	public static DivisionComboDataSet getAcademicRef(MrBean user) {
		DivisionComboDataSet res = new DivisionComboDataSet();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = new ContentDao().getAcademicRef(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static int getUploadCount(String str, ArrayList<PhotoUploadData> uploadList) {

		int uploadCount = 0;
		String exist = "";
		int c = 0;
		String Arr[] = new String[100];
		Arr = str.split(" alt=\"");
		c = Arr[1].indexOf(" width=\"");

		exist = Arr[1].substring(0, c - 1);

		for (int i = 0; i < uploadList.size(); i++) {
			if (uploadList.get(i).getDesc().equalsIgnoreCase(exist)) {
				uploadCount = i;
			}
		}
		return uploadCount;
	}

	public static ArticleJunData initJUNData(ArticleJunData typeData, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		if (typeData.getSyskey() == 0) {
			typeData.setCreateddate(todayDate);
		}
		typeData.setModifieddate(todayDate);
		if (!typeData.getUserid().equals("") && !typeData.getUsername().equals("")) {
			typeData.setUserid(typeData.getUserid());
			typeData.setUsername(typeData.getUsername());
		} else {
			typeData.setUserid(user.getUser().getUserId());
			typeData.setUsername(user.getUser().getUserName());
		}
		typeData.setRecordStatus(1);
		typeData.setSyncStatus(1);
		typeData.setSyncBatch(0);
		typeData.setUsersyskey(0);
		return typeData;
	}

	private static boolean isExistUpload(long syskey, Connection conn) throws SQLException {
		boolean isExit = false;
		ResultSet rs = null;
		String sql = "Select * from fmr006 where n1='" + syskey + "'";
		PreparedStatement ps = conn.prepareStatement(sql);
		rs = ps.executeQuery();
		if (rs.next()) {
			isExit = true;
		}
		return isExit;
	}

	public static ArrayList<PhotoUploadData> photoListLoopRemove(ArrayList<PhotoUploadData> uploadList) {
		ArrayList<Integer> count = new ArrayList<Integer>();
		HashSet<Integer> hSetNumbers = new HashSet<Integer>();
		for (int i = 0; i < uploadList.size(); i++) {
			String name = uploadList.get(i).getDesc();
			for (int ii = i + 1; ii < uploadList.size(); ii++) {
				if (name.equalsIgnoreCase(uploadList.get(ii).getDesc())) {
					hSetNumbers.add(ii);
				}
			}
		}
		// Collections.sort(count,Collections.reverseOrder());
		// Collections.reverseOrder();
		for (int strNumber : hSetNumbers)
			count.add(strNumber);
		Collections.sort(count, Collections.reverseOrder());
		// Collections.sort(count,Collections.reverseOrder());
		for (int j = 0; j < count.size(); j++) {
			int cnt = count.get(j);
			uploadList.remove(cnt - 1);
		}
		/*
		 * for(int j = count.size()-1 ; j >=0 ; j--) { int cnt = count.get(j);
		 * uploadList.remove(cnt-1); }
		 */
		return uploadList;
	}

	public static ArrayList<PhotoUploadData> photoListRemove(String content, ArrayList<PhotoUploadData> uploadList) {
		ArrayList<Integer> count = new ArrayList<Integer>();
		for (int i = 0; i < uploadList.size(); i++) {
			String desc = "alt=\"" + uploadList.get(i).getDesc() + "\"";
			if ((!content.contains(desc) && !content.contains(uploadList.get(i).getName()))
					|| (content.contains(desc) && content.contains(uploadList.get(i).getName()))) {
				count.add(i);
			}
		}

		for (int j = count.size() - 1; j >= 0; j--) {
			int cnt = count.get(j);
			uploadList.remove(cnt);
		}

		return uploadList;
	}

	public static ArticleData readDataContentMenu(long key, MrBean user) {
		ArticleData res = new ArticleData();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = new ContentDao().read(key, conn);
			// res.setCropdata(ArticleJunDao.search(String.valueOf(key),
			// conn).getCrop());
			res.setUploadlist(UploadDao.searchUploadList(String.valueOf(res.getSyskey()), conn));
			res.setResizelist(UploadDao.search(String.valueOf(res.getSyskey()), "", conn).getUploads());

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	public static Resultb2b saveContenMenu(ArticleData data, String filePath, MrBean user) {
		Resultb2b res = new Resultb2b();
		// ArticleJunData jundata = new ArticleJunData();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			conn.setAutoCommit(false);
			data = setStatusData(data, user);
			if (data.getN4() == 0) {
				data.setT3("news");
			}
			if (data.getN4() == 1) {
				data.setT3("article");
			}
			if (data.getN4() == 2) {
				data.setT3("business");
			}
			if (data.getN4() == 3) {
				data.setT3("interview");
			}
			if (data.getN4() == 4) {
				data.setT3("leadership");
			}
			if (data.getN4() == 5) {
				data.setT3("innovation");
			}
			if (data.getN4() == 6) {
				data.setT3("general");
			}
			if (data.getN4() == 7) {
				data.setT3("investment");
			}
			if (data.getN4() == 8) {
				data.setT3("user post");
			}
			if (data.getSyskey() == 0) {
				data.setSyskey(SysKeyMgr.getSysKey(1, "syskey", conn));
				res = ArticleDao.insert(data, conn);
				if (res.isState()) {
					for (int i = 0; i < data.getUploadlist().size(); i++) {
						boolean flag = false;
						UploadData obj = setPhotoUploadData(data.getUploadlist().get(i), data.getResizelist()[i],
								data.getSyskey(), user);
						obj.setModifiedTime(data.getModifiedTime());
						obj.setCreatedTime(data.getCreatedTime());
						obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey", conn));
						flag = UploadDao.insert(obj, conn);
						res.setState(flag);
					}
				}
			} else {
				res = ArticleDao.update(data, conn);
				if (res.isState()) {
					if (isExistUpload(data.getSyskey(), conn)) {
						String sql = " DELETE FROM FMr006 WHERE N1 = '" + data.getSyskey() + "' ";
						PreparedStatement ps = conn.prepareStatement(sql);
						ps.executeUpdate();
					}

					boolean flag = false;
					for (int i = 0; i < data.getUploadlist().size(); i++) {
						UploadData obj = setPhotoUploadData(data.getUploadlist().get(i), data.getResizelist()[i],
								data.getSyskey(), user);
						obj.setSyskey(SysKeyMgr.getSysKey(1, "syskey", conn));
						obj.setModifiedTime(data.getModifiedTime());
						obj.setCreatedTime(data.getCreatedTime());
						flag = UploadDao.insert(obj, conn);
						res.setState(flag);
					}

				}
			}
			if (res.isState()) {
				res.setKeyResult(data.getSyskey());
				res.getLongResult().add(data.getSyskey());
				conn.commit();
			} else
				conn.rollback();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return res;
	}

	public static UploadData setPhotoUploadData(PhotoUploadData d, String resizefilename, long articleKey,
			MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		UploadData data = new UploadData();
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(data.getCreatedTime());
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(data.getModifiedTime());
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		data.setT1(d.getName());// photo name
		data.setT3(String.valueOf(d.getSerial()));// photo serial no
		data.setT4(d.getDesc());// description
		data.setT5(d.getOrder());// description
		data.setT6(resizefilename);
		data.setT7(d.getUrl());

		data.setN1(articleKey);// fmr002 sk
		return data;
	}

	public static ArticleData setStatusData(ArticleData data, MrBean user) {
		String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		Date today = new Date();
		if (data.getSyskey() == 0) {
			data.setCreatedDate(todayDate);
			data.setCreatedTime(Geneal.getTimeAMPM(today));
		}
		data.setModifiedDate(todayDate);
		data.setModifiedTime(Geneal.getTimeAMPM(today));
		if (data.getSyskey() == 0) {
			data.setModifiedUserId("");
			data.setModifiedUserName("");

		} else {
			data.setModifiedUserId(data.getModifiedUserId());
			data.setModifiedUserName(data.getModifiedUserName());
		}
		// data.setUserId(data.getUserId());
		// data.setUserName(data.getUserName());
		if (!data.getUserId().equals("") && !data.getUserName().equals("")) {
			data.setUserId(data.getUserId());
			data.setUserName(data.getUserName());
		} else {
			data.setUserId(user.getUser().getUserId());
			data.setUserName(user.getUser().getUserName());
		}
		/*
		 * if (ServerUtil.isZawgyiEncoded(data.getT2())) {
		 * data.setT2(FontChangeUtil.zg2uni(data.getT2())); }
		 */
		data.setRecordStatus(1);
		data.setSyncStatus(1);
		data.setSyncBatch(0);
		data.setUserSyskey(0);
		return data;
	}

	public ArticleData readDataBySyskey(long key, MrBean user) {
		ArticleData res = new ArticleData();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = new ContentDao().read(key, conn);
			res.setCropdata(new ArticleJunDao().search(String.valueOf(key), conn).getCrop());
			if (res.getN10() == 1 || res.getN10() == 2) {
				res.setUpload(UploadDao.search(String.valueOf(res.getSyskey()), "Video", conn).getUploads());
				res.setUploadlist(UploadDao.searchUploadList(String.valueOf(res.getSyskey()), conn));
			} else {
				res.setVideoUpload(UploadDao.searchVideoList(String.valueOf(res.getSyskey()), conn).getVideoUpload());
				res.setUpload(UploadDao.search(String.valueOf(res.getSyskey()), "Video", conn).getUploads());
				res.setUploadlist(UploadDao.searchUploadList(String.valueOf(res.getSyskey()), conn));
				res.setResizelist(UploadDao.search(String.valueOf(res.getSyskey()), "", conn).getUploads());
				if (res.getT3().equalsIgnoreCase("Video")) {
					res.setResizelist(UploadDao.search(String.valueOf(res.getSyskey()), "Video", conn).getUploads());
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	/* searchQuesList */
	public ContentListingDataList selectContent(FilterDataset p) {
		ContentListingDataList res = new ContentListingDataList();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = new ContentDao().selectContent(p, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
	
	public DispaymentList getMerchantList(int currentPage, int totalCount, int pageSize, String aMerchantID,
			String aFeature, String status, String aFromDate, String aToDate, String aFromAcc, String aToAcc,
			String did, String initiatedby, String transrefno, String processingCode, String proDisCode,
			PayTemplateDetail[] templateData,String rptitle,String select,String merchantName,String download){//

		DispaymentList merchantret = new DispaymentList();

		Connection l_Conn = null;
		MerchantReportDao l_DAO = new MerchantReportDao();
		try {
			l_Conn = ConnAdmin.getConn("001", "");
			merchantret = l_DAO.getMerchantList(currentPage, totalCount, pageSize, aMerchantID, 
					status, aFromDate,aToDate, aFromAcc, aToAcc, did, 
					initiatedby, transrefno, processingCode, proDisCode, templateData,l_Conn);//aFeature, 
			/*if (download.equals("1")) {
				if (merchantret.isState()) {
					if (merchantret.getData().length != 0) {
						DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
						Date date = new Date();
						String param = (dateFormat.format(date));
						if (select == "PDF" || select.equalsIgnoreCase("PDF")) {
							String fileName = "merchant-transfer-" + param + ".pdf";
							boolean ret;
							ret = PDFMgr.merchantTransReport(merchantret, fileName, rptitle, aFromDate, aToDate,processingCode,aFeature,
									aMerchantID,merchantName);
							if (ret) {
								merchantret.setState(true);
								merchantret.getStringResult().add(fileName);
							} else {
								merchantret.setState(false);
								merchantret.setMsgDesc("Can't write File");
							}

						} else if (select == "Excel" || select.equalsIgnoreCase("Excel")) {
							String fileName = "merchant-transfer-" + param + ".xls";
							boolean ret = ExcelFileMgr.merchantTransReport(merchantret, fileName, rptitle, aFromDate, aToDate,processingCode,aFeature,
									aMerchantID,merchantName);
							if (ret) {
								merchantret.setState(true);
								merchantret.getStringResult().add(fileName);
							} else {
								merchantret.setState(false);
								merchantret.setMsgDesc("Can't write File");
							}
						}
					}else {
						merchantret.setState(false);
						merchantret.setMsgDesc("No Record Found");
					}
				} else {
					merchantret.setState(false);
					merchantret.setMsgDesc("No Record Found");
				}
			}*/
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return merchantret;
	}
}
