package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nirvasoft.rp.dao.ATMLocationDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Lov3;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.shared.ATMLocation;
import com.nirvasoft.rp.shared.ATMLocationList;
import com.nirvasoft.rp.shared.LocationData;
import com.nirvasoft.rp.util.ServerUtil;

public class ATMLocationMgr {

	public Result deleteATMLocator(ATMLocation data) {

		Result ret = new Result();
		ATMLocationDao atmdao = new ATMLocationDao();
		Connection l_Conn = null;
		try {
			l_Conn = ConnAdmin.getConn("001", "");
			ret = atmdao.deleteATMLocator(data, l_Conn);

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {
			ServerUtil.closeConnection(l_Conn);
		}
		return ret;
	}

	public LocationData getATMLocation(String id) {
		// TODO Auto-generated method stub
		// ATMLocationResData ret = new ATMLocationResData();
		LocationData ret = new LocationData();
		Connection l_Conn = null;
		ATMLocationDao dao = new ATMLocationDao();
		try {
			l_Conn = ConnAdmin.getConn("001", "");
			ret = dao.getATMLocation(id, l_Conn);

		} catch (Exception e) {
			e.printStackTrace();
			ret.setCode("0014");
			ret.setDesc(e.getMessage());
		} finally {
			ServerUtil.closeConnection(l_Conn);
		}
		return ret;
	}

	public ATMLocation getATMLocatorByID(String atmId) {

		ATMLocation ret = new ATMLocation();
		ATMLocationDao atmdao = new ATMLocationDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			ret = atmdao.getATMLocatorByID(atmId, conn);

		} catch (SQLException e) {

			e.printStackTrace();
		} finally {

			ServerUtil.closeConnection(conn);
		}

		return ret;
	}

	public ATMLocationList getATMLocatorList(String searchText, int pageSize, int currentPage) {
		ATMLocationList res = new ATMLocationList();
		ATMLocationDao atmdao = new ATMLocationDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = atmdao.getATMLocatorList(searchText, pageSize, currentPage, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return res;
	}

	public String getDate() {
		Date todaydate = new Date();
		DateFormat dateformat = new SimpleDateFormat("yyyyMMdd");
		String today = dateformat.format(todaydate);
		return today;
	}

	public Lov3 getLocationCbo() {
		Lov3 lov = new Lov3();
		ATMLocationDao dao = new ATMLocationDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			lov = dao.getLocationCbo(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return lov;
	}

	public Result saveATMLocator(ATMLocation data) {

		Result ret = new Result();
		ATMLocationDao atmdao = new ATMLocationDao();
		Connection l_Conn = null;

		try {
			if (data.getT1().equals("")) {
				l_Conn = ConnAdmin.getConn("001", "");
				data.setCreatedDate(getDate());
				data.setModifiedDate(getDate());
				ret = atmdao.saveATMLocator(data, l_Conn);
				if (ret.isState()) {
					ret.setMsgDesc("Saved Successfully");
				} else {
					if (!ret.getMsgCode().equals("0014")) {
						ret.setMsgDesc("Data already exists!");
					} else {
						ret.setMsgDesc("Saved Fail");
					}
				}
			} else {
				l_Conn = ConnAdmin.getConn("001", "");
				if (data.getCreatedDate().equals("")) {
					data.setCreatedDate(getDate());
				}
				data.setModifiedDate(getDate());
				ret = atmdao.updateATMLocator(data, l_Conn);
				if (ret.isState()) {
					ret.setMsgDesc("Updated Successfully");
					ret.setMsgCode("0000");
				} else {
					ret.setMsgDesc("Updated Fail");
					ret.setMsgCode("0014");
				}

			}
		} catch (Exception e) {

			e.printStackTrace();

		} finally {

			ServerUtil.closeConnection(l_Conn);
		}

		return ret;

	}

}
