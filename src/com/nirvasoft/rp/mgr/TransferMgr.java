package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

import com.nirvasoft.cms.dao.RegisterMobileDao;
import com.nirvasoft.fcpayment.core.data.fcchannel.DisPaymentTransactionData;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.dao.HolidayDao;
import com.nirvasoft.rp.dao.TransferDao;
import com.nirvasoft.rp.dao.WJunctionDao;
import com.nirvasoft.rp.dao.WalletTransactionDao;
import com.nirvasoft.rp.dao.icbs.AccountDao;
import com.nirvasoft.rp.dao.icbs.AccountGLTransactionDao;
import com.nirvasoft.rp.dao.icbs.AccountTransactionDao;
import com.nirvasoft.rp.dao.icbs.GLDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.shared.AccTransferReq;
import com.nirvasoft.rp.shared.ChatData;
import com.nirvasoft.rp.shared.EPIXResponse;
import com.nirvasoft.rp.shared.Response;
import com.nirvasoft.rp.shared.TransferConfirmResData;
import com.nirvasoft.rp.shared.TransferOutReq;
import com.nirvasoft.rp.shared.TransferOutRes;
import com.nirvasoft.rp.shared.TransferReqV2Data;
import com.nirvasoft.rp.shared.TransferResData;
import com.nirvasoft.rp.shared.WalletTransaction;
import com.nirvasoft.rp.shared.icbs.AccountData;
import com.nirvasoft.rp.shared.icbs.AccountGLTransactionData;
import com.nirvasoft.rp.shared.icbs.AccountTransactionData;
import com.nirvasoft.rp.util.Constant;
import com.nirvasoft.rp.util.Geneal;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ServerGlobal;

public class TransferMgr {

	public static String getEffectiveTransDate(String pDate, Connection conn) throws SQLException {
		HolidayDao dao = new HolidayDao();
		int year = Integer.parseInt(pDate.substring(0, 4));
		int month = Integer.parseInt(pDate.substring(4, 6));
		int day = Integer.parseInt(pDate.substring(6, 8));
		Calendar cal = Calendar.getInstance();
		month--; // In Java Calendar, month is starting zero.
		cal.set(year, month, day);
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		String effectiveDate = pDate;
		// Checking is Runned EOD
		if (dao.isRunnedEOD(pDate, conn)) {
			cal.clear();
			cal.set(year, month, ++day);
			effectiveDate = df.format(cal.getTime());
			effectiveDate = getEffectiveTransDate(effectiveDate, conn);
		} else if (GeneralUtil.isWeekEnd(pDate)) {
			cal.clear();
			cal.set(year, month, ++day);
			effectiveDate = df.format(cal.getTime());
			effectiveDate = getEffectiveTransDate(effectiveDate, conn);
		} else if (dao.getBankHolidayCheck(pDate, conn)) { // Checking
															// BankHoliday
			cal.set(year, month, ++day);
			effectiveDate = df.format(cal.getTime());
			effectiveDate = getEffectiveTransDate(effectiveDate, conn);
		} else {
			effectiveDate = df.format(cal.getTime());
		}
		return effectiveDate;
	}

	public boolean checkPassword(String password, String userid) {

		boolean res = false;
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			if (conn.equals("null") || conn.equals("")) {

			} else {
				res = RegisterMobileDao.CheckPassword(password, userid, conn);
			}

		} catch (Exception e) {
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("checkPassword.... " + e.toString());
				l_err.add("-----------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
			}

		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}

			} catch (SQLException e) {
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("");
					l_err.add("=============================================================================");
					l_err.add("Time :" + GeneralUtil.getTime());
					l_err.add("checkPassword.... " + e.toString());
					l_err.add("-----------------------------------------------------------------------------");
					GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
				}
			}
		}

		return res;
	}

	public EPIXResponse checkTransLimit(String fromAccount, String userID, double amount) throws Exception {
		EPIXResponse ret = new EPIXResponse();
		String minTransLimit = "";
		String dayTransLimit = "";
		String maxTransLimit = "";
		double transAmtPerday = 0.00;
		Connection conn = null;
		conn = new DAOManager().openICBSConnection();
		minTransLimit = ConnAdmin.readConfig("MinTransLimit");
		dayTransLimit = ConnAdmin.readConfig("DayTransLimit");
		maxTransLimit = ConnAdmin.readConfig("MaxTransLimit");
		if (minTransLimit.equals("")) {
			minTransLimit = "0";
		}
		if (dayTransLimit.equals("")) {
			dayTransLimit = "0";
		}
		transAmtPerday = new AccountTransactionDao().getTransAmountPerDayICBS(fromAccount, userID,
				GeneralUtil.datetoString(), conn);
		if (amount < Double.parseDouble(minTransLimit)) {
			conn.close();
			ret.setCode("0014");
			ret.setDesc("Transfer Amount must be at least " + GeneralUtil.formatNumber(Double.valueOf(minTransLimit))
					+ " MMK");
			return ret;
		} else {
			ret.setCode("0000");
			ret.setDesc("Success");
		}
		if (amount > Double.parseDouble(maxTransLimit)) {
			conn.close();
			ret.setCode("0014");
			ret.setDesc("Transfer Amount exceeded over " + GeneralUtil.formatNumber(Double.valueOf(maxTransLimit))
					+ " MMK");
			return ret;
		} else {
			ret.setCode("0000");
			ret.setDesc("Success");
		}
		if (amount + transAmtPerday > Double.parseDouble(dayTransLimit)) {
			conn.close();
			ret.setCode("0014");
			ret.setDesc("Transaction exceeded over daily transaction limit "
					+ GeneralUtil.formatNumber(Double.valueOf(dayTransLimit)) + " MMK");
			return ret;
		} else {
			ret.setCode("0000");
			ret.setDesc("Success");
		}
		conn.close();
		return ret;
	}

	public TransferResData getRandomRefKey() {
		TransferResData res = new TransferResData();
		try {
			Random rnd = new Random();
			int n = 0;
			n = 100000 + rnd.nextInt(900000);
			if (n == 0) {
				res.setCode("0014");
				res.setDesc("Invalid Random Number!");
			} else {
				res.setRefKey(String.valueOf(n));
				res.setCode("0000");
			}
		} catch (Exception e) {
			e.printStackTrace();
			res.setCode("0014");
			res.setDesc(e.getMessage());
		}
		return res;
	}

	public TransferResData getRefkey() {
		TransferResData res = new TransferResData();
		Connection conn = null;
		DAOManager daomgr = new DAOManager();
		try {
			conn = ConnAdmin.getConn("001", "");
			String key = daomgr.getRefKey(conn);
			if (key.equals("0014")) {
				res.setCode("0014");
				res.setDesc("Key cannot generated!");
			} else {
				res.setRefKey(key);
				res.setCode("0000");
			}
			res.setRefKey(daomgr.getRefKey(conn));
		} catch (Exception e) {

			e.printStackTrace();
			res.setCode("0014");
			res.setDesc(e.getMessage());
		} finally {
			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {

				e.printStackTrace();
				res.setCode("0014");
				res.setDesc(e.getMessage());
			}
		}
		return res;
	}

	private Response makeCashDepositGLTransaction(String drAccNo, double drPrevBal, String crAccNo, double crPrevBal,
			double amount, long transRef, int status, Connection conn) throws SQLException {
		Response ret = new Response();
		AccountGLTransactionData data = new AccountGLTransactionData();
		ArrayList<AccountGLTransactionData> dataList = new ArrayList<AccountGLTransactionData>();
		AccountGLTransactionDao dao = new AccountGLTransactionDao();

		// prepare Cr Info
		if (!(crAccNo.equals(""))) {
			data = new AccountGLTransactionData();
			data.setTransNo(transRef);
			data.setAccNumber(crAccNo);
			data.setGLCode1("L050101001001001");
			data.setGLCode2("A010101001001001");
			data.setGLCode3("");
			data.setGLCode4("");
			data.setGLCode5("");
			data.setBaseCurCode("MMK");
			data.setBaseCurOperator("/");
			data.setBaseRate(1);
			data.setBaseAmount(amount);
			data.setMediumCurCode("");
			data.setMediumCurOperator("");
			data.setMediumRate(0);
			data.setMediumAmount(0);
			data.setTrCurCode("MMK");
			data.setTrCurOperator("");
			data.setTrRate(1);
			data.setTrAmount(amount);
			data.setTrPrevBalance(crPrevBal);
			data.setN1(status);
			data.setN2(0);
			data.setN3(0);
			data.setT1("");
			data.setT2("");
			data.setT3("");
			dataList.add(data);
		}
		if (dao.save(dataList, conn)) {
			ret.setMessageCode("0000");
			ret.setMessageDesc("AccountGLTransaction saved successfully.");
		} else {
			ret.setMessageCode("0018");
			ret.setMessageDesc("AccountGLTransaction saved fail.");
		}
		return ret;
	}

	private Response makeCashDepositTransaction(String drAccNo, double drPrevBal, String crAccNo, double crPrevBal,
			double amount, int transRef, int status, String subRef, String isTrOrRev, String drRemark, String crRemark,
			String drAccRef, String crAccRef, String superVisorID, Connection conn) throws SQLException {
		Response ret = new Response();
		AccountTransactionData data = new AccountTransactionData();
		// ArrayList<AccountTransactionData> dataList = new
		// ArrayList<AccountTransactionData>();
		AccountTransactionDao dao = new AccountTransactionDao();
		String effectiveDate = getEffectiveTransDate(GeneralUtil.getDateYYYYMMDDSimple(), conn);
		String XtransRef = "0";

		// prepare Cr Info
		if (!crAccNo.equals("")) {
			data = new AccountTransactionData();
			data.setBranchCode("002");
			data.setWorkStation("Wallet");
			if (isTrOrRev.equalsIgnoreCase("REV")) {
				data.setTransRef(transRef);
			} else {
				data.setTransRef(Integer.parseInt(XtransRef));
			}
			data.setSerialNo(0);
			data.setTellerId("admin");
			data.setSupervisorId(superVisorID);
			data.setTransTime(GeneralUtil.getDateYYYYMMDDHHMMSS());
			data.setTransDate(GeneralUtil.getDateYYYYMMDD());
			data.setDescription("Topup" + ", " + GeneralUtil.getDateYYYYMMDDHHMMSS());
			data.setChequeNo("");
			data.setCurrencyCode("MMK");
			data.setCurrencyRate(1);
			data.setAmount(amount);
			data.setTransType(10);
			data.setAccNumber(crAccNo);
			data.setPrevBalance(crPrevBal);
			data.setPrevUpdate(GeneralUtil.getDateYYYYMMDD());
			data.setEffectiveDate(effectiveDate);
			data.setContraDate("");
			data.setStatus(status);
			data.setAccRef(crAccRef);
			data.setRemark(crRemark);
			data.setSystemCode(0);
			data.setSubRef(subRef);

			ret = dao.addAccTransactionV1(data, conn);
			if (ret.getMessageCode().equals("0000")) {
				XtransRef = ret.getTrnRefNo();
				if (isTrOrRev.equalsIgnoreCase("TR")) {
					if (subRef.equals(""))
						subRef = XtransRef;
					if (dao.updateTransRefByTransNo(Integer.parseInt(XtransRef), subRef, conn)) {
						ret.setMessageCode("0000");
						ret.setMessageDesc("Transaction saved successfully.");
					}
				} else {
					ret.setMessageCode("0000");
					ret.setMessageDesc("Transaction saved successfully.");
				}
			} else {
				ret.setMessageCode("0018");
				ret.setMessageDesc("Transaction saved fail.");
			}
		} else {
			ret.setMessageCode("0018");
			ret.setMessageDesc("Transaction saved fail.");
		}
		return ret;
	}

	private Response makeGLTransaction(String drAccNo, double drPrevBal, String crAccNo, double crPrevBal,
			double amount, int transRef, int status, Connection conn) throws SQLException {
		Response ret = new Response();
		AccountGLTransactionData data = new AccountGLTransactionData();
		ArrayList<AccountGLTransactionData> dataList = new ArrayList<AccountGLTransactionData>();
		AccountGLTransactionDao dao = new AccountGLTransactionDao();
		boolean isGLAccount = new GLDao().isGLAccount(drAccNo,conn);
		
		
		// prepare Dr Info
		if (!(drAccNo.equals(""))) {
			data.setTransNo(transRef);
			data.setAccNumber(drAccNo);
			if(isGLAccount)
				data.setGLCode1("");
			else
				data.setGLCode1("L050101001001001");
			data.setGLCode2("A010101001001001");
			data.setGLCode3("");
			data.setGLCode4("");
			data.setGLCode5("");
			data.setBaseCurCode("MMK");
			data.setBaseCurOperator("/");
			data.setBaseRate(1);
			data.setBaseAmount(amount);
			data.setMediumCurCode("");
			data.setMediumCurOperator("");
			data.setMediumRate(0);
			data.setMediumAmount(0);
			data.setTrCurCode("MMK");
			data.setTrCurOperator("");
			data.setTrRate(1);
			data.setTrAmount(amount);
			data.setTrPrevBalance(drPrevBal);
			data.setN1(status);
			data.setN2(0);
			data.setN3(0);
			data.setT1("");
			data.setT2("");
			data.setT3("");
			dataList.add(data);
		}
		// prepare Cr Info
		if (!(crAccNo.equals(""))) {
			isGLAccount = new GLDao().isGLAccount(crAccNo, conn);
			data = new AccountGLTransactionData();
			data.setTransNo(transRef + 1);
			data.setAccNumber(crAccNo);
			if(isGLAccount)
				data.setGLCode1("");
			else
				data.setGLCode1("L050101001001001");
			data.setGLCode2("A010101001001001");
			data.setGLCode3("");
			data.setGLCode4("");
			data.setGLCode5("");
			data.setBaseCurCode("MMK");
			data.setBaseCurOperator("/");
			data.setBaseRate(1);
			data.setBaseAmount(amount);
			data.setMediumCurCode("");
			data.setMediumCurOperator("");
			data.setMediumRate(0);
			data.setMediumAmount(0);
			data.setTrCurCode("MMK");
			data.setTrCurOperator("");
			data.setTrRate(1);
			data.setTrAmount(amount);
			data.setTrPrevBalance(crPrevBal);
			data.setN1(status);
			data.setN2(0);
			data.setN3(0);
			data.setT1("");
			data.setT2("");
			data.setT3("");
			dataList.add(data);
		}
		if (dao.save(dataList, conn)) {
			ret.setMessageCode("0000");
			ret.setMessageDesc("AccountGLTransaction saved successfully.");
		} else {
			ret.setMessageCode("0018");
			ret.setMessageDesc("AccountGLTransaction saved fail.");
		}
		return ret;
	}

	private Response makeTransaction(String drAccNo, double drPrevBal, String crAccNo, double crPrevBal, double amount,
			int transRef, int status, String subRef, String isTrOrRev, String drRemark, String crRemark,
			String drAccRef, String crAccRef, String superVisorID, String drDesc, String crDesc, Connection conn)
			throws SQLException {
		Response ret = new Response();
		AccountTransactionData data = new AccountTransactionData();
		// ArrayList<AccountTransactionData> dataList = new
		// ArrayList<AccountTransactionData>();
		AccountTransactionDao dao = new AccountTransactionDao();
		String effectiveDate = getEffectiveTransDate(GeneralUtil.getDateYYYYMMDDSimple(), conn);
		String XtransRef = "";
		// prepare Dr Info
		if (!drAccNo.equals("")) {
			data.setBranchCode("002");
			data.setWorkStation("Wallet");
			if (isTrOrRev.equalsIgnoreCase("REV")) {
				data.setTransRef(transRef);
			}
			data.setSerialNo(0);
			data.setTellerId("admin");
			data.setSupervisorId(superVisorID);
			data.setTransTime(GeneralUtil.getDateYYYYMMDDHHMMSS());
			data.setTransDate(GeneralUtil.getDateYYYYMMDD());
			data.setDescription(drDesc + ", " + GeneralUtil.getDateYYYYMMDDHHMMSS());
			data.setChequeNo("");
			data.setCurrencyCode("MMK");
			data.setCurrencyRate(1);
			data.setAmount(amount);
			data.setTransType(705);
			data.setAccNumber(drAccNo);
			data.setPrevBalance(drPrevBal);
			data.setPrevUpdate(GeneralUtil.getDateYYYYMMDD());
			data.setEffectiveDate(effectiveDate);
			data.setContraDate("");
			data.setStatus(status);
			data.setAccRef(drAccRef);
			data.setRemark(drRemark);
			data.setSystemCode(0);
			data.setSubRef(subRef);
			ret = dao.addAccTransactionV1(data, conn);
			if (ret.getMessageCode().equals("0000")) {
				XtransRef = ret.getTrnRefNo();
				if (isTrOrRev.equalsIgnoreCase("TR")) {
					/*
					 * if (subRef.equals("")) subRef = XtransRef;
					 */
					if (dao.updateTransRefByTransNo(Integer.parseInt(XtransRef), subRef, conn)) {
						ret.setMessageCode("0000");
						// subRef = XtransRef;
					}
				}
			}
		}
		// prepare Cr Info
		if (ret.getMessageCode().equals("0000")) {
			if (!crAccNo.equals("")) {
				data = new AccountTransactionData();
				data.setBranchCode("002");
				data.setWorkStation("Wallet");
				if (isTrOrRev.equalsIgnoreCase("REV")) {
					data.setTransRef(transRef);
				} else {
					data.setTransRef(Integer.parseInt(XtransRef));
				}
				data.setSerialNo(0);
				data.setTellerId("admin");
				data.setSupervisorId(superVisorID);
				data.setTransTime(GeneralUtil.getDateYYYYMMDDHHMMSS());
				data.setTransDate(GeneralUtil.getDateYYYYMMDD());
				data.setDescription(crDesc + ", " + GeneralUtil.getDateYYYYMMDDHHMMSS());
				data.setChequeNo("");
				data.setCurrencyCode("MMK");
				data.setCurrencyRate(1);
				data.setAmount(amount);
				data.setTransType(205);
				data.setAccNumber(crAccNo);
				data.setPrevBalance(crPrevBal);
				data.setPrevUpdate(GeneralUtil.getDateYYYYMMDD());
				data.setEffectiveDate(effectiveDate);
				data.setContraDate("");
				data.setStatus(status);
				data.setAccRef(crAccRef);
				data.setRemark(crRemark);
				data.setSystemCode(0);
				data.setSubRef(subRef);

				ret = dao.addAccTransactionV1(data, conn);
				if (ret.getMessageCode().equals("0000")) {
					ret.setMessageCode("0000");
					ret.setMessageDesc("AccountTransaction saved successfully.");
					ret.setTrnRefNo(XtransRef);
				} else {
					ret.setMessageCode("0018");
					ret.setMessageDesc("AccountTransaction saved fail.");
				}
			}
		}
		return ret;
	}

	public TransferOutRes payment(TransferOutReq reqData) {
		Connection conn = null;
		Connection conn2 = null;
		TransferOutRes ret = new TransferOutRes();
		AccountData l_DrAccData = new AccountData();
		AccountData l_CrAccData = new AccountData();
		Response res = new Response();
		DAOManager l_DAOMgr = new DAOManager();
		AccountDao l_AccDao = new AccountDao();
		String transRef = "";
		GeneralUtil.readFCServiceSetting();
		EPIXResponse epiRes = new EPIXResponse();
		try {
			conn = l_DAOMgr.openConnection();
			conn2 = l_DAOMgr.openICBSConnection();
			if (conn != null) {
				// get Debit Info
				String drWAccNo = new WJunctionDao().getDataByID(reqData.getUserID(), conn).getAccNumber();
				l_DrAccData = l_AccDao.getAccountByAccNo(drWAccNo, conn2);
				String crWAccNo = "";
				if (!reqData.getBeneficiaryID().startsWith("+")) {
					crWAccNo = reqData.getBeneficiaryID();
				} else {
					crWAccNo = new WJunctionDao().getDataByID(reqData.getBeneficiaryID(), conn).getAccNumber();
					l_CrAccData = l_AccDao.getAccountByAccNo(crWAccNo, conn2);
				}
				if (l_DrAccData.getAccountNumber().equals("")) {
					ret.setCode("0019");
					ret.setDesc("Sender doesn't exist.");
					return ret;
				} else if (crWAccNo.equals("")) {
					ret.setCode("0019");
					ret.setDesc("Payee doesn't exist.");
					return ret;
				}
				epiRes = checkTransLimit(drWAccNo, "", reqData.getAmount());
				if (!epiRes.getCode().equals("0000")) {
					ret.setCode(epiRes.getCode());
					ret.setDesc(epiRes.getDesc());
					return ret;
				}
				if (l_AccDao.getBalanceByIDV2(drWAccNo, conn2) < reqData.getAmount()) {
					ret.setCode("0014");
					ret.setDesc("Incifficient balance!");
					return ret;
				}
				String isTrOrRev = "TR";
				// make transaction
				String drRemark = "Payment to " + reqData.getBeneficiaryID();
				String crRemark = "Receipt from " + reqData.getUserID();
				String drDesc = "Payment";
				String crDesc = "Receipt";
				res = makeTransaction(l_DrAccData.getAccountNumber(), l_DrAccData.getCurrentBalance(), crWAccNo,
						l_CrAccData.getCurrentBalance(), reqData.getAmount(), 0, 1, reqData.getRemark(), isTrOrRev,
						drRemark, crRemark, "", "", "", drDesc, crDesc, conn2);
				if (res.getMessageCode().equals("0000")) {
					transRef = res.getTrnRefNo();
					res = makeGLTransaction(l_DrAccData.getAccountNumber(), l_DrAccData.getCurrentBalance(), crWAccNo,
							l_CrAccData.getCurrentBalance(), reqData.getAmount(), Integer.parseInt(res.getTrnRefNo()),
							1, conn2);
				}
				if (res.getMessageCode().equals("0000")) {
					if (l_AccDao.updateDrCurrentBal(l_DrAccData.getAccountNumber(), reqData.getAmount(),
							GeneralUtil.getDateYYYYMMDD(), GeneralUtil.getDateYYYYMMDD(), conn2)) {
						if (!reqData.getBeneficiaryID().startsWith("+")) {
							new WalletTransactionDao().save(updateWalletDataCr(reqData, transRef, "", "2"), conn);
							ret.setBankRefNo(transRef);
							ret.setTransDate(GeneralUtil.mobiledateformat(GeneralUtil.datetoString()));
							ret.setCode("0000");
							ret.setDesc("Payment successful");
							return ret;
						} else {
							if (l_AccDao.updateCrCurrentBal(crWAccNo, reqData.getAmount(),
									GeneralUtil.getDateYYYYMMDD(), GeneralUtil.getDateYYYYMMDD(), conn2)) {
								if (res.getMessageCode().equals("0000")) {
									String notiMessageCr = "You have received"
											+ GeneralUtil.formatNumber(reqData.getAmount()) + " Ks from "
											+ reqData.getUserID() + " with Ref No. " + transRef + ".";
									new WalletTransactionDao()
											.save(updateWalletDataCr(reqData, transRef, notiMessageCr, "2"), conn);
									ret.setBankRefNo(transRef);
									ret.setTransDate(GeneralUtil.mobiledateformat(GeneralUtil.datetoString()));
									ret.setCode("0000");
									ret.setDesc("Payment successful");
								} else {
									ret.setCode(res.getMessageCode());
									ret.setDesc("Server Error!");
								}
							}
						}
					}
				}
			} else {
				ret.setCode("0015");
				ret.setDesc("Connection error.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			ret.setCode("0014");
			ret.setDesc("Server error.");
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					ret.setCode("0014");
					ret.setDesc("Server error.");
				}
			}
			if (conn2 != null) {
				try {
					if (!conn2.isClosed())
						conn2.close();
				} catch (SQLException e) {
					ret.setCode("0014");
					ret.setDesc("Server error.");
				}
			}
		}
		return ret;
	}

	public Response paymentUtility(String fromAcc, String accNumber, String userID, double amount, String reference,
			Connection conn) throws SQLException {
		Response ret = new Response();
		AccountDao l_AccDao = new AccountDao();
		String isTrOrRev = "TR";
		// make transaction
		int transRef = 0;
		String crRemark = "";
		String drRemark = "";
		String drDesc = "Utility Payment";
		String crDesc = "Utility Payment";
		conn.setAutoCommit(false);
		ret = makeTransaction(fromAcc, 0, accNumber, 0, amount, 0, 1, "", isTrOrRev, drRemark, crRemark, "", "", "",
				drDesc, crDesc, conn);
		if (ret.getMessageCode().equals("0000")) {
			transRef = Integer.parseInt(ret.getTrnRefNo());
			ret = makeGLTransaction(fromAcc, 0, accNumber, 0, amount, transRef, 1, conn);
		}
		if (ret.getMessageCode().equals("0000")) {
			if (l_AccDao.updateDrCurrentBal(fromAcc, amount, GeneralUtil.getDateYYYYMMDD(),
					GeneralUtil.getDateYYYYMMDD(), conn)) {
				if (ret.getMessageCode().equals("0000")) {
					ret.setTrnRefNo(String.valueOf(transRef));
					ret.setMessageCode("0000");
					ret.setMessageDesc("Payment successful.");
					conn.commit();
				} else {
					ret.setMessageCode("0014");
					ret.setMessageDesc("Payment failed.");
					conn.rollback();
				}
			} else {
				ret.setMessageCode("0014");
				ret.setMessageDesc("Payment failed.");
				conn.rollback();
			}
		} else {
			ret.setMessageCode("0014");
			ret.setMessageDesc("Payment failed.");
			conn.rollback();
		}
		return ret;
	}
	
	public Response walletToAccount(String fromAcc, String accNumber, String userID, double amount, String reference,
			Connection conn) throws SQLException {
		Response ret = new Response();
		AccountDao l_AccDao = new AccountDao();
		String isTrOrRev = "TR";
		// make transaction
		int transRef = 0;
		String crRemark = "";
		String drRemark = "";
		String drDesc = "Wallet to Account";
		String crDesc = "Wallet to Account";
		conn.setAutoCommit(false);
		ret = makeTransaction(fromAcc, 0, accNumber, 0, amount, 0, 1, "", isTrOrRev, drRemark, crRemark, "", "", "",
				drDesc, crDesc, conn);
		if (ret.getMessageCode().equals("0000")) {
			transRef = Integer.parseInt(ret.getTrnRefNo());
			ret = makeGLTransaction(fromAcc, 0, accNumber, 0, amount, transRef, 1, conn);
		}
		if (ret.getMessageCode().equals("0000")) {
			if (l_AccDao.updateDrCurrentBal(fromAcc, amount, GeneralUtil.getDateYYYYMMDD(),
					GeneralUtil.getDateYYYYMMDD(), conn)) {
				if (ret.getMessageCode().equals("0000")) {
					ret.setTrnRefNo(String.valueOf(transRef));
					ret.setMessageCode("0000");
					ret.setMessageDesc("Transfer successful.");
					conn.commit();
				} else {
					ret.setMessageCode("0014");
					ret.setMessageDesc("Transfer failed.");
					conn.rollback();
				}
			} else {
				ret.setMessageCode("0014");
				ret.setMessageDesc("Transfer failed.");
				conn.rollback();
			}
		} else {
			ret.setMessageCode("0014");
			ret.setMessageDesc("Transfer failed.");
			conn.rollback();
		}
		return ret;
	}

	private void prepareTransferTrans(TransferReqV2Data data) {

		data.setMpokey(data.getRefKey());
		data.setStatus(Constant.getrequest);
		data.setFromreqdatetime(Geneal.getCurrentDateYYYYMMDDHHMMSS());
	}

	public Response reverseForPaymentUtility(String fromAcc, String accNumber, String userID, double amount,
			String reference, Connection conn) throws SQLException {
		Response ret = new Response();
		AccountDao l_AccDao = new AccountDao();
		String isTrOrRev = "TR";
		// make transaction
		int transRef = 0;
		String crRemark = "";
		String drRemark = "";
		String drDesc = "Reversed, Utiltiy Payment";
		String crDesc = "Reversed, Utiltiy Payment";
		conn.setAutoCommit(false);
		ret = makeTransaction(fromAcc, 0, accNumber, 0, amount, 0, 1, "", isTrOrRev, drRemark, crRemark, "", "", "",
				drDesc, crDesc, conn);
		if (ret.getMessageCode().equals("0000")) {
			transRef = Integer.parseInt(ret.getTrnRefNo());
			ret = makeGLTransaction(fromAcc, 0, accNumber, 0, amount, transRef, 1, conn);
		}
		if (ret.getMessageCode().equals("0000")) {
			if (l_AccDao.updateCrCurrentBal(accNumber, amount, GeneralUtil.getDateYYYYMMDD(),
					GeneralUtil.getDateYYYYMMDD(), conn)) {
				if (ret.getMessageCode().equals("0000")) {
					ret.setTrnRefNo(String.valueOf(transRef));
					ret.setMessageCode("0000");
					ret.setMessageDesc("Reversed successfully.");
					conn.commit();
				} else {
					ret.setMessageCode("0014");
					ret.setMessageDesc("Reversed failed.");
					conn.rollback();
				}
			} else {
				ret.setMessageCode("0014");
				ret.setMessageDesc("Reversed failed.");
				conn.rollback();
			}
		} else {
			ret.setMessageCode("0014");
			ret.setMessageDesc("Reversed failed.");
			conn.rollback();
		}
		return ret;
	}

	public TransferResData saveTransferTrans(TransferReqV2Data data) {

		TransferResData res = new TransferResData();
		TransferDao dao = new TransferDao();
		Connection conn = null;
		TransferMgr dbmgr = new TransferMgr();
		try {
			conn = ConnAdmin.getConn("001", "");
			res = dbmgr.getRefkey();
			if (res.getCode().equals("0000")) {
				data.setMpokey(res.getRefKey());
			} else {
				res.setCode("0014");
			}
			prepareTransferTrans(data);
			if (dao.saveTransferTrans(data, conn)) {
				res.setCode("0000");
			} else {
				res.setCode("0014");
			}
		} catch (Exception e) {

			e.printStackTrace();
			res.setCode("0014");
			res.setDesc(e.getMessage());
		} finally {

			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}

			} catch (SQLException e) {

				e.printStackTrace();
				res.setCode("0014");
				res.setDesc(e.getMessage());
			}
		}

		return res;
	}

	public Response topupWallet(String fromAcc, String accNumber, String userID, double amount, String reference,
			Connection conn) throws SQLException {
		Response ret = new Response();
		AccountDao l_AccDao = new AccountDao();
		EPIXResponse epiRes = new EPIXResponse();
		String isTrOrRev = "TR";
		// make transaction
		int transRef = 0;
		String crRemark = "";
		String drRemark = "";
		String drDesc = "Wallet Topup";
		String crDesc = "Wallet Topup";
		conn.setAutoCommit(false);
		ret = makeTransaction(fromAcc, 0, accNumber, 0, amount, 0, 1, "", isTrOrRev, drRemark, crRemark, "", "", "",
				drDesc, crDesc, conn);
		if (ret.getMessageCode().equals("0000")) {
			transRef = Integer.parseInt(ret.getTrnRefNo());
			ret = makeGLTransaction(fromAcc, 0, accNumber, 0, amount, transRef, 1, conn);
		}
		if (ret.getMessageCode().equals("0000")) {
			if (l_AccDao.updateCrCurrentBal(accNumber, amount, GeneralUtil.getDateYYYYMMDD(),
					GeneralUtil.getDateYYYYMMDD(), conn)) {
				if (ret.getMessageCode().equals("0000")) {
					ret.setTrnRefNo(String.valueOf(transRef));
					ret.setMessageCode("0000");
					ret.setMessageDesc("Topup successfully.");
					conn.commit();
				} else {
					ret.setMessageCode("0014");
					ret.setMessageDesc("Topup failed.");
					conn.rollback();
				}
			} else {
				ret.setMessageCode("0014");
				ret.setMessageDesc("Topup failed.");
				conn.rollback();
			}
		} else {
			ret.setMessageCode("0014");
			ret.setMessageDesc("Topup failed.");
			conn.rollback();
		}
		return ret;
	}

	public Response transDeposit(String accNumber, String userID, double amount, String reference, Connection conn)
			throws SQLException {
		Response ret = new Response();
		AccountDao l_AccDao = new AccountDao();
		String isTrOrRev = "TR";
		// make transaction
		long transRef = 0;
		conn.setAutoCommit(false);
		String crRemark = "Topup to " + userID;

		ret = makeCashDepositTransaction("", 0, accNumber, 0, amount, 0, 0, "", isTrOrRev, "", crRemark, "", "", "",
				conn);

		if (ret.getMessageCode().equals("0000")) {
			transRef = Long.valueOf(ret.getTrnRefNo());
			ret = makeCashDepositGLTransaction("", 0, accNumber, 0, amount, transRef, 0, conn);
		}
		if (ret.getMessageCode().equals("0000")) {
			if (l_AccDao.updateCrCurrentBal(accNumber, amount, GeneralUtil.getDateYYYYMMDD(),
					GeneralUtil.getDateYYYYMMDD(), conn)) {
				if (ret.getMessageCode().equals("0000")) {
					/*
					 * String notiMessageCr = "You have received "
					 * +GeneralUtil.formatNumber(amount)+" Ks from +" +
					 * reqData.getPayeeID() +" with Ref No. "+transRef+"."; new
					 * WalletTransactionDao().save(updateWalletDataCr(reqData,
					 * transRef, notiMessageCr), conn);
					 * ret.setBankRefNo(transRef);
					 */
					// ret.setTransDate(GeneralUtil.mobiledateformat(GeneralUtil.datetoString()));
					ret.setMessageCode("0000");
					ret.setMessageDesc("Topup successfully.");
					conn.commit();
				} else {
					ret.setMessageCode("0014");
					ret.setMessageDesc("Topup failed.");
					conn.rollback();
				}
			} else {
				ret.setMessageCode("0014");
				ret.setMessageDesc("Topup failed.");
				conn.rollback();
			}
		} else {
			ret.setMessageCode("0014");
			ret.setMessageDesc("Topup failed.");
			conn.rollback();
		}
		return ret;
	}

	public TransferConfirmResData transferconfirm(TransferReqV2Data req) {
		Connection conn = null;
		TransferConfirmResData res = new TransferConfirmResData();
		DAOManager l_DAOMgr = new DAOManager();
		WalletTransactionDao dao = new WalletTransactionDao();
		try {
			conn = l_DAOMgr.openConnection();
			String userID = dao.getUserByTransID(req.getTransID(), conn);
			if (!userID.equalsIgnoreCase("")) {
				if (dao.update(req.getTransID(), req.getMpokey(), req.getRefKey(), userID, req.getToname(), conn)) {
					res.setTransid(req.getTransID());
					res.setCode("0000");
					res.setDesc("Received transaction completed");
				}
			} else {
				res.setTransid(req.getTransID());
				res.setCode("0014");
				res.setDesc("Not found Transaction ID!");
			}

		} catch (Exception e) {
			e.printStackTrace();
			res.setCode("0014");
			res.setDesc("Server error.");
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					res.setCode("0014");
					res.setDesc("Server error.");
				}
			}
		}
		return res;
	}

	public TransferResData transferIn(TransferReqV2Data req) {
		Connection conn = null;
		Connection conn2 = null;
		Response ret = new Response();
		TransferResData res = new TransferResData();
		AccountData l_CrAccData = new AccountData();
		DAOManager l_DAOMgr = new DAOManager();
		AccountDao l_AccDao = new AccountDao();
		String transRef = "";
		String crWAccNo = "";
		String crName = "";
		try {
			conn = l_DAOMgr.openConnection();
			// get Account / GL by MerchantID and prev
			crWAccNo = new WJunctionDao().getDataByID(req.getToaccount(), conn).getAccNumber();
			if (req.getToname().equals("")) {
				crName = new WJunctionDao().getNameByID(req.getToaccount(), conn);
			} else {
				crName = req.getToname();
			}
			if (crWAccNo.equals("")) {
				res.setCode("0020");
				res.setDesc("Beneficiary wallet account doesn't exist.");
			} else {
				ret.setMessageCode("0000");
			}
			conn2 = l_DAOMgr.openICBSConnection();
			String isTrOrRev = "TR";
			// make transaction
			String drRemark = "Transfer to " + req.getToaccount() + " ( " + GeneralUtil.getAppName(req.getTooperator())
					+ " )";
			String crRemark = "Transfer from " + req.getFromaccount() + "( "
					+ GeneralUtil.getAppName(req.getFromoperator()) + " )";
			String notiMessage = "You have received " + GeneralUtil.formatNumber(req.getAmount()) + " Ks from +"
					+ req.getFromaccount() + " via " + GeneralUtil.getAppName(req.getFromoperator()) + " with Ref No. "
					+ req.getTransID() + ".";
			if (ret.getMessageCode().equals("0000")) {
				String drDesc = "Wallet Transfer";
				String crDesc = "Wallet Transfer";
				ret = makeTransaction("3898001", 0, crWAccNo, l_CrAccData.getCurrentBalance(), req.getAmount(), 0, 1,
						req.getTransID(), isTrOrRev, drRemark, crRemark, "", "", "", drDesc, crDesc, conn2);
			} else {
				res.setCode("0014");
				res.setDesc("Credit Transfer failed");
			}
			if (ret.getMessageCode().equals("0000")) {
				transRef = ret.getTrnRefNo();
				ret = makeGLTransaction("3898001", 0, crWAccNo, l_CrAccData.getCurrentBalance(), req.getAmount(),
						Integer.parseInt(ret.getTrnRefNo()), 1, conn2);
			} else {
				res.setCode("0014");
				res.setDesc("Credit Transfer failed");
			}
			if (ret.getMessageCode().equals("0000")) {
				if (l_AccDao.updateCrCurrentBal(crWAccNo, req.getAmount(), GeneralUtil.getDateYYYYMMDD(),
						GeneralUtil.getDateYYYYMMDD(), conn2)) {
					if (ret.getMessageCode().equals("0000")) {
						if (new WalletTransactionDao().save(updateWalletData(req, req.getTransID(), notiMessage),
								conn)) {
							res.setTransID(req.getTransID());
							res.setRefKey(transRef);
							res.setCode("0000");
							res.setT1(crName);
							res.setT2("");
							res.setT3("");
							res.setDesc("Credit Transferred successfully");
						}
					} else {
						res.setCode("0014");
						res.setDesc("Credit Transfer failed");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			res.setCode("0014");
			res.setDesc("Server error.");
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					res.setCode("0014");
					res.setDesc("Server error.");
				}
			}
			if (conn2 != null) {
				try {
					if (!conn2.isClosed())
						conn2.close();
				} catch (SQLException e) {
					res.setCode("0014");
					res.setDesc("Server error.");
				}
			}
		}
		return res;
	}

	public TransferOutRes transferOut(TransferOutReq reqData) {
		Connection conn = null;
		Connection conn2 = null;
		TransferOutRes ret = new TransferOutRes();
		AccountData l_DrAccData = new AccountData();
		Response res = new Response();
		DAOManager l_DAOMgr = new DAOManager();
		AccountDao l_AccDao = new AccountDao();
		EPIXResponse epiRes = new EPIXResponse();
		String transRef = "";
		GeneralUtil.readFCServiceSetting();
		try {
			conn = l_DAOMgr.openConnection();
			conn2 = l_DAOMgr.openICBSConnection();
			if (conn != null) {
				// get Debit Info
				String drWAccNo = new WJunctionDao().getDataByID(reqData.getUserID(), conn).getAccNumber();
				String crWAccNo = new WJunctionDao().getDataByID(reqData.getBeneficiaryID(), conn).getAccNumber();
				l_DrAccData = l_AccDao.getAccountByAccNo(drWAccNo, conn2);
				if (l_DrAccData.getAccountNumber().equals("")) {
					ret.setCode("0019");
					ret.setDesc("Payee doesn't exist.");
					return ret;
				}
				epiRes = checkTransLimit(drWAccNo, "", reqData.getAmount());
				if (!epiRes.getCode().equals("0000")) {
					ret.setCode(epiRes.getCode());
					ret.setDesc(epiRes.getDesc());
					return ret;
				}
				if (l_AccDao.getBalanceByIDV2(drWAccNo, conn2) < reqData.getAmount()) {
					ret.setCode("0014");
					ret.setDesc("Incifficient balance!");
					return ret;
				}

				String isTrOrRev = "TR";
				// make transaction
				String drRemark = "Transfer to " + reqData.getBeneficiaryID() + " ( "
						+ GeneralUtil.getAppName(reqData.getToInstitutionCode()) + " )";
				String crRemark = "Transfer from " + reqData.getUserID() + "( "
						+ GeneralUtil.getAppName(reqData.getFromInstitutionCode()) + " )";
				String drDesc = "Wallet Transfer";
				String crDesc = "Wallet Transfer";
				res = makeTransaction(l_DrAccData.getAccountNumber(), l_DrAccData.getCurrentBalance(), "3898001", 0,
						reqData.getAmount(), 0, 1, reqData.getRemark(), isTrOrRev, drRemark, crRemark, "", "", "",
						drDesc, crDesc, conn2);
				if (res.getMessageCode().equals("0000")) {
					transRef = res.getTrnRefNo();
					res = makeGLTransaction(l_DrAccData.getAccountNumber(), l_DrAccData.getCurrentBalance(), "3898001",
							0, reqData.getAmount(), Integer.parseInt(res.getTrnRefNo()), 1, conn2);
				}
				if (res.getMessageCode().equals("0000")) {
					if (l_AccDao.updateDrCurrentBal(l_DrAccData.getAccountNumber(), reqData.getAmount(),
							GeneralUtil.getDateYYYYMMDD(), GeneralUtil.getDateYYYYMMDD(), conn2)) {
						if (res.getMessageCode().equals("0000")) {
							/*
							 * MPOSoapServiceProxy proxy = new
							 * MPOSoapServiceProxy();
							 * proxy.setEndpoint(com.nirvasoft.rp.util.
							 * ServerGlobal.getMPOServiceURL());
							 * com.nirvasoft.mpo.service.TransferResData
							 * mpoResData = new
							 * com.nirvasoft.mpo.service.TransferResData();
							 * mpoResData = proxy.transfer("MPO123456",
							 * transRef, reqData.getFromInstitutionCode(),
							 * reqData.getPayeeID(), "1", reqData.getFromName(),
							 * reqData.getToInstitutionCode(),
							 * reqData.getBeneficiaryID(), "1",
							 * reqData.getToName(), reqData.getAmount(),
							 * reqData.getBankCharges(), reqData.getRemark(),
							 * "", "", ""); if
							 * (mpoResData.getCode().equals("0000")) { //
							 * TransferResData wResData = //
							 * saveTransferTrans(reqData); String notiMessage =
							 * "Your transaction with Ref No. " + transRef +
							 * " is completed."; new
							 * WalletTransactionDao().save(updateWalletDataDr(
							 * reqData, transRef, notiMessage), conn);
							 * ret.setBankRefNo(transRef);
							 * ret.setTransDate(GeneralUtil.mobiledateformat(
							 * GeneralUtil.datetoString()));
							 * ret.setCode("0000"); ret.setDesc(
							 * "Transfered successfully"); } else {
							 * ret.setCode(mpoResData.getCode()); ret.setDesc(
							 * "Server Error!"); }
							 * 
							 */
							ret.setBankRefNo(transRef);
							ret.setTransDate(GeneralUtil.mobiledateformat(GeneralUtil.datetoString()));
							ret.setDesc("Transfered successfully");
							ret.setCode("0000");
						} else {
							ret.setCode(res.getMessageCode());
							ret.setDesc("Server Error!");
						}
					}
				}
			} else {
				ret.setCode("0015");
				ret.setDesc("Connection error.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			ret.setCode("0014");
			ret.setDesc("Server error.");
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					ret.setCode("0014");
					ret.setDesc("Server error.");
				}
			}
			if (conn2 != null) {
				try {
					if (!conn2.isClosed())
						conn2.close();
				} catch (SQLException e) {
					ret.setCode("0014");
					ret.setDesc("Server error.");
				}
			}
		}
		return ret;
	}
	public String goFromAccount(String userid,Connection pConn){
		String fromAccount="";		
		AccountDao l_AccDao = new AccountDao();
		try {			
				fromAccount=l_AccDao.getFromAccountNumber(userid,pConn);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return fromAccount;
		
	}
	public String goToName(String userid,Connection pConn){
		String fromAccount="";		
		AccountDao l_AccDao = new AccountDao();			
		try {			
			
				fromAccount=l_AccDao.goToName(userid,pConn);				
			
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return fromAccount;
		
	}
	public String goToAccount(String merchantid,Connection pConn){
		String toaccount="";		
		AccountDao l_AccDao = new AccountDao();		
		try {			
			
			toaccount=l_AccDao.getFromAccountNumber(merchantid,pConn);			
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return toaccount;
		
	}

	public TransferOutRes transferOutInternal(TransferOutReq reqData) {
		Connection conn = null;
		Connection conn2 = null;
		TransferOutRes ret = new TransferOutRes();
		AccountData l_DrAccData = new AccountData();
		AccountData l_CrAccData = new AccountData();
		Response res = new Response();
		DAOManager l_DAOMgr = new DAOManager();
		AccountDao l_AccDao = new AccountDao();
		String transRef = "";
		GeneralUtil.readFCServiceSetting();
		EPIXResponse epiRes = new EPIXResponse();
		try {
			conn = l_DAOMgr.openConnection();
			conn2 = l_DAOMgr.openICBSConnection();
			if (conn != null) {
				// get Debit Info
				String drWAccNo = new WJunctionDao().getDataByID(reqData.getUserID(), conn).getAccNumber();
				String crWAccNo = new WJunctionDao().getDataByID(reqData.getBeneficiaryID(), conn).getAccNumber();
				l_DrAccData = l_AccDao.getAccountByAccNo(drWAccNo, conn2);
				l_CrAccData = l_AccDao.getAccountByAccNo(crWAccNo, conn2);
				if (l_DrAccData.getAccountNumber().equals("")) {
					ret.setCode("0019");
					ret.setDesc("Sender doesn't exist.");
					return ret;
				} else if (crWAccNo.equals("")) {
					ret.setCode("0019");
					ret.setDesc("Beneficiary doesn't exist.");
					return ret;
				}
				epiRes = checkTransLimit(drWAccNo, "", reqData.getAmount());
				if (!epiRes.getCode().equals("0000")) {
					ret.setCode(epiRes.getCode());
					ret.setDesc(epiRes.getDesc());
					return ret;
				}
				if (l_AccDao.getBalanceByIDV2(drWAccNo, conn2) < reqData.getAmount()) {
					ret.setCode("0014");
					ret.setDesc("Incifficient balance!");
					return ret;
				}
				String isTrOrRev = "TR";
				// make transaction
				String drRemark = "Transfer to " + reqData.getBeneficiaryID();
				String crRemark = "Transfer from " + reqData.getUserID();
				String drDesc = "Transfer";
				String crDesc = "Transfer";
				conn2.setAutoCommit(false);
				res = makeTransaction(l_DrAccData.getAccountNumber(), l_DrAccData.getCurrentBalance(), crWAccNo,
						l_CrAccData.getCurrentBalance(), reqData.getAmount(), 0, 1, reqData.getRemark(), isTrOrRev,
						drRemark, crRemark, "", "", "", drDesc, crDesc, conn2);
				if (res.getMessageCode().equals("0000")) {
					transRef = res.getTrnRefNo();
					res = makeGLTransaction(l_DrAccData.getAccountNumber(), l_DrAccData.getCurrentBalance(), crWAccNo,
							l_CrAccData.getCurrentBalance(), reqData.getAmount(), Integer.parseInt(res.getTrnRefNo()),
							1, conn2);
				}
				if (res.getMessageCode().equals("0000")) {
					if (l_AccDao.updateDrCurrentBal(l_DrAccData.getAccountNumber(), reqData.getAmount(),
							GeneralUtil.getDateYYYYMMDD(), GeneralUtil.getDateYYYYMMDD(), conn2)) {
						if (l_AccDao.updateCrCurrentBal(crWAccNo, reqData.getAmount(), GeneralUtil.getDateYYYYMMDD(),
								GeneralUtil.getDateYYYYMMDD(), conn2)) {
							if (res.getMessageCode().equals("0000")) {
								// String notiMessageCr = "You have received "
								// +
								// GeneralUtil.formatNumber(reqData.getAmount())
								// + " Ks from +"
								// + reqData.getUserID() + " with Ref No. " +
								// transRef + ".";
								String notiMessage = "Transferred " + GeneralUtil.formatNumber(reqData.getAmount())
										+ " Ks" + " to " + reqData.getToName() + " (" + reqData.getBeneficiaryID()
										+ ") with Ref No. " + transRef + ".";

								new WalletTransactionDao().save(updateWalletDataDr(reqData, transRef, notiMessage, "1"),
										conn);
								ret.setBankRefNo(transRef);
								ret.setTransDate(GeneralUtil.mobiledateformat(GeneralUtil.datetoString()));
								ret.setCode("0000");
								ret.setDesc("Transfered successfully");
								conn2.commit();
								String transferMessage = "Transferred " + GeneralUtil.formatNumber(reqData.getAmount())
										+ " Ks from " + reqData.getFromName() + " (" + reqData.getUserID() + ") to "
										+ reqData.getToName() + " (" + reqData.getBeneficiaryID()
										+ ") with Reference : " + reqData.getRemark() + " and Trans No : " + transRef
										+ ".";
								ret.setCode("0000");
								ret.setDesc("Transfered successfully");

							} else {
								ret.setCode(res.getMessageCode());
								ret.setDesc("Server Error!");
								conn2.rollback();
							}
						}
					}
				} else {
					ret.setCode(res.getMessageCode());
					ret.setDesc("Server Error!");
					conn2.rollback();
				}
			} else {
				ret.setCode("0015");
				ret.setDesc("Connection error.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			ret.setCode("0014");
			ret.setDesc("Server error.");
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					ret.setCode("0014");
					ret.setDesc("Server error.");
				}
			}
			if (conn2 != null) {
				try {
					if (!conn2.isClosed())
						conn2.close();
				} catch (SQLException e) {
					ret.setCode("0014");
					ret.setDesc("Server error.");
				}
			}
		}
		return ret;
	}

	private ChatData updateChatData(TransferOutReq reqData, String refKey, String transferMessage) {
		ChatData ret = new ChatData();
		ret.setUserId(reqData.getUserID());
		ret.setUserName(reqData.getFromName());
		ret.setT5(reqData.getsKey());// from
		ret.setT6(reqData.getTosKey());// to
		ret.setT9(reqData.getTosKey());
		ret.setT2(transferMessage);
		ret.setT19("Users");
		return ret;
	}

	private WalletTransaction updateWalletData(TransferReqV2Data req, String refKey, String notiMessage) {
		WalletTransaction ret = new WalletTransaction();
		ret.setNotiMessage(notiMessage);
		// ret.setUserID(req.getToaccount()); //change
		ret.setUserID(req.getFromaccount());
		ret.setFromAccount(req.getFromaccount());
		ret.setToAccount(req.getToaccount());
		ret.setFromOperator(req.getFromoperator());
		ret.setToOperator(req.getTooperator());
		ret.setTransID(req.getTransID());
		ret.setSwitchKey(req.getMpokey());
		ret.setRefKey(req.getRefKey());
		ret.setAmount(req.getAmount());
		ret.setCode("0000");
		ret.setDesc("Credit Transferred successfully");
		ret.setTransDate(GeneralUtil.datetoString());
		ret.setTransType("C");
		ret.setTransStatus(2);
		return ret;
	}

	public WalletTransaction updateWalletDataCr(TransferOutReq req, String refKey, String notiMessage,
			String transType) {
		WalletTransaction ret = new WalletTransaction();
		ret.setCreatedDate(GeneralUtil.datetoString() + " " + GeneralUtil.getTime());
		ret.setModifiedDate(GeneralUtil.datetoString() + " " + GeneralUtil.getTime());
		ret.setNotiMessage(notiMessage);
		ret.setUserID(req.getBeneficiaryID());
		ret.setFromAccount(req.getPayeeID());
		ret.setToAccount(req.getBeneficiaryID());
		ret.setFromOperator(req.getFromInstitutionCode());
		ret.setToOperator(req.getToInstitutionCode());
		ret.setTransID(refKey);
		ret.setSwitchKey("");
		ret.setRefKey(req.getRefNo());
		ret.setAmount(req.getAmount());
		ret.setCode("0000");
		ret.setDesc("Transferred successfully");
		ret.setTransDate(GeneralUtil.datetoString());
		ret.setTransType(transType);
		ret.setTransStatus(2);
		ret.setT1(req.getFromName());
		ret.setT2(req.getToName());
		return ret;
	}
	public WalletTransaction updateWalletDataCredit(AccTransferReq req,  String refKey,String transdate, String notiMessage,
			String transType,String effectiveDate) {
		WalletTransaction ret = new WalletTransaction();
		ret.setCreatedDate(GeneralUtil.datetoString() + " " + GeneralUtil.getTime());
		ret.setModifiedDate(GeneralUtil.datetoString() + " " + GeneralUtil.getTime());
		ret.setNotiMessage(notiMessage);
		ret.setUserID(req.getSenderCode());
		ret.setFromAccount(req.getFromAccount());
		ret.setToAccount(req.getToAccount());
		ret.setTransID(refKey);
		ret.setCommissionCharges(req.getComm());
		ret.setSwitchKey("");
		ret.setRefKey(req.getRefNo());
		ret.setAmount(Double.parseDouble(req.getAmount()));
		ret.setCode("0000");
		ret.setDesc("Transferred successfully");
		ret.setTransDate(transdate);
		ret.setTransType(transType);
		ret.setTransStatus(2);
		ret.setT1(req.getFromName());
		ret.setT2(req.getToName());
		//ret.setT2(req.getCusName());
		ret.setMerchantID(req.getMerchantID());//start
		ret.setEffectiveDate(effectiveDate);
		//ret.setTransTime(GeneralUtil.datetoString() + " " + GeneralUtil.getTime());
		ret.setToDescription(req.getTaxDesc());
		ret.setDueDate(req.getDueDate());
		ret.setOtherStatus("");
		//ret.setPendltyaccount("");
		//ret.setPendltyamount(req.getPenalty());
		//ret.setDiscountAmount("");
		return ret;
	}

	public WalletTransaction updateWalletDataDr(TransferOutReq req, String refKey, String notiMessage) {
		WalletTransaction ret = new WalletTransaction();
		ret.setNotiMessage(notiMessage);
		// ret.setUserID(req.getPayeeID()); //change
		ret.setUserID(req.getUserID());
		ret.setFromAccount(req.getPayeeID());
		ret.setToAccount(req.getBeneficiaryID());
		ret.setFromOperator(req.getFromInstitutionCode());
		ret.setToOperator(req.getToInstitutionCode());
		ret.setTransID(refKey);
		ret.setSwitchKey("");
		ret.setRefKey("");
		ret.setAmount(req.getAmount());
		ret.setCode("0000");
		ret.setDesc("Debit Transferred successfully");
		ret.setTransDate(GeneralUtil.datetoString());
		ret.setTransType("D");
		ret.setTransStatus(1);
		ret.setT1(req.getFromName());
		ret.setT2(req.getToName());

		return ret;
	}

	public WalletTransaction updateWalletDataDr(TransferOutReq req, String refKey, String notiMessage,
			String transType) {
		WalletTransaction ret = new WalletTransaction();
		ret.setCreatedDate(GeneralUtil.datetoString() + " " + GeneralUtil.getTime());
		ret.setModifiedDate(GeneralUtil.datetoString() + " " + GeneralUtil.getTime());
		ret.setNotiMessage(notiMessage);
		ret.setUserID(req.getUserID());
		ret.setFromAccount(req.getPayeeID());
		ret.setToAccount(req.getBeneficiaryID());
		ret.setFromOperator(req.getFromInstitutionCode());
		ret.setToOperator(req.getToInstitutionCode());
		ret.setTransID(refKey);
		ret.setSwitchKey("");
		ret.setRefKey(req.getRefNo());
		ret.setAmount(req.getAmount());
		ret.setCode("0000");
		ret.setDesc("Transferred successfully");
		ret.setTransDate(GeneralUtil.datetoString());
		ret.setTransType(transType);
		ret.setTransStatus(2);
		ret.setT1(req.getFromName());
		ret.setT2(req.getToName());
		return ret;
	}

	public Response payment(String fromAcc, String toAcc, String userID, double amount, String paymentType,
			double preBalCustomer, double preBalMerchant, boolean isMerchantAccountGL, Connection conn) throws SQLException {
		Response ret = new Response();
		AccountDao l_AccDao = new AccountDao();
		String isTrOrRev = "TR";
		// make transaction
		int transRef = 0;
		String crRemark = "";
		String drRemark = "";
		String drDesc = paymentType;
		String crDesc = paymentType;
		conn.setAutoCommit(false);
		ret = makeTransaction(fromAcc, preBalCustomer, toAcc, preBalMerchant, amount, 0, 1, "", isTrOrRev, drRemark, crRemark, "", "",
				"", drDesc, crDesc, conn);
		if (ret.getMessageCode().equals("0000")) {
			transRef = Integer.parseInt(ret.getTrnRefNo());
			ret = makeGLTransactionMerchantPayment(fromAcc, preBalCustomer, toAcc, preBalMerchant, amount, transRef, 1, isMerchantAccountGL, conn);
		}
		if (ret.getMessageCode().equals("0000")) {
			if (l_AccDao.updateDrCurrentBal(fromAcc, amount, GeneralUtil.getDateYYYYMMDD(),
					GeneralUtil.getDateYYYYMMDD(), conn)) {
				if (ret.getMessageCode().equals("0000")) {
					ret.setTrnRefNo(String.valueOf(transRef));
					ret.setMessageCode("0000");
					ret.setMessageDesc("Payment successful.");
					conn.commit();
				} else {
					ret.setMessageCode("0014");
					ret.setMessageDesc("Payment failed.");
					conn.rollback();
				}
			} else {
				ret.setMessageCode("0014");
				ret.setMessageDesc("Payment failed.");
				conn.rollback();
			}
		} else {
			ret.setMessageCode("0014");
			ret.setMessageDesc("Payment failed.");
			conn.rollback();
		}
		return ret;
	}

	private Response makeGLTransactionMerchantPayment(String drAccNo, double drPrevBal, String crAccNo,
			double crPrevBal, double amount, int transRef, int status, boolean isMerchantAccountGL, Connection conn) throws SQLException {
		Response ret = new Response();
		AccountGLTransactionData data = new AccountGLTransactionData();
		ArrayList<AccountGLTransactionData> dataList = new ArrayList<AccountGLTransactionData>();
		AccountGLTransactionDao dao = new AccountGLTransactionDao();
		// prepare Dr Info
		if (!(drAccNo.equals(""))) {
			data.setTransNo(transRef);
			data.setAccNumber(drAccNo);
			data.setGLCode1("L050101001001001");
			data.setGLCode2("A010101001001001");
			data.setGLCode3("");
			data.setGLCode4("");
			data.setGLCode5("");
			data.setBaseCurCode("MMK");
			data.setBaseCurOperator("/");
			data.setBaseRate(1);
			data.setBaseAmount(amount);
			data.setMediumCurCode("");
			data.setMediumCurOperator("");
			data.setMediumRate(0);
			data.setMediumAmount(0);
			data.setTrCurCode("MMK");
			data.setTrCurOperator("");
			data.setTrRate(1);
			data.setTrAmount(amount);
			data.setTrPrevBalance(drPrevBal);
			data.setN1(status);
			data.setN2(0);
			data.setN3(0);
			data.setT1("");
			data.setT2("");
			data.setT3("");
			dataList.add(data);
		}
		// prepare Cr Info
		if (!(crAccNo.equals(""))) {
			data = new AccountGLTransactionData();
			data.setTransNo(transRef + 1);
			data.setAccNumber(crAccNo);
			if (isMerchantAccountGL) {
				data.setGLCode1("");
				data.setGLCode2("A010101001001001");
			}else{
				data.setGLCode1("L050101001001001");
				data.setGLCode2("L050101001001001");
			}
			data.setGLCode3("");
			data.setGLCode4("");
			data.setGLCode5("");
			data.setBaseCurCode("MMK");
			data.setBaseCurOperator("/");
			data.setBaseRate(1);
			data.setBaseAmount(amount);
			data.setMediumCurCode("");
			data.setMediumCurOperator("");
			data.setMediumRate(0);
			data.setMediumAmount(0);
			data.setTrCurCode("MMK");
			data.setTrCurOperator("");
			data.setTrRate(1);
			data.setTrAmount(amount);
			data.setTrPrevBalance(crPrevBal);
			data.setN1(status);
			data.setN2(0);
			data.setN3(0);
			data.setT1("");
			data.setT2("");
			data.setT3("");
			dataList.add(data);
		}
		if (dao.save(dataList, conn)) {
			ret.setMessageCode("0000");
			ret.setMessageDesc("AccountGLTransaction saved successfully.");
		} else {
			ret.setMessageCode("0018");
			ret.setMessageDesc("AccountGLTransaction saved fail.");
		}
		return ret;
	}

}
