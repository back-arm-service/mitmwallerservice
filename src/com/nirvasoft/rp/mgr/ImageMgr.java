package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.dao.ImageDao;
import com.nirvasoft.rp.shared.ImageData;
import com.nirvasoft.rp.shared.ImageRequest;
import com.nirvasoft.rp.shared.ImageResponse;

public class ImageMgr {

	public ImageResponse getAllImage(ImageRequest req) {
		ImageResponse res = new ImageResponse();
		DAOManager dao = new DAOManager();
		Connection conn = null;
		ImageDao daoDoc = new ImageDao();
		List<ImageData> imageList = null;

		try {
			conn = dao.openConnection();
			imageList = daoDoc.getAllImage(req, conn);
		} catch (Exception e) {
			imageList = null;

			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				imageList = null;

				e.printStackTrace();
			}
		}

		if (imageList != null && imageList.size() != 0) {
			ImageData imgListArr[] = new ImageData[imageList.size()];

			for (int i = 0; i < imageList.size(); i++) {
				ImageData doc = new ImageData();

				doc.setSyskey(imageList.get(i).getSyskey());
				doc.setCreatedDate(imageList.get(i).getCreatedDate());
				doc.setUserid(imageList.get(i).getUserid());
				doc.setImageName(imageList.get(i).getImageName());
				doc.setLatitude(imageList.get(i).getLatitude());
				doc.setLongitude(imageList.get(i).getLongitude());
				doc.setLocation(imageList.get(i).getLocation());
				doc.setImagePath(imageList.get(i).getImagePath());
				doc.setT1(imageList.get(i).getT1());
				doc.setT2(imageList.get(i).getT2());
				doc.setT3(imageList.get(i).getT3());
				doc.setT4(imageList.get(i).getT4());
				doc.setT5(imageList.get(i).getT5());
				doc.setN1(imageList.get(i).getN1());
				doc.setN2(imageList.get(i).getN2());
				doc.setN3(imageList.get(i).getN3());
				doc.setN4(imageList.get(i).getN4());
				doc.setN5(imageList.get(i).getN5());

				imgListArr[i] = doc;
			}

			res.setCode("0000");
			res.setDesc("Getting All Image List Success.");
			res.setImageList(imgListArr);
		} else {
			res.setCode("0014");
			res.setDesc("No Pdf Found!");
		}

		return res;
	}

	public ImageResponse insertImage(ImageData req) {
		ImageDao l_dao = new ImageDao();
		ImageResponse ret = new ImageResponse();

		try {
			DAOManager dao = new DAOManager();
			Connection conn = dao.openConnection();
			ret = l_dao.insertImage(req, conn);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ret.setCode("0014");
			ret.setDesc(e.getMessage());
		}
		return ret;
	}

}
