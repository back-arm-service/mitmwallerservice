package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.dao.LOVDetailsDao;
import com.nirvasoft.rp.shared.AccountTypeListResponseData;

public class LOVDetailsMgr {

	public AccountTypeListResponseData getAccountTypeList() {
		AccountTypeListResponseData responseData = null;

		DAOManager daoDB = new DAOManager();
		Connection conn1 = null;

		LOVDetailsDao dao = new LOVDetailsDao();

		try {
			conn1 = daoDB.openConnection();

			responseData = dao.getAccountTypeList(conn1);
		} catch (Exception e) {
			e.printStackTrace();

			responseData = new AccountTypeListResponseData();
			responseData.setCode("0014");
			responseData.setDesc(e.getMessage());
		} finally {
			try {
				if (conn1 != null) {
					if (!conn1.isClosed()) {
						conn1.close();
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();

				responseData = new AccountTypeListResponseData();
				responseData.setCode("0014");
				responseData.setDesc(e.getMessage());
			}
		}

		return responseData;
	}

}
