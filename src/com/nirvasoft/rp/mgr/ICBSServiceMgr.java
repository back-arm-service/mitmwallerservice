
package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.util.ArrayList;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.data.CommRes;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.mgr.icbs.DBRateSetupMgr;
import com.nirvasoft.rp.mgr.icbs.DBTransactionMgr;
import com.nirvasoft.rp.shared.AccTransferReq;
import com.nirvasoft.rp.shared.AccountGLTransactionData;
import com.nirvasoft.rp.shared.DataResult;
import com.nirvasoft.rp.shared.TransactionData;
import com.nirvasoft.rp.shared.icbs.RateAmountRangeSetupData;
import com.nirvasoft.rp.shared.icbs.RateSetupData;
import com.nirvasoft.rp.shared.icbs.ReferenceAccountsData;
import com.nirvasoft.rp.shared.icbs.SMSReturnData;
import com.nirvasoft.rp.shared.icbs.SMSTransferData;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.GeneralUtility;
import com.nirvasoft.rp.util.ServerGlobal;
import com.nirvasoft.rp.util.SharedLogic;
import com.nirvasoft.rp.util.StrUtil;

public class ICBSServiceMgr {
	public static String AbsolutePath = "";
	private static int mTrCrTransType = 320;
	private static int mTrDrTransType = 820;
	/*private static int mIBTTrCrTransType = 321;
	private static int mIBTTrDrTransType = 821;
	private static int mBeerTrCrTransType = 327;
	private static int mBeerTrDrTransType = 827;
	private static int mDTrCrTransType = 322;
	private static int mDTrDrTransType = 822;
	private static int mETrCrTransType = 323;
	private static int mETrDrTransType = 823;
	private static int mBillTrCrTransType = 324;
	private static int mBillTrDrTransType = 824;
	private static long m_AutoPrintTransRef = 0;*/
	private static int mMobileOwnTRCom_SameBr = 12;

	private static int mMobileInterTRCom_SameBr = 13;
	private static int mMobileOwnTRCom_DiffBr_SameRegion = 14;
	private static int mMobileOwnTRCom_DiffBr_DiffRegion = 15;
	private static int mMobileInterTRCom_DiffBr_SameRegion = 16;
	private static int mMobileInterTRCom_DiffBr_DiffRegion = 17;

	public SMSReturnData postTransfer(SMSTransferData pTransferData,boolean isDrGL,boolean isCrGL,
			com.nirvasoft.rp.shared.AccountData fromAccData, com.nirvasoft.rp.shared.AccountData toAccData, 
			Connection pConn) throws Exception {
		SMSReturnData l_SMSReturnData = new SMSReturnData();
		DataResult l_DataResult = new DataResult();
		ArrayList<TransactionData> l_TransactionList = new ArrayList<TransactionData>();
		ArrayList<AccountGLTransactionData> l_AccGLList = new ArrayList<AccountGLTransactionData>();
		DAOManager l_DaoMgr = new DAOManager();
		
		ArrayList<String> l_ArlLogString = new ArrayList<String>();
		String l_EffectiveDate = "";				
		double trFee = 0;
		Connection l_Conn1 = null;
		String pByForceCheque = "";
		String l_TodayDate = GeneralUtility.getTodayDate();
		String l_TransactionTime = DBTransactionMgr.getCurrentDateYYYYMMDDHHMMSS();
		String l_TrDateTime = GeneralUtility.getDateTimeDDMMYYYYHHMMSS();
		// GeneralUtility.setAmountLimitList(GeneralUtility.readMaxTransAmountMobile());
		// // by
		// reference
		System.out.println("start openICBSConnection  "+GeneralUtil.getDateTime());		
		System.out.println("end openICBSConnection  "+GeneralUtil.getDateTime());
		
		if (pTransferData.isFinishCutOffTime()) {
			l_EffectiveDate = GeneralUtility.getTomorrowDate();
			l_EffectiveDate = DBTransactionMgr.getEffectiveTransDate(l_EffectiveDate,pConn);
		} else {
			l_EffectiveDate = DBTransactionMgr.getEffectiveTransDate(l_TodayDate,pConn);
		}		
		//l_FromAccountData.setGlStatus(l_AccDAO.getFromGL(pTransferData.getFromAccNumber(),pConn));
		if (!isDrGL) {
			if (!fromAccData.getAccountNumber().equals("")) {
				pTransferData.setFromBranchCode(fromAccData.getBranchCode());
			} else {
				l_SMSReturnData.setDescription("Invalid From Account No.");
				l_SMSReturnData.setStatus(false);
				l_SMSReturnData.setCode("0014");
				return l_SMSReturnData;
			}
		}else {			
			fromAccData.setBranchCode(ServerGlobal.getmBranchCode());;
			fromAccData.setProduct(ServerGlobal.getmProduct());
			fromAccData.setType(ServerGlobal.getmAccType());
			fromAccData.setCurrencyCode(ServerGlobal.getmCurrencyCode());
			fromAccData.setCashInHandGL(ServerGlobal.getmCashInHand());
			//fromAccData.setZone(ServerGlobal.getmZone());
			l_DataResult.setStatus(true);
		}
		
		if (l_DataResult.getStatus()) {
			//l_ToAccountData.setGlStatus(l_AccDAO.getGL(pTransferData.getToAccNumber(), pConn));
			if (!isCrGL) {				
				if (!toAccData.getAccountNumber().equals("")) {					
					pTransferData.setToBranchCode(toAccData.getBranchCode());
				} else {
					l_SMSReturnData.setDescription("Invalid To Account No.");
					l_SMSReturnData.setStatus(false);
					l_SMSReturnData.setCode("0014");
					return l_SMSReturnData;
				}
			}else {				
				toAccData.setBranchCode(ServerGlobal.getmBranchCode());;
				toAccData.setProduct(ServerGlobal.getmProduct());
				toAccData.setType(ServerGlobal.getmAccType());
				toAccData.setCurrencyCode(ServerGlobal.getmCurrencyCode());
				toAccData.setCashInHandGL(ServerGlobal.getmCashInHand());
				//toAccData.setZone(ServerGlobal.getmZone());	
			}
		} 
		pTransferData.setCurrencyCode(fromAccData.getCurrencyCode());
		pTransferData.setFromCCY(fromAccData.getCurrencyCode());		
		pTransferData.setToCCY(toAccData.getCurrencyCode());		
		try {
			/*if (l_DataResult.getStatus()) {
				l_DataResult = checkAccount(l_FromAccountData.getProduct(), l_ToAccountData.getProduct(),
						l_FromAccountData.getAccountNumber(), l_ToAccountData.getAccountNumber(), l_Conn);
			}*/
			//if (l_DataResult.getStatus()) {				

			
				// get commission
				/*trFee = getMobileTransferCharges(pTransferData.getAmount(), pTransferData.getDebitIBTKey(),
						l_FromAccountData.getBranchCode(), l_ToAccountData.getBranchCode(), l_FromAccountData.getZone(),
						l_ToAccountData.getZone(),pConn);
				pTransferData.setTransactionFee(trFee);*/
				System.out.println("end getMobileTransferCharges  "+GeneralUtil.getDateTime());
				// Preparation				
				l_TransactionList = prepareTransferSameBranchWithoutComm(pTransferData, l_TodayDate,
						l_EffectiveDate, l_ArlLogString, "", l_TransactionTime, l_TrDateTime, pByForceCheque,
						fromAccData, toAccData,isDrGL,isCrGL, pConn);
				System.out.println("end prepareTransferSameBranchWithoutComm  "+GeneralUtil.getDateTime());
				
				l_AccGLList = DBTransactionMgr.prepareAccountGLTransactions(l_TransactionList, pConn);
				System.out.println("end prepareAccountGLTransactions  "+GeneralUtil.getDateTime());

				/*l_TransactionList = DBTransactionMgr.prepareBaseCurrencys(l_TransactionList);
				System.out.println("end prepareBaseCurrencys  "+GeneralUtil.getDateTime());
			
				*/
				/*if (l_DataResult.getStatus()) {*/
					l_Conn1 = l_DaoMgr.openICBSConnection();
					System.out.println("end prepareAccountGLTransactions  "+GeneralUtil.getDateTime());
					 
					l_Conn1.setAutoCommit(false);
					l_ArlLogString.add("	Post Transaction ");

					System.out.println("start post  "+GeneralUtil.getDateTime());
					l_DataResult = com.nirvasoft.rp.core.ccbs.data.db.DBTransactionMgr.postWallet(l_TransactionList,l_AccGLList,ServerGlobal.getmMinBalSetting(),l_Conn1);
					System.out.println("end post  "+GeneralUtil.getDateTime());

					if (l_DataResult.getStatus()) {
						//l_Conn1.commit();
						l_SMSReturnData.setStatus(true);
						l_SMSReturnData.setCode("0000");
						l_SMSReturnData.setTransactionNumber(l_DataResult.getTransactionNumber());
						l_SMSReturnData.setComm1(trFee);
						l_SMSReturnData.setCcy(pTransferData.getCurrencyCode());
						l_SMSReturnData.setDescription("Posted successfully");
					} else {
						l_SMSReturnData.setStatus(false);
						l_SMSReturnData.setCode("0014");						
						l_SMSReturnData.setDescription(l_DataResult.getDescription());
					}

					
					/*}
			} else {
				l_SMSReturnData.setDescription(l_DataResult.getDescription());
				l_SMSReturnData.setStatus(false);
				l_SMSReturnData.setCode("0014");
				return l_SMSReturnData;
			}*/
		} catch (Exception e) {
			// l_Conn1.rollback();
			e.printStackTrace();
			l_SMSReturnData.setStatus(false);
			l_SMSReturnData.setDescription("Transaction Failed." + e.getMessage());
			l_ArlLogString.add("Transaction Roll Back" + l_DataResult.getDescription() + e.getMessage());
			l_SMSReturnData.setErrorCode("06");
		}finally{
			
			if(l_DataResult.getStatus()){
				l_Conn1.commit();
			}else 
				l_Conn1.rollback();
			if (!l_Conn1.isClosed() || l_Conn1 != null) {
				l_Conn1.close();
			}
				
		}
		return l_SMSReturnData;
	}
	
	private ArrayList<TransactionData> prepareTransferSameBranchWithoutComm(SMSTransferData pTransferData,
		String p_TransactionDate, String p_EffectiveDate, ArrayList<String> l_ArlLogString, String pType,
		String l_TransactionTime, String l_TrDateTime, String pByForceCheque, com.nirvasoft.rp.shared.AccountData fromAccData,
		com.nirvasoft.rp.shared.AccountData toAccData,boolean isDrGL,boolean isCrGL, Connection conn) throws Exception {
	ArrayList<TransactionData> l_TransactionList = new ArrayList<TransactionData>();
	TransactionData l_TransactionData = new TransactionData();
	//ReferenceAccountsData l_RefData = new ReferenceAccountsData();
	double drCCYRate = ServerGlobal.getmCurRate();
	double crCCYRate = ServerGlobal.getmCurRate();
	/*DBFECCurrencyRateMgr.getFECCurrencyRate(pTransferData.getToCCY(), "smTransfer",
			pTransferData.getToBranchCode(), conn)*/
	double totalfee = pTransferData.getAmount() + pTransferData.getTransactionFee();
	/*l_RefData = DBSystemMgr.getReferenceAccCode("MTRCOM" + pTransferData.getCurrencyCode()
			+ pTransferData.getFromBranchCode() + fromAccData.getProduct(), conn);
	String l_ComGL = l_RefData.getGLCode();*/

	if (totalfee > 0) {
		// Dr From Customer
		l_TransactionData = new TransactionData();
		l_TransactionData.setIsMobile(true);
		l_TransactionData.setAccountNumber(pTransferData.getFromAccNumber());		
		if(isDrGL) {
			l_TransactionData.setReferenceNumber("");
			l_TransactionData.setProductGL(fromAccData.getProductGL());	
			l_TransactionData.setProductGL(ServerGlobal.getmBranchCode());
		}else{
			if(ServerGlobal.isHasCheque())
				l_TransactionData.setReferenceNumber(pByForceCheque);
			l_TransactionData.setProductGL("");
			l_TransactionData.setPreviousBalance(fromAccData.getCurrentBalance());
			l_TransactionData.setProductType(fromAccData.getProduct());
			l_TransactionData.setAccType(fromAccData.getType());
			l_TransactionData.setAvaliableBal(fromAccData.getAvailableBalance());
			l_TransactionData.setAccStatus(fromAccData.getStatus());
			l_TransactionData.setPreviousDate(fromAccData.getLastUpdate());
			l_TransactionData.setBranchCode(pTransferData.getFromBranchCode());
		}
		
		l_TransactionData.setWorkstation(pTransferData.getUserID());
		l_TransactionData.setUserID("Mobile");
		l_TransactionData.setAuthorizerID("Mobile");
		l_TransactionData.setSupervisorId("Mobile");
		l_TransactionData.setCurrencyCode(pTransferData.getFromCCY());
		l_TransactionData.setCurrencyRate(drCCYRate);
		l_TransactionData.setTransactionDate(p_TransactionDate);
		l_TransactionData.setTransactionTime(l_TransactionTime);
		l_TransactionData.setEffectiveDate(p_EffectiveDate);
		l_TransactionData.setContraDate("19000101");
		l_TransactionData.setStatus((byte) 1);
		l_TransactionData.setRemark(pTransferData.getRemark());
		l_TransactionData.setAmount(totalfee);
		l_TransactionData.setBaseAmount(StrUtil.round2decimals(pTransferData.getAmount() * drCCYRate));
		l_TransactionData.setDescription(pTransferData.getToAccNumber() + ", Mobile Transfer" + ", " + l_TrDateTime);
		l_TransactionData.setTransactionType(mTrDrTransType);
		l_TransactionData.setSubRef(pTransferData.getReferenceNo());
		l_TransactionData.setCashInHandGL(fromAccData.getCashInHandGL());
		l_TransactionData.setAccRef("");
		l_TransactionData.setT1("");		
		l_TransactionList.add(l_TransactionData);
		
		// Cr To Customer
		l_TransactionData = new TransactionData();
		l_TransactionData.setIsMobile(true);
		l_TransactionData.setAccountNumber(pTransferData.getToAccNumber());
		
		l_TransactionData.setReferenceNumber("");		
		l_TransactionData.setWorkstation(pTransferData.getUserID());
		l_TransactionData.setUserID("Mobile");
		l_TransactionData.setAuthorizerID("Mobile");
		l_TransactionData.setSupervisorId("Mobile");
		l_TransactionData.setCurrencyCode(pTransferData.getToCCY());
		l_TransactionData.setCurrencyRate(crCCYRate);
		l_TransactionData.setTransactionDate(p_TransactionDate);
		l_TransactionData.setTransactionTime(l_TransactionTime);
		l_TransactionData.setEffectiveDate(p_EffectiveDate);
		l_TransactionData.setContraDate("19000101");
		l_TransactionData.setStatus((byte) 1);
		l_TransactionData.setRemark(pTransferData.getRemark());
		l_TransactionData.setAmount(pTransferData.getAmount());
		/*if (!pTransferData.getFromCCY().equalsIgnoreCase(pTransferData.getToCCY())) {
			l_TransactionData.setAmount(StrUtil.round2decimals(
					DBTransactionMgr.getTransAmount(pTransferData.getAmount(), drCCYRate, crCCYRate, "Dr")));
		}*/
		l_TransactionData.setBaseAmount(StrUtil.round2decimals(pTransferData.getAmount() * drCCYRate));
		l_TransactionData
				.setDescription(pTransferData.getFromAccNumber() + ", Mobile  Transfer" + ", " + l_TrDateTime);
		l_TransactionData.setTransactionType(mTrCrTransType);
		l_TransactionData.setSubRef(pTransferData.getReferenceNo());
		l_TransactionData.setCashInHandGL(toAccData.getCashInHandGL());
		l_TransactionData.setAccRef("");
		l_TransactionData.setT1("");
		if (isCrGL) {
			l_TransactionData.setProductGL("");	
			l_TransactionData.setBranchCode(ServerGlobal.getmBranchCode());
		}else{
			l_TransactionData.setProductGL(toAccData.getProductGL());		
			l_TransactionData.setProductType(toAccData.getProduct());
			l_TransactionData.setAccType(toAccData.getType());
			l_TransactionData.setPreviousBalance(toAccData.getCurrentBalance());
			l_TransactionData.setAvaliableBal(toAccData.getAvailableBalance());
			l_TransactionData.setAccStatus(toAccData.getStatus());
			l_TransactionData.setPreviousDate(toAccData.getLastUpdate());
			l_TransactionData.setBranchCode(pTransferData.getFromBranchCode());
		}
		l_TransactionList.add(l_TransactionData);
		if (pTransferData.getTransactionFee() > 0) {
			// Cr To Income GL
			l_TransactionData = new TransactionData();
			l_TransactionData.setIsMobile(true);
			//l_TransactionData.setAccountNumber(l_ComGL);
			l_TransactionData.setReferenceNumber("");
			l_TransactionData.setBranchCode(pTransferData.getFromBranchCode());
			l_TransactionData.setWorkstation("Mobile");
			l_TransactionData.setUserID(pTransferData.getUserID());
			//l_TransactionData.setAuthorizerID(ConnAdmin.readExternalUrl("MobileMakerID"));
			l_TransactionData.setCurrencyCode(pTransferData.getCurrencyCode());
			l_TransactionData.setCurrencyRate(drCCYRate);
			l_TransactionData.setTransactionDate(p_TransactionDate);
			l_TransactionData.setTransactionTime(l_TransactionTime);
			l_TransactionData.setEffectiveDate(p_EffectiveDate);
			l_TransactionData.setContraDate("19000101");
			l_TransactionData.setStatus((byte) 1);
			l_TransactionData.setRemark(pTransferData.getRemark());
			l_TransactionData.setAmount(pTransferData.getAmount());
			l_TransactionData
					.setDescription(pTransferData.getFromAccNumber() + ", Mobile  Transfer" + ", " + l_TrDateTime);
			l_TransactionData.setTransactionType(mTrCrTransType);
			l_TransactionData.setSubRef(pTransferData.getReferenceNo());
			l_TransactionData.setCashInHandGL(fromAccData.getCashInHandGL());
			l_TransactionData.setAccRef("");
			l_TransactionData.setT1("GL");
			l_TransactionData.setProductGL("");
			l_TransactionList.add(l_TransactionData);
		}
	}

	return l_TransactionList;
}
	ArrayList<TransactionData> arl = null;

	private DataResult checkAccount(String pFromProduct, String pToProduct, String pFromAccount, String pToAccount,
			Connection pConn) throws Exception {
		DataResult l_Result = new DataResult();
		// Checking Account is Exist Or Not
		l_Result.setStatus(true);
		if (GeneralUtility.isCardAccount(pFromAccount)) {
			l_Result.setStatus(true);
		}
		if (l_Result.getStatus()) {
			if (SharedLogic.isFD(pFromProduct)) {
				l_Result.setStatus(false);
				l_Result.setErrorFlag(1);
				l_Result.setDescription("Not Allowed Fixed Deposit Account.");
			}
		}
		if (l_Result.getStatus()) {
			if (SharedLogic.isLoan(pFromProduct)) {
				l_Result.setStatus(false);
				l_Result.setErrorFlag(1);
				l_Result.setDescription("Not Allowed Loan Account.");
			}
		}
		if (GeneralUtility.isCardAccount(pToAccount)) {
			l_Result.setStatus(true);
		}

		if (l_Result.getStatus()) {
			if (SharedLogic.isFD(pToProduct)) {
				l_Result.setStatus(false);
				l_Result.setErrorFlag(1);
				l_Result.setDescription("Not Allowed Fixed Deposit Account.");
			}
		}
		if (l_Result.getStatus()) {
			if (SharedLogic.isLoan(pToProduct)) {
				l_Result.setStatus(false);
				l_Result.setErrorFlag(1);
				l_Result.setDescription("Not Allowed Loan Account.");
			}
		}
		return l_Result;
	}
	public double getMobileTransferCharges(double pTrAmount, String pTransferType, String FromBr, String ToBr,
			int FromZone, int ToZone,Connection pConn) throws Exception {
		RateSetupData l_RateSetupData = new RateSetupData();
		RateAmountRangeSetupData l_AmountRangeData = new RateAmountRangeSetupData();
		int rateType = 0;
		double l_TotalFee = 0.0;
		if (pTransferType.equals("1")) { // own
			if (FromBr.equals(ToBr)) { // same br
				rateType = mMobileOwnTRCom_SameBr;
			} else { // diff br
				if (FromZone == ToZone) { // same region
					rateType = mMobileOwnTRCom_DiffBr_SameRegion;
				} else { // diff region
					rateType = mMobileOwnTRCom_DiffBr_DiffRegion;
				}
			}
		} else { // Inter
			if (FromBr.equals(ToBr)) { // same br
				rateType = mMobileInterTRCom_SameBr;
			} else { // diff br
				if (FromZone == ToZone) { // same region
					rateType = mMobileInterTRCom_DiffBr_SameRegion;
				} else { // diff region
					rateType = mMobileInterTRCom_DiffBr_DiffRegion;
				}
			}
		}
		l_RateSetupData = DBRateSetupMgr.getCommissionRateSetupData(rateType, FromBr, "0", "0",pConn);

		// Total Fee on Com 12
		if (l_RateSetupData.getCom12() > 0) {
			if (l_RateSetupData.getCom12Type().equals("F")) {
				l_TotalFee = l_RateSetupData.getCom12();
			} else if (l_RateSetupData.getCom12Type().equals("R")) {
				l_TotalFee = pTrAmount * l_RateSetupData.getCom12();
				if (l_TotalFee < l_RateSetupData.getCom12Min()) {
					l_TotalFee = l_RateSetupData.getCom12Min();
				} else if (l_TotalFee > l_RateSetupData.getCom12Max()) {
					if (l_RateSetupData.getCom12Max() != 0) {
						l_TotalFee = l_RateSetupData.getCom12Max();
					}
				}
			} else {
				l_AmountRangeData = GeneralUtility.getRateByAmountRange(pTrAmount,
						l_RateSetupData.getCom12AmountRangeDatalist());
				l_TotalFee = pTrAmount * l_AmountRangeData.getCRate();
				if (l_TotalFee < l_AmountRangeData.getComMin()) {
					l_TotalFee = l_AmountRangeData.getComMin();
				} else if (l_TotalFee > l_AmountRangeData.getComMax()) {
					if (l_AmountRangeData.getComMax() != 0) {
						l_TotalFee = l_AmountRangeData.getComMax();
					}
				}
			}
		}
		return l_TotalFee;
	}
	
	public SMSReturnData postTransferNew(AccTransferReq disData ,boolean isDrGL,boolean isCrGL,
			com.nirvasoft.rp.shared.AccountData fromAccData, com.nirvasoft.rp.shared.AccountData toAccData, String reqTrnRef, String pTransType,
			Connection pConn) throws Exception {
		SMSReturnData l_SMSReturnData = new SMSReturnData();
		DataResult l_DataResult = new DataResult();
		ArrayList<TransactionData> l_TransactionList = new ArrayList<TransactionData>();
		ArrayList<AccountGLTransactionData> l_AccGLList = new ArrayList<AccountGLTransactionData>();
		DAOManager l_DaoMgr = new DAOManager();
		
		ArrayList<String> l_ArlLogString = new ArrayList<String>();
		String l_EffectiveDate = "";				
		double trFee = 0;
		Connection l_Conn1 = null;
		String pByForceCheque = "";
		String l_TodayDate = GeneralUtility.getTodayDate();
		String l_TransactionTime = DBTransactionMgr.getCurrentDateYYYYMMDDHHMMSS();
		String l_TrDateTime = GeneralUtility.getDateTimeDDMMYYYYHHMMSS();
		// GeneralUtility.setAmountLimitList(GeneralUtility.readMaxTransAmountMobile());
		// // by
		// reference
		System.out.println("start openICBSConnection  "+GeneralUtil.getDateTime());		
		System.out.println("end openICBSConnection  "+GeneralUtil.getDateTime());
		
		/*if (GeneralUtility.checkCutOfTime()) {
			l_EffectiveDate = GeneralUtility.getTomorrowDate();
			l_EffectiveDate = DBTransactionMgr.getEffectiveTransDate(l_EffectiveDate,pConn);
		} else {
			l_EffectiveDate = DBTransactionMgr.getEffectiveTransDate(l_TodayDate,pConn);
		}*/
		l_EffectiveDate = DBTransactionMgr.getEffectiveTransDate(l_TodayDate,pConn);
		try {
			CommRes merchantComm = new CommRes();
			double com1,com2,totalcom;
			boolean isCommGL = false;
				//get Merchant commisssion
				/*merchantComm =new MCommRateMappingMgr().getAgentComm(disData.getMerchantID(),"", Double.parseDouble(disData.getAmount()));
				com1 = merchantComm.getCommessionAmt1();
				if(com1!= 0.0){
					isCommGL = true;
				}
				com2 = merchantComm.getCommessionAmt2();
				if(com2!= 0.0){
					isCommGL = true;
				}
				totalcom = com1 + com2;
				disData.setComm(totalcom);*/
			
				System.out.println("end getMobileTransferCharges  "+GeneralUtil.getDateTime());
				// Preparation	
					if(!disData.getBankCharges().equals("")){
						isCommGL = true;
					}
					l_TransactionList = prepareTransferSameBranchWithoutCommNew(disData, l_TodayDate,
							l_EffectiveDate, l_ArlLogString, "", l_TransactionTime, l_TrDateTime, pByForceCheque,
							fromAccData, toAccData,isDrGL,isCrGL,reqTrnRef,pTransType,trFee,merchantComm,isCommGL, pConn);
					System.out.println("end prepareTransferSameBranchWithoutComm  "+GeneralUtil.getDateTime());
					
					l_AccGLList = DBTransactionMgr.prepareAccountGLTransactions(l_TransactionList, pConn);
					System.out.println("end prepareAccountGLTransactions  "+GeneralUtil.getDateTime());
				
					l_Conn1 = l_DaoMgr.openICBSConnection();
					System.out.println("end prepareAccountGLTransactions  "+GeneralUtil.getDateTime());
					 
					l_Conn1.setAutoCommit(false);
					l_ArlLogString.add("	Post Transaction ");

					System.out.println("start post  "+GeneralUtil.getDateTime());

					l_DataResult = com.nirvasoft.rp.core.ccbs.data.db.DBTransactionMgr.postWallet(l_TransactionList,l_AccGLList,ServerGlobal.getmMinBalSetting(),l_Conn1);
					System.out.println("Result post  "+l_DataResult.getDescription());
					System.out.println("end post  "+GeneralUtil.getDateTime());
					System.out.println("Transaction List: " +l_TransactionList );
					System.out.println("Acc GL List: " +l_AccGLList );
					System.out.println("Acc Response List: " +l_DataResult );

					if (l_DataResult.getStatus()) {
						//l_Conn1.commit();
						l_SMSReturnData.setStatus(true);
						l_SMSReturnData.setCode("0000");
						l_SMSReturnData.setTransactionNumber(l_DataResult.getTransactionNumber());
						l_SMSReturnData.setComm1(trFee);
						l_SMSReturnData.setCcy(ServerGlobal.getmCurrencyCode());
						//l_SMSReturnData.setEffectiveDate(l_DataResult.getEffectiveDate());
						l_SMSReturnData.setEffectiveDate(l_EffectiveDate);
						l_SMSReturnData.setDescription("Posted successfully");
						l_SMSReturnData.setComAccount1("");
						l_SMSReturnData.setComAccoutn2("");
						//l_SMSReturnData.setComm1(com1);
						//l_SMSReturnData.setComm2(com2);
					} else {
						l_SMSReturnData.setStatus(false);
						l_SMSReturnData.setCode("0014");						
						l_SMSReturnData.setDescription(l_DataResult.getDescription());
					}
					/*}
			} else {
				l_SMSReturnData.setDescription(l_DataResult.getDescription());
				l_SMSReturnData.setStatus(false);
				l_SMSReturnData.setCode("0014");
				return l_SMSReturnData;
			}*/
		} catch (Exception e) {
			// l_Conn1.rollback();
			e.printStackTrace();
			l_SMSReturnData.setStatus(false);
			l_SMSReturnData.setDescription("Transaction Failed." + e.getMessage());
			l_ArlLogString.add("Transaction Roll Back" + l_DataResult.getDescription() + e.getMessage());
			l_SMSReturnData.setErrorCode("06");
		}finally{
			
			if(l_DataResult.getStatus()){
				l_Conn1.commit();
			}else 
				l_Conn1.rollback();
			if (!l_Conn1.isClosed() || l_Conn1 != null) {
				l_Conn1.close();
			}
				
		}
		return l_SMSReturnData;
	}
	public SMSReturnData postMPTPaymentReq (AccTransferReq disData ,boolean isDrGL,boolean isCrGL,
			com.nirvasoft.rp.shared.AccountData fromAccData, com.nirvasoft.rp.shared.AccountData toAccData, String reqTrnRef, String pTransType,
			Connection pConn) throws Exception {
		SMSReturnData l_SMSReturnData = new SMSReturnData();
		DataResult l_DataResult = new DataResult();
		ArrayList<TransactionData> l_TransactionList = new ArrayList<TransactionData>();
		ArrayList<AccountGLTransactionData> l_AccGLList = new ArrayList<AccountGLTransactionData>();
		DAOManager l_DaoMgr = new DAOManager();
		
		ArrayList<String> l_ArlLogString = new ArrayList<String>();
		String l_EffectiveDate = "";				
		double trFee = 0;
		Connection l_Conn1 = null;
		String pByForceCheque = "";
		String l_TodayDate = GeneralUtility.getTodayDate();
		String l_TransactionTime = DBTransactionMgr.getCurrentDateYYYYMMDDHHMMSS();
		String l_TrDateTime = GeneralUtility.getDateTimeDDMMYYYYHHMMSS();
		// GeneralUtility.setAmountLimitList(GeneralUtility.readMaxTransAmountMobile());
		// // by
		// reference
		System.out.println("start openICBSConnection  "+GeneralUtil.getDateTime());		
		System.out.println("end openICBSConnection  "+GeneralUtil.getDateTime());
		
		/*if (GeneralUtility.checkCutOfTime()) {
			l_EffectiveDate = GeneralUtility.getTomorrowDate();
			l_EffectiveDate = DBTransactionMgr.getEffectiveTransDate(l_EffectiveDate,pConn);
		} else {
			l_EffectiveDate = DBTransactionMgr.getEffectiveTransDate(l_TodayDate,pConn);
		}*/
		l_EffectiveDate = DBTransactionMgr.getEffectiveTransDate(l_TodayDate,pConn);
		try {
			CommRes merchantComm = new CommRes();
			double com1,com2,totalcom;
			boolean isCommGL = true;
				
					l_TransactionList = prepareMPTTopup(disData, l_TodayDate,
							l_EffectiveDate, l_ArlLogString, "", l_TransactionTime, l_TrDateTime, pByForceCheque,
							fromAccData, toAccData,isDrGL,isCrGL,reqTrnRef,pTransType,trFee,merchantComm,isCommGL, pConn);
					System.out.println("end prepareTransferSameBranchWithoutComm  "+GeneralUtil.getDateTime());
					
					l_AccGLList = DBTransactionMgr.prepareAccountGLTransactions(l_TransactionList, pConn);
					System.out.println("end prepareAccountGLTransactions  "+GeneralUtil.getDateTime());
				
					l_Conn1 = l_DaoMgr.openICBSConnection();
					System.out.println("end prepareAccountGLTransactions  "+GeneralUtil.getDateTime());
					 
					l_Conn1.setAutoCommit(false);
					l_ArlLogString.add("	Post Transaction ");

					System.out.println("start post  "+GeneralUtil.getDateTime());

					l_DataResult = com.nirvasoft.rp.core.ccbs.data.db.DBTransactionMgr.postWallet(l_TransactionList,l_AccGLList,ServerGlobal.getmMinBalSetting(),l_Conn1);
					System.out.println("Result post  "+l_DataResult.getDescription());
					System.out.println("end post  "+GeneralUtil.getDateTime());
					System.out.println("Transaction List: " +l_TransactionList );
					System.out.println("Acc GL List: " +l_AccGLList );
					System.out.println("Acc Response List: " +l_DataResult );

					if (l_DataResult.getStatus()) {
						//l_Conn1.commit();
						l_SMSReturnData.setStatus(true);
						l_SMSReturnData.setCode("0000");
						l_SMSReturnData.setTransactionNumber(l_DataResult.getTransactionNumber());
						l_SMSReturnData.setComm1(trFee);
						l_SMSReturnData.setCcy(ServerGlobal.getmCurrencyCode());
						//l_SMSReturnData.setEffectiveDate(l_DataResult.getEffectiveDate());
						l_SMSReturnData.setEffectiveDate(l_EffectiveDate);
						l_SMSReturnData.setDescription("Posted successfully");
						l_SMSReturnData.setComAccount1("");
						l_SMSReturnData.setComAccoutn2("");
						//l_SMSReturnData.setComm1(com1);
						//l_SMSReturnData.setComm2(com2);
					} else {
						l_SMSReturnData.setStatus(false);
						l_SMSReturnData.setCode("0014");						
						l_SMSReturnData.setDescription(l_DataResult.getDescription());
					}
					/*}
			} else {
				l_SMSReturnData.setDescription(l_DataResult.getDescription());
				l_SMSReturnData.setStatus(false);
				l_SMSReturnData.setCode("0014");
				return l_SMSReturnData;
			}*/
		} catch (Exception e) {
			// l_Conn1.rollback();
			e.printStackTrace();
			l_SMSReturnData.setStatus(false);
			l_SMSReturnData.setDescription("Transaction Failed." + e.getMessage());
			l_ArlLogString.add("Transaction Roll Back" + l_DataResult.getDescription() + e.getMessage());
			l_SMSReturnData.setErrorCode("06");
		}finally{
			
			if(l_DataResult.getStatus()){
				l_Conn1.commit();
			}else 
				l_Conn1.rollback();
			if (!l_Conn1.isClosed() || l_Conn1 != null) {
				l_Conn1.close();
			}
				
		}
		return l_SMSReturnData;
	}
	
	private ArrayList<TransactionData> prepareTransferSameBranchWithoutCommNew(AccTransferReq pTransferData,
			String p_TransactionDate, String p_EffectiveDate, ArrayList<String> l_ArlLogString, String pType,
			String l_TransactionTime, String l_TrDateTime, String pByForceCheque, com.nirvasoft.rp.shared.AccountData fromAccData,
			com.nirvasoft.rp.shared.AccountData toAccData,boolean isDrGL,boolean isCrGL,String reqTrnRef,String pTransType,
			double trFee,CommRes merchantComm,boolean isCommGL,Connection conn) throws Exception {
		ArrayList<TransactionData> l_TransactionList = new ArrayList<TransactionData>();
		TransactionData l_TransactionData = new TransactionData();
		ReferenceAccountsData l_RefData = new ReferenceAccountsData();
		double drCCYRate = ServerGlobal.getmCurRate();
		double crCCYRate = ServerGlobal.getmCurRate();
		/*DBFECCurrencyRateMgr.getFECCurrencyRate(pTransferData.getToCCY(), "smTransfer",
				pTransferData.getToBranchCode(), conn)*/
		double amt= 0.0;
		double totalfee = 0;
		double bankCharges = 0;	
		double penelty = 0;
		double totalamt = 0;
		if (!pTransferData.getBankCharges().equals("")) {
			 bankCharges = Double.parseDouble(pTransferData.getBankCharges());
		}
		if(!pTransferData.getPenalty().equals("")){
			penelty = Double.parseDouble(pTransferData.getPenalty());
		}
		if(!pTransferData.getCurrentAmount().equals("")){
			amt = Double.parseDouble(pTransferData.getCurrentAmount());
		}else{
			amt = Double.parseDouble(pTransferData.getAmount());
		}
			 totalamt = amt + penelty;
			 totalfee = totalamt + bankCharges ;
		/*}else{
			totalamt = Double.parseDouble(pTransferData.getAmount());	
			totalfee = totalamt + trFee;
		}*/
		
		/*if (!pTransferData.getBankCharges().equals("") && !pTransferData.getPenalty().equals("")) {
			 bankCharges = Double.parseDouble(pTransferData.getBankCharges());
			 amt = Double.parseDouble(pTransferData.getCurrentAmount());
			 penelty = Double.parseDouble(pTransferData.getPenalty());
			 totalamt = amt + penelty;
			 totalfee = totalamt + bankCharges ;
		}else{
			totalamt = Double.parseDouble(pTransferData.getAmount());	
			totalfee = totalamt + trFee;
		}*/
		
		//double totalfee = amt + trFee + merchantComm.getCommessionAmt1() + merchantComm.getCommessionAmt2() ;		
		

		if (totalfee > 0 ) {
			// Dr From Customer
			l_TransactionData = new TransactionData();
			l_TransactionData.setIsMobile(true);
			l_TransactionData.setAccountNumber(pTransferData.getFromAccount());		
			if(isDrGL) {
				l_TransactionData.setReferenceNumber("");
				l_TransactionData.setProductGL("");	
				l_TransactionData.setBranchCode(ServerGlobal.getmBranchCode());				
				l_TransactionData.setCurrencyCode(ServerGlobal.getmCurrencyCode() );
				l_TransactionData.setCashInHandGL(ServerGlobal.getmCashInHand());
			}else{
				if(ServerGlobal.isHasCheque())
					l_TransactionData.setReferenceNumber(pByForceCheque);
				l_TransactionData.setProductGL(fromAccData.getProductGL());
				l_TransactionData.setPreviousBalance(fromAccData.getCurrentBalance());
				l_TransactionData.setProductType(fromAccData.getProduct());
				l_TransactionData.setAccType(fromAccData.getType());
				l_TransactionData.setAvaliableBal(fromAccData.getAvailableBalance());
				l_TransactionData.setAccStatus(fromAccData.getStatus());
				l_TransactionData.setPreviousDate(fromAccData.getLastUpdate());
				l_TransactionData.setBranchCode(fromAccData.getBranchCode());
				l_TransactionData.setCurrencyCode(ServerGlobal.getmCurrencyCode());
				l_TransactionData.setCashInHandGL(fromAccData.getCashInHandGL());
			}
			
			l_TransactionData.setWorkstation(pTransferData.getSenderCode());
			l_TransactionData.setUserID("Mobile");
			l_TransactionData.setAuthorizerID("Mobile");
			l_TransactionData.setSupervisorId("Mobile");			
			l_TransactionData.setCurrencyRate(drCCYRate);
			l_TransactionData.setTransactionDate(p_TransactionDate);
			l_TransactionData.setTransactionTime(l_TransactionTime);
			l_TransactionData.setEffectiveDate(p_EffectiveDate);
			l_TransactionData.setContraDate("19000101");
			l_TransactionData.setStatus((byte) 1);
			l_TransactionData.setAmount(totalfee);
			l_TransactionData.setBaseAmount(StrUtil.round2decimals( totalfee * drCCYRate));
			if(pTransType.equals("1")){
				l_TransactionData.setRemark("Transfer");
				l_TransactionData.setDescription(pTransferData.getReceiverCode() + "," + pTransferData.getToName() + ","+ l_TrDateTime);
				l_TransactionData.setAccRef("1");
			}
			if(pTransType.equals("2")){
				l_TransactionData.setRemark("Bill Payment");
				l_TransactionData.setDescription(pTransferData.getToAccount() + "," +pTransferData.getToName() +" Payment" + "," + l_TrDateTime);
				l_TransactionData.setAccRef("2");
			}
			if(pTransType.equals("4")){
				l_TransactionData.setRemark("Top up");
				l_TransactionData.setDescription(pTransferData.getReceiverCode() + "," + pTransferData.getToName() + "," + l_TrDateTime);
				l_TransactionData.setAccRef("4");
			}
			if(pTransType.equals("5")){
				l_TransactionData.setRemark("Transfer Wallet to Account");
				l_TransactionData.setDescription(pTransferData.getSenderCode() + "," + pTransferData.getToName() + "," + l_TrDateTime);
				l_TransactionData.setAccRef("5");
			}
			if(pTransType.equals("6")){
				l_TransactionData.setRemark("Disbursement");
				l_TransactionData.setDescription(pTransferData.getSenderCode() + "," + pTransferData.getToName() + "," + l_TrDateTime);
				l_TransactionData.setAccRef("6");
			}
			l_TransactionData.setTransactionType(mTrDrTransType);
			l_TransactionData.setSubRef(reqTrnRef);
			l_TransactionData.setT1("");		
			l_TransactionList.add(l_TransactionData);
			
			// Cr To Customer
			l_TransactionData = new TransactionData();
			l_TransactionData.setIsMobile(true);
			l_TransactionData.setAccountNumber(pTransferData.getToAccount());
			
			l_TransactionData.setReferenceNumber("");		
			l_TransactionData.setWorkstation(pTransferData.getSenderCode());
			l_TransactionData.setUserID("Mobile");
			l_TransactionData.setAuthorizerID("Mobile");
			l_TransactionData.setSupervisorId("Mobile");			
			l_TransactionData.setCurrencyRate(crCCYRate);
			l_TransactionData.setTransactionDate(p_TransactionDate);
			l_TransactionData.setTransactionTime(l_TransactionTime);
			l_TransactionData.setEffectiveDate(p_EffectiveDate);
			l_TransactionData.setContraDate("19000101");
			l_TransactionData.setStatus((byte) 1);			
			l_TransactionData.setAmount(totalamt);
			/*if (!pTransferData.getFromCCY().equalsIgnoreCase(pTransferData.getToCCY())) {
				l_TransactionData.setAmount(StrUtil.round2decimals(
						DBTransactionMgr.getTransAmount(pTransferData.getAmount(), drCCYRate, crCCYRate, "Dr")));
			}*/
			l_TransactionData.setBaseAmount(StrUtil.round2decimals(amt * drCCYRate));
			if(pTransType.equals("1")){
				l_TransactionData.setRemark("Transfer");
				l_TransactionData.setDescription(pTransferData.getSenderCode() + "," + pTransferData.getFromName() + "," + l_TrDateTime);
				l_TransactionData.setAccRef("1");
			}
			if(pTransType.equals("2")){
				l_TransactionData.setRemark("Bill Payment");
				l_TransactionData.setDescription(pTransferData.getSenderCode() + "," +pTransferData.getFromName() + "," + l_TrDateTime);
				l_TransactionData.setAccRef("2");
			}
			if(pTransType.equals("4")){
				l_TransactionData.setRemark("Top up");
				l_TransactionData.setDescription(pTransferData.getAgentAccount() + "," + pTransferData.getFromName() + ","+ l_TrDateTime);
				l_TransactionData.setAccRef("4");
			}
			if(pTransType.equals("5")){
				l_TransactionData.setRemark("Transfer Wallet to Account");
				l_TransactionData.setDescription(pTransferData.getAgentAccount() + "," + pTransferData.getFromName() + "," + l_TrDateTime);
				l_TransactionData.setAccRef("5");
			}
			if(pTransType.equals("6")){
				l_TransactionData.setRemark("Disbursement");
				l_TransactionData.setDescription(pTransferData.getSenderCode() + "," + pTransferData.getToName() + "," + l_TrDateTime);
				l_TransactionData.setAccRef("6");
			}
			
			l_TransactionData.setTransactionType(mTrCrTransType);
			l_TransactionData.setSubRef(reqTrnRef);
			
			//l_TransactionData.setAccRef("");
			l_TransactionData.setT1("");
			if (isCrGL) {
				l_TransactionData.setProductGL("");	
				l_TransactionData.setBranchCode(ServerGlobal.getmBranchCode());
				l_TransactionData.setCurrencyCode(ServerGlobal.getmCurrencyCode());
				l_TransactionData.setCashInHandGL(ServerGlobal.getmCashInHand());
				
			}else{
				l_TransactionData.setProductGL(toAccData.getProductGL());		
				l_TransactionData.setProductType(toAccData.getProduct());
				l_TransactionData.setAccType(toAccData.getType());
				l_TransactionData.setPreviousBalance(toAccData.getCurrentBalance());
				l_TransactionData.setAvaliableBal(toAccData.getAvailableBalance());
				l_TransactionData.setAccStatus(toAccData.getStatus());
				l_TransactionData.setPreviousDate(toAccData.getLastUpdate());
				l_TransactionData.setBranchCode(toAccData.getBranchCode());
				l_TransactionData.setCurrencyCode(ServerGlobal.getmCurrencyCode());
				l_TransactionData.setCashInHandGL(toAccData.getCashInHandGL());
			}
			l_TransactionList.add(l_TransactionData);
			//Cr to InComeGL			
			String l_ComGL = pTransferData.getFromAccount();
			if(trFee > 0){
				l_TransactionData = new TransactionData();
				l_TransactionData.setIsMobile(true);
				l_TransactionData.setAccountNumber(l_ComGL);
				
				l_TransactionData.setReferenceNumber("");		
				l_TransactionData.setWorkstation(pTransferData.getSenderCode());
				l_TransactionData.setUserID("Mobile");
				l_TransactionData.setAuthorizerID("Mobile");
				l_TransactionData.setSupervisorId("Mobile");			
				l_TransactionData.setCurrencyRate(crCCYRate);
				l_TransactionData.setTransactionDate(p_TransactionDate);
				l_TransactionData.setTransactionTime(l_TransactionTime);
				l_TransactionData.setEffectiveDate(p_EffectiveDate);
				l_TransactionData.setContraDate("19000101");
				l_TransactionData.setStatus((byte) 1);
				l_TransactionData.setAmount(trFee);				
				l_TransactionData.setBaseAmount(StrUtil.round2decimals(trFee * drCCYRate));
				if(pTransType.equals("1")){
					l_TransactionData.setRemark("Transfer Commission");
					l_TransactionData.setDescription(pTransferData.getSenderCode() + "," + pTransferData.getFromName() + "," + l_TrDateTime);
					l_TransactionData.setAccRef("1");
				}
				if(pTransType.equals("4")){
					l_TransactionData.setRemark("Top up Commission");
					l_TransactionData.setDescription(pTransferData.getAgentAccount() + "," + pTransferData.getFromName() + ","+ l_TrDateTime);
					l_TransactionData.setAccRef("4");
				}
				
				l_TransactionData.setTransactionType(mTrCrTransType);
				l_TransactionData.setSubRef(reqTrnRef);
				
				l_TransactionData.setAccRef("");
				l_TransactionData.setT1("");
				if (isCrGL) {
					l_TransactionData.setProductGL("");	
					l_TransactionData.setBranchCode(ServerGlobal.getmBranchCode());
					l_TransactionData.setCurrencyCode(ServerGlobal.getmCurrencyCode());
					l_TransactionData.setCashInHandGL(ServerGlobal.getmCashInHand());
				}else{
					l_TransactionData.setProductGL(toAccData.getProductGL());		
					l_TransactionData.setProductType(toAccData.getProduct());
					l_TransactionData.setAccType(toAccData.getType());
					l_TransactionData.setPreviousBalance(toAccData.getCurrentBalance());
					l_TransactionData.setAvaliableBal(toAccData.getAvailableBalance());
					l_TransactionData.setAccStatus(toAccData.getStatus());
					l_TransactionData.setPreviousDate(toAccData.getLastUpdate());
					l_TransactionData.setBranchCode(toAccData.getBranchCode());
					l_TransactionData.setCurrencyCode(ServerGlobal.getmCurrencyCode());
					l_TransactionData.setCashInHandGL(toAccData.getCashInHandGL());
				}
				l_TransactionList.add(l_TransactionData);
			}
			// Cr To MerchantGL1
			l_RefData = DBSystemMgr.getReferenceAccCode("ODC1MMK02" , conn);//MTRCOMMMK002
			String Com1gl = l_RefData.getGLCode();
			if(bankCharges > 0){
				l_TransactionData = new TransactionData();
				l_TransactionData.setIsMobile(true);
				l_TransactionData.setAccountNumber(Com1gl);
				l_TransactionData.setReferenceNumber("");		
				l_TransactionData.setWorkstation(pTransferData.getSenderCode());
				l_TransactionData.setUserID("Mobile");
				l_TransactionData.setAuthorizerID("Mobile");
				l_TransactionData.setSupervisorId("Mobile");			
				l_TransactionData.setCurrencyRate(crCCYRate);
				l_TransactionData.setTransactionDate(p_TransactionDate);
				l_TransactionData.setTransactionTime(l_TransactionTime);
				l_TransactionData.setEffectiveDate(p_EffectiveDate);
				l_TransactionData.setContraDate("19000101");
				l_TransactionData.setStatus((byte) 1);
				l_TransactionData.setAmount(bankCharges);				
				l_TransactionData.setBaseAmount(StrUtil.round2decimals(merchantComm.getCommessionAmt1() * drCCYRate));
				if(pTransType.equals("2")){
					l_TransactionData.setRemark("Bill Commission");
					l_TransactionData.setDescription(pTransferData.getSenderCode() + "," +pTransferData.getFromName() + "," + l_TrDateTime);
					l_TransactionData.setAccRef("2");
				}
				l_TransactionData.setTransactionType(mTrCrTransType);
				l_TransactionData.setT1("");
				if (isCrGL || isCommGL) {
					l_TransactionData.setProductGL("");	
					l_TransactionData.setBranchCode(ServerGlobal.getmBranchCode());
					l_TransactionData.setCurrencyCode(ServerGlobal.getmCurrencyCode());
					l_TransactionData.setCashInHandGL(ServerGlobal.getmCashInHand());
				}else{
					l_TransactionData.setProductGL(toAccData.getProductGL());		
					l_TransactionData.setProductType(toAccData.getProduct());
					l_TransactionData.setAccType(toAccData.getType());
					l_TransactionData.setPreviousBalance(toAccData.getCurrentBalance());
					l_TransactionData.setAvaliableBal(toAccData.getAvailableBalance());
					l_TransactionData.setAccStatus(toAccData.getStatus());
					l_TransactionData.setPreviousDate(toAccData.getLastUpdate());
					l_TransactionData.setBranchCode(toAccData.getBranchCode());
					l_TransactionData.setCurrencyCode(ServerGlobal.getmCurrencyCode());
					l_TransactionData.setCashInHandGL(toAccData.getCashInHandGL());
				}
				l_TransactionList.add(l_TransactionData);			
			}
			// Cr To MerchantGL2
			l_RefData = DBSystemMgr.getReferenceAccCode("ODC2MMK02" , conn);//MTRCOMMMK002
			String Com2gl = l_RefData.getGLCode();
			if(merchantComm.getCommessionAmt2() > 0){
				l_TransactionData = new TransactionData();
				l_TransactionData.setIsMobile(true);
				l_TransactionData.setAccountNumber(Com2gl);
				
				l_TransactionData.setReferenceNumber("");		
				l_TransactionData.setWorkstation(pTransferData.getSenderCode());
				l_TransactionData.setUserID("Mobile");
				l_TransactionData.setAuthorizerID("Mobile");
				l_TransactionData.setSupervisorId("Mobile");			
				l_TransactionData.setCurrencyRate(crCCYRate);
				l_TransactionData.setTransactionDate(p_TransactionDate);
				l_TransactionData.setTransactionTime(l_TransactionTime);
				l_TransactionData.setEffectiveDate(p_EffectiveDate);
				l_TransactionData.setContraDate("19000101");
				l_TransactionData.setStatus((byte) 1);
				l_TransactionData.setRemark("mWallet");
				l_TransactionData.setAmount(merchantComm.getCommessionAmt2());				
				l_TransactionData.setBaseAmount(StrUtil.round2decimals(merchantComm.getCommessionAmt2() * drCCYRate));
				if(pTransType.equals("2")){
					l_TransactionData.setRemark("Bill Commission");
					l_TransactionData.setDescription(pTransferData.getToAccount() + "," +pTransferData.getToName() +" Commission" + "," + l_TrDateTime);
					l_TransactionData.setAccRef("2");
				}
				l_TransactionData.setTransactionType(mTrCrTransType);
				l_TransactionData.setSubRef(reqTrnRef);
				l_TransactionData.setT1("");
				if (isCrGL || isCommGL) {
					l_TransactionData.setProductGL("");	
					l_TransactionData.setBranchCode(ServerGlobal.getmBranchCode());
					l_TransactionData.setCurrencyCode(ServerGlobal.getmCurrencyCode());
					l_TransactionData.setCashInHandGL(ServerGlobal.getmCashInHand());
				}else{
					l_TransactionData.setProductGL(toAccData.getProductGL());		
					l_TransactionData.setProductType(toAccData.getProduct());
					l_TransactionData.setAccType(toAccData.getType());
					l_TransactionData.setPreviousBalance(toAccData.getCurrentBalance());
					l_TransactionData.setAvaliableBal(toAccData.getAvailableBalance());
					l_TransactionData.setAccStatus(toAccData.getStatus());
					l_TransactionData.setPreviousDate(toAccData.getLastUpdate());
					l_TransactionData.setBranchCode(toAccData.getBranchCode());
					l_TransactionData.setCurrencyCode(ServerGlobal.getmCurrencyCode());
					l_TransactionData.setCashInHandGL(toAccData.getCashInHandGL());
				}
				l_TransactionList.add(l_TransactionData);
			
			}
		}
		return l_TransactionList;
	}
	private ArrayList<TransactionData> prepareMPTTopup(AccTransferReq pTransferData,
			String p_TransactionDate, String p_EffectiveDate, ArrayList<String> l_ArlLogString, String pType,
			String l_TransactionTime, String l_TrDateTime, String pByForceCheque, com.nirvasoft.rp.shared.AccountData fromAccData,
			com.nirvasoft.rp.shared.AccountData toAccData,boolean isDrGL,boolean isCrGL,String reqTrnRef,String pTransType,
			double trFee,CommRes merchantComm,boolean isCommGL,Connection conn) throws Exception {
		ArrayList<TransactionData> l_TransactionList = new ArrayList<TransactionData>();
		TransactionData l_TransactionData = new TransactionData();
		ReferenceAccountsData l_RefData = new ReferenceAccountsData();
		double drCCYRate = ServerGlobal.getmCurRate();
		double crCCYRate = ServerGlobal.getmCurRate();
		/*DBFECCurrencyRateMgr.getFECCurrencyRate(pTransferData.getToCCY(), "smTransfer",
				pTransferData.getToBranchCode(), conn)*/
		double amt= 0.0;
		double totalfee = 0;
		double totalamt = 0;
		double disAmount = 0; 
		double actualAmount = 0;
		
		if(!pTransferData.getCurrentAmount().equals("")){
			amt = Double.parseDouble(pTransferData.getCurrentAmount());
		}else{
			amt = Double.parseDouble(pTransferData.getAmount());
		}
			 totalamt = amt ;
			 totalfee = totalamt;
			 double topupDis = Double.parseDouble(ConnAdmin.readDiscountConfig("TopupDiscount"));
				if(topupDis > 0){
					disAmount = com.nirvasoft.rp.util.GeneralUtility.calculatePercentage(totalfee, topupDis);
//					l_DISGL = DBSystemMgr.getReferenceAccCode("MTRDIS"+ pTransferData.getFromBranchCode()).getGLCode();
					actualAmount = totalfee - disAmount;//ndh
				}
			 
		if (totalfee > 0 ) {
			// Dr From Customer
			l_TransactionData = new TransactionData();
			l_TransactionData.setIsMobile(true);
			l_TransactionData.setAccountNumber(pTransferData.getFromAccount());		
//			if(isDrGL) {
//				l_TransactionData.setReferenceNumber("");
//				l_TransactionData.setProductGL("");	
//				l_TransactionData.setBranchCode(ServerGlobal.getmBranchCode());				
//				l_TransactionData.setCurrencyCode(ServerGlobal.getmCurrencyCode() );
//				l_TransactionData.setCashInHandGL(ServerGlobal.getmCashInHand());
//			}else{
				if(ServerGlobal.isHasCheque())
					l_TransactionData.setReferenceNumber(pByForceCheque);
				l_TransactionData.setProductGL(fromAccData.getProductGL());
				l_TransactionData.setPreviousBalance(fromAccData.getCurrentBalance());
				l_TransactionData.setProductType(fromAccData.getProduct());
				l_TransactionData.setAccType(fromAccData.getType());
				l_TransactionData.setAvaliableBal(fromAccData.getAvailableBalance());
				l_TransactionData.setAccStatus(fromAccData.getStatus());
				l_TransactionData.setPreviousDate(fromAccData.getLastUpdate());
				l_TransactionData.setBranchCode(fromAccData.getBranchCode());
				l_TransactionData.setCurrencyCode(ServerGlobal.getmCurrencyCode());
				l_TransactionData.setCashInHandGL(fromAccData.getCashInHandGL());
			//}
			
			l_TransactionData.setWorkstation(pTransferData.getSenderCode());
			l_TransactionData.setUserID("Mobile");
			l_TransactionData.setAuthorizerID("Mobile");
			l_TransactionData.setSupervisorId("Mobile");			
			l_TransactionData.setCurrencyRate(drCCYRate);
			l_TransactionData.setTransactionDate(p_TransactionDate);
			l_TransactionData.setTransactionTime(l_TransactionTime);
			l_TransactionData.setEffectiveDate(p_EffectiveDate);
			l_TransactionData.setContraDate("19000101");
			l_TransactionData.setStatus((byte) 1);
			l_TransactionData.setAmount(actualAmount);
			l_TransactionData.setBaseAmount(StrUtil.round2decimals( totalfee * drCCYRate));
			
			if(pTransType.equals("2")){
				l_TransactionData.setRemark("Bill Payment");
				l_TransactionData.setDescription(pTransferData.getToAccount() + "," +pTransferData.getToName() +" Payment" + "," + l_TrDateTime);
				l_TransactionData.setAccRef("2");
			}
			
			l_TransactionData.setTransactionType(mTrDrTransType);
			l_TransactionData.setSubRef(reqTrnRef);
			l_TransactionData.setT1("");		
			l_TransactionList.add(l_TransactionData);
			
			// Cr To Customer
			l_TransactionData = new TransactionData();
			l_TransactionData.setIsMobile(true);
			l_TransactionData.setAccountNumber(pTransferData.getToAccount());
			
			l_TransactionData.setReferenceNumber("");		
			l_TransactionData.setWorkstation(pTransferData.getSenderCode());
			l_TransactionData.setUserID("Mobile");
			l_TransactionData.setAuthorizerID("Mobile");
			l_TransactionData.setSupervisorId("Mobile");			
			l_TransactionData.setCurrencyRate(crCCYRate);
			l_TransactionData.setTransactionDate(p_TransactionDate);
			l_TransactionData.setTransactionTime(l_TransactionTime);
			l_TransactionData.setEffectiveDate(p_EffectiveDate);
			l_TransactionData.setContraDate("19000101");
			l_TransactionData.setStatus((byte) 1);			
			l_TransactionData.setAmount(totalamt);
			/*if (!pTransferData.getFromCCY().equalsIgnoreCase(pTransferData.getToCCY())) {
				l_TransactionData.setAmount(StrUtil.round2decimals(
						DBTransactionMgr.getTransAmount(pTransferData.getAmount(), drCCYRate, crCCYRate, "Dr")));
			}*/
			l_TransactionData.setBaseAmount(StrUtil.round2decimals(amt * drCCYRate));
			
			if(pTransType.equals("2")){
				l_TransactionData.setRemark("Bill Payment");
				l_TransactionData.setDescription(pTransferData.getSenderCode() + "," +pTransferData.getFromName() + "," + l_TrDateTime);
				l_TransactionData.setAccRef("2");
			}
			
			l_TransactionData.setTransactionType(mTrCrTransType);
			l_TransactionData.setSubRef(reqTrnRef);
			
			//l_TransactionData.setAccRef("");
			l_TransactionData.setT1("");
			if (isCrGL) {
				l_TransactionData.setProductGL("");	
				l_TransactionData.setBranchCode(ServerGlobal.getmBranchCode());
				l_TransactionData.setCurrencyCode(ServerGlobal.getmCurrencyCode());
				l_TransactionData.setCashInHandGL(ServerGlobal.getmCashInHand());
				
			}else{
				l_TransactionData.setProductGL(toAccData.getProductGL());		
				l_TransactionData.setProductType(toAccData.getProduct());
				l_TransactionData.setAccType(toAccData.getType());
				l_TransactionData.setPreviousBalance(toAccData.getCurrentBalance());
				l_TransactionData.setAvaliableBal(toAccData.getAvailableBalance());
				l_TransactionData.setAccStatus(toAccData.getStatus());
				l_TransactionData.setPreviousDate(toAccData.getLastUpdate());
				l_TransactionData.setBranchCode(toAccData.getBranchCode());
				l_TransactionData.setCurrencyCode(ServerGlobal.getmCurrencyCode());
				l_TransactionData.setCashInHandGL(toAccData.getCashInHandGL());
			}
			l_TransactionList.add(l_TransactionData);
			//Cr to InComeGL			
			
			// Cr To MerchantGL1
			l_RefData = DBSystemMgr.getReferenceAccCode("MTRDIS002" , conn);//MTRCOMMMK002
			String Com1gl = l_RefData.getGLCode();
				l_TransactionData = new TransactionData();
				l_TransactionData.setIsMobile(true);
				l_TransactionData.setAccountNumber(Com1gl);
				l_TransactionData.setReferenceNumber("");		
				l_TransactionData.setWorkstation(pTransferData.getSenderCode());
				l_TransactionData.setUserID("Mobile");
				l_TransactionData.setAuthorizerID("Mobile");
				l_TransactionData.setSupervisorId("Mobile");			
				l_TransactionData.setCurrencyRate(crCCYRate);
				l_TransactionData.setTransactionDate(p_TransactionDate);
				l_TransactionData.setTransactionTime(l_TransactionTime);
				l_TransactionData.setEffectiveDate(p_EffectiveDate);
				l_TransactionData.setContraDate("19000101");
				l_TransactionData.setStatus((byte) 1);
				l_TransactionData.setAmount(disAmount);				
				l_TransactionData.setBaseAmount(StrUtil.round2decimals(merchantComm.getCommessionAmt1() * drCCYRate));
				if(pTransType.equals("2")){
					l_TransactionData.setRemark("Discount Amount");
					l_TransactionData.setDescription(pTransferData.getSenderCode() + "," +pTransferData.getFromName() + "," + l_TrDateTime);
					l_TransactionData.setAccRef("2");
				}
				l_TransactionData.setTransactionType(mTrDrTransType);//dr to bank GL
				l_TransactionData.setT1("");
				if (isCrGL || isCommGL) {
					l_TransactionData.setProductGL("");	
					l_TransactionData.setBranchCode(ServerGlobal.getmBranchCode());
					l_TransactionData.setCurrencyCode(ServerGlobal.getmCurrencyCode());
					l_TransactionData.setCashInHandGL(ServerGlobal.getmCashInHand());
				}else{
					l_TransactionData.setProductGL(toAccData.getProductGL());		
					l_TransactionData.setProductType(toAccData.getProduct());
					l_TransactionData.setAccType(toAccData.getType());
					l_TransactionData.setPreviousBalance(toAccData.getCurrentBalance());
					l_TransactionData.setAvaliableBal(toAccData.getAvailableBalance());
					l_TransactionData.setAccStatus(toAccData.getStatus());
					l_TransactionData.setPreviousDate(toAccData.getLastUpdate());
					l_TransactionData.setBranchCode(toAccData.getBranchCode());
					l_TransactionData.setCurrencyCode(ServerGlobal.getmCurrencyCode());
					l_TransactionData.setCashInHandGL(toAccData.getCashInHandGL());
				}
				l_TransactionList.add(l_TransactionData);
		}
		return l_TransactionList;
	}
	public double getMobileTransferCharges(double pTrAmount, String pTransferType, String FromBr, String ToBr,
			int FromZone, int ToZone) throws Exception {
		RateSetupData l_RateSetupData = new RateSetupData();
		RateAmountRangeSetupData l_AmountRangeData = new RateAmountRangeSetupData();
		int rateType = 0;
		double l_TotalFee = 0.0;
		if (pTransferType.equals("1")) { // own
			if (FromBr.equals(ToBr)) { // same br
				rateType = mMobileOwnTRCom_SameBr;
			} else { // diff br
				if (FromZone == ToZone) { // same region
					rateType = mMobileOwnTRCom_DiffBr_SameRegion;
				} else { // diff region
					rateType = mMobileOwnTRCom_DiffBr_DiffRegion;
				}
			}
		} else { // Inter
			if (FromBr.equals(ToBr)) { // same br
				rateType = mMobileInterTRCom_SameBr;
			} else { // diff br
				if (FromZone == ToZone) { // same region
					rateType = mMobileInterTRCom_DiffBr_SameRegion;
				} else { // diff region
					rateType = mMobileInterTRCom_DiffBr_DiffRegion;
				}
			}
		}
		l_RateSetupData = DBRateSetupMgr.getCommissionRateSetupData(rateType, FromBr, "0", "0");

		// Total Fee on Com 12
		if (l_RateSetupData.getCom12() > 0) {
			if (l_RateSetupData.getCom12Type().equals("F")) {
				l_TotalFee = l_RateSetupData.getCom12();
			} else if (l_RateSetupData.getCom12Type().equals("R")) {
				l_TotalFee = pTrAmount * l_RateSetupData.getCom12();
				if (l_TotalFee < l_RateSetupData.getCom12Min()) {
					l_TotalFee = l_RateSetupData.getCom12Min();
				} else if (l_TotalFee > l_RateSetupData.getCom12Max()) {
					if (l_RateSetupData.getCom12Max() != 0) {
						l_TotalFee = l_RateSetupData.getCom12Max();
					}
				}
			} else {
				l_AmountRangeData = GeneralUtility.getRateByAmountRange(pTrAmount,
						l_RateSetupData.getCom12AmountRangeDatalist());
				l_TotalFee = pTrAmount * l_AmountRangeData.getCRate();
				if (l_TotalFee < l_AmountRangeData.getComMin()) {
					l_TotalFee = l_AmountRangeData.getComMin();
				} else if (l_TotalFee > l_AmountRangeData.getComMax()) {
					if (l_AmountRangeData.getComMax() != 0) {
						l_TotalFee = l_AmountRangeData.getComMax();
					}
				}
			}
		}
		return l_TotalFee;
	}

}
