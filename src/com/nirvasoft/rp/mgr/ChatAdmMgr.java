package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;

import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.rp.dao.ChatDao;
import com.nirvasoft.rp.data.ContantArr;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.shared.ChannelDataset;
import com.nirvasoft.rp.shared.FilterDataset;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.shared.TransferOutReq;
import com.nirvasoft.rp.shared.TransferOutRes;
import com.nirvasoft.rp.shared.WalletRequestData;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ServerGlobal;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class ChatAdmMgr {
	public ChannelDataset getChannelList(FilterDataset data) {
		ChannelDataset arr = new ChannelDataset();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			arr = new ChatDao().getChannelList(data, conn);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return arr;
	}

	public ContantArr getChatContact(String channelkey) {
		ContantArr arr = new ContantArr();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			arr = new ChatDao().getChatContact(conn, channelkey);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return arr;
	}
	
	@SuppressWarnings("unchecked")
	public static String registerChatUser(WalletRequestData req) throws Exception {
		String res = "";
		GeneralUtil.readCSServiceSetting();
		String aUrl = ServerGlobal.getCsURL() + "service001/registerUser";
		ClientConfig cc = new DefaultClientConfig();
		cc.getClasses().add(JacksonJsonProvider.class);
		Client client = Client.create(cc);
		WebResource webResource = client.resource(aUrl);
		ClientResponse response = null;
		JSONObject obj = new JSONObject();
		obj.put("t1", req.getUserID());
		obj.put("t2", req.getName());
		obj.put("t3", "DC001");
		obj.put("t9", "010");
		obj.put("t20", '0');
		response = webResource.type("application/json").post(ClientResponse.class, obj);
		String jsonStr = response.getEntity(String.class);
		JSONParser parser = new JSONParser();
		Object obj1 = null;
		obj1 = parser.parse(jsonStr);
		JSONObject jsonObject = (JSONObject) obj1;		
		if((Long) jsonObject.get("syskey") > 0){
			res = (String.valueOf((Long)jsonObject.get("syskey")));
		}
		return res;
	}
}
