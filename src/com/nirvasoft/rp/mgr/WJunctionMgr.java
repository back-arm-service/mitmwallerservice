package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.dao.WJunctionDao;
import com.nirvasoft.rp.shared.AccountListResponseData;
import com.nirvasoft.rp.shared.NewAccountRequestData;
import com.nirvasoft.rp.shared.NewAccountResponseData;
import com.nirvasoft.rp.shared.ResponseData;

public class WJunctionMgr {

	public NewAccountResponseData addNewAccount(NewAccountRequestData requestData) {
		NewAccountResponseData responseData = null;

		DAOManager daoDB = new DAOManager();
		Connection conn1 = null;

		WJunctionDao dao = new WJunctionDao();

		try {
			conn1 = daoDB.openConnection();

			int serialNo = dao.getSerialNo(requestData.getUserID(), conn1);

			if (serialNo > 0) {
				requestData.setSerialNo(++serialNo);

				responseData = dao.addNewAccount(requestData, conn1);
			} else {
				responseData = new NewAccountResponseData();
				responseData.setCode("0014");
				responseData.setDesc("Invalid serial number!");
			}
		} catch (Exception e) {
			e.printStackTrace();

			responseData = new NewAccountResponseData();
			responseData.setCode("0014");
			responseData.setDesc(e.getMessage());
		} finally {
			try {
				if (conn1 != null) {
					if (!conn1.isClosed()) {
						conn1.close();
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();

				responseData = new NewAccountResponseData();
				responseData.setCode("0014");
				responseData.setDesc(e.getMessage());
			}
		}

		return responseData;
	}

	public AccountListResponseData getAccountList(String userID) {
		AccountListResponseData responseData = null;

		DAOManager daoDB = new DAOManager();
		Connection conn1 = null;

		WJunctionDao dao = new WJunctionDao();

		try {
			conn1 = daoDB.openConnection();

			responseData = dao.getAccountList(userID, conn1);
		} catch (Exception e) {
			e.printStackTrace();

			responseData = new AccountListResponseData();
			responseData.setCode("0014");
			responseData.setDesc(e.getMessage());
		} finally {
			try {
				if (conn1 != null) {
					if (!conn1.isClosed()) {
						conn1.close();
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();

				responseData = new AccountListResponseData();
				responseData.setCode("0014");
				responseData.setDesc(e.getMessage());
			}
		}

		return responseData;
	}
	
	public ResponseData checkAccNoByUserID(String userID, String accNo) {
		ResponseData ret = new ResponseData();
		DAOManager daoDB = new DAOManager();
		Connection conn = null;
		WJunctionDao dao = new WJunctionDao();
		try {
			conn = daoDB.openConnection();
			if(dao.checkAccNoByUserID(userID, accNo, conn)){
				ret.setCode("0000");
				ret.setDesc("Success");
			} else {
				ret.setCode("0014");
				ret.setDesc("Invalid Request");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return ret;
	}

}
