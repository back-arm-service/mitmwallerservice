package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.nirvasoft.mfi.dao.MobileMemberDao;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.dao.UserRegistrationDao;
import com.nirvasoft.rp.dao.icbs.AccountDao;
import com.nirvasoft.rp.data.CityStateData;
import com.nirvasoft.rp.data.RegisterRequestData;
import com.nirvasoft.rp.data.RegisterResponseData;
import com.nirvasoft.rp.data.UserInfoResponse;
import com.nirvasoft.rp.framework.LOVDetails;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.shared.RegisterListResponseData;
import com.nirvasoft.rp.shared.StateListResponseData;
import com.nirvasoft.rp.shared.SyskeyRequestData;
import com.nirvasoft.rp.shared.SyskeyResponseData;
import com.nirvasoft.rp.shared.WalletRequestData;
import com.nirvasoft.rp.shared.WalletResponseData;

public class UserRegistrationMgr {

	public boolean checkUser(String request) {
		UserRegistrationDao l_dao = new UserRegistrationDao();
		DAOManager dao = new DAOManager();
		boolean response = false;
		Connection conn = null;

		try {
			conn = dao.openConnection();
			response = l_dao.checkUser(request, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return response;
	}
	
	public WalletResponseData checkUserByUserID(String userID) {
		UserRegistrationDao l_dao = new UserRegistrationDao();
		DAOManager dao = new DAOManager();
		WalletResponseData response = new WalletResponseData();
		Connection conn = null;

		try {
			conn = dao.openConnection();
			response = l_dao.checkUserByUserID(userID, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return response;
	}

	public WalletResponseData checkUserRegistration(WalletRequestData request) {
		UserRegistrationDao l_dao = new UserRegistrationDao();
		DAOManager dao = new DAOManager();
		WalletResponseData response = new WalletResponseData();
		Connection conn = null;
		Connection conn2 = null;

		try {
			conn = dao.openConnection();
			response = l_dao.checkUserRegistrationV2(request, conn);

			if (!response.getAccountNo().equalsIgnoreCase("")) {
				if (response.getRegion().contains(request.getRegion()) && l_dao.checkUUID(request, conn)) {
					response.setMessageCode("0000");
					response.setMessageDesc("Success");
					response.setType("User registered and same uuid");
				}

				if (response.getRegion().contains(request.getRegion()) && !l_dao.checkUUID(request, conn)) {
					response.setMessageCode("0001");
					response.setMessageDesc("Success");
					response.setType("User registered and diff uuid");
				}

				if (!response.getRegion().contains(request.getRegion()) && l_dao.checkUUID(request, conn)) {
					response.setMessageCode("0002");
					response.setMessageDesc("Success");
					response.setType("User not registered and same uuid");
					response.setAlreadyRegister("Your mobile number is already registered.");

					String oldRegion = response.getRegion();
					String oldRegionNumber = response.getRegionNumber();
					String newRegion = request.getRegion();
					l_dao.updateRegion(oldRegion, oldRegionNumber, newRegion, response.getsKey(), conn);

					String[] channelSysKeys = l_dao.getChannelSysKey(newRegion, conn);
					response.setChannels(channelSysKeys);
				}

				if (!response.getRegion().contains(request.getRegion()) && !l_dao.checkUUID(request, conn)) {
					response.setMessageCode("0003");
					response.setMessageDesc("Success");
					response.setType("User not registered and diff uuid");
					response.setAlreadyRegister("Your mobile number is already registered.");

					String oldRegion = response.getRegion();
					String oldRegionNumber = response.getRegionNumber();
					String newRegion = request.getRegion();
					l_dao.updateRegion(oldRegion, oldRegionNumber, newRegion, response.getsKey(), conn);

					String[] channelSysKeys = l_dao.getChannelSysKey(newRegion, conn);
					response.setChannels(channelSysKeys);
				}

				String region = request.getRegion();
				String regionNumber = "";

				if (region.equalsIgnoreCase("MANDALAY")) {
					regionNumber = "10000000";
				} else if (region.equalsIgnoreCase("YANGON")) {
					regionNumber = "13000000";
				} else {
					regionNumber = "00000000";
				}

				response.setRegion(region);
				response.setRegionNumber(regionNumber);

				conn2 = dao.openICBSConnection();
				response.setBalance(new AccountDao().getBalanceByIDV2(response.getAccountNo(), conn2));
			} else {
				response.setMessageCode("0009");
				response.setMessageDesc("Success");
				response.setType("unregistration");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.setMessageCode("0014");
			response.setMessageDesc(e.getMessage());
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
				if (conn2 != null) {
					if (!conn2.isClosed())
						conn2.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				response.setMessageCode("0014");
				response.setMessageDesc(e.getMessage());
			}
		}

		return response;
	}
	
	public WalletResponseData checkUserRegistrationV2(WalletRequestData request) {
		UserRegistrationDao l_dao = new UserRegistrationDao();
		DAOManager dao = new DAOManager();
		WalletResponseData response = new WalletResponseData();
		Connection conn = null;
		Connection conn2 = null;
		Result res = new Result();
		try {
			conn = dao.openConnection();
			response = l_dao.checkUserRegistrationV2(request, conn);
			if (!response.getAccountNo().equalsIgnoreCase("")) {
				res = new MobileMemberDao().checkPhoneNo(request.getUserID(), conn);
				if (res.isState() == false) {
					response.setAppSyskey(res.getSyskey());
				}else{
					response.setAppSyskey(0);
				}
					response.setMessageCode("0000");
					response.setMessageDesc("Success");
					String region = request.getRegion();
					String regionNumber = "";
					regionNumber = "00000000";
					response.setRegion(region);
					response.setRegionNumber(regionNumber);
					conn2 = dao.openICBSConnection();
					response.setBalance(new AccountDao().getBalanceByIDV2(response.getAccountNo(), conn2));				
			} else {
				response.setMessageCode("0009");
				response.setMessageDesc("Success");
				response.setType("unregistration");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setMessageCode("0014");
			response.setMessageDesc(e.getMessage());
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
				if (conn2 != null) {
					if (!conn2.isClosed())
						conn2.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				response.setMessageCode("0014");
				response.setMessageDesc(e.getMessage());
			}
		}

		return response;
	}

	public RegisterListResponseData getAllRegister() {
		RegisterListResponseData res = new RegisterListResponseData();
		DAOManager dao = new DAOManager();
		Connection conn = null;
		UserRegistrationDao userDAO = new UserRegistrationDao();
		List<RegisterResponseData> reslist = null;

		try {
			conn = dao.openConnection();
			reslist = userDAO.getAllRegister(conn);
		} catch (Exception e) {
			reslist = null;

			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				reslist = null;

				e.printStackTrace();
			}
		}

		if (reslist != null && reslist.size() != 0) {
			RegisterResponseData resArr[] = new RegisterResponseData[reslist.size()];

			for (int i = 0; i < reslist.size(); i++) {
				RegisterResponseData response = new RegisterResponseData();

				response.setSyskey(reslist.get(i).getSyskey());
				response.setCreatedDate(reslist.get(i).getCreatedDate());
				response.setModifiedDate(reslist.get(i).getModifiedDate());
				response.setUserID(reslist.get(i).getUserID());
				response.setUserName(reslist.get(i).getUserName());
				response.setSyskey(reslist.get(i).getSyskey());
				response.setT1(reslist.get(i).getT1());
				response.setT2(reslist.get(i).getT2());
				response.setT3(reslist.get(i).getT3());
				response.setT4(reslist.get(i).getT4());
				response.setT5(reslist.get(i).getT5());
				response.setT6(reslist.get(i).getT6());
				response.setT7(reslist.get(i).getT7());
				response.setT8(reslist.get(i).getT8());
				response.setT9(reslist.get(i).getT9());
				response.setT10(reslist.get(i).getT10());
				response.setT11(reslist.get(i).getT11());
				response.setT12(reslist.get(i).getT12());
				response.setT13(reslist.get(i).getT13());
				response.setT14(reslist.get(i).getT14());
				response.setT15(reslist.get(i).getT15());
				response.setT16(reslist.get(i).getT16());
				response.setT17(reslist.get(i).getT17());
				response.setT18(reslist.get(i).getT18());
				response.setT19(reslist.get(i).getT19());
				response.setT20(reslist.get(i).getT20());
				response.setT21(reslist.get(i).getT21());
				response.setT22(reslist.get(i).getT22());
				response.setT23(reslist.get(i).getT23());
				response.setT24(reslist.get(i).getT24());
				response.setT25(reslist.get(i).getT25());
				response.setT26(reslist.get(i).getT26());
				response.setT27(reslist.get(i).getT27());
				response.setT28(reslist.get(i).getT28());
				response.setT29(reslist.get(i).getT29());
				response.setT30(reslist.get(i).getT30());
				response.setT31(reslist.get(i).getT31());
				response.setT32(reslist.get(i).getT32());
				response.setT33(reslist.get(i).getT33());
				response.setT34(reslist.get(i).getT34());
				response.setT35(reslist.get(i).getT35());
				response.setT36(reslist.get(i).getT36());
				response.setT37(reslist.get(i).getT37());
				response.setT38(reslist.get(i).getT38());
				response.setT39(reslist.get(i).getT39());
				response.setT40(reslist.get(i).getT40());
				response.setT41(reslist.get(i).getT41());
				response.setT42(reslist.get(i).getT42());
				response.setT43(reslist.get(i).getT43());
				response.setT44(reslist.get(i).getT44());
				response.setT45(reslist.get(i).getT45());
				response.setT46(reslist.get(i).getT46());
				response.setT47(reslist.get(i).getT47());
				response.setT48(reslist.get(i).getT48());
				response.setT49(reslist.get(i).getT49());
				response.setT50(reslist.get(i).getT50());
				response.setUsersyskey(reslist.get(i).getUsersyskey());
				response.setT51(reslist.get(i).getT51());
				response.setT52(reslist.get(i).getT52());
				response.setT53(reslist.get(i).getT53());
				response.setT54(reslist.get(i).getT54());
				response.setT55(reslist.get(i).getT55());
				response.setT56(reslist.get(i).getT56());
				response.setT57(reslist.get(i).getT57());
				response.setT58(reslist.get(i).getT58());
				response.setT59(reslist.get(i).getT59());
				response.setT60(reslist.get(i).getT60());
				response.setT61(reslist.get(i).getT61());
				response.setT62(reslist.get(i).getT62());
				response.setT63(reslist.get(i).getT63());
				response.setT64(reslist.get(i).getT64());
				response.setT65(reslist.get(i).getT65());
				response.setT66(reslist.get(i).getT66());
				response.setT67(reslist.get(i).getT67());
				response.setT68(reslist.get(i).getT68());
				response.setT69(reslist.get(i).getT69());
				response.setT70(reslist.get(i).getT70());
				response.setT71(reslist.get(i).getT71());
				response.setT72(reslist.get(i).getT72());
				response.setT73(reslist.get(i).getT73());
				response.setT74(reslist.get(i).getT74());
				response.setT75(reslist.get(i).getT75());
				response.setT76(reslist.get(i).getT76());
				response.setT77(reslist.get(i).getT77());
				response.setT78(reslist.get(i).getT78());
				response.setT79(reslist.get(i).getT79());
				response.setT80(reslist.get(i).getT80());

				resArr[i] = response;
			}

			res.setCode("0000");
			res.setDesc("Getting State List Success.");
			res.setResList(resArr);
		} else {
			res.setCode("0014");
			res.setDesc("Getting State List Fail.");
		}

		return res;
	}

	public WalletResponseData getName(WalletRequestData request) {
		UserRegistrationDao l_dao = new UserRegistrationDao();
		DAOManager dao = new DAOManager();
		WalletResponseData response = new WalletResponseData();
		Connection conn = null;

		try {
			conn = dao.openConnection();
			response = l_dao.getName(request, conn);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.setMessageCode("0014");
			response.setMessageDesc(e.getMessage());
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				response.setMessageCode("0014");
				response.setMessageDesc(e.getMessage());
			}
		}

		return response;
	}

	// t7=region(E.G. "YANGON", "MANDALAY", or "BLANK")
	public RegisterResponseData getRegisterDC(long sysKey) {
		Connection conn = null;
		RegisterResponseData response = null;

		try {
			DAOManager dao = new DAOManager();
			conn = dao.openConnection();
			response = new UserRegistrationDao().getRegisterDC(sysKey, conn);
		} catch (Exception e) {
			response = null;

			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				response = null;

				e.printStackTrace();
			}
		}

		if (response == null) {
			response = new RegisterResponseData();
			response.setMessageCode("0014");
			response.setMessageDesc("Getting user information was unsuccessful.");
		}

		return response;
	}

	public StateListResponseData getStateList() {
		StateListResponseData responseStateList = new StateListResponseData();
		DAOManager dao = new DAOManager();
		Connection conn = null;
		UserRegistrationDao userDAO = new UserRegistrationDao();
		List<CityStateData> stateList = null;

		try {
			conn = dao.openConnection();
			stateList = userDAO.getStateList(conn);
		} catch (Exception e) {
			stateList = null;

			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				stateList = null;

				e.printStackTrace();
			}
		}

		if (stateList != null && stateList.size() != 0) {
			LOVDetails stateArr[] = new LOVDetails[stateList.size()];

			for (int i = 0; i < stateList.size(); i++) {
				LOVDetails state = new LOVDetails();
				state.setValue(stateList.get(i).getCode());
				state.setCaption(stateList.get(i).getDespEng());

				stateArr[i] = state;
			}

			responseStateList.setCode("0000");
			responseStateList.setDesc("Getting State List Success.");
			responseStateList.setStateList(stateArr);
		} else {
			responseStateList.setCode("0014");
			responseStateList.setDesc("Getting State List Fail.");
		}

		return responseStateList;
	}

	public StateListResponseData getStateListDC() {
		StateListResponseData responseStateList = new StateListResponseData();
		DAOManager dao = new DAOManager();
		Connection conn = null;
		UserRegistrationDao userDAO = new UserRegistrationDao();
		List<CityStateData> stateList = null;

		try {
			conn = dao.openConnection();
			stateList = userDAO.getStateListDC(conn);
		} catch (Exception e) {
			stateList = null;

			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				stateList = null;

				e.printStackTrace();
			}
		}

		if (stateList != null && stateList.size() != 0) {
			LOVDetails stateArr[] = new LOVDetails[stateList.size()];

			for (int i = 0; i < stateList.size(); i++) {
				LOVDetails state = new LOVDetails();
				state.setValue(stateList.get(i).getCode());
				state.setCaption(stateList.get(i).getDespEng());

				stateArr[i] = state;
			}

			responseStateList.setCode("0000");
			responseStateList.setDesc("Getting State List Success.");
			responseStateList.setStateList(stateArr);
		} else {
			responseStateList.setCode("0014");
			responseStateList.setDesc("Getting State List Fail.");
		}

		return responseStateList;
	}

	public SyskeyResponseData getTosyskey(SyskeyRequestData request) {// wcs
		Connection conn = null;
		SyskeyResponseData response = null;

		try {
			DAOManager dao = new DAOManager();
			conn = dao.openConnection();
			response = new UserRegistrationDao().getTosyskey(request.getBeneficiaryID(), request.getSessionID(), conn);
		} catch (Exception e) {
			response = null;

			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				response = null;

				e.printStackTrace();
			}
		}

		if (response == null) {
			response = new SyskeyResponseData();
			response.setMessageCode("0014");
			response.setMessageDesc("Getting user information was unsuccessful.");
		}

		return response;
	}

	// t7=region(E.G. "YANGON", "YANGON,MANDALAY")
	public RegisterResponseData registerSelect(long sysKey, String t7) {
		Connection conn = null;
		RegisterResponseData response = null;

		try {
			DAOManager dao = new DAOManager();
			conn = dao.openConnection();
			response = new UserRegistrationDao().registerSelect(sysKey, t7, conn);
		} catch (Exception e) {
			response = null;

			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				response = null;

				e.printStackTrace();
			}
		}

		if (response == null) {
			response = new RegisterResponseData();
			response.setMessageCode("0014");
			response.setMessageDesc("Getting user information was unsuccessful.");
		}

		return response;
	}

	public RegisterResponseData registerUpdate(RegisterRequestData request) {
		Connection conn = null;
		RegisterResponseData response = null;

		try {
			DAOManager dao = new DAOManager();
			conn = dao.openConnection();

			// request.setUsersyskey(request.getSyskey());

			response = new UserRegistrationDao().registerUpdate(request, conn);

			// for checking admin has been approved or not.
			// later we can add others too.
			RegisterResponseData responseForT38And7And36 = new UserRegistrationDao()
					.registerSelect(Long.parseLong(request.getHautosys()), request.getT7(), conn);

			response.setT7(responseForT38And7And36.getT7());
			response.setT36(responseForT38And7And36.getT36());
			response.setT38(responseForT38And7And36.getT38());
		} catch (Exception e) {
			response = null;

			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				response = null;

				e.printStackTrace();
			}
		}

		if (response == null) {
			response = new RegisterResponseData();
			response.setMessageCode("0014");
			response.setMessageDesc("User updates failed.");
		}

		return response;
	}

	public WalletResponseData resetUser(String userid, int regtype) {
		UserRegistrationDao l_dao = new UserRegistrationDao();
		DAOManager dao = new DAOManager();
		WalletResponseData ret = new WalletResponseData();
		Connection conn = null;

		boolean res = false;
		try {
			conn = dao.openConnection();
			res = l_dao.resetUser(userid, conn);
			if (res) {
				ret.setMessageCode("0000");
				ret.setMessageDesc("Success");
			} else {
				ret.setMessageCode("0014");
				ret.setMessageDesc("Fail!");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ret.setMessageCode("0014");
			ret.setMessageDesc(e.getMessage());

		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				ret.setMessageCode("0014");
				ret.setMessageDesc(e.getMessage());
			}
		}
		return ret;
	}

	public RegisterResponseData saveProfile(RegisterRequestData request) {
		Connection conn = null;
		RegisterResponseData response = null;

		try {
			DAOManager dao = new DAOManager();
			conn = dao.openConnection();

			response = new UserRegistrationDao().saveProfileUserRegistration(request, conn);

			if (response.getMessageCode().equals("0000")) {
				response = new UserRegistrationDao().saveProfileRegister(request, conn);
			}
			if (response.getMessageCode().equals("0000")) {
				response = new UserRegistrationDao().saveProfileContact(request, conn);
			}
		} catch (Exception e) {
			response = null;

			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				response = null;

				e.printStackTrace();
			}
		}

		if (response == null) {
			response = new RegisterResponseData();
			response.setMessageCode("0014");
			response.setMessageDesc("User updates failed.");
		}

		return response;
	}

	public WalletResponseData saveUserRegistration(String accNumber, String customerid, String userid, String sessionid,
			String phoneNo, String cusName, String nrc, String instututionCode, int regType, String chatapiskey,
			String region, String phoneType, String deviceId, String password, String appVersion, String regionNumber,
			Connection conn) {
		UserRegistrationDao l_dao = new UserRegistrationDao();
		WalletResponseData ret = new WalletResponseData();
		String resKey = "";

		try {
			resKey = l_dao.saveUserRegistration(accNumber, customerid, userid, sessionid, phoneNo, cusName, nrc,
					regType, instututionCode, chatapiskey, region, phoneType, deviceId, password, appVersion,
					regionNumber, conn);

			if (!resKey.equals("")) {
				ret.setMessageCode("0000");
				ret.setMessageDesc("Success");
				ret.setsKey(resKey);
				ret.setUserId(userid);
				ret.setName(cusName);
				ret.setT16("user-icon.png");
				ret.setRegion("");
				ret.setRegionNumber("");
			} else {
				ret.setMessageCode("0014");
				ret.setMessageDesc("Fail User Register");
				ret.setsKey(resKey);
			}
		} catch (Exception e) {
			e.printStackTrace();
			ret.setMessageCode("0014");
			ret.setMessageDesc(e.getMessage());
		}
		return ret;
	}

	public UserInfoResponse selectUserInfo() {
		Connection conn = null;
		UserInfoResponse response = new UserInfoResponse();

		try {
			DAOManager dao = new DAOManager();
			conn = dao.openConnection();

			response = new UserRegistrationDao().selectUserInfo(conn);
		} catch (Exception e) {
			response.setMessageCode("0014");
			response.setMessageDesc("Fail");
			response.setList(null);

			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				response.setMessageCode("0014");
				response.setMessageDesc("Fail");
				response.setList(null);

				e.printStackTrace();
			}
		}

		return response;
	}

	public WalletResponseData updateDeviceId(WalletRequestData request) {
		UserRegistrationDao l_dao = new UserRegistrationDao();
		DAOManager dao = new DAOManager();
		WalletResponseData response = new WalletResponseData();
		Connection conn = null;

		try {
			conn = dao.openConnection();

			boolean updateIsOk = l_dao.saveNewDifferentUUID(request, conn);

			if (!updateIsOk) {
				response.setMessageCode("0014");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.setMessageCode("0014");
			response.setMessageDesc(e.getMessage());
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				response.setMessageCode("0014");
				response.setMessageDesc(e.getMessage());
			}
		}

		return response;
	}

	public WalletResponseData updateDeviceIdDC(WalletRequestData request) {
		WalletResponseData response = new WalletResponseData();

		DAOManager dao = new DAOManager();
		UserRegistrationDao l_dao = new UserRegistrationDao();

		Connection conn = null;

		try {
			conn = dao.openConnection();

			boolean updateIsOk = l_dao.updateDeviceIdDC(request, conn);

			if (!updateIsOk) {
				response.setMessageCode("0014");
				response.setMessageDesc("Updated Device Id fail.");
			} else {
				response.setMessageCode("0000");
				response.setMessageDesc("Updated Device Id success.");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			response.setMessageCode("0014");
			response.setMessageDesc(e.getMessage());
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				response.setMessageCode("0014");
				response.setMessageDesc(e.getMessage());
			}
		}

		return response;
	}

}
