package com.nirvasoft.rp.mgr;

import java.sql.Connection;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.dao.MerchantDao;
import com.nirvasoft.rp.dao.WJunctionDao;
import com.nirvasoft.rp.dao.WalletTransactionDao;
import com.nirvasoft.rp.dao.icbs.AccountDao;
import com.nirvasoft.rp.dao.icbs.GLDao;
import com.nirvasoft.rp.data.MerchantFeatureData;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.shared.Response;
import com.nirvasoft.rp.shared.TransferOutReq;
import com.nirvasoft.rp.shared.TransferOutRes;
import com.nirvasoft.rp.shared.WJunction;

public class PaymentMgr {

	public TransferOutRes payment(TransferOutReq request) {
		TransferOutRes response = new TransferOutRes();
		TransferMgr trmgr = new TransferMgr();
		DAOManager daomgr = new DAOManager();
		Response res = new Response();
		Connection conn1 = null;
		Connection conn2 = null;
		WJunction wJunction = new WJunction();
		
		try {
			conn1 = ConnAdmin.getConn();
			wJunction = new WJunctionDao().getDataByID(request.getUserID(), conn1);
			
			conn2 = daomgr.openICBSConnection();
			double curBalCustomer = 0;
			curBalCustomer = new AccountDao().getBalanceByIDV2(wJunction.getAccNumber(), conn2);
			if (curBalCustomer < request.getAmount()) {
				response.setCode("0014");
				response.setDesc("Incifficient balance!");
				return response;
			}
			
			MerchantFeatureData merchantAccData = new MerchantFeatureData();
			merchantAccData = new MerchantDao().getMerchantAcc(request.getMerchantID(), conn1);
			String merchantAcc = "";
			merchantAcc = merchantAccData.getAccountNumber();
			
			boolean isGLAccount = new GLDao().isGLAccount(merchantAcc, conn1);
			double curBalMerchant = 0;
			if(!isGLAccount){
				curBalMerchant = new AccountDao().getBalanceByIDV2(merchantAcc, conn2);
			}
			
			res = trmgr.payment(wJunction.getAccNumber(), merchantAcc, request.getUserID(), request.getAmount(),
					request.getPaymentType(), curBalCustomer, curBalMerchant, isGLAccount, conn2);
			
			if (res.getMessageCode().equals("0000")) {
				response.setBankRefNo(res.getTrnRefNo());
				request.setBeneficiaryID(merchantAcc);
				new WalletTransactionDao().save(new TransferMgr().updateWalletDataCr(request, res.getTrnRefNo(), "", "2"), conn1);
					response.setCode("0000");
					response.setDesc("Payment is successful");	
			} else {
				response.setCode("0014");
				response.setDesc("Payment failed.");
			}
		} catch (Exception e) {			
			response.setCode("0014");
			response.setDesc("Payment failed.");
			e.printStackTrace();
		} finally {
			com.nirvasoft.rp.util.ServerUtil.closeConnection(conn1);
			com.nirvasoft.rp.util.ServerUtil.closeConnection(conn2);
		}
		
		return response;
	}
	
	public String getToAccount(String merchantid) {
		String response = "";
		DAOManager daomgr = new DAOManager();
		Connection conn1 = null;
		Connection conn2 = null;
		
		try {
			conn1 = daomgr.openConnection();			
			MerchantFeatureData merchantAccData = new MerchantFeatureData();
			merchantAccData = new MerchantDao().getMerchantAccV2(merchantid, conn1);
			String merchantAcc = "";
			response = merchantAccData.getAccountNumber();
			
			/*boolean isGLAccount = new GLDao().isGLAccount(merchantAcc, conn2);
			double curBalMerchant = 0;
			if(!isGLAccount){
				curBalMerchant = new AccountDao().getBalanceByIDV2(merchantAcc, conn2);
			}*/
		} catch (Exception e) {						
			e.printStackTrace();
		} finally {
			com.nirvasoft.rp.util.ServerUtil.closeConnection(conn1);
		}
		
		return response;
	}

}
