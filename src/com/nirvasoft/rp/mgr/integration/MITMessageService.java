
package com.nirvasoft.rp.mgr.integration;

import java.net.URL;
import java.security.GeneralSecurityException;
import java.sql.Connection;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.nirvasoft.gateway.messaging.webservice.MessagingGatewayProxy;
import com.nirvasoft.gateway.messaging.webservice.Response;
import com.nirvasoft.rp.dao.integration.MessageDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.shared.integration.MessageData;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.SMSUtility;
import com.nirvasoft.rp.util.TimeBasedOneTimePasswordUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class MITMessageService {
	static String oleOptValue = "";
	static String currentOptValue = "";

	public static int checkOTP(String otpValue, String userID, String phoneNo) throws GeneralSecurityException {
		String secretValue = userID + phoneNo;
		String code = null;
		try {
		} catch (NumberFormatException e) {
			long codes;
			return 3;
		}
		long timeWindow = System.currentTimeMillis() / 1000L;
		for (long i = timeWindow; i >= timeWindow - Integer.parseInt(SMSUtility.StepSeconds); i -= 1L) {
			code = TimeBasedOneTimePasswordUtil.generateOTPNumber(secretValue, i);
			if (code.equals(otpValue)) {
				GeneralUtil.writeLog("SMSLog", "OTP Checking", "Phone No : " + phoneNo + " SUCCESS");
				return 0;
			}
		}
		GeneralUtil.writeLog("SMSLog", "OTP Checking", "Phone No : " + phoneNo + " FAIL");
		return 1;
	}

	public static int checkOTP1(String otpValue, String userID, String phoneNo) throws Exception {
		String secretValue = userID + phoneNo;
		String code = TimeBasedOneTimePasswordUtil.generateCurrentNumber(secretValue);
		if (secretValue.equals(code)) {
			if (code.equals(otpValue)) {
				return 0;
			}
			return 1;
		}
		return 2;
	}

	public static String getOTP(String userID, String phoneNo) throws GeneralSecurityException {
		String secretValue = userID + phoneNo;
		String base32Secret = "";
		if ((secretValue.equals("")) || (secretValue.equals(null))) {
			base32Secret = "MITSECRET";
		} else {
			base32Secret = secretValue;
		}
		String code = TimeBasedOneTimePasswordUtil.generateCurrentNumber(base32Secret);
		if (!currentOptValue.equals(code)) {
			oleOptValue = currentOptValue;
			currentOptValue = code;
		}
		if (phoneNo.length() > 0) {
			String message = SMSUtility.OTPMessage.replace("<OTPCode>", code);
			System.out.println(message);
			GeneralUtil.writeLog("SMSLog", "Information", "Phone No :" + phoneNo + "   Otp Value : ******");
			String smsstatus = MITMessageService.sendSMS(phoneNo, message, "text");
			if (smsstatus.toLowerCase().contains("success")) {
				GeneralUtil.writeLog("SMSLog", "Information", "Sending SMS SUCCESS for Phone No :" + phoneNo);
				smsstatus = "";
				return code;
			}
			GeneralUtil.writeLog("SMSLog", "Error",
					"Sending SMS Fail for Phone No :" + phoneNo + " caused by " + smsstatus);
			return "000000";
		}
		return "There is no Phone No.";
	}

	public static boolean isEmpty(String str) {
		return (str == null) || (str.isEmpty());
	}

	public static void printResult(Response res) {
		if (!isEmpty(res.getRESULT())) {
			System.out.println("Result = " + res.getRESULT());
		}
		if (!isEmpty(res.getKEY())) {
			System.out.println("Key = " + res.getKEY());
		}
		if (!isEmpty(res.getERRORCODE())) {
			System.out.println("Error Code = " + res.getERRORCODE());
			GeneralUtil.writeLog("SMSLog", "Error", "SMS Send Fail\tError Code >>>" + res.getERRORCODE());
		}
		if (!isEmpty(res.getERRORDESC())) {
			System.out.println("Error Description = " + res.getERRORDESC());
			GeneralUtil.writeLog("SMSLog", "Error", "SMS Send Fail\tError Code >>>" + res.getERRORDESC());
		}
	}

	private static boolean saveMsgData(MessageData data) throws Exception {
		boolean res = false;
		Connection conn = null;
		conn = ConnAdmin.getConn("001", "");
		MessageDao msgDao = new MessageDao();
		res = msgDao.saveMessageData(data, conn);			
		return res;

	}

	public static String sendMessage(String phoneNo, String message) throws GeneralSecurityException {
		String smsstatus = MITMessageService.sendSMS(phoneNo, message, "text");
		if (smsstatus.toLowerCase().contains("success")) {
			GeneralUtil.writeLog("SMSLog", "Information", "Sending SMS SUCCESS for Phone No :" + phoneNo);
			smsstatus = "";
			return "0000";
		}
		GeneralUtil.writeLog("SMSLog", "Error",
				"Sending SMS Fail for Phone No :" + phoneNo + " caused by " + smsstatus);
		return smsstatus;
	}

	public static String sendSMS(String phoneNo, String message, String type) {
		Response res = null;
		System.out.println("Prepare Data to Call SMS Service.....");
		GeneralUtil.writeLog("SMSLog", "Information", "Prepare to call SMS Service.");
		URL url = null;
		try {
			url = new URL(SMSUtility.URL);
			MessagingGatewayProxy proxy = new MessagingGatewayProxy();
			proxy.setEndpoint(url.toString());
			res = proxy.sendSMS(phoneNo, message, 1, SMSUtility.UserName, SMSUtility.Password, "", "", "", "", 0, "", 0,
					"");
			printResult(res);
		} catch (Exception e) {
			e.printStackTrace();
			GeneralUtil.writeLog("SMSLog", "Error", "Calling SMS Service Fail.");
			GeneralUtil.writeLog("SMSLog", "Error", e.getLocalizedMessage());
		}
		if (!isEmpty(res.getRESULT())) {
			return res.getRESULT();
		}
		return res.getERRORDESC();
	}
	
	@SuppressWarnings("unchecked")
	public static MessageData sendSMSApi(String phoneNo, String message) throws Exception {
		MessageData ret = new MessageData();
		Client client = Client.create();
		String url = "";
		if(SMSUtility.Type.equalsIgnoreCase("M")){
			url = SMSUtility.URL2 + "sendByModem";
		} else {
			url = SMSUtility.URL2 + "sendBySendQuick";
		}
		WebResource webResource = client.resource(url);
		ClientResponse response = null;
		JSONObject obj = new JSONObject();
		obj.put("phoneNo", "0"+ phoneNo.substring(3));
		obj.put("message", message);
		response = webResource.type("application/json").post(ClientResponse.class, obj.toString());
		String jsonStr = response.getEntity(String.class);
		JSONParser parser = new JSONParser();
		Object object = parser.parse(jsonStr);
		JSONObject jsonObject = (JSONObject) object;
		String status = String.valueOf(jsonObject.get("status"));
		String responseMessage = (String) jsonObject.get("responseMessage");
		ret.setStatus(status);
		ret.setResponseMessage(responseMessage);
		ret.setCreatedDate((String) jsonObject.get("createdDate"));
		ret.setDateReceived((String) jsonObject.get("dateReceived"));
		ret.setDateSent((String) jsonObject.get("dateSent"));
		ret.setDeliveryStatus((String) jsonObject.get("deliveryStatus"));
		ret.setMessage((String) jsonObject.get("message"));
		ret.setModifiedDate((String) jsonObject.get("modifiedDate"));
		ret.setResponseMessage((String) jsonObject.get("responseMessage"));
		ret.setToAddress((String) jsonObject.get("toAddress"));
		saveMsgData(ret);
		return ret;
	}
	
	public static Response sendSMSMessage(String phoneNo, String message, String type) {
		Response res = null;
		System.out.println("Prepare Data to Call SMS Service.....");
		GeneralUtil.writeLog("SMSLog", "Information", phoneNo + " : " + message);
		URL url = null;
		try {
			url = new URL(SMSUtility.URL);
			MessagingGatewayProxy proxy = new MessagingGatewayProxy();
			proxy.setEndpoint(url.toString());
			res = proxy.sendSMS(phoneNo, message, 1, SMSUtility.UserName, SMSUtility.Password, "", "", "", "", 0, "", 0,
					"");
			printResult(res);
		} catch (Exception e) {
			e.printStackTrace();
			GeneralUtil.writeLog("SMSLog", "Error", "Calling SMS Service Fail.");
			GeneralUtil.writeLog("SMSLog", "Error", e.getLocalizedMessage());
		}
		return res;
	}
}