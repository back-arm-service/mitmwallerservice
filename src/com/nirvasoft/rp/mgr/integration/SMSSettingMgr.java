package com.nirvasoft.rp.mgr.integration;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.cms.framework.Result;
import com.nirvasoft.rp.dao.integration.SMSSettingDao;
import com.nirvasoft.rp.data.MessageRequest;
import com.nirvasoft.rp.data.MessageResponse;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Lov3;
import com.nirvasoft.rp.shared.integration.SMSSettingData;
import com.nirvasoft.rp.shared.integration.SMSSettingDataSet;
import com.nirvasoft.rp.util.ServerUtil;

public class SMSSettingMgr {

	public Result deleteSMSSetting(long id) {

		Result ret = new Result();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			ret = new SMSSettingDao().deleteSMSSetting(id, conn);

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return ret;

	}

	public Lov3 getFunctionLov() {

		Lov3 lov3 = new Lov3();
		SMSSettingDao dao = new SMSSettingDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			lov3 = dao.getFunctionLov(conn);

		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();

			}
		}

		return lov3;

	}

	public Lov3 getServiceLov() {

		Lov3 lov3 = new Lov3();
		SMSSettingDao dao = new SMSSettingDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			lov3 = dao.getServiceLov(conn);

		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();

			}
		}

		return lov3;

	}

	public SMSSettingData getSMSSettingDataById(long id) {

		SMSSettingData ret = new SMSSettingData();
		SMSSettingDao dao = new SMSSettingDao();
		Connection l_Conn = null;

		try {

			// l_Conn = ConnAdmin.getAnotherConn("ConncetionConfig.txt", "001");
			l_Conn = ConnAdmin.getConn("001", "");
			ret = dao.getSMSSettingDataById(id, l_Conn);

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return ret;
	}

	public SMSSettingDataSet getSMSSettingList(String searchText, int pageSize, int currentPage) {
		SMSSettingDataSet res = new SMSSettingDataSet();
		SMSSettingDao m_dao = new SMSSettingDao();
		Connection conn = null;
		try {
			// conn = ConnAdmin.getAnotherConn("ConncetionConfig.txt", "001");
			conn = ConnAdmin.getConn("001", "");
			res = m_dao.getSMSSettingList(searchText, pageSize, currentPage, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return res;
	}

	public MessageResponse readMessageSetting(String service, String function, String merchantID, String sessionID,
			String userID) {
		MessageResponse rs = new MessageResponse();
		SMSSettingDao dao = new SMSSettingDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			if (dao.readMessageSetting(service, function, merchantID, conn)) {
				rs.setCode("0000");
				rs.setDesc("Success");
				rs.setrKey("true");
			} else {
				rs.setCode("0000");
				rs.setDesc("No OTP Service");
				rs.setrKey("false");
			}
		} catch (Exception e) {

			e.printStackTrace();

		} finally {

			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return rs;
	}
	public SMSSettingData getSMSNotiData(String service, String function,String merchantID) {
		MessageResponse rs = new MessageResponse();
		SMSSettingDao dao = new SMSSettingDao();
		SMSSettingData data=new SMSSettingData();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			data=dao.getSMSNotiData(service,function,merchantID,conn);
//			if (dao.readMessageSetting(service, function, merchantID, conn)) {
//				rs.setCode("0000");
//				rs.setDesc("Success");
//				rs.setrKey("true");
//			} else {
//				rs.setCode("0000");
//				rs.setDesc("No OTP Service");
//				rs.setrKey("false");
//			}
		} catch (Exception e) {

			e.printStackTrace();

		} finally {

			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return data;
	}

	public MessageResponse readNotiMessageSetting(MessageRequest data) {
		MessageResponse rs = new MessageResponse();
		SMSSettingDao dao = new SMSSettingDao();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			if (dao.readNotiMessageSetting(data,conn)) {
				rs.setCode("0000");
				rs.setDesc("Success Noti Service");
				rs.setrKey("true");
			} else {
				rs.setCode("0000");
				rs.setDesc("No Noti Serive");
				rs.setrKey("false");
			}
		} catch (Exception e) {

			e.printStackTrace();

		} finally {

			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return rs;
	}

	public Result saveSMSSetting(SMSSettingData data) {

		Result ret = new Result();
		SMSSettingDao dao = new SMSSettingDao();
		Connection conn = null;

		try {
			// l_Conn = ConnAdmin.getAnotherConn("ConncetionConfig.txt", "001");
			conn = ConnAdmin.getConn("001", "");
			if (data.getId() == 0) {
				ret = dao.saveSMSSetting(data, conn);
			} else {
				ret = dao.updateSMSSetting(data, conn);
			}

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return ret;

	}
}
