package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.rp.dao.CommRateDetailDao;
import com.nirvasoft.rp.dao.CommRateHeaderDao;
import com.nirvasoft.rp.data.CommRateDetailData;
import com.nirvasoft.rp.data.CommRateHeaderData;
import com.nirvasoft.rp.framework.ConnAdmin;

public class CommRateMgr {

	public CommRateHeaderData getComRatedataByComRefNo(String comRef) {
		CommRateHeaderData ret = new CommRateHeaderData();
		CommRateHeaderDao hdrDao = new CommRateHeaderDao();
		CommRateDetailDao dtlDao = new CommRateDetailDao();
		Connection conn = null;
		CommRateDetailData[] detailArr = null;

		conn = ConnAdmin.getConn("001", "");
		if (conn != null) {
			try {
				ret = hdrDao.getComRatedataByComRefNo(comRef, conn);

				detailArr = dtlDao.getComRatedataByHkey(ret.getSyskey(), comRef, conn);

				ret.setCommdetailArr(detailArr);

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("e.getMessage() " + e.getMessage());
			} finally {
				if (conn != null) {
					try {
						if (!conn.isClosed()) {
							conn.close();
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}
		} else {
			System.out.println("Connection is null");
		}

		return ret;
	}

}
