package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.dao.NotiSessionDao;
import com.nirvasoft.rp.dao.WalletTransactionDao;
import com.nirvasoft.rp.shared.NotiReq;
import com.nirvasoft.rp.shared.NotiRes;
import com.nirvasoft.rp.shared.NotiSession;
import com.nirvasoft.rp.shared.WalletTransaction;

public class NotiSessionMgr {

	public NotiRes getNotiCount(NotiReq req) {
		NotiRes ret = new NotiRes();
		Connection conn = null;
		DAOManager l_DAOMgr = new DAOManager();
		WalletTransactionDao wDao = new WalletTransactionDao();
		NotiSessionDao nDao = new NotiSessionDao();
		try {
			conn = l_DAOMgr.openConnection();
			int notiCount = wDao.getNotiCount(req.getUserID(), nDao.getMaxNotiKey(req.getUserID(), conn), conn);
			ret.setNotiCount(String.valueOf(notiCount));
			ret.setCode("0000");
			ret.setDesc("Success");
		} catch (Exception e) {
			ret.setCode("0014");
			ret.setDesc("Server Error!");
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					ret.setCode("0014");
					ret.setDesc("Server error.");
				}
			}
		}
		return ret;
	}

	public NotiRes getNotiData(NotiReq req) {
		NotiRes ret = new NotiRes();
		Connection conn = null;
		DAOManager l_DAOMgr = new DAOManager();
		WalletTransactionDao wDao = new WalletTransactionDao();
		NotiSessionDao nDao = new NotiSessionDao();

		try {
			conn = l_DAOMgr.openConnection();
			int notiCount = wDao.getNotiCount(req.getUserID(), nDao.getMaxNotiKey(req.getUserID(), conn), conn);
			if (notiCount > 0) {
				ArrayList<WalletTransaction> wtData = new ArrayList<WalletTransaction>();
				wtData = wDao.getNoti(req.getUserID(), notiCount, conn);
				nDao.delete(req.getUserID(), conn);
				nDao.save(updateNotiSessionData(wtData), conn);
			}
			ret.setNotiCount(String.valueOf(notiCount));
			ret.setData(nDao.getNoti(req.getUserID(), conn));
			if (ret.getData().length > 0) {
				ret.setCode("0000");
				ret.setDesc("Success");
			} else {
				ret.setCode("0014");
				ret.setDesc("No record found");
			}

		} catch (Exception e) {
			ret.setCode("0014");
			ret.setDesc("Server Error!");
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					ret.setCode("0014");
					ret.setDesc("Server error.");
				}
			}
		}
		return ret;
	}

	private ArrayList<NotiSession> updateNotiSessionData(ArrayList<WalletTransaction> wtData) {
		ArrayList<NotiSession> ret = new ArrayList<NotiSession>();
		for (int i = 0; i < wtData.size(); i++) {
			NotiSession data = new NotiSession();
			data.setNotiKey(wtData.get(i).getAutoKey());
			data.setUserID(wtData.get(i).getUserID());
			data.setNotiMessage(wtData.get(i).getNotiMessage());
			data.setNotiType(wtData.get(i).getNotiType());
			//data.setCreatedDate(wtData.get(i).getCreatedDate());
			ret.add(data);
		}
		return ret;
	}
}
