package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.DocumentData;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.rp.dao.DocDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.shared.DocumentDataset;
import com.nirvasoft.rp.shared.DocumentListingDataList;
import com.nirvasoft.rp.shared.FilterDataset;

public class DocMgr {
	// atn
	public static DocumentData setStatus(DocumentData data) {
		// String todayDate = new SimpleDateFormat("yyyyMMdd").format(new
		// Date());
		String todayDate = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
		if (data.getAutokey() == 0) {
			data.setCreatedDate(todayDate);
		}
		data.setModifiedDate(todayDate);
		if (data.getAutokey() == 0) {
			data.setModifiedby("");
		} else {
			data.setModifiedby(data.getModifiedby());
		}
		/*
		 * if (ServerUtil.isZawgyiEncoded(data.getT1())) {
		 * data.setT1(FontChangeUtil.zg2uni(data.getT1()));
		 * 
		 * }
		 */
		data.setRecordStatus(1);
		return data;
	}

	/* atn */
	/*
	 * public DocumentDataSet searchDocumentList(PagerData p,String
	 * userid,String statustype,MrBean user) { DocumentDataSet res = new
	 * DocumentDataSet(); Connection conn = null; try { conn =
	 * ConnAdmin.getConn("001", ""); res = new
	 * DocDao().searchDocumentList(p,userid,statustype,conn); } catch
	 * (SQLException e) { e.printStackTrace(); } return res; }
	 */
	/* atn */
	public Resultb2b deleteDoc(DocumentData data) {
		Resultb2b res = new Resultb2b();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			data = setStatus(data);
			res = new DocDao().deleteDoc(data, conn);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return res;
	}

	public DocumentData DocBysyskey(long key) {
		DocumentData res = new DocumentData();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = new DocDao().readDoc(key, conn);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return res;
	}

	/*public DocumentListingDataList getDocumentlist(FilterDataset p) {
		DocumentListingDataList res = new DocumentListingDataList();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = new DocDao().getDocumentlist(p, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}*/

	/* atn */
	/*public Resultb2b savedocument(DocumentData data) {
		Resultb2b res = new Resultb2b();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			data = setStatus(data);
			if (data.getAutokey() == 0) {
				res = new DocDao().insert(data, conn);

			} else {
				res = new DocDao().update(data, conn);
			}
			if (res.isState()) {
				res.setKeyResult(data.getAutokey());
				res.getLongResult().add(data.getAutokey());
				conn.commit();
			} else
				conn.rollback();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return res;
	}*/

	// DBS Function
	//
	/*
	 * public TransactionListingDataset getTransactionListingData(FilterDataset
	 * aFilterDataset) { Connection l_Conn = null; DocDao l_TransactionDAO = new
	 * DocDao();
	 * 
	 * TransactionListingDataset l_Dataset = new TransactionListingDataset();
	 * 
	 * try { // Create connection l_Conn = ConnAdmin.getConn("001", "");
	 * 
	 * // Customer Listing Data l_Dataset =
	 * l_TransactionDAO.getTransactionListingData(aFilterDataset, l_Conn); }
	 * catch(Exception e) { // Print stack trace e.printStackTrace(); } finally
	 * { try { // Close connection if(!l_Conn.isClosed()) l_Conn.close(); }
	 * catch (SQLException eSQL) { eSQL.printStackTrace(); } }
	 * 
	 * return l_Dataset; }
	 */
	/* atn */
	/*public DocumentDataset searchDocList(FilterDataset p, String userid, MrBean user) {
		DocumentDataset res = new DocumentDataset();
		Connection conn = null;
		try {
			conn = ConnAdmin.getConn("001", "");
			res = new DocDao().searchDocList(p, userid, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}*/

}
