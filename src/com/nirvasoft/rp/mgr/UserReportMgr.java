package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.rp.dao.UserReportDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Lov3;
import com.nirvasoft.rp.shared.UserDetailReportList;

public class UserReportMgr {

	/*
	 * public String getCustomerID(String userid,String branchCode) { String cif
	 * = ""; Connection l_Conn = null; UserReportDao l_DAO = new
	 * UserReportDao(); try { l_Conn = ConnAdmin.getConn("001", ""); cif =
	 * l_DAO.getCustomerID(userid,branchCode,"", l_Conn); } catch (Exception e)
	 * { e.printStackTrace(); } finally { try { if (!l_Conn.isClosed())
	 * l_Conn.close(); } catch (SQLException e) { e.printStackTrace(); } }
	 * return cif; }
	 */

	public UserDetailReportList getUserReportList(int currentPage, int totalCount, int pageSize, String aFromDate,
			String aToDate, String userName, String nrc, String phoneno, String usertype, String status,
			String branchCode, String userID, int click) {

		UserDetailReportList ret = new UserDetailReportList();

		Connection l_Conn = null;
		UserReportDao l_DAO = new UserReportDao();
		try {
			l_Conn = ConnAdmin.getConn("001", "");
			ret = l_DAO.getUserReportList(currentPage, totalCount, pageSize, aFromDate, aToDate, userName, nrc, phoneno,
					usertype, status, branchCode, userID, click, l_Conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return ret;
	}

	public Lov3 getUserType() {

		Lov3 lov3 = new Lov3();
		Connection conn = null;
		UserReportDao dao = new UserReportDao();

		try {
			conn = ConnAdmin.getConn("001", "");
			lov3 = dao.getUserType(conn);

		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();

			}
		}

		return lov3;

	}

	/*
	 * public String getBankName() {
	 * 
	 * String branchCode=""; Connection conn = null; BranchDao dao = new
	 * BranchDao();
	 * 
	 * try { conn = ConnAdmin.getConn("001", ""); branchCode =
	 * dao.getActiveBankName(conn);
	 * 
	 * } catch (Exception e) {
	 * 
	 * e.printStackTrace(); } finally { try { if (!conn.isClosed())
	 * conn.close(); } catch (SQLException e) { e.printStackTrace();
	 * 
	 * } }
	 * 
	 * return branchCode;
	 * 
	 * }
	 */

}
