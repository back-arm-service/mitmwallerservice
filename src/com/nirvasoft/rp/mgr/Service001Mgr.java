package com.nirvasoft.rp.mgr;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.rp.dao.Service001Dao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Profile;
import com.nirvasoft.rp.shared.AgentTransferReq;
import com.nirvasoft.rp.shared.MSigninRequestData;
import com.nirvasoft.rp.shared.MSigninResponseData;
import com.nirvasoft.rp.shared.MerchantResData;
import com.nirvasoft.rp.shared.ReferenceResData;
import com.nirvasoft.rp.util.Response;
import com.nirvasoft.rp.util.ServerUtil;

public class Service001Mgr {

	public static String[] getMainMenu(Profile p) {
		Connection con = ConnAdmin.getConn("001", "");
		ArrayList<String> arr = Service001Dao.getMainMenu(p, con);
		String[] ary = new String[arr.size()];
		for (int i = 0; i < arr.size(); i++) {
			ary[i] = arr.get(i);
		}

		return ary;

	}

	/*
	 * public String mobilelogin (MobileProfile mprofile){ String str="";
	 * 
	 * Connection con = null; Service001Dao service_dao = new Service001Dao();
	 * try { con = ConnAdmin.getConn("001", "");
	 * str=service_dao.mobilelogin(mprofile, con); return str; }catch(Exception
	 * e) { e.printStackTrace(); mprofile.setCode("0014"); mprofile.setDesc(
	 * "Contact to Bank Administrator");
	 * 
	 * } finally {
	 * 
	 * ServerUtil.closeConnection(con);
	 * 
	 * } return str; }
	 */

	public static String[] getSubMenuItem(Profile p, String parent) {
		Connection con = ConnAdmin.getConn("001", "");
		ArrayList<String> arr = Service001Dao.getSubMenuItem(p, con, parent);
		String[] ary = new String[arr.size()];
		for (int i = 0; i < arr.size(); i++) {
			ary[i] = arr.get(i);
		}

		return ary;

	}

	public Boolean checkForcePwdChange(MSigninRequestData p) {
		Boolean st = false;
		Service001Dao s_dao = new Service001Dao();
		Connection con = null;
		try {
			con = ConnAdmin.getConn("001", "");
			st = s_dao.checkForcePwdChange(p, con);
			return st;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(con);
		}
		return st;
	}

	/*
	 * //suwai public static String getMerchant(MrBean user) { Connection conn =
	 * ConnAdmin.getConn(user.getUser().getOrganizationID(),""); String res =
	 * ""; try { res = Service001Dao.getMerchant(conn); } catch (SQLException e)
	 * { e.printStackTrace(); } return res; }
	 */

	/* wkk */
	public MerchantResData getMerchant(String imageURL, String paymentType) {
		Connection conn = null;
		MerchantResData response = new MerchantResData();
		Service001Dao dao = new Service001Dao();

		try {
			conn = ConnAdmin.getConn("001", "");
			response = dao.getMerchant(imageURL, paymentType, conn);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}

		return response;
	}

	public String getMerchantByPC(String processingCode) {
		String ret = "";
		Service001Dao dao = new Service001Dao();
		Connection con = null;
		try {
			con = ConnAdmin.getConn("001", "");
			ret = dao.getMerchantByPC(processingCode, con);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(con);
		}
		return ret;
	}

	public ReferenceResData getReferenceData(String processingCode) {
		Connection conn = null;
		ReferenceResData response = new ReferenceResData();
		Service001Dao dao = new Service001Dao();
		try {
			conn = ConnAdmin.getConn("001", "");
			response = dao.getReferenceData(processingCode, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
		return response;
	}

	public String getUserName(String userName) {
		String st = "";

		Connection con = ConnAdmin.getConn("001", "");
		try {
			st = Service001Dao.getUserData(userName, con);
			return st;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(con);
		}
		return st;
	}

	public boolean isMasterUser(String userID) {
		Service001Dao dao = new Service001Dao();
		Connection con = ConnAdmin.getConn("001", "");
		Boolean res = false;
		try {
			res = dao.isMasterUser(userID, con);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(con);
		}
		return res;
	}

	public MSigninResponseData mobilelogin(MSigninRequestData mprofile) {
		MSigninResponseData res = new MSigninResponseData();

		Connection con = null;
		Service001Dao service_dao = new Service001Dao();
		try {
			con = ConnAdmin.getConn("001", "");
			res = service_dao.mobilelogin(mprofile, con);
		} catch (Exception e) {
			e.printStackTrace();
			res.setCode("0014");
			res.setDesc("Contact to Bank Administrator");

		} finally {

			ServerUtil.closeConnection(con);

		}
		return res;
	}

	public Response saveAgentTransaction(AgentTransferReq ret, boolean isEncash, String transDate) throws SQLException {
		Response response = new Response();
		Service001Dao dao = new Service001Dao();
		Connection conn = ConnAdmin.getConn("001", "");
		if (isEncash == false) {
			response = dao.saveAgentTransaction(ret, "AgentTransaction", transDate, conn);
		} else {
			response = dao.updateAgentTransaction(ret, "AgentTransaction", transDate, conn);
		}
		return response;

	}
	

}
