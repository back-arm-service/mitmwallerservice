package com.nirvasoft.rp.mgr.icbs;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import com.nirvasoft.fcpayment.core.data.fcchannel.DisPaymentTransactionData;
import com.nirvasoft.rp.dao.CMSMerchantAccDao;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.dao.DispaymentTransationDao;
import com.nirvasoft.rp.dao.HolidayDao;
import com.nirvasoft.rp.dao.icbs.AccountDao;
import com.nirvasoft.rp.dao.icbs.AccountGLTransactionDao;
import com.nirvasoft.rp.dao.icbs.AccountTransactionDao;
import com.nirvasoft.rp.dao.icbs.ICBSDao;
import com.nirvasoft.rp.data.MCommRateMappingData;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.mgr.ICBSServiceMgr;
import com.nirvasoft.rp.mgr.MCommRateMappingMgr;
import com.nirvasoft.rp.shared.AccTransferReq;
import com.nirvasoft.rp.shared.AccountActivityResponse;
import com.nirvasoft.rp.shared.CMSMerchantAccRefData;
import com.nirvasoft.rp.shared.ChequeEnquiryRes;
import com.nirvasoft.rp.shared.DepositAccDataResult;
import com.nirvasoft.rp.shared.EPIXResponse;
import com.nirvasoft.rp.shared.ResponseBalance;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.shared.icbs.AccountData;
import com.nirvasoft.rp.shared.icbs.AccountGLTransactionData;
import com.nirvasoft.rp.shared.icbs.AccountTransactionData;
import com.nirvasoft.rp.shared.icbs.AgentEncashData;
import com.nirvasoft.rp.shared.icbs.AgentResData;
import com.nirvasoft.rp.shared.icbs.GetOutstandingBillResData;
import com.nirvasoft.rp.shared.icbs.SMSReturnData;
import com.nirvasoft.rp.shared.icbs.SMSTransferData;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.GeneralUtility;
import com.nirvasoft.rp.util.Response;
import com.nirvasoft.rp.util.ServerGlobal;

public class ICBSMgr {

	public static String getEffectiveTransDate(String pDate, Connection conn) throws SQLException {
		HolidayDao dao = new HolidayDao();
		int year = Integer.parseInt(pDate.substring(0, 4));
		int month = Integer.parseInt(pDate.substring(4, 6));
		int day = Integer.parseInt(pDate.substring(6, 8));
		Calendar cal = Calendar.getInstance();
		month--; // In Java Calendar, month is starting zero.
		cal.set(year, month, day);
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		String effectiveDate = pDate;
		// Checking is Runned EOD
		if (dao.isRunnedEOD(pDate, conn)) {
			cal.clear();
			cal.set(year, month, ++day);
			effectiveDate = df.format(cal.getTime());
			effectiveDate = getEffectiveTransDate(effectiveDate, conn);
		} else if (GeneralUtil.isWeekEnd(pDate)) {
			cal.clear();
			cal.set(year, month, ++day);
			effectiveDate = df.format(cal.getTime());
			effectiveDate = getEffectiveTransDate(effectiveDate, conn);
		} else if (dao.getBankHolidayCheck(pDate, conn)) { // Checking
															// BankHoliday
			cal.set(year, month, ++day);
			effectiveDate = df.format(cal.getTime());
			effectiveDate = getEffectiveTransDate(effectiveDate, conn);
		} else {
			effectiveDate = df.format(cal.getTime());
		}
		return effectiveDate;
	}


	public EPIXResponse doPaymentNFC(String cardNo, double amount, String currencyCode, String transDate,
			String merchantID, String terminalID, String traceNo, String secCode, String ExpDate, String pin,
			String narrative) {
		Connection conn = null;
		Connection conn2 = null;
		EPIXResponse ret = new EPIXResponse();
		AccountData l_DrAccData = new AccountData();
		AccountData l_CrAccData = new AccountData();
		DAOManager l_DAOMgr = new DAOManager();
		AccountDao l_AccDao = new AccountDao();
		CMSMerchantAccRefData l_MCData = new CMSMerchantAccRefData();
		CMSMerchantAccDao l_MCDao = new CMSMerchantAccDao();
		String transRef = "";
		try {
			conn = l_DAOMgr.openICBSConnection();
			if (conn != null) {
				l_DrAccData = l_AccDao.getAccountByID(cardNo, conn);
				if (l_DrAccData.getAccountNumber().equals("")) {
					ret.setCode("0019");
					ret.setDesc("Debit account doesn't exist.");
				} else {
					if (l_DrAccData.getCurrentBalance() < amount) {
						ret.setCode("0021");
						ret.setDesc("Insufficient balance.");
					} else {
						if (!merchantID.equalsIgnoreCase("")) {
							conn2 = l_DAOMgr.openConnection();
							l_MCData = l_MCDao.readDataAccById(merchantID, conn2);
							if (l_MCData.getAccountNumber().equals("")) {
								ret.setCode("0022");
								ret.setDesc("Merchant doesn't exist.");
							} else {
								l_CrAccData.setAccountNumber(l_MCData.getAccountNumber());
								ret.setCode("0000");
								conn2.close();
							}
						}
					}
				}
				// conn.setAutoCommit(false);
				String isTrOrRev = "TR";
				// make transaction
				String drRemark = "NFC Payment, " + l_MCData.getT10(); // Merchant
																		// Name
				String crRemark = "NFC Payment, " + l_MCData.getT10();

				// make transaction
				if (ret.getCode().equals("0000")) {
					ret = makeTransaction(l_DrAccData.getAccountNumber(), l_DrAccData.getCurrentBalance(),
							l_MCData.getAccountNumber(), 0, amount, 0, 1, "", isTrOrRev, drRemark, crRemark, "", "", "",
							narrative, conn);
				}
				if (ret.getCode().equals("0000")) {
					transRef = ret.getXref();
					ret = makeGLTransaction(l_DrAccData.getAccountNumber(), l_DrAccData.getCurrentBalance(),
							l_MCData.getAccountNumber(), 0, amount, Integer.parseInt(ret.getXref()), 1, conn);
				}
				if (ret.getCode().equals("0000")) {
					if (l_AccDao.updateDrCurrentBal(l_DrAccData.getAccountNumber(), amount,
							GeneralUtil.getDateYYYYMMDD(), GeneralUtil.getDateYYYYMMDD(), conn)) {
						if (l_MCData.getAccountNumber().length() > 7) {
							if (l_AccDao.updateCrCurrentBal(l_MCData.getAccountNumber(), amount,
									GeneralUtil.getDateYYYYMMDD(), GeneralUtil.getDateYYYYMMDD(), conn)) {
								// conn.setAutoCommit(true);
								ret.setXref(transRef);
								ret.setCode("0000");
								ret.setDesc("Payment Successful.");
							}
						} else {
							// conn.setAutoCommit(true);
							ret.setXref(transRef);
							ret.setCode("0000");
							ret.setDesc("Payment Successful.");
						}
					}
				}
			} else {
				ret.setCode("0015");
				ret.setDesc("Connection error.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			ret.setCode("0014");
			ret.setDesc("Server error.");
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					ret.setCode("0014");
					ret.setDesc("Server error.");
				}
			}
			if (conn2 != null) {
				try {
					if (!conn2.isClosed())
						conn2.close();
				} catch (SQLException e) {
					ret.setCode("0014");
					ret.setDesc("Server error.");
				}
			}
		}
		return ret;
	}

	public EPIXResponse doPaymentV1(String id, String drAcc, String merchantID, String amount, String comAmt,
			String taxAmt, String reqTrnRef, String narrative) {
		Connection conn = null;
		Connection conn2 = null;
		EPIXResponse ret = new EPIXResponse();
		AccountData l_DrAccData = new AccountData();
		DAOManager l_DAOMgr = new DAOManager();
		AccountDao l_AccDao = new AccountDao();
		CMSMerchantAccRefData l_MCData = new CMSMerchantAccRefData();
		CMSMerchantAccDao l_MCDao = new CMSMerchantAccDao();
		String transRef = "";
		try {
			conn = l_DAOMgr.openICBSConnection();
			if (conn != null) {
				l_DrAccData = l_AccDao.getAccountByID(drAcc, conn);
				if (l_DrAccData.getAccountNumber().equals("")) {
					ret.setCode("0019");
					ret.setDesc("Debit account doesn't exist.");
				} else {
					if (l_DrAccData.getCurrentBalance() < Double.parseDouble(amount)) {
						ret.setCode("0021");
						ret.setDesc("Insufficient balance.");
					} else {
						conn2 = l_DAOMgr.openConnection();
						l_MCData = l_MCDao.readDataAccById(merchantID, conn2);
						if (l_MCData.getAccountNumber().equals("")) {
							ret.setCode("0022");
							ret.setDesc("Merchant doesn't exist.");
						} else {
							ret.setCode("0000");
							conn2.close();
						}
					}
				}
				// conn.setAutoCommit(false);
				String isTrOrRev = "TR";
				// make transaction
				String drRemark = "Bill Payment, " + l_MCData.getT10(); // Merchant
																		// Name
				String crRemark = "Bill Payment, " + l_MCData.getT10(); // Merchant
																		// Name
				ArrayList<Double> amountList = new ArrayList<Double>();
				ArrayList<String> accList = new ArrayList<String>();
				if (amount.equalsIgnoreCase("")) {
					amount = "0";
				}
				if (taxAmt.equalsIgnoreCase("")) {
					taxAmt = "0";
				}
				if (comAmt.equalsIgnoreCase("")) {
					comAmt = "0";
				}
				amountList.add(Double.parseDouble(amount) + Double.parseDouble(taxAmt));
				accList.add(l_MCData.getAccountNumber());
				if (!comAmt.equalsIgnoreCase("0")) {
					amountList.add(Double.parseDouble(comAmt));
					String comGL = ConnAdmin.readConfig("ComGL");
					accList.add(comGL);
				}
				// make transaction
				for (int i = 0; i < accList.size(); i++) {
					if (i != 0) {
						drRemark = "Bill Payment Charges, " + l_MCData.getT10(); // Merchant
																					// Name
						crRemark = "Bill Payment Charges, " + l_MCData.getT10(); // Merchant
																					// Name
					}
					if (ret.getCode().equals("0000")) {
						ret = makeTransaction(l_DrAccData.getAccountNumber(), l_DrAccData.getCurrentBalance(),
								accList.get(i), 0, amountList.get(i), 0, 1, reqTrnRef, isTrOrRev, drRemark, crRemark,
								id, "", merchantID, narrative, conn);
					}
					if (ret.getCode().equals("0000")) {
						transRef = ret.getXref();
						ret = makeGLTransaction(l_DrAccData.getAccountNumber(), l_DrAccData.getCurrentBalance(),
								accList.get(i), 0, amountList.get(i), Integer.parseInt(ret.getXref()), 1, conn);
					}
					if (ret.getCode().equals("0000")) {
						if (l_AccDao.updateDrCurrentBal(l_DrAccData.getAccountNumber(), Double.parseDouble(amount),
								GeneralUtil.getDateYYYYMMDD(), GeneralUtil.getDateYYYYMMDD(), conn)) {
							if (accList.get(i).length() > 7) {
								if (l_AccDao.updateCrCurrentBal(accList.get(i), amountList.get(i),
										GeneralUtil.getDateYYYYMMDD(), GeneralUtil.getDateYYYYMMDD(), conn)) {
									// conn.setAutoCommit(true);
									ret.setFlexcubeid(transRef);
									ret.setCode("0000");
									ret.setDesc("Payment successfully.");
								}
							} else {
								// conn.setAutoCommit(true);
								ret.setFlexcubeid(transRef);
								ret.setCode("0000");
								ret.setDesc("Payment successfully.");
							}
						}
					}
				}
			} else {
				ret.setCode("0015");
				ret.setDesc("Connection error.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			ret.setCode("0014");
			ret.setDesc("Server error.");
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					ret.setCode("0014");
					ret.setDesc("Server error.");
				}
			}
			if (conn2 != null) {
				try {
					if (!conn2.isClosed())
						conn2.close();
				} catch (SQLException e) {
					ret.setCode("0014");
					ret.setDesc("Server error.");
				}
			}
		}
		return ret;
	}

	public ChequeEnquiryRes enquiryCheque(String accountNo, String chequeNo) {
		ICBSDao l_dao = new ICBSDao();
		DAOManager dao = new DAOManager();
		ChequeEnquiryRes ret = new ChequeEnquiryRes();
		Connection conn = null;
		String status = "";
		try {
			conn = dao.openICBSConnection();
			status = l_dao.enquiryCheque(accountNo, chequeNo, conn);
			if (!status.equals("")) {
				ret.setCode("0000");
				ret.setDesc("Success");
				ret.setStatus(status);
			} else {
				ret.setCode("0014");
				ret.setDesc("Cheque not found");
				ret.setStatus(status);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ret.setCode("0014");
			ret.setDesc(e.getMessage());

		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				ret.setCode("0014");
				ret.setDesc(e.getMessage());
			}

		}
		return ret;
	}

	public AccountActivityResponse getAccountActivity(String accNumber, String customerNo, String durationType,
			String fromDate, String toDate, String currentPage, String pageSize) {
		ICBSDao l_dao = new ICBSDao();
		DAOManager dao = new DAOManager();
		AccountActivityResponse res = new AccountActivityResponse();
		Connection conn = null;
		try {
			conn = dao.openICBSConnection();
			res = l_dao.getAccountActivity(accNumber, customerNo, durationType, fromDate, toDate, currentPage, pageSize,
					conn);
		} catch (Exception e) {
			e.printStackTrace();
			res.setCode("0014");
			res.setDesc(e.getMessage());
			GeneralUtil.readDebugLogStatus();
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Error Message : " + e.getMessage());
				l_err.add("=============================================================================");
				GeneralUtil.writeLog(l_err);
			}
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				res.setCode("0014");
				res.setDesc(e.getMessage());
				GeneralUtil.readDebugLogStatus();
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("");
					l_err.add("Time :" + GeneralUtil.getTime());
					l_err.add("Error Message : " + e.getMessage());
					l_err.add("=============================================================================");
					GeneralUtil.writeLog(l_err);
				}
			}
		}
		return res;
	}

	public AccountActivityResponse getAccountActivityList(String accNumber, String customerNo, String durationType,
			String fromDate, String toDate, String currentPage, String pageSize) {
		ICBSDao l_dao = new ICBSDao();
		DAOManager dao = new DAOManager();
		AccountActivityResponse res = new AccountActivityResponse();
		Connection conn = null;
		try {
			conn = dao.openICBSConnection();
			res = l_dao.getAccountActivityList(accNumber, customerNo, durationType, fromDate, toDate, currentPage,
					pageSize, conn);
		} catch (Exception e) {
			e.printStackTrace();
			res.setCode("0014");
			res.setDesc(e.getMessage());
			GeneralUtil.readDebugLogStatus();
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Error Message : " + e.getMessage());
				l_err.add("=============================================================================");
				GeneralUtil.writeLog(l_err);
			}
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				res.setCode("0014");
				res.setDesc(e.getMessage());
				GeneralUtil.readDebugLogStatus();
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("");
					l_err.add("Time :" + GeneralUtil.getTime());
					l_err.add("Error Message : " + e.getMessage());
					l_err.add("=============================================================================");
					GeneralUtil.writeLog(l_err);
				}
			}
		}
		return res;
	}

	public DepositAccDataResult getAccountSummary(String customerid) {
		ICBSDao l_dao = new ICBSDao();
		DAOManager dao = new DAOManager();
		DepositAccDataResult cusres = new DepositAccDataResult();
		Connection conn = null;
		try {
			conn = dao.openICBSConnection();
			cusres = l_dao.getAccountSummary(customerid, conn);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			cusres.setCode("0014");
			cusres.setDesc(e.getMessage());
			GeneralUtil.readDebugLogStatus();
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Error Message : " + e.getMessage());
				l_err.add("=============================================================================");
				GeneralUtil.writeLog(l_err);
			}
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				cusres.setCode("0014");
				cusres.setDesc(e.getMessage());
				GeneralUtil.readDebugLogStatus();
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("");
					l_err.add("Time :" + GeneralUtil.getTime());
					l_err.add("Error Message : " + e.getMessage());
					l_err.add("=============================================================================");
					GeneralUtil.writeLog(l_err);
				}
			}
		}
		return cusres;
	}

	public AgentEncashData getAGEncashDataByRefno(String id, String RefNo, String agentType) {
		// TODO Auto-generated method stub
		AgentEncashData ret = new AgentEncashData();
		Connection l_Conn = null;
		ICBSDao dao = new ICBSDao();
		try {
			l_Conn = ConnAdmin.getConn("001", "");
			ret = dao.getAGEncashData(RefNo, agentType, l_Conn);
			ret.setFromid(id);
		} catch (Exception e) {
			e.printStackTrace();
			ret.setCode("0014");
			ret.setDesc(e.getMessage());
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return ret;
	}

	public AgentResData getAgentUser(String id, String comRef, String type) {
		// TODO Auto-generated method stub
		AgentResData ret = new AgentResData();
		Connection l_Conn = null;
		ICBSDao dao = new ICBSDao();
		try {
			l_Conn = ConnAdmin.getConn("001", "");
			ret = dao.getAgentUser(id, comRef, type, l_Conn);

		} catch (Exception e) {
			e.printStackTrace();
			ret.setCode("0014");
			ret.setDesc(e.getMessage());
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return ret;
	}

	public ResponseBalance getBalanceByID(String id) {
		Connection conn = null;
		ResponseBalance ret = new ResponseBalance();
		AccountDao l_AccDao = new AccountDao();
		String accNo = "";
		double balance = 0.00;
		DAOManager l_DAOMgr = new DAOManager();
		try {
			conn = l_DAOMgr.openICBSConnection();
			if (conn != null) {
				balance = l_AccDao.getBalanceByIDV2(id, conn);
				ret.setAccountNo(id);
				ret.setBalance(balance);
				ret.setCode("0000");
				ret.setDesc("Balance get successfully.");
			} else {
				ret.setCode("0015");
				ret.setDesc("Connection error.");
			}
		} catch (Exception e) {
			ret.setCode("0014");
			ret.setDesc("Server error.");
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					ret.setCode("0014");
					ret.setDesc("Server error.");
				}
			}
		}
		return ret;
	}

	public GetOutstandingBillResData getOutstandingBill(String meterID, int status, String merchantID) {
		// TODO Auto-generated method stub
		GetOutstandingBillResData billData = new GetOutstandingBillResData();
		MCommRateMappingData rateResult = new MCommRateMappingData();
		MCommRateMappingMgr mCommRateMgr = new MCommRateMappingMgr();
		ICBSDao dao = new ICBSDao();
		Connection l_Conn = null;
		double bankchargesAmt = 0.00;
		double penaltyAmt = 0.00;
		try {
			l_Conn = ConnAdmin.getConn("001", "");
			billData = dao.getOustandingBill(meterID, status, l_Conn);
			rateResult = mCommRateMgr.getMCommMappingToCalculateChargewByMerId(merchantID, "");
			if (billData.getCode().equalsIgnoreCase("0000")) {
				// calculation
				DecimalFormat df = new DecimalFormat("#,###.00");
				for (int j = 0; j < billData.getOutData().length; j++) {
					// for bank charges
					if (!rateResult.getCommRef1().equals("-")) {
						bankchargesAmt = GeneralUtility.calculateIndividualCharges(rateResult.getCommObj1(), 3,
								Double.valueOf(billData.getOutData()[j].getAmount()));
					}
					System.out.println("bankchargesAmt " + bankchargesAmt);
					billData.getOutData()[j].setBankChargesAmt(df.format(bankchargesAmt));
					// for penalty
					if (Integer.valueOf(billData.getOutData()[j].getPenaltyDays()) >= Integer
							.valueOf(rateResult.getPenaltyDays())) {
						if (!rateResult.getCommRef2().equals("-")) {
							penaltyAmt = GeneralUtility.calculateIndividualCharges(rateResult.getCommObj2(), 3,
									Double.valueOf(billData.getOutData()[j].getAmount()));
						}

						System.out.println("Penalty amt " + penaltyAmt);
						billData.getOutData()[j].setPenaltyAmt(df.format(penaltyAmt));
					}
					billData.getOutData()[j].setAmount(df.format(Double.valueOf(billData.getOutData()[j].getAmount())));
				}
			} else {
				billData.setCode("0014");
				billData.setDesc("No Bill Outstanding");
			}

		} catch (Exception e) {
			e.printStackTrace();
			billData.setCode("0014");
			billData.setDesc(e.getMessage());
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return billData;
	}

	private EPIXResponse makeGLTransaction(String drAccNo, double drPrevBal, String crAccNo, double crPrevBal,
			double amount, int transRef, int status, Connection conn) throws SQLException {
		EPIXResponse ret = new EPIXResponse();
		AccountGLTransactionData data = new AccountGLTransactionData();
		ArrayList<AccountGLTransactionData> dataList = new ArrayList<AccountGLTransactionData>();
		AccountGLTransactionDao dao = new AccountGLTransactionDao();
		// prepare Dr Info
		if (!(drAccNo.equals(""))) {
			data.setTransNo(transRef);
			data.setAccNumber(drAccNo);
			data.setGLCode1("D301010000");
			data.setGLCode2("D101010000");
			data.setGLCode3("");
			data.setGLCode4("");
			data.setGLCode5("");
			data.setBaseCurCode("MMK");
			data.setBaseCurOperator("/");
			data.setBaseRate(1);
			data.setBaseAmount(amount);
			data.setMediumCurCode("");
			data.setMediumCurOperator("");
			data.setMediumRate(0);
			data.setMediumAmount(0);
			data.setTrCurCode("MMK");
			data.setTrCurOperator("");
			data.setTrRate(1);
			data.setTrAmount(amount);
			data.setTrPrevBalance(drPrevBal);
			data.setN1(status);
			data.setN2(0);
			data.setN3(0);
			data.setT1("");
			data.setT2("");
			data.setT3("");
			dataList.add(data);
		}
		// prepare Cr Info
		if (!(crAccNo.equals(""))) {
			data = new AccountGLTransactionData();
			data.setTransNo(transRef + 1);
			data.setAccNumber(crAccNo);
			data.setGLCode1("D301010000");
			data.setGLCode2("D301010000");
			data.setGLCode3("");
			data.setGLCode4("");
			data.setGLCode5("");
			data.setBaseCurCode("MMK");
			data.setBaseCurOperator("/");
			data.setBaseRate(1);
			data.setBaseAmount(amount);
			data.setMediumCurCode("");
			data.setMediumCurOperator("");
			data.setMediumRate(0);
			data.setMediumAmount(0);
			data.setTrCurCode("MMK");
			data.setTrCurOperator("");
			data.setTrRate(1);
			data.setTrAmount(amount);
			data.setTrPrevBalance(crPrevBal);
			data.setN1(status);
			data.setN2(0);
			data.setN3(0);
			data.setT1("");
			data.setT2("");
			data.setT3("");
			dataList.add(data);
		}
		if (dao.save(dataList, conn)) {
			ret.setCode("0000");
			ret.setDesc("AccountGLTransaction saved successfully.");
		} else {
			ret.setCode("0018");
			ret.setDesc("AccountGLTransaction saved fail.");
		}
		return ret;
	}

	private EPIXResponse makeTransaction(String drAccNo, double drPrevBal, String crAccNo, double crPrevBal,
			double amount, int transRef, int status, String subRef, String isTrOrRev, String drRemark, String crRemark,
			String drAccRef, String crAccRef, String superVisorID, String narrative, Connection conn)
			throws SQLException {
		EPIXResponse ret = new EPIXResponse();
		AccountTransactionData data = new AccountTransactionData();
		// ArrayList<AccountTransactionData> dataList = new
		// ArrayList<AccountTransactionData>();
		AccountTransactionDao dao = new AccountTransactionDao();
		String effectiveDate = getEffectiveTransDate(GeneralUtil.getDateYYYYMMDDSimple(), conn);
		String XtransRef = "";
		// prepare Dr Info
		if (!drAccNo.equals("")) {
			data.setBranchCode("001");
			data.setWorkStation("Mobile");
			if (isTrOrRev.equalsIgnoreCase("REV")) {
				data.setTransRef(transRef);
			}
			data.setSerialNo(0);
			data.setTellerId("admin");
			data.setSupervisorId(superVisorID);
			data.setTransTime(GeneralUtil.getDateYYYYMMDDHHMMSS());
			data.setTransDate(GeneralUtil.getDateYYYYMMDD());
			data.setDescription(drRemark);
			data.setChequeNo("");
			data.setCurrencyCode("MMK");
			data.setCurrencyRate(1);
			data.setAmount(amount);
			data.setTransType(705);
			data.setAccNumber(drAccNo);
			data.setPrevBalance(drPrevBal);
			data.setPrevUpdate(GeneralUtil.getDateYYYYMMDD());
			data.setEffectiveDate(effectiveDate);
			data.setContraDate("");
			data.setStatus(status);
			data.setAccRef(drAccRef);
			data.setRemark(narrative);
			data.setSystemCode(0);
			data.setSubRef(subRef);
			ret = dao.addAccTransaction(data, conn);
			if (ret.getCode().equals("0000")) {
				XtransRef = ret.getXref();
				if (isTrOrRev.equalsIgnoreCase("TR")) {
					if (subRef.equals(""))
						subRef = XtransRef;
					if (dao.updateTransRefByTransNo(Integer.parseInt(XtransRef), subRef, conn)) {
						ret.setCode("0000");
						// subRef = XtransRef;
					}
				}
			}
		}
		// prepare Cr Info
		if (ret.getCode().equals("0000")) {
			if (!crAccNo.equals("")) {
				data = new AccountTransactionData();
				data.setBranchCode("001");
				data.setWorkStation("Mobile");
				if (isTrOrRev.equalsIgnoreCase("REV")) {
					data.setTransRef(transRef);
				} else {
					data.setTransRef(Integer.parseInt(XtransRef));
				}
				data.setSerialNo(0);
				data.setTellerId("admin");
				data.setSupervisorId(superVisorID);
				data.setTransTime(GeneralUtil.getDateYYYYMMDDHHMMSS());
				data.setTransDate(GeneralUtil.getDateYYYYMMDD());
				data.setDescription(crRemark);
				data.setChequeNo("");
				data.setCurrencyCode("MMK");
				data.setCurrencyRate(1);
				data.setAmount(amount);
				data.setTransType(205);
				data.setAccNumber(crAccNo);
				data.setPrevBalance(crPrevBal);
				data.setPrevUpdate(GeneralUtil.getDateYYYYMMDD());
				data.setEffectiveDate(effectiveDate);
				data.setContraDate("");
				data.setStatus(status);
				data.setAccRef(crAccRef);
				data.setRemark(narrative);
				data.setSystemCode(0);
				data.setSubRef(subRef);

				ret = dao.addAccTransaction(data, conn);
				if (ret.getCode().equals("0000")) {
					ret.setCode("0000");
					ret.setDesc("AccountTransaction saved successfully.");
					ret.setXref(XtransRef);
				} else {
					ret.setCode("0018");
					ret.setDesc("AccountTransaction saved fail.");
				}
			}
		}
		return ret;
	}

	public EPIXResponse reversalPaymentWallet(String id, String merchantID, String amount, String comAmt, String taxAmt,
			String trnRef, String narrative) {
		Connection conn = null;
		Connection conn2 = null;
		EPIXResponse ret = new EPIXResponse();
		AccountData l_CrAccData = new AccountData();
		AccountTransactionData l_ATData = new AccountTransactionData();
		DAOManager l_DAOMgr = new DAOManager();
		AccountDao l_AccDao = new AccountDao();
		AccountGLTransactionDao l_AGDao = new AccountGLTransactionDao();
		AccountTransactionDao l_ATDao = new AccountTransactionDao();
		CMSMerchantAccRefData l_MCData = new CMSMerchantAccRefData();
		CMSMerchantAccDao l_MCDao = new CMSMerchantAccDao();
		int transRef = Integer.parseInt(trnRef);
		try {
			conn = l_DAOMgr.openICBSConnection();
			if (conn != null) {
				l_CrAccData = l_AccDao.getAccountByID(id, conn);
				if (l_CrAccData.getAccountNumber().equals("")) {
					ret.setCode("0019");
					ret.setDesc("Credit account doesn't exist.");
				} else {
					conn2 = l_DAOMgr.openConnection();
					l_MCData = l_MCDao.readDataAccById(merchantID, conn2);
					if (l_MCData.getAccountNumber().equals("")) {
						ret.setCode("0022");
						ret.setDesc("Merchant doesn't exist.");
					} else {
						ret.setCode("0000");
						conn2.close();
					}
				}
				l_ATData = l_ATDao.getDataByTransRef(transRef, conn);
				if (l_ATData.getAmount() != Double.parseDouble(amount)) {
					ret.setCode("0022");
					ret.setDesc("Amount doesn't match.");
				} else {
					ret.setCode("0000");
				}
				// conn.setAutoCommit(false);
				if (ret.getCode().equals("0000")) {
					if (l_ATDao.updateStatusByTransRef(transRef, conn)) {
						// make transaction
						String isTrOrRev = "REV";
						String drRemark = "Reversal to " + l_MCData.getT10(); // MerchantName
						String crRemark = "Reversal to " + l_MCData.getT10();
						if (ret.getCode().equals("0000")) {
							ret = makeTransaction("3898001", 0, l_CrAccData.getAccountNumber(),
									l_CrAccData.getCurrentBalance(), Double.parseDouble(amount), transRef, 255,
									String.valueOf(transRef), isTrOrRev, drRemark, crRemark, "", id, merchantID,
									narrative, conn);
						}
						if (ret.getCode().equals("0000")) {
							ret = makeGLTransaction("3898001", 0, l_CrAccData.getAccountNumber(),
									l_CrAccData.getCurrentBalance(), Double.parseDouble(amount),
									Integer.parseInt(ret.getXref()), 255, conn);
						}
						if (l_AGDao.updateStatusByTransRef(transRef, conn)) {
							if (ret.getCode().equals("0000")) {
								if (l_AccDao.updateCrCurrentBal(l_CrAccData.getAccountNumber(),
										Double.parseDouble(amount), GeneralUtil.getDateYYYYMMDD(),
										GeneralUtil.getDateYYYYMMDD(), conn)) {
									if (ret.getCode().equals("0000")) {
										// conn.setAutoCommit(true);
										conn.close();
										ret.setXref(trnRef);
										ret.setCode("0000");
										ret.setDesc("Reverse successfully.");
									}
								}
							}
						}
					} else {
						ret.setCode("0022");
						ret.setDesc("Transaction not found.");
					}
				}
			} else {
				ret.setCode("0015");
				ret.setDesc("Connection error.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			ret.setCode("0014");
			ret.setDesc("Server error.");
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					ret.setCode("0014");
					ret.setDesc("Server error.");
				}
			}
			if (conn2 != null) {
				try {
					if (!conn2.isClosed())
						conn2.close();
				} catch (SQLException e) {
					ret.setCode("0014");
					ret.setDesc("Server error.");
				}
			}
		}
		return ret;
	}

	public ResponseData stopCheque(String accountNo, String chequeNo) {
		ICBSDao l_dao = new ICBSDao();
		DAOManager dao = new DAOManager();
		ResponseData ret = new ResponseData();
		Connection conn = null;
		try {
			conn = dao.openICBSConnection();
			ret = l_dao.stopCheque(accountNo, chequeNo, conn);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ret.setCode("0014");
			ret.setDesc(e.getMessage());

		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				ret.setCode("0014");
				ret.setDesc(e.getMessage());
			}
		}
		return ret;
	}

	public Response updateScheduleEntry(String merchantID, String meterNo, String billNo, String transRef)
			throws SQLException {
		Response response = new Response();
		ICBSDao dao = new ICBSDao();
		Connection conn = ConnAdmin.getConn("001", "");
		try {
			response = dao.updateScheduleEntry(merchantID, meterNo, billNo, transRef, conn);
		} catch (Exception e) {
			e.printStackTrace();
			response.setResponsecode("0014");
			response.setResponsedesc(e.getMessage());
		} finally {
			if (!conn.isClosed()) {
				conn.close();
			}
		}
		return response;

	}
	public EPIXResponse doAccountTransfer(DisPaymentTransactionData disData, String trType, String reqTrnRef,
			String narrative,Connection pConniCBS,boolean isDrGL,boolean isCrGl,
			com.nirvasoft.rp.shared.AccountData fromAccData, com.nirvasoft.rp.shared.AccountData toAccData,
			Connection pConn) {
		EPIXResponse ret = new EPIXResponse();
		ICBSServiceMgr icbsMgr = new ICBSServiceMgr();
		SMSTransferData pTransferData = new SMSTransferData();
		SMSReturnData pResData = new SMSReturnData();
		try {
			System.out.println("start limit  "+GeneralUtil.getDateTime());
			//ret = checkTransLimit(disData.getFromAccount(), disData.getT24(), disData.getAmount(),pConn);
			System.out.println("end limit  "+GeneralUtil.getDateTime());
			//if (ret.getCode().equals("0000")) {
				// prepare data
				pTransferData.setAmount(disData.getAmount());
				/*pTransferData.setCreditIBTKey("");
				pTransferData.setCurrencyCode("");
				pTransferData.setCustomerID("");*/
				pTransferData.setDebitIBTKey(trType);
				pTransferData.setUserID(disData.getMobileuserid());
				// to calculate
				System.out.println("start checkCutOfTime  "+GeneralUtil.getDateTime());
				pTransferData.setFinishCutOffTime(GeneralUtility.checkCutOfTime());
				System.out.println("end checkCutOfTime  "+GeneralUtil.getDateTime());
				pTransferData.setFromAccNumber(disData.getFromAccount());
				pTransferData.setFromBranchCode(disData.getFromBranch());
				/*pTransferData.setFromName("");
				pTransferData.setFromNrc("");
				pTransferData.setFromProductCode("");
				pTransferData.setFromZone("1");
				pTransferData.setInstitutionCode("");*/
				pTransferData.setIsAgent(false);
				pTransferData.setIsCreditAgent(false);
				/*pTransferData.setLimitAmount(0);
				pTransferData.setMerchantID("");
				pTransferData.setOnlineSerialNo(0);*/
				pTransferData.setReferenceNo(reqTrnRef);
				pTransferData.setRemark(narrative);
				pTransferData.setSameAgent(false);
				/*pTransferData.setTerminalId("");*/
				pTransferData.setToAccNumber(disData.getToAccount());
				pTransferData.setToBranchCode(disData.getToBranch());
				/*pTransferData.setToName("");
				pTransferData.setToNrc("");
				pTransferData.setToProductCode("");
				pTransferData.setToZone("1");
				pTransferData.setTransactionFee(0);*/
				// call ICBSServiceMgr
				System.out.println("start postTransfer  "+GeneralUtil.getDateTime());
				pResData = icbsMgr.postTransfer(pTransferData,isDrGL,isCrGl,fromAccData,toAccData,pConniCBS);
				System.out.println("end postTransfer  "+GeneralUtil.getDateTime());
				if (pResData.getStatus()) {
					ret.setCode("0000");
					ret.setDesc(pResData.getDescription());
					ret.setFlexcubeid(String.valueOf(pResData.getTransactionNumber()));
					ret.setCharges(String.valueOf(pResData.getComm1()));
					ret.setParam1(pResData.getCcy());
				} else {
					ret.setCode("0014");
					ret.setDesc(pResData.getDescription());
				}
			/*} else {
				return ret;
			}*/
		} catch (Exception e) {
			e.printStackTrace();
			ret.setCode("0014");
			ret.setDesc("fail");
			
		}
		return ret;
	}
	public EPIXResponse checkTransLimit(String fromAccount, String userID, double amount,Connection pConn) throws SQLException {
		EPIXResponse ret = new EPIXResponse();
		String minTransLimit = "";
		String dayTransLimit = "";
		String maxTransLimit = "";
		double transAmtPerday = 0.00;
		
		minTransLimit = ConnAdmin.minTransLimit;
		dayTransLimit = ConnAdmin.dayTransLimit;
		maxTransLimit = ConnAdmin.maxTransLimit;
		if (minTransLimit.equals("")) {
			minTransLimit = "0";
		}
		if (dayTransLimit.equals("")) {
			dayTransLimit = "0";
		}
		transAmtPerday = new DispaymentTransationDao().getTransAmountPerDayICBS(fromAccount, userID,
				GeneralUtil.datetoString(), pConn);		
		if (amount > Double.parseDouble(maxTransLimit)) {			
			ret.setCode("0014");
			ret.setDesc("Transfer Amount exceeded over " + GeneralUtil.formatNumber(Double.valueOf(maxTransLimit))
					+ " MMK");
			return ret;
		} else {
			ret.setCode("0000");
			ret.setDesc("Success");
		}
		if (amount + transAmtPerday > Double.parseDouble(dayTransLimit)) {
			ret.setCode("0014");
			ret.setDesc("Transaction exceeded over daily transaction limit "
					+ GeneralUtil.formatNumber(Double.valueOf(dayTransLimit)) + " MMK");
			return ret;
		} else {
			ret.setCode("0000");
			ret.setDesc("Success");
		}		
		return ret;
	}
	
	public EPIXResponse doAccountTransferNew(AccTransferReq disData, String reqTrnRef,Connection pConniCBS,boolean isDrGL,boolean isCrGl,
			com.nirvasoft.rp.shared.AccountData fromAccData, com.nirvasoft.rp.shared.AccountData toAccData,String pTransType,
			Connection pConn) {
		EPIXResponse ret = new EPIXResponse();
		ICBSServiceMgr icbsMgr = new ICBSServiceMgr();		
		SMSReturnData pResData = new SMSReturnData();
		try {			
			
			pResData = icbsMgr.postTransferNew(disData,isDrGL,isCrGl,fromAccData,toAccData,reqTrnRef,pTransType, pConniCBS);
			System.out.println("end postTransfer  "+GeneralUtil.getDateTime());
			if (pResData.getStatus()) {
				ret.setCode("0000");
				ret.setDesc(pResData.getDescription());
				ret.setFlexcubeid(String.valueOf(pResData.getTransactionNumber()));
				ret.setCharges(String.valueOf(pResData.getComm1()));
				ret.setEffectiveDate(pResData.getEffectiveDate());
				ret.setParam1(pResData.getCcy());
				ret.setComm1(String.valueOf(pResData.getComm1()));
				ret.setComm2(String.valueOf(pResData.getComm2()));
			} else {
				ret.setCode("0014");
				ret.setDesc(pResData.getDescription());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			ret.setCode("0014");
			ret.setDesc("fail");
			
		}
		return ret;
	}
	public EPIXResponse doMPTPayment(AccTransferReq disData, String reqTrnRef,Connection pConniCBS,boolean isDrGL,boolean isCrGl,
			com.nirvasoft.rp.shared.AccountData fromAccData, com.nirvasoft.rp.shared.AccountData toAccData,String pTransType,
			Connection pConn) {
		EPIXResponse ret = new EPIXResponse();
		ICBSServiceMgr icbsMgr = new ICBSServiceMgr();		
		SMSReturnData pResData = new SMSReturnData();
		try {			
			
			pResData = icbsMgr.postMPTPaymentReq(disData,isDrGL,isCrGl,fromAccData,toAccData,reqTrnRef,pTransType, pConniCBS);
			System.out.println("end postTransfer  "+GeneralUtil.getDateTime());
			if (pResData.getStatus()) {
				ret.setCode("0000");
				ret.setDesc(pResData.getDescription());
				ret.setFlexcubeid(String.valueOf(pResData.getTransactionNumber()));
				ret.setCharges(String.valueOf(pResData.getComm1()));
				ret.setEffectiveDate(pResData.getEffectiveDate());
				ret.setParam1(pResData.getCcy());
				ret.setComm1(String.valueOf(pResData.getComm1()));
				ret.setComm2(String.valueOf(pResData.getComm2()));
			} else {
				ret.setCode("0014");
				ret.setDesc(pResData.getDescription());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			ret.setCode("0014");
			ret.setDesc("fail");
			
		}
		return ret;
	}

	
}
