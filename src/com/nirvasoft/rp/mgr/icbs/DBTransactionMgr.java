package com.nirvasoft.rp.mgr.icbs;

import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import com.nirvasoft.rp.core.ccbs.data.db.DBFECCurrencyRateMgr;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.dao.icbs.BankHolidayDAO;
import com.nirvasoft.rp.dao.icbs.LastDatesDAO;
import com.nirvasoft.rp.shared.AccountGLTransactionData;
import com.nirvasoft.rp.shared.DataResult;
import com.nirvasoft.rp.shared.TransactionData;
import com.nirvasoft.rp.util.StrUtil;

public class DBTransactionMgr {
	private static long m_AutoPrintTransRef = 0;
	private static String m_ProductGL = "L050101001001001";
	private static int mAccountLinkCrTransType = 201;
	private static int mAccountLinkDrTransType = 701;
	private static final AtomicInteger counter = new AtomicInteger(123456789);

	public static String getCurrentDateYYYYMMDDHHMMSS() {
		String pattern = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		return format.format(new Date());
	}

	public static String getEffectiveTransDate(String aDate) throws Exception {
		String effectiveDate = "";
		DAOManager myConnectionUtil = new DAOManager();
		Connection conn = myConnectionUtil.openICBSConnection();
		try {
			BankHolidayDAO l_BankHolidayDAO = new BankHolidayDAO();
			LastDatesDAO l_LastDateDAO = new LastDatesDAO();
			effectiveDate = getEffectiveTransDate(aDate, l_BankHolidayDAO, l_LastDateDAO, conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null)
				if (!conn.isClosed())
					conn.close();
		}

		return effectiveDate;
	}
	
	public static String getEffectiveTransDate(String aDate,Connection pConn) throws Exception {
		String effectiveDate = "";		
		try {
			BankHolidayDAO l_BankHolidayDAO = new BankHolidayDAO();
			LastDatesDAO l_LastDateDAO = new LastDatesDAO();
			effectiveDate = getEffectiveTransDate(aDate, l_BankHolidayDAO, l_LastDateDAO, pConn);
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		return effectiveDate;
	}
	public static String getEffectiveTransDate(String pDate, BankHolidayDAO pDAO, LastDatesDAO pLastDateDAO,
			Connection conn) throws SQLException {
		int year = Integer.parseInt(pDate.substring(0, 4));
		int month = Integer.parseInt(pDate.substring(4, 6));
		int day = Integer.parseInt(pDate.substring(6, 8));
		Calendar cal = Calendar.getInstance();
		month--; // In Java Calendar, month is starting zero.
		cal.set(year, month, day);

		DateFormat df = new SimpleDateFormat("yyyyMMdd");

		String effectiveDate = pDate;
		// Checking is Runned EOD
		if (pLastDateDAO.isRunnedEOD(pDate, conn)) {
			cal.clear();
			cal.set(year, month, ++day);
			effectiveDate = df.format(cal.getTime());
			effectiveDate = getEffectiveTransDate(effectiveDate, pDAO, pLastDateDAO, conn);
		} else if (StrUtil.isWeekEnd(pDate)) {
			cal.clear();
			cal.set(year, month, ++day);
			effectiveDate = df.format(cal.getTime());
			effectiveDate = getEffectiveTransDate(effectiveDate, pDAO, pLastDateDAO, conn);
		} else if (pDAO.getBankHolidayCheck(pDate, conn)) { // Checking
															// BankHoliday
			cal.set(year, month, ++day);
			effectiveDate = df.format(cal.getTime());
			effectiveDate = getEffectiveTransDate(effectiveDate, pDAO, pLastDateDAO, conn);
		} else {
			effectiveDate = df.format(cal.getTime());
		}
		return effectiveDate;
	}
	public static ArrayList<AccountGLTransactionData> prepareAccountGLTransactions(
			ArrayList<TransactionData> pTransactionDatas, Connection conn) {
		ArrayList<AccountGLTransactionData> ret = new ArrayList<AccountGLTransactionData>();
		try {
			/*double l_CurrencyRate = DBFECCurrencyRateMgr.getFECCurrencyRate(pTransactionDatas.get(0).getCurrencyCode(),
					"smTransfer", pTransactionDatas.get(0).getBranchCode(), conn);*/
			for (int i = 0; i < pTransactionDatas.size(); i++) {
				ret.add(prepareAccountGLTransaction(pTransactionDatas.get(i), pTransactionDatas.get(0).getCurrencyRate()));
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return ret;
	}
	public static AccountGLTransactionData prepareAccountGLTransaction(TransactionData pTransData, double baseRate) {
		AccountGLTransactionData object = new AccountGLTransactionData();
		object.setTransNo(pTransData.getTransactionNumber());
		object.setAccNumber(pTransData.getAccountNumber());
		object.setGLCode1(pTransData.getProductGL());
		object.setGLCode2(pTransData.getCashInHandGL());
		object.setGLCode3("");
		object.setGLCode4("");
		object.setGLCode5("");
		object.setBaseCurCode("MMK");
		object.setBaseCurOperator("/");
		object.setBaseRate(baseRate);
		object.setBaseAmount(StrUtil.round2decimals(
				(pTransData.getAmount() * (pTransData.getCurrencyRate() * 1000) / 1000), RoundingMode.HALF_UP));
		object.setMediumCurCode("");
		object.setMediumCurOperator("");
		object.setTrCurCode(pTransData.getCurrencyCode());
		object.setTrCurOperator(""); // To Set Transaction Cur Operator
		object.setTrRate(pTransData.getCurrencyRate());
		object.setTrAmount(StrUtil.round2decimals(pTransData.getAmount(), RoundingMode.HALF_UP));
		object.setTrPrevBalance(StrUtil.round2decimals(pTransData.getPreviousBalance(), RoundingMode.HALF_UP));
		object.setN1(0);
		object.setN2(0);
		object.setN3(0);
		object.setT1("");
		object.setT2("");
		object.setT3("");
		return object;
	}
	public static ArrayList<TransactionData> prepareBaseCurrencys(ArrayList<TransactionData> pDatas) {
		ArrayList<TransactionData> ret = new ArrayList<TransactionData>();
		for (int i = 0; i < pDatas.size(); i++) {
			ret.add(prepareBaseCurrency(pDatas.get(i)));
		}
		return ret;
	}
	public static TransactionData prepareBaseCurrency(TransactionData pData) {
		TransactionData ret = new TransactionData();
		ret = pData;
		ret.setAmount(StrUtil.round2decimals((ret.getAmount() * (ret.getCurrencyRate() * 1000) / 1000),
				RoundingMode.HALF_UP));
		ret.setPreviousBalance(StrUtil.round2decimals(
				(ret.getPreviousBalance() * (ret.getCurrencyRate() * 1000) / 1000), RoundingMode.HALF_UP));
		ret.setCurrencyCode("MMK");
		ret.setCurrencyRate(1);
		return ret;
	}
	public static double getTransAmount(double aTrAmt, double aDrCurRate, double aCrCurRate, String aIsDrAmt) {
		double l_TransAmount = 0;
		if (aIsDrAmt.equalsIgnoreCase("Dr")) {
			l_TransAmount = (aTrAmt * aDrCurRate) / aCrCurRate;
		} else {
			l_TransAmount = (aTrAmt * aCrCurRate) / aDrCurRate;
		}
		if (l_TransAmount == Double.NaN) {
			l_TransAmount = 0.0;
		}
		return l_TransAmount;
	}
}
