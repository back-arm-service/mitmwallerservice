package com.nirvasoft.rp.mgr.icbs;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.dao.icbs.ProductsDAO;
import com.nirvasoft.rp.shared.ProductData;

public class DBProductMgr {
	public static ArrayList<ProductData> getProductsConfiguration() throws Exception {
		DAOManager myConnectionUtil = new DAOManager();
		Connection conn = myConnectionUtil.openICBSConnection();

		ArrayList<ProductData> ret = new ArrayList<ProductData>();
		try {
			ret = getProductsConfiguration(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				if (!conn.isClosed()) {
					conn.close();
				}
			}
		}

		return ret;
	}

	public static ArrayList<ProductData> getProductsConfiguration(Connection pConn) throws Exception {
		ArrayList<ProductData> ret = new ArrayList<ProductData>();

		ProductsDAO l_productDAO = new ProductsDAO();
		if (l_productDAO.getProductsConfiguration(pConn))			
		{
			ret = l_productDAO.getProductDataList();
		}

		return ret;
	}

	public static ArrayList<ProductData> getProductsNumber() throws Exception {
		DAOManager myConnectionUtil = new DAOManager();
		Connection conn = myConnectionUtil.openICBSConnection();
		ArrayList<ProductData> ret = new ArrayList<ProductData>();

		try {
			ret = getProductsNumber(conn);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				if (!conn.isClosed()) {
					conn.close();
				}
			}
		}
		return ret;
	}

	public static ArrayList<ProductData> getProductsNumber(Connection pConn) throws Exception {
		ArrayList<ProductData> ret = new ArrayList<ProductData>();

		ProductsDAO l_productDAO = new ProductsDAO();
		if (l_productDAO.getProductsNumber(pConn))			
		{
			ret = l_productDAO.getProductDataList();
		}

		return ret;
	}

	public static ArrayList<ProductData> getProductsType() throws Exception {
		DAOManager myConnectionUtil = new DAOManager();
		Connection conn = myConnectionUtil.openICBSConnection();
		ArrayList<ProductData> ret = new ArrayList<ProductData>();
		try {
			ret = getProductsType(conn);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				if (!conn.isClosed()) {
					conn.close();
				}
			}
		}
		return ret;
	}

	public static ArrayList<ProductData> getProductsType(Connection pConn) throws Exception {
		ArrayList<ProductData> ret = new ArrayList<ProductData>();

		ProductsDAO l_productDAO = new ProductsDAO();
		if (l_productDAO.getProductsType(pConn))
			;
		{
			ret = l_productDAO.getProductDataList();
		}

		return ret;
	}
}
