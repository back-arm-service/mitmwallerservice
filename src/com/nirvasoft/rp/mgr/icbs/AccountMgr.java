package com.nirvasoft.rp.mgr.icbs;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.cms.dao.RegisterDao;
import com.nirvasoft.epixws.webservice.CustomerInfo;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.dao.icbs.AccountDao;
import com.nirvasoft.rp.mgr.WalletMgr;
import com.nirvasoft.rp.shared.CustomerData;
import com.nirvasoft.rp.shared.RegisterListResponseData;
import com.nirvasoft.rp.shared.SessionData;
import com.nirvasoft.rp.shared.TransferOutRes;
import com.nirvasoft.rp.shared.WalletCheckRes;
import com.nirvasoft.rp.shared.WalletRequestData;
import com.nirvasoft.rp.shared.WalletResponseData;

public class AccountMgr {

	public WalletCheckRes checkWallet(SessionData req) {
		WalletCheckRes response = new WalletCheckRes();
		DAOManager dao = new DAOManager();
		Connection conn = null;
		try {
			conn = dao.openConnection();
			response = new RegisterDao().checkWallet(req, conn);
		} catch (Exception e) {
			response.setCode("0014");
			response.setDesc("Server Error!");
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed()) {
						conn.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return response;
	}

	public WalletResponseData getBalance(WalletRequestData data) {
		WalletResponseData response = new WalletResponseData();
		DAOManager dao = new DAOManager();
		Connection conn1 = null;
		Connection conn2 = null;
		boolean success = false;

		try {
			conn1 = dao.openConnection();
			String accountNumber = new AccountDao().getAccountNumber(data.getUserID(), conn1);

			if (accountNumber != null) {
				conn2 = dao.openICBSConnection();
				String balance = new AccountDao().getBalance(accountNumber, conn2);

				if (balance != null) {
					success = true;
					response.setBalance(Double.parseDouble(balance));
				}
			}

			if (success) {
				response.setMessageCode("0000");
				response.setMessageDesc("Getting balance success.");
			} else {
				response.setMessageCode("0014");
				response.setMessageDesc("Getting balance fail.");
			}
		} catch (Exception e) {
			response.setMessageCode("0014");
			response.setMessageDesc("Getting balance fail.");

			e.printStackTrace();
		} finally {
			if (conn1 != null) {
				try {
					if (!conn1.isClosed()) {
						conn1.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return response;
	}

	public CustomerData getCustomerNameAndNrcByAccount(String account, boolean isAccount, boolean isGL) {
		CustomerData customerData = new CustomerData();
		CustomerInfo customerInfo = new CustomerInfo();
		DAOManager dao = new DAOManager();
		Connection conn1 = null;
		Connection conn2 = null;

		try {			
			if (isAccount) {
				conn1 = dao.openConnection();
				customerInfo = new AccountDao().getCustomerData(account, conn1);
			} else if (isGL) {
				conn2 = dao.openICBSConnection();
				customerInfo = new AccountDao().getGLData(account, conn2);
			}
			if (customerInfo.getCode().equals("0000")) {
				if (isAccount) {
					customerData.setName(customerInfo.getName());
					if (customerInfo.getNRC().equals(null)) {
						customerData.setNrc("");
					} else {
						customerData.setNrc(customerInfo.getNRC());
					}
					customerData.setChkAccount(true);
				} else if (isGL) {
					customerData.setGlDesp(customerInfo.getName());
					customerData.setChkGL(true);
				}
				customerData.setMessage(customerInfo.getDesc());
				customerData.setMsgCode(customerInfo.getCode());
				customerData.setMsgDesc(customerInfo.getDesc());
			} else {
				customerData.setMsgCode("0014");
				customerData.setMsgDesc(customerInfo.getDesc());
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return customerData;
	}

	// checkAcc
	public TransferOutRes checkAcc(String userid) {
		TransferOutRes res = new TransferOutRes();
		Connection conn = null;
		RegisterDao regDao = new RegisterDao();
		WalletMgr wlMgr = new WalletMgr();
		try {
			conn = new DAOManager().openConnection();
			String account = regDao.joinedAcc(userid, conn);
			if (account.equals("")) {
				res = wlMgr.checkUserAcc(account);
			} else{
				res.setCode("0001");
			}
				
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;

	}

}
