
package com.nirvasoft.rp.mgr.icbs;

import java.sql.Connection;
import java.sql.SQLException;

import com.nirvasoft.rp.dao.CAJunctionDao;
import com.nirvasoft.rp.dao.CustomerDao;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.mfi.mgr.MobileMemberDataMgr;
import com.nirvasoft.mfi.users.data.MobileMemberData;
import com.nirvasoft.rp.dao.AccountDao;
import com.nirvasoft.rp.mgr.UserRegistrationMgr;
import com.nirvasoft.rp.shared.CAJunctionData;
import com.nirvasoft.rp.shared.WCustomerData;
import com.nirvasoft.rp.shared.WalletResponseData;
import com.nirvasoft.rp.shared.icbs.AccountData;
import com.nirvasoft.rp.util.GeneralUtil;

public class DBAccountMgr {

	public WalletResponseData openAccount(String userid, String sessionid, String custName, String nrc, String sKey,
			String institutionCode, String region, String phoneType, String deviceId, String password,
			String appVersion, String regionNumber){
		// String sKey is chatapi syskey.
		WalletResponseData resp = new WalletResponseData();

		UserRegistrationMgr userRegmgr = new UserRegistrationMgr();
		DAOManager daomgr = new DAOManager();

		Connection conn = null;
		Connection conn1 = null;

		CustomerDao cusdao = new CustomerDao();
		AccountDao l_AccDao = new AccountDao();

		String accNumber = "";
		String ccyCode = "1";
		//String ccyCode = "";
	    String reserveword = "0";
		String branchCode="";
		String productCode="";
		String accCode="";
		String prodGL="";
		String cashGL="";
		String customerID = "";
//		String prefix = branchCode + productCode + accCode + ccyCode + reserveword;
		resp.setName(custName);
		resp.setNrc(nrc);
		try {
			conn = daomgr.openICBSConnection();
			conn.setAutoCommit(false);
			conn1 = daomgr.openConnection();
			conn1.setAutoCommit(false);
			customerID = cusdao.getLastID(conn);
			
			resp = saveCustomerInfo(customerID, userid, custName, nrc, conn);

			if (resp.getMessageCode().equals("0000")) {				
				branchCode=l_AccDao.getBranchCode(conn);//ndh
				productCode=l_AccDao.getProductCode(conn);
				accCode=l_AccDao.getAccountCode(conn);
				prodGL=l_AccDao.getProductGL(conn);
				//ccyCode=l_AccDao.getCurrencyCode(conn);
				cashGL=l_AccDao.getCashGL(conn);//ndh
				//String prefix = branchCode + productCode + accCode;
				String prefix = branchCode + productCode + accCode + ccyCode + reserveword;
				accNumber = l_AccDao.getMaxSerialNoNSB(prefix, conn);
				accNumber = GeneralUtil.getCheckDigitAdded(accNumber);
				resp = saveAccountInfo(accNumber, userid, customerID, branchCode, conn);
			}
			if (resp.getMessageCode().equals("0000")) {				

				resp = saveCAJunctionInfo(accNumber, customerID, conn);
			}

			if (resp.getMessageCode().equals("0000")) {
				resp = saveT00005Info(accNumber, productCode, accCode, ccyCode, branchCode, prodGL, cashGL, conn);
			}
			// End of iCBS
			if (resp.getMessageCode().equals("0000")) {					
					resp = userRegmgr.saveUserRegistration(accNumber, customerID, userid, sessionid, userid, custName,
							nrc, institutionCode, 0, sKey, region, phoneType, deviceId, password, appVersion,
							regionNumber, conn1);
					if (resp.getMessageCode().equals("0000")) {
						MobileMemberData data = new MobileMemberData();
						//data.setRegistered_mobileNo(userid);
						//data = MobileMemberDataMgr.saveMobileMember(data);
						//if(data.getMsgCode().equals("0000")){
							conn.commit();
							conn1.commit();
							
							resp.setInstitutionCode(institutionCode);
							resp.setInstitutionName(GeneralUtil.setInstitutionName(institutionCode));
							resp.setAccountNo(accNumber);
							resp.setSessionID("");
							resp.setBalance(0.00);
							resp.setName(custName);
							resp.setMessageCode("0000");
							resp.setMessageDesc("Account opened successfully.");
							conn.close();
							conn1.close();
						//}
					}else{	
						conn.rollback();
						conn1.rollback();
					}
			}else{
				conn.rollback();
				conn1.rollback();
			}
		} catch (Exception e) {
			try {
				conn1.rollback();
			} catch (SQLException e1) {				
				e1.printStackTrace();
			}
			e.printStackTrace();
			resp.setMessageCode("0014");
			resp.setMessageDesc("Register Fail");
			e.printStackTrace();
		} finally {
			if (conn != null || conn1 != null) {
				try {
					if (!conn.isClosed()) {
						conn.close();
					}
					if (!conn1.isClosed()) {
						conn1.close();
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return resp;
	}

	private WalletResponseData saveAccountInfo(String accNumber, String userid, String customerID, String branchCode,
			Connection conn) throws SQLException {
		AccountDao l_AccDao = new AccountDao();
		WalletResponseData ret = new WalletResponseData();
		AccountData l_AccData = new AccountData();
		l_AccData.setAccountNumber(accNumber);
		l_AccData.setCurrencyCode("MMK");
		l_AccData.setOpeningBalance(0);
		l_AccData.setOpeningDate(GeneralUtil.getDateYYYYMMDD());
		l_AccData.setCurrentBalance(0);
		l_AccData.setClosingDate("9999-12-31");
		l_AccData.setLastUpdate("1900-01-01");
		l_AccData.setStatus(0);
		l_AccData.setLastTransDate("1900-01-01");
		l_AccData.setDrawingType("100");
		l_AccData.setAccountName(userid);
		l_AccData.setRemark("Wallet account");
		l_AccData.setBranchCode(branchCode);
		if (l_AccDao.save(l_AccData, conn) && l_AccDao.saveAccountsBalance(l_AccData, conn)) {
			if (l_AccDao.saveCurrentAcc(accNumber, conn)) {
				ret.setMessageCode("0000");
				ret.setMessageDesc("Account saved successfully.");
			} else {
				ret.setMessageCode("0014");
				ret.setMessageDesc("Account saved fail.");
			}
		} else {
			ret.setMessageCode("0014");
			ret.setMessageDesc("Account opening failed.");
		}
		return ret;
	}

	private WalletResponseData saveCAJunctionInfo(String accNumber, String customerID, Connection conn)
			throws SQLException {
		WalletResponseData ret = new WalletResponseData();
		CAJunctionData l_CAJunctionData = new CAJunctionData();
		CAJunctionDao l_CAJanDao = new CAJunctionDao();
		l_CAJunctionData.setAccNumber(accNumber);
		l_CAJunctionData.setCustomerId(customerID);	
		l_CAJunctionData.setAccType(110);
		l_CAJunctionData.setrType(110);
		if (l_CAJanDao.save(l_CAJunctionData, conn)) {
			ret.setMessageCode("0000");
			ret.setMessageDesc("CAJunction saved successfully.");
		} else {
			ret.setMessageCode("0014");
			ret.setMessageDesc("Account opening failed.");
		}
		return ret;
	}

	private WalletResponseData saveCustomerInfo(String customerID, String id, String custName, String nrc,
			Connection conn) throws SQLException {
		CustomerDao l_CusDao = new CustomerDao();
		WalletResponseData ret = new WalletResponseData();
		WCustomerData l_CusData = new WCustomerData();
		l_CusData.setName(custName);
		l_CusData.setPhone(id);
		l_CusData.setNrcNo(nrc);
		l_CusData.setUniversalId("002");
		l_CusData.setCustomerID(customerID);
		l_CusData.setmStatus(1);
		if (l_CusDao.save(l_CusData, conn)) {
			ret.setMessageCode("0000");
			ret.setMessageDesc("Customer info saved successfully.");
		} else {
			ret.setMessageCode("0014");
			ret.setMessageDesc("Account opening failed.");
		}
		return ret;
	}

	private WalletResponseData saveT00005Info(String accNumber, String prodCode, String accCode, String ccy,
			String branchCode, String prodGL, String cashGL, Connection conn) throws SQLException {
		WalletResponseData ret = new WalletResponseData();
		AccountDao l_AccDao = new AccountDao();
		if (l_AccDao.saveT00005(accNumber, prodCode, accCode, ccy, branchCode, prodGL, cashGL, conn)) {
			ret.setMessageCode("0000");
			ret.setMessageDesc("Product saved successfully.");
		} else {
			ret.setMessageCode("0014");
			ret.setMessageDesc("Account opening failed.");
		}
		return ret;
	}
	
}
