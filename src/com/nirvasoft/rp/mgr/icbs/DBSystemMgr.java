package com.nirvasoft.rp.mgr.icbs;

import java.sql.Connection;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.dao.icbs.ReferenceAccountsDAO;
import com.nirvasoft.rp.shared.icbs.ReferenceAccountsData;

public class DBSystemMgr {
	public static ReferenceAccountsData getReferenceAccCode(String pCode) throws Exception {
		ReferenceAccountsData l_ReferenceAccountData = new ReferenceAccountsData();
		DAOManager myConnectionUtil = new DAOManager();
		Connection conn = myConnectionUtil.openICBSConnection();

		try {
			l_ReferenceAccountData = getReferenceAccCode(pCode, conn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null)
				if (!conn.isClosed())
					conn.close();
		}
		return l_ReferenceAccountData;
	}
	
	public static ReferenceAccountsData getReferenceAccCode(String pCode, Connection pConn) {
		ReferenceAccountsData object = new ReferenceAccountsData();
		ReferenceAccountsDAO l_DAO = new ReferenceAccountsDAO();
		try {
			if (l_DAO.getReferenceAccCode(pCode, pConn)) {
				object = l_DAO.getReferenceAccountsDataBean();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return object;
	}
	
}
