package com.nirvasoft.rp.mgr.icbs;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import com.nirvasoft.rp.dao.CMSMerchantAccDao;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.dao.HolidayDao;
import com.nirvasoft.rp.dao.icbs.AccountDao;
import com.nirvasoft.rp.dao.icbs.AccountGLTransactionDao;
import com.nirvasoft.rp.dao.icbs.AccountTransactionDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.shared.CMSMerchantAccRefData;
import com.nirvasoft.rp.shared.EPIXResponse;
import com.nirvasoft.rp.shared.ResponseBalance;
import com.nirvasoft.rp.shared.icbs.AccountData;
import com.nirvasoft.rp.shared.icbs.AccountGLTransactionData;
import com.nirvasoft.rp.shared.icbs.AccountTransactionData;
import com.nirvasoft.rp.util.GeneralUtil;

public class ICBSTransferMgr {

	public static String getEffectiveTransDate(String pDate, Connection conn) throws SQLException {
		HolidayDao dao = new HolidayDao();
		int year = Integer.parseInt(pDate.substring(0, 4));
		int month = Integer.parseInt(pDate.substring(4, 6));
		int day = Integer.parseInt(pDate.substring(6, 8));
		Calendar cal = Calendar.getInstance();
		month--; // In Java Calendar, month is starting zero.
		cal.set(year, month, day);
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		String effectiveDate = pDate;
		// Checking is Runned EOD
		if (dao.isRunnedEOD(pDate, conn)) {
			cal.clear();
			cal.set(year, month, ++day);
			effectiveDate = df.format(cal.getTime());
			effectiveDate = getEffectiveTransDate(effectiveDate, conn);
		} else if (GeneralUtil.isWeekEnd(pDate)) {
			cal.clear();
			cal.set(year, month, ++day);
			effectiveDate = df.format(cal.getTime());
			effectiveDate = getEffectiveTransDate(effectiveDate, conn);
		} else if (dao.getBankHolidayCheck(pDate, conn)) { // Checking
															// BankHoliday
			cal.set(year, month, ++day);
			effectiveDate = df.format(cal.getTime());
			effectiveDate = getEffectiveTransDate(effectiveDate, conn);
		} else {
			effectiveDate = df.format(cal.getTime());
		}
		return effectiveDate;
	}

	public EPIXResponse doAccountTransfer(String id, String fromAcc, String toAcc, String merchantID, String amount,
			String comAmt, String taxAmt, String reqTrnRef, String narrative) {
		Connection conn = null;
		Connection conn2 = null;
		EPIXResponse ret = new EPIXResponse();
		AccountData l_DrAccData = new AccountData();
		AccountData l_CrAccData = new AccountData();
		DAOManager l_DAOMgr = new DAOManager();
		AccountDao l_AccDao = new AccountDao();
		CMSMerchantAccRefData l_MCData = new CMSMerchantAccRefData();
		CMSMerchantAccDao l_MCDao = new CMSMerchantAccDao();
		String transRef = "";
		try {
			conn = l_DAOMgr.openICBSConnection();
			if (conn != null) {
				l_DrAccData = l_AccDao.getAccountByID(fromAcc, conn);
				if (l_DrAccData.getAccountNumber().equals("")) {
					ret.setCode("0019");
					ret.setDesc("Debit account doesn't exist.");
				} else {
					if (l_DrAccData.getCurrentBalance() < Double.parseDouble(amount)) {
						ret.setCode("0021");
						ret.setDesc("Insufficient balance.");
					} else {
						if (!toAcc.equalsIgnoreCase("")) {
							l_CrAccData = l_AccDao.getAccountByID(toAcc, conn);
							if (l_DrAccData.getAccountNumber().equals("")) {
								ret.setCode("0022");
								ret.setDesc("Credit account doesn't exist.");
							} else {
								ret.setCode("0000");
							}
						} else if (!merchantID.equalsIgnoreCase("")) {
							conn2 = l_DAOMgr.openConnection();
							l_MCData = l_MCDao.readDataAccById(merchantID, conn2);
							if (l_MCData.getAccountNumber().equals("")) {
								ret.setCode("0022");
								ret.setDesc("Merchant doesn't exist.");
							} else {
								l_CrAccData.setAccountNumber(l_MCData.getAccountNumber());
								ret.setCode("0000");
								conn2.close();
							}
						}
					}
				}
				// conn.setAutoCommit(false);
				String isTrOrRev = "TR";
				// make transaction
				String drRemark = "";
				String crRemark = "";
				ArrayList<Double> amountList = new ArrayList<Double>();
				ArrayList<String> accList = new ArrayList<String>();
				if (amount.equalsIgnoreCase("")) {
					amount = "0";
				}
				if (taxAmt.equalsIgnoreCase("")) {
					taxAmt = "0";
				}
				if (comAmt.equalsIgnoreCase("")) {
					comAmt = "0";
				}
				amountList.add(Double.parseDouble(amount) + Double.parseDouble(taxAmt));
				accList.add(l_CrAccData.getAccountNumber());
				if (!comAmt.equalsIgnoreCase("0")) {
					if (!comAmt.equalsIgnoreCase("0.0")) {
						amountList.add(Double.parseDouble(comAmt));
						String comGL = ConnAdmin.readConfig("ComGL");
						accList.add(comGL);
					}
				}
				// make transaction
				for (int i = 0; i < accList.size(); i++) {
					drRemark = "Mobile Transfer";
					crRemark = "Mobile Transfer";
					if (ret.getCode().equals("0000")) {
						ret = makeTransaction(l_DrAccData.getAccountNumber(), l_DrAccData.getCurrentBalance(),
								accList.get(i), 0, amountList.get(i), 0, 1, reqTrnRef, isTrOrRev, drRemark, crRemark,
								id, "", "", narrative, conn);
					}
					if (ret.getCode().equals("0000")) {
						transRef = ret.getXref();
						ret = makeGLTransaction(l_DrAccData.getAccountNumber(), l_DrAccData.getCurrentBalance(),
								accList.get(i), 0, amountList.get(i), Integer.parseInt(ret.getXref()), 1, conn);
					}
					if (ret.getCode().equals("0000")) {
						if (l_AccDao.updateDrCurrentBal(l_DrAccData.getAccountNumber(), Double.parseDouble(amount),
								GeneralUtil.getDateYYYYMMDD(), GeneralUtil.getDateYYYYMMDD(), conn)) {
							if (accList.get(i).length() > 7) {
								if (l_AccDao.updateCrCurrentBal(accList.get(i), amountList.get(i),
										GeneralUtil.getDateYYYYMMDD(), GeneralUtil.getDateYYYYMMDD(), conn)) {
									// conn.setAutoCommit(true);
									ret.setXref(transRef);
									ret.setCode("0000");
									ret.setDesc("Transfer successfully.");
								}
							} else {
								// conn.setAutoCommit(true);
								ret.setXref(transRef);
								ret.setCode("0000");
								ret.setDesc("Transfer successfully.");
							}
						}
					}
				}
			} else {
				ret.setCode("0015");
				ret.setDesc("Connection error.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			ret.setCode("0014");
			ret.setDesc("Server error.");
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					ret.setCode("0014");
					ret.setDesc("Server error.");
				}
			}
			if (conn2 != null) {
				try {
					if (!conn2.isClosed())
						conn2.close();
				} catch (SQLException e) {
					ret.setCode("0014");
					ret.setDesc("Server error.");
				}
			}
		}
		return ret;
	}

	public EPIXResponse doPayment(String id, String drAcc, String merchantID, String amount, String comAmt,
			String taxAmt, String reqTrnRef, String narrative) {
		Connection conn = null;
		Connection conn2 = null;
		EPIXResponse ret = new EPIXResponse();
		AccountData l_DrAccData = new AccountData();
		DAOManager l_DAOMgr = new DAOManager();
		AccountDao l_AccDao = new AccountDao();
		CMSMerchantAccRefData l_MCData = new CMSMerchantAccRefData();
		CMSMerchantAccDao l_MCDao = new CMSMerchantAccDao();
		String transRef = "";
		try {
			conn = l_DAOMgr.openICBSConnection();
			if (conn != null) {
				l_DrAccData = l_AccDao.getAccountByID(drAcc, conn);
				if (l_DrAccData.getAccountNumber().equals("")) {
					ret.setCode("0019");
					ret.setDesc("Debit account doesn't exist.");
				} else {
					if (l_DrAccData.getCurrentBalance() < Double.parseDouble(amount)) {
						ret.setCode("0021");
						ret.setDesc("Insufficient balance.");
					} else {
						conn2 = l_DAOMgr.openConnection();
						l_MCData = l_MCDao.readDataAccById(merchantID, conn2);
						if (l_MCData.getAccountNumber().equals("")) {
							ret.setCode("0022");
							ret.setDesc("Merchant doesn't exist.");
						} else {
							ret.setCode("0000");
							conn2.close();
						}
					}
				}
				// conn.setAutoCommit(false);
				String isTrOrRev = "TR";
				// make transaction
				String drRemark = "Bill Payment, " + l_MCData.getT10(); // Merchant
																		// Name
				String crRemark = "Bill Payment, " + l_MCData.getT10(); // Merchant
																		// Name
				ArrayList<Double> amountList = new ArrayList<Double>();
				ArrayList<String> accList = new ArrayList<String>();
				if (amount.equalsIgnoreCase("")) {
					amount = "0";
				}
				if (taxAmt.equalsIgnoreCase("")) {
					taxAmt = "0";
				}
				if (comAmt.equalsIgnoreCase("")) {
					comAmt = "0";
				}
				amountList.add(Double.parseDouble(amount) + Double.parseDouble(taxAmt));
				accList.add(l_MCData.getAccountNumber());
				if (!comAmt.equalsIgnoreCase("0")) {
					amountList.add(Double.parseDouble(comAmt));
					String comGL = ConnAdmin.readConfig("ComGL");
					accList.add(comGL);
				}
				// make transaction
				for (int i = 0; i < accList.size(); i++) {
					if (i != 0) {
						drRemark = "Bill Payment Charges, " + l_MCData.getT10(); // Merchant
																					// Name
						crRemark = "Bill Payment Charges, " + l_MCData.getT10(); // Merchant
																					// Name
					}
					if (ret.getCode().equals("0000")) {
						ret = makeTransaction(l_DrAccData.getAccountNumber(), l_DrAccData.getCurrentBalance(),
								accList.get(i), 0, amountList.get(i), 0, 1, reqTrnRef, isTrOrRev, drRemark, crRemark,
								id, "", merchantID, narrative, conn);
					}
					if (ret.getCode().equals("0000")) {
						transRef = ret.getXref();
						ret = makeGLTransaction(l_DrAccData.getAccountNumber(), l_DrAccData.getCurrentBalance(),
								accList.get(i), 0, amountList.get(i), Integer.parseInt(ret.getXref()), 1, conn);
					}
					if (ret.getCode().equals("0000")) {
						if (l_AccDao.updateDrCurrentBal(l_DrAccData.getAccountNumber(), Double.parseDouble(amount),
								GeneralUtil.getDateYYYYMMDD(), GeneralUtil.getDateYYYYMMDD(), conn)) {
							if (accList.get(i).length() > 7) {
								if (l_AccDao.updateCrCurrentBal(accList.get(i), amountList.get(i),
										GeneralUtil.getDateYYYYMMDD(), GeneralUtil.getDateYYYYMMDD(), conn)) {
									// conn.setAutoCommit(true);
									ret.setFlexcubeid(transRef);
									ret.setCode("0000");
									ret.setDesc("Payment successfully.");
								}
							} else {
								// conn.setAutoCommit(true);
								ret.setFlexcubeid(transRef);
								ret.setCode("0000");
								ret.setDesc("Payment successfully.");
							}
						}
					}
				}
			} else {
				ret.setCode("0015");
				ret.setDesc("Connection error.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			ret.setCode("0014");
			ret.setDesc("Server error.");
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					ret.setCode("0014");
					ret.setDesc("Server error.");
				}
			}
			if (conn2 != null) {
				try {
					if (!conn2.isClosed())
						conn2.close();
				} catch (SQLException e) {
					ret.setCode("0014");
					ret.setDesc("Server error.");
				}
			}
		}
		return ret;
	}

	public EPIXResponse doPaymentNFC(String cardNo, double amount, String currencyCode, String transDate,
			String merchantID, String terminalID, String traceNo, String secCode, String ExpDate, String pin,
			String narrative) {
		Connection conn = null;
		Connection conn2 = null;
		EPIXResponse ret = new EPIXResponse();
		AccountData l_DrAccData = new AccountData();
		AccountData l_CrAccData = new AccountData();
		DAOManager l_DAOMgr = new DAOManager();
		AccountDao l_AccDao = new AccountDao();
		CMSMerchantAccRefData l_MCData = new CMSMerchantAccRefData();
		CMSMerchantAccDao l_MCDao = new CMSMerchantAccDao();
		String transRef = "";
		try {
			conn = l_DAOMgr.openICBSConnection();
			if (conn != null) {
				l_DrAccData = l_AccDao.getAccountByID(cardNo, conn);
				if (l_DrAccData.getAccountNumber().equals("")) {
					ret.setCode("0019");
					ret.setDesc("Debit account doesn't exist.");
				} else {
					if (l_DrAccData.getCurrentBalance() < amount) {
						ret.setCode("0021");
						ret.setDesc("Insufficient balance.");
					} else {
						if (!merchantID.equalsIgnoreCase("")) {
							conn2 = l_DAOMgr.openConnection();
							l_MCData = l_MCDao.readDataAccById(merchantID, conn2);
							if (l_MCData.getAccountNumber().equals("")) {
								ret.setCode("0022");
								ret.setDesc("Merchant doesn't exist.");
							} else {
								l_CrAccData.setAccountNumber(l_MCData.getAccountNumber());
								ret.setCode("0000");
								conn2.close();
							}
						}
					}
				}
				// conn.setAutoCommit(false);
				String isTrOrRev = "TR";
				// make transaction
				String drRemark = "NFC Payment, " + l_MCData.getT10(); // Merchant
																		// Name
				String crRemark = "NFC Payment, " + l_MCData.getT10();

				// make transaction
				if (ret.getCode().equals("0000")) {
					ret = makeTransaction(l_DrAccData.getAccountNumber(), l_DrAccData.getCurrentBalance(),
							l_MCData.getAccountNumber(), 0, amount, 0, 1, "", isTrOrRev, drRemark, crRemark, "", "", "",
							narrative, conn);
				}
				if (ret.getCode().equals("0000")) {
					transRef = ret.getXref();
					ret = makeGLTransaction(l_DrAccData.getAccountNumber(), l_DrAccData.getCurrentBalance(),
							l_MCData.getAccountNumber(), 0, amount, Integer.parseInt(ret.getXref()), 1, conn);
				}
				if (ret.getCode().equals("0000")) {
					if (l_AccDao.updateDrCurrentBal(l_DrAccData.getAccountNumber(), amount,
							GeneralUtil.getDateYYYYMMDD(), GeneralUtil.getDateYYYYMMDD(), conn)) {
						if (l_MCData.getAccountNumber().length() > 7) {
							if (l_AccDao.updateCrCurrentBal(l_MCData.getAccountNumber(), amount,
									GeneralUtil.getDateYYYYMMDD(), GeneralUtil.getDateYYYYMMDD(), conn)) {
								// conn.setAutoCommit(true);
								ret.setXref(transRef);
								ret.setCode("0000");
								ret.setDesc("Payment Successful.");
							}
						} else {
							// conn.setAutoCommit(true);
							ret.setXref(transRef);
							ret.setCode("0000");
							ret.setDesc("Payment Successful.");
						}
					}
				}
			} else {
				ret.setCode("0015");
				ret.setDesc("Connection error.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			ret.setCode("0014");
			ret.setDesc("Server error.");
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					ret.setCode("0014");
					ret.setDesc("Server error.");
				}
			}
			if (conn2 != null) {
				try {
					if (!conn2.isClosed())
						conn2.close();
				} catch (SQLException e) {
					ret.setCode("0014");
					ret.setDesc("Server error.");
				}
			}
		}
		return ret;
	}

	public ResponseBalance getBalanceByID(String id) {
		Connection conn = null;
		ResponseBalance ret = new ResponseBalance();
		AccountDao l_AccDao = new AccountDao();
		String accNo = "";
		double balance = 0.00;
		DAOManager l_DAOMgr = new DAOManager();
		try {
			conn = l_DAOMgr.openICBSConnection();
			if (conn != null) {
				AccountData data = new AccountData();
				data = l_AccDao.getBalanceByID(id, conn);
				accNo = data.getAccountNumber();
				balance = data.getCurrentBalance();
				if (!accNo.equals("")) {
					ret.setAccountNo(accNo);
					ret.setBalance(balance);
					ret.setCode("0000");
					ret.setDesc("Balance get successfully.");
				} else {
					ret.setCode("0016");
					ret.setDesc("No record found.");
				}
			} else {
				ret.setCode("0015");
				ret.setDesc("Connection error.");
			}
		} catch (Exception e) {
			ret.setCode("0014");
			ret.setDesc("Server error.");
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					ret.setCode("0014");
					ret.setDesc("Server error.");
				}
			}
		}
		return ret;
	}

	private EPIXResponse makeGLTransaction(String drAccNo, double drPrevBal, String crAccNo, double crPrevBal,
			double amount, int transRef, int status, Connection conn) throws SQLException {
		EPIXResponse ret = new EPIXResponse();
		AccountGLTransactionData data = new AccountGLTransactionData();
		ArrayList<AccountGLTransactionData> dataList = new ArrayList<AccountGLTransactionData>();
		AccountGLTransactionDao dao = new AccountGLTransactionDao();
		// prepare Dr Info
		if (!(drAccNo.equals(""))) {
			data.setTransNo(transRef);
			data.setAccNumber(drAccNo);
			data.setGLCode1("D301010000");
			data.setGLCode2("D101010000");
			data.setGLCode3("");
			data.setGLCode4("");
			data.setGLCode5("");
			data.setBaseCurCode("MMK");
			data.setBaseCurOperator("/");
			data.setBaseRate(1);
			data.setBaseAmount(amount);
			data.setMediumCurCode("");
			data.setMediumCurOperator("");
			data.setMediumRate(0);
			data.setMediumAmount(0);
			data.setTrCurCode("MMK");
			data.setTrCurOperator("");
			data.setTrRate(1);
			data.setTrAmount(amount);
			data.setTrPrevBalance(drPrevBal);
			data.setN1(status);
			data.setN2(0);
			data.setN3(0);
			data.setT1("");
			data.setT2("");
			data.setT3("");
			dataList.add(data);
		}
		// prepare Cr Info
		if (!(crAccNo.equals(""))) {
			data = new AccountGLTransactionData();
			data.setTransNo(transRef + 1);
			data.setAccNumber(crAccNo);
			data.setGLCode1("D301010000");
			data.setGLCode2("D301010000");
			data.setGLCode3("");
			data.setGLCode4("");
			data.setGLCode5("");
			data.setBaseCurCode("MMK");
			data.setBaseCurOperator("/");
			data.setBaseRate(1);
			data.setBaseAmount(amount);
			data.setMediumCurCode("");
			data.setMediumCurOperator("");
			data.setMediumRate(0);
			data.setMediumAmount(0);
			data.setTrCurCode("MMK");
			data.setTrCurOperator("");
			data.setTrRate(1);
			data.setTrAmount(amount);
			data.setTrPrevBalance(crPrevBal);
			data.setN1(status);
			data.setN2(0);
			data.setN3(0);
			data.setT1("");
			data.setT2("");
			data.setT3("");
			dataList.add(data);
		}
		if (dao.save(dataList, conn)) {
			ret.setCode("0000");
			ret.setDesc("AccountGLTransaction saved successfully.");
		} else {
			ret.setCode("0018");
			ret.setDesc("AccountGLTransaction saved fail.");
		}
		return ret;
	}

	private EPIXResponse makeTransaction(String drAccNo, double drPrevBal, String crAccNo, double crPrevBal,
			double amount, int transRef, int status, String subRef, String isTrOrRev, String drRemark, String crRemark,
			String drAccRef, String crAccRef, String superVisorID, String narrative, Connection conn)
			throws SQLException {
		EPIXResponse ret = new EPIXResponse();
		AccountTransactionData data = new AccountTransactionData();
		// ArrayList<AccountTransactionData> dataList = new
		// ArrayList<AccountTransactionData>();
		AccountTransactionDao dao = new AccountTransactionDao();
		String effectiveDate = getEffectiveTransDate(GeneralUtil.getDateYYYYMMDDSimple(), conn);
		String XtransRef = "";
		// prepare Dr Info
		if (!drAccNo.equals("")) {
			data.setBranchCode("001");
			data.setWorkStation("Mobile");
			if (isTrOrRev.equalsIgnoreCase("REV")) {
				data.setTransRef(transRef);
			}
			data.setSerialNo(0);
			data.setTellerId("admin");
			data.setSupervisorId(superVisorID);
			data.setTransTime(GeneralUtil.getDateYYYYMMDDHHMMSS());
			data.setTransDate(GeneralUtil.getDateYYYYMMDD());
			data.setDescription(drRemark);
			data.setChequeNo("");
			data.setCurrencyCode("MMK");
			data.setCurrencyRate(1);
			data.setAmount(amount);
			data.setTransType(705);
			data.setAccNumber(drAccNo);
			data.setPrevBalance(drPrevBal);
			data.setPrevUpdate(GeneralUtil.getDateYYYYMMDD());
			data.setEffectiveDate(effectiveDate);
			data.setContraDate("");
			data.setStatus(status);
			data.setAccRef(drAccRef);
			data.setRemark(narrative);
			data.setSystemCode(0);
			data.setSubRef(subRef);
			ret = dao.addAccTransaction(data, conn);
			if (ret.getCode().equals("0000")) {
				XtransRef = ret.getXref();
				if (isTrOrRev.equalsIgnoreCase("TR")) {
					if (subRef.equals(""))
						subRef = XtransRef;
					if (dao.updateTransRefByTransNo(Integer.parseInt(XtransRef), subRef, conn)) {
						ret.setCode("0000");
						// subRef = XtransRef;
					}
				}
			}
		}
		// prepare Cr Info
		if (ret.getCode().equals("0000")) {
			if (!crAccNo.equals("")) {
				data = new AccountTransactionData();
				data.setBranchCode("001");
				data.setWorkStation("Mobile");
				if (isTrOrRev.equalsIgnoreCase("REV")) {
					data.setTransRef(transRef);
				} else {
					data.setTransRef(Integer.parseInt(XtransRef));
				}
				data.setSerialNo(0);
				data.setTellerId("admin");
				data.setSupervisorId(superVisorID);
				data.setTransTime(GeneralUtil.getDateYYYYMMDDHHMMSS());
				data.setTransDate(GeneralUtil.getDateYYYYMMDD());
				data.setDescription(crRemark);
				data.setChequeNo("");
				data.setCurrencyCode("MMK");
				data.setCurrencyRate(1);
				data.setAmount(amount);
				data.setTransType(205);
				data.setAccNumber(crAccNo);
				data.setPrevBalance(crPrevBal);
				data.setPrevUpdate(GeneralUtil.getDateYYYYMMDD());
				data.setEffectiveDate(effectiveDate);
				data.setContraDate("");
				data.setStatus(status);
				data.setAccRef(crAccRef);
				data.setRemark(narrative);
				data.setSystemCode(0);
				data.setSubRef(subRef);

				ret = dao.addAccTransaction(data, conn);
				if (ret.getCode().equals("0000")) {
					ret.setCode("0000");
					ret.setDesc("AccountTransaction saved successfully.");
					ret.setXref(XtransRef);
				} else {
					ret.setCode("0018");
					ret.setDesc("AccountTransaction saved fail.");
				}
			}
		}
		return ret;
	}

	public EPIXResponse reversalPaymentWallet(String id, String merchantID, String amount, String comAmt, String taxAmt,
			String trnRef, String narrative) {
		Connection conn = null;
		Connection conn2 = null;
		EPIXResponse ret = new EPIXResponse();
		AccountData l_CrAccData = new AccountData();
		AccountTransactionData l_ATData = new AccountTransactionData();
		DAOManager l_DAOMgr = new DAOManager();
		AccountDao l_AccDao = new AccountDao();
		AccountGLTransactionDao l_AGDao = new AccountGLTransactionDao();
		AccountTransactionDao l_ATDao = new AccountTransactionDao();
		CMSMerchantAccRefData l_MCData = new CMSMerchantAccRefData();
		CMSMerchantAccDao l_MCDao = new CMSMerchantAccDao();
		int transRef = Integer.parseInt(trnRef);
		try {
			conn = l_DAOMgr.openICBSConnection();
			if (conn != null) {
				l_CrAccData = l_AccDao.getAccountByID(id, conn);
				if (l_CrAccData.getAccountNumber().equals("")) {
					ret.setCode("0019");
					ret.setDesc("Credit account doesn't exist.");
				} else {
					conn2 = l_DAOMgr.openConnection();
					l_MCData = l_MCDao.readDataAccById(merchantID, conn2);
					if (l_MCData.getAccountNumber().equals("")) {
						ret.setCode("0022");
						ret.setDesc("Merchant doesn't exist.");
					} else {
						ret.setCode("0000");
						conn2.close();
					}
				}
				l_ATData = l_ATDao.getDataByTransRef(transRef, conn);
				if (l_ATData.getAmount() != Double.parseDouble(amount)) {
					ret.setCode("0022");
					ret.setDesc("Amount doesn't match.");
				} else {
					ret.setCode("0000");
				}
				// conn.setAutoCommit(false);
				if (ret.getCode().equals("0000")) {
					if (l_ATDao.updateStatusByTransRef(transRef, conn)) {
						// make transaction
						String isTrOrRev = "REV";
						String drRemark = "Reversal to " + l_MCData.getT10(); // MerchantName
						String crRemark = "Reversal to " + l_MCData.getT10();
						if (ret.getCode().equals("0000")) {
							ret = makeTransaction("3898001", 0, l_CrAccData.getAccountNumber(),
									l_CrAccData.getCurrentBalance(), Double.parseDouble(amount), transRef, 255,
									String.valueOf(transRef), isTrOrRev, drRemark, crRemark, "", id, merchantID,
									narrative, conn);
						}
						if (ret.getCode().equals("0000")) {
							ret = makeGLTransaction("3898001", 0, l_CrAccData.getAccountNumber(),
									l_CrAccData.getCurrentBalance(), Double.parseDouble(amount),
									Integer.parseInt(ret.getXref()), 255, conn);
						}
						if (l_AGDao.updateStatusByTransRef(transRef, conn)) {
							if (ret.getCode().equals("0000")) {
								if (l_AccDao.updateCrCurrentBal(l_CrAccData.getAccountNumber(),
										Double.parseDouble(amount), GeneralUtil.getDateYYYYMMDD(),
										GeneralUtil.getDateYYYYMMDD(), conn)) {
									if (ret.getCode().equals("0000")) {
										// conn.setAutoCommit(true);
										conn.close();
										ret.setXref(trnRef);
										ret.setCode("0000");
										ret.setDesc("Reverse successfully.");
									}
								}
							}
						}
					} else {
						ret.setCode("0022");
						ret.setDesc("Transaction not found.");
					}
				}
			} else {
				ret.setCode("0015");
				ret.setDesc("Connection error.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			ret.setCode("0014");
			ret.setDesc("Server error.");
		} finally {
			if (conn != null) {
				try {
					if (!conn.isClosed())
						conn.close();
				} catch (SQLException e) {
					ret.setCode("0014");
					ret.setDesc("Server error.");
				}
			}
			if (conn2 != null) {
				try {
					if (!conn2.isClosed())
						conn2.close();
				} catch (SQLException e) {
					ret.setCode("0014");
					ret.setDesc("Server error.");
				}
			}
		}
		return ret;
	}

}
