package com.nirvasoft.rp.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Geneal {

	public static int mLastIndx;
	public static HashMap<String, String> l_MsgList = new HashMap<String, String>();

	public static String commaTrim(String p) {
		double retDouble = Double.MIN_VALUE;
		String ret = "0";
		String l_Str = "";

		try {
			l_Str = p.replace(",", "");
			if (l_Str.equals(""))
				retDouble = 0;
			else
				retDouble = Double.parseDouble(l_Str);
			DecimalFormat l_df = new DecimalFormat("###0.00");
			ret = l_df.format(retDouble);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return ret;
	}

	public static String datetimeOracleFormat(String p_date) {// 25/08/2014
		String ret = "";
		p_date = p_date.replace("/", "");// 25082014
		try {
			ret = p_date.substring(4, 8) + "-" + p_date.substring(2, 4) + "-" + p_date.substring(0, 2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;// 2014-08-25
	}

	public static String datetimeTostring(String pDate) {
		String l_Date = "";
		l_Date = pDate.substring(6, 8) + "/" + pDate.substring(4, 6) + "/" + pDate.substring(0, 4);
		return l_Date;
	}

	public static String datetoString() {
		String l_date = "";
		java.util.Date l_Date = new java.util.Date();
		SimpleDateFormat l_sdf = new SimpleDateFormat("yyyyMMdd");
		l_date = l_sdf.format(l_Date);

		return l_date;
	}

	public static String dateToString(String aDateFormat) {
		String l_date = "";
		String[] l_Format = aDateFormat.split("/");
		l_date = l_Format[2] + l_Format[1] + l_Format[0];

		return l_date;
	}

	public static String datetoStringwithSlash() {
		String l_date = "";
		java.util.Date l_Date = new java.util.Date();
		SimpleDateFormat l_sdf = new SimpleDateFormat("yyyy/MM/dd");
		l_date = l_sdf.format(l_Date);

		return l_date;
	}

	public static String datetoStringwithSlashMAB() {
		String l_date = "";
		java.util.Date l_Date = new java.util.Date();
		SimpleDateFormat l_sdf = new SimpleDateFormat("dd/MM/yyyy");
		l_date = l_sdf.format(l_Date);

		return l_date;
	}

	public static String decimalstoString(double pAmount) {
		DecimalFormat l_df = new DecimalFormat("###0.00");
		return l_df.format(pAmount);
	}

	public static String formatDate2DDMMYYYY() {// in Ledger Balance
		String ret = "";
		try {
			String l_date = "";
			java.util.Date l_Date = new java.util.Date();
			SimpleDateFormat l_sdf = new SimpleDateFormat("dd/MM/yyyy");//// 03/09/2014
			l_date = l_sdf.format(l_Date);

			return l_date;
		} catch (Exception ex) {
		}
		return ret;
	}

	public static String formatDate2DDMMYYYY(Date d) {
		String ret = "";
		try {
			SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
			ret = fmt.format(d);
		} catch (Exception ex) {
		}
		return ret;
	}

	public static String formatDBDate2MIT(String p) {
		return formatYYYYMMDD2MIT(p); // <<<<<<<<<<<<<<<<<<<<<<<<<<<< COFIG
	}

	public static String formatNumber(double pAmount) {
		String l_result = "0.00";
		DecimalFormat l_df = new DecimalFormat("###0.00");
		l_result = l_df.format(pAmount);
		return l_result;
	}

	public static String formatNumberWithComma(double pAmount) {
		String l_result = "0.00";
		DecimalFormat l_df = new DecimalFormat("#,##0.00");
		l_result = l_df.format(pAmount);
		return l_result;
	}

	public static String formatPassbookDate(String d) {

		SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat format2 = new SimpleDateFormat("dd-MMM-yy");
		Date date = null;
		try {
			date = format1.parse(d);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return format2.format(date).toUpperCase();
	}

	public static String formatYYYYMMDD2MIT(String p) {
		String ret = "";
		try {
			if (p.length() >= 10) {
				ret = p.replaceAll("-", "").substring(0, 8);
			} else {
				ret = "19000101";
			}

		} catch (Exception exp) {
		}
		return ret;
	}

	public static String getBudgetYearForSave(String aDate) {
		// aDate ==> yyyyMMdd 20120322

		String l_BYear = "";

		int l_Month = Integer.parseInt(aDate.substring(4, 6));
		int l_Year = Integer.parseInt(aDate.substring(2, 4));
		int l_NYear = 0;

		if (l_Month >= 4) {
			l_NYear = l_Year + 1;
			l_BYear = l_Year + "" + l_NYear;

		} else {
			l_NYear = l_Year - 1;
			l_BYear = l_NYear + "" + l_Year;
		}

		return l_BYear;
	}

	public static String getCurrentDateYYYYMMDDHHMMSS() {
		java.util.Date l_Date = new Date();
		SimpleDateFormat l_sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
		String l_dateString = l_sdf.format(l_Date);
		return l_dateString;
	}

	public static String getLastTwoDays() {
		String l_date = "";
		java.util.Date l_Date = new java.util.Date();
		SimpleDateFormat l_sdf = new SimpleDateFormat("yyyyMMdd");

		l_date = l_sdf.format(l_Date);

		int year = Integer.parseInt(l_date.substring(0, 4));
		int month = Integer.parseInt(l_date.substring(4, 6));
		int day = Integer.parseInt(l_date.substring(6, 8));

		Calendar cal = Calendar.getInstance();
		cal.set(year, month - 1, day - 2); // month start from 0

		l_date = l_sdf.format(cal.getTime());

		return l_date;
	}

	public static String getMsg(String p) {
		String ret = "Unspecified Message!";
		if (p.equals("ServerError"))
			ret = "Server connection failure! :(";
		else if (p.equals("CodeError"))
			ret = "Error reading system codes!";
		else
			ret = l_MsgList.get(p);
		return ret;
	}

	public static String getShootMonth(String monthText) {
		String month = "";

		if (monthText.equals("January")) {
			month = "01";
		} else if (monthText.equals("February")) {
			month = "02";
		} else if (monthText.equals("March")) {
			month = "03";
		} else if (monthText.equals("April")) {
			month = "04";
		} else if (monthText.equals("May")) {
			month = "05";
		} else if (monthText.equals("June")) {
			month = "06";
		} else if (monthText.equals("July")) {
			month = "07";
		} else if (monthText.equals("August")) {
			month = "08";
		} else if (monthText.equals("September")) {
			month = "09";
		} else if (monthText.equals("October")) {
			month = "10";
		} else if (monthText.equals("November")) {
			month = "11";
		} else if (monthText.equals("December")) {
			month = "12";
		}

		return month;
	}

	public static Long GetSyskey() {
		Date l_Date = new Date();
		String number = "";
		DateFormat l_DateFormat = DateFormat.getDateInstance(DateFormat.SHORT, Locale.UK);
		String l_trDate = l_DateFormat.format(l_Date);

		l_DateFormat = DateFormat.getTimeInstance(DateFormat.LONG, Locale.UK);
		String l_trTime = l_DateFormat.format(l_Date);

		StringBuilder l_Str = new StringBuilder();

		l_Str.append(l_trDate.substring(6, 8));// year
		l_Str.append(l_trDate.substring(3, 5));// month
		l_Str.append(l_trDate.substring(0, 2));// day
		l_Str.append(l_trTime.substring(0, 2));// hour
		l_Str.append(l_trTime.substring(3, 5));// minute
		l_Str.append(l_trTime.substring(6, 8));// second

		Random random = new Random();
		int l_Number = (random.nextInt(99));

		if (mLastIndx > 9999) {
			mLastIndx = 1;
		} else {
			mLastIndx += 1;
		}

		l_Str.append(String.format("%02d", l_Number));
		l_Str.append(String.format("%04d", mLastIndx));

		System.out.println(l_Str);

		number = l_Str.toString();
		return Long.parseLong(number);
	}

	public static String getTime() {
		String l_Time = "";
		SimpleDateFormat l_DateFormat = new SimpleDateFormat("HH:mm:ss");
		Calendar l_Canlendar = Calendar.getInstance();
		l_Time = l_DateFormat.format(l_Canlendar.getTime());

		return l_Time;
	}

	public static String getTimeAMPM(Date d) {
		String strDateFormat = "hh:mm a";
		SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat);
		String smallLetterTimeOnly = sdf.format(d);
		return smallLetterTimeOnly.toLowerCase();
	}

	public static String GetXREF() {
		Date l_Date = new Date();

		DateFormat l_DateFormat = DateFormat.getDateInstance(DateFormat.SHORT, Locale.UK);
		String l_trDate = l_DateFormat.format(l_Date);

		l_DateFormat = DateFormat.getTimeInstance(DateFormat.LONG, Locale.UK);
		String l_trTime = l_DateFormat.format(l_Date);
		StringBuilder l_Str = new StringBuilder();
		l_Str.append("FJB");
		l_Str.append(l_trDate.substring(6, 8));// year
		l_Str.append(l_trDate.substring(3, 5));// month
		l_Str.append(l_trDate.substring(0, 2));// day
		l_Str.append(l_trTime.substring(0, 2));// hour
		l_Str.append(l_trTime.substring(3, 5));// minute
		l_Str.append(l_trTime.substring(6, 8));// second
		Random random = new Random();
		int l_Number = (random.nextInt(99));
		l_Str.append(String.format("%02d", l_Number));

		return l_Str.toString();

	}

	public static boolean isDate(String p) {
		return (toDate(p) != null);
	}

	public static boolean isHaveSpecialChar(String pName) {
		Pattern p = Pattern.compile("[\\p{Punct}]+");
		Matcher m = p.matcher(pName);
		boolean b = m.find();

		return b;
	}

	public static boolean isNumber(String p) {
		boolean l_ret = false;
		try {
			if (p.matches("\\d*"))
				l_ret = true;

		} catch (Exception ex) {
		}
		return l_ret;
	}

	public static boolean isWeekEnd(String pDate) { // Date Format yyyymmdd
		boolean ret = false;
		int year = Integer.parseInt(pDate.substring(0, 4));
		int month = Integer.parseInt(pDate.substring(4, 6));
		int day = Integer.parseInt(pDate.substring(6, 8));

		Calendar cal = Calendar.getInstance();
		cal.set(year, month - 1, day); // month start from 0

		if (cal.get(Calendar.DAY_OF_WEEK) == 1 || cal.get(Calendar.DAY_OF_WEEK) == 7) {
			ret = true;
		}
		return ret;
	}

	public static String leadZeros(String p, int size) {
		String ret = p;
		for (int i = p.length(); i < size; i++) {
			ret = "0" + ret;
		}
		return ret;
	}

	public static HashMap<String, String> readSystemMsgCodes() throws Exception {

		String l_FilePath = ServerGlobal.getAppPath() + "reference\\MsgCode.txt";

		BufferedReader l_BufReader = new BufferedReader(new FileReader(l_FilePath));
		String l_Str = "";
		while ((l_Str = l_BufReader.readLine()) != null) {

			l_MsgList.put(l_Str.split("__")[0], l_Str.split("__")[1]);

		}
		l_BufReader.close();

		return l_MsgList;
	}

	public static double round2decimals(double p) {
		return round2decimals(p, RoundingMode.HALF_EVEN);
	}

	public static double round2decimals(double pAmount, RoundingMode mode) {
		double l_result = 0;
		DecimalFormat l_df = new DecimalFormat("###0.00");
		l_df.setRoundingMode(mode);

		l_result = Double.valueOf(l_df.format(pAmount));
		return l_result;
	}

	public static String timetoString() {
		String l_date = "";
		java.util.Date l_Date = new java.util.Date();
		SimpleDateFormat l_sdf = new SimpleDateFormat("hhmmss");
		l_date = l_sdf.format(l_Date);

		return l_date;
	}

	public static Date toDate(String p) {
		return toDate(p, "dd/MM/yyyy");
	}

	public static Date toDate(String p, String fmt) {
		Date ret = null;
		try {
			DateFormat full = new SimpleDateFormat(fmt); // based on system
															// configuration
			@SuppressWarnings("unused")
			DateFormat d = new SimpleDateFormat("dd");
			@SuppressWarnings("unused")
			DateFormat m = new SimpleDateFormat("MM");
			@SuppressWarnings("unused")
			DateFormat y = new SimpleDateFormat("yyyy");
			ret = full.parse(p); // new Date(System.currentTimeMillis());
			// System.out.println("Date: " + full.format(ret) + " ["+
			// d.format(ret) + "-"+ m.format(ret) + "-"+ y.format(ret) + "] ");
		} catch (Exception e) {
		}
		return ret;
	}

	public static String trimAll(String s) {
		return trimLeft(s).trim();
	}

	public static String trimLeft(String s) {
		String whitespace = new String(" \t\n\r "); //
		int j = 0;
		while (j < s.length() && whitespace.indexOf(s.charAt(j)) != -1)
			j++;
		s = s.substring(j, s.length());
		return s;
	}

}
