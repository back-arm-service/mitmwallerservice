package com.nirvasoft.rp.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import com.nirvasoft.rp.dao.DAOManager;

public class SMSUtility {
	public static String OutputDigit = "";
	public static String StepSeconds = "";
	public static String EncryptType = "";
	public static String OTPMessage = "";

	public static String URL = "";
	public static String NameSpace = "";
	public static String UserName = "";
	public static String Password = "";
	public static String URL2 = "";
	public static String Type = "";
	static {
		BufferedReader br = null;
		try {
			br = new BufferedReader(
					new FileReader(DAOManager.AbsolutePath + "WEB-INF" + "/SMS/config/MITSMSConfig.txt"));
			String st = br.readLine();
			String[] stArr = st.split("Url :");
			URL = stArr[1].trim();

			st = br.readLine();
			stArr = st.split("NameSpace :");
			NameSpace = stArr[1].trim();

			st = br.readLine();
			stArr = st.split("Username :");
			UserName = stArr[1].trim();

			st = br.readLine();
			stArr = st.split("Password :");
			Password = stArr[1].trim();
			
			st = br.readLine();
			stArr = st.split("Url2 :");
			URL2 = stArr[1].trim();
			
			st = br.readLine();
			stArr = st.split("Type :");
			Type = stArr[1].trim();
		} catch (Exception ex) {
			System.out.println("*** Error in config : " + ex.toString());
			try {
				br.close();
			} catch (IOException ex1) {
				System.out.println("*** Error in config : " + ex1.toString());
			}
		} finally {
			try {
				br.close();
			} catch (IOException ex) {
				System.out.println("*** Error in config : " + ex.toString());
			}
		}
	}

}