package com.nirvasoft.rp.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FontUtil {

	private static Pattern zawgyi = Pattern.compile("[\u105a\u1060-\u1097]|" + "[\u1033\u1034]|" + "\u1031\u108f|"
			+ "\u1031[\u103b-\u103e]|" + "[\u102b-\u1030\u1032]\u1031|" + " \u1031| \u103b|"
			+ "^\u1031|^\u103b|\u1038\u103b|\u1038\u1031|" + "[\u102d\u102e\u1032]\u103b|\u1039[^\u1000-\u1021]|\u1039$"
			+ "|\u1004\u1039[\u1001-\u102a\u103f\u104e]" + "|\u1039[^u1000-\u102a\u103f\u104e]"
			+ "|\u103c\u103b|\u103d\u103b" + "|\u103e\u103b|\u103d\u103c" + "|\u103e\u103c|\u103e\u103d"
			+ "|\u103b\u103c" + "|[\u102f\u1030\u102b\u102c][\u102d\u102e\u1032]" + "|[\u102b\u102c][\u102f\u102c]"
			+ "|[\u1040-\u1049][\u102b-\u103e\u102b-\u1030\u1032\u1036\u1037\u1038\u103a]"
			+ "|^[\u1040\u1047][^\u1040-\u1049]"
			+ "|[\u1000-\u102a\u103f\u104e]\u1039[\u101a\u101b\u101d\u101f\u1022-\u103f]" + "|\u103a\u103e"
			+ "|\u1036\u102b]" + "|\u102d[\u102e\u1032]|\u102e[\u102d\u1032]|\u1032[\u102d\u102e]"

			+ "|\u102f\u1030|\u1030\u102f" + "|\u102b\u102c|\u102c\u102b"
			+ "|[\u1090-\u1099][\u102b-\u1030\u1032\u1037\u103a-\u103e]"

			+ "|[\u1000-\u10f4][\u1090-\u1099][\u1000-\u104f]"
			+ "|^[\u1090-\u1099][\u1000-\u102a\u103f\u104e\u104a\u104b]" + "|[\u1000-\u104f][\u1090-\u1099]$"
			+ "|[\u105e-\u1060\u1062-\u1064\u1067-\u106d\u1071-\u1074\u1082-\u108d" + "\u108f\u109a-\u109d]"
			+ "[\u102b-\u103e]" + "|[\u1000-\u102a]\u103a[\u102d\u102e\u1032]"// 1031
																				// after
																				// other
																				// vowel
																				// signs
			+ "|[\u102b-\u1030\u1032\u1036-\u1038\u103a]\u1031" + "|[\u1087-\u108d][\u106e-\u1070\u1072-\u1074]"

			+ "|^[\u105e-\u1060\u1062-\u1064\u1067-\u106d\u1071-\u1074" + "\u1082-\u108d\u108f\u109a-\u109d]"
			+ "|[\u0020\u104a\u104b][\u105e-\u1060\u1062-\u1064\u1067-\u106d"
			+ "\u1071-\u1074\u1082-\u108d\u108f\u109a-\u109d]" + "|[\u1036\u103a][\u102d-\u1030\u1032]"
			+ "|[\u1025\u100a]\u1039"

			+ "|[\u108e-\u108f][\u1050-\u108d]" + "|\u102d-\u1030\u1032\u1036-\u1037]\u1039]"
			+ "|[\u1000-\u102a\u103f\u104e]\u1037\u1039"

			+ "|[\u1000-\u102a\u103f\u104e]\u102c\u1039[\u1000-\u102a\u103f\u104e]"
			+ "|[\u102b-\u1030\u1032][\u103b-\u103e]" + "|\u1032[\u103b-\u103e]" + "|\u101b\u103c"
			+ "|[\u1000-\u102a\u103f\u104e]\u1039[\u1000-\u102a\u103f\u104e]\u1039" + "[\u1000-\u102a\u103f\u104e]"
			+ "|[\u1000-\u102a\u103f\u104e]\u1039[\u1000-\u102a\u103f\u104e]" + "[\u102b\u1032\u103d]"

			+ "|[\u1000\u1005\u100f\u1010\u1012\u1014\u1015\u1019\u101a]\u1039\u1021" + "|[\u1000\u1010]\u1039\u1019"
			+ "|\u1004\u1039\u1000" + "|\u1015\u1039[\u101a\u101e]" + "|\u1000\u1039\u1001\u1036"
			+ "|\u1039\u1011\u1032"

			+ "|\u1037\u1032" + "|\u1036\u103b"

			+ "|\u102f\u102f");
	private static Pattern myanmar3Ptrn = Pattern.compile(
			"[ဃငဆဇဈဉညတဋဌဍဎဏဒဓနဘရဝဟဠအ]်|ျ[က-အ]ါ|ျ[ါ-း]|[^\1031]စ် |\u103e|\u103f|\u1031[^\u1000-\u1021\u103b\u1040\u106a\u106b\u107e-\u1084\u108f\u1090]|\u1031$|\u100b\u1039|\u1031[က-အ]\u1032|\u1025\u102f|\u103c\u103d[\u1000-\u1001]/");

	public static boolean isZawgyi(String font) {
		boolean l_ret = false;
		if (((Matcher) zawgyi.matcher(font)).matches()) {
			l_ret = true;
		}
		return l_ret;
	}

	public static Boolean isZawgyiEncoded(String value) {
		Matcher matcher = zawgyi.matcher(value);
		return matcher.find();
	}

	public static boolean isMyanmar3(String font) {
		boolean l_ret = false;
		if (((Matcher) myanmar3Ptrn.matcher(font)).matches()) {
			l_ret = true;
		}
		return l_ret;
	}

	public static Boolean isUniEncoded(String value) {
		Matcher matcher = myanmar3Ptrn.matcher(value);
		return matcher.find();
	}

	public static String myan_number[] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };

	public static String changeMyanNumber(String number) {
		if (number.contains(".")) {
			number = number.replace(".", "/");
			String[] splitcomma = number.split("/");
			number = "";
			for (int i = (splitcomma.length - 1); i >= 0; i--) {
				if (splitcomma[i].length() < 2) {
					splitcomma[i] = "၀" + splitcomma[i];
				}
				number += splitcomma[i];
			}
		}
		String result = "";
		String num[] = number.split("");
		for (int i = 0; i < num.length; i++) {
			switch (num[i]) {
			case "၀":
				result += myan_number[0];
				break;
			case "၁":
				result += myan_number[1];
				break;
			case "၂":
				result += myan_number[2];
				break;
			case "၃":
				result += myan_number[3];
				break;
			case "၄":
				result += myan_number[4];
				break;
			case "၅":
				result += myan_number[5];
				break;
			case "၆":
				result += myan_number[6];
				break;
			case "၇":
				result += myan_number[7];
				break;
			case "၈":
				result += myan_number[8];
				break;
			case "၉":
				result += myan_number[9];
				break;
			default:
				result += "";
				break;
			}

		}
		return result;
	}

	public static String eng_number[] = { "၀", "၁", "၂", "၃", "၄", "၅", "၆", "၇", "၈", "၉" };

	public static String changeEngNumber(String number) {
		if (number.contains(",")) {
			String[] splitcomma = number.split(",");
			number = "";
			for (int i = 0; i < splitcomma.length; i++) {
				number += splitcomma[i];
			}
		}
		String result = "";
		String num[] = number.split("");
		for (int i = 0; i < num.length; i++) {
			switch (num[i]) {
			case "0":
				result += eng_number[0];
				break;
			case "1":
				result += eng_number[1];
				break;
			case "2":
				result += eng_number[2];
				break;
			case "3":
				result += eng_number[3];
				break;
			case "4":
				result += eng_number[4];
				break;
			case "5":
				result += eng_number[5];
				break;
			case "6":
				result += eng_number[6];
				break;
			case "7":
				result += eng_number[7];
				break;
			case "8":
				result += eng_number[8];
				break;
			case "9":
				result += eng_number[9];
				break;
			}
		}
		if (!result.equals("")) {
			String year = result.substring(0, 4);
			String month = result.substring(4, 6);
			String day = result.substring(6);
			result = day + "." + month + "." + year;
		}
		return result;
	}

}
