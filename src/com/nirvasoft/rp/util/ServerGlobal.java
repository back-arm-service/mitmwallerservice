package com.nirvasoft.rp.util;

public class ServerGlobal {

	private static String mAppPath = "";
	private static String mSeparator = "\\|\\|\\_\\_";
	private static String mFCSeparator = "\\_\\_\\|";
	private static boolean mSetWriteLog = false;
	private static String fortuneYgnMerchant;
	private static String fortuneMdyMerchant;
	private static String mFCRTServiceURL = "";
	private static String MPOServiceURL = "";
	private static String mFCMakerID = "";
	private static String mFCCheckerID = "";
	private static String mEPIXServiceURL = "";

	private static String mFCACServiceURL = "";
	private static String mFCCUServiceURL = "";
	private static int mBrCodeStart = 0;
	private static int mBrCodeEnd = 0;
	private static String mXREFPrefix = "";
	private static String BankName = "";
	// CNP
	private static String mGetBillURL = "";
	private static String mMakePaymentURL = "";
	private static String mGetTxnURL = "";
	private static String mTopupURL = "";
	private static String mAPIKey = "";
	private static String DueTime;
	private static String DueDay;
	private static String BankRate = "";

	// Sky Pay
	private static String mRechargeUrl = "";
	private static String muserName = "";
	private static String mpass = "";
	private static String mpop = "";
	private static String mEntID = "";
	// CCD
	private static String mGetCCDURL = "";
	private static String mGetCCDURLTrans = "";
	private static String mGetCCDKEY = "";
	private static String mGetCCDINITVECTOR = "";
	private static String mbURL = "";
	private static String mGetCCDPURL = "";
	private static String mGetCCDPaidUnPaidURL = "";
	private static String csURL = "";
	
	private static String mBranchCode = "";
	private static String mProduct = "";
	private static String mAccType = "";
	private static String mCurrencyCode = "MMK";
	private static String mCashInHand = "";
	private static int mZone = 0;
	
	private static double mCurRate = 0;
	private static int mMinBalSetting = 0;
	private static String mByForceCheque = "";
	private static boolean isHasCheque = false;
	private static String mWalletGL = "";
	private static String loanGL = "";
	
	private static String mCutOffTimeHr = "";
	private static String mCutOffTimeMin = "";
	
	public static int CREDIT = 2;
	public static int TDeposit = 6;
	
	private static String mGetCCDConnectTimeout = "";
	private static String mGetCCDReadTimeout = "";
	
	//loan	
	private static int activeStatus;
	private static int loginStatus;
	private static int suspendStatus;
	private static int deleteStatus;
	private static int AnonymousPos;
	private static int LoanmemberPos;
	private static int GuarantorPos;
	private static int SuspendedPos;
	private static int ActivePos;
	private static int RegisteredPos;
	private static int LoginPos;
	private static int totalbitstatus;
	
	private static int loanActivePos;
	private static int loanInprocessPos;
	private static int loanApprovedPos;
	private static int loanGoodsDeliveredPos;
	private static int loanRepaymentInProgressPos;
	private static int loanRepaymentCompletedPos;
	private static int loantotalbitsstatus;
	private static String mfiURL = "";//hp loan
	
	//staff loan
	private static String ProuctImagePath="";
	private static String OutletImagePath="";
	private static String ProductLoanImagePath="";
	private static String StaffLoanImagePath="";
	private static String HirePurchaseImagePath="";
	private static String EducationLoanImagePath ="";
	private static String CommercialLoanImagePath ="";
	private static String OverdraftLoanImagePath ="";
	private static String JICATwoStepLoanImagePath ="";
	private static String CGILoanImagePath ="";
	private static String CFBLoanImagePath  ="";
	private static String StaffLoanPDFDownloadPath="";
	
	private static String mrhforg ="";
	private static String smidborg ="";
	
	public static String getLoanGL() {
		return loanGL;
	}

	public static void setLoanGL(String loanGL) {
		ServerGlobal.loanGL = loanGL;
	}

	public static String getMrhforg() {
		return mrhforg;
	}

	public static void setMrhforg(String mrhforg) {
		ServerGlobal.mrhforg = mrhforg;
	}

	public static String getSmidborg() {
		return smidborg;
	}

	public static void setSmidborg(String smidborg) {
		ServerGlobal.smidborg = smidborg;
	}

	public static String getStaffLoanPDFDownloadPath() {
		return StaffLoanPDFDownloadPath;
	}

	public static void setStaffLoanPDFDownloadPath(String staffLoanPDFDownloadPath) {
		StaffLoanPDFDownloadPath = staffLoanPDFDownloadPath;
	}

	public static String getProuctImagePath() {
		return ProuctImagePath;
	}

	public static void setProuctImagePath(String prouctImagePath) {
		ProuctImagePath = prouctImagePath;
	}

	public static String getOutletImagePath() {
		return OutletImagePath;
	}

	public static void setOutletImagePath(String outletImagePath) {
		OutletImagePath = outletImagePath;
	}

	public static String getProductLoanImagePath() {
		return ProductLoanImagePath;
	}

	public static void setProductLoanImagePath(String productLoanImagePath) {
		ProductLoanImagePath = productLoanImagePath;
	}

	public static String getHirePurchaseImagePath() {
		return HirePurchaseImagePath;
	}

	public static void setHirePurchaseImagePath(String hirePurchaseImagePath) {
		HirePurchaseImagePath = hirePurchaseImagePath;
	}

	public static String getEducationLoanImagePath() {
		return EducationLoanImagePath;
	}

	public static void setEducationLoanImagePath(String educationLoanImagePath) {
		EducationLoanImagePath = educationLoanImagePath;
	}

	public static String getCommercialLoanImagePath() {
		return CommercialLoanImagePath;
	}

	public static void setCommercialLoanImagePath(String commercialLoanImagePath) {
		CommercialLoanImagePath = commercialLoanImagePath;
	}

	public static String getOverdraftLoanImagePath() {
		return OverdraftLoanImagePath;
	}

	public static void setOverdraftLoanImagePath(String overdraftLoanImagePath) {
		OverdraftLoanImagePath = overdraftLoanImagePath;
	}

	public static String getJICATwoStepLoanImagePath() {
		return JICATwoStepLoanImagePath;
	}

	public static void setJICATwoStepLoanImagePath(String jICATwoStepLoanImagePath) {
		JICATwoStepLoanImagePath = jICATwoStepLoanImagePath;
	}

	public static String getCGILoanImagePath() {
		return CGILoanImagePath;
	}

	public static void setCGILoanImagePath(String cGILoanImagePath) {
		CGILoanImagePath = cGILoanImagePath;
	}

	public static String getCFBLoanImagePath() {
		return CFBLoanImagePath;
	}

	public static void setCFBLoanImagePath(String cFBLoanImagePath) {
		CFBLoanImagePath = cFBLoanImagePath;
	}

	public static String getStaffLoanImagePath() {
		return StaffLoanImagePath;
	}

	public static void setStaffLoanImagePath(String staffLoanImagePath) {
		StaffLoanImagePath = staffLoanImagePath;
	}

	public static String getMfiURL() {
		return mfiURL;
	}

	public static void setMfiURL(String mfiURL) {
		ServerGlobal.mfiURL = mfiURL;
	}

	public static String getmGetCCDURLTrans() {
		return mGetCCDURLTrans;
	}

	public static void setmGetCCDURLTrans(String mGetCCDURLTrans) {
		ServerGlobal.mGetCCDURLTrans = mGetCCDURLTrans;
	}

	public static String getmGetCCDConnectTimeout() {
		return mGetCCDConnectTimeout;
	}

	public static void setmGetCCDConnectTimeout(String mGetCCDConnectTimeout) {
		ServerGlobal.mGetCCDConnectTimeout = mGetCCDConnectTimeout;
	}

	public static String getmGetCCDReadTimeout() {
		return mGetCCDReadTimeout;
	}

	public static void setmGetCCDReadTimeout(String mGetCCDReadTimeout) {
		ServerGlobal.mGetCCDReadTimeout = mGetCCDReadTimeout;
	}

	public static String getAppPath() {
		return mAppPath;
	}

	public static String getBankName() {
		return BankName;
	}

	public static String getmGetCCDPaidUnPaidURL() {
		return mGetCCDPaidUnPaidURL;
	}

	public static void setmGetCCDPaidUnPaidURL(String mGetCCDPaidUnPaidURL) {
		ServerGlobal.mGetCCDPaidUnPaidURL = mGetCCDPaidUnPaidURL;
	}

	public static String getBankRate() {
		return BankRate;
	}

	public static String getDueDay() {
		return DueDay;
	}

	public static String getDueTime() {
		return DueTime;
	}

	public static String getFCCheckerID() {
		return mFCCheckerID;
	}

	public static String getFCMakerID() {
		return mFCMakerID;
	}

	public static String getFCRTServiceURL() {
		return mFCRTServiceURL;
	}

	public static String getFCSeparator() {
		return mFCSeparator;
	}

	public static String getFortuneMdyMerchant() {
		return fortuneMdyMerchant;
	}

	public static String getFortuneYgnMerchant() {
		return fortuneYgnMerchant;
	}

	public static String getmAPIKey() {
		return mAPIKey;
	}

	public static String getmAppPath() {
		return mAppPath;
	}

	public static int getmBrCodeEnd() {
		return mBrCodeEnd;
	}

	public static int getmBrCodeStart() {
		return mBrCodeStart;
	}

	public static String getMbURL() {
		return mbURL;
	}

	public static String getMEntID() {
		return mEntID;
	}
	
	public static String getCsURL() {
		return csURL;
	}

	public static void setCsURL(String csURL) {
		ServerGlobal.csURL = csURL;
	}

	public static String getmEPIXServiceURL() {
		return mEPIXServiceURL;
	}

	public static String getmFCACServiceURL() {
		return mFCACServiceURL;
	}

	public static String getmFCCUServiceURL() {
		return mFCCUServiceURL;
	}

	public static String getmFCRTServiceURL() {
		return mFCRTServiceURL;
	}

	public static String getmFCSeparator() {
		return mFCSeparator;
	}

	public static String getmGetBillURL() {
		return mGetBillURL;
	}

	public static String getmGetCCDINITVECTOR() {
		return mGetCCDINITVECTOR;
	}

	public static String getmGetCCDKEY() {
		return mGetCCDKEY;
	}

	public static String getmGetCCDURL() {
		return mGetCCDURL;
	}

	public static String getmGetTxnURL() {
		return mGetTxnURL;
	}

	public static String getmMakePaymentURL() {
		return mMakePaymentURL;
	}

	public static String getMpass() {
		return mpass;
	}

	public static String getMpop() {
		return mpop;
	}

	public static String getMPOServiceURL() {
		return MPOServiceURL;
	}

	public static String getmRechargeUrl() {
		return mRechargeUrl;
	}

	public static String getmSeparator() {
		return mSeparator;
	}

	public static String getmTopupURL() {
		return mTopupURL;
	}

	public static String getMuserName() {
		return muserName;
	}

	public static String getmXREFPrefix() {
		return mXREFPrefix;
	}

	public static String getSeparator() {
		return mSeparator;
	}

	public static boolean ismSetWriteLog() {
		return mSetWriteLog;
	}

	public static boolean isWriteLog() {
		return mSetWriteLog;
	}

	public static void setAppPath(String mAppPath) {
		ServerGlobal.mAppPath = mAppPath;
	}

	public static void setBankName(String bankName) {
		BankName = bankName;
	}

	public static void setBankRate(String bankRate) {
		BankRate = bankRate;
	}

	public static void setDueDay(String dueDay) {
		DueDay = dueDay;
	}

	public static void setDueTime(String dueTime) {
		DueTime = dueTime;
	}

	public static void setFCCheckerID(String mFCCheckerID) {
		ServerGlobal.mFCCheckerID = mFCCheckerID;
	}

	public static void setFCMakerID(String mFCMakerID) {
		ServerGlobal.mFCMakerID = mFCMakerID;
	}

	public static void setFCRTServiceURL(String mFCURL) {
		ServerGlobal.mFCRTServiceURL = mFCURL;
	}

	public static void setFortuneMdyMerchant(String fortuneMdyMerchant) {
		ServerGlobal.fortuneMdyMerchant = fortuneMdyMerchant;
	}

	public static void setFortuneYgnMerchant(String fortuneYgnMerchant) {
		ServerGlobal.fortuneYgnMerchant = fortuneYgnMerchant;
	}

	public static void setmAPIKey(String mAPIKey) {
		ServerGlobal.mAPIKey = mAPIKey;
	}

	public static void setmAppPath(String mAppPath) {
		ServerGlobal.mAppPath = mAppPath;
	}

	public static void setmBrCodeEnd(int mBrCodeEnd) {
		ServerGlobal.mBrCodeEnd = mBrCodeEnd;
	}

	public static void setmBrCodeStart(int mBrCodeStart) {
		ServerGlobal.mBrCodeStart = mBrCodeStart;
	}

	public static void setMbURL(String mbURL) {
		ServerGlobal.mbURL = mbURL;
	}
	

	public static void setMEntID(String mEntID) {
		ServerGlobal.mEntID = mEntID;
	}

	public static void setmEPIXServiceURL(String mEPIXServiceURL) {
		ServerGlobal.mEPIXServiceURL = mEPIXServiceURL;
	}

	public static void setmFCACServiceURL(String mFCACServiceURL) {
		ServerGlobal.mFCACServiceURL = mFCACServiceURL;
	}

	public static void setmFCCUServiceURL(String mFCCUServiceURL) {
		ServerGlobal.mFCCUServiceURL = mFCCUServiceURL;
	}

	public static void setmFCRTServiceURL(String mFCRTServiceURL) {
		ServerGlobal.mFCRTServiceURL = mFCRTServiceURL;
	}

	public static void setmFCSeparator(String mFCSeparator) {
		ServerGlobal.mFCSeparator = mFCSeparator;
	}

	public static void setmGetBillURL(String mGetBillURL) {
		ServerGlobal.mGetBillURL = mGetBillURL;
	}

	public static void setmGetCCDINITVECTOR(String mGetCCDINITVECTOR) {
		ServerGlobal.mGetCCDINITVECTOR = mGetCCDINITVECTOR;
	}

	public static void setmGetCCDKEY(String mGetCCDKEY) {
		ServerGlobal.mGetCCDKEY = mGetCCDKEY;
	}

	public static void setmGetCCDURL(String mGetCCDURL) {
		ServerGlobal.mGetCCDURL = mGetCCDURL;
	}

	public static void setmGetTxnURL(String mGetTxnURL) {
		ServerGlobal.mGetTxnURL = mGetTxnURL;
	}

	public static void setmMakePaymentURL(String mMakePaymentURL) {
		ServerGlobal.mMakePaymentURL = mMakePaymentURL;
	}

	public static void setMpass(String mpass) {
		ServerGlobal.mpass = mpass;
	}

	public static void setMpop(String mpop) {
		ServerGlobal.mpop = mpop;
	}

	public static void setMPOServiceURL(String mPOServiceURL) {
		MPOServiceURL = mPOServiceURL;
	}

	public static void setmRechargeUrl(String mRechargeUrl) {
		ServerGlobal.mRechargeUrl = mRechargeUrl;
	}

	public static void setmSeparator(String mSeparator) {
		ServerGlobal.mSeparator = mSeparator;
	}

	public static void setmSetWriteLog(boolean mSetWriteLog) {
		ServerGlobal.mSetWriteLog = mSetWriteLog;
	}

	public static void setmTopupURL(String mTopupURL) {
		ServerGlobal.mTopupURL = mTopupURL;
	}

	public static void setMuserName(String muserName) {
		ServerGlobal.muserName = muserName;
	}

	public static void setmXREFPrefix(String mXREFPrefix) {
		ServerGlobal.mXREFPrefix = mXREFPrefix;
	}

	public static void setWriteLog(boolean mWLog) {
		ServerGlobal.mSetWriteLog = mWLog;
	}

	public static String getmGetCCDPURL() {
		return mGetCCDPURL;
	}

	public static void setmGetCCDPURL(String mGetCCDPURL) {
		ServerGlobal.mGetCCDPURL = mGetCCDPURL;
	}

	public static String getmBranchCode() {
		return mBranchCode;
	}

	public static void setmBranchCode(String mBranchCode) {
		ServerGlobal.mBranchCode = mBranchCode;
	}

	public static String getmProduct() {
		return mProduct;
	}

	public static void setmProduct(String mProduct) {
		ServerGlobal.mProduct = mProduct;
	}

	public static String getmAccType() {
		return mAccType;
	}

	public static void setmAccType(String mAccType) {
		ServerGlobal.mAccType = mAccType;
	}

	public static String getmCurrencyCode() {
		return mCurrencyCode;
	}

	public static void setmCurrencyCode(String mCurrencyCode) {
		ServerGlobal.mCurrencyCode = mCurrencyCode;
	}

	public static String getmCashInHand() {
		return mCashInHand;
	}

	public static void setmCashInHand(String mCashInHand) {
		ServerGlobal.mCashInHand = mCashInHand;
	}

	public static int getmZone() {
		return mZone;
	}

	public static void setmZone(int mZone) {
		ServerGlobal.mZone = mZone;
	}

	public static Double getmCurRate() {
		return mCurRate;
	}

	public static void setmCurRate(Double mCurRate) {
		ServerGlobal.mCurRate = mCurRate;
	}

	public static int getmMinBalSetting() {
		return mMinBalSetting;
	}

	public static void setmMinBalSetting(int mMinBalSetting) {
		ServerGlobal.mMinBalSetting = mMinBalSetting;
	}

	public static String getmByForceCheque() {
		return mByForceCheque;
	}

	public static void setmByForceCheque(String mByForceCheque) {
		ServerGlobal.mByForceCheque = mByForceCheque;
	}

	public static boolean isHasCheque() {
		return isHasCheque;
	}

	public static void setHasCheque(boolean isHasCheque) {
		ServerGlobal.isHasCheque = isHasCheque;
	}

	public static String getmWalletGL() {
		return mWalletGL;
	}

	public static void setmWalletGL(String mWalletGL) {
		ServerGlobal.mWalletGL = mWalletGL;
	}

	public static String getmCutOffTimeHr() {
		return mCutOffTimeHr;
	}

	public static void setmCutOffTimeHr(String mCutOffTimeHr) {
		ServerGlobal.mCutOffTimeHr = mCutOffTimeHr;
	}

	public static String getmCutOffTimeMin() {
		return mCutOffTimeMin;
	}

	public static void setmCutOffTimeMin(String mCutOffTimeMin) {
		ServerGlobal.mCutOffTimeMin = mCutOffTimeMin;
	}

	public static int getTotalbitstatus() {
		return totalbitstatus;
	}

	public static void setTotalbitstatus(int totalbitstatus) {
		ServerGlobal.totalbitstatus = totalbitstatus;
	}

	public static int getLoanmemberPos() {
		return LoanmemberPos;
	}

	public static void setLoanmemberPos(int loanmemberPos) {
		LoanmemberPos = loanmemberPos;
	}

	public static int getGuarantorPos() {
		return GuarantorPos;
	}

	public static void setGuarantorPos(int guarantorPos) {
		GuarantorPos = guarantorPos;
	}

	public static int getLoantotalbitsstatus() {
		return loantotalbitsstatus;
	}

	public static void setLoantotalbitsstatus(int loantotalbitsstatus) {
		ServerGlobal.loantotalbitsstatus = loantotalbitsstatus;
	}

	public static int getActiveStatus() {
		return activeStatus;
	}

	public static void setActiveStatus(int activeStatus) {
		ServerGlobal.activeStatus = activeStatus;
	}

	public static int getLoginStatus() {
		return loginStatus;
	}

	public static void setLoginStatus(int loginStatus) {
		ServerGlobal.loginStatus = loginStatus;
	}

	public static int getSuspendStatus() {
		return suspendStatus;
	}

	public static void setSuspendStatus(int suspendStatus) {
		ServerGlobal.suspendStatus = suspendStatus;
	}

	public static int getDeleteStatus() {
		return deleteStatus;
	}

	public static void setDeleteStatus(int deleteStatus) {
		ServerGlobal.deleteStatus = deleteStatus;
	}

	public static int getAnonymousPos() {
		return AnonymousPos;
	}

	public static void setAnonymousPos(int anonymousPos) {
		AnonymousPos = anonymousPos;
	}

	public static int getSuspendedPos() {
		return SuspendedPos;
	}

	public static void setSuspendedPos(int suspendedPos) {
		SuspendedPos = suspendedPos;
	}

	public static int getActivePos() {
		return ActivePos;
	}

	public static void setActivePos(int activePos) {
		ActivePos = activePos;
	}

	public static int getRegisteredPos() {
		return RegisteredPos;
	}

	public static void setRegisteredPos(int registeredPos) {
		RegisteredPos = registeredPos;
	}

	public static int getLoginPos() {
		return LoginPos;
	}

	public static void setLoginPos(int loginPos) {
		LoginPos = loginPos;
	}

	public static int getLoanActivePos() {
		return loanActivePos;
	}

	public static void setLoanActivePos(int loanActivePos) {
		ServerGlobal.loanActivePos = loanActivePos;
	}

	public static int getLoanInprocessPos() {
		return loanInprocessPos;
	}

	public static void setLoanInprocessPos(int loanInprocessPos) {
		ServerGlobal.loanInprocessPos = loanInprocessPos;
	}

	public static int getLoanApprovedPos() {
		return loanApprovedPos;
	}

	public static void setLoanApprovedPos(int loanApprovedPos) {
		ServerGlobal.loanApprovedPos = loanApprovedPos;
	}

	public static int getLoanGoodsDeliveredPos() {
		return loanGoodsDeliveredPos;
	}

	public static void setLoanGoodsDeliveredPos(int loanGoodsDeliveredPos) {
		ServerGlobal.loanGoodsDeliveredPos = loanGoodsDeliveredPos;
	}

	public static int getLoanRepaymentInProgressPos() {
		return loanRepaymentInProgressPos;
	}

	public static void setLoanRepaymentInProgressPos(int loanRepaymentInProgressPos) {
		ServerGlobal.loanRepaymentInProgressPos = loanRepaymentInProgressPos;
	}

	public static int getLoanRepaymentCompletedPos() {
		return loanRepaymentCompletedPos;
	}

	public static void setLoanRepaymentCompletedPos(int loanRepaymentCompletedPos) {
		ServerGlobal.loanRepaymentCompletedPos = loanRepaymentCompletedPos;
	}	
	
}