package com.nirvasoft.rp.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.nirvasoft.rp.shared.TransferReqV2Data;

public class WriteXML {

	// public static void main(String[] args) {
	// // TODO Auto-generated method stub
	// TransferReqData req = new TransferReqData();
	// req.setTransid("1");
	// req.setRefkey("129087");
	// req.setAmount(100000.00);
	// req.setNarration("Oct");
	//
	// ModifyBalanceXmlFile(req.getAmount());
	// WriteTransactionsXmlFile(req);
	//
	// }

	// utility method to create text node
	private static Node getTransactionElements(Document doc, Element element, String name, String value) {
		Element node = doc.createElement(name);
		node.appendChild(doc.createTextNode(value));
		return node;
	}

	private static Node getTransactionsData(Document doc, String date, String id, String refno, String desc,
			String credit, String balance) {
		Element transaction = doc.createElement("Transaction");

		// set id attribute
		transaction.setAttribute("id", id);
		transaction.appendChild(getTransactionElements(doc, transaction, "Date", date));
		transaction.appendChild(getTransactionElements(doc, transaction, "RefNo", refno));
		transaction.appendChild(getTransactionElements(doc, transaction, "Desc", desc));
		transaction.appendChild(getTransactionElements(doc, transaction, "Debit", "0.00"));
		transaction.appendChild(getTransactionElements(doc, transaction, "Credit", credit));
		transaction.appendChild(getTransactionElements(doc, transaction, "Balance", balance));

		return transaction;
	}

	public static void ModifyBalanceXmlFile(double addBalance)
			throws SAXException, IOException, ParserConfigurationException {
		File file = new File("D:\\MPO\\POC\\WalletWebUI\\WalletWebUI\\WebContent\\Balance.xml");
		updateXML(file, addBalance);
	}

	// Save the updated DOM into the XML back
	private static void saveToXML(File file, Document doc) {
		try {
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");

			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			DOMSource source = new DOMSource(doc);

			transformer.transform(source, result);

			String strTemp = writer.toString();

			FileWriter fileWriter = new FileWriter(file);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

			bufferedWriter.write(strTemp);
			bufferedWriter.flush();
			bufferedWriter.close();
		} catch (Exception ex) {
		}
	}

	// Read into the relevant node and update the text value
	private static void updateTextValue(Document doc, double newAddValue) {
		Element root = doc.getDocumentElement();

		// Return the NodeList on a given tag name
		NodeList childNodes = root.getElementsByTagName("Balance");

		for (int index = 0; index < childNodes.getLength(); index++) {

			NodeList subChildNodes = childNodes.item(index).getChildNodes();

			String val = subChildNodes.item(0).getTextContent().replaceAll(",", "");

			double originalval = Double.parseDouble(val);
			double newVal = originalval + newAddValue;
			String formatdata = new DecimalFormat("#,###.00").format(newVal);
			subChildNodes.item(0).setTextContent(formatdata);

		}
	}

	private static void updateXML(File file, double strAddValue)
			throws SAXException, IOException, ParserConfigurationException {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();

		// Convert CDATA nodes to Text node append
		docFactory.setCoalescing(true);

		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(file);

		updateTextValue(doc, strAddValue);
		saveToXML(file, doc);
	}

	public static void WriteTransactionsXmlFile(TransferReqV2Data req)
			throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.newDocument();
		// add elements to Document
		Element rootElement = doc.createElementNS("", "Transactions");
		// append root element to document
		doc.appendChild(rootElement);

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		String localDate = LocalDate.now().format(dtf);

		String format2 = new DecimalFormat("#,###.00").format(req.getAmount());

		// append first child element to root element
		rootElement.appendChild(getTransactionsData(doc, localDate, req.getTransID(), req.getRefKey(),
				req.getNarration(), String.valueOf(format2), String.valueOf(300000.00)));

		// for output to file, console
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		// for pretty print
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		DOMSource source = new DOMSource(doc);

		// write to console or file
		StreamResult console = new StreamResult(System.out);
		StreamResult file = new StreamResult(
				new File("D:\\MPO\\POC\\WalletWebUI\\WalletWebUI\\WebContent\\Transactions.xml"));

		// write data
		transformer.transform(source, console);
		transformer.transform(source, file);
		System.out.println("DONE");
	}

}
