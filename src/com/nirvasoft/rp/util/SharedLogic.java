package com.nirvasoft.rp.util;

import java.util.ArrayList;

import com.nirvasoft.fcpayment.core.fcchannel.Object.SharedLogic.Account;
import com.nirvasoft.rp.framework.Ref;
import com.nirvasoft.rp.shared.AccountGLTransactionData;
import com.nirvasoft.rp.shared.DataResult;
import com.nirvasoft.rp.shared.ProductData;
import com.nirvasoft.rp.shared.ProductSetupData;
import com.nirvasoft.rp.shared.SystemData;
import com.nirvasoft.rp.shared.TransactionData;
import com.nirvasoft.rp.shared.icbs.AccountData;
import com.nirvasoft.rp.shared.icbs.Enum.TransCode;
import com.nirvasoft.rp.shared.icbs.ProductFeatureType;

public class SharedLogic {
	private static SystemData pSystemData;	
	
	public static int getSystemSettingT12N1(String pT1) {
		int ret = 255;
		for (int i = 0; i < pSystemData.getSystemSettingDatas().size(); i++) {
			if (pSystemData.getSystemSettingDatas().get(i).getT1().equals(pT1)) {
				ret = pSystemData.getSystemSettingDatas().get(i).getN1();
				break;
			}
		}
		return ret;
	}
	public static double getAccountMinBalance(String productCode) {
		double ret = 0;
		for (int i = 0; i < pSystemData.getProducts().size(); i++) {
			if (pSystemData.getProducts().get(i).getProductCode().equals(productCode)) {
				ret = pSystemData.getProducts().get(i).getMinBalance();
				break;
			}
		}
		return ret;
	}
	public static boolean isFD(String productCode) {
		boolean ret = false;
		int l_ProdFeatureType = getProductFeature(productCode);
		if (l_ProdFeatureType == ProductFeatureType.FD) {
			ret = true;
		}
		return ret;
	}

	public static boolean isGLAccount(String a) {
		boolean ret = false;
		if (a.length() == 16 && !SharedUtil.isNumber(a.substring(0, 1)) && SharedUtil.isNumber(a.substring(1, 16)))
			ret = true;
		return ret;
	}

	public static boolean isLoan(String productCode) {
		boolean ret = false;
		int l_ProdFeatureType = getProductFeature(productCode);
		if (l_ProdFeatureType == ProductFeatureType.LA) {
			ret = true;
		}
		return ret;
	}
	public static int getProductFeature(String productCode) {
		int ret = 0;
		for (int i = 0; i < pSystemData.getProducts().size(); i++) {
			if (pSystemData.getProducts().get(i).getProductCode().equals(productCode)) {
				ret = (int) pSystemData.getProducts().get(i).getProductFeatureType();
			}
		}
		return ret;
	}

}	