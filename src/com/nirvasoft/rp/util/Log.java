package com.nirvasoft.rp.util;

import java.util.ArrayList;

import com.nirvasoft.rp.shared.DepositAccDataResult;
import com.nirvasoft.rp.shared.ResponseData;

public class Log {
	public void accountSummaryLog(DepositAccDataResult response, String userID) {

		// Response log
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Account Summary Response Message : " + userID);
			l_err.add(response.toString());
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
		}
	}

	public void changePwdLog(ResponseData response) {

		// Response log
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Change Password Response Message : ");
			l_err.add(response.toString());
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
		}
	}
}
