package com.nirvasoft.rp.util;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "RESPONSE")
@XmlAccessorType(XmlAccessType.FIELD)
public class Response {

	@XmlElement(name = "ID", required = true)
	private String id;

	@XmlElement(name = "ResponseCode", required = true)
	private String responsecode;

	@XmlElement(name = "ResponseDesc")
	private String responsedesc;

	@XmlElement(name = "TransactionID")
	private String transactionid;

	@XmlElement(name = "TransactionDateTime")
	private String transactiondatetime;

	public Response() {
		clearProperty();
	}

	void clearProperty() {
		id = "";
		responsecode = "";
		responsedesc = "";
		transactionid = "";
		transactiondatetime = "";
	}

	public String getId() {
		return id;
	}

	public String getResponsecode() {
		return responsecode;
	}

	public String getResponsedesc() {
		return responsedesc;
	}

	public String getTransactiondatetime() {
		return transactiondatetime;
	}

	public String getTransactionid() {
		return transactionid;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setResponsecode(String responsecode) {
		this.responsecode = responsecode;
	}

	public void setResponsedesc(String responsedesc) {
		this.responsedesc = responsedesc;
	}

	public void setTransactiondatetime(String transactiondatetime) {
		this.transactiondatetime = transactiondatetime;
	}

	public void setTransactionid(String transactionid) {
		this.transactionid = transactionid;
	}

}
