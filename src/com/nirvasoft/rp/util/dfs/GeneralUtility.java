package com.nirvasoft.rp.util.dfs;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.nirvasoft.rp.shared.TransactionData;

public class GeneralUtility {
	private static String mVersionNo = "1.1.5";//For Version
	
	public static String getVersionNo() {
		return mVersionNo;
	}
	public static void setVersionNo(String mVersionNo) {
		GeneralUtility.mVersionNo = mVersionNo;
	}
	
	public static String getTodayDate(){
		DateFormat l_DateFormat = new SimpleDateFormat("yyyyMMdd");
		Calendar l_Calendar = Calendar.getInstance();
		String l_TodayDate = l_DateFormat.format(l_Calendar.getTime());
		return l_TodayDate;
	}
	public static String getTodayDateTime(){
		DateFormat l_DateFormat = new SimpleDateFormat("yyyyMMdd");
		Calendar l_Calendar = Calendar.getInstance();
		String l_TodayDate = l_DateFormat.format(l_Calendar.getTime());
		return l_TodayDate;
	}
	
	public static void writeLog(ArrayList<String> arl, String l_TodayDate){
		//String l_FileName = "ReferenceWSLog"+l_TodayDate+".txt";
		//String l_FilePath = com.nirvasoft.rp.dao.SingletonServer.getAbsPath() +"ReferenceWS/Log/";		
		//File dir = new File(l_FilePath);
		//dir.mkdirs();		
		//FileUtil.writeList(l_FilePath + l_FileName, arl, true);
	}
	public static String chngeyyyyMMddtoyyyyMMMdd(String pdate){
		String formatted = "";
		//from 2017-12-06 to 2017-Dec-06
		 try {
			 Date parsed = new SimpleDateFormat("yyyy-MM-dd").parse(pdate);
			 formatted = new SimpleDateFormat("yyyy-MMM-dd").format(parsed);
			 System.out.println(formatted);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formatted;
	}
	
	public static String chngeyyyyMMddtoddMMMyyyy(String pdate){
		String formatted = "";
		//from 2017-12-06 to 06-Dec-2017
		 try {
			 Date parsed = new SimpleDateFormat("yyyy-MM-dd").parse(pdate);
			 formatted = new SimpleDateFormat("dd-MMM-yyyy").format(parsed);
			 System.out.println(formatted);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formatted;
	}
	
	public static String chngeyyyyMMddtoddMMyyyy(String pdate){
		String formatted = "";
		//from 2017-12-06 to 2017-Dec-06
		 try {
			 if(!pdate.equals("")){	//nlkm 23032018
				 Date parsed = new SimpleDateFormat("yyyy-MM-dd").parse(pdate);
				 formatted = new SimpleDateFormat("dd-MMM-yyyy").format(parsed);
			 }
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formatted;
	}
	public static String chngeyyyyMMddtoddMMyyyy3(String pdate){
		String formatted = "";
		//from 06.12.2017  to 2017-Dec-06
		 try {
			 Date parsed = new SimpleDateFormat("dd.MM.yyyy").parse(pdate);
			 formatted = new SimpleDateFormat("dd-MMM-yyyy").format(parsed);
			 System.out.println(formatted);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formatted;
	}
	public static String chngeyyyyMMddtoddMMyyyy2(String pdate){
		String formatted = "";
		//from 20171206 to 06/Dec/2017
		 try {
			 Date parsed = new SimpleDateFormat("yyyyMMdd").parse(pdate);
			 formatted = new SimpleDateFormat("dd/MMM/yyyy").format(parsed);
			 System.out.println(formatted);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formatted;
	}
	//tts
	public static String chngeyyyyMMddtoddMMyyyy9(String pdate){
		String formatted = "";
		//from 20171206 to 06/Dec/2017
		 try {
			 Date parsed = new SimpleDateFormat("yyyyMMdd").parse(pdate);
			 formatted = new SimpleDateFormat("dd MMMM yyyy").format(parsed);
			 System.out.println(formatted);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formatted;
	}

	public static String chngeyyyyMMddtoddMMyyyy5(String pdate){
        String formatted = "";
        //from 20171206 to 06-Dec-2017
         try {
             Date parsed = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(pdate);
             formatted = new SimpleDateFormat("dd-MMM-yyyy").format(parsed);
             System.out.println(formatted);
            
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return formatted;
    }
	public static String chngeyyyyMMddtoddMMyyyy6(String pdate){
		String formatted = "";
		//from 06.12.2017  to 2017-Dec-06
		 try {
			 Date parsed = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss").parse(pdate);
			 formatted = new SimpleDateFormat("dd-MMM-yyyy").format(parsed);
			 System.out.println(formatted);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formatted;
	}
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}
	public static double roundTwoDecimals(double d) {
	    DecimalFormat twoDForm = new DecimalFormat("#");
	    return Double.valueOf(twoDForm.format(d));
	}
	public static void main (String[] args){
	   //System.out.println(roundTwoDecimals(19.53125));
		
		 System.out.println(subDate("2018-03-01", 1));
	}

	public static String subDate(String sdate,int day){
		Date l_date = new Date();
		String retDate = "";
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			l_date = df.parse(sdate);
			Calendar cal = GregorianCalendar.getInstance();
			cal.setTime(l_date);
			cal.add(GregorianCalendar.DATE, -(day));
			retDate = df.format(cal.getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return retDate;		
	}
	public static String chngeClientDatetoyyyyMMdd(String pdate){
		String formatted = "";
		//from 2017-12-06 to 2017-Dec-06
		 try {
			 Date parsed = new SimpleDateFormat("yyyy-MM-dd").parse(pdate);
			 formatted = new SimpleDateFormat("yyyyMMdd").format(parsed);
			 System.out.println(formatted);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formatted;
	}
		public static int comapreDate(String date1,String date2){
		//
		Date d1 = new Date();
		Date d2 = new Date();
		int ret=0;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			d1 = sdf.parse(date1);
			d2 = sdf.parse(date2);		
			
			ret = d1.compareTo(d2);
			//ret     >0	 =>Date1 is after Date2
			//ret     <0 	 =>Date1 is before Date2
			//ret     =0 	 =>Date1 is before Date2
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ret;
		
		}
	public static int converMonth(String str){
		int i=0;
		if(str.equalsIgnoreCase("JAN")){
			i =1;
		}
		else if(str.equalsIgnoreCase("FEB")){
			i =2;
		}
		else if(str.equalsIgnoreCase("MAR")){
			i=3;
		}
		else if(str.equalsIgnoreCase("APR")){
			i=4;
		}
		else if(str.equalsIgnoreCase("MAY")){
			i=5;
		}
		else if(str.equalsIgnoreCase("JUN")){
			i=6;
		}
		else if(str.equalsIgnoreCase("JUL")){
			i=7;
		}
		else if(str.equalsIgnoreCase("AUG")){
			i=8;
		}
		else if(str.equalsIgnoreCase("SEP")){
			i = 9;
		}
		else if(str.equalsIgnoreCase("OCT")){
			i=10;
		}
		else if(str.equalsIgnoreCase("NOV")){
			i=11;
		}
		else if(str.equalsIgnoreCase("DEC")){
			i = 12;
		}
		return i;
		
	}
	
	public static String converMonthForExcel(String str){
		String month="";		
		if(str.equalsIgnoreCase("JAN")){
			month = "January";
		}
		else if(str.equalsIgnoreCase("FEB")){
			month = "February";
		}
		else if(str.equalsIgnoreCase("MAR")){
			month = "March";
		}
		else if(str.equalsIgnoreCase("APR")){
			month = "April";
		}
		else if(str.equalsIgnoreCase("MAY")){
			month = "May";
		}
		else if(str.equalsIgnoreCase("JUN")){
			month = "June";
		}
		else if(str.equalsIgnoreCase("JUL")){
			month = "July";
		}
		else if(str.equalsIgnoreCase("AUG")){
			month = "August";
		}
		else if(str.equalsIgnoreCase("SEP")){
			month = "September";
		}
		else if(str.equalsIgnoreCase("OCT")){
			month = "October";
		}
		else if(str.equalsIgnoreCase("NOV")){
			month = "November";
		}
		else if(str.equalsIgnoreCase("DEC")){
			month = "December";
		}
		 
		 System.out.println("Converting month format:::"+month);
		return month;
		
	}
	
	public static String chngeyyyyMMddtoddMMyyyy4(String pdate){
		String formatted = "";
		//from 06.12.2017  to 2017-Dec-06
		 try {
			 Date parsed = new SimpleDateFormat("yyyy-MM-dd").parse(pdate);
			 formatted = new SimpleDateFormat("dd.MM.yyyy").format(parsed);
			 System.out.println(formatted);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formatted;
	}
	public static String changeddMMyyyytoyyyyMMdd(String pdate){
		String formatted = "";
		//from 06.12.2017  to 2017-Dec-06
		 try {
			 Date parsed = new SimpleDateFormat("dd/MM/yyyy").parse(pdate);
			 formatted = new SimpleDateFormat("yyyy-MM-dd").format(parsed);
			 System.out.println(formatted);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formatted;
	}
	public  String getPreviousMonthEndDate(String aDate){
		// aDate == 20111001
		// ret == 30/09/2011
		//int day = Integer.valueOf(aDate.substring(6, 8));
		int day = 01;
		int month = Integer.valueOf(aDate.substring(4, 6));
		int year = Integer.valueOf(aDate.substring(0, 4));
				
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		Calendar c = Calendar.getInstance();
		c.set(year, month - 2 , day);		
		String ret = getMonthEnd(dateFormat.format(c.getTime()));
		return ret;
		
	}
	 public static String getMonthEnd(String aDate){
			// aDate == 31/12/2011
		  	// Edit By WHA ==> For FEB Month End Case (31/02/2012) instead of (29/02/2011)

			String[] l_date = aDate.split("/");
			int day = Integer.parseInt(l_date[0]);
			int month = Integer.parseInt(l_date[1]);
			int year = Integer.parseInt(l_date[2]);
			day = 1; // Cause We Only Need Month End
			
			month --; // In Java calendar month is starting from zero.
			Calendar calendar = Calendar.getInstance();	
			calendar.set(year, month, day);
				
			int lastDate = calendar.getActualMaximum(Calendar.DATE);
			calendar.set(year, month, lastDate);
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			String ret = df.format(calendar.getTime());
			return ret;
	}
		public String getStartDay(String aDate){
			
			String day = aDate.substring(6, 8);
			String month = aDate.substring(4, 6);
			String year = aDate.substring(0, 4);
			String ret = "";
			
			if(!day.equals("01")){
				ret = "01/" + month + "/" + year;
			}else{
				ret = day + "/" + month + "/" + year ;
			}
			
			return ret;
		}
		public String formatYYYYMMDD2DDMMYYYY(String p) {
	    	String ret="";
	    	try{
	    		ret = p.substring(6, 8) + "/" + p.substring(4, 6) + "/" + p.substring(0, 4) ;
	    	}catch (Exception exp){}
	    	return ret;
	    }
		public static String formatInQuery(String p) {
			String[] l_DataList =  p.split(",");
			String l_Data = "";
			if(l_DataList.length > 1){
				for(int i=0; i<l_DataList.length;i++){
					l_DataList[i] = "'"+ l_DataList[i] +"'";
				}
			}else{
				l_DataList[0] = "'" + l_DataList[0] + "'";
			}
			
			for(int i=0; i<l_DataList.length; i++){
				if(i != l_DataList.length-1){
					if(l_Data.equals("")){
						l_Data = l_DataList[i] + "," ;
					}else{
						l_Data = l_Data + l_DataList[i] + "," ;
					}				
				}else{
					l_Data = l_Data + l_DataList[i];			
				}
			}
			return l_Data;
		}
		public String GetAccHeads(ArrayList<TransactionData> aList){
			String l_AccHeadsList = "";
			String l_AccHead = "";
			
			int l_LastIndex = aList.size() - 1 ;
			for (int i = 0; i < aList.size(); i++) {
				l_AccHead = "'" + aList.get(i).getAccountNumber() + "'" ;
				l_AccHeadsList += (i == l_LastIndex) ? l_AccHead: l_AccHead + ",";
			}
			return l_AccHeadsList;
		}
		public static String chngeyyyyMMddtoddMMyyyy7(String pdate){
			String formatted = "";
			//from 20171206 to 06-Dec-2017
			 try {
				 Date parsed = new SimpleDateFormat("yyyyMMdd").parse(pdate);
				 formatted = new SimpleDateFormat("dd.MM.yyyy").format(parsed);
				 System.out.println(formatted);
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return formatted;
		}
		
		// nlkm 
		public static String chngeyyyyMMddtoddMMyyyy8(String pdate){
			String formatted = "";
			//from 06/12/2017  to 2017-12-06
			 try {
				 Date parsed = new SimpleDateFormat("dd/MM/yyyy").parse(pdate);
				 formatted = new SimpleDateFormat("yyyy-MM-dd").format(parsed);
				 System.out.println(formatted);
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return formatted;
		}
		
		public static String chngeddMMyyyytoyyyyMMdd8(String pdate){
			String formatted = "";
			//from 06-12-2017  to 2017-12-06
			 try {
				 Date parsed = new SimpleDateFormat("dd-MM-yyyy").parse(pdate);
				 formatted = new SimpleDateFormat("yyyy-MM-dd").format(parsed);
				 System.out.println(formatted);
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return formatted;
		}
		
		public static String chngeyyyyMMddtoddMMyyyy10(String pdate){
			String formatted = "";
			//from 2017-12-06 0to 06-12-2017
			 try {
				 Date parsed = new SimpleDateFormat("yyyy-MM-dd").parse(pdate);
				 formatted = new SimpleDateFormat("dd-MM-yyyy").format(parsed);
				 System.out.println(formatted);
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return formatted;
		}
		
		public static String getCurrentDateYYYYMMDDHHMMSS(){
		    String pattern = "yyyy-MM-dd HH:mm:ss";
		    SimpleDateFormat format = new SimpleDateFormat(pattern);
		    return format.format(new Date());
		}
		//mmm
	public static String commaTrim(String p){
		if(p.startsWith(";;")){
			p=p.replaceFirst(";;", "");
		}
		double retDouble = Double.MIN_VALUE;
		String ret = "0";
		String l_Str = "";
		
		try {
			l_Str = p.replace(",", "");
			if (l_Str.equals(""))
				retDouble = 0;
			else retDouble = Double.parseDouble(l_Str);
			ret = String.valueOf(retDouble);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return ret;
	}
	public static double rate(double nper, double pmt, double pv){
	    //System.out.println("function rate : " + nper + " " + pmt + " pv " + pv);

	    double error = 0.0000001;
	    double high =  1.00;
	    double low = 0.00;

	    double rate = (2.0 * (nper * pmt - pv)) / (pv * nper);

	    while(true) {
	        // Check for error margin
	        double calc = Math.pow(1 + rate, nper);
	        calc = (rate * calc) / (calc - 1.0);
	        calc -= pmt / pv;

	        if (calc > error) {
	            // Guess is too high, lower the guess
	            high = rate;
	            rate = (high + low) / 2;
	        } 
	        else if (calc < -error) {
	            // Guess is too low, higher the guess.
	            low = rate;
	            rate = (high + low) / 2;
	        } 
	        else {
	            // Acceptable guess
	            break;
	        }
	    }

	    //System.out.println("Rate : " + rate);
	    return rate;
	}
	  public static String addMonthMRHF(String pDate, int months) {
	    	String ret = "";
	
		int year = Integer.parseInt(pDate.substring(0, 4));
		int month = Integer.parseInt(pDate.substring(4, 6));
		int day = Integer.parseInt(pDate.substring(6, 8));
		
		month --; 	// month start from 0
		month = month + months;
		if(day>15)
			month+=1;
		day=1;
	
		Calendar cal = Calendar.getInstance();
		cal.set(year, month, day); 
	
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		ret = dateFormat.format(cal.getTime());
	
		return ret;
	}
	public static String getCurrentDate(){
	    String pattern = "dd/MM/yyyy";
	    SimpleDateFormat format = new SimpleDateFormat(pattern);
	    return format.format(new Date());
	}
	
	public static Date convertStringToDate(String dateInString) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
        Date date = new Date();
        try {
            date = formatter.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
	
	public static Date convertStringToDate1(String dateInString) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yy");
        Date date = new Date();
        try {
            date = formatter.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
	
	public static Date convertStringToDate2(String dateInString) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        try {
            date = formatter.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
	
}
