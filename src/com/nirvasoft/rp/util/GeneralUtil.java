
package com.nirvasoft.rp.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.nirvasoft.fcpayment.core.data.fcchannel.CMSModuleConfigData;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.framework.ServerSession;
import com.nirvasoft.rp.mgr.BranchMgr;
import com.nirvasoft.rp.shared.Constant;
import com.nirvasoft.rp.shared.DepositAccData;

import password.DESedeEncryption;
enum ImagePath
{	
	ProuctImagePath,
	OutletImagePath,
	ProductLoanImagePath,
	StaffLoanImagePath,
	HirePurchaseImagePath,
	EducationLoanImagePath,
	CommercialLoanImagePath,
	OverdraftLoanImagePath,
	JICATwoStepLoanImagePath,
	CGILoanImagePath,
	CFBLoanImagePath,
	StaffLoanPDFDownloadPath,
	
}
enum OrgType{
	MRHFOrg,
	SMIDBOrg
}
public class GeneralUtil {
	private static String apppath = "";
	private static  String url = "", initVector = "", key = "", pUrl = "", urlPaidUnPaid = "",urlTrans="",urlTimeout1="",urlTimeout2="";

	// wcs
	public static String addMonth(String date1, int i) {
		String[] date = date1.split("-");
		System.out.println("Result 1 : " + date);

		int day = Integer.parseInt(date[2]);
		System.out.println("Result day: " + day);

		int month = Integer.parseInt(date[1]);
		System.out.println("Result month : " + month);

		int year = Integer.parseInt(date[0]);
		System.out.println("Result year : " + year);

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		Calendar c = Calendar.getInstance();
		c.set(year, month - 1, day);
		System.out.println("set date is " + dateFormat.format(c.getTime()));
		c.add(Calendar.MONTH, i);
		System.out.println("add date " + dateFormat.format(c.getTime()));
		return dateFormat.format(c.getTime());
	}

	public static String changedateformat(String pdate) {
		String l_date = "";
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		try {
			pdate = pdate.replace('/', '-');
			Date varDate = dateFormat.parse(pdate);
			dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			l_date = dateFormat.format(varDate);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return l_date;
	}

	public static String changeDateFormat(String dateAndTime) {
		String date = dateAndTime.substring(0, dateAndTime.indexOf(" "));
		SimpleDateFormat fm = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String date_string = "";
		try {
			date_string = fm.format(formatter.parse(date));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date_string;
	}

	public static String changeTimeFormat(String dateAndTime) {
		String time = dateAndTime.substring(dateAndTime.indexOf(" "));
		SimpleDateFormat fm = new SimpleDateFormat("HH:mm a", Locale.ENGLISH);
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
		String date_string = "";
		try {
			date_string = fm.format(formatter.parse(time));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date_string;
	}

	public static int compareDate(String date1, String date2) {
		//
		Date d1 = new Date();// expdate
		Date d2 = new Date();// todaydate
		int ret = 0;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			d1 = sdf.parse(date1);
			d2 = sdf.parse(date2);

			ret = d1.compareTo(d2);
			// ret >0 =>Date1 is after Date2
			// ret <0 =>Date1 is before Date2
			// ret =0 =>Date1 is before Date2

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ret;

	}

	public static String datetoddMMMyyyywithSlach(String aDate) {
		String l_date = "";
		java.util.Date l_Date = new java.util.Date();
		SimpleDateFormat l_sdf = new SimpleDateFormat("dd/MMM/yyyy");
		SimpleDateFormat l_sdf2 = new SimpleDateFormat("dd-MM-yyyy");
		try {
			l_Date = l_sdf.parse(aDate);
			l_date = l_sdf2.format(l_Date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return l_date;
	}

	public static String datetoString() {
		String l_date = "";
		java.util.Date l_Date = new java.util.Date();
		SimpleDateFormat l_sdf = new SimpleDateFormat("yyyyMMdd");
		l_date = l_sdf.format(l_Date);

		return l_date;
	}

	public static String datetoStringFlexcubeFormat() {
		String l_date = "";
		java.util.Date l_Date = new java.util.Date();
		SimpleDateFormat l_sdf = new SimpleDateFormat("yyyy-MM-dd");
		l_date = l_sdf.format(l_Date);

		return l_date;
	}

	@SuppressWarnings("deprecation")
	public static String datetoStringNew() {
		String l_date = "";
		java.util.Date l_Date = new java.util.Date();
		l_date = getStartZero(4, String.valueOf(l_Date.getYear() + 1900))
				+ getStartZero(2, String.valueOf(l_Date.getMonth() + 1))
				+ getStartZero(2, String.valueOf(l_Date.getDate()));

		return l_date;
	}

	public static int doCheckDigit(String s, boolean evenPosition) {
		int sum = 0;
		for (int i = s.length() - 1; i >= 0; i--) {
			int n = Integer.parseInt(s.substring(i, i + 1));
			if (evenPosition) {
				n *= 2;
				if (n > 9) {
					n = (n % 10) + 1;
				}
			}
			sum += n;
			evenPosition = !evenPosition;
		}
		return sum;
	}

	public static String formatddMMYYYY2YYYYMMdd(String p) {
		String ret = "";
		String dd = "";
		String mm = "";
		try {
			int d = Integer.parseInt(p.substring(0, 2));

			int m = Integer.parseInt(p.substring(3, 5));

			int y = Integer.parseInt(p.substring(6, 10));

			if (d < 10) {
				dd = "0" + d;
			} else {
				dd = p.substring(0, 2);
			}
			if (m < 10) {
				mm = "0" + m;
			} else {
				mm = p.substring(3, 5);
			}
			ret = y + "" + mm + "" + dd;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return ret;
	}

	public static String formatddMMYYYY2YYYYMMDD(String p) {
		String ret = "";
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		SimpleDateFormat f2 = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
		Date d;
		try {
			d = f.parse(p);
			ret = f2.format(d);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return ret;
	}

	public static String formatNumber(double pAmount) {
		String l_result = "0.00";
		DecimalFormat l_df = new DecimalFormat("#,###.00");
		l_result = l_df.format(pAmount);
		return l_result;
	}

	public static String getAppName(String appCode) {
		String appName = "";
		if (appCode.equals(Constant.appCode1)) {
			appName = Constant.appName1;
		} else if (appCode.equals(Constant.appCode2)) {
			appName = Constant.appName2;
		}
		return appName;
	}

	public static String getCheckDigit(String s) {
		int digit = 10 - doCheckDigit(s, true) % 10;
		String ret = "" + digit;
		return ret.substring(ret.length() - 1, ret.length());
	}

	public static String getCheckDigitAdded(String accountnumber) {
		return accountnumber + getCheckDigit(accountnumber);
	}

	public static String getDate() {
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dat = sdf.format(d);
		return dat;
	}

	public static String getDateTime() {
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
		String dat = sdf.format(d);
		return dat;
	}

	public static String getDateYYYYMMDD() {
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dat = sdf.format(d);
		return dat;
	}

	public static String getDateYYYYMMDDHHMMSS() {
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dat = sdf.format(d);
		return dat;
	}

	public static String getDateYYYYMMDDHHMMSSSLASH() {
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String dat = sdf.format(d);
		return dat;
	}

	public static String getDateYYYYMMDDSimple() {
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String dat = sdf.format(d);
		return dat;
	}

	public static String getStartZero(int aZeroCount, String aValue) {
		while (aValue.length() < aZeroCount) {
			aValue = "0" + aValue;
		}
		return aValue;
	}

	public static String getTime() {
		String l_Time = "";
		SimpleDateFormat l_DateFormat = new SimpleDateFormat("HH:mm:ss");
		Calendar l_Canlendar = Calendar.getInstance();
		l_Time = l_DateFormat.format(l_Canlendar.getTime());

		return l_Time;
	}

	public static boolean isWeekEnd(String pDate) { // Date Format yyyymmdd
		boolean ret = false;
		int year = Integer.parseInt(pDate.substring(0, 4));
		int month = Integer.parseInt(pDate.substring(4, 6));
		int day = Integer.parseInt(pDate.substring(6, 8));
		Calendar cal = Calendar.getInstance();
		cal.set(year, month - 1, day); // month start from 0
		if (cal.get(Calendar.DAY_OF_WEEK) == 1 || cal.get(Calendar.DAY_OF_WEEK) == 7) {
			ret = true;
		}
		return ret;
	}

	public static String leadZeros(long p, int size) {
		return leadZeros("" + p, size);
	}

	public static String leadZeros(String p, int size) {
		String ret = p;
		for (int i = p.length(); i < size; i++) {
			ret = "0" + ret;
		}
		return ret;
	}

	public static String mobiledateformat(String pdate) {
		// yyyymmdd to dd-mm-yyyy
		String l_date = "";
		l_date = pdate.substring(6, 8) + '-' + pdate.substring(4, 6) + '-' + pdate.substring(0, 4);

		return l_date;
	}

	public static String oracledatetoString() {
		String l_date = "";
		java.util.Date l_Date = new java.util.Date();
		SimpleDateFormat l_sdf = new SimpleDateFormat("yyyy-MM-dd");
		l_date = l_sdf.format(l_Date);

		return l_date;
	}

	public static String oraclelastday(int lastday) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		Date date = new Date();
		String todate = dateFormat.format(date);

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -lastday); // 2 or 5
		Date todate1 = cal.getTime();
		String fromdate = dateFormat.format(todate1);

		System.out.println("Date Format :" + fromdate);

		return fromdate;
	}

	public static String readAdImage() {
		String ret = "";
		try {
			apppath = DAOManager.AbsolutePath + "WEB-INF\\data";
			ret = apppath + "\\ad\\image\\";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	public static ArrayList<String> readAdLink() {
		ArrayList<String> l_resultList = null;
		try {
			String path = ServerSession.serverPath + "WEB-INF//reference";
			l_resultList = FileUtil.readList(path + "\\link.txt");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return l_resultList;
	}

	public static void readCCDAPIServiceSetting() {
	
		ArrayList<String> ccdConnList = new ArrayList<String>();
		try {
			String apppath = System.getenv("CMSService");
			ccdConnList = FileUtil.readList(DAOManager.AbsolutePath + "WEB-INF/data/CcdApiConncetionConfig.txt");
			if (ccdConnList.size() > 0) {
				int i = 0;
				url = ccdConnList.get(0).split("GetURL:")[1];
				urlTrans = ccdConnList.get(1).split("GetDailyURL:")[1];							
				key = ccdConnList.get(2).split("KEY:")[1];
				initVector = ccdConnList.get(3).split("InitVector:")[1];
				urlTimeout1 = ccdConnList.get(11).split("ConnectTimeout_Sync:")[1];
				urlTimeout2 = ccdConnList.get(12).split("ReadTimeout_Sync:")[1];
				pUrl = ccdConnList.get(13).split("GetPenaltyURL:")[1];
				urlPaidUnPaid = ccdConnList.get(14).split("CheckPaidOrUnPaid:")[1];
			}
			ServerGlobal.setmGetCCDURL(url.trim());
			ServerGlobal.setmGetCCDURLTrans(urlTrans.trim());
			ServerGlobal.setmGetCCDKEY(key.trim());
			ServerGlobal.setmGetCCDINITVECTOR(initVector.trim());
			ServerGlobal.setmGetCCDConnectTimeout(urlTimeout1.trim());
			ServerGlobal.setmGetCCDReadTimeout(urlTimeout2.trim());
			ServerGlobal.setmGetCCDPURL(pUrl.trim());
			ServerGlobal.setmGetCCDPaidUnPaidURL(urlPaidUnPaid.trim());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void readCNPAPIServiceSetting() {
		String url1 = "", url2 = "", url3 = "", url4 = "", apiKey = "", duetime = "", dueday = "", bankRate = "";
		ArrayList<String> cnpConnList = new ArrayList<String>();
		try {
			String apppath = System.getenv("CMSService");
			cnpConnList = FileUtil.readList(apppath + "\\CNP\\CnpApiConncetionConfig.txt");
			if (cnpConnList.size() > 0) {
				int i = 0;
				url1 = cnpConnList.get(i++).split("GetBillURL:")[1];
				url2 = cnpConnList.get(i++).split("MakePaymentURL:")[1];
				url3 = cnpConnList.get(i++).split("GetTxnURL:")[1];
				url4 = cnpConnList.get(i++).split("TopupURL:")[1]; // remove for
																	// uat
				apiKey = cnpConnList.get(i++).split("ApiKey:")[1];
				dueday = cnpConnList.get(i++).split("DueDay:")[1];
				duetime = cnpConnList.get(11).split("StartHour:")[1]; // 15
				bankRate = cnpConnList.get(13).split("BankRate:")[1]; // 15
			}
			ServerGlobal.setmGetBillURL(url1.trim());
			ServerGlobal.setmMakePaymentURL(url2.trim());
			ServerGlobal.setmGetTxnURL(url3.trim());
			ServerGlobal.setmTopupURL(url4.trim());
			ServerGlobal.setmAPIKey(apiKey.trim());
			ServerGlobal.setDueDay(dueday.trim());
			ServerGlobal.setDueTime(duetime.trim());
			ServerGlobal.setBankRate(bankRate.trim());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void readCNPDebugLogStatus() {
		try {
			String apppath = System.getenv("CMSService");
			ArrayList<String> l_resultList = FileUtil.readList(apppath + "\\CNP\\debugconfig\\Log.txt");
			if (l_resultList != null && l_resultList.size() > 0) {
				if (l_resultList.get(0).split(":")[1].trim().equals("Yes")) {
					ServerGlobal.setWriteLog(true);
				} else {
					ServerGlobal.setWriteLog(false);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public static void readDataConfig() {

		ArrayList<String> l_resultList = null;
		try {
			l_resultList = FileUtil.readList(DAOManager.AbsolutePath + "/data/DataConfig.txt");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (String string : l_resultList) {
			if (string.startsWith("FortuneYgnMerchant")) {
				ServerGlobal.setFortuneYgnMerchant(string.split(ServerGlobal.getSeparator())[1]);
			}
			if (string.startsWith("FortuneMdyMerchant")) {
				ServerGlobal.setFortuneMdyMerchant(string.split(ServerGlobal.getSeparator())[1]);
			}

		}

	}

		
	// suwai
	public static void readDebugLogStatus() {
		try {

			apppath = DAOManager.AbsolutePath + "WEB-INF\\data";
			System.out.println("readDebugLogStatus path"+apppath);
			ArrayList<String> l_resultList = FileUtil.readList(apppath + "\\Log.txt");
			

			if (l_resultList != null && l_resultList.size() > 0) {
				System.out.println(l_resultList.get(0).split(":")[1]);
				if (l_resultList.get(0).split(":")[1].trim().equals("Yes")) {
					ServerGlobal.setWriteLog(true);
				} else {
					ServerGlobal.setWriteLog(false);
				}
			}
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	
	

	public static void readEPIXWSDebugLogStatus() {
		try {

			apppath = System.getenv("CMSService");
			ArrayList<String> l_resultList = FileUtil.readList(apppath + "\\EPIXWS\\debugconfig\\Log.txt");

			if (l_resultList != null && l_resultList.size() > 0) {
				System.out.println(l_resultList.get(0).split(":")[1]);
				if (l_resultList.get(0).split(":")[1].trim().equals("Yes")) {
					ServerGlobal.setWriteLog(true);
				} else {
					ServerGlobal.setWriteLog(false);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public static void readFCACServiceSetting() {

		String mPath = DAOManager.AbsolutePath + "WEB-INF/data";
		ArrayList<String> l_resultList = null;
		try {
			l_resultList = FileUtil.readList(mPath + "/data/ServiceSetting.txt");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Get Service Path URL
		for (String string : l_resultList) {
			if (string.startsWith("AC__")) {
				String url = string.split(ServerGlobal.getFCSeparator())[1];
				ServerGlobal.setmFCACServiceURL(url);
			}
		}
	}

	public static void readFCCUServiceSetting() {

		String mPath = DAOManager.AbsolutePath + "WEB-INF/data";
		ArrayList<String> l_resultList = null;
		try {
			l_resultList = FileUtil.readList(mPath + "/data/ServiceSetting.txt");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Get Service Path URL
		for (String string : l_resultList) {
			if (string.startsWith("CU")) {
				String url = string.split(ServerGlobal.getFCSeparator())[1];
				ServerGlobal.setmFCCUServiceURL(url);
			}
		}
	}

	public static void readFCServiceSetting() {
		String mPath = DAOManager.AbsolutePath + "WEB-INF/data";
		ArrayList<String> l_resultList = null;
		try {
			l_resultList = FileUtil.readList(mPath + "/data/ServiceSetting.txt");
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Get Service Path URL
		for (String string : l_resultList) {
			if (string.startsWith("RT")) {
				String url = string.split(ServerGlobal.getFCSeparator())[1];
				String makerID = string.split(ServerGlobal.getFCSeparator())[2];
				String checkerID = string.split(ServerGlobal.getFCSeparator())[3];
				ServerGlobal.setFCRTServiceURL(url);
				ServerGlobal.setFCMakerID(makerID);
				ServerGlobal.setFCCheckerID(checkerID);
			}
			if (string.substring(0, 2).equalsIgnoreCase("AC")) {
				String url = string.split(ServerGlobal.getFCSeparator())[1];
				String makerID = string.split(ServerGlobal.getFCSeparator())[2];
				String checkerID = string.split(ServerGlobal.getFCSeparator())[3];
				ServerGlobal.setmFCACServiceURL(url);
				ServerGlobal.setFCMakerID(makerID);
				ServerGlobal.setFCCheckerID(checkerID);
			}
			if (string.startsWith("MPO")) {
				String url = string.split(ServerGlobal.getFCSeparator())[1];
				String makerID = string.split(ServerGlobal.getFCSeparator())[2];
				String checkerID = string.split(ServerGlobal.getFCSeparator())[3];
				ServerGlobal.setMPOServiceURL(url);
				ServerGlobal.setFCMakerID(makerID);
				ServerGlobal.setFCCheckerID(checkerID);
			}
		}
	}

	public static void readMBServiceSetting() {

		ArrayList<String> l_resultList = null;
		try {
			l_resultList = FileUtil.readList(DAOManager.AbsolutePath + "WEB-INF/data/ServiceSetting.txt");
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Get Service Path URL
		for (String string : l_resultList) {
			if (string.startsWith("MB")) {
				String url = string.split(ServerGlobal.getFCSeparator())[1];
				ServerGlobal.setMbURL(url);
			}
		}
	}
	public static void readMFIServiceSetting() {

		ArrayList<String> l_resultList = null;
		try {
			l_resultList = FileUtil.readList(DAOManager.AbsolutePath + "WEB-INF/data/ServiceSetting.txt");
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Get Service Path URL
		for (String string : l_resultList) {
			if (string.startsWith("MF")) {
				String url = string.split(ServerGlobal.getFCSeparator())[1];
				ServerGlobal.setMfiURL(url);
			}
		}
	}
	
	public static void readCSServiceSetting() {

		ArrayList<String> l_resultList = null;
		try {
			l_resultList = FileUtil.readList(DAOManager.AbsolutePath + "WEB-INF/data/ServiceSetting.txt");
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Get Service Path URL
		for (String string : l_resultList) {
			if (string.startsWith("CS")) {
				String url = string.split(ServerGlobal.getFCSeparator())[1];
				ServerGlobal.setCsURL(url);
			}
		}
	}

	public static void readSkyPayAPIServiceSetting() {
		ArrayList<String> apiDataList = new ArrayList<String>();
		String RechargeUrl = "";
		String userName = "";
		String pass = "";
		String pop = "";
		String entid = "";
		try {
			String apppath = System.getenv("CMSService");
			apiDataList = FileUtil.readList(apppath + "\\SkyPay\\SkypayApiConnConfig.txt");
			if (apiDataList.size() > 0) {
				RechargeUrl = apiDataList.get(0).split("RechargeURL:")[1];
				userName = apiDataList.get(4).split("USER:")[1];
				pass = apiDataList.get(5).split("PASS:")[1];
				pop = apiDataList.get(6).split("POS:")[1];
				entid = apiDataList.get(7).split("ENTID:")[1];
			}

			ServerGlobal.setmRechargeUrl(RechargeUrl);
			ServerGlobal.setMuserName(userName);
			ServerGlobal.setMpass(pass);
			ServerGlobal.setMpop(pop);
			ServerGlobal.setMEntID(entid);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void readSkyPayDebugLogStatus() {
		try {
			String apppath = System.getenv("CMSService");
			ArrayList<String> l_resultList = FileUtil.readList(apppath + "\\SkyPay\\debugconfig\\Log.txt");
			if (l_resultList != null && l_resultList.size() > 0) {
				if (l_resultList.get(0).split(":")[1].trim().equals("Yes")) {
					ServerGlobal.setWriteLog(true);
				} else {
					ServerGlobal.setWriteLog(false);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public static void readSMSDebugLogStatus() {
		try {

			apppath = System.getenv("CMSService");
			ArrayList<String> l_resultList = FileUtil.readList(apppath + "\\SMS\\debugconfig\\Log.txt");

			if (l_resultList != null && l_resultList.size() > 0) {
				System.out.println(l_resultList.get(0).split(":")[1]);
				if (l_resultList.get(0).split(":")[1].trim().equals("Yes")) {
					ServerGlobal.setWriteLog(true);
				} else {
					ServerGlobal.setWriteLog(false);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public static DepositAccData[] removeArray(DepositAccData[] arr, int index) {
		List<DepositAccData> tempList = new ArrayList<DepositAccData>(Arrays.asList(arr));
		tempList.remove(index);
		return tempList.toArray(new DepositAccData[0]);
	}

	public static String setInstitutionName(String institutionCode) {
		if (institutionCode.equalsIgnoreCase("WM00001")) {
			return "Wave Money";
		} else if (institutionCode.equalsIgnoreCase("OK00001")) {
			return "OK Dollar";
		}
		return "";
	}

	public static void writeDebugLog(String userID, String title, String description) {
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Login User ID :" + userID);
			l_err.add(title + description);
			l_err.add("=============================================================================");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}
	}

	public static boolean writeLog(ArrayList<String> pErrorList) {
		boolean l_result = false;

		try {
			String l_Date = datetoString();
			String l_Path = DAOManager.AbsolutePath + "log\\Web";
			System.out.println(l_Path);
			File dir = new File(l_Path);
			dir.mkdirs();
			FileUtil.writeList(l_Path + "\\" + l_Date + ".txt", pErrorList, true);
			l_result = true;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			l_result = false;
		}
		return l_result;

	}

	// suwai
	public static boolean writeLog(ArrayList<String> pErrorList, String logpath) {
		boolean l_result = false;

		try {
			String path = System.getenv("CMSService");
			String l_Date = datetoString();
			String l_Path = path + logpath;
			File dir = new File(l_Path);
			dir.mkdirs();
			FileUtil.writeList(l_Path + "\\" + l_Date + ".txt", pErrorList, true);
			l_result = true;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			l_result = false;
		}
		return l_result;

	}

	public static void writeLog(String fileName, String msgType, String msgData) {
		try {
			String cpath = System.getenv("CMSService");
			PrintWriter writer = new PrintWriter(new BufferedWriter(
					new FileWriter(cpath + "/SMS/log/" + getDateYYYYMMDD() + "_" + fileName + ".txt", true)));
			writer.println(getDateTime() + "\t:\t" + msgType + " : " + msgData);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static boolean writeLogWebService(ArrayList<String> pErrorList, String logpath) {
		boolean l_result = false;

		try {
			String path = System.getenv("CMSService");
			String l_Date = datetoString();
			String l_Path = path + logpath;
			File dir = new File(l_Path);
			dir.mkdirs();
			FileUtil.writeList(l_Path + "\\" + l_Date + ".txt", pErrorList, true);
			l_result = true;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			l_result = false;
		}
		return l_result;

	}
	
	public static long diffDateTime(String date1, String date2) {
		//
		Date d1 = new Date();// expdate
		Date d2 = new Date();// todaydate
		long diffMinutes = 0;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			d1 = sdf.parse(date1);
			d2 = sdf.parse(date2);
			long diff = d1.getTime() - d2.getTime();
			diffMinutes = diff / (60 * 1000) % 60; 

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return diffMinutes;
	}
	//atn
	public static void readBrCodeLength(Connection pConn) throws SQLException {
		BranchMgr dbmgr = new BranchMgr();
		CMSModuleConfigData data = new CMSModuleConfigData();
		data = dbmgr.getCMSModuleConfig(pConn);
		ServerGlobal.setmBrCodeStart(data.getBranchCodeStart());
		ServerGlobal.setmBrCodeEnd(data.getBranchCodeLength());
		ServerGlobal.setmXREFPrefix(data.getXREFPrefix());
		ServerGlobal.setBankName(data.getBankName());
	}
	
	public String formatYYYYMMDD2DDMMYYYY(String p) {
    	String ret="";
    	try{
    		ret = p.substring(6, 8) + "/" + p.substring(4, 6) + "/" + p.substring(0, 4) ;
    	}catch (Exception exp){}
    	return ret;
    }
	public String formatDDMMYYYY2YYYYMMDD(String p) {
    	String ret="";
    	try{
    		ret = p.substring(0, 4) + "-" + p.substring(4, 6) + "-" + p.substring(6, 8) ;
    	}catch (Exception exp){}
    	return ret;
    }
	public static String getNowDateTimeByPlusMillisecond(long millisecond) {
        Date nowDateTime = new Date();
        long timeResult = nowDateTime.getTime() + millisecond;
        Date dateResult = new Date(timeResult);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd hh:mm:ss");
        String dateString = dateFormat.format(dateResult);
        return dateString;
    }
	public static void readMemberStatus() {
		try {
			
			apppath = System.getenv("mBanking360");
			ArrayList<String> l_resultList = FileUtil.readList(apppath + "\\MFI\\Mobile\\memberstatus\\status.txt");
			
			if (l_resultList != null && l_resultList.size() > 0) {
				int i=0;
				for(i=0;i<l_resultList.size();i++)
				{

					String str=l_resultList.get(i).split(":")[0];
					 System.out.println(str);
					if (str.equalsIgnoreCase("Active")) {
						ServerGlobal.setActiveStatus(Integer.parseInt(l_resultList.get(i).split(":")[1]));
					} else if(str.equalsIgnoreCase("Login")) {
						ServerGlobal.setLoginStatus(Integer.parseInt(l_resultList.get(i).split(":")[1]));
					}else if(str.equalsIgnoreCase("Suspend")) {
						ServerGlobal.setSuspendStatus(Integer.parseInt(l_resultList.get(i).split(":")[1]));
					}else if(str.equalsIgnoreCase("Delete")) {
						ServerGlobal.setDeleteStatus(Integer.parseInt(l_resultList.get(i).split(":")[1]));
					}else if(str.equalsIgnoreCase("AnonymousPos")) {
						ServerGlobal.setAnonymousPos(Integer.parseInt(l_resultList.get(i).split(":")[1]));
					}else if(str.equalsIgnoreCase("RegisteredPos")) {
						ServerGlobal.setRegisteredPos(Integer.parseInt(l_resultList.get(i).split(":")[1]));
					}else if(str.equalsIgnoreCase("LoanmemberPos")) {
						ServerGlobal.setLoanmemberPos(Integer.parseInt(l_resultList.get(i).split(":")[1]));
					}else if(str.equalsIgnoreCase("GuarantorPos")) {
						ServerGlobal.setGuarantorPos(Integer.parseInt(l_resultList.get(i).split(":")[1]));
					}else if(str.equalsIgnoreCase("SuspendedPos")) {
						ServerGlobal.setSuspendedPos(Integer.parseInt(l_resultList.get(i).split(":")[1]));
					}else if(str.equalsIgnoreCase("ActivePos")) {
						ServerGlobal.setActivePos(Integer.parseInt(l_resultList.get(i).split(":")[1]));
					}else if(str.equalsIgnoreCase("LoginPos")) {
						ServerGlobal.setLoginPos(Integer.parseInt(l_resultList.get(i).split(":")[1]));
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	public static void readLoanStatus() {
		try {
			
			apppath = System.getenv("mBanking360");
			ArrayList<String> l_resultList = FileUtil.readList(apppath + "\\MFI\\Mobile\\memberstatus\\loanstatus.txt");
			
			if (l_resultList != null && l_resultList.size() > 0) {
				int i=0;
				for(i=0;i<l_resultList.size();i++)
				{

					String str=l_resultList.get(i).split(":")[0];
					 System.out.println(str);
					if (str.equalsIgnoreCase("ActivePos")) {
						ServerGlobal.setLoanActivePos(Integer.parseInt(l_resultList.get(i).split(":")[1]));
					} else if(str.equalsIgnoreCase("InprocessPos")) {
						ServerGlobal.setLoanInprocessPos(Integer.parseInt(l_resultList.get(i).split(":")[1]));
					}else if(str.equalsIgnoreCase("ApprovedPos")) {
						ServerGlobal.setLoanApprovedPos(Integer.parseInt(l_resultList.get(i).split(":")[1]));
					}else if(str.equalsIgnoreCase("GoodsDeliveredPos")) {
						ServerGlobal.setLoanGoodsDeliveredPos(Integer.parseInt(l_resultList.get(i).split(":")[1]));
					}else if(str.equalsIgnoreCase("RepaymentInProgressPos")) {
						ServerGlobal.setLoanRepaymentInProgressPos(Integer.parseInt(l_resultList.get(i).split(":")[1]));
					}else if(str.equalsIgnoreCase("RepaymentCompletedPos")) {
						ServerGlobal.setLoanRepaymentCompletedPos(Integer.parseInt(l_resultList.get(i).split(":")[1]));
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	 public static void readImagePath() {
			try {
				
				apppath = System.getenv("mBanking360");
				ArrayList<String> l_resultList = FileUtil.readList(apppath + "\\MFI\\Mobile\\ImagePath.txt");
				
				if (l_resultList != null && l_resultList.size() > 0) {
					int i=0;
					for(i=0;i<l_resultList.size();i++)
					{

						String str=l_resultList.get(i).split(":")[0];
						
						if (str.trim().equalsIgnoreCase(ImagePath.ProuctImagePath.toString())) {
							ServerGlobal.setProuctImagePath(l_resultList.get(i).split(":")[1]);
						}else if(str.trim().equalsIgnoreCase(ImagePath.OutletImagePath.toString())) {
							ServerGlobal.setOutletImagePath(l_resultList.get(i).split(":")[1]);
						}else if(str.trim().equalsIgnoreCase(ImagePath.ProductLoanImagePath.toString())) {
							ServerGlobal.setProductLoanImagePath(l_resultList.get(i).split(":")[1]);
						}else if(str.trim().equalsIgnoreCase(ImagePath.StaffLoanImagePath.toString())) {
							ServerGlobal.setStaffLoanImagePath(l_resultList.get(i).split(":")[1]);
						}else if(str.trim().equalsIgnoreCase(ImagePath.HirePurchaseImagePath.toString())) {
							ServerGlobal.setHirePurchaseImagePath(l_resultList.get(i).split(":")[1]);
						}else if(str.trim().equalsIgnoreCase(ImagePath.EducationLoanImagePath.toString())) {
							ServerGlobal.setEducationLoanImagePath(l_resultList.get(i).split(":")[1]);
						}else if(str.trim().equalsIgnoreCase(ImagePath.CommercialLoanImagePath.toString())) {
							ServerGlobal.setCommercialLoanImagePath(l_resultList.get(i).split(":")[1]);
						}else if(str.trim().equalsIgnoreCase(ImagePath.OverdraftLoanImagePath.toString())) {
							ServerGlobal.setOverdraftLoanImagePath(l_resultList.get(i).split(":")[1]);
						}else if(str.trim().equalsIgnoreCase(ImagePath.JICATwoStepLoanImagePath.toString())) {
							ServerGlobal.setJICATwoStepLoanImagePath(l_resultList.get(i).split(":")[1]);
						}else if(str.trim().equalsIgnoreCase(ImagePath.CGILoanImagePath.toString())) {
							ServerGlobal.setCGILoanImagePath(l_resultList.get(i).split(":")[1]);
						}else if(str.trim().equalsIgnoreCase(ImagePath.CFBLoanImagePath.toString())) {
							ServerGlobal.setCFBLoanImagePath(l_resultList.get(i).split(":")[1]);
						}else if(str.trim().equalsIgnoreCase(ImagePath.StaffLoanPDFDownloadPath.toString())) {
							ServerGlobal.setStaffLoanPDFDownloadPath(l_resultList.get(i).split(":")[1]);
						} 
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("");
					l_err.add("=============================================================================");
					l_err.add("Time :" + GeneralUtil.getTime());
					l_err.add("readImagePath Function.... : " + e.toString());
					l_err.add("---------------------------------------------------------------------------");
					GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
				}
			}
		}
	 public static void readOrgId() {
			try {
				
				apppath = System.getenv("mBanking360");
				ArrayList<String> l_resultList = FileUtil.readList(apppath + "\\MFI\\Mobile\\OrgConfig.txt");
				
				if (l_resultList != null && l_resultList.size() > 0) {
					int i=0;
					for(i=0;i<l_resultList.size();i++)
					{
						
						String str=l_resultList.get(i).split(":")[0];
						 
						if (str.trim().equalsIgnoreCase(OrgType.MRHFOrg.toString())) {
							ServerGlobal.setMrhforg(l_resultList.get(i).split(":")[1].trim());
						}else if(str.trim().equalsIgnoreCase(OrgType.SMIDBOrg.toString())) {
							ServerGlobal.setSmidborg(l_resultList.get(i).split(":")[1].trim());
						}
							
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("");
					l_err.add("=============================================================================");
					l_err.add("Time :" + GeneralUtil.getTime());
					l_err.add("readOrgId Function.... : " + e.toString());
					l_err.add("---------------------------------------------------------------------------");
					GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
				}
			}
		}
	 public static String[] readLoanType() {
		   String[] arr = null ;
			try {
				
				apppath = System.getenv("mBanking360");
				
				ArrayList<String> l_resultList = FileUtil.readList(apppath + "\\MFI\\Mobile\\LoanType.txt");
				
				if (l_resultList != null && l_resultList.size() > 0) {
					int i=0;
					for(i=0;i<l_resultList.size();i++)
					{

						String str=l_resultList.get(i).split(":")[0];					 
						if (str.trim().equalsIgnoreCase("LoanType")) {
							arr=l_resultList.get(i).split(":")[1].trim().split(",");					
							
						}
					}
					DESedeEncryption myEncryptor = new DESedeEncryption();
					for(i=0;i<arr.length;i++)
					{
						arr[i]=myEncryptor.decrypt(arr[i]);
					}
				}
			} catch (Exception e) {
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("");
					l_err.add("=============================================================================");
					l_err.add("Time :" + GeneralUtil.getTime());
					l_err.add("Read Loan Type Config : " +e.toString());
					l_err.add("---------------------------------------------------------------------------");
					GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
				}
			}
			return arr;
		}
}
