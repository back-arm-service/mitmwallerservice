package com.nirvasoft.rp.users.data;

import javax.xml.bind.annotation.XmlRootElement;

import com.nirvasoft.rp.data.MessageRequest;
import com.nirvasoft.rp.shared.TransferOutReq;

@XmlRootElement
public class WalletTransferData {

	private MessageRequest messageRequest;
	private TransferOutReq transferOutReq;

	public WalletTransferData() {
		clearProperties();
	}

	private void clearProperties() {
		messageRequest = new MessageRequest();
		transferOutReq = new TransferOutReq();

	}

	public MessageRequest getMessageRequest() {
		return messageRequest;
	}

	public TransferOutReq getTransferOutReq() {
		return transferOutReq;
	}

	public void setMessageRequest(MessageRequest messageRequest) {
		this.messageRequest = messageRequest;
	}

	public void setTransferOutReq(TransferOutReq transferOutReq) {
		this.transferOutReq = transferOutReq;
	}

}