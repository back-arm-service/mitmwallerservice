package com.nirvasoft.rp.users.data;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MerchantData {
	private String merchantcode;
	private String code;
	public MerchantData() {
		clearProperties();
	}

	protected void clearProperties() {
		this.merchantcode = "";
		this.code = "";
	}

	public String getMerchantcode() {
		return merchantcode;
	}

	public void setMerchantcode(String merchantcode) {
		this.merchantcode = merchantcode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
}
