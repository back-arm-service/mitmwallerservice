package com.nirvasoft.rp.users.data;

import javax.xml.bind.annotation.XmlRootElement;

import com.nirvasoft.rp.data.UserCIFJunction;

@XmlRootElement
public class UserCIFJunctionArr {
	private String userID;
	private UserCIFJunction[] data;
	private String messagecode;
	private String messagedesc;

	public UserCIFJunction[] getData() {
		return data;
	}

	public String getMessagecode() {
		return messagecode;
	}

	public String getMessagedesc() {
		return messagedesc;
	}

	public String getUserID() {
		return userID;
	}

	public void setData(UserCIFJunction[] data) {
		this.data = data;
	}

	public void setMessagecode(String messagecode) {
		this.messagecode = messagecode;
	}

	public void setMessagedesc(String messagedesc) {
		this.messagedesc = messagedesc;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

}
