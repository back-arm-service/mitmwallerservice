package com.nirvasoft.rp.users.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OTPData {
	private String otpcode;
	private String messagecode;
	private String messagedesc;

	public OTPData() {
		clearProperty();
	}

	void clearProperty() {
		otpcode = "";
		messagecode = "";
		messagedesc = "";
	}

	public String getMessagecode() {
		return messagecode;
	}

	public String getMessagedesc() {
		return messagedesc;
	}

	public String getOtpcode() {
		return otpcode;
	}

	public void setMessagecode(String messagecode) {
		this.messagecode = messagecode;
	}

	public void setMessagedesc(String messagedesc) {
		this.messagedesc = messagedesc;
	}

	public void setOtpcode(String otpcode) {
		this.otpcode = otpcode;
	}

}
