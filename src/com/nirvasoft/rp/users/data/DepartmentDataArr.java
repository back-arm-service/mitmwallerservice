package com.nirvasoft.rp.users.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DepartmentDataArr {
	private DepartmentData[] data;

	public DepartmentDataArr() {
		super();
		data = new DepartmentData[1];
		/*
		 * UserData d = new UserData(); //d.sethub("hayyyyyyyyyyyyyyyyy success"
		 * ); data[0] = d; d = new UserData(); //d.sethub(
		 * "hayyyyyyyyyyyyyyyyy success2"); data[1] = d;
		 */

	}

	public DepartmentData[] getData() {
		return data;
	}

	public void setData(DepartmentData[] data) {
		this.data = data;
	}

}
