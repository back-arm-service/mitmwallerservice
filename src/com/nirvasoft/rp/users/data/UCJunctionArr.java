package com.nirvasoft.rp.users.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UCJunctionArr {
	private String userID;
	private UCJunction[] data;
	private String messagecode;
	private String messagedesc;

	public UCJunction[] getData() {
		return data;
	}

	public String getMessagecode() {
		return messagecode;
	}

	public String getMessagedesc() {
		return messagedesc;
	}

	public String getUserID() {
		return userID;
	}

	public void setData(UCJunction[] data) {
		this.data = data;
	}

	public void setMessagecode(String messagecode) {
		this.messagecode = messagecode;
	}

	public void setMessagedesc(String messagedesc) {
		this.messagedesc = messagedesc;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

}
