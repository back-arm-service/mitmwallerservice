package com.nirvasoft.rp.users.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OTPReqData {

	private String userid;
	private String phoneno;
	private String otpcode;
	private String code;
	private String desc;
	private String sessionID;
	private boolean state = false;

	public OTPReqData() {
		clearProperty();
	}

	void clearProperty() {
		userid = "";
		phoneno = "";
		otpcode = "";
		code = "";
		desc = "";
		sessionID = "";

	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public String getotpcode() {
		return otpcode;
	}

	public String getphno() {
		return phoneno;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getuserid() {
		return userid;
	}

	public boolean isState() {
		return state;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setotpcode(String otpcode) {
		this.otpcode = otpcode;
	}

	public void setphno(String phoneno) {
		this.phoneno = phoneno;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public void setuserid(String userid) {
		this.userid = userid;
	}

	@Override
	public String toString() {
		return "OTPReqData [userid=" + userid + ", phoneno=" + phoneno + ", otpcode=" + otpcode + ", code=" + code
				+ ", desc=" + desc + "]";
	}

}
