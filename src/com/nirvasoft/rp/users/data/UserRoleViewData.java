package com.nirvasoft.rp.users.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserRoleViewData {
	private long syskey;
	private String t2;
	private boolean flag;
	private long[] rolesyskey;

	public UserRoleViewData() {
		clearProperties();
	}

	private void clearProperties() {
		this.syskey = 0;
		this.t2 = "";
		this.flag = false;

	}

	public long[] getRolesyskey() {
		return rolesyskey;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getT2() {
		return t2;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public void setRolesyskey(long[] rolesyskey) {
		this.rolesyskey = rolesyskey;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}
}
