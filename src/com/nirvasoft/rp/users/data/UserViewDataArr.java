package com.nirvasoft.rp.users.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserViewDataArr {
	private UserViewData[] data;
	private String searchText;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private String msgCode = "";
	private String msgDesc = "";
	private String sessionID = "";

	public UserViewDataArr() {
		super();
		data = new UserViewData[1];
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public UserViewData[] getdata() {
		return data;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public int getPageSize() {
		return pageSize;
	}

	public String getSearchText() {
		return searchText;
	}

	public String getSessionID() {
		return sessionID;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setdata(UserViewData[] data) {
		this.data = data;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
}
