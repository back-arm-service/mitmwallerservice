package com.nirvasoft.rp.users.data;

public class MenuViewData extends MenuData {

	private String parentMenu;

	public MenuViewData() {
		clearProperties();
	}

	@Override
	public void clearProperties() {
		super.clearProperties();
		this.parentMenu = "";
	}

	public String getParentMenu() {
		return parentMenu;
	}

	public void setParentMenu(String parentMenu) {
		this.parentMenu = parentMenu;
	}

}
