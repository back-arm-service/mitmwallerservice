package com.nirvasoft.rp.users.data;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.nirvasoft.rp.shared.ExcludedAccount;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserData {
	private int srno;// atn
	private long syskey;
	private long autokey;
	private String chkAccount;
	private String chkGL;
	private String keyResult;
	private String createdDate;
	private String modifiedDate;
	private String userId;
	private String userName;
	private String password;
	private String organizationID;
	private int recordStatus;
	private int syncStatus;
	private long syncBatch;
	private String customerid;
	private String msgCode = "";
	private String msgDesc = "";
	private String sessionId = "";
	private String parentID = "";
	private String accountNumber = "";
	private String username = "";
	private int n1 = 0;

	private long usersyskey;
	private String t1;
	private String t2;
	private String t3;
	private String t4;
	private String t5;
	private String t6;
	private String t7;
	private String t8;
	private String t9;
	private String t10;
	private String t11;
	private String t12;
	private String t21;
	private String t41;
	private String state;
	private String city;
	private long[] rolesyskey;
	private UserRoleViewData[] userrolelist;
	private PersonData person;
	private String name;
	private int loginstatus;
	private UCJunction[] data;
	private ExcludedAccount[] exList;

	public UserData() {
		clearProperties();
	}

	protected void clearProperties() {
		this.srno = 0;
		this.syskey = 0;
		this.autokey = 0;
		this.createdDate = "";
		this.modifiedDate = "";
		this.userId = "";
		this.userName = "";
		this.recordStatus = 0;
		this.syncStatus = 0;
		this.syncBatch = 0;
		this.usersyskey = 0;
		this.t1 = "";
		this.t2 = "";
		this.t3 = "";
		this.t4 = "";
		this.t5 = "";
		this.t6 = "";
		this.t7 = "";
		this.state = "";
		this.city = "";
		this.customerid = "";
		this.loginstatus = 0;
		this.msgCode = "";
		this.msgDesc = "";
		this.sessionId = "";
		this.person = new PersonData();
		this.exList = null;
		this.n1 = 0;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public long getAutokey() {
		return autokey;
	}

	public String getChkAccount() {
		return chkAccount;
	}

	public String getChkGL() {
		return chkGL;
	}

	public String getCity() {
		return city;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public String getCustomerid() {
		return customerid;
	}

	public UCJunction[] getData() {
		return data;
	}

	public ExcludedAccount[] getExList() {
		return exList;
	}

	public String getKeyResult() {
		return keyResult;
	}

	public int getLoginstatus() {
		return loginstatus;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public int getN1() {
		return n1;
	}

	public String getName() {
		return name;
	}

	public String getOrganizationID() {
		return organizationID;
	}

	public String getParentID() {
		return parentID;
	}

	public String getPassword() {
		return password;
	}

	public PersonData getPerson() {
		return person;
	}

	public int getRecordStatus() {
		return recordStatus;
	}

	public long[] getRolesyskey() {
		return rolesyskey;
	}

	public String getSessionId() {
		return sessionId;
	}

	public int getSrno() {
		return srno;
	}

	public String getState() {
		return state;
	}

	public long getSyncBatch() {
		return syncBatch;
	}

	public int getSyncStatus() {
		return syncStatus;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getT1() {
		return t1;
	}

	public String getT10() {
		return t10;
	}

	public String getT11() {
		return t11;
	}

	public String getT12() {
		return t12;
	}

	public String getT2() {
		return t2;
	}

	public String getT21() {
		return t21;
	}

	public String getT3() {
		return t3;
	}

	public String getT4() {
		return t4;
	}

	public String getT41() {
		return t41;
	}

	public String getT5() {
		return t5;
	}

	public String getT6() {
		return t6;
	}

	public String getT7() {
		return t7;
	}

	public String getT8() {
		return t8;
	}

	public String getT9() {
		return t9;
	}

	public String getUserId() {
		return userId;
	}

	public String getUsername() {
		return username;
	}

	public String getUserName() {
		return userName;
	}

	public UserRoleViewData[] getUserrolelist() {
		return userrolelist;
	}

	public long getUsersyskey() {
		return usersyskey;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setAutokey(long autokey) {
		this.autokey = autokey;
	}

	public void setChkAccount(String chkAccount) {
		this.chkAccount = chkAccount;
	}

	public void setChkGL(String chkGL) {
		this.chkGL = chkGL;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}

	public void setData(UCJunction[] data) {
		this.data = data;
	}

	public void setExList(ExcludedAccount[] exList) {
		this.exList = exList;
	}

	public void setKeyResult(String keyResult) {
		this.keyResult = keyResult;
	}

	public void setLoginstatus(int loginstatus) {
		this.loginstatus = loginstatus;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setOrganizationID(String organizationID) {
		this.organizationID = organizationID;
	}

	public void setParentID(String parentID) {
		this.parentID = parentID;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPerson(PersonData person) {
		this.person = person;
	}

	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}

	public void setRolesyskey(long[] result) {
		this.rolesyskey = result;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public void setSrno(int srno) {
		this.srno = srno;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setSyncBatch(long syncBatch) {
		this.syncBatch = syncBatch;
	}

	public void setSyncStatus(int syncStatus) {
		this.syncStatus = syncStatus;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT10(String t10) {
		this.t10 = t10;
	}

	public void setT11(String t11) {
		this.t11 = t11;
	}

	public void setT12(String t12) {
		this.t12 = t12;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setT21(String t21) {
		this.t21 = t21;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public void setT4(String t4) {
		this.t4 = t4;
	}

	public void setT41(String t41) {
		this.t41 = t41;
	}

	public void setT5(String t5) {
		this.t5 = t5;
	}

	public void setT6(String t6) {
		this.t6 = t6;
	}

	public void setT7(String t7) {
		this.t7 = t7;
	}

	public void setT8(String t8) {
		this.t8 = t8;
	}

	public void setT9(String t9) {
		this.t9 = t9;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setUserrolelist(UserRoleViewData[] userrolelist) {
		this.userrolelist = userrolelist;
	}

	public void setUsersyskey(long usersyskey) {
		this.usersyskey = usersyskey;
	}

}
