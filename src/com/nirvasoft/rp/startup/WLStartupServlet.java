
package com.nirvasoft.rp.startup;

import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.nirvasoft.rp.core.ccbs.LogicMgr;
import com.nirvasoft.rp.core.ccbs.data.db.DBFECCurrencyRateMgr;
import com.nirvasoft.rp.dao.AccountDao;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.ServerSession;
import com.nirvasoft.rp.mgr.DBSystemMgr;
import com.nirvasoft.rp.shared.SharedLogic;
import com.nirvasoft.rp.shared.icbs.ReferenceAccountsData;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.GeneralUtility;
import com.nirvasoft.rp.util.ServerGlobal;

public class WLStartupServlet implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {		
		// TODO Auto-generated method stub
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {		
		getPath(arg0);
		GeneralUtil.readDebugLogStatus();
		ConnAdmin.readConnection();
		DAOManager.readICBSConnection();
		ConnAdmin.readConfig();
		readDeafultData();
		GeneralUtil.readCCDAPIServiceSetting();		
	}
	
	public void getPath(ServletContextEvent contextPath) {		
		ServerSession.serverPath = contextPath.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = contextPath.getServletContext().getRealPath("/") + "/";
	}
	
	public void readDeafultData() {
		DAOManager l_Dao = new DAOManager();
		AccountDao l_AccDao = new AccountDao();	
		Connection l_Conn = null,l_Conn1=null;
		try {
			l_Conn = l_Dao.openICBSConnection();
			ServerGlobal.setmBranchCode(l_AccDao.getBranchCode(l_Conn));
			ServerGlobal.setmProduct(l_AccDao.getProductCode(l_Conn));
			ServerGlobal.setmAccType("01");
			ServerGlobal.setmCurrencyCode("MMK");
			ServerGlobal.setmCashInHand(l_AccDao.getCashGL(l_Conn));
			ServerGlobal.setmZone(52);
			SharedLogic.setSystemData(DBSystemMgr.readSystemData(l_Conn));
			com.nirvasoft.rp.shared.SharedLogic.setSystemData(SharedLogic.getSystemData());
			ServerGlobal.setmMinBalSetting(SharedLogic.getSystemSettingT12N1("ACTMINBAL"));
			ServerGlobal.setmCurRate(DBFECCurrencyRateMgr.getFECCurrencyRate(ServerGlobal.getmCurrencyCode(), "smTransfer",
					ServerGlobal.getmBranchCode(), l_Conn));
			ServerGlobal.setmByForceCheque(DBSystemMgr.getByForceCheck(l_Conn));
			ReferenceAccountsData l_RefAccData = new ReferenceAccountsData();
			GeneralUtility.cutoffTime = GeneralUtility.getCutOffTime();
			ServerGlobal.setmCutOffTimeHr(GeneralUtility.cutoffTime.getHour());
			ServerGlobal.setmCutOffTimeMin(GeneralUtility.cutoffTime.getMinutes());
			l_RefAccData = DBSystemMgr.getReferenceAccCode("WTR"+ServerGlobal.getmBranchCode(),l_Conn);
			ServerGlobal.setmWalletGL(l_RefAccData.getGLCode());
			l_RefAccData = DBSystemMgr.getReferenceAccCode("LA"+ServerGlobal.getmBranchCode(),l_Conn);
            ServerGlobal.setLoanGL(l_RefAccData.getGLCode());
			ServerGlobal.setHasCheque(LogicMgr.Account.getProductCodeToProductData(ServerGlobal.getmProduct()).hasCheque());
			l_Conn1 = ConnAdmin.getConn();
			GeneralUtil.readBrCodeLength(l_Conn1);	
			
		} catch (Exception e) {			
			e.printStackTrace();
		}finally {			
			try {
				if(!l_Conn.isClosed())
				l_Conn.close();
				if(!l_Conn1.isClosed())
					l_Conn.close();
			} catch (SQLException e) {				
				e.printStackTrace();
			}			
		}
		
	}
	
	public void addMInBalSetting() {
		
	}
	
}
