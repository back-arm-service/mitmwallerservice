package com.nirvasoft.rp.framework;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ResultTwo {

	private boolean state = false;
	private String msgCode = "";
	private String msgDesc = "";
	private String userid;

	public ResultTwo() {
		clearProperties();
	}

	private void clearProperties() {
		state = false;
		msgCode = "";
		msgDesc = "";
	}

	public String getMsgCode() {
		return msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public String getUserid() {
		return userid;
	}

	public boolean isState() {
		return state;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

}
