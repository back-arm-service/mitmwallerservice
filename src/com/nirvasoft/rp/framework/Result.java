package com.nirvasoft.rp.framework;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

import com.nirvasoft.cms.shared.TempData;

@XmlRootElement
public class Result {

	private double amount = 0.00;
	private String Keyst = "";
	private boolean state = false;
	private String merchantID = "";
	private String userid = "";
	private String msgCode = "";
	private String msgDesc = "";
	private long keyResult = 0;
	private String loginID = ""; // Login ID
	private String phNo = "";
	private String sessionID = "";
	private String otpcode = "";
	private String keyString = "";
	private int status = 0;
	long syskey = 0;
	long memberSyskey=0;
	long gSyskey=0;
	private boolean isOld = false;
	private ArrayList<Long> longResult = new ArrayList<Long>();
	private ArrayList<String> stringResult = new ArrayList<String>();
	private ArrayList<TempData> merchantdata = new ArrayList<TempData>();
	
	
	public long getgSyskey() {
		return gSyskey;
	}

	public void setgSyskey(long gSyskey) {
		this.gSyskey = gSyskey;
	}

	public String getKeyString() {
		return keyString;
	}

	public void setKeyString(String keyString) {
		this.keyString = keyString;
	}

	public ArrayList<TempData> getMerchantdata() {
		return merchantdata;
	}

	public void setMerchantdata(ArrayList<TempData> merchantdata) {
		this.merchantdata = merchantdata;
	}

	private String fname = "";

	private long[] key;

	public Result() {
		clearProperties();
	}

	public long getMemberSyskey() {
		return memberSyskey;
	}

	public void setMemberSyskey(long memberSyskey) {
		this.memberSyskey = memberSyskey;
	}

	private void clearProperties() {
		state = false;
		merchantID = "";
		userid = "";
		msgCode = "";
		msgDesc = "";
		keyResult = 0;

		longResult = new ArrayList<Long>();
		stringResult = new ArrayList<String>();
	}

	public double getAmount() {
		return amount;
	}

	public String getFname() {
		return fname;
	}

	public long[] getKey() {
		return key;
	}

	public long getKeyResult() {
		return keyResult;
	}

	public String getKeyst() {
		return Keyst;
	}

	public String getLoginID() {
		return loginID;
	}

	public ArrayList<Long> getLongResult() {
		return longResult;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public String getOtpcode() {
		return otpcode;
	}

	public String getPhNo() {
		return phNo;
	}

	public String getSessionID() {
		return sessionID;
	}

	public ArrayList<String> getStringResult() {
		return stringResult;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getUserid() {
		return userid;
	}

	public boolean isOld() {
		return isOld;
	}

	public boolean isState() {
		return state;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public void setKey(long[] key) {
		this.key = key;
	}

	public void setKeyResult(long keyResult) {
		this.keyResult = keyResult;
	}

	public void setKeyst(String keyst) {
		Keyst = keyst;
	}

	public void setLoginID(String loginID) {
		this.loginID = loginID;
	}

	public void setLongResult(ArrayList<Long> longResult) {
		this.longResult = longResult;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public void setOld(boolean isOld) {
		this.isOld = isOld;
	}

	public void setOtpcode(String otpcode) {
		this.otpcode = otpcode;
	}

	public void setPhNo(String phNo) {
		this.phNo = phNo;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public void setStringResult(ArrayList<String> stringResult) {
		this.stringResult = stringResult;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}


}
