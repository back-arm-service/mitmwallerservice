package com.nirvasoft.rp.framework;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class Ref {

	private String value;
	private String caption;
	private String processingCode;

	public Ref() {
		clearProperty();
	}

	void clearProperty() {
		value = "";
		caption = "";
		processingCode = "";
	}

	public String getcaption() {
		return caption;
	}

	public String getProcessingCode() {
		return processingCode;
	}

	public String getvalue() {
		return value;
	}

	public void setcaption(String caption) {
		this.caption = caption;
	}

	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}

	public void setvalue(String value) {
		this.value = value;
	}

}
