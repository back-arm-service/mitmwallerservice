package com.nirvasoft.rp.framework;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LOVDetails {

	private String value;
	private String caption;

	public LOVDetails() {
		this.clearProperty();
	}

	private void clearProperty() {
		this.value = "";
		this.caption = "";
	}

	public String getCaption() {
		return caption;
	}

	public String getValue() {
		return value;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "LOVDetails [value=" + value + ", caption=" + caption + "]";
	}

}
