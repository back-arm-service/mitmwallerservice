package com.nirvasoft.rp.framework;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Menu {
	private String menuitem;
	private String caption;
	private Menu[] menuitems;

	public String getCaption() {
		return caption;
	}

	public String getMenuItem() {
		return menuitem;
	}

	public Menu[] getMenuItems() {
		return menuitems;
	}

	public void setCaption(String p) {
		this.caption = p;
	}

	public void setMenuItem(String p) {
		this.menuitem = p;
	}

	public void setMenuItems(Menu[] p) {
		this.menuitems = p;
	}

	@Override
	public String toString() {
		return "Menu [menuitem=" + menuitem + ", caption=" + caption + ", menuitems=" + Arrays.toString(menuitems)
				+ "]";
	}

}
