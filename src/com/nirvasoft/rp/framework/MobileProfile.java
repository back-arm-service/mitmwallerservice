package com.nirvasoft.rp.framework;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

import com.nirvasoft.rp.shared.DepositAccData;

@XmlRootElement
public class MobileProfile {
	private String userID;
	private String password;
	private DepositAccData[] debitacc;
	private String code;
	private String desc;

	public MobileProfile() {
		userID = "";
		password = "";
		code = "";
		desc = "";
		debitacc = null;

	}

	public String getCode() {
		return code;
	}

	public DepositAccData[] getDebitacc() {
		return debitacc;
	}

	public String getDesc() {
		return desc;
	}

	public String getPassword() {
		return password;
	}

	public String getUserID() {
		return userID;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDebitacc(DepositAccData[] debitacc) {
		this.debitacc = debitacc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	@Override
	public String toString() {
		return "MobileProfile [userID=" + userID + ", password=" + password + ", debitacc=" + Arrays.toString(debitacc)
				+ ", code=" + code + ", desc=" + desc + "]";
	}

}
