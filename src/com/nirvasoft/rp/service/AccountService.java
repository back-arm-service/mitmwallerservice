package com.nirvasoft.rp.service;

import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.framework.ServerSession;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.shared.UserRegistrationData;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ServerGlobal;

public class AccountService {
	@Context
	HttpServletRequest request;

	@Context
	private HttpServletResponse response;

	@javax.ws.rs.core.Context
	ServletContext context;

	public void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
	}

	@POST
	@Path("register")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ResponseData register(UserRegistrationData req) {
		getPath();
		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("User Registration.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}

		ResponseData res = new ResponseData();

		// Response log
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("User Registration: " + response.toString());
			l_err.add("=============================================================================");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}
		return res;
	}

}
