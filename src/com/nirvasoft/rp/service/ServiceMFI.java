package com.nirvasoft.rp.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.io.FilenameUtils;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.nirvasoft.mfi.mgr.ApplicationFormMgr;
import com.nirvasoft.mfi.mgr.MobileMemberDataMgr;
import com.nirvasoft.mfi.mgr.OrganizationMgr;
import com.nirvasoft.mfi.mgr.PaymentInstallmentMgr;
import com.nirvasoft.mfi.mgr.PaymentTermMgr;
import com.nirvasoft.mfi.mgr.ProductListMgr;
import com.nirvasoft.mfi.mgr.RegionReferenceMgr;
import com.nirvasoft.mfi.mgr.ShopListMgr;
import com.nirvasoft.mfi.mgr.TownshipReferenceMgr;
import com.nirvasoft.mfi.users.data.ApplicationFormData;
import com.nirvasoft.mfi.users.data.ApplicationFormDataArray;
import com.nirvasoft.mfi.users.data.MfiRequest;
import com.nirvasoft.mfi.users.data.MobileMemberData;
import com.nirvasoft.mfi.users.data.MobileMemberDataArray;
import com.nirvasoft.mfi.users.data.OrganizationData;
import com.nirvasoft.mfi.users.data.PaymentInstallmentDataArray;
import com.nirvasoft.mfi.users.data.PaymentTermDataArray;
import com.nirvasoft.mfi.users.data.ProductListDataArray;
import com.nirvasoft.mfi.users.data.RegionDataArray;
import com.nirvasoft.mfi.users.data.ShopListDataArray;
import com.nirvasoft.mfi.users.data.TownshipDataArray;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.dao.WalletTransactionDao;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.framework.ServerSession;
import com.nirvasoft.rp.shared.AccTransferReq;
import com.nirvasoft.rp.shared.GoPaymentResponse;
import com.nirvasoft.rp.shared.ResultData;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.GeneralUtility;
import com.nirvasoft.rp.util.ServerGlobal;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.sun.jersey.multipart.FormDataParam;

@Path("/serviceMFI")
public class ServiceMFI {
	@Context
	HttpServletRequest req;
	@Context
	private HttpServletResponse response;
	@javax.ws.rs.core.Context
	ServletContext context;
	public void getPath() {
		ServerSession.serverPath = req.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = req.getServletContext().getRealPath("/") + "/";
	}
	
	@GET
	@Path("getShopListByRegionSyskeyandSearchData")
	@Produces(MediaType.APPLICATION_JSON)

	public ShopListDataArray getShopListByRegionSyskeyandSearchData(@QueryParam("regionsyskey") String regionsyskey,@QueryParam("searchdata") String searchdata) {
		getPath();
		ShopListDataArray res = ShopListMgr.getShopListByRegionSyskeyandSearchData(regionsyskey,searchdata);

		return res;
	}
	
	@GET
	@Path("getRegionList")
	@Produces(MediaType.APPLICATION_JSON)

	public RegionDataArray getRegionList() {
		getPath();
		RegionDataArray res = RegionReferenceMgr.getRegionRefList();

		return res;
	}
	
	@GET
	@Path("getProductListByOutletSyskey")
	@Produces(MediaType.APPLICATION_JSON)

	public ProductListDataArray getProductListByOutletSyskey(@QueryParam("outletsyskey") String syskey) {
		getPath();
		ProductListDataArray res = ProductListMgr.getProductListByOutletSyskey(syskey);

		return res;
	}
	
	@GET
	@Path("getMemberData")
	@Produces(MediaType.APPLICATION_JSON)

	public MobileMemberData getMemberDataBysyskey(@QueryParam("syskey") String syskey) {
		getPath();
		MobileMemberData data = new MobileMemberData();
		data = MobileMemberDataMgr.getMemberBysyskey(syskey);

		return data;
	}
	@GET
	@Path("getTownshipList")
	@Produces(MediaType.APPLICATION_JSON)

	public TownshipDataArray getTownshipList(@QueryParam("regionRefSyskey") String regionRefSyskey) {
		getPath();
		TownshipDataArray res = TownshipReferenceMgr.getTownshipRefListByRegion(Long.parseLong(regionRefSyskey));

		return res;
	}
	@GET
	@Path("checkGuarantor")
	@Produces(MediaType.APPLICATION_JSON)

	public MobileMemberDataArray checkGuarantor(@QueryParam("syskey") String applicantsyskey) {
		getPath();
		MobileMemberDataArray data = new MobileMemberDataArray();
		data = MobileMemberDataMgr.getGuarantorBysyskey(applicantsyskey);

		return data;
	}
	@GET
	@Path("getPaymentTermsByOrgSyskey")
	@Produces(MediaType.APPLICATION_JSON)

	public PaymentTermDataArray getPaymentTermsByOrgSyskey(@QueryParam("orgsyskey") String syskey) {
		getPath();
		PaymentTermDataArray res = PaymentTermMgr.getPaymentByOrgSyskey(syskey);

		return res;
	}
	@GET
	@Path("getLoanListByApplicantSyskeyandLoanStatus")
	@Produces(MediaType.APPLICATION_JSON)
	public ApplicationFormDataArray getLoanListByApplicantSyskeyandLoanStatus(
			@QueryParam("syskey") String applicantsyskey, @QueryParam("loanstatus") String loanstatus) {
		getPath();
		ApplicationFormDataArray res = new ApplicationFormDataArray();
		res = ApplicationFormMgr.getLoanListBySyskeyandLoanstatus(applicantsyskey, loanstatus);
		return res;

	}
	@GET
	@Path("getLoanhistoryByAppandLoanSyskey")
	@Produces(MediaType.APPLICATION_JSON)
	public ApplicationFormDataArray getLoanhistoryByAppandLoanSyskey(@QueryParam("syskey") String applicantsyskey,
			@QueryParam("loansyskey") String loansyskey) {
		getPath();
		ApplicationFormDataArray res = new ApplicationFormDataArray();
		res = ApplicationFormMgr.getLoanDataByApplicantandLoanSyskey(applicantsyskey, loansyskey);
		return res;

	}
	
	@GET
	@Path("loadLoanRepaymentInstallmentlist")
	@Produces(MediaType.APPLICATION_JSON)
	public ApplicationFormDataArray loadLoanRepaymentInstallmentlist(@QueryParam("syskey") String applicantsyskey,
			@QueryParam("loanstatus") String loanstatus) {
		getPath();
		ApplicationFormDataArray res = new ApplicationFormDataArray();
		res = ApplicationFormMgr.getLoanListBySyskeyandRepaymentstatus(applicantsyskey, loanstatus);
		return res;

	}
	
	@GET
	@Path("loadRepaymentInstallmentlistByLoansyskey")
	@Produces(MediaType.APPLICATION_JSON)
	public PaymentInstallmentDataArray loadRepaymentInstallmentlistByLoansyskey(@QueryParam("loansyskey") String loansyskey) {
		getPath();
		PaymentInstallmentDataArray res = new PaymentInstallmentDataArray();
		res = PaymentInstallmentMgr.getLoanListBySyskeyandRepaymentstatus( loansyskey);
		return res;

	}
	@POST
	@Path("applyLoan")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON)
	public Result applyLoan(ApplicationFormData data) {
		getPath();
		System.out.println(data.getLoandata().toString());
		//System.out.println(data.getApplyProductList().toString());
		GeneralUtil.readMemberStatus();
		GeneralUtil.readLoanStatus();
		Result res = new Result();
		MfiRequest mfirequest=new MfiRequest();

//		res = GeneralUtility.normalizePhoneNoformat(data.getApplicantdata().getMobile());
//		if (res.isState() == false)// Invalid
//		{
//			res.setMsgCode("0014");
//			res.setMsgDesc("Invalid Applicant Mobile Phone No.");
//		} else {

			//res = GeneralUtility.normalizePhoneNoformat(data.getApplicantdata().getCompany_ph_no());
			//if (res.isState() == false)// Invalid
			//{
			//	res.setMsgCode("0014");
			//	res.setMsgDesc("Invalid Applicant Company Phone No.");
			//} else {
				//res = GeneralUtility.normalizePhoneNoformat(data.getGuarantordata().getMobile());
				//if (res.isState() == false)// Invalid
				//{
				//	res.setMsgCode("0014");
				//	res.setMsgDesc("Invalid Guarantor Mobile Phone No.");
				//} else {
					//res = GeneralUtility.normalizePhoneNoformat(data.getGuarantordata().getCompany_ph_no());
					//if (res.isState() == false)// Invalid
					//{
					//	res.setMsgCode("0014");
					//	res.setMsgDesc("Invalid Guarantor Company Phone No.");
					//} else {
						String dobs = data.getApplicantdata().getDob();
						String ayear = dobs.substring(0, 4);
						String amonth = dobs.substring(4, 6);
						String aday = dobs.substring(6, 8);
						int age = 0;
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

						Calendar dob = Calendar.getInstance();

						try {
							dob.setTime(sdf.parse(ayear + "-" + amonth + "-" + aday));
							age = GeneralUtility.getAge(dob);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if (age < 18)// Invalid
						{
							res.setMsgCode("0014");
							res.setMsgDesc("Applicant Age must be greater than 18.");
						} else {
							dobs = data.getGuarantordata().getDob();
							ayear = dobs.substring(0, 4);
							amonth = dobs.substring(4, 6);
							aday = dobs.substring(6, 8);
							sdf = new SimpleDateFormat("yyyy-MM-dd");

							try {
								dob.setTime(sdf.parse(ayear + "-" + amonth + "-" + aday));
								age = GeneralUtility.getAge(dob);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							if (age < 18)// Invalid
							{
								res.setMsgCode("0014");
								res.setMsgDesc("Guarantor Age must be greater than 18.");
							} else {

								res = ApplicationFormMgr.saveApplicantData(data);
								if(res.getMsgCode().equals("0000")){
									mfirequest.setT1(String.valueOf(res.getSyskey()));
									mfirequest.setT2(data.getApplicantdata().getMobile());
									mfirequest.setT3(data.getApplicantdata().getMember_name());
									mfirequest.setT4("70");
									ResultData response=new ResultData();
									response=MFICallAPI(mfirequest);
								}
								
								
							} // end of guarantor age validation
						} // end of applicant age validation
					//} // end of guarantor company phone no
				//} // end of guarantor phone no
			//} // end of chek applicant company phno
		//} // end of check applicant phone no

		return res;

	}
	
	public ResultData MFICallAPI(MfiRequest mfireq) {
		ResultData res = new ResultData();
		GeneralUtil.readMFIServiceSetting();
		String aUrl = ServerGlobal.getMfiURL();	
        int responseCode = -1;
        String responseJSONBody = "";
        try {
        	
        	String ecodedValue1 = URLEncoder.encode(mfireq.getT3(), StandardCharsets.UTF_8.name());
    		//String decodedValue1 = URLDecoder.decode(ecodedValue1, StandardCharsets.UTF_8.name());
    		
        	aUrl = aUrl.replace("<parameter1>",mfireq.getT1());
        	aUrl = aUrl.replace("<parameter2>",mfireq.getT2());
        	aUrl = aUrl.replace("<parameter3>",ecodedValue1);
        	aUrl = aUrl.replace("<parameter4>",mfireq.getT4());
     
            URL url = new URL(aUrl);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setConnectTimeout(10000);
            httpURLConnection.setReadTimeout(10000);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestMethod("POST");
            responseCode = httpURLConnection.getResponseCode();
            responseJSONBody = convertStreamToString(httpURLConnection.getInputStream());
            
            if (responseCode == 200 && responseJSONBody != null && !responseJSONBody.trim().equals("")) {
                JSONParser jsonParser = new JSONParser();
                Object obj = jsonParser.parse(responseJSONBody);
                JSONObject jsonObj = (JSONObject) obj;
                res.setMsgCode((String) jsonObj.get("msgCode"));
                if( res.getMsgCode().equals("0000")){
                	
                }else{
                	
                }
            } else {
                res.setMsgCode("0014");
                res.setMsgDesc("Call MFI Service Response Fail");
                return res;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            res.setMsgCode("0014");
            res.setMsgDesc("Call MFI Service Connection Fail");
            //res.setError(ex.getMessage());
            return res;
        }
        return res;
    }
	 private String convertStreamToString(InputStream inputStream) throws Exception {
	        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
	        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	        StringBuilder stringBuilder = new StringBuilder();
	        String line = null;
	        while ((line = bufferedReader.readLine()) != null) {
	            stringBuilder.append(line);
	        }
	        return stringBuilder.toString();
	    }
	private byte[] getRequestJSONBodyWithByteFormat(String t1, String t2, String t3,String t4)
            throws Exception {
        HashMap<String, String> mapObj = new HashMap<>();
        mapObj.put("t1", t1);
        mapObj.put("t2", t2);
        mapObj.put("t3", t3);
        mapObj.put("t4", t4);
        JSONObject requestJSONObj = new JSONObject(mapObj);
        return requestJSONObj.toString().getBytes(Charset.forName("UTF-8"));
    }
	@GET
	@Path("getOrganizationSyskey")
	@Produces(MediaType.APPLICATION_JSON)

	public OrganizationData getOrganizationSyskey() {
		getPath();
		OrganizationData res = new OrganizationData();
		long syskey = OrganizationMgr.getOrganizationSyskey();
		res.setSyskey(syskey);
		return res;
	}
	@POST
	@Path("/mobileupload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.TEXT_PLAIN)
	public String mobileuploadFile(@FormDataParam("file") InputStream fis,
			@FormDataParam("file") FormDataContentDisposition fdcd) {
		System.out.println("I m here=" + fdcd);
		String orgname = "upload/image/userProfile/"; 
		
		String filePath = req.getServletContext().getRealPath("/" + orgname);
		filePath = filePath.replace("WalletService", "MFIMedia");

		OutputStream outpuStream = null;		
		String fileName = fdcd.getFileName();		
		String basename = FilenameUtils.getBaseName(fileName);		
		String extension = FilenameUtils.getExtension(fileName);
		String fullname = basename + "." + extension;
		filePath += fullname;		
		try {
			int read = 0;
			byte[] bytes = new byte[1024];
			outpuStream = new FileOutputStream(new File(filePath));
			while ((read = fis.read(bytes)) != -1) {
				outpuStream.write(bytes, 0, read);
			}
			outpuStream.flush();
			outpuStream.close();
		} catch (IOException iox) {
			iox.printStackTrace();
		} finally {
			if (outpuStream != null) {
				try {
					outpuStream.close();
				} catch (Exception ex) {
				}
			}
		}
		return fullname;
	}
	@POST
	@Path("getMonthlyAmountByTerm")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON)
	public ApplicationFormData getMonthlyAmountByTerm(ApplicationFormData data) {
		getPath();
		ApplicationFormData res = new ApplicationFormData();

		try {
			res = ApplicationFormMgr.getMonthlyAmountByTerm(data);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			data.setMsgcode("0014");
			data.setMsgDesc("Connection Fail.");
		}

		return res;

	}
	@GET
	@Path("getGuarantorData")
	@Produces(MediaType.APPLICATION_JSON)

	public MobileMemberData getGuarantorDataBysyskey(@QueryParam("syskey") String syskey) {
		getPath();
		MobileMemberData data = new MobileMemberData();
		data = MobileMemberDataMgr.getGuarantorDataByGuarantorsyskey(syskey);

		return data;
	}
	
}
