package com.nirvasoft.rp.service;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

import com.nirvasoft.cms.dao.RegisterDao;
import com.nirvasoft.cms.dao.UploadDao;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.framework.ServerSession;
import com.nirvasoft.cms.mgr.QnAMgr;
import com.nirvasoft.cms.mgr.RegisterMgr;
import com.nirvasoft.cms.shared.FileUploadResponseData;
import com.nirvasoft.cms.shared.RegisterData;
import com.nirvasoft.cms.shared.UploadData;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.rp.dao.DAOManager;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

@Path("/fileAdm")
public class FileUploadAdm {
	@Context
	HttpServletRequest request;

	@javax.ws.rs.core.Context
	ServletContext context;
	public void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
	}
	private MrBean getUser() {
		getPath();
        DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		MrBean user = new MrBean();
		user.getUser().setOrganizationID("001");
		user.getUser().setUserId(Service001.userid);
		user.getUser().setUserName(Service001.username);
		return user;
	}
	
	@POST
	@Path("fileupload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public String uploadFile(@QueryParam("f") String filePath, @QueryParam("fn") String inputFileName,
			@QueryParam("id") String userid, @QueryParam("type") String type, @QueryParam("imgUrl") String imgUrl)
			throws AWTException {
		// System.out.println("File Upload in cms: " );
		String result = "{\"code\":\"ERROR\"}";
		String sfilePath = "";
		String outputFileName = "";
		String fileName="";							
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		if (inputFileName.contains("/")) {
			inputFileName = inputFileName.substring(inputFileName.lastIndexOf("/") + 1);
		}
		sfilePath = request.getServletContext().getRealPath("/") + "/" + filePath;
		filePath = request.getServletContext().getRealPath("/") + "/" + filePath;	
		System.out.println(filePath+"Flie Path");
		if (filePath.contains("DigitalConnect")) {
			filePath = filePath.replace("DigitalConnect", "DigitalMedia");
		}
		if (sfilePath.contains("DigitalConnect")) {
			sfilePath = sfilePath.replace("DigitalConnect", "DigitalMedia");
		}
		if (filePath.contains("WalletService")) {
			filePath = filePath.replace("WalletService", "DigitalMedia");
		}
		if (sfilePath.contains("WalletService")) {
			sfilePath = sfilePath.replace("WalletService", "DigitalMedia");
		}
		String extension = FilenameUtils.getExtension(inputFileName);
		if (extension.equalsIgnoreCase("JPEG") || extension.equalsIgnoreCase("JPG")
				|| extension.equalsIgnoreCase("PNG")) {
			filePath += "/image";
			sfilePath += "/smallImage";
			imgUrl += "upload/smallImage";

			
			if (Integer.parseInt(type) == 9) {
				filePath += "/WriterImage";
				sfilePath += "/WriterImage";
				imgUrl += "/WriterImage";
			} else if (Integer.parseInt(type) == 10) {
				filePath += "/videoImage";
				sfilePath += "/videoImage";
				imgUrl += "/videoImage";
				if (extension.equalsIgnoreCase("JPEG") || extension.equalsIgnoreCase("JPG")
						|| extension.equalsIgnoreCase("PNG")) {
				if (Integer.parseInt(type) == 0 || Integer.parseInt(type) == 1 || Integer.parseInt(type) == 2
						|| Integer.parseInt(type) == 3 || Integer.parseInt(type) == 4 || Integer.parseInt(type) == 5
						|| Integer.parseInt(type) == 6 || Integer.parseInt(type) == 7 || Integer.parseInt(type) == 10 ) {//7,10
					filePath += "/" + ServerUtil.datetoString();
					sfilePath += "/" + ServerUtil.datetoString();
					imgUrl += "/" + ServerUtil.datetoString();
				}
				}
			} else {
				filePath += "/contentImage";
				sfilePath += "/contentImage";
				imgUrl += "/contentImage";
			}

			if (Integer.parseInt(type) == 0 || Integer.parseInt(type) == 1 || Integer.parseInt(type) == 2
					|| Integer.parseInt(type) == 3 || Integer.parseInt(type) == 4 || Integer.parseInt(type) == 5
					|| Integer.parseInt(type) == 6 || Integer.parseInt(type) == 7 ) {//7,10
				filePath += "/" + ServerUtil.datetoString();
				sfilePath += "/" + ServerUtil.datetoString();
				imgUrl += "/" + ServerUtil.datetoString();
			}
			outputFileName = "IMG";
		} else if (extension.equalsIgnoreCase("MP4") || extension.equalsIgnoreCase("FLV")
				|| extension.equalsIgnoreCase("WEBM") || extension.equalsIgnoreCase("3GP")
				|| extension.equalsIgnoreCase("3GP2") || extension.equalsIgnoreCase("MPEG4")
				|| extension.equalsIgnoreCase("MPEG") || extension.equalsIgnoreCase("WMV")
				|| extension.equalsIgnoreCase("AVI")) {
			filePath += "/video";
			
			if (Integer.parseInt(type) == 0 || Integer.parseInt(type) == 1 || Integer.parseInt(type) == 2
					|| Integer.parseInt(type) == 3 || Integer.parseInt(type) == 4 || Integer.parseInt(type) == 5
					|| Integer.parseInt(type) == 6 || Integer.parseInt(type) == 7 || Integer.parseInt(type) == 10) {//7,10
				filePath += "/" + ServerUtil.datetoString();
				sfilePath += "/" + ServerUtil.datetoString();
				imgUrl += "/" + ServerUtil.datetoString();
			}
			outputFileName = "VDO";
		}else if (extension.equalsIgnoreCase("PDF")) {// || extension.equalsIgnoreCase("DOC")|| extension.equalsIgnoreCase("DOCX") || extension.equalsIgnoreCase("PPTX")
			filePath += "/pdf";
			sfilePath += "/pdf";
			imgUrl += "upload/pdf";
			if (Integer.parseInt(type) == 9) {
				filePath += "/law";
				sfilePath += "/law";
				imgUrl += "/law";
			} else if (Integer.parseInt(type) == 10) {
				filePath += "/form";
				sfilePath += "//form";
				imgUrl += "/form";
			} 
			if (Integer.parseInt(type) == 0 || Integer.parseInt(type) == 1 || Integer.parseInt(type) == 2
					|| Integer.parseInt(type) == 3 || Integer.parseInt(type) == 4 || Integer.parseInt(type) == 5
					|| Integer.parseInt(type) == 6 || Integer.parseInt(type) == 7 || Integer.parseInt(type) == 10) {
				filePath += "/" + ServerUtil.datetoString();
				sfilePath += "/" + ServerUtil.datetoString();
				imgUrl += "/" + ServerUtil.datetoString();
			}
			outputFileName = inputFileName;
		} /* else {
			filePath += "/other";
			outputFileName = "FMR";
		}*/
		File dirSm = new File(sfilePath);
		if (!dirSm.exists())
			dirSm.mkdirs();
		File dir = new File(filePath);
		if (!dir.exists())
			dir.mkdirs();
		String dtFormat = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		if(!extension.equalsIgnoreCase("PDF")){
		outputFileName += userid + dtFormat + "." + extension;
		}
		filePath += "/" + outputFileName;
		sfilePath += "/" + outputFileName;
		imgUrl += "/" + outputFileName;
		File l_file = new File(filePath);
		File s_file = new File(sfilePath);
		//String videoduration=VideoTime(filePath);
		try {
			@SuppressWarnings("unchecked")
			List<FileItem> items = upload.parseRequest(request);
			for (FileItem item : items) {
				if (item.isFormField()) {
				} else {
					if (l_file.exists()) {
						try {
							l_file.delete();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					if (s_file.exists()) {
						try {
							s_file.delete();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					if (l_file.createNewFile()) {
						item.write(l_file);
					}
					if (!extension.equalsIgnoreCase("MP4")) {
						if (!extension.equalsIgnoreCase("xls")) {
							if(!extension.equalsIgnoreCase("PDF")){
								if (s_file.createNewFile()) {
									item.write(s_file);
									// boolean res = resize(300, 300, sfilePath,
									// l_file);
									// boolean res =
									// createThumbnail(sfilePath,sfilePath);
									boolean res = createThumbnailMobileImage(sfilePath, sfilePath);
									// if (res == true) {
									result = "{\"code\":\"SUCCESS\",\"fileName\":\"" +ServerUtil.datetoString()+"/"+ outputFileName
											+ "\",\"sfileName\":\"" + outputFileName + "\",\"url\":\"" + imgUrl + "\"}";
									// }
								}
							}else {
								result = "{\"code\":\"SUCCESS\",\"fileName\":\"" + outputFileName
										+ "\",\"sfileName\":\"" + outputFileName + "\",\"url\":\"" + imgUrl + "\"}";
							}							
						} else {
							result = "{\"code\":\"SUCCESS\",\"fileName\":\"" + outputFileName + "\"}";
						}
					} else {
						result = "{\"code\":\"SUCCESS\",\"fileName\":\"" + ServerUtil.datetoString()+"/"+outputFileName + "\"}";
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		 if (extension.equalsIgnoreCase("MP4") || extension.equalsIgnoreCase("FLV")
					|| extension.equalsIgnoreCase("WEBM") || extension.equalsIgnoreCase("3GP")
					|| extension.equalsIgnoreCase("3GP2") || extension.equalsIgnoreCase("MPEG4")
					|| extension.equalsIgnoreCase("MPEG") || extension.equalsIgnoreCase("WMV")
					|| extension.equalsIgnoreCase("AVI")) {
			 //String videofile=VideoTime(imgUrl);
			//String videofile=VideoTime("E:/MobileBanking/DC/CONSOLE/DigitalConsole/WebContent/export/video/snowfall_winter_snowflakes_cold_907");
			//System.out.println("'" + videofile.substring(videofile.indexOf("app")).replace("\\", "/") + "',");
			}
		System.out.println(result+"Output Result");
		return result;
	}

	// upload image for mobile
	@POST
	@Path("/uploadImage")
	@Consumes(MediaType.MULTIPART_FORM_DATA + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON)
	public Resultb2b uploadImage() {
		getPath();
        DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		System.out.println("image Upload in cms: ");
		Resultb2b res = new Resultb2b();
		String inputFileName = "";
		String filePath = "upload";
		String sfilePath = "upload";
		String outputFileName = "";
		String userid = "";
		String mobile = "";
		String username = "";
		String postSys = "";
		String time = "";
		try {
			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);
			@SuppressWarnings("unchecked")
			List<FileItem> items = upload.parseRequest(request);
			int i = 0;
			for (FileItem item : items) {
				if (item.isFormField()) {
					if (i == 0) {
						inputFileName = item.getString();
					} else if (i == 1) {
						userid = item.getString();
					} else if (i == 2) {
						mobile = item.getString();
					} else if (i == 3) {
						username = item.getString();
					} else if (i == 4) {
						postSys = item.getString();
					} else if (i == 5) {
						time = item.getString();
					}
					i++;
				} else {
					if (inputFileName.contains("/")) {
						inputFileName = inputFileName.substring(inputFileName.lastIndexOf("/") + 1);
					}
					sfilePath = request.getServletContext().getRealPath("/") + "/" + filePath;
					filePath = request.getServletContext().getRealPath("/") + "/" + filePath;
					String extension = FilenameUtils.getExtension(inputFileName);
					if (extension.equalsIgnoreCase("JPEG") || extension.equalsIgnoreCase("JPG")
							|| extension.equalsIgnoreCase("PNG")) {
						filePath += "/image";
						sfilePath += "/smallImage";
						outputFileName = "IMG";
					} else if (extension.equalsIgnoreCase("MP4") || extension.equalsIgnoreCase("FLV")
							|| extension.equalsIgnoreCase("WEBM") || extension.equalsIgnoreCase("3GP")
							|| extension.equalsIgnoreCase("3GP2") || extension.equalsIgnoreCase("MPEG4")
							|| extension.equalsIgnoreCase("MPEG") || extension.equalsIgnoreCase("WMV")
							|| extension.equalsIgnoreCase("AVI")) {
						filePath += "/video";
						outputFileName = "VDO";
					}/* else {
						filePath += "/other";
						outputFileName = "FMR";
					}*/
					File dirSm = new File(sfilePath);
					if (!dirSm.exists())
						dirSm.mkdirs();
					File dir = new File(filePath);
					if (!dir.exists())
						dir.mkdirs();

					String dtFormat = new SimpleDateFormat("yyyyMMdd").format(new Date());
					outputFileName += userid + dtFormat + inputFileName;
					filePath += "/" + outputFileName;
					sfilePath += "/" + outputFileName;
					File l_file = new File(filePath);
					File s_file = new File(sfilePath);
					if (l_file.exists()) {
						try {
							l_file.delete();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					if (s_file.exists()) {
						try {
							s_file.delete();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					if (l_file.createNewFile()) {
						item.write(l_file);
					}
					if (s_file.createNewFile()) {
						item.write(s_file);
						// boolean ret = resize(300, 300, sfilePath, l_file);
						boolean ret = createThumbnail(sfilePath, sfilePath);
						// boolean ret =
						// createThumbnailMobileImage(sfilePath,sfilePath);
						if (ret == true) {
							String fileName = outputFileName;
							String sfileName = outputFileName;
							UploadData data = new UploadData();
							data.setUserId(mobile);
							data.setUserName(username);
							data.setT1(fileName);
							data.setT6(sfileName);
							data.setCreatedTime(time);
							data.setModifiedTime(time);
							data = QnAMgr.setUploadDataMobile(data, Long.parseLong(postSys), getUser());
							res = UploadDao.insertUpload(data, getUser());
						}
					}
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
			res.setState(false);
		} catch (Exception e) {
			e.printStackTrace();
			res.setState(false);
		}
		return res;
	}

	// android version using ionic
	@POST
	@Path("/mobileupload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.TEXT_PLAIN)
	public String mobileuploadFile(@FormDataParam("file") InputStream fis,
			@FormDataParam("file") FormDataContentDisposition fdcd) {
		getPath();
        DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		System.out.println("I m here=" + fdcd);
		String orgname = "upload/image/userProfile"; // request.getServletContext().getRealPath("/")
														// + "/"
		System.out.println("real path = " + request.getServletContext().getRealPath("/" + orgname));
		String desc = request.getParameter("desc");
		File theDir = new File(request.getServletContext().getRealPath("/" + orgname));
		if (!theDir.exists()) {
			try {
				boolean success = theDir.mkdir();
				System.out.println("folder=" + success);
			} catch (SecurityException se) {
				// handle it
				se.printStackTrace();
			}
		} else {
			System.out.println("real path1 = " + request.getServletContext().getRealPath("/" + orgname));
			File childDir = new File(request.getServletContext().getRealPath("/" + orgname));
			if (!childDir.exists()) {
				try {
					childDir.mkdir();
				} catch (SecurityException se) {
					// handle it
					se.printStackTrace();
				}
			}
		}
		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd_'at'_hh-mm-ss_a");
		String now = ft.format(dNow);
		OutputStream outpuStream = null;
		System.out.println("I m here" + fis.toString());
		String fileName = fdcd.getFileName();
		System.out.println("File Name: " + fileName);
		String basename = FilenameUtils.getBaseName(fileName);
		// basename = basename.replaceAll("\\s+","");
		String extension = FilenameUtils.getExtension(fileName);
		String fullname = basename + "." + extension;
		System.out.println(basename); // file
		System.out.println(extension);
		System.out.println(fullname);
		String filePath = request.getServletContext().getRealPath("/" + orgname + "/" + fullname);
		System.out.println("File Path3: " + filePath);
		try {
			int read = 0;
			byte[] bytes = new byte[1024];
			outpuStream = new FileOutputStream(new File(filePath));
			while ((read = fis.read(bytes)) != -1) {
				outpuStream.write(bytes, 0, read);
			}
			outpuStream.flush();
			outpuStream.close();
		} catch (IOException iox) {
			iox.printStackTrace();
		} finally {
			if (outpuStream != null) {
				try {
					outpuStream.close();
				} catch (Exception ex) {
				}
			}
		}
		return fullname;
	}

	public static boolean createThumbnail(String imageUrl, String targetPath) {
		final int imageSize = 200;
		File thumbnail = new File(targetPath);
		try {
			thumbnail.getParentFile().mkdirs();
			thumbnail.createNewFile();
			BufferedImage sourceImage = ImageIO.read(new File(imageUrl));
			float width = 50;
			float height = 50;
			BufferedImage img2;
			float scaledWidth = (width / height) * (float) imageSize;
			float scaledHeight = imageSize;
			BufferedImage img = new BufferedImage((int) scaledWidth, (int) scaledHeight, sourceImage.getType());
			Image scaledImage = sourceImage.getScaledInstance((int) scaledWidth, (int) scaledHeight,
					Image.SCALE_SMOOTH);
			img.createGraphics().drawImage(scaledImage, 0, 0, null);
			int offset = (int) ((scaledWidth - scaledHeight) / 2f);
			img2 = img.getSubimage(offset, 0, imageSize, imageSize);
			ImageIO.write(img2, "png", thumbnail);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean createThumbnailMobileImage(String imageUrl, String targetPath) {
		final int imageSize = 200;
		File thumbnail = new File(targetPath);
		try {
			thumbnail.getParentFile().mkdirs();
			thumbnail.createNewFile();
			BufferedImage sourceImage = ImageIO.read(new File(imageUrl));
			// float width = 50;
			// float height = 50;
			BufferedImage img2;
			// float scaledWidth = (width / height) * (float) imageSize;
			// float scaledHeight = imageSize;
			float origwidth = 0;
			float origheight = 0;
			origwidth = sourceImage.getWidth();
			origheight = sourceImage.getHeight();

			double scaledWidth = 0;
			double scaledHeight = 0;
			double ratio = 0;

			if (origwidth > origheight) {
				ratio = 1.0 * (origwidth / origheight);
				// System.out.println("Math.floor: " + Math.floor(ratio));
				// ratio = Math.floor(ratio);
			} else {
				ratio = 1.0 * (origheight / origwidth);
			}
			scaledWidth = origwidth;
			scaledHeight = origheight;

			while (scaledWidth > scaledHeight && scaledWidth > 990) {
				scaledWidth = scaledWidth / ratio;
				scaledHeight = scaledHeight / ratio;
			}

			while (scaledHeight > scaledWidth && scaledHeight > 990) {
				scaledWidth = scaledWidth / ratio;
				scaledHeight = scaledHeight / ratio;
			}

			if (scaledWidth == scaledHeight) {
				scaledWidth = 400;
				scaledHeight = 400;
			}

			BufferedImage img = new BufferedImage((int) scaledWidth, (int) scaledHeight, sourceImage.getType());
			Image scaledImage = sourceImage.getScaledInstance((int) scaledWidth, (int) scaledHeight,
					Image.SCALE_SMOOTH);
			img.createGraphics().drawImage(scaledImage, 0, 0, null);
			int offset = (int) ((scaledWidth - scaledHeight) / 2f);
			// img2 = img.getSubimage(offset, 0, imageSize, imageSize);
			int width = (int) scaledWidth;
			int height = (int) scaledHeight;
			// img2 = img.getSubimage(offset, 0, width, height);
			ImageIO.write(img, "png", thumbnail);
			return true;
		} catch (IOException e) {
			System.out.println("error: " + e);
			e.printStackTrace();
		}
		return false;
	}

	/*
	 * @GET
	 * 
	 * @Path("fileRemove")
	 * 
	 * @Produces(MediaType.APPLICATION_JSON) public FileUploadResponseData
	 * deleteFile(@QueryParam("fn") String fileName) { FileUploadResponseData
	 * result = new FileUploadResponseData(); String filePath =
	 * request.getServletContext().getRealPath("/") + "\\upload\\image"; String
	 * sfilePath = request.getServletContext().getRealPath("/") +
	 * "\\upload\\smallImage"; try{ File l_file = new File(filePath + "\\" +
	 * fileName); File s_file = new File(sfilePath + "\\" + fileName);
	 * if(l_file.delete()) { result.setCode("SUCCESS"); }
	 * if(result.getCode()=="SUCCESS"){ if(s_file.delete()) {
	 * result.setCode("SUCCESS"); } } } catch(Exception e) {
	 * e.printStackTrace(); } return result; }
	 */

	/* upload Profile for mobile */
	@POST
	@Path("/uploadProfile")
	@Consumes(MediaType.MULTIPART_FORM_DATA + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON)
	public RegisterData uploadProfile() {
		getPath();
        DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		Resultb2b res = new Resultb2b();
		RegisterData data = new RegisterData();
		String inputFileName = "";
		String filePath = "upload";
		String outputFileName = "";
		String userid = "";
		String mobile = "";
		String username = "";
		String time = "";
		try {
			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);
			@SuppressWarnings("unchecked")
			List<FileItem> items = upload.parseRequest(request);
			int i = 0;
			for (FileItem item : items) {
				if (item.isFormField()) {
					if (i == 0) {
						inputFileName = item.getString();
					} else if (i == 1) {
						userid = item.getString();
					} else if (i == 2) {
						mobile = item.getString();
					} else if (i == 3) {
						username = item.getString();
					} else if (i == 4) {
						time = item.getString();
					}
					i++;

				} else {
					if (inputFileName.contains("/")) {
						inputFileName = inputFileName.substring(inputFileName.lastIndexOf("/") + 1);
					}
					filePath = request.getServletContext().getRealPath("/") + "/" + filePath;
					String extension = FilenameUtils.getExtension(inputFileName);
					if (extension.equalsIgnoreCase("JPEG") || extension.equalsIgnoreCase("JPG")
							|| extension.equalsIgnoreCase("PNG")) {
						filePath += "/smallImage";
						outputFileName = "IMG";
					}
					File dir = new File(filePath);
					if (!dir.exists())
						dir.mkdirs();
					String dtFormat = new SimpleDateFormat("yyyyMMdd").format(new Date());
					outputFileName += userid + dtFormat + inputFileName;
					filePath += "/" + outputFileName;
					File l_file = new File(filePath);
					if (l_file.exists()) {
						try {
							l_file.delete();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					if (l_file.createNewFile()) {
						item.write(l_file);
						String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
						data.setUserid(mobile);
						data.setT1(mobile);
						data.setUsername(username);
						data.setT16(outputFileName);
						data.setRecordStatus(1);
						data.setSyncStatus(1);
						data.setSyncBatch(0);
						data.setModifieddate(todayDate);
						res = RegisterDao.UpdateImageUpload(data, getUser());
						if (res.isState()) {
							data = RegisterMgr.readByID(data.getT1(), getUser());
						}
					}
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
			res.setState(false);
		} catch (Exception e) {
			e.printStackTrace();
			res.setState(false);
		}
		return data;
	}

	@GET
	@Path("fileRemove")
	@Produces(MediaType.APPLICATION_JSON)
	public FileUploadResponseData deleteFile(@QueryParam("fn") String fileName, @QueryParam("type") String type) {
		getPath();
        DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		FileUploadResponseData result = new FileUploadResponseData();
		String filePath = request.getServletContext().getRealPath("/") + "\\upload\\image";
		String sfilePath = request.getServletContext().getRealPath("/") + "\\upload\\smallImage";
		if (type.equalsIgnoreCase("10")) {
			filePath += "\\WriterImage";
			sfilePath += "\\WriterImage";
		}
		try {
			File l_file = new File(filePath + "\\" + fileName);
			File s_file = new File(sfilePath + "\\" + fileName);
			if (l_file.exists()) {
				if (l_file.delete()) {
					result.setCode("SUCCESS");
				}
				if (s_file.delete()) {
					result.setCode("SUCCESS");
				}
			} else {
				if (QnAMgr.isUploadFileExit(fileName, getUser())) {
					result.setCode(QnAMgr.deleteCorrect(fileName, getUser()));
				}
			}
			/*
			 * if(result.getCode()=="SUCCESS"){ if(s_file.delete()) {
			 * result.setCode("SUCCESS"); } }
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}	

	@POST
	@Path("/mobileUploadImage")
	@Consumes(MediaType.MULTIPART_FORM_DATA + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON)
	public Resultb2b mobileUploadImage(@FormDataParam("file") InputStream fis,
			@FormDataParam("file") FormDataContentDisposition fdcd) {
		getPath();
        DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		Resultb2b res = new Resultb2b();
		String filePath = "upload";
		String outputFileName = "";

		filePath = request.getServletContext().getRealPath("/") + filePath;
		filePath += "/image/chatImage";
		File dir = new File(filePath);
		if (!dir.exists())
			dir.mkdirs();
		try {
			System.out.println("file write" + filePath);
			int read = 0;
			filePath += "/" + fdcd.getFileName();
			byte[] bytes = new byte[1024];
			FileOutputStream outpuStream = new FileOutputStream(new File(filePath));
			while ((read = fis.read(bytes)) != -1) {
				outpuStream.write(bytes, 0, read);
			}
			outpuStream.flush();
			outpuStream.close();
			res.setMsgCode(outputFileName);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return res;
	}

	@POST
	@Path("cmsUploadImage")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public String cmsUploadImage(@QueryParam("f") String filePath, @QueryParam("fn") String inputFileName,
			@QueryParam("id") String userid) throws AWTException {
		getPath();
        DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		// System.out.println("File Upload in cms: " );
		String result = "{\"code\":\"ERROR\"}";
		// String sfilePath = "";
		String outputFileName = "";
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		if (inputFileName.contains("/")) {
			inputFileName = inputFileName.substring(inputFileName.lastIndexOf("/") + 1);
		}
		// sfilePath = request.getServletContext().getRealPath("/") + "/" +
		// filePath;
		filePath = request.getServletContext().getRealPath("/") + "/" + filePath;
		String extension = FilenameUtils.getExtension(inputFileName);
		if (extension.equalsIgnoreCase("JPEG") || extension.equalsIgnoreCase("JPG")
				|| extension.equalsIgnoreCase("PNG")) {
			filePath += "/image/chatImage";
			// sfilePath += "/smallImage/chatImage";
			outputFileName = "IMG";
		} else if (extension.equalsIgnoreCase("MP4") || extension.equalsIgnoreCase("FLV")
				|| extension.equalsIgnoreCase("WEBM") || extension.equalsIgnoreCase("3GP")
				|| extension.equalsIgnoreCase("3GP2") || extension.equalsIgnoreCase("MPEG4")
				|| extension.equalsIgnoreCase("MPEG") || extension.equalsIgnoreCase("WMV")
				|| extension.equalsIgnoreCase("AVI")) {
			filePath += "/video";
			outputFileName = "VDO";
		} /*else {
			filePath += "/other";
			outputFileName = "FMR";
		}*/
		/*
		 * File dirSm = new File(sfilePath); if (!dirSm.exists())
		 * dirSm.mkdirs();
		 */
		File dir = new File(filePath);
		if (!dir.exists())
			dir.mkdirs();
		String dtFormat = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		outputFileName += userid + dtFormat + "." + extension;
		System.out.println(
				"output file name : " + outputFileName + "  / userid  " + userid + "  /  dtFormat  " + dtFormat);
		filePath += "/" + outputFileName;
		// sfilePath += "/" + outputFileName;
		File l_file = new File(filePath);
		// File s_file = new File(sfilePath);
		try {
			@SuppressWarnings("unchecked")
			List<FileItem> items = upload.parseRequest(request);
			for (FileItem item : items) {
				if (item.isFormField()) {
				} else {
					if (l_file.exists()) {
						try {
							l_file.delete();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					/*
					 * if (s_file.exists()) { try { s_file.delete(); } catch
					 * (Exception e) { e.printStackTrace(); } }
					 */
					if (l_file.createNewFile()) {
						item.write(l_file);
					}
					/*
					 * if (!extension.equalsIgnoreCase("MP4")) { if
					 * (!extension.equalsIgnoreCase("xls")) { if
					 * (s_file.createNewFile()) { item.write(s_file); //boolean
					 * res = resize(300, 300, sfilePath, l_file); //boolean res
					 * = createThumbnail(sfilePath,sfilePath); boolean res =
					 * createThumbnailMobileImage(sfilePath,sfilePath); if (res
					 * == true) { result =
					 * "{\"code\":\"SUCCESS\",\"fileName\":\"" + outputFileName
					 * + "\",\"sfileName\":\"" + outputFileName + "\"}"; } } }
					 * else { result = "{\"code\":\"SUCCESS\",\"fileName\":\"" +
					 * outputFileName + "\"}"; } } else { result =
					 * "{\"code\":\"SUCCESS\",\"fileName\":\"" + outputFileName
					 * + "\"}"; }
					 */
					result = "{\"code\":\"SUCCESS\",\"fileName\":\"" + outputFileName + "\"}";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// System.out.println("result: " + result);
		return result;
	}
	
	@POST
	@Path("fleUploadNew")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public String fleUploadNew(@QueryParam("f") String filePath, @QueryParam("fn") String inputFileName,
			@QueryParam("id") String userid, @QueryParam("type") String type, @QueryParam("imgUrl") String imgUrl)
			throws AWTException {
		// System.out.println("File Upload in cms: " );
		String result = "{\"code\":\"ERROR\"}";
		String sfilePath = "";
		String outputFileName = "";
		String fileName="";							
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		if (inputFileName.contains("/")) {
			inputFileName = inputFileName.substring(inputFileName.lastIndexOf("/") + 1);
		}
		sfilePath = request.getServletContext().getRealPath("/") + "/" + filePath;
		filePath = request.getServletContext().getRealPath("/") + "/" + filePath;	
		System.out.println(filePath+"Flie Path");
		if (filePath.contains("DigitalConnect")) {
			filePath = filePath.replace("DigitalConnect", "DigitalMedia");
		}
		if (sfilePath.contains("DigitalConnect")) {
			sfilePath = sfilePath.replace("DigitalConnect", "DigitalMedia");
		}
		if (filePath.contains("WalletService")) {
			filePath = filePath.replace("WalletService", "DigitalMedia");
		}
		if (sfilePath.contains("WalletService")) {
			sfilePath = sfilePath.replace("WalletService", "DigitalMedia");
		}
		String extension = FilenameUtils.getExtension(inputFileName);
		if (extension.equalsIgnoreCase("JPEG") || extension.equalsIgnoreCase("JPG")
				|| extension.equalsIgnoreCase("PNG")) {
			filePath += "/image";
			sfilePath += "/smallImage";
			imgUrl += "upload/smallImage";

			
			if (Integer.parseInt(type) == 9) {
				filePath += "/WriterImage";
				sfilePath += "/WriterImage";
				imgUrl += "/WriterImage";
			} else if (Integer.parseInt(type) == 10) {
				filePath += "/videoImage";
				sfilePath += "/videoImage";
				imgUrl += "/videoImage";
				if (extension.equalsIgnoreCase("JPEG") || extension.equalsIgnoreCase("JPG")
						|| extension.equalsIgnoreCase("PNG")) {
				if (Integer.parseInt(type) == 0 || Integer.parseInt(type) == 1 || Integer.parseInt(type) == 2
						|| Integer.parseInt(type) == 3 || Integer.parseInt(type) == 4 || Integer.parseInt(type) == 5
						|| Integer.parseInt(type) == 6 || Integer.parseInt(type) == 7 || Integer.parseInt(type) == 10 ) {//7,10
					filePath += "/" + ServerUtil.datetoString();
					sfilePath += "/" + ServerUtil.datetoString();
					imgUrl += "/" + ServerUtil.datetoString();
				}
				}
			} else {
				filePath += "/contentImage";
				sfilePath += "/contentImage";
				imgUrl += "/contentImage";
			}

			if (Integer.parseInt(type) == 0 || Integer.parseInt(type) == 1 || Integer.parseInt(type) == 2
					|| Integer.parseInt(type) == 3 || Integer.parseInt(type) == 4 || Integer.parseInt(type) == 5
					|| Integer.parseInt(type) == 6 || Integer.parseInt(type) == 7 ) {//7,10
				filePath += "/" + ServerUtil.datetoString();
				sfilePath += "/" + ServerUtil.datetoString();
				imgUrl += "/" + ServerUtil.datetoString();
			}
			outputFileName = "IMG";
		} else if (extension.equalsIgnoreCase("MP4") || extension.equalsIgnoreCase("FLV")
				|| extension.equalsIgnoreCase("WEBM") || extension.equalsIgnoreCase("3GP")
				|| extension.equalsIgnoreCase("3GP2") || extension.equalsIgnoreCase("MPEG4")
				|| extension.equalsIgnoreCase("MPEG") || extension.equalsIgnoreCase("WMV")
				|| extension.equalsIgnoreCase("AVI")) {
			filePath += "/video";
			
			if (Integer.parseInt(type) == 0 || Integer.parseInt(type) == 1 || Integer.parseInt(type) == 2
					|| Integer.parseInt(type) == 3 || Integer.parseInt(type) == 4 || Integer.parseInt(type) == 5
					|| Integer.parseInt(type) == 6 || Integer.parseInt(type) == 7 || Integer.parseInt(type) == 10) {//7,10
				filePath += "/" + ServerUtil.datetoString();
				sfilePath += "/" + ServerUtil.datetoString();
				imgUrl += "/" + ServerUtil.datetoString();
			}
			outputFileName = "VDO";
		}else if (extension.equalsIgnoreCase("PDF")) {// || extension.equalsIgnoreCase("DOC")|| extension.equalsIgnoreCase("DOCX") || extension.equalsIgnoreCase("PPTX")
			filePath += "/pdf";
			sfilePath += "/pdf";
			imgUrl += "upload/pdf";
			if (Integer.parseInt(type) == 9) {
				filePath += "/law";
				sfilePath += "/law";
				imgUrl += "/law";
			} else if (Integer.parseInt(type) == 10) {
				filePath += "/form";
				sfilePath += "//form";
				imgUrl += "/form";
			} 
			if (Integer.parseInt(type) == 0 || Integer.parseInt(type) == 1 || Integer.parseInt(type) == 2
					|| Integer.parseInt(type) == 3 || Integer.parseInt(type) == 4 || Integer.parseInt(type) == 5
					|| Integer.parseInt(type) == 6 || Integer.parseInt(type) == 7 || Integer.parseInt(type) == 10) {
				filePath += "/" + ServerUtil.datetoString();
				sfilePath += "/" + ServerUtil.datetoString();
				imgUrl += "/" + ServerUtil.datetoString();
			}
			outputFileName = inputFileName;
		}/*  else {
			filePath += "/other";
			outputFileName = "FMR";
		}*/
		File dirSm = new File(sfilePath);
		if (!dirSm.exists())
			dirSm.mkdirs();
		File dir = new File(filePath);
		if (!dir.exists())
			dir.mkdirs();
		String dtFormat = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		if(!extension.equalsIgnoreCase("PDF")){
		outputFileName += userid + dtFormat + "." + extension;
		}
		filePath += "/" + outputFileName;
		sfilePath += "/" + outputFileName;
		imgUrl += "/" + outputFileName;
		File l_file = new File(filePath);
		File s_file = new File(sfilePath);
		//String videoduration=VideoTime(filePath);
		try {
			@SuppressWarnings("unchecked")
			List<FileItem> items = upload.parseRequest(request);
			for (FileItem item : items) {
				if (item.isFormField()) {
				} else {
					if (l_file.exists()) {
						try {
							l_file.delete();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					if (s_file.exists()) {
						try {
							s_file.delete();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					if (l_file.createNewFile()) {
						item.write(l_file);
					}
					if (!extension.equalsIgnoreCase("MP4")) {
						if (!extension.equalsIgnoreCase("xls")) {
							if(!extension.equalsIgnoreCase("PDF")){
								if (s_file.createNewFile()) {
									item.write(s_file);
									boolean res = createThumbnailMobileImage(sfilePath, sfilePath);
									result = "{\"code\":\"SUCCESS\",\"fileName\":\"" +ServerUtil.datetoString()+"/"+ outputFileName
											+ "\",\"sfileName\":\"" + outputFileName + "\",\"url\":\"" + imgUrl + "\"}";
								}
							}else {
								result = "{\"code\":\"SUCCESS\",\"fileName\":\"" + outputFileName
										+ "\",\"sfileName\":\"" + outputFileName + "\",\"url\":\"" + imgUrl + "\"}";
							}							
						} else {
							result = "{\"code\":\"SUCCESS\",\"fileName\":\"" + outputFileName + "\"}";
						}
					} else {
						result = "{\"code\":\"SUCCESS\",\"fileName\":\"" + ServerUtil.datetoString()+"/"+outputFileName + "\"}";
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		 if (extension.equalsIgnoreCase("MP4") || extension.equalsIgnoreCase("FLV")
					|| extension.equalsIgnoreCase("WEBM") || extension.equalsIgnoreCase("3GP")
					|| extension.equalsIgnoreCase("3GP2") || extension.equalsIgnoreCase("MPEG4")
					|| extension.equalsIgnoreCase("MPEG") || extension.equalsIgnoreCase("WMV")
					|| extension.equalsIgnoreCase("AVI")) {
			}
		System.out.println(result+"Output Result");
		return result;
	}
	@GET
	@Path("fileRemoveContent")
	@Produces(MediaType.APPLICATION_JSON)
	public FileUploadResponseData deleteFileContent(@QueryParam("fn") String fileName, @QueryParam("url") String url) {
		FileUploadResponseData result = new FileUploadResponseData();
		String sfilePath="",filePath="";
		//int tdyDate = url.lastIndexOf("/");
		//String pathName = url.substring(tdyDate - 8, tdyDate);
		//String filePath = request.getServletContext().getRealPath("/") + "\\upload\\image\\contentImage\\" + pathName
		//		+ "\\" + fileName;
		//String sfilePath = request.getServletContext().getRealPath("/") + "\\upload\\smallImage\\contentImage\\"
		//		+ pathName + "\\" + fileName;
		
		sfilePath = request.getServletContext().getRealPath("/") + "/"  + "\\upload\\image\\contentImage\\" + fileName;
		filePath = request.getServletContext().getRealPath("/") + "/" + "\\upload\\image\\contentImage\\" + fileName;	
		
		if (filePath.contains("DigitalConnect")) {
			filePath = filePath.replace("DigitalConnect", "DigitalMedia");
		}
		if (sfilePath.contains("DigitalConnect")) {
			sfilePath = sfilePath.replace("DigitalConnect", "DigitalMedia");
		}
		if (filePath.contains("WalletService")) {
			filePath = filePath.replace("WalletService", "DigitalMedia");
		}
		if (sfilePath.contains("WalletService")) {
			sfilePath = sfilePath.replace("WalletService", "DigitalMedia");
		}
		System.out.println(filePath + "Flie Path");
		
		try {
			File l_file = new File(filePath);
			File s_file = new File(sfilePath);
			if (l_file.exists()) {
				if (l_file.delete()) {
					result.setCode("SUCCESS");
				}
				if (s_file.delete()) {
					result.setCode("SUCCESS");
				}
			} else {
				if (QnAMgr.isUploadFileExit(fileName, getUser())) {
					result.setCode(QnAMgr.deleteCorrect(fileName, getUser()));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
