
package com.nirvasoft.rp.service;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.data.MobileuserListingDataList;
import com.nirvasoft.rp.data.TicketArr1;
import com.nirvasoft.rp.data.TicketData1;
import com.nirvasoft.rp.data.TicketListingDataList;
import com.nirvasoft.rp.data.TicketRequest;
import com.nirvasoft.rp.data.TicketResponse;
import com.nirvasoft.rp.framework.Lov3;
import com.nirvasoft.rp.framework.ServerSession;
import com.nirvasoft.rp.mgr.MobileuserAdmMgr;
import com.nirvasoft.rp.mgr.SessionMgr;
import com.nirvasoft.rp.mgr.TicketAdmMgr;
import com.nirvasoft.rp.shared.FilterDataset;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.shared.SessionData;

@Path("/serviceTicketAdm")
public class ServiceTicketAdm {
	@Context
	HttpServletRequest request;

	@POST
	@Path("deleteTicket")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TicketData1 deleteTicket(TicketResponse request) {
		TicketData1 response = null;

		getPath();
		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime_Old(request.getSessionID(), request.getUserID());

		if (sessionState.getCode().equals("0000")) {
			response = new TicketAdmMgr().deleteTicket(request);
		} else {
			response = new TicketData1();
			response.setCode(sessionState.getCode());
			response.setDesc(sessionState.getDesc());
			response.setState(false);
		}

		return response;
	}

	// acmy
	@POST
	@Path("getAllTicket")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TicketArr1 getAllTicket(TicketArr1 tdata, @QueryParam("ticketRegion") String ticketRegion) {
		TicketArr1 response = new TicketArr1();

		getPath();
		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime_Old(tdata.getSessionID(), tdata.getUserID());

		if (sessionState.getCode().equals("0000")) {
			response = new TicketAdmMgr().getAllTicket(tdata, ticketRegion);
		} else {
			response.setMsgCode(sessionState.getCode());
			response.setMsgDesc(sessionState.getDesc());
		}

		return response;
	}

	// atn
	// @POST
	// @Path("getTickets")
	// @Produces(MediaType.APPLICATION_JSON)
	// @Consumes(MediaType.APPLICATION_JSON)
	// public TicketArr1 getTickets(TicketArr1 tdata,
	// @QueryParam("ticketStatus") String tstatus,
	// @QueryParam("ticketRegion") String ticketRegion) {
	// TicketArr1 response = new TicketArr1();
	//
	// getPath();
	// DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") +
	// "/";
	// ResponseData sessionState = new ResponseData();
	// sessionState = new SessionMgr().updateActivityTime(tdata.getSessionID(),
	// tdata.getUserID());
	//
	// if (sessionState.getCode().equals("0000")) {
	// response = new TicketAdmMgr().getTickets(tdata, tstatus, ticketRegion);
	// } else {
	// response.setMsgCode(sessionState.getCode());
	// response.setMsgDesc(sessionState.getDesc());
	// }
	//
	// return response;
	// }

	@POST
	@Path("getAllTicketPOC")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TicketArr1 getAllTickets(TicketArr1 tdata, @QueryParam("ticketChannel") String ticketChannel) {
		TicketArr1 response = new TicketArr1();

		getPath();
		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime_Old(tdata.getSessionID(), tdata.getUserID());

		if (sessionState.getCode().equals("0000")) {
			response = new TicketAdmMgr().getAllTickets(tdata, ticketChannel);
		} else {
			response.setMsgCode(sessionState.getCode());
			response.setMsgDesc(sessionState.getDesc());
		}

		return response;
	}

	@POST
	@Path("getAllUsertypes")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TicketArr1 getAllUsertypes(TicketArr1 tdata, @QueryParam("ticketUsertype") String ticketUsertype) {
		TicketArr1 response = new TicketArr1();

		getPath();
		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime_Old(tdata.getSessionID(), tdata.getUserID());

		if (sessionState.getCode().equals("0000")) {
			response = new TicketAdmMgr().getAllUsertypes(tdata, ticketUsertype);
		} else {
			response.setMsgCode(sessionState.getCode());
			response.setMsgDesc(sessionState.getDesc());
		}

		return response;
	}

	// for mobileUser
	@POST
	@Path("getMobileuser")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public MobileuserListingDataList getMobileuser(FilterDataset p) {
		getPath();
		MobileuserListingDataList res = new MobileuserListingDataList();
		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime(p.getSessionID(), p.getUserID());

		if (sessionState.getCode().equals("0000")) {
			res = new MobileuserAdmMgr().getMobileuser(p);
		}else{
			res.setMsgCode(sessionState.getCode());
			res.setMsgDesc(sessionState.getDesc());
		}
		
		return res;
	}

	@POST
	@Path("getOneTicket")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TicketData1 getOneTicket(TicketRequest request) {
		TicketData1 response = null;

		getPath();
		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime_Old(request.getSessionID(), request.getUserID());

		if (sessionState.getCode().equals("0000")) {
			response = new TicketAdmMgr().getOneTicket(request.getTicketNo());
		} else {
			response = new TicketData1();
			response.setCode(sessionState.getCode());
			response.setDesc(sessionState.getDesc());
		}

		return response;
	}

	private void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
	}

	@POST
	@Path("getPOCTickets")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TicketListingDataList getPOCTickets(FilterDataset aFilterDataset) {
		TicketListingDataList response = new TicketListingDataList();

		getPath();

		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime_Old(aFilterDataset.getSessionID(), aFilterDataset.getUserID());

		if (sessionState.getCode().equals("0000")) {
			response = new TicketAdmMgr().getPOCTickets(aFilterDataset);
		} else {
			response.setMsgCode(sessionState.getCode());
			response.setMsgDesc(sessionState.getDesc());
		}

		return response;
	}

	@POST
	@Path("getTickets")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TicketListingDataList getTickets(FilterDataset aFilterDataset) {
		TicketListingDataList response = new TicketListingDataList();

		getPath();

		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime_Old(aFilterDataset.getSessionID(), aFilterDataset.getUserID());

		if (sessionState.getCode().equals("0000")) {
			response = new TicketAdmMgr().getTickets(aFilterDataset);
		} else {
			response.setMsgCode(sessionState.getCode());
			response.setMsgDesc(sessionState.getDesc());
		}

		return response;
	}

	@POST
	@Path("getTicketStatus")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Lov3 getTicketStatus(SessionData data) {
		Lov3 response = null;

		getPath();

		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime_Old(data.getSessionID(), data.getUserID());

		if (sessionState.getCode().equals("0000")) {
			response = new TicketAdmMgr().getTicketStatus();
		} else {
			response = new Lov3();
			response.setMsgCode(sessionState.getCode());
			response.setMsgDesc(sessionState.getDesc());
		}

		return response;
	}

	@POST
	@Path("saveTicket")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TicketResponse saveTicket(TicketResponse request) {
		TicketResponse response = null;

		getPath();
		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime_Old(request.getSessionID(), request.getUserID());

		if (sessionState.getCode().equals("0000")) {
			response = new TicketAdmMgr().saveTicket(request);
		} else {
			response = new TicketResponse();
			response.setMsgCode(sessionState.getCode());
			response.setMsgDesc(sessionState.getDesc());
			response.setState(false);
		}

		return response;
	}

	// atn
	@POST
	@Path("updateMyTicket")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TicketData1 updateMyTicket(TicketData1 request) {
		TicketData1 response = null;
		getPath();
		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime_Old(request.getSessionID(), request.getUserID());
		if (sessionState.getCode().equals("0000")) {
			response = new TicketAdmMgr().updateMyTicket(request);
		} else {
			response = new TicketData1();
			response.setCode(sessionState.getCode());
			response.setDesc(sessionState.getDesc());
			response.setState(false);
		}

		return response;
	}

	@POST
	@Path("updateTicket")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TicketResponse updateTicket(TicketResponse request) {
		TicketResponse response = null;

		getPath();
		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime_Old(request.getSessionID(), request.getUserID());

		if (sessionState.getCode().equals("0000")) {
			response = new TicketAdmMgr().updateTicket(request);
		} else {
			response = new TicketResponse();
			response.setMsgCode(sessionState.getCode());
			response.setMsgDesc(sessionState.getDesc());
			response.setState(false);
		}

		return response;
	}
}
