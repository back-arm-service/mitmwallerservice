
package com.nirvasoft.rp.service;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Result;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.mgr.CommRateCMSMgr;
import com.nirvasoft.cms.mgr.FAQMgr;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.shared.CommRateHeaderData;
import com.nirvasoft.cms.shared.DivisionComboDataSet;
import com.nirvasoft.cms.shared.DocumentData;
import com.nirvasoft.cms.shared.FAQData;
import com.nirvasoft.cms.shared.FAQListingData;
import com.nirvasoft.cms.shared.MCommRateMappingData;
import com.nirvasoft.cms.shared.MCommRateMappingMappingDataSet;
import com.nirvasoft.cms.util.FontChangeUtil;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.data.BranchCodeData;
import com.nirvasoft.rp.data.BranchData;
import com.nirvasoft.rp.data.CityStateData;
import com.nirvasoft.rp.data.ContantArr;
import com.nirvasoft.rp.data.ContentListingDataList;
import com.nirvasoft.rp.framework.FAQDataset;
import com.nirvasoft.rp.framework.Lov3;
import com.nirvasoft.rp.framework.Ref;
import com.nirvasoft.rp.framework.ServerSession;
import com.nirvasoft.rp.mgr.ATMLocationMgr;
import com.nirvasoft.rp.mgr.BranchMgr;
import com.nirvasoft.rp.mgr.ChatAdmMgr;
import com.nirvasoft.rp.mgr.ContentMgr;
import com.nirvasoft.rp.mgr.DocMgr;
import com.nirvasoft.rp.mgr.MerchantMgr;
import com.nirvasoft.rp.mgr.MobileuserAdmMgr;
import com.nirvasoft.rp.mgr.RegionMgr;
import com.nirvasoft.rp.mgr.SessionMgr;
import com.nirvasoft.rp.mgr.UserDataMgr;
import com.nirvasoft.rp.mgr.integration.SMSSettingMgr;
import com.nirvasoft.rp.shared.ATMLocation;
import com.nirvasoft.rp.shared.CMSMerchantAccJunctionArr;
import com.nirvasoft.rp.shared.CMSMerchantData;
import com.nirvasoft.rp.shared.CMSMerchantDataSet;
import com.nirvasoft.rp.shared.ChannelDataset;
import com.nirvasoft.rp.shared.ComboDataset;
import com.nirvasoft.rp.shared.DispaymentList;
import com.nirvasoft.rp.shared.DocumentDataset;
import com.nirvasoft.rp.shared.DocumentListingDataList;
import com.nirvasoft.rp.shared.FilterDataset;
import com.nirvasoft.rp.shared.MerchantData;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.shared.SessionData;
import com.nirvasoft.rp.shared.mobileuserobj;
import com.nirvasoft.rp.shared.integration.SMSSettingData;
import com.nirvasoft.rp.shared.integration.SMSSettingDataSet;

@Path("/serviceCMS")
public class ServiceCMS {
	public static String userid = "";
	@Context
	HttpServletRequest request;
	@Context
	private HttpServletResponse response;

	@javax.ws.rs.core.Context
	ServletContext context;

	// for mobileUser
	@POST
	@Path("approvedbyuser")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Result approvedbyuser(mobileuserobj data) {
		getPath();
		Result res = new Result();
		SessionMgr s_mgr = new SessionMgr();
		ResponseData response = new ResponseData();
		response = s_mgr.updateActivityTime_Old(data.getSessionID(), data.getUserID());
		if (response.getCode().equals("0000")) {
			res = new MobileuserAdmMgr().approvedbyuser(data.getSyskey(), data.getUserstatus());
		} else {
			res.setMsgCode(response.getCode());
			res.setMsgDesc(response.getDesc());
		}
		return res;
	}

	/* atn */
	@POST
	@Path("deleteDoc")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Resultb2b deleteDoc(DocumentData p) {
		getPath();
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		Resultb2b res = new Resultb2b();
		res = new DocMgr().deleteDoc(p);
		return res;
	}

	/* deleteFAQ */
	@POST
	@Path("deleteFAQ")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public FAQDataset deleteFAQ(FAQData p) {
		getPath();
		ResponseData response = new ResponseData();
		response = new SessionMgr().updateActivityTime(p.getSessionID(), p.getUserID());

		FAQDataset res = new FAQDataset();
		if (response.getCode().equals("0000")) {
			res = FAQMgr.deleteFAQ(p.getSyskey());
		} else {
			res.setMsgCode(response.getCode());
			res.setMsgDesc(response.getDesc());
			res.setState(false);
		}
		return res;
	}

	@POST
	@Path("deleteFlexCommRate")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON)
	public Result deleteFlexCommRate(CommRateHeaderData data) {
		Result res = new Result();
		CommRateCMSMgr mgr = new CommRateCMSMgr();
		getPath();
		SessionMgr s_mgr = new SessionMgr();
		ResponseData response = new ResponseData();
		response = s_mgr.updateActivityTime(data.getSessionID(), data.getCreatedUserID());
		if (response.getCode().equals("0000")) {
			res = mgr.deleteFlexCommRate(data.getCommRef());
		} else {
			res.setMsgCode(response.getCode());
			res.setMsgDesc(response.getDesc());
		}
		return res;
	}

	@POST
	@Path("deleteMerchant")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON)
	public Result deleteMerchant(CMSMerchantData data) {
		getPath();
		ResponseData response = new ResponseData();
		Result res = new Result();

		response = new SessionMgr().updateActivityTime(data.getSessionID(), data.getCreatedUserID());
		if (response.getCode().equals("0000")) {
			res = new MerchantMgr().deleteCMSMerchantData(data);
		} else {
			res.setMsgCode(response.getCode());
			res.setMsgDesc(response.getDesc());
		}
		return res;
	}

	@POST
	@Path("deleteMerchantCommRateMapping")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON)
	public Result deleteMerchantCommRateMapping(MCommRateMappingData data) {
		getPath();
		Result res = new Result();
		CommRateCMSMgr mgr = new CommRateCMSMgr();
		getPath();
		SessionMgr s_mgr = new SessionMgr();
		ResponseData response = new ResponseData();
		response = s_mgr.updateActivityTime_Old(data.getSessionID(), data.getUserID());
		if (response.getCode().equals("0000")) {
			res = mgr.deleteMerchantCommRateMapping(data.getMerchantID(), data.getN1(), data.getT1());
		} else {
			res.setMsgCode(response.getCode());
			res.setMsgDesc(response.getDesc());
		}
		return res;
	}

	@POST
	@Path("deleteSMSSetting")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON)
	public Result deleteSMSSetting(SMSSettingData data) {
		Result res = new Result();
		getPath();
		SessionMgr s_mgr = new SessionMgr();
		ResponseData response = new ResponseData();
		response = s_mgr.updateActivityTime(data.getSessionID(), data.getUserID());
		if (response.getCode().equals("0000")) {
			res = new SMSSettingMgr().deleteSMSSetting(data.getId());
		} else {
			res.setMsgCode(response.getCode());
			res.setMsgDesc(response.getDesc());
		}

		return res;

	}
	// SMS Setting End

	/* atn */
	@GET
	@Path("DocBysyskey")
	@Produces(MediaType.APPLICATION_JSON)
	public DocumentData DocBysyskey() {
		getPath();
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		DocumentData ret;
		String key = request.getParameter("key");
		ret = new DocMgr().DocBysyskey(Long.parseLong(key));
		return ret;
	}

	/* atn */
	/*
	 * @POST
	 * 
	 * @Path("searchDocumentList")
	 * 
	 * @Produces(MediaType.APPLICATION_JSON)
	 * 
	 * @Consumes(MediaType.APPLICATION_JSON) public DocumentDataSet
	 * searchDocumentList(PagerData p, @QueryParam("userID") String userid,
	 * 
	 * @QueryParam("statustype") String statustype) { getPath();
	 * DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") +
	 * "/"; // long status = Long.parseLong(request.getParameter("status"));//
	 * for // save if (p.getT1() != "" && p.getT1().contains("/")) {
	 * p.setT1(ServerUtil.datetoString1(p.getT1()));// SearchVal==p.getT1 }
	 * DocumentDataSet res = new DocumentDataSet(); res = new
	 * DocMgr().searchDocumentList(p, userid, statustype, getUser()); return
	 * res; }
	 */

	// BranchCode data combo
	@GET
	@Path("getAllBranch")
	@Produces(MediaType.APPLICATION_JSON)
	public Lov3 getAllBranch() {
		ArrayList<BranchData> res = new ArrayList<BranchData>();
		Lov3 lov = new Lov3();
		getPath();
		res = new BranchMgr().getAllBranch();
		Ref brancharr[] = new Ref[res.size()];

		for (int i = 0; i < res.size(); i++) {
			Ref ref = new Ref();
			ref.setvalue(res.get(i).getBranchCode());
			ref.setcaption(res.get(i).getBranchCode());
			brancharr[i] = ref;
		}

		lov.setRef018(brancharr);
		return lov;
	}

	@GET
	@Path("getATMLocatorByID")
	@Produces(MediaType.APPLICATION_JSON)
	public ATMLocation getATMLocatorByID() {

		ATMLocation res = new ATMLocation();
		ATMLocationMgr mgr = new ATMLocationMgr();
		String id = request.getParameter("id");
		getPath();
		String sessionId = request.getParameter("sessionID");
		String userID = request.getParameter("userID");
		SessionMgr s_mgr = new SessionMgr();
		ResponseData response = new ResponseData();
		response = s_mgr.updateActivityTime_Old(sessionId, userID);
		if (response.getCode().equals("0000")) {
			res = mgr.getATMLocatorByID(id);
		} else {
			res.setMsgCode(response.getCode());
			res.setMsgDesc(response.getDesc());
		}

		return res;

	}

	@GET
	@Path("getBrCode")
	@Produces(MediaType.APPLICATION_JSON)
	public BranchCodeData getBrCode() {
		BranchCodeData res = new BranchCodeData();
		getPath();
		ResponseData sessionState = new ResponseData();
		String sessionID = request.getParameter("sessionID");
		String userID = request.getParameter("userID");
		sessionState = new SessionMgr().updateActivityTime_Old(sessionID, userID);
		if (sessionState.getCode().equals("0000")) {
		res = new CommRateCMSMgr().getBrCode();
		} else
			res = null;
		return res;
	}

	// channel
	@POST
	@Path("getChannelList")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ChannelDataset getChannelList(FilterDataset p) {

		getPath();
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		ChannelDataset arr = new ChannelDataset();
		ResponseData sessionRes = new ResponseData();
		SessionMgr session_mgr = new SessionMgr();
		arr = new ChatAdmMgr().getChannelList(p);

		return arr;
	}

	// chat
	@POST
	@Path("getChatContact")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ContantArr getChatContact(SessionData data, @QueryParam("channelkey") String channelkey) {

		getPath();
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		ContantArr arr = new ContantArr();
		ResponseData sessionRes = new ResponseData();
		SessionMgr session_mgr = new SessionMgr();
		sessionRes = session_mgr.updateActivityTime_Old(data.getSessionID(), data.getUserID());
		if (sessionRes.getCode().equalsIgnoreCase("0000")) {
			arr = new ChatAdmMgr().getChatContact(channelkey);
		} else {
			arr.setMsgcode(sessionRes.getCode());
			arr.setMsgdesc(sessionRes.getDesc());
		}
		return arr;
	}

	/*@POST
	@Path("getDocumentlist")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DocumentListingDataList getDocumentlist(FilterDataset p) {
		this.getPath();
		DocumentListingDataList res = new DocumentListingDataList();
		SessionMgr s_mgr = new SessionMgr();
		ResponseData response = new ResponseData();
		response = s_mgr.updateActivityTime(p.getSessionID(), p.getUserID());
		if (response.getCode().equals("0000")) {
			res = new DocMgr().getDocumentlist(p);
		}else{
			res.setMsgCode(response.getCode());
			res.setMsgDesc(response.getDesc());
		}		
		return res;
	}*/

	@GET
	@Path("getFAQbysyskey")
	@Produces(MediaType.APPLICATION_JSON)
	public FAQData getFAQbysyskey() {
		getPath();
		FAQData ret = new FAQData();
		String key = request.getParameter("id");
		String sessionId = request.getParameter("sessionID");
		String userID = request.getParameter("userID");
		SessionMgr s_mgr = new SessionMgr();
		ResponseData response = new ResponseData();
		response = s_mgr.updateActivityTime(sessionId, userID);
		if (response.getCode().equals("0000")) {
			ret = new FAQMgr().getFAQbysyskey(Long.parseLong(key));
		} else {
			ret.setMsgCode(response.getCode());
			ret.setMsgDesc(response.getDesc());
		}

		return ret;
	}

	/* FAQmenu ListData */
	@POST
	@Path("getFAQList")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public FAQListingData getFAQList(FilterDataset p) {
		getPath();
		ResponseData response = new ResponseData();
		response = new SessionMgr().updateActivityTime(p.getSessionID(), p.getUserID());

		FAQListingData res = new FAQListingData();
		if (response.getCode().equals("0000")) {
			res = FAQMgr.getFAQList(p);
		} else {
			res.setMsgCode(response.getCode());
			res.setMsgDesc(response.getDesc());
		}

		return res;
	}

	@GET
	@Path("getFlexCommRateDataByID")
	@Produces(MediaType.APPLICATION_JSON)
	public CommRateHeaderData getFlexCommRateDataByID() {

		CommRateHeaderData res = new CommRateHeaderData();
		CommRateCMSMgr Mgr = new CommRateCMSMgr();
		String id = request.getParameter("id");
		String sessionId = request.getParameter("sessionID");
		String userID = request.getParameter("userID");
		getPath();

		SessionMgr s_mgr = new SessionMgr();
		ResponseData response = new ResponseData();
		response = s_mgr.updateActivityTime(sessionId, userID);
		if (response.getCode().equals("0000")) {
			res = Mgr.getFlexCommRateDataByID(id);
		} else {
			res.setMsgCode(response.getCode());
			res.setMsgDesc(response.getDesc());
		}
		return res;
	}

	@GET
	@Path("getFlexCommRateList")
	@Produces(MediaType.APPLICATION_JSON)
	public CommRateHeaderData getFlexCommRateList(@QueryParam("searchVal") String searchText,
			@QueryParam("pagesize") int pageSize, @QueryParam("currentpage") int currentPage,
			@QueryParam("sessionID") String sessionID, @QueryParam("userID") String userID) {

		CommRateHeaderData res = new CommRateHeaderData();
		CommRateCMSMgr u_mgr = new CommRateCMSMgr();
		getPath();

		SessionMgr s_mgr = new SessionMgr();
		ResponseData response = new ResponseData();
		response = s_mgr.updateActivityTime(sessionID, userID);
		if (response.getCode().equals("0000")) {
			res = u_mgr.getFlexCommRateList(searchText, pageSize, currentPage);
		} else {
			res.setMsgCode(response.getCode());
			res.setMsgDesc(response.getDesc());
		}
		return res;
	}

	// Merchant start
	@GET
	@Path("getMerchantCommData")
	@Produces(MediaType.APPLICATION_JSON)
	public MCommRateMappingData getMerchantCommData() {
		getPath();
		MCommRateMappingData res = new MCommRateMappingData();
		CommRateCMSMgr mgr = new CommRateCMSMgr();
		String id = request.getParameter("syskey");
		getPath();
		String sessionId = request.getParameter("sessionID");
		String userID = request.getParameter("userID");
		SessionMgr s_mgr = new SessionMgr();
		ResponseData response = new ResponseData();
		response = s_mgr.updateActivityTime_Old(sessionId, userID);
		if (response.getCode().equals("0000")) {
			res = mgr.getMerchantCommData(id);
		} else {
			res.setCode(response.getCode());
			res.setDesc(response.getDesc());
		}

		return res;

	}

	@GET
	@Path("getMerchantCommRateMappingList")
	@Produces(MediaType.APPLICATION_JSON)
	public MCommRateMappingMappingDataSet getMerchantCommRateMappingList(@QueryParam("searchVal") String searchText,
			@QueryParam("pagesize") int pageSize, @QueryParam("currentpage") int currentPage,
			@QueryParam("sessionID") String sessionID, @QueryParam("userID") String userID) {

		MCommRateMappingMappingDataSet res = new MCommRateMappingMappingDataSet();
		CommRateCMSMgr Mgr = new CommRateCMSMgr();
		getPath();
		SessionMgr s_mgr = new SessionMgr();
		ResponseData response = new ResponseData();
		response = s_mgr.updateActivityTime(sessionID, userID);
		if (response.getCode().equals("0000")) {
			res = Mgr.getMerchantCommRateMappingList(searchText, pageSize, currentPage);
		} else {
			res.setMsgCode(response.getCode());
			res.setMsgDesc(response.getDesc());
		}

		return res;
	}
	// Merchant end

	// Merchant start
	@GET
	@Path("getMerchantDataByID")
	@Produces(MediaType.APPLICATION_JSON)
	public CMSMerchantData getMerchantDataByID() {

		String sessionID = request.getParameter("sessionID");
		String userID = request.getParameter("userID");
		getPath();
		ResponseData response = new ResponseData();
		CMSMerchantData res = new CMSMerchantData();

		response = new SessionMgr().updateActivityTime(sessionID, userID);
		if (response.getCode().equals("0000")) {
			String id = request.getParameter("id");
			res = new MerchantMgr().getMerchantDataByID(id);
		} else {
			res.setMsgCode(response.getCode());
			res.setMsgDesc(response.getDesc());
		}
		return res;
	}

	private void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
	}

	// SMS Setting start
	@GET
	@Path("getSMSSettingDataById")
	@Produces(MediaType.APPLICATION_JSON)
	public SMSSettingData getSMSSettingDataById() {

		SMSSettingData res = new SMSSettingData();
		SMSSettingMgr mgr = new SMSSettingMgr();
		String id = request.getParameter("id");
		getPath();
		String sessionId = request.getParameter("sessionID");
		String userID = request.getParameter("userID");
		SessionMgr s_mgr = new SessionMgr();
		ResponseData response = new ResponseData();
		response = s_mgr.updateActivityTime(sessionId, userID);
		if (response.getCode().equals("0000")) {
			res = mgr.getSMSSettingDataById(Long.parseLong(id));
		} else {
			res.setCode(response.getCode());
			res.setDesc(response.getDesc());
		}

		return res;

	}

	@GET
	@Path("getSMSSettingList")
	@Produces(MediaType.APPLICATION_JSON)
	public SMSSettingDataSet getSMSSettingList(@QueryParam("searchVal") String searchText,
			@QueryParam("pagesize") int pageSize, @QueryParam("currentpage") int currentPage,
			@QueryParam("sessionID") String sessionID, @QueryParam("userID") String userID) {

		SMSSettingDataSet res = new SMSSettingDataSet();
		SMSSettingMgr Mgr = new SMSSettingMgr();
		getPath();
		SessionMgr s_mgr = new SessionMgr();
		ResponseData response = new ResponseData();
		response = s_mgr.updateActivityTime(sessionID, userID);
		if (response.getCode().equals("0000")) {
			res = Mgr.getSMSSettingList(searchText, pageSize, currentPage);
		} else {
			res.setMsgCode(response.getCode());
			res.setMsgDesc(response.getDesc());
		}

		return res;
	}

	@GET
	@Path("getStateListByRegion")
	@Produces(MediaType.APPLICATION_JSON)
	public ComboDataset getStateListByRegion() {
		DAOManager.AbsolutePath = context.getRealPath("");
		ComboDataset res = new ComboDataset();
		String regionCode = request.getParameter("regionCode");
		res = RegionMgr.getStateListByRegion(regionCode);
		return res;
	}

	// atn
	/*@GET
	@Path("getStateListByUserID")
	@Produces(MediaType.APPLICATION_JSON)
	public Lov3 getStateListByUserID(@QueryParam("userID") String userID, @QueryParam("type") String type) {
		ArrayList<CityStateData> res = new ArrayList<CityStateData>();
		Lov3 lov = new Lov3();
		getPath();
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		res = new UserDataMgr().getStateListByUserID(userID, type);
		Ref statearr[] = new Ref[res.size()];
		for (int i = 0; i < res.size(); i++) {
			Ref ref = new Ref();
			ref.setvalue(res.get(i).getCode());
			ref.setcaption(res.get(i).getDespEng());
			statearr[i] = ref;
		}
		lov.setRefstate(statearr);
		return lov;
	}*/

	/* getStatusList */
	@GET
	@Path("getStatusList")
	@Produces(MediaType.APPLICATION_JSON)
	public DivisionComboDataSet getStatusList() {
		getPath();
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		DivisionComboDataSet res = new DivisionComboDataSet();
		long status = Long.parseLong(request.getParameter("status"));// for//
																		// save////
																		// status
		res = new UserDataMgr().getStatusList(status, getUser());
		return res;
	}

	private MrBean getUser() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		MrBean user = new MrBean();
		user.getUser().setOrganizationID("001");
		user.getUser().setUserId(Service001.userid);
		user.getUser().setUserName(Service001.username);
		return user;
	}

	// Merchant setup end

	@GET
	@Path("readBySyskey")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleData readDataByQuestion() {
		getPath();
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		ArticleData ret;
		String key = request.getParameter("key");
		ret = new ContentMgr().readDataBySyskey(Long.parseLong(key), getUser());
		return ret;
	}

	/* Save(ContentMenu) */
	@POST
	@Path("saveContenMenu")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Resultb2b saveContenMenu(ArticleData p) throws IOException {
		getPath();
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		Resultb2b res = new Resultb2b();
		String imgurl = request.getParameter("imgurl");
		String typeurl = "upload/smallImage/";
		String today = ServerUtil.datetoString();
		typeurl += "contentImage/" + today + "/";
		p.setT10(imgurl + typeurl);
		String filePath = request.getServletContext().getRealPath("/") + "/upload";
		filePath += "/image";

		if (p.getN4() == 8) {
			String[] resizeList = new String[p.getUploadlist().size()];

			for (int i = 0; i < p.getUploadlist().size(); i++) {
				String imageName = p.getUploadlist().get(i).getName()
						.substring(p.getUploadlist().get(i).getName().lastIndexOf("/") + 1);
				p.getUploadlist().get(i).setName(imageName);
				p.getUploadlist().get(i).setSerial(i + 1);
				p.getUploadlist().get(i).setUrl(imgurl + typeurl + p.getUploadlist().get(i).getName());
				resizeList[i] = p.getUploadlist().get(i).getName();
			}

			p.setResizelist(resizeList);
		}

		if (p.getN4() != 8)
			// when photo looped , photo looped remove
			p.setUploadlist(ContentMgr.photoListLoopRemove(p.getUploadlist()));
		//

		if (p.getN4() != 8)
			// when photo removed , uploadlist removed
			p.setUploadlist(ContentMgr.photoListRemove(p.getT2(), p.getUploadlist()));

		//
		if (!ServerUtil.isUniEncoded(p.getT2())) { // name
			p.setT2(FontChangeUtil.zg2uni(p.getT2()));
		}
		if (!ServerUtil.isUniEncoded(p.getT1())) { // name
			p.setT1(FontChangeUtil.zg2uni(p.getT1()));
		}

		// convert_Base64_To_Image
		while (p.getT2().contains(" src=\"data:image/")) {
			int a = p.getT2().indexOf(" src=\"data:image/");
			int uploadCount = 0;

			String replaceString = "";
			if (a > 0) {
				uploadCount = ContentMgr.getUploadCount(p.getT2(), p.getUploadlist());
				int b = p.getT2().indexOf(" alt="); // end index
				String exist = p.getT2().substring(a + 17, b - 2);
				// System.out.println("p.t2 : " + p.getT2());
				// System.out.println("b: " + b);
				String replace = imgurl + typeurl + p.getUploadlist().get(uploadCount).getName();
				// System.out.println("replace: " + replace);
				replaceString = p.getT2().replace(exist, replace);
				while (!exist.equalsIgnoreCase(replace)) {
					// System.out.println("hi: ");
					replaceString = p.getT2().replace(exist, replace);
					a = replaceString.indexOf(" src=\"data:image/"); // start
																		// index
					b = replaceString.indexOf(" alt="); // end index
					exist = replaceString.substring(a + 17, b - 1);
				}
				StringBuffer buffer = new StringBuffer(replaceString);
				buffer.replace(a + 6, a + 17, "");
				buffer.replace(b - 10, b - 6, "");
				replaceString = buffer.toString();
				// uploadCount++;
				p.setT2(replaceString);
				// System.out.println("Str: " + replaceString);
			}
		}

		// when photo is remove
		String newString = p.getT2();
		// System.out.println("before remove: " + newString);
		while (newString.contains("src=\"upload/smallImage")) {
			System.out.println("when photo is remove");
			int c = newString.indexOf("src=\"upload/");
			int d = newString.indexOf("upload/");
			// System.out.println("c: " + c);
			// System.out.println("d: " + d);
			StringBuffer buff = new StringBuffer(newString);
			// System.out.println("buff: " + buff);
			// System.out.println("c: " + newString.charAt(c+4));
			// System.out.println("d: " + newString.charAt(d));
			buff.replace(c + 4, c + 5, "\"" + imgurl);
			newString = buff.toString();
			// System.out.println("newString: " + newString);
		}

		// width and height tag remove
		while (newString.contains("width=")) {
			StringBuffer buff2 = new StringBuffer(newString);
			int e = newString.indexOf("width=");
			buff2.replace(e, e + 25, "");
			newString = buff2.toString();
		}
		p.setT2(newString);
		//
		System.out.println("p.getT2(): " + p.getT2() + "\nuploadlist: " + p.getUploadlist());
		//
		res = ContentMgr.saveContenMenu(p, filePath, getUser());
		return res;
	}

	// Document
	/*@POST
	@Path("savemyDoc")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Resultb2b savedocument(DocumentData p) {
		getPath();
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		Resultb2b res = new Resultb2b();
		String filePath = request.getServletContext().getRealPath("/") + "/upload";
		filePath += "/image";
		Service001.userid = p.getUserId();
		System.out.println(p.getUserId());
		res = new DocMgr().savedocument(p);
		return res;
	}*/

	/* Save(FAQMenu) */
	@POST
	@Path("saveFAQMenu")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public FAQDataset saveFAQMenu(FAQData p) throws IOException {
		getPath();
		ResponseData response = new ResponseData();
		response = new SessionMgr().updateActivityTime(p.getSessionID(), p.getUserID());

		FAQDataset res = new FAQDataset();
		if (response.getCode().equals("0000")) {
			res = FAQMgr.saveFAQMenu(p);
		} else {
			res.setMsgCode(response.getCode());
			res.setMsgDesc(response.getDesc());
			res.setState(false);
		}
		return res;
	}

	// Merchant setup start
	@POST
	@Path("saveFlexCommRate")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public CommRateHeaderData saveFlexCommRate(CommRateHeaderData aData) {
		DAOManager.AbsolutePath = context.getRealPath("");
		CommRateCMSMgr mgr = new CommRateCMSMgr();
		getPath();
		SessionMgr s_mgr = new SessionMgr();
		ResponseData response = new ResponseData();

		response = s_mgr.updateActivityTime(aData.getSessionID(), aData.getCreatedUserID());
		if (response.getCode().equals("0000")) {
			aData = mgr.saveFlexCommRate(aData);
		} else {
			aData.setMsgCode(response.getCode());
			aData.setMsgDesc(response.getDesc());
			aData.setState("false");
		}
		return aData;
	}

	@POST
	@Path("saveMerchant")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON)
	public Result saveMerchant(CMSMerchantData data) {
		getPath();
		ResponseData response = new ResponseData();
		response = new SessionMgr().updateActivityTime(data.getSessionID(), data.getCreatedUserID());

		Result res = new Result();

		if (response.getCode().equals("0000")) {
			res = new MerchantMgr().saveCMSMerchantData(data);
		} else {
			res.setMsgCode(response.getCode());
			res.setMsgDesc(response.getDesc());
			res.setState(false);
		}

		return res;
	}

	@POST
	@Path("saveMerchantCommRateMapping")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON)
	public Result saveMerchantCommRateMapping(MCommRateMappingData data) {
		getPath();
		Result res = new Result();
		CommRateCMSMgr mgr = new CommRateCMSMgr();
		getPath();

		SessionMgr s_mgr = new SessionMgr();
		ResponseData response = new ResponseData();
		response = s_mgr.updateActivityTime(data.getSessionID(), data.getUserID());
		if (response.getCode().equals("0000")) {
			res = mgr.saveMerchantCommRateMapping(data);
		} else {
			res.setMsgCode(response.getCode());
			res.setMsgDesc(response.getDesc());
			res.setState(false);
		}
		return res;
	}

	@POST
	@Path("saveSMSSetting")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON)
	public Result saveSMSSetting(SMSSettingData data) {
		Result res = new Result();
		SMSSettingMgr mgr = new SMSSettingMgr();
		getPath();

		SessionMgr s_mgr = new SessionMgr();
		ResponseData response = new ResponseData();
		response = s_mgr.updateActivityTime(data.getSessionID(), data.getUserID());
		if (response.getCode().equals("0000")) {
			res = mgr.saveSMSSetting(data);
		} else {
			res.setMsgCode(response.getCode());
			res.setMsgDesc(response.getDesc());
			res.setState(false);
		}

		return res;

	}

	/* atn */
	/*@POST
	@Path("searchDocList")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DocumentDataset searchDocList(FilterDataset p, @QueryParam("userID") String userid) {
		getPath();
		DocumentDataset res = new DocumentDataset();
		res = new DocMgr().searchDocList(p, userid, getUser());
		return res;
	}*/

	@POST
	@Path("searchQuesList")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ContentListingDataList searchQuesList(FilterDataset p) {
		this.getPath();
		ContentListingDataList res = new ContentListingDataList();
		res = new ContentMgr().selectContent(p);
		return res;
	}

	
	@POST
	@Path("selectContent")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ContentListingDataList selectContent(FilterDataset p) {
		this.getPath();
		ContentListingDataList res = new ContentListingDataList();
		ResponseData response = new ResponseData();
		response = new SessionMgr().updateActivityTime(p.getSessionID(), p.getUserID(),"contentmenu");
		if (response.getCode().equals("0000")) {
			res = new ContentMgr().selectContent(p);
		}else {
			res.setMsgCode(response.getCode());
			res.setMsgDesc(response.getDesc());
		}
		
		return res;
	}

	// Merchant Profile list
		@GET
		@Path("getMerchantList")
		@Produces(MediaType.APPLICATION_JSON)
		public CMSMerchantDataSet getMerchantList(@QueryParam("searchVal") String searchText,
				@QueryParam("pagesize") int pageSize, @QueryParam("currentpage") int currentPage,
				@QueryParam("sessionID") String sessionID, @QueryParam("userID") String userID) {

			CMSMerchantDataSet res = new CMSMerchantDataSet();
			MerchantMgr u_mgr = new MerchantMgr();
			getPath();
			SessionMgr s_mgr = new SessionMgr();
			ResponseData response = new ResponseData();

			response = s_mgr.updateActivityTime(sessionID, userID,"");
			if (response.getCode().equals("0000")) {
				res = u_mgr.getAllMerchantData(searchText, pageSize, currentPage);
			} else {
				res.setMsgCode("0016");
				res.setMsgDesc(response.getDesc());
			}
			return res;
		}
}
