package com.nirvasoft.rp.service;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Result;
import com.nirvasoft.cms.mgr.ContentMenuMgr;
import com.nirvasoft.cms.mgr.VideoMgr;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.framework.ServerSession;
import com.nirvasoft.rp.mgr.SessionMgr;
import com.nirvasoft.rp.shared.ResponseData;

@Path("/serviceContentNew")
public class ServiceContentMenuNew {
	
	@Context
	HttpServletRequest request;
	
	private MrBean getUser() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		MrBean user = new MrBean();
		user.getUser().setOrganizationID("001");
		user.getUser().setUserId(Service001.userid);
		user.getUser().setUserName(Service001.username);
		return user;
	}
	private void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
	}
	
	/* Save Content And Video */
	@POST
	@Path("goSave")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Result saveCMSMenu(ArticleData p) throws IOException, SQLException {
		getPath();
		Result res = new Result();
		p.setN1(0);
		String formID = "";
		if(p.getT3().equals("Video")){ 
			formID = "videomenu";
		}else formID ="contentmenu";
		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime(p.getSessionId(), p.getUserId(),formID);
		if (sessionState.getCode().equals("0000")) {
		res = ContentMenuMgr.saveCMSMenu(p, getUser());
		}else{
			res.setState(false);
			res.setMsgDesc(sessionState.getDesc());			
		}			
		return res;
	}
	/* readBySyskey */
	@GET
	@Path("readBySyskey")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleData readDataByQuestion() {
		ArticleData ret = new ArticleData();
		String key = request.getParameter("key");
		ResponseData sessionState = new ResponseData();
		String sessionID = request.getParameter("sessionID");
		String userID = request.getParameter("userID");
		sessionState = new SessionMgr().updateActivityTime(sessionID, userID,"contentmenu");
		if (sessionState.getCode().equals("0000")) {
		ret = ContentMenuMgr.readDataBySyskey(Long.parseLong(key), getUser());
		}else 
			ret = null;
		return ret;
	}
	/* Delete Content And Video */
	@POST
	@Path("goDelete")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Result deleteContent(ArticleData p) {
		Result res = new Result();
		ResponseData sessionState = new ResponseData();
		String formID = "";
		if(p.getT3().equals("Video")){ 
			formID = "videomenu";
		}else formID ="contentmenu";
		sessionState = new SessionMgr().updateActivityTime(p.getSessionId(), p.getUserId(),formID);
		if (sessionState.getCode().equals("0000")) {
		res = ContentMenuMgr.deleteContent(p.getSyskey(), p.getT3(), getUser());
		}else{
			res.setState(false);
			res.setMsgDesc(sessionState.getDesc());			
		}
		return res;
	}
	
}
