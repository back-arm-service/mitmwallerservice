package com.nirvasoft.rp.service;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.ServerSession;
import com.nirvasoft.rp.mgr.MyTrustManager;
import com.nirvasoft.rp.mgr.OCRMgr;
import com.nirvasoft.rp.shared.Image;
import com.nirvasoft.rp.shared.OcrRes;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

@Path("/ocr")
public class ServiceOCR {

	@Context
	HttpServletRequest request;
	@Context
	private HttpServletResponse response;
	@javax.ws.rs.core.Context
	ServletContext context;

	@SuppressWarnings("unchecked")
	@POST
	@Path("sendImageOCR")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public OcrRes addNewAccount(Image req) {
		OcrRes res = new OcrRes();
		try {
			getPath();

			String token = new OCRMgr().getToken();

			if (token.equals("")) {
				String aUrl = ConnAdmin.readHuaweiOCR("urlToken");
				ClientConfig cc = new DefaultClientConfig();
				cc.getClasses().add(JacksonJsonProvider.class);
				Client client = Client.create(cc);
				WebResource webResource = client.resource(aUrl);
				String reqJSON = ConnAdmin.readHuaweiOCR("req");
				JSONParser parser2 = new JSONParser();
				Object obj3 = null;
				obj3 = parser2.parse(reqJSON);
				JSONObject obj = (JSONObject) obj3;
				ClientResponse response = null;
				MyTrustManager.disableSSL();
				response = webResource.type("application/json").header("X-Auth-Token", token).post(ClientResponse.class,
						obj);
				token = response.getHeaders().getFirst("X-Subject-Token");

				String messageCode = new OCRMgr().insertToken(token);

				if (messageCode != "0000") {
					res.setCode("0014");
					res.setDesc("Getting token failed.");
				}
			}

			String aUrl = ConnAdmin.readHuaweiOCR("url") + "myanmar-id-card";
			ClientConfig cc = new DefaultClientConfig();
			cc.getClasses().add(JacksonJsonProvider.class);
			Client client = Client.create(cc);
			WebResource webResource = client.resource(aUrl);
			JSONObject obj = new JSONObject();
			obj.put("image", req.getImage());
			ClientResponse response = null;
			MyTrustManager.disableSSL();
			response = webResource.type("application/json").header("X-Auth-Token", token).post(ClientResponse.class,
					obj);
			String jsonStr = response.getEntity(String.class);
			JSONParser parser = new JSONParser();
			Object obj1 = null;
			obj1 = parser.parse(jsonStr);
			JSONObject jsonObject1 = (JSONObject) obj1;
			String side = "";
			if (jsonObject1 != null) {
				Object obj2 = null;
				obj2 = jsonObject1.get("result");
				JSONObject jsonObject2 = (JSONObject) obj2;
				if (jsonObject2 != null) {
					side = (String) jsonObject2.get("side");
					res.setSide((String) jsonObject2.get("side"));
					res.setCl((String) jsonObject2.get("class"));
					res.setCode("0000");
					res.setDesc("Success");
					if (side.equals("front")) {
						res.setName((String) jsonObject2.get("name"));
						res.setNrcID((String) jsonObject2.get("nrc_id"));
						res.setBirth((String) jsonObject2.get("birth"));
						res.setFatherName((String) jsonObject2.get("father_name"));
						res.setIssueDate((String) jsonObject2.get("issue_date"));
						res.setBloodGroup((String) jsonObject2.get("blood_group"));
						res.setBlReligion((String) jsonObject2.get("bloodlines_religion"));
						res.setHeight((String) jsonObject2.get("height"));

					} else if (side.equals("back")) {
						res.setAddress((String) jsonObject2.get("address"));
						res.setNrcIDBack((String) jsonObject2.get("nrc_id_back"));
						res.setCardID((String) jsonObject2.get("card_id"));
						res.setProfession((String) jsonObject2.get("profession"));
					}
				} else {
					res.setCode((String) jsonObject1.get("error_code"));
					res.setDesc((String) jsonObject1.get("error_msg"));
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return res;
	}

	public void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
	}

}
