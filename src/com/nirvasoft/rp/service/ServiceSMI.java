package com.nirvasoft.rp.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.nirvasoft.mfi.mgr.ApplicationFormMgr;
import com.nirvasoft.mfi.mgr.BankChargesMgr;
import com.nirvasoft.mfi.mgr.LoanTypeListMgr;
import com.nirvasoft.mfi.mgr.MobileMemberDataMgr;
import com.nirvasoft.mfi.mgr.PaymentInstallmentMgr;
import com.nirvasoft.mfi.mgr.RegionReferenceMgr;
import com.nirvasoft.mfi.mgr.StaffLoanDataMgr;
import com.nirvasoft.mfi.mgr.StaffLoanFormMgr;
import com.nirvasoft.mfi.mgr.TownshipReferenceMgr;
import com.nirvasoft.mfi.users.data.ApplicationFormData;
import com.nirvasoft.mfi.users.data.BankChargesArrayInterface;
import com.nirvasoft.mfi.users.data.LoanTypeArrayInterface;
import com.nirvasoft.mfi.users.data.LoginHistoryData;
import com.nirvasoft.mfi.users.data.MfiRequest;
import com.nirvasoft.mfi.users.data.MobileMemberData;
import com.nirvasoft.mfi.users.data.RegionDataArray;
import com.nirvasoft.mfi.users.data.StaffLoan;
import com.nirvasoft.mfi.users.data.StaffLoanApplicantImpl;
import com.nirvasoft.mfi.users.data.StaffLoanApplicantInterface;
import com.nirvasoft.mfi.users.data.StaffLoanDataArray;
import com.nirvasoft.mfi.users.data.StaffLoanGuarantorDataArray;
import com.nirvasoft.mfi.users.data.StaffLoanGuarantorImpl;
import com.nirvasoft.mfi.users.data.StaffLoanGuarantorInterface;
import com.nirvasoft.mfi.users.data.StaffLoanInterface;
import com.nirvasoft.mfi.users.data.StaffLoanPaymentInstallmentArray;
import com.nirvasoft.mfi.users.data.TownshipDataArray;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.framework.ServerSession;
import com.nirvasoft.rp.shared.ResultData;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.GeneralUtility;
import com.nirvasoft.rp.util.ServerGlobal;

@Path("/serviceSMI")
public class ServiceSMI {
	@Context
	HttpServletRequest request;

	@Context
	private HttpServletResponse response;

	String UPLOAD_DIRECTORY = "";
	String FONT_DIRECTORY = "";
	String IMAGE_DIRECTORY = "";

	@javax.ws.rs.core.Context
	ServletContext context;
	public static String userid = "";
	public static String username = "";
	public static String userpsw = "";

	private void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";

	}
	
	@GET
	@Path("getLoanhistoryByAppandLoanSyskey")
	@Produces(MediaType.APPLICATION_JSON)
	public StaffLoanInterface getLoanhistoryByAppandLoanSyskey(@QueryParam("syskey") String applicantsyskey,
			@QueryParam("loansyskey") String loansyskey) {
		getPath();
		StaffLoanInterface res = new StaffLoan();
		
		res = StaffLoanFormMgr.getLoanListBySyskeyandLoanSyskey(applicantsyskey, loansyskey);
		return res;

	}
	//diff
	@GET
	@Path("getLoanApplicantData")
	@Produces(MediaType.APPLICATION_JSON)

	public StaffLoanApplicantInterface getLoanApplicantBysyskey(@QueryParam("syskey") String syskey) {
		getPath();
		StaffLoanApplicantInterface data = new StaffLoanApplicantImpl();
		data = StaffLoanDataMgr.getStaffLoanApplicantBysyskey(syskey);

		return data;
	}
	//same
	/*@GET
	@Path("getRegionList")
	@Produces(MediaType.APPLICATION_JSON)

	public RegionDataArray getRegionList() {
		getPath();
		RegionDataArray res = RegionReferenceMgr.getRegionRefList();

		return res;
	}*/

	
	//same
	/*@GET
	@Path("getTownshipList")
	@Produces(MediaType.APPLICATION_JSON)

	public TownshipDataArray getTownshipList(@QueryParam("regionRefSyskey") String regionRefSyskey) {
		getPath();
		TownshipDataArray res = TownshipReferenceMgr.getTownshipRefListByRegion(Long.parseLong(regionRefSyskey));

		return res;
	}*/

	//diff
	@GET
	@Path("checkLoanGuarantor")
	@Produces(MediaType.APPLICATION_JSON)

	public StaffLoanGuarantorDataArray checkLoanGuarantor(@QueryParam("syskey") String applicantsyskey) {
		getPath();
		StaffLoanGuarantorDataArray data = new StaffLoanGuarantorDataArray();
		data = StaffLoanDataMgr.getGuarantorBysyskey(applicantsyskey);

		return data;
	}

	//diff
	@POST
	@Path("applyLoan")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON)
	public Result applyLoan(StaffLoan loan) {
		
	//StaffLoanImpl data	
		getPath();
		GeneralUtil.readDebugLogStatus();
		GeneralUtil.readMemberStatus();
		GeneralUtil.readLoanStatus();
		GeneralUtil.readImagePath();
		GeneralUtil.readOrgId();
		Result res = new Result();
		Result respdf=new Result();
		MfiRequest mfirequest=new MfiRequest();
		
//		res.setMsgCode("0000");
//		res.setMsgDesc("Save Successfully");
		
//		if(loan.getStaffLoanApplicant().getStaffSyskey()==0)
//		{
//			res.setMsgCode("0014");
//			res.setMsgDesc("Staff Loan Syskey is Invalid.");
//			return res;
//		}
		
		if(loan.getStaffLoanApplicant().getStaffName().equals("") || loan.getStaffLoanApplicant().getStaffName().equals(" ") || loan.getStaffLoanApplicant().getStaffName().equals(null))
		{
			res.setMsgCode("0014");
			res.setMsgDesc("Enter Applicant Name.");
			return res;
		}
		
		if(loan.getStaffLoanApplicant().getStaffNrc().equals("") || loan.getStaffLoanApplicant().getStaffNrc().equals(" ") || loan.getStaffLoanApplicant().getStaffNrc().equals(null))
		{
			res.setMsgCode("0014");
			res.setMsgDesc("Enter Applicant NRC.");
			return res;
		}

//		res = GeneralUtility.normalizePhoneNoformat(loan.getStaffLoanApplicant().getMobile());
//		if(res.isState()==false)
//		{
//			res.setMsgCode("0014");
//			res.setMsgDesc("Invalid Applicant Mobile Phone No.");
//			return res;
//		}
//		
		if(loan.getStaffLoanApplicant().getDepartment().equals("") || loan.getStaffLoanApplicant().getDepartment().equals(" ") || loan.getStaffLoanApplicant().getDepartment().equals(null))
		{
			res.setMsgCode("0014");
			res.setMsgDesc("Enter Applicant Current Department.");
			return res;
		}
		if(loan.getStaffLoanApplicant().getCurrentPosition().equals("") || loan.getStaffLoanApplicant().getCurrentPosition().equals(" ") || loan.getStaffLoanApplicant().getCurrentPosition().equals(null))
		{
			res.setMsgCode("0014");
			res.setMsgDesc("Enter Applicant Current Position.");
			return res;
		}
		if(loan.getStaffLoanApplicant().getMonthlyBasicIncome()<=0)
		{
			res.setMsgCode("0014");
			res.setMsgDesc("Enter Applicant Monthly Basic Income.");
			return res;
		}
		if(loan.getStaffLoanApplicant().getPhotoPath().equals("") || loan.getStaffLoanApplicant().getPhotoPath().equals(" ") || loan.getStaffLoanApplicant().getPhotoPath().equals(null))
		{
			res.setMsgCode("0014");
			res.setMsgDesc("Enter Applicant Photo.");
			return res;
		}
		if(loan.getStaffLoanApplicant().getFrontNRCImagePath().equals("") || loan.getStaffLoanApplicant().getFrontNRCImagePath().equals(" ") || loan.getStaffLoanApplicant().getFrontNRCImagePath().equals(null))
		{
			res.setMsgCode("0014");
			res.setMsgDesc("Enter Applicant Front NRC Image.");
			return res;
		}
		if(loan.getStaffLoanApplicant().getBackNRCImagePath().equals("") || loan.getStaffLoanApplicant().getBackNRCImagePath().equals(" ") || loan.getStaffLoanApplicant().getBackNRCImagePath().equals(null))
		{
			res.setMsgCode("0014");
			res.setMsgDesc("Enter Applicant Back NRC Image.");
			return res;
		}
		
		//For Guatrantor 1
		if(loan.getStaffLoanGuarantor1().getGuarantorName().equals("") || loan.getStaffLoanGuarantor1().getGuarantorName().equals(" ") || loan.getStaffLoanGuarantor1().getGuarantorName().equals(null))
		{
			res.setMsgCode("0014");
			res.setMsgDesc("Enter Guarantor1 Name.");
			return res;
		}
		
		if(loan.getStaffLoanGuarantor1().getGuarantorNrc().equals("") || loan.getStaffLoanGuarantor1().getGuarantorNrc().equals(" ") || loan.getStaffLoanGuarantor1().getGuarantorNrc().equals(null))
		{
			res.setMsgCode("0014");
			res.setMsgDesc("Enter Guarantor1 NRC.");
			return res;
		}

//		res = GeneralUtility.normalizePhoneNoformat(loan.getStaffLoanGuarantor1().getMobile());
//		if(res.isState()==false)
//		{
//			res.setMsgCode("0014");
//			res.setMsgDesc("Invalid Guarantor1 Mobile Phone No.");
//			return res;
//		}
		
		if(loan.getStaffLoanGuarantor1().getDepartment().equals("") || loan.getStaffLoanGuarantor1().getDepartment().equals(" ") || loan.getStaffLoanGuarantor1().getDepartment().equals(null))
		{
			res.setMsgCode("0014");
			res.setMsgDesc("Enter Guarantor1 Current Department.");
			return res;
		}
		if(loan.getStaffLoanGuarantor1().getCurrentPosition().equals("") || loan.getStaffLoanGuarantor1().getCurrentPosition().equals(" ") || loan.getStaffLoanGuarantor1().getCurrentPosition().equals(null))
		{
			res.setMsgCode("0014");
			res.setMsgDesc("Enter Guarantor1 Current Position.");
			return res;
		}
		//for guarantor 2
//		if(loan.getStaffLoanGuarantor2().getGuarantorName().equals("") || loan.getStaffLoanGuarantor2().getGuarantorName().equals(" ") || loan.getStaffLoanGuarantor2().getGuarantorName().equals(null))
//		{
//			res.setMsgCode("0014");
//			res.setMsgDesc("Enter Guarantor2 Name.");
//			return res;
//		}
//		
//		if(loan.getStaffLoanGuarantor2().getGuarantorNrc().equals("") || loan.getStaffLoanGuarantor2().getGuarantorNrc().equals(" ") || loan.getStaffLoanGuarantor2().getGuarantorNrc().equals(null))
//		{
//			res.setMsgCode("0014");
//			res.setMsgDesc("Enter Guarantor2 NRC.");
//			return res;
//		}

//		res = GeneralUtility.normalizePhoneNoformat(loan.getStaffLoanGuarantor2().getMobile());
//		if(res.isState()==false)
//		{
//			res.setMsgCode("0014");
//			res.setMsgDesc("Invalid Guarantor2 Mobile Phone No.");
//			return res;
//		}
//		
//		if(loan.getStaffLoanGuarantor2().getDepartment().equals("") || loan.getStaffLoanGuarantor2().getDepartment().equals(" ") || loan.getStaffLoanGuarantor2().getDepartment().equals(null))
//		{
//			res.setMsgCode("0014");
//			res.setMsgDesc("Enter Guarantor2 Current Department.");
//			return res;
//		}
//		
//		if(loan.getStaffLoanGuarantor2().getCurrentPosition().equals("") || loan.getStaffLoanGuarantor2().getCurrentPosition().equals(" ") || loan.getStaffLoanGuarantor2().getCurrentPosition().equals(null))
//		{
//			res.setMsgCode("0014");
//			res.setMsgDesc("Enter Guarantor2 Current Position.");
//			return res;
//		}
		//for loan data
		if(loan.getStaffLoanData().getLoanAmount()<0)
		{
			res.setMsgCode("0014");
			res.setMsgDesc("Invalid Loan Amount.");
			return res;
		}
		if(loan.getStaffLoanData().getDepartmentChiefRecommendation().equals("") || loan.getStaffLoanData().getDepartmentChiefRecommendation().equals(" ") || loan.getStaffLoanData().getDepartmentChiefRecommendation().equals(null))
		{
			res.setMsgCode("0014");
			res.setMsgDesc("Enter Department Chief Recommendation.");
			return res;
		}
		if(loan.getStaffLoanData().getStaffIDCardImagePath().equals("") || loan.getStaffLoanData().getStaffIDCardImagePath().equals(" ") || loan.getStaffLoanData().getStaffIDCardImagePath().equals(null))
		{
			res.setMsgCode("0014");
			res.setMsgDesc("Enter Staff ID Card Copy.");
			return res;
		}
		if(loan.getStaffLoanData().getFrontFamilyRegImagePath().equals("") || loan.getStaffLoanData().getFrontFamilyRegImagePath().equals(" ") || loan.getStaffLoanData().getFrontFamilyRegImagePath().equals(null))
		{
			res.setMsgCode("0014");
			res.setMsgDesc("Enter Front Family Registration.");
			return res;
		}
		if(loan.getStaffLoanData().getBackFamilyRegImagePath().equals("") || loan.getStaffLoanData().getBackFamilyRegImagePath().equals(" ") || loan.getStaffLoanData().getBackFamilyRegImagePath().equals(null))
		{
			res.setMsgCode("0014");
			res.setMsgDesc("Enter back Family Registration.");
			return res;
		}
		loan.getStaffLoanData().setAppliedDate(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));		
		//respdf=staffLoanDownload(loan);	
//		
//		if(respdf.getMsgCode().equals("0000"))
//		{	

		res = StaffLoanFormMgr.saveStaffLoanData(loan);
			
			if(res.getMsgCode().equals("0000"))
			{
				res.setMsgDesc("Submit Successful.");
				if(res.getMsgCode().equals("0000")){
					mfirequest.setT1(String.valueOf(res.getSyskey()));//loan 
					mfirequest.setT2(loan.getStaffLoanApplicant().getMobile());
					mfirequest.setT3(loan.getStaffLoanApplicant().getStaffName());
					mfirequest.setT4("79");
					ResultData response=new ResultData();
					response=MFICallAPI(mfirequest);
				}
//				res.setGua1downloadlink(respdf.getGua1downloadlink());
//				res.setGua1downloadname(respdf.getGua1downloadname());
//				res.setGua2downloadlink(respdf.getGua2downloadlink());
//				res.setGua2downloadname(respdf.getGua2downloadname());
			}else
			{
				res.setMsgDesc(res.getMsgDesc() +"\n Submit Fail.");
			}
			
		//}
	

		return res;

	}
	public ResultData MFICallAPI(MfiRequest mfireq) {
		ResultData res = new ResultData();
		GeneralUtil.readMFIServiceSetting();
		String aUrl = ServerGlobal.getMfiURL();	
        int responseCode = -1;
        String responseJSONBody = "";
        try {
        	
        	String ecodedValue1 = URLEncoder.encode(mfireq.getT3(), StandardCharsets.UTF_8.name());
    		//String decodedValue1 = URLDecoder.decode(ecodedValue1, StandardCharsets.UTF_8.name());
    		
        	aUrl = aUrl.replace("<parameter1>",mfireq.getT1());
        	aUrl = aUrl.replace("<parameter2>",mfireq.getT2());
        	aUrl = aUrl.replace("<parameter3>",ecodedValue1);
        	aUrl = aUrl.replace("<parameter4>",mfireq.getT4());
     
            URL url = new URL(aUrl);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setConnectTimeout(10000);
            httpURLConnection.setReadTimeout(10000);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestMethod("POST");
            responseCode = httpURLConnection.getResponseCode();
            responseJSONBody = convertStreamToString(httpURLConnection.getInputStream());
            
            if (responseCode == 200 && responseJSONBody != null && !responseJSONBody.trim().equals("")) {
                JSONParser jsonParser = new JSONParser();
                Object obj = jsonParser.parse(responseJSONBody);
                JSONObject jsonObj = (JSONObject) obj;
                res.setMsgCode((String) jsonObj.get("msgCode"));
                if( res.getMsgCode().equals("0000")){
                	
                }else{
                	
                }
            } else {
                res.setMsgCode("0014");
                res.setMsgDesc("Call MFI Service Response Fail");
                return res;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            res.setMsgCode("0014");
            res.setMsgDesc("Call MFI Service Connection Fail");
            //res.setError(ex.getMessage());
            return res;
        }
        return res;
    }
	 private String convertStreamToString(InputStream inputStream) throws Exception {
	        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
	        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	        StringBuilder stringBuilder = new StringBuilder();
	        String line = null;
	        while ((line = bufferedReader.readLine()) != null) {
	            stringBuilder.append(line);
	        }
	        return stringBuilder.toString();
	    }
	//diff
	@GET
	@Path("getBankChargesByOutletLoanTypeSyskey")
	@Produces(MediaType.APPLICATION_JSON)

	public BankChargesArrayInterface getBankChargesByLoanTypeSyskey(@QueryParam("Outletloantypesyskey") String Outletloantypesyskey) {
		getPath();

		GeneralUtil.readDebugLogStatus();
		BankChargesArrayInterface res =BankChargesMgr.getBankChargesList(Outletloantypesyskey);
		
		return res;
	}

	//diff
	@GET
	@Path("getLoanGuarantorData")
	@Produces(MediaType.APPLICATION_JSON)

	public StaffLoanGuarantorInterface getLoanGuarantorDataBysyskey(@QueryParam("syskey") String syskey) {
		getPath();
		StaffLoanGuarantorInterface data = new StaffLoanGuarantorImpl();
		data = StaffLoanDataMgr.getGuarantorDataByGuarantorsyskey(syskey);

		return data;
	}

	//diff
	@GET
	@Path("getLoanTypeList")
	@Produces(MediaType.APPLICATION_JSON)

	public LoanTypeArrayInterface getLoanTypeList() {
		getPath();
		GeneralUtil.readDebugLogStatus();
		GeneralUtil.readOrgId();
		LoanTypeArrayInterface res =LoanTypeListMgr.getLoanTypeList(ServerGlobal.getSmidborg());
		
		return res;
		
	}
	
	//diff
	@GET
	@Path("loadRepaymentInstallmentlistByLoansyskey")
	@Produces(MediaType.APPLICATION_JSON)
	public StaffLoanPaymentInstallmentArray loadRepaymentInstallmentlistByLoansyskey(@QueryParam("loansyskey") String loansyskey) {
		getPath();
		StaffLoanPaymentInstallmentArray res = new StaffLoanPaymentInstallmentArray();
		res = PaymentInstallmentMgr.getStaffLoanRepaymentSchedule( loansyskey);
		return res;

	}
	
	//DIFF
	@GET
	@Path("loadLoanRepaymentInstallmentlist")
	@Produces(MediaType.APPLICATION_JSON)
	public StaffLoanDataArray loadLoanRepaymentInstallmentlist(
			@QueryParam("syskey") String applicantsyskey, @QueryParam("loanstatus") String loanstatus) {
		getPath();
		StaffLoanDataArray res = new StaffLoanDataArray();
		res = StaffLoanFormMgr.getLoanListBySyskeyandLoanstatus(applicantsyskey, loanstatus);
		return res;

	}
	
	//diff
	@POST
	@Path("updateLoan")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON)
	public Result updateLoan(StaffLoan loan) {
		
	//StaffLoanImpl data	
		getPath();
		GeneralUtil.readDebugLogStatus();
		GeneralUtil.readMemberStatus();
		GeneralUtil.readLoanStatus();
		GeneralUtil.readImagePath();
		GeneralUtil.readOrgId();
		Result res = new Result();
		
//		res.setMsgCode("0000");
//		res.setMsgDesc("Save Successfully");
		if(loan.getStaffLoanGuarantor1().getGuarantorSignImagePath().equals("") || loan.getStaffLoanGuarantor1().getGuarantorSignImagePath().equals(" ") || loan.getStaffLoanGuarantor1().getGuarantorSignImagePath().equals(null))
		{
			res.setMsgCode("0014");
			res.setMsgDesc("Upload Guarantor1's Sign Image.");
			return res;
		}
		if(loan.getStaffLoanGuarantor2().getGuarantorSignImagePath().equals("") || loan.getStaffLoanGuarantor2().getGuarantorSignImagePath().equals(" ") || loan.getStaffLoanGuarantor2().getGuarantorSignImagePath().equals(null))
		{
			res.setMsgCode("0014");
			res.setMsgDesc("Upload Guarantor1's Sign Image.");
			return res;
		}
		res = StaffLoanDataMgr.updateGuarantorSign(loan);
			

		return res;

	}
	/*@POST
	@Path("getMonthlyAmountByTerm1")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON)
	public StaffLoan getMonthlyAmountByTerm(StaffLoan data) {
		getPath();
		StaffLoan res = new StaffLoan();

		try {
			res = StaffLoanFormMgr.getMonthlyAmountByTerm(data);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			data.setMsgcode("0014");
			data.setMsgDesc("Connection Fail.");
		}

		return res;

	}*/
}
