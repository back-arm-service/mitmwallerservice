
package com.nirvasoft.rp.service;

import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.nirvasoft.cms.framework.Result;
import com.nirvasoft.cms.mgr.CommRateCMSMgr;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.data.BranchData;
import com.nirvasoft.rp.framework.Lov3;
import com.nirvasoft.rp.framework.Ref;
import com.nirvasoft.rp.framework.ServerSession;
import com.nirvasoft.rp.mgr.ATMLocationMgr;
import com.nirvasoft.rp.mgr.BranchMgr;
import com.nirvasoft.rp.mgr.LOVAdmMgr;
import com.nirvasoft.rp.mgr.SessionMgr;
import com.nirvasoft.rp.mgr.UserReportMgr;
import com.nirvasoft.rp.mgr.VersionHistoryMgr;
import com.nirvasoft.rp.mgr.integration.SMSSettingMgr;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.shared.SessionData;
import com.nirvasoft.rp.util.FileUtil;

@Path("/serviceAdmLOV")
public class ServiceAdmLOV {
	@Context
	HttpServletRequest request;
	@Context
	private HttpServletResponse response;
	@javax.ws.rs.core.Context
	ServletContext context;
	

	@POST
	@Path("getTransferTypes")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Lov3 getAccountTransferTypes(SessionData data) {
		Lov3 response = null;

		getPath();
		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime_Old(data.getSessionID(), data.getUserID());

		if (sessionState.getCode().equals("0000")) {
			response = new LOVAdmMgr().getTypes();
		} else {
			response = new Lov3();
			response.setMsgCode(sessionState.getCode());
			response.setMsgDesc(sessionState.getDesc());
		}

		return response;
	}

	// BranchCode data combo
	@GET
	@Path("getAllBranchCode")
	@Produces(MediaType.APPLICATION_JSON)
	public Lov3 getAllBranchCode() {
		BranchMgr branch_mgr = new BranchMgr();
		ArrayList<BranchData> res = new ArrayList<BranchData>();
		Lov3 lov = new Lov3();
		getPath();

		res = branch_mgr.getAllBranch();
		Ref brancharr[] = new Ref[res.size()];
		for (int i = 0; i < res.size(); i++) {
			Ref ref = new Ref();
			ref.setvalue(res.get(i).getBranchCode());
			ref.setcaption(res.get(i).getBranchCode());

			brancharr[i] = ref;

		}
		lov.setRef024(brancharr);
		return lov;
	}

	@GET
	@Path("getAllCommRef")
	@Produces(MediaType.APPLICATION_JSON)
	public Lov3 getAllCommRef() {
		CommRateCMSMgr Mgr = new CommRateCMSMgr();
		ArrayList<Result> res = new ArrayList<Result>();
		Lov3 lov = new Lov3();
		getPath();
		ResponseData sessionState = new ResponseData();
		String sessionID = request.getParameter("sessionID");
		String userID = request.getParameter("userID");
		sessionState = new SessionMgr().updateActivityTime(sessionID, userID,"Merchant Comm Mapping");
		if (sessionState.getCode().equals("0000")) {
			res = Mgr.getAllCommRef();
			Ref refarr[] = new Ref[res.size()];
			for (int i = 0; i < res.size(); i++) {
				Ref ref = new Ref();
				ref.setvalue(res.get(i).getKeyString());
				ref.setcaption(res.get(i).getKeyst());

				refarr[i] = ref;

			}
			lov.setRefcharges(refarr);
		} else
			res = null;
		return lov;
	}

	@GET
	@Path("getAllFeatures")
	@Produces(MediaType.APPLICATION_JSON)
	public Lov3 getAllFeatures() {
		LOVAdmMgr u_mgr = new LOVAdmMgr();
		ArrayList<Result> res = new ArrayList<Result>();
		Lov3 lov = new Lov3();
		getPath();
		res = u_mgr.getAllFeatures();
		Ref arr[] = new Ref[res.size()];

		for (int i = 0; i < res.size(); i++) {
			Ref ref = new Ref();
			ref.setvalue(res.get(i).getKeyst());
			ref.setcaption(res.get(i).getKeyString());
			arr[i] = ref;
		}
		lov.setRefFeature(arr);
		return lov;
	}

	@GET
	@Path("getAllProcessingCode")
	@Produces(MediaType.APPLICATION_JSON)
	public Lov3 getAllProcessingCode() {
		Lov3 lov = new Lov3();
		ArrayList<Ref> res = new ArrayList<Ref>();
		getPath();
		FileUtil file = new FileUtil();
		try {
			ResponseData sessionState = new ResponseData();
			String sessionID = request.getParameter("sessionID");
			String userID = request.getParameter("userID");
			sessionState = new SessionMgr().updateActivityTime_Old(sessionID, userID);
			if (sessionState.getCode().equals("0000")) {
				res = file.getAllProcessingCode();
				Ref processingcodearr[] = new Ref[res.size()];
				for (int i = 0; i < res.size(); i++) {
					Ref ref = new Ref();
					ref.setvalue(res.get(i).getvalue());
					ref.setcaption(res.get(i).getcaption());
					processingcodearr[i] = ref;
				}
				lov.setRefProcessingCode(processingcodearr);
			} else
				res = null;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return lov;
	}

	// version history start
	@POST
	@Path("getAppCodesForVersionHistory")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Lov3 getAppCodesForVersionHistory(SessionData data) {
		Lov3 response = null;

		getPath();
		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime(data.getSessionID(), data.getUserID(),"version-history");

		if (sessionState.getCode().equals("0000")) {
			response = new VersionHistoryMgr().getAppCodes();
		} else {
			response = new Lov3();
			response.setMsgCode(sessionState.getCode());
			response.setMsgDesc(sessionState.getDesc());
		}

		return response;
	}

	@POST
	@Path("getChannelkey")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Lov3 getChannelkey(SessionData data) {
		Lov3 response = null;

		getPath();

		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime_Old(data.getSessionID(), data.getUserID());

		if (sessionState.getCode().equals("0000")) {
			response = new LOVAdmMgr().getChannelkey(data);
		} else {
			response = new Lov3();
			response.setMsgCode(sessionState.getCode());
			response.setMsgDesc(sessionState.getDesc());
		}

		return response;
	}

	@GET
	@Path("getCodeFromLovdeatils")
	@Produces(MediaType.APPLICATION_JSON)
	public Lov3 getCodeFromLovdeatils(@QueryParam("merchantId") String merchantID) {
		ArrayList<Result> res = new ArrayList<Result>();
		Lov3 lov = new Lov3();
		getPath();
		res = new LOVAdmMgr().getCodeFromLovdeatils(merchantID);
		Ref refarr[] = new Ref[res.size()];
		for (int i = 0; i < res.size(); i++) {
			Ref ref = new Ref();
			ref.setvalue(res.get(i).getMsgCode());
			ref.setcaption(res.get(i).getMsgDesc());
			refarr[i] = ref;
		}
		lov.setReflovdetails(refarr);
		return lov;
	}

	@POST
	@Path("getDomainType")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Lov3 getDomainType(SessionData data) {
		Lov3 response = null;
		getPath();
		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime_Old(data.getSessionID(), data.getUserID());

		if (sessionState.getCode().equals("0000")) {
			response = new LOVAdmMgr().getDomainType();
		} else {
			response = new Lov3();
			response.setMsgCode(sessionState.getCode());
			response.setMsgDesc(sessionState.getDesc());
		}

		return response;
	}

	@POST
	@Path("getFileType")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Lov3 getFileType(SessionData data) {
		Lov3 response = null;

		getPath();

		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime_Old(data.getSessionID(), data.getUserID());

		if (sessionState.getCode().equals("0000")) {
			response = new LOVAdmMgr().getFileType();
		} else {
			response = new Lov3();
			response.setMsgCode(sessionState.getCode());
			response.setMsgDesc(sessionState.getDesc());
		}

		return response;
	}

	// SMS start
	@GET
	@Path("getFunctionLov")
	@Produces(MediaType.APPLICATION_JSON)
	public Lov3 getFunctionLov() {
		getPath();
		SMSSettingMgr Mgr = new SMSSettingMgr();
		Lov3 res = new Lov3();
		ResponseData sessionState = new ResponseData();
		String sessionID = request.getParameter("sessionID");
		String userID = request.getParameter("userID");
		sessionState = new SessionMgr().updateActivityTime(sessionID, userID);
		if (sessionState.getCode().equals("0000")) {
			res = Mgr.getFunctionLov();
		} else
			res = null;
		return res;
	}

	@GET
	@Path("getmerchantidlistdetail")
	@Produces(MediaType.APPLICATION_JSON)
	public Lov3 getLoadReferences1() {
		getPath();
		String userId = request.getParameter("userID");
		Lov3 res = new Lov3();
		ResponseData sessionState = new ResponseData();
		String sessionID = request.getParameter("sessionID");
		String userID = request.getParameter("userID");
		sessionState = new SessionMgr().updateActivityTime(sessionID, userID);
		if (sessionState.getCode().equals("0000")) {
			res = new LOVAdmMgr().getMerchantIDListDetail(userId);
		} else
			res = null;
		return res;
	}	

	// version history end
	// locator setup start
	@GET
	@Path("getLocationCbo")
	@Produces(MediaType.APPLICATION_JSON)
	public Lov3 getLocationCbo() {
		Lov3 res = new Lov3();
		ATMLocationMgr mgr = new ATMLocationMgr();
		res = mgr.getLocationCbo();
		return res;
	}
	// locator setup end

	// merchant
	@GET
	@Path("getOrderList")
	@Produces(MediaType.APPLICATION_JSON)
	public Lov3 getOrderList() {
		LOVAdmMgr u_mgr = new LOVAdmMgr();
		ArrayList<Result> res = new ArrayList<Result>();
		Lov3 lov = new Lov3();
		getPath();
		res = u_mgr.getOrderList();
		Ref arr[] = new Ref[res.size()];

		for (int i = 0; i < res.size(); i++) {
			Ref ref = new Ref();
			ref.setvalue(res.get(i).getKeyst());
			ref.setcaption(res.get(i).getKeyString());
			arr[i] = ref;
		}
		lov.setRefOrder(arr);
		return lov;

	}

	private void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
	}

	@GET
	@Path("getPDFType")
	@Produces(MediaType.APPLICATION_JSON)
	public Lov3 getPDFType() {
		Lov3 response = null;

		getPath();

		response = new LOVAdmMgr().getFileType();
		return response;
	}

	@POST
	@Path("getPOCUserType")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Lov3 getPOCUserType(SessionData data) {
		Lov3 response = null;

		getPath();

		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime_Old(data.getSessionID(), data.getUserID());

		if (sessionState.getCode().equals("0000")) {
			response = new LOVAdmMgr().getPOCUserType();
		} else {
			response = new Lov3();
			response.setMsgCode(sessionState.getCode());
			response.setMsgDesc(sessionState.getDesc());
		}

		return response;
	}

	@GET
	@Path("getServiceLov")
	@Produces(MediaType.APPLICATION_JSON)
	public Lov3 getServiceLov() {
		getPath();
		Lov3 res = new Lov3();
		ResponseData sessionState = new ResponseData();
		String sessionID = request.getParameter("sessionID"); 
		String userID = request.getParameter("userID");
		sessionState = new SessionMgr().updateActivityTime(sessionID, userID);
		if (sessionState.getCode().equals("0000")) {
			res = new SMSSettingMgr().getServiceLov();
		}else res = null;
		return res;
	}
	// SMS end

	@POST
	@Path("getStatusCodesForVersionHistory")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Lov3 getStatusCodesForVersionHistory(SessionData data) {
		Lov3 response = null;

		getPath();
		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime(data.getSessionID(), data.getUserID(),"version-history");

		if (sessionState.getCode().equals("0000")) {
			response = new VersionHistoryMgr().getStatusCodes();
		} else {
			response = new Lov3();
			response.setMsgCode(sessionState.getCode());
			response.setMsgDesc(sessionState.getDesc());
		}

		return response;
	}

	@POST
	@Path("getUserType")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Lov3 getUserType(SessionData data) {
		this.getPath();
		UserReportMgr Mgr = new UserReportMgr();
		Lov3 response = new Lov3();

		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime(data.getSessionID(), data.getUserID());

		if (sessionState.getCode().equals("0000")) {
			response = Mgr.getUserType();
		} else {
			response = new Lov3();
			response.setMsgCode(sessionState.getCode());
			response.setMsgDesc(sessionState.getDesc());
		}

		return response;
	}

	// merchant commission setup
	@GET
	@Path("getZone")
	@Produces(MediaType.APPLICATION_JSON)
	public Lov3 getZone() {
		getPath();
		Lov3 res = new Lov3();
		CommRateCMSMgr dbmgr = new CommRateCMSMgr();
		res = dbmgr.getZone();
		return res;
	}

	@POST
	@Path("getLovType")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Lov3 getLovType(SessionData data) {
		Lov3 response = null;
		getPath();
		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime
				(data.getSessionID(), data.getUserID());

		if (sessionState.getCode().equals("0000")) {
			response = new LOVAdmMgr().getLovType(data.getLovDesc());
		} else {
			response = new Lov3();
			response.setMsgCode(sessionState.getCode());
			response.setMsgDesc(sessionState.getDesc());
		}

		return response;
	}
}
