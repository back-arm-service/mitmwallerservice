package com.nirvasoft.rp.service;

import java.sql.SQLException;

import com.nirvasoft.fcpayment.core.data.fcchannel.DisPaymentTransactionData;
import com.nirvasoft.fcpayment.core.fcchannel.Object.PaymentType;
import com.nirvasoft.rp.dao.DispaymentTransationDao;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.mgr.QuickPayMgr;
import com.nirvasoft.rp.shared.CMSMerchantData;
import com.nirvasoft.rp.shared.EPIXResponse;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.Response;

public class DisplaymentTransactionTempDataImpl {

	static EPIXResponse ret = new EPIXResponse();
	static DisPaymentTransactionData transdata = new DisPaymentTransactionData();

	static String drccy = "MMK";
	static String crccy = "MMK";
	static CMSMerchantData merchantinfo = new CMSMerchantData();

	public static Response getToAccountByPCode(String processingcode) {
		Response res = new Response();
		String toAccount = "";
		QuickPayMgr qpmgr = new QuickPayMgr();
		try {
			toAccount = qpmgr.getToAccountByPCode(processingcode);
			res.setId(toAccount);
			res.setResponsecode("0000");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			res.setResponsecode("0014");
			res.setResponsedesc(e.getMessage());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			res.setResponsecode("0014");
			res.setResponsedesc(e.getMessage());
		}
		return res;

	}

	public static Response savetmpDislaymentInfo(DisPaymentTransactionData transdata, String processingcode,
			String merchantid, boolean isAccountTransfer, String isAgent) {

		Response response = new Response();
		CMSMerchantData cmsdata = new CMSMerchantData();
		QuickPayMgr qpmgr = new QuickPayMgr();

		try {
			if (!isAccountTransfer) {
				cmsdata = qpmgr.getMerchantIDByPCode(processingcode, merchantid);
				merchantid = cmsdata.getUserId();
				if (merchantid.equals("")) {
					response.setResponsecode("0014");
					response.setResponsedesc("There is no Merchant ID");
					return response;
				}
				merchantinfo = qpmgr.getMerchantInfo(merchantid);
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.setResponsecode("0014");
			response.setResponsedesc(e.getMessage());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.setResponsecode("0014");
			response.setResponsedesc(e.getMessage());
		} // CMSMerchant

		transdata.setCurrencycode(crccy);// currency code
		transdata.setChequeNo("");// cheque no
		transdata.setN2(9);// Posted
		transdata.setN3(4);// for branch
		transdata.setIsUser("A");// for branch
		transdata.setT10("");// Payment Type
		transdata.setN4(PaymentType.AATTYPE);
		transdata.setRecmethod(PaymentType.AAT);
		transdata.setTranstime(GeneralUtil.datetoString() + " " + GeneralUtil.getTime());
		transdata.setTransdate(GeneralUtil.datetoString());
		transdata.setIsVIP("0");
		transdata.setT20(cmsdata.getUserId());// Merchant ID // suwai
		transdata.setT19(cmsdata.getUserName());// Merchant Name // suwai

		if (!merchantid.equals(PaymentType.telenor)) {

			if (merchantinfo.getN1() == 1) {

				if (transdata.getT15().contains(",")) {
					transdata.setT15(transdata.getT15().replace(",", ""));

				}

				if (transdata.getT18().contains(",")) {
					transdata.setT18(transdata.getT18().replace(",", ""));
				}

				if (String.valueOf(transdata.getAmount()).contains(",")) {
					transdata.setAmount(
							Double.parseDouble(String.valueOf(transdata.getAmount()).trim().replace(",", "")));// txn
					// amount
				}
				transdata.setN11(1);
			} else if (merchantinfo.getN1() == 0) {

				if (transdata.getT15().contains(",")) {
					transdata.setT15(transdata.getT15().replace(",", ""));

				}

				if (transdata.getT18().contains(",")) {
					transdata.setT18(transdata.getT18().replace(",", ""));
				}

				if (String.valueOf(transdata.getAmount()).contains(",")) {
					transdata.setAmount(
							Double.parseDouble(String.valueOf(transdata.getAmount()).trim().replace(",", "")));

				}
				transdata.setN11(0);
			}

			// email
			if (merchantinfo.getN2() == 1) {

				transdata.setN12(1);
			} else if (merchantinfo.getN2() == 0) {

				transdata.setN12(0);
			}

			// sms
			if (merchantinfo.getN3() == 1) {

				transdata.setN13(1);
			} else if (merchantinfo.getN3() == 0) {

				transdata.setN13(0);
			}
		}
		// for Agent Transfer
		if (isAccountTransfer) {
			transdata.setN19(Integer.valueOf(isAgent));
		}

		try {
			response = DispaymentTransationDao.postTransation(transdata, "TmpDisPaymentTransaction");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.setResponsecode("0014");
			response.setResponsedesc(e.getMessage());
		}
		return response;

	}
	public Response saveDislaymentInfo(DisPaymentTransactionData ret) throws SQLException {
		Response response = new Response();
		response = DispaymentTransationDao.postTransation(ret, "DisPaymentTransaction");
		return response;

	}
}
