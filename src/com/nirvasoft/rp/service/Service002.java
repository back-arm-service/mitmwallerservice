package com.nirvasoft.rp.service;

import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.data.FAQResponseDataArr;
import com.nirvasoft.rp.data.MessageRequest;
import com.nirvasoft.rp.data.MessageResponse;
import com.nirvasoft.rp.data.QRData;
import com.nirvasoft.rp.data.QRDataResponse;
import com.nirvasoft.rp.framework.ServerSession;
import com.nirvasoft.rp.mgr.ATMLocationMgr;
import com.nirvasoft.rp.mgr.FAQMgr;
import com.nirvasoft.rp.mgr.QRDataMgr;
import com.nirvasoft.rp.mgr.SessionMgr;
import com.nirvasoft.rp.mgr.integration.SMSSettingMgr;
import com.nirvasoft.rp.shared.ATMLocationReqData;
import com.nirvasoft.rp.shared.LocationData;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ServerGlobal;

@Path("/service002")
public class Service002 {
	@Context
	HttpServletRequest request;
	@Context
	private HttpServletResponse response;
	@javax.ws.rs.core.Context
	ServletContext context;

	@POST
	@Path("decryptData")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public QRDataResponse decryptData(QRData data) {
		getPath();
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Decrypt QRData... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
		}
		QRDataResponse qrData = QRDataMgr.decrypt(data.getData());
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Decrypt QRData : ");
			l_err.add("=============================================================================");
			GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
		}
		return qrData;
	}

	@GET
	@Path("getFAQList")
	@Produces(MediaType.APPLICATION_JSON)
	public FAQResponseDataArr getFAQList() {
		getPath();

		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Getting State List.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}

		FAQResponseDataArr faqList = new FAQMgr().getFAQList();

		return faqList;
	}

	// getATMLocation
	@POST
	@Path("getLocation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public LocationData getLocation(ATMLocationReqData atmres) {
		getPath();
		GeneralUtil.readDebugLogStatus();
		// Request log
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("KEY :" + atmres.getId());
			l_err.add("ATM Request Message : ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
		}
		String id = atmres.getId();
		// ATMLocationResData res = new ATMLocationResData();
		LocationData res = new LocationData();
		ATMLocationMgr atMgr = new ATMLocationMgr();
		DAOManager.AbsolutePath = context.getRealPath("");
		res = atMgr.getATMLocation(id);
		// Response log
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("KEY :" + atmres.getId());
			l_err.add("ATM Response Message : " + res.toString());
			l_err.add("=============================================================================");
			GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
		}
		return res;
	}

	private void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
	}

	@POST
	@Path("readMessageSetting")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public MessageResponse readMessageSetting(MessageRequest req) {
		
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("readMessageSetting... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
		}
		MessageResponse res = new MessageResponse();
		ResponseData resdata = new ResponseData();
		resdata = new SessionMgr().updateActivityTime_Old(req.getSessionID(),req.getUserID()); // update
//		if (!resdata.getCode().equalsIgnoreCase("0000")) {
//			res.setCode(resdata.getCode());
//			res.setDesc(resdata.getDesc());
//			return res;
//		}
		MessageResponse rs = readMessageSettingData(req);
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("readMessageSetting : ");
			l_err.add("=============================================================================");
			GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
		}
		return rs;
	}

	public MessageResponse readMessageSettingData(MessageRequest data) {
		SMSSettingMgr mgr = new SMSSettingMgr();
		MessageResponse rs = mgr.readMessageSetting("3", data.getType(), data.getMerchantID(), data.getSessionID(),
				data.getUserID());
		return rs;
	}
	@POST
	@Path("readNotiMessageSetting")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public MessageResponse readNotiMessageSetting(MessageRequest req) {
		SMSSettingMgr mgr = new SMSSettingMgr();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("readMessageSetting... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
		}
		MessageResponse res = new MessageResponse();
		ResponseData resdata = new ResponseData();
//		resdata = new SessionMgr().updateActivityTime_Old(req.getSessionID(),req.getUserID()); // update
//		if (!resdata.getCode().equalsIgnoreCase("0000")) {
//			res.setCode(resdata.getCode());
//			res.setDesc(resdata.getDesc());
//			return res;
//		}
		res=mgr.readNotiMessageSetting(req);
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("readMessageSetting : ");
			l_err.add("=============================================================================");
			GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
		}
		return res;
	}


}
