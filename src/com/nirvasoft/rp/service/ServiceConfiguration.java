package com.nirvasoft.rp.service;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.nirvasoft.cms.mgr.SetupMobileMgr;
import com.nirvasoft.cms.shared.PagerMobileData;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.shared.SetupDataSet;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.framework.ServerSession;
import com.nirvasoft.rp.mgr.UserRegistrationMgr;

@Path("/serviceConfiguration")
public class ServiceConfiguration {
	@Context
	HttpServletRequest request;
	@Context
	private HttpServletResponse response;
	@javax.ws.rs.core.Context
	ServletContext context;

	// Menu Order For Mobile TDA
	@POST
	@Path("getMenuOrderList")
	@Produces(MediaType.APPLICATION_JSON)
	public SetupDataSet getMenuOrderList(PagerMobileData data) {
		getPath();

		SetupDataSet res = new SetupDataSet();

		if (new UserRegistrationMgr().checkUser(data.getUsersk()))
			res = new SetupMobileMgr().getMenuOrderList(data, getUser());

		return res;
	}

	public void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = context.getRealPath("");
	}

	private MrBean getUser() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		MrBean user = new MrBean();
		user.getUser().setOrganizationID("001");
		user.getUser().setUserId(Service001.userid);
		user.getUser().setUserName(Service001.username);
		return user;
	}

	@GET
	@Path("savefordownload")
	public void savefordownload() {
		getPath();
		String version = request.getParameter("version");
		String type = "web";
		new SetupMobileMgr().savefordownload(version, type, getUser());
	}

}
