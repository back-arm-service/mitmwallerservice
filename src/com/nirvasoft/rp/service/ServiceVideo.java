package com.nirvasoft.rp.service;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.nirvasoft.cms.mgr.ArticleMobileMgr;
import com.nirvasoft.cms.mgr.VideoMobileMgr;
import com.nirvasoft.cms.shared.PagerMobileData;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.ResultMobile;
import com.nirvasoft.cms.framework.ServerSession;
import com.nirvasoft.cms.shared.ArticleDataSet;
import com.nirvasoft.rp.dao.DAOManager;

@Path("/serviceVideo")
public class ServiceVideo {
	@Context
	HttpServletRequest request;
	@javax.ws.rs.core.Context
	ServletContext context;

	@GET
	@Path("clickLikeVideo")
	@Produces(MediaType.APPLICATION_JSON)
	public ResultMobile clickLikeVideo() {
		this.getPath();
		ResultMobile ret;
		String key = request.getParameter("key");
		String userSK = request.getParameter("userSK");
		ret = new VideoMobileMgr().clickLikeVideo(Long.parseLong(key), userSK, "Video", getUser());
		return ret;
	}

	@GET
	@Path("clickUnlikeVideo")
	@Produces(MediaType.APPLICATION_JSON)
	public ResultMobile clickUnlikeArticle() {
		this.getPath();
		ResultMobile ret;
		String userSK = "0";
		String key = request.getParameter("key");
		if (request.getParameter("userSK") != null)
			userSK = request.getParameter("userSK");
		ret = new VideoMobileMgr().UnlikeVideo(Long.parseLong(key), Long.parseLong(userSK), getUser());
		return ret;
	}

	/////////////////////////// Mobile///////////////////////

	public void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
	}

	private MrBean getUser() {
		this.getPath();
		MrBean user = new MrBean();
		user.getUser().setOrganizationID("001");
		user.getUser().setUserId(Service001.userid);
		user.getUser().setUserName(Service001.username);
		return user;
	}

	// mobile => video list based on crop and all
	@POST
	@Path("searchVideo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArticleDataSet searchVideo(PagerMobileData p) {
		this.getPath();
		String userSK = "0";
		String searchVal = request.getParameter("searchVal");
		ArticleDataSet res = new ArticleDataSet();
		if (request.getParameter("userSK") != null)
			userSK = request.getParameter("userSK");
		String mobile = request.getParameter("mobile");
		String firstRefresh = request.getParameter("firstRefresh");
		res = new ArticleMobileMgr().searchVideo(p, mobile, firstRefresh, searchVal, userSK, "", getUser());
		return res;
	}

}
