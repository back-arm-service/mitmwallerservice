package com.nirvasoft.rp.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.framework.ServerSession;
import com.nirvasoft.cms.mgr.ArticleMgr;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.shared.ArticleDataSet;
import com.nirvasoft.cms.shared.CommentDataSet;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.rp.dao.DAOManager;

@Path("/serviceArticleAdm")
public class ServiceArticleAdm {
	public static String convertStreamToString(InputStream inStream) throws Exception {
		InputStreamReader inputStream = new InputStreamReader(inStream);
		BufferedReader bReader = new BufferedReader(inputStream);
		StringBuilder sb = new StringBuilder();
		String line = null;
		while ((line = bReader.readLine()) != null) {
			sb.append(line);
		}
		return sb.toString();
	}

	@Context
	HttpServletRequest request;
	@javax.ws.rs.core.Context
	ServletContext context;

	@GET
	@Path("articleLikeList")
	@Produces(MediaType.APPLICATION_JSON)
	public CommentDataSet articleLikeList() {
		String articleSK = request.getParameter("articleSK");
		CommentDataSet ret;
		ret = ArticleMgr.searchLikeUserList(articleSK, getUser());
		return ret;
	}

	////////////// Mobile//////////////////
	// mobile click dislike
	@GET
	@Path("clickDisLikeArticle")
	@Produces(MediaType.APPLICATION_JSON)
	public Resultb2b clickDisLikeArticle() {
		Resultb2b ret;
		String key = request.getParameter("key");// fmr002 syskey
		String userSK = request.getParameter("userSK");
		ret = ArticleMgr.clickDisLikeArticle(Long.parseLong(key), userSK, "news & media", getUser());
		return ret;
	}

	/* for like(contentmenu) */
	@GET
	@Path("clickLikeArticle")
	@Produces(MediaType.APPLICATION_JSON)
	public Resultb2b clickLikeArticle() {
		Resultb2b ret;
		String key = request.getParameter("key");// fmr002 syskey
		String userSK = request.getParameter("userSK");
		ret = ArticleMgr.clickLikeArticle(Long.parseLong(key), userSK, "news & media", getUser());
		return ret;
	}
	
	/* for unlike(contentmenu) */
	@GET
	@Path("clickUnlikeArticle")
	@Produces(MediaType.APPLICATION_JSON)
	public Resultb2b clickUnlikeArticle() {
		Resultb2b ret;
		String userSK = "0";
		String key = request.getParameter("key");
		if (request.getParameter("userSK") != null)
			userSK = request.getParameter("userSK");
		ret = ArticleMgr.UnlikeArticle(Long.parseLong(key), Long.parseLong(userSK), getUser());
		return ret;
	}

	@GET
	@Path("clickLikeComment")
	@Produces(MediaType.APPLICATION_JSON)
	public Resultb2b clickLikeComment() {
		Resultb2b ret;
		String postkey = request.getParameter("postkey");
		String comkey = request.getParameter("comkey");
		String userSK = request.getParameter("userSK");
		String type = request.getParameter("type");
		ret = ArticleMgr.clickLikeComment(Long.parseLong(postkey), Long.parseLong(comkey), Long.parseLong(userSK), type,
				getUser());
		return ret;
	}

	// mobile click undislike
	@GET
	@Path("clickUnDislikeArticle")
	@Produces(MediaType.APPLICATION_JSON)
	public Resultb2b clickUnDislikeArticle() {
		Resultb2b ret;
		String userSK = "0";
		String key = request.getParameter("key");
		if (request.getParameter("userSK") != null)
			userSK = request.getParameter("userSK");
		ret = ArticleMgr.UnDislikeArticle(Long.parseLong(key), Long.parseLong(userSK), getUser());
		return ret;
	}	

	@GET
	@Path("clickUnLikeComment")
	@Produces(MediaType.APPLICATION_JSON)
	public Resultb2b clickUnLikeComment() {
		Resultb2b ret;
		String postkey = request.getParameter("postkey");
		String comkey = request.getParameter("comkey");
		String userSK = request.getParameter("userSK");
		String type = request.getParameter("type");
		ret = ArticleMgr.clickUnLikeComment(Long.parseLong(postkey), Long.parseLong(comkey), Long.parseLong(userSK),
				type, getUser());
		return ret;
	}

	/* Contenmenulist(deletecomment) */
	@POST
	@Path("deleteComment")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArticleDataSet deleteComment(ArticleData p) {
		ArticleDataSet res = new ArticleDataSet();
		res = ArticleMgr.deleteComment(p, getUser());
		return res;
	}

	/* Contenmenulist(getallcomment) */
	@GET
	@Path("getComments")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleDataSet getComments() {
		String id = request.getParameter("id");
		ArticleDataSet res = new ArticleDataSet();
		res = ArticleMgr.getComments(id, getUser());
		return res;
	}

	public void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
	}

	//////////////////// CMS/////////////////////

	private MrBean getUser() {
		getPath();
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		MrBean user = new MrBean();
		user.getUser().setOrganizationID("001");
		user.getUser().setUserId(Service001.userid);
		user.getUser().setUserName(Service001.username);
		return user;
	}

	@GET
	@Path("readVideoBySyskey")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleData readDataByVideo() {
		ArticleData ret;
		String key = request.getParameter("key");
		ret = ArticleMgr.readDataBySyskey(Long.parseLong(key), getUser());
		return ret;
	}

	/* Contenmenulist(savecomment) */
	@POST
	@Path("saveComment")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArticleDataSet saveComment(ArticleData p) {
		ArticleDataSet res = new ArticleDataSet();
		res = ArticleMgr.saveComment(p, getUser());
		return res;
	}

	@GET
	@Path("searchLike")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleDataSet searchLike() {
		long key = Long.parseLong(request.getParameter("key"));
		String type = "News & Media";
		ArticleDataSet ret;
		ret = ArticleMgr.searchLike(type, key, getUser());
		return ret;
	}

	/* Web Question and Answer List */
	@POST
	@Path("searchVideoList")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArticleDataSet searchQuesList(PagerData p) {
		long status = Long.parseLong(request.getParameter("status"));// for save
		String statetype = request.getParameter("statetype");// for combo status
		if (p.getT1() != "" && p.getT1().contains("/")) {
			p.setT1(ServerUtil.datetoString1(p.getT1()));// SearchVal==p.getT1
		}
		ArticleDataSet res = new ArticleDataSet();
		res = ArticleMgr.searchVideoList(p, status, statetype, getUser());
		return res;
	}

	// TDA(VideoUserview)
	@POST
	@Path("searchVideoUserList")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArticleDataSet searchVideoUserList(PagerData p) {
		String searchVal = request.getParameter("searchVal");
		String mobile = request.getParameter("mobile").replace(" ", "+");
		ArticleDataSet res = new ArticleDataSet();
		res = ArticleMgr.searchVideoUserLists(p, mobile, searchVal, getUser());
		return res;
	}

	@GET
	@Path("viewByID")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleDataSet viewByID() {
		String id = request.getParameter("id");
		ArticleDataSet res = new ArticleDataSet();
		res = ArticleMgr.viewArticle(id, getUser());
		return res;
	}

	// mobile (for save content)
	@GET
	@Path("viewByKey")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleDataSet viewByKey() {
		String userSK = "0";
		String id = request.getParameter("id");
		if (request.getParameter("userSK") != null) {
			userSK = request.getParameter("userSK");
		}
		ArticleDataSet res = new ArticleDataSet();
		res = ArticleMgr.viewNews(id, userSK, getUser());
		return res;
	}

}
