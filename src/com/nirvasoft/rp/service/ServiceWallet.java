
package com.nirvasoft.rp.service;

import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.framework.ServerSession;
import com.nirvasoft.rp.mgr.SessionMgr;
import com.nirvasoft.rp.mgr.TransferMgr;
import com.nirvasoft.rp.mgr.WalletMgr;
import com.nirvasoft.rp.mgr.icbs.AccountMgr;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.shared.SessionData;
import com.nirvasoft.rp.shared.TopupReq;
import com.nirvasoft.rp.shared.TopupRes;
import com.nirvasoft.rp.shared.TransferOutReq;
import com.nirvasoft.rp.shared.TransferOutRes;
import com.nirvasoft.rp.shared.WalletCheckRes;
import com.nirvasoft.rp.shared.WalletRequestData;
import com.nirvasoft.rp.shared.WalletResponseData;
import com.nirvasoft.rp.users.data.WalletTransferData;
import com.nirvasoft.rp.util.AesUtil;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ServerGlobal;
import com.nirvasoft.rp.util.ServerUtil;

@Path("/wallet")
public class ServiceWallet {
	@Context
	HttpServletRequest request;
	@Context
	private HttpServletResponse response;
	@javax.ws.rs.core.Context
	ServletContext context;

	@POST
	@Path("checkWallet")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public WalletCheckRes checkWallet(SessionData req) {
		getPath();
		WalletCheckRes res = new WalletCheckRes();

		/*if (req.getUserID() == null || req.getUserID().trim().equals("")) {
			res.setCode("0014");
			res.setDesc("Invalid Phone Number.");
		} else {
			// Request log
			
		}*/
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Getting Balance.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}
		res = new AccountMgr().checkWallet(req);
		return res;
	}

	@POST
	@Path("getBalance")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public WalletResponseData getBalance(WalletRequestData req) {
		getPath();
		WalletResponseData res = new WalletResponseData();
		if (req.getUserID() == null || req.getUserID().trim().equals("")) {
			res.setMessageCode("0014");
			res.setMessageDesc("Invalid Phone Number.");
		} else {
			// Request log
			GeneralUtil.readDebugLogStatus();
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Getting Balance.... ");
				l_err.add("-----------------------------------------------------------------------------");
				l_err.add("Request Data :" + req.toString());
				GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
			}
			ResponseData resdata = new ResponseData();
			/*resdata = new SessionMgr().updateActivityTime_Old(req.getSessionID(),req.getUserID()); // update
			if (!resdata.getCode().equalsIgnoreCase("0000")) {
				res.setMessageCode(resdata.getCode());
				res.setMessageDesc(resdata.getDesc());
				return res;
			}*/
			res = new AccountMgr().getBalance(req);
		}

		return res;
	}
	@POST
	@Path("getBalanceWL")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public WalletResponseData getBalanceWL(WalletRequestData req) {
		getPath();
		WalletResponseData res = new WalletResponseData();
		if (req.getUserID() == null || req.getUserID().trim().equals("")) {
			res.setMessageCode("0014");
			res.setMessageDesc("Invalid Phone Number.");
		} else {
			// Request log
			GeneralUtil.readDebugLogStatus();
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Getting Balance.... ");
				l_err.add("-----------------------------------------------------------------------------");
				l_err.add("Request Data :" + req.toString());
				GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
			}
			ResponseData resdata = new ResponseData();
			resdata = new SessionMgr().updateActivityTime_Old(req.getSessionID(),req.getUserID()); // update
			if (!resdata.getCode().equalsIgnoreCase("0000")) {
				res.setMessageCode(resdata.getCode());
				res.setMessageDesc(resdata.getDesc());
				return res;
			}
			res = new AccountMgr().getBalance(req);
		}

		return res;
	}

	public void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
	}

	@POST
	@Path("paymentUtility")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TransferOutRes paymentUtility(WalletTransferData wdata) {
		getPath();
		GeneralUtil.readDebugLogStatus();
		GeneralUtil.writeDebugLog(wdata.getMessageRequest().getUserID(), "Go Utility Transfer Req : ", "");
		TransferOutRes res = new TransferOutRes();
		try {
			if (wdata.getTransferOutReq().getPassword().equals("")) {
				res.setCode("0014");
				res.setDesc("Invalid Password");
				return res;
			} else {
				SessionMgr session_mgr = new SessionMgr();
				ResponseData resdata = new ResponseData();
				resdata = session_mgr.updateActivityTime_Old(wdata.getTransferOutReq().getSessionID(),
						wdata.getTransferOutReq().getUserID()); // update
				if (!resdata.getCode().equalsIgnoreCase("0000")) {
					res.setCode(resdata.getCode());
					res.setDesc(resdata.getDesc());
					return res;
				}
				String password = wdata.getTransferOutReq().getPassword();
				if (!wdata.getTransferOutReq().getIv().equals("")) {
					AesUtil util = new AesUtil(128, 1000);
					password = util.decrypt(wdata.getTransferOutReq().getSalt(), wdata.getTransferOutReq().getIv(), password);
				}
				wdata.getTransferOutReq().setPassword(ServerUtil.hashPassword(password));
				boolean right = new TransferMgr().checkPassword(wdata.getTransferOutReq().getPassword(),
						wdata.getMessageRequest().getUserID());
				if (right) {
					res = new WalletMgr().paymentUtility(wdata.getTransferOutReq());
				} else {
					res.setCode("0014");
					res.setDesc("Invalid Password");
					return res;
				}
			}
		} catch (Exception e) {
			res.setCode("0014");
			res.setDesc(e.toString());
			return res;
		}

		GeneralUtil.writeDebugLog(wdata.getMessageRequest().getUserID(), "Go Utility Transfer Res : ",
				res.getBankRefNo());
		return res;
	}
	
	@POST
	@Path("walletToAccount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TransferOutRes walletToAccount(WalletTransferData wdata) {
		getPath();
		GeneralUtil.readDebugLogStatus();
		GeneralUtil.writeDebugLog(wdata.getMessageRequest().getUserID(), "Go Utility Transfer Req : ", "");
		TransferOutRes res = new TransferOutRes();
		try {
			/*if (wdata.getTransferOutReq().getPassword().equals("")) {
				res.setCode("0014");
				res.setDesc("Invalid Password");
				return res;
			} else {
				
			}*/
			SessionMgr session_mgr = new SessionMgr();
			ResponseData resdata = new ResponseData();
			/*resdata = session_mgr.updateActivityTime_Old(wdata.getTransferOutReq().getSessionID(),
					wdata.getTransferOutReq().getUserID()); // update
			if (!resdata.getCode().equalsIgnoreCase("0000")) {
				res.setCode(resdata.getCode());
				res.setDesc(resdata.getDesc());
				return res;
			}*/
			/*String password = wdata.getTransferOutReq().getPassword();
			if (!wdata.getTransferOutReq().getIv().equals("")) {
				AesUtil util = new AesUtil(128, 1000);
				password = util.decrypt(wdata.getTransferOutReq().getSalt(), wdata.getTransferOutReq().getIv(), password);
			}
			wdata.getTransferOutReq().setPassword(ServerUtil.hashPassword(password));
			boolean right = new TransferMgr().checkPassword(wdata.getTransferOutReq().getPassword(),
					wdata.getMessageRequest().getUserID());
			if (right) {
				
			} else {
				res.setCode("0014");
				res.setDesc("Invalid Password");
				return res;
			}*/
			res = new WalletMgr().walletToAccount(wdata.getTransferOutReq());
		} catch (Exception e) {
			res.setCode("0014");
			res.setDesc(e.toString());
			return res;
		}

		GeneralUtil.writeDebugLog(wdata.getMessageRequest().getUserID(), "Go Utility Transfer Res : ",
				res.getBankRefNo());
		return res;
	}

	// top up
	@POST
	@Path("topupWallet")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TopupRes topupWallet(TopupReq topupreq) {
		getPath();
		boolean hasError = false;
		TopupRes response = new TopupRes();
		if (topupreq.getAmount() == null || topupreq.getAmount().trim().length() == 0) {
			hasError = true;
		}
		if (topupreq.getAmount() != null && topupreq.getAmount().trim().length() != 0) {
			try {
				topupreq.setAmountdbl(Long.parseLong(topupreq.getAmount().trim()));
			} catch (Exception e) {
				response.setMessageCode("0014");
				response.setMessageDesc("Invalid Amount.");

				return response;
			}
		}
		if (topupreq.getAmountdbl() <= 0) {
			hasError = true;
		}
		if (hasError) {
			response.setMessageCode("0014");
			response.setMessageDesc("Invalid Amount.");
		} else {
			// Request log
			GeneralUtil.readDebugLogStatus();
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("User Registration.... ");
				l_err.add("-----------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
			}
			/*ResponseData resdata = new ResponseData();
			resdata = new SessionMgr().updateActivityTime(topupreq.getSessionID(),
					topupreq.getUserID()); // update
			if (!resdata.getCode().equalsIgnoreCase("0000")) {
				response.setMessageCode(resdata.getCode());
				response.setMessageDesc(resdata.getDesc());
				return response;
			}*/
			response = new WalletMgr().topupWallet(topupreq);
		}

		return response;
	}

	@POST
	@Path("wallettransfer")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TransferOutRes wallettransfer(WalletTransferData wdata) {
		getPath();
		TransferOutRes res = new TransferOutRes();
		// Request log
		GeneralUtil.readDebugLogStatus();
		GeneralUtil.writeDebugLog(wdata.getMessageRequest().getUserID(), "Go Wallet Transfer Req : ", "");
		try {
			SessionMgr session_mgr = new SessionMgr();
			ResponseData resdata = new ResponseData();
			resdata = validateTransferReq(wdata.getTransferOutReq());
			if(resdata.getCode().equals("0000")){
				resdata = session_mgr.updateActivityTime_Old(wdata.getMessageRequest().getSessionID(),
						wdata.getMessageRequest().getUserID()); // update
				if (!resdata.getCode().equalsIgnoreCase("0000")) {
					res.setCode(resdata.getCode());
					res.setDesc(resdata.getDesc());
					return res;
				}
				if (wdata.getTransferOutReq().getPassword().equals("")) {
					res.setCode("0014");
					res.setDesc("Invalid Password");
					return res;
				} else {
					String password = wdata.getTransferOutReq().getPassword();
					if (!wdata.getTransferOutReq().getIv().equals("")) {
						AesUtil util = new AesUtil(128, 1000);
						password = util.decrypt(wdata.getTransferOutReq().getSalt(), wdata.getTransferOutReq().getIv(), password);
					}
					wdata.getTransferOutReq().setPassword(ServerUtil.hashPassword(password));
					boolean right = new TransferMgr().checkPassword(wdata.getTransferOutReq().getPassword(),
							wdata.getMessageRequest().getUserID());
					if (right) {
						res = new TransferMgr().transferOutInternal(wdata.getTransferOutReq());
					} else {
						res.setCode("0014");
						res.setDesc("Invalid Password");
						return res;
					}
				}
			} else {
				res.setCode(resdata.getCode());
				res.setDesc(resdata.getDesc());
			}
		} catch (Exception e) {
			res.setCode("0014");
			res.setDesc(e.toString());
			return res;
		}
		return res;
	}
	
	private ResponseData validateTransferReq(TransferOutReq data){
		ResponseData ret = new ResponseData();
		if(data.getUserID().equalsIgnoreCase(data.getBeneficiaryID())){
			ret.setCode("0014");
			ret.setDesc("Payee and Beneficiary ID should not same!");
		} else {
			ret.setCode("0000");
			ret.setDesc("Success");
		}
		return ret;
	}

}
