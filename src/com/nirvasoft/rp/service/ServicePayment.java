package com.nirvasoft.rp.service;

import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.nirvasoft.mfi.dao.MobileMemberDao;
import com.nirvasoft.rp.core.ccbs.data.db.DBAccountMgr;
//import com.nirvasoft.fcpayment.core.data.fcchannel.DisPaymentTransactionData;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.dao.WalletTransactionDao;
import com.nirvasoft.rp.dao.icbs.GLDao;
import com.nirvasoft.rp.data.CommRes;
import com.nirvasoft.rp.data.MessageRequest;
import com.nirvasoft.rp.data.MessageResponse;
import com.nirvasoft.rp.data.UtilityResponse;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.mgr.MCommRateMappingMgr;
import com.nirvasoft.rp.mgr.MerchantMgr;
import com.nirvasoft.rp.mgr.PaymentMgr;
import com.nirvasoft.rp.mgr.SessionMgr;
import com.nirvasoft.rp.mgr.TransferMgr;
import com.nirvasoft.rp.mgr.UtilityMgr;
import com.nirvasoft.rp.mgr.icbs.AccountMgr;
import com.nirvasoft.rp.mgr.icbs.ICBSMgr;
import com.nirvasoft.rp.shared.AccTransferReq;
import com.nirvasoft.rp.shared.AccountData;
import com.nirvasoft.rp.shared.CMSMerchantData;
import com.nirvasoft.rp.shared.EPIXResponse;
import com.nirvasoft.rp.shared.GoPaymentResponse;
import com.nirvasoft.rp.shared.NotiAdminResponse;
import com.nirvasoft.rp.shared.NotiData;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.shared.SessionData;
import com.nirvasoft.rp.shared.TransferOutReq;
import com.nirvasoft.rp.shared.TransferOutRes;
import com.nirvasoft.rp.users.data.WalletTransferData;
import com.nirvasoft.rp.util.AesUtil;
import com.nirvasoft.rp.util.FileUtil;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ServerGlobal;
import com.nirvasoft.rp.util.ServerUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

@Path("/payment")
public class ServicePayment {
	@Context
	HttpServletRequest request;
	@Context
	private HttpServletResponse response;
	@javax.ws.rs.core.Context
	ServletContext context;
	@POST
	@Path("payMerchant")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TransferOutRes payment(WalletTransferData wdata) {		
		GeneralUtil.writeDebugLog(wdata.getMessageRequest().getUserID(), "Go Payment Transfer Req : ", "");
		TransferOutRes res = new TransferOutRes();
		try {
			SessionMgr session_mgr = new SessionMgr();
			ResponseData resdata = new ResponseData();
			resdata = session_mgr.updateActivityTime_Old(wdata.getTransferOutReq().getSessionID(),
					wdata.getTransferOutReq().getUserID()); // update
			if (!resdata.getCode().equalsIgnoreCase("0000")) {
				res.setCode(resdata.getCode());
				res.setDesc(resdata.getDesc());
				return res;
			}
			res = new PaymentMgr().payment(wdata.getTransferOutReq());
		} catch (Exception e) {
			res.setCode("0014");
			res.setDesc(e.toString());
			return res;
		}
		GeneralUtil.writeDebugLog(wdata.getMessageRequest().getUserID(), "Go Payment Transfer Res : ", res.getBankRefNo());
		return res;
	}
	//Wallet to  Wallet Transfer for Agent
	@POST
	@Path("goTransfer")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GoPaymentResponse goWalletToWalletTransferDeposit(AccTransferReq aReq) {
		System.out.println("Parameter:"+aReq.toString());
		GeneralUtil.writeDebugLog(aReq.getSenderCode(), "Go Wallet Transfer Request Message : ", aReq.toString());
		GoPaymentResponse response = new GoPaymentResponse();
		EPIXResponse ret = new EPIXResponse();	
		MessageRequest messagerequest=new MessageRequest();//ndh
		MessageResponse messageresponse=new MessageResponse();
		NotiData notiRequest = new NotiData();
		NotiAdminResponse notiResponse=new NotiAdminResponse();
		new ResponseData();	
		ICBSMgr dbmgr = new ICBSMgr();	
		Connection l_Conn = null;
		Connection l_ConniCBS = null;	
		DAOManager l_Dao = new DAOManager();
		boolean isDrGL=false, isCrGL=false;
		AccountData fromAccData = new AccountData();
		AccountData toAccData = new AccountData();
		SessionMgr mgr = new SessionMgr();
		EPIXResponse epiRes = new EPIXResponse();
		boolean right =true;
		ResponseData chk_res = new ResponseData();
		/*if(aReq.getAppType().equals("wallet")){
			if(!aReq.getPassword().equals("")){
			String password = aReq.getPassword();
			if (!aReq.getIv().equals("")) {
				AesUtil util = new AesUtil(128, 1000);
				password = util.decrypt(aReq.getSalt(), aReq.getIv(), password);
			}
			aReq.setPassword(ServerUtil.hashPassword(password));
			 right = new TransferMgr().checkPassword(aReq.getPassword(),
					aReq.getSenderCode());
		     }
		}*/
		if (right) {
			try {
				System.out
						.println("--------------------------------Start----------------------------------------------");
				System.out.println("Start " + GeneralUtil.getDateTime());

				l_Conn = ConnAdmin.getConn();
				l_ConniCBS = l_Dao.openICBSConnection();
				// get From Account
				System.out.println("Sender Code : " + aReq.getSenderCode());
				new DBAccountMgr();
				fromAccData = DBAccountMgr.getAccountWallet("", aReq.getSenderCode(), ServerGlobal.getmMinBalSetting(),
						l_ConniCBS);
				aReq.setFromAccount(fromAccData.getAccountNumber());
				// check daily limit,minimum limit
				epiRes = new TransferMgr().checkTransLimit(fromAccData.getAccountNumber(), "",
						Double.parseDouble(aReq.getAmount()));
				if (!epiRes.getCode().equals("0000")) {
					response.setCode(epiRes.getCode());
					response.setDesc(epiRes.getDesc());
					return response;
				}
				// get To Account
				System.out.println("Sender Code : " + aReq.getReceiverCode());
				toAccData = DBAccountMgr.getAccountWallet("", aReq.getReceiverCode(), ServerGlobal.getmMinBalSetting(),
						l_ConniCBS);
				aReq.setToAccount(toAccData.getAccountNumber());
				System.out.println("Save Displayment info " + GeneralUtil.getDateTime());
				String env = "icbs";
				if (env.equalsIgnoreCase("icbs")) {
					System.out.println("readSystemData " + GeneralUtil.getDateTime());
					System.out.println("start doAccountTransfer " + GeneralUtil.getDateTime());
					ret = dbmgr.doAccountTransferNew(aReq, aReq.getNarrative(), l_ConniCBS, isDrGL, isCrGL, fromAccData,
							toAccData, "1", l_Conn);
					System.out.println("end doAccountTransfer " + GeneralUtil.getDateTime());
					if (ret.getCode().equals("0000")) {
						response.setCode("0000");
						response.setDesc("Wallet transferred successfully");
						response.setBankRefNumber(ret.getFlexcubeid());
						response.setBankTaxRefNumber(ret.getParam2());
						String transDate = GeneralUtil.datetoString();
						response.setTransactionDate(transDate);
						response.setEffectiveDate(ret.getEffectiveDate());
						response.setActualDate(GeneralUtil.getDateYYYYMMDDSimple());
						response.setCardExpire("");
						new WalletTransactionDao()
								.saveWalletTrans(
										new TransferMgr().updateWalletDataCredit(aReq, response.getBankRefNumber(),
												response.getTransactionDate(), "", "1", ret.getEffectiveDate()),
										l_Conn);
						response.setCode("0000");
						response.setDesc("Wallet transferred successfully");
						messagerequest.setUserID(aReq.getSenderCode());
						messagerequest.setSessionID(aReq.getToken());
						messagerequest.setMerchantID("");
						messagerequest.setServiceType("4");
						messagerequest.setType("12");
						messageresponse = new Service002().readNotiMessageSetting(messagerequest);
						if (messageresponse.getrKey().equals("true")) {
							notiRequest.setT1("");
							notiRequest.setTitle("Wallet Transfer");
							notiRequest.setSessionid(aReq.getToken());
							notiRequest.setUserid(aReq.getSenderCode());
							notiRequest.setReceiverID(aReq.getReceiverCode());
							notiRequest.setType("2");
							notiRequest.setAutokey("0");
							notiRequest.setTypedescription("token");
							notiRequest.setNotiType("transfer");
							notiRequest.setServiceType("4");
							notiRequest.setFunctionType("12");
							notiRequest.setAmount(aReq.getAmount());
							notiRequest.setRefNo(response.getBankRefNumber());
							notiRequest.setName(aReq.getFromName());
							notiRequest.setMerchantName("");
							notiRequest.setTransactionType("transfer");
							notiRequest.setReceiverName(aReq.getToName());
							notiResponse = new Service007().sentNoti(notiRequest);
						} else {
							System.out.println("No noti Service");
						}

					} else {
						response.setCode(ret.getCode());
						response.setDesc(ret.getDesc());
					}
				}
				GeneralUtil.writeDebugLog(aReq.getSenderCode(), "go Wallet Transfer Response Message : ",
						response.toString());
				System.out.println("end sendMessageService  " + GeneralUtil.getDateTime());
				System.out.println("----------------------------------END--------------------------------------------");
			} catch (Exception e) {
				e.printStackTrace();
				response.setCode("0014");
				response.setDesc("Internal Error");
				GeneralUtil.writeDebugLog(aReq.getSenderCode(), "go Wallet Transfer Response Message : ",
						response.toString());
				return response;
			}

			finally {
				try {
					if (!l_Conn.isClosed())
						l_Conn.close();
					if (!l_ConniCBS.isClosed())
						l_ConniCBS.close();
				} catch (SQLException e) {
					e.printStackTrace();
					response.setCode("0014");
					response.setDesc("Internal Error");
					GeneralUtil.writeDebugLog(aReq.getSenderCode(), "go Wallet Transfer Response Message : ",
							e.getMessage());

				}
			}

		} else {
			response.setCode("0014");
			response.setDesc("Invalid Password");
			return response;
		}
		
		return response;
	}
	
	@POST
	@Path("goMerchantPayment")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GoPaymentResponse goMerchantPaymentTransferDeposit(AccTransferReq aReq) {
		// Request log
		System.out.println("Parameter:" + aReq.toString());
		GeneralUtil.writeDebugLog(aReq.getSenderCode(), "Go Merchant Payment Transfer Request : ", aReq.toString());
		GoPaymentResponse response = new GoPaymentResponse();
		EPIXResponse ret = new EPIXResponse();
		MessageRequest messagerequest=new MessageRequest();//ndh
		MessageResponse messageresponse=new MessageResponse();
		NotiData notiRequest = new NotiData();
		NotiAdminResponse notiResponse=new NotiAdminResponse();
		new ResponseData();
		ICBSMgr dbmgr = new ICBSMgr();
		Connection l_Conn = null;
		Connection l_ConniCBS = null;
		DAOManager l_Dao = new DAOManager();
		GLDao l_GlDao = new GLDao();
		EPIXResponse epiRes = new EPIXResponse();
		MerchantMgr merchantMgr = new MerchantMgr();
		String transtype = "", toAccount = "";
		boolean isDrGL = false, isCrGL = false;
		boolean right = true;
		AccountData fromAccData = new AccountData();
		AccountData toAccData = new AccountData();
		SessionMgr mgr = new SessionMgr();
		ResponseData chk_res = new ResponseData();
		if (!aReq.getPassword().equals("")) {
			String password = aReq.getPassword();// ndh
			if (!aReq.getIv().equals("")) {
				AesUtil util = new AesUtil(128, 1000);
				password = util.decrypt(aReq.getSalt(), aReq.getIv(), password);
			}
			aReq.setPassword(ServerUtil.hashPassword(password));
			right = new TransferMgr().checkPassword(aReq.getPassword(), aReq.getSenderCode());
		}
		try {
			if (right) {				
				chk_res = mgr.updateActivityTime_Old(aReq.getToken(), aReq.getSenderCode());				
				if (chk_res.getCode().equals("0000")) {
					System.out.println(
							"--------------------------------Start----------------------------------------------");
					System.out.println("Start " + GeneralUtil.getDateTime());
					l_Conn = ConnAdmin.getConn();
					l_ConniCBS = l_Dao.openICBSConnection();

					// String bankcomm = new
					// FileUtil().readUrlString("BankComm");
					// aReq.setBankCharges(bankcomm);
					// From Account
					if (aReq.getFromAccount().equals("")) {
						System.out.println("Sender : " + aReq.getSenderCode());
						new DBAccountMgr();
						fromAccData = DBAccountMgr.getAccountWallet("", aReq.getSenderCode(),
								ServerGlobal.getmMinBalSetting(), l_ConniCBS);
						aReq.setFromAccount(fromAccData.getAccountNumber());

					}
					// check daily limit,trans limit
					/*epiRes = new TransferMgr().checkTransLimit(fromAccData.getAccountNumber(), "",
							Double.parseDouble(aReq.getAmount()));
					if (!epiRes.getCode().equals("0000")) {
						response.setCode(epiRes.getCode());
						response.setDesc(epiRes.getDesc());
						return response;
					}*/
					// To Account
					CMSMerchantData merchantdata =new CMSMerchantData();
					if (aReq.getToAccount().equals("")) {
						merchantdata = merchantMgr.getMerchantAccount(aReq.getMerchantID());
						toAccount = merchantdata.getAccountNumber();
						aReq.setToName(merchantdata.getBranchname());
						if(toAccount.contains("+959")){
							toAccData = DBAccountMgr.getAccountWallet("", toAccount, ServerGlobal.getmMinBalSetting(),
									l_ConniCBS);
							aReq.setToAccount(toAccData.getAccountNumber());
						}else{
							aReq.setToAccount(toAccount);
						}
					}
					if (l_GlDao.isGLAccount(toAccount, l_ConniCBS)) {
						isDrGL = false;
						isCrGL = true;
					}

					String env = "icbs";
					if (env.equalsIgnoreCase("icbs")) {
						ret = dbmgr.doAccountTransferNew(aReq, aReq.getRefNo(), l_ConniCBS, isDrGL, isCrGL, fromAccData,
								toAccData, "2", l_Conn);
						if (ret.getCode().equals("0000")) {
							response.setCode("0000");
							response.setDesc("Merchant  transferred successfully");
							response.setBankRefNumber(ret.getFlexcubeid());
							response.setBankTaxRefNumber(ret.getParam2());
							String transDate = GeneralUtil.datetoString();
							response.setTransactionDate(transDate);
							response.setEffectiveDate(ret.getEffectiveDate());
							response.setCardExpire("");
							//response.setBankCharges(ret.getComm1());
							//response.setPenalty(ret.getComm2());
							/*if(aReq.getMerchantID().equals("M00019")){
								aReq.setBankCharges(ret.getComm1());//atn
								aReq.setPenalty(ret.getComm2());
							}*/
							UtilityResponse utilityRes = new UtilityResponse();
							System.out.println("start saveWalletTrans  " + GeneralUtil.getDateTime());
							new WalletTransactionDao()
									.saveWalletTrans(
											new TransferMgr().updateWalletDataCredit(aReq, response.getBankRefNumber(),
													response.getTransactionDate(), "","2",ret.getEffectiveDate()),l_Conn);
							response.setCode("0000");
							response.setDesc("Merchant transferred successfully");
							if (ret.getCode().equals("0000") && aReq.getMerchantID().equals("M00012") && !toAccount.contains("+959")) {
								response = SkynetAccountDataPrepare(aReq, response.getBankRefNumber());
							}else if (ret.getCode().equals("0000") && aReq.getMerchantID().equals("M00012")){
								response = SkynetCallAPI(aReq, response.getBankRefNumber(),response.getTransactionDate());
							}else if (ret.getCode().equals("0000") && aReq.getMerchantID().equals("M00019") && !toAccount.contains("+959")){
								utilityRes = UtilityPaymentAPI(aReq, response.getBankRefNumber());
								new WalletTransactionDao().updateForUtility(utilityRes,aReq.getSenderCode(),response.getBankRefNumber(), l_Conn);
							}else if (ret.getCode().equals("0000") && aReq.getMerchantID().equals("M00019")){
								UtilityMgr utilityMgr = new UtilityMgr();								
								utilityRes = utilityMgr.callCCDAPI(aReq,response.getBankRefNumber(),response.getBankTaxRefNumber(),response.getEffectiveDate());
								new WalletTransactionDao().updateForUtility(utilityRes,aReq.getSenderCode(),response.getBankRefNumber(), l_Conn);							
							}
							messagerequest.setUserID(aReq.getSenderCode());
							messagerequest.setSessionID(aReq.getToken());
							messagerequest.setMerchantID("");
							messagerequest.setServiceType("4");
							messagerequest.setType("1");
							messageresponse=new Service002().readNotiMessageSetting(messagerequest);
							if(messageresponse.getrKey().equals("true")){
								notiRequest.setT1("");
								notiRequest.setTitle("Payment");
								notiRequest.setSessionid(aReq.getToken());
								notiRequest.setUserid(aReq.getSenderCode());
								notiRequest.setReceiverID(aReq.getMerchantID());
								notiRequest.setType("2");
								notiRequest.setAutokey("0");
								notiRequest.setTypedescription("token");
								notiRequest.setNotiType("transfer");
								notiRequest.setServiceType("4");
								notiRequest.setFunctionType("1");
								notiRequest.setAmount(aReq.getAmount());
								notiRequest.setRefNo(response.getBankRefNumber());
								notiRequest.setName(aReq.getFromName());
								notiRequest.setTransactionType("payment");
//								notiRequest.setMerchantName("");
								
//								notiRequest.setReceiverName("");
								notiResponse=new Service007().sentNoti(notiRequest);
							}
							else{
								System.out.println("No noti Service");
							}
							

						} else {
							response.setCode(ret.getCode());
							response.setDesc(ret.getDesc());
						}
					}
				} else {
					response.setCode(chk_res.getCode());
					response.setDesc(chk_res.getDesc());
					GeneralUtil.writeDebugLog(aReq.getSenderCode(), "go Merchant Payment  Transfer Response Message : ",
							response.toString());
					return response;
				}
			} else {
				response.setCode("0015");
				response.setDesc("Invalid Password");
				return response;
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode("0014");
			response.setDesc("Internal Error");
			GeneralUtil.writeDebugLog(aReq.getSenderCode(), "go Merchant Payment Response Message : ",
					response.toString());
			return response;
		}/* finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
				if (!l_ConniCBS.isClosed())
					l_ConniCBS.close();
			} catch (SQLException e) {
				e.printStackTrace();
				response.setCode("0016");
				response.setDesc("Internal Error");
				GeneralUtil.writeDebugLog(aReq.getSenderCode(), "go Merchant Payment  connection Message : ",
						e.getMessage());
			}
		}*/
		return response;
	}
	
	@SuppressWarnings("unchecked")
	public GoPaymentResponse SkynetCallAPI(AccTransferReq req,String transID,String transDate) {
		GoPaymentResponse res = new GoPaymentResponse();
		GeneralUtil.readMBServiceSetting();
		String aUrl = ServerGlobal.getMbURL() + "serviceWalletApp/SkynetAPICalling";		
		ClientConfig cc = new DefaultClientConfig();
		cc.getClasses().add(JacksonJsonProvider.class);
		Client client = Client.create(cc);
		WebResource webResource = client.resource(aUrl);
		ClientResponse response = null;
		JSONObject obj = new JSONObject();
		obj.put("packageName",req.getPackageName());
		obj.put("voucherType",req.getVoucherType());
		obj.put("subscriptionno",req.getSubscriptionno());
		obj.put("packagetype",req.getPackagetype());
		
		obj.put("moviecode",req.getMoviecode());
		obj.put("moviename",req.getMoviename());
		obj.put("startdate",req.getStartdate());
		obj.put("enddate",req.getEnddate());
		obj.put("usage_service_catalog_identifier__id",req.getUsage_service_catalog_identifier__id());
		
		try {
			response = webResource.type("application/json").post(ClientResponse.class, obj);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		String jsonStr = response.getEntity(String.class);
		JSONParser parser = new JSONParser();
		Object obj1 = null;
		Connection l_Conn = null;
		
			try {
				obj1 = parser.parse(jsonStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			JSONObject jsonObject = (JSONObject) obj1;
			res.setCode((String) jsonObject.get("code"));
			res.setOtherResponseCode((String) jsonObject.get("skynetC.[ode"));
			res.setOtherResponseDesc((String) jsonObject.get("skynetDesc"));
			if (!res.getCode().equals("0000")) {
				res.setCode("0014");
				res.setDesc("Skynet Transaction Fail");
			}else {
				l_Conn =ConnAdmin.getConn();
				try {
					new WalletTransactionDao().updateForSkynet(res,req.getBankCharges(),req.getCardNo(),req.getPackageName(),req.getVoucherType(),req.getSenderCode(),transID, l_Conn);
					res.setBankRefNumber(transID);
					res.setTransactionDate(transDate);
					res.setCode("0000");
					res.setDesc("Update Wallet Transaction Successfully");
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
			}
		
		return res;
	}
	
	@SuppressWarnings("unchecked")
	public UtilityResponse UtilityPaymentAPI(AccTransferReq req,String transID) {
		UtilityResponse res = new UtilityResponse();
		GeneralUtil.readMBServiceSetting();
		String aUrl = ServerGlobal.getMbURL() + "serviceWalletApp/goUtilityTransfer";		
		ClientConfig cc = new DefaultClientConfig();
		cc.getClasses().add(JacksonJsonProvider.class);
		Client client = Client.create(cc);
		WebResource webResource = client.resource(aUrl);
		ClientResponse response = null;
		JSONObject obj = new JSONObject();
		obj.put("userID",req.getSenderCode());
		obj.put("amount",req.getCurrentAmount());
		//obj.put("bankCharges", req.getBankCharges());
		obj.put("totalAmount", req.getCurrentAmount());
		obj.put("penalty", req.getPenalty());
		obj.put("narrative",req.getNarrative());
		obj.put("refNo",req.getRefNo());
		obj.put("billId",req.getBillId());
		obj.put("cusName",req.getCusName());
		obj.put("ccyCode","MMK");
		obj.put("deptName",req.getDeptName());
		obj.put("taxDesc",req.getTaxDesc());
		obj.put("t1","");
		obj.put("t2","");
		obj.put("dueDate",req.getDueDate());
		obj.put("paidBy","mWallet");
		obj.put("vendorCode",req.getVendorCode());
		obj.put("merchantID","M00001");
		obj.put("belateday",req.getBelateday());
		obj.put("fromName",req.getFromName());
		
		try {
			response = webResource.type("application/json").post(ClientResponse.class, obj);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		String jsonStr = response.getEntity(String.class);
		JSONParser parser = new JSONParser();
		Object obj1 = null;
		Connection l_Conn = null;
		try {
			obj1 = parser.parse(jsonStr);
			JSONObject jsonObject = (JSONObject) obj1;
			res.setCode((String) jsonObject.get("code"));
			res.setDesc((String) jsonObject.get("desc"));
			res.setiCBStxnRef((String) jsonObject.get("bankRefNumber"));
			res.setEffectiveDate((String) jsonObject.get("effectiveDate"));
			res.setStatus((String)( jsonObject.get("OtherStatus")));
			if (!res.getCode().equals("0000")) {
				res.setCode("0014");
				res.setDesc("Skynet Transaction Fail");
			}else {
				l_Conn =ConnAdmin.getConn();								
				res.setCode("0000");
				res.setDesc("Update Wallet Transaction Successfully");
				
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return res;
	}
	
	@SuppressWarnings("unchecked")
	public GoPaymentResponse SkynetAccountDataPrepare(AccTransferReq req,String transID) {
		GoPaymentResponse res = new GoPaymentResponse();
		GeneralUtil.readMBServiceSetting();
		String aUrl = ServerGlobal.getMbURL() + "serviceWalletApp/goSkynetTransfer";		
		ClientConfig cc = new DefaultClientConfig();
		cc.getClasses().add(JacksonJsonProvider.class);
		Client client = Client.create(cc);
		WebResource webResource = client.resource(aUrl);
		ClientResponse response = null;
		JSONObject obj = new JSONObject();
		obj.put("userID", req.getSenderCode());
		obj.put("merchantID", "M00003");
		obj.put("amount", req.getCurrentAmount());
		//obj.put("bankCharges", req.getBankCharges());
		obj.put("totalAmount",req.getCurrentAmount());
		obj.put("ccyCode","MMK");
		obj.put("paidBy","mWallet");
		obj.put("userName",req.getFromName());
		
		obj.put("cardNo",req.getCardNo());
		obj.put("packageName",req.getPackageName());
		obj.put("voucherType",req.getVoucherType());
		obj.put("subscriptionno",req.getSubscriptionno());
		obj.put("packagetype",req.getPackagetype());
		
		obj.put("moviecode",req.getMoviecode());
		obj.put("moviename",req.getMoviename());
		obj.put("startdate",req.getStartdate());
		obj.put("enddate",req.getEnddate());
		obj.put("usage_service_catalog_identifier__id",req.getUsage_service_catalog_identifier__id());
		
		try {
			response = webResource.type("application/json").post(ClientResponse.class, obj);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		String jsonStr = response.getEntity(String.class);
		JSONParser parser = new JSONParser();
		Object obj1 = null;
		Connection l_Conn = null;
		try {
			obj1 = parser.parse(jsonStr);
			JSONObject jsonObject = (JSONObject) obj1;
			res.setCode((String) jsonObject.get("code"));
			res.setOtherResponseCode((String) jsonObject.get("skynetCode"));
			res.setOtherResponseDesc((String) jsonObject.get("skynetDesc"));
			res.setBankRefNumber((String) jsonObject.get("bankRefNumber"));
			res.setTransactionDate((String) jsonObject.get("transactionDate"));
			if (!res.getCode().equals("0000")) {
				res.setCode("0014");
				res.setDesc("Skynet Transaction Fail");
			}else {
				l_Conn =ConnAdmin.getConn();
				if (req.getVoucherType().equals("")){
					req.setPackageName(req.getMoviename());
					req.setVoucherType("Weekly");
				}else{
					String vouchertype = req.getVoucherType().substring(req.getVoucherType().indexOf('-')+1);
					req.setVoucherType(vouchertype);
				}
				new WalletTransactionDao().updateForSkynet(res,req.getBankCharges(),req.getCardNo(),req.getPackageName(),req.getVoucherType(),req.getSenderCode(),transID, l_Conn);
				res.setCode("0000");
				res.setDesc("Update Wallet Transaction Successfully");
				
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return res;
	}
	
	@POST
	@Path("goAgentTransferDeposit")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GoPaymentResponse goAgentTransferDeposit(AccTransferReq aReq) {
		// Request log
		System.out.println("Parameter:" + aReq.toString());
		GeneralUtil.writeDebugLog(aReq.getReceiverCode(), "Go Agent to Transfer Request : ", aReq.toString());

		GoPaymentResponse response = new GoPaymentResponse();
		EPIXResponse ret = new EPIXResponse();
		ICBSMgr dbmgr = new ICBSMgr();
		MessageRequest messagerequest=new MessageRequest();
		NotiData notiRequest = new NotiData();
		NotiAdminResponse notiResponse=new NotiAdminResponse();
		MessageResponse messageresponse=new MessageResponse();
		Connection l_Conn = null;
		Connection l_ConniCBS = null;
		DAOManager l_Dao = new DAOManager();
		// GLDao l_GlDao = new GLDao();
		String toName = "";
		boolean isDrGL = true;
		boolean isCrGL = false;
		AccountData fromAccData = new AccountData();
		AccountData toAccData = new AccountData();
		SessionMgr mgr = new SessionMgr();
		ResponseData chk_res = new ResponseData();
		new FileUtil();
		try {
			String hashString =aReq.getReceiverCode()+aReq.getRefNo()+aReq.getAmount();
			String hashValue = new FileUtil().hmacSha1(hashString,"efa0db82bdf2a7f08505fe1e5b7603de34991a83");
			//chk_res = mgr.updateActivityTime(aReq.getToken(), aReq.getSenderCode());
			// if (chk_res.getCode().equals("0000")) {
			if(hashValue.equals(aReq.getHashValue())){						
			System.out.println("--------------------------------Start----------------------------------------------");
			System.out.println("Start " + GeneralUtil.getDateTime());
			l_Conn = ConnAdmin.getConn();
			l_ConniCBS = l_Dao.openICBSConnection();
			// Sender GL
			aReq.setFromAccount(ServerGlobal.getmWalletGL());
			// Receiver Account
			new DBAccountMgr();
			toAccData = DBAccountMgr.getAccountWallet("", aReq.getReceiverCode(), ServerGlobal.getmMinBalSetting(),
					l_ConniCBS);
			aReq.setToAccount(toAccData.getAccountNumber());
			// Receiver Name
			toName = new TransferMgr().goToName(aReq.getReceiverCode(), l_Conn);
			aReq.setToName(toName);

			String env = "icbs";
				if (env.equalsIgnoreCase("icbs")) {
					if (!aReq.getFromAccount().equals("")) {
						ret = dbmgr.doAccountTransferNew(aReq, aReq.getRefNo(), l_ConniCBS, isDrGL, isCrGL, fromAccData,
								toAccData, "4", l_Conn);
						if (ret.getCode().equals("0000")) {
							response.setCode("0000");
							response.setDesc("Wallet Topup transferred successfully");
							response.setBankRefNumber(ret.getFlexcubeid());
							response.setBankTaxRefNumber(ret.getParam2());
							String transDate = GeneralUtil.datetoString();
							response.setTransactionDate(transDate);
							response.setEffectiveDate(ret.getEffectiveDate());
							response.setCardExpire("");
							System.out.println("start saveWalletTrans  " + GeneralUtil.getDateTime());
							new WalletTransactionDao().saveWalletTrans(
									new TransferMgr().updateWalletDataCredit(aReq, response.getBankRefNumber(),
											response.getTransactionDate(), "", "4", ret.getCharges()),
									l_Conn);
							System.out.println("end saveWalletTrans  " + GeneralUtil.getDateTime());
							response.setCode("0000");
							response.setDesc("Wallet Topup transferred successfully");
							messagerequest.setUserID(aReq.getSenderCode());
							messagerequest.setSessionID(aReq.getToken());
							messagerequest.setMerchantID("");
							messagerequest.setServiceType("4");
							messagerequest.setType("12");
							messageresponse=new Service002().readNotiMessageSetting(messagerequest);
							if(messageresponse.getrKey().equals("true")){
								notiRequest.setT1("");
								notiRequest.setTitle("Wallet Transfer");
								notiRequest.setSessionid(aReq.getToken());
								notiRequest.setUserid(aReq.getAgentAccount());
								notiRequest.setReceiverID(aReq.getReceiverCode());
								notiRequest.setType("2");
								notiRequest.setAutokey("0");
								notiRequest.setTypedescription("token");
								notiRequest.setNotiType("transfer");
								notiRequest.setServiceType("4");
								notiRequest.setFunctionType("12");
								notiRequest.setAmount(aReq.getAmount());
								notiRequest.setRefNo(response.getBankRefNumber());
								notiRequest.setName(aReq.getFromName());
								notiRequest.setMerchantName("");
								notiRequest.setTransactionType("transfer");
								notiRequest.setReceiverName(aReq.getToName());
								notiResponse=new Service007().sentNoti(notiRequest);
							}
							else{
								System.out.println("No noti Service");
							}
							

						} else {
							response.setCode(ret.getCode());
							response.setDesc(ret.getDesc());
						}
					}else{
						response.setCode("0014");
						response.setDesc("acount number not found");
					}
				}
			}else{
				response.setCode("0014");
				response.setDesc("Invalid Data");
			}
			GeneralUtil.writeDebugLog(aReq.getReceiverCode(), "go Wallet Topup Transfer Response Message : ",
					response.toString());
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode("0014");
			response.setDesc("Internal Error");
			GeneralUtil.writeDebugLog(aReq.getReceiverCode(), "go Wallet Topup Transfer Response Message : ",
					response.toString());
			return response;
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
				if (!l_ConniCBS.isClosed())
					l_ConniCBS.close();
			} catch (SQLException e) {
				e.printStackTrace();
				response.setCode("0014");
				response.setDesc("Internal Error");
				GeneralUtil.writeDebugLog(aReq.getReceiverCode(), "go Internal Transfer connection Message : ",
						e.getMessage());
			}

		}
		return response;
	}

	@POST
	@Path("goWalletTransferToAccount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GoPaymentResponse goWalletTransferToAccount(AccTransferReq aReq) {
		// Request log
		System.out.println("Parameter:" + aReq.toString());
		GeneralUtil.writeDebugLog(aReq.getSenderCode(), "Go Agent to Transfer Request : ", aReq.toString());

		GoPaymentResponse response = new GoPaymentResponse();
		EPIXResponse ret = new EPIXResponse();
		ICBSMgr dbmgr = new ICBSMgr();
		Connection l_Conn = null;
		Connection l_ConniCBS = null;
		DAOManager l_Dao = new DAOManager();
		// GLDao l_GlDao = new GLDao();
		boolean isDrGL = false;
		boolean isCrGL = true;
		AccountData fromAccData = new AccountData();
		AccountData toAccData = new AccountData();
		SessionMgr mgr = new SessionMgr();
		ResponseData chk_res = new ResponseData();
		new FileUtil();
		try {
			if (aReq.getApkType().equals("mWallet")) {
				chk_res = mgr.updateActivityTime_Old(aReq.getToken(), aReq.getSenderCode());
			} else {
				chk_res.setCode("0000");
			}
			String hashString = aReq.getSenderCode() + aReq.getRefNo() + aReq.getAmount();
			String hashValue = new FileUtil().hmacSha1(hashString, "efa0db82bdf2a7f08505fe1e5b7603de34991a83");
			if (chk_res.getCode().equals("0000")) {
				if (hashValue.equals(aReq.getHashValue())) {
					System.out.println(
							"--------------------------------Start----------------------------------------------");
					System.out.println("Start " + GeneralUtil.getDateTime());
					l_Conn = ConnAdmin.getConn();
					l_ConniCBS = l_Dao.openICBSConnection();
					// Sender Account
					new DBAccountMgr();
					fromAccData = DBAccountMgr.getAccountWallet("", aReq.getSenderCode(),
							ServerGlobal.getmMinBalSetting(), l_ConniCBS);
					aReq.setFromAccount(fromAccData.getAccountNumber());
					// Receiver Account
					aReq.setToAccount(ServerGlobal.getmWalletGL());
					String env = "icbs";
					if (env.equalsIgnoreCase("icbs")) {
						if (!aReq.getFromAccount().equals("")) {
							ret = dbmgr.doAccountTransferNew(aReq, aReq.getRefNo(), l_ConniCBS, isDrGL, isCrGL,
									fromAccData, toAccData, "5", l_Conn);
							if (ret.getCode().equals("0000")) {
								response.setCode("0000");
								response.setDesc("Wallet Topup transferred successfully");
								response.setBankRefNumber(ret.getFlexcubeid());
								response.setBankTaxRefNumber(ret.getParam2());
								String transDate = GeneralUtil.datetoString();
								response.setTransactionDate(transDate);
								response.setEffectiveDate(ret.getEffectiveDate());
								response.setCardExpire("");
								System.out.println("start saveWalletTrans  " + GeneralUtil.getDateTime());
								new WalletTransactionDao().saveWalletTrans(
										new TransferMgr().updateWalletDataCredit(aReq, response.getBankRefNumber(),
												response.getTransactionDate(), "", "5", ret.getCharges()),
										l_Conn);
								System.out.println("end saveWalletTrans  " + GeneralUtil.getDateTime());
								response.setCode("0000");
								response.setDesc("Wallet Topup transferred successfully");

							} else {
								response.setCode(ret.getCode());
								response.setDesc(ret.getDesc());
							}
						} else {
							response.setCode("0014");
							response.setDesc("acount number not found");
						}
					}
				} else {
					response.setCode("0014");
					response.setDesc("Invalid Data");
				}
				GeneralUtil.writeDebugLog(aReq.getReceiverCode(), "Go Wallet to Account Transfer Response Message : ",
						response.toString());
			} else {
				response.setCode(chk_res.getCode());
				response.setDesc(chk_res.getDesc());
				GeneralUtil.writeDebugLog(aReq.getSenderCode(), "go Merchant Payment  Transfer Response Message : ",
						response.toString());
				return response;
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode("0014");
			response.setDesc("Internal Error");
			GeneralUtil.writeDebugLog(aReq.getReceiverCode(), "Go Wallet to Account Transfer Response Message : ",
					response.toString());
			return response;
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
				if (!l_ConniCBS.isClosed())
					l_ConniCBS.close();
			} catch (SQLException e) {
				e.printStackTrace();
				response.setCode("0014");
				response.setDesc("Internal Error");
				GeneralUtil.writeDebugLog(aReq.getReceiverCode(), "go Internal Transfer connection Message : ",
						e.getMessage());
			}

		}
		return response;
	}
	
	@POST
	@Path("calculateComm")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public CommRes calculateComm(TransferOutReq obj) {
		CommRes res = new CommRes();
		SessionMgr session_mgr = new SessionMgr();
		ResponseData resdata = new ResponseData();
		MCommRateMappingMgr mCommRateMgr = new MCommRateMappingMgr();
		resdata = session_mgr.updateActivityTime_Old(obj.getSessionID(),obj.getUserID()); 		
		if (resdata.getCode().equalsIgnoreCase("0000")) {
			res = mCommRateMgr.getAgentComm(obj.getMerchantID(),"",obj.getAmount());
		}else{
			return res;
		}
		return res;
	}
	
	@POST
	@Path("checkAcc")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TransferOutRes checkAccount(SessionData req){
		TransferOutRes res = new TransferOutRes();
		AccountMgr accMgr =new AccountMgr();
		SessionMgr session_mgr = new SessionMgr();
		ResponseData resdata = new ResponseData();
		resdata = session_mgr.updateActivityTime_Old(req.getSessionID(),req.getUserID());
		if (resdata.getCode().equalsIgnoreCase("0000")) {
			res =accMgr.checkAcc(req.getUserID());
		}else{
			res.setCode(resdata.getCode());
			res.setDesc(resdata.getDesc());
		}
		return res;
	}
	
	@POST
	@Path("checkContactCardNo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TransferOutRes checkContactCardNo(SessionData req) {
		TransferOutRes res = new TransferOutRes();		
		SessionMgr session_mgr = new SessionMgr();
		ResponseData resdata = new ResponseData();
		Connection l_conn = null;		
		try {
			l_conn = ConnAdmin.getConn();
			resdata = session_mgr.updateActivityTime_Old(req.getSessionID(), req.getUserID());
			if (resdata.getCode().equalsIgnoreCase("0000")) {
				if(new WalletTransactionDao().checkCardNo(req.getT1(),req.getUserID(), l_conn)){
					res.setCode("0000");					
				}else 
					res.setCode("0001");
			} else {
				res.setCode(resdata.getCode());
				res.setDesc(resdata.getDesc());
			}
		} catch (SQLException e) {			
			e.printStackTrace();
		}
		return res;
	}
	
	@POST
	@Path("addContactCardNo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TransferOutRes addContactCardNo(SessionData req) {
		TransferOutRes res = new TransferOutRes();		
		SessionMgr session_mgr = new SessionMgr();
		ResponseData resdata = new ResponseData();
		Connection l_conn = null;		
		try {
			l_conn = ConnAdmin.getConn();
			resdata = session_mgr.updateActivityTime_Old(req.getSessionID(), req.getUserID());
			if (resdata.getCode().equalsIgnoreCase("0000")) {
				if(new WalletTransactionDao().addContactCardNo(req, l_conn)){
					res.setCode("0000");					
				}else
					res.setCode("0014");
					res.setDesc("Insert Fail");
			} else {
				res.setCode(resdata.getCode());
				res.setDesc(resdata.getDesc());
			}
		} catch (SQLException e) {			
			e.printStackTrace();
		}
		return res;
	}
	@POST
	@Path("getCardNo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TransferOutRes getCardNo(SessionData req) {
		TransferOutRes res = new TransferOutRes();		
		SessionMgr session_mgr = new SessionMgr();
		ResponseData resdata = new ResponseData();
		Connection l_conn = null;		
		try {
			l_conn = ConnAdmin.getConn();
			resdata = session_mgr.updateActivityTime_Old(req.getSessionID(), req.getUserID());
			if (resdata.getCode().equalsIgnoreCase("0000")) {
				res = new WalletTransactionDao().getCardNo(req, l_conn);
			} else {
				res.setCode(resdata.getCode());
				res.setDesc(resdata.getDesc());
			}
		} catch (SQLException e) {			
			e.printStackTrace();
		}
		return res;
	}
	//Wallet to  Wallet Transfer for Wallet
		@POST
		@Path("goWalletTransfer")
		@Produces(MediaType.APPLICATION_JSON)
		@Consumes(MediaType.APPLICATION_JSON)
		public GoPaymentResponse goWalletTransfer(AccTransferReq aReq) {
			System.out.println("Parameter:"+aReq.toString());
			GeneralUtil.writeDebugLog(aReq.getSenderCode(), "Go Wallet Transfer Request Message : ", aReq.toString());
			GoPaymentResponse response = new GoPaymentResponse();
			EPIXResponse ret = new EPIXResponse();	
			MessageRequest messagerequest=new MessageRequest();//ndh
			MessageResponse messageresponse=new MessageResponse();
			NotiData notiRequest = new NotiData();
			NotiAdminResponse notiResponse=new NotiAdminResponse();
			new ResponseData();	
			ICBSMgr dbmgr = new ICBSMgr();	
			Connection l_Conn = null;
			Connection l_ConniCBS = null;	
			DAOManager l_Dao = new DAOManager();
			boolean isDrGL=false, isCrGL=false;
			AccountData fromAccData = new AccountData();
			AccountData toAccData = new AccountData();
			SessionMgr mgr = new SessionMgr();
			EPIXResponse epiRes = new EPIXResponse();
			boolean right =true;
			ResponseData chk_res = new ResponseData();
			/*if(aReq.getAppType().equals("wallet")){
				if(!aReq.getPassword().equals("")){
				String password = aReq.getPassword();
				if (!aReq.getIv().equals("")) {
					AesUtil util = new AesUtil(128, 1000);
					password = util.decrypt(aReq.getSalt(), aReq.getIv(), password);
				}
				aReq.setPassword(ServerUtil.hashPassword(password));
				 right = new TransferMgr().checkPassword(aReq.getPassword(),
						aReq.getSenderCode());
			     }
			}*/
			if (right) {
				try 
				{
					chk_res = mgr.updateActivityTime_Old(aReq.getToken(), aReq.getSenderCode());
					if (chk_res.getCode().equals("0000")) {
						System.out.println("--------------------------------Start----------------------------------------------");
						System.out.println("Start "+GeneralUtil.getDateTime());				
					
						l_Conn =ConnAdmin.getConn();
						l_ConniCBS= l_Dao.openICBSConnection();	
						// get From Account
						System.out.println("Sender Code : " + aReq.getSenderCode());
						new DBAccountMgr();
						fromAccData = DBAccountMgr.getAccountWallet("", aReq.getSenderCode(), ServerGlobal.getmMinBalSetting(),l_ConniCBS);
						aReq.setFromAccount(fromAccData.getAccountNumber());
						//check daily limit,minimum limit
						epiRes = new TransferMgr().checkTransLimit(fromAccData.getAccountNumber(), "", Double.parseDouble(aReq.getAmount()));
						if (!epiRes.getCode().equals("0000")) {
							response.setCode(epiRes.getCode());
							response.setDesc(epiRes.getDesc());
							return response;
						}
						// get To Account
						System.out.println("Sender Code : " + aReq.getReceiverCode());
						toAccData = DBAccountMgr.getAccountWallet("", aReq.getReceiverCode(), ServerGlobal.getmMinBalSetting(), l_ConniCBS);
						aReq.setToAccount(toAccData.getAccountNumber());
						System.out.println("Save Displayment info "+GeneralUtil.getDateTime());
							String env="icbs";
							if (env.equalsIgnoreCase("icbs")) {
								System.out.println("readSystemData "+GeneralUtil.getDateTime());						
								System.out.println("start doAccountTransfer "+GeneralUtil.getDateTime());
								ret = dbmgr.doAccountTransferNew(aReq, aReq.getNarrative(),l_ConniCBS,isDrGL,isCrGL,fromAccData,toAccData,"1",l_Conn);
								System.out.println("end doAccountTransfer "+GeneralUtil.getDateTime());
							if (ret.getCode().equals("0000")) {
								response.setCode("0000");
								response.setDesc("Wallet transferred successfully");
								response.setBankRefNumber(ret.getFlexcubeid());
								response.setBankTaxRefNumber(ret.getParam2());
								String transDate = GeneralUtil.datetoString();
								response.setTransactionDate(transDate);
								response.setEffectiveDate(ret.getEffectiveDate());
								response.setActualDate(GeneralUtil.getDateYYYYMMDDSimple());
								response.setCardExpire("");
								new WalletTransactionDao().saveWalletTrans(new TransferMgr().updateWalletDataCredit(aReq, response.getBankRefNumber(),response.getTransactionDate(), "","1",ret.getEffectiveDate()),l_Conn);
									response.setCode("0000");
									response.setDesc("Wallet transferred successfully");
									messagerequest.setUserID(aReq.getSenderCode());
									messagerequest.setSessionID(aReq.getToken());
									messagerequest.setMerchantID("");
									messagerequest.setServiceType("4");
									messagerequest.setType("12");
									messageresponse=new Service002().readNotiMessageSetting(messagerequest);
									if(messageresponse.getrKey().equals("true")){
										notiRequest.setT1("");
										notiRequest.setTitle("Wallet Transfer");
										notiRequest.setSessionid(aReq.getToken());
										notiRequest.setUserid(aReq.getSenderCode());
										notiRequest.setReceiverID(aReq.getReceiverCode());
										notiRequest.setType("2");
										notiRequest.setAutokey("0");
										notiRequest.setTypedescription("token");
										notiRequest.setNotiType("transfer");
										notiRequest.setServiceType("4");
										notiRequest.setFunctionType("12");
										notiRequest.setAmount(aReq.getAmount());
										notiRequest.setRefNo(response.getBankRefNumber());
										notiRequest.setName(aReq.getFromName());
										notiRequest.setMerchantName("");
										notiRequest.setTransactionType("transfer");
										notiRequest.setReceiverName(aReq.getToName());
										notiResponse=new Service007().sentNoti(notiRequest);
									}
									else{
										System.out.println("No noti Service");
									}
									
							} else {
								response.setCode(ret.getCode());
								response.setDesc(ret.getDesc());
							}					
						}			
					GeneralUtil.writeDebugLog(aReq.getSenderCode() , "go Wallet Transfer Response Message : ",	response.toString());
					System.out.println("end sendMessageService  "+GeneralUtil.getDateTime());
					System.out.println("----------------------------------END--------------------------------------------");
					}
					else{
						response.setCode(chk_res.getCode());
						response.setDesc(chk_res.getDesc());
						GeneralUtil.writeDebugLog(aReq.getSenderCode(), "go Wallet Transfer Response Message : ",
								response.toString());
						return response;
					}
				} catch (Exception e) {
					e.printStackTrace();
					response.setCode("0014");
					response.setDesc("Internal Error");
					GeneralUtil.writeDebugLog(aReq.getSenderCode(), "go Wallet Transfer Response Message : ", response.toString());
					return response;
				}
				
				finally{
					try {
						if(!l_Conn.isClosed())
							l_Conn.close();
						if(!l_ConniCBS.isClosed())
							l_ConniCBS.close();
					} catch (SQLException e) {				
						e.printStackTrace();
						response.setCode("0014");
						response.setDesc("Internal Error");
						GeneralUtil.writeDebugLog(aReq.getSenderCode(), "go Wallet Transfer Response Message : ", e.getMessage());
					
				}
				}
				
			} else {
				response.setCode("0014");
				response.setDesc("Invalid Password");
				return response;
			}
			
			return response;
		}

	@POST
	@Path("goAccountToWallet")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GoPaymentResponse goAccountToWallet(AccTransferReq aReq) {
		// Request log
		System.out.println("Parameter:" + aReq.toString());
		GeneralUtil.writeDebugLog(aReq.getReceiverCode(), "Go Agent to Transfer Request : ", aReq.toString());

		GoPaymentResponse response = new GoPaymentResponse();
		EPIXResponse ret = new EPIXResponse();
		ICBSMgr dbmgr = new ICBSMgr();
		MessageRequest messagerequest = new MessageRequest();
		NotiData notiRequest = new NotiData();
		NotiAdminResponse notiResponse = new NotiAdminResponse();
		MessageResponse messageresponse = new MessageResponse();
		Connection l_Conn = null;
		Connection l_ConniCBS = null;
		DAOManager l_Dao = new DAOManager();
		// GLDao l_GlDao = new GLDao();
		String toName = "";
		boolean isDrGL = true;
		boolean isCrGL = false;
		AccountData fromAccData = new AccountData();
		AccountData toAccData = new AccountData();
		SessionMgr mgr = new SessionMgr();
		ResponseData chk_res = new ResponseData();
		new FileUtil();
		try {
			String hashString = aReq.getReceiverCode() + aReq.getRefNo() + aReq.getAmount();
			String hashValue = new FileUtil().hmacSha1(hashString, "efa0db82bdf2a7f08505fe1e5b7603de34991a83");
			chk_res = mgr.updateActivityTime_Old(aReq.getToken(), aReq.getSenderCode());
			if (chk_res.getCode().equals("0000")) {
				if (hashValue.equals(aReq.getHashValue())) {
					System.out.println(
							"--------------------------------Start----------------------------------------------");
					System.out.println("Start " + GeneralUtil.getDateTime());
					l_Conn = ConnAdmin.getConn();
					l_ConniCBS = l_Dao.openICBSConnection();
					// Sender GL
					aReq.setFromAccount(ServerGlobal.getmWalletGL());
					// Receiver Account
					new DBAccountMgr();
					toAccData = DBAccountMgr.getAccountWallet("", aReq.getReceiverCode(),
							ServerGlobal.getmMinBalSetting(), l_ConniCBS);
					aReq.setToAccount(toAccData.getAccountNumber());
					// Receiver Name
					toName = new TransferMgr().goToName(aReq.getReceiverCode(), l_Conn);
					aReq.setToName(toName);

					String env = "icbs";
					if (env.equalsIgnoreCase("icbs")) {
						if (!aReq.getFromAccount().equals("")) {
							ret = dbmgr.doAccountTransferNew(aReq, aReq.getRefNo(), l_ConniCBS, isDrGL, isCrGL,
									fromAccData, toAccData, "4", l_Conn);
							if (ret.getCode().equals("0000")) {
								response.setCode("0000");
								response.setDesc("Wallet Topup transferred successfully");
								response.setBankRefNumber(ret.getFlexcubeid());
								response.setBankTaxRefNumber(ret.getParam2());
								String transDate = GeneralUtil.datetoString();
								response.setTransactionDate(transDate);
								response.setEffectiveDate(ret.getEffectiveDate());
								response.setCardExpire("");
								System.out.println("start saveWalletTrans  " + GeneralUtil.getDateTime());
								new WalletTransactionDao().saveWalletTrans(
										new TransferMgr().updateWalletDataCredit(aReq, response.getBankRefNumber(),
												response.getTransactionDate(), "", "4", ret.getCharges()),
										l_Conn);
								System.out.println("end saveWalletTrans  " + GeneralUtil.getDateTime());
								response.setCode("0000");
								response.setDesc("Wallet Topup transferred successfully");
								messagerequest.setUserID(aReq.getSenderCode());
								messagerequest.setSessionID(aReq.getToken());
								messagerequest.setMerchantID("");
								messagerequest.setServiceType("4");
								messagerequest.setType("12");
								messageresponse = new Service002().readNotiMessageSetting(messagerequest);
								if (messageresponse.getrKey().equals("true")) {
									notiRequest.setT1("");
									notiRequest.setTitle("Wallet Transfer");
									notiRequest.setSessionid(aReq.getToken());
									notiRequest.setUserid(aReq.getAgentAccount());
									notiRequest.setReceiverID(aReq.getReceiverCode());
									notiRequest.setType("2");
									notiRequest.setAutokey("0");
									notiRequest.setTypedescription("token");
									notiRequest.setNotiType("transfer");
									notiRequest.setServiceType("4");
									notiRequest.setFunctionType("12");
									notiRequest.setAmount(aReq.getAmount());
									notiRequest.setRefNo(response.getBankRefNumber());
									notiRequest.setName(aReq.getFromName());
									notiRequest.setMerchantName("");
									notiRequest.setTransactionType("transfer");
									notiRequest.setReceiverName(aReq.getToName());
									notiResponse = new Service007().sentNoti(notiRequest);
								} else {
									System.out.println("No noti Service");
								}

							} else {
								response.setCode(ret.getCode());
								response.setDesc(ret.getDesc());
							}
						} else {
							response.setCode("0014");
							response.setDesc("acount number not found");
						}
					}
				} else {
					response.setCode("0014");
					response.setDesc("Invalid Data");
				}
				GeneralUtil.writeDebugLog(aReq.getReceiverCode(), "go Wallet Topup Transfer Response Message : ",
						response.toString());
			} else {
				response.setCode(chk_res.getCode());
				response.setDesc(chk_res.getDesc());
				GeneralUtil.writeDebugLog(aReq.getSenderCode(), "go Wallet Transfer Response Message : ",
						response.toString());
				return response;
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode("0014");
			response.setDesc("Internal Error");
			GeneralUtil.writeDebugLog(aReq.getReceiverCode(), "go Wallet Topup Transfer Response Message : ",
					response.toString());
			return response;
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
				if (!l_ConniCBS.isClosed())
					l_ConniCBS.close();
			} catch (SQLException e) {
				e.printStackTrace();
				response.setCode("0014");
				response.setDesc("Internal Error");
				GeneralUtil.writeDebugLog(aReq.getReceiverCode(), "go Internal Transfer connection Message : ",
						e.getMessage());
			}

		}
		return response;
	}
	//disburseLoan
	@POST
	@Path("disburseLoan")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GoPaymentResponse godisburseLoan(AccTransferReq aReq) {
		// Request log
		System.out.println("Parameter:" + aReq.toString());
		GeneralUtil.writeDebugLog(aReq.getReceiverCode(), "Go Agent to Transfer Request : ", aReq.toString());
		NotiAdminResponse notiResponse=new NotiAdminResponse();
		GoPaymentResponse response = new GoPaymentResponse();
		EPIXResponse ret = new EPIXResponse();
		ICBSMgr dbmgr = new ICBSMgr();
		MessageRequest messagerequest=new MessageRequest();
		NotiData notiRequest = new NotiData();
		MessageResponse messageresponse=new MessageResponse();
		Connection l_Conn = null;
		Connection l_ConniCBS = null;
		DAOManager l_Dao = new DAOManager();
		// GLDao l_GlDao = new GLDao();
		String toName = "";
		boolean isDrGL = true;
		boolean isCrGL = false;
		AccountData fromAccData = new AccountData();
		AccountData toAccData = new AccountData();
		ResponseData chk_res = new ResponseData();
		new FileUtil();
		try {
			l_Conn = ConnAdmin.getConn();
			l_ConniCBS = l_Dao.openICBSConnection();
			// Sender GL
			aReq.setFromAccount(ServerGlobal.getLoanGL());
			// Receiver Account
			Result res = new MobileMemberDao().getPhoneNo(Long.parseLong(aReq.getLoanSyskey()), l_Conn);
			if(!res.getPhNo().equals("")){
				aReq.setReceiverCode(res.getPhNo());
				new DBAccountMgr();
				toAccData = DBAccountMgr.getAccountWallet("", aReq.getReceiverCode(), ServerGlobal.getmMinBalSetting(),
						l_ConniCBS);
				aReq.setToAccount(toAccData.getAccountNumber());
			}
			// Receiver Name
			toName = new TransferMgr().goToName(aReq.getReceiverCode(), l_Conn);
			aReq.setToName(toName);

			String env = "icbs";
				if (env.equalsIgnoreCase("icbs")) {
					if (!aReq.getFromAccount().equals("")) {
						ret = dbmgr.doAccountTransferNew(aReq, aReq.getRefNo(), l_ConniCBS, isDrGL, isCrGL, fromAccData,
								toAccData, "6", l_Conn);
						if (ret.getCode().equals("0000")) {
							response.setCode("0000");
							response.setDesc("Wallet Topup transferred successfully");
							response.setBankRefNumber(ret.getFlexcubeid());
							response.setBankTaxRefNumber(ret.getParam2());
							String transDate = GeneralUtil.datetoString();
							response.setTransactionDate(transDate);
							response.setEffectiveDate(ret.getEffectiveDate());
							response.setCardExpire("");
							System.out.println("start saveWalletTrans  " + GeneralUtil.getDateTime());
							new WalletTransactionDao().saveWalletTrans(
									new TransferMgr().updateWalletDataCredit(aReq, response.getBankRefNumber(),
											response.getTransactionDate(), "", "4", ret.getCharges()),
									l_Conn);
							System.out.println("end saveWalletTrans  " + GeneralUtil.getDateTime());
							response.setCode("0000");
							response.setDesc("Wallet Topup transferred successfully");
							messagerequest.setUserID(aReq.getSenderCode());
							messagerequest.setSessionID(aReq.getToken());
							messagerequest.setMerchantID("");
							messagerequest.setServiceType("4");
							messagerequest.setType("12");
							messageresponse=new Service002().readNotiMessageSetting(messagerequest);
							if(messageresponse.getrKey().equals("true")){
								notiRequest.setT1("");
								notiRequest.setTitle("Wallet Transfer");
								notiRequest.setSessionid(aReq.getToken());
								notiRequest.setUserid("+959771112390");
								notiRequest.setReceiverID(aReq.getReceiverCode());
								notiRequest.setType("2");
								notiRequest.setAutokey("0");
								notiRequest.setTypedescription("token");
								notiRequest.setNotiType("transfer");
								notiRequest.setServiceType("4");
								notiRequest.setFunctionType("12");
								notiRequest.setAmount(aReq.getAmount());
								notiRequest.setRefNo(response.getBankRefNumber());
								notiRequest.setName("MFI");
								notiRequest.setMerchantName("");
								notiRequest.setTransactionType("transfer");
								notiRequest.setReceiverName(aReq.getToName());
								notiResponse=new Service007().sentNoti(notiRequest);
							}
							else{
								System.out.println("No noti Service");
							}
							

						} else {
							response.setCode(ret.getCode());
							response.setDesc(ret.getDesc());
						}
					}else{
						response.setCode("0014");
						response.setDesc("acount number not found");
					}
				}
			GeneralUtil.writeDebugLog(aReq.getReceiverCode(), "go Wallet Topup Transfer Response Message : ",
					response.toString());
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode("0014");
			response.setDesc("Internal Error");
			GeneralUtil.writeDebugLog(aReq.getReceiverCode(), "go Wallet Topup Transfer Response Message : ",
					response.toString());
			return response;
		} finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
				if (!l_ConniCBS.isClosed())
					l_ConniCBS.close();
			} catch (SQLException e) {
				e.printStackTrace();
				response.setCode("0014");
				response.setDesc("Internal Error");
				GeneralUtil.writeDebugLog(aReq.getReceiverCode(), "go Internal Transfer connection Message : ",
						e.getMessage());
			}

		}
		return response;
	}
	@POST
	@Path("goMptTopup")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GoPaymentResponse goMptTopup(AccTransferReq aReq) {
		// Request log
		System.out.println("Parameter:" + aReq.toString());
		GeneralUtil.writeDebugLog(aReq.getSenderCode(), "Go Merchant Payment Transfer Request : ", aReq.toString());
		GoPaymentResponse response = new GoPaymentResponse();
		EPIXResponse ret = new EPIXResponse();
		MessageRequest messagerequest=new MessageRequest();//ndh
		MessageResponse messageresponse=new MessageResponse();
		NotiData notiRequest = new NotiData();
		NotiAdminResponse notiResponse=new NotiAdminResponse();
		new ResponseData();
		ICBSMgr dbmgr = new ICBSMgr();
		Connection l_Conn = null;
		Connection l_ConniCBS = null;
		DAOManager l_Dao = new DAOManager();
		GLDao l_GlDao = new GLDao();
		EPIXResponse epiRes = new EPIXResponse();
		MerchantMgr merchantMgr = new MerchantMgr();
		String transtype = "", toAccount = "";
		boolean isDrGL = false, isCrGL = false;
		boolean right = true;
		AccountData fromAccData = new AccountData();
		AccountData toAccData = new AccountData();
		SessionMgr mgr = new SessionMgr();
		ResponseData chk_res = new ResponseData();
		if (!aReq.getPassword().equals("")) {
			String password = aReq.getPassword();// ndh
			if (!aReq.getIv().equals("")) {
				AesUtil util = new AesUtil(128, 1000);
				password = util.decrypt(aReq.getSalt(), aReq.getIv(), password);
			}
			aReq.setPassword(ServerUtil.hashPassword(password));
			right = new TransferMgr().checkPassword(aReq.getPassword(), aReq.getSenderCode());
		}
		try {
			if (right) {				
				chk_res = mgr.updateActivityTime_Old(aReq.getToken(), aReq.getSenderCode());				
				if (chk_res.getCode().equals("0000")) {
					System.out.println(
							"--------------------------------Start----------------------------------------------");
					System.out.println("Start " + GeneralUtil.getDateTime());
					l_Conn = ConnAdmin.getConn();
					l_ConniCBS = l_Dao.openICBSConnection();

					// String bankcomm = new
					// FileUtil().readUrlString("BankComm");
					// aReq.setBankCharges(bankcomm);
					// From Account
					if (aReq.getFromAccount().equals("")) {
						System.out.println("Sender : " + aReq.getSenderCode());
						new DBAccountMgr();
						fromAccData = DBAccountMgr.getAccountWallet("", aReq.getSenderCode(),
								ServerGlobal.getmMinBalSetting(), l_ConniCBS);
						aReq.setFromAccount(fromAccData.getAccountNumber());

					}
					// check daily limit,trans limit
					/*epiRes = new TransferMgr().checkTransLimit(fromAccData.getAccountNumber(), "",
							Double.parseDouble(aReq.getAmount()));
					if (!epiRes.getCode().equals("0000")) {
						response.setCode(epiRes.getCode());
						response.setDesc(epiRes.getDesc());
						return response;
					}*/
					// To Account
					CMSMerchantData merchantdata =new CMSMerchantData();
					if (aReq.getToAccount().equals("")) {
						merchantdata = merchantMgr.getMerchantAccount(aReq.getMerchantID());
						toAccount = merchantdata.getAccountNumber();
						aReq.setToName(merchantdata.getBranchname());
						if(toAccount.contains("+959")){
							toAccData = DBAccountMgr.getAccountWallet("", toAccount, ServerGlobal.getmMinBalSetting(),
									l_ConniCBS);
							aReq.setToAccount(toAccData.getAccountNumber());
						}else{
							aReq.setToAccount(toAccount);
						}
					}
					if (l_GlDao.isGLAccount(toAccount, l_ConniCBS)) {
						isDrGL = false;
						isCrGL = true;
					}

					String env = "icbs";
					if (env.equalsIgnoreCase("icbs")) {
						ret = dbmgr.doMPTPayment(aReq, aReq.getRefNo(), l_ConniCBS, isDrGL, isCrGL, fromAccData,
								toAccData, "2", l_Conn);
						if (ret.getCode().equals("0000")) {
							response.setCode("0000");
							response.setDesc("Merchant  transferred successfully");
							response.setBankRefNumber(ret.getFlexcubeid());
							response.setBankTaxRefNumber(ret.getParam2());
							String transDate = GeneralUtil.datetoString();
							response.setTransactionDate(transDate);
							response.setEffectiveDate(ret.getEffectiveDate());
							response.setCardExpire("");
							//response.setBankCharges(ret.getComm1());
							//response.setPenalty(ret.getComm2());
							/*if(aReq.getMerchantID().equals("M00019")){
								aReq.setBankCharges(ret.getComm1());//atn
								aReq.setPenalty(ret.getComm2());
							}*/
							UtilityResponse utilityRes = new UtilityResponse();
							System.out.println("start saveWalletTrans  " + GeneralUtil.getDateTime());
							new WalletTransactionDao()
									.saveWalletTrans(
											new TransferMgr().updateWalletDataCredit(aReq, response.getBankRefNumber(),
													response.getTransactionDate(), "","2",ret.getEffectiveDate()),l_Conn);
							response.setCode("0000");
							response.setDesc("Merchant transferred successfully");
							messagerequest.setUserID(aReq.getSenderCode());
							messagerequest.setSessionID(aReq.getToken());
							messagerequest.setMerchantID("");
							messagerequest.setServiceType("4");
							messagerequest.setType("1");
							messageresponse=new Service002().readNotiMessageSetting(messagerequest);
							if(messageresponse.getrKey().equals("true")){
								notiRequest.setT1("");
								notiRequest.setTitle("Payment");
								notiRequest.setSessionid(aReq.getToken());
								notiRequest.setUserid(aReq.getSenderCode());
								notiRequest.setReceiverID(aReq.getMerchantID());
								notiRequest.setType("2");
								notiRequest.setAutokey("0");
								notiRequest.setTypedescription("token");
								notiRequest.setNotiType("transfer");
								notiRequest.setServiceType("4");
								notiRequest.setFunctionType("1");
								notiRequest.setAmount(aReq.getAmount());
								notiRequest.setRefNo(response.getBankRefNumber());
								notiRequest.setName("MFI");
								notiRequest.setTransactionType("payment");
//								notiRequest.setMerchantName("");
								
//								notiRequest.setReceiverName("");
								notiResponse=new Service007().sentNoti(notiRequest);
							}
							else{
								System.out.println("No noti Service");
							}
							

						} else {
							response.setCode(ret.getCode());
							response.setDesc(ret.getDesc());
						}
					}
				} else {
					response.setCode(chk_res.getCode());
					response.setDesc(chk_res.getDesc());
					GeneralUtil.writeDebugLog(aReq.getSenderCode(), "go Merchant Payment  Transfer Response Message : ",
							response.toString());
					return response;
				}
			} else {
				response.setCode("0015");
				response.setDesc("Invalid Password");
				return response;
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode("0014");
			response.setDesc("Internal Error");
			GeneralUtil.writeDebugLog(aReq.getSenderCode(), "go Merchant Payment Response Message : ",
					response.toString());
			return response;
		}/* finally {
			try {
				if (!l_Conn.isClosed())
					l_Conn.close();
				if (!l_ConniCBS.isClosed())
					l_ConniCBS.close();
			} catch (SQLException e) {
				e.printStackTrace();
				response.setCode("0016");
				response.setDesc("Internal Error");
				GeneralUtil.writeDebugLog(aReq.getSenderCode(), "go Merchant Payment  connection Message : ",
						e.getMessage());
			}
		}*/
		return response;
	}
	
}
