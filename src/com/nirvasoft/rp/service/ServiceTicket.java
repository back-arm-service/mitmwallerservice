package com.nirvasoft.rp.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.ServerSession;
import com.nirvasoft.rp.mgr.TicketMgr;
import com.nirvasoft.rp.shared.TicketData;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ServerGlobal;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

@Path("/serviceTicket")
public class ServiceTicket {

	@Context
	HttpServletRequest request;
	@Context
	private HttpServletResponse response;
	@javax.ws.rs.core.Context
	ServletContext context;

	private boolean downloadTicketImage(String imageNameAndDatePath) {
		getPath();

		String projectDirectory = DAOManager.AbsolutePath;

		if (projectDirectory.contains("WalletService")) {
			projectDirectory = projectDirectory.replace("WalletService", "DigitalMedia");
		}

		// upload\\image\\ticketImage\\
		String toPath = projectDirectory + ConnAdmin.readConfigFile("reference//configImage.txt", "ipTicketImageDM");

		// https://chatting.azurewebsites.net/uploads/smallImage/
		String fromPath = ConnAdmin.readConfigFile("reference//configImage.txt", "ipTicketImageChat")
				+ imageNameAndDatePath;

		boolean isSuccess = false;

		try {
			URL url1 = new URL(fromPath);

			FileUtils.copyURLToFile(url1, new File(toPath + imageNameAndDatePath));

			isSuccess = true;
		} catch (Exception e) {
			isSuccess = false;
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return isSuccess;
	}

	public void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
	}

	@POST
	@Path("/insertTicket")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TicketData insertTicket(TicketData requestData) {
		TicketData response = null;

		getPath();
		DAOManager.AbsolutePath = context.getRealPath("");

		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Insert Ticket.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}

		response = new TicketMgr().insertTicket(requestData);

		boolean isSuccess = downloadTicketImage(requestData.getT13());
		if (response != null) {
			if (isSuccess != true) {
				response.setMessageDesc("Download fail.");
			} else {
				response.setMessageDesc("Download success.");
			}
		}

		return response;
	}

	@POST
	@Path("/mobileupload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.TEXT_PLAIN)
	public String mobileuploadFile(@FormDataParam("file") InputStream fis,
			@FormDataParam("file") FormDataContentDisposition fdcd) {
		this.getPath();
		String responseFilePath = "upload/image/ticketImage/";
		String filePath = request.getServletContext().getRealPath("");
		System.out.println("File Path is = " + filePath);
		filePath = filePath.replace("WalletService", "DigitalMedia");
		System.out.println(filePath);
		filePath += responseFilePath;
		System.out.println(filePath);

		String fileName = fdcd.getFileName();
		System.out.println("fileName is = " + fileName);

		String extension = FilenameUtils.getExtension(fileName);
		System.out.println("extension is = " + extension);

		if (extension.equalsIgnoreCase("JPEG") || extension.equalsIgnoreCase("JPG")
				|| extension.equalsIgnoreCase("PNG")) {
			filePath += GeneralUtil.datetoStringNew();
			filePath += "/";
			System.out.println("filePath is = " + filePath);
			responseFilePath += GeneralUtil.datetoStringNew();
			responseFilePath = "/" + responseFilePath + "/";
			System.out.println("responseFilePath is = " + responseFilePath);
		}

		File dir = new File(filePath);
		if (!dir.exists())
			dir.mkdirs();

		String basename = FilenameUtils.getBaseName(fileName);
		System.out.println("basename is = " + basename);
		String fullname = basename + "." + extension;
		System.out.println("fullname is = " + fullname);
		filePath += fullname;
		System.out.println("filePath is = " + filePath);
		OutputStream outpuStream = null;

		try {
			int read = 0;
			byte[] bytes = new byte[1024];
			outpuStream = new FileOutputStream(new File(filePath));
			while ((read = fis.read(bytes)) != -1) {
				outpuStream.write(bytes, 0, read);
			}
			outpuStream.flush();
			outpuStream.close();
		} catch (IOException iox) {
			iox.printStackTrace();
		} finally {
			if (outpuStream != null) {
				try {
					outpuStream.close();
				} catch (Exception ex) {
				}
			}
		}
		return responseFilePath;
	}

}
