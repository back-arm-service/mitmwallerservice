package com.nirvasoft.rp.service;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.data.VideoListingDataList;
import com.nirvasoft.rp.mgr.SessionMgr;
import com.nirvasoft.rp.mgr.VideoMgr;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.framework.ServerSession;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.shared.ArticleDataSet;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.cms.shared.VideoDataSet;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.rp.service.Service001;
import com.nirvasoft.rp.shared.FilterDataset;
import com.nirvasoft.rp.shared.ResponseData;

@Path("/serviceVideoAdm")
public class ServiceVideoAdm {
	@Context
	HttpServletRequest request;
	@javax.ws.rs.core.Context
	ServletContext context;
	public void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";		
	}

	private MrBean getUser() {
		this.getPath();
		MrBean user = new MrBean();
		user.getUser().setOrganizationID("001");
		user.getUser().setUserId(Service001.userid);
		user.getUser().setUserName(Service001.username);
		return user;
	}	

	/* readBySyskey */
	@GET
	@Path("readBySyskey")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleData readDataByQuestion() {
		this.getPath();
		ArticleData ret;
		String key = request.getParameter("key");
		ret = new VideoMgr().readDataBySyskey(Long.parseLong(key), getUser());
		return ret;
	}

	@POST
	@Path("saveVideo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Resultb2b saveVideo(ArticleData p) {
		this.getPath();
		Resultb2b res = new Resultb2b();
		String filePath = request.getServletContext().getRealPath("/") + "/upload";
		filePath += "/image";
		Service001.userid = p.getUserId();
		Service001.username = p.getUserName();
		System.out.println(p.getUserId());
		res =new VideoMgr().saveVideo1(p, filePath, getUser());		 
		return res;
	}

	@GET
	@Path("viewAdminVidoe")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleDataSet viewByID() {
		this.getPath();
		String id = request.getParameter("id");
		String userid = request.getParameter("userid");
		ArticleDataSet res = new ArticleDataSet();
		res = new VideoMgr().viewVideo(id, userid);		 
		return res;
	}

	@GET
	@Path("searchLike")
	@Produces(MediaType.APPLICATION_JSON)
	public VideoDataSet searchLike() {
		this.getPath();
		String type = request.getParameter("type");
		long key = Long.parseLong(request.getParameter("key"));
		VideoDataSet ret;
		ret =  new VideoMgr().searchLike(type, key, getUser());		
		return ret;
	}

	@GET
	@Path("clickLikeVideo")
	@Produces(MediaType.APPLICATION_JSON)
	public Resultb2b clickLikeVideo() {
		this.getPath();
		Resultb2b ret;
		String key = request.getParameter("key");
		String userSK = request.getParameter("userSK");
		ret = new VideoMgr().clickLikeVideo(Long.parseLong(key), userSK, "Video", getUser());		 
		return ret;
	}	
	// video
		@POST
		@Path("searchVideolist")
		@Produces(MediaType.APPLICATION_JSON)
		@Consumes(MediaType.APPLICATION_JSON)
		public VideoListingDataList searchVideolist(FilterDataset p) {
			this.getPath();
			VideoListingDataList res = new VideoListingDataList();
			SessionMgr session_mgr = new SessionMgr();
			ResponseData sessionRes = new ResponseData();
			sessionRes = session_mgr.updateActivityTime(p.getSessionID(), p.getUserID());
			if (sessionRes.getCode().equalsIgnoreCase("0000")) {
				res = new VideoMgr().searchVideolist(p);
			} else {
				res.setMsgCode(sessionRes.getCode());
				res.setMsgDesc(sessionRes.getDesc());
			}

			return res;
		}
	
	/* readBySyskey */
	@GET
	@Path("readBySyskeyNew")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleData readBySyskeyNew() {
		this.getPath();
		ArticleData ret;
		String key = request.getParameter("key");
		String sessionID = request.getParameter("sessionID");
		String userID = request.getParameter("userID");
		SessionMgr session_mgr = new SessionMgr();
		ResponseData sessionRes = new ResponseData();
		sessionRes = session_mgr.updateActivityTime(sessionID, userID,"videomenu");
		if (sessionRes.getCode().equalsIgnoreCase("0000")) {
		ret = new VideoMgr().readBySyskeyNew(Long.parseLong(key));
		}else 
			ret = null;
		return ret;
	}

}
