package com.nirvasoft.rp.service;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.util.FontChangeUtil;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.framework.ServerSession;
import com.nirvasoft.rp.mgr.PostMgr;

@Path("/servicePost")
public class ServicePost {
	public static boolean createThumbnailMobileImage(String imageUrl, String targetPath) {
		File thumbnail = new File(targetPath);
		try {
			thumbnail.getParentFile().mkdirs();
			thumbnail.createNewFile();
			BufferedImage sourceImage = ImageIO.read(new File(imageUrl));
			float origwidth = 0;
			float origheight = 0;
			origwidth = sourceImage.getWidth();
			origheight = sourceImage.getHeight();

			double scaledWidth = 0;
			double scaledHeight = 0;
			double ratio = 0;

			if (origwidth > origheight) {
				ratio = 1.0 * (origwidth / origheight);
			} else {
				ratio = 1.0 * (origheight / origwidth);
			}
			scaledWidth = origwidth;
			scaledHeight = origheight;

			while (scaledWidth > scaledHeight && scaledWidth > 990) {
				scaledWidth = scaledWidth / ratio;
				scaledHeight = scaledHeight / ratio;
			}

			while (scaledHeight > scaledWidth && scaledHeight > 990) {
				scaledWidth = scaledWidth / ratio;
				scaledHeight = scaledHeight / ratio;
			}

			if (scaledWidth == scaledHeight) {
				scaledWidth = 400;
				scaledHeight = 400;
			}

			BufferedImage img = new BufferedImage((int) scaledWidth, (int) scaledHeight, sourceImage.getType());
			Image scaledImage = sourceImage.getScaledInstance((int) scaledWidth, (int) scaledHeight,
					Image.SCALE_SMOOTH);
			img.createGraphics().drawImage(scaledImage, 0, 0, null);
			ImageIO.write(img, "png", thumbnail);
			return true;
		} catch (IOException e) {
			System.out.println("error: " + e);
			e.printStackTrace();
		}
		return false;
	}

	@Context
	HttpServletRequest request;
	@Context
	private HttpServletResponse response;

	@javax.ws.rs.core.Context
	ServletContext context;

	private void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
	}

	/* Save(savePost) */
	@POST
	@Path("savePost")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Resultb2b saveContenMenu(ArticleData p) throws IOException {
		Resultb2b res = new Resultb2b();

		getPath();

		String imgurl = request.getParameter("imgurl");
		// String imgurl = "http://192.168.205.83:8080/DigitalMedia/";
		String typeurl = "upload/image/contentImage/";
		String today = ServerUtil.datetoString();
		typeurl += today + "/";
		p.setT10(imgurl + typeurl);

		String[] resizeList = new String[p.getUploadlist().size()];

		for (int i = 0; i < p.getUploadlist().size(); i++) {
			p.getUploadlist().get(i).setSerial(i + 1);
			String imageName = p.getUploadlist().get(i).getName()
					.substring(p.getUploadlist().get(i).getName().lastIndexOf("/") + 1);
			p.getUploadlist().get(i).setName(imageName);
			resizeList[i] = p.getUploadlist().get(i).getName();

			if (p.getUploadlist().get(i).getUrl().equals("")) {
				p.getUploadlist().get(i).setUrl(imgurl + typeurl + p.getUploadlist().get(i).getName());
			}
		}

		p.setResizelist(resizeList);

		if (!ServerUtil.isUniEncoded(p.getT2())) { // post text
			p.setT2(FontChangeUtil.zg2uni(p.getT2()));
		}

		System.out.println("p.getT2(): " + p.getT2() + "\nuploadlist: " + p.getUploadlist());

		res = PostMgr.savePost(p);

		return res;
	}

	@POST
	@Path("fileupload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public String uploadFile(@QueryParam("f") String filePath, @QueryParam("fn") String inputFileName,
			@QueryParam("imgUrl") String imgUrl) throws AWTException {
		String result = "{\"code\":\"ERROR\"}";
		// String sfilePath = "";
		String outputFileName = "";

		if (inputFileName.contains("/")) {
			inputFileName = inputFileName.substring(inputFileName.lastIndexOf("/") + 1);
		}
		// sfilePath = request.getServletContext().getRealPath("/") + "/" +
		// filePath;
		filePath = request.getServletContext().getRealPath("/") + "/" + filePath;
		if (filePath.contains("WalletService")) {
			filePath = filePath.replace("WalletService", "DigitalMedia");
		}
		// if (sfilePath.contains("WalletService")) {
		// sfilePath = sfilePath.replace("WalletService", "DigitalMedia");
		// }

		String extension = FilenameUtils.getExtension(inputFileName);
		if (extension.equalsIgnoreCase("JPEG") || extension.equalsIgnoreCase("JPG")
				|| extension.equalsIgnoreCase("PNG")) {
			filePath += "/image";
			// sfilePath += "/smallImage";
			imgUrl += "upload/image";

			filePath += "/contentImage";
			// sfilePath += "/contentImage";
			imgUrl += "/contentImage";

			filePath += "/" + ServerUtil.datetoString();
			// sfilePath += "/" + ServerUtil.datetoString();
			imgUrl += "/" + ServerUtil.datetoString();
			outputFileName = "IMG";
			File dir = new File(filePath);
			if (!dir.exists())
				dir.mkdirs();
			String dtFormat = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
			outputFileName += dtFormat + "." + extension;
			filePath += "/" + outputFileName;
			// sfilePath += "/" + outputFileName;
			imgUrl += "/" + outputFileName;
			File l_file = new File(filePath);
			// File s_file = new File(sfilePath);

			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);

			try {
				@SuppressWarnings("unchecked")
				List<FileItem> items = upload.parseRequest(request);
				for (FileItem item : items) {
					if (item.isFormField()) {
					} else {
						if (l_file.exists()) {
							try {
								l_file.delete();
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						// if (s_file.exists()) {
						// try {
						// s_file.delete();
						// } catch (Exception e) {
						// e.printStackTrace();
						// }
						// }
						if (l_file.createNewFile()) {
							item.write(l_file);
							result = "{\"code\":\"SUCCESS\",\"fileName\":\"" + outputFileName + "\",\"sfileName\":\""
									+ outputFileName + "\",\"url\":\"" + imgUrl + "\"}";
							// }
							// if (!extension.equalsIgnoreCase("MP4")) {
							// if (!extension.equalsIgnoreCase("xls")) {
							// if (!extension.equalsIgnoreCase("PDF")) {
							// //if (s_file.createNewFile()) {
							// //item.write(s_file);
							// //boolean res = createThumbnailMobileImage(sfilePath,
							// sfilePath);
							// //if (res == true) {
							//
							// //}
							// //}
							// } else {
							// result = "{\"code\":\"SUCCESS\",\"fileName\":\"" +
							// outputFileName
							// + "\",\"sfileName\":\"" + outputFileName +
							// "\",\"url\":\"" + imgUrl + "\"}";
							// }
							// } else {
							// result = "{\"code\":\"SUCCESS\",\"fileName\":\"" +
							// outputFileName + "\"}";
							// }
						} else {
							result = "{\"code\":\"SUCCESS\",\"fileName\":\"" + outputFileName + "\"}";
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		
		System.out.println("result: " + result);
		return result;
	}

}
