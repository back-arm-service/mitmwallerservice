package com.nirvasoft.rp.service;

public class Fault {

	private String faultCode;
	private String faultString;

	public String getFaultCode() {
		return faultCode;
	}

	public String getFaultString() {
		return faultString;
	}

	public void setFaultCode(String faultCode) {
		this.faultCode = faultCode;
	}

	public void setFaultString(String faultString) {
		this.faultString = faultString;
	}

}
