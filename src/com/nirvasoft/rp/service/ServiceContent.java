package com.nirvasoft.rp.service;

import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.nirvasoft.cms.framework.ResultMobile;
import com.nirvasoft.cms.mgr.ContentMobileMgr;
import com.nirvasoft.cms.shared.PagerMobileData;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.ServerSession;
import com.nirvasoft.cms.shared.ContentData;
import com.nirvasoft.cms.shared.ContentDataSet;
import com.nirvasoft.cms.shared.UserViewDataset;
import com.nirvasoft.rp.dao.DAOManager;

@Path("/serviceContent")
public class ServiceContent {
	@Context
	HttpServletRequest request;

	@javax.ws.rs.core.Context
	ServletContext context;

	// get all content_writer list
	@POST
	@Path("getContentWriterList")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserViewDataset getContentWriterList(PagerMobileData p) throws SQLException {
		this.getPath();
		UserViewDataset res = new UserViewDataset();
		res = new ContentMobileMgr().getContentWriterList(p, getUser());
		return res;
	}

	public void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = context.getRealPath("");
	}

	private MrBean getUser() {
		this.getPath();
		MrBean user = new MrBean();
		user.getUser().setOrganizationID("001");
		user.getUser().setUserId(Service001.userid);
		user.getUser().setUserName(Service001.username);
		return user;
	}

	// Moibile Save Content
	@POST
	@Path("saveContent")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ResultMobile saveContent(ContentData p) {
		this.getPath();
		ResultMobile res = new ResultMobile();
		res = new ContentMobileMgr().saveContent(p, getUser());
		return res;
	}

	// Mobile For SaveContent
	@POST
	@Path("searchContent")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ContentDataSet searchContent(PagerMobileData p) {
		this.getPath();
		String searchVal = request.getParameter("searchVal");
		long userSK = Long.parseLong(request.getParameter("userSK"));
		ContentDataSet res = new ContentDataSet();
		res = new ContentMobileMgr().searchContent(p, userSK, searchVal, getUser());
		return res;
	}

	// Mobile TDA UnsaveContent
	@POST
	@Path("unsaveContent")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ResultMobile unsaveContent(ContentData p) {
		this.getPath();
		ResultMobile res = new ResultMobile();
		res = new ContentMobileMgr().unsaveContent(p, getUser());
		return res;
	}

}
