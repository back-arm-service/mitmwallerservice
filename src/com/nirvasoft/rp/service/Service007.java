package com.nirvasoft.rp.service;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.nirvasoft.rp.framework.ServerSession;
import com.nirvasoft.rp.mgr.NotiMgr;
import com.nirvasoft.cms.mgr.NotiScheduler;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.rp.mgr.SessionMgr;
import com.nirvasoft.rp.mgr.integration.SMSSettingMgr;
import com.nirvasoft.rp.shared.NotiAdminResponse;
import com.nirvasoft.rp.shared.NotiData;
import com.nirvasoft.rp.shared.NotiResponse;
import com.nirvasoft.rp.shared.NotificationDataSet;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.shared.integration.SMSSettingData;
// import com.nirvasoft.rp.shared.ServerGlobal;
import com.nirvasoft.rp.util.FontChangeUtil;
import com.nirvasoft.rp.util.FontUtil;
import com.nirvasoft.rp.util.GeneralUtil;

@Path("/service007")
public class Service007 {
	@Context
	HttpServletRequest request;

	@Context
	private HttpServletResponse response;

	@javax.ws.rs.core.Context
	ServletContext context;

	private void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
	}
	@POST
	@Path("sentNoti")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public NotiAdminResponse sentNoti(NotiData req) throws SQLException {
		NotiAdminResponse res = new NotiAdminResponse();
		NotiResponse response=new NotiResponse();
		ResponseData responseObj = new ResponseData();
		SMSSettingData responseData=new SMSSettingData();
		SMSSettingMgr smgr = new SMSSettingMgr();
		NotiMgr mgr=new NotiMgr();
		String fromSite;
		String toSite;
		String bothSite;
		String fromMessage=null;
		String toMessage=null;
		double amt=Double.parseDouble(req.getAmount());
		String amount=ServerUtil.formatNumber(amt);
		
		responseData=smgr.getSMSNotiData(req.getServiceType(),req.getFunctionType(),req.getMerchantID());
		if(responseData.getCode().equals("0000")){
			fromSite=responseData.getFrom();
			toSite=responseData.getTo();
			bothSite=responseData.getBoth();
			if(req.getTransactionType().equals("transfer")){
			if(fromSite.equals("1")){
				if (!responseData.getFromMsg().equalsIgnoreCase("") && !responseData.getFromMsg().equalsIgnoreCase("null")) {
					fromMessage = responseData.getFromMsg().replace("<amount>", amount);
					fromMessage=fromMessage.replace("<sender name>",req.getReceiverName());
					fromMessage=fromMessage.replace("<sender ph no>", req.getReceiverID());
					fromMessage=fromMessage.replace("<ref no>", req.getRefNo());
				}
				req.setDescription(fromMessage);
			}
			if(toSite.equals("1")){
				if (!responseData.getToMsg().equalsIgnoreCase("") && !responseData.getToMsg().equalsIgnoreCase("null")) {
					toMessage = responseData.getFromMsg().replace("<amount>", amount);
					toMessage=fromMessage.replace("<sender ph no>", req.getUserid());
					toMessage=fromMessage.replace("<ref no>", req.getRefNo());
				}
				req.setDescription(toMessage);
			}
			if(bothSite.equals("1")){
				if (!responseData.getFromMsg().equalsIgnoreCase("") && !responseData.getFromMsg().equalsIgnoreCase("null")) {
					fromMessage = responseData.getFromMsg().replace("<amount>", amount);
					fromMessage=fromMessage.replace("<sender name>",req.getReceiverName());
					fromMessage=fromMessage.replace("<sender ph no>", req.getReceiverID());
					fromMessage=fromMessage.replace("<ref no>", req.getRefNo());
				}
				req.setDescription(fromMessage);
				if (!responseData.getToMsg().equalsIgnoreCase("") && !responseData.getToMsg().equalsIgnoreCase("null")) {
					toMessage = responseData.getToMsg().replace("<amount>", amount);
					toMessage=toMessage.replace("<sender name>",req.getName());
					toMessage=toMessage.replace("<sender ph no>", req.getUserid());
					toMessage=toMessage.replace("<ref no>", req.getRefNo());
				}
				//req.setDescription(toMessage);
			
			}
		}
			else{
				if(fromSite.equals("1")){
					if (!responseData.getFromMsg().equalsIgnoreCase("") && !responseData.getFromMsg().equalsIgnoreCase("null")) {
						fromMessage = responseData.getFromMsg().replace("<amount>", amount);
						fromMessage=fromMessage.replace("<sender name>",req.getName());
						fromMessage=fromMessage.replace("<sender ph no>", req.getUserid());
						fromMessage=fromMessage.replace("<merchant name>", req.getMerchantName());
						fromMessage=fromMessage.replace("<merchantID>", req.getReceiverID());
						fromMessage=fromMessage.replace("<ref no>", req.getRefNo());
					}
					req.setDescription(fromMessage);
				}
				
				
			}
			responseObj=validateNotiTokenUser(req);
//		if (!res.getCode().equals("0000")) {
//			res.setCode(res.getCode());
//			res.setDesc(res.getDesc());
//		} else {
//			if (req.getAutokey() != null && !req.getAutokey().trim().equalsIgnoreCase("")) {
//				req.setAutokey(req.getAutokey().trim());
//			} else {
//				req.setAutokey("0");
//			}
//		}
		if(req.getNotiType().equals("transfer")){
			res=mgr.CheckUserToken(req);
		}
		else{
			res=mgr.selectUserToken(req);
			req.setT1(res.getDesc());
		}
		req.setAutokey(res.getAutokey());
		if(res.getCode()=="0000"){
			response=mgr.callFcmFirebase(req);
			if(bothSite.equals("1")){
				req.setUserid(req.getReceiverID());
				req.setDescription(toMessage);
				req.setAutokey("0");
				res=mgr.selectUserToken(req);
				req.setT1(res.getDesc());
				req.setAutokey(res.getAutokey());
				response=mgr.callFcmFirebase(req);
			}
		}
	}
		return res;
	}
	public ResponseData validateNotiTokenUser(NotiData req) throws SQLException {
		ResponseData response = new ResponseData();
		if (req.getAutokey() != null && !req.getAutokey().trim().equalsIgnoreCase("")) {
			try {
				long autokey = Long.parseLong(req.getAutokey());

				if (autokey < 0) {
					response.setCode("0014");
					response.setDesc("Invalid Request.");
					return response;
				}
			} catch (Exception ex) {
				ex.printStackTrace();

				response.setCode("0014");
				response.setDesc("Invalid Request.");
				return response;
			}
		}
		if(req.getSessionid()==null&& !req.getAutokey().trim().equalsIgnoreCase("")){
			response.setCode("0014");
			response.setDesc("Session Invalid");
			return response;
		}
		if(req.getUserid()==null&& !req.getUserid().trim().equalsIgnoreCase("")){
			response.setCode("0014");
			response.setDesc("UserID Invalid");
			return response;
		}
		
		if (req.getTitle() == null || req.getTitle().trim().equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("Please fill Title.");
			return response;
		}

		if (req.getDescription() == null || req.getDescription().trim().equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("Please fill Description.");
			return response;
		}

		if (req.getType() == null || req.getType().trim().equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("Invalid Request.");
			return response;
		} else {
				int type = Integer.parseInt(req.getType());
		}
		return response;
	
		}
	
	

	@POST
	@Path("insertOrUpdateNoti")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public NotiAdminResponse insertOrUpdateNoti(NotiData req) {
		getPath();
		NotiAdminResponse res = new NotiAdminResponse();
		ResponseData responseObj = new ResponseData();
		responseObj = validateNotiRequestObj(req);

		if (!responseObj.getCode().equals("0000")) {
			res.setCode(responseObj.getCode());
			res.setDesc(responseObj.getDesc());
			return res;
		} else {
			if (req.getAutokey() != null && !req.getAutokey().trim().equalsIgnoreCase("")) {
				req.setAutokey(req.getAutokey().trim());
			} else {
				req.setAutokey("0");
			}

			boolean ready = prepareNotiRequestObj(req);

			if (ready) {
				res = new NotiMgr().insertOrUpdateNoti(req);

				if (res.getCode().equals("0000")) {
					ResponseData res2 = NotiScheduler.setSchedule(req.getDate(), req.getTime());
					// System.out.print(res2);

					if (!res2.getCode().equals("0000")) {
						res.setException(res2.getDesc());
					}
			
				}
			}
		}
		return res;
	}

	public ResponseData validateNotiRequestObj(NotiData req) {
		ResponseData response = new ResponseData();

		if (req.getAutokey() != null && !req.getAutokey().trim().equalsIgnoreCase("")) {
			try {
				long autokey = Long.parseLong(req.getAutokey());

				if (autokey < 0) {
					response.setCode("0014");
					response.setDesc("Invalid Request.");
					return response;
				}
			} catch (Exception ex) {
				ex.printStackTrace();

				response.setCode("0014");
				response.setDesc("Invalid Request.");
				return response;
			}
		}

		if (req.getTitle() == null || req.getTitle().trim().equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("Please fill Title.");
			return response;
		}

		if (req.getDescription() == null || req.getDescription().trim().equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("Please fill Description.");
			return response;
		}

		if (req.getType() == null || req.getType().trim().equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("Invalid Request.");
			return response;
		} else {
			try {
				int type = Integer.parseInt(req.getType());

				if (type != 1 && type != 2 && type != 3) {
					response.setCode("0014");
					response.setDesc("Invalid Request.");
					return response;
				}
			} catch (Exception ex) {
				ex.printStackTrace();

				response.setCode("0014");
				response.setDesc("Invalid Request.");
				return response;
			}
		}

		if (req.getTypedescription() == null || req.getTypedescription().trim().equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("Invalid Request.");
			return response;
		} else {
			if (req.getTypedescription() != null && req.getTypedescription().trim().length() > 200) {
				response.setCode("0014");
				response.setDesc("Invalid Request.");
				return response;
			}
		}

		if (req.getDate() == null || req.getDate().trim().equalsIgnoreCase("") || req.getDate().trim().length() > 10) {
			response.setCode("0014");
			response.setDesc("Invalid Request.");
			return response;
		} else {
			if (req.getDate() != null && !req.getDate().trim().equalsIgnoreCase("")
					&& req.getDate().trim().length() == 10) {
				try {
					String userDate = req.getDate().trim();
					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					Date userDateObj = df.parse(userDate);
					Date todayDateObj1 = new Date();
					String todayString = df.format(todayDateObj1);
					Date todayDateObj2 = df.parse(todayString);
					if (todayDateObj2.getTime() > userDateObj.getTime()) {
						response.setCode("0014");
						response.setDesc("Invalid Date.");
						return response;
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();

					response.setCode("0014");
					response.setDesc("Invalid Request.");
					return response;
				}
			} else {
				response.setCode("0014");
				response.setDesc("Invalid Request.");
				return response;
			}
		}

		if (req.getTime() == null || req.getTime().trim().equalsIgnoreCase("") || req.getTime().trim().length() > 8) {
			response.setCode("0014");
			response.setDesc("Invalid Request.");
			return response;
		} else {
			if (req.getTime() != null && !req.getTime().trim().equalsIgnoreCase("")
					&& req.getTime().trim().length() == 8) {
				try {
					String userTime = req.getTime().trim();
					SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
					df.parse(userTime);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();

					response.setCode("0014");
					response.setDesc("Invalid Request.");
					return response;
				}
			} else {
				response.setCode("0014");
				response.setDesc("Invalid Request.");
				return response;
			}
		}
		
		try {
			String date = req.getDate().trim();
			String time = req.getTime().trim();
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date dateTimeObj = df.parse(date+" "+time);
			Date todayObj = new Date();
			String todayString = df.format(todayObj);
			Date todayObj2 = df.parse(todayString);
			if (todayObj2.getTime() > dateTimeObj.getTime()) {
				response.setCode("0014");
				response.setDesc("Invalid Time.");
				return response;
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			response.setCode("0014");
			response.setDesc("Invalid Request.");
			return response;
		}

		if (req.getUserid() == null || req.getUserid().trim().equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("Invalid Request.");
			return response;
		} else {
			if (req.getUserid() != null && req.getUserid().trim().length() > 50) {
				response.setCode("0014");
				response.setDesc("Invalid Request.");
				return response;
			}
		}

		if (req.getUsername() == null || req.getUsername().trim().equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("Invalid Request.");
			return response;
		}

		response.setCode("0000");
		response.setDesc("Validated successfully.");

		return response;
	}
	@POST
	@Path("tokenNotiCount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ResponseData  tokenNotiCount(NotiData req){
		ResponseData data=new ResponseData();
		int count=0;
		if (req.getUserid() != null && !req.getUserid().trim().equalsIgnoreCase("")) {
			NotiMgr mgr=new NotiMgr();
//			count=mgr.tokenNotiCount(req.getUserid());
			data=mgr.tokenNotiCount(req.getUserid());
		}
		return data;
	}
	@POST
	@Path("removeNotiCount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ResponseData  removeNotiCount(NotiData req){
		ResponseData data=new ResponseData();
		int count=0;
		if (req.getUserid() != null && !req.getUserid().trim().equalsIgnoreCase("")) {
			NotiMgr mgr=new NotiMgr();
//			count=mgr.tokenNotiCount(req.getUserid());
			data=mgr.removeNotiCount(req.getUserid());
		}
		return data;
	}

	public boolean prepareNotiRequestObj(NotiData req) {
//		if (FontUtil.isZawgyi(req.getTitle().trim())) {
//			req.setTitle(FontChangeUtil.zg2uni(req.getTitle().trim()));
//		} else {
			req.setTitle(req.getTitle().trim());
//		}

//		if (FontUtil.isZawgyi(req.getDescription().trim())) {
//			req.setDescription(FontChangeUtil.zg2uni(req.getDescription().trim()));
//		} else {
			req.setDescription(req.getDescription().trim());
//		}
		req.setType(req.getType().trim());
		req.setTypedescription(req.getTypedescription().trim());
		req.setDate(req.getDate().trim());
		req.setTime(req.getTime().trim());
		req.setUserid(req.getUserid().trim());
		req.setUsername(req.getUsername().trim());
		req.setLastuserid("");
		req.setLastusername("");
		req.setSendtofcmfirebase("");
		req.setResponsecode("");
		req.setResponsebody("");
		req.setT1("");
		req.setT2("");
		req.setT3("");
		req.setN1("0");
		req.setN2("0");
		req.setN3("0");
		req.setModifiedhistory("");

		return true;
	}
	@POST
	@Path("getAllTokenNotification")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public NotificationDataSet getAllTokenNotification(NotificationDataSet request) {
		getPath();
		NotificationDataSet response = null;
		ResponseData sessionState = new ResponseData();
			response = new NotiMgr().getAllTokenNoti(request.getUserID(),request.getPageSize(),
					request.getCurrentPage());
		return response;
	}

	@POST
	@Path("getAllNotification")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public NotificationDataSet getAllNotification(NotificationDataSet request) {
		getPath();
		NotificationDataSet response = null;
		ResponseData sessionState = new ResponseData();
			response = new NotiMgr().getAllNoti(request.getSearchText().trim(), request.getPageSize(),
					request.getCurrentPage());
			return response;
	}

	@POST
	@Path("deleteNoti")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public NotiData deleteNoti(NotiData req) {
		getPath();
		NotiData response = null;
		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime(req.getSessionid(), req.getUserid(), "notification-delete");

		if (sessionState.getCode().equals("0000")) {
			if (req.getAutokey() != null && req.getAutokey().trim().length() != 0) {
				long autokey = 0;

				try {
					autokey = Long.parseLong(req.getAutokey().trim());
				} catch (Exception e) {
					response = new NotiData();
					response.setCode("0014");
					response.setDesc("Invalid Request.");
					return response;
				}

				response = new NotiMgr().deleteNoti(autokey);
			} else {
				response = new NotiData();
				response.setCode("0014");
				response.setDesc("Invalid Request.");
				return response;
			}
		} else {
			response = new NotiData();
			response.setCode(sessionState.getCode());
			response.setDesc(sessionState.getDesc());
			return response;
		}
		return response;
	}

}
