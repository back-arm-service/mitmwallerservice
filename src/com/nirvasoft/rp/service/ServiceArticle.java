package com.nirvasoft.rp.service;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.nirvasoft.cms.mgr.ArticleMgr;
import com.nirvasoft.cms.mgr.ArticleMobileMgr;
import com.nirvasoft.cms.mgr.QnAMobileMgr;
import com.nirvasoft.cms.shared.PagerMobileData;
import com.nirvasoft.cms.shared.RegisterDataSet;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.ResultMobile;
import com.nirvasoft.cms.framework.ServerSession;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.shared.ArticleDataSet;
import com.nirvasoft.cms.shared.SetupDataSet;
import com.nirvasoft.cms.shared.UserData;
import com.nirvasoft.cms.shared.UserDataset;
import com.nirvasoft.rp.dao.DAOManager;

@Path("/serviceArticle")
public class ServiceArticle {
	@Context
	HttpServletRequest request;
	@javax.ws.rs.core.Context
	ServletContext context;

	// mobile click dislike
	@GET
	@Path("clickDisLikeArticle")
	@Produces(MediaType.APPLICATION_JSON)
	public ResultMobile clickDisLikeArticle() {
		getPath();
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		ResultMobile ret;
		String key = request.getParameter("key");// fmr002 syskey
		String userSK = request.getParameter("userSK");
		ret = new ArticleMobileMgr().clickDisLikeArticle(Long.parseLong(key), userSK, "news & media", getUser());
		return ret;
	}
	@GET
	@Path("getCommentLikePerson")
	@Produces(MediaType.APPLICATION_JSON)
    public RegisterDataSet getCommentLikePerson() {
		RegisterDataSet res = new RegisterDataSet();
		String key = request.getParameter("key");
        res = new ArticleMobileMgr().getCommentLikePerson(Long.parseLong(key) , getUser());
        return res;
    }
	@GET
	@Path("getArticleLikePerson")
	@Produces(MediaType.APPLICATION_JSON)
    public RegisterDataSet getArticleLikePerson() {
		RegisterDataSet res = new RegisterDataSet();
		String key = request.getParameter("key");
        res = new ArticleMobileMgr().getArticleLikePerson(Long.parseLong(key) , getUser());
        return res;
    }

	@GET
	@Path("clickLikeArticle")
	@Produces(MediaType.APPLICATION_JSON)
	public ResultMobile clickLikeArticle() {
		getPath();
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		ResultMobile ret;
		String key = request.getParameter("key");// fmr002 syskey
		String userSK = request.getParameter("userSK");
		String type = request.getParameter("type");// type
		if (type.equalsIgnoreCase("news")) {
			type = "news & media";
		}
		ret = new ArticleMobileMgr().clickLikeArticle(Long.parseLong(key), userSK, type, getUser());
		return ret;
	}

	/////////////////////////// Mobile/////////////////////

	@GET
	@Path("clickLikeComment")
	@Produces(MediaType.APPLICATION_JSON)
	public ResultMobile clickLikeComment() {
		getPath();
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		ResultMobile ret;
		String postkey = request.getParameter("postkey");
		String comkey = request.getParameter("comkey");
		String userSK = request.getParameter("userSK");
		String type = request.getParameter("type");
		ret = new ArticleMobileMgr().clickLikeComment(Long.parseLong(postkey), Long.parseLong(comkey), Long.parseLong(userSK),
				type, getUser());
		return ret;
	}

	// mobile click undislike
	@GET
	@Path("clickUnDislikeArticle")
	@Produces(MediaType.APPLICATION_JSON)
	public ResultMobile clickUnDislikeArticle() {
		getPath();
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		ResultMobile ret;
		String userSK = "0";
		String key = request.getParameter("key");
		if (request.getParameter("userSK") != null)
			userSK = request.getParameter("userSK");
		ret = new ArticleMobileMgr().UnDislikeArticle(Long.parseLong(key), Long.parseLong(userSK), getUser());
		return ret;
	}

	@GET
	@Path("clickUnlikeArticle")
	@Produces(MediaType.APPLICATION_JSON)
	public ResultMobile clickUnlikeArticle() {
		getPath();
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		ResultMobile ret;
		String userSK = "0";
		String key = request.getParameter("key");
		if (request.getParameter("userSK") != null)
			userSK = request.getParameter("userSK");
		ret = new ArticleMobileMgr().UnlikeArticle(Long.parseLong(key), Long.parseLong(userSK), getUser());
		return ret;
	}

	@GET
	@Path("clickUnLikeComment")
	@Produces(MediaType.APPLICATION_JSON)
	public ResultMobile clickUnLikeComment() {
		getPath();
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		ResultMobile ret;
		String postkey = request.getParameter("postkey");
		String comkey = request.getParameter("comkey");
		String userSK = request.getParameter("userSK");
		String type = request.getParameter("type");
		ret = new ArticleMobileMgr().clickUnLikeComment(Long.parseLong(postkey), Long.parseLong(comkey),
				Long.parseLong(userSK), type, getUser());
		return ret;
	}

	@GET
	@Path("getArticleDataBySyskey")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArticleDataSet getArticleDataBySyskey() {
		getPath();
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		ArticleDataSet res = new ArticleDataSet();
		String syskey = request.getParameter("syskey");
		res = new ArticleMobileMgr().getArticleDataBySyskey(Long.parseLong(syskey), getUser());
		return res;
	}

	@POST
	@Path("getArticleListByContentWriter")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SetupDataSet getArticleListByContentWriter(PagerMobileData p) {
		getPath();
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		SetupDataSet res = new SetupDataSet();
		String usersk = request.getParameter("usersk");
		String userid = request.getParameter("userid");
		res = new ArticleMobileMgr().getArticleListByContentWriter(usersk, userid, p, getUser());
		return res;
	}

	public void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
	}

	private MrBean getUser() {
		getPath();
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		MrBean user = new MrBean();
		user.getUser().setOrganizationID("001");
		user.getUser().setUserId(Service001.userid);
		user.getUser().setUserName(Service001.username);
		return user;
	}

	// TDA for share
	@GET
	@Path("readForShare")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleData readForShare() {
		getPath();
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		ArticleData ret;
		String postsk = request.getParameter("postsk");
		String type = request.getParameter("type");// type=news &//
													// media,education,question
		ret = new ArticleMobileMgr().readForShare(Long.parseLong(postsk), type, getUser());
		return ret;
	}

	@GET
	@Path("readShare")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArticleDataSet readShare() {
		getPath();
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		ArticleDataSet ret;
		String postsk = request.getParameter("id");
		String type = request.getParameter("name");// type=news &//
													// media,education,question
		if (type.equalsIgnoreCase("news")) {
			type = "news & media";
		}
		ret = new ArticleMobileMgr().readShare(Long.parseLong(postsk), type, getUser());
		return ret;
	}

	/* mobile => article list based on crop and all */
	@POST
	@Path("searchArticleList")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArticleDataSet searchArticleList(PagerMobileData p) {
		getPath();
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		String searchVal = "";
		String userSK = "0";
		if (request.getParameter("searchVal") != null) {
			searchVal = request.getParameter("searchVal");
			if (searchVal.equalsIgnoreCase("All"))
				searchVal = "";
		}
		if (request.getParameter("userSK") != null)
			userSK = request.getParameter("userSK");
		String mobile = request.getParameter("mobile");
		String firstRefresh = request.getParameter("firstRefresh");
		String type = request.getParameter("type");
		if (type.equalsIgnoreCase("news")) {
			type = "news";
		}
		ArticleDataSet res = new ArticleDataSet();
		res = new ArticleMobileMgr().searchArticleList(p, mobile, firstRefresh, searchVal, userSK, type, getUser());
		return res;
	}

	@POST
	@Path("searchArticleListByContentWriter")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SetupDataSet searchArticleListByContentWriter(PagerMobileData p) {
		getPath();
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		SetupDataSet res = new SetupDataSet();
		String usersk = request.getParameter("usersk");
		String userid = request.getParameter("userid");
		String startdate = request.getParameter("startdate");
		String enddate = request.getParameter("enddate");
		res = new ArticleMobileMgr().searchArticleListByContentWriter(usersk, userid, startdate, enddate, p, getUser());
		return res;
	}

	// mobile (for save content)
	@GET
	@Path("viewByKey")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleDataSet viewByKey() {
		getPath();
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		ArticleDataSet res = new ArticleDataSet();
		String userSK = "0";
		String id = request.getParameter("id");
		String type = request.getParameter("type");
		if (type.equalsIgnoreCase("news")) {
			type = "news & media";
		}
		if (request.getParameter("userSK") != null) {
			userSK = request.getParameter("userSK");
		}
		res = new ArticleMobileMgr().viewNews(id, userSK, type, getUser());
		return res;
	}

}
