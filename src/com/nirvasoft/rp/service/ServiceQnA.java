package com.nirvasoft.rp.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.nirvasoft.cms.mgr.ArticleMgr;
import com.nirvasoft.cms.mgr.QnAMobileMgr;
import com.nirvasoft.cms.shared.PagerMobileData;
import com.nirvasoft.cms.shared.RegisterDataSet;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.ResultMobile;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.shared.ArticleDataSet;
import com.nirvasoft.cms.util.FontChangeUtil;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.framework.ServerSession;

@Path("/serviceQuestion")
public class ServiceQnA {
	public static String convertStreamToString(InputStream inStream) throws Exception {
		InputStreamReader inputStream = new InputStreamReader(inStream);
		BufferedReader bReader = new BufferedReader(inputStream);
		StringBuilder sb = new StringBuilder();
		String line = null;
		while ((line = bReader.readLine()) != null) {
			sb.append(line);
		}
		return sb.toString();
	}

	@Context
	HttpServletRequest request;
	@Context
	private HttpServletResponse response;
	@javax.ws.rs.core.Context
	ServletContext context;

	/* click dilike question */
	@GET
	@Path("clickDisLikeQuestion")
	@Produces(MediaType.APPLICATION_JSON)
	public ResultMobile clickDisLikeQuestion() {
		getPath();
		ResultMobile ret;
		String key = request.getParameter("key");
		String userSK = request.getParameter("userSK");
		ret = new QnAMobileMgr().clickDisLikeQuestion(Long.parseLong(key), userSK, "question", getUser());
		return ret;
	}

	////////////////////////////////////////////////////// Mobile////////////////////////////////////////////////////

	/* like question */
	@GET
	@Path("clickLikeQuestion")
	@Produces(MediaType.APPLICATION_JSON)
	public ResultMobile clickLikeQuestion() {
		getPath();
		ResultMobile ret;
		String key = request.getParameter("key");
		String userSK = request.getParameter("userSK");
		ret = new QnAMobileMgr().clickLikeQuestion(Long.parseLong(key), userSK, "question", getUser());
		return ret;
	}

	/* click undilike question */
	@GET
	@Path("clickUnDislikeQuestion")
	@Produces(MediaType.APPLICATION_JSON)
	public ResultMobile clickUnDislikeQuestion() {
		getPath();
		ResultMobile ret;
		String userSK = "0";
		String key = request.getParameter("key");
		if (request.getParameter("userSK") != null) {
			userSK = request.getParameter("userSK");
		}
		ret = new QnAMobileMgr().UnDislikeQuestion(Long.parseLong(key), Long.parseLong(userSK), getUser());
		return ret;
	}

	/* click unlike question */
	@GET
	@Path("clickUnlikeQuestion")
	@Produces(MediaType.APPLICATION_JSON)
	public ResultMobile clickUnlikeQuestion() {
		getPath();
		ResultMobile ret;
		String userSK = "0";
		String key = request.getParameter("key");
		if (request.getParameter("userSK") != null) {
			userSK = request.getParameter("userSK");
		}
		ret = new QnAMobileMgr().UnlikeQuestion(Long.parseLong(key), Long.parseLong(userSK), getUser());
		return ret;
	}

	@GET
	@Path("/deleteComment")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ResultMobile deleteComment() {
		getPath();
		String syskey = request.getParameter("syskey");
		ResultMobile res = new ResultMobile();
		res = new QnAMobileMgr().deleteComment(Long.parseLong(syskey), getUser());
		return res;
	}

	/* deletePrivatePost */
	@POST
	@Path("deletePrivatePost")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ResultMobile deletePrivatePost(ArticleData p) {
		getPath();
		ResultMobile res = new ResultMobile();
		res = new QnAMobileMgr().deletePrivatePost(p, getUser());
		return res;
	}

	/* save comment */
	/*
	 * @POST
	 * 
	 * @Path("saveAnswer")
	 * 
	 * @Produces(MediaType.APPLICATION_JSON)
	 * 
	 * @Consumes(MediaType.APPLICATION_JSON) public ArticleDataSet
	 * saveAnswer(ArticleData p) { ArticleDataSet res = new ArticleDataSet();
	 * ArticleDataSet ret = new ArticleDataSet(); res = QnAMgr.saveComment(p,
	 * getUser()); if (res.isState()) { if (p.getN3() == (-1)) { ret =
	 * QnAMgr.readForCommentNotiAdmin(p, getUser()); } else { ret =
	 * QnAMgr.readForCommentNoti(p, getUser()); } for (int i = 0; i <
	 * ret.getData().length; i++) { p.setT3(ret.getData()[i].getT3()); if
	 * (ServerUtil.isZawgyiEncoded(p.getT1())) {
	 * p.setT1(FontChangeUtil.zg2uni(p.getT1())); } if
	 * (ServerUtil.isZawgyiEncoded(p.getT2())) {
	 * p.setT2(FontChangeUtil.zg2uni(p.getT2())); } if (p.getT2().length() > 21)
	 * { p.setT2(p.getT2().substring(0, 20)); } else {
	 * p.setSyskey(ret.getData()[i].getSyskey()); }
	 * p.setSyskey(ret.getData()[i].getSyskey()); // goNotificationLinkIOS(p); }
	 * 
	 * } return res; }
	 */

	@GET
	@Path("/deleteReplyComment")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ResultMobile deleteReplyComment() {
		getPath();
		String syskey = request.getParameter("syskey");
		ResultMobile res = new ResultMobile();
		res = new QnAMobileMgr().deleteReplyComment(Long.parseLong(syskey), getUser());
		return res;
	}

	/* commente notification */
	/*
	 * public int goNotificationLinkForSaveComment(ArticleData p) { String
	 * FCM_URL = "https://fcm.googleapis.com/fcm/send"; // String
	 * FCM_SERVER_API_KEY = // "AIzaSyCyHIF2pJE_JwwyQmcI2m0thLw_sxwkng8"; String
	 * FCM_SERVER_API_KEY = "AIzaSyDcaERuLE19XrNuZXZL8P4bOSq18KnYoZg";
	 * System.out.println(p.getT3() + "  OUT PUT DEVIVE ID"); String
	 * deviceRegistrationId = p.getT3(); int responseCode = -1; String
	 * responseBody = null; byte[] postData; try { System.out.println(
	 * "Sending FCM request"); if (p.getT3().equalsIgnoreCase("quesiton")) {
	 * postData = getPostData(deviceRegistrationId, p.getT2(), p); } else {
	 * postData = getPostDataComment(deviceRegistrationId, p.getT2(), p); } URL
	 * url = new URL(FCM_URL); HttpsURLConnection httpURLConnection =
	 * (HttpsURLConnection) url.openConnection(); // set timeputs to 10 seconds
	 * httpURLConnection.setConnectTimeout(60000);
	 * httpURLConnection.setReadTimeout(20000);
	 * httpURLConnection.setDoInput(true); httpURLConnection.setDoOutput(true);
	 * httpURLConnection.setUseCaches(false);
	 * httpURLConnection.setRequestMethod("POST");
	 * httpURLConnection.setRequestProperty("Content-Type", "application/json");
	 * httpURLConnection.setRequestProperty("Content-Length",
	 * Integer.toString(postData.length));
	 * httpURLConnection.setRequestProperty("Authorization", "key=" +
	 * FCM_SERVER_API_KEY); OutputStream out =
	 * httpURLConnection.getOutputStream(); out.write(postData); out.close();
	 * responseCode = httpURLConnection.getResponseCode(); // success if
	 * (responseCode == HttpStatus.SC_OK) { responseBody =
	 * convertStreamToString(httpURLConnection.getInputStream());
	 * System.out.println("FCM message sent : " + responseBody); } // failure
	 * else { responseBody =
	 * convertStreamToString(httpURLConnection.getErrorStream());
	 * System.out.println( "Sending FCM request failed for regId: " +
	 * deviceRegistrationId + " response: " + responseBody); } } catch
	 * (IOException ioe) { System.out.println(
	 * "IO Exception in sending FCM request. regId: " + deviceRegistrationId);
	 * ioe.printStackTrace(); } catch (Exception e) { System.out.println(
	 * "Unknown exception in sending FCM request. regId: " +
	 * deviceRegistrationId); e.printStackTrace(); } return responseCode;
	 * 
	 * }
	 */

	/*
	 * public byte[] getPostDataComment(String registrationId, String t2,
	 * ArticleData p) throws JSONException { HashMap<String, String> dataMap =
	 * new HashMap<>(); // HashMap<String, String> objectMap = new HashMap<>();
	 * JSONObject payloadObject = new JSONObject(); dataMap.put("title",
	 * "The Farmer"); dataMap.put("body", p.getT2()); dataMap.put("key",
	 * Long.toString(p.getN1())); dataMap.put("content", "Question & Answer");
	 * dataMap.put("notitype", "question"); dataMap.put("click_action",
	 * ".activity.DrawerActivity"); JSONObject data = new JSONObject(dataMap);
	 * // JSONObject objdata = new JSONObject(objectMap);
	 * payloadObject.put("notification", data); payloadObject.put("to",
	 * registrationId); payloadObject.put("delay_while_idle", false);
	 * payloadObject.put("priority", "high");
	 * payloadObject.put("content_available", true);
	 * payloadObject.put("collapse_key", "Updates Available"); return
	 * payloadObject.toString().getBytes(Charset.forName("UTF-8")); }
	 */

	/*
	 * public byte[] getPostData(String registrationId, String t2, ArticleData
	 * p) throws JSONException { HashMap<String, String> dataMap = new
	 * HashMap<>(); // HashMap<String, String> objectMap = new HashMap<>();
	 * JSONObject payloadObject = new JSONObject(); dataMap.put("title",
	 * "The Farmer"); dataMap.put("body", p.getT2()); dataMap.put("key",
	 * Long.toString(p.getSyskey())); dataMap.put("content", "Question & Answer"
	 * ); dataMap.put("notitype", "question"); dataMap.put("click_action",
	 * ".activity.DrawerActivity"); JSONObject data = new JSONObject(dataMap);
	 * // JSONObject objdata = new JSONObject(objectMap);
	 * payloadObject.put("notification", data); payloadObject.put("to",
	 * registrationId); // payloadObject.put("to", "/topics/allDevices");//for
	 * all payloadObject.put("delay_while_idle", false);
	 * payloadObject.put("priority", "high");
	 * payloadObject.put("content_available", true);
	 * payloadObject.put("collapse_key", "Updates Available"); return
	 * payloadObject.toString().getBytes(Charset.forName("UTF-8")); }
	 */

	// atn
	@GET
	@Path("getCommentmobile")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleDataSet getCommentmobile() {
		getPath();
		String id = request.getParameter("id");
		String userSK = request.getParameter("userSK");
		String sessionKey = request.getParameter("sessionKey");
		ArticleDataSet res = new ArticleDataSet();
		res = new QnAMobileMgr().getComment(id,sessionKey, userSK, getUser());
		System.out.println("res: " + res.getData());
		return res;
	}

	/* get all comment */
	@GET
	@Path("getCommentReplymobile")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleDataSet getCommentReplymobile() {
		getPath();
		String id = request.getParameter("id");
		String userSK = request.getParameter("userSK");
		ArticleDataSet res = new ArticleDataSet();
		res = new QnAMobileMgr().getCommentReplymobile(id, userSK, getUser());
		System.out.println("res: " + res.getData());
		return res;
	}

	public void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
	}

	private MrBean getUser() {
		this.getPath();
		MrBean user = new MrBean();
		user.getUser().setOrganizationID("001");
		user.getUser().setUserId(Service001.userid);
		user.getUser().setUserName(Service001.username);
		return user;
	}

	/* PrivateQuestionList */
	@POST
	@Path("PrivatePostList")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArticleDataSet PrivatePostList(PagerMobileData p) {
		getPath();
		ArticleDataSet res = new ArticleDataSet();
		String userSK = "";
		String type = request.getParameter("type");
		if (type.equalsIgnoreCase("news")) {
			type = "news & media";
		}
		if (request.getParameter("userSK") != null) {
			userSK = request.getParameter("userSK");
		}
		res = new QnAMobileMgr().PrivatePostList(p, userSK, type, getUser());
		return res;
	}

	/* save commment new */
	@POST
	@Path("saveAnswer")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArticleDataSet saveAnswer(ArticleData p) {
		getPath();
		ArticleDataSet res = new ArticleDataSet();
		ArticleDataSet ret = new ArticleDataSet();
		if (ServerUtil.isZawgyiEncoded(p.getT2())) {
			p.setT2(FontChangeUtil.zg2uni(p.getT2()));
		}
		res = new QnAMobileMgr().saveComment(p, getUser());
		if (res.isState()) {
			if (p.getN3() == (-1)) {
				ret = new QnAMobileMgr().readForCommentNotiAdmin(p, getUser());
			} else {
				ret = new QnAMobileMgr().readForCommentNoti(p, getUser());
			}
			for (int i = 0; i < ret.getData().length; i++) {
				p.setT3(ret.getData()[i].getT3());
				if (ServerUtil.isZawgyiEncoded(p.getT1())) {
					p.setT1(FontChangeUtil.zg2uni(p.getT1()));
				}
				if (ServerUtil.isZawgyiEncoded(p.getT2())) {
					p.setT2(FontChangeUtil.zg2uni(p.getT2()));
				}
				if (p.getT2().length() > 21) {
					p.setT2(p.getT2().substring(0, 20));
				} else {
					p.setSyskey(ret.getData()[i].getSyskey());
				}
				p.setSyskey(ret.getData()[i].getSyskey());
				// goNotificationLinkIOS(p);
			}

		}
		return res;
	}

	/* save commment new */
	@POST
	@Path("saveCommentLike")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArticleDataSet saveCommentLike(ArticleData p) throws SQLException {
		getPath();
		ArticleDataSet res = new ArticleDataSet();
		res = new QnAMobileMgr().saveCommentLike(p, getUser());

		return res;
	}

	// Reply comment
	@POST
	@Path("saveCommentReply")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArticleDataSet saveCommentReply(ArticleData p) {
		getPath();
		ArticleDataSet res = new ArticleDataSet();
		ArticleDataSet ret = new ArticleDataSet();
		if (ServerUtil.isZawgyiEncoded(p.getT2())) {
			p.setT2(FontChangeUtil.zg2uni(p.getT2()));
		}
		res = new QnAMobileMgr().saveCommentReply(p, getUser());
		if (res.isState()) {
			if (p.getN3() == (-1)) {
				ret = new QnAMobileMgr().readForCommentReplyNotiAdmin(p, getUser());
			} else {
				ret = new QnAMobileMgr().readForCommentReplyNoti(p, getUser());
			}
			for (int i = 0; i < ret.getData().length; i++) {
				p.setT3(ret.getData()[i].getT3());
				if (ServerUtil.isZawgyiEncoded(p.getT1())) {
					p.setT1(FontChangeUtil.zg2uni(p.getT1()));
				}
				if (ServerUtil.isZawgyiEncoded(p.getT2())) {
					p.setT2(FontChangeUtil.zg2uni(p.getT2()));
				}
				if (p.getT2().length() > 21) {
					p.setT2(p.getT2().substring(0, 20));
				} else {
					p.setSyskey(ret.getData()[i].getSyskey());
				}
				p.setSyskey(ret.getData()[i].getSyskey());
				// goNotificationLinkIOS(p);
			}

		}
		return res;
	}

	/* ionic noti for first */

	/* ionic notification */

	/* save question mobile */
	@POST
	@Path("saveQuestions")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArticleData saveQuestions(ArticleData p) {
		getPath();
		ArticleData res = new ArticleData();
		res = new QnAMobileMgr().saveQuestions(p, getUser());
		return res;
	}

	/* mobile => question list based on crop and all */
	@POST
	@Path("searchQuestionList")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArticleDataSet searchQuestionList(PagerMobileData p) {
		getPath();
		String searchVal = "";
		String userSK = "0";
		if (request.getParameter("searchVal") != null) {
			searchVal = request.getParameter("searchVal");
			if (searchVal.equalsIgnoreCase("All"))
				searchVal = "";
		}
		if (request.getParameter("userSK") != null)
			userSK = request.getParameter("userSK");
		String mobile = request.getParameter("mobile");
		String firstRefresh = request.getParameter("firstRefresh");
		ArticleDataSet res = new ArticleDataSet();
		res = new QnAMobileMgr().searchQuestionList(p, mobile, firstRefresh, searchVal, userSK, "", getUser());
		return res;
	}

	/* UpdatePrivatePost */
	@POST
	@Path("UpdatePrivatePost")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArticleData UpdatePrivatePost(ArticleData p) {
		getPath();
		ArticleData res = new ArticleData();
		res = new QnAMobileMgr().UpdatePrivatepost(p, getUser());
		return res;
	}

	/* mobile (for save content detail) */
	@GET
	@Path("viewByKey")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleDataSet viewByKey() {
		getPath();
		String userSK = "0";
		String id = request.getParameter("id");
		if (request.getParameter("userSK") != null) {
			userSK = request.getParameter("userSK");
		}
		ArticleDataSet res = new ArticleDataSet();
		res = new QnAMobileMgr().viewQuestions(id, userSK, getUser());
		return res;
	}	
}
