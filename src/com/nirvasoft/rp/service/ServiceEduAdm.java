package com.nirvasoft.rp.service;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.framework.ServerSession;
import com.nirvasoft.cms.mgr.EduMgr;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.shared.ArticleDataSet;
import com.nirvasoft.cms.shared.CommentDataSet;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.rp.dao.DAOManager;

@Path("/serviceEduAdm")
public class ServiceEduAdm {
	@Context
	HttpServletRequest request;
	@javax.ws.rs.core.Context
	ServletContext context;

	@GET
	@Path("clickDisLikeEdu")
	@Produces(MediaType.APPLICATION_JSON)
	public Resultb2b clickDisLikeEdu() {
		this.getPath();
		Resultb2b ret;
		String key = request.getParameter("key");
		String userSK = request.getParameter("userSK");
		ret = EduMgr.clickDisLikeEdu(Long.parseLong(key), userSK, "education", getUser());
		return ret;
	}

	@GET
	@Path("clickLikeEdu")
	@Produces(MediaType.APPLICATION_JSON)
	public Resultb2b clickLikeEdu() {
		this.getPath();
		Resultb2b ret;
		String key = request.getParameter("key");
		String userSK = request.getParameter("userSK");
		ret = EduMgr.clickLikeEdu(Long.parseLong(key), userSK, "education", getUser());
		return ret;
	}

	@GET
	@Path("clickUnDislikeEdu")
	@Produces(MediaType.APPLICATION_JSON)
	public Resultb2b clickUnDislikeEdu() {
		this.getPath();
		Resultb2b ret;
		String userSK = "0";
		String key = request.getParameter("key");
		if (request.getParameter("userSK") != null)
			userSK = request.getParameter("userSK");
		ret = EduMgr.UnDislikeEdu(Long.parseLong(key), Long.parseLong(userSK), getUser());
		return ret;
	}

	@GET
	@Path("clickUnlikeEdu")
	@Produces(MediaType.APPLICATION_JSON)
	public Resultb2b clickUnlikeEdu() {
		this.getPath();
		Resultb2b ret;
		String userSK = "0";
		String key = request.getParameter("key");
		if (request.getParameter("userSK") != null)
			userSK = request.getParameter("userSK");
		ret = EduMgr.UnlikeEdu(Long.parseLong(key), Long.parseLong(userSK), getUser());
		return ret;
	}

	@POST
	@Path("deleteComment")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArticleDataSet deleteComment(ArticleData p) {
		this.getPath();
		ArticleDataSet res = new ArticleDataSet();
		res = EduMgr.deleteComment(p, getUser());
		return res;
	}

	@POST
	@Path("deleteEdu")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Resultb2b deleteEdu(ArticleData p) {
		this.getPath();
		Resultb2b res = new Resultb2b();
		Long key = p.getSyskey();
		res = EduMgr.deleteEdu(key, getUser());

		return res;
	}

	@GET
	@Path("eduLikeList")
	@Produces(MediaType.APPLICATION_JSON)
	public CommentDataSet eduLikeList() {
		this.getPath();
		String articleSK = request.getParameter("articleSK");
		CommentDataSet ret;
		ret = EduMgr.searchLikeUserList(articleSK, getUser());
		return ret;
	}

	@GET
	@Path("getComments")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleDataSet getComments() {
		this.getPath();
		String id = request.getParameter("id");
		ArticleDataSet res = new ArticleDataSet();
		res = EduMgr.getComments(id, getUser());
		return res;
	}

	public void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = context.getRealPath("");
	}

	private MrBean getUser() {
		this.getPath();
		MrBean user = new MrBean();
		user.getUser().setOrganizationID("001");
		user.getUser().setUserId(Service001.userid);
		user.getUser().setUserName(Service001.username);
		return user;
	}

	@GET
	@Path("readBySyskey")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleData readDataByArticle() {
		this.getPath();
		ArticleData ret;
		String key = request.getParameter("key");
		ret = EduMgr.readDataBySyskey(Long.parseLong(key), getUser());
		return ret;
	}

	@GET
	@Path("readVideoBySyskey")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleData readDataByVideo() {
		this.getPath();
		ArticleData ret;
		String key = request.getParameter("key");
		ret = EduMgr.readDataBySyskey(Long.parseLong(key), getUser());
		return ret;
	}

	@POST
	@Path("saveComment")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArticleDataSet saveComment(ArticleData p) {
		this.getPath();
		ArticleDataSet res = new ArticleDataSet();
		res = EduMgr.saveComment(p, getUser());
		return res;
	}

	// mobile
	@POST
	@Path("searchEdu")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArticleDataSet searchEdu(PagerData p) {
		this.getPath();
		String searchVal = request.getParameter("searchVal");
		ArticleDataSet res = new ArticleDataSet();
		res = EduMgr.searchEdu(p, searchVal, getUser());
		return res;
	}

	/// web
	@POST
	@Path("searchEduLists")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArticleDataSet searchEduLists(PagerData p) {
		this.getPath();
		String croptype = request.getParameter("croptype");
		if (p.getT1() != "" && p.getT1().contains("/")) {
			p.setT1(ServerUtil.datetoString1(p.getT1()));// SearchVal==p.getT1
		}
		ArticleDataSet res = new ArticleDataSet();
		res = EduMgr.searchEduLists(p, croptype, getUser());
		return res;
	}

	@GET
	@Path("searchLike")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleDataSet searchLike() {
		this.getPath();
		String type = request.getParameter("type");
		String key = request.getParameter("key");
		ArticleDataSet ret;
		ret = EduMgr.searchLike(type, Long.parseLong(key), getUser());
		return ret;
	}

	@GET
	@Path("searchLikeCount")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleDataSet searchLikeCount() {
		this.getPath();
		String id = request.getParameter("key");
		ArticleDataSet res = new ArticleDataSet();
		res = EduMgr.searchLikeCount(id, getUser());
		return res;
	}

	@GET
	@Path("viewByID")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleDataSet viewByID() {
		this.getPath();
		String id = request.getParameter("id");
		ArticleDataSet res = new ArticleDataSet();
		res = EduMgr.viewArticle(id, getUser());
		return res;
	}

	// mobile (for save content)
	@GET
	@Path("viewByKey")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleDataSet viewByKey() {
		this.getPath();
		String userSK = "0";
		String id = request.getParameter("id");// post
		if (request.getParameter("userSK") != null) {
			userSK = request.getParameter("userSK");// Login User
		}
		ArticleDataSet res = new ArticleDataSet();
		res = EduMgr.viewEdu(id, userSK, getUser());
		return res;
	}

}
