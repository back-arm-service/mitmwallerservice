package com.nirvasoft.rp.service;

import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.nirvasoft.cms.mgr.DocumentMgr;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.framework.ServerSession;
import com.nirvasoft.rp.shared.DocumentListResponseData;
import com.nirvasoft.rp.shared.DocumentRequest;
import com.nirvasoft.rp.shared.DocumentResponse;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ServerGlobal;

@Path("/serviceDocument")
public class ServiceDocument {

	@Context
	HttpServletRequest request;
	@Context
	private HttpServletResponse response;
	@javax.ws.rs.core.Context
	ServletContext context;

	@POST
	@Path("getAllPdf")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DocumentListResponseData getAllPdf(DocumentRequest req) {
		getPath();

		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Getting State List.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}

		DocumentListResponseData res = new DocumentMgr().getAllPdf();

		return res;
	}

	@POST
	@Path("getKeyword")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DocumentResponse getKeyword(DocumentRequest req) {
		getPath();

		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Getting All Document List By Region.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}

		DocumentResponse res = new DocumentMgr().getKeyword(req);

		return res;
	}

	private void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
	}

	@POST
	@Path("getPdf")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DocumentResponse getPdf(DocumentRequest req) {
		getPath();

		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Getting Document List By Region and Type.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}

		DocumentResponse responseStateList = new DocumentMgr().getPdfByTypeAndRegion(req);

		return responseStateList;
	}

}
