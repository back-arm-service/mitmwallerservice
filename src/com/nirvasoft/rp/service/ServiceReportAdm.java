package com.nirvasoft.rp.service;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.nirvasoft.cms.shared.TempData;
import com.nirvasoft.cms.shared.WalletTransData;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.nirvasoft.cms.mgr.ExcelFileMgr;
import com.nirvasoft.cms.mgr.ReportMgr;
import com.nirvasoft.cms.shared.WalletTransDataRequest;
import com.nirvasoft.cms.shared.WalletTransDataResponse;
import com.nirvasoft.cms.util.CommonMgr;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.framework.ServerSession;
import com.nirvasoft.rp.mgr.SessionMgr;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.shared.UserReportData;

import net.sf.jasperreports.engine.JasperPrint;

@Path("/ServiceReportAdm")
public class ServiceReportAdm {
	@Context
	HttpServletRequest request;
	@Context
	private HttpServletResponse response;
	@javax.ws.rs.core.Context
	ServletContext context;
	String UPLOAD_DIRECTORY = "";
	String FONT_DIRECTORY = "";
	String IMAGE_DIRECTORY = "";
	private static ArrayList<TempData> l_TempList;
	private static ArrayList<TempData> lstTmpData;
	

	@GET
	@Path("/downloadexcel")
	@Produces(MediaType.TEXT_PLAIN)
	public String downloadexcel() {
		this.getPath();
		String path = context.getRealPath("");
		String filename = request.getParameter("filename");
		String folder = "/ExcelExport";
		CommonMgr.downloadFile(folder, filename, response);
		return "Ok";
	}

	/*
	 * @POST
	 * 
	 * @Path("getAccountTrans")
	 * 
	 * @Produces(MediaType.APPLICATION_JSON)
	 * 
	 * @Consumes(MediaType.APPLICATION_JSON) public Result
	 * getAccountTrans(WalletTransDataRequest data) { Result res = new Result();
	 * res.setState(true); getPath(); ResponseData sessionState = new
	 * ResponseData(); sessionState = new
	 * SessionMgr().updateActivityTime(data.getSessionID(), data.getUserID());
	 * 
	 * if (sessionState.getCode().equals("0000")) { wlData = new
	 * ReportMgr().getReportData(data); if (wlData.size() != 0) {
	 * res.setMsgCode("0000"); } else { res.setMsgCode("0016"); res.setMsgDesc(
	 * "Data not found"); } } else { res.setMsgCode(sessionState.getCode());
	 * res.setMsgDesc(sessionState.getDesc()); } return res; }
	 */

	/*
	 * @GET
	 * 
	 * @Path("printTransactionReport") public void printTransactionReport()
	 * throws JsonParseException, JsonMappingException, IOException { String
	 * pRptCode = "AccTransReport"; JasperPrint jPrint = null; try {
	 * UPLOAD_DIRECTORY = context.getRealPath("/") + "\\rpt\\"; FONT_DIRECTORY =
	 * context.getRealPath("/") + "\\fontmap\\fontmap.json"; IMAGE_DIRECTORY =
	 * context.getRealPath("/") + "\\images\\icon.png"; jPrint =
	 * CommonMgr.printTransactionReport(pRptCode, wlData, UPLOAD_DIRECTORY,
	 * FONT_DIRECTORY, IMAGE_DIRECTORY); CommonMgr.exportPdf(jPrint, response);
	 * } catch (Exception e) { e.printStackTrace(); } }
	 */
	@GET
	@Path("MerchantTransferReport")
	@Consumes(MediaType.APPLICATION_JSON)
	public void MerchantTransferReport() throws JsonParseException, JsonMappingException, IOException {
		String pRptCode = "rptWalletTransfer";
		try {
			UPLOAD_DIRECTORY = context.getRealPath("/") + "\\rpt\\";
			FONT_DIRECTORY = context.getRealPath("/") + "\\fontmap\\fontmap.json";
			IMAGE_DIRECTORY = context.getRealPath("/") + "\\images\\icon.png";
			JasperPrint jPrint = CommonMgr.MerchantTransferReport(pRptCode, l_TempList,lstTmpData, UPLOAD_DIRECTORY, FONT_DIRECTORY,
					IMAGE_DIRECTORY);
			CommonMgr.exportPdf(jPrint, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@POST
	@Path("getWalletTopupReportJasperDownload")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Result getWalletTopupListJasperDownload(WalletTransDataRequest p) {
		this.getPath();
		Result ret = new Result();
		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime(p.getSessionID(), p.getUserID(),"acctransfer");
		if (sessionState.getCode().equals("0000")) {
			lstTmpData = new ReportMgr().getWalletTransferReportJasperDownload(p);
			l_TempList = new ReportMgr().getWalletTransferCountReportJasper(p);
			 if(lstTmpData.size()>0)
		     ret.setState(true);
		     else
		     ret.setState(false);
			
		} else {
			ret.setState(false);
			ret.setMsgCode(sessionState.getCode());
			ret.setMsgDesc(sessionState.getDesc());
		}
		return ret;
	}
	@POST
	@Path("getWalletTopupReportJasper")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public WalletTransDataResponse getWalletTopupListJasper(WalletTransDataRequest p) {
		this.getPath();
		Result ret = new Result();
		ResponseData sessionState = new ResponseData();
		WalletTransDataResponse response=new WalletTransDataResponse();
		String download = request.getParameter("download");
		sessionState = new SessionMgr().updateActivityTime(p.getSessionID(), p.getUserID(),"acctransfer");
		if (sessionState.getCode().equals("0000")) {
				response=new ReportMgr().getWalletTransferReportJasper(p);
			}
			
		 else {
			 response.setState(false);
			 response.setMsgCode(sessionState.getCode());
			 response.setMsgDesc(sessionState.getDesc());
		}
		return response;
	}
	@GET
	@Path("downloadReport")
	@Consumes(MediaType.APPLICATION_JSON)
	public void downloadReport(@QueryParam("sessionID") String sessionId, @QueryParam("userID") String userID) {
		String fileType = request.getParameter("fileType");
		String fileName = request.getParameter("fileName");
		String folder = "";
		if (fileType.equalsIgnoreCase("pdf")) {
			folder = "/download/pdf";
		} 
		if (fileType.equalsIgnoreCase("excel")){
			folder = "/download/excel";
		}			
		SessionMgr seiMgr = new SessionMgr();
		ResponseData sessionRes = seiMgr.updateActivityTime(sessionId, userID);
		if (sessionRes.getCode().equalsIgnoreCase("0000"))
			CommonMgr.downloadPDFFile(fileType, folder, fileName, response);
	}

	private void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
	}

	@POST
	@Path("/exportTransactionReport")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Result getRepaymentSummeryDownload(WalletTransDataRequest data) {
		this.getPath();
		Result res = new Result();
		DAOManager.AbsolutePath = context.getRealPath("");
		String path = context.getRealPath("");
		String folder = path + "/ExcelExport";
		String fileName = new ExcelFileMgr().exportTransactionReport(data, folder);
		res.setMsgCode(fileName);
		return res;
	}

	// user report start
	@POST
	@Path("getUserReportList1")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserReportData getUserReportList1(UserReportData p) {
		this.getPath();
//		Result ret = new Result();
		UserReportData ret=new UserReportData();
		ResponseData sessionState = new ResponseData();
		String download = request.getParameter("download");
		sessionState = new SessionMgr().updateActivityTime(p.getSessionID(), p.getUserID(),"User Report");
		if (sessionState.getCode().equals("0000")) {
			ret = new ReportMgr().getUserReportList1(p,download );
		} else {
			ret.setMsgCode(sessionState.getCode());
			ret.setMsgDesc(sessionState.getDesc());
		}
		return ret;
	}
	@POST
	@Path("getWalletTopupReport")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public WalletTransDataResponse getWalletTopupList(WalletTransDataRequest p) {
		this.getPath();
		//ArrayList<WalletTransData> ret = new ArrayList<WalletTransData>();
		WalletTransDataResponse list = new WalletTransDataResponse();
		ResponseData sessionState = new ResponseData();
		String download = request.getParameter("download");
		sessionState = new SessionMgr().updateActivityTime(p.getSessionID(), p.getUserID(),"acctransfer");
		if (sessionState.getCode().equals("0000")) {
			list = new ReportMgr().getWalletTopupList(p,download);
		} else {
		list.setMsgCode(sessionState.getCode());
		list.setMsgDesc(sessionState.getDesc());
		}
		return list;
	}
	@POST
	@Path("getSettlementReport")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public WalletTransDataResponse getSettlementList(WalletTransDataRequest p) {
		this.getPath();
		Result ret = new Result();
		WalletTransDataResponse list = new WalletTransDataResponse();
		ResponseData sessionState = new ResponseData();
		String download = request.getParameter("download");
		sessionState = new SessionMgr().updateActivityTime(p.getSessionID(), p.getUserID(),"settlement-report");
		if (sessionState.getCode().equals("0000")) {
			list = new ReportMgr().getSettlementList(p,download);
		} else {
			ret.setMsgCode(sessionState.getCode());
			ret.setMsgDesc(sessionState.getDesc());
		}
		return list;
	}
	@POST
	@Path("gopostTransaction")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public WalletTransDataResponse gopostTransaction(WalletTransDataRequest p) {
		this.getPath();
		Result ret = new Result();
		WalletTransDataResponse list = new WalletTransDataResponse();
		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime(p.getSessionID(), p.getUserID(),"settlement-report");
		if (sessionState.getCode().equals("0000")) {
			list = new ReportMgr().gopostTransaction(p);
		} else {
			ret.setMsgCode(sessionState.getCode());
			ret.setMsgDesc(sessionState.getDesc());
		}
		return list;
	}
}
