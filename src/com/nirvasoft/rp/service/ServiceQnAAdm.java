package com.nirvasoft.rp.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONException;
import org.json.JSONObject;

import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Result;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.framework.ServerSession;
import com.nirvasoft.cms.mgr.QnAMgr;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.shared.ArticleDataSet;
import com.nirvasoft.cms.shared.DivisionComboDataSet;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.cms.util.ServerUtil;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.shared.ContentReq;

@Path("/serviceQuestionAdm")
public class ServiceQnAAdm {
	@Context
	HttpServletRequest request;
	@javax.ws.rs.core.Context
	ServletContext context;
	public void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";		
	}

	private MrBean getUser() {
		this.getPath();
		MrBean user = new MrBean();
		user.getUser().setOrganizationID("001");
		user.getUser().setUserId(Service001.userid);
		user.getUser().setUserName(Service001.username);
		return user;
	}

	////////////////////////////////// CMS///////////////////////////////////////////////////////////
	/* Web Question and Answer List */
	@POST
	@Path("searchQuesList")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArticleDataSet searchQuesList(PagerData p) {
		this.getPath();
		long status = Long.parseLong(request.getParameter("status"));// for save
		String statetype = request.getParameter("statetype");// for combo status
		if (p.getT1() != "" && p.getT1().contains("/")) {
			p.setT1(ServerUtil.datetoString1(p.getT1()));// SearchVal==p.getT1
		}
		ArticleDataSet res = new ArticleDataSet();
		res = QnAMgr.searchQuesList(p, status, statetype, getUser());
		return res;
	}

	/* deleteQuestion */
	/*@POST
	@Path("deleteQuestion")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Resultb2b deleteQuestion(ArticleData p) {
		this.getPath();
		Resultb2b res = new Resultb2b();
		res = QnAMgr.deleteQuestion(p.getSyskey(), p.getT3(), getUser());
		return res;
	}*/

	/* readBySyskey */
	@GET
	@Path("readBySyskey")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleData readDataByQuestion() {
		this.getPath();
		ArticleData ret;
		String key = request.getParameter("key");
		ret = QnAMgr.readDataBySyskey(Long.parseLong(key), getUser());
		return ret;
	}

	/* getStatusList */
	@GET
	@Path("getStatusList")
	@Produces(MediaType.APPLICATION_JSON)
	public DivisionComboDataSet getStatusList() {
		this.getPath();
		DivisionComboDataSet res = new DivisionComboDataSet();
		long status = Long.parseLong(request.getParameter("status"));// for
																		// save//
																		// status
		res = QnAMgr.getStatusList(status, getUser());
		return res;
	}

	/* saveQuestion */
	@POST
	@Path("saveQuestion")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Resultb2b saveQuestion(ArticleData p) {
		this.getPath();
		Resultb2b res = new Resultb2b();
		res = QnAMgr.saveQuestion(p, getUser());
		return res;
	}

	@GET
	@Path("getMenuCount")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleDataSet searchCropAllEdu() {
		this.getPath();
		ArticleDataSet res = new ArticleDataSet();
		long status = Long.parseLong(request.getParameter("status"));
		res = QnAMgr.getMenuCount(status, getUser());
		return res;
	}

	/* clickLikeQuestion */
	@GET
	@Path("clickLikeQuestion")
	@Produces(MediaType.APPLICATION_JSON)
	public Resultb2b clickLikeQuestion() {
		this.getPath();
		Resultb2b ret;
		String key = request.getParameter("key");
		String userSK = request.getParameter("userSK");
		ret = QnAMgr.clickLikeQuestion(Long.parseLong(key), userSK, "question", getUser());
		return ret;
	}

	@POST
	@Path("convertToAns")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArticleDataSet convertToAns(ArticleData p) {
		this.getPath();
		ArticleDataSet res = new ArticleDataSet();
		res = QnAMgr.saveConvertAns(p, getUser());
		return res;
	}

	// Comment Save For Mobile
	/*@POST
	@Path("saveAnswer")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArticleDataSet saveAnswer(ArticleData p) {
		this.getPath();
		ArticleDataSet res = new ArticleDataSet();
		ArticleDataSet ret = new ArticleDataSet();
		int result;
		res = QnAMgr.saveComment(p, getUser());
		if (res.isState()) {
			if (p.getN3() == (-1)) {
				ret = QnAMgr.readForCommentNotiAdmin(p, getUser());
			} else {
				ret = QnAMgr.readForCommentNoti(p, getUser());
			}
			for (int i = 0; i < ret.getData().length; i++) {
				p.setT3(ret.getData()[i].getT3());
				if (ServerUtil.isZawgyiEncoded(p.getT1())) {
					p.setT1(FontChangeUtil.zg2uni(p.getT1()));
				}
				if (ServerUtil.isZawgyiEncoded(p.getT2())) {
					p.setT2(FontChangeUtil.zg2uni(p.getT2()));
				}
				if (p.getT2().length() > 21) {
					p.setT2(p.getT2().substring(0, 20));
				} else {
					p.setSyskey(ret.getData()[i].getSyskey());
				}
				p.setSyskey(ret.getData()[i].getSyskey());
				result = goNotificationLinkForSaveComment(p);
			}

		}
		return res;
	}*/

	/*@POST
	@Path("saveAns")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArticleDataSet saveAns(ArticleData p) {
		this.getPath();
		ArticleDataSet res = new ArticleDataSet();
		ArticleDataSet ret = new ArticleDataSet();
		int result;
		res = QnAMgr.saveAnswer(p, getUser());
		if (res.isState()) {
			ret = QnAMgr.readForCommentNoti(p, getUser());
			for (int i = 0; i < ret.getData().length; i++) {
				p.setT3(ret.getData()[i].getT3());
				if (ServerUtil.isZawgyiEncoded(p.getT1())) {
					p.setT1(FontChangeUtil.zg2uni(p.getT1()));
				}
				if (ServerUtil.isZawgyiEncoded(p.getT2())) {
					p.setT2(FontChangeUtil.zg2uni(p.getT2()));
				}
				if (p.getT2().length() > 21) {
					p.setT2(p.getT2().substring(0, 20));
				} else {
					p.setSyskey(ret.getData()[i].getSyskey());
				}
				p.setSyskey(ret.getData()[i].getSyskey());
				result = goNotificationLinkForSaveComment(p);

			}
		}
		return res;
	}*/

	/*public int goNotificationLinkForSaveComment(ArticleData p) {
		this.getPath();
		String FCM_URL = "https://fcm.googleapis.com/fcm/send";
		// String FCM_SERVER_API_KEY =
		// "AIzaSyCyHIF2pJE_JwwyQmcI2m0thLw_sxwkng8";
		String FCM_SERVER_API_KEY = "AIzaSyDcaERuLE19XrNuZXZL8P4bOSq18KnYoZg";
		String deviceRegistrationId = p.getT3();
		int responseCode = -1;
		String responseBody = null;
		byte[] postData;
		try {
			if (p.getT3().equalsIgnoreCase("quesiton")) {
				postData = getPostData(deviceRegistrationId, p.getT2(), p);
			} else {
				postData = getPostDataComment(deviceRegistrationId, p.getT2(), p);
			}
			URL url = new URL(FCM_URL);
			HttpsURLConnection httpURLConnection = (HttpsURLConnection) url.openConnection();
			// set timeputs to 10 seconds
			httpURLConnection.setConnectTimeout(60000);
			httpURLConnection.setReadTimeout(20000);
			httpURLConnection.setDoInput(true);
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setUseCaches(false);
			httpURLConnection.setRequestMethod("POST");
			httpURLConnection.setRequestProperty("Content-Type", "application/json");
			httpURLConnection.setRequestProperty("Content-Length", Integer.toString(postData.length));
			httpURLConnection.setRequestProperty("Authorization", "key=" + FCM_SERVER_API_KEY);
			OutputStream out = httpURLConnection.getOutputStream();
			out.write(postData);
			out.close();
			responseCode = httpURLConnection.getResponseCode();
			// success
			if (responseCode == HttpStatus.SC_OK) {
				responseBody = convertStreamToString(httpURLConnection.getInputStream());
				System.out.println("FCM message sent : " + responseBody);
			}
			// failure
			else {
				responseBody = convertStreamToString(httpURLConnection.getErrorStream());
				System.out.println(
						"Sending FCM request failed for regId: " + deviceRegistrationId + " response: " + responseBody);
			}
		} catch (IOException ioe) {
			System.out.println("IO Exception in sending FCM request. regId: " + deviceRegistrationId);
			ioe.printStackTrace();
		} catch (Exception e) {
			System.out.println("Unknown exception in sending FCM request. regId: " + deviceRegistrationId);
			e.printStackTrace();
		}
		return responseCode;

	}*/

	public byte[] getPostData(String registrationId, String t2, ArticleData p) throws JSONException {
		HashMap<String, String> dataMap = new HashMap<>();
		HashMap<String, String> objectMap = new HashMap<>();
		JSONObject payloadObject = new JSONObject();
		dataMap.put("title", "The Farmer");
		dataMap.put("body", p.getT2());
		dataMap.put("key", Long.toString(p.getSyskey()));
		dataMap.put("content", "Question & Answer");
		dataMap.put("notitype", "question");
		dataMap.put("click_action", ".activity.DrawerActivity");
		JSONObject data = new JSONObject(dataMap);
		JSONObject objdata = new JSONObject(objectMap);
		payloadObject.put("notification", data);
		payloadObject.put("to", registrationId);
		// payloadObject.put("to", "/topics/allDevices");//for all
		payloadObject.put("delay_while_idle", false);
		payloadObject.put("priority", "high");
		payloadObject.put("content_available", true);
		payloadObject.put("collapse_key", "Updates Available");
		return payloadObject.toString().getBytes(Charset.forName("UTF-8"));
	}

	@GET
	@Path("searchLikeCount")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleDataSet searchLikeCount() {
		this.getPath();
		String id = request.getParameter("key");
		ArticleDataSet res = new ArticleDataSet();
		res = QnAMgr.searchLikeCount(id, getUser());
		return res;
	}

	@GET
	@Path("getAnswers")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleDataSet getAnswers() {
		this.getPath();
		String id = request.getParameter("id");
		ArticleDataSet res = new ArticleDataSet();
		res = QnAMgr.getComments(id, getUser());
		return res;
	}

	// delete for comment
	@POST
	@Path("deleteAnswer")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArticleDataSet deleteAnswer(ArticleData p) {
		this.getPath();
		ArticleDataSet res = new ArticleDataSet();
		res = QnAMgr.deleteComment(p, getUser());
		return res;
	}

	public byte[] getPostDataComment(String registrationId, String t2, ArticleData p) throws JSONException {
		HashMap<String, String> dataMap = new HashMap<>();
		HashMap<String, String> objectMap = new HashMap<>();
		JSONObject payloadObject = new JSONObject();
		dataMap.put("title", "The Farmer");
		dataMap.put("body", p.getT2());
		dataMap.put("key", Long.toString(p.getN1()));
		dataMap.put("content", "Question & Answer");
		dataMap.put("notitype", "question");
		dataMap.put("click_action", ".activity.DrawerActivity");
		JSONObject data = new JSONObject(dataMap);
		JSONObject objdata = new JSONObject(objectMap);
		payloadObject.put("notification", data);
		payloadObject.put("to", registrationId);
		// payloadObject.put("to", "/topics/allDevices");//for all
		payloadObject.put("delay_while_idle", false);
		payloadObject.put("priority", "high");
		payloadObject.put("content_available", true);
		payloadObject.put("collapse_key", "Updates Available");
		return payloadObject.toString().getBytes(Charset.forName("UTF-8"));
	}

	public static String convertStreamToString(InputStream inStream) throws Exception {
		InputStreamReader inputStream = new InputStreamReader(inStream);
		BufferedReader bReader = new BufferedReader(inputStream);
		StringBuilder sb = new StringBuilder();
		String line = null;
		while ((line = bReader.readLine()) != null) {
			sb.append(line);
		}
		return sb.toString();
	}

	@GET
	@Path("viewByID")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleDataSet viewByID() {
		this.getPath();
		String id = request.getParameter("id");
		ArticleDataSet res = new ArticleDataSet();
		res = QnAMgr.viewQuestion(id, getUser());
		return res;
	}

	@GET
	@Path("searchLike")
	@Produces(MediaType.APPLICATION_JSON)
	public ArticleDataSet searchLike() {
		this.getPath();
		ArticleDataSet ret;
		long key = Long.parseLong(request.getParameter("key"));
		ret = QnAMgr.searchLike(key, getUser());
		return ret;
	}
	
	/* deleteContent */
	@POST
	@Path("deleteContent")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Result deleteContent(ContentReq req) {
		this.getPath();
		Result res = new Result();
		res = QnAMgr.deleteQuestion(Long.parseLong(req.getSyskey()), req.getType(), getUser());
		return res;
	}

}
