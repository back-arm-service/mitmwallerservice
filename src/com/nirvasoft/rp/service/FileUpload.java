package com.nirvasoft.rp.service;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

import com.nirvasoft.cms.framework.ResultMobile;
import com.nirvasoft.cms.framework.ServerSession;
import com.nirvasoft.rp.dao.DAOManager;

@Path("/file")
public class FileUpload {
	@Context
	HttpServletRequest request;

	@javax.ws.rs.core.Context
	ServletContext context;

	public void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
	}

	@POST
	@Path("/uploadImage")
	@Consumes(MediaType.MULTIPART_FORM_DATA + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON)
	public ResultMobile uploadProfile() {
		getPath();
		ResultMobile res = new ResultMobile();
		String inputFileName = "";
		String filePath = "upload";
		String outputFileName = "";
		String userid = "";
		String mobile = "";
		String username = "";
		String time = "";
		System.out.println("image Upload in service: ");
		try {
			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);
			@SuppressWarnings("unchecked")
			List<FileItem> items = upload.parseRequest(request);
			int i = 0;
			for (FileItem item : items) {
				if (item.isFormField()) {
					if (i == 0) {
						inputFileName = item.getString();
					} else if (i == 1) {
						userid = item.getString();
					} else if (i == 2) {
						mobile = item.getString();
					} else if (i == 3) {
						username = item.getString();
					} else if (i == 4) {
						time = item.getString();
					}
					i++;
				} else {
					if (inputFileName.contains("/")) {
						inputFileName = inputFileName.substring(inputFileName.lastIndexOf("/") + 1);
					}
					filePath = request.getServletContext().getRealPath("/") + "/" + filePath;
					System.out.println(filePath);
					String extension = FilenameUtils.getExtension(inputFileName);
					if (extension.equalsIgnoreCase("JPEG") || extension.equalsIgnoreCase("JPG")
							|| extension.equalsIgnoreCase("PNG")) {
						filePath += "/smallImage";
						System.out.println("filePath: " + filePath);
						outputFileName = "IMG";
						File dir = new File(filePath);
						if (!dir.exists())
							dir.mkdirs();
						String dtFormat = new SimpleDateFormat("yyyyMMdd").format(new Date());
						outputFileName += userid + dtFormat + inputFileName;
						filePath += "/" + outputFileName;
						File l_file = new File(filePath);
						if (l_file.exists()) {
							try {
								l_file.delete();
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						if (l_file.createNewFile()) {
							item.write(l_file);
							res.setMsgCode(outputFileName);
						}
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			res.setState(false);
		} catch (Exception e) {
			e.printStackTrace();
			res.setState(false);
		}
		return res;
	}

}
