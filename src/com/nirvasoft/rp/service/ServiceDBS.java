package com.nirvasoft.rp.service;

import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;


import com.nirvasoft.rp.mgr.dfs.DBApplicationDataMgr;
import com.nirvasoft.rp.dao.dfs.DAOManager;
import com.nirvasoft.rp.framework.ServerSession;
import com.nirvasoft.rp.shared.dfs.FilterDataset;
import com.nirvasoft.rp.shared.dfs.TransactionListingDataset;

@Path("/serviceDBS")
public class ServiceDBS {
	@Context
	HttpServletRequest request;
	String UPLOAD_DIRECTORY = "";
	String FONT_DIRECTORY = "";
	String IMAGE_DIRECTORY = "";

	@Context
	HttpServletResponse response;

	@javax.ws.rs.core.Context
	static ServletContext context;

	public void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
	}

	@POST
	@Path("getTransactionListingDataset")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TransactionListingDataset getTransactionListingDataset(FilterDataset aFilterDataset)
			throws IllegalArgumentException, SQLException {
		getPath();
		DBApplicationDataMgr l_DataManager = new DBApplicationDataMgr();
		TransactionListingDataset l_ListingDataset = new TransactionListingDataset();

		l_ListingDataset = l_DataManager.getTransactionListingData(aFilterDataset);
		return l_ListingDataset;
	}
	
	
}
