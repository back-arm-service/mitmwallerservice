package com.nirvasoft.rp.service;

import java.security.SecureRandom;
import java.util.Base64;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.nirvasoft.rp.data.TokenResponse;
import com.nirvasoft.rp.mgr.TokenMgr;
import com.nirvasoft.rp.shared.tokenData;
@Path("/serviceToken")
public class ServiceToken {
	@Context
	HttpServletRequest req;
	@Context
	private HttpServletResponse response;
	@javax.ws.rs.core.Context
	ServletContext context;
	private static final SecureRandom secureRandom = new SecureRandom(); //threadsafe
	private static final Base64.Encoder base64Encoder = Base64.getUrlEncoder(); //threadsafe
	
	@POST
	@Path("getToken")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TokenResponse GetToken(tokenData treq){ 
		TokenResponse tres = new TokenResponse();
		//Generate token 
		byte[] randomBytes = new byte[24];
	    secureRandom.nextBytes(randomBytes);
	    String token =base64Encoder.encodeToString(randomBytes);
	    treq.setToken(token);
		boolean status =new TokenMgr().validToken(treq);
		if(status == true){
		    tres.setToken(token);
		    tres.setMsgCode("0000");
		    tres.setMsgDesc("OK");
		}else{
			tres.setMsgCode("0014");
		    tres.setMsgDesc("Invalid Login. Authentication failed.");
		}
		return tres;
	}
}
