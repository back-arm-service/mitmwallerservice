package com.nirvasoft.rp.service;

import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.data.ChannelKeyRequest;
import com.nirvasoft.rp.data.ContactListReq;
import com.nirvasoft.rp.framework.ServerSession;
import com.nirvasoft.rp.mgr.ChatMgr;
import com.nirvasoft.rp.mgr.SessionMgr;
import com.nirvasoft.rp.shared.Contact;
import com.nirvasoft.rp.shared.ContactArr;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.shared.SessionData;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ServerGlobal;

@Path("/chatservice")
public class ChatService {
	@Context
	HttpServletRequest request;

	@Context
	private HttpServletResponse response;

	@javax.ws.rs.core.Context
	ServletContext context;

	@POST
	@Path("addContact")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ResponseData addContact(Contact req) {
		getPath();
		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Add Contact: ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}
		ResponseData res = new ResponseData();
		res = new SessionMgr().updateActivityTime_Old(req.getSessionID(),
				req.getUserID()); // update
		if (!res.getCode().equalsIgnoreCase("0000")) {
			return res;
		}
		if (!req.getPhone().equals(req.getUserID())) {
			res = new ChatMgr().addContact(req);
		} else {
			res.setCode("0014");
			res.setDesc("Cannot add your own contact");
			return res;
		}

		// Response log
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Add Contact: " + response.toString());
			l_err.add("=============================================================================");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}
		return res;
	}

	@POST
	@Path("getContact")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ContactArr getContact(SessionData req) {
		getPath();
		ContactArr res = new ContactArr();
		ResponseData resdata = new ResponseData();
		resdata = new SessionMgr().updateActivityTime_Old(req.getSessionID(),
				req.getUserID()); // update
		if (!resdata.getCode().equalsIgnoreCase("0000")) {
			res.setCode(resdata.getCode());
			res.setDesc(resdata.getDesc());
			return res;
		}
		return new ChatMgr().getContact(req.getUserID());
	}
	
	@POST
	@Path("checkPhoneNo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Contact checkPhoneNo(SessionData req) {
		getPath();
		Contact res = new Contact();
		ResponseData resdata = new ResponseData();
		resdata = new SessionMgr().updateActivityTime_Old(req.getSessionID(),
				req.getUserID()); // update
		if (!resdata.getCode().equalsIgnoreCase("0000")) {
			res.setCode(resdata.getCode());
			res.setDesc(resdata.getDesc());
			return res;
		}
		return new ChatMgr().checkPhoneNo(req.getLoginID());
	}

	public void getPath() {
		ServerSession.serverPath = request.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = request.getServletContext().getRealPath("/") + "/";
	}

	@POST
	@Path("updateChannelKey")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ResponseData updateChannelKey(ChannelKeyRequest req) {
		getPath();
		return new ChatMgr().updateChannelKey(req);
	}
	
	@POST
	@Path("getContactListByPhoneNo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ContactArr getContactListByUser(ContactListReq req) {
		getPath();
		GeneralUtil.readDebugLogStatus();
		ContactArr contactArr = new ContactArr();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Add Contact List: ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}
		ResponseData resdata = new ResponseData();
		resdata = new SessionMgr().updateActivityTime_Old(req.getSessionID(),
				req.getUserID()); // update
		if (!resdata.getCode().equalsIgnoreCase("0000")) {
			contactArr.setCode(resdata.getCode());
			contactArr.setDesc(resdata.getDesc());
			return contactArr;
		}
		contactArr=new ChatMgr().getContactListByPhoneNo(req);//new ChatMgr().getContact(req.getUserID());
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Add Contact List: " + response.toString());
			l_err.add("=============================================================================");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}
		return contactArr;
	}
	
	@POST
	@Path("addContactList")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ContactArr addContactList(ContactListReq req) {
		getPath();
		GeneralUtil.readDebugLogStatus();
		ContactArr contactArr = new ContactArr();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Add Contact List: ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}
		ResponseData resdata = new ResponseData();
		resdata = new SessionMgr().updateActivityTime_Old(req.getSessionID(),
				req.getUserID()); // update
		if (!resdata.getCode().equalsIgnoreCase("0000")) {
			contactArr.setCode(resdata.getCode());
			contactArr.setDesc(resdata.getDesc());
			return contactArr;
		}
		contactArr=new ChatMgr().addContactList(req);//new ChatMgr().getContact(req.getUserID());
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Add Contact List: " + response.toString());
			l_err.add("=============================================================================");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}
		return contactArr;
	}
}
