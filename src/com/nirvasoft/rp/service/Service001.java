
package com.nirvasoft.rp.service;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.FilenameUtils;

import com.nirvasoft.cms.util.CommonMgr;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.dao.WJunctionDao;
import com.nirvasoft.rp.data.MessageResponse;
import com.nirvasoft.rp.data.PswPolicyData;
import com.nirvasoft.rp.data.RegisterRequestData;
import com.nirvasoft.rp.data.RegisterResponseData;
import com.nirvasoft.rp.data.UserInfoResponse;
import com.nirvasoft.rp.data.VersionHistoryData;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.framework.ResultTwo;
import com.nirvasoft.rp.framework.ServerSession;
import com.nirvasoft.rp.mgr.ATMLocationMgr;
import com.nirvasoft.rp.mgr.AdvertiseMgr;
import com.nirvasoft.rp.mgr.ChatAdmMgr;
import com.nirvasoft.rp.mgr.DocumentMgr;
import com.nirvasoft.rp.mgr.ImageMgr;
import com.nirvasoft.rp.mgr.LOVDetailsMgr;
import com.nirvasoft.rp.mgr.MerchantMgr;
import com.nirvasoft.rp.mgr.NotiSessionMgr;
import com.nirvasoft.rp.mgr.PwdMgr;
import com.nirvasoft.rp.mgr.Service001Mgr;
import com.nirvasoft.rp.mgr.SessionMgr;
import com.nirvasoft.rp.mgr.TransferMgr;
import com.nirvasoft.rp.mgr.UserAdmMgr;
import com.nirvasoft.rp.mgr.UserDataMgr;
import com.nirvasoft.rp.mgr.UserRegistrationMgr;
import com.nirvasoft.rp.mgr.VersionHistoryMgr;
import com.nirvasoft.rp.mgr.VersionMgr;
import com.nirvasoft.rp.mgr.WJunctionMgr;
import com.nirvasoft.rp.mgr.WalletMgr;
import com.nirvasoft.rp.mgr.icbs.AccountMgr;
import com.nirvasoft.rp.mgr.icbs.DBAccountMgr;
import com.nirvasoft.rp.mgr.icbs.ICBSMgr;
import com.nirvasoft.rp.shared.ATMLocation;
import com.nirvasoft.rp.shared.ATMLocationList;
import com.nirvasoft.rp.shared.AccountActivityRequest;
import com.nirvasoft.rp.shared.AccountActivityResponse;
import com.nirvasoft.rp.shared.AccountListRequestData;
import com.nirvasoft.rp.shared.AccountListResponseData;
import com.nirvasoft.rp.shared.AccountTypeListRequestData;
import com.nirvasoft.rp.shared.AccountTypeListResponseData;
import com.nirvasoft.rp.shared.AdvertiseRes;
import com.nirvasoft.rp.shared.CheckOTPRequestData;
import com.nirvasoft.rp.shared.CustomerData;
import com.nirvasoft.rp.shared.DepositAccData;
import com.nirvasoft.rp.shared.DepositAccDataResult;
import com.nirvasoft.rp.shared.DocumentRequest;
import com.nirvasoft.rp.shared.DocumentResponse;
import com.nirvasoft.rp.shared.ImageData;
import com.nirvasoft.rp.shared.ImageRequest;
import com.nirvasoft.rp.shared.ImageResponse;
import com.nirvasoft.rp.shared.MerchantReq;
import com.nirvasoft.rp.shared.MerchantResData;
import com.nirvasoft.rp.shared.NewAccountRequestData;
import com.nirvasoft.rp.shared.NewAccountResponseData;
import com.nirvasoft.rp.shared.NotiReq;
import com.nirvasoft.rp.shared.NotiRes;
import com.nirvasoft.rp.shared.OTPReq;
import com.nirvasoft.rp.shared.OTPRes;
import com.nirvasoft.rp.shared.PwdData;
import com.nirvasoft.rp.shared.RegisterListResponseData;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.shared.SessionData;
import com.nirvasoft.rp.shared.StateListResponseData;
import com.nirvasoft.rp.shared.SyskeyRequestData;
import com.nirvasoft.rp.shared.SyskeyResponseData;
import com.nirvasoft.rp.shared.TopupReq;
import com.nirvasoft.rp.shared.TopupRes;
import com.nirvasoft.rp.shared.TransferOutReq;
import com.nirvasoft.rp.shared.TransferOutRes;
import com.nirvasoft.rp.shared.TransferReqV2Data;
import com.nirvasoft.rp.shared.TransferResData;
import com.nirvasoft.rp.shared.VersionHistoryDataSet;
import com.nirvasoft.rp.shared.VersionReq;
import com.nirvasoft.rp.shared.VersionRes;
import com.nirvasoft.rp.shared.WalletRequestData;
import com.nirvasoft.rp.shared.WalletResponseData;
import com.nirvasoft.rp.users.data.UserData;
import com.nirvasoft.rp.users.data.UserViewDataArr;
import com.nirvasoft.rp.users.data.WalletTransferData;
import com.nirvasoft.rp.util.AesUtil;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ReadFile;
import com.nirvasoft.rp.util.ServerGlobal;
import com.nirvasoft.rp.util.ServerUtil;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

@Path("/service001")
public class Service001 {

	public static String userid = "";
	public static String username = "";

	public static String userpsw = "";
	public static String regsyskey = "";
	@Context
	HttpServletRequest req;
	@Context
	private HttpServletResponse response;
	@javax.ws.rs.core.Context
	ServletContext context;

	@POST
	@Path("activatedeactivateUser")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Result activateUserData(String skey, @QueryParam("k") String status, @QueryParam("id") String userId,
			@QueryParam("sessionID") String sessionId, @QueryParam("operation") String operation) {

		getPath();
		Result res = new Result();
		UserAdmMgr u_mgr = new UserAdmMgr();
		SessionMgr session_mgr = new SessionMgr();
		ResponseData sessionRes = new ResponseData();
		String formID ="";
		if(operation.equalsIgnoreCase("activate")){
			 formID ="Activate User";
		}else if(operation.equalsIgnoreCase("deactivate")){
			 formID ="Deactivate User";
		}
		sessionRes = session_mgr.updateActivityTime(sessionId, userId, formID);
		if (sessionRes.getCode().equalsIgnoreCase("0000")) {
			skey = skey.substring(1, skey.length() - 1);
			long syskey = 0;
			syskey = Long.parseLong(skey);
			res = u_mgr.activatedeactivateUserData(syskey, status, userId);
		} else {
			res.setMsgCode(sessionRes.getCode());
			res.setMsgDesc(sessionRes.getDesc());
		}
		return res;
	}

	@POST
	@Path("addNewAccount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public NewAccountResponseData addNewAccount(NewAccountRequestData requestData) {
		NewAccountResponseData responseData = null;
		getPath();
		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Add New Account :" + requestData.getUserID());
			l_err.add("Request Message : " + requestData.getUserID() + ":" + requestData.getSessionID());
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
		}
		ResponseData validateUserID = new ResponseData();
		validateUserID = validateID(requestData.getSessionID(), requestData.getUserID());
		if (validateUserID.getCode().equals("0000")) {
			responseData = new WJunctionMgr().addNewAccount(requestData);
			if (!responseData.getCode().equals("0000")) {
				// Response log
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("Time :" + GeneralUtil.getTime());
					l_err.add("Add New Account :" + requestData.getUserID());
					l_err.add("Response Message : " + responseData.toString());
					l_err.add("Internal Message : " + responseData.getDesc());
					l_err.add("=============================================================================");
					GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
				}
			}
		} else {
			responseData = new NewAccountResponseData();
			responseData.setCode("0014");
			responseData.setDesc("Invalid UserID!");
			// Response log
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Add New Account :" + requestData.getUserID());
				l_err.add("Response Message : " + responseData.toString());
				l_err.add("Internal Message : " + responseData.getDesc());
				l_err.add("=============================================================================");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}
		// Response log
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Add New Account :" + requestData.getUserID());
			l_err.add("Response Message : " + responseData.toString());
			l_err.add("=============================================================================");
			GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
		}
		return responseData;
	}

	@POST
	@Path("changePassword")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ResultTwo changePassword(PwdData pData) {
		getPath();
		PwdMgr mgr = new PwdMgr();
		ResultTwo ret = new ResultTwo();
		SessionMgr session_mgr = new SessionMgr();
		ResponseData resdata = new ResponseData();
		resdata = session_mgr.updateActivityTime_Old(pData.getSessionid(), pData.getUserid()); // update
		if (resdata.getCode().equalsIgnoreCase("0000")) {
			session_mgr.saveActivity(pData.getSessionid(), pData.getUserid(), "changedpassword", "false", ""); // for
			if (!pData.getIv().equals("")) {
				AesUtil util = new AesUtil(128, 1000);
				String oldpassword = util.decrypt(pData.getSalt(), pData.getIv(), pData.getPassword());
				pData.setPassword(oldpassword);
				String newpassword = util.decrypt(pData.getSalt(), pData.getIv(), pData.getNewpassword());
				pData.setNewpassword(newpassword);
			}
			if (mgr.checkOldPassword(pData)) {
				ret = mgr.checkPasswordPolicyPattern(pData.getNewpassword());
				if (ret.getMsgCode().equalsIgnoreCase("0000")) {
					ret = mgr.changePwd(pData);
				}
			} else {
				ret.setMsgCode("014");
				ret.setMsgDesc("Please check old password");
			}
		} else {
			ret.setMsgCode(resdata.getCode());
			ret.setMsgDesc(resdata.getDesc());
		}
		return ret;
	}

	// Check OTP Code
	@POST
	@Path("checkOTP")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ResponseData checkOTP(CheckOTPRequestData request) {
		getPath();
		SessionMgr session_mgr = new SessionMgr();
		ResponseData response = new ResponseData();
		ResponseData resultdata = new ResponseData();
		resultdata = session_mgr.validatecheckOTPReq(request.getSessionID(), request.getUserID(), request.getrKey(),
				request.getOtpCode());// check request params blank or not
		if (resultdata.getCode().equalsIgnoreCase("0000")) {
			resultdata = new ResponseData();
			resultdata = session_mgr.updateActivityTime_Old(request.getSessionID(), request.getUserID());
			if (resultdata.getCode().equals("0000")) {
				session_mgr.saveActivity(request.getSessionID(), request.getUserID(), "checkOTP", "false", "");// for
																												// loginHistory
				response = session_mgr.checkOtpCode(request.getSessionID(), request.getUserID(), request.getrKey(),
						request.getOtpCode(), request.getsKey());
				if (response.getCode().equalsIgnoreCase("0014")) {
					return response;
				}
			} else {
				response.setCode(resultdata.getCode());
				response.setDesc(resultdata.getDesc());
			}
		} else {
			response.setCode(resultdata.getCode());
			response.setDesc(resultdata.getDesc());
		}
		return response;
	}

	@POST
	@Path("checkUserRegistration")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	private WalletResponseData checkUserRegistration(WalletRequestData req) {
		WalletResponseData response = new WalletResponseData();
		getPath();
		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Check User Registration.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}
		response = new UserRegistrationMgr().checkUserRegistration(req);
		SessionMgr session_mgr = new SessionMgr();
		Result result = new Result();
		String sessionId = "";
		if (!response.getMessageCode().equalsIgnoreCase("0014")) {// If
			// code"0000",user
			// already
			// register
			// and can
			// signin,If
			// not,user
			// can't
			// Service001.userid = data.getUserID();
			// Service001.username = response.getName();
			// Service001.regsyskey = response.getsKey();
			result = session_mgr.insertSession(req.getUserID());
			if (!result.isState()) {
				response.setMessageCode("0014");
				response.setMessageDesc("User already logged in");
				// Response log
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("Time :" + GeneralUtil.getTime());
					l_err.add("Login User ID :" + req.getUserID());
					l_err.add("Wallet Signin Response Message : " + response.toString());
					l_err.add("Internal Message : " + "User already logged in the same time");
					l_err.add("=============================================================================");
					GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
				}
				return response;
			}
			sessionId = result.getSessionID();
			response.setSessionID(sessionId);
			session_mgr.saveActivity(sessionId, req.getUserID(), "walletSignin", "false", "");// for loginHistory
		}
		// Response log
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Check User Registration : " + response.toString());
			l_err.add("=============================================================================");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}
		return response;
	}

	
	@POST
	@Path("deleteATMLocator")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON)
	public Result deleteATMLocator(ATMLocation data) {
		Result res = new Result();
		ATMLocationMgr mgr = new ATMLocationMgr();
		getPath();
		SessionMgr s_mgr = new SessionMgr();
		ResponseData response = new ResponseData();
		response = s_mgr.updateActivityTime(data.getSessionID(), data.getUserid());
		if (response.getCode().equals("0000")) {
			res = mgr.deleteATMLocator(data);
		} else {
			res.setMsgCode(response.getCode());
			res.setMsgDesc(response.getDesc());
			res.setState(false);
		}

		return res;

	}

	@POST
	@Path("deleteVersionHistory")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public VersionHistoryData deleteVersionHistory(VersionHistoryData request) {
		VersionHistoryData response = null;

		getPath();
		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime(request.getSessionID(), request.getUserID(),"version-history");

		if (sessionState.getCode().equals("0000")) {
			if (request.getAutokey() != null && request.getAutokey().trim().length() != 0) {
				breakForParseException: {
					try {
						Long.parseLong(request.getAutokey().trim());
					} catch (Exception e) {
						response = new VersionHistoryData();
						response.setMsgDesc("Invalid Autokey.");
						response.setState(false);

						break breakForParseException;
					}

					response = new VersionHistoryMgr().delete(Long.parseLong(request.getAutokey().trim()));
				}
			} else {
				response = new VersionHistoryData();
				response.setMsgDesc("Blank Autokey.");
				response.setState(false);
			}
		} else {
			response = new VersionHistoryData();
			response.setMsgCode(sessionState.getCode());
			response.setMsgDesc(sessionState.getDesc());
			response.setState(false);
		}

		return response;
	}

	@GET
	@Path("downloadUserReport")
	@Consumes(MediaType.APPLICATION_JSON)
	public void downloadReport(@QueryParam("sessionID") String sessionId, @QueryParam("userID") String userID) {
		String fileType = req.getParameter("fileType");
		String fileName = req.getParameter("fileName");
		String folder = "";
		if (fileType.equals("pdf")) {
			folder = "/download/pdf";
		} else
			folder = "/download/excel";
		SessionMgr seiMgr = new SessionMgr();
		ResponseData sessionRes = seiMgr.updateActivityTime_Old(sessionId, userID);
		if (sessionRes.getCode().equalsIgnoreCase("0000"))
			CommonMgr.downloadPDFFile(fileType, folder, fileName, response);
	}

	@POST
	@Path("getAccountActivity")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public AccountActivityResponse getAccountActivity(AccountActivityRequest accountPara) {
		getPath();

		String accNumber = accountPara.getAcctNo();
		String customerNo = accountPara.getCustomerNo();
		int currentPage = accountPara.getCurrentPage();
		int pageSize = accountPara.getPageSize();
		String durationType = accountPara.getDurationType();
		String fromDate = accountPara.getFromDate();
		String toDate = accountPara.getToDate();
		// Request Log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Key :" + accountPara.getAcctNo());
			l_err.add("AccountActivity Request Message : " + accountPara.toString());
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
		}
		AccountActivityResponse res = new AccountActivityResponse();
		boolean dateisnull = false;
		ICBSMgr imgr = new ICBSMgr();
		ResponseData response = new ResponseData();
		// response = mgr.validateactivity(accountPara.getAcctNo(),
		// accountPara.getSessionID(), accountPara.getUserID());
		response.setCode("0000");
		if (response.getCode().equalsIgnoreCase("0000")) {
			response = new ResponseData();
			// response =
			// mgr.updateActivityTime(accountPara.getSessionID(),accountPara.getUserID());
			response.setCode("0000");
			if (response.getCode().equals("0000")) {
				// mgr.saveActivity(accountPara.getSessionID(),
				// accountPara.getUserID(), "getAccountActivity", "false", "");
				if (durationType.equals("3")) {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
					if (fromDate == null || fromDate == "" || fromDate.equals("")
							|| fromDate.equalsIgnoreCase("null")) {
						res.setDesc("Start date is mandatory");
						res.setCode("0014");
						dateisnull = true;
					} else if (toDate.equalsIgnoreCase(null) || toDate.equals("")) {
						res.setDesc("End date is mandatory");
						res.setCode("0014");
						dateisnull = true;
					} else
						try {
							if (!sdf.parse(toDate).equals(sdf.parse(fromDate))) {
								if (!sdf.parse(toDate).after(sdf.parse(fromDate))) {
									res.setDesc("Start date must not exceed end date!");
									res.setCode("0014");
									dateisnull = true;
								}
							}
						} catch (ParseException e) {
							res.setCode("0014");
							res.setDesc(e.getMessage());
							e.printStackTrace();
						}
				}
				if (!dateisnull) {
					try {
						// res = epixService.getAccountActivity(accNumber,
						// customerNo, durationType, fromDate, toDate,
						// currentPage, pageSize);
						res = imgr.getAccountActivity(accNumber, customerNo, durationType, fromDate, toDate,
								String.valueOf(currentPage), String.valueOf(pageSize));
					} catch (Exception e) {
						e.printStackTrace();
						res.setCode("0014");
						res.setDesc(e.getMessage());
					}
				}
				res.setCurrentPage(accountPara.getCurrentPage());
				res.setPageSize(accountPara.getPageSize());
				res.setDurationType(accountPara.getDurationType());
				res.setFromDate(accountPara.getFromDate());
				res.setToDate(accountPara.getToDate());
			} else {
				res.setCode(response.getCode());
				res.setDesc(response.getDesc());
			}
		} else {
			res.setCode(response.getCode());
			res.setDesc(response.getDesc());
		}

		res.setAcctNo(accountPara.getAcctNo());
		res.setCustomerNo(accountPara.getCustomerNo());

		// Response Log
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Key :" + accountPara.getAcctNo());
			l_err.add("AccountActivity Response Message : ");
			l_err.add(res.toString());
			l_err.add("=============================================================================");
			GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
		}
		return res;
	}

	@POST
	@Path("getAccountActivityList")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public AccountActivityResponse getAccountActivityList(AccountActivityRequest accountPara) {
		getPath();
		String accNumber = accountPara.getAcctNo();
		String customerNo = accountPara.getCustomerNo();
		int currentPage = accountPara.getCurrentPage();
		int pageSize = accountPara.getPageSize();
		String durationType = accountPara.getDurationType();
		String fromDate = accountPara.getFromDate();
		String toDate = accountPara.getToDate();
		// Request Log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Key :" + accountPara.getAcctNo());
			l_err.add("AccountActivity Request Message : " + accountPara.toString());
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
		}
		AccountActivityResponse res = new AccountActivityResponse();
		boolean dateisnull = false;
		ICBSMgr imgr = new ICBSMgr();
		SessionMgr session_mgr = new SessionMgr();
		ResponseData resdata = new ResponseData();
		resdata = session_mgr.updateActivityTime_Old(accountPara.getSessionID(),
				accountPara.getUserID()); // update
//		if (resdata.getCode().equalsIgnoreCase("0000")) {
			resdata = new WJunctionMgr().checkAccNoByUserID(accountPara.getUserID(), accountPara.getAcctNo());
			if (resdata.getCode().equalsIgnoreCase("0000")) {
				if (durationType.equals("3")) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				if (fromDate == null || fromDate == "" || fromDate.equals("")
						|| fromDate.equalsIgnoreCase("null")) {
					res.setDesc("Start date is mandatory");
					res.setCode("0014");
					dateisnull = true;
				} else if (toDate == null || toDate == "" || toDate.equals("")) {
					res.setDesc("End date is mandatory");
					res.setCode("0014");
					dateisnull = true;
				} else
					try {
						if (!sdf.parse(toDate).equals(sdf.parse(fromDate))) {
							if (!sdf.parse(toDate).after(sdf.parse(fromDate))) {
								res.setDesc("Start date must not exceed end date!");
								res.setCode("0014");
								dateisnull = true;
							}
						}
					} catch (ParseException e) {
						res.setCode("0014");
						res.setDesc(e.getMessage());
						e.printStackTrace();
					}
			}
			if (!dateisnull) {
				try {
					// res = epixService.getAccountActivity(accNumber,
					// customerNo, durationType, fromDate, toDate,
					// currentPage, pageSize);
					res = imgr.getAccountActivityList(accNumber, customerNo, durationType, fromDate, toDate,
							String.valueOf(currentPage), String.valueOf(pageSize));
				} catch (Exception e) {
					e.printStackTrace();
					res.setCode("0014");
					res.setDesc(e.getMessage());
				}
			}
			res.setCurrentPage(accountPara.getCurrentPage());
			res.setPageSize(accountPara.getPageSize());
			res.setDurationType(accountPara.getDurationType());
			res.setFromDate(accountPara.getFromDate());
			res.setToDate(accountPara.getToDate());
			}
		/*} else {
			res.setCode(resdata.getCode());
			res.setDesc(resdata.getDesc());
		}*/
		res.setAcctNo(accountPara.getAcctNo());
		res.setCustomerNo(accountPara.getCustomerNo());

		// Response Log
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Key :" + accountPara.getAcctNo());
			l_err.add("AccountActivity Response Message : ");
			l_err.add(res.toString());
			l_err.add("=============================================================================");
			GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
		}
		return res;
	}
	@POST
	@Path("getTransactionActivityList")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public AccountActivityResponse getTransactionActivityList(AccountActivityRequest accountPara) {
		getPath();
		String accNumber = accountPara.getAcctNo();
		String customerNo = accountPara.getCustomerNo();
		int currentPage = accountPara.getCurrentPage();
		int pageSize = accountPara.getPageSize();
		String durationType = accountPara.getDurationType();
		String fromDate = accountPara.getFromDate();
		String toDate = accountPara.getToDate();
		// Request Log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Key :" + accountPara.getAcctNo());
			l_err.add("AccountActivity Request Message : " + accountPara.toString());
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
		}
		AccountActivityResponse res = new AccountActivityResponse();
		boolean dateisnull = false;
		ICBSMgr imgr = new ICBSMgr();
		SessionMgr session_mgr = new SessionMgr();
		ResponseData resdata = new ResponseData();
		resdata = session_mgr.updateActivityTime_Old(accountPara.getSessionID(),
				accountPara.getUserID()); // update
		if (resdata.getCode().equalsIgnoreCase("0000")) {
			resdata = new WJunctionMgr().checkAccNoByUserID(accountPara.getUserID(), accountPara.getAcctNo());
			if (resdata.getCode().equalsIgnoreCase("0000")) {
				if (durationType.equals("3")) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				if (fromDate == null || fromDate == "" || fromDate.equals("")
						|| fromDate.equalsIgnoreCase("null")) {
					res.setDesc("Start date is mandatory");
					res.setCode("0014");
					dateisnull = true;
				} else if (toDate == null || toDate == "" || toDate.equals("")) {
					res.setDesc("End date is mandatory");
					res.setCode("0014");
					dateisnull = true;
				} else
					try {
						if (!sdf.parse(toDate).equals(sdf.parse(fromDate))) {
							if (!sdf.parse(toDate).after(sdf.parse(fromDate))) {
								res.setDesc("Start date must not exceed end date!");
								res.setCode("0014");
								dateisnull = true;
							}
						}
					} catch (ParseException e) {
						res.setCode("0014");
						res.setDesc(e.getMessage());
						e.printStackTrace();
					}
			}
			if (!dateisnull) {
				try {
					// res = epixService.getAccountActivity(accNumber,
					// customerNo, durationType, fromDate, toDate,
					// currentPage, pageSize);
					res = imgr.getAccountActivityList(accNumber, customerNo, durationType, fromDate, toDate,
							String.valueOf(currentPage), String.valueOf(pageSize));
				} catch (Exception e) {
					e.printStackTrace();
					res.setCode("0014");
					res.setDesc(e.getMessage());
				}
			}
			res.setCurrentPage(accountPara.getCurrentPage());
			res.setPageSize(accountPara.getPageSize());
			res.setDurationType(accountPara.getDurationType());
			res.setFromDate(accountPara.getFromDate());
			res.setToDate(accountPara.getToDate());
			}
		} else {
			res.setCode(resdata.getCode());
			res.setDesc(resdata.getDesc());
		}
		res.setAcctNo(accountPara.getAcctNo());
		res.setCustomerNo(accountPara.getCustomerNo());

		// Response Log
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Key :" + accountPara.getAcctNo());
			l_err.add("AccountActivity Response Message : ");
			l_err.add(res.toString());
			l_err.add("=============================================================================");
			GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
		}
		return res;
	}

	@POST
	@Path("getAccountList")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public AccountListResponseData getAccountList(AccountListRequestData requestData) {
		AccountListResponseData responseData = new AccountListResponseData();
		getPath();
		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Account List :" + requestData.getUserID());
			l_err.add("Request Message : " + requestData.getUserID() + ":" + requestData.getSessionID());
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
		}
		ResponseData validateUserID = new ResponseData();
		validateUserID = validateID(requestData.getSessionID(), requestData.getUserID());
		if (validateUserID.getCode().equals("0000")) {
			SessionMgr session_mgr = new SessionMgr();
			ResponseData resdata = new ResponseData();
			resdata = session_mgr.updateActivityTime_Old(requestData.getSessionID(),requestData.getUserID()); // update
			if (resdata.getCode().equalsIgnoreCase("0000")) {
				responseData = new WJunctionMgr().getAccountList(requestData.getUserID());
			} else {
				responseData.setCode(resdata.getCode());
				responseData.setDesc(resdata.getDesc());
				return responseData;
			}
			if (!responseData.getCode().equals("0000")) {
				// Response log
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("Time :" + GeneralUtil.getTime());
					l_err.add("Account List :" + requestData.getUserID());
					l_err.add("Response Message : " + responseData.toString());
					l_err.add("Internal Message : " + responseData.getDesc());
					l_err.add("=============================================================================");
					GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
				}
			}
			responseData = new WJunctionMgr().getAccountList(requestData.getUserID());
		} else {
			responseData = new AccountListResponseData();
			responseData.setCode("0014");
			responseData.setDesc("Invalid UserID!");
			// Response log
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Account List :" + requestData.getUserID());
				l_err.add("Response Message : " + responseData.toString());
				l_err.add("Internal Message : " + responseData.getDesc());
				l_err.add("=============================================================================");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}

		// Response log
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Account List :" + requestData.getUserID());
			l_err.add("Response Message : " + responseData.toString());
			l_err.add("=============================================================================");
			GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
		}

		return responseData;
	}

	@POST
	@Path("getAccountSummary")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DepositAccDataResult getAccountSummary(SessionData req) {
		getPath();
		DepositAccDataResult depresult = new DepositAccDataResult();
		String cusid = "";
		DepositAccData[] depositaccdataarr = null;// Flexcube // Account // List
		Connection conn = null;
		try {
			// Request log
			GeneralUtil.readDebugLogStatus();
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Account Summary :" + req.getUserID());
				l_err.add("Request Message : " + req.getUserID() + ":" + req.getSessionID());
				l_err.add("-----------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");

			}
			conn = new DAOManager().openConnection();
			ResponseData l_res = new ResponseData();
			l_res = validateID(req.getSessionID(), req.getUserID());
			// l_res =
			// mgr.updateActivityTime(res.getSessionID(),res.getUserID());
			if (l_res.getCode().equals("0000")) {
				// mgr.saveActivity(req.getSessionID(), req.getUserID(),
				// "getAccountSummary", "false", "");
				cusid = new WJunctionDao().getCIFByID(req.getUserID(), conn);
				if (!(cusid.equals("") || cusid.equals("null"))) {
					try {
						depresult = new ICBSMgr().getAccountSummary(cusid);
						if (!depresult.getCode().equals("0000")) {
							// Response log
							if (ServerGlobal.isWriteLog()) {
								ArrayList<String> l_err = new ArrayList<String>();
								l_err.add("Time :" + GeneralUtil.getTime());
								l_err.add("Account Summary :" + req.getUserID());
								l_err.add("Response Message : " + depresult.toString());
								l_err.add("Internal Message : " + depresult.getDesc());
								l_err.add(
										"=============================================================================");
								GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
							}
							return depresult;
						} else {
							depositaccdataarr = depresult.getDataList();// null
						}
					} catch (Exception e) {
						e.printStackTrace();
						if (ServerGlobal.isWriteLog()) {
							ArrayList<String> l_err = new ArrayList<String>();
							l_err.add("Time :" + GeneralUtil.getTime());
							l_err.add("Account Summary :" + req.getUserID());
							l_err.add("Response Message : " + depresult.toString());
							l_err.add("Internal Message : " + e.getMessage());
							l_err.add("=============================================================================");
							GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
						}
						return depresult;
					}
					depresult.setDataList(depositaccdataarr);
				} else {
					depresult.setDataList(depositaccdataarr);
					depresult.setCode("0014");
					depresult.setDesc("Invalid User Id!");
				}
			} else {
				depresult.setCode(l_res.getCode());
				depresult.setDesc(l_res.getDesc());
				// Response log
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("Time :" + GeneralUtil.getTime());
					l_err.add("Account Summary :" + req.getUserID());
					l_err.add("Response Message : " + depresult.toString());
					l_err.add("Internal Message : " + depresult.getDesc());
					l_err.add("=============================================================================");
					GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
				}
				return depresult;
			}
			// Response log
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Account Summary :" + req.getUserID());
				l_err.add("Response Message : " + depresult.toString());
				l_err.add("=============================================================================");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");

			}
		} catch (Exception e) {
			depresult.setCode("0014");
			depresult.setDesc("Server Error!");
		} finally {
			try {
				if (conn != null) {
					if (!conn.isClosed())
						conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return depresult;
	}

	@POST
	@Path("getAccountType")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public AccountTypeListResponseData getAccountType(AccountTypeListRequestData requestData) {
		AccountTypeListResponseData responseData = null;

		getPath();

		// Request log
		GeneralUtil.readDebugLogStatus();

		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Account Type :" + requestData.getUserID());
			l_err.add("Request Message : " + requestData.getUserID() + ":" + requestData.getSessionID());
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
		}

		ResponseData validateUserID = new ResponseData();
		validateUserID = validateID(requestData.getSessionID(), requestData.getUserID());

		if (validateUserID.getCode().equals("0000")) {
			responseData = new LOVDetailsMgr().getAccountTypeList();

			if (!responseData.getCode().equals("0000")) {
				// Response log
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("Time :" + GeneralUtil.getTime());
					l_err.add("Account Type :" + requestData.getUserID());
					l_err.add("Response Message : " + responseData.toString());
					l_err.add("Internal Message : " + responseData.getDesc());
					l_err.add("=============================================================================");
					GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
				}
			}
		} else {
			responseData = new AccountTypeListResponseData();
			responseData.setCode("0014");
			responseData.setDesc("Invalid UserID!");

			// Response log
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Account Type :" + requestData.getUserID());
				l_err.add("Response Message : " + responseData.toString());
				l_err.add("Internal Message : " + responseData.getDesc());
				l_err.add("=============================================================================");
				GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
			}
		}

		// Response log
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Account Type :" + requestData.getUserID());
			l_err.add("Response Message : " + responseData.toString());
			l_err.add("=============================================================================");
			GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
		}

		return responseData;
	}

	@POST
	@Path("getAllPdf")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DocumentResponse getAllPdf(DocumentRequest req) {
		getPath();

		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Getting All Document List By Region.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}

		DocumentResponse responseStateList = new DocumentMgr().getAllPdfByRegion(req);

		return responseStateList;
	}

	@POST
	@Path("getAllRegister")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public RegisterListResponseData getAllRegister(RegisterRequestData request) {
		getPath();

		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Getting State List.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}

		RegisterListResponseData res = new UserRegistrationMgr().getAllRegister();

		return res;
	}

	@POST
	@Path("getAllVersionHistory")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public VersionHistoryDataSet getAllVersionHistory(VersionHistoryDataSet request) {
		VersionHistoryDataSet response = null;

		getPath();
		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime(request.getSessionID(), request.getUserID(),"version-history");

		if (sessionState.getCode().equals("0000")) {
			response = new VersionHistoryMgr().getAll(request.getSearchText().trim(), request.getPageSize(),
					request.getCurrentPage());
		} else {
			response = new VersionHistoryDataSet();
			response.setMsgCode(sessionState.getCode());
			response.setMsgDesc(sessionState.getDesc());
		}

		return response;
	}

	@GET
	@Path("getATMLocatorByID")
	@Produces(MediaType.APPLICATION_JSON)
	public ATMLocation getATMLocatorByID() {

		ATMLocation res = new ATMLocation();
		ATMLocationMgr mgr = new ATMLocationMgr();
		String id = req.getParameter("id");
		getPath();
		String sessionId = req.getParameter("sessionID");
		String userID = req.getParameter("userID");
		SessionMgr s_mgr = new SessionMgr();
		ResponseData response = new ResponseData();
		response = s_mgr.updateActivityTime(sessionId, userID);
		if (response.getCode().equals("0000")) {
			res = mgr.getATMLocatorByID(id);
		} else {
			res.setMsgCode(response.getCode());
			res.setMsgDesc(response.getDesc());
		}

		return res;

	}

	@GET
	@Path("getATMLocatorList")
	@Produces(MediaType.APPLICATION_JSON)
	public ATMLocationList getATMLocatorList(@QueryParam("searchVal") String searchText,
			@QueryParam("pagesize") int pageSize, @QueryParam("currentpage") int currentPage,
			@QueryParam("sessionID") String sessionID, @QueryParam("userID") String userID) {

		ATMLocationList res = new ATMLocationList();
		ATMLocationMgr mgr = new ATMLocationMgr();
		getPath();

		SessionMgr s_mgr = new SessionMgr();
		ResponseData response = new ResponseData();
		response = s_mgr.updateActivityTime(sessionID, userID);
		if (response.getCode().equals("0000")) {
			res = mgr.getATMLocatorList(searchText, pageSize, currentPage);
		} else {
			res.setMsgCode(response.getCode());
			res.setMsgDesc(response.getDesc());
		}

		return res;
	}

	@POST
	@Path("getImageLocation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ImageResponse getImageLocation(ImageRequest req) {
		getPath();

		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Getting All Image List.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}

		ImageResponse result = new ImageMgr().getAllImage(req);

		return result;
	}

	@POST
	@Path("getKeyword")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DocumentResponse getKeyword(DocumentRequest req) {
		getPath();

		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Getting All Document List By Region.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}

		DocumentResponse responseStateList = new DocumentMgr().getKeyword(req);

		return responseStateList;
	}

	@POST
	@Path("getName")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public WalletResponseData getName(WalletRequestData data) {
		WalletResponseData response = new WalletResponseData();

		getPath();

		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Get Name Request : " + data.toString());
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}

		response = new UserRegistrationMgr().getName(data);

		// Response log
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Get Name Response : " + response.toString());
			l_err.add("=============================================================================");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}
		return response;
	}

	@POST
	@Path("getNotiCount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public NotiRes getNotiCount(NotiReq req) {
		getPath();
		NotiRes res = new NotiRes();
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Get Noti.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}
		ResponseData resdata = new ResponseData();
		resdata = new SessionMgr().updateActivityTime_Old(req.getSessionID(),
				req.getUserID()); // update
		if (!resdata.getCode().equalsIgnoreCase("0000")) {
			res.setCode(resdata.getCode());
			res.setDesc(resdata.getDesc());
			return res;
		}
		return new NotiSessionMgr().getNotiCount(req);
	}

	@POST
	@Path("getNotiData")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public NotiRes getNotiData(NotiReq req) {
		getPath();
		NotiRes res = new NotiRes();
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Get Noti.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}
		ResponseData resdata = new ResponseData();
		resdata = new SessionMgr().updateActivityTime_Old(req.getSessionID(),
				req.getUserID()); // update
		if (!resdata.getCode().equalsIgnoreCase("0000")) {
			res.setCode(resdata.getCode());
			res.setDesc(resdata.getDesc());
			return res;
		}
		return new NotiSessionMgr().getNotiData(req);
	}

	// reset password end
	// version history start
	@POST
	@Path("getOneVersionHistoryByAutoKey")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public VersionHistoryData getOneVersionHistoryByAutoKey(VersionHistoryData request) {
		VersionHistoryData response = null;

		getPath();
		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime(request.getSessionID(), request.getUserID(),"version-history");

		if (sessionState.getCode().equals("0000")) {
			if (request.getAutokey() != null && request.getAutokey().trim().length() != 0) {
				breakForParseException: {
					try {
						Long.parseLong(request.getAutokey().trim());
					} catch (Exception e) {
						response = new VersionHistoryData();
						response.setMsgCode("0014");
						response.setMsgDesc("Invalid Autokey.");
						response.setState(false);

						break breakForParseException;
					}

					response = new VersionHistoryMgr().getOneByAutoKey(Long.parseLong(request.getAutokey().trim()));
				}
			} else {
				response = new VersionHistoryData();
				response.setMsgCode("0014");
				response.setMsgDesc("Blank Autokey.");
				response.setState(false);
			}
		} else {
			response = new VersionHistoryData();
			response.setMsgCode(sessionState.getCode());
			response.setMsgDesc(sessionState.getDesc());
			response.setState(false);
		}

		return response;
	}

	@POST
	@Path("getOTP")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public OTPRes getOTP(OTPReq request) {
		getPath();
		SessionMgr session_mgr = new SessionMgr();
		ResponseData resdata = new ResponseData();
		OTPRes response = new OTPRes();
		Result res = new Result();
		String flag="0";//ndh
		resdata = validateID(request.getSessionID(), request.getUserID());
		if (resdata.getCode().equalsIgnoreCase("0000")) {
			resdata = new ResponseData();
			resdata = session_mgr.updateActivityTime_Old(request.getSessionID(), request.getUserID()); // update
			if (resdata.getCode().equalsIgnoreCase("0000")) {
				session_mgr.saveActivity(request.getSessionID(), request.getUserID(), "getOTP", "false", ""); // for
				res = session_mgr.checkOTPDuration(request.getSessionID());
				if (res.getMsgCode().equalsIgnoreCase("0000")) {
					request.setFlag("0");//ndh
					response = session_mgr.sendOTPCode(request, 0);
				} else if (res.getMsgCode().equalsIgnoreCase("0001")) {
					response = session_mgr.sendOTPCode(request, 1); // within 5
				} else {
					response.setCode(res.getMsgCode());
					response.setDesc(res.getMsgDesc());
				}
				if (response.getCode().equalsIgnoreCase("0014")) {
					return response;
				}
			} else {
				response.setCode(resdata.getCode());
				response.setDesc(resdata.getDesc());
			}
		} else {
			response.setCode(resdata.getCode());
			response.setDesc(resdata.getDesc());
			response.setrKey("");
		}
		return response;
	}
	
	/*@POST
	@Path("getLoginOTP")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public OTPRes getLoginOTP(OTPReq request) {
		getPath();
		SessionMgr session_mgr = new SessionMgr();
		ResponseData resdata = new ResponseData();
		OTPRes response = new OTPRes();
		Result result = new Result();
		resdata = validateIDV2(request.getUserID());
		result = session_mgr.insertSession(request.getUserID());
		request.setSessionID(result.getSessionID());
		session_mgr.saveActivity(request.getSessionID(), request.getUserID(), "getLoginOTP", "false", "");// for loginHistory
		if (resdata.getCode().equalsIgnoreCase("0000")) {
			resdata = new ResponseData();
			resdata = session_mgr.updateActivityTime_Old(request.getSessionID(), request.getUserID()); // update
			if (resdata.getCode().equalsIgnoreCase("0000")) {
				session_mgr.saveActivity(request.getSessionID(), request.getUserID(), "getLoginOTP", "false", ""); // for
				response = session_mgr.sendOTPCode(request, 0);
				if(response.getCode().equalsIgnoreCase("0000")){
					// response.setSessionID(request.getSessionID());
				}else {
					return response;
				}
			}else{
				response.setCode(resdata.getCode());
				response.setDesc(resdata.getDesc());
			}
		} else {
			response.setCode(resdata.getCode());
			response.setDesc(resdata.getDesc());
		}
		return response;
	}*/
	@POST
	@Path("getLoginOTP")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public OTPRes getLoginOTP(OTPReq request) {
		getPath();
		String deviceid;
		String flag="0";
		SessionMgr session_mgr = new SessionMgr();
		ResponseData resdata = new ResponseData();
		ResponseData resultdata = new ResponseData();
		OTPRes response = new OTPRes();
		Result result = new Result();
		resdata = validateIDV2(request.getUserID());
		if (resdata.getCode().equalsIgnoreCase("0000")) {
			resultdata=session_mgr.selectDeviceID(request.getUserID());
			deviceid=resultdata.getDesc(); 
			if(deviceid.equals(request.getDeviceID())){
				flag="0";
			}else{
				flag="1";
				resultdata=session_mgr.updateT2(request.getUserID(),deviceid,flag);
			}
			
		
			request.setFlag("0");
			resdata = new ResponseData();				
				response = session_mgr.sendOTPCode(request, 0);
				if(response.getCode().equalsIgnoreCase("0000")){
					session_mgr.saveActivity(request.getSessionID(), request.getUserID(), "getLoginOTP", "false", ""); 
				}else {
					return response;
				}
			/*}else{
				response.setCode(resdata.getCode());
				response.setDesc(resdata.getDesc());
			}*/
		} else {
			response.setCode(resdata.getCode());
			response.setDesc(resdata.getDesc());
		}
		return response;
	}

	public void getPath() {
		ServerSession.serverPath = req.getServletContext().getRealPath("/") + "/";
		DAOManager.AbsolutePath = req.getServletContext().getRealPath("/") + "/";
	}

	@POST
	@Path("getPayee")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public MerchantResData getPayee(MerchantReq req) {
		getPath();
		return new Service001Mgr().getMerchant("", "");
	}

	@POST
	@Path("getPdf")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DocumentResponse getPdf(DocumentRequest req) {
		getPath();

		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Getting Document List By Region and Type.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}

		DocumentResponse responseStateList = new DocumentMgr().getPdfByTypeAndRegion(req);

		return responseStateList;
	}

	@POST
	@Path("getRegister")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public RegisterResponseData getRegister(RegisterRequestData request) {
		getPath();

		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Get Register.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}

		RegisterResponseData response = new UserRegistrationMgr().registerSelect(Long.parseLong(request.getHautosys()),
				request.getT7());

		return response;
	}

	@POST
	@Path("getRegisterDC")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public RegisterResponseData getRegisterDC(RegisterRequestData request) {
		getPath();

		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Get Register.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}

		RegisterResponseData response = new UserRegistrationMgr().getRegisterDC(Long.parseLong(request.getHautosys()));

		return response;
	}

	@GET
	@Path("getStateList")
	@Produces(MediaType.APPLICATION_JSON)
	public StateListResponseData getStateList() {
		getPath();

		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Getting State List.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}

		StateListResponseData responseStateList = new UserRegistrationMgr().getStateList();

		return responseStateList;
	}

	@GET
	@Path("getStateListDC")
	@Produces(MediaType.APPLICATION_JSON)
	public StateListResponseData getStateListDC() {
		getPath();

		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Getting State List.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}

		StateListResponseData responseStateList = new UserRegistrationMgr().getStateListDC();

		return responseStateList;
	}

	@POST
	@Path("getTosyskey")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public SyskeyResponseData getTosyskey(SyskeyRequestData request) {
		getPath();

		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("getTosyskey.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}

		SyskeyResponseData response = new UserRegistrationMgr().getTosyskey(request);

		return response;
	}

	@GET
	@Path("getUserListing")
	@Produces(MediaType.APPLICATION_JSON)
	public UserViewDataArr getUserlisting(@QueryParam("searchVal") String searchText,
			@QueryParam("pagesize") int pageSize, @QueryParam("currentpage") int currentPage,
			@QueryParam("operation") String operation, @QueryParam("sessionID") String sessionId,
			@QueryParam("userID") String userID) {
		getPath();
		ResponseData sessionRes = new ResponseData();
		String formID ="";
		if(operation.equalsIgnoreCase("activate")){
			 formID ="Activate User";
		}else if(operation.equalsIgnoreCase("deactivate")){
			 formID ="Deactivate User";
		}else if(operation.equalsIgnoreCase("lock")){
			 formID ="Lock User";
		}else if(operation.equalsIgnoreCase("unlock")){
			 formID ="Unlock User";
		}else if(operation.equalsIgnoreCase("allbankuser")){
			 formID ="Branch User Setup";
		}
		sessionRes = new SessionMgr().updateActivityTime(sessionId, userID,formID);
		Service001Mgr mgr = new Service001Mgr();

		UserViewDataArr res = new UserViewDataArr();

		if (sessionRes.getCode().equalsIgnoreCase("0000")) {
			res = new UserAdmMgr().getAllUserData(searchText, pageSize, currentPage, operation,mgr.isMasterUser(userID));
		} else {
			res.setMsgCode(sessionRes.getCode());
			res.setMsgDesc(sessionRes.getDesc());
		}

		return res;
	}

	// reset password start
	// check Name and Nrc By ID
	@POST
	@Path("getUserNameAndNrc")
	@Produces(MediaType.APPLICATION_JSON)
	public UserData getUserNameAndNrc(SessionData udata) {
		getPath();
		ResponseData sessionRes = new ResponseData();
		sessionRes = new SessionMgr().updateActivityTime(udata.getSessionID(), udata.getUserID(),"Reset Password");
		UserData data = new UserData();

		if (sessionRes.getCode().equalsIgnoreCase("0000")) {
			data = new UserAdmMgr().getUserNameAndNrc(udata);
		} else {
			data.setMsgCode(sessionRes.getCode());
			data.setMsgDesc(sessionRes.getDesc());
		}
		return data;
	}

	@POST
	@Path("getVersion")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public VersionRes getVersion(VersionReq aReq) {
		getPath();
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Get Version Req.... " + aReq.toString());
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
		}
		VersionRes response = new VersionRes();
		if (validateGetVersion(aReq).getCode().equals("0000")) {
			response = new VersionMgr().getVersion(aReq);
		} else {
			return response;
		}
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Get Version Res : " + response.toString());
			l_err.add("=============================================================================");
			GeneralUtil.writeLog(l_err, "\\Mobile\\log\\");
		}
		return response;
	}

	@POST
	@Path("/imageupload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.TEXT_PLAIN)
	public String imageUploadFile(@FormDataParam("file") InputStream fis,
			@FormDataParam("file") FormDataContentDisposition fdcd) {
		getPath();
		System.out.println("I m here=" + fdcd);
		String orgname = "upload/location/"; // request.getServletContext().getRealPath("/")
												// + "/"
		System.out.println("real path = " + req.getServletContext().getRealPath("/" + orgname));
		String filePath = req.getServletContext().getRealPath("/" + orgname);
		filePath = filePath.replace("WalletService", "DigitalMedia");

		OutputStream outpuStream = null;
		System.out.println("I m here" + fis.toString());
		String fileName = fdcd.getFileName();
		System.out.println("File Name: " + fileName);
		String basename = FilenameUtils.getBaseName(fileName);
		// basename = basename.replaceAll("\\s+","");
		String extension = FilenameUtils.getExtension(fileName);
		String fullname = basename + "." + extension;
		filePath += fullname;
		System.out.println("File Path3: " + filePath);
		try {
			int read = 0;
			byte[] bytes = new byte[1024];
			outpuStream = new FileOutputStream(new File(filePath));
			while ((read = fis.read(bytes)) != -1) {
				outpuStream.write(bytes, 0, read);
			}
			outpuStream.flush();
			outpuStream.close();
		} catch (IOException iox) {
			iox.printStackTrace();
		} finally {
			if (outpuStream != null) {
				try {
					outpuStream.close();
				} catch (Exception ex) {
				}
			}
		}
		return fullname;
	}

	@POST
	@Path("insertImageLocation")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ImageResponse insertImageLocation(ImageData imgreq) {
		ImageResponse response = null;

		getPath();

		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Insert ImageLocation.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}

		response = new ImageMgr().insertImage(imgreq);

		return response;
	}

	@POST
	@Path("lockunlockUser")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Result lockunlockUser(String skey, @QueryParam("k") String status, @QueryParam("userid") String userid,
			@QueryParam("sessionID") String sessionId,@QueryParam("operation") String operation) {
		getPath();
		ResponseData sessionRes = new ResponseData();
		String formID ="";
		if(operation.equalsIgnoreCase("lock")){
			 formID ="Lock User";
		}else if(operation.equalsIgnoreCase("unlock")){
			 formID ="Unlock User";
		}
		sessionRes = new SessionMgr().updateActivityTime(sessionId, userid, formID);
		Result res = new Result();

		if (sessionRes.getCode().equalsIgnoreCase("0000")) {
			skey = skey.substring(1, skey.length() - 1);
			long syskey = 0;
			syskey = Long.parseLong(skey);

			res = new UserAdmMgr().lockUnlockUserData(userid, syskey, status);
		} else {
			res.setMsgCode(sessionRes.getCode());
			res.setMsgDesc(sessionRes.getDesc());
		}

		return res;
	}
	// activate/deactivate user & lock/unlock user end

	@POST
	@Path("/mobileupload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.TEXT_PLAIN)
	public String mobileuploadFile(@FormDataParam("file") InputStream fis,
			@FormDataParam("file") FormDataContentDisposition fdcd) {
		getPath();
		String fullname = "";
		System.out.println("I m here=" + fdcd);
		String orgname = "upload/image/userProfile/"; // request.getServletContext().getRealPath("/")
														// + "/"
		System.out.println("real path = " + req.getServletContext().getRealPath("/" + orgname));
		String filePath = req.getServletContext().getRealPath("/" + orgname);
		filePath = filePath.replace("WalletService", "DigitalMedia");

		OutputStream outpuStream = null;
		System.out.println("I m here" + fis.toString());
		String fileName = fdcd.getFileName();
		System.out.println("File Name: " + fileName);
		String basename = FilenameUtils.getBaseName(fileName);
		// basename = basename.replaceAll("\\s+","");
		String extension = FilenameUtils.getExtension(fileName);
		if (extension.equalsIgnoreCase("JPEG") || extension.equalsIgnoreCase("JPG")
				|| extension.equalsIgnoreCase("PNG")) {
			fullname = basename + "." + extension;
			filePath += fullname;
			System.out.println("File Path3: " + filePath);
			//resize atn
			  File file = new File(filePath);
			  if (file.exists()) {
	                double bytes = file.length();
	                if (bytes > 256000) {
	                    resize(filePath, new File(filePath));
	                }
	            }//resize end
			try {
				int read = 0;
				byte[] bytes = new byte[1024];
				outpuStream = new FileOutputStream(new File(filePath));
				while ((read = fis.read(bytes)) != -1) {
					outpuStream.write(bytes, 0, read);
				}
				outpuStream.flush();
				outpuStream.close();
			} catch (IOException iox) {
				iox.printStackTrace();
			} finally {
				if (outpuStream != null) {
					try {
						outpuStream.close();
					} catch (Exception ex) {
					}
				}
			}
		} 
		return fullname;
	}
	public static boolean resize(String filePath, File uploadedFile) {
        try {
            // reads input image
            BufferedImage inputImage = ImageIO.read(uploadedFile);

            // creates output image
                int newWidth = (int) (inputImage.getWidth() * 0.5);
                int newHeight = (int) (inputImage.getHeight() * 0.5);
                BufferedImage outputImage = new BufferedImage(newWidth, newHeight, inputImage.getType());

                // scales the input image to the output image
                Graphics2D g2d = outputImage.createGraphics();
                g2d.drawImage(inputImage, 0, 0, newWidth, newHeight, null);
                g2d.dispose();

                // extracts extension of output file
                String formatName = filePath.substring(filePath.lastIndexOf(".") + 1);

                // writes to output file
                ImageIO.write(outputImage, formatName, new File(filePath));
    
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

	@POST
	@Path("openAccount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public WalletResponseData openAccount(WalletRequestData data) {
		getPath();

		WalletResponseData response = new WalletResponseData();
		DBAccountMgr mgr = new DBAccountMgr();
		PwdMgr pMgr = new PwdMgr();
		SessionMgr session_mgr = new SessionMgr();
		ResultTwo ret = new ResultTwo();
		Result result = new Result();
		String sessionId = "";
		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("User Registration.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}
		if (!data.getIv().equals("")) {
			AesUtil util = new AesUtil(128, 1000);
			String password = util.decrypt(data.getSalt(), data.getIv(), data.getPassword());
			data.setPassword(password);
		}
		ret = pMgr.checkPasswordPolicyPattern(data.getPassword());
		if (ret.getMsgCode().equalsIgnoreCase("0000")) {
			data.setPassword(ServerUtil.hashPassword(data.getPassword()));
		} else {
			response.setMessageCode(ret.getMsgCode());
			response.setMessageDesc(ret.getMsgDesc());
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Login User ID :" + data.getUserID());
				l_err.add("Wallet Registration Response Message : " + response.toString());
				l_err.add("Internal Message : " + "User already logged in the same time");
				l_err.add("=============================================================================");
				GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
			}
			return response;
		}
		ResponseData resultdata = new ResponseData();
		resultdata = session_mgr.updateActivityTime_Old(data.getSessionID(), data.getUserID());
		//if (resultdata.getCode().equals("0000")) {
			session_mgr.saveActivity(data.getSessionID(), data.getUserID(), "register", "false", "");// for
			response = mgr.openAccount(data.getUserID(), data.getSessionID(), data.getName(), data.getNrc(), data.getsKey(),
					data.getInstitutionCode(), data.getRegion(), data.getPhoneType(), data.getField1(), data.getPassword(),
					data.getAppVersion(), data.getRegionNumber());
			if (response.getMessageCode().equalsIgnoreCase("0000")) {
				result = session_mgr.insertSession(data.getUserID());
				if (!result.isState()) {
					response.setMessageCode("0014");
					response.setMessageDesc("User already logged in");
					// Response log
					if (ServerGlobal.isWriteLog()) {
						ArrayList<String> l_err = new ArrayList<String>();
						l_err.add("Time :" + GeneralUtil.getTime());
						l_err.add("Login User ID :" + data.getUserID());
						l_err.add("Wallet Registration Response Message : " + response.toString());
						l_err.add("Internal Message : " + "User already logged in the same time");
						l_err.add("=============================================================================");
						GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
					}
					return response;
				}
				sessionId = result.getSessionID();
				response.setSessionID(sessionId);
				session_mgr.saveActivity(sessionId, data.getUserID(), "Registration", "false", "");// for
																									// loginHistory
			}
		/*} else {
			response.setMessageCode(resultdata.getCode());
			response.setMessageDesc(resultdata.getDesc());
		}*/
		// Response log
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("User Registration : " + response.toString());
			l_err.add("=============================================================================");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}
		return response;
	}
	
	@POST
	@Path("openAgentAccount")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public WalletResponseData openAgentAccount(WalletRequestData data) {		
		WalletResponseData response = new WalletResponseData();
		DBAccountMgr mgr = new DBAccountMgr();
		PwdMgr pMgr = new PwdMgr();
		SessionMgr session_mgr = new SessionMgr();
		ResultTwo ret = new ResultTwo();
		Result result = new Result();
		String sessionId = "";
		// Request log		
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("User Registration.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}
		try {
//			if (!data.getIv().equals("")) {
//				AesUtil util = new AesUtil(128, 1000);
//				String password = util.decrypt(data.getSalt(), data.getIv(), data.getPassword());
//				data.setPassword(password);
//			}
			data.setPassword(ServerUtil.hashPassword(data.getPassword()));
			// check existing user and get its syskey and ?
			response = new UserRegistrationMgr().checkUserByUserID(data.getUserID());
			if (!response.getMessageCode().equals("0000")) {
				// create chatting account
				data.setsKey(ChatAdmMgr.registerChatUser(data));
				if(!data.getsKey().equals("")){
					response = mgr.openAccount(data.getUserID(), data.getSessionID(), data.getName(), data.getNrc(), data.getsKey(),
							data.getInstitutionCode(), data.getRegion(), data.getPhoneType(), data.getField1(), data.getPassword(),
							data.getAppVersion(), data.getRegionNumber());
				}
			}
			result = session_mgr.insertSession(data.getUserID());			
			sessionId = result.getSessionID();
			response.setSessionID(sessionId);
			response.setUserId(data.getUserID());
			session_mgr.saveActivity(sessionId, data.getUserID(), "Registration", "false", "");// for
			// Response log
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("User Registration : " + response.toString());
				l_err.add("=============================================================================");
				GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
			}
		} catch(Exception e) {
			e.printStackTrace();
			response.setMessageCode("0014");
			response.setMessageDesc("Server Error:" + e.getMessage());
		}
		return response;
	}

	@POST
	@Path("payment")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TransferOutRes payment(TransferOutReq data) {
		getPath();
		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Payment.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}
		TransferOutRes res = new TransferOutRes();
		ResponseData resdata = new ResponseData();
		resdata = new SessionMgr().updateActivityTime_Old(data.getSessionID(),
				data.getUserID()); // update
		if (!resdata.getCode().equalsIgnoreCase("0000")) {
			res.setCode(resdata.getCode());
			res.setDesc(resdata.getDesc());
			return res;
		}
		return new TransferMgr().payment(data);
	}

	@POST
	@Path("readFile")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<String> readFile() {
		getPath();

		String path = DAOManager.AbsolutePath + "WEB-INF//data//ConnectionConfig.txt";
		ArrayList<String> abc = null;
		try {
			abc = ReadFile.readConnection(path);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return abc;
	}

	@POST
	@Path("readPswPolicy")
	@Produces(MediaType.APPLICATION_JSON)
	public PswPolicyData readPswPolicy(PswPolicyData data) {
		getPath();

		PswPolicyData pwdData = new PswPolicyData();
		PwdMgr pmgr = new PwdMgr();

		SessionMgr s_mgr = new SessionMgr();
		ResponseData response = new ResponseData();
		response = s_mgr.updateActivityTime_Old(data.getSessionID(), data.getUserID());
		if (response.getCode().equals("0000")) {
			pwdData = pmgr.readPswPolicy();
			pwdData.setMsgCode("0000");
			pwdData.setMsgDesc(response.getDesc());
		} else {
			pwdData.setMsgCode("0016");
			pwdData.setMsgDesc(response.getDesc());
		}
		return pwdData;
	}
	// locator setup end

	// activate/deactivate user & lock/unlock user
	// get create user data
	@POST
	@Path("getUserProfileData")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserData readUserProfileDataBySyskey(String skey, @QueryParam("sessionID") String sessionId,
			@QueryParam("userID") String userID,@QueryParam("operation") String operation) {
		getPath();
		ResponseData sessionRes = new ResponseData();
		UserData res = new UserData();
		String formID ="";
		if(operation.equalsIgnoreCase("activate")){
			 formID ="Activate User";
		}else if(operation.equalsIgnoreCase("deactivate")){
			 formID ="Deactivate User";
		}else if(operation.equalsIgnoreCase("lock")){
			 formID ="Lock User";
		}else if(operation.equalsIgnoreCase("unlock")){
			 formID ="Unlock User";
		}
		sessionRes = new SessionMgr().updateActivityTime(sessionId, userID,formID);
		if (sessionRes.getCode().equalsIgnoreCase("0000")) {
			skey = skey.substring(1, skey.length() - 1);
			long syskey = Long.parseLong(skey);

			res = UserAdmMgr.readUserProfileDataBySyskey(syskey);

		} else {
			res.setMsgCode(sessionRes.getCode());
			res.setMsgDesc(sessionRes.getDesc());
		}

		return res;
	}

	@POST
	@Path("resetPassword")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ResultTwo resetPassword(PwdData pData) {
		getPath();
		PwdMgr mgr = new PwdMgr();
		ResultTwo ret = new ResultTwo();
		SessionMgr session_mgr = new SessionMgr();
		ResponseData resdata = new ResponseData();
		resdata = session_mgr.updateActivityTime_Old(pData.getSessionid(), pData.getUserid()); // update
		if (resdata.getCode().equalsIgnoreCase("0000")) {
			session_mgr.saveActivity(pData.getSessionid(), pData.getUserid(), "resetpassword", "false", ""); // for;
			if (!pData.getIv().equals("")) {
				AesUtil util = new AesUtil(128, 1000);
				String newpassword = util.decrypt(pData.getSalt(), pData.getIv(), pData.getNewpassword());
				pData.setNewpassword(newpassword);
			}
			ret = mgr.resetPassword(pData);
		} else {
			ret.setMsgCode(resdata.getCode());
			ret.setMsgDesc(resdata.getDesc());
		}
		return ret;
	}

	@POST
	@Path("resetPasswordbyId")
	@Produces(MediaType.APPLICATION_JSON)
	public Result resetPasswordById(SessionData udata) {
		getPath();
		ResponseData sessionRes = new ResponseData();
		sessionRes = new SessionMgr().updateActivityTime(udata.getSessionID(), udata.getUserID(),"Reset Password");

		Result res = new Result();

		if (sessionRes.getCode().equalsIgnoreCase("0000")) {
			res = new UserAdmMgr().resetPasswordById(udata);
		} else {
			res.setMsgCode(sessionRes.getCode());
			res.setMsgDesc(sessionRes.getDesc());
		}

		return res;
	}

	@POST
	@Path("resetUser")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public WalletResponseData resetUser(WalletRequestData data) {
		getPath();

		WalletResponseData res = new WalletResponseData();
		UserRegistrationMgr mgr = new UserRegistrationMgr();

		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("User Registration.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}
		res = mgr.resetUser(data.getUserID(), 0);
		return res;
	}

	// version history end
	// locator setup start
	@POST
	@Path("saveATMLocator")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces(MediaType.APPLICATION_JSON)
	public Result saveATMLocator(ATMLocation data) {
		Result res = new Result();
		ATMLocationMgr mgr = new ATMLocationMgr();
		getPath();

		SessionMgr s_mgr = new SessionMgr();
		ResponseData response = new ResponseData();
		response = s_mgr.updateActivityTime(data.getSessionID(), data.getUserid());
		if (response.getCode().equals("0000")) {
			res = mgr.saveATMLocator(data);
		} else {
			res.setMsgCode(response.getCode());
			res.setMsgDesc(response.getDesc());
			res.setState(false);
		}

		return res;

	}

	@POST
	@Path("saveProfile")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public RegisterResponseData saveProfile(RegisterRequestData request) {
		RegisterResponseData response = new RegisterResponseData();
		getPath();
		boolean hasError = false;
		String errorMsg = "";
		if (request.getUserName() == null || request.getUserName().trim().length() == 0) {
			hasError = true;
			errorMsg = "Invalid Name.";
		}
		ResponseData resdata = new ResponseData();
		resdata = new SessionMgr().updateActivityTime_Old(request.getSessionID(),
				request.getUserID()); // update
		if (!resdata.getCode().equalsIgnoreCase("0000")) {
			response.setMessageCode(resdata.getCode());
			response.setMessageDesc(resdata.getDesc());
			return response;
		}
		if (hasError) {
			response = new RegisterResponseData();
			response.setMessageCode("0014");
			response.setMessageDesc(errorMsg);
		} else {
			getPath();
			// Request log
			GeneralUtil.readDebugLogStatus();
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Save Profile.... ");
				l_err.add("-----------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
			}

			response = new UserRegistrationMgr().saveProfile(request);
		}

		return response;
	}

	@POST
	@Path("saveVersionHistory")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public VersionHistoryData saveVersionHistory(VersionHistoryData request) {
		VersionHistoryData response = null;

		getPath();
		ResponseData sessionState = new ResponseData();
		sessionState = new SessionMgr().updateActivityTime(request.getSessionID(), request.getUserID(),"version-history");

		if (sessionState.getCode().equals("0000")) {
			if (request.getAutokey() != null && request.getAutokey().trim().length() != 0) {
				try {
					Long.parseLong(request.getAutokey().trim());
					request.setAutokey(request.getAutokey().trim());
				} catch (Exception e) {
					response = new VersionHistoryData();
					response.setMsgCode("0014");
					response.setMsgDesc("Invalid Autokey.");
					response.setState(false);

					return response;
				}
			}

			if (request.getAppcode() == null || request.getAppcode().trim().length() == 0) {
				response = new VersionHistoryData();
				response.setMsgCode("0014");
				response.setMsgDesc("Blank App Code.");
				response.setState(false);
			} else if (request.getVersion() == null || request.getVersion().trim().length() == 0) {
				response = new VersionHistoryData();
				response.setMsgCode("0014");
				response.setMsgDesc("Blank Version.");
				response.setState(false);
			} else if (request.getVersion().trim().length() > 20) {
				response = new VersionHistoryData();
				response.setMsgCode("0014");
				response.setMsgDesc("Invalid Version. Its Maximum Character Is 20.");
				response.setState(false);
			} else if (request.getVersiontitle().trim().length() > 50) {
				response = new VersionHistoryData();
				response.setMsgCode("0014");
				response.setMsgDesc("Invalid Version Title. Its Maximum Character Is 50.");
				response.setState(false);
			} else if (request.getDescription().trim().length() > 500) {
				response = new VersionHistoryData();
				response.setMsgCode("0014");
				response.setMsgDesc("Invalid Description. Its Maximum Character Is 500.");
				response.setState(false);
			} else if (request.getStartdate() == null || request.getStartdate().trim().length() == 0) {
				response = new VersionHistoryData();
				response.setMsgCode("0014");
				response.setMsgDesc("Blank Start Date.");
				response.setState(false);
			} else if (request.getStartdate().trim().length() != 0
					&& !request.getStartdate().matches("\\d{4}-\\d{2}-\\d{2}")) {
				response = new VersionHistoryData();
				response.setMsgCode("0014");
				response.setMsgDesc("Invalid Start Date.");
				response.setState(false);
			} else if (request.getDuedate() == null || request.getDuedate().trim().length() == 0) {
				response = new VersionHistoryData();
				response.setMsgCode("0014");
				response.setMsgDesc("Blank Due Date.");
				response.setState(false);
			} else if (request.getDuedate().trim().length() != 0
					&& !request.getDuedate().matches("\\d{4}-\\d{2}-\\d{2}")) {
				response = new VersionHistoryData();
				response.setMsgCode("0014");
				response.setMsgDesc("Invalid Due Date.");
				response.setState(false);
			} else if (request.getStatus() == null || request.getStatus().trim().length() == 0) {
				response = new VersionHistoryData();
				response.setMsgCode("0014");
				response.setMsgDesc("Blank Status.");
				response.setState(false);
			}

			if (response != null) {
				return response;
			}

			if (request.getStatus() != null && request.getStatus().trim().length() != 0) {
				try {
					Long.parseLong(request.getStatus().trim());
					request.setStatus(request.getStatus().trim());
				} catch (Exception e) {
					response = new VersionHistoryData();
					response.setMsgCode("0014");
					response.setMsgDesc("Invalid Status.");
					response.setState(false);

					return response;
				}
			}

			request.setAppcode(request.getAppcode().trim());
			request.setVersion(request.getVersion().trim());
			request.setVersiontitle(request.getVersiontitle().trim());
			request.setDescription(request.getDescription().trim());
			request.setStartdate(request.getStartdate().trim().replaceAll("[-+.^:,]", ""));
			request.setDuedate(request.getDuedate().trim().replaceAll("[-+.^:,]", ""));
			request.setRemark(request.getRemark().trim());

			response = new VersionHistoryMgr().save(request);
		} else {
			response = new VersionHistoryData();
			response.setMsgCode(sessionState.getCode());
			response.setMsgDesc(sessionState.getDesc());
			response.setState(false);
		}

		return response;
	}

	@POST
	@Path("selectUserInfo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UserInfoResponse selectUserInfo(SessionData data) {
		getPath();

		UserInfoResponse response = new UserRegistrationMgr().selectUserInfo();

		return response;
	}

	// top up
	@POST
	@Path("topup")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TopupRes topup(TopupReq topupreq) {
		getPath();

		boolean hasError = false;
		TopupRes response = new TopupRes();

		if (topupreq.getAmount() == null || topupreq.getAmount().trim().length() == 0) {
			hasError = true;
		}

		if (topupreq.getAmount() != null && topupreq.getAmount().trim().length() != 0) {
			try {
				topupreq.setAmountdbl(Long.parseLong(topupreq.getAmount().trim()));
			} catch (Exception e) {
				response.setMessageCode("0014");
				response.setMessageDesc("Invalid Amount.");

				return response;
			}
		}

		if (topupreq.getAmountdbl() <= 0) {
			hasError = true;
		}

		if (hasError) {
			response.setMessageCode("0014");
			response.setMessageDesc("Invalid Amount.");
		} else {
			// Request log
			GeneralUtil.readDebugLogStatus();
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("User Registration.... ");
				l_err.add("-----------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
			}

			response = new WalletMgr().topup(topupreq);
		}

		return response;
	}

	@POST
	@Path("transferOut")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TransferOutRes transferOut(TransferOutReq data) {
		getPath();
		TransferOutRes ret = new TransferOutRes();
		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("User Registration.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}
		ResponseData resultdata = new ResponseData();
		resultdata = validateTransferReq(data);
		if(resultdata.getCode().equals("0000")){
			resultdata = new SessionMgr().updateActivityTime_Old(data.getSessionID(), data.getUserID());
			if (resultdata.getCode().equals("0000")) {
				ret = new TransferMgr().transferOutInternal(data);
			} else {
				ret.setCode(resultdata.getCode());
				ret.setDesc(resultdata.getDesc());
			}
		} else {
			ret.setCode(resultdata.getCode());
			ret.setDesc(resultdata.getDesc());
		}
		
		return ret;
	}

	@POST
	@Path("updateDeviceId")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public WalletResponseData updateDeviceId(WalletRequestData data) {
		WalletResponseData response = new WalletResponseData();

		getPath();

		UserRegistrationMgr mgr = new UserRegistrationMgr();
		SessionMgr session_mgr = new SessionMgr();
		Result result = new Result();
		String sessionId = "";

		// Request log
		GeneralUtil.readDebugLogStatus();

		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Update DeviceId.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}

		response = mgr.updateDeviceId(data);

		if (response.getMessageCode().equalsIgnoreCase("0000")) {
			result = session_mgr.insertSession(data.getUserID());
			if (!result.isState()) {
				response.setMessageCode("0014");
				response.setMessageDesc("User already logged in");

				// Response log
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("Time :" + GeneralUtil.getTime());
					l_err.add("Login User ID :" + data.getUserID());
					l_err.add("Update DeviceId Response Message : " + response.toString());
					l_err.add("Internal Message : " + "User already logged in the same time");
					l_err.add("=============================================================================");
					GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
				}

				return response;
			}

			sessionId = result.getSessionID();
			response.setSessionID(sessionId);
			session_mgr.saveActivity(sessionId, data.getUserID(), "walletSignin", "false", "");// for
																								// loginHistory
		}

		// Response log
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Update DeviceId : " + response.toString());
			l_err.add("=============================================================================");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}

		return response;
	}

	@POST
	@Path("updateDeviceIdDC")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public WalletResponseData updateDeviceIdDC(WalletRequestData data) {
		WalletResponseData response = new WalletResponseData();

		getPath();

		UserRegistrationMgr mgr = new UserRegistrationMgr();
		SessionMgr session_mgr = new SessionMgr();
		Result result = new Result();
		String sessionId = "";

		// Request log
		GeneralUtil.readDebugLogStatus();

		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Update DeviceId.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}

		response = mgr.updateDeviceIdDC(data);

		if (response.getMessageCode().equalsIgnoreCase("0000")) {
			result = session_mgr.insertSession(data.getUserID());

			if (!result.isState()) {
				response.setMessageCode("0014");
				response.setMessageDesc("User already logged in");

				// Response log
				if (ServerGlobal.isWriteLog()) {
					ArrayList<String> l_err = new ArrayList<String>();
					l_err.add("Time :" + GeneralUtil.getTime());
					l_err.add("Login User ID :" + data.getUserID());
					l_err.add("Update DeviceId Response Message : " + response.toString());
					l_err.add("Internal Message : " + "User already logged in the same time");
					l_err.add("=============================================================================");
					GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
				}

				return response;
			}

			sessionId = result.getSessionID();
			response.setSessionID(sessionId);
			session_mgr.saveActivity(sessionId, data.getUserID(), "walletSignin", "false", "");// for
																								// loginHistory
		}

		// Response log
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("Update DeviceId : " + response.toString());
			l_err.add("=============================================================================");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}

		return response;
	}

	@POST
	@Path("saveRegister")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public RegisterResponseData updateRegister(RegisterRequestData request) {
		RegisterResponseData response = null;

		getPath();

		boolean hasError = false;
		String errorMsg = "";
		/*
		 * if (request.getT23() == null || request.getT23().trim().length() ==
		 * 0) { hasError = true; errorMsg = "Invalid Passbook."; } if
		 * (request.getT22() == null || request.getT22().trim().length() == 0) {
		 * hasError = true; errorMsg = "Invalid ID."; }
		 */
		/*
		 * if (request.getT3() == null || request.getT3().trim().length() == 0)
		 * { hasError = true; errorMsg = "Invalid Name."; } if (request.getT20()
		 * == null || request.getT20().trim().length() == 0) { hasError = true;
		 * errorMsg = "Invalid Father Name."; } if (request.getT21() == null ||
		 * request.getT21().trim().length() == 0) { hasError = true; errorMsg =
		 * "Invalid NRC."; } if (request.getT25() == null ||
		 * request.getT25().trim().length() == 0) { hasError = true; errorMsg =
		 * "Invalid Date of Birth."; } if (request.getT7() == null ||
		 * request.getT7().trim().length() == 0) { hasError = true; errorMsg =
		 * "Invalid Region."; } if (request.getT36() == null ||
		 * request.getT36().trim().length() == 0) { hasError = true; errorMsg =
		 * "Invalid Region."; } if (request.getT38() == null ||
		 * request.getT38().trim().length() == 0) { hasError = true; errorMsg =
		 * "Invalid Status."; }
		 */
		if (hasError) {
			response = new RegisterResponseData();
			response.setMessageCode("0014");
			response.setMessageDesc(errorMsg);
		} else {
			// Request log
			GeneralUtil.readDebugLogStatus();
			if (ServerGlobal.isWriteLog()) {
				ArrayList<String> l_err = new ArrayList<String>();
				l_err.add("");
				l_err.add("=============================================================================");
				l_err.add("Time :" + GeneralUtil.getTime());
				l_err.add("Register Update.... ");
				l_err.add("-----------------------------------------------------------------------------");
				GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
			}

			response = new UserRegistrationMgr().registerUpdate(request);
		}

		return response;
	}

	@POST
	@Path("/uploadNRC")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.TEXT_PLAIN)
	public String uploadNRC(@FormDataParam("file") InputStream fis,
			@FormDataParam("file") FormDataContentDisposition fdcd) {
		getPath();
		System.out.println("I m here=" + fdcd);
		String orgname = "upload/image/userNRC/"; // request.getServletContext().getRealPath("/")
													// + "/"
		System.out.println("real path = " + req.getServletContext().getRealPath("/" + orgname));
		String filePath = req.getServletContext().getRealPath("/" + orgname);
		filePath = filePath.replace("WalletService", "DigitalMedia");

		OutputStream outpuStream = null;
		System.out.println("I m here" + fis.toString());
		String fileName = fdcd.getFileName();
		System.out.println("File Name: " + fileName);
		String basename = FilenameUtils.getBaseName(fileName);
		// basename = basename.replaceAll("\\s+","");
		String extension = FilenameUtils.getExtension(fileName);
		String fullname = basename + "." + extension;
		filePath += fullname;
		System.out.println("File Path3: " + filePath);
		try {
			int read = 0;
			byte[] bytes = new byte[1024];
			outpuStream = new FileOutputStream(new File(filePath));
			while ((read = fis.read(bytes)) != -1) {
				outpuStream.write(bytes, 0, read);
			}
			outpuStream.flush();
			outpuStream.close();
		} catch (IOException iox) {
			iox.printStackTrace();
		} finally {
			if (outpuStream != null) {
				try {
					outpuStream.close();
				} catch (Exception ex) {
				}
			}
		}
		return fullname;
	}

	private VersionRes validateGetVersion(VersionReq aReq) {
		VersionRes ret = new VersionRes();
		if (aReq.getAppCode().equals("") || aReq.getAppCode().equals(null)) {
			ret.setCode("0014");
			ret.setDesc("App code is mandatory");
			return ret;
		} else if (aReq.getVersion().equals("") || aReq.getVersion().equals(null)) {
			ret.setCode("0014");
			ret.setDesc("Version is mandatory");
			return ret;
		} else {
			ret.setCode("0000");
			ret.setDesc("Success");
		}
		return ret;
	}

	public ResponseData validateID(String sessionId, String userId) {
		ResponseData response = new ResponseData();
		getPath();
		if (userId.equalsIgnoreCase("null") || userId.equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("User ID is mandatory");
		} else if (sessionId.equalsIgnoreCase("null") || sessionId.equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("Session ID is mandatory");
		} else {
			response.setCode("0000");
			response.setDesc("Success");
		}
		return response;
	}
	
	public ResponseData validateIDV2(String userId) {
		ResponseData response = new ResponseData();
		getPath();
		if (userId.equalsIgnoreCase("null") || userId.equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("User ID is mandatory");
		} else {
			response.setCode("0000");
			response.setDesc("Success");
		}
		return response;
	}

	public ResponseData validateWL(String sessionId, String userId, String requestType) {
		ResponseData response = new ResponseData();

		getPath();

		// if (requestType.equals("11")){
		if (userId.equalsIgnoreCase("null") || userId.equalsIgnoreCase("")) {
			response.setCode("0014");
			response.setDesc("User ID is mandatory");
		} else {
			response.setCode("0000");
			response.setDesc("Success");
		}
		/*
		 * }else{ if(sessionId.equalsIgnoreCase("null") ||
		 * sessionId.equalsIgnoreCase("")){ response.setCode("0014");
		 * response.setDesc("Session ID is mandatory"); }else
		 * if(userId.equalsIgnoreCase("null") || userId.equalsIgnoreCase("")){
		 * response.setCode("0014"); response.setDesc("User ID is mandatory");
		 * }else{ response.setCode("0000"); response.setDesc("Success"); } }
		 */
		return response;
	}

	public TransferResData validateTransferIn(TransferReqV2Data aData) {
		TransferResData res = new TransferResData();

		getPath();

		res.setCode("0000");
		if (aData.getApikey() == null || aData.getApikey().equals("") || !aData.getApikey().equals("MPO123456")) {
			res.setCode("0014");
			res.setDesc("Invalid Request Parameter");
			return res;
		}

		if (aData.getTransID() == null || aData.getTransID().equals("")) {
			res.setCode("0014");
			res.setDesc("Transaction ID is required");
			return res;
		}

		if (aData.getFromoperator() == null || aData.getFromoperator().equals("")) {
			res.setCode("0014");
			res.setDesc("Sender Operator is required");
			return res;
		}

		if (aData.getFromaccount() == null || aData.getFromaccount().equals("")) {
			res.setCode("0014");
			res.setDesc("Sender Account is required");
			return res;
		}

		if (aData.getFromwallet() == null || aData.getFromwallet().equals("")) {
			res.setCode("0014");
			res.setDesc("Sender Wallet is required");
			return res;
		}

		if (aData.getTooperator() == null || aData.getTooperator().equals("")) {
			res.setCode("0014");
			res.setDesc("Beneficiary Operator is required");
			return res;
		}

		if (aData.getToaccount() == null || aData.getToaccount().equals("")) {
			res.setCode("0014");
			res.setDesc("Beneficiary Account is required");
			return res;
		}

		if (aData.getTowallet() == null || aData.getTowallet().equals("")) {
			res.setCode("0014");
			res.setDesc("Beneficiary Wallet is required");
			return res;
		}

		if (aData.getAmount() <= 0.00) {
			res.setCode("0014");
			res.setDesc("Invalid Amount");
			return res;
		}

		return res;
	}

	// tzz
	@POST
	@Path("walletpayment")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TransferOutRes walletpayment(WalletTransferData wdata) {
		getPath();

		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("wallettransfer.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}
		// Check whether password is needed or not
		TransferOutRes res = new TransferOutRes();
		Service002 s002 = new Service002();
		MessageResponse response = new MessageResponse();
		response = s002.readMessageSettingData(wdata.getMessageRequest());
		if (response.getrKey().equals("true")) {
			if (wdata.getTransferOutReq().getPassword().equals("")) {
				res.setCode("0014");
				res.setDesc("Invalid Password");
			} else {
				try {
					
					String password = wdata.getTransferOutReq().getPassword();
					if (!wdata.getTransferOutReq().getIv().equals("")) {
						AesUtil util = new AesUtil(128, 1000);
						password = util.decrypt(wdata.getTransferOutReq().getSalt(), wdata.getTransferOutReq().getIv(), password);
					}
					wdata.getTransferOutReq().setPassword(ServerUtil.hashPassword(password));
					boolean right = new TransferMgr().checkPassword(wdata.getTransferOutReq().getPassword(),
							wdata.getMessageRequest().getUserID());
					if (right) {
						res = payment(wdata.getTransferOutReq());
					} else {
						res.setCode("0014");
						res.setDesc("Invalid Password");
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					res.setCode("0014");
					res.setDesc(e.toString());
				}

			}
		}
		// end whether password is needed or not

		return res;
	}
	// end

	// tzz
	@POST
	@Path("wallettransfer")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public TransferOutRes wallettransfer(WalletTransferData wdata) {
		getPath();

		// Request log
		GeneralUtil.readDebugLogStatus();
		if (ServerGlobal.isWriteLog()) {
			ArrayList<String> l_err = new ArrayList<String>();
			l_err.add("");
			l_err.add("=============================================================================");
			l_err.add("Time :" + GeneralUtil.getTime());
			l_err.add("wallettransfer.... ");
			l_err.add("-----------------------------------------------------------------------------");
			GeneralUtil.writeLog(l_err, "\\Wallet\\log\\");
		}
		// Check whether password is needed or not
		TransferOutRes res = new TransferOutRes();
		if (wdata.getTransferOutReq().getPassword().equals("")) {
			res.setCode("0014");
			res.setDesc("Invalid Password");
		} else {
			try {
				String password = wdata.getTransferOutReq().getPassword();
				if (!wdata.getTransferOutReq().getIv().equals("")) {
					AesUtil util = new AesUtil(128, 1000);
					password = util.decrypt(wdata.getTransferOutReq().getSalt(), wdata.getTransferOutReq().getIv(), password);
				}
				wdata.getTransferOutReq().setPassword(ServerUtil.hashPassword(password));

				boolean right = new TransferMgr().checkPassword(wdata.getTransferOutReq().getPassword(),
						wdata.getMessageRequest().getUserID());
				if (right) {
					res = transferOut(wdata.getTransferOutReq());
				} else {
					res.setCode("0014");
					res.setDesc("Invalid Password");
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				res.setCode("0014");
				res.setDesc(e.toString());
			}

		}
		return res;
	}
	@POST
	@Path("login")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public WalletResponseData login(CheckOTPRequestData req) {
		WalletResponseData ret = new WalletResponseData();
		//String deviceid=req.getDeviceID();
		String deviceid;
		String t8;
		getPath();
		GeneralUtil.writeDebugLog(req.getUserID(), "Mobile Signin Request Message : ", req.toString());
		SessionMgr session_mgr = new SessionMgr();
		ResponseData response = new ResponseData();
		ResponseData resultdata = new ResponseData();
		ResponseData resdevice = new ResponseData();
		//resdevice=session_mgr.removeDeviceId(deviceid);
		resultdata = session_mgr.validatecheckOTPReqV2(req.getSessionID(), req.getUserID(), req.getrKey(),
				req.getOtpCode());// check request params blank or not
//		resultdata=session_mgr.selectDeviceID(req.getUserID());
//		deviceid=resultdata.getDesc();                                                                                                                                                                                                                                                                                                                   
//		if(deviceid.equals("")|| deviceid.equals(null)){
//			t8="0";
//		}
//		else{
//		if(deviceid.equals(req.getDeviceID())){
//			t8="0";
//		}
//		else{
//			t8="1";
//		}
//		}
		
		
		response = session_mgr.checkOtpCodeV2(req.getUserID(), req.getrKey(),
				req.getOtpCode(), req.getsKey());
		if (response.getCode().equalsIgnoreCase("0000")) {
			req.setSessionID(response.getDesc());
			resultdata = session_mgr.updateActivityTime_Old(req.getSessionID(), req.getUserID());
			if (resultdata.getCode().equals("0000")) {
				session_mgr.saveActivity(req.getSessionID(), req.getUserID(), "login", "false", "");// for check user registration
				WalletRequestData wReq = new WalletRequestData();
				wReq.setUserID(req.getUserID());
				wReq.setDeviceId(req.getDeviceID());
				wReq.setFcmToken(req.getFcmToken());
				wReq.setSessionID(req.getSessionID());
//				wReq.setT8(t8);
				ret = new UserRegistrationMgr().checkUserRegistrationV2(wReq);
				ret.setSessionID(req.getSessionID());
				System.out.print("Finger Print"+ret.getSessionID());
			} else {
				ret.setMessageCode(resultdata.getCode());
				ret.setMessageDesc(resultdata.getDesc());
			}
		} else {
			ret.setMessageCode(response.getCode());
			ret.setMessageDesc(response.getDesc());
			return ret;
		}
		
		return ret;
	}
	
	private ResponseData validateTransferReq(TransferOutReq data){
		ResponseData ret = new ResponseData();
		if(data.getUserID().equalsIgnoreCase(data.getBeneficiaryID())){
			ret.setCode("0014");
			ret.setDesc("Payee and Beneficiary ID should not same!");
		} else {
			ret.setCode("0000");
			ret.setDesc("Success");
		}
		return ret;
	}
	// Merchant Account Mapping Start //
		@GET
		@Path("getCustomerNameandNrcByAccount")
		@Produces(MediaType.APPLICATION_JSON)
		public CustomerData getCustomerNameandNrcByAccount() {
			String account = req.getParameter("account");
			String check = req.getParameter("check");
			String sessionID = req.getParameter("sessionID");
			String userID = req.getParameter("userID");
			String k = req.getParameter("k");
			CustomerData res = new CustomerData();
			DAOManager.AbsolutePath = context.getRealPath("");
			AccountMgr mgr = new AccountMgr();
			SessionMgr s_mgr = new SessionMgr();
			ResponseData response = new ResponseData();
			String formID = "Merchant Profile";
			try {
				getPath();
				response = s_mgr.updateActivityTime(sessionID, userID,formID);
				if (response.getCode().equals("0000")) {
					if (check.equals("Account")) {
						res = mgr.getCustomerNameAndNrcByAccount(account.trim(), true, false);
					} else if (check.equals("GL")) {
						res = mgr.getCustomerNameAndNrcByAccount(account.trim(), false, true);
					} else {
						res.setChkOther(true);
					}
				} else {
					res.setMsgCode(response.getCode());
					res.setMsgDesc(response.getDesc());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return res;
		}
		@GET
		@Path("getAdLink")
		@Produces(MediaType.APPLICATION_JSON)
		@Consumes(MediaType.APPLICATION_JSON)
		public AdvertiseRes getAdLink() {
			getPath();
			AdvertiseRes response = new AdvertiseRes();
			AdvertiseMgr adMgr = new AdvertiseMgr();
			response = adMgr.getAdLink();
			return response;
		}
		@POST
		@Path("checkDeviceIdV2")
		@Produces(MediaType.APPLICATION_JSON)
		@Consumes(MediaType.APPLICATION_JSON)
		public WalletResponseData checkDeviceIdV2(CheckOTPRequestData request){
			String userid;
			String sessionid;
  			String type="11";
			String otpcode="123456";
			String flag="0";
			String deviceID=request.getDeviceID();
			OTPRes response=new OTPRes();
			ResponseData res=new ResponseData();
			ResponseData resultdata = new ResponseData();
			SessionMgr session_mgr = new SessionMgr();
			WalletResponseData ret = new WalletResponseData();
			res=session_mgr.checkDeviceId(deviceID);
			if(res.getCode().equals("0000")){
			userid=res.getDesc();
			response=session_mgr.generateSessionID(userid,type,otpcode,deviceID,flag);
			if(response.getCode().equalsIgnoreCase("0000")){
			sessionid=response.getSessionID();
			resultdata=session_mgr.updateActivityTime_Old(sessionid,userid);
			if (resultdata.getCode().equals("0000")) {
				session_mgr.saveActivity(sessionid,userid, "login", "false", "");// for
				// check user registration
				WalletRequestData wReq = new WalletRequestData();
				wReq.setUserID(userid);
				wReq.setDeviceId(request.getDeviceID());
				wReq.setFcmToken(request.getFcmToken());
				wReq.setSessionID(request.getSessionID());
				ret = new UserRegistrationMgr().checkUserRegistrationV2(wReq);
				ret.setSessionID(sessionid);
				ret.setUserId(userid);
				System.out.print("Session ID"+ret);
			} else {
				ret.setMessageCode(resultdata.getCode());
				response.setDesc(resultdata.getDesc());
				return ret;
			}
			}
			else {
				ret.setMessageCode(response.getCode());
				ret.setMessageDesc(response.getDesc());
				return ret;
			}
		}
			else {
				ret.setMessageCode(res.getCode());
				ret.setMessageDesc(res.getDesc());
				return ret;
			}
			return ret;
		}

	@GET
	@Path("showAccountList")
	@Produces(MediaType.APPLICATION_JSON)
	public UserViewDataArr showAccountList(@QueryParam("searchVal") String searchText,@QueryParam("pagesize") int pageSize, @QueryParam("currentpage") int currentPage,
			@QueryParam("sessionID") String sessionId, @QueryParam("userID") String userID, @QueryParam("chk") String chk) {
		ServerSession.serverPath = req.getServletContext().getRealPath("/") + "/";
		ResponseData sessionRes = new ResponseData();
		sessionRes = new SessionMgr().updateActivityTime(sessionId, userID, "");
		Service001Mgr mgr = new Service001Mgr();
		getPath();
		UserViewDataArr res = new UserViewDataArr();

		if (sessionRes.getCode().equalsIgnoreCase("0000")) {
			res = new UserDataMgr().showAccountList(searchText, pageSize, currentPage, mgr.isMasterUser(userID),chk);
		} else {
			res.setMsgCode(sessionRes.getCode());
			res.setMsgDesc(sessionRes.getDesc());
		}

		return res;
	}

}
