package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AccountGLData {
	private boolean isAccount = false;
	private boolean isGL = false;
	private boolean isOther = false;

	public AccountGLData() {
		clearProperties();
	}

	private void clearProperties() {
		isAccount = false;
		isGL = false;
		isOther = false;

	}

	public boolean isAccount() {
		return isAccount;
	}

	public boolean isGL() {
		return isGL;
	}

	public boolean isOther() {
		return isOther;
	}

	public void setAccount(boolean isAccount) {
		this.isAccount = isAccount;
	}

	public void setGL(boolean isGL) {
		this.isGL = isGL;
	}

	public void setOther(boolean isOther) {
		this.isOther = isOther;
	}

}
