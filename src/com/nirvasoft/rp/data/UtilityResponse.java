package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class UtilityResponse {
	private String billId;
	private String billAmount;
	private String ccyCode;
	private String dueDate;
	private String txnRef;
	private String penalty;
	private String txnDate;
	private String refNo;
	private String commAmount;
	private String discountAmout;
	private String totalAmount;
	private String paidBy;
	private String addT1;
	private String addT2;
	private String addT3;
	private String addN1;
	private String addN2;
	private String addN3;
	private String code;
	private String desc;
	private String effectiveDate;
	private String ccdresmsg;
	private String status;
	private String message;
	private String iCBStxnRef;

	public void clearProperty() {
		refNo = "";
		billAmount = "";
		ccyCode = "";
		dueDate = "";
		txnRef = "";
		penalty = "";
		txnDate = "";
		code = "";
		desc = "";
		commAmount = "";
		discountAmout = "";
		totalAmount = "";
		paidBy = "";
		addT1 = "";
		addT2 = "";
		addT3 = "";
		addN1 = "";
		addN2 = "";
		addN3 = "";
		effectiveDate = "";
		ccdresmsg = "";
		status = "";
		message = "";
		iCBStxnRef= "";
	}

	public String getiCBStxnRef() {
		return iCBStxnRef;
	}

	public void setiCBStxnRef(String iCBStxnRef) {
		this.iCBStxnRef = iCBStxnRef;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCcdresmsg() {
		return ccdresmsg;
	}

	public void setCcdresmsg(String ccdresmsg) {
		this.ccdresmsg = ccdresmsg;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getAddN1() {
		return addN1;
	}

	public String getAddN2() {
		return addN2;
	}

	public String getAddN3() {
		return addN3;
	}

	public String getAddT1() {
		return addT1;
	}

	public String getAddT2() {
		return addT2;
	}

	public String getAddT3() {
		return addT3;
	}

	public String getBillAmount() {
		return billAmount;
	}

	public String getBillId() {
		return billId;
	}

	public String getCcyCode() {
		return ccyCode;
	}

	public String getCode() {
		return code;
	}

	public String getCommAmount() {
		return commAmount;
	}

	public String getDesc() {
		return desc;
	}

	public String getDiscountAmout() {
		return discountAmout;
	}

	public String getDueDate() {
		return dueDate;
	}

	public String getPaidBy() {
		return paidBy;
	}

	public String getPenalty() {
		return penalty;
	}

	public String getRefNo() {
		return refNo;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public String getTxnDate() {
		return txnDate;
	}

	public String getTxnRef() {
		return txnRef;
	}

	public void setAddN1(String addN1) {
		this.addN1 = addN1;
	}

	public void setAddN2(String addN2) {
		this.addN2 = addN2;
	}

	public void setAddN3(String addN3) {
		this.addN3 = addN3;
	}

	public void setAddT1(String addT1) {
		this.addT1 = addT1;
	}

	public void setAddT2(String addT2) {
		this.addT2 = addT2;
	}

	public void setAddT3(String addT3) {
		this.addT3 = addT3;
	}

	public void setBillAmount(String billAmount) {
		this.billAmount = billAmount;
	}

	public void setBillId(String billId) {
		this.billId = billId;
	}

	public void setCcyCode(String ccyCode) {
		this.ccyCode = ccyCode;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCommAmount(String commAmount) {
		this.commAmount = commAmount;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setDiscountAmout(String discountAmout) {
		this.discountAmout = discountAmout;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public void setPaidBy(String paidBy) {
		this.paidBy = paidBy;
	}

	public void setPenalty(String penalty) {
		this.penalty = penalty;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}

	public void setTxnRef(String txnRef) {
		this.txnRef = txnRef;
	}

	@Override
	public String toString() {
		return "UtilityResponse [billId=" + billId + ", billAmount=" + billAmount + ", ccyCode=" + ccyCode
				+ ", dueDate=" + dueDate + ", txnRef=" + txnRef + ", penalty=" + penalty + ", txnDate=" + txnDate
				+ ", refNo=" + refNo + ", commAmount=" + commAmount + ", discountAmout=" + discountAmout
				+ ", totalAmount=" + totalAmount + ", paidBy=" + paidBy + ", addT1=" + addT1 + ", addT2=" + addT2
				+ ", addT3=" + addT3 + ", addN1=" + addN1 + ", addN2=" + addN2 + ", addN3=" + addN3 + ", code=" + code
				+ ", desc=" + desc + "]";
	}

}
