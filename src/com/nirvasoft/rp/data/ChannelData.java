package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ChannelData {
	private long autokey;
	private long channelSyskey;
	private String channelName;
	private String region;
	private String regionNumber;

	public ChannelData() {
		clearProperties();
	}

	private void clearProperties() {

		channelSyskey = 0;
		autokey = 0;
		channelName = "";
		region = "";
		regionNumber = "";
	}

	public long getAutokey() {
		return autokey;
	}

	public String getChannelName() {
		return channelName;
	}

	public long getChannelSyskey() {
		return channelSyskey;
	}

	public String getRegion() {
		return region;
	}

	public String getRegionNumber() {
		return regionNumber;
	}

	public void setAutokey(long autokey) {
		this.autokey = autokey;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public void setChannelSyskey(long channelSyskey) {
		this.channelSyskey = channelSyskey;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public void setRegionNumber(String regionNumber) {
		this.regionNumber = regionNumber;
	}

}
