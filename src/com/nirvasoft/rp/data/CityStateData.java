package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CityStateData {
	private int isLocal;
	private String code;
	private String despEng;

	public CityStateData() {
		clearProperties();
	}

	private void clearProperties() {
		this.code = "";
		this.despEng = "";
		this.isLocal = 0;
	}

	public String getCode() {
		return code;
	}

	public String getDespEng() {
		return despEng;
	}

	public int getIsLocal() {
		return isLocal;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDespEng(String despEng) {
		this.despEng = despEng;
	}

	public void setIsLocal(int isLocal) {
		this.isLocal = isLocal;
	}

}
