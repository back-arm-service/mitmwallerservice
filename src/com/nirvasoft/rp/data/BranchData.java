package com.nirvasoft.rp.data;

public class BranchData {

	private long syskey;
	private String createddate;
	private String modifieddate;
	private String userid;
	private String username;
	private int recordstatus;
	private String branchCode;
	private String branchName;
	private String address;
	private String phone;
	private String fax;
	private int isOtherBranch;
	private String t1;
	private String t2;
	private String t3;
	private String t4;
	private long banksyskey;
	private long n1;
	private int n2;
	private int n3;

	public BranchData() {
		clearProperties();
	}

	private void clearProperties() {

		syskey = 0;
		createddate = "";
		modifieddate = "";
		userid = "";
		username = "";
		recordstatus = 0;
		branchCode = "";
		branchName = "";
		address = "";
		phone = "";
		fax = "";
		isOtherBranch = 0;
		t1 = "";
		t2 = "";
		t3 = "";
		t4 = "";
		banksyskey = 0;
		n1 = 0;
		n2 = 0;
		n3 = 0;

	}

	public String getAddress() {
		return address;
	}

	public long getBanksyskey() {
		return banksyskey;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public String getBranchName() {
		return branchName;
	}

	public String getCreateddate() {
		return createddate;
	}

	public String getFax() {
		return fax;
	}

	public int getIsOtherBranch() {
		return isOtherBranch;
	}

	public String getModifieddate() {
		return modifieddate;
	}

	public long getN1() {
		return n1;
	}

	public int getN2() {
		return n2;
	}

	public int getN3() {
		return n3;
	}

	public String getPhone() {
		return phone;
	}

	public int getRecordstatus() {
		return recordstatus;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getT1() {
		return t1;
	}

	public String getT2() {
		return t2;
	}

	public String getT3() {
		return t3;
	}

	public String getT4() {
		return t4;
	}

	public String getUserid() {
		return userid;
	}

	public String getUsername() {
		return username;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setBanksyskey(long banksyskey) {
		this.banksyskey = banksyskey;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public void setIsOtherBranch(int isOtherBranch) {
		this.isOtherBranch = isOtherBranch;
	}

	public void setModifieddate(String modifieddate) {
		this.modifieddate = modifieddate;
	}

	public void setN1(long n1) {
		this.n1 = n1;
	}

	public void setN2(int n2) {
		this.n2 = n2;
	}

	public void setN3(int n3) {
		this.n3 = n3;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setRecordstatus(int recordstatus) {
		this.recordstatus = recordstatus;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public void setT4(String t4) {
		this.t4 = t4;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
