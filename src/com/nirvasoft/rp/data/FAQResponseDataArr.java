package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FAQResponseDataArr {
	private FAQResponseData[] faqData;
	private String code;
	private String desc;

	public FAQResponseDataArr() {
		clearProperties();
	}

	public void clearProperties() {

	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public FAQResponseData[] getFaqData() {
		return faqData;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setFaqData(FAQResponseData[] faqData) {
		this.faqData = faqData;
	}

}
