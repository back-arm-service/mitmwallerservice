package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterRequestData {

	private String hautosys;
	private long syskey;
	private String createdDate;
	private String modifiedDate;
	private String userName;
	private int recordstatus;
	private int syncstatus;
	private int syncbatch;
	private String t1;
	private String t2;
	private String t3;
	private String t4;
	private String t5;
	private String t6;
	private String t7;
	private String t8;
	private String t9;
	private String t10;
	private String t11;
	private String t12;
	private String t13;
	private String t14;
	private String t15;
	private String t16;
	private String t17;
	private String t18;
	private String t19;
	private String t20;
	private String t21;
	private String t22;
	private String t23;
	private String t24;
	private String t25;
	private String t26;
	private String t27;
	private String t28;
	private String t29;
	private String t30;
	private String t31;
	private String t32;
	private String t33;
	private String t34;
	private String t35;
	private String t36;
	private String t37;
	private String t38;
	private String t39;
	private String t40;
	private String t41;
	private String t42;
	private String t43;
	private String t44;
	private String t45;
	private String t46;
	private String t47;
	private String t48;
	private String t49;
	private String t50;
	private long usersyskey;
	private String t51;
	private String t52;
	private String t53;
	private String t54;
	private String t55;
	private String t56;
	private String t57;
	private String t58;
	private String t59;
	private String t60;
	private String t61;
	private String t62;
	private String t63;
	private String t64;
	private String t65;
	private String t66;
	private String t67;
	private String t68;
	private String t69;
	private String t70;
	private String t71;
	private String t72;
	private String t73;
	private String t74;
	private String t75;
	private String t76;
	private String t77;
	private String t78;
	private String t79;
	private String t80;
	private String sessionID;
	private String userID;

	public RegisterRequestData() {
		clearProperty();
	}

	private void clearProperty() {
		this.hautosys = "";
		this.syskey = 0;
		this.userID = "";
		this.userName = "";
		this.createdDate = "";
		this.modifiedDate = "";
		this.recordstatus = 0;
		this.syncstatus = 0;
		this.syncbatch = 0;
		this.t1 = "";
		this.t2 = "";
		this.t3 = "";
		this.t4 = "";
		this.t5 = "";
		this.t6 = "";
		this.t7 = "";
		this.t8 = "";
		this.t9 = "";
		this.t10 = "";
		this.t11 = "";
		this.t12 = "";
		this.t13 = "";
		this.t14 = "";
		this.t15 = "";
		this.t16 = "";
		this.t17 = "";
		this.t18 = "";
		this.t19 = "";
		this.t20 = "";
		this.t21 = "";
		this.t22 = "";
		this.t23 = "";
		this.t24 = "";
		this.t25 = "";
		this.t26 = "";
		this.t27 = "";
		this.t28 = "";
		this.t29 = "";
		this.t30 = "";
		this.t31 = "";
		this.t32 = "";
		this.t33 = "";
		this.t34 = "";
		this.t35 = "";
		this.t36 = "";
		this.t37 = "";
		this.t38 = "";
		this.t39 = "";
		this.t40 = "";
		this.t41 = "";
		this.t42 = "";
		this.t43 = "";
		this.t44 = "";
		this.t45 = "";
		this.t46 = "";
		this.t47 = "";
		this.t48 = "";
		this.t49 = "";
		this.t50 = "";
		this.usersyskey = 0;
		this.t51 = "";
		this.t52 = "";
		this.t53 = "";
		this.t54 = "";
		this.t55 = "";
		this.t56 = "";
		this.t57 = "";
		this.t58 = "";
		this.t59 = "";
		this.t60 = "";
		this.t61 = "";
		this.t62 = "";
		this.t63 = "";
		this.t64 = "";
		this.t65 = "";
		this.t66 = "";
		this.t67 = "";
		this.t68 = "";
		this.t69 = "";
		this.t70 = "";
		this.t71 = "";
		this.t72 = "";
		this.t73 = "";
		this.t74 = "";
		this.t75 = "";
		this.t76 = "";
		this.t77 = "";
		this.t78 = "";
		this.t79 = "";
		this.t80 = "";
		this.sessionID = "";
		this.userID = "";
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public String getHautosys() {
		return hautosys;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public int getRecordstatus() {
		return recordstatus;
	}

	public int getSyncbatch() {
		return syncbatch;
	}

	public int getSyncstatus() {
		return syncstatus;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getT1() {
		return t1;
	}

	public String getT10() {
		return t10;
	}

	public String getT11() {
		return t11;
	}

	public String getT12() {
		return t12;
	}

	public String getT13() {
		return t13;
	}

	public String getT14() {
		return t14;
	}

	public String getT15() {
		return t15;
	}

	public String getT16() {
		return t16;
	}

	public String getT17() {
		return t17;
	}

	public String getT18() {
		return t18;
	}

	public String getT19() {
		return t19;
	}

	public String getT2() {
		return t2;
	}

	public String getT20() {
		return t20;
	}

	public String getT21() {
		return t21;
	}

	public String getT22() {
		return t22;
	}

	public String getT23() {
		return t23;
	}

	public String getT24() {
		return t24;
	}

	public String getT25() {
		return t25;
	}

	public String getT26() {
		return t26;
	}

	public String getT27() {
		return t27;
	}

	public String getT28() {
		return t28;
	}

	public String getT29() {
		return t29;
	}

	public String getT3() {
		return t3;
	}

	public String getT30() {
		return t30;
	}

	public String getT31() {
		return t31;
	}

	public String getT32() {
		return t32;
	}

	public String getT33() {
		return t33;
	}

	public String getT34() {
		return t34;
	}

	public String getT35() {
		return t35;
	}

	public String getT36() {
		return t36;
	}

	public String getT37() {
		return t37;
	}

	public String getT38() {
		return t38;
	}

	public String getT39() {
		return t39;
	}

	public String getT4() {
		return t4;
	}

	public String getT40() {
		return t40;
	}

	public String getT41() {
		return t41;
	}

	public String getT42() {
		return t42;
	}

	public String getT43() {
		return t43;
	}

	public String getT44() {
		return t44;
	}

	public String getT45() {
		return t45;
	}

	public String getT46() {
		return t46;
	}

	public String getT47() {
		return t47;
	}

	public String getT48() {
		return t48;
	}

	public String getT49() {
		return t49;
	}

	public String getT5() {
		return t5;
	}

	public String getT50() {
		return t50;
	}

	public String getT51() {
		return t51;
	}

	public String getT52() {
		return t52;
	}

	public String getT53() {
		return t53;
	}

	public String getT54() {
		return t54;
	}

	public String getT55() {
		return t55;
	}

	public String getT56() {
		return t56;
	}

	public String getT57() {
		return t57;
	}

	public String getT58() {
		return t58;
	}

	public String getT59() {
		return t59;
	}

	public String getT6() {
		return t6;
	}

	public String getT60() {
		return t60;
	}

	public String getT61() {
		return t61;
	}

	public String getT62() {
		return t62;
	}

	public String getT63() {
		return t63;
	}

	public String getT64() {
		return t64;
	}

	public String getT65() {
		return t65;
	}

	public String getT66() {
		return t66;
	}

	public String getT67() {
		return t67;
	}

	public String getT68() {
		return t68;
	}

	public String getT69() {
		return t69;
	}

	public String getT7() {
		return t7;
	}

	public String getT70() {
		return t70;
	}

	public String getT71() {
		return t71;
	}

	public String getT72() {
		return t72;
	}

	public String getT73() {
		return t73;
	}

	public String getT74() {
		return t74;
	}

	public String getT75() {
		return t75;
	}

	public String getT76() {
		return t76;
	}

	public String getT77() {
		return t77;
	}

	public String getT78() {
		return t78;
	}

	public String getT79() {
		return t79;
	}

	public String getT8() {
		return t8;
	}

	public String getT80() {
		return t80;
	}

	public String getT9() {
		return t9;
	}

	public String getUserID() {
		return userID;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public String getUserName() {
		return userName;
	}

	public long getUsersyskey() {
		return usersyskey;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public void setHautosys(String hautosys) {
		this.hautosys = hautosys;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public void setRecordstatus(int recordstatus) {
		this.recordstatus = recordstatus;
	}

	public void setSyncbatch(int syncbatch) {
		this.syncbatch = syncbatch;
	}

	public void setSyncstatus(int syncstatus) {
		this.syncstatus = syncstatus;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT10(String t10) {
		this.t10 = t10;
	}

	public void setT11(String t11) {
		this.t11 = t11;
	}

	public void setT12(String t12) {
		this.t12 = t12;
	}

	public void setT13(String t13) {
		this.t13 = t13;
	}

	public void setT14(String t14) {
		this.t14 = t14;
	}

	public void setT15(String t15) {
		this.t15 = t15;
	}

	public void setT16(String t16) {
		this.t16 = t16;
	}

	public void setT17(String t17) {
		this.t17 = t17;
	}

	public void setT18(String t18) {
		this.t18 = t18;
	}

	public void setT19(String t19) {
		this.t19 = t19;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setT20(String t20) {
		this.t20 = t20;
	}

	public void setT21(String t21) {
		this.t21 = t21;
	}

	public void setT22(String t22) {
		this.t22 = t22;
	}

	public void setT23(String t23) {
		this.t23 = t23;
	}

	public void setT24(String t24) {
		this.t24 = t24;
	}

	public void setT25(String t25) {
		this.t25 = t25;
	}

	public void setT26(String t26) {
		this.t26 = t26;
	}

	public void setT27(String t27) {
		this.t27 = t27;
	}

	public void setT28(String t28) {
		this.t28 = t28;
	}

	public void setT29(String t29) {
		this.t29 = t29;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public void setT30(String t30) {
		this.t30 = t30;
	}

	public void setT31(String t31) {
		this.t31 = t31;
	}

	public void setT32(String t32) {
		this.t32 = t32;
	}

	public void setT33(String t33) {
		this.t33 = t33;
	}

	public void setT34(String t34) {
		this.t34 = t34;
	}

	public void setT35(String t35) {
		this.t35 = t35;
	}

	public void setT36(String t36) {
		this.t36 = t36;
	}

	public void setT37(String t37) {
		this.t37 = t37;
	}

	public void setT38(String t38) {
		this.t38 = t38;
	}

	public void setT39(String t39) {
		this.t39 = t39;
	}

	public void setT4(String t4) {
		this.t4 = t4;
	}

	public void setT40(String t40) {
		this.t40 = t40;
	}

	public void setT41(String t41) {
		this.t41 = t41;
	}

	public void setT42(String t42) {
		this.t42 = t42;
	}

	public void setT43(String t43) {
		this.t43 = t43;
	}

	public void setT44(String t44) {
		this.t44 = t44;
	}

	public void setT45(String t45) {
		this.t45 = t45;
	}

	public void setT46(String t46) {
		this.t46 = t46;
	}

	public void setT47(String t47) {
		this.t47 = t47;
	}

	public void setT48(String t48) {
		this.t48 = t48;
	}

	public void setT49(String t49) {
		this.t49 = t49;
	}

	public void setT5(String t5) {
		this.t5 = t5;
	}

	public void setT50(String t50) {
		this.t50 = t50;
	}

	public void setT51(String t51) {
		this.t51 = t51;
	}

	public void setT52(String t52) {
		this.t52 = t52;
	}

	public void setT53(String t53) {
		this.t53 = t53;
	}

	public void setT54(String t54) {
		this.t54 = t54;
	}

	public void setT55(String t55) {
		this.t55 = t55;
	}

	public void setT56(String t56) {
		this.t56 = t56;
	}

	public void setT57(String t57) {
		this.t57 = t57;
	}

	public void setT58(String t58) {
		this.t58 = t58;
	}

	public void setT59(String t59) {
		this.t59 = t59;
	}

	public void setT6(String t6) {
		this.t6 = t6;
	}

	public void setT60(String t60) {
		this.t60 = t60;
	}

	public void setT61(String t61) {
		this.t61 = t61;
	}

	public void setT62(String t62) {
		this.t62 = t62;
	}

	public void setT63(String t63) {
		this.t63 = t63;
	}

	public void setT64(String t64) {
		this.t64 = t64;
	}

	public void setT65(String t65) {
		this.t65 = t65;
	}

	public void setT66(String t66) {
		this.t66 = t66;
	}

	public void setT67(String t67) {
		this.t67 = t67;
	}

	public void setT68(String t68) {
		this.t68 = t68;
	}

	public void setT69(String t69) {
		this.t69 = t69;
	}

	public void setT7(String t7) {
		this.t7 = t7;
	}

	public void setT70(String t70) {
		this.t70 = t70;
	}

	public void setT71(String t71) {
		this.t71 = t71;
	}

	public void setT72(String t72) {
		this.t72 = t72;
	}

	public void setT73(String t73) {
		this.t73 = t73;
	}

	public void setT74(String t74) {
		this.t74 = t74;
	}

	public void setT75(String t75) {
		this.t75 = t75;
	}

	public void setT76(String t76) {
		this.t76 = t76;
	}

	public void setT77(String t77) {
		this.t77 = t77;
	}

	public void setT78(String t78) {
		this.t78 = t78;
	}

	public void setT79(String t79) {
		this.t79 = t79;
	}

	public void setT8(String t8) {
		this.t8 = t8;
	}

	public void setT80(String t80) {
		this.t80 = t80;
	}

	public void setT9(String t9) {
		this.t9 = t9;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setUsersyskey(long usersyskey) {
		this.usersyskey = usersyskey;
	}

}
