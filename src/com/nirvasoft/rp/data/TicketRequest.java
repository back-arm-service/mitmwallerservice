package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class TicketRequest {

	private String msgCode;
	private String msgDesc;
	private String sessionID;
	private String userID;
	private String ticketNo;

	public TicketRequest() {
		clearProperties();
	}

	public void clearProperties() {
		this.msgCode = "";
		this.msgDesc = "";
		this.sessionID = "";
		this.userID = "";
		this.ticketNo = "";
	}

	public String getMsgCode() {
		return msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getTicketNo() {
		return ticketNo;
	}

	public String getUserID() {
		return userID;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

}
