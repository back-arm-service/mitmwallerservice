package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import com.nirvasoft.cms.shared.DocumentData;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class DocumentListingDataList {

	private String sessionID;
	private String userID;
	private String msgCode = "";
	private String msgDesc = "";
	private DocumentData[] documentData;

	private String fromdate;
	private String todate;
	private boolean alldate;
	private String branchfilter;
	private String custtypeFilter;
	private String townshipFilter;
	private String villageFilter;
	private String searchText;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private String promotionType;
	private String status;

	public DocumentListingDataList() {
		clearProperty();
	}

	private void clearProperty() {
		this.sessionID = "";
		this.userID = "";
		this.msgCode = "";
		this.msgDesc = "";
		this.documentData = null;

		this.searchText = "";
		this.fromdate = "";
		this.todate = "";
		this.alldate = false;
		this.branchfilter = "";
		this.totalCount = 0;
		this.currentPage = 0;
		this.pageSize = 0;
		this.custtypeFilter = "";
		this.townshipFilter = "";
		this.villageFilter = "";
		this.promotionType = "";
		this.status = "";
	}


	public DocumentData[] getDocumentData() {
		return documentData;
	}

	public void setDocumentData(DocumentData[] documentData) {
		this.documentData = documentData;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public String getFromdate() {
		return fromdate;
	}

	public void setFromdate(String fromdate) {
		this.fromdate = fromdate;
	}

	public String getTodate() {
		return todate;
	}

	public void setTodate(String todate) {
		this.todate = todate;
	}

	public boolean isAlldate() {
		return alldate;
	}

	public void setAlldate(boolean alldate) {
		this.alldate = alldate;
	}

	public String getBranchfilter() {
		return branchfilter;
	}

	public void setBranchfilter(String branchfilter) {
		this.branchfilter = branchfilter;
	}

	public String getCusttypeFilter() {
		return custtypeFilter;
	}

	public void setCusttypeFilter(String custtypeFilter) {
		this.custtypeFilter = custtypeFilter;
	}

	public String getTownshipFilter() {
		return townshipFilter;
	}

	public void setTownshipFilter(String townshipFilter) {
		this.townshipFilter = townshipFilter;
	}

	public String getVillageFilter() {
		return villageFilter;
	}

	public void setVillageFilter(String villageFilter) {
		this.villageFilter = villageFilter;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getPromotionType() {
		return promotionType;
	}

	public void setPromotionType(String promotionType) {
		this.promotionType = promotionType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
