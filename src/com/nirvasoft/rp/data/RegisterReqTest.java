package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RegisterReqTest {

	private String name;
	private String nrc;

	public RegisterReqTest() {
		clearProperty();
	}

	private void clearProperty() {
		this.name = "";
		this.nrc = "";
	}

	public String getName() {
		return name;
	}

	public String getNrc() {
		return nrc;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNrc(String nrc) {
		this.nrc = nrc;
	}

}
