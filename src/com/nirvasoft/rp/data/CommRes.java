package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CommRes {
	private String code;
	private String desc;
	private double commAmount;
	private String type;
	private double commessionAmt1;
	private double commessionAmt2;

	public CommRes() {
		clearProperty();
	}

	private void clearProperty() {
		code = "";
		desc = "";
		commAmount = 0.00;
		type = "";
		commessionAmt1 = 0.00;
		commessionAmt2 = 0.00;
	}

	public double getCommessionAmt1() {
		return commessionAmt1;
	}

	public void setCommessionAmt1(double commessionAmt1) {
		this.commessionAmt1 = commessionAmt1;
	}

	public double getCommessionAmt2() {
		return commessionAmt2;
	}

	public void setCommessionAmt2(double commessionAmt2) {
		this.commessionAmt2 = commessionAmt2;
	}

	public String getCode() {
		return code;
	}

	public double getCommAmount() {
		return commAmount;
	}

	public String getDesc() {
		return desc;
	}

	public String getType() {
		return type;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCommAmount(double commAmount) {
		this.commAmount = commAmount;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setType(String type) {
		this.type = type;
	}

}
