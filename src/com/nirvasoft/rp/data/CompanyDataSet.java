package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CompanyDataSet {
	private CompanyData[] data;

	public CompanyDataSet() {
		super();
		data = new CompanyData[0];
	}

	public CompanyData[] getData() {
		return data;
	}

	public void setData(CompanyData[] data) {
		this.data = data;
	}

}
