package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class V_AssetData extends AssetData {

	private String brand;

	private String company;
	private String currency;

	public V_AssetData() {
		clearProperties();
	}

	@Override
	public void clearProperties() {
		super.clearProperties();
		this.brand = "";
		this.company = "";
		this.currency = "";
	}

	public String getBrand() {
		return brand;
	}

	public String getCompany() {
		return company;
	}

	public String getCurrency() {
		return currency;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

}
