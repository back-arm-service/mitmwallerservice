package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CommRateDetailData {

	private Long syskey;
	private Long hkey;
	private String commRef;
	private int zone;
	private int commType;
	private double fromAmt;
	private double toAmt;
	private double amount;
	private double minAmt;
	private double maxAmt;
	private String gLorAC;

	public CommRateDetailData() {

		clearProperty();
	}

	private void clearProperty() {
		syskey = 0L;
		hkey = 0L;
		commRef = "";
		zone = 0;
		commType = 0;
		fromAmt = 0.0;
		toAmt = 0.0;
		amount = 0.0;
		gLorAC = "";
		minAmt = 0.0;
		maxAmt = 0.0;
	}

	public double getAmount() {
		return amount;
	}

	public String getCommRef() {
		return commRef;
	}

	public int getCommType() {
		return commType;
	}

	public double getFromAmt() {
		return fromAmt;
	}

	public String getgLorAC() {
		return gLorAC;
	}

	public Long getHkey() {
		return hkey;
	}

	public double getMaxAmt() {
		return maxAmt;
	}

	public double getMinAmt() {
		return minAmt;
	}

	public Long getSyskey() {
		return syskey;
	}

	public double getToAmt() {
		return toAmt;
	}

	public int getZone() {
		return zone;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void setCommRef(String commRef) {
		this.commRef = commRef;
	}

	public void setCommType(int commType) {
		this.commType = commType;
	}

	public void setFromAmt(double fromAmt) {
		this.fromAmt = fromAmt;
	}

	public void setgLorAC(String gLorAC) {
		this.gLorAC = gLorAC;
	}

	public void setHkey(Long hkey) {
		this.hkey = hkey;
	}

	public void setMaxAmt(double maxAmt) {
		this.maxAmt = maxAmt;
	}

	public void setMinAmt(double minAmt) {
		this.minAmt = minAmt;
	}

	public void setSyskey(Long syskey) {
		this.syskey = syskey;
	}

	public void setToAmt(double toAmt) {
		this.toAmt = toAmt;
	}

	public void setZone(int zone) {
		this.zone = zone;
	}

}
