package com.nirvasoft.rp.data;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class QRDataResponse {
	private String code;
	private String desc;
	private QRData[] data;
	private String statusCode;

	public QRDataResponse() {
		clearProperty();
	}

	public void clearProperty() {
		code = "";
		desc = "";
		data = null;
		statusCode = "";
	}

	public String getCode() {
		return code;
	}

	public QRData[] getData() {
		return data;
	}

	public String getDesc() {
		return desc;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setData(QRData[] data) {
		this.data = data;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	@Override
	public String toString() {
		return "QRDataResponse [code=" + code + ", desc=" + desc + ", data=" + Arrays.toString(data) + ", statusCode="
				+ statusCode + "]";
	}

}
