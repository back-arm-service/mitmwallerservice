package com.nirvasoft.rp.data;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserInfoResponse {

	private String messageCode;
	private String messageDesc;
	private List<RegisterResTest> list;

	public UserInfoResponse() {
		clearProperty();
	}

	private void clearProperty() {
		this.messageCode = "";
		this.messageDesc = "";
		this.list = null;
	}

	public List<RegisterResTest> getList() {
		return list;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public String getMessageDesc() {
		return messageDesc;
	}

	public void setList(List<RegisterResTest> list) {
		this.list = list;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public void setMessageDesc(String messageDesc) {
		this.messageDesc = messageDesc;
	}

}
