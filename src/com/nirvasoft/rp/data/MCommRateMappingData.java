package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MCommRateMappingData {

	private long syskey;
	private String createdDate;
	private String modifiedDate;
	private String merchantID;
	private String merchantName;

	private int kindOfComGiver;
	private String productCode;
	private String gL;
	private String commRef1;
	private String commRef2;
	private String commRef3;
	private String commRef4;
	private String commRef5;
	private String commRef6;
	private int recordStatus;

	private CommRateHeaderData commObj1;
	private CommRateHeaderData commObj2;
	private CommRateHeaderData commObj3;
	private CommRateHeaderData commObj4;
	private CommRateHeaderData commObj5;

	private String code;
	private String desc;

	private String penaltyDays;

	public MCommRateMappingData() {
		clearProperty();
	}

	private void clearProperty() {
		syskey = 0L;
		createdDate = "";
		modifiedDate = "";
		merchantID = "";
		kindOfComGiver = -1;
		productCode = "";
		gL = "";
		commRef1 = "";
		commRef2 = "";
		commRef3 = "";
		commRef4 = "";
		commRef5 = "";
		commRef6 = "";
		recordStatus = 0;
		merchantName = "";
		code = "";
		desc = "";
		penaltyDays = "0";
		commObj1 = new CommRateHeaderData();
		commObj2 = new CommRateHeaderData();
		commObj3 = new CommRateHeaderData();
		commObj4 = new CommRateHeaderData();
		commObj5 = new CommRateHeaderData();
	}

	public String getCode() {
		return code;
	}

	public CommRateHeaderData getCommObj1() {
		return commObj1;
	}

	public CommRateHeaderData getCommObj2() {
		return commObj2;
	}

	public CommRateHeaderData getCommObj3() {
		return commObj3;
	}

	public CommRateHeaderData getCommObj4() {
		return commObj4;
	}

	public CommRateHeaderData getCommObj5() {
		return commObj5;
	}

	public String getCommRef1() {
		return commRef1;
	}

	public String getCommRef2() {
		return commRef2;
	}

	public String getCommRef3() {
		return commRef3;
	}

	public String getCommRef4() {
		return commRef4;
	}

	public String getCommRef5() {
		return commRef5;
	}

	public String getCommRef6() {
		return commRef6;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public String getDesc() {
		return desc;
	}

	public String getgL() {
		return gL;
	}

	public int getKindOfComGiver() {
		return kindOfComGiver;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public String getPenaltyDays() {
		return penaltyDays;
	}

	public String getProductCode() {
		return productCode;
	}

	public int getRecordStatus() {
		return recordStatus;
	}

	public long getSyskey() {
		return syskey;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCommObj1(CommRateHeaderData commObj1) {
		this.commObj1 = commObj1;
	}

	public void setCommObj2(CommRateHeaderData commObj2) {
		this.commObj2 = commObj2;
	}

	public void setCommObj3(CommRateHeaderData commObj3) {
		this.commObj3 = commObj3;
	}

	public void setCommObj4(CommRateHeaderData commObj4) {
		this.commObj4 = commObj4;
	}

	public void setCommObj5(CommRateHeaderData commObj5) {
		this.commObj5 = commObj5;
	}

	public void setCommRef1(String commRef1) {
		this.commRef1 = commRef1;
	}

	public void setCommRef2(String commRef2) {
		this.commRef2 = commRef2;
	}

	public void setCommRef3(String commRef3) {
		this.commRef3 = commRef3;
	}

	public void setCommRef4(String commRef4) {
		this.commRef4 = commRef4;
	}

	public void setCommRef5(String commRef5) {
		this.commRef5 = commRef5;
	}

	public void setCommRef6(String commRef6) {
		this.commRef6 = commRef6;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setgL(String gL) {
		this.gL = gL;
	}

	public void setKindOfComGiver(int kindOfComGiver) {
		this.kindOfComGiver = kindOfComGiver;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public void setPenaltyDays(String penaltyDays) {
		this.penaltyDays = penaltyDays;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

}
