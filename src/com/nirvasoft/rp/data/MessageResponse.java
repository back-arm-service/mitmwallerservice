package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MessageResponse {
	private String rKey;
	private String code;
	private String desc;
	private String sKey;
	private String otpCode;
	private String otpMessage;
	private String phone;

	public MessageResponse() {
		clearProperty();
	}

	public void clearProperty() {
		this.rKey = "";
		this.code = "";
		this.desc = "";
		this.sKey = "";
		this.otpCode = "";
		this.otpMessage = "";
		this.phone = "";
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public String getOtpCode() {
		return otpCode;
	}

	public String getOtpMessage() {
		return otpMessage;
	}

	public String getPhone() {
		return phone;
	}

	public String getrKey() {
		return rKey;
	}

	public String getsKey() {
		return sKey;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setOtpCode(String otpCode) {
		this.otpCode = otpCode;
	}

	public void setOtpMessage(String otpMessage) {
		this.otpMessage = otpMessage;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setrKey(String rKey) {
		this.rKey = rKey;
	}

	public void setsKey(String sKey) {
		this.sKey = sKey;
	}

	@Override
	public String toString() {
		return "MessageResponse [rKey=" + rKey + ", code=" + code + ", desc=" + desc + ", sKey=" + sKey + ", otpCode="
				+ otpCode + ", otpMessage=" + otpMessage + ", phone=" + phone + "]";
	}

}
