package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class TicketData1 {

	private String code;
	private String desc;
	private boolean state;
	private String sessionID;
	private long autokey;
	private long syskey;
	private String createdDate;
	private String modifiedDate;
	private String userID;
	private String date;
	private String time;
	private String shootDate;
	private String shootTime;
	private String T1;
	private String T2;
	private String T3;
	private String T4;
	private String T5;
	private String T6;
	private String T7;
	private String T8;
	private String T9;
	private String T10;
	private String T11;
	private String T12;
	private String T13;
	private String T14;
	private String T15;
	private String T16;
	private String T17;
	private String T18;
	private String T19;
	private String T20;
	private long n1;
	private int n2;
	private long n3;
	private long n4;
	private long n5;
	private long n6;
	private long n7;
	private long n8;
	private long n9;
	private long n10;

	private int srno;

	public TicketData1() {
		clearProperties();
	}

	public void clearProperties() {
		this.T1 = "";
		this.T2 = "";
		this.T3 = "";
		this.T4 = "";
		this.T5 = "";
		this.T6 = "";
		this.T7 = "";
		this.T8 = "";
		this.T9 = "";
		this.T10 = "";
		this.T11 = "";
		this.T12 = "";
		this.T13 = "";
		this.T14 = "";
		this.T15 = "";
		this.T16 = "";
		this.T17 = "";
		this.T18 = "";
		this.T19 = "";
		this.T20 = "";
		this.n1 = 0;
		this.n2 = 0;
		this.n3 = 0;
		this.n4 = 0;
		this.n5 = 0;
		this.n6 = 0;
		this.n7 = 0;
		this.n8 = 0;
		this.n9 = 0;
		this.n10 = 0;
		this.createdDate = "";
		this.modifiedDate = "";
		this.shootDate = "";
		this.shootTime = "";
		this.userID = "";
	}

	public long getAutokey() {
		return autokey;
	}

	public String getCode() {
		return code;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public String getDate() {
		return date;
	}

	public String getDesc() {
		return desc;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public long getN1() {
		return n1;
	}

	public long getN10() {
		return n10;
	}

	public int getN2() {
		return n2;
	}

	public long getN3() {
		return n3;
	}

	public long getN4() {
		return n4;
	}

	public long getN5() {
		return n5;
	}

	public long getN6() {
		return n6;
	}

	public long getN7() {
		return n7;
	}

	public long getN8() {
		return n8;
	}

	public long getN9() {
		return n9;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getShootDate() {
		return shootDate;
	}

	public String getShootTime() {
		return shootTime;
	}

	public int getSrno() {
		return srno;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getT1() {
		return T1;
	}

	public String getT10() {
		return T10;
	}

	public String getT11() {
		return T11;
	}

	public String getT12() {
		return T12;
	}

	public String getT13() {
		return T13;
	}

	public String getT14() {
		return T14;
	}

	public String getT15() {
		return T15;
	}

	public String getT16() {
		return T16;
	}

	public String getT17() {
		return T17;
	}

	public String getT18() {
		return T18;
	}

	public String getT19() {
		return T19;
	}

	public String getT2() {
		return T2;
	}

	public String getT20() {
		return T20;
	}

	public String getT3() {
		return T3;
	}

	public String getT4() {
		return T4;
	}

	public String getT5() {
		return T5;
	}

	public String getT6() {
		return T6;
	}

	public String getT7() {
		return T7;
	}

	public String getT8() {
		return T8;
	}

	public String getT9() {
		return T9;
	}

	public String getTime() {
		return time;
	}

	public String getUserID() {
		return userID;
	}

	public boolean isState() {
		return state;
	}

	public void setAutokey(long autokey) {
		this.autokey = autokey;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public void setN1(long n1) {
		this.n1 = n1;
	}

	public void setN10(long n10) {
		this.n10 = n10;
	}

	public void setN2(int n2) {
		this.n2 = n2;
	}

	public void setN3(long n3) {
		this.n3 = n3;
	}

	public void setN4(long n4) {
		this.n4 = n4;
	}

	public void setN5(long n5) {
		this.n5 = n5;
	}

	public void setN6(long n6) {
		this.n6 = n6;
	}

	public void setN7(long n7) {
		this.n7 = n7;
	}

	public void setN8(long n8) {
		this.n8 = n8;
	}

	public void setN9(long n9) {
		this.n9 = n9;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setShootDate(String shootDate) {
		this.shootDate = shootDate;
	}

	public void setShootTime(String shootTime) {
		this.shootTime = shootTime;
	}

	public void setSrno(int srno) {
		this.srno = srno;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setT1(String t1) {
		T1 = t1;
	}

	public void setT10(String t10) {
		T10 = t10;
	}

	public void setT11(String t11) {
		T11 = t11;
	}

	public void setT12(String t12) {
		T12 = t12;
	}

	public void setT13(String t13) {
		T13 = t13;
	}

	public void setT14(String t14) {
		T14 = t14;
	}

	public void setT15(String t15) {
		T15 = t15;
	}

	public void setT16(String t16) {
		T16 = t16;
	}

	public void setT17(String t17) {
		T17 = t17;
	}

	public void setT18(String t18) {
		T18 = t18;
	}

	public void setT19(String t19) {
		T19 = t19;
	}

	public void setT2(String t2) {
		T2 = t2;
	}

	public void setT20(String t20) {
		T20 = t20;
	}

	public void setT3(String t3) {
		T3 = t3;
	}

	public void setT4(String t4) {
		T4 = t4;
	}

	public void setT5(String t5) {
		T5 = t5;
	}

	public void setT6(String t6) {
		T6 = t6;
	}

	public void setT7(String t7) {
		T7 = t7;
	}

	public void setT8(String t8) {
		T8 = t8;
	}

	public void setT9(String t9) {
		T9 = t9;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}
}
