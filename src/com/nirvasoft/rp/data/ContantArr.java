package com.nirvasoft.rp.data;

public class ContantArr {
	private ContactData[] contactArr;
	private String msgcode;
	private String msgdesc;

	public ContantArr() {
		clearProperties();
	}

	public void clearProperties() {
		contactArr = null;
	}

	public ContactData[] getContactArr() {
		return contactArr;
	}

	public String getMsgcode() {
		return msgcode;
	}

	public String getMsgdesc() {
		return msgdesc;
	}

	public void setContactArr(ContactData[] contactArr) {
		this.contactArr = contactArr;
	}

	public void setMsgcode(String msgcode) {
		this.msgcode = msgcode;
	}

	public void setMsgdesc(String msgdesc) {
		this.msgdesc = msgdesc;
	}

}
