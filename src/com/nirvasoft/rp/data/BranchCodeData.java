package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author MIT
 *
 */
/**
 * @author MIT
 *
 */
@XmlRootElement
public class BranchCodeData {

	private int startBrCode;
	private int endBrCode;

	public BranchCodeData() {
		clearProperties();
	}

	public void clearProperties() {
		this.startBrCode = 0;
		this.endBrCode = 0;
	}

	public int getEndBrCode() {
		return endBrCode;
	}

	public int getStartBrCode() {
		return startBrCode;
	}

	public void setEndBrCode(int endBrCode) {
		this.endBrCode = endBrCode;
	}

	public void setStartBrCode(int startBrCode) {
		this.startBrCode = startBrCode;
	}

	@Override
	public String toString() {
		return "BranchCodeData [startBrCode=" + startBrCode + ", endBrCode=" + endBrCode + "]";
	}

}
