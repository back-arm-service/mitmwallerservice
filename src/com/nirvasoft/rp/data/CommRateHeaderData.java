package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CommRateHeaderData {

	private long syskey;
	private String createdDate;
	private String modifiedDate;
	private String commRef;
	private String description;
	private int mainType;
	private int recordStatus;
	private String msgCode;
	private String msgDesc;
	private boolean state;

	private CommRateDetailData[] commdetailArr;

	public CommRateHeaderData() {

		clearProperty();
	}

	private void clearProperty() {
		syskey = 0L;
		createdDate = "";
		modifiedDate = "";
		commRef = "";
		description = "";
		mainType = 0;
		recordStatus = 0;
		commdetailArr = null;
		msgCode = "";
		msgDesc = "";
		state = false;
	}

	public CommRateDetailData[] getCommdetailArr() {
		return commdetailArr;
	}

	public String getCommRef() {
		return commRef;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public String getDescription() {
		return description;
	}

	public int getMainType() {
		return mainType;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public int getRecordStatus() {
		return recordStatus;
	}

	public boolean getState() {
		return state;
	}

	public long getSyskey() {
		return syskey;
	}

	public void setCommdetailArr(CommRateDetailData[] commdetailArr) {
		this.commdetailArr = commdetailArr;
	}

	public void setCommRef(String commRef) {
		this.commRef = commRef;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setMainType(int mainType) {
		this.mainType = mainType;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

}
