package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AgentData {

	private long syskey;
	private long id;

	private String accountNumber;
	private String parentID;
	private String sessionID;

	private String userID;
	private String t1;
	private String t2;
	private String t3;
	private String t4;
	private String t5;
	private int n1;
	private int n2;
	private int n3;
	private int n4;
	private int n5;

	private String code;
	private String desc;

	public AgentData() {
		clearProperty();
	}

	private void clearProperty() {
		syskey = 0L;
		code = "";
		desc = "";
		accountNumber = "";
		parentID = "";
		userID = "";
		sessionID = "";

	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public long getId() {
		return id;
	}

	public int getN1() {
		return n1;
	}

	public int getN2() {
		return n2;
	}

	public int getN3() {
		return n3;
	}

	public int getN4() {
		return n4;
	}

	public int getN5() {
		return n5;
	}

	public String getParentID() {
		return parentID;
	}

	public String getSessionID() {
		return sessionID;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getT1() {
		return t1;
	}

	public String getT2() {
		return t2;
	}

	public String getT3() {
		return t3;
	}

	public String getT4() {
		return t4;
	}

	public String getT5() {
		return t5;
	}

	public String getUserID() {
		return userID;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public void setN2(int n2) {
		this.n2 = n2;
	}

	public void setN3(int n3) {
		this.n3 = n3;
	}

	public void setN4(int n4) {
		this.n4 = n4;
	}

	public void setN5(int n5) {
		this.n5 = n5;
	}

	public void setParentID(String parentID) {
		this.parentID = parentID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public void setT4(String t4) {
		this.t4 = t4;
	}

	public void setT5(String t5) {
		this.t5 = t5;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

}
