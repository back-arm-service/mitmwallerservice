package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RegisterResTest {

	private long syskey;
	private String policyno;
	private String typeofcover;
	private String address;
	private String stateregion;

	public RegisterResTest() {
		clearProperty();
	}

	private void clearProperty() {
		this.syskey = 0;
		this.policyno = "";
		this.typeofcover = "";
		this.address = "";
		this.stateregion = "";
	}

	public String getAddress() {
		return address;
	}

	public String getPolicyno() {
		return policyno;
	}

	public String getStateregion() {
		return stateregion;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getTypeofcover() {
		return typeofcover;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setPolicyno(String policyno) {
		this.policyno = policyno;
	}

	public void setStateregion(String stateregion) {
		this.stateregion = stateregion;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setTypeofcover(String typeofcover) {
		this.typeofcover = typeofcover;
	}

}
