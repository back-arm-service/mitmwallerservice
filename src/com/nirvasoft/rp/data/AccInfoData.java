package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author MIT
 *
 */
/**
 * @author MIT
 *
 */
@XmlRootElement
public class AccInfoData {

	private String srno;
	private String cif;
	private String name;
	private String nrc;
	private String account;
	private String ccy;
	private Boolean check;
	private int isvisible;

	public AccInfoData() {
		clearProperties();
	}

	public void clearProperties() {
		this.srno = "";
		this.cif = "";
		this.name = "";
		this.nrc = "";
		this.account = "";
		this.ccy = "";
		this.isvisible = 0;
		this.check = false;
	}

	public String getAccount() {
		return account;
	}

	public String getCcy() {
		return ccy;
	}

	public Boolean getCheck() {
		return check;
	}

	public String getCif() {
		return cif;
	}

	public int getIsvisible() {
		return isvisible;
	}

	public String getName() {
		return name;
	}

	public String getNrc() {
		return nrc;
	}

	public String getSrno() {
		return srno;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public void setCheck(Boolean check) {
		this.check = check;
	}

	public void setCif(String cif) {
		this.cif = cif;
	}

	public void setIsvisible(int isvisible) {
		this.isvisible = isvisible;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNrc(String nrc) {
		this.nrc = nrc;
	}

	public void setSrno(String srno) {
		this.srno = srno;
	}

	@Override
	public String toString() {
		return "AccInfoData [srno=" + srno + ", cif=" + cif + ", name=" + name + ", nrc=" + nrc + ", account=" + account
				+ ", ccy=" + ccy + ", check=" + check + ", isvisible=" + isvisible + "]";
	}

}
