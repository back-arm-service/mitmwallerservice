package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ChannelKeyRequest {

	private String syskeyOne;
	private String syskeyTwo;
	private String phoneOne;
	private String phoneTwo;
	private String channelkey;

	public ChannelKeyRequest() {
		clearProperty();
	}

	private void clearProperty() {
		this.syskeyOne = "";
		this.syskeyTwo = "";
		this.phoneOne = "";
		this.phoneTwo = "";
		this.channelkey = "";
	}

	public String getChannelkey() {
		return channelkey;
	}

	public String getPhoneOne() {
		return phoneOne;
	}

	public String getPhoneTwo() {
		return phoneTwo;
	}

	public String getSyskeyOne() {
		return syskeyOne;
	}

	public String getSyskeyTwo() {
		return syskeyTwo;
	}

	public void setChannelkey(String channelkey) {
		this.channelkey = channelkey;
	}

	public void setPhoneOne(String phoneOne) {
		this.phoneOne = phoneOne;
	}

	public void setPhoneTwo(String phoneTwo) {
		this.phoneTwo = phoneTwo;
	}

	public void setSyskeyOne(String syskeyOne) {
		this.syskeyOne = syskeyOne;
	}

	public void setSyskeyTwo(String syskeyTwo) {
		this.syskeyTwo = syskeyTwo;
	}

}
