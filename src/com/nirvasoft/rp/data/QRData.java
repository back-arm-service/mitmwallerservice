package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class QRData {

	private String userID;
	private String sessionID;
	private String data;
	private String billId;
	private String refNo;
	private String cusName;
	private String billAmount;
	private String ccyCode;
	private String deptAcc;
	private String dueDate;
	private String deptName;
	private String taxDesc;
	private String t1;
	private String t2;
	private String vendorCode;
	private String code;
	private String desc;
	private String bankCharges;
	private String totalAmount;
	private String penalty;
	private String belatedDays;
	private String penaltyAccount;
	private String statusCode;

	public QRData() {
		clearProperty();
	}

	public void clearProperty() {
		userID = "";
		sessionID = "";
		data = "";
		billId = "";
		refNo = "";
		cusName = "";
		billAmount = "";
		ccyCode = "";
		deptAcc = "";
		dueDate = "";
		deptName = "";
		taxDesc = "";
		t1 = "";
		t2 = "";
		vendorCode = "";
		code = "";
		desc = "";
		bankCharges = "";
		totalAmount = "";
		penalty = "";
		belatedDays = "";
		penaltyAccount = "";
		statusCode = "";
	}
	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	public String getBankCharges() {
		return bankCharges;
	}

	public String getBillAmount() {
		return billAmount;
	}

	public String getBillId() {
		return billId;
	}

	public String getCcyCode() {
		return ccyCode;
	}

	public String getCode() {
		return code;
	}

	public String getCusName() {
		return cusName;
	}

	public String getData() {
		return data;
	}

	public String getDeptAcc() {
		return deptAcc;
	}

	public String getDeptName() {
		return deptName;
	}

	public String getDesc() {
		return desc;
	}

	public String getDueDate() {
		return dueDate;
	}

	public String getRefNo() {
		return refNo;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getT1() {
		return t1;
	}

	public String getT2() {
		return t2;
	}

	public String getTaxDesc() {
		return taxDesc;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public String getUserID() {
		return userID;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setBankCharges(String bankCharges) {
		this.bankCharges = bankCharges;
	}

	public void setBillAmount(String billAmount) {
		this.billAmount = billAmount;
	}

	public void setBillId(String billId) {
		this.billId = billId;
	}

	public void setCcyCode(String ccyCode) {
		this.ccyCode = ccyCode;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public void setData(String data) {
		this.data = data;
	}

	public void setDeptAcc(String deptAcc) {
		this.deptAcc = deptAcc;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setTaxDesc(String taxDesc) {
		this.taxDesc = taxDesc;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getPenalty() {
		return penalty;
	}

	public void setPenalty(String penalty) {
		this.penalty = penalty;
	}

	public String getBelatedDays() {
		return belatedDays;
	}

	public void setBelatedDays(String belatedDays) {
		this.belatedDays = belatedDays;
	}

	public String getPenaltyAccount() {
		return penaltyAccount;
	}

	public void setPenaltyAccount(String penaltyAccount) {
		this.penaltyAccount = penaltyAccount;
	}

	@Override
	public String toString() {
		return "QRData [userID=" + userID + ", sessionID=" + sessionID + ", data=" + data + ", billId=" + billId
				+ ", refNo=" + refNo + ", cusName=" + cusName + ", billAmount=" + billAmount + ", ccyCode=" + ccyCode
				+ ", deptAcc=" + deptAcc + ", dueDate=" + dueDate + ", deptName=" + deptName + ", taxDesc=" + taxDesc
				+ ", t1=" + t1 + ", t2=" + t2 + ", vendorCode=" + vendorCode + ", code=" + code + ", desc=" + desc
				+ ", bankCharges=" + bankCharges + ", totalAmount=" + totalAmount + ", penalty=" + penalty
				+ ", belatedDays=" + belatedDays + ", penaltyAccount=" + penaltyAccount + ", statusCode=" + statusCode
				+ "]";
	}
}
