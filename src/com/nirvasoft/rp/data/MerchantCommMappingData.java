package com.nirvasoft.rp.data;

public class MerchantCommMappingData {
	private long syskey;
	private String createddate;
	private String modifieddate;
	private int recordstatus;
	private String merchantId;
	private String merchantName;
	private int commIssuer;
	private String advance;
	private String charges1;
	private String charges2;
	private String charges3;
	private String charges4;
	private String charges5;
	private int n1;
	private String t1;
	private String userId;
	private String sessionId;
	private String msgCode;
	private String msgDesc;

	public MerchantCommMappingData() {
		clearProperties();
	}

	private void clearProperties() {
		this.syskey = 0;
		this.createddate = "";
		this.modifieddate = "";
		this.merchantId = "";
		this.commIssuer = 0;
		this.advance = "";
		this.charges1 = "";
		this.charges2 = "";
		this.charges3 = "";
		this.charges4 = "";
		this.charges5 = "";
		this.n1 = 0;
		this.t1 = "";
		this.userId = "";
		this.sessionId = "";
		this.msgCode = "";
		this.msgDesc = "";
		this.merchantName = "";
	}

	public String getAdvance() {
		return advance;
	}

	public String getCharges1() {
		return charges1;
	}

	public String getCharges2() {
		return charges2;
	}

	public String getCharges3() {
		return charges3;
	}

	public String getCharges4() {
		return charges4;
	}

	public String getCharges5() {
		return charges5;
	}

	public int getCommIssuer() {
		return commIssuer;
	}

	public String getCreateddate() {
		return createddate;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public String getModifieddate() {
		return modifieddate;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public int getN1() {
		return n1;
	}

	public int getRecordstatus() {
		return recordstatus;
	}

	public String getSessionId() {
		return sessionId;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getT1() {
		return t1;
	}

	public String getUserId() {
		return userId;
	}

	public void setAdvance(String advance) {
		this.advance = advance;
	}

	public void setCharges1(String charges1) {
		this.charges1 = charges1;
	}

	public void setCharges2(String charges2) {
		this.charges2 = charges2;
	}

	public void setCharges3(String charges3) {
		this.charges3 = charges3;
	}

	public void setCharges4(String charges4) {
		this.charges4 = charges4;
	}

	public void setCharges5(String charges5) {
		this.charges5 = charges5;
	}

	public void setCommIssuer(int commIssuer) {
		this.commIssuer = commIssuer;
	}

	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public void setModifieddate(String modifieddate) {
		this.modifieddate = modifieddate;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public void setRecordstatus(int recordstatus) {
		this.recordstatus = recordstatus;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "{syskey=" + syskey + ", createddate=" + createddate + ", modifieddate=" + modifieddate
				+ ", recordstatus=" + recordstatus + ", merchantId=" + merchantId + ", commIssuer=" + commIssuer
				+ ", advance=" + advance + ", charges1=" + charges1 + ", charges2=" + charges2 + ", charges3="
				+ charges3 + ", charges4=" + charges4 + ", charges5=" + charges5 + ", n1=" + n1 + ", t1=" + t1
				+ ", userId=" + userId + ", sessionId=" + sessionId + ", msgCode=" + msgCode + ", msgDesc=" + msgDesc
				+ "}";
	}

}
