package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.nirvasoft.cms.shared.ArticleData;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContentListingDataList {

	private String sessionID;
	private String userID;
	private String msgCode = "";
	private String msgDesc = "";
	// private int totalCount;
	// private int currentPage;
	// private int pageSize;
	// private String searchText;
	// private String aFromDate;
	// private String aToDate;
	// private boolean alldate;
	// private boolean allStatus;
	// private boolean newStatus;
	// private boolean wipStatus;
	// private boolean closedStatus;
	// private boolean rejectedStatus;
	private ArticleData[] contentData;

	private String fromdate;
	private String todate;
	private boolean alldate;
	private String branchfilter;
	private String custtypeFilter;
	private String townshipFilter;
	private String villageFilter;
	private String searchText;
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private String promotionType;
	private String status;

	public ContentListingDataList() {
		clearProperty();
	}

	private void clearProperty() {
		this.sessionID = "";
		this.userID = "";
		this.msgCode = "";
		this.msgDesc = "";
		this.contentData = null;

		this.searchText = "";
		this.fromdate = "";
		this.todate = "";
		this.alldate = false;
		this.branchfilter = "";
		this.totalCount = 0;
		this.currentPage = 0;
		this.pageSize = 0;
		this.custtypeFilter = "";
		this.townshipFilter = "";
		this.villageFilter = "";
		this.promotionType = "";
		this.status = "";
	}

	public String getBranchfilter() {
		return branchfilter;
	}

	public ArticleData[] getContentData() {
		return contentData;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public String getCusttypeFilter() {
		return custtypeFilter;
	}

	public String getFromdate() {
		return fromdate;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public int getPageSize() {
		return pageSize;
	}

	public String getPromotionType() {
		return promotionType;
	}

	public String getSearchText() {
		return searchText;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getStatus() {
		return status;
	}

	public String getTodate() {
		return todate;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public String getTownshipFilter() {
		return townshipFilter;
	}

	public String getUserID() {
		return userID;
	}

	public String getVillageFilter() {
		return villageFilter;
	}

	public boolean isAlldate() {
		return alldate;
	}

	public void setAlldate(boolean alldate) {
		this.alldate = alldate;
	}

	public void setBranchfilter(String branchfilter) {
		this.branchfilter = branchfilter;
	}

	public void setContentData(ArticleData[] contentData) {
		this.contentData = contentData;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setCusttypeFilter(String custtypeFilter) {
		this.custtypeFilter = custtypeFilter;
	}

	public void setFromdate(String fromdate) {
		this.fromdate = fromdate;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setPromotionType(String promotionType) {
		this.promotionType = promotionType;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setTodate(String todate) {
		this.todate = todate;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public void setTownshipFilter(String townshipFilter) {
		this.townshipFilter = townshipFilter;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public void setVillageFilter(String villageFilter) {
		this.villageFilter = villageFilter;
	}

}
