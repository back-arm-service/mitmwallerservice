package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ContactData {

	private long syskey;
	private String t1;
	private String t3;
	private String t16;

	public ContactData() {
		clearProperties();
	}

	private void clearProperties() {

		syskey = 0;
		t1 = "";
		t3 = "";
		t16 = "";
	}

	public long getSyskey() {
		return syskey;
	}

	public String getT1() {
		return t1;
	}

	public String getT16() {
		return t16;
	}

	public String getT3() {
		return t3;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT16(String t16) {
		this.t16 = t16;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

}
