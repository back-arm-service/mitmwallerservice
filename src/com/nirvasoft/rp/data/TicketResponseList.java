package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class TicketResponseList {

	private String sessionID;
	private String userID;
	private String msgCode = "";
	private String msgDesc = "";
	private TicketResponse[] ticketList;

	public TicketResponseList() {
		clearProperty();
	}

	private void clearProperty() {
		sessionID = "";
		userID = "";
		msgCode = "";
		msgDesc = "";
		ticketList = null;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public String getSessionID() {
		return sessionID;
	}

	public TicketResponse[] getTicketList() {
		return ticketList;
	}

	public String getUserID() {
		return userID;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setTicketList(TicketResponse[] ticketList) {
		this.ticketList = ticketList;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

}
