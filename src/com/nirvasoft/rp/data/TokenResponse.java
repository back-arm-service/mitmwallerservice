package com.nirvasoft.rp.data;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

import com.nirvasoft.rp.framework.ResultTwo;
import com.nirvasoft.rp.shared.Advertise;
@XmlRootElement
public class TokenResponse {
	private String token;
	private String msgCode;
	private String msgDesc;
	
	public TokenResponse() {
		clearProperties();
	}

	private void clearProperties() {
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}
	
}
