package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CityStateComboDataSet {

	private CityStateData[] data;

	public CityStateComboDataSet() {
		super();
		data = new CityStateData[0];

	}

	public CityStateData[] getData() {
		return data;
	}

	public void setData(CityStateData[] data) {
		this.data = data;
	}

}