package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AssetDataSet {
	private AssetData[] data;

	public AssetDataSet() {
		super();
		data = new AssetData[0];
	}

	public AssetData[] getData() {
		return data;
	}

	public void setData(AssetData[] data) {
		this.data = data;
	}

}
