package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EachAccount {

	private String serialNo;// n1
	private String accNumber;
	private String accountType;// t2

	public EachAccount() {
		clearProperty();
	}

	private void clearProperty() {
		this.serialNo = "";
		this.accNumber = "";
		this.accountType = "";
	}

	public String getAccNumber() {
		return accNumber;
	}

	public String getAccountType() {
		return accountType;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setAccNumber(String accNumber) {
		this.accNumber = accNumber;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	@Override
	public String toString() {
		return "EachAccount [serialNo=" + serialNo + ", accNumber=" + accNumber + ", accountType=" + accountType + "]";
	}

}
