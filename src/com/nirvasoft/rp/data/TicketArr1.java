package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class TicketArr1 {

	private String sessionID;
	private String userID;
	private String msgCode = "";
	private String msgDesc = "";
	private int totalCount;
	private int currentPage;
	private int pageSize;
	private String searchText;
	private String aFromDate;
	private String aToDate;
	private boolean alldate;
	private boolean allStatus;
	private boolean newStatus;
	private boolean wipStatus;
	private boolean closedStatus;
	private boolean rejectedStatus;
	private TicketData1[] ticketData;

	public TicketArr1() {
		clearProperty();
	}

	private void clearProperty() {
		this.sessionID = "";
		this.userID = "";
		this.msgCode = "";
		this.msgDesc = "";
		this.ticketData = null;
	}

	public String getaFromDate() {
		return aFromDate;
	}

	public String getaToDate() {
		return aToDate;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public int getPageSize() {
		return pageSize;
	}

	public String getSearchText() {
		return searchText;
	}

	public String getSessionID() {
		return sessionID;
	}

	public TicketData1[] getTicketData() {
		return ticketData;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public String getUserID() {
		return userID;
	}

	public boolean isAlldate() {
		return alldate;
	}

	public boolean isAllStatus() {
		return allStatus;
	}

	public boolean isClosedStatus() {
		return closedStatus;
	}

	public boolean isNewStatus() {
		return newStatus;
	}

	public boolean isRejectedStatus() {
		return rejectedStatus;
	}

	public boolean isWipStatus() {
		return wipStatus;
	}

	public void setaFromDate(String aFromDate) {
		this.aFromDate = aFromDate;
	}

	public void setAlldate(boolean alldate) {
		this.alldate = alldate;
	}

	public void setAllStatus(boolean allStatus) {
		this.allStatus = allStatus;
	}

	public void setaToDate(String aToDate) {
		this.aToDate = aToDate;
	}

	public void setClosedStatus(boolean closedStatus) {
		this.closedStatus = closedStatus;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public void setNewStatus(boolean newStatus) {
		this.newStatus = newStatus;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setRejectedStatus(boolean rejectedStatus) {
		this.rejectedStatus = rejectedStatus;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setTicketData(TicketData1[] ticketData) {
		this.ticketData = ticketData;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public void setWipStatus(boolean wipStatus) {
		this.wipStatus = wipStatus;
	}

}
