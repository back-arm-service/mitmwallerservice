package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author MIT
 *
 */
/**
 * @author MIT
 *
 */
@XmlRootElement
public class FAQResponseData {
	private String questionEng;
	private String questionUni;
	private String answerEng;
	private String answerUni;
	private boolean selected;

	public FAQResponseData() {
		clearProperties();
	}

	public void clearProperties() {

		this.questionEng = "";
		this.questionUni = "";
		this.answerEng = "";
		this.answerUni = "";
		this.selected = false;
	}

	public String getAnswerEng() {
		return answerEng;
	}

	public String getAnswerUni() {
		return answerUni;
	}

	public String getQuestionEng() {
		return questionEng;
	}

	public String getQuestionUni() {
		return questionUni;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setAnswerEng(String answerEng) {
		this.answerEng = answerEng;
	}

	public void setAnswerUni(String answerUni) {
		this.answerUni = answerUni;
	}

	public void setQuestionEng(String questionEng) {
		this.questionEng = questionEng;
	}

	public void setQuestionUni(String questionUni) {
		this.questionUni = questionUni;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	@Override
	public String toString() {
		return "FAQResponseData [questionEng=" + questionEng + ", questionUni=" + questionUni + ", answerEng="
				+ answerEng + ", answerUni=" + answerUni + ", selected=" + selected + "]";
	}

}
