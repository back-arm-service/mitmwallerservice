package com.nirvasoft.rp.data;

import java.util.ArrayList;

import com.nirvasoft.rp.shared.DepositAccData;

public class CustInfoData {
	private String customerId;
	private String code;
	private String desc;
	private ArrayList<DepositAccData> accountInfoarr;

	public CustInfoData() {
		clearProperty();
	}

	void clearProperty() {
		customerId = "";
		code = "";
		desc = "";
		accountInfoarr = new ArrayList<>();
	}

	public ArrayList<DepositAccData> getAccountInfoarr() {
		return accountInfoarr;
	}

	public String getCode() {
		return code;
	}

	public String getCustomerId() {
		return customerId;
	}

	public String getDesc() {
		return desc;
	}

	public void setAccountInfoarr(ArrayList<DepositAccData> accountInfoarr) {
		this.accountInfoarr = accountInfoarr;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
