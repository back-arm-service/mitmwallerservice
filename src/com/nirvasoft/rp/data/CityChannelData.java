package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CityChannelData {
	private int isLocal;
	private String channelSyskey;
	private String channel;

	public CityChannelData() {
		clearProperties();
	}

	private void clearProperties() {
		this.channelSyskey = "";
		this.channel = "";
		this.isLocal = 0;
	}

	public String getChannel() {
		return channel;
	}

	public String getChannelSyskey() {
		return channelSyskey;
	}

	public int getIsLocal() {
		return isLocal;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public void setChannelSyskey(String channelSyskey) {
		this.channelSyskey = channelSyskey;
	}

	public void setIsLocal(int isLocal) {
		this.isLocal = isLocal;
	}

}
