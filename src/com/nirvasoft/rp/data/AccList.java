package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AccList {
	private AccInfoData[] accArr;

	public AccList() {
		clearProperties();
	}

	public void clearProperties() {

	}

	public AccInfoData[] getAccArr() {
		return accArr;
	}

	public void setAccArr(AccInfoData[] accArr) {
		this.accArr = accArr;
	}

}
