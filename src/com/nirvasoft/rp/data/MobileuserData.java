package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MobileuserData {

	// private boolean state;
	private long autokey;
	private long syskey;
	private String t1;
	private String t3;
	private String t16;
	private String t20;
	private String t21;
	private String t24;
	private String t25;
	private String t36;
	private String t38;
	private String createdDate;
	private String accNumber;

	public void clearProperties() {
		this.t1 = "";
		this.t3 = "";
		this.t16 = "";
		this.t20 = "";
		this.t21 = "";
		this.t24 = "";
		this.t25 = "";
		this.t36 = "";
		this.t38 = "";
		this.createdDate = "";
		this.accNumber = "";
	}

	public String getAccNumber() {
		return accNumber;
	}

	public long getAutokey() {
		return autokey;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public long getSyskey() {
		return syskey;
	}

	public String getT1() {
		return t1;
	}

	public String getT16() {
		return t16;
	}

	public String getT20() {
		return t20;
	}

	public String getT21() {
		return t21;
	}

	public String getT24() {
		return t24;
	}

	public String getT25() {
		return t25;
	}

	public String getT3() {
		return t3;
	}

	public String getT36() {
		return t36;
	}

	public String getT38() {
		return t38;
	}

	public void setAccNumber(String accNumber) {
		this.accNumber = accNumber;
	}

	public void setAutokey(long autokey) {
		this.autokey = autokey;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT16(String t16) {
		this.t16 = t16;
	}

	public void setT20(String t20) {
		this.t20 = t20;
	}

	public void setT21(String t21) {
		this.t21 = t21;
	}

	public void setT24(String t24) {
		this.t24 = t24;
	}

	public void setT25(String t25) {
		this.t25 = t25;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public void setT36(String t36) {
		this.t36 = t36;
	}

	public void setT38(String t38) {
		this.t38 = t38;
	}

}
