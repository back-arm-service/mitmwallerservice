package com.nirvasoft.rp.data;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

import com.nirvasoft.rp.shared.Contact;

@XmlRootElement
public class ContactListReq {
	private String userID;
	private String sessionID;
	private Contact[] data;

	public ContactListReq() {
		clearProperty();
	}

	public void clearProperty() {
		userID = "";
		sessionID = "";
		data = null;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public Contact[] getData() {
		return data;
	}

	public void setData(Contact[] data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "ContactListReq [userID=" + userID + ", sessionID=" + sessionID + ", data=" + Arrays.toString(data)
				+ "]";
	}
}
