package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MessageRequest {
	private String type;
	private String userID;
	private String sessionID;
	private String merchantID;
	private String rKey;
	private String serviceType;

	public MessageRequest() {
		clearProperty();
	}

	public void clearProperty() {
		this.type = "";
		this.userID = "";
		this.sessionID = "";
		this.merchantID = "";
		this.rKey = "";
		this.serviceType="";

	}
	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getType() {
		return type;
	}

	public String getUserID() {
		return userID;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public String getrKey() {
		return rKey;
	}

	public void setrKey(String rKey) {
		this.rKey = rKey;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	@Override
	public String toString() {
		return "MessageRequest [type=" + type + ", userID=" + userID + ", sessionID=" + sessionID + ", merchantID="
				+ merchantID + ", rKey=" + rKey + "]";
	}

}
