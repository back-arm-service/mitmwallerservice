package com.nirvasoft.rp.data;

public class ChannelDataArr {
	private ChannelData[] channeldata;
	private String msgcode;
	private String msgdesc;
	String reg;// atn

	public ChannelDataArr() {
		clearProperties();
	}

	public void clearProperties() {
		channeldata = null;
	}

	public ChannelData[] getChanneldata() {
		return channeldata;
	}

	public String getMsgcode() {
		return msgcode;
	}

	public String getMsgdesc() {
		return msgdesc;
	}

	public String getReg() {
		return reg;
	}

	public void setChanneldata(ChannelData[] channeldata) {
		this.channeldata = channeldata;
	}

	public void setMsgcode(String msgcode) {
		this.msgcode = msgcode;
	}

	public void setMsgdesc(String msgdesc) {
		this.msgdesc = msgdesc;
	}

	public void setReg(String reg) {
		this.reg = reg;
	}

}
