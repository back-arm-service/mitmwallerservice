package com.nirvasoft.rp.data;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class FeatureData {
	private long syskey;
	private String merchantID;
	private String feature;
	private int featurevalue;
	private String branchCode;
	private String accountNumber;
	private String order;
	private int orderValue;
	private String TransType;

	public FeatureData() {
		clearProperties();
	}

	private void clearProperties() {

		merchantID = "";
		feature = "";
		featurevalue = 0;
		accountNumber = "";
		branchCode = "";
		order = "";
		orderValue = 0;
		TransType = "";

	}

	public String getTransType() {
		return TransType;
	}

	public void setTransType(String transType) {
		TransType = transType;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public String getFeature() {
		return feature;
	}

	public int getFeaturevalue() {
		return featurevalue;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public String getOrder() {
		return order;
	}

	public int getOrderValue() {
		return orderValue;
	}

	public long getSyskey() {
		return syskey;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public void setFeature(String feature) {
		this.feature = feature;
	}

	public void setFeaturevalue(int featurevalue) {
		this.featurevalue = featurevalue;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public void setOrderValue(int orderValue) {
		this.orderValue = orderValue;
	}

	public void setSyskey(long syskey) {
		this.syskey = syskey;
	}

}

