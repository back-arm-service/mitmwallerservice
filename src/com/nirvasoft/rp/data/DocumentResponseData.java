package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DocumentResponseData {

	private String messagecode;
	private String messagedesc;
	private long autokey;
	private String createddate;
	private String modifieddate;
	private String filename;
	private String userid;
	private String status;
	private int recordstatus;
	private String filetype;
	private String region;
	private String filepath;
	private String postedby;
	private String modifiedby;
	private String filesize;
	private String pages;
	private String title;
	private String t1;
	private String t2;
	private String t3;
	private String t4;
	private String t5;
	private int n1;
	private int n2;
	private int n3;
	private int n4;
	private int n5;

	public DocumentResponseData() {
		clearProperty();
	}

	private void clearProperty() {
		this.messagecode = "";
		this.messagedesc = "";
		this.autokey = 0;
		this.createddate = "";
		this.modifieddate = "";
		this.filename = "";
		this.userid = "";
		this.status = "";
		this.recordstatus = 0;
		this.filetype = "";
		this.region = "";
		this.filepath = "";
		this.postedby = "";
		this.modifiedby = "";
		this.filesize = "";
		this.pages = "";
		this.title = "";
		this.t1 = "";
		this.t2 = "";
		this.t3 = "";
		this.t4 = "";
		this.t5 = "";
		this.n1 = 0;
		this.n2 = 0;
		this.n3 = 0;
		this.n4 = 0;
		this.n5 = 0;
	}

	public long getAutokey() {
		return autokey;
	}

	public String getCreateddate() {
		return createddate;
	}

	public String getFilename() {
		return filename;
	}

	public String getFilepath() {
		return filepath;
	}

	public String getFilesize() {
		return filesize;
	}

	public String getFiletype() {
		return filetype;
	}

	public String getMessagecode() {
		return messagecode;
	}

	public String getMessagedesc() {
		return messagedesc;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public String getModifieddate() {
		return modifieddate;
	}

	public int getN1() {
		return n1;
	}

	public int getN2() {
		return n2;
	}

	public int getN3() {
		return n3;
	}

	public int getN4() {
		return n4;
	}

	public int getN5() {
		return n5;
	}

	public String getPages() {
		return pages;
	}

	public String getPostedby() {
		return postedby;
	}

	public int getRecordstatus() {
		return recordstatus;
	}

	public String getRegion() {
		return region;
	}

	public String getStatus() {
		return status;
	}

	public String getT1() {
		return t1;
	}

	public String getT2() {
		return t2;
	}

	public String getT3() {
		return t3;
	}

	public String getT4() {
		return t4;
	}

	public String getT5() {
		return t5;
	}

	public String getTitle() {
		return title;
	}

	public String getUserid() {
		return userid;
	}

	public void setAutokey(long autokey) {
		this.autokey = autokey;
	}

	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public void setFilesize(String filesize) {
		this.filesize = filesize;
	}

	public void setFiletype(String filetype) {
		this.filetype = filetype;
	}

	public void setMessagecode(String messagecode) {
		this.messagecode = messagecode;
	}

	public void setMessagedesc(String messagedesc) {
		this.messagedesc = messagedesc;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	public void setModifieddate(String modifieddate) {
		this.modifieddate = modifieddate;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public void setN2(int n2) {
		this.n2 = n2;
	}

	public void setN3(int n3) {
		this.n3 = n3;
	}

	public void setN4(int n4) {
		this.n4 = n4;
	}

	public void setN5(int n5) {
		this.n5 = n5;
	}

	public void setPages(String pages) {
		this.pages = pages;
	}

	public void setPostedby(String postedby) {
		this.postedby = postedby;
	}

	public void setRecordstatus(int recordstatus) {
		this.recordstatus = recordstatus;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public void setT4(String t4) {
		this.t4 = t4;
	}

	public void setT5(String t5) {
		this.t5 = t5;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

}
