package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class V_AssetDataSet {
	private V_AssetData[] data;

	public V_AssetDataSet() {
		super();
		data = new V_AssetData[0];
	}

	public V_AssetData[] getData() {
		return data;
	}

	public void setData(V_AssetData[] data) {
		this.data = data;
	}

}
