package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class TicketResponse {

	private String sessionID;
	private String userID;
	private String msgCode;
	private String msgDesc;
	private boolean state;

	private String autoKey;
	private String syskey;
	private String createdDate;
	private String modifiedDate;
	private String code;
	private String ticketNo;
	private String senderKey;
	private String senderName;
	private String message;
	private String location;
	private String shortDate;
	private String shortTime;
	private String image;
	private String channelKey;
	private String receiverKey;
	private String t1;
	private String t2;
	private String t3;
	private String t4;
	private String t5;
	private String t6;
	private String t7;
	private String t8;
	private String t9;
	private String t10;
	private String t11;
	private String t12;
	private String t13;
	private String t14;
	private String t15;
	private String t16;
	private String t17;
	private String t18;
	private String t19;
	private String t20;
	private String n1;
	private String n2;
	private String n3;
	private String n4;
	private String n5;
	private String n6;
	private String n7;
	private String n8;
	private String n9;
	private String n10;
	private String n11;
	private String n12;
	private String n13;
	private String n14;
	private String n15;
	private String n16;
	private String n17;
	private String n18;
	private String n19;
	private String n20;

	public TicketResponse() {
		this.clearProperties();
	}

	private void clearProperties() {
		this.sessionID = "";
		this.userID = "";
		this.msgCode = "";
		this.msgDesc = "";
		this.state = false;

		this.t1 = "";
		this.t2 = "";
		this.t3 = "";
		this.t4 = "";
		this.t5 = "";
		this.t6 = "";
		this.t7 = "";
		this.t8 = "";
		this.t9 = "";
		this.t10 = "";
		this.t11 = "";
		this.t12 = "";
		this.t13 = "";
		this.t14 = "";
		this.t15 = "";
		this.t16 = "";
		this.t17 = "";
		this.t18 = "";
		this.t19 = "";
		this.t20 = "";

		this.n1 = "";
		this.n2 = "";
		this.n3 = "";
		this.n4 = "";
		this.n5 = "";
		this.n6 = "";
		this.n7 = "";
		this.n8 = "";
		this.n9 = "";
		this.n10 = "";
		this.n11 = "";
		this.n12 = "";
		this.n13 = "";
		this.n14 = "";
		this.n15 = "";
		this.n16 = "";
		this.n17 = "";
		this.n18 = "";
		this.n19 = "";
		this.n20 = "";
	}

	public String getAutoKey() {
		return autoKey;
	}

	public String getChannelKey() {
		return channelKey;
	}

	public String getCode() {
		return code;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public String getImage() {
		return image;
	}

	public String getLocation() {
		return location;
	}

	public String getMessage() {
		return message;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public String getMsgDesc() {
		return msgDesc;
	}

	public String getN1() {
		return n1;
	}

	public String getN10() {
		return n10;
	}

	public String getN11() {
		return n11;
	}

	public String getN12() {
		return n12;
	}

	public String getN13() {
		return n13;
	}

	public String getN14() {
		return n14;
	}

	public String getN15() {
		return n15;
	}

	public String getN16() {
		return n16;
	}

	public String getN17() {
		return n17;
	}

	public String getN18() {
		return n18;
	}

	public String getN19() {
		return n19;
	}

	public String getN2() {
		return n2;
	}

	public String getN20() {
		return n20;
	}

	public String getN3() {
		return n3;
	}

	public String getN4() {
		return n4;
	}

	public String getN5() {
		return n5;
	}

	public String getN6() {
		return n6;
	}

	public String getN7() {
		return n7;
	}

	public String getN8() {
		return n8;
	}

	public String getN9() {
		return n9;
	}

	public String getReceiverKey() {
		return receiverKey;
	}

	public String getSenderKey() {
		return senderKey;
	}

	public String getSenderName() {
		return senderName;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getShortDate() {
		return shortDate;
	}

	public String getShortTime() {
		return shortTime;
	}

	public String getSyskey() {
		return syskey;
	}

	public String getT1() {
		return t1;
	}

	public String getT10() {
		return t10;
	}

	public String getT11() {
		return t11;
	}

	public String getT12() {
		return t12;
	}

	public String getT13() {
		return t13;
	}

	public String getT14() {
		return t14;
	}

	public String getT15() {
		return t15;
	}

	public String getT16() {
		return t16;
	}

	public String getT17() {
		return t17;
	}

	public String getT18() {
		return t18;
	}

	public String getT19() {
		return t19;
	}

	public String getT2() {
		return t2;
	}

	public String getT20() {
		return t20;
	}

	public String getT3() {
		return t3;
	}

	public String getT4() {
		return t4;
	}

	public String getT5() {
		return t5;
	}

	public String getT6() {
		return t6;
	}

	public String getT7() {
		return t7;
	}

	public String getT8() {
		return t8;
	}

	public String getT9() {
		return t9;
	}

	public String getTicketNo() {
		return ticketNo;
	}

	public String getUserID() {
		return userID;
	}

	public boolean isState() {
		return state;
	}

	public void setAutoKey(String autoKey) {
		this.autoKey = autoKey;
	}

	public void setChannelKey(String channelKey) {
		this.channelKey = channelKey;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}

	public void setMsgDesc(String msgDesc) {
		this.msgDesc = msgDesc;
	}

	public void setN1(String n1) {
		this.n1 = n1;
	}

	public void setN10(String n10) {
		this.n10 = n10;
	}

	public void setN11(String n11) {
		this.n11 = n11;
	}

	public void setN12(String n12) {
		this.n12 = n12;
	}

	public void setN13(String n13) {
		this.n13 = n13;
	}

	public void setN14(String n14) {
		this.n14 = n14;
	}

	public void setN15(String n15) {
		this.n15 = n15;
	}

	public void setN16(String n16) {
		this.n16 = n16;
	}

	public void setN17(String n17) {
		this.n17 = n17;
	}

	public void setN18(String n18) {
		this.n18 = n18;
	}

	public void setN19(String n19) {
		this.n19 = n19;
	}

	public void setN2(String n2) {
		this.n2 = n2;
	}

	public void setN20(String n20) {
		this.n20 = n20;
	}

	public void setN3(String n3) {
		this.n3 = n3;
	}

	public void setN4(String n4) {
		this.n4 = n4;
	}

	public void setN5(String n5) {
		this.n5 = n5;
	}

	public void setN6(String n6) {
		this.n6 = n6;
	}

	public void setN7(String n7) {
		this.n7 = n7;
	}

	public void setN8(String n8) {
		this.n8 = n8;
	}

	public void setN9(String n9) {
		this.n9 = n9;
	}

	public void setReceiverKey(String receiverKey) {
		this.receiverKey = receiverKey;
	}

	public void setSenderKey(String senderKey) {
		this.senderKey = senderKey;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setShortDate(String shortDate) {
		this.shortDate = shortDate;
	}

	public void setShortTime(String shortTime) {
		this.shortTime = shortTime;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public void setSyskey(String syskey) {
		this.syskey = syskey;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT10(String t10) {
		this.t10 = t10;
	}

	public void setT11(String t11) {
		this.t11 = t11;
	}

	public void setT12(String t12) {
		this.t12 = t12;
	}

	public void setT13(String t13) {
		this.t13 = t13;
	}

	public void setT14(String t14) {
		this.t14 = t14;
	}

	public void setT15(String t15) {
		this.t15 = t15;
	}

	public void setT16(String t16) {
		this.t16 = t16;
	}

	public void setT17(String t17) {
		this.t17 = t17;
	}

	public void setT18(String t18) {
		this.t18 = t18;
	}

	public void setT19(String t19) {
		this.t19 = t19;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setT20(String t20) {
		this.t20 = t20;
	}

	public void setT3(String t3) {
		this.t3 = t3;
	}

	public void setT4(String t4) {
		this.t4 = t4;
	}

	public void setT5(String t5) {
		this.t5 = t5;
	}

	public void setT6(String t6) {
		this.t6 = t6;
	}

	public void setT7(String t7) {
		this.t7 = t7;
	}

	public void setT8(String t8) {
		this.t8 = t8;
	}

	public void setT9(String t9) {
		this.t9 = t9;
	}

	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

}
