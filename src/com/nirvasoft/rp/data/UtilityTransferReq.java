package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UtilityTransferReq {

	private String userID;
	private String sessionID;
	private String fromAccount;
	private String toAccount;
	private String merchantID;
	private String amount;
	private String bankCharges;
	private String narrative;
	private String refNo;
	private String field1;
	private String field2;
	private String sKey;
	private String billId;
	private String cusName;
	private String billAmount;
	private String ccyCode;
	private String dueDate;
	private String t1;
	private String t2;
	private String txnRef;
	private String penalty;
	private String txnDate;
	private String paidBy;
	private String vendorCode;
	private String deptName;
	private String taxDesc;
	private String totalAmount;

	public UtilityTransferReq() {
		clearProperty();
	}

	public void clearProperty() {
		userID = "";
		sessionID = "";
		fromAccount = "";
		toAccount = "";
		merchantID = "";
		amount = "0.00";
		bankCharges = "0.00";
		narrative = "";
		refNo = "";
		field1 = "";
		field2 = "";
		sKey = "";
		billId = "";
		refNo = "";
		cusName = "";
		billAmount = "";
		ccyCode = "";
		dueDate = "";
		t1 = "";
		t2 = "";
		txnRef = "";
		penalty = "";
		txnDate = "";
		paidBy = "";
		vendorCode = "";
		deptName = "";
		taxDesc = "";
		totalAmount = "";
	}

	public String getAmount() {
		return amount;
	}

	public String getBankCharges() {
		return bankCharges;
	}

	public String getBillAmount() {
		return billAmount;
	}

	public String getBillId() {
		return billId;
	}

	public String getCcyCode() {
		return ccyCode;
	}

	public String getCusName() {
		return cusName;
	}

	public String getDeptName() {
		return deptName;
	}

	public String getDueDate() {
		return dueDate;
	}

	public String getField1() {
		return field1;
	}

	public String getField2() {
		return field2;
	}

	public String getFromAccount() {
		return fromAccount;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public String getNarrative() {
		return narrative;
	}

	public String getPaidBy() {
		return paidBy;
	}

	public String getPenalty() {
		return penalty;
	}

	public String getRefNo() {
		return refNo;
	}

	public String getSessionID() {
		return sessionID;
	}

	public String getsKey() {
		return sKey;
	}

	public String getT1() {
		return t1;
	}

	public String getT2() {
		return t2;
	}

	public String getTaxDesc() {
		return taxDesc;
	}

	public String getToAccount() {
		return toAccount;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public String getTxnDate() {
		return txnDate;
	}

	public String getTxnRef() {
		return txnRef;
	}

	public String getUserID() {
		return userID;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public void setBankCharges(String bankCharges) {
		this.bankCharges = bankCharges;
	}

	public void setBillAmount(String billAmount) {
		this.billAmount = billAmount;
	}

	public void setBillId(String billId) {
		this.billId = billId;
	}

	public void setCcyCode(String ccyCode) {
		this.ccyCode = ccyCode;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public void setFromAccount(String fromAccount) {
		this.fromAccount = fromAccount;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public void setNarrative(String narrative) {
		this.narrative = narrative;
	}

	public void setPaidBy(String paidBy) {
		this.paidBy = paidBy;
	}

	public void setPenalty(String penalty) {
		this.penalty = penalty;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setsKey(String sKey) {
		this.sKey = sKey;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public void setTaxDesc(String taxDesc) {
		this.taxDesc = taxDesc;
	}

	public void setToAccount(String toAccount) {
		this.toAccount = toAccount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}

	public void setTxnRef(String txnRef) {
		this.txnRef = txnRef;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	@Override
	public String toString() {
		return "UtilityTransferReq [userID=" + userID + ", sessionID=" + sessionID + ", fromAccount=" + fromAccount
				+ ", toAccount=" + toAccount + ", merchantID=" + merchantID + ", amount=" + amount + ", bankCharges="
				+ bankCharges + ", narrative=" + narrative + ", refNo=" + refNo + ", field1=" + field1 + ", field2="
				+ field2 + ", sKey=" + sKey + ", billId=" + billId + ", cusName=" + cusName + ", billAmount="
				+ billAmount + ", ccyCode=" + ccyCode + ", dueDate=" + dueDate + ", t1=" + t1 + ", t2=" + t2
				+ ", txnRef=" + txnRef + ", penalty=" + penalty + ", txnDate=" + txnDate + ", paidBy=" + paidBy
				+ ", vendorCode=" + vendorCode + ", deptName=" + deptName + ", taxDesc=" + taxDesc + ", totalAmount="
				+ totalAmount + "]";
	}

}
