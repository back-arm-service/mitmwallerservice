package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BrandDataSet {

	private BrandData[] data;

	public BrandDataSet() {
		super();
		data = new BrandData[0];

	}

	public BrandData[] getData() {
		return data;
	}

	public void setData(BrandData[] data) {
		this.data = data;
	}

}