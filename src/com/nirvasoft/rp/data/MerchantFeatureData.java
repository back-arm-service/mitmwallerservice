package com.nirvasoft.rp.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MerchantFeatureData {

	private String autoKey;
	private String merchantID;
	private String feature;
	private String accountNumber;
	private String branchCode;
	private String t1;
	private String t2;
	private String n1;
	private String n2;

	public MerchantFeatureData() {
		clearProperties();
	}

	public void clearProperties() {
		this.autoKey = "";
		this.merchantID = "";
		this.feature = "";
		this.accountNumber = "";
		this.branchCode = "";
		this.t1 = "";
		this.t2 = "";
		this.n1 = "";
		this.n2 = "";
	}

	public String getAutoKey() {
		return autoKey;
	}

	public void setAutoKey(String autoKey) {
		this.autoKey = autoKey;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public String getFeature() {
		return feature;
	}

	public void setFeature(String feature) {
		this.feature = feature;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getT1() {
		return t1;
	}

	public void setT1(String t1) {
		this.t1 = t1;
	}

	public String getT2() {
		return t2;
	}

	public void setT2(String t2) {
		this.t2 = t2;
	}

	public String getN1() {
		return n1;
	}

	public void setN1(String n1) {
		this.n1 = n1;
	}

	public String getN2() {
		return n2;
	}

	public void setN2(String n2) {
		this.n2 = n2;
	}

	@Override
	public String toString() {
		return "AccInfoData [autoKey=" + autoKey + ", merchantID=" + merchantID + ", feature=" + feature
				+ ", accountNumber=" + accountNumber + ", branchCode=" + branchCode + ", t1=" + t1 + ", t2=" + t2
				+ ", n1=" + n1 + ", n2=" + n2 + "]";
	}

}

