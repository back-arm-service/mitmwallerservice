package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.DocumentData;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;
import com.nirvasoft.rp.shared.DocumentDataset;
import com.nirvasoft.rp.shared.DocumentListingDataList;
import com.nirvasoft.rp.shared.FilterData;
import com.nirvasoft.rp.shared.FilterDataset;

public class DocDao {
	// atn
	public static DBRecord definedb(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("autokey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("fileName", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("status", (byte) 5));
		ret.getFields().add(new DBField("fileType", (byte) 5));
		ret.getFields().add(new DBField("region", (byte) 5));
		ret.getFields().add(new DBField("filePath", (byte) 5));
		ret.getFields().add(new DBField("postedBy", (byte) 5));
		ret.getFields().add(new DBField("modifiedBy", (byte) 5));
		ret.getFields().add(new DBField("fileSize", (byte) 5));
		ret.getFields().add(new DBField("title", (byte) 5));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 1));
		ret.getFields().add(new DBField("n2", (byte) 1));
		ret.getFields().add(new DBField("n3", (byte) 1));
		ret.getFields().add(new DBField("n4", (byte) 1));
		ret.getFields().add(new DBField("n5", (byte) 1));
		return ret;
	}

	/* atn */
	public static DocumentData getDocRecord(DBRecord adbr) {
		DocumentData ret = new DocumentData();
		ret.setAutokey(adbr.getLong("autokey"));
		ret.setCreatedDate(adbr.getString("createddate"));
		ret.setTitle(adbr.getString("title"));
		ret.setModifiedDate(adbr.getString("modifieddate"));
		ret.setFileName(adbr.getString("fileName"));
		ret.setUserId(adbr.getString("userid"));
		ret.setStatus(adbr.getString("status"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setFilePath(adbr.getString("filepath"));
		ret.setRegion(adbr.getString("region"));
		ret.setFileType(adbr.getString("filetype"));
		ret.setFileSize(adbr.getString("filesize"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setN1(adbr.getInt("n1"));
		ret.setN2(adbr.getInt("n2"));
		ret.setN3(adbr.getInt("n3"));
		ret.setN4(adbr.getInt("n4"));
		ret.setN5(adbr.getInt("n5"));
		return ret;
	}

	/* atn */
	public static boolean isCodeExist(DocumentData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(definedb("document"),
				" where RecordStatus<>4 AND  RecordStatus = 1 AND autokey = " + obj.getAutokey() + " ", "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	/* atn */
	public Resultb2b deleteDoc(DocumentData data, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = "UPDATE Document SET RecordStatus=4,modifiedDate=?,modifiedby=? WHERE RecordStatus = 1 AND autokey=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, data.getModifiedDate());
		stmt.setString(2, data.getModifiedby());
		stmt.setLong(3, data.getAutokey());
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Successfully");
		} else {
			res.setMsgDesc("Deleting Unsuccessful");
		}
		return res;
	}

	public int fillQueryParameters(ArrayList<FilterData> aFilterList, PreparedStatement aStatement)
			throws SQLException {
		int l_Index = 1;

		String l_T1, l_T2;
		String l_DataType, l_Condition;

		for (FilterData iFilterData : aFilterList) {
			l_T1 = iFilterData.getT1();
			l_T2 = iFilterData.getT2();

			l_DataType = iFilterData.getDatatype();
			l_Condition = iFilterData.getCondition();

			if (l_DataType.equals("numeric")) {
				if (!isInteger(l_T1))
					l_T1 = "-1";
				if (!isInteger(l_T2))
					l_T2 = "-1";
			}

			// For Wild Card Search
			//
			switch (l_Condition) {
			case "bw":
				l_T1 = l_T1 + "%";
				l_T2 = l_T2 + "%";
				break;

			case "ew":
				l_T1 = "%" + l_T1;
				l_T2 = "%" + l_T2;
				break;

			case "c":
				l_T1 = "%" + l_T1 + "%";
				l_T2 = "%" + l_T2 + "%";
				break;

			default:
				break;
			}

			switch (l_DataType) {
			case "date":
			case "string":

			case "simplesearch":
				aStatement.setString(l_Index++, l_T1);
				if (l_Condition.equals("bt"))
					aStatement.setString(l_Index++, l_T2);
				break;

			case "numeric":
			default:
				aStatement.setInt(l_Index++, Integer.parseInt(l_T1));
				if (l_Condition.equals("bt"))
					aStatement.setInt(l_Index++, Integer.parseInt(l_T2));
				break;
			}
		}

		return (l_Index - 1);
	}

	// DBS Function
	//
	public boolean findReversalFilter(ArrayList<FilterData> aFilterList) {
		String l_ItemID, l_T1;
		boolean l_FindReversalFilter = false;

		for (FilterData iFilterData : aFilterList) {
			l_T1 = iFilterData.getT1();
			l_ItemID = iFilterData.getItemid();

			// 16 - Filter Type (dr/cr/rev), 2 - Reversal Transaction
			//
			if (l_ItemID.equals("16") && l_T1.equals("")) {
				l_FindReversalFilter = true;
				break;
			}
		}

		return l_FindReversalFilter;
	}

	public String getCriteriaItem(FilterData aFilterItem) {
		String l_CriteriaItem = "";

		String l_T1 = aFilterItem.getT1();
		String l_ItemID = aFilterItem.getItemid();

		String l_DataType = aFilterItem.getDatatype();
		String l_FieldName = aFilterItem.getFieldname();
		String l_Condition = aFilterItem.getCondition();

		// Prepare table alias
		l_FieldName = "A." + l_FieldName;

		// Prepare Condition
		//
		switch (l_Condition) {
		case "bw": // bw - Begin With
		case "ew": // ew - End With
		case "c":
			l_Condition = "LIKE ?";
			break; // c - Contains

		case "eq":
			l_Condition = "= ?";
			break; // eq - Equal
		case "bt":
			l_Condition = ">= ? AND " + l_FieldName + " <= ?";
			break; // bt - Between

		case "gt":
			l_Condition = "> ?";
			break; // gt - Greater Than
		case "geq":
			l_Condition = ">= ?";
			break; // geq - Greater Than Equal

		case "lt":
			l_Condition = "< ?";
			break; // lt - Less Than
		case "leq":
			l_Condition = "<= ?";
			break; // leq - Less Than Equal

		default:
			l_Condition = "= ?";
			break;
		}

		// Criteria Item
		l_CriteriaItem = "(" + l_FieldName + " " + l_Condition + ")";

		// Return criteria string
		return l_CriteriaItem;
	}

	// document List
	/*public DocumentListingDataList getDocumentlist(FilterDataset req, Connection conn) throws SQLException {

		DocumentListingDataList res = new DocumentListingDataList();

		String query = "select t5 from UVM005_A where t1=?";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, req.getUserID());
		ResultSet rs1 = ps.executeQuery();
		String regioncode = "";

		while (rs1.next()) {
			regioncode = rs1.getString("t5");
		}

		int startPage = (req.getPageNo() - 1) * req.getPageSize();
		int endPage = req.getPageSize() + startPage;

		ArrayList<DocumentData> datalist = new ArrayList<DocumentData>();
		PreparedStatement stat;
		String whereclause = "";
		whereclause = " where RecordStatus<>4  AND  RecordStatus = 1 ";

		// Get Filter List
		ArrayList<FilterData> l_FilterList = req.getFilterList();

		// Clear Filter List
		if (l_FilterList.get(0).getItemid() == "")
			l_FilterList.clear();

		// 1 - Simple Search String [ , , , ]
		String simplesearch = "";
		FilterData l_FilterData = getFilterData("1", l_FilterList);
		if (l_FilterData != null)
			simplesearch = l_FilterData.getT1();

		// 3 - status
		String statusString = "";
		l_FilterData = getFilterData("2", l_FilterList);
		if (l_FilterData != null)
			statusString = l_FilterData.getT1();

		if (!simplesearch.equals("")) {
			whereclause += "AND (title LIKE N'%" + simplesearch + "%' OR  filename LIKE N'%" + simplesearch
					+ "%' OR  status LIKE N'%" + simplesearch + "%'" + " OR  modifiedDate LIKE N'%" + simplesearch
					+ "%'" + " OR  postedby LIKE N'%" + simplesearch + "%'" + " OR  modifiedby LIKE N'%" + simplesearch
					+ "%') ";
		}

		if (!statusString.equalsIgnoreCase("")) {
			whereclause = whereclause + " AND (n1=" + Integer.parseInt(statusString) + ") ";
		}

		String sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY autokey desc) AS RowNum,* FROM"
				+ " ( select * from document" + whereclause + ") b) " + " AS RowConstrainedResult" + " WHERE (RowNum > "
				+ startPage + " AND RowNum <= " + endPage + ")";

		PreparedStatement ps1 = conn.prepareStatement(sql);
		ResultSet rs = ps1.executeQuery();
		int srno = startPage + 1;
		while (rs.next()) {
			DocumentData data = new DocumentData();
			data.setSrno(srno++);// srno
			data.setAutokey(rs.getLong("autokey"));
			data.setTitle(rs.getString("title"));// title
			data.setFileName(rs.getString("filename"));
			data.setFilePath(rs.getString("filepath"));
			data.setFileType(rs.getString("fileType"));
			data.setModifiedDate(rs.getString("modifieddate").substring(0, 10));
			data.setModifiedTime(rs.getString("modifieddate").substring(11, 16));
			data.setStatus(rs.getString("status"));
			data.setPostedby(rs.getString("postedby"));
			data.setModifiedby(rs.getString("modifiedby"));
			if (data.getN1() == 1) {
				data.setT3("Draft");
			}
			if (data.getN1() == 2) {
				data.setT3("Pending");
			}
			if (data.getN1() == 3) {
				data.setT3("Modification");
			}
			if (data.getN1() == 4) {
				data.setT3("Approve");
			}
			if (data.getN1() == 5) {
				data.setT3("Publish");
			}
			if (data.getN1() == 6) {
				data.setT3("Reject");
			}
			datalist.add(data);
		}
		res.setPageSize(req.getPageSize());
		res.setCurrentPage(req.getPageNo());

		DocumentData[] dataarray = null;
		if (datalist.size() > 0) {
			dataarray = new DocumentData[datalist.size()];
			dataarray = datalist.toArray(dataarray);
		}

		String qry = "SELECT count(*) as recCount FROM (SELECT ROW_NUMBER() "
				+ "OVER (ORDER BY autokey desc) AS RowNum,* FROM (" + "select * from document " + whereclause
				+ ") b) AS RowConstrainedResult";

		stat = conn.prepareStatement(qry);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		res.setDocumentData(dataarray);
		return res;
	}*/

	public FilterData getFilterData(String aItemID, ArrayList<FilterData> aFilterList) {
		FilterData l_FilterData = null;

		for (FilterData iFilterData : aFilterList) {
			if (iFilterData.getItemid().equals(aItemID)) {
				l_FilterData = iFilterData;
				break;
			}
		}

		return l_FilterData;
	}

	// atn
	/*public Resultb2b insert(DocumentData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String regionobj = "";
		if (obj.getT2().equals("00000000")) {
			obj.setRegion("-");
		} else if (obj.getT2().equals("13000000")) {
			obj.setRegion("YANGON");
		} else if (obj.getT2().equals("10000000")) {
			obj.setRegion("MANDALAY");
		}
		if (obj.getN1() == 1) {
			obj.setStatus("Draft");
		}
		if (obj.getN1() == 2) {
			obj.setStatus("Pending");
		}
		if (obj.getN1() == 4) {
			obj.setStatus("Approved");
		}
		if (obj.getN1() == 5) {
			obj.setStatus("Publish");
		}
		if (obj.getN1() == 6) {
			obj.setStatus("Reject");
		}
		String query = "INSERT INTO Document (createddate, modifieddate,filename, userid,"// 4
				+ " status,recordStatus,filetype,region,filepath,filesize,title,"// 7
				+ "t1, t2, t3, t4, t5,n1,n2, n3, n4,postedby,modifiedby)"// 11
				+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			if (!isCodeExist(obj, conn)) {
				PreparedStatement ps = conn.prepareStatement(query);
				ps.setString(1, obj.getCreatedDate());
				ps.setString(2, obj.getModifiedDate());
				ps.setString(3, obj.getFileName());
				ps.setString(4, obj.getUserId());
				ps.setString(5, obj.getStatus());
				ps.setInt(6, obj.getRecordStatus());
				ps.setString(7, obj.getFileType());
				ps.setString(8, obj.getRegion());
				ps.setString(9, obj.getFilePath());
				ps.setString(10, obj.getFileSize());
				ps.setString(11, obj.getTitle());
				ps.setString(12, obj.getT1());
				ps.setString(13, obj.getT2());
				ps.setString(14, obj.getT3());
				ps.setString(15, obj.getT4());
				ps.setString(16, obj.getT5());
				ps.setInt(17, obj.getN1());
				ps.setInt(18, obj.getN2());
				ps.setInt(19, obj.getN3());
				ps.setInt(20, obj.getN4());
				// ps.setInt(22, obj.getN5());
				ps.setString(21, obj.getPostedby());
				ps.setString(22, obj.getModifiedby());
				if (ps.executeUpdate() > 0) {
					res.setState(true);
					res.setMsgDesc("Saved Successfully!");
				} else {
					res.setMsgDesc("Document Already Exist!");
				}
			}
			String sql = "select TOP 1 autokey FROM document ORDER BY autokey DESC";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				obj.setAutokey(rs.getInt("autokey"));
			}
		} catch (SQLException e) {

		}
		return res;
	}*/

	public boolean isInteger(Object object) {
		boolean l_isInteger = false;

		if (object instanceof Integer) {
			l_isInteger = true;
		} else {
			String string = object.toString();

			try {
				Integer.parseInt(string);
				l_isInteger = true;
			} catch (Exception e) {

			}
		}

		return l_isInteger;
	}

	public DocumentData readDoc(long autokey, Connection conn) throws SQLException {
		DocumentData ret = new DocumentData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(definedb("document"),
				"where RecordStatus<>4 AND  RecordStatus = 1 AND autokey=" + autokey, "", conn);
		if (dbrs.size() > 0)
			ret = getDocRecord(dbrs.get(0));
		/*
		 * if (ServerUtil.isUniEncoded(ret.getT2())) {
		 * ret.setT2(FontChangeUtil.uni2zg(ret.getT2())); }
		 */
		return ret;
	}

	/*public DocumentDataset searchDocList(FilterDataset aFilterDataset, String userid, Connection aConn)
			throws SQLException {
		int iIndex = 1;

		String l_Query = "";
		String l_SelectClause = "";

		String l_CriteriaSep = "OR";
		String l_CriteriaClause = "";
		String l_RecordCountClause = "";
		String whereClause = "";

		ResultSet l_Result;
		PreparedStatement l_Statement;

		int l_TotalRecords = 0;

		int l_PageNo, l_PageSize;
		int l_StartRecord, l_EndRecord;

		String l_CriteriaItem;

		ArrayList<FilterData> l_FilterList;
		ArrayList<String> l_CriteriaList = new ArrayList<String>();

		DocumentDataset l_Dataset = new DocumentDataset();
		ArrayList<DocumentData> l_DataList = new ArrayList<DocumentData>();

		// By region roll
		String query = "select t5 from UVM005_A where t1='" + aFilterDataset.getUserID() + "' ";
		PreparedStatement ps;
		ps = aConn.prepareStatement(query);
		l_Result = ps.executeQuery();
		String regioncode = "";

		while (l_Result.next()) {
			regioncode = l_Result.getString("t5");
		}
		// Record Count Clause
		//
		l_RecordCountClause = "SELECT COUNT(1) As TotalRecords FROM dbo.document";

		// Select Clause
		//
		l_SelectClause = "SELECT ROW_NUMBER() OVER (ORDER BY autokey) AS RowNum, * FROM dbo.document";

		// Get Filter List
		l_FilterList = aFilterDataset.getFilterList();

		// Clear Filter List
		
		 * if (l_FilterList.get(0).getItemid() == "") l_FilterList.clear();
		 

		// Clear Filter List
		if (l_FilterList.equals(""))
			l_FilterList.clear();

		whereClause += " Where recordstatus=1 and recordstatus<>4 ";

		// 1 - Simple Search String [ , , , ]
		String simplesearch = "";
		FilterData l_FilterData = getFilterData("1", l_FilterList);
		if (l_FilterData != null)
			simplesearch = l_FilterData.getT1();

		// 2 - State
		String state = "";
		l_FilterData = getFilterData("2", l_FilterList);
		if (l_FilterData != null)
			state = l_FilterData.getT1();

		// 3 - Status
		String status = "";
		l_FilterData = getFilterData("3", l_FilterList);
		if (l_FilterData != null)
			status = l_FilterData.getT1();

		// 1 - Simple Search String [ , , , ]
		if (!simplesearch.equals("")) {
			simplesearch = "%" + simplesearch + "%";
			whereClause += "AND (title like '" + simplesearch + "' ) OR " + "(filename like '" + simplesearch + "' )";
		}

		// 2 - State
		if (!state.equals("")) {
			if (state.equals("00000000")) {
				whereClause += "AND (t12 = '10000000' OR t12 = '13000000') ";
			} else {
				whereClause += "AND t12 = '" + state + "' ";
			}
		} else {
			if (!regioncode.equals("00000000")) {
				whereClause += "AND t12 = '" + regioncode + "' ";
			}
		}

		// 3 - Status
		if (!status.equals("")) {
			if (status.equals("All")) {
				whereClause += "AND (t10 = 'New' OR t10 = 'WIP' OR t10 = 'Closed' OR t10 = 'Rejected') ";
			} else {
				whereClause += "AND t10 = '" + status + "' ";
			}
		}

		// 4 - Date
		l_FilterData = getFilterData("4", l_FilterList);
		String date = "";
		String fromDate = "";
		String toDate = "";

		if (l_FilterData != null) {
			if (l_FilterData.getCondition().equals("bt")) {
				fromDate = l_FilterData.getT1();
				toDate = l_FilterData.getT2();
				whereClause += "AND ModifiedDate >= '" + fromDate + "' and ModifiedDate <= '" + toDate + "' ";
			} else {
				date = l_FilterData.getT1();
				String l_Condition = "";
				switch (l_FilterData.getCondition()) {
				case "eq":
					l_Condition = "=";
					break; // eq - Equal

				case "gt":
					l_Condition = ">";
					break; // gt - Greater Than
				case "geq":
					l_Condition = ">=";
					break; // geq - Greater Than Equal

				case "lt":
					l_Condition = "<";
					break; // lt - Less Than
				case "leq":
					l_Condition = "<=";
					break; // leq - Less Than Equal
				}

				whereClause += "AND ModifiedDate " + l_Condition + " '" + date + "' ";
			}
		}
		// Prepare criteria clause
		
		 * if (l_CriteriaList.size() > 0) { if (aFilterDataset.getFilterSource()
		 * == 0) l_CriteriaSep = " OR "; // 0 - Common Search else l_CriteriaSep
		 * = " AND "; // 1 - Advanced Search
		 * 
		 * l_CriteriaClause = "(" + String.join(l_CriteriaSep, whereClause) +
		 * ")";// l_CriteriaList }
		 
		// Clear criteria list
		// l_CriteriaList.clear();

		// Add criteria clause
		if (!l_CriteriaClause.equals(""))
			l_CriteriaList.add(l_CriteriaClause);

		// Prepare criteria clause
		if (l_CriteriaList.size() > 0)
			l_CriteriaClause = " WHERE (" + String.join(" AND ", l_CriteriaList) + ")";

		// Get Total Records Count
		//
		l_Query = l_RecordCountClause + whereClause;
		l_Statement = aConn.prepareStatement(l_Query);

		// Set parameters
		// fillQueryParameters(l_FilterList, l_Statement);

		// Execute Query
		l_Result = l_Statement.executeQuery();

		if (l_Result.next()) {
			l_TotalRecords = l_Result.getInt("TotalRecords");
			l_Dataset.setTotalCount(l_TotalRecords);
		}

		l_Result.close();
		l_Statement.close();
		l_Query = "SELECT RowNum, * FROM ( " + l_SelectClause + whereClause
				+ ") As Resultset WHERE (? = 0) OR (RowNum > ? AND RowNum <= ?)";
		l_Statement = aConn.prepareStatement(l_Query);
		// Set parameters
		//
		// iIndex = fillQueryParameters(l_FilterList, l_Statement);

		// Calculate Pagination
		//
		l_PageNo = aFilterDataset.getPageNo();
		l_PageSize = aFilterDataset.getPageSize();

		l_StartRecord = (l_PageNo - 1) * l_PageSize;
		l_EndRecord = l_StartRecord + l_PageSize;

		// Move index pointer
		// iIndex = iIndex + 1;

		// Parameters for Pagination
		//
		l_Statement.setInt(1, l_PageNo);
		l_Statement.setInt(2, l_StartRecord);
		l_Statement.setInt(3, l_EndRecord);

		// Execute Query
		l_Result = l_Statement.executeQuery();

		while (l_Result.next()) {
			DocumentData data = new DocumentData();
			data.setAutokey(l_Result.getLong("autokey"));
			data.setTitle(l_Result.getString("title"));// title
			data.setFileName(l_Result.getString("filename"));
			data.setFilePath(l_Result.getString("filepath"));
			data.setModifiedDate(l_Result.getString("modifieddate").substring(0, 10));
			data.setModifiedTime(l_Result.getString("modifieddate").substring(11, 16));
			data.setStatus(l_Result.getString("status"));
			data.setPostedby(l_Result.getString("postedby"));
			data.setModifiedby(l_Result.getString("modifiedby"));
			l_DataList.add(data);
		}

		l_Result.close();
		l_Statement.close();

		// Set User ID, User Name
		//
		l_Dataset.setUserID(aFilterDataset.getUserID());
		l_Dataset.setUserName(aFilterDataset.getUserName());

		// Add list
		l_Dataset.setDataList(l_DataList);

		// Return list
		return l_Dataset;
	}*/

	public DocumentDataset searchDocList1(FilterDataset aFilterDataset, String userid, Connection conn)
			throws SQLException {
		DocumentDataset ticketres = new DocumentDataset();
		ArrayList<DocumentData> l_DataList = new ArrayList<DocumentData>();
		String query = "select t5 from UVM005_A where t1='" + aFilterDataset.getUserID() + "' ";
		PreparedStatement ps;
		ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		String regioncode = "";
		while (rs.next()) {
			regioncode = rs.getString("t5");
		}
		int startPage = (aFilterDataset.getPageNo() - 1) * aFilterDataset.getPageSize();
		int endPage = aFilterDataset.getPageSize() + startPage;

		String whereClause = "";
		whereClause = "where n2=1 and n2<>4 ";

		// Get Filter List
		ArrayList<FilterData> l_FilterList = aFilterDataset.getFilterList();

		// Clear Filter List
		if (l_FilterList.get(0).getItemid() == "")
			l_FilterList.clear();

		// 1 - Simple Search String [ , , , ]
		String simplesearch = "";
		FilterData l_FilterData = getFilterData("1", l_FilterList);
		if (l_FilterData != null)
			simplesearch = l_FilterData.getT1();

		// 2 - State
		String state = "";
		l_FilterData = getFilterData("2", l_FilterList);
		if (l_FilterData != null)
			state = l_FilterData.getT1();

		// 3 - Status
		String status = "";
		l_FilterData = getFilterData("3", l_FilterList);
		if (l_FilterData != null)
			status = l_FilterData.getT1();

		if (!simplesearch.equals("")) {
			simplesearch = "%" + simplesearch + "%";
			whereClause += " and " + "(modifiedDate like '" + simplesearch + "' ) OR " + "(fileName like '"
					+ simplesearch + "' ) OR " + "(status like '" + simplesearch + "' ) OR " + "(title like '"
					+ simplesearch + "' ) OR " + "(t2 like '" + simplesearch + "' ) ";
		}

		// 2 - State
		if (!state.equals("")) {
			if (state.equals("00000000")) {
				whereClause = whereClause + " AND (t2 = '10000000' OR t2 = '13000000') ";
			} else {
				whereClause = whereClause + " AND t2 = '" + state + "' ";
			}
		} else {
			if (!regioncode.equals("00000000")) {
				whereClause = whereClause + " AND t2 = '" + regioncode + "' ";
			}
		}

		// 3 - Status
		if (!status.equals("")) {
			if (status.equals("All")) {
				whereClause = whereClause
						+ " AND (status = 'Publish' OR status = 'Draft' OR status = 'Closed' OR status = 'Rejected') ";
			} else {
				whereClause = whereClause + " AND status = '" + status + "' ";
			}
		}

		l_FilterData = getFilterData("4", l_FilterList);
		String date = "";
		String fromDate = "";
		String toDate = "";

		if (l_FilterData != null) {
			if (l_FilterData.getCondition().equals("bt")) {
				fromDate = l_FilterData.getT1().replace("-", "");
				toDate = l_FilterData.getT2().replace("-", "");
				whereClause += " and date >= '" + fromDate + "' and date <= '" + toDate + "' ";
			} else {
				date = l_FilterData.getT1().replace("-", "");
				String l_Condition = "";
				switch (l_FilterData.getCondition()) {
				case "eq":
					l_Condition = "=";
					break; // eq - Equal

				case "gt":
					l_Condition = ">";
					break; // gt - Greater Than
				case "geq":
					l_Condition = ">=";
					break; // geq - Greater Than Equal

				case "lt":
					l_Condition = "<";
					break; // lt - Less Than
				case "leq":
					l_Condition = "<=";
					break; // leq - Less Than Equal
				}

				whereClause += " and date " + l_Condition + " '" + date + "' ";
			}
		}
		query = "SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY autokey desc) AS RowNum,* from "
				+ "(select * from document " + whereClause + " )b)AS RowContraintResult WHERE " + "(RowNum > "
				+ startPage + " AND RowNum <= " + endPage + ")";
		ps = conn.prepareStatement(query);
		rs = ps.executeQuery();
		ArrayList<DocumentData> docarrlist = new ArrayList<DocumentData>();
		DocumentData[] docarr = null;

		while (rs.next()) {
			DocumentData data = new DocumentData();
			data.setAutokey(rs.getLong("autokey"));
			data.setTitle(rs.getString("title"));// title
			data.setFileName(rs.getString("filename"));
			data.setFilePath(rs.getString("filepath"));
			data.setModifiedDate(rs.getString("modifieddate").substring(0, 10));
			data.setModifiedTime(rs.getString("modifieddate").substring(11, 16));
			data.setStatus(rs.getString("status"));
			data.setPostedby(rs.getString("postedby"));
			data.setModifiedby(rs.getString("modifiedby"));
			l_DataList.add(data);
		}
		/*
		 * if (docarrlist.size() > 0) { docarr = new
		 * DocumentData[docarrlist.size()]; for (int i = 0; i <
		 * docarrlist.size(); i++) { docarr[i] = docarrlist.get(i); } }
		 */
		String totalqry = "";
		totalqry = "SELECT COUNT(*) AS tot FROM document " + whereClause;
		ps = conn.prepareStatement(totalqry);
		ResultSet rSet = ps.executeQuery();
		rSet.next();
		ticketres.setTotalCount(rSet.getInt("tot"));
		ticketres.setDataList(l_DataList);
		if (fromDate.equals(""))
			// ticketres.setAlldate(true);
			// ticketres. (fromDate);
			// ticketres.setTodate(toDate);
			ticketres.setPageNo(aFilterDataset.getPageNo());
		ticketres.setPageSize(aFilterDataset.getPageSize());

		return ticketres;
	}

	// atn
	public Resultb2b update(DocumentData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String regionobj = "";
		if (obj.getT2().equals("00000000")) {
			obj.setRegion("-");
		}
		if (obj.getT2().equals("13000000")) {
			obj.setRegion("YANGON");
		}
		if (obj.getT2().equals("10000000")) {
			obj.setRegion("MANDALAY");
		}
		if (obj.getN1() == 1) {
			obj.setStatus("Draft");
		}
		if (obj.getN1() == 2) {
			obj.setStatus("Pending");
		}
		if (obj.getN1() == 4) {
			obj.setStatus("Approved");
		}
		if (obj.getN1() == 5) {
			obj.setStatus("Publish");
		}
		if (obj.getN1() == 6) {
			obj.setStatus("Reject");
		}
		String query = "UPDATE Document SET [modifieddate]=?, [filename]=?, [userid]=?, "
				+ " [status]=?, [recordStatus]=?, [filetype]=?,[region]=?,[filePath]=?,[fileSize]=?,[title]=?, "
				+ " [t1]=?, [t2]=?, [t3]=?,[t4]=?, [t5]=?,[n1]=?, [n2]=?, [n3]=?,[n4]=?,modifiedby=?"
				+ "WHERE RecordStatus<>4 AND RecordStatus = 1 AND autokey=" + obj.getAutokey() + "";
		try {
			if (isCodeExist(obj, conn)) {
				PreparedStatement ps = conn.prepareStatement(query);
				ps.setString(1, obj.getModifiedDate());
				ps.setString(2, obj.getFileName());
				ps.setString(3, obj.getUserId());
				ps.setString(4, obj.getStatus());
				ps.setInt(5, obj.getRecordStatus());
				ps.setString(6, obj.getFileType());
				ps.setString(7, obj.getRegion());
				ps.setString(8, obj.getFilePath());
				ps.setString(9, obj.getFileSize());
				ps.setString(10, obj.getTitle());
				ps.setString(11, obj.getT1());
				ps.setString(12, obj.getT2());
				ps.setString(13, obj.getT3());
				ps.setString(14, obj.getT4());
				ps.setString(15, obj.getT5());
				ps.setInt(16, obj.getN1());
				ps.setInt(17, obj.getN2());
				ps.setInt(18, obj.getN3());
				ps.setInt(19, obj.getN4());
				ps.setString(20, obj.getModifiedby());
				// ps.setInt(22, obj.getN5());

				if (ps.executeUpdate() > 0) {
					res.setState(true);
					res.setMsgDesc("Updated Successfully!");
				} else {
					res.setMsgDesc("No Such Document to Update!");
				}
				String sql = "select TOP 1 autokey FROM document ORDER BY autokey DESC";
				ps = conn.prepareStatement(sql);
				ResultSet rs = ps.executeQuery();
				if (rs.next()) {
					obj.setAutokey(rs.getInt("autokey"));
				}
			}
		} catch (SQLException e) {

		}

		return res;
	}

}
