package com.nirvasoft.rp.dao.integration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.nirvasoft.cms.framework.Result;
import com.nirvasoft.rp.data.MessageRequest;
import com.nirvasoft.rp.framework.Lov3;
import com.nirvasoft.rp.framework.Ref;
import com.nirvasoft.rp.mgr.SessionMgr;
import com.nirvasoft.rp.shared.integration.SMSSettingData;
import com.nirvasoft.rp.shared.integration.SMSSettingDataSet;

public class SMSSettingDao {

	public boolean checkOTPSetting(String service, String function, String merchantid, Connection conn)
			throws SQLException {
		boolean ret = false;
		String sql = "select ID from MessageSetting where ServiceCode = ? and FunctionCode = ? and Active = 1";
		if (!merchantid.equals("")) {
			sql += " and merchantid =?";
		}
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setString(i++, service);
		ps.setString(i++, function);
		if (!merchantid.equals("")) {
			ps.setString(i++, merchantid);
		}
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			ret = true;
		}
		return ret;
	}

	public Result deleteSMSSetting(long id, Connection l_Conn) throws SQLException {
		Result ret = new Result();

		String query = "UPDATE MessageSetting SET Active = 4  Where Id = ? ";
		PreparedStatement upsmt = l_Conn.prepareStatement(query);
		upsmt.setLong(1, id);
		if (upsmt.executeUpdate() > 0) {
			ret.setState(true);
			ret.setMsgDesc("Deleted Successfully.");
			ret.setMsgCode("0000");

		} else {
			ret.setState(false);
			ret.setMsgDesc("Deleted Fail.");
			ret.setMsgCode("0014");
		}
		return ret;

	}

	private long generateKey(String function, String sessionid, String userid, String status, Connection mConn)
			throws SQLException {
		long l_RefKey = 0;
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		String sql = "INSERT INTO SecuritySession(securityKey ,UserID ,SessionID, CreatedDate ,ModifiedDate ,FunctionCode ,Status) "
				+ " VALUES(?,?,?,?,?,?,? )";
		PreparedStatement ps = mConn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		int i = 1;
		ps.setString(i++, "0");
		ps.setString(i++, userid);
		ps.setString(i++, sessionid);
		ps.setString(i++, date);
		ps.setString(i++, date);
		ps.setString(i++, function);
		ps.setString(i++, status);
		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys();
		if (rs != null && rs.next()) {
			l_RefKey = rs.getLong(1);
		}

		rs.close();
		ps.close();

		return l_RefKey;
	}

	public Lov3 getFunctionLov(Connection conn) throws SQLException {

		Lov3 lov3 = new Lov3();
		Ref[] refarray = null;
		ArrayList<Ref> reflist = new ArrayList<Ref>();
		PreparedStatement stmt = conn.prepareStatement(
				"select code,Description from lovdetails where hkey = ( select syskey from LOVHeader where Description like '%Function%')");

		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			Ref ref = new Ref();
			ref.setvalue(rs.getString("code"));
			ref.setcaption(rs.getString("Description"));
			reflist.add(ref);
		}

		stmt.close();
		rs.close();

		refarray = new Ref[reflist.size()];
		for (int i = 0; i < reflist.size(); i++) {
			refarray[i] = reflist.get(i);
		}
		lov3.setReffunction(refarray);

		return lov3;

	}

	public ArrayList<SMSSettingData> checkMessageSetting(String service, String function, String merchantid,
			Connection conn) throws SQLException {
		ArrayList<SMSSettingData> ret = new ArrayList<SMSSettingData>();
		String sql = "select * from MessageSetting where ServiceCode = ? and FunctionCode = ? and Active = 1 ";// active=recordstatus
		/*if (!merchantid.equals("")) {
			sql += " and merchantid = ? ";
		}*/
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setString(i++, service);
		ps.setString(i++, function);
		/*if (!merchantid.equals("")) {
			ps.setString(i++, merchantid);
		}*/
		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			SMSSettingData data = new SMSSettingData();
			data.setServiceCode(rs.getString("ServiceCode"));// rs.getString("Dbcolumnname")
			data.setServiceDesc(rs.getString("ServiceDesc"));
			data.setFunCode(rs.getString("FunctionCode"));
			data.setFunDesc(rs.getString("FunctionDesc"));
			data.setFrom(rs.getString("FromSite"));
			data.setFromMsg(rs.getString("FromMessage"));
			data.setTo(rs.getString("ToSite"));
			data.setActive(rs.getString("Active"));
			ret.add(data);

			// data = readData(rs, data);

		}

		return ret;
	}

	public Lov3 getServiceLov(Connection conn) throws SQLException {

		Lov3 lov3 = new Lov3();
		Ref[] refarray = null;
		ArrayList<Ref> reflist = new ArrayList<Ref>();
		String sql = "";
		sql = "select code,Description from lovdetails where hkey = ( select syskey from LOVHeader where Description like '%Service%')";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			Ref ref = new Ref();
			ref.setvalue(rs.getString("code"));
			ref.setcaption(rs.getString("Description"));
			reflist.add(ref);
		}

		stmt.close();
		rs.close();

		refarray = new Ref[reflist.size()];
		for (int i = 0; i < reflist.size(); i++) {
			refarray[i] = reflist.get(i);
		}
		lov3.setRefservice(refarray);

		return lov3;

	}

	public SMSSettingData getSMSSettingDataById(long id, Connection l_Conn) throws SQLException {

		SMSSettingData ret = new SMSSettingData();

		String l_Query = "SELECT ID,ServiceCode ,ServiceDesc ,FunctionCode ,FunctionDesc ,FromSite ,FromMessage ,ToSite , ToMessage ,MerchantID ,Operator ,Active,BothSite,Language,LanguageDesc FROM MessageSetting WHERE Active <> 4 And ID = ?";
		PreparedStatement pstmt = l_Conn.prepareStatement(l_Query);
		pstmt.setLong(1, id);
		ResultSet rs = pstmt.executeQuery();

		while (rs.next()) {
			ret.setId(rs.getLong("ID"));
			ret.setMerchantID(rs.getString("MerchantID"));
			ret.setServiceCode(rs.getString("ServiceCode"));
			ret.setServiceDesc(rs.getString("ServiceDesc"));
			ret.setFunCode(rs.getString("FunctionCode"));
			ret.setFunDesc(rs.getString("FunctionDesc"));
			ret.setFrom(rs.getString("FromSite"));
			ret.setFromMsg(rs.getString("FromMessage"));
			ret.setTo(rs.getString("ToSite"));
			ret.setToMsg(rs.getString("ToMessage"));
			ret.setOperatorType(rs.getString("Operator"));
			ret.setActive(rs.getString("Active"));
			ret.setBoth(rs.getString("BothSite"));
			ret.setLanguage(rs.getString("Language"));
			ret.setLanguageDesc(rs.getString("LanguageDesc"));
		}

		return ret;
	}

	public SMSSettingDataSet getSMSSettingList(String searchtext, int pageSize, int currentPage, Connection conn)
			throws SQLException {
		String searchText = searchtext.replace("'", "''");
		ArrayList<SMSSettingData> datalist = new ArrayList<SMSSettingData>();
		SMSSettingDataSet response = new SMSSettingDataSet();
		int startPage = (currentPage - 1) * pageSize;
		int endPage = pageSize + startPage;
		String query;

		String whereClause = "";

		if (searchText.equals("")) {
			whereClause = "where Active <> 4";
		} else if (searchText.equalsIgnoreCase("Yes")) {
			whereClause = "where Active = 1";
		} else if (searchText.equalsIgnoreCase("No")) {
			whereClause = "where Active = 0";
		} else {
			whereClause = " where Active <> 4 and (ServiceDesc like ? or FunctionDesc like ? or FromMessage like ? "
					+ "or ToMessage like ?  Or  MerchantID like ?  Or Operator like ? )";
		}
		String l_Query = "SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY ID) AS RowNum,"
				+ " ID,ServiceCode ,ServiceDesc ,FunctionCode ,FunctionDesc ,FromSite ,FromMessage ,ToSite , ToMessage ,MerchantID ,Operator ,Active ,BothSite ,Language ,LanguageDesc "
				+ "FROM   MessageSetting " + whereClause + " ) AS RowConstrainedResult " + " WHERE (RowNum > "
				+ startPage + " AND RowNum <= " + endPage + ")";

		System.out.println("SMS Setting query : " + l_Query);

		PreparedStatement ps = conn.prepareStatement(l_Query);
		if (searchText.equals("") || searchText.equalsIgnoreCase("Yes") || searchText.equalsIgnoreCase("No")) {
		} else {
			for (int i = 1; i < 7; i++) {
				ps.setString(i, "%" + searchText + "%");
			}
		}
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			SMSSettingData ret = new SMSSettingData();
			ret.setId(rs.getLong("ID"));
			ret.setMerchantID(rs.getString("MerchantID"));
			ret.setServiceCode(rs.getString("ServiceCode"));
			ret.setServiceDesc(rs.getString("ServiceDesc"));
			ret.setFunCode(rs.getString("FunctionCode"));
			ret.setFunDesc(rs.getString("FunctionDesc"));
			ret.setFrom(rs.getString("FromSite"));
			ret.setFromMsg(rs.getString("FromMessage"));
			ret.setTo(rs.getString("ToSite"));
			ret.setToMsg(rs.getString("ToMessage"));
			ret.setOperatorType(rs.getString("Operator"));
			ret.setActive(rs.getString("Active"));
			ret.setBoth(rs.getString("BothSite"));
			ret.setLanguage(rs.getString("Language"));
			ret.setLanguageDesc(rs.getString("LanguageDesc"));
			datalist.add(ret);
		}

		SMSSettingData[] dataarr = new SMSSettingData[datalist.size()];
		for (int i = 0; i < datalist.size(); i++) {
			dataarr[i] = datalist.get(i);
		}

		response.setData(dataarr);
		response.setCurrentPage(currentPage);
		response.setPageSize(pageSize);
		response.setSearchText(searchText);

		PreparedStatement pstm = conn.prepareStatement("Select COUNT(*) As total FROM MessageSetting " + whereClause);
		if (searchText.equals("") || searchText.equalsIgnoreCase("Yes") || searchText.equalsIgnoreCase("No")) {
		} else {
			for (int i = 1; i < 7; i++) {
				pstm.setString(i, "%" + searchText + "%");
			}
		}
		ResultSet result = pstm.executeQuery();
		result.next();
		response.setTotalCount(result.getInt("total"));

		return response;
	}

	public boolean isCodeExist(SMSSettingData data, Connection conn) throws SQLException {
		boolean ret = false;
		String l_Query = "";
		PreparedStatement psmt = null;
		String whereclause = "";
		if (data.getMerchantID().equals("") && data.getOperatorType().equals("")) {
			whereclause = " ";
		} else {
			whereclause = " And MerchantID = ? And Operator = ? ";
		}
		l_Query = " SELECT Top(1) ServiceCode FROM MessageSetting WHERE Active <> 4 And ServiceCode = ? And FunctionCode = ? And FromSite =? And ToSite = ?"
				+ whereclause;

		psmt = conn.prepareStatement(l_Query);
		int i = 1;
		psmt.setString(i++, data.getServiceCode());
		psmt.setString(i++, data.getFunCode());
		psmt.setString(i++, data.getFrom());
		psmt.setString(i++, data.getTo());
		if (data.getMerchantID().equals("") && data.getOperatorType().equals("")) {
		} else {
			psmt.setString(i++, data.getMerchantID());
			psmt.setString(i++, data.getOperatorType());
		}
		ResultSet rs = psmt.executeQuery();
		while (rs.next()) {
			ret = true;
		}
		return ret;
	}

	public boolean readMessageSetting(String service, String function, String merchantID, Connection conn)
			throws SQLException {
		boolean ret = false;
		String sql = "select ID from MessageSetting where ServiceCode = ? and FunctionCode = ? and Active = 1";
		if (!merchantID.equals("")) {
			sql += " and merchantid =?";
		}
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setString(i++, service);
		ps.setString(i++, function);
		if (!merchantID.equals("")) {
			ps.setString(i++, merchantID);
		}
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			ret = true;
		}
		return ret;
	}
	public SMSSettingData getSMSNotiData(String service, String function,String merchantID, Connection conn)
			throws SQLException {
		boolean ret = false;
		SMSSettingData response=new SMSSettingData();
		
		String sql = "select FromSite,ToSite,BothSite,FromMessage,ToMessage  from MessageSetting where ServiceCode = ? and FunctionCode = ? and Active = 1";
		if (!merchantID.equals("")) {
			sql += " and merchantid =?";
		}
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setString(i++, service);
		ps.setString(i++, function);
//		if (!merchantID.equals("")) {
//			ps.setString(i++, merchantID);
//		}
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			response.setCode("0000");
			response.setDesc("Successfully");
			response.setFrom(rs.getString("FromSite")); 
			response.setTo(rs.getString("ToSite"));
			response.setBoth(rs.getString("BothSite"));
			response.setFromMsg(rs.getString("FromMessage"));
			response.setToMsg(rs.getString("ToMessage"));
		}
		else{
			response.setCode("0014");
			response.setDesc("No Noti Data");
		}
		return response;
	}
	public boolean readNotiMessageSetting(MessageRequest data, Connection conn)
			throws SQLException {
		boolean ret = false;
		String sql = "select ID from MessageSetting where ServiceCode = ? and FunctionCode = ? and Active = 1";
		if (!data.getMerchantID().equals("")) {
			sql += " and merchantid =?";
		}
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setString(i++, data.getServiceType());
		ps.setString(i++, data.getType());
		if (!data.getMerchantID().equals("")) {
			ps.setString(i++,data.getMerchantID());
		}
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			ret = true;
		}
		return ret;
	}

	public String saveSecuritySession(String function, String sessionid, String userid, String status, Connection conn)
			throws SQLException {

		String skey = "";
		String reskey = "";
		SessionMgr ssmgr = new SessionMgr();
		skey = ssmgr.generateOTP();
		long key = 0L;
		key = generateKey(function, sessionid, userid, status, conn);
		skey = String.valueOf(key) + skey;
		String query = "UPDATE SecuritySession SET SecurityKey=? WHERE AutoKey = ? and SessionId=? and Userid=? and Status=?";
		PreparedStatement pstmt = conn.prepareStatement(query);
		int j = 1;
		pstmt.setString(j++, String.valueOf(skey));
		pstmt.setString(j++, String.valueOf(key));
		pstmt.setString(j++, sessionid);
		pstmt.setString(j++, userid);
		pstmt.setString(j++, status);

		int rst = pstmt.executeUpdate();
		if (rst > 0) {
			reskey = skey;
		} else {
			reskey = "";
		}
		pstmt.close();
		return reskey;
	}

	public Result saveSMSSetting(SMSSettingData data, Connection aConn) throws SQLException {

		Result ret = new Result();

		if (isCodeExist(data, aConn)) {
			ret.setMsgCode("0014");
			ret.setMsgDesc("Data already exists");
			ret.setState(false);

		} else {
			String query = "INSERT INTO MessageSetting (ServiceCode ,ServiceDesc ,FunctionCode ,FunctionDesc ,FromSite ,FromMessage ,ToSite ,ToMessage ,MerchantID ,Operator ,Active,BothSite,Language,LanguageDesc)"
					+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement psmt = aConn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			int i = 1;
			psmt.setString(i++, data.getServiceCode());
			psmt.setString(i++, data.getServiceDesc());
			psmt.setString(i++, data.getFunCode());
			psmt.setString(i++, data.getFunDesc());
			psmt.setString(i++, data.getFrom());
			psmt.setString(i++, data.getFromMsg());
			psmt.setString(i++, data.getTo());
			psmt.setString(i++, data.getToMsg());
			psmt.setString(i++, data.getMerchantID());
			psmt.setString(i++, data.getOperatorType());
			psmt.setString(i++, data.getActive());
			psmt.setString(i++, data.getBoth());
			psmt.setString(i++, data.getLanguage());
			psmt.setString(i++,data.getLanguageDesc());
			psmt.executeUpdate();
			ResultSet result = psmt.getGeneratedKeys();
			if (result != null && result.next()) {
				long l_RefKey = result.getLong(1);
				ret.setKeyResult(l_RefKey);
				System.out.println("Retur key : " + l_RefKey);
				ret.setMsgCode("0000");
				ret.setMsgDesc("Saved Successfully");
				ret.setState(true);
			} else {
				ret.setMsgCode("0014");
				ret.setMsgDesc("Saved Fail");
				ret.setState(false);
			}
		}
		return ret;
	}

	public Result updateSMSSetting(SMSSettingData data, Connection aConn) throws SQLException {

		Result ret = new Result();

		String query = "Update MessageSetting Set ServiceCode = ? ,ServiceDesc =? ,FunctionCode =? ,FunctionDesc =?,FromSite =? ,FromMessage =? ,ToSite =?,"
				+ " ToMessage =?,MerchantID =?,Operator =?,Active =? Where Id = ?";

		PreparedStatement psmt = aConn.prepareStatement(query);
		int i = 1;
		psmt.setString(i++, data.getServiceCode());
		psmt.setString(i++, data.getServiceDesc());
		psmt.setString(i++, data.getFunCode());
		psmt.setString(i++, data.getFunDesc());
		psmt.setString(i++, data.getFrom());
		psmt.setString(i++, data.getFromMsg());
		psmt.setString(i++, data.getTo());
		psmt.setString(i++, data.getToMsg());
		psmt.setString(i++, data.getMerchantID());
		psmt.setString(i++, data.getOperatorType());
		psmt.setString(i++, data.getActive());
		psmt.setLong(i++, data.getId());

		if (psmt.executeUpdate() > 0) {
			ret.setKeyResult(data.getId());
			ret.setMsgCode("0000");
			ret.setMsgDesc("Updated Successfully");
			ret.setState(true);
		} else {
			ret.setMsgCode("0014");
			ret.setMsgDesc("Updated Fail");
			ret.setState(false);
		}
		return ret;
	}

}
