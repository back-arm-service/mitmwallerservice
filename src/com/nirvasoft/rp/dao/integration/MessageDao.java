package com.nirvasoft.rp.dao.integration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.nirvasoft.rp.shared.integration.MessageData;

public class MessageDao {

	public boolean saveMessageData(MessageData msgData, Connection conn)
			throws SQLException {
		boolean ret = false;
		String query = "insert into SMSNoti (createdDate, modifiedDate, userId, userName, recordStatus,"
				+ "toAddress, message, dateReceived, dateSent,retriesCount,serviceCode,status,responseMessage,deliveryStatus,"
				+ "t1,t2,t3,t4,t5,n1,n2,n3,n4,n5) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		PreparedStatement ps = conn.prepareStatement(query);
		int col = 1;
		ps.setString(col++, msgData.getCreatedDate());
		ps.setString(col++, msgData.getModifiedDate());
		ps.setString(col++, msgData.getUserId());
		ps.setString(col++, msgData.getUserName());
		ps.setInt(col++, msgData.getRecordStatus());
		ps.setString(col++, msgData.getToAddress());
		ps.setString(col++, msgData.getMessage());
		ps.setString(col++, msgData.getDateReceived());
		ps.setString(col++, msgData.getDateSent());
		ps.setInt(col++, Integer.parseInt(msgData.getRetriesCount()));
		ps.setString(col++, msgData.getServiceCode());
		ps.setInt(col++, Integer.parseInt(msgData.getStatus()));
		ps.setString(col++, msgData.getResponseMessage());
		ps.setInt(col++, Integer.parseInt(msgData.getDeliveryStatus()));
		ps.setString(col++, msgData.getT1());
		ps.setString(col++, msgData.getT2());
		ps.setString(col++, msgData.getT3());
		ps.setString(col++, msgData.getT4());
		ps.setString(col++, msgData.getT5());
		ps.setInt(col++, msgData.getN1());
		ps.setInt(col++, msgData.getN2());
		ps.setInt(col++, msgData.getN3());
		ps.setInt(col++, msgData.getN4());
		ps.setInt(col++, msgData.getN5());
		if (ps.executeUpdate() > 0) {
			ret = true;
		}
		ps.close();
		return ret;
	}
}
