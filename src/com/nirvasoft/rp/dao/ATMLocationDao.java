package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.nirvasoft.rp.framework.Lov3;
import com.nirvasoft.rp.framework.Ref;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.shared.ATMLocation;
import com.nirvasoft.rp.shared.ATMLocationList;
import com.nirvasoft.rp.shared.ATMLocationResData;
import com.nirvasoft.rp.shared.LocationData;
import com.nirvasoft.rp.shared.LovRes;

public class ATMLocationDao {

	// public ATMLocationResData getATMLocation(String id, Connection l_Conn)
	// throws SQLException {
	// // TODO Auto-generated method stub
	// ATMLocationResData res = new ATMLocationResData();
	// res.setId(id);
	// ArrayList<ATMLocation> dataList = new ArrayList<ATMLocation>();
	// String sql="";
	// PreparedStatement ps=null;
	// sql = "select
	// latitude,longitude,address,phone1,phone2,branchCode,name,locationType,services,workingHour"
	// + " from Locator ";
	// if (!id.equals("")) {
	// sql += " Where branchcode = ? ";
	// ps = l_Conn.prepareStatement(sql);
	// ps.setString(1, id);
	// }else{
	// ps = l_Conn.prepareStatement(sql);
	// }
	// //System.out.println("Search query :: " + sql);
	//
	// ResultSet rs = ps.executeQuery();
	// while (rs.next()) {
	// ATMLocation data = new ATMLocation();
	// data.setLatitude(rs.getString("latitude"));
	// data.setLongitude(rs.getString("longitude"));
	// data.setAddress(rs.getString("address"));
	// data.setPhone1(rs.getString("phone1"));
	// data.setPhone2(rs.getString("phone2"));
	//
	// if (rs.getString("branchcode") == null || rs.getString("branchcode") ==
	// "") {
	// data.setBranchCode("");
	// } else {
	// data.setBranchCode(rs.getString("branchcode"));
	// }
	// data.setName(rs.getString("name"));
	// data.setLocationType(rs.getString("locationType"));
	// data.setServices(rs.getString("services"));
	// data.setWorkingHour(rs.getString("workingHour"));
	// dataList.add(data);
	// }
	// ATMLocation[] dataArr = new ATMLocation[dataList.size()];
	// for (int i = 0; i < dataList.size(); i++) {
	// dataArr[i] = dataList.get(i);
	// }
	//
	// if (dataList.size() > 0) {
	// res.setData(dataArr);
	// res.setCode("0000");
	// res.setDesc("Successfully");
	// } else {
	// res.setCode("0014");
	// res.setDesc("Parameter is not found");
	// }
	//
	// ps.close();
	// rs.close();
	// return res;
	// }

	public static String leadZero(int code, int count) {
		String l_result = "";
		String l_code = String.valueOf(code);
		if (l_code.trim().length() < count) {
			for (int i = l_code.trim().length(); i < count; i++) {
				l_result += "0";
			}
		}
		l_result += code;
		return l_result;
	}

	public Result deleteATMLocator(ATMLocation data, Connection l_Conn) throws SQLException {
		String atmId = data.getT1();
		Result ret = new Result();
		String sql1 = " DELETE Locator WHERE t1=? ";
		PreparedStatement stmt1 = l_Conn.prepareStatement(sql1);
		stmt1.setString(1, atmId);
		int rs = stmt1.executeUpdate();
		if (rs > 0) {
			ret.setMsgDesc("Deleted successfully");
			ret.setState(true);
		} else {
			ret.setMsgDesc("Delete Failed");
			ret.setState(false);
		}

		return ret;
	}

	public LocationData getATMLocation(String id, Connection l_Conn) throws SQLException {
		// TODO Auto-generated method stub
		ATMLocationResData res = new ATMLocationResData();
		res.setId(id);
		ArrayList<ATMLocation> dataList = new ArrayList<ATMLocation>();
		ArrayList<LovRes> typeList = new ArrayList<LovRes>();
		PreparedStatement prepare = l_Conn.prepareStatement(// SUBSTRING(Description,0,
															// CHARINDEX(' ',
															// Description))
				"select Code, Description from LOVDetails where HKey =(select syskey from LOVHeader where Description='Location Type')");
		ResultSet rset = prepare.executeQuery();
		while (rset.next()) {
			LovRes lov = new LovRes();
			lov.setCode(rset.getString("Code"));
			lov.setDesc(rset.getString("Description"));
			typeList.add(lov);
		}
		prepare.close();
		rset.close();
		String sql = "";
		String locType = "";
		PreparedStatement ps = null;
		if (typeList.size() > 0) {
			for (int i = 0; i < typeList.size(); i++) {
				locType += "'" + typeList.get(i).getCode() + "'" + ", ";
			}
			locType = locType.substring(0, locType.lastIndexOf(", "));
			locType = " and locationType in (" + locType + ")";
		}

		sql = "select latitude,longitude,address,phone1,phone2,branchCode,name,locationType,services,workingHour"
				+ " from Locator  where 1=1 " + locType;

		if (!id.equals("")) {
			sql += " and branchcode = ? ";
		}
		ps = l_Conn.prepareStatement(sql);
		int ii = 1;
		if (!id.equals("")) {
			ps.setString(ii++, id);
		}
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ATMLocation data = new ATMLocation();
			data.setLatitude(rs.getString("latitude"));
			data.setLongitude(rs.getString("longitude"));
			data.setAddress(rs.getString("address"));
			data.setPhone1(rs.getString("phone1"));
			data.setPhone2(rs.getString("phone2"));
			if (rs.getString("branchcode") == null || rs.getString("branchcode") == "") {
				data.setBranchCode("");
			} else {
				data.setBranchCode(rs.getString("branchcode"));
			}
			data.setName(rs.getString("name"));
			data.setLocationType(rs.getString("locationType"));
			data.setServices(rs.getString("services"));
			data.setWorkingHour(rs.getString("workingHour"));
			dataList.add(data);
		}

		LocationData locData = new LocationData();
		ArrayList<ATMLocationResData> atmDataList = new ArrayList<ATMLocationResData>();
		for (int j = 0; j < typeList.size(); j++) {
			ArrayList<ATMLocation> dataListl = new ArrayList<ATMLocation>();
			ATMLocationResData data = new ATMLocationResData();
			for (ATMLocation atm : dataList) {
				if (typeList.get(j).getCode().equals(atm.getLocationType())) {
					dataListl.add(atm);
				}
			}
			data.setLocationType(typeList.get(j).getCode());
			data.setLocationName(typeList.get(j).getDesc());
			data.setDataList(dataListl);
			atmDataList.add(data);
		}

		ATMLocationResData[] atmArr = new ATMLocationResData[atmDataList.size()];
		for (int j = 0; j < atmDataList.size(); j++) {
			atmArr[j] = atmDataList.get(j);
		}
		if (atmDataList.size() > 0) {
			locData.setData(atmArr);
			locData.setCode("0000");
			locData.setDesc("Successfully");
		} else {
			locData.setCode("0014");
			locData.setDesc("Parameter is not found");
		}
		ps.close();
		rs.close();
		return locData;
	}

	public ATMLocation getATMLocatorByID(String atmId, Connection l_Conn) throws SQLException {

		ATMLocation ret = new ATMLocation();
		String l_Query = "SELECT Latitude,Longitude,Address,Phone1,Phone2,BranchCode,Name,LocationType,t1 FROM Locator WHERE t1=?";
		PreparedStatement pstmt = l_Conn.prepareStatement(l_Query);
		pstmt.setString(1, atmId);
		ResultSet rs = pstmt.executeQuery();

		while (rs.next()) {
			ret.setLatitude(rs.getString("Latitude"));
			ret.setLongitude(rs.getString("Longitude"));
			ret.setAddress(rs.getString("Address"));
			ret.setPhone1(rs.getString("Phone1"));
			ret.setPhone2(rs.getString("Phone2"));
			ret.setBranchCode(rs.getString("BranchCode"));
			ret.setName(rs.getString("Name"));
			ret.setLocationType(rs.getString("LocationType"));
			ret.setT1(rs.getString("t1"));
		}

		return ret;
	}

	public ATMLocationList getATMLocatorList(String searchtext, int pageSize, int currentPage, Connection conn)
			throws SQLException {
		String searchText = searchtext.replace("'", "''");
		ArrayList<ATMLocation> datalist = new ArrayList<ATMLocation>();
		ATMLocationList ret = new ATMLocationList();
		int startPage = (currentPage - 1) * pageSize;
		int endPage = pageSize + startPage;
		boolean text = false;
		String whereClause = "";
		if (searchText.equals("")) {
			whereClause = " ";
		} else {
			if (searchText.equalsIgnoreCase("ATM Locator") || searchText.equalsIgnoreCase("Branch Locator")) {
				if (searchText.equalsIgnoreCase("All Locator")) {
					searchText = "All";
				}else if (searchText.equalsIgnoreCase("ATM Locator")) {
					searchText = "ATM";
				} else if (searchText.equalsIgnoreCase("Branch Locator")) {
					searchText = "Branch";
				}else if (searchText.equalsIgnoreCase("Agent Locator")) {
					searchText = "Agent";
				}else if (searchText.equalsIgnoreCase("Merchant Locator")) {
					searchText = "Merchant";
				}
				whereClause = " where (t1 = ?  or Name = ? or Latitude = ? or Longitude =?  Or  Address = ? or"
						+ " Phone1 =? Or Phone2 = ? or BranchCode = ? or LocationType = ?)";
				text = true;
			} else {
				whereClause = " where (t1 like ? or Name like ? or Latitude like ? or Longitude like ? Or Address like ?"
						+ " or Phone1 like ? Or Phone2 like ? or BranchCode like ? or LocationType = ?)";
			}
		}

		String l_Query = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY p.[t1],p.[Name],p.[LocationType]) AS RowNum, "
				+ " p.[t1],p.[Name], p.[Latitude], p.[Longitude], p.[Address], p.[Phone1], p.[Phone2], p.[BranchCode],p.[LocationType]"
				+ " FROM [Locator] AS p " + whereClause + " ) AS RowConstrainedResult " + " WHERE (RowNum > "
				+ startPage + " AND RowNum <= " + endPage + ")";
		PreparedStatement ps = conn.prepareStatement(l_Query);

		if (searchText.equals("")) {

		} else if (text) {
			for (int i = 1; i < 10; i++) {
				ps.setString(i, searchText);
			}
		} else {
			for (int i = 1; i < 9; i++) {
				ps.setString(i, "%" + searchText + "%");
			}
			ps.setString(9, searchText);
		}
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ATMLocation data = new ATMLocation();
			data.setT1(rs.getString("t1"));
			data.setName(rs.getString("Name"));
			data.setLatitude(rs.getString("Latitude"));
			data.setLongitude(rs.getString("Longitude"));
			data.setAddress(rs.getString("Address"));
			data.setPhone1(rs.getString("Phone1"));
			data.setPhone2(rs.getString("Phone2"));
			data.setBranchCode(rs.getString("BranchCode"));
			data.setLocationType(rs.getString("LocationType"));
			datalist.add(data);
		}

		ATMLocation[] dataarr = new ATMLocation[datalist.size()];
		for (int i = 0; i < datalist.size(); i++) {
			dataarr[i] = datalist.get(i);
		}

		ret.setData(dataarr);
		ret.setCurrentPage(currentPage);
		ret.setPageSize(pageSize);
		ret.setSearchText(searchtext);

		PreparedStatement pstm = conn.prepareStatement("Select COUNT(*) As total FROM Locator " + whereClause);
		if (searchText.equals("")) {

		} else if (text) {
			for (int i = 1; i < 10; i++) {
				pstm.setString(i, searchText);
			}
		} else {
			for (int i = 1; i < 9; i++) {
				pstm.setString(i, "%" + searchText + "%");
			}
			pstm.setString(9, searchText);
		}
		ResultSet result = pstm.executeQuery();
		result.next();
		ret.setTotalCount(result.getInt("total"));

		return ret;
	}

	public String getAutoGenCode(Connection con) {
		String id = "";
		String sql = "SELECT CODE,Serial,n1 FROM ATMSetting WHERE ID = (SELECT MAX(ID) FROM ATMSetting)";
		Statement stmt;
		String codestr = "";
		String serial = "";
		String newSerial = "";
		int count = 0;
		int code = 0;
		try {
			stmt = con.createStatement();
			ResultSet res = stmt.executeQuery(sql);
			while (res.next()) {
				codestr = res.getString("CODE");
				serial = res.getString("Serial");
				count = res.getInt("n1");

			}
			code = Integer.parseInt(serial) + 1;
			newSerial = leadZero(code, count);
			id = codestr + newSerial;
			String query = "INSERT INTO ATMSetting ( CODE ,Serial ,ATMid,t1,t2,n1,n2) VALUES (?,?,?,?,?,?,?)";
			PreparedStatement psmt = con.prepareStatement(query);
			int j = 1;
			psmt.setString(j++, codestr);
			psmt.setString(j++, newSerial);
			psmt.setString(j++, id);
			psmt.setString(j++, "");
			psmt.setString(j++, "");
			psmt.setInt(j++, count);
			psmt.setInt(j++, 0);
			if (psmt.executeUpdate() > 0) {
				System.out.println("Updated successfully");

			} else {
				System.out.println("update failed");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}

	public Lov3 getLocationCbo(Connection conn) throws SQLException {
		Lov3 lov3 = new Lov3();
		Ref[] refarray = null;
		ArrayList<Ref> reflist = new ArrayList<Ref>();
		String query = "select code,Description from lovdetails where hkey = ( select syskey from LOVHeader where Description like 'Location Type')";
		PreparedStatement stmt = conn.prepareStatement(query);
		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			Ref ref = new Ref();
			ref.setvalue(rs.getString("code"));
			ref.setcaption(rs.getString("Description"));
			reflist.add(ref);
		}

		stmt.close();
		rs.close();

		refarray = new Ref[reflist.size()];
		for (int i = 0; i < reflist.size(); i++) {
			refarray[i] = reflist.get(i);
		}
		lov3.setRefLocType(refarray);
		return lov3;
	}

	public boolean isCodeExist(ATMLocation obj, Connection conn) {
		ArrayList<ATMLocation> dataList = new ArrayList<ATMLocation>();
		try {
			String l_Query = " SELECT * FROM Locator WHERE t1 = ? ";

			PreparedStatement pstmt = conn.prepareStatement(l_Query);
			pstmt.setString(1, obj.getT1());
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				ATMLocation data = new ATMLocation();
				data.setT1(rs.getString("t1"));
				dataList.add(data);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (dataList.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public Result saveATMLocator(ATMLocation data, Connection aConn) {

		Result ret = new Result();
		String atmId = "";
		atmId = getAutoGenCode(aConn);
		data.setT1(atmId);
		if (!isCodeExist(data, aConn)) {
			try {
				String query = "INSERT INTO Locator ( CreatedDate ,ModifiedDate ,Latitude ,Longitude ,Address ,Phone1 ,Phone2 ,BranchCode ,Name ,LocationType,t1) "
						+ "VALUES (?,?,?,?,?,?,?,?,?,?,?)";
				PreparedStatement psmt = aConn.prepareStatement(query);

				int i = 1;
				psmt.setString(i++, data.getCreatedDate());
				psmt.setString(i++, data.getModifiedDate());
				psmt.setString(i++, data.getLatitude());
				psmt.setString(i++, data.getLongitude());
				psmt.setString(i++, data.getAddress());
				psmt.setString(i++, data.getPhone1());
				psmt.setString(i++, data.getPhone2());
				psmt.setString(i++, data.getBranchCode());
				psmt.setString(i++, data.getName());
				psmt.setString(i++, data.getLocationType());
				psmt.setString(i++, data.getT1());

				if (psmt.executeUpdate() > 0) {
					ret.setMsgCode("0000");
					ret.setState(true);
					ret.setUserid(data.getT1());
				} else {
					ret.setMsgCode("0014");
					ret.setState(false);
				}

			} catch (Exception e) {
				e.printStackTrace();
				ret.setMsgCode("0014");
				ret.setState(false);
				ret.setMsgCode(e.getMessage());

			}
		} else {
			ret.setMsgCode("0015");
			ret.setState(false);
		}
		return ret;

	}

	public Result updateATMLocator(ATMLocation data, Connection l_Conn) {
		Result ret = new Result();
		try {
			String query = "UPDATE Locator SET ModifiedDate =?, Latitude=?,"
					+ " Longitude=?, Address=?, Phone1=?, Phone2=?, BranchCode=?, Name=?,LocationType=?  WHERE t1=?";

			PreparedStatement pstmt = l_Conn.prepareStatement(query);
			int i = 1;
			pstmt.setString(i++, data.getModifiedDate());
			pstmt.setString(i++, data.getLatitude());
			pstmt.setString(i++, data.getLongitude());
			pstmt.setString(i++, data.getAddress());
			pstmt.setString(i++, data.getPhone1());
			pstmt.setString(i++, data.getPhone2());
			pstmt.setString(i++, data.getBranchCode());
			pstmt.setString(i++, data.getName());
			pstmt.setString(i++, data.getLocationType());
			pstmt.setString(i++, data.getT1());// ATM id

			if (pstmt.executeUpdate() > 0) {
				ret.setMsgDesc("Updated successfully");
				ret.setState(true);
				ret.setUserid(data.getT1());
			} else {
				ret.setMsgDesc("Updated fail");
				ret.setState(false);
			}

			pstmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

}
