package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;
import com.nirvasoft.rp.data.TicketArr1;
import com.nirvasoft.rp.data.TicketData1;
import com.nirvasoft.rp.data.TicketListingDataList;
import com.nirvasoft.rp.data.TicketResponse;
import com.nirvasoft.rp.data.TicketResponseList;
import com.nirvasoft.rp.shared.FilterData;
import com.nirvasoft.rp.shared.FilterDataset;

public class TicketAdmDao {

	public static DBRecord defineTckDetail() {
		DBRecord ret = new DBRecord();
		ret.setTableName("TicketDetail"); // Table Name
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("autokey", (byte) 2));
		ret.getFields().add(new DBField("createdDate", (byte) 5));
		ret.getFields().add(new DBField("modifiedDate", (byte) 5));
		ret.getFields().add(new DBField("userID", (byte) 5));
		ret.getFields().add(new DBField("date", (byte) 5));
		ret.getFields().add(new DBField("time", (byte) 5));
		ret.getFields().add(new DBField("shootdate", (byte) 5));
		ret.getFields().add(new DBField("shoottime", (byte) 5));
		ret.getFields().add(new DBField("T1", (byte) 5));
		ret.getFields().add(new DBField("T2", (byte) 5));
		ret.getFields().add(new DBField("T3", (byte) 5));
		ret.getFields().add(new DBField("T4", (byte) 5));
		ret.getFields().add(new DBField("T5", (byte) 5));
		ret.getFields().add(new DBField("T6", (byte) 5));
		ret.getFields().add(new DBField("T7", (byte) 5));
		ret.getFields().add(new DBField("T8", (byte) 5));
		ret.getFields().add(new DBField("T9", (byte) 5));
		ret.getFields().add(new DBField("T10", (byte) 5));
		ret.getFields().add(new DBField("T11", (byte) 5));
		ret.getFields().add(new DBField("T12", (byte) 5));
		ret.getFields().add(new DBField("T13", (byte) 5));
		ret.getFields().add(new DBField("T14", (byte) 5));
		ret.getFields().add(new DBField("T15", (byte) 5));
		ret.getFields().add(new DBField("T16", (byte) 5));
		ret.getFields().add(new DBField("T17", (byte) 5));
		ret.getFields().add(new DBField("T18", (byte) 5));
		ret.getFields().add(new DBField("T19", (byte) 5));
		ret.getFields().add(new DBField("T20", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 1));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));

		return ret;
	}

	/* atn */
	public static boolean isCodeExist(TicketData1 obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(defineTckDetail(),
				" where n2<>4 AND  n2 = 1 AND autokey = " + obj.getAutokey() + " ", "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static DBRecord setDBRecordTckDetail(TicketData1 aObj) {
		DBRecord ret = defineTckDetail();
		ret.setValue("createdate", aObj.getCreatedDate());
		ret.setValue("modifieddate", aObj.getModifiedDate());
		ret.setValue("userid", aObj.getUserID());
		ret.setValue("date", aObj.getDate());
		ret.setValue("time", aObj.getTime());
		ret.setValue("shootdate", aObj.getShootDate());
		ret.setValue("shoottime", aObj.getShootTime());
		ret.setValue("T1", aObj.getT1());
		ret.setValue("T2", aObj.getT2());
		ret.setValue("T3", aObj.getT3());
		ret.setValue("T4", aObj.getT4());
		ret.setValue("T5", aObj.getT5());
		ret.setValue("T6", aObj.getT6());
		ret.setValue("T7", aObj.getT7());
		ret.setValue("T8", aObj.getT8());
		ret.setValue("T9", aObj.getT9());
		ret.setValue("T10", aObj.getT10());
		ret.setValue("T11", aObj.getT11());
		ret.setValue("T12", aObj.getT12());
		ret.setValue("T13", aObj.getT13());
		ret.setValue("T14", aObj.getT14());
		ret.setValue("T15", aObj.getT15());
		ret.setValue("T16", aObj.getT16());
		ret.setValue("T17", aObj.getT17());
		ret.setValue("T18", aObj.getT18());
		ret.setValue("T19", aObj.getT19());
		ret.setValue("T20", aObj.getT20());
		ret.setValue("n1", aObj.getN1());
		ret.setValue("n2", aObj.getN2());
		ret.setValue("n3", aObj.getN3());
		ret.setValue("n4", aObj.getN4());
		ret.setValue("n5", aObj.getN5());
		ret.setValue("n6", aObj.getN6());
		ret.setValue("n7", aObj.getN7());
		ret.setValue("n8", aObj.getN8());
		ret.setValue("n9", aObj.getN9());
		ret.setValue("n10", aObj.getN10());
		return ret;
	}

	public TicketData1 deleteTicket(TicketResponse request, Connection conn) throws SQLException {
		TicketData1 response = new TicketData1();

		PreparedStatement preparedStatement = null;

		try {
			String query = "DELETE FROM Ticket WHERE AutoKey = ?;";
			preparedStatement = conn.prepareStatement(query);
			preparedStatement.setLong(1, Long.parseLong(request.getAutoKey()));

			if (preparedStatement.executeUpdate() > 0) {
				response.setCode("0000");
				response.setDesc("Deleted successfully.");
				response.setState(true);
			} else {
				response.setCode("0014");
				response.setDesc("Deleted fail.");
				response.setState(false);
			}
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}

		return response;
	}

	// atn
	// public TicketArr1 getTickets(TicketArr1 tdata, String tstatus, String
	// tregion, Connection conn)
	// throws SQLException {
	// ArrayList<TicketData1> tarrlist = new ArrayList<TicketData1>();
	// TicketData1[] ticketarr = null;
	// TicketArr1 ticketres = new TicketArr1();
	// String searchText = tdata.getSearchText().replace("'", "''");
	// String query = "";
	// String whereClause = "";
	// String regioncode = "";
	// PreparedStatement ps;
	// int startPage = (tdata.getCurrentPage() - 1) * tdata.getPageSize();
	// int endPage = tdata.getPageSize() + startPage;
	// whereClause = "where n2=1 and n2<>4";
	//
	// if ((!searchText.isEmpty()) && (searchText.trim().length() > 0)) {
	// whereClause += " and (t1 like '%" + searchText + "%' )";
	// }
	// if (!tdata.isAlldate()) {
	// whereClause += " and date >= '" + tdata.getaFromDate() + "' and date <=
	// '" + tdata.getaToDate() + "'";
	// }
	// /*
	// * if (!data1.getCusttypeFilter().trim().equalsIgnoreCase("undefined")
	// * && !data1.getCusttypeFilter().trim().equals("")) { whereClause +=
	// * " and CustomerDesc = N'" + data1.getCusttypeFilter() + "'"; }
	// */
	//
	// String qry = "select t5 from UVM005_A where t1='" + tdata.getUserID() +
	// "' ";
	// ps = conn.prepareStatement(qry);
	// ResultSet rs = ps.executeQuery();
	// while (rs.next()) {
	// regioncode = rs.getString("t5");
	// }
	// if (tstatus.equals("All")) {
	// if (regioncode.equals("00000000") && tregion.equals("00000000")) {
	// query = "SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY autokey desc)
	// AS RowNum,* from "
	// + "(select * from TicketDetail " + whereClause + " )b)AS
	// RowContraintResult WHERE "
	// + "(RowNum > " + startPage + " AND RowNum <= " + endPage + ")";
	// } else {
	// query = "SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY autokey desc)
	// AS RowNum,* from "
	// + "(select * from TicketDetail " + whereClause + " and t12='" + tregion
	// + "' )b)AS RowContraintResult WHERE " + "(RowNum > " + startPage + " AND
	// RowNum <= " + endPage
	// + ")";
	// }
	// } else {
	// if (regioncode.equals("00000000") && tregion.equals("00000000")) {
	// query = "SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY autokey desc)
	// AS RowNum,* from "
	// + "(select * from TicketDetail " + whereClause + " and t10='" + tstatus
	// + "' )b)AS RowContraintResult WHERE " + "(RowNum > " + startPage + " AND
	// RowNum <= " + endPage
	// + ")";
	// } else {
	// query = "SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY autokey desc)
	// AS RowNum,* from "
	// + "(select * from TicketDetail " + whereClause + " and t10='" + tstatus +
	// "' and t12='"
	// + tregion + "' )b)AS RowContraintResult WHERE " + "(RowNum > " +
	// startPage + " AND RowNum <= "
	// + endPage + ")";
	// }
	// }
	// ps = conn.prepareStatement(query);
	// rs = ps.executeQuery();
	// while (rs.next()) {
	// TicketData1 retdata = new TicketData1();
	// retdata = readTicket(rs);
	// tarrlist.add(retdata);
	// }
	// if (tarrlist.size() > 0) {
	// ticketarr = new TicketData1[tarrlist.size()];
	// for (int i = 0; i < tarrlist.size(); i++) {
	// ticketarr[i] = tarrlist.get(i);
	// }
	// }
	// String totalqry = "";
	// if (tstatus.equals("All")) {
	// if (regioncode.equals("00000000") && tregion.equals("00000000")) {
	// totalqry = "SELECT COUNT(*) AS tot FROM TicketDetail " + whereClause;
	// } else {
	// totalqry = "SELECT COUNT(*) AS tot FROM TicketDetail " + whereClause +
	// "and t12='" + tregion + "'";
	// }
	// } else {
	// if (regioncode.equals("00000000") && tregion.equals("00000000")) {
	// totalqry = "SELECT COUNT(*) AS tot FROM TicketDetail " + whereClause +
	// "and t10='" + tstatus + "'";
	// } else {
	// totalqry = "SELECT COUNT(*) AS tot FROM TicketDetail " + whereClause +
	// "and t10='" + tstatus
	// + "' and t12='" + tregion + "'";
	// }
	// }
	// ps = conn.prepareStatement(totalqry);
	// ResultSet rSet = ps.executeQuery();
	// rSet.next();
	// ticketres.setTotalCount(rSet.getInt("tot"));
	// ticketres.setTicketData(ticketarr);
	// ticketres.setAlldate(tdata.isAlldate());
	// ticketres.setaFromDate(tdata.getaFromDate());
	// ticketres.setaToDate(tdata.getaToDate());
	// ticketres.setSearchText(searchText);
	// ticketres.setCurrentPage(tdata.getCurrentPage());
	// ticketres.setPageSize(tdata.getPageSize());
	//
	// return ticketres;
	// }

	// acmy
	public TicketArr1 getAllTicket(TicketArr1 tdata, String ticketRegion, Connection conn) throws SQLException {
		// record status 1 or 4. 4 is delete. 1 is valid.
		String whereClause = "where n2=1 and n2<>4 ";

		// if all date true, no need to check from date and to date
		if (!tdata.isAlldate()) {
			whereClause += " and date >= '" + tdata.getaFromDate() + "' and date <= '" + tdata.getaToDate() + "' ";
		}

		// if ticket status is all, no need to check other status
		// if not, need to check status chosen by user
		if ((!tdata.isAllStatus())
				&& (tdata.isNewStatus() || tdata.isWipStatus() || tdata.isClosedStatus() || tdata.isRejectedStatus())) {
			whereClause += "and ";

			String statusEach = "";

			if (tdata.isNewStatus()) {
				statusEach = "t10='New' ";
			}

			if (tdata.isWipStatus()) {
				if (statusEach.equals(""))
					statusEach = "t10='WIP' ";
				else
					statusEach += "or t10='WIP' ";
			}

			if (tdata.isClosedStatus()) {
				if (statusEach.equals(""))
					statusEach = "t10='Closed' ";
				else
					statusEach += "or t10='Closed' ";
			}

			if (tdata.isRejectedStatus()) {
				if (statusEach.equals(""))
					statusEach = "t10='Rejected' ";
				else
					statusEach += "or t10='Rejected' ";
			}

			whereClause += statusEach;
		}

		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		TicketArr1 ticketres = null;

		try {
			// check user can access which region code
			String query = "select t5 from UVM005_A where t1='" + tdata.getUserID() + "';";
			ps1 = conn.prepareStatement(query);
			rs1 = ps1.executeQuery();
			String regioncode = "";

			while (rs1.next()) {
				regioncode = rs1.getString("t5");
			}
			if (regioncode.equals("00000000") && ticketRegion.equals("00000000")) {
				query = "select * from TicketDetail " + whereClause + ";";
			} else {
				query = "select * from TicketDetail " + whereClause + " and t12='" + ticketRegion + "';";
			}

			ps2 = conn.prepareStatement(query);
			rs2 = ps2.executeQuery();
			ArrayList<TicketData1> tarrlist = new ArrayList<TicketData1>();

			while (rs2.next()) {
				TicketData1 ret = new TicketData1();
				int i = 1;
				ret.setAutokey(Long.parseLong(rs2.getString(i++)));
				ret.setSyskey(Long.parseLong(rs2.getString(i++)));
				ret.setCreatedDate(rs2.getString(i++));
				ret.setModifiedDate(rs2.getString(i++));
				ret.setUserID(rs2.getString(i++));
				ret.setDate(rs2.getString(i++));
				ret.setTime(rs2.getString(i++));

				String shootDate = rs2.getString(i++);
				String shootDay = shootDate.substring(8);
				String shootMonth = shootDate.substring(5, 7);
				String shootYear = shootDate.substring(0, 4);
				shootDate = shootDay + "/" + shootMonth + "/" + shootYear;
				ret.setShootDate(shootDate);

				String shootTime = rs2.getString(i++);
				shootTime = shootTime.substring(shootTime.indexOf(" ") + 1);
				ret.setShootTime(shootTime);

				ret.setT1(rs2.getString(i++));
				ret.setT2(rs2.getString(i++));
				ret.setT3(rs2.getString(i++));
				ret.setT4(rs2.getString(i++));
				ret.setT5(rs2.getString(i++));
				ret.setT6(rs2.getString(i++));
				ret.setT7(rs2.getString(i++));
				ret.setT8(rs2.getString(i++));
				ret.setT9(rs2.getString(i++));
				ret.setT10(rs2.getString(i++));
				ret.setT11(rs2.getString(i++));
				ret.setT12(rs2.getString(i++));
				ret.setT13(rs2.getString(i++));
				ret.setT14(rs2.getString(i++));
				ret.setT15(rs2.getString(i++));
				ret.setT16(rs2.getString(i++));
				ret.setT17(rs2.getString(i++));
				ret.setT18(rs2.getString(i++));
				ret.setT19(rs2.getString(i++));
				ret.setT20(rs2.getString(i++));
				ret.setN1(rs2.getLong(i++));
				ret.setN2(rs2.getInt(i++));
				ret.setN3(rs2.getLong(i++));
				ret.setN4(rs2.getLong(i++));
				ret.setN5(rs2.getLong(i++));
				ret.setN6(rs2.getLong(i++));
				ret.setN7(rs2.getLong(i++));
				ret.setN8(rs2.getLong(i++));
				ret.setN9(rs2.getLong(i++));
				ret.setN10(rs2.getLong(i++));
				tarrlist.add(ret);
			}

			TicketData1[] ticketarr = null;

			if (tarrlist.size() > 0) {
				ticketarr = new TicketData1[tarrlist.size()];

				for (int i = 0; i < tarrlist.size(); i++) {
					ticketarr[i] = tarrlist.get(i);
				}
			}

			ticketres = new TicketArr1();
			ticketres.setTicketData(ticketarr);
			ticketres.setAlldate(tdata.isAlldate());
			ticketres.setAllStatus(tdata.isAllStatus());
			ticketres.setNewStatus(tdata.isNewStatus());
			ticketres.setWipStatus(tdata.isWipStatus());
			ticketres.setClosedStatus(tdata.isClosedStatus());
			ticketres.setRejectedStatus(tdata.isRejectedStatus());
		} finally {
			ps1.close();
			ps2.close();
			rs1.close();
			rs2.close();
		}

		return ticketres;
	}

	public TicketResponseList getAllTicketList(String[] ticketList, Connection conn) throws SQLException {
		TicketResponseList response = null;

		String query = "SELECT * FROM [WLDC].[dbo].[Ticket]";

		if (ticketList.length > 0) {
			query = query + " WHERE TicketNo='" + ticketList[0] + "'";

			for (int i = 1; i < ticketList.length; i++) {
				query = query + " OR TicketNo='" + ticketList[i] + "'";
			}
		}

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			preparedStatement = conn.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			resultSet = preparedStatement.executeQuery();

			int totalRow = 0;
			resultSet.last();
			totalRow = resultSet.getRow();

			if (totalRow == 0) {
				response = new TicketResponseList();
				response.setMsgCode("0009");
				response.setMsgDesc("There is no ticket list.");
				response.setTicketList(null);
			}

			if (totalRow != 0) {
				resultSet.beforeFirst();
				TicketResponse[] dataarr = new TicketResponse[totalRow];
				int x = 0;

				while (resultSet.next()) {
					TicketResponse data = new TicketResponse();
					data.setAutoKey(resultSet.getString("AutoKey"));
					data.setSyskey(resultSet.getString("Syskey"));
					data.setCreatedDate(resultSet.getString("CreatedDate"));
					data.setModifiedDate(resultSet.getString("ModifiedDate"));
					data.setCode(resultSet.getString("Code"));
					data.setTicketNo(resultSet.getString("TicketNo"));
					data.setSenderKey(resultSet.getString("SenderKey"));
					data.setSenderName(resultSet.getString("SenderName"));
					data.setMessage(resultSet.getString("Message"));
					data.setLocation(resultSet.getString("Location"));
					data.setShortDate(resultSet.getString("ShortDate"));
					data.setShortTime(resultSet.getString("ShortTime"));
					data.setImage(resultSet.getString("Image"));
					data.setChannelKey(resultSet.getString("ChannelKey"));
					data.setReceiverKey(resultSet.getString("ReceiverKey"));
					data.setT1(resultSet.getString("T1"));
					data.setT2(resultSet.getString("T2"));
					data.setT3(resultSet.getString("T3"));
					data.setT4(resultSet.getString("T4"));
					data.setT5(resultSet.getString("T5"));
					data.setT6(resultSet.getString("T6"));
					data.setT7(resultSet.getString("T7"));
					data.setT8(resultSet.getString("T8"));
					data.setT9(resultSet.getString("T9"));
					data.setT10(resultSet.getString("T10"));
					data.setT11(resultSet.getString("T11"));
					data.setT12(resultSet.getString("T12"));
					data.setT13(resultSet.getString("T13"));
					data.setT14(resultSet.getString("T14"));
					data.setT15(resultSet.getString("T15"));
					data.setT16(resultSet.getString("T16"));
					data.setT17(resultSet.getString("T17"));
					data.setT18(resultSet.getString("T18"));
					data.setT19(resultSet.getString("T19"));
					data.setT20(resultSet.getString("T20"));
					data.setN1(resultSet.getString("N1"));
					data.setN2(resultSet.getString("N2"));
					data.setN3(resultSet.getString("N3"));
					data.setN4(resultSet.getString("N4"));
					data.setN5(resultSet.getString("N5"));
					data.setN6(resultSet.getString("N6"));
					data.setN7(resultSet.getString("N7"));
					data.setN8(resultSet.getString("N8"));
					data.setN9(resultSet.getString("N9"));
					data.setN10(resultSet.getString("N10"));
					data.setN11(resultSet.getString("N11"));
					data.setN12(resultSet.getString("N12"));
					data.setN13(resultSet.getString("N13"));
					data.setN14(resultSet.getString("N14"));
					data.setN15(resultSet.getString("N15"));
					data.setN16(resultSet.getString("N16"));
					data.setN17(resultSet.getString("N17"));
					data.setN18(resultSet.getString("N18"));
					data.setN19(resultSet.getString("N19"));
					data.setN20(resultSet.getString("N20"));

					dataarr[x++] = data;
				}

				preparedStatement.close();
				resultSet.close();
				query = "";
				preparedStatement = null;
				resultSet = null;

				response = new TicketResponseList();
				response.setMsgCode("0000");
				response.setMsgDesc("Selected successfully.");
				response.setTicketList(dataarr);
			}
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}

			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}

		return response;
	}

	public TicketArr1 getAllTickets(TicketArr1 tdata, String ticketChannel, String searchText, Connection conn)
			throws SQLException {
		// record status 1 or 4. 4 is delete. 1 is valid.
		String whereClause = "where n2=1 and n2<>4 ";

		// if all date true, no need to check from date and to date
		if (!tdata.isAlldate()) {
			whereClause += " and date >= '" + tdata.getaFromDate() + "' and date <= '" + tdata.getaToDate() + "' ";
		}

		// if ticket status is all, no need to check other status
		// if not, need to check status chosen by user
		if ((!tdata.isAllStatus())
				&& (tdata.isNewStatus() || tdata.isWipStatus() || tdata.isClosedStatus() || tdata.isRejectedStatus())) {
			whereClause += "and ";

			String statusEach = "";

			if (tdata.isNewStatus()) {
				statusEach = "t10='New' ";
			}

			if (tdata.isWipStatus()) {
				if (statusEach.equals(""))
					statusEach = "t10='WIP' ";
				else
					statusEach += "or t10='WIP' ";
			}

			if (tdata.isClosedStatus()) {
				if (statusEach.equals(""))
					statusEach = "t10='Closed' ";
				else
					statusEach += "or t10='Closed' ";
			}

			if (tdata.isRejectedStatus()) {
				if (statusEach.equals(""))
					statusEach = "t10='Rejected' ";
				else
					statusEach += "or t10='Rejected' ";
			}

			whereClause += statusEach;
		}

		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		TicketArr1 ticketres = null;

		try {

			String query = "";
			if (ticketChannel.equals("")) {
				query = "select * from TicketDetail " + whereClause + ";";
			} else {
				query = "select * from TicketDetail " + whereClause + " and t12='" + ticketChannel + "';";
			}

			ps2 = conn.prepareStatement(query);
			rs2 = ps2.executeQuery();
			ArrayList<TicketData1> tarrlist = new ArrayList<TicketData1>();

			while (rs2.next()) {
				TicketData1 ret = new TicketData1();
				int i = 1;
				ret.setAutokey(Long.parseLong(rs2.getString(i++)));
				ret.setSyskey(Long.parseLong(rs2.getString(i++)));
				ret.setCreatedDate(rs2.getString(i++));
				ret.setModifiedDate(rs2.getString(i++));
				ret.setUserID(rs2.getString(i++));
				ret.setDate(rs2.getString(i++));
				ret.setTime(rs2.getString(i++));

				String shootDate = rs2.getString(i++);
				String shootDay = shootDate.substring(8);
				String shootMonth = shootDate.substring(5, 7);
				String shootYear = shootDate.substring(0, 4);
				shootDate = shootDay + "/" + shootMonth + "/" + shootYear;
				ret.setShootDate(shootDate);

				String shootTime = rs2.getString(i++);
				shootTime = shootTime.substring(shootTime.indexOf(" ") + 1);
				ret.setShootTime(shootTime);

				ret.setT1(rs2.getString(i++));
				ret.setT2(rs2.getString(i++));
				ret.setT3(rs2.getString(i++));
				ret.setT4(rs2.getString(i++));
				ret.setT5(rs2.getString(i++));
				ret.setT6(rs2.getString(i++));
				ret.setT7(rs2.getString(i++));
				ret.setT8(rs2.getString(i++));
				ret.setT9(rs2.getString(i++));
				ret.setT10(rs2.getString(i++));
				ret.setT11(rs2.getString(i++));
				ret.setT12(rs2.getString(i++));
				ret.setT13(rs2.getString(i++));
				ret.setT14(rs2.getString(i++));
				ret.setT15(rs2.getString(i++));
				ret.setT16(rs2.getString(i++));
				ret.setT17(rs2.getString(i++));
				ret.setT18(rs2.getString(i++));
				ret.setT19(rs2.getString(i++));
				ret.setT20(rs2.getString(i++));
				ret.setN1(rs2.getLong(i++));
				ret.setN2(rs2.getInt(i++));
				ret.setN3(rs2.getLong(i++));
				ret.setN4(rs2.getLong(i++));
				ret.setN5(rs2.getLong(i++));
				ret.setN6(rs2.getLong(i++));
				ret.setN7(rs2.getLong(i++));
				ret.setN8(rs2.getLong(i++));
				ret.setN9(rs2.getLong(i++));
				ret.setN10(rs2.getLong(i++));
				tarrlist.add(ret);
			}

			TicketData1[] ticketarr = null;

			if (tarrlist.size() > 0) {
				ticketarr = new TicketData1[tarrlist.size()];

				for (int i = 0; i < tarrlist.size(); i++) {
					ticketarr[i] = tarrlist.get(i);
				}
			}

			ticketres = new TicketArr1();
			ticketres.setTicketData(ticketarr);
			ticketres.setAlldate(tdata.isAlldate());
			ticketres.setAllStatus(tdata.isAllStatus());
			ticketres.setNewStatus(tdata.isNewStatus());
			ticketres.setWipStatus(tdata.isWipStatus());
			ticketres.setClosedStatus(tdata.isClosedStatus());
			ticketres.setRejectedStatus(tdata.isRejectedStatus());
		} finally {
			// ps1.close();
			ps2.close();
			// rs1.close();
			rs2.close();
		}

		return ticketres;
	}

	// atn
	/*
	 * public TicketResult insertmyTicket(TicketData1 request, Connection conn)
	 * throws SQLException { TicketResult response = new TicketResult();
	 * 
	 * PreparedStatement preparedStatement = null; try { if
	 * (!isCodeExist(request, conn)) { String tsql =
	 * DBMgr.insertString(defineTckDetail(), conn); PreparedStatement stmt =
	 * conn.prepareStatement(tsql); DBRecord dbr =
	 * setDBRecordTckDetail(request); DBMgr.setValues(stmt, dbr); int count =
	 * stmt.executeUpdate(); if (count > 0) { response.setMsgCode("0000");
	 * response.setMsgDesc("Saved successfully."); response.setState(true); }
	 * else { response.setMsgCode("0014"); response.setMsgDesc("Saved fail.");
	 * response.setState(false); } } String sql =
	 * "select TOP 1 id FROM ticketDetail2 ORDER BY id DESC"; PreparedStatement
	 * ps = conn.prepareStatement(sql); ResultSet rs = ps.executeQuery(); if
	 * (rs.next()) { request.setId(rs.getLong("id")); } } finally { if
	 * (preparedStatement != null) { preparedStatement.close(); } } return
	 * response; }
	 */

	public TicketArr1 getAllUsertypes(TicketArr1 tdata, String ticketUsertype, Connection conn) throws SQLException {
		// record status 1 or 4. 4 is delete. 1 is valid.
		String whereClause = "where n2=1 and n2<>4 ";

		// if all date true, no need to check from date and to date
		if (!tdata.isAlldate()) {
			whereClause += " and date >= '" + tdata.getaFromDate() + "' and date <= '" + tdata.getaToDate() + "' ";
		}

		// if ticket status is all, no need to check other status
		// if not, need to check status chosen by user
		if ((!tdata.isAllStatus())
				&& (tdata.isNewStatus() || tdata.isWipStatus() || tdata.isClosedStatus() || tdata.isRejectedStatus())) {
			whereClause += "and ";

			String statusEach = "";

			if (tdata.isNewStatus()) {
				statusEach = "t10='New' ";
			}

			if (tdata.isWipStatus()) {
				if (statusEach.equals(""))
					statusEach = "t10='WIP' ";
				else
					statusEach += "or t10='WIP' ";
			}

			if (tdata.isClosedStatus()) {
				if (statusEach.equals(""))
					statusEach = "t10='Closed' ";
				else
					statusEach += "or t10='Closed' ";
			}

			if (tdata.isRejectedStatus()) {
				if (statusEach.equals(""))
					statusEach = "t10='Rejected' ";
				else
					statusEach += "or t10='Rejected' ";
			}

			whereClause += statusEach;
		}

		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		TicketArr1 ticketres = null;

		try {
			// check user can access which region code
			String query = "";
			if (ticketUsertype.equals("")) {
				query = "select * from TicketDetail " + whereClause + ";";
			} else {
				query = "select * from TicketDetail " + whereClause + " and t16='" + ticketUsertype + "';";
			}

			ps2 = conn.prepareStatement(query);
			rs2 = ps2.executeQuery();
			ArrayList<TicketData1> tarrlist = new ArrayList<TicketData1>();

			while (rs2.next()) {
				TicketData1 ret = new TicketData1();
				int i = 1;
				ret.setAutokey(Long.parseLong(rs2.getString(i++)));
				ret.setSyskey(Long.parseLong(rs2.getString(i++)));
				ret.setCreatedDate(rs2.getString(i++));
				ret.setModifiedDate(rs2.getString(i++));
				ret.setUserID(rs2.getString(i++));
				ret.setDate(rs2.getString(i++));
				ret.setTime(rs2.getString(i++));

				String shootDate = rs2.getString(i++);
				String shootDay = shootDate.substring(8);
				String shootMonth = shootDate.substring(5, 7);
				String shootYear = shootDate.substring(0, 4);
				shootDate = shootDay + "/" + shootMonth + "/" + shootYear;
				ret.setShootDate(shootDate);

				String shootTime = rs2.getString(i++);
				shootTime = shootTime.substring(shootTime.indexOf(" ") + 1);
				ret.setShootTime(shootTime);

				ret.setT1(rs2.getString(i++));
				ret.setT2(rs2.getString(i++));
				ret.setT3(rs2.getString(i++));
				ret.setT4(rs2.getString(i++));
				ret.setT5(rs2.getString(i++));
				ret.setT6(rs2.getString(i++));
				ret.setT7(rs2.getString(i++));
				ret.setT8(rs2.getString(i++));
				ret.setT9(rs2.getString(i++));
				ret.setT10(rs2.getString(i++));
				ret.setT11(rs2.getString(i++));
				ret.setT12(rs2.getString(i++));
				ret.setT13(rs2.getString(i++));
				ret.setT14(rs2.getString(i++));
				ret.setT15(rs2.getString(i++));
				ret.setT16(rs2.getString(i++));
				ret.setT17(rs2.getString(i++));
				ret.setT18(rs2.getString(i++));
				ret.setT19(rs2.getString(i++));
				ret.setT20(rs2.getString(i++));
				ret.setN1(rs2.getLong(i++));
				ret.setN2(rs2.getInt(i++));
				ret.setN3(rs2.getLong(i++));
				ret.setN4(rs2.getLong(i++));
				ret.setN5(rs2.getLong(i++));
				ret.setN6(rs2.getLong(i++));
				ret.setN7(rs2.getLong(i++));
				ret.setN8(rs2.getLong(i++));
				ret.setN9(rs2.getLong(i++));
				ret.setN10(rs2.getLong(i++));
				tarrlist.add(ret);
			}

			TicketData1[] ticketarr = null;

			if (tarrlist.size() > 0) {
				ticketarr = new TicketData1[tarrlist.size()];

				for (int i = 0; i < tarrlist.size(); i++) {
					ticketarr[i] = tarrlist.get(i);
				}
			}

			ticketres = new TicketArr1();
			ticketres.setTicketData(ticketarr);
			ticketres.setAlldate(tdata.isAlldate());
			ticketres.setAllStatus(tdata.isAllStatus());
			ticketres.setNewStatus(tdata.isNewStatus());
			ticketres.setWipStatus(tdata.isWipStatus());
			ticketres.setClosedStatus(tdata.isClosedStatus());
			ticketres.setRejectedStatus(tdata.isRejectedStatus());
		} finally {
			// ps1.close();
			ps2.close();
			// rs1.close();
			rs2.close();
		}

		return ticketres;
	}

	public FilterData getFilterData(String aItemID, ArrayList<FilterData> aFilterList) {
		FilterData l_FilterData = null;

		for (FilterData iFilterData : aFilterList) {
			if (iFilterData.getItemid().equals(aItemID)) {
				l_FilterData = iFilterData;
				break;
			}
		}

		return l_FilterData;
	}

	public TicketData1 getOneTicket(String ticketNo, Connection conn) throws SQLException {
		TicketData1 response = new TicketData1();

		String query = "SELECT * FROM TicketDetail WHERE t1='" + ticketNo + "'";
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			preparedStatement = conn.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			int i = 1;

			if (resultSet.next()) {
				response.setAutokey(Long.parseLong(resultSet.getString(i++)));
				response.setSyskey(Long.parseLong(resultSet.getString(i++)));
				response.setCreatedDate(resultSet.getString(i++));
				response.setModifiedDate(resultSet.getString(i++));
				response.setUserID(resultSet.getString(i++));
				response.setDate(resultSet.getString(i++));
				response.setTime(resultSet.getString(i++));

				String shootDate = resultSet.getString(i++);
				String shootDay = shootDate.substring(8);
				String shootMonth = shootDate.substring(5, 7);
				String shootYear = shootDate.substring(0, 4);
				shootDate = shootDay + "/" + shootMonth + "/" + shootYear;
				response.setShootDate(shootDate);

				String shootTime = resultSet.getString(i++);
				shootTime = shootTime.substring(shootTime.indexOf(" ") + 1);
				response.setShootTime(shootTime);

				response.setT1(resultSet.getString(i++));
				response.setT2(resultSet.getString(i++));
				response.setT3(resultSet.getString(i++));
				response.setT4(resultSet.getString(i++));
				response.setT5(resultSet.getString(i++));
				response.setT6(resultSet.getString(i++));
				response.setT7(resultSet.getString(i++));
				response.setT8(resultSet.getString(i++));
				response.setT9(resultSet.getString(i++));
				response.setT10(resultSet.getString(i++));
				response.setT11(resultSet.getString(i++));
				response.setT12(resultSet.getString(i++));
				response.setT13(resultSet.getString(i++));
				response.setT14(resultSet.getString(i++));
				response.setT15(resultSet.getString(i++));
				response.setT16(resultSet.getString(i++));
				response.setT17(resultSet.getString(i++));
				response.setT18(resultSet.getString(i++));
				response.setT19(resultSet.getString(i++));
				response.setT20(resultSet.getString(i++));
				response.setN1(resultSet.getLong(i++));
				response.setN2(resultSet.getInt(i++));
				response.setN3(resultSet.getLong(i++));
				response.setN4(resultSet.getLong(i++));
				response.setN5(resultSet.getLong(i++));
				response.setN6(resultSet.getLong(i++));
				response.setN7(resultSet.getLong(i++));
				response.setN8(resultSet.getLong(i++));
				response.setN9(resultSet.getLong(i++));
				response.setN10(resultSet.getLong(i++));
				response.setCode("0000");
				response.setDesc("Selected successfully.");
				response.setState(true);
			} else {
				response.setCode("0009");
				response.setDesc("There is no ticket.");
				response.setState(false);
			}
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}

			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}

		return response;
	}

	public TicketListingDataList getPOCTickets(FilterDataset aFilterDataset, Connection conn) throws SQLException {
		TicketListingDataList ticketres = new TicketListingDataList();

		int startPage = (aFilterDataset.getPageNo() - 1) * aFilterDataset.getPageSize();
		int endPage = aFilterDataset.getPageSize() + startPage;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		String whereClause = "";
		whereClause = "where n2=1 and n2<>4 and t12='13000000'";

		// Get Filter List
		ArrayList<FilterData> l_FilterList = aFilterDataset.getFilterList();

		// Clear Filter List
		// if (l_FilterList.get(0).getItemid() == "")
		// l_FilterList.clear();
		if (l_FilterList.equals(null))
			l_FilterList.clear();

		// 1 - Simple Search String [ , , , ]
		String simplesearch = "";
		FilterData l_FilterData = getFilterData("1", l_FilterList);
		if (l_FilterData != null)
			simplesearch = l_FilterData.getT1();

		// 2 - State
		String channelkey = "";
		l_FilterData = getFilterData("2", l_FilterList);
		if (l_FilterData != null)
			channelkey = l_FilterData.getT1();

		// 3 - Status
		String status = "";
		l_FilterData = getFilterData("3", l_FilterList);
		if (l_FilterData != null)
			status = l_FilterData.getT1();

		// 5 - usertype
		String usertype = "";
		l_FilterData = getFilterData("5", l_FilterList);
		if (l_FilterData != null)
			usertype = l_FilterData.getT1();

		// 1 - Simple Search String [ , , , ]
		if (!simplesearch.equals("")) {
			simplesearch = "%" + simplesearch + "%";
			whereClause += " and " + "(t1 like '" + simplesearch + "' ) OR " + "(t15 like '" + simplesearch + "' ) OR "
					+ "(t5 like '" + simplesearch + "' ) OR " + "(t7 like '" + simplesearch + "' ) OR " + "(t8 like '"
					+ simplesearch + "' ) OR " + "(shootDate like '" + simplesearch + "' ) OR " + "(shootTime like '"
					+ simplesearch + "' ) ";
		}

		// 2 - channel
		if (!channelkey.equals("")) {
			whereClause = whereClause + " AND t4 ='" + channelkey + "'";
		}

		// 3 - Status
		if (!status.equals("")) {
			if (status.equals("All")) {
				whereClause = whereClause + " AND (t10 = 'New' OR t10 = 'WIP' OR t10 = 'Closed' OR t10 = 'Rejected') ";
			} else {
				whereClause = whereClause + " AND t10 = '" + status + "' ";
			}
		}

		l_FilterData = getFilterData("4", l_FilterList);
		String date = "";
		String fromDate = "";
		String toDate = "";

		if (l_FilterData != null) {
			if (l_FilterData.getCondition().equals("bt")) {
				fromDate = l_FilterData.getT1();
				toDate = l_FilterData.getT2();
				whereClause += " and date >= '" + fromDate + "' and date <= '" + toDate + "' ";
			} else {
				date = l_FilterData.getT1();
				String l_Condition = "";
				switch (l_FilterData.getCondition()) {
				case "eq":
					l_Condition = "=";
					break; // eq - Equal

				case "gt":
					l_Condition = ">";
					break; // gt - Greater Than
				case "geq":
					l_Condition = ">=";
					break; // geq - Greater Than Equal

				case "lt":
					l_Condition = "<";
					break; // lt - Less Than
				case "leq":
					l_Condition = "<=";
					break; // leq - Less Than Equal
				}

				whereClause += " and shootdate " + l_Condition + " '" + date + "' ";
			}
		}
		// 5 - usertype
		if (!usertype.equals("")) {
			whereClause = whereClause + " AND t16 ='" + usertype + "'";
		}

		query = "SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY autokey desc) AS RowNum,* from "
				+ "(select * from TicketDetail " + whereClause + " )b)AS RowContraintResult WHERE " + "(RowNum > "
				+ startPage + " AND RowNum <= " + endPage + ")";
		ps = conn.prepareStatement(query);
		rs = ps.executeQuery();
		ArrayList<TicketData1> tarrlist = new ArrayList<TicketData1>();
		TicketData1[] ticketarr = null;

		while (rs.next()) {
			TicketData1 retdata = new TicketData1();
			retdata = readTicket(rs);
			tarrlist.add(retdata);
		}
		if (tarrlist.size() > 0) {
			ticketarr = new TicketData1[tarrlist.size()];
			for (int i = 0; i < tarrlist.size(); i++) {
				ticketarr[i] = tarrlist.get(i);
			}
		}
		String totalqry = "";
		totalqry = "SELECT COUNT(*) AS tot FROM TicketDetail " + whereClause;
		ps = conn.prepareStatement(totalqry);
		ResultSet rSet = ps.executeQuery();
		rSet.next();
		ticketres.setTotalCount(rSet.getInt("tot"));
		ticketres.setTicketData(ticketarr);
		if (fromDate.equals(""))
			ticketres.setAlldate(true);
		ticketres.setFromdate(fromDate);
		ticketres.setTodate(toDate);
		// ticketres.setSearchText(searchText);
		ticketres.setCurrentPage(aFilterDataset.getPageNo());
		ticketres.setPageSize(aFilterDataset.getPageSize());

		return ticketres;
	}

	public TicketListingDataList getTickets(FilterDataset aFilterDataset, Connection conn) throws SQLException {
		TicketListingDataList ticketres = new TicketListingDataList();

		String query = "select t5 from UVM005_A where t1='" + aFilterDataset.getUserID() + "' ";
		PreparedStatement ps;
		ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		String regioncode = "";

		while (rs.next()) {
			regioncode = rs.getString("t5");
		}

		int startPage = (aFilterDataset.getPageNo() - 1) * aFilterDataset.getPageSize();
		int endPage = aFilterDataset.getPageSize() + startPage;

		String whereClause = "";
		whereClause = "where n2=1 and n2<>4 ";

		// Get Filter List
		ArrayList<FilterData> l_FilterList = aFilterDataset.getFilterList();

		// Clear Filter List
		if (l_FilterList.get(0).getItemid() == "")
			l_FilterList.clear();

		// 1 - Simple Search String [ , , , ]
		String simplesearch = "";
		FilterData l_FilterData = getFilterData("1", l_FilterList);
		if (l_FilterData != null)
			simplesearch = l_FilterData.getT1();

		// 2 - State
		String state = "";
		l_FilterData = getFilterData("2", l_FilterList);
		if (l_FilterData != null)
			state = l_FilterData.getT1();

		// 3 - Status
		String status = "";
		l_FilterData = getFilterData("3", l_FilterList);
		if (l_FilterData != null)
			status = l_FilterData.getT1();

		// 1 - Simple Search String [ , , , ]
		if (!simplesearch.equals("")) {
			simplesearch = "%" + simplesearch + "%";
			whereClause += " and " + "(t1 like '" + simplesearch + "' ) OR " + "(t15 like '" + simplesearch + "' ) OR "
					+ "(t5 like '" + simplesearch + "' ) OR " + "(t7 like '" + simplesearch + "' ) OR " + "(t8 like '"
					+ simplesearch + "' ) OR " + "(shootDate like '" + simplesearch + "' ) OR " + "(shootTime like '"
					+ simplesearch + "' ) ";
		}

		// 2 - State
		if (!state.equals("")) {
			if (state.equals("00000000")) {
				whereClause = whereClause + " AND (t12 = '10000000' OR t12 = '13000000') ";
			} else {
				whereClause = whereClause + " AND t12 = '" + state + "' ";
			}
		} else {
			if (!regioncode.equals("00000000")) {
				whereClause = whereClause + " AND t12 = '" + regioncode + "' ";
			}
		}

		// 3 - Status
		if (!status.equals("")) {
			if (status.equals("All")) {
				whereClause = whereClause + " AND (t10 = 'New' OR t10 = 'WIP' OR t10 = 'Closed' OR t10 = 'Rejected') ";
			} else {
				whereClause = whereClause + " AND t10 = '" + status + "' ";
			}
		}

		l_FilterData = getFilterData("4", l_FilterList);
		String date = "";
		String fromDate = "";
		String toDate = "";

		if (l_FilterData != null) {
			if (l_FilterData.getCondition().equals("bt")) {
				fromDate = l_FilterData.getT1();
				toDate = l_FilterData.getT2();
				whereClause += " and date >= '" + fromDate + "' and date <= '" + toDate + "' ";
			} else {
				date = l_FilterData.getT1();
				String l_Condition = "";
				switch (l_FilterData.getCondition()) {
				case "eq":
					l_Condition = "=";
					break; // eq - Equal

				case "gt":
					l_Condition = ">";
					break; // gt - Greater Than
				case "geq":
					l_Condition = ">=";
					break; // geq - Greater Than Equal

				case "lt":
					l_Condition = "<";
					break; // lt - Less Than
				case "leq":
					l_Condition = "<=";
					break; // leq - Less Than Equal
				}

				whereClause += " and shootdate " + l_Condition + " '" + date + "' ";
			}
		}

		query = "SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY t1 desc) AS RowNum,* from "
				+ "(select * from TicketDetail " + whereClause + " )b)AS RowContraintResult WHERE " + "(RowNum > "
				+ startPage + " AND RowNum <= " + endPage + ")";
		ps = conn.prepareStatement(query);
		rs = ps.executeQuery();
		ArrayList<TicketData1> tarrlist = new ArrayList<TicketData1>();
		TicketData1[] ticketarr = null;

		while (rs.next()) {
			TicketData1 retdata = new TicketData1();
			retdata = readTicket(rs);
			tarrlist.add(retdata);
		}
		if (tarrlist.size() > 0) {
			ticketarr = new TicketData1[tarrlist.size()];
			for (int i = 0; i < tarrlist.size(); i++) {
				ticketarr[i] = tarrlist.get(i);
			}
		}
		String totalqry = "";
		totalqry = "SELECT COUNT(*) AS tot FROM TicketDetail " + whereClause;
		ps = conn.prepareStatement(totalqry);
		ResultSet rSet = ps.executeQuery();
		rSet.next();
		ticketres.setTotalCount(rSet.getInt("tot"));
		ticketres.setTicketData(ticketarr);
		if (fromDate.equals(""))
			ticketres.setAlldate(true);
		ticketres.setFromdate(fromDate);
		ticketres.setTodate(toDate);
		// ticketres.setSearchText(searchText);
		ticketres.setCurrentPage(aFilterDataset.getPageNo());
		ticketres.setPageSize(aFilterDataset.getPageSize());

		return ticketres;
	}

	public TicketData1 readTicket(ResultSet rs) throws SQLException {
		TicketData1 ret = new TicketData1();
		int i = 1;
		ret.setSrno(i++);
		ret.setAutokey(rs.getLong(i++));
		ret.setSyskey(rs.getLong(i++));
		ret.setCreatedDate(rs.getString(i++));
		ret.setModifiedDate(rs.getString(i++));
		ret.setUserID(rs.getString(i++));
		ret.setDate(rs.getString(i++));
		ret.setTime(rs.getString(i++));

		String shootDate = rs.getString(i++);
		String shootDay = shootDate.substring(8);
		String shootMonth = shootDate.substring(5, 7);
		String shootYear = shootDate.substring(0, 4);
		shootDate = shootDay + "/" + shootMonth + "/" + shootYear;
		ret.setShootDate(shootDate);

		String shootTime = rs.getString(i++);
		shootTime = shootTime.substring(shootTime.indexOf(" ") + 1);
		ret.setShootTime(shootTime);

		ret.setT1(rs.getString(i++));
		ret.setT2(rs.getString(i++));
		ret.setT3(rs.getString(i++));
		ret.setT4(rs.getString(i++));
		ret.setT5(rs.getString(i++));
		ret.setT6(rs.getString(i++));
		ret.setT7(rs.getString(i++));
		ret.setT8(rs.getString(i++));
		ret.setT9(rs.getString(i++));
		ret.setT10(rs.getString(i++));
		ret.setT11(rs.getString(i++));
		ret.setT12(rs.getString(i++));
		ret.setT13(rs.getString(i++));
		ret.setT14(rs.getString(i++));
		ret.setT15(rs.getString(i++));
		ret.setT16(rs.getString(i++));
		ret.setT17(rs.getString(i++));
		ret.setT18(rs.getString(i++));
		ret.setT19(rs.getString(i++));
		ret.setT20(rs.getString(i++));
		ret.setN1(rs.getLong(i++));
		ret.setN2(rs.getInt(i++));
		ret.setN3(rs.getLong(i++));
		ret.setN4(rs.getLong(i++));
		ret.setN5(rs.getLong(i++));
		ret.setN6(rs.getLong(i++));
		ret.setN7(rs.getLong(i++));
		ret.setN8(rs.getLong(i++));
		ret.setN9(rs.getLong(i++));
		ret.setN10(rs.getLong(i++));
		return ret;
	}

	public TicketResponse saveTicket(TicketResponse request, Connection conn) throws SQLException {
		TicketResponse response = new TicketResponse();

		PreparedStatement preparedStatement = null;
		String createdDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
		String modifiedDate = new SimpleDateFormat("yyyyMMdd").format(new Date());

		try {
			String query = "INSERT INTO Ticket (Syskey, CreatedDate, ModifiedDate, "
					+ "Code, TicketNo, SenderKey, SenderName, Message, Location,"
					+ "ShortDate, ShortTime, Image, ChannelKey, ReceiverKey, "
					+ "T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,T15,T16,T17,T18,T19,T20,"
					+ "N1,N2,N3,N4,N5,N6,N7,N8,N9,N10,N11,N12,N13,N14,N15,N16,N17,N18,N19,N20)" + " VALUES (?,?,?,"
					+ "?,?,?,?,?,?," + "?,?,?,?,?," + "?,?,?,?,?,?,?,?,?,?," + "?,?,?,?,?,?,?,?,?,?,"
					+ "?,?,?,?,?,?,?,?,?,?," + "?,?,?,?,?,?,?,?,?,?);";
			preparedStatement = conn.prepareStatement(query);
			int i = 1;
			preparedStatement.setLong(i++, 0);
			preparedStatement.setString(i++, createdDate);
			preparedStatement.setString(i++, modifiedDate);

			preparedStatement.setString(i++, request.getCode());
			preparedStatement.setString(i++, request.getTicketNo());
			preparedStatement.setString(i++, request.getSenderKey());
			preparedStatement.setString(i++, request.getSenderName());
			preparedStatement.setString(i++, request.getMessage());
			preparedStatement.setString(i++, request.getLocation());

			preparedStatement.setString(i++, request.getShortDate());
			preparedStatement.setString(i++, request.getShortTime());
			preparedStatement.setString(i++, request.getImage());
			preparedStatement.setString(i++, request.getChannelKey());
			preparedStatement.setString(i++, request.getReceiverKey());

			preparedStatement.setString(i++, request.getT1());
			preparedStatement.setString(i++, request.getT2());
			preparedStatement.setString(i++, request.getT3());
			preparedStatement.setString(i++, request.getT4());
			preparedStatement.setString(i++, request.getT5());

			preparedStatement.setString(i++, request.getT6());
			preparedStatement.setString(i++, request.getT7());
			preparedStatement.setString(i++, request.getT8());
			preparedStatement.setString(i++, request.getT9());
			preparedStatement.setString(i++, request.getT10());

			preparedStatement.setString(i++, request.getT11());
			preparedStatement.setString(i++, request.getT12());
			preparedStatement.setString(i++, request.getT13());
			preparedStatement.setString(i++, request.getT14());
			preparedStatement.setString(i++, request.getT15());

			preparedStatement.setString(i++, request.getT16());
			preparedStatement.setString(i++, request.getT17());
			preparedStatement.setString(i++, request.getT18());
			preparedStatement.setString(i++, request.getT19());
			preparedStatement.setString(i++, request.getT20());

			preparedStatement.setInt(i++, Integer.parseInt(request.getN1()));
			preparedStatement.setInt(i++, Integer.parseInt(request.getN2()));
			preparedStatement.setInt(i++, Integer.parseInt(request.getN3()));
			preparedStatement.setInt(i++, Integer.parseInt(request.getN4()));
			preparedStatement.setInt(i++, Integer.parseInt(request.getN5()));

			preparedStatement.setInt(i++, Integer.parseInt(request.getN6()));
			preparedStatement.setInt(i++, Integer.parseInt(request.getN7()));
			preparedStatement.setInt(i++, Integer.parseInt(request.getN8()));
			preparedStatement.setInt(i++, Integer.parseInt(request.getN9()));
			preparedStatement.setInt(i++, Integer.parseInt(request.getN10()));

			preparedStatement.setInt(i++, Integer.parseInt(request.getN11()));
			preparedStatement.setInt(i++, Integer.parseInt(request.getN12()));
			preparedStatement.setInt(i++, Integer.parseInt(request.getN13()));
			preparedStatement.setInt(i++, Integer.parseInt(request.getN14()));
			preparedStatement.setInt(i++, Integer.parseInt(request.getN15()));

			preparedStatement.setInt(i++, Integer.parseInt(request.getN16()));
			preparedStatement.setInt(i++, Integer.parseInt(request.getN17()));
			preparedStatement.setInt(i++, Integer.parseInt(request.getN18()));
			preparedStatement.setInt(i++, Integer.parseInt(request.getN19()));
			preparedStatement.setInt(i++, Integer.parseInt(request.getN20()));

			if (preparedStatement.executeUpdate() > 0) {
				response.setMsgCode("0000");
				response.setMsgDesc("Saved successfully.");
				response.setState(true);
			} else {
				response.setMsgCode("0014");
				response.setMsgDesc("Saved fail.");
				response.setState(false);
			}
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}

		return response;
	}

	// atn
	public TicketData1 updatemyTicket(TicketData1 request, Connection conn) throws SQLException {
		TicketData1 response = new TicketData1();
		PreparedStatement preparedStatement = null;

		String todayDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		request.setModifiedDate(todayDate);

		try {
			String query = "UPDATE TicketDetail SET ModifiedDate=?, userid=?, t10=?, T20=? WHERE AutoKey = ?;";
			if (isCodeExist(request, conn)) {
				preparedStatement = conn.prepareStatement(query);
				int i = 1;
				preparedStatement.setString(i++, request.getModifiedDate());
				preparedStatement.setString(i++, request.getUserID());
				preparedStatement.setString(i++, request.getT10());
				preparedStatement.setString(i++, request.getT20());
				preparedStatement.setLong(i++, request.getAutokey());

				if (preparedStatement.executeUpdate() > 0) {
					response.setCode("0000");
					response.setDesc("Updated successfully.");
					response.setState(true);
					response.setT1(request.getT1());
				} else {
					response.setCode("0014");
					response.setDesc("Update failed.");
					response.setState(false);
				}
			}
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}

		return response;
	}

	public TicketResponse updateTicket(TicketResponse request, Connection conn) throws SQLException {
		TicketResponse response = new TicketResponse();

		String modifiedDate = new SimpleDateFormat("yyyyMMdd").format(new Date());

		PreparedStatement preparedStatement = null;

		try {
			String query = "UPDATE Ticket SET ModifiedDate = ?, Code = ?, SenderName = ?, T9 = ?, T20 = ?, N1 = ?, N2 = ? WHERE AutoKey = ?;";
			preparedStatement = conn.prepareStatement(query);
			int i = 1;
			preparedStatement.setString(i++, modifiedDate);
			preparedStatement.setString(i++, request.getCode());
			preparedStatement.setString(i++, request.getSenderName());
			preparedStatement.setString(i++, request.getT9());
			preparedStatement.setString(i++, request.getT20());
			preparedStatement.setInt(i++, Integer.parseInt(request.getN1()));
			preparedStatement.setInt(i++, Integer.parseInt(request.getN2()));
			preparedStatement.setLong(i, Long.parseLong(request.getAutoKey()));

			if (preparedStatement.executeUpdate() > 0) {
				response.setMsgCode("0000");
				response.setMsgDesc("Updated successfully.");
				response.setState(true);
			} else {
				response.setMsgCode("0014");
				response.setMsgDesc("Updated fail.");
				response.setState(false);
			}
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}

		return response;
	}

}
