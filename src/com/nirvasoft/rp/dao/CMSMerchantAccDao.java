package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;
import com.nirvasoft.rp.shared.CMSMerchantAccRefData;

public class CMSMerchantAccDao {

	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("CMSMerchantAccRef");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("CreatedDate", (byte) 5));
		ret.getFields().add(new DBField("ModifiedDate", (byte) 5));
		ret.getFields().add(new DBField("CreatedUserID", (byte) 5));
		ret.getFields().add(new DBField("ModifiedUserID", (byte) 5));
		ret.getFields().add(new DBField("AccountNumber", (byte) 5));
		ret.getFields().add(new DBField("branchname", (byte) 5));
		ret.getFields().add(new DBField("branchCode", (byte) 5));
		ret.getFields().add(new DBField("T1", (byte) 5));
		ret.getFields().add(new DBField("T2", (byte) 5));
		ret.getFields().add(new DBField("T3", (byte) 5));
		ret.getFields().add(new DBField("T4", (byte) 5));
		ret.getFields().add(new DBField("T5", (byte) 5));
		ret.getFields().add(new DBField("T6", (byte) 5));
		ret.getFields().add(new DBField("T7", (byte) 5));
		ret.getFields().add(new DBField("T8", (byte) 5));
		ret.getFields().add(new DBField("T9", (byte) 5));
		ret.getFields().add(new DBField("T10", (byte) 5));
		ret.getFields().add(new DBField("N1", (byte) 1));
		ret.getFields().add(new DBField("N2", (byte) 1));
		ret.getFields().add(new DBField("N3", (byte) 1));
		ret.getFields().add(new DBField("N4", (byte) 1));
		ret.getFields().add(new DBField("N5", (byte) 1));
		ret.getFields().add(new DBField("N6", (byte) 1));
		ret.getFields().add(new DBField("N7", (byte) 1));
		ret.getFields().add(new DBField("N8", (byte) 1));
		ret.getFields().add(new DBField("N9", (byte) 1));
		ret.getFields().add(new DBField("N10", (byte) 1));
		ret.getFields().add(new DBField("recordStatus", (byte) 1));
		return ret;
	}

	public static DBRecord defineBySyskey() {
		DBRecord ret = new DBRecord();
		ret.setTableName("CMSMerchantAccRef");
		ret.setFields(new ArrayList<DBField>());

		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("CreatedDate", (byte) 5));
		ret.getFields().add(new DBField("ModifiedDate", (byte) 5));
		ret.getFields().add(new DBField("CreatedUserID", (byte) 5));
		ret.getFields().add(new DBField("ModifiedUserID", (byte) 5));
		ret.getFields().add(new DBField("AccountNumber", (byte) 5));
		ret.getFields().add(new DBField("branchname", (byte) 5));
		ret.getFields().add(new DBField("branchCode", (byte) 5));
		ret.getFields().add(new DBField("T1", (byte) 5));
		ret.getFields().add(new DBField("T2", (byte) 5));
		ret.getFields().add(new DBField("T3", (byte) 5));
		ret.getFields().add(new DBField("T4", (byte) 5));
		ret.getFields().add(new DBField("T5", (byte) 5));
		ret.getFields().add(new DBField("T6", (byte) 5));
		ret.getFields().add(new DBField("T7", (byte) 5));
		ret.getFields().add(new DBField("T8", (byte) 5));
		ret.getFields().add(new DBField("T9", (byte) 5));
		ret.getFields().add(new DBField("T10", (byte) 5));
		ret.getFields().add(new DBField("N1", (byte) 1));
		ret.getFields().add(new DBField("N2", (byte) 1));
		ret.getFields().add(new DBField("N3", (byte) 1));
		ret.getFields().add(new DBField("N4", (byte) 1));
		ret.getFields().add(new DBField("N5", (byte) 1));
		ret.getFields().add(new DBField("N6", (byte) 1));
		ret.getFields().add(new DBField("N7", (byte) 1));
		ret.getFields().add(new DBField("N8", (byte) 1));
		ret.getFields().add(new DBField("N9", (byte) 1));
		ret.getFields().add(new DBField("N10", (byte) 1));
		ret.getFields().add(new DBField("recordStatus", (byte) 1));
		return ret;
	}

	public static CMSMerchantAccRefData getDBRecord(DBRecord aDBRecord) {

		CMSMerchantAccRefData ret = new CMSMerchantAccRefData();

		ret.setSyskey(aDBRecord.getLong("sysKey"));
		ret.setCreatedDate(aDBRecord.getString("CreatedDate"));
		ret.setModifiedDate(aDBRecord.getString("ModifiedDate"));
		ret.setModifiedUserID(aDBRecord.getString("ModifiedUserID"));
		ret.setCreatedUserID(aDBRecord.getString("CreatedUserID"));
		ret.setAccountNumber(aDBRecord.getString("AccountNumber"));
		ret.setBranchCode(aDBRecord.getString("branchCode"));
		ret.setBranchname(aDBRecord.getString("branchname"));
		ret.setT1(aDBRecord.getString("T1"));
		ret.setT2(aDBRecord.getString("T2"));
		ret.setT3(aDBRecord.getString("T3"));
		ret.setT4(aDBRecord.getString("T4"));
		ret.setT5(aDBRecord.getString("T5"));
		ret.setT6(aDBRecord.getString("T6"));
		ret.setT7(aDBRecord.getString("T7"));
		ret.setT8(aDBRecord.getString("T8"));
		ret.setT9(aDBRecord.getString("T9"));
		ret.setT10(aDBRecord.getString("T10"));
		ret.setN1(aDBRecord.getInt("N1"));
		ret.setN2(aDBRecord.getInt("N2"));
		ret.setN3(aDBRecord.getInt("N3"));
		ret.setN3(aDBRecord.getInt("N4"));
		ret.setN3(aDBRecord.getInt("N5"));
		ret.setN3(aDBRecord.getInt("N6"));
		ret.setN3(aDBRecord.getInt("N7"));
		ret.setN3(aDBRecord.getInt("N8"));
		ret.setN3(aDBRecord.getInt("N9"));
		ret.setN3(aDBRecord.getInt("N10"));
		ret.setRecordstatus(aDBRecord.getInt("recordStatus"));

		return ret;
	}

	public static ArrayList<CMSMerchantAccRefData> read(String mID, Connection aConnection) throws SQLException {
		ArrayList<CMSMerchantAccRefData> ret = new ArrayList<CMSMerchantAccRefData>();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(defineBySyskey(), "where t1='" + mID + "' and recordstatus<>4",
				"", aConnection);

		for (int i = 0; i < dbrs.size(); i++) {
			ret.add(getDBRecord(dbrs.get(i)));
		}

		return ret;

	}

	/*
	 * public static ArrayList<CMSMerchantAccRefData> read(String mID,String
	 * toaccount, Connection aConnection) throws SQLException {
	 * ArrayList<CMSMerchantAccRefData> ret = new
	 * ArrayList<CMSMerchantAccRefData>(); ArrayList<DBRecord> dbrs =
	 * DBMgr.getDBRecords(defineBySyskey(), "where t1='" + mID +
	 * "' and AccountNumber = '" + toaccount + "' and recordstatus<>4", "",
	 * aConnection);
	 * 
	 * for (int i = 0; i < dbrs.size(); i++) {
	 * ret.add(getDBRecord(dbrs.get(i))); }
	 * 
	 * return ret;
	 * 
	 * }
	 */

	public static String readAccById(String mID, Connection aConnection) throws SQLException {
		String ret = "";
		String sql = "select * from CMSMerchantAccRef where t1= ? and t2='MPTTRUST' and recordstatus<>4 ";
		PreparedStatement stmt = aConnection.prepareStatement(sql);
		stmt.setString(1, mID);
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			ret = rs.getString("AccountNumber");
		}
		stmt.close();
		return ret;

	}

	public static DBRecord setDBRecord(CMSMerchantAccRefData aObj) {
		DBRecord aDBRecord = define(); // define
		aDBRecord.setValue("syskey", aObj.getSyskey());
		aDBRecord.setValue("CreatedDate", aObj.getCreatedDate());
		aDBRecord.setValue("ModifiedDate", aObj.getModifiedDate());
		aDBRecord.setValue("CreatedUserID", aObj.getCreatedUserID());
		aDBRecord.setValue("ModifiedUserID", aObj.getModifiedUserID());
		aDBRecord.setValue("AccountNumber", aObj.getAccountNumber());
		aDBRecord.setValue("branchCode", aObj.getBranchCode());
		aDBRecord.setValue("branchname", aObj.getBranchname());
		aDBRecord.setValue("T1", aObj.getT1());
		aDBRecord.setValue("T2", aObj.getT2());
		aDBRecord.setValue("T3", aObj.getT3());
		aDBRecord.setValue("T4", aObj.getT4());
		aDBRecord.setValue("T5", aObj.getT5());
		aDBRecord.setValue("T6", aObj.getT6());
		aDBRecord.setValue("T7", aObj.getT7());
		aDBRecord.setValue("T8", aObj.getT8());
		aDBRecord.setValue("T9", aObj.getT9());
		aDBRecord.setValue("T10", aObj.getT10());
		aDBRecord.setValue("N1", aObj.getN1());
		aDBRecord.setValue("N2", aObj.getN2());
		aDBRecord.setValue("N3", aObj.getN3());
		aDBRecord.setValue("N4", aObj.getN4());
		aDBRecord.setValue("N5", aObj.getN5());
		aDBRecord.setValue("N6", aObj.getN6());
		aDBRecord.setValue("N7", aObj.getN7());
		aDBRecord.setValue("N8", aObj.getN8());
		aDBRecord.setValue("N9", aObj.getN9());
		aDBRecord.setValue("N10", aObj.getN10());
		aDBRecord.setValue("recordStatus", aObj.getRecordstatus());
		return aDBRecord;
	}

	public CMSMerchantAccRefData readDataAccById(String mID, Connection aConnection) throws SQLException {
		CMSMerchantAccRefData ret = new CMSMerchantAccRefData();
		String sql = "select a.UserName, ca.AccountNumber, ca.branchCode from CMSMerchant  a "
				+ "inner join CMSMerchantAccRef ca on a.UserId = ca.T1 "
				+ "where a.UserId = ? and ca.recordStatus <> 4 ";
		PreparedStatement stmt = aConnection.prepareStatement(sql);
		stmt.setString(1, mID);
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			ret.setAccountNumber(rs.getString("AccountNumber"));
			ret.setBranchCode(rs.getString("branchCode"));
			ret.setT10(rs.getString("UserName"));
		}
		stmt.close();
		return ret;

	}
}
