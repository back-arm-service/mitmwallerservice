package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBRecord;
import com.nirvasoft.rp.data.MCommRateMappingData;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.MrBean;
import com.nirvasoft.rp.framework.Profile;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.mgr.MCommRateMappingMgr;
import com.nirvasoft.rp.shared.AgentTransferReq;
import com.nirvasoft.rp.shared.LovData;
import com.nirvasoft.rp.shared.MSigninRequestData;
import com.nirvasoft.rp.shared.MSigninResponseData;
import com.nirvasoft.rp.shared.MerchantListData;
import com.nirvasoft.rp.shared.MerchantResData;
import com.nirvasoft.rp.shared.ReferenceListData;
import com.nirvasoft.rp.shared.ReferenceResData;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.GeneralUtility;
import com.nirvasoft.rp.util.Response;
import com.nirvasoft.rp.util.ServerUtil;

public class Service001Dao {

	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("UVM005_A");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		return ret;
	}

	public static ArrayList<String> getMainMenu(Profile p, Connection con) {
		ArrayList<String> ret = new ArrayList<String>();
		try {
			String sql = "SELECT t2 FROM UVM022_A " + "WHERE syskey IN (SELECT n2 FROM UVM023_A WHERE "
					+ "n1 IN (SELECT n2 FROM JUN002_A WHERE n1 IN (SELECT syskey FROM UVM005_A " + "WHERE (t1=?))))"
					+ " AND n2=0 AND RecordStatus<> 4 ";
			sql += " order by n6";

			PreparedStatement stat = con.prepareStatement(sql);
			stat.setString(1, p.getUserID());
			ResultSet result = stat.executeQuery();
			while (result.next()) {
				ret.add(result.getString("t2"));

			}

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		return ret;

	}

	public static long[] getParentKey(MrBean user, Connection con, String userid) {
		ArrayList<Long> key = new ArrayList<Long>();

		try {

			String sql = "SELECT DISTINCT syskey FROM UVM022_A " + "WHERE syskey IN (SELECT n2 FROM UVM023_A WHERE "
					+ "n1 IN (SELECT n2 FROM JUN002_A WHERE n1 IN (SELECT syskey FROM UVM005_A " + "WHERE t1=?)))"
					+ " AND n2=0 AND RecordStatus<> 4 ORDER BY syskey ";

			PreparedStatement stat = con.prepareStatement(sql);
			stat.setString(1, user.getUser().getUserId());
			ResultSet result = stat.executeQuery();

			while (result.next()) {
				key.add(result.getLong("syskey"));

			}

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		long parentkey[] = new long[key.size()];

		for (int i = 0; i < key.size(); i++) {
			parentkey[i] = key.get(i);
		}

		return parentkey;

	}

	public static ArrayList<String> getSubMenuItem(Profile p, Connection con, String parent) {
		ArrayList<String> ret = new ArrayList<String>();

		try {

			String sql = "SELECT t2 FROM UVM022_A " + " WHERE syskey IN (SELECT n2 FROM UVM023_A WHERE "
					+ " n1 IN (SELECT n2 FROM JUN002_A WHERE n1 IN " + "(SELECT syskey FROM UVM005_A WHERE (t1=?))))"
					+ " AND RecordStatus<> 4 AND n2 IN (SELECT DISTINCT syskey from UVM022_A WHERE t2=" + "\'" + parent
					+ "\')";
			sql += " order by n6 ";

			PreparedStatement stat = con.prepareStatement(sql);
			stat.setString(1, p.getUserID());
			ResultSet result = stat.executeQuery();

			while (result.next()) {
				ret.add(result.getString("t2"));

			}

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		return ret;

	}

	public static String getUserData(String userid, Connection con) throws SQLException {
		String sql = " SELECT *  FROM View_User_A WHERE (userid=? COLLATE SQL_Latin1_General_CP1_CS_AS) ";
		PreparedStatement stat = con.prepareStatement(sql);
		stat.setString(1, userid);
		ResultSet result = stat.executeQuery();
		String u = "";
		if (result.next()) {
			u = result.getString("username");
			return u;
		}
		return u;

	}

	public static Boolean isMobileUser(String name, String psw, Connection con) {
		boolean result = true;
		try {
			String query = "Select * From UVM005_A Where n2=2 and recordStatus<> 4 and (t1=? COLLATE SQL_Latin1_General_CP1_CS_AS)";
			PreparedStatement stmt = con.prepareStatement(query);
			int k = 1;
			stmt.setString(k++, name);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				res.getInt("n2");
				result = false;
				return result;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;

	}

	public static void main(String[] args) {
		Service001Dao dao = new Service001Dao();
		Connection conn = null;
		conn = ConnAdmin.getConn("001", "");
		try {
			dao.getMerchant("", "1", conn);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			ServerUtil.closeConnection(conn);
		}
	}

	private String mTableName = "tblSession";

	public Boolean checkForcePwdChange(MSigninRequestData p, Connection con) throws SQLException {
		Boolean st = false;
		int flag = 0;
		String name = p.getUserID();
		String sql = "SELECT n10 FROM UVM012_A  WHERE recordStatus <> 4 AND (t1 = ? COLLATE SQL_Latin1_General_CP1_CS_AS)";
		PreparedStatement stat = con.prepareStatement(sql);
		stat.setString(1, name);
		ResultSet result = stat.executeQuery();
		if (result.next()) {
			flag = result.getInt("n10");
		}
		if (flag == 1) {
			st = true;
		}

		return st;
	}

	public MerchantResData getMerchant(String imageURL, String paymentType, Connection conn) throws SQLException {
		MerchantListData[] merchantarr = null;
		MerchantResData response = new MerchantResData();
		ArrayList<MerchantListData> ArrList = new ArrayList<>();
		String sql = "select A.UserId as MerchantID, A.UserName as MerchantName from CMSMerchant a where a.recordstatus <> 4";
		PreparedStatement stat = conn.prepareStatement(sql);
		// stat.setString(1, paymentType);
		ResultSet res = stat.executeQuery();
		while (res.next()) {
			MerchantListData data = new MerchantListData();
			data.setMerchantID(res.getString("MerchantID"));
			data.setMerchantName(res.getString("MerchantName"));
			//data.setAccountNo(res.getString("AccountNo"));
			ArrList.add(data);
		}
		merchantarr = new MerchantListData[ArrList.size()];
		for (int i = 0; i < ArrList.size(); i++) {
			merchantarr[i] = ArrList.get(i);
		}
		if (ArrList.size() > 0) {
			response.setCode("0000");
			response.setDesc("Success");
			response.setMerchantList(merchantarr);
		} else {
			response.setCode("0014");
			response.setDesc("Failed");
			response.setMerchantList(merchantarr);
		}

		return response;
	}

	public String getMerchantByPC(String processingCode, Connection con) throws SQLException {
		String sql = "select merchantID from PayTemplateHeader where ProcessingCode = ?";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, processingCode);
		ResultSet rs = ps.executeQuery();
		String ret = "";
		if (rs.next()) {
			ret = rs.getString("merchantID");
		}
		return ret;
	}

	public ReferenceResData getReferenceData(String processingCode, Connection conn) throws SQLException {
		LovData[] lovarr = null;
		ArrayList<LovData> lovList = new ArrayList<LovData>();
		ReferenceResData response = new ReferenceResData();
		ArrayList<Long> lovKeyList = new ArrayList<Long>();
		String merchantID = "";
		String sql1 = "select d.LovKey, h.MerchantID from PayTemplateHeader H inner join PayTemplateDetail D on H.SysKey = D.HKey and FieldShow "
				+ "in (1,3,4,6) and DataType = 'lov' AND H.ProcessingCode = ? and h.n1<>4 order by d.FieldID; ";
		PreparedStatement ps1 = conn.prepareStatement(sql1);
		ps1.setString(1, processingCode);
		ResultSet rs1 = ps1.executeQuery();
		while (rs1.next()) {
			long lovKey = 0;
			lovKey = rs1.getLong(1);
			merchantID = rs1.getString(2);
			lovKeyList.add(lovKey);
		}
		ps1.close();
		rs1.close();
		if (lovKeyList.size() > 0) {
			String sql2 = "select code, description from LOVDetails where HKey =? ";
			PreparedStatement ps2 = conn.prepareStatement(sql2);
			ps2.setLong(1, lovKeyList.get(0));
			ResultSet rs2 = ps2.executeQuery();
			while (rs2.next()) {
				LovData lovData = new LovData();
				lovData.setValue(rs2.getString("code"));
				lovData.setName(rs2.getString("description"));
				lovList.add(lovData);
			}
			ps2.close();
			rs2.close();
			if (lovKeyList.size() > 1) {
				String sql3 = "select code, description from LOVDetails where HKey = ?  order by syskey";
				PreparedStatement ps3 = conn.prepareStatement(sql3);
				ps3.setLong(1, lovKeyList.get(1));
				ResultSet rs3 = ps3.executeQuery();
				if (processingCode.equalsIgnoreCase("050200")) {
					ArrayList<ReferenceListData> billList = new ArrayList<ReferenceListData>();
					ArrayList<ReferenceListData> topupList = new ArrayList<ReferenceListData>();
					ReferenceListData[] billArr = null;
					ReferenceListData[] topupArr = null;
					while (rs3.next()) {
						ReferenceListData refData = new ReferenceListData();
						refData.setName(rs3.getString("description"));
						refData.setValue(rs3.getString("code"));
						if (refData.getValue().startsWith("B")) {
							if (refData.getValue().equalsIgnoreCase("B-MPT")) {
								MCommRateMappingData rateResult = new MCommRateMappingData();
								MCommRateMappingMgr mCommRateMgr = new MCommRateMappingMgr();
								rateResult = mCommRateMgr.getMCommMappingToCalculateChargewByMerId(merchantID, "B-MPT");
								if (!rateResult.getCommRef1().equals("-") && !rateResult.getCommRef1().equals("")) {
									refData.setBankCharges(GeneralUtil.formatNumber(
											GeneralUtility.calculateIndividualCharges(rateResult.getCommObj1(), 3, 0)));
								}
								if (!rateResult.getCommRef1().equals("-") && !rateResult.getCommRef1().equals("")) {
									refData.setMerchantCharges(GeneralUtil.formatNumber(
											GeneralUtility.calculateIndividualCharges(rateResult.getCommObj2(), 3, 0)));
								}
							}
							billList.add(refData);
						} else if (refData.getValue().startsWith("T")) {
							topupList.add(refData);
						}
					}
					if (billList.size() > 0) {
						billArr = new ReferenceListData[billList.size()];
						for (int i = 0; i < billList.size(); i++) {
							billArr[i] = billList.get(i);
						}
					}
					if (topupList.size() > 0) {
						topupArr = new ReferenceListData[topupList.size()];
						for (int i = 0; i < topupList.size(); i++) {
							topupArr[i] = topupList.get(i);
						}
					}
					if (lovList.size() > 0) {
						for (int i = 0; i < lovList.size(); i++) {
							if (lovList.get(i).getValue().equalsIgnoreCase("BILL")) {
								lovList.get(i).setDataList(billArr);
							} else if (lovList.get(i).getValue().equalsIgnoreCase("TOPUP")) {
								lovList.get(i).setDataList(topupArr);
							}
						}
					}
				} else if (processingCode.equalsIgnoreCase("040300")) {
					ArrayList<ReferenceListData> mptList = new ArrayList<ReferenceListData>();
					ArrayList<ReferenceListData> tList = new ArrayList<ReferenceListData>();
					ArrayList<ReferenceListData> oList = new ArrayList<ReferenceListData>();
					ArrayList<ReferenceListData> mecList = new ArrayList<ReferenceListData>();
					ReferenceListData[] mptArr = null;
					ReferenceListData[] tArr = null;
					ReferenceListData[] oArr = null;
					ReferenceListData[] mecArr = null;
					// skypay ref list
					while (rs3.next()) {
						ReferenceListData refData = new ReferenceListData();
						refData.setName(rs3.getString("description"));
						refData.setValue(rs3.getString("code"));
						if (!refData.getValue().equalsIgnoreCase("")) {
							if (refData.getValue().split("-")[1].startsWith("MPT")) {
								mptList.add(refData);
							} else if (refData.getValue().split("-")[1].startsWith("TELENOR")) {
								tList.add(refData);
							} else if (refData.getValue().split("-")[1].startsWith("OOREDOO")) {
								oList.add(refData);
							} else if (refData.getValue().split("-")[1].startsWith("MEC")) {
								mecList.add(refData);
							}
						}
					}
					if (mptList.size() > 0) {
						mptArr = new ReferenceListData[mptList.size()];
						for (int i = 0; i < mptList.size(); i++) {
							mptArr[i] = mptList.get(i);
						}
					}
					if (tList.size() > 0) {
						tArr = new ReferenceListData[tList.size()];
						for (int i = 0; i < tList.size(); i++) {
							tArr[i] = tList.get(i);
						}
					}
					if (oList.size() > 0) {
						oArr = new ReferenceListData[oList.size()];
						for (int i = 0; i < oList.size(); i++) {
							oArr[i] = oList.get(i);
						}
					}
					if (mecList.size() > 0) {
						mecArr = new ReferenceListData[mecList.size()];
						for (int i = 0; i < mecList.size(); i++) {
							mecArr[i] = mecList.get(i);
						}
					}
					if (lovList.size() > 0) {
						for (int i = 0; i < lovList.size(); i++) {
							if (lovList.get(i).getValue().equalsIgnoreCase("MPT")) {
								lovList.get(i).setDataList(mptArr);
							} else if (lovList.get(i).getValue().equalsIgnoreCase("TELENOR")) {
								lovList.get(i).setDataList(tArr);
							} else if (lovList.get(i).getValue().equalsIgnoreCase("OOREDOO")) {
								lovList.get(i).setDataList(oArr);
							} else if (lovList.get(i).getValue().equalsIgnoreCase("MEC")) {
								lovList.get(i).setDataList(mecArr);
							}
						}
					}

				}
				ps3.close();
				rs3.close();
			}
		} else {
			response.setCode("0014");
			response.setDesc("Reference not found");
			return response;
		}
		if (lovList.size() > 0) {
			lovarr = new LovData[lovList.size()];
			for (int i = 0; i < lovList.size(); i++) {
				lovarr[i] = lovList.get(i);
			}
			if (lovarr.length > 0) {
				response.setCode("0000");
				response.setDesc("Success");
				response.setDataList(lovarr);
			} else {
				response.setCode("0014");
				response.setDesc("Reference not found");
			}
		}
		return response;
	}

	public boolean isMasterUser(String userID, Connection con) throws SQLException {
		Boolean res = false;
		int flag = 0;
		String sql = "Select n8 from UVM005_A where t1=?";
		PreparedStatement stat = null;
		stat = con.prepareStatement(sql);
		stat.setString(1, userID);
		ResultSet result = stat.executeQuery();
		while (result.next()) {
			flag = result.getInt("n8");
		}
		if (flag == 1) {
			res = true;
		}
		return res;
	}

	public void LockUserAcc(String userName, Connection con) throws SQLException {
		String sql = "UPDATE UVM005_A SET n1=?,n7=? WHERE recordStatus <> 4 and t1=?";
		PreparedStatement stmt = con.prepareStatement(sql);
		int i = 1;
		stmt.setInt(i++, 3);
		stmt.setInt(i++, 11);
		stmt.setString(i++, userName);
		stmt.executeUpdate();
	}

	public MSigninResponseData mobilelogin(MSigninRequestData mprofile, Connection con) {

		MSigninResponseData response = new MSigninResponseData();
		String userid = mprofile.getUserID();
		String pwd = mprofile.getPassword();
		boolean isUserIDExists = false;
		boolean isUserValid = false;
		String sql = "";
		String sqlretryCount = "";

		int retryCount = 0;
		int lockstatus = 0;
		pwd = ServerUtil.encryptPIN(pwd);
		int recordStatus = 0;
		int lock = 0;
		Result resultReset = new Result();
		PreparedStatement stmt2 = null;
		try {
			sql = "Select t1 as userid, RecordStatus,n7 as lockstatus From UVM005_A Where n2 = ? and recordStatus <> 4 and ((t1=? COLLATE SQL_Latin1_General_CP1_CS_AS and t2=?))";
			stmt2 = con.prepareStatement(sql);
			int k = 1;
			stmt2.setInt(k++, 1);// n2 = 1 ==> mobile user, 2 = web user
			stmt2.setString(k++, userid);// MMPPM
			stmt2.setString(k++, pwd);

			ResultSet result = stmt2.executeQuery();
			if (result.next()) {
				recordStatus = result.getInt("RecordStatus");
				lock = result.getInt("lockstatus");
				userid = result.getString("userid");
				isUserValid = true;
			}

			if (!isUserValid) {
				sqlretryCount = "Select n1,n7,recordStatus,t1 from UVM005_A where recordStatus <> 4 and n2 = 1 and (t1=? COLLATE SQL_Latin1_General_CP1_CS_AS)";
				PreparedStatement stmt1 = con.prepareStatement(sqlretryCount);
				stmt1.setString(1, userid);
				ResultSet res = stmt1.executeQuery();

				if (res.next()) {
					retryCount = res.getInt("n1");
					recordStatus = res.getInt("recordStatus");
					lockstatus = res.getInt("n7");
					isUserIDExists = true;
				}

				if (!isUserIDExists) {
					response.setCode("0014");
					response.setDesc("Invalid User ID or Password");

				} else {
					if (recordStatus == 1) {
						response.setCode("0014");
						response.setDesc("Your account is not activated.Please contact bank administrator");
					}
					if (recordStatus == 21) {
						response.setCode("0014");
						response.setDesc("Your account is deactivated.Please contact bank administrator");
					}
					if (recordStatus == 2 && retryCount == 3 && lockstatus == 11) {
						response.setCode("0014");
						response.setDesc("Your account has been locked.Please contact bank administrator");
					}
					if (recordStatus == 2 && retryCount < 3 && lockstatus != 11) {
						int count = ++retryCount;
						sql = "UPDATE UVM005_A SET n1=? WHERE recordStatus <> 4 and n2 = 1 and (t1=? COLLATE SQL_Latin1_General_CP1_CS_AS )";
						PreparedStatement stmt = con.prepareStatement(sql);
						int i = 1;
						stmt.setInt(i++, count);
						stmt.setString(i++, userid);
						int rs = stmt.executeUpdate();
						if (rs > 0) {

							if (count == 3) {
								LockUserAcc(userid, con);
								response.setCode("0014");
								response.setDesc("Your acount has been locked.Please contact bank administrator");
							}
						}
						if (count < 3) {
							response.setCode("0014");
							response.setDesc("Invalid User ID or Password");
						}
					}
				}

			} else {
				if (recordStatus == 2 && lock == 11) {// recordstatus = 2 ==>
														// activated
					response.setCode("0014");
					response.setDesc("Your acount has been locked.Please contact bank administrator");
				} else if (recordStatus == 21) {
					response.setCode("0014");
					response.setDesc("Your acount is deactivated.Please contact bank administrator");
				} else {
					resultReset = resetRetryCount(userid, con);
					if (resultReset.getMsgCode().equalsIgnoreCase("0000")) {
						response.setCode("0000");
						response.setDesc("Logged in Successfully");
					}

				}
			}
			if (response.getCode().equalsIgnoreCase("0014") && isUserIDExists)// to
																				// know
																				// lastlogintimefail
			{
				String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
				String insertquery = "INSERT INTO " + mTableName + " (UserID,LogInDateTime,Status) VALUES(?,?,?)";// tblSession
				PreparedStatement stmt3 = con.prepareStatement(insertquery);
				int i = 1;
				int rs = 0;
				stmt3.setString(i++, userid);
				stmt3.setString(i++, date);
				stmt3.setInt(i++, 8);
				rs = stmt3.executeUpdate();
				if (rs == 0) {
					response.setCode("0014");
					response.setDesc("Insert Failed");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			response.setCode("0014");
			response.setDesc("Contact to Bank Administrator");
		}
		return response;

	}

	public Result resetRetryCount(String userid, Connection conn) throws SQLException {
		Result res = new Result();
		String sql = "";
		sql = "UPDATE UVM005_A SET n7 = ?, n1 = ? WHERE recordStatus <> 4 and (t1 = ? COLLATE SQL_Latin1_General_CP1_CS_AS)";
		PreparedStatement stmt = conn.prepareStatement(sql);
		int i = 1;
		stmt.setInt(i++, 0);
		stmt.setInt(i++, 0);
		stmt.setString(i++, userid);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setMsgCode("0000");
		} else
			res.setMsgCode("0014");

		stmt.close();

		return res;
	}

	public Response saveAgentTransaction(AgentTransferReq transdata, String tableName, String transDate,
			Connection conn) throws SQLException {
		Response response = new Response();
		String sql = "Set Dateformat dmy; insert into AgentTransaction(TransferType,MobileUserID,FromAccount,FromName,ToAccount,"
				+ "ToName,Amount,Commission,DraweeName,DraweeNRC,DraweePhone,PayeeName,PayeeNRC,PayeePhone,"
				+ "DrawingDate,EncashDate,DTransRef,ETransRef,Status) Values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setInt(1, Integer.valueOf(transdata.getField1()));
		stmt.setString(2, transdata.getUserID());
		stmt.setString(3, transdata.getFromAccount());
		stmt.setString(4, transdata.getFromName());
		stmt.setString(5, transdata.getToAccount());
		stmt.setString(6, transdata.getToName());
		stmt.setDouble(7, Double.valueOf(transdata.getAmount()));
		stmt.setDouble(8, Double.valueOf(transdata.getBankCharges()));
		stmt.setString(9, transdata.getDraweeName());
		stmt.setString(10, transdata.getDraweeNRC());
		stmt.setString(11, transdata.getDraweePhone());
		stmt.setString(12, transdata.getPayeeName());
		stmt.setString(13, transdata.getPayeeNRC());
		stmt.setString(14, transdata.getPayeePhone());
		stmt.setString(15, transDate);
		stmt.setString(16, "19000101");
		stmt.setString(17, transdata.getField2());
		stmt.setString(18, "");
		stmt.setInt(19, 1);
		if (stmt.executeUpdate() > 0) {
			response.setResponsecode("0000");
			response.setResponsedesc("Saved Successfully.");
		} else {
			response.setResponsecode("0014");
			response.setResponsedesc("Saved Fail.");
		}
		if (!conn.isClosed()) {
			conn.close();
		}
		return response;
	}

	public Response updateAgentTransaction(AgentTransferReq transdata, String tableName, String transDate,
			Connection conn) throws SQLException {
		Response response = new Response();
		String sql = "Set Dateformat dmy; Update AgentTransaction Set EncashDate=?,ETransRef=?,Status=2,t1=? "
				+ "Where DTransRef=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, transDate);
		stmt.setString(2, transdata.getField2());
		stmt.setString(3, transdata.getToAccount());
		stmt.setString(4, transdata.getRefNo());
		stmt.executeUpdate();
		response.setResponsecode("0000");
		response.setResponsedesc("Saved Successfully.");
		if (!conn.isClosed()) {
			conn.close();
		}
		return response;
	}

}
