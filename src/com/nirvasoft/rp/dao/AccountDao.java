package com.nirvasoft.rp.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.nirvasoft.rp.dao.icbs.AccountExtendedDAO;
import com.nirvasoft.rp.shared.icbs.AccountData;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.SharedUtil;
import com.nirvasoft.rp.util.StrUtil;

public class AccountDao {

	public static String mTableNameCurrent = "CurrentAccount";
	public static String mTableName = "Accounts";

	private AccountData AccBean;

	public boolean getAccount(String aAccNumber, Connection conn)
			throws ParserConfigurationException, SAXException, IOException, ClassNotFoundException, SQLException {
		boolean result = false;
		PreparedStatement pstmt = conn.prepareStatement(
				"SELECT a.AccNumber, a.CurrencyCode, a.OpeningBalance, a.OpeningDate, a.CurrentBalance, a.ClosingDate, "
						+ " a.LastUpdate, a.Status, a.LastTransDate, a.DrawType, a.AccName, a.AccNRC, a.SAccNo, a.Remark, "
						+ "t.N1 FROM Accounts a  inner join T00005 t on a.AccNumber = t.t1 "
						+ " where a.AccNumber = ? ");
		pstmt.setString(1, aAccNumber);
		pstmt.setQueryTimeout(DAOManager.getNormalTime());
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			AccBean = new AccountData();
			readRecord(AccBean, rs, "single", conn);
			AccBean.setCurrencyRate(1);
			result = true;
		}
		pstmt.close();
		rs.close();
		return result;
	}

	public boolean getGL(String aAccNumber,Connection conn) throws Exception {
		boolean result = false;
		String l_Query = "";		
		l_Query = "SELECT accNo FROM GL Where accNo = ?";
		PreparedStatement pstmt = conn.prepareStatement(l_Query);
		pstmt.setString(1, aAccNumber);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			result = true;
		}
		return result;
	}
	public boolean getFromGL(String aAccNumber,Connection pConn) throws Exception {
		boolean result = false;
		String l_Query = "";		
		l_Query = "SELECT GLcode FROM ReferenceAccounts Where GLCode= ? And GLAccNumber = ?";
		PreparedStatement pstmt = pConn.prepareStatement(l_Query);
		pstmt.setString(1, aAccNumber);
		pstmt.setString(2, "WTR01");
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			result = true;
		}
		return result;
	}

	public AccountData getAccountsBean() {
		return AccBean;
	}
	private void readRecord(AccountData aAccBean, ResultSet aRS, String pFrom, Connection pConn) throws SQLException {
		AccountExtendedDAO l_AccExtDAO = new AccountExtendedDAO();

		aAccBean.setAccountNumber(aRS.getString("AccNumber"));

		aAccBean.setCurrencyCode(aRS.getString("CurrencyCode"));
		aAccBean.setOpeningBalance(StrUtil.round2decimals(aRS.getDouble("OpeningBalance")));
		aAccBean.setOpeningDate(SharedUtil.formatDBDate2MIT(aRS.getString("OpeningDate")));
		aAccBean.setCurrentBalance(StrUtil.round2decimals(aRS.getDouble("CurrentBalance")));
		aAccBean.setClosingDate(SharedUtil.formatDBDate2MIT(aRS.getString("ClosingDate")));
		aAccBean.setLastUpdate(SharedUtil.formatDBDate2MIT(aRS.getString("LastUpdate")));
		aAccBean.setStatus(aRS.getByte("Status"));
		aAccBean.setLastTransDate(SharedUtil.formatDBDate2MIT(aRS.getString("LastTransDate")));
		aAccBean.setDrawingType(aRS.getString("DrawType"));
		aAccBean.setAccountName(aRS.getString("AccName"));
		aAccBean.setAccountID(aRS.getString("AccNRC"));
		aAccBean.setShortAccountCode(aRS.getString("SAccNo"));
		aAccBean.setRemark(aRS.getString("Remark"));
		aAccBean.setZone(aRS.getInt("N1"));

		if (pFrom.equalsIgnoreCase("browser")) {
			aAccBean.setAccountDescription(aRS.getString("Name"));
			aAccBean.setZoneCode(aRS.getInt("ZONECODE"));
			aAccBean.setProduct(aRS.getString("PRODUCT"));
			aAccBean.setType(aRS.getString("TYPE"));
			aAccBean.setBranchCode(aRS.getString("BRANCHCODE"));
			aAccBean.setProductGL(aRS.getString("PRODUCTGL"));
			aAccBean.setCashInHandGL(aRS.getString("CASHINHANDGL"));
		} else {

			aAccBean = l_AccExtDAO.getData(aAccBean, pConn);
		}
		//aAccBean.setMinimumBalance(SharedLogic.getAccountMinBalance(aAccBean.getProduct()));

	}
	public String getBranchCode(Connection conn) throws Exception {
		String ret = "";
		String sql = "SELECT BranchCode from BankDatabases where BranchCode like '%02'";
		PreparedStatement pstmt = conn.prepareStatement(sql);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			ret = rs.getString(1);
		}
		pstmt.close();
		rs.close();
	
		return ret;
	}
	public String getProductCode(Connection conn) throws Exception {
		String ret = "";
		String sql = "select  Top 1 T1 from T00001 Where N1=6 AND n2=3 ";
		PreparedStatement pstmt = conn.prepareStatement(sql);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			ret = rs.getString(1);
		}
		pstmt.close();
		rs.close();
	
		return ret;
	}
	public String getAccountCode(Connection conn) throws Exception {
		String ret = "";
		String sql = "select  Top 1 AccountCode from AccType";
		PreparedStatement pstmt = conn.prepareStatement(sql);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			ret = rs.getString(1);
		}
		pstmt.close();
		rs.close();
	
		return ret;
	}
	public String getProductGL(Connection conn) throws Exception {
		String ret = "";
		String sql = " SELECT T4 FROM T00001 Where T1=01  AND N1=9 AND N2=1";
		PreparedStatement pstmt = conn.prepareStatement(sql);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			ret = rs.getString(1);
	}
		pstmt.close();
		rs.close();
	
		return ret;
	}
	public String getCurrencyCode(Connection conn) throws Exception {
		String ret = "";
		
		String sql = " SELECT N1 FROM CurrencyTable Where CurCode='MMK'";
		PreparedStatement pstmt = conn.prepareStatement(sql);
		ResultSet rs = pstmt.executeQuery();
		
		while (rs.next()) {
			ret = rs.getString(1);
	     }
	
		pstmt.close();
		rs.close();
	
		return ret;
	}
	public String getCashGL(Connection conn) throws Exception {
		String ret = "";
		String sql = " SELECT T4 FROM T00001 Where T1=01  AND N1=10 AND N2=1";
		PreparedStatement pstmt = conn.prepareStatement(sql);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			ret = rs.getString(1);
	}
		pstmt.close();
		rs.close();
	
		return ret;
	}
	public String getMaxSerialNo(String prefix, Connection conn) throws Exception {
		String accNumber = "";
		String sql = "Select ISNULL(MAX(CAST(SubString(AccNumber,7,6) AS INT)),0) From " + mTableName
				+ " Where SubString(AccNumber,1,6) = ? ";
		PreparedStatement pstmt = conn.prepareStatement(sql);
		pstmt.setString(1, prefix);
		ResultSet rs = pstmt.executeQuery();
		int id = 0;
		while (rs.next()) {
			id = rs.getInt(1);
		}
		pstmt.close();
		rs.close();
		id += 1;
		accNumber = prefix + GeneralUtil.leadZeros("" + id, 6);
		return accNumber;
	}
	public String getMaxSerialNoNSB(String prefix, Connection conn) throws Exception {
		String accNumber = "";
		String sql = "Select ISNULL(MAX(CAST(SubString(AccNumber,10,6) AS INT)),0) From " + mTableName
				+ " Where SubString(AccNumber,1,9) = ? ";
		PreparedStatement pstmt = conn.prepareStatement(sql);
		pstmt.setString(1, prefix);
		ResultSet rs = pstmt.executeQuery();
		int id = 0;
		while (rs.next()) {
			id = rs.getInt(1);
		}
		pstmt.close();
		rs.close();
		id += 1;
		accNumber = prefix + GeneralUtil.leadZeros("" + id, 6);
		return accNumber;
	}
	public boolean saveAccountsBalance(AccountData data, Connection conn) throws SQLException {
		boolean ret = false;
		String sql = "INSERT INTO Accounts_Balance VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement ps = conn.prepareStatement(sql);
		updateRecord(data, ps, true);
		if (ps.executeUpdate() > 0) {
			ret = true;
		}
		ps.close();
		return ret;
	}
	private void updateRecord(AccountData aData, PreparedStatement ps, boolean isFlag) throws SQLException {
		int i = 1;
		ps.setString(i++, aData.getAccountNumber());
		ps.setString(i++, aData.getCurrencyCode());
		ps.setDouble(i++, aData.getOpeningBalance());
		ps.setString(i++, aData.getOpeningDate());
		if (isFlag) {
			ps.setDouble(i++, aData.getCurrentBalance());
		}
		ps.setString(i++, aData.getClosingDate());
		ps.setString(i++, aData.getLastUpdate());
		ps.setInt(i++, aData.getStatus());
		ps.setString(i++, aData.getLastTransDate());
		ps.setString(i++, aData.getDrawingType());
		ps.setString(i++, aData.getAccountName());
		ps.setString(i++, aData.getpAccountNrc());//ndh
		ps.setString(i++, aData.getpSAccNo());//ndh
//		ps.setString(i++, "");
//		ps.setString(i++, "");
		ps.setString(i++, aData.getRemark());
		ps.setString(i++, aData.getBranchCode());
	}
	public boolean save(AccountData data, Connection conn) throws SQLException {
		boolean ret = false;
		String sql = "INSERT INTO " + mTableName + " VALUES(?,?,?,?,?"
				+ ",?,?,?,?,?"
				+ ",?,?,?,?,?)";
		PreparedStatement ps = conn.prepareStatement(sql);
		updateRecord(data, ps, true);
		if (ps.executeUpdate() > 0) {
			ret = true;
		}
		ps.close();
		return ret;
	}
	public boolean saveCurrentAcc(String accNumber, Connection conn) throws SQLException {
		boolean ret = false;
		String sql = "INSERT INTO " + mTableNameCurrent + "(AccNumber) VALUES(?)";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, accNumber);
		if (ps.executeUpdate() > 0) {
			ret = true;
		}
		ps.close();
		return ret;
	}
	public boolean saveT00005(String accNumber, String prodCode, String accCode, String ccy, String branchCode,
			String prodGL, String cashGL, Connection conn) throws SQLException {
		boolean ret = false;
		String sql = "INSERT INTO T00005 VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement ps = conn.prepareStatement(sql);
		updateRecordT00005(accNumber, prodCode, accCode, ccy, branchCode, prodGL, cashGL, ps);
		if (ps.executeUpdate() > 0) {
			ret = true;
		}
		ps.close();
		return ret;
	}
	private void updateRecordT00005(String accNumber, String prodCode, String accCode, String ccy, String branchCode,
			String prodGL, String cashGL, PreparedStatement ps) throws SQLException {
		int i = 1;
		ps.setInt(i++, 52);
		ps.setInt(i++, 0);
		ps.setInt(i++, 0);
		ps.setInt(i++, 0);
		ps.setString(i++, accNumber);
		ps.setString(i++, prodCode);
		ps.setString(i++, accCode);
		if (ccy.equals("1.0")){
			ps.setString(i++, "MMK");
		} else {
			ps.setString(i++, ccy);
		}
		ps.setString(i++, branchCode);
		ps.setString(i++, prodGL);
		ps.setString(i++, cashGL);
		ps.setString(i++, "");

	}
	
	
}
