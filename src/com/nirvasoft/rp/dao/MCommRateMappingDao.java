package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.nirvasoft.rp.data.MCommRateMappingData;

public class MCommRateMappingDao {

	private final String tblname = "MCommRateMapping";

	public MCommRateMappingData getMCommMappingdataByMerId(String pMerId, String operator, Connection pConn)
			throws SQLException {

		MCommRateMappingData data = new MCommRateMappingData();
		String whereclause = "";
		if(!operator.equals("")){
			whereclause +=" And t1=?";
		}
		String sql = "select MerchantID,KindOfComIssuer,"
				+ "CommRef1,CommRef2,CommRef3,CommRef4,CommRef5,N2 as penaltyDays from " + tblname
				+ " where RecordStatus<>4 and MerchantID = ? " + whereclause;//and t1 = ?

		PreparedStatement ps = pConn.prepareStatement(sql);
		ps.setString(1, pMerId);
		if(!operator.equals("")){
			ps.setString(2, operator);
		}
		
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			data.setMerchantID(rs.getString("MerchantID"));
			data.setKindOfComGiver(rs.getInt("KindOfComIssuer"));
			data.setCommRef1(rs.getString("CommRef1"));
			data.setCommRef2(rs.getString("CommRef2"));
			data.setCommRef3(rs.getString("CommRef3"));
			data.setCommRef4(rs.getString("CommRef4"));
			data.setCommRef5(rs.getString("CommRef5"));
			data.setPenaltyDays(rs.getString("penaltyDays"));
		}

		return data;

	}
}
