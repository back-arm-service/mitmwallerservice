package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.cms.shared.ComboData;
import com.nirvasoft.rp.shared.ComboDataset;

public class RegionDAO {

	public static ComboDataset getStateListByRegion(String regionCode, Connection l_Conn) throws SQLException {
		ComboDataset ret = new ComboDataset();
		ArrayList<ComboData> branchData = new ArrayList<ComboData>();
		String condition = "";
		if (!regionCode.equals("") && !regionCode.equals("ALL")) {
			condition = " and Region in(" + regionCode + ")";
		}

		String Query = "select Code,DespEng from AddressRef " + condition;
		PreparedStatement pstmt = l_Conn.prepareStatement(Query);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			ComboData data = new ComboData();
			data.setValue(rs.getLong("Code"));
			data.setCaption(rs.getString("DespEng"));
			branchData.add(data);
		}
		ComboData[] dataarry = new ComboData[branchData.size()];
		for (int i = 0; i < branchData.size(); i++) {
			dataarry[i] = branchData.get(i);
		}

		ret.setData(dataarry);
		return ret;
	}

}
