package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.nirvasoft.rp.shared.BillPaymentTransaction;
import com.nirvasoft.rp.shared.BillPaymentTransactionReq;
import com.nirvasoft.rp.shared.BillPaymentTransactionRes;
import com.nirvasoft.rp.util.GeneralUtil;

public class BillPaymentTransactionDao {

	public BillPaymentTransactionRes getBillPaymentTransList(BillPaymentTransactionReq req, Connection conn)
			throws ParseException {
		BillPaymentTransactionRes res = new BillPaymentTransactionRes();
		try {
			int pagesize = req.getPageSize();
			int currentpage = req.getCurrentPage();
			int startPage = (currentpage - 1) * pagesize;
			int endPage = pagesize + startPage;
			String merchantID = req.getMerchantID();
			String userId = req.getUserID();
			String account = req.getAcctNo();
			String fromDate = req.getFromDate();
			String toDate = req.getToDate();
			String status = req.getStatus();
			String durationType = req.getDurationType();
			DecimalFormat df = new DecimalFormat("#,###.00");
			ArrayList<BillPaymentTransaction> dataList = new ArrayList<BillPaymentTransaction>();
			String whereclause = "";
			if (!merchantID.equals("")) {
				whereclause += " and t20 = ? ";
			}
			if (!account.equals("")) {
				whereclause += " and fromaccount = ? ";
			}
			if (userId != null || userId != "") {
				whereclause += " and T24 = ? ";
			}
			if (durationType.equals("0")) {
				startPage = 0;
				endPage = 10;
			}
			if (durationType.equals("1")) {
				whereclause += " and transdate <= replace(cast(GETDATE() as date),'-','') and transdate >= replace(cast(dateadd(day, -2, getdate()) as date),'-','')";

			}
			if (durationType.equals("2")) {
				whereclause += " and transdate <= replace(cast(GETDATE() as date),'-','') and transdate >= replace(cast(dateadd(day, -5, getdate()) as date),'-','')";
			}
			if (durationType.equals("3")) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				if (fromDate == null || fromDate == "" || fromDate.equalsIgnoreCase(null) || fromDate.equals("")) {
					res.setDesc("Start date is mandatory");
					res.setCode("0014");
					return res;
				} else if (toDate == null || toDate == "" || toDate.equalsIgnoreCase(null) || toDate.equals("")) {
					res.setDesc("End date is mandatory");
					res.setCode("0014");
					return res;
				} else if (!sdf.parse(toDate).equals(sdf.parse(fromDate))) {
					if (!sdf.parse(toDate).after(sdf.parse(fromDate))) {
						res.setDesc("Start date must not exceed end date!");
						res.setCode("0014");
						return res;
					} else {
						whereclause += " and transdate between ? and ? ";

					}
				} else {
					whereclause += " and transdate between ? and ? "; // EQUAL
				}

			}
			if (!status.equals("")) {
				if (status.equalsIgnoreCase("fail")) {
					whereclause += " and t14 <>1 ";
				} else if (status.equalsIgnoreCase("success")) {
					whereclause += " and t14=1 ";
				}
			}
			String processingCode = "";
			String sql = "";
			String sql2 = "select processingCode from PayTemplateHeader where merchantid = ?";
			PreparedStatement ps2 = conn.prepareStatement(sql2);
			ps2.setString(1, merchantID);
			ResultSet rs2 = ps2.executeQuery();
			while (rs2.next()) {
				processingCode = rs2.getString("processingCode");
			}
			if (processingCode.equals("050200")) {
				sql = "select * from (select Row_Number() Over (Order By Transtime desc) as rownumber, "
						+ "customercode as cardNo, Transtime, transDate, FlexRefNo as bankRefNumber, amount - ( Cast(T15 as float) + Cast(T25 as float)) as amount, CurrencyCode as CCY, "
						+ "FromAccount as debitAccount, t8 as packageName, t13 as sMonth, t19 as merchantName, t11 as expiredDate, "
						+ "t14 as resultCode, t12 as resultDesc,Cast(T15 as float) as ServiceCharges, Cast(T25 as float) as Tax,"
						+ " Cast(T15 as float) + Cast(T18 as float) as TotalAmount  from dispaymenttransaction where 1=1 "
						+ whereclause + ") as billResult where 1=1 and  rownumber > " + startPage + " And rownumber <= "
						+ endPage + " order By Transtime desc";
			} else if (processingCode.equals("080400")) {
				sql = "select * from (select Row_Number() Over (Order By Transtime desc) as rownumber, "
						+ "customercode as cardNo, Transtime, transDate, FlexRefNo as bankRefNumber, amount, CurrencyCode as CCY, "
						+ "FromAccount as debitAccount, t8 as packageName, t13 as sMonth, t19 as merchantName, t11 as expiredDate, "
						+ "t14 as resultCode, t12 as resultDesc,Cast(T15 as float) as ServiceCharges, Cast(T18 as float) as Tax,"
						+ " amount + Cast(T15 as float) + Cast(T18 as float) as TotalAmount  from dispaymenttransaction where 1=1 "
						+ whereclause + ") as billResult where 1=1 and  rownumber > " + startPage + " And rownumber <= "
						+ endPage + " order By Transtime desc";
			}
			PreparedStatement ps = conn.prepareStatement(sql);
			int j = 1;
			if (!merchantID.equals("")) {
				ps.setString(j++, merchantID);
			}
			if (!account.equals("")) {
				ps.setString(j++, account);
			}
			if (userId != null || userId != "") {
				ps.setString(j++, userId);
			}
			if (durationType.equals("3")) {
				ps.setString(j++, fromDate);
				ps.setString(j++, toDate);
			}
			System.out.println("whereclause " + whereclause);
			System.out.println("sql " + sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				BillPaymentTransaction data = new BillPaymentTransaction();
				String expDate = "";
				data.setCardNo(rs.getString("cardNo"));
				String transdate = rs.getString("transDate");
				data.setTransDate(GeneralUtil.mobiledateformat(transdate));
				data.setBankRefNumber(rs.getString("bankRefNumber"));
				data.setAmount(df.format(rs.getDouble("amount")));
				data.setAmountServiceCharges(df.format(Double.parseDouble(rs.getString("ServiceCharges"))));
				if (rs.getString("Tax").equals("")) {
					data.setAmountTax(df.format(0));
				} else {
					data.setAmountTax(df.format(Double.parseDouble(rs.getString("Tax"))));
				}
				data.setAmountTotal(df.format(Double.parseDouble(rs.getString("TotalAmount"))));
				data.setCcy(rs.getString("CCY"));
				data.setDebitAccount(rs.getString("debitAccount"));
				data.setPackageName(rs.getString("packageName"));
				data.setMonth(rs.getString("sMonth"));
				data.setTxnCode("1");
				expDate = rs.getString("expiredDate");
				if (!(expDate.equals("") || expDate.equalsIgnoreCase("null"))) {
					if (expDate.matches(
							"^(([0-9])|([0-2][0-9])|([3][0-1]))\\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\\/[0-9]{4}$")) {
						data.setExpiredDate(GeneralUtil.datetoddMMMyyyywithSlach(expDate));
					} else if (expDate.matches("^[0-9]{8}$")) {

						data.setExpiredDate(expDate.substring(0, 4) + "-" + expDate.substring(4, 6) + "-"
								+ expDate.substring(6, 8));
					}
				}
				data.setMerchantName(rs.getString("merchantName"));
				data.setResultCode(rs.getString("resultCode"));
				data.setResultDesc(rs.getString("resultDesc"));
				dataList.add(data);
			}
			BillPaymentTransaction[] dataArr = new BillPaymentTransaction[dataList.size()];
			for (int i = 0; i < dataList.size(); i++) {
				dataArr[i] = dataList.get(i);
			}
			if (dataList.size() > 0) {
				res.setBillResults(dataArr);
				res.setCode("0000");
				res.setDesc("Successfully");

			} else {
				res.setCode("0014");
				res.setDesc("There is no data");
			}

			PreparedStatement pstm = conn
					.prepareStatement("Select COUNT(*) As total from dispaymenttransaction where 1=1 " + whereclause);
			int k = 1;
			if (!merchantID.equals("")) {
				pstm.setString(k++, merchantID);
			}
			if (!account.equals("")) {
				pstm.setString(k++, account);
			}
			if (userId != null || userId != "") {
				pstm.setString(k++, userId);
			}
			if (durationType.equals("3")) {
				pstm.setString(k++, fromDate);
				pstm.setString(k++, toDate);
			}
			ResultSet result = pstm.executeQuery();
			result.next();
			int totalcount = result.getInt("total");
			// if duration type is default,for only 10 record requirement
			if (durationType.equals("0")) {
				if (totalcount > 10) {
					res.setTotalCount(10);
				} else {
					res.setTotalCount(result.getInt("total"));
				}
			} else {
				res.setTotalCount(result.getInt("total"));
			}
			// end
			res.setCurrentPage(req.getCurrentPage());
			res.setPageSize(req.getPageSize());

			if (res.getTotalCount() % pagesize != 0) {
				res.setPageCount((res.getTotalCount() / pagesize) + 1);
			} else {
				res.setPageCount((res.getTotalCount() / pagesize));
			}
			ps.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
			res.setCode("0014");
			res.setDesc(e.getMessage());
		}
		return res;
	}

}
