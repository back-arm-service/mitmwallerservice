package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.shared.CMSMerchantData;

public class QuickPayDAO {

	public CMSMerchantData getMerchantIDByProcessingCode(String aPCode, String aMerchantID, Connection aConnection)
			throws SQLException {
		CMSMerchantData data = new CMSMerchantData();
		PreparedStatement stmt = aConnection.prepareStatement(
				"select MerchantID,MerchantName from PayTemplateHeader where ProcessingCode = ? and MerchantID = ? ");
		stmt.setString(1, aPCode);
		stmt.setString(2, aMerchantID);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			data.setUserId(rs.getString("MerchantID"));
			data.setUserName(rs.getString("MerchantName"));
		}

		stmt.close();
		rs.close();

		return data;
	}

	public CMSMerchantData getMerchantInfo(String aMerchantID, Connection aConnection) throws SQLException {
		CMSMerchantData l_data = new CMSMerchantData();
		PreparedStatement stmt = aConnection
				.prepareStatement("select * from CMSMerchant where recordstatus<>4 and userid = ?");
		stmt.setString(1, aMerchantID);
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			l_data.setUserId(rs.getString("userid"));
			l_data.setUserName(rs.getString("username"));
			l_data.setN1(rs.getInt("n1"));
			l_data.setN2(rs.getInt("n2"));
			l_data.setN3(rs.getInt("n3"));
			l_data.setSamecity(rs.getDouble("sameCity"));
			l_data.setDiffcity(rs.getDouble("diffCity"));
			l_data.setRate(rs.getDouble("rate"));
			l_data.setMinimumamount(rs.getDouble("minimumAmount"));
			l_data.setEmailtype(rs.getInt("emailType"));
			l_data.setN4(rs.getInt("n4"));
			l_data.setN5(rs.getInt("n5"));
			l_data.setT13(rs.getString("T13"));
			l_data.setT14(rs.getString("T14"));
			l_data.setT15(rs.getString("T15"));
			l_data.setT16(rs.getString("T16"));
			l_data.setT17(rs.getString("T17"));
			l_data.setT18(rs.getString("T18"));
			l_data.setT19(rs.getString("T19"));
		}

		stmt.close();
		rs.close();

		return l_data;
	}

	public String getToAccountByProcessingCode(String aPCode, Connection aConnection) throws SQLException {
		String AccountNumber = "";
		PreparedStatement stmt = aConnection.prepareStatement(
				"select AccountNumber from CMSMerchantAccRef where recordStatus = 0 and t1 = (select MerchantID from PayTemplateHeader where ProcessingCode = ? )");
		stmt.setString(1, aPCode);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			AccountNumber = rs.getString("AccountNumber");
		}

		stmt.close();
		rs.close();

		return AccountNumber;
	}

	public Result IsDrCrAccountsExist(String mobileUserId, String drAcctNo, String crAcctNo, String aMerchantID,
			Connection conn) throws SQLException {
		Result ret = new Result();
		PreparedStatement stmt = null;

		String cr_query = "select Top(1) t1 from CMSMerchantAccRef Where recordStatus <> 4 and T1 = ? and AccountNumber =  ?";
		stmt = conn.prepareStatement(cr_query);
		stmt.setString(1, aMerchantID);
		stmt.setString(2, crAcctNo);
		ResultSet rst = stmt.executeQuery();
		if (rst.next()) {
			ret.setState(true);
			ret.setMsgCode("0000");
		} else {
			ret.setState(false);
			ret.setMsgCode("0014");
			ret.setMsgDesc("Merchant Credit Account is not mapping!");
		}

		stmt.close();
		rst.close();

		return ret;
	}

	public void updateSkyNetPlusRevStatus(int n2Status, String desc, String reversaldatetime, String aRSPName,
			String aCardExpire, String aResultText, int aResultCodeNo, String aFlexID, Connection aConnection)
			throws SQLException {
		PreparedStatement stmt = aConnection.prepareStatement(
				"update dispaymenttransaction set n2 = ? , t2 = ?, t4 = ?, t7 = ?, t11 = ?, t12 = ? , t14 = ? where flexrefno = ? ");
		int index = 1;
		stmt.setInt(index++, n2Status);
		stmt.setString(index++, desc);
		stmt.setString(index++, reversaldatetime);
		stmt.setString(index++, aRSPName);
		stmt.setString(index++, aCardExpire);
		stmt.setString(index++, aResultText);
		stmt.setInt(index++, aResultCodeNo);
		stmt.setString(index++, aFlexID);
		stmt.executeUpdate();

		stmt.close();

	}

	public void updateSkyNetStatus(String aRSPName, String aCardExpire, String aResultText, int aResultCodeNo,
			String aFlexID, Connection aConnection) throws SQLException {
		PreparedStatement stmt = aConnection.prepareStatement(
				"update dispaymenttransaction set t7 = ?, t11 = ?, t12 = ? , t14 = ? where flexrefno = ? ");
		stmt.setString(1, aRSPName);
		stmt.setString(2, aCardExpire);
		stmt.setString(3, aResultText);
		stmt.setInt(4, aResultCodeNo);
		stmt.setString(5, aFlexID);
		stmt.executeUpdate();
		stmt.close();
	}
}
