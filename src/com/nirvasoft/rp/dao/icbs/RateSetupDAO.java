package com.nirvasoft.rp.dao.icbs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.shared.icbs.RateAmountRangeSetupData;
import com.nirvasoft.rp.shared.icbs.RateSetupData;

public class RateSetupDAO {

	private RateSetupData m_rateSetup;
	private ArrayList<RateSetupData> m_ratesetupList;
	private String mTable = "RateSetup";

	public RateSetupDAO() {
		m_rateSetup = new RateSetupData();
		m_ratesetupList = new ArrayList<RateSetupData>();
	}

	public boolean addratesetup(RateSetupData pdata, Connection pconn) throws SQLException {
		boolean l_result = false;
		String l_query = "INSERT INTO " + mTable
				+ " ([Type],[FromBank],[FromBranch],[ToBank],[ToBranch],[InOut],[Com1],[Com1Type],[Com1Min],[Com1Max],"
				+ "[Com2],[Com2Type],[Com2Min],[Com2Max],[Com3],[Com3Type],[Com3Min],[Com3Max],[Com4],[Com4Type],"
				+ "[Com4Min],[Com4Max],[Com5],[Com5Type],[Com5Min],[Com5Max],[Com6],[Com6Type],[Com6Min],[Com6Max],"
				+ "[Com7],[Com7Type],[Com7Min],[Com7Max],[Com8],[Com8Type],[Com8Min],[Com8Max],[Com9],[Com9Type],"
				+ "[Com9Min],[Com9Max],[Com10],[Com10Type],[Com10Min],[Com10Max],[Com11],[Com11Type],[Com11Min],[Com11Max],"
				+ "[Com12],[Com12Type],[Com12Min],[Com12Max],[N1],[N2],[N3],[T1],[T2],[T3],"
				+ "[Com1DecimalType],[Com2DecimalType],[Com3DecimalType],[Com4DecimalType],[Com5DecimalType],[Com6DecimalType]"
				+ ",[Com7DecimalType],[Com8DecimalType],[Com9DecimalType],[Com10DecimalType],[Com11DecimalType],[Com12DecimalType])"
				+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?"
				+ ",?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement l_pstmt = pconn.prepareStatement(l_query);
		updaterecord(pdata, l_pstmt);
		l_pstmt.setQueryTimeout(DAOManager.getNormalTime());
		if (l_pstmt.executeUpdate() > 0) {
			l_result = true;
		}
		l_pstmt.close();

		return l_result;
	}

	public String getBankName(Connection pconn) throws SQLException {
		String result = "";
		String l_query = "SELECT BankName FROM BankSetup";
		PreparedStatement l_pstmt = pconn.prepareStatement(l_query);
		ResultSet l_RS = l_pstmt.executeQuery();
		while (l_RS.next()) {
			result = l_RS.getString("BankName");
		}
		l_pstmt.close();
		return result;
	}

	public RateSetupData getBankNamenBranchName(Connection pconn, RateSetupData aData, int Bank) throws SQLException {
		String BranchCode = "";
		if (Bank == 0) {
			BranchCode = aData.getFromBranch();
		} else {
			BranchCode = aData.getToBranch();
		}
		String l_query = "SELECT Bs.BankName,Bd.BranchName FROM BankSetup Bs, BankDatabases Bd WHERE Bd.BranchCode='"
				+ BranchCode + "'";
		PreparedStatement l_pstmt = pconn.prepareStatement(l_query);
		ResultSet l_RS = l_pstmt.executeQuery();
		while (l_RS.next()) {
			if (Bank == 0) {
				aData.setFromBank(l_RS.getString("BankName"));
				aData.setFromBranch(l_RS.getString("BranchName"));
			} else {
				aData.setToBank(l_RS.getString("BankName"));
				aData.setToBranch(l_RS.getString("BranchName"));
			}

		}
		l_pstmt.close();
		return aData;
	}

	public String getBrachName(Connection pconn, String aBranchCode) throws SQLException {
		String result = "";
		String l_query = "SELECT BranchName FROM BankDatabases WHERE BranchCode ='" + aBranchCode + "'";
		PreparedStatement l_pstmt = pconn.prepareStatement(l_query);
		ResultSet l_RS = l_pstmt.executeQuery();
		while (l_RS.next()) {
			result = l_RS.getString("BranchName");
		}
		l_pstmt.close();
		return result;
	}

	public RateSetupData getrateSetup() {
		return m_rateSetup;
	}

	public RateSetupData getRateSetupData(int pSerialNo, Connection pconn) throws SQLException {
		String l_query = "SELECT SerialNo,Type,FromBank,FromBranch,ToBank,ToBranch,InOut,Com1,Com1Type,Com1Min,Com1Max,Com2,Com2Type,Com2Min,"
				+ "Com2Max,Com3,Com3Type,Com3Min,Com3Max,Com4,Com4Type,Com4Min,Com4Max,Com5,Com5Type,Com5Min,Com5Max,Com6,Com6Type,"
				+ "Com6Min,Com6Max,Com7,Com7Type,Com7Min,Com7Max,Com8,Com8Type,Com8Min,Com8Max,Com9,Com9Type,Com9Min,Com9Max,"
				+ "Com10,Com10Type,Com10Min,Com10Max,Com11,Com11Type,Com11Min,Com11Max,Com12,Com12Type,Com12Min,Com12Max,Com1DecimalType,"
				+ "Com2DecimalType,Com3DecimalType,Com4DecimalType,Com5DecimalType,Com6DecimalType,Com7DecimalType,Com8DecimalType,"
				+ "Com9DecimalType,Com10DecimalType,Com11DecimalType,Com12DecimalType FROM " + mTable
				+ " WHERE SerialNo = " + pSerialNo;
		PreparedStatement l_pstmt = pconn.prepareStatement(l_query);
		ResultSet l_RS = l_pstmt.executeQuery();
		while (l_RS.next()) {
			m_rateSetup = new RateSetupData();
			readRecord(m_rateSetup, l_RS);
			// m_RateSetupData.add(m_rateSetup);
		}
		l_pstmt.close();
		return m_rateSetup;
	}

	public RateSetupData getRateSetupData(int pType, String pFromBranch, String pToBank, String ptoBranch,
			Connection pConn) throws SQLException {
		RateSetupData l_RateSetupData = new RateSetupData();
		String l_Query = "SELECT SerialNo,Type,FromBank,FromBranch,ToBank,ToBranch,InOut,Com1,Com1Type,Com1Min,Com1Max,Com2,Com2Type,Com2Min,"
				+ "Com2Max,Com3,Com3Type,Com3Min,Com3Max,Com4,Com4Type,Com4Min,Com4Max,Com5,Com5Type,Com5Min,Com5Max,Com6,Com6Type,"
				+ "Com6Min,Com6Max,Com7,Com7Type,Com7Min,Com7Max,Com8,Com8Type,Com8Min,Com8Max,Com9,Com9Type,Com9Min,Com9Max,"
				+ "Com10,Com10Type,Com10Min,Com10Max,Com11,Com11Type,Com11Min,Com11Max,Com12,Com12Type,Com12Min,Com12Max,Com1DecimalType,"
				+ "Com2DecimalType,Com3DecimalType,Com4DecimalType,Com5DecimalType,Com6DecimalType,Com7DecimalType,Com8DecimalType,"
				+ "Com9DecimalType,Com10DecimalType,Com11DecimalType,Com12DecimalType FROM " + mTable
				+ " WHERE Type = ? " + "AND FromBank = 0 AND FromBranch = ? AND ToBank = ? AND ToBranch = ?";
		PreparedStatement l_pstmt = pConn.prepareStatement(l_Query);
		l_pstmt.setInt(1, pType);
		l_pstmt.setString(2, pFromBranch);
		l_pstmt.setString(3, pToBank);
		l_pstmt.setString(4, ptoBranch);
		ResultSet l_Rs = l_pstmt.executeQuery();
		while (l_Rs.next()) {
			readRecord(l_RateSetupData, l_Rs);
		}
		return l_RateSetupData;
	}

	public boolean getRateSetupDataForATMCommission(int pSerialNo, Connection pconn) throws SQLException {
		boolean l_result = false;
		String l_query = "SELECT SerialNo,Type,FromBank,FromBranch,ToBank,ToBranch,InOut,Com1,Com1Type,Com1Min,Com1Max,Com2,Com2Type,Com2Min,"
				+ "Com2Max,Com3,Com3Type,Com3Min,Com3Max,Com4,Com4Type,Com4Min,Com4Max,Com5,Com5Type,Com5Min,Com5Max,Com6,Com6Type,"
				+ "Com6Min,Com6Max,Com7,Com7Type,Com7Min,Com7Max,Com8,Com8Type,Com8Min,Com8Max,Com9,Com9Type,Com9Min,Com9Max,"
				+ "Com10,Com10Type,Com10Min,Com10Max,Com11,Com11Type,Com11Min,Com11Max,Com12,Com12Type,Com12Min,Com12Max,Com1DecimalType,"
				+ "Com2DecimalType,Com3DecimalType,Com4DecimalType,Com5DecimalType,Com6DecimalType,Com7DecimalType,Com8DecimalType,"
				+ "Com9DecimalType,Com10DecimalType,Com11DecimalType,Com12DecimalType FROM " + mTable
				+ " WHERE SerialNo = " + pSerialNo;
		PreparedStatement l_pstmt = pconn.prepareStatement(l_query);
		ResultSet l_RS = l_pstmt.executeQuery();
		while (l_RS.next()) {
			l_result = true;
		}
		l_pstmt.close();
		return l_result;
	}

	public ArrayList<RateSetupData> getratesetupList() {
		return m_ratesetupList;
	}

	public ArrayList<RateSetupData> getRateSetupList(String pfield, Connection pconn) throws SQLException {
		ArrayList<RateSetupData> m_RateSetupList = new ArrayList<RateSetupData>();
		String str[] = pfield.split(",");
		String branchCode = "";
		String bankCode = "";
		if (str.length > 1) {
			branchCode = str[0];
			bankCode = str[1];
		} else {
			if (str[0].length() == 1) {
				bankCode = str[0];
			} else {
				branchCode = str[0];
			}
		}
		String l_query = "SELECT SerialNo,Type,FromBank,FromBranch,ToBank,ToBranch,InOut,Com1,Com1Type,Com1Min,Com1Max,Com2,Com2Type,Com2Min,"
				+ "Com2Max,Com3,Com3Type,Com3Min,Com3Max,Com4,Com4Type,Com4Min,Com4Max,Com5,Com5Type,Com5Min,Com5Max,Com6,Com6Type,"
				+ "Com6Min,Com6Max,Com7,Com7Type,Com7Min,Com7Max,Com8,Com8Type,Com8Min,Com8Max,Com9,Com9Type,Com9Min,Com9Max,"
				+ "Com10,Com10Type,Com10Min,Com10Max,Com11,Com11Type,Com11Min,Com11Max,Com12,Com12Type,Com12Min,Com12Max,Com1DecimalType,"
				+ "Com2DecimalType,Com3DecimalType,Com4DecimalType,Com5DecimalType,Com6DecimalType,Com7DecimalType,Com8DecimalType,"
				+ "Com9DecimalType,Com10DecimalType,Com11DecimalType,Com12DecimalType FROM " + mTable + " WHERE 1=1";
		if (!branchCode.equals("")) {
			l_query += " AND FromBranch ='" + branchCode + "' OR ToBranch = '" + branchCode + "'";
		}
		if (str.length > 1 || !bankCode.equals("")) {
			l_query += "AND ToBank ='" + bankCode + "' OR FromBank ='" + bankCode + "'";
		}
		PreparedStatement l_pstmt = pconn.prepareStatement(l_query);
		ResultSet l_RS = l_pstmt.executeQuery();
		while (l_RS.next()) {
			m_rateSetup = new RateSetupData();
			readRecord(m_rateSetup, l_RS);
			m_RateSetupList.add(m_rateSetup);
		}
		l_pstmt.close();
		return m_RateSetupList;
	}

	public ArrayList<RateSetupData> getRateSetupList(String pfield, int pType, Connection pconn) throws SQLException {
		ArrayList<RateSetupData> m_RateSetupList = new ArrayList<RateSetupData>();
		String str[] = pfield.split(",");
		String branchCode = "";
		String bankCode = "";
		if (str.length > 1) {
			branchCode = str[0];
			bankCode = str[1];
		} else {
			if (str[0].length() == 1) {
				bankCode = str[0];
			} else {
				branchCode = str[0];
			}
		}
		String l_query = "SELECT SerialNo,Type,FromBank,FromBranch,ToBank,ToBranch,InOut,Com1,Com1Type,Com1Min,Com1Max,Com2,Com2Type,Com2Min,"
				+ "Com2Max,Com3,Com3Type,Com3Min,Com3Max,Com4,Com4Type,Com4Min,Com4Max,Com5,Com5Type,Com5Min,Com5Max,Com6,Com6Type,"
				+ "Com6Min,Com6Max,Com7,Com7Type,Com7Min,Com7Max,Com8,Com8Type,Com8Min,Com8Max,Com9,Com9Type,Com9Min,Com9Max,"
				+ "Com10,Com10Type,Com10Min,Com10Max,Com11,Com11Type,Com11Min,Com11Max,Com12,Com12Type,Com12Min,Com12Max,Com1DecimalType,"
				+ "Com2DecimalType,Com3DecimalType,Com4DecimalType,Com5DecimalType,Com6DecimalType,Com7DecimalType,Com8DecimalType,"
				+ "Com9DecimalType,Com10DecimalType,Com11DecimalType,Com12DecimalType FROM " + mTable + " WHERE 1=1";
		if (!branchCode.equals("")) {
			l_query += " AND FromBranch ='" + branchCode + "' OR ToBranch = '" + branchCode + "'";
		}
		if (str.length > 1 || !bankCode.equals("")) {
			l_query += " AND ToBank ='" + bankCode + "' OR FromBank ='" + bankCode + "'";
		}

		l_query += " AND Type = " + pType + "";

		PreparedStatement l_pstmt = pconn.prepareStatement(l_query);
		ResultSet l_RS = l_pstmt.executeQuery();
		while (l_RS.next()) {
			m_rateSetup = new RateSetupData();
			// readRecord(m_rateSetup, l_RS);
			readRecord(m_rateSetup, l_RS, pconn);
			m_RateSetupList.add(m_rateSetup);
		}
		l_pstmt.close();
		return m_RateSetupList;
	}

	private void readRecord(RateSetupData pdata, ResultSet pRS) throws SQLException {
		pdata.setSerialNo(pRS.getInt("SerialNo"));
		pdata.setType(pRS.getInt("Type"));
		pdata.setFromBank(pRS.getString("FromBank"));
		pdata.setFromBranch(pRS.getString("FromBranch"));
		pdata.setToBank(pRS.getString("ToBank"));
		pdata.setToBranch(pRS.getString("ToBranch"));
		pdata.setInOut(pRS.getInt("InOut"));
		pdata.setCom1(pRS.getDouble("Com1"));
		pdata.setCom1Type(pRS.getString("Com1Type"));
		pdata.setCom1Min(pRS.getDouble("Com1Min"));
		pdata.setCom1Max(pRS.getDouble("Com1Max"));
		pdata.setCom2(pRS.getDouble("Com2"));
		pdata.setCom2Type(pRS.getString("Com2Type"));
		pdata.setCom2Min(pRS.getDouble("Com2Min"));
		pdata.setCom2Max(pRS.getDouble("Com2Max"));
		pdata.setCom3(pRS.getDouble("Com3"));
		pdata.setCom3Type(pRS.getString("Com3Type"));
		pdata.setCom3Min(pRS.getDouble("Com3Min"));
		pdata.setCom3Max(pRS.getDouble("Com3Max"));
		pdata.setCom4(pRS.getDouble("Com4"));
		pdata.setCom4Type(pRS.getString("Com4Type"));
		pdata.setCom4Min(pRS.getDouble("Com4Min"));
		pdata.setCom4Max(pRS.getDouble("Com4Max"));
		pdata.setCom5(pRS.getDouble("Com5"));
		pdata.setCom5Type(pRS.getString("Com5Type"));
		pdata.setCom5Min(pRS.getDouble("Com5Min"));
		pdata.setCom5Max(pRS.getDouble("Com5Max"));
		pdata.setCom6(pRS.getDouble("Com6"));
		pdata.setCom6Type(pRS.getString("Com6Type"));
		pdata.setCom6Min(pRS.getDouble("Com6Min"));
		pdata.setCom6Max(pRS.getDouble("Com6Max"));
		pdata.setCom7(pRS.getDouble("Com7"));
		pdata.setCom7Type(pRS.getString("Com7Type"));
		pdata.setCom7Min(pRS.getDouble("Com7Min"));
		pdata.setCom7Max(pRS.getDouble("Com7Max"));
		pdata.setCom8(pRS.getDouble("Com8"));
		pdata.setCom8Type(pRS.getString("Com8Type"));
		pdata.setCom8Min(pRS.getDouble("Com8Min"));
		pdata.setCom8Max(pRS.getDouble("Com8Max"));
		pdata.setCom9(pRS.getDouble("Com9"));
		pdata.setCom9Type(pRS.getString("Com9Type"));
		pdata.setCom9Min(pRS.getDouble("Com9Min"));
		pdata.setCom9Max(pRS.getDouble("Com9Max"));
		pdata.setCom10(pRS.getDouble("Com10"));
		pdata.setCom10Type(pRS.getString("Com10Type"));
		pdata.setCom10Min(pRS.getDouble("Com10Min"));
		pdata.setCom10Max(pRS.getDouble("Com10Max"));
		pdata.setCom11(pRS.getDouble("Com11"));
		pdata.setCom11Type(pRS.getString("Com11Type"));
		pdata.setCom11Min(pRS.getDouble("Com11Min"));
		pdata.setCom11Max(pRS.getDouble("Com11Max"));
		pdata.setCom12(pRS.getDouble("Com12"));
		pdata.setCom12Type(pRS.getString("Com12Type"));
		pdata.setCom12Min(pRS.getDouble("Com12Min"));
		pdata.setCom12Max(pRS.getDouble("Com12Max"));
		pdata.setCom1DecimalType(pRS.getInt("Com1DecimalType"));
		pdata.setCom2DecimalType(pRS.getInt("Com2DecimalType"));
		pdata.setCom3DecimalType(pRS.getInt("Com3DecimalType"));
		pdata.setCom4DecimalType(pRS.getInt("Com4DecimalType"));
		pdata.setCom5DecimalType(pRS.getInt("Com5DecimalType"));
		pdata.setCom6DecimalType(pRS.getInt("Com6DecimalType"));
		pdata.setCom7DecimalType(pRS.getInt("Com7DecimalType"));
		pdata.setCom8DecimalType(pRS.getInt("Com8DecimalType"));
		pdata.setCom9DecimalType(pRS.getInt("Com9DecimalType"));
		pdata.setCom10DecimalType(pRS.getInt("Com10DecimalType"));
		pdata.setCom11DecimalType(pRS.getInt("Com11DecimalType"));
		pdata.setCom12DecimalType(pRS.getInt("Com12DecimalType"));
	}

	private void readRecord(RateSetupData pdata, ResultSet pRS, Connection pConn) throws SQLException {
		ArrayList<RateAmountRangeSetupData> l_AmtData = new ArrayList<RateAmountRangeSetupData>();
		RateAmountRangeSetupDAO l_AmtDAO = new RateAmountRangeSetupDAO();

		pdata.setSerialNo(pRS.getInt("SerialNo"));
		pdata.setType(pRS.getInt("Type"));
		pdata.setFromBank(pRS.getString("FromBank"));
		pdata.setFromBranch(pRS.getString("FromBranch"));
		pdata.setToBank(pRS.getString("ToBank"));
		pdata.setToBranch(pRS.getString("ToBranch"));
		pdata.setInOut(pRS.getInt("InOut"));
		pdata.setCom1(pRS.getDouble("Com1"));
		pdata.setCom1Type(pRS.getString("Com1Type"));
		pdata.setCom1Min(pRS.getDouble("Com1Min"));
		pdata.setCom1Max(pRS.getDouble("Com1Max"));
		pdata.setCom2(pRS.getDouble("Com2"));
		pdata.setCom2Type(pRS.getString("Com2Type"));
		pdata.setCom2Min(pRS.getDouble("Com2Min"));
		pdata.setCom2Max(pRS.getDouble("Com2Max"));
		pdata.setCom3(pRS.getDouble("Com3"));
		pdata.setCom3Type(pRS.getString("Com3Type"));
		pdata.setCom3Min(pRS.getDouble("Com3Min"));
		pdata.setCom3Max(pRS.getDouble("Com3Max"));
		pdata.setCom4(pRS.getDouble("Com4"));
		pdata.setCom4Type(pRS.getString("Com4Type"));
		pdata.setCom4Min(pRS.getDouble("Com4Min"));
		pdata.setCom4Max(pRS.getDouble("Com4Max"));
		pdata.setCom5(pRS.getDouble("Com5"));
		pdata.setCom5Type(pRS.getString("Com5Type"));
		pdata.setCom5Min(pRS.getDouble("Com5Min"));
		pdata.setCom5Max(pRS.getDouble("Com5Max"));
		pdata.setCom6(pRS.getDouble("Com6"));
		pdata.setCom6Type(pRS.getString("Com6Type"));
		pdata.setCom6Min(pRS.getDouble("Com6Min"));
		pdata.setCom6Max(pRS.getDouble("Com6Max"));
		pdata.setCom7(pRS.getDouble("Com7"));
		pdata.setCom7Type(pRS.getString("Com7Type"));
		pdata.setCom7Min(pRS.getDouble("Com7Min"));
		pdata.setCom7Max(pRS.getDouble("Com7Max"));
		pdata.setCom8(pRS.getDouble("Com8"));
		pdata.setCom8Type(pRS.getString("Com8Type"));
		pdata.setCom8Min(pRS.getDouble("Com8Min"));
		pdata.setCom8Max(pRS.getDouble("Com8Max"));
		pdata.setCom9(pRS.getDouble("Com9"));
		pdata.setCom9Type(pRS.getString("Com9Type"));
		pdata.setCom9Min(pRS.getDouble("Com9Min"));
		pdata.setCom9Max(pRS.getDouble("Com9Max"));
		pdata.setCom10(pRS.getDouble("Com10"));
		pdata.setCom10Type(pRS.getString("Com10Type"));
		pdata.setCom10Min(pRS.getDouble("Com10Min"));
		pdata.setCom10Max(pRS.getDouble("Com10Max"));
		pdata.setCom11(pRS.getDouble("Com11"));
		pdata.setCom11Type(pRS.getString("Com11Type"));
		pdata.setCom11Min(pRS.getDouble("Com11Min"));
		pdata.setCom11Max(pRS.getDouble("Com11Max"));
		pdata.setCom12(pRS.getDouble("Com12"));
		pdata.setCom12Type(pRS.getString("Com12Type"));
		pdata.setCom12Min(pRS.getDouble("Com12Min"));
		pdata.setCom12Max(pRS.getDouble("Com12Max"));
		pdata.setCom1DecimalType(pRS.getInt("Com1DecimalType"));
		pdata.setCom2DecimalType(pRS.getInt("Com2DecimalType"));
		pdata.setCom3DecimalType(pRS.getInt("Com3DecimalType"));
		pdata.setCom4DecimalType(pRS.getInt("Com4DecimalType"));
		pdata.setCom5DecimalType(pRS.getInt("Com5DecimalType"));
		pdata.setCom6DecimalType(pRS.getInt("Com6DecimalType"));
		pdata.setCom7DecimalType(pRS.getInt("Com7DecimalType"));
		pdata.setCom8DecimalType(pRS.getInt("Com8DecimalType"));
		pdata.setCom9DecimalType(pRS.getInt("Com9DecimalType"));
		pdata.setCom10DecimalType(pRS.getInt("Com10DecimalType"));
		pdata.setCom11DecimalType(pRS.getInt("Com11DecimalType"));
		pdata.setCom12DecimalType(pRS.getInt("Com12DecimalType"));

		if (pdata.getCom1Type().equals("F") || pdata.getCom1Type().equals("R") || pdata.getCom1Type().equals("P")
				|| pdata.getCom1Type().equals("")) {
			// System.out.println("pdata.getCom1Type() "+pdata.getCom1Type());
		} else {
			System.out.println("pdata.getCom1Type() " + pdata.getCom1Type());
			if (pdata.getCom1Type().length() >= 2) {

				l_AmtData = new ArrayList<RateAmountRangeSetupData>();
				l_AmtDAO.getRateAmountRangeDataListByCommRangeType(
						Integer.parseInt(pdata.getCom1Type().substring(1, pdata.getCom1Type().length())), pConn);
				l_AmtData = l_AmtDAO.getRateAmountRangeDataList();
				pdata.setCom1AmountRangeDatalist(l_AmtData);
			}
		}

		if (pdata.getCom2Type().equals("F") || pdata.getCom2Type().equals("R") || pdata.getCom2Type().equals("P")
				|| pdata.getCom3Type().equals("")) {

		} else {
			System.out.println("pdata.getCom2Type() " + pdata.getCom2Type());
			if (pdata.getCom2Type().length() >= 2) {
				l_AmtData = new ArrayList<RateAmountRangeSetupData>();
				l_AmtDAO.getRateAmountRangeDataListByCommRangeType(
						Integer.parseInt(pdata.getCom2Type().substring(1, pdata.getCom2Type().length())), pConn);
				l_AmtData = l_AmtDAO.getRateAmountRangeDataList();
				pdata.setCom2AmountRangeDatalist(l_AmtData);
			}
		}

		if (pdata.getCom3Type().equals("F") || pdata.getCom3Type().equals("R") || pdata.getCom3Type().equals("P")
				|| pdata.getCom3Type().equals("")) {

		} else {
			System.out.println("pdata.getCom3Type() " + pdata.getCom3Type());
			if (pdata.getCom3Type().length() >= 2) {
				l_AmtData = new ArrayList<RateAmountRangeSetupData>();
				l_AmtDAO.getRateAmountRangeDataListByCommRangeType(
						Integer.parseInt(pdata.getCom3Type().substring(1, pdata.getCom3Type().length())), pConn);
				l_AmtData = l_AmtDAO.getRateAmountRangeDataList();
				pdata.setCom3AmountRangeDatalist(l_AmtData);
			}
		}
	}

	public boolean saveRateSetupData(RateSetupData pdata, Connection pconn) throws SQLException {
		boolean l_result = false;
		String l_query = "INSERT INTO " + mTable
				+ "([Type],FromBank,FromBranch,ToBank,ToBranch,InOut,Com1,Com1Type,Com1Min,Com1Max,Com1DecimalType,"
				+ "Com2,Com2Type,Com2Min,Com2Max,Com2DecimalType,Com3,Com3Type,Com3Min,Com3Max,Com3DecimalType,Com4,Com4Type,Com4Min,Com4Max,Com4DecimalType,Com5,Com5Type,Com5Min,Com5Max,Com5DecimalType,"
				+ "Com6,Com6Type,Com6Min,Com6Max,Com6DecimalType,Com7,Com7Type,Com7Min,Com7Max,Com7DecimalType,Com8,Com8Type,Com8Min,Com8Max,Com8DecimalType,Com9,Com9Type,Com9Min,Com9Max,Com9DecimalType,"// 51
				+ "Com10,Com10Type,Com10Min,Com10Max,Com10DecimalType,Com11,Com11Type,Com11Min,Com11Max,Com11DecimalType,Com12,Com12Type,Com12Min,Com12Max,Com12DecimalType) VALUES(?,?,?,?,?,?,?,?,?,?,?,"// 11
				+ "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement l_pstmt = pconn.prepareStatement(l_query);
		updateRecordforSave(pdata, l_pstmt);
		l_pstmt.setQueryTimeout(DAOManager.getNormalTime());
		if (l_pstmt.executeUpdate() > 0) {
			l_result = true;
		}
		l_pstmt.close();
		return l_result;
	}

	public void setrateSetup(RateSetupData m_rateSetup) {
		this.m_rateSetup = m_rateSetup;
	}

	public void setratesetupList(ArrayList<RateSetupData> m_ratesetupList) {
		this.m_ratesetupList = m_ratesetupList;
	}

	public RateSetupData setValueforFromBankNameNBranchName(RateSetupData pdata, String pBankName, String pBranchName) {
		pdata.setFromBank(pBankName);
		pdata.setFromBranch(pBranchName);
		return pdata;
	}

	public RateSetupData setValueforToBankNameNBranchName(RateSetupData pdata, String pBankName, String pBranchName) {
		pdata.setToBank(pBankName);
		pdata.setToBranch(pBranchName);
		return pdata;
	}

	public boolean updateRateSetupData(RateSetupData pdata, Connection pconn) throws SQLException {
		boolean l_result = false;
		String l_query = "UPDATE " + mTable
				+ " SET Com1=?, Com1Type=?, Com1Min=?, Com1Max=?, Com1DecimalType=?,Com2=?, Com2Type=?, Com2Min=?, Com2Max=?, Com2DecimalType=?,"
				+ "Com3=?, Com3Type=?, Com3Min=?, Com3Max=?, Com3DecimalType=?,Com4=?, Com4Type=?, Com4Min=?, Com4Max=?, Com4DecimalType=?,"
				+ "Com5=?, Com5Type=?, Com5Min=?, Com5Max=?, Com5DecimalType=?,Com6=?, Com6Type=?, Com6Min=?, Com6Max=?, Com6DecimalType=?,"
				+ "Com7=?, Com7Type=?, Com7Min=?, Com7Max=?, Com7DecimalType=?,Com8=?, Com8Type=?, Com8Min=?, Com8Max=?, Com8DecimalType=?,"
				+ "Com9=?, Com9Type=?, Com9Min=?, Com9Max=?, Com9DecimalType=?,Com10=?, Com10Type=?, Com10Min=?, Com10Max=?, Com10DecimalType=?,"
				+ "Com11=?, Com11Type=?, Com11Min=?, Com11Max=?, Com11DecimalType=?,Com12=?, Com12Type=?, Com12Min=?, Com12Max=?, Com12DecimalType=? WHERE SerialNo=?";
		PreparedStatement l_pstmt = pconn.prepareStatement(l_query);
		updateRecords(pdata, l_pstmt);
		l_pstmt.setQueryTimeout(DAOManager.getNormalTime());
		if (l_pstmt.executeUpdate() > 0) {
			l_result = true;
		}
		l_pstmt.close();
		return l_result;

	}

	public void updaterecord(RateSetupData pdata, PreparedStatement pPS) throws SQLException {
		int l_index = 1;
		pPS.setInt(l_index++, pdata.getType());
		pPS.setString(l_index++, pdata.getFromBank());
		pPS.setString(l_index++, pdata.getFromBranch());
		pPS.setString(l_index++, pdata.getToBank());
		pPS.setString(l_index++, pdata.getToBranch());
		pPS.setInt(l_index++, pdata.getInOut());
		pPS.setDouble(l_index++, pdata.getCom1());
		pPS.setString(l_index++, pdata.getCom1Type());
		pPS.setDouble(l_index++, pdata.getCom1Min());
		pPS.setDouble(l_index++, pdata.getCom1Max());
		pPS.setDouble(l_index++, pdata.getCom2());
		pPS.setString(l_index++, pdata.getCom2Type());
		pPS.setDouble(l_index++, pdata.getCom2Min());
		pPS.setDouble(l_index++, pdata.getCom2Max());
		pPS.setDouble(l_index++, pdata.getCom3());
		pPS.setString(l_index++, pdata.getCom3Type());
		pPS.setDouble(l_index++, pdata.getCom3Min());
		pPS.setDouble(l_index++, pdata.getCom3Max());
		pPS.setDouble(l_index++, pdata.getCom4());
		pPS.setString(l_index++, pdata.getCom4Type());
		pPS.setDouble(l_index++, pdata.getCom4Min());
		pPS.setDouble(l_index++, pdata.getCom4Max());
		pPS.setDouble(l_index++, pdata.getCom5());
		pPS.setString(l_index++, pdata.getCom5Type());
		pPS.setDouble(l_index++, pdata.getCom5Min());
		pPS.setDouble(l_index++, pdata.getCom5Max());
		pPS.setDouble(l_index++, pdata.getCom6());
		pPS.setString(l_index++, pdata.getCom6Type());
		pPS.setDouble(l_index++, pdata.getCom6Min());
		pPS.setDouble(l_index++, pdata.getCom6Max());
		pPS.setDouble(l_index++, pdata.getCom7());
		pPS.setString(l_index++, pdata.getCom7Type());
		pPS.setDouble(l_index++, pdata.getCom7Min());
		pPS.setDouble(l_index++, pdata.getCom7Max());
		pPS.setDouble(l_index++, pdata.getCom8());
		pPS.setString(l_index++, pdata.getCom8Type());
		pPS.setDouble(l_index++, pdata.getCom8Min());
		pPS.setDouble(l_index++, pdata.getCom8Max());
		pPS.setDouble(l_index++, pdata.getCom9());
		pPS.setString(l_index++, pdata.getCom9Type());
		pPS.setDouble(l_index++, pdata.getCom9Min());
		pPS.setDouble(l_index++, pdata.getCom9Max());
		pPS.setDouble(l_index++, pdata.getCom10());
		pPS.setString(l_index++, pdata.getCom10Type());
		pPS.setDouble(l_index++, pdata.getCom10Min());
		pPS.setDouble(l_index++, pdata.getCom10Max());
		pPS.setDouble(l_index++, pdata.getCom11());
		pPS.setString(l_index++, pdata.getCom11Type());
		pPS.setDouble(l_index++, pdata.getCom11Min());
		pPS.setDouble(l_index++, pdata.getCom11Max());
		pPS.setDouble(l_index++, pdata.getCom12());
		pPS.setString(l_index++, pdata.getCom12Type());
		pPS.setDouble(l_index++, pdata.getCom12Min());
		pPS.setDouble(l_index++, pdata.getCom12Max());
		pPS.setInt(l_index++, pdata.getN1());
		pPS.setInt(l_index++, pdata.getN2());
		pPS.setInt(l_index++, pdata.getN3());
		pPS.setString(l_index++, pdata.getT1());
		pPS.setString(l_index++, pdata.getT2());
		pPS.setString(l_index++, pdata.getT3());
		pPS.setDouble(l_index++, pdata.getCom1DecimalType());
		pPS.setDouble(l_index++, pdata.getCom2DecimalType());
		pPS.setDouble(l_index++, pdata.getCom3DecimalType());
		pPS.setDouble(l_index++, pdata.getCom4DecimalType());
		pPS.setDouble(l_index++, pdata.getCom5DecimalType());
		pPS.setDouble(l_index++, pdata.getCom6DecimalType());
		pPS.setDouble(l_index++, pdata.getCom7DecimalType());
		pPS.setDouble(l_index++, pdata.getCom8DecimalType());
		pPS.setDouble(l_index++, pdata.getCom9DecimalType());
		pPS.setDouble(l_index++, pdata.getCom10DecimalType());
		pPS.setDouble(l_index++, pdata.getCom11DecimalType());
		pPS.setDouble(l_index++, pdata.getCom12DecimalType());

	}

	private void updateRecordforSave(RateSetupData pdata, PreparedStatement pPS) throws SQLException {
		int l_index = 1;
		pPS.setInt(l_index++, pdata.getType());
		pPS.setString(l_index++, pdata.getFromBank());
		pPS.setString(l_index++, pdata.getFromBranch());
		pPS.setString(l_index++, pdata.getToBank());
		pPS.setString(l_index++, pdata.getToBranch());
		pPS.setInt(l_index++, pdata.getInOut());
		pPS.setDouble(l_index++, pdata.getCom1());
		pPS.setString(l_index++, pdata.getCom1Type());
		pPS.setDouble(l_index++, pdata.getCom1Min());
		pPS.setDouble(l_index++, pdata.getCom1Max());
		pPS.setInt(l_index++, pdata.getCom1DecimalType());
		pPS.setDouble(l_index++, pdata.getCom2());
		pPS.setString(l_index++, pdata.getCom2Type());
		pPS.setDouble(l_index++, pdata.getCom2Min());
		pPS.setDouble(l_index++, pdata.getCom2Max());
		pPS.setInt(l_index++, pdata.getCom2DecimalType());
		pPS.setDouble(l_index++, pdata.getCom3());
		pPS.setString(l_index++, pdata.getCom3Type());
		pPS.setDouble(l_index++, pdata.getCom3Min());
		pPS.setDouble(l_index++, pdata.getCom3Max());
		pPS.setInt(l_index++, pdata.getCom3DecimalType());
		pPS.setDouble(l_index++, pdata.getCom4());
		pPS.setString(l_index++, pdata.getCom4Type());
		pPS.setDouble(l_index++, pdata.getCom4Min());
		pPS.setDouble(l_index++, pdata.getCom4Max());
		pPS.setInt(l_index++, pdata.getCom4DecimalType());
		pPS.setDouble(l_index++, pdata.getCom5());
		pPS.setString(l_index++, pdata.getCom5Type());
		pPS.setDouble(l_index++, pdata.getCom5Min());
		pPS.setDouble(l_index++, pdata.getCom5Max());
		pPS.setInt(l_index++, pdata.getCom5DecimalType());
		pPS.setDouble(l_index++, pdata.getCom6());
		pPS.setString(l_index++, pdata.getCom6Type());
		pPS.setDouble(l_index++, pdata.getCom6Min());
		pPS.setDouble(l_index++, pdata.getCom6Max());
		pPS.setInt(l_index++, pdata.getCom6DecimalType());
		pPS.setDouble(l_index++, pdata.getCom7());
		pPS.setString(l_index++, pdata.getCom7Type());
		pPS.setDouble(l_index++, pdata.getCom7Min());
		pPS.setDouble(l_index++, pdata.getCom7Max());
		pPS.setInt(l_index++, pdata.getCom7DecimalType());
		pPS.setDouble(l_index++, pdata.getCom8());
		pPS.setString(l_index++, pdata.getCom8Type());
		pPS.setDouble(l_index++, pdata.getCom8Min());
		pPS.setDouble(l_index++, pdata.getCom8Max());
		pPS.setInt(l_index++, pdata.getCom8DecimalType());
		pPS.setDouble(l_index++, pdata.getCom9());
		pPS.setString(l_index++, pdata.getCom9Type());
		pPS.setDouble(l_index++, pdata.getCom9Min());
		pPS.setDouble(l_index++, pdata.getCom9Max());
		pPS.setInt(l_index++, pdata.getCom9DecimalType());
		pPS.setDouble(l_index++, pdata.getCom10());
		pPS.setString(l_index++, pdata.getCom10Type());
		pPS.setDouble(l_index++, pdata.getCom10Min());
		pPS.setDouble(l_index++, pdata.getCom10Max());
		pPS.setInt(l_index++, pdata.getCom10DecimalType());
		pPS.setDouble(l_index++, pdata.getCom11());
		pPS.setString(l_index++, pdata.getCom11Type());
		pPS.setDouble(l_index++, pdata.getCom11Min());
		pPS.setDouble(l_index++, pdata.getCom11Max());
		pPS.setInt(l_index++, pdata.getCom11DecimalType());
		pPS.setDouble(l_index++, pdata.getCom12());
		pPS.setString(l_index++, pdata.getCom12Type());
		pPS.setDouble(l_index++, pdata.getCom12Min());
		pPS.setDouble(l_index++, pdata.getCom12Max());
		pPS.setInt(l_index++, pdata.getCom12DecimalType());
	}

	private void updateRecords(RateSetupData pdata, PreparedStatement pPS) throws SQLException {
		int l_index = 1;
		pPS.setDouble(l_index++, pdata.getCom1());
		pPS.setString(l_index++, pdata.getCom1Type());
		pPS.setDouble(l_index++, pdata.getCom1Min());
		pPS.setDouble(l_index++, pdata.getCom1Max());
		pPS.setInt(l_index++, pdata.getCom1DecimalType());
		pPS.setDouble(l_index++, pdata.getCom2());
		pPS.setString(l_index++, pdata.getCom2Type());
		pPS.setDouble(l_index++, pdata.getCom2Min());
		pPS.setDouble(l_index++, pdata.getCom2Max());
		pPS.setInt(l_index++, pdata.getCom2DecimalType());
		pPS.setDouble(l_index++, pdata.getCom3());
		pPS.setString(l_index++, pdata.getCom3Type());
		pPS.setDouble(l_index++, pdata.getCom3Min());
		pPS.setDouble(l_index++, pdata.getCom3Max());
		pPS.setInt(l_index++, pdata.getCom3DecimalType());
		pPS.setDouble(l_index++, pdata.getCom4());
		pPS.setString(l_index++, pdata.getCom4Type());
		pPS.setDouble(l_index++, pdata.getCom4Min());
		pPS.setDouble(l_index++, pdata.getCom4Max());
		pPS.setInt(l_index++, pdata.getCom4DecimalType());
		pPS.setDouble(l_index++, pdata.getCom5());
		pPS.setString(l_index++, pdata.getCom5Type());
		pPS.setDouble(l_index++, pdata.getCom5Min());
		pPS.setDouble(l_index++, pdata.getCom5Max());
		pPS.setInt(l_index++, pdata.getCom5DecimalType());
		pPS.setDouble(l_index++, pdata.getCom6());
		pPS.setString(l_index++, pdata.getCom6Type());
		pPS.setDouble(l_index++, pdata.getCom6Min());
		pPS.setDouble(l_index++, pdata.getCom6Max());
		pPS.setInt(l_index++, pdata.getCom6DecimalType());
		pPS.setDouble(l_index++, pdata.getCom7());
		pPS.setString(l_index++, pdata.getCom7Type());
		pPS.setDouble(l_index++, pdata.getCom7Min());
		pPS.setDouble(l_index++, pdata.getCom7Max());
		pPS.setInt(l_index++, pdata.getCom7DecimalType());
		pPS.setDouble(l_index++, pdata.getCom8());
		pPS.setString(l_index++, pdata.getCom8Type());
		pPS.setDouble(l_index++, pdata.getCom8Min());
		pPS.setDouble(l_index++, pdata.getCom8Max());
		pPS.setInt(l_index++, pdata.getCom8DecimalType());
		pPS.setDouble(l_index++, pdata.getCom9());
		pPS.setString(l_index++, pdata.getCom9Type());
		pPS.setDouble(l_index++, pdata.getCom9Min());
		pPS.setDouble(l_index++, pdata.getCom9Max());
		pPS.setInt(l_index++, pdata.getCom9DecimalType());
		pPS.setDouble(l_index++, pdata.getCom10());
		pPS.setString(l_index++, pdata.getCom10Type());
		pPS.setDouble(l_index++, pdata.getCom10Min());
		pPS.setDouble(l_index++, pdata.getCom10Max());
		pPS.setInt(l_index++, pdata.getCom10DecimalType());
		pPS.setDouble(l_index++, pdata.getCom11());
		pPS.setString(l_index++, pdata.getCom11Type());
		pPS.setDouble(l_index++, pdata.getCom11Min());
		pPS.setDouble(l_index++, pdata.getCom11Max());
		pPS.setInt(l_index++, pdata.getCom11DecimalType());
		pPS.setDouble(l_index++, pdata.getCom12());
		pPS.setString(l_index++, pdata.getCom12Type());
		pPS.setDouble(l_index++, pdata.getCom12Min());
		pPS.setDouble(l_index++, pdata.getCom12Max());
		pPS.setInt(l_index++, pdata.getCom12DecimalType());
		pPS.setInt(l_index++, pdata.getSerialNo());
	}

}
