package com.nirvasoft.rp.dao.icbs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.nirvasoft.rp.util.SharedUtil;

public class LastDatesDAO {

	// APH 11/09/2014
	public String addDayToLastProcessedDate(String pDate, int pNoOfDate, Connection pConn) throws SQLException {
		String ret = "";

		String l_Query = "SELECT DateAdd(d," + pNoOfDate + ",'" + pDate + "')";
		PreparedStatement pstmt = pConn.prepareStatement(l_Query);
		ResultSet rs = pstmt.executeQuery();

		while (rs.next()) {
			ret = SharedUtil.formatDBDate2MIT(rs.getString(1));
		}
		return ret;
	}

	public boolean addLastDatesEOD(String pProcessedDate, String pTodayDate, Connection conn) throws SQLException {
		boolean result = false;
		String l_Query = "INSERT INTO LastDates (LastProcessedDate, StartedAt) " + "VALUES (?, ?)";
		PreparedStatement pstmt = conn.prepareStatement(l_Query);
		pstmt.setString(1, pProcessedDate);
		pstmt.setString(2, pTodayDate);

		pstmt.execute();
		result = true;
		pstmt.close();
		return result;
	}

	public String getLastProcessedDate(Connection pConn) throws SQLException {
		String ret = "";
		String l_Query = "SELECT MAX(LASTPROCESSEDDATE) AS LASTPROCESSEDDATE FROM LASTDATES";

		PreparedStatement pstmt = pConn.prepareStatement(l_Query);
		ResultSet rs = pstmt.executeQuery();

		while (rs.next())
			ret = SharedUtil.formatDBDate2MIT(rs.getString(1));

		return ret;
	}

	public boolean isRunnedEOD(String pProcessedDate, Connection conn) throws SQLException {
		boolean result = false;
		String l_Query = "SELECT * FROM LastDates WHERE CONVERT(DateTime,LastProcessedDate) = ?";
		PreparedStatement pstmt = conn.prepareStatement(l_Query);
		pstmt.setString(1, pProcessedDate);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			result = true;
		}
		return result;
	}

}
