package com.nirvasoft.rp.dao.icbs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.shared.icbs.AccountGLTransactionData;

public class AccountGLTransactionDao {
	public static String mTableName = "AccountGLTransaction";

	public boolean addAccountGLTransaction(AccountGLTransactionData pAccountGLTransactionData, Connection conn)
			throws SQLException {
		boolean result = false;
		StringBuffer l_Query = new StringBuffer();
		l_Query.append("INSERT INTO ACCOUNTGLTRANSACTION (TransNo, AccNumber, GLCode1, GLCode2, GLCode3, GLCode4, "
				+ "GLCode5, BaseCurCode, BaseCurOperator, BaseRate, BaseAmount, MediumCurCode, MediumCurOperator, "
				+ "MediumRate, MediumAmount, TrCurCode,TrCurOperator, TrRate, TrAmount, TrPrevBalance, N1, N2, N3, T1, T2, T3) VALUES "
				+ "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

		PreparedStatement pstmt = conn.prepareStatement(l_Query.toString());
		pstmt.setQueryTimeout(DAOManager.getTransTime());
		updateRecord(pAccountGLTransactionData, pstmt);
		pstmt.setQueryTimeout(DAOManager.getTransTime());
		if (pstmt.executeUpdate() > 0) {
			result = true;
		}
		pstmt.close();

		return result;
	}

	public boolean checkTableExist(String pTableName, Connection conn) throws SQLException {
		boolean l_result = false;
		String l_query = "select case when exists((select * from information_schema.tables where table_name = '"
				+ pTableName + "')) then 1 else 0 end";
		PreparedStatement l_pstmt = conn.prepareStatement(l_query);
		l_pstmt.setQueryTimeout(DAOManager.getTransTime());
		ResultSet l_RS = l_pstmt.executeQuery();
		while (l_RS.next()) {
			if (l_RS.getInt(1) == 1) {
				l_result = true;
			}
		}
		return l_result;
	}

	public boolean save(ArrayList<AccountGLTransactionData> data, Connection conn) throws SQLException {
		boolean ret = false;
		String sql = "INSERT INTO " + mTableName + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement ps = conn.prepareStatement(sql);
		if (data.size() > 0) {
			for (int i = 0; i < data.size(); i++) {
				updateRecord(data.get(i), ps);
				ps.addBatch();
			}
		}
		int i[] = ps.executeBatch();
		if (i.length > 0) {
			ret = true;
		}
		ps.close();
		return ret;
	}

	public void updateRecord(AccountGLTransactionData pAccountGLTransactionData, PreparedStatement ps)
			throws SQLException {
		int index = 1;
		ps.setLong(index++, pAccountGLTransactionData.getTransNo());
		ps.setString(index++, pAccountGLTransactionData.getAccNumber());
		ps.setString(index++, pAccountGLTransactionData.getGLCode1());
		ps.setString(index++, pAccountGLTransactionData.getGLCode2());
		ps.setString(index++, pAccountGLTransactionData.getGLCode3());
		ps.setString(index++, pAccountGLTransactionData.getGLCode4());
		ps.setString(index++, pAccountGLTransactionData.getGLCode5());
		ps.setString(index++, pAccountGLTransactionData.getBaseCurCode());
		ps.setString(index++, pAccountGLTransactionData.getBaseCurOperator());
		ps.setDouble(index++, pAccountGLTransactionData.getBaseRate());
		ps.setDouble(index++, pAccountGLTransactionData.getBaseAmount());
		ps.setString(index++, pAccountGLTransactionData.getMediumCurCode());
		ps.setString(index++, pAccountGLTransactionData.getMediumCurOperator());
		ps.setDouble(index++, pAccountGLTransactionData.getMediumRate());
		ps.setDouble(index++, pAccountGLTransactionData.getMediumAmount());
		ps.setString(index++, pAccountGLTransactionData.getTrCurCode());
		ps.setString(index++, pAccountGLTransactionData.getTrCurOperator());
		ps.setDouble(index++, pAccountGLTransactionData.getTrRate());
		ps.setDouble(index++, pAccountGLTransactionData.getTrAmount());
		ps.setDouble(index++, pAccountGLTransactionData.getTrPrevBalance());
		ps.setInt(index++, pAccountGLTransactionData.getN1());
		ps.setInt(index++, pAccountGLTransactionData.getN2());
		ps.setInt(index++, pAccountGLTransactionData.getN3());
		ps.setString(index++, pAccountGLTransactionData.getT1());
		ps.setString(index++, pAccountGLTransactionData.getT2());
		ps.setString(index++, pAccountGLTransactionData.getT3());

	}

	public boolean updateStatusByTransRef(int transNo, Connection conn) throws SQLException {
		boolean ret = false;
		String sql = "update " + mTableName + " set n1 = 254 where TransNo = ? or TransNo = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setInt(i++, transNo);
		ps.setInt(i++, transNo + 1);
		if (ps.executeUpdate() > 0) {
			ret = true;
		}
		ps.close();
		return ret;
	}
}
