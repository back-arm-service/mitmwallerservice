package com.nirvasoft.rp.dao.icbs;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.nirvasoft.epixws.webservice.CustomerInfo;
import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.shared.DataResult;
import com.nirvasoft.rp.shared.icbs.AccountData;
import com.nirvasoft.rp.shared.icbs.AddressData;
import com.nirvasoft.rp.shared.icbs.ICBSCustomerData;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.SharedUtil;

public class AccountDao {

	public static String mTableName = "Accounts";
	public static String mTableNameCurrent = "CurrentAccount";
	public static String mTableRegister = "register";

	private AccountData AccBean;

	public AccountData getAccountByAccNo(String accNumber, Connection conn) throws SQLException {
		AccountData ret = new AccountData();
		String sql = "select * from " + mTableName + " where AccNumber = ? and status = 0";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, accNumber);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			ret = readRecord(rs);
		}
		ps.close();
		rs.close();
		return ret;
	}

	public AccountData getAccountByCard(String cardNo, Connection conn) throws SQLException {
		AccountData ret = new AccountData();
		String sql = "select a.* from t00012 t " + " inner join accounts a on t.t6 = a.AccNumber "
				+ " where t.t1 = ? and a.Status <> 3";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, cardNo);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			ret = readRecord(rs);
		}
		ps.close();
		rs.close();
		return ret;
	}

	public AccountData getAccountByID(String id, Connection conn) throws SQLException {
		AccountData ret = new AccountData();
		String sql = " select a.*,t.n1, t.t5 from Accounts a  inner join T00005 t on a.AccNumber = t.t1 "
				+ " where a.AccNumber = ? and a.status = 0";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, id);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			ret = readRecord(rs);
		}
		ps.close();
		rs.close();
		return ret;
	}

	public String getAccountNumber(String userid, Connection conn1) throws SQLException {
		String accountNumber = null;
		String l_Query = "SELECT AccNumber FROM WJunction WHERE userid=?";
		PreparedStatement pstmt1 = conn1.prepareStatement(l_Query);
		pstmt1.setString(1, userid);
		ResultSet rs1 = pstmt1.executeQuery();

		if (rs1.next()) {
			accountNumber = rs1.getString("AccNumber");
		}

		pstmt1.close();
		rs1.close();

		return accountNumber;
	}
	public String getFromAccountNumber(String userid, Connection conn1) throws SQLException {
		String accountNumber = null;
		String l_Query = "SELECT AccNumber FROM "+ mTableName+ " WHERE AccName=?";
		PreparedStatement pstmt1 = conn1.prepareStatement(l_Query);
		pstmt1.setString(1, userid);
		ResultSet rs1 = pstmt1.executeQuery();

		if (rs1.next()) {
			accountNumber = rs1.getString("AccNumber");
		}

		pstmt1.close();
		rs1.close();

		return accountNumber;
	}
	public String goToName(String userid, Connection conn1) throws SQLException {
		String toname = null;
		String l_Query = "SELECT username FROM "+mTableRegister+ " WHERE userid=?";
		PreparedStatement pstmt1 = conn1.prepareStatement(l_Query);
		pstmt1.setString(1, userid);
		ResultSet rs1 = pstmt1.executeQuery();

		if (rs1.next()) {
			toname = rs1.getString("username");
		}

		pstmt1.close();
		rs1.close();

		return toname;
	}
	public CustomerInfo getCustomerData(String account, Connection conn) throws SQLException {
		CustomerInfo cusret = new CustomerInfo();
		boolean isData = false;
		String l_Query = "select r.username As Name,r.t21 As NrcNo from wjunction w inner join register r on w.userid=r.userid WHERE w.AccNumber=?";
		PreparedStatement ps = conn.prepareStatement(l_Query);
		ps.setString(1, account);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			cusret.setName(rs.getString("Name"));
			if (rs.getString("NrcNo") == null || rs.getString("NrcNo") == "") {
				cusret.setNRC("");;
			} else {
				cusret.setNRC(rs.getString("NrcNo"));
			}

			isData = true;
		}
		if (isData == true) {
			cusret.setCode("0000");
			cusret.setDesc("Successfully");

		} else {
			cusret.setCode("0014");
			cusret.setDesc("Account No. is not found");

		}
		ps.close();
		rs.close();
		return cusret;
	}
	public CustomerInfo getGLData(String account, Connection conn) throws SQLException {
		CustomerInfo cusret = new CustomerInfo();
		boolean isData = false;
		String sql = "select AccDesc from GL where AccNo = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, account);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			cusret.setName(rs.getString("AccDesc"));
			isData = true;
		}
		if (isData == true) {
			cusret.setCode("0000");
			cusret.setDesc("Successfully");
		} else {
			cusret.setCode("0014");
			cusret.setDesc("GL is not found");
		}
		ps.close();
		rs.close();
		return cusret;
	}

	public AccountData getAccountsBean() {
		return AccBean;
	}

	public AddressData getAddressNew(String pAccountNo, Connection pConn) throws SQLException {
		AddressData objAddr = new AddressData();
		String l_Query = "SELECT Phone FROM Customer C INNER JOIN CAJunction CA ON C.CustomerId = CA.CustomerId WHERE CA.AccNumber =  ?";
		PreparedStatement pstmt = pConn.prepareStatement(l_Query);
		pstmt.setString(1, pAccountNo);
		pstmt.setQueryTimeout(DAOManager.getConnectionTime());
		ResultSet aRS = pstmt.executeQuery();
		while (aRS.next()) {
			if (!aRS.getString("Phone").trim().equals("") && aRS.getString("Phone").trim().split(",").length > 0) {
				String st = aRS.getString("Phone");
				objAddr.setTel1(st.split(",")[0]);
				objAddr.setTel2(st.split(",").length > 1 ? st.split(",")[1] : "");
				objAddr.setTel3(st.split(",").length > 2 ? st.split(",")[2] : "");
			}
		}

		return objAddr;
	}

	public String getBalance(String accountNumber, Connection conn) throws SQLException {
		String balance = null;
		String sql = "select CurrentBalance from " + mTableName + " where AccNumber = ? and status = 0";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, accountNumber);
		ResultSet rs = ps.executeQuery();

		if (rs.next()) {
			balance = String.valueOf(rs.getDouble("CurrentBalance"));
		}

		ps.close();
		rs.close();

		return balance;
	}

	public AccountData getBalanceByID(String id, Connection conn) throws SQLException {
		AccountData ret = new AccountData();
		String sql = "select * from " + mTableName + " where AccNumber = ? and status = 0";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, id);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			ret = readRecord(rs);
		}
		ps.close();
		rs.close();
		return ret;
	}

	public double getBalanceByIDV2(String id, Connection conn) throws SQLException {
		double ret = 0.00;
		String sql = "select CurrentBalance from " + mTableName + " where AccNumber = ? and status = 0";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, id);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			ret = rs.getDouble("CurrentBalance");
		}
		ps.close();
		rs.close();
		return ret;
	}

	public AccountData getCardAccountByID(String id, String cardType, Connection conn) throws SQLException {
		AccountData ret = new AccountData();
		String sql = "select c.* from customer A inner join CAJunction B on A.CustomerID =B.CustomerID "
				+ " inner join Accounts C on B.AccNumber = C.AccNumber where A.Phone =? and SUBSTRING(c.AccNumber,1, 4) = '0004' ";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, id);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			ret = readRecord(rs);
		}
		ps.close();
		rs.close();
		return ret;
	}

	public double getCurrentBalance(String aAccNumber, Connection conn)
			throws ParserConfigurationException, SAXException, IOException, ClassNotFoundException, SQLException {
		double result = 0;

		PreparedStatement pstmt = conn.prepareStatement("SELECT CurrentBalance FROM Accounts WHERE AccNumber=?");

		pstmt.setString(1, aAccNumber);
		ResultSet rs = pstmt.executeQuery();

		while (rs.next()) {
			result = rs.getDouble(1);
		}
		pstmt.close();

		return result;
	}

	public ICBSCustomerData getCustomer(String pAccountNo, Connection pConn) throws SQLException {
		ICBSCustomerData ret = new ICBSCustomerData();
		String l_Query = "SELECT Name,NrcNo,OldNrcNo,BCNo FROM Customer C INNER JOIN CAJunction CA ON C.CustomerId = CA.CustomerId WHERE CA.AccNumber = ? AND CA.AccType = CA.RType";
		PreparedStatement pstmt = pConn.prepareStatement(l_Query);
		pstmt.setString(1, pAccountNo);
		pstmt.setQueryTimeout(DAOManager.getConnectionTime());
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			ret.setName(rs.getString("Name"));
			ret.setIC(rs.getString("NrcNo"));
			ret.setOldIC(rs.getString("OldNrcNo"));
			ret.setBcNo(rs.getString("BCNo"));
		}
		return ret;
	}	

	public AccountData getODAvailableBalance(String pDate, String pAccNumber, Connection pConn) throws SQLException {
		AccountData l_AccData = new AccountData();

		String l_Query = "select sum(a.SanctionAmountBr) as ODLimit, "
				+ "(case when b.todexpdate >= ? then b.todlimit else 0 end  ) as todlimit,  c.currentbalance from aodf a "
				+ "inner join currentaccount b on a.accnumber = b.accnumber "
				+ "inner join accounts c on  a.accnumber = c.accnumber " + "where a.status in ('1') "
				+ "and a.expdate >= ? " + "and a.sanctiondatebr <= ? " + "and a.batchno <> 0 " + "and a.odtype <> 0 "
				+ "and a.odtype <> 100 " + "and a.accnumber = ? " + "group by todexpdate, todlimit, currentbalance ";

		PreparedStatement pstmt = pConn.prepareStatement(l_Query);
		pstmt.setString(1, pDate);
		pstmt.setString(2, pDate);
		pstmt.setString(3, pDate);
		pstmt.setString(4, pAccNumber);

		pstmt.setQueryTimeout(DAOManager.getNormalTime());
		ResultSet l_RS = pstmt.executeQuery();

		while (l_RS.next()) {
			l_AccData.setCurrentBalance(l_RS.getDouble("CurrentBalance"));
			l_AccData.setOdLimit(l_RS.getDouble("ODLimit"));
			l_AccData.setTODLimit(l_RS.getDouble("TODLimit"));

			/*
			 * l_AccData.setAccountNumber(l_RS.getString("AccNumber"));
			 * l_AccData.setOdExpDate(SharedUtil.formatDBDate2MIT(l_RS.getString
			 * ("ODExpDate")));
			 * l_AccData.setTODExpDate(SharedUtil.formatDBDate2MIT(l_RS.
			 * getString("TODExpDate")));
			 */
		}

		l_Query = "select currentbalance from accounts where accnumber = ? ";
		pstmt = pConn.prepareStatement(l_Query);
		pstmt.setString(1, pAccNumber);
		pstmt.setQueryTimeout(DAOManager.getNormalTime());

		l_RS = pstmt.executeQuery();
		while (l_RS.next()) {
			l_AccData.setCurrentBalance(l_RS.getDouble("CurrentBalance"));
		}
		pstmt.close();

		return l_AccData;
	}

	
	public AccountData getODAvailableBalance(String pDate, String pAccNumber, int pProcessType, Connection pConn) {
		AccountData l_AccData = new AccountData();
		try {
			String l_Query = "select sum(a.sanctionamountbr) as ODLimit, "
					+ "(case when b.todexpdate >= ? then b.todlimit else 0 end  ) as todlimit,  c.currentbalance from aodf a "
					+ "inner join currentaccount b on a.accnumber = b.accnumber "
					+ "inner join accounts c on  a.accnumber = c.accnumber " + "where a.status in ('1') "
					+ "and a.sanctiondatebr <= ? " + "and a.batchno <> 0 " + "and a.odtype <> 0 "
					+ "and a.odtype <> 100 " + "and a.accnumber = ? "
					+ "group by todexpdate, todlimit, currentbalance ";

			PreparedStatement pstmt = pConn.prepareStatement(l_Query);
			pstmt.setString(1, pDate);
			pstmt.setString(2, pDate);
			pstmt.setString(3, pAccNumber);

			ResultSet l_RS = pstmt.executeQuery();

			while (l_RS.next()) {
				l_AccData.setCurrentBalance(l_RS.getDouble("CurrentBalance"));
				l_AccData.setOdLimit(l_RS.getDouble("ODLimit"));
				l_AccData.setTODLimit(l_RS.getDouble("TODLimit"));
			}

			if (pProcessType == 4 || pProcessType == 8) {
				l_Query = "select sum(a.sanctionamountbr) as ODLimit, "
						+ "(case when b.todexpdate >= ? then b.todlimit else 0 end  ) as todlimit,  c.currentbalance from aodf a "
						+ "inner join currentaccount b on a.accnumber = b.accnumber "
						+ "inner join accounts c on  a.accnumber = c.accnumber " + "where a.status in ('1') "
						+ "and (a.batchno = 0 or a.odtype in (0,100)) " + "and a.accnumber = ? "
						+ "group by todexpdate, todlimit, currentbalance ";

				pstmt = pConn.prepareStatement(l_Query);
				pstmt.setString(1, pDate);
				pstmt.setString(2, pAccNumber);

				l_RS = pstmt.executeQuery();

				while (l_RS.next()) {
					System.out.println(l_AccData.getOdLimit() + l_RS.getDouble("ODLimit"));
					l_AccData.setOdLimit(l_AccData.getOdLimit() + l_RS.getDouble("ODLimit"));
					l_AccData.setTODLimit(l_AccData.getTODLimit() + l_RS.getDouble("TODLimit"));
				}
			}

			l_Query = "select currentbalance from accounts where accnumber = ? ";
			pstmt = pConn.prepareStatement(l_Query);
			pstmt.setString(1, pAccNumber);
			l_RS = pstmt.executeQuery();
			while (l_RS.next()) {
				l_AccData.setCurrentBalance(l_RS.getDouble("CurrentBalance"));
			}
			if (pProcessType != 4) {
				l_Query = "Select ExpDate from AODF Where AccNumber =?  And BatchNo <> 0 And Status = 1 and ExpDate < ? ";
				pstmt = pConn.prepareStatement(l_Query);
				pstmt.setString(1, pAccNumber);
				pstmt.setString(2, pDate);
				l_RS = pstmt.executeQuery();
				while (l_RS.next()) {
					l_AccData.setExpire(true);
				}
			}
			pstmt.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return l_AccData;
	}

	private AccountData readRecord(ResultSet rs) throws SQLException {
		AccountData data = new AccountData();
		int i = 1;
		data.setAccountNumber(rs.getString(i++));
		data.setCurrencyCode(rs.getString(i++));
		data.setOpeningBalance(rs.getDouble(i++));
		data.setOpeningDate(rs.getString(i++));
		data.setCurrentBalance(rs.getDouble(i++));
		data.setClosingDate(rs.getString(i++));
		data.setLastUpdate(rs.getString(i++));
		data.setStatus(rs.getInt(i++));
		data.setLastTransDate(rs.getString(i++));
		data.setDrawingType(rs.getString(i++));
		data.setAccountName(rs.getString(i++));
		data.setRemark(rs.getString(i++));
		/*
		 * data.setZone(rs.getInt("n1"));
		 * data.setBranchCode(rs.getString("t5"));
		 */
		return data;
	}

	public boolean save(AccountData data, Connection conn) throws SQLException {
		boolean ret = false;
		String sql = "INSERT INTO " + mTableName + " VALUES(?,?,?,?,?"
				+ ",?,?,?,?,?"
				+ ",?,?,?,?,?)";
		PreparedStatement ps = conn.prepareStatement(sql);
		updateRecord(data, ps, true);
		if (ps.executeUpdate() > 0) {
			ret = true;
		}
		ps.close();
		return ret;
	}
	

	public boolean saveCurrentAcc(String accNumber, Connection conn) throws SQLException {
		boolean ret = false;
		String sql = "INSERT INTO " + mTableNameCurrent + "(AccNumber) VALUES(?)";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, accNumber);
		if (ps.executeUpdate() > 0) {
			ret = true;
		}
		ps.close();
		return ret;
	}

//	public boolean saveT00005(String accNumber, String prodCode, String accCode,String branchCode,
//			String prodGL, String cashGL, Connection conn) throws SQLException {
//		boolean ret = false;
//		
//		String sql = "INSERT INTO T00005 VALUES(?,?,?,?,?,?,?,?,?,?,?)";
//		PreparedStatement ps = conn.prepareStatement(sql);
//		updateRecordT00005(accNumber, prodCode, accCode, branchCode, prodGL, cashGL, ps);
//		if (ps.executeUpdate() > 0) {
//			ret = true;
//		}
//		ps.close();
//		return ret;
//	}
	public boolean saveT00005(String accNumber, String prodCode, String accCode, String ccy, String branchCode,
			String prodGL, String cashGL, Connection conn) throws SQLException {
		boolean ret = false;
		String sql = "INSERT INTO T00005 VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement ps = conn.prepareStatement(sql);
		updateRecordT00005(accNumber, prodCode, accCode, ccy, branchCode, prodGL, cashGL, ps);
		if (ps.executeUpdate() > 0) {
			ret = true;
		}
		ps.close();
		return ret;
	}

	
	public void setAccountsBean(AccountData aAccBean) {
		AccBean = aAccBean;
	}

	public DataResult updateAccounts_Balance(String acc, Connection conn) {
		DataResult ret = new DataResult();

		PreparedStatement pstmt = null;
		try {
			pstmt = conn.prepareStatement(
					"UPDATE Accounts_Balance SET " + "CurrentBalance = CurrentBalance " + "WHERE AccNumber=?");
			pstmt.setString(1, acc);
			pstmt.executeUpdate();
			pstmt.close();
			ret.setStatus(true);

		} catch (SQLException e) {
			e.printStackTrace();
			ret.setStatus(false);
		}
		return ret;
	}

	public boolean updateAccountStatusForClosingWithdrawal(String pAccNumber, int pStatus, String date, Connection conn)
			throws SQLException {
		boolean ret = false;
		PreparedStatement pstmt = conn.prepareStatement(
				"UPDATE Accounts SET Status = ?, LastUpdate = ?, ClosingDate = ? WHERE AccNumber = ?");
		pstmt.setInt(1, pStatus);
		pstmt.setString(2, SharedUtil.formatDateStr2MIT(date));
		pstmt.setString(3, SharedUtil.formatDateStr2MIT(date));
		pstmt.setString(4, pAccNumber);
		pstmt.setQueryTimeout(DAOManager.getNormalTime());
		if (pstmt.executeUpdate() > 0) {
			ret = true;
		}
		pstmt.close();

		return ret;
	}

	public DataResult updateBalance(String acc, Connection conn) throws SQLException {
		DataResult ret = new DataResult();

		PreparedStatement pstmt = null;

		pstmt = conn
				.prepareStatement("UPDATE Accounts SET " + "CurrentBalance = CurrentBalance " + "WHERE AccNumber=?");
		pstmt.setString(1, acc);
		pstmt.setQueryTimeout(DAOManager.getNormalTime());
		if (pstmt.executeUpdate() > 0)
			ret.setStatus(true);
		else
			ret.setStatus(false);
		pstmt.close();

		return ret;
	}

	public boolean updateCrCurrentBal(String id, double amount, String lastTransDate, String lastUpdate,
			Connection conn) throws SQLException {
		boolean ret = false;
		String sql = "update " + mTableName
				+ " set currentBalance = currentbalance + ? ,LastTransDate= ? , LastUpdate=?  where AccNumber = ? ";
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setDouble(i++, amount);
		ps.setString(i++, lastTransDate);
		ps.setString(i++, lastUpdate);
		ps.setString(i++, id);
		if (ps.executeUpdate() > 0) {
			ret = true;
		}
		ps.close();
		return ret;
	}

	public boolean updateDrCurrentBal(String id, double amount, String lastTransDate, String lastUpdate,
			Connection conn) throws SQLException {
		boolean ret = false;
		String sql = "update " + mTableName
				+ " set currentBalance = currentbalance - ?, LastTransDate= ? , LastUpdate=?  where AccNumber = ? ";
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setDouble(i++, amount);
		ps.setString(i++, lastTransDate);
		ps.setString(i++, lastUpdate);
		ps.setString(i++, id);
		if (ps.executeUpdate() > 0) {
			ret = true;
		}
		ps.close();
		return ret;
	}

	private void updateRecord(AccountData aData, PreparedStatement ps, boolean isFlag) throws SQLException {
		int i = 1;
		ps.setString(i++, aData.getAccountNumber());
		ps.setString(i++, aData.getCurrencyCode());
		ps.setDouble(i++, aData.getOpeningBalance());
		ps.setString(i++, aData.getOpeningDate());
		if (isFlag) {
			ps.setDouble(i++, aData.getCurrentBalance());
		}
		ps.setString(i++, aData.getClosingDate());
		ps.setString(i++, aData.getLastUpdate());
		ps.setInt(i++, aData.getStatus());
		ps.setString(i++, aData.getLastTransDate());
		ps.setString(i++, aData.getDrawingType());
		ps.setString(i++, aData.getAccountName());
		ps.setString(i++, aData.getpAccountNrc());//ndh
		ps.setString(i++, aData.getpSAccNo());//ndh
//		ps.setString(i++, "");
//		ps.setString(i++, "");
		ps.setString(i++, aData.getRemark());
		ps.setString(i++, aData.getBranchCode());
	}
	
	private void updateRecordT00005(String accNumber, String prodCode, String accCode, String ccy, String branchCode,
			String prodGL, String cashGL, PreparedStatement ps) throws SQLException {
		int i = 1;
		ps.setInt(i++, 52);
		ps.setInt(i++, 0);
		ps.setInt(i++, 0);
		ps.setInt(i++, 0);
		ps.setString(i++, accNumber);
		ps.setString(i++, prodCode);
		ps.setString(i++, accCode);
		if (ccy.equals("1.0")){
			ps.setString(i++, "MMK");
		} else {
			ps.setString(i++, ccy);
		}
		ps.setString(i++, branchCode);
		ps.setString(i++, prodGL);
		ps.setString(i++, cashGL);
		ps.setString(i++, "");

	}


}
