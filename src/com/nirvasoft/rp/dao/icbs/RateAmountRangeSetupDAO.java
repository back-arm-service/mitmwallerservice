package com.nirvasoft.rp.dao.icbs;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.shared.icbs.RateAmountRangeSetupData;

public class RateAmountRangeSetupDAO {

	private RateAmountRangeSetupData mAmountRangeData;
	private ArrayList<RateAmountRangeSetupData> mAmountRangeDataList;
	private String mTable = "RateAmountRangeSetup";

	public RateAmountRangeSetupDAO() {
		mAmountRangeData = new RateAmountRangeSetupData();
		mAmountRangeDataList = new ArrayList<RateAmountRangeSetupData>();
	}

	public boolean checkAmount(double FAmt, double TAmt, Connection pconn) throws SQLException {
		boolean l_result = false;
		String l_query = "SELECT * FROM " + mTable + " WHERE FromAmount<=" + FAmt + " AND ToAmount>=" + FAmt
				+ " OR FromAmount<=" + TAmt + " AND ToAmount>=" + TAmt;
		PreparedStatement l_pstmt = pconn.prepareStatement(l_query);
		ResultSet l_RS = l_pstmt.executeQuery();
		while (l_RS.next()) {
			l_result = true;
		}
		l_pstmt.close();
		return l_result;
	}

	public boolean checkAmountForUpdate(double FAmt, double TAmt, Connection pconn) throws SQLException {
		boolean l_result = false;
		String l_query = "SELECT ComRangeType FROM " + mTable + " WHERE FromAmount=" + FAmt + " AND ToAmount=" + TAmt;
		PreparedStatement l_pstmt = pconn.prepareStatement(l_query);
		ResultSet l_RS = l_pstmt.executeQuery();
		while (l_RS.next()) {
			l_result = true;
		}
		l_pstmt.close();
		return l_result;
	}

	public boolean deleteAmountRangeData(int pSerialNo, Connection pconn) throws SQLException {
		boolean l_result = false;
		String l_query = "DELETE FROM " + mTable + " WHERE SerialNo=" + pSerialNo;
		PreparedStatement l_pstmt = pconn.prepareStatement(l_query);
		l_pstmt.setQueryTimeout(DAOManager.getNormalTime());
		if (l_pstmt.executeUpdate() > 0) {
			l_result = true;
		}
		l_pstmt.close();
		return l_result;
	}

	public RateAmountRangeSetupData getRateAmountRangeData() {
		return mAmountRangeData;
	}

	public ArrayList<RateAmountRangeSetupData> getRateAmountRangeDataList() {
		return mAmountRangeDataList;
	}

	public boolean getRateAmountRangeDataList(int pComRangeType, Connection pConn) throws SQLException {
		boolean l_result = false;
		mAmountRangeDataList = new ArrayList<RateAmountRangeSetupData>();

		StringBuilder l_Query = new StringBuilder("");
		l_Query.append("SELECT SerialNo, ComRangeType, FromAmount, ToAmount, CRate, ComMin, ComMax, CommType From "
				+ mTable + " WHERE 1=1");
		if (pComRangeType != 0) {
			l_Query.append(" AND ComRangeType = " + pComRangeType);
		}
		PreparedStatement l_pstmt = pConn.prepareStatement(l_Query.toString());
		ResultSet l_Rs = l_pstmt.executeQuery();
		while (l_Rs.next()) {
			l_result = true;
			mAmountRangeData = new RateAmountRangeSetupData();
			readRecord(mAmountRangeData, l_Rs);
			mAmountRangeDataList.add(mAmountRangeData);
		}
		return l_result;
	}

	public boolean getRateAmountRangeDataList(String pCommType, Connection pConn) throws SQLException {
		boolean l_result = false;
		mAmountRangeDataList = new ArrayList<RateAmountRangeSetupData>();

		StringBuilder l_Query = new StringBuilder("");
		l_Query.append("SELECT SerialNo, ComRangeType, FromAmount, ToAmount, CRate, ComMin, ComMax, CommType From "
				+ mTable + " WHERE 1=1");
		if (!pCommType.equals("")) {
			l_Query.append(" AND CommType = '" + pCommType + "'");
		}
		System.out.println(l_Query);
		PreparedStatement l_pstmt = pConn.prepareStatement(l_Query.toString());
		ResultSet l_Rs = l_pstmt.executeQuery();
		while (l_Rs.next()) {
			l_result = true;
			mAmountRangeData = new RateAmountRangeSetupData();
			readRecord(mAmountRangeData, l_Rs);
			mAmountRangeDataList.add(mAmountRangeData);
		}
		return l_result;
	}

	public boolean getRateAmountRangeDataListByCommRangeType(int pComRangeType, Connection pConn) throws SQLException {
		boolean l_result = false;
		mAmountRangeDataList = new ArrayList<RateAmountRangeSetupData>();

		StringBuilder l_Query = new StringBuilder("");
		l_Query.append("SELECT SerialNo, ComRangeType, FromAmount, ToAmount, CRate, ComMin, ComMax, CommType From "
				+ mTable + " WHERE 1=1");
		l_Query.append(" AND ComRangeType = ?");

		System.out.println(l_Query);
		PreparedStatement l_pstmt = pConn.prepareStatement(l_Query.toString());
		l_pstmt.setInt(1, pComRangeType);
		ResultSet l_Rs = l_pstmt.executeQuery();
		while (l_Rs.next()) {
			l_result = true;
			mAmountRangeData = new RateAmountRangeSetupData();
			readRecord(mAmountRangeData, l_Rs);
			mAmountRangeDataList.add(mAmountRangeData);
		}
		return l_result;
	}

	public RateAmountRangeSetupData getRateAmountRangeSetupData(int pSerialNo, Connection pconn) throws SQLException {
		String l_query = "SELECT * FROM " + mTable + " WHERE 1=1";
		if (pSerialNo != 0) {
			l_query += " AND SerialNo =" + pSerialNo;
		}
		PreparedStatement l_pstmt = pconn.prepareStatement(l_query);
		ResultSet l_RS = l_pstmt.executeQuery();
		while (l_RS.next()) {
			mAmountRangeData = new RateAmountRangeSetupData();
			readRecord(mAmountRangeData, l_RS);
		}
		l_pstmt.close();
		return mAmountRangeData;
	}

	public ArrayList<RateAmountRangeSetupData> getRateAmountRangeSetupDataList(Connection pconn) throws SQLException {
		ArrayList<RateAmountRangeSetupData> m_AmountRangeList = new ArrayList<RateAmountRangeSetupData>();
		String l_query = "SELECT * FROM " + mTable;
		PreparedStatement l_pstmt = pconn.prepareStatement(l_query);
		ResultSet l_RS = l_pstmt.executeQuery();
		while (l_RS.next()) {
			mAmountRangeData = new RateAmountRangeSetupData();
			readRecord(mAmountRangeData, l_RS);
			m_AmountRangeList.add(mAmountRangeData);
		}
		l_pstmt.close();
		return m_AmountRangeList;
	}

	public ArrayList<RateAmountRangeSetupData> getRateAmountRangeTypeSearchOption(String condition, Connection conn)
			throws ParserConfigurationException, SAXException, IOException, ClassNotFoundException, SQLException {

		mAmountRangeDataList = new ArrayList<RateAmountRangeSetupData>();
		StringBuffer l_query = new StringBuffer("");
		l_query.append("SET DATEFORMAT DMY; SELECT * " + "FROM " + mTable + " WHERE 1 = 1 ");
		l_query.append(condition);
		PreparedStatement pstmt = conn.prepareStatement(l_query.toString());

		ResultSet rs = pstmt.executeQuery();
		int i = 0;
		while (rs.next()) {
			RateAmountRangeSetupData l_DataBean = new RateAmountRangeSetupData();
			readRecord(l_DataBean, rs);
			mAmountRangeDataList.add(i++, l_DataBean);
		}
		pstmt.close();
		return mAmountRangeDataList;
	}

	private void readRecord(RateAmountRangeSetupData pdata, ResultSet pRs) throws SQLException {

		pdata.setSerialNo(pRs.getInt("SerialNo"));
		pdata.setComRangeType(pRs.getInt("ComRangeType"));
		pdata.setFromAmount(pRs.getDouble("FromAmount"));
		pdata.setToAmount(pRs.getDouble("ToAmount"));
		pdata.setCRate(pRs.getDouble("CRate"));
		pdata.setComMin(pRs.getDouble("ComMin"));
		pdata.setComMax(pRs.getDouble("ComMax"));
		pdata.setCommType(pRs.getString("CommType"));
	}

	public boolean saveAmountRangeData(RateAmountRangeSetupData pdata, Connection pconn) throws SQLException {
		boolean l_result = false;
		String l_query = "INSERT INTO " + mTable + "(ComRangeType,FromAmount,ToAmount,CRate,ComMin,ComMax,CommType) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement l_pstmt = pconn.prepareStatement(l_query);
		updateRecords(pdata, l_pstmt, false);
		l_pstmt.setQueryTimeout(DAOManager.getNormalTime());
		if (l_pstmt.executeUpdate() > 0) {
			l_result = true;
		}
		l_pstmt.close();
		return l_result;
	}

	public void setRateAmountRangeData(RateAmountRangeSetupData mAmountRangeData) {
		this.mAmountRangeData = mAmountRangeData;
	}

	public void setRateAmountRangeDataList(ArrayList<RateAmountRangeSetupData> mAmountRangeDataList) {
		this.mAmountRangeDataList = mAmountRangeDataList;
	}

	public boolean updateAmountRangeData(RateAmountRangeSetupData pdata, Connection pconn) throws SQLException {
		boolean l_result = false;
		String l_query = "UPDATE " + mTable
				+ " SET ComRangeType=?, FromAmount=?, ToAmount=?, CRate=?, ComMin=?, ComMax=?, CommType=? WHERE SerialNo=? ";
		PreparedStatement l_pstmt = pconn.prepareStatement(l_query);
		updateRecords(pdata, l_pstmt, true);
		l_pstmt.setQueryTimeout(DAOManager.getNormalTime());
		if (l_pstmt.executeUpdate() > 0) {
			l_result = true;
		}
		l_pstmt.close();
		return l_result;
	}

	private void updateRecords(RateAmountRangeSetupData pdata, PreparedStatement pPS, boolean pUpdate)
			throws SQLException {
		int l_index = 1;
		pPS.setInt(l_index++, pdata.getComRangeType());
		pPS.setDouble(l_index++, pdata.getFromAmount());
		pPS.setDouble(l_index++, pdata.getToAmount());
		pPS.setDouble(l_index++, pdata.getCRate());
		pPS.setDouble(l_index++, pdata.getComMin());
		pPS.setDouble(l_index++, pdata.getComMax());
		pPS.setString(l_index++, pdata.getCommType());
		if (pUpdate) {
			pPS.setInt(l_index++, pdata.getSerialNo());
		}
	}

}
