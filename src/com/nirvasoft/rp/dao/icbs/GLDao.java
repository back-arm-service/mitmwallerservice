package com.nirvasoft.rp.dao.icbs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GLDao {

	public static String mTableName = "GL";

	public boolean isGLAccount(String accNumber, Connection conn) throws SQLException {
		boolean isGL = false;
		String sql = "SELECT Accno FROM " + mTableName + " WHERE AccNo = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, accNumber);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			isGL = true;
		}
		ps.close();
		rs.close();
		return isGL;
	}

}
