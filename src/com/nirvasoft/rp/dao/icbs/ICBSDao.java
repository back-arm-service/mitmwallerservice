
package com.nirvasoft.rp.dao.icbs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.rp.data.CommRateHeaderData;
import com.nirvasoft.rp.mgr.CommRateMgr;
import com.nirvasoft.rp.shared.AccountActivityData;
import com.nirvasoft.rp.shared.AccountActivityResponse;
import com.nirvasoft.rp.shared.AgentTransferReq;
import com.nirvasoft.rp.shared.DepositAccData;
import com.nirvasoft.rp.shared.DepositAccDataResult;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.shared.icbs.AgentData;
import com.nirvasoft.rp.shared.icbs.AgentEncashData;
import com.nirvasoft.rp.shared.icbs.AgentResData;
import com.nirvasoft.rp.shared.icbs.GetOutstandingBillData;
import com.nirvasoft.rp.shared.icbs.GetOutstandingBillResData;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.Response;

public class ICBSDao {

	public boolean checkAGGroup(String fromid, String toid, Connection l_Conn) throws SQLException {

		String fromparentID = "";
		String toparentID = "";
		String sql = "";

		sql = "select ParentID From Agent where LoginId=?";
		PreparedStatement ps = l_Conn.prepareStatement(sql);
		ps.setString(1, fromid);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			fromparentID = rs.getString("ParentId");
		}
		ps.close();
		rs.close();

		sql = "select ParentID From Agent where LoginId=?";
		PreparedStatement ps1 = l_Conn.prepareStatement(sql);
		ps1.setString(1, toid);
		ResultSet rs1 = ps1.executeQuery();
		if (rs1.next()) {
			toparentID = rs1.getString("ParentId");
		}
		ps1.close();
		rs1.close();

		// is same group
		if (fromparentID.equalsIgnoreCase(toparentID)) {
			return true;
		} else {
			return false;
		}
	}

	public String enquiryCheque(String accountNo, String chequeNo, Connection conn) throws SQLException {
		String ret = "";
		String chqChar = "";
		String chqNum = "";
		long chqNumLong = 0;
		if (!chequeNo.matches("\\d")) {
			for (int i = 0; i < chequeNo.length(); i++) {
				String tmpChar = "";
				tmpChar = String.valueOf(chequeNo.charAt(i));
				if (tmpChar.matches("\\d")) {
					chqNum += tmpChar;
				} else {
					chqChar += tmpChar;
				}
			}
		}
		if (!chqNum.equalsIgnoreCase("")) {
			chqNumLong = Long.parseLong(chqNum);
			chqNum = String.valueOf(chqNumLong);
		}
		String sql = "select right(substring(sheets,1,(?-chknofrom+1)),1) from ( "
				+ " select AccNumber, ChknoFrom,ChkNoFrom+(len(sheets)-1) as endchq, (len(sheets)) as lengthChq,sheets, "
				+ " chknochar,dateissued,dateapproved,status  from checkissue where accnumber=?) A where  "
				+ " A.chknofrom <= ? and A.endchq >=?  and chknochar=?";
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setString(i++, chqNum);
		ps.setString(i++, accountNo);
		ps.setString(i++, chqNum);
		ps.setString(i++, chqNum);
		ps.setString(i++, chqChar);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			ret = String.valueOf(rs.getInt(1));
		}
		ps.close();
		rs.close();
		if (ret.equalsIgnoreCase("0")) {
			ret = "Unpaid";
		} else if (ret.equalsIgnoreCase("1")) {
			ret = "Paid";
		} else if (ret.equalsIgnoreCase("2")) {
			ret = "Stopped";
		} else if (ret.equalsIgnoreCase("3")) {
			ret = "Cancel";
		}

		return ret;
	}

	public AccountActivityResponse getAccountActivity(String accNumber, String customerID, String durationType,
			String fromDate, String toDate, String currentPage, String pageSize, Connection conn) throws SQLException {
		int pagesize = Integer.parseInt(pageSize);
		int currentpage = Integer.parseInt(currentPage);
		int startPage = (currentpage - 1) * pagesize;
		int endPage = pagesize + startPage;
		AccountActivityResponse ret = new AccountActivityResponse();
		ArrayList<AccountActivityData> dataList = new ArrayList<AccountActivityData>();
		String tmpTableName = "Tmp" + accNumber;
		String sql1 = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[" + tmpTableName
				+ "]') AND type in (N'U')) " + "	DROP TABLE " + tmpTableName;
		PreparedStatement ps = conn.prepareStatement(sql1);
		ps.executeUpdate();
		String whereclause = "";
		if (!accNumber.equals("")) {
			whereclause += " and a.AccNumber = '" + accNumber + "' "; // NRC
		}
		if (durationType.equals("0")) {
			startPage = 0;
			endPage = 10;
		}
		if (durationType.equals("1")) {
			String nowdate = GeneralUtil.oracledatetoString();
			String lasttwodate = GeneralUtil.oraclelastday(2); // TRUNC(to_date('2017-06-12','yyyy-mm-dd'))
			whereclause += " and a.EffectiveDate >= '" + lasttwodate + "' and a.EffectiveDate <= '" + nowdate + "'";
		}
		if (durationType.equals("2")) {
			String nowdate = GeneralUtil.oracledatetoString();
			String lastfivedate = GeneralUtil.oraclelastday(5); // TRUNC(to_date('2017-06-12','yyyy-mm-dd'))
			whereclause += " and a.EffectiveDate >= '" + lastfivedate + "' and a.EffectiveDate <= '" + nowdate + "'";
		}
		if (durationType.equals("3")) {
			if (fromDate.equalsIgnoreCase(null) || fromDate.equals("")) {
				ret.setDesc("from date is null");
				ret.setCode("0014");
				return ret;
			} else if (toDate.equalsIgnoreCase(null) || toDate.equals("")) {
				ret.setDesc("to date is null");
				ret.setCode("0014");
				return ret;
			} else {
				String fdate = fromDate.substring(0, 4) + "-" + fromDate.substring(4, 6) + "-"
						+ fromDate.substring(6, 8);
				String tdate = toDate.substring(0, 4) + "-" + toDate.substring(4, 6) + "-" + toDate.substring(6, 8);
				whereclause += " and a.EffectiveDate >= '" + fdate + "' and a.EffectiveDate <= '" + tdate + "'";
			}
		}
		String sql2 = "select b.TrCurCode,a.accnumber,b.TrAmount,a.EffectiveDate,a.Description,a.TransRef,"
				+ "a.TransNo,(case when a.TransType>=500 then '1' else '2' end) as DrCr,a.remark, a.SubRef into "
				+ tmpTableName + " from accounttransactionold a "
				+ " inner join accountgltransaction b on a.transno=b.transno " + whereclause;
		ps = conn.prepareStatement(sql2);
		ps.executeUpdate();
		String sql3 = "Insert Into " + tmpTableName
				+ "(TrCurCode,Accnumber,TrAmount,EffectiveDate,Description,TransRef,TransNo,DrCr,remark, a.SubRef) "
				+ "select b.TrCurCode,a.accnumber,b.TrAmount,a.EffectiveDate,a.Description,a.TransRef,"
				+ "a.TransNo,(case when a.TransType>=500 then '1' else '2' end) as DrCr,a.remark, a.SubRef "
				+ "from accounttransaction a inner join accountgltransaction b on a.transno=b.transno " + whereclause;
		ps = conn.prepareStatement(sql3);
		ps.executeUpdate();
		String sql4 = "Select * from (select Row_Number() Over (Order By effectivedate desc, transno desc) as rownumber,* "
				+ "from " + tmpTableName + ") c where 1=1 and c.rownumber > " + startPage + " And c.rownumber <= "
				+ endPage + " " + "order By c.effectivedate desc, c.transno desc";
		ps = conn.prepareStatement(sql4);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			AccountActivityData data = new AccountActivityData();
			data = readAccountActivityData(rs);
			dataList.add(data);
		}
		String sql5 = "Select COUNT(*) As total from " + tmpTableName;
		ps = conn.prepareStatement(sql5);
		PreparedStatement pstm = conn.prepareStatement(sql5);
		ResultSet result = pstm.executeQuery();
		result.next();
		int totalcount = result.getInt("total");
		// if duration type is default,for only 10 record requirement
		if (durationType.equals("0")) {
			if (totalcount > 10) {
				ret.setTotalCount(10);
			} else {
				ret.setTotalCount(result.getInt("total"));
			}
		} else {
			ret.setTotalCount(result.getInt("total"));
		}
		AccountActivityData[] dataArr = new AccountActivityData[dataList.size()];
		for (int i = 0; i < dataList.size(); i++) {
			dataArr[i] = dataList.get(i);
		}
		// end
		if (dataList.size() > 0) {
			ret.setData(dataArr);
			ret.setCode("0000");
			ret.setDesc("Successfully");
		} else {
			ret.setCode("0014");
			ret.setDesc("There is no data");
		}
		ps.close();
		rs.close();
		return ret;
	}

	public AccountActivityResponse getAccountActivityList(String accNumber, String customerID, String durationType,
			String fromDate, String toDate, String currentPage, String pageSize, Connection conn) throws SQLException {
		ResultSet rs = null;
		int pagesize = Integer.parseInt(pageSize);
		int currentpage = Integer.parseInt(currentPage);
		int startPage = (currentpage - 1) * pagesize;
		int endPage = pagesize + startPage;
		AccountActivityResponse ret = new AccountActivityResponse();
		ArrayList<AccountActivityData> dataList = new ArrayList<AccountActivityData>();
		String tmpTableName = "Tmp" + accNumber;
		String sql1 = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[" + tmpTableName
				+ "]') AND type in (N'U')) " + "	DROP TABLE " + tmpTableName;
		PreparedStatement ps = conn.prepareStatement(sql1);
		ps.executeUpdate();
		
		String whereclause = "";
		if (!accNumber.equals("")) {
			whereclause += " and a.AccNumber = '" + accNumber + "' "; // NRC
		}
		if (durationType.equals("0")) {
			startPage = 0;
			endPage = 10;
		}
		if (durationType.equals("1")) {
			
			String nowdate = GeneralUtil.oracledatetoString();
			String lasttwodate = GeneralUtil.oraclelastday(2); // TRUNC(to_date('2017-06-12','yyyy-mm-dd'))
			whereclause += " and a.Transdate >= '" + lasttwodate + "' and a.Transdate <= '" + nowdate + "'";
		}
		if (durationType.equals("2")) {
			String nowdate = GeneralUtil.oracledatetoString();
			String lastfivedate = GeneralUtil.oraclelastday(5); // TRUNC(to_date('2017-06-12','yyyy-mm-dd'))
			whereclause += " and a.Transdate > '" + lastfivedate + "' and a.Transdate <= '" + nowdate + "'";
		}
		if (durationType.equals("3")) {
			if (fromDate.equalsIgnoreCase(null) || fromDate.equals("")) {
				ret.setDesc("from date is null");
				ret.setCode("0014");
				return ret;
			} else if (toDate.equalsIgnoreCase(null) || toDate.equals("")) {
				ret.setDesc("to date is null");
				ret.setCode("0014");
				return ret;
			} else {
				String fdate = fromDate.substring(0, 4) + "-" + fromDate.substring(4, 6) + "-"
						+ fromDate.substring(6, 8);
				String tdate = toDate.substring(0, 4) + "-" + toDate.substring(4, 6) + "-" + toDate.substring(6, 8);
				whereclause += " and a.Transdate >= '" + fdate + "' and a.Transdate <= '" + tdate + "'";
			}
		}
		String sql2 = "select b.TrCurCode,a.accnumber,b.TrAmount,a.TransDate,a.Description,a.TransRef,a.AccRef,"
				+ "a.TransNo,(case when a.TransType>=500 then '1' else '2' end) as DrCr,a.remark, a.SubRef into "
				+ tmpTableName + " from accounttransactionold a "
				+ " inner join accountgltransaction b on a.transno=b.transno " + whereclause;
		ps = conn.prepareStatement(sql2);
		ps.executeUpdate();
		String sql3 = "Insert Into " + tmpTableName
				+ "(TrCurCode,Accnumber,TrAmount,a.TransDate,Description,TransRef,TransNo,DrCr,remark, a.SubRef, a.AccRef) "
				+ "select b.TrCurCode,a.accnumber,b.TrAmount,a.TransDate,a.Description,a.TransRef,"
				+ "a.TransNo,(case when a.TransType>=500 then '1' else '2' end) as DrCr,a.remark, a.SubRef,a.AccRef"
				+ " from accounttransaction a inner join accountgltransaction b on a.transno=b.transno " + whereclause;
		ps = conn.prepareStatement(sql3);
		ps.executeUpdate();
		if (durationType.equals("0")) {
		String sql4 = "Select * from (select Row_Number() Over (Order By TransDate desc, transno desc) as rownumber,* "
				+ "from " + tmpTableName + ") c where 1=1 and c.rownumber > " + startPage + " And c.rownumber <= "
				+ endPage + " " + "order By c.TransDate desc, c.transno desc";
		ps = conn.prepareStatement(sql4);
		rs = ps.executeQuery();
		while (rs.next()) {
			AccountActivityData data = new AccountActivityData();
			data = readAccountActivityData(rs);
			dataList.add(data);
		}}
		else{
		String sql4 = "Select * from (select Row_Number() Over (Order By TransDate desc, transno desc) as rownumber,* "
				+ "from " + tmpTableName + ") c where 1=1" + "order By c.TransDate desc, c.transno desc";
		ps = conn.prepareStatement(sql4);
		rs = ps.executeQuery();
		while (rs.next()) {
			AccountActivityData data = new AccountActivityData();
			data = readAccountActivityData(rs);
			dataList.add(data);
		}}
		String sql5 = "Select COUNT(*) As total from " + tmpTableName;
		ps = conn.prepareStatement(sql5);
		PreparedStatement pstm = conn.prepareStatement(sql5);
		ResultSet result = pstm.executeQuery();
		result.next();
		int totalcount = result.getInt("total");
		// if duration type is default,for only 10 record requirement
		if (durationType.equals("0")) {
			if (totalcount > 10) {
				ret.setTotalCount(10);
			} else {
				ret.setTotalCount(result.getInt("total"));
			}
			ret.setPageCount(1);
		} else {
			ret.setTotalCount(result.getInt("total"));
			int pagecount = result.getInt("total") / 10;
			int value = result.getInt("total") % 10;
			if (value > 0) {
				pagecount += 1;
			}
			ret.setPageCount(pagecount);
		}
		AccountActivityData[] dataArr = new AccountActivityData[dataList.size()];
		for (int i = 0; i < dataList.size(); i++) {
			dataArr[i] = dataList.get(i);
		}
		// end
		if (dataList.size() > 0) {
			ret.setData(dataArr);
			ret.setCode("0000");
			ret.setDesc("Successfully");
		} else {
			ret.setCode("0014");
			ret.setDesc("There is no data");
		}
		ps.close();
		rs.close();
		return ret;
	}

	public DepositAccDataResult getAccountSummary(String custno, Connection conn) throws SQLException {
		DepositAccDataResult cusret = new DepositAccDataResult();
		ArrayList<DepositAccData> ret = new ArrayList<DepositAccData>();
		DepositAccData[] dataArr = null;
		boolean isData = false;
		String sql = " select (case when(isnull(t12.t1,'')<>'') then 'Card Account' else p.ProcessCode5 end) as Description,"
				+ " a.currentBalance, a.currencyCode, a.AccNumber,t12.t1 from CAJunction ca inner join accounts a "
				+ "on ca.AccNumber = a.AccNumber inner join T00005 t on a.AccNumber = t.T1 inner join acctype "
				+ "at on t.t3 = at.AccountCode and ca.AccType = ca.RType "
				+ " inner join ProductType p on t.T2=p.ProductCode left join T00012 t12 on a.AccNumber=t12.t6 "
				+ "where ca.CustomerId in (" + custno + ") and a.status <> 3";
		PreparedStatement ps = conn.prepareStatement(sql);
		System.out.println("account-summary-sql: " + sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			DepositAccData aData = new DepositAccData();
			aData.setDepositAcc(rs.getString("AccNumber"));
			aData.setCcy(rs.getString("currencyCode"));
			aData.setAccType(rs.getString("Description"));
			aData.setAvlBal(String.valueOf(rs.getDouble("currentBalance")));
			ret.add(aData);
			isData = true;
		}
		if (isData == true) {
			cusret.setCode("0000");
			cusret.setDesc("Successfully");
			if (ret.size() > 0) {
				dataArr = new DepositAccData[ret.size()];
				for (int i = 0; i < ret.size(); i++) {
					dataArr[i] = ret.get(i);
				}
			}
			cusret.setDataList(dataArr);

		} else {
			cusret.setCode("0014");
			cusret.setDesc("There is no data.");
		}
		ps.close();
		rs.close();
		return cusret;
	}

	public AgentEncashData getAGEncashData(String refNo, String agentType, Connection l_Conn) throws SQLException {
		// TODO Auto-generated method stub
		AgentEncashData res = new AgentEncashData();
		ArrayList<AgentTransferReq> dataList = new ArrayList<AgentTransferReq>();

		String sql = "select MobileUserID,FromAccount,FromName,ToAccount,ToName,Amount,DraweeName,DraweeNRC,"
				+ "DraweePhone,PayeeName,PayeeNRC,PayeePhone from AgentTransaction Where Status=1 and DTransRef=? and TransferType=? ";
		PreparedStatement ps = l_Conn.prepareStatement(sql);
		ps.setString(1, refNo.trim());
		ps.setString(2, agentType.trim());
		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			AgentTransferReq data = new AgentTransferReq();
			data.setUserID(rs.getString("MobileUserID"));
			data.setFromAccount(rs.getString("FromAccount"));
			data.setFromName(rs.getString("FromName"));
			data.setToAccount(rs.getString("ToAccount"));
			data.setToName(rs.getString("ToName"));
			data.setAmount(rs.getString("Amount"));
			data.setDraweeName(rs.getString("DraweeName"));
			data.setDraweeNRC(rs.getString("DraweeNRC"));
			data.setDraweePhone(rs.getString("DraweePhone"));
			data.setPayeeName(rs.getString("PayeeName"));
			data.setPayeeNRC(rs.getString("PayeeNRC"));
			data.setPayeePhone(rs.getString("PayeePhone"));
			data.setRefNo(refNo);
			dataList.add(data);
		}

		AgentTransferReq[] dataArr = new AgentTransferReq[dataList.size()];
		for (int i = 0; i < dataList.size(); i++) {
			dataArr[i] = dataList.get(i);
		}

		if (dataArr.length > 0) {
			res.setCode("0000");
			res.setDesc("Success");
			res.setData(dataArr);
		} else {
			res.setCode("0014");
			res.setDesc("No Record Found!");
		}
		ps.close();
		rs.close();

		return res;
	}

	public AgentResData getAgentUser(String id, String comRef, String Type, Connection l_Conn) throws SQLException {
		// TODO Auto-generated method stub
		AgentResData res = new AgentResData();
		res.setId(id);
		ArrayList<AgentData> dataList = new ArrayList<AgentData>();
		int isParent = 0;
		String parentID = "";
		String sql1 = "";
		String fromAccount = "";
		String fromName = "";
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		CommRateMgr comMgr = new CommRateMgr();

		String sql3 = "select B.name,B.LogInID,B.ParentID,B.AgentAccNumber,B.N1 from UVM005_A As A "
				+ "inner join Agent As B on A.t1=B.LogInID where A.t1=? and A.n3=1 and A.RecordStatus=2";
		PreparedStatement ps3 = l_Conn.prepareStatement(sql3);
		ps3.setString(1, id);
		ResultSet rs3 = ps3.executeQuery();
		if (rs3.next()) {
			isParent = rs3.getInt("N1");
			parentID = rs3.getString("ParentId");
			fromAccount = rs3.getString("AgentAccNumber");
			fromName = rs3.getString("name");
		} else {
			res.setCode("0014");
			res.setDesc("This user is not Agent User!");
		}
		// is parent
		if (!fromAccount.equals("")) {
			if (Type.equals("1")) { // Agent to Agent
				System.out.println("Is parent :: " + isParent);
				if (isParent == 1) {
					sql1 = "select * from Agent where ParentID=? and n1=0";
				} else {
					sql1 = "select * from Agent where LoginId<>? and (LogInID=? or ParentID=?)";
				}
				ps1 = l_Conn.prepareStatement(sql1);
				int j = 1;
				if (isParent == 1) {
					ps1.setString(j++, id);
				} else {
					ps1.setString(j++, id);
					ps1.setString(j++, parentID);
					ps1.setString(j++, parentID);
				}
				System.out.println("Search query :: " + sql1);
				rs1 = ps1.executeQuery();
				while (rs1.next()) {
					AgentData data = new AgentData();
					data.setUserID(rs1.getString("loginID"));
					data.setParentID(rs1.getString("ParentId"));
					data.setAccNumber(rs1.getString("AgentAccNumber"));
					data.setName(rs1.getString("Name"));
					data.setRecordStatus(rs1.getInt("RecordStatus"));
					data.setN1(rs1.getInt("N1"));
					dataList.add(data);
				}

				AgentData[] dataArr = new AgentData[dataList.size()];
				for (int i = 0; i < dataList.size(); i++) {
					dataArr[i] = dataList.get(i);
				}

				if (dataArr.length > 0) {
					res.setCode("0000");
					res.setDesc("Success");
					res.setFromAccount(fromAccount);
					res.setFromName(fromName);
					res.setData(dataArr);
					CommRateHeaderData commAgent = comMgr.getComRatedataByComRefNo(comRef);
					res.setComData(commAgent);
				} else {
					res.setFromAccount(fromAccount);
					res.setFromName(fromName);
					res.setCode("0014");
					res.setDesc("It has not Sub Agent!");
				}
				ps1.close();
				rs1.close();
			} else { // Agent to customer, Agent to Non-Customer
				if (!fromAccount.equals("")) {
					res.setCode("0000");
					res.setDesc("Success");
					res.setFromAccount(fromAccount);
					res.setFromName(fromName);
					CommRateHeaderData commAgent = comMgr.getComRatedataByComRefNo(comRef);
					res.setComData(commAgent);
				} else {
					res.setCode("0014");
					res.setDesc("This user is not Agent User!");
				}
			}
		}
		ps3.close();
		rs3.close();
		return res;
	}

	public GetOutstandingBillResData getOustandingBill(String meterID, int status, Connection l_Conn)
			throws SQLException {
		// TODO Auto-generated method stub
		GetOutstandingBillResData res = new GetOutstandingBillResData();
		res.setMeterID(meterID);
		ArrayList<GetOutstandingBillData> dataList = new ArrayList<GetOutstandingBillData>();
		String todayDate = GeneralUtil.getDateYYYYMMDDSimple();

		String sql1 = "Set Dateformat dmy; select ScheduleName,MerchantID,MeterID,BillNo,LastUnit,ThisUnit,TotalUnit,Rate,"
				+ "ConservationFee,Amount,LastDate,Status,DATEDIFF(Day,LastDate,'" + todayDate
				+ "') as penaltyDays from scheduleentry Where MeterID=? and status=?";
		PreparedStatement ps = l_Conn.prepareStatement(sql1);
		ps.setString(1, meterID);
		ps.setInt(2, status);
		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			GetOutstandingBillData data = new GetOutstandingBillData();
			data.setScheduleName(rs.getString("ScheduleName"));
			data.setMerchantID(rs.getString("MerchantID"));
			data.setMeterID(rs.getString("MeterID"));
			data.setBillNo(rs.getString("BillNo"));
			data.setLastUnit(rs.getString("LastUnit"));
			data.setThisUnit(rs.getString("ThisUnit"));
			data.setTotalUnit(rs.getString("TotalUnit"));
			data.setRate(rs.getString("Rate"));
			data.setConservationFee(rs.getString("ConservationFee"));
			data.setAmount(rs.getString("Amount"));
			data.setLastDate(rs.getString("LastDate"));
			data.setStatus(rs.getString("Status"));
			data.setPenaltyDays(rs.getString("penaltyDays"));
			dataList.add(data);
		}

		GetOutstandingBillData[] dataArr = new GetOutstandingBillData[dataList.size()];
		for (int i = 0; i < dataList.size(); i++) {
			dataArr[i] = dataList.get(i);
		}

		if (dataArr.length > 0) {
			res.setCode("0000");
			res.setDesc("Success");
			res.setOutData(dataArr);
		} else {
			res.setCode("0014");
			res.setDesc("No Bill Outstanding!");
		}

		return res;
	}

	AccountActivityData readAccountActivityData(ResultSet rs) throws SQLException {
		AccountActivityData ret = new AccountActivityData();
		ret.setCodTxnCurr(rs.getString("TrCurCode"));
		ret.setNbrAccount(rs.getString("accNumber"));
		ret.setTxnAmount(GeneralUtil.formatNumber(Double.parseDouble(rs.getString("TrAmount"))));
		ret.setTxnDateTime(rs.getString("TransDate"));
		ret.setTxnDate(rs.getString("TransDate"));
		if (ret.getTxnDate().length() > 10) {
			ret.setTxnDate(ret.getTxnDate().substring(0, 10));
		}
		ret.setTxnTypeDesc(rs.getString("Description"));
		if (!rs.getString("remark").equalsIgnoreCase("")) {
			ret.setTxnTypeDesc(rs.getString("Description") + ", " + rs.getString("remark"));
		}
		if (rs.getString("TransRef") == null || rs.getString("TransRef") == "") {
			ret.setTxtReferenceNo("");
		} else {
			ret.setTxtReferenceNo(rs.getString("TransRef"));
		}
		ret.setDrcr(rs.getString("DrCr"));
		ret.setSubRef(rs.getString("SubRef"));
		ret.setAccRef(rs.getString("AccRef"));
		ret.setRemark(rs.getString("Remark"));
		return ret;
	}

	public ResponseData stopCheque(String accountNo, String chequeNo, Connection conn) throws SQLException {
		ResponseData ret = new ResponseData();
		String chqChar = "";
		String chqNum = "";
		String stopStatus = "2";
		int status = 0;
		String desc = "";
		long chqNumLong = 0;
		if (!chequeNo.matches("\\d")) {
			for (int i = 0; i < chequeNo.length(); i++) {
				String tmpChar = "";
				tmpChar = String.valueOf(chequeNo.charAt(i));
				if (tmpChar.matches("\\d")) {
					chqNum += tmpChar;
				} else {
					chqChar += tmpChar;
				}
			}
		}
		if (!chqNum.equalsIgnoreCase("")) {
			chqNumLong = Long.parseLong(chqNum);
			chqNum = String.valueOf(chqNumLong);
		}
		String sql1 = "select right(substring(sheets,1,(?-chknofrom+1)),1) from ( "
				+ " select AccNumber, ChknoFrom,ChkNoFrom+(len(sheets)-1) as endchq, (len(sheets)) as lengthChq,sheets, "
				+ " chknochar,dateissued,dateapproved,status  from checkissue where accnumber=?) A where  "
				+ " A.chknofrom <= ? and A.endchq >=? and chknochar=?";
		PreparedStatement ps1 = conn.prepareStatement(sql1);
		int i = 1;
		ps1.setString(i++, chqNum);
		ps1.setString(i++, accountNo);
		ps1.setString(i++, chqNum);
		ps1.setString(i++, chqNum);
		ps1.setString(i++, chqChar);
		ResultSet rs1 = ps1.executeQuery();
		if (rs1.next()) {
			status = rs1.getInt(1);
			if (status == 0) {
				String sql2 = "update Checkissue Set sheets=substring(A.sheets,1,(?-A.chknofrom))+?+"
						+ "substring(A.sheets,(?-A.chknofrom)+2,A.lengthchq) from (select AccNumber, ChknoFrom,ChkNoFrom+(len(sheets)-1) "
						+ "as endchq, (len(sheets)) as lengthChq,sheets,chknochar,dateissued,dateapproved,status  "
						+ "from checkissue where accnumber=?) A inner join Checkissue C on A.Accnumber=C.AccNumber "
						+ "and A.Chknofrom=C.Chknofrom  where A.chknofrom <= ? and A.endchq >=? and A.chknochar=? ";
				PreparedStatement ps2 = conn.prepareStatement(sql2);
				int j = 1;
				ps2.setString(j++, chqNum);
				ps2.setString(j++, stopStatus);
				ps2.setString(j++, chqNum);
				ps2.setString(j++, accountNo);
				ps2.setString(j++, chqNum);
				ps2.setString(j++, chqNum);
				ps2.setString(j++, chqChar);
				if (ps2.executeUpdate() > 0) {
					ret.setCode("0000");
					ret.setDesc("Updated successfully.");
				}
				ps2.close();
			} else {
				if (status == 1) {
					desc = "Paid";
				} else if (status == 2) {
					desc = "Stopped";
				} else if (status == 3) {
					desc = "Cancel";
				}
				ret.setCode("0014");
				ret.setDesc("Cheque status is " + desc);
			}
		} else {
			ret.setCode("0014");
			ret.setDesc("Cheque not found");
		}
		ps1.close();
		rs1.close();
		return ret;
	}

	public Response updateScheduleEntry(String merchantID, String meterNo, String billNo, String transRef,
			Connection conn) throws SQLException {
		Response response = new Response();
		String sql = "Set Dateformat dmy; Update ScheduleEntry Set Status=1,TransRef=? "
				+ "Where MerchantID=? and MeterID=? and BillNo=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, transRef);
		stmt.setString(2, merchantID);
		stmt.setString(3, meterNo);
		stmt.setString(4, billNo);
		stmt.executeUpdate();
		response.setResponsecode("0000");
		response.setResponsedesc("Success");
		if (!conn.isClosed()) {
			conn.close();
		}
		return response;
	}
}
