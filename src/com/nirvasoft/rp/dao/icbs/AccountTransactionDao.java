package com.nirvasoft.rp.dao.icbs;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.shared.DataResult;
import com.nirvasoft.rp.shared.EPIXResponse;
import com.nirvasoft.rp.shared.Response;
import com.nirvasoft.rp.shared.icbs.AccountTransactionData;
import com.nirvasoft.rp.shared.icbs.TransactionData;
import com.nirvasoft.rp.shared.icbs.TransactionViewData;
import com.nirvasoft.rp.util.SharedUtil;

public class AccountTransactionDao {
	public static String mTableName = "AccountTransaction";
	public static String mTableNameOther = "AccountTransactionOld";

	private ArrayList<TransactionData> lstTransData;

	public AccountTransactionDao() {
		lstTransData = new ArrayList<TransactionData>();
	}

	public EPIXResponse addAccTransaction(AccountTransactionData data, Connection conn) throws SQLException {
		EPIXResponse result = new EPIXResponse();
		String sql = "INSERT INTO " + mTableName + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		updateRecord(data, ps);
		if (ps.executeUpdate() > 0) {
			result.setCode("0000");
			ResultSet rs = ps.getGeneratedKeys();
			if (rs != null && rs.next()) {
				result.setXref(rs.getString(1));
			} else
				result.setCode("0014");
			rs.close();
		}
		ps.close();
		return result;

	}

	public DataResult addAccTransaction(TransactionData pAccTransData, Connection conn)
			throws ParserConfigurationException, SAXException, IOException, ClassNotFoundException, SQLException {
		DataResult result = new DataResult();
		String sql = "INSERT INTO AccountTransaction VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement preparedstatement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		updateRecord(pAccTransData, preparedstatement);
		preparedstatement.setQueryTimeout(DAOManager.getTransTime());
		if (preparedstatement.executeUpdate() > 0) {
			result.setStatus(true);
			ResultSet rs = preparedstatement.getGeneratedKeys();
			if (rs != null && rs.next()) {
				result.setTransactionNumber(rs.getLong(1));
			} else
				result.setStatus(false);
			rs.close();
		}
		preparedstatement.close();

		return result;

	}

	public Response addAccTransactionV1(AccountTransactionData data, Connection conn) throws SQLException {
		Response result = new Response();
		String sql = "INSERT INTO " + mTableName + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		updateRecord(data, ps);
		if (ps.executeUpdate() > 0) {
			result.setMessageCode("0000");
			ResultSet rs = ps.getGeneratedKeys();
			if (rs != null && rs.next()) {
				result.setTrnRefNo(rs.getString(1));
			} else
				result.setMessageCode("0014");
			rs.close();
		}
		ps.close();
		return result;

	}

	public boolean checkAlreadyReceipt(String transRef, String accNumber, Connection conn) throws SQLException {
		boolean ret = false;
		String sql = "select * from " + mTableName + " where transType=205 and accNumber= ? and subRef =? "
				+ "union all select * from " + mTableNameOther + " where transType=205 and accNumber= ? and subRef =?";
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setString(i++, accNumber);
		ps.setString(i++, transRef);
		ps.setString(i++, accNumber);
		ps.setString(i++, transRef);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			ret = true;
		}
		ps.close();
		rs.close();
		return ret;
	}

	public boolean getAccTransactionsTransRefNew(int pTransRef, Connection conn) throws SQLException {
		boolean ret = false;
		if (getAccTransactionsTransRefReversal(pTransRef, conn)) {
			ret = true;
		} else {
			if (getAccTransactionsTransRefReversalOld(pTransRef, conn)) {
				ret = true;
			}
		}
		return ret;
	}

	public boolean getAccTransactionsTransRefReversal(int pTransRef, Connection conn) throws SQLException {
		boolean result = false;
		String l_Query = "SELECT TransNo,BranchCode,WorkStation,TransRef,SerialNo,TellerId,SupervisorId,TransTime,TransDate,Description,ChequeNo,CurrencyCode,CurrencyRate, "
				+ "Amount,TransType,AccNumber,PrevBalance,PrevUpdate,EffectiveDate,ContraDate,Status,AccRef,remark,SystemCode,SubRef "
				+ "FROM AccountTransaction  WHERE 1=1" + "AND TransRef = ? AND Status <254 Order By TransNo";

		PreparedStatement pstmt = conn.prepareStatement(l_Query);
		pstmt.setInt(1, pTransRef);
		int i = 0;
		pstmt.setQueryTimeout(DAOManager.getTransTime());
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			TransactionData l_TransData = new TransactionData();
			readRecord(l_TransData, rs);
			lstTransData.add(i++, l_TransData);
			result = true;
		}
		pstmt.close();
		return result;
	}

	public boolean getAccTransactionsTransRefReversalOld(int pTransRef, Connection conn) throws SQLException {
		boolean result = false;
		String l_Query = "SELECT TransNo,BranchCode,WorkStation,TransRef,SerialNo,TellerId,SupervisorId,TransTime,TransDate,Description,ChequeNo,CurrencyCode,CurrencyRate, "
				+ "Amount,TransType,AccNumber,PrevBalance,PrevUpdate,EffectiveDate,ContraDate,Status,AccRef,remark,SystemCode,SubRef "
				+ "FROM AccountTransactionOld  WHERE 1=1" + "AND TransRef = ? AND Status <254 Order By TransNo";

		PreparedStatement pstmt = conn.prepareStatement(l_Query);
		pstmt.setInt(1, pTransRef);
		int i = 0;
		pstmt.setQueryTimeout(DAOManager.getTransTime());
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			TransactionData l_TransData = new TransactionData();
			readRecord(l_TransData, rs);
			lstTransData.add(i++, l_TransData);
			result = true;
		}
		pstmt.close();
		return result;
	}

	public AccountTransactionData getDataByTransRef(int transRef, Connection conn) throws SQLException {
		AccountTransactionData ret = new AccountTransactionData();
		String sql = "select * from " + mTableName + " where transRef = ? order by transNo desc";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, transRef);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			ret = readRecord(rs);
		}
		ps.close();
		rs.close();
		return ret;
	}

	public int getMaxTransRef(Connection conn) throws Exception {
		int ret = 0;
		String sql = "Select ISNULL(MAX(CAST(TransNo AS INT)),0) From " + mTableName;
		PreparedStatement pstmt = conn.prepareStatement(sql);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			ret = rs.getInt(1);
		}
		pstmt.close();
		rs.close();
		ret += 1;
		return ret;
	}

	public AccountTransactionData getPaymentTransByTransRef(int transRef, Connection conn) throws SQLException {
		AccountTransactionData ret = new AccountTransactionData();
		String sql = "select * from " + mTableName + " where transType='705' and transRef= ?"
				+ " union all select * from " + mTableNameOther + " where transType='705' and transRef=?";
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setInt(i++, transRef);
		ps.setInt(i++, transRef);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			ret = readRecord(rs);
		}
		ps.close();
		rs.close();
		return ret;
	}

	public TransactionData getTotalCountofSADrTransactionOfWeek(String pAccNo, String pFromDate, String pToDate,
			int pCount, Connection pConn) throws SQLException {
		TransactionData l_TranData = new TransactionData();

		String l_Query = "SELECT COUNT(*) AS [Count],AT.AccNumber, AT.PrevUpdate FROM (SELECT * FROM AccountTransaction"
				+ " UNION SELECT * FROM AccountTransactionOld) AS AT WHERE AccNumber = ?"
				+ " AND EffectiveDate>=? AND EffectiveDate<=? AND TransType >= 500 AND Status < 254 GROUP BY AccNumber,PrevUpdate ";
		PreparedStatement l_pstmt = pConn.prepareStatement(l_Query);
		l_pstmt.setString(1, pAccNo);
		l_pstmt.setString(2, pFromDate);
		l_pstmt.setString(3, pToDate);
		ResultSet l_RS = l_pstmt.executeQuery();

		while (l_RS.next()) {
			if (l_RS.getInt("Count") >= pCount) {
				l_TranData.setPreviousDate(SharedUtil.formatDBDate2MIT(l_RS.getString("PrevUpdate")));
				l_TranData.setAccountNumber(l_RS.getString("AccNumber"));
			}

		}
		l_pstmt.close();
		return l_TranData;
	}

	public int getTotalCountofSADrTransactionOfWeekNew(String pAccNo, String pFromDate, String pToDate, int pCount,
			Connection pConn) throws SQLException {
		int ret = 0;
		String l_Query = "SELECT COUNT(*) AS [Count] FROM AccountTransactionOld AS AT WHERE AccNumber = ?"
				+ " AND EffectiveDate>=? AND EffectiveDate<=? AND TransType >= 500 AND Status < 254 ";
		PreparedStatement l_pstmt = pConn.prepareStatement(l_Query);
		l_pstmt.setString(1, pAccNo);
		l_pstmt.setString(2, pFromDate);
		l_pstmt.setString(3, pToDate);
		ResultSet l_RS = l_pstmt.executeQuery();

		while (l_RS.next()) {
			ret = l_RS.getInt("Count");
		}

		l_Query = "SELECT COUNT(*) AS [Count] FROM  AccountTransaction AS AT WHERE AccNumber = ?"
				+ " AND EffectiveDate>=? AND EffectiveDate<=? AND TransType >= 500 AND Status < 254 ";
		l_pstmt = pConn.prepareStatement(l_Query);
		l_pstmt.setString(1, pAccNo);
		l_pstmt.setString(2, pFromDate);
		l_pstmt.setString(3, pToDate);
		l_RS = l_pstmt.executeQuery();

		while (l_RS.next()) {
			ret += l_RS.getInt("Count");
		}

		l_pstmt.close();
		return ret;
	}

	public int getTotalTransCountPerDay(String pAccNumber, String pEffectiveDate, int pType, int pDateType,
			Connection conn) throws SQLException {
		int result = 0;
		String l_TransTypeList = "";
		if (pType == 1) { // ATM
			l_TransTypeList = "590,965,966,969";// On Us, On Us IBT, Off Us
												// Issuer, IBT Charges
												// This case does not include
												// Card Account to Card Account
												// Transfer.
		} else if (pType == 2) { // POS
			l_TransTypeList = "959"; // POS Sales

		} else if (pType == 3) {
			l_TransTypeList = "820,821,822,823,824,825";
			// Mobile Dr Tr, Mobile IBT Dr Tr, Mobile Remit Drawing Dr Tr,
			// Mobile Remit Encash Dr Tr, Mobile Bill Dr Tr, NFC Dr Tr
		} else if (pType == 5) {
			l_TransTypeList = "827";
			// Mobile Beer
		} else {
			l_TransTypeList = "";
		}
		StringBuffer l_Query = new StringBuffer(
				"SELECT COUNT(Amount) FROM ( SELECT * FROM AccountTransaction WHERE AccNumber = ? ");
		if (pDateType == 0) {
			l_Query.append("AND EffectiveDate = ? ");
		} else {
			l_Query.append("AND TransDate = ? ");
		}

		l_Query.append("AND TransType IN (" + l_TransTypeList + ") AND Status<254 UNION ");
		l_Query.append("SELECT * FROM AccountTransactionOld WHERE AccNumber = ? ");
		if (pDateType == 0) {
			l_Query.append("AND EffectiveDate = ? ");
		} else {
			l_Query.append("AND TransDate = ? ");
		}
		l_Query.append("AND TransType IN (" + l_TransTypeList + ") AND Status<254 ) AT ");

		PreparedStatement pstmt = conn.prepareStatement(l_Query.toString());

		pstmt.setString(1, pAccNumber);
		pstmt.setString(2, pEffectiveDate);
		pstmt.setString(3, pAccNumber);
		pstmt.setString(4, pEffectiveDate);

		ResultSet rs = pstmt.executeQuery();

		while (rs.next()) {
			result = rs.getInt(1);
		}
		pstmt.close();

		return result;
	}

	public ArrayList<TransactionData> getTransactionDataList() {
		return lstTransData;
	}

	public double getTransAmountPerDayICBS(String fromAccount, String userid, String transdate, Connection conn)
			throws SQLException {
		double ret = 0.00;
		String sql = "select sum(Amount) as amount from " + mTableName + " where accnumber = ? and transdate = ? ";
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setString(i++, fromAccount);
		ps.setString(i++, transdate);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ret = rs.getDouble(1);
		}
		ps.close();
		rs.close();
		return ret;
	}

	public TransactionViewData[] miniStatement(String accNumber, String fromDate, String toDate, Connection conn)
			throws SQLException {
		ArrayList<TransactionViewData> arrList = new ArrayList<TransactionViewData>();
		String sql = "select * from (select transRef,accNumber,amount,FORMAT(transTime , 'dd MMM yyyy HH:mm') transTime,currencyCode, "
				+ "(case when transType>500 then 'Dr' "
				+ "    when transType<500 then 'Cr' end) as drcr,remark as transDesc ,supervisorID, transNo "
				+ "from AccountTransaction " + "where transDate between '" + fromDate + "' and '" + toDate + "' "
				+ "and accNumber= '" + accNumber + "' and status not in (254,255) " + "union all "
				+ "select transRef, accNumber,amount,FORMAT(transTime , 'dd MMM yyyy HH:mm') transTime,currencyCode, "
				+ "(case when transType>500 then 'Dr' "
				+ "     when transType<500 then 'Cr' end) as drcr,remark as transDesc,supervisorID, transNo " + "from "
				+ mTableNameOther + " where transDate between '" + fromDate + "' and '" + toDate + "' "
				+ "and accNumber='" + accNumber + "' and status not in (254,255)) as t order by t.transNo desc";
		PreparedStatement ps = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE,
				ResultSet.CONCUR_READ_ONLY);
		ResultSet rs = ps.executeQuery();
		int index = 0;
		rs.last();
		int size = rs.getRow();
		rs.beforeFirst();
		TransactionViewData[] dataList = new TransactionViewData[size];
		while (rs.next()) {
			int i = 1;
			TransactionViewData data = new TransactionViewData();
			data.setSerialNo(rs.getRow());
			data.setTransRef(rs.getInt(i++));
			data.setAccNumber(rs.getString(i++));
			data.setAmount(rs.getDouble(i++));
			data.setTransDateTime(rs.getString(i++));
			data.setCurrencyCode(rs.getString(i++));
			data.setDrcr(rs.getString(i++));
			data.setDescription(rs.getString(i++));
			data.setMerchantID(rs.getString(i++));
			arrList.add(data);
			dataList[index] = data;
			index++;
		}

		ps.close();
		rs.close();
		return dataList;
	}

	private AccountTransactionData readRecord(ResultSet rs) throws SQLException {
		AccountTransactionData data = new AccountTransactionData();
		int i = 1;
		data.setTransNo(rs.getInt(i++));
		data.setBranchCode(rs.getString(i++));
		data.setWorkStation(rs.getString(i++));
		data.setTransRef(rs.getInt(i++));
		data.setSerialNo(rs.getInt(i++));
		data.setTellerId(rs.getString(i++));
		data.setSupervisorId(rs.getString(i++));
		data.setTransTime(rs.getString(i++));
		data.setTransDate(rs.getString(i++));
		data.setDescription(rs.getString(i++));
		data.setChequeNo(rs.getString(i++));
		data.setCurrencyCode(rs.getString(i++));
		data.setCurrencyRate(rs.getInt(i++));
		data.setAmount(rs.getDouble(i++));
		data.setTransType(rs.getInt(i++));
		data.setAccNumber(rs.getString(i++));
		data.setPrevBalance(rs.getDouble(i++));
		data.setPrevUpdate(rs.getString(i++));
		data.setEffectiveDate(rs.getString(i++));
		data.setContraDate(rs.getString(i++));
		data.setStatus(rs.getInt(i++));
		data.setAccRef(rs.getString(i++));
		data.setRemark(rs.getString(i++));
		data.setSystemCode(rs.getInt(i++));
		data.setSubRef(rs.getString(i++));
		return data;
	}

	private void readRecord(TransactionData pTransData, ResultSet aRS) {
		try {
			pTransData.setTransactionNumber(aRS.getInt("TransNo"));
			pTransData.setBranchCode(aRS.getString("BranchCode"));
			pTransData.setWorkstation(aRS.getString("WorkStation"));
			pTransData.setTransactionReference(aRS.getLong("TransRef"));
			pTransData.setSerial(aRS.getInt("SerialNo"));
			pTransData.setUserID(aRS.getString("TellerId"));
			pTransData.setAuthorizerID(aRS.getString("SupervisorId"));
			pTransData.setTransactionTime(SharedUtil.formatDBDateTime2MIT(aRS.getString("TransTime")));
			pTransData.setTransactionDate(SharedUtil.formatDBDate2MIT(aRS.getString("TransDate")));
			pTransData.setDescription(aRS.getString("Description"));
			pTransData.setReferenceNumber(aRS.getString("ChequeNo"));
			pTransData.setCurrencyCode(aRS.getString("CurrencyCode"));
			pTransData.setCurrencyRate(aRS.getDouble("CurrencyRate"));
			pTransData.setAmount(aRS.getDouble("Amount"));
			pTransData.setTransactionType(aRS.getInt("TransType"));
			pTransData.setAccountNumber(aRS.getString("AccNumber"));
			pTransData.setPreviousBalance(aRS.getDouble("PrevBalance"));
			pTransData.setPreviousDate(SharedUtil.formatDBDate2MIT(aRS.getString("PrevUpdate")));
			pTransData.setEffectiveDate(SharedUtil.formatDBDate2MIT(aRS.getString("EffectiveDate")));
			pTransData.setContraDate(SharedUtil.formatDBDate2MIT(aRS.getString("ContraDate")));
			pTransData.setStatus(aRS.getInt("Status"));
			pTransData.setAccRef(aRS.getString("AccRef"));
			pTransData.setRemark(aRS.getString("remark"));
			pTransData.setSystemCode(aRS.getInt("SystemCode"));
			pTransData.setSubRef(aRS.getString("SubRef"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public TransactionViewData[] rejectPaymentList(String accNumber, Connection conn) throws SQLException {
		ArrayList<TransactionViewData> arrList = new ArrayList<TransactionViewData>();
		String sql = "select transRef,accNumber,amount,transTime,currencyCode , (case when transType<500 then 'Debit'  "
				+ " when transType>500 then 'Credit' end) as drcr,remark as transDesc from " + mTableName
				+ " where accNumber ='" + accNumber + "' and  Remark like 'Payment with%' "
				+ " and transRef not in ( select subRef from accountTransaction  where accNumber ='3898001' "
				+ " and transType =705 and Remark like 'Receipt with%' ) "
				+ " union all select transRef,accNumber,amount,transTime,currencyCode , (case when transType<500 then 'Debit'  "
				+ " when transType>500 then 'Credit' end) as drcr,remark as transDesc from " + mTableNameOther
				+ " where accNumber ='" + accNumber + "' and  Remark like 'Payment with%' "
				+ " and transRef not in ( select subRef from accountTransaction  where accNumber ='3898001' "
				+ " and transType =705 and Remark like 'Receipt with%' )";
		PreparedStatement ps = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE,
				ResultSet.CONCUR_READ_ONLY);
		ResultSet rs = ps.executeQuery();
		int index = 0;
		rs.last();
		int size = rs.getRow();
		rs.beforeFirst();
		TransactionViewData[] dataList = new TransactionViewData[size];
		while (rs.next()) {
			TransactionViewData data = new TransactionViewData();
			int i = 1;
			data.setTransRef(rs.getInt(i++));
			data.setAccNumber(rs.getString(i++));
			data.setAmount(rs.getDouble(i++));
			data.setTransDateTime(rs.getString(i++));
			data.setCurrencyCode(rs.getString(i++));
			data.setDrcr(rs.getString(i++));
			data.setDescription(rs.getString(i++));
			data.setSerialNo(rs.getRow());
			arrList.add(data);
			dataList[index] = data;
			index++;
		}

		ps.close();
		rs.close();
		return dataList;
	}

	public boolean save(ArrayList<AccountTransactionData> data, Connection conn) throws SQLException {
		boolean ret = false;
		String sql = "INSERT INTO " + mTableName + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement ps = conn.prepareStatement(sql);
		if (data.size() > 0) {
			for (int i = 0; i < data.size(); i++) {
				updateRecord(data.get(i), ps);
				ps.addBatch();
			}
		}
		int i[] = ps.executeBatch();
		if (i.length > 0) {
			ret = true;
		}
		ps.close();
		return ret;
	}

	public TransactionViewData[] transListByID(String accRef, String fromDate, String toDate, Connection conn)
			throws SQLException {
		ArrayList<TransactionViewData> arrList = new ArrayList<TransactionViewData>();
		String sql = "select * from (select transRef,accNumber,amount,transTime,currencyCode, "
				+ "(case when transType>500 then 'Debit' "
				+ "    when transType<500 then 'Credit' end) as drcr,remark as transDesc , transNo "
				+ "from AccountTransaction " + "where transDate between '" + fromDate + "' and '" + toDate + "' "
				+ "and accRef= '" + accRef + "' and status not in (254,255) " + "union all "
				+ "select transRef, accNumber,amount,transTime,currencyCode, "
				+ "(case when transType>500 then 'Debit' "
				+ "     when transType<500 then 'Credit' end) as drcr,remark as transDesc, transNo " + "from "
				+ mTableNameOther + " where transDate between '" + fromDate + "' and '" + toDate + "' " + "and accRef='"
				+ accRef + "' and status not in (254,255)) as t order by t.transNo desc";
		PreparedStatement ps = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE,
				ResultSet.CONCUR_READ_ONLY);
		ResultSet rs = ps.executeQuery();
		int index = 0;
		rs.last();
		int size = rs.getRow();
		rs.beforeFirst();
		TransactionViewData[] dataList = new TransactionViewData[size];
		while (rs.next()) {
			TransactionViewData data = new TransactionViewData();
			int i = 1;
			data.setTransRef(rs.getInt(i++));
			data.setAccNumber(rs.getString(i++));
			data.setAmount(rs.getDouble(i++));
			data.setTransDateTime(rs.getString(i++));
			data.setCurrencyCode(rs.getString(i++));
			data.setDrcr(rs.getString(i++));
			data.setDescription(rs.getString(i++));
			data.setSerialNo(rs.getRow());
			arrList.add(data);
			dataList[index] = data;
			index++;
		}

		ps.close();
		rs.close();
		return dataList;
	}

	private void updateRecord(AccountTransactionData data, PreparedStatement ps) throws SQLException {
		int i = 1;
		ps.setString(i++, data.getBranchCode());
		ps.setString(i++, data.getWorkStation());
		ps.setInt(i++, data.getTransRef());
		ps.setInt(i++, data.getSerialNo());
		ps.setString(i++, data.getTellerId());
		ps.setString(i++, data.getSupervisorId());
		ps.setString(i++, data.getTransTime());
		ps.setString(i++, data.getTransDate());
		ps.setString(i++, data.getDescription());
		ps.setString(i++, data.getChequeNo());
		ps.setString(i++, data.getCurrencyCode());
		ps.setInt(i++, data.getCurrencyRate());
		ps.setDouble(i++, data.getAmount());
		ps.setInt(i++, data.getTransType());
		ps.setString(i++, data.getAccNumber());
		ps.setDouble(i++, data.getPrevBalance());
		ps.setString(i++, data.getPrevUpdate());
		ps.setString(i++, data.getEffectiveDate());
		ps.setString(i++, data.getContraDate());
		ps.setInt(i++, data.getStatus());
		ps.setString(i++, data.getAccRef());
		ps.setString(i++, data.getRemark());
		ps.setInt(i++, data.getSystemCode());
		ps.setString(i++, data.getSubRef());
	}

	private void updateRecord(TransactionData pTransData, PreparedStatement aPS) throws SQLException {

		// aPS.setLong(1, pTransData.getTransactionNumber());
		aPS.setString(1, pTransData.getBranchCode() == null ? "001" : pTransData.getBranchCode());
		aPS.setString(2, pTransData.getWorkstation() == null ? "001" : pTransData.getWorkstation());
		aPS.setLong(3, pTransData.getTransactionReference());
		aPS.setInt(4, pTransData.getSerial());
		aPS.setString(5, pTransData.getUserID());
		aPS.setString(6, pTransData.getAuthorizerID());
		aPS.setString(7, pTransData.getTransactionTime() == null ? "12:00:00" : pTransData.getTransactionTime());
		aPS.setString(8, pTransData.getTransactionDate() == null ? SharedUtil.getCurrentDate()
				: pTransData.getTransactionDate());
		aPS.setString(9, pTransData.getDescription() == null ? "" : pTransData.getDescription());
		aPS.setString(10, pTransData.getReferenceNumber() == null ? "" : pTransData.getReferenceNumber());
		aPS.setString(11, pTransData.getCurrencyCode() == null ? "MMK" : pTransData.getCurrencyCode());
		aPS.setDouble(12, pTransData.getCurrencyRate() == 0 ? 1 : pTransData.getCurrencyRate());
		aPS.setDouble(13, pTransData.getAmount());
		aPS.setInt(14, pTransData.getTransactionType());
		aPS.setString(15, pTransData.getAccountNumber());
		aPS.setDouble(16, pTransData.getPreviousBalance());
		aPS.setString(17,
				pTransData.getPreviousDate() == null ? SharedUtil.getCurrentDate() : pTransData.getPreviousDate());
		aPS.setString(18,
				pTransData.getEffectiveDate() == null ? SharedUtil.getCurrentDate() : pTransData.getEffectiveDate());
		aPS.setString(19, pTransData.getContraDate() == null ? "01/01/1900" : pTransData.getContraDate());
		aPS.setInt(20, pTransData.getStatus());
		aPS.setString(21, pTransData.getAccRef() == null ? "" : pTransData.getAccRef());
		aPS.setString(22, pTransData.getRemark() == null ? "" : pTransData.getRemark());
		aPS.setInt(23, pTransData.getSystemCode());
		aPS.setString(24, pTransData.getSubRef() == null ? "" : pTransData.getSubRef());

	}

	public boolean updateStatusByTransRef(int transRef, Connection conn) throws SQLException {
		boolean ret = false;
		String sql = "update " + mTableName + " set status = 254 where TransRef = ? and status = 1";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, transRef);
		if (ps.executeUpdate() > 0) {
			ret = true;
		}
		ps.close();
		return ret;
	}

	public boolean updateTransRef(int ltobefltRef, long ltobeupdatedRef, Connection conn) throws SQLException {
		boolean ret = false;

		PreparedStatement pstmt = null;
		pstmt = conn.prepareStatement("UPDATE AccountTransaction SET " + "TransRef =? " + " WHERE TransRef=?");

		pstmt.setLong(1, ltobeupdatedRef);
		pstmt.setInt(2, ltobefltRef);
		pstmt.setQueryTimeout(DAOManager.getTransTime());
		if (pstmt.executeUpdate() > 0) {
			ret = true;
		}
		pstmt.close();

		return ret;
	}

	public boolean updateTransRefByTransNo(int transNo, String subRef, Connection conn) throws SQLException {
		boolean ret = false;
		String sql = "update " + mTableName + " set TransRef = ?,subRef=? where transNo = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setInt(i++, transNo);
		ps.setString(i++, subRef);
		ps.setInt(i++, transNo);
		if (ps.executeUpdate() > 0) {
			ret = true;
		}
		ps.close();
		return ret;
	}

}
