package com.nirvasoft.rp.dao.icbs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.rp.dao.DAOManager;
import com.nirvasoft.rp.shared.icbs.ProductFeatureData;

public class ProductFeatureDAO {
	private String mTableName = "T00001";
	private ProductFeatureData mProductFeatureData;

	public boolean deleteProductFeature(String aProductId, Connection aConn) throws SQLException {
		boolean l_result = false;
		int i = 1;

		String l_query = "DELETE " + mTableName + " WHERE T1 = ? ";

		PreparedStatement l_pstmt = aConn.prepareStatement(l_query);
		l_pstmt.setString(i++, aProductId);
		l_pstmt.setQueryTimeout(DAOManager.getNormalTime());
		if (l_pstmt.executeUpdate() > 0)
			l_result = true;

		return l_result;
	}

	public ArrayList<ProductFeatureData> getProdcutFeatures(String pProductId, Connection pConn) {
		ArrayList<ProductFeatureData> ret = new ArrayList<ProductFeatureData>();
		try {
			String l_Query = "SELECT SYSKEY, T1, T2, T3, T4, T5, N1, N2, N3, N4, N5 FROM " + mTableName
					+ " WHERE 1=1 AND T1 = ?";

			PreparedStatement pstmt = pConn.prepareStatement(l_Query);
			pstmt.setString(1, pProductId);

			ResultSet l_Rs = pstmt.executeQuery();
			while (l_Rs.next()) {
				ProductFeatureData object = new ProductFeatureData();
				readRecord(object, l_Rs);
				ret.add(object);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return ret;
	}

	public ProductFeatureData getProductFeature() {
		return mProductFeatureData;
	}

	private void readRecord(ProductFeatureData pProductFeatureData, ResultSet pRs) throws SQLException {
		pProductFeatureData.setSysKey(pRs.getLong("SysKey"));
		pProductFeatureData.setProductId(pRs.getString("T1"));
		pProductFeatureData.setFeatureId(pRs.getLong("N1"));
		pProductFeatureData.setFeatureValue(pRs.getLong("N2"));
		pProductFeatureData.setFeatureDescription(pRs.getString("T2"));
		pProductFeatureData.setExpression(pRs.getString("T3"));
		pProductFeatureData.setGLCode(pRs.getString("T4"));
	}

	public void setProductFeature(ProductFeatureData p) {
		mProductFeatureData = p;
	}
}
