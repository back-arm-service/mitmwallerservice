package com.nirvasoft.rp.dao.icbs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.nirvasoft.rp.shared.icbs.ReferenceAccountsData;

public class ReferenceAccountsDAO {
	private ReferenceAccountsData ReferenceAccountsDataBean;

	private ReferenceAccountsData mReferenceAccountsData;

	public boolean getReferenceAccCode(String pCode, Connection conn) throws SQLException {
		boolean ret = false;
		PreparedStatement pstmt = null;

		pstmt = conn.prepareStatement("SELECT GLCode FROM ReferenceAccounts WHERE GLAccNumber = ? ");
		pstmt.setString(1, pCode);
		ResultSet rs = pstmt.executeQuery();
		if (rs.next()) {
			ReferenceAccountsDataBean = new ReferenceAccountsData();
			ReferenceAccountsDataBean.setGLCode(rs.getString(1));
			//readRecord(ReferenceAccountsDataBean, rs);
			ret = true;
		}
		pstmt.close();

		return ret;
	}
	public ReferenceAccountsData getReferenceAccountsData() {
		return mReferenceAccountsData;
	}
	public ReferenceAccountsData getReferenceAccountsDataBean() {
		return ReferenceAccountsDataBean;
	}
	
	private void readRecord(ReferenceAccountsData aReferenceAccountsDataBean, ResultSet aRS) throws SQLException {
		aReferenceAccountsDataBean.setFunctionID(aRS.getInt("FunctionID"));
		aReferenceAccountsDataBean.setDescription(aRS.getString("Description"));
		aReferenceAccountsDataBean.setGLCode(aRS.getString("GLCode"));
		aReferenceAccountsDataBean.setGLAccNumber(aRS.getString("GLAccNumber"));
	}
}
