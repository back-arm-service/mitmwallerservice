package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.nirvasoft.rp.shared.WCustomerData;
import com.nirvasoft.rp.util.GeneralUtil;

public class CustomerDao {

	public static String mTableName = "Customer";

	public String getLastID(Connection conn) throws Exception {
		String ret = "";
		String sql = "SELECT ISNULL(MAX(CAST(CustomerID AS INT)),0) FROM " + mTableName;
		PreparedStatement pstmt = conn.prepareStatement(sql);
		ResultSet rs = pstmt.executeQuery();
		int id = 0;
		while (rs.next()) {
			id = rs.getInt(1);
		}
		pstmt.close();
		rs.close();
		id += 1;
		ret = GeneralUtil.leadZeros("" + id, 10);
		return ret;
	}

	/*
	 * public String getLastIDByBranch(String pBranchCode, Connection conn)
	 * throws Exception{ String strKey = ""; String sql=
	 * "Select ISNULL(MAX(CAST(SubString(CustomerID,4,7) AS INT)),0) From "
	 * +mTableName+" Where SubString(CustomerID,1,3) = ? "; PreparedStatement
	 * pstmt=conn.prepareStatement(sql); pstmt.setString(1, pBranchCode);
	 * ResultSet rs=pstmt.executeQuery(); int id=0; while(rs.next()){
	 * id=rs.getInt(1); } pstmt.close(); rs.close(); id += 1; strKey
	 * =StrUtil.leadZeros(""+ id , 7); strKey = StrUtil.leadZeros(pBranchCode ,
	 * 3) + strKey; return strKey; }
	 */

	public boolean isExistCustomerByNrc(String nrc, Connection conn) throws SQLException {
		boolean ret = true;
		String sql = "select * from " + mTableName + " where NrcNo = ? ";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, nrc);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			ret = false;
		}
		ps.close();
		rs.close();
		return ret;
	}

	public boolean save(WCustomerData data, Connection conn) throws SQLException {
		boolean ret = false;
		String sql = "insert into " + mTableName
				+ " values(?,?,?,?,?,"
				+ "?,?,?,?,?"
				+ ",?,?,?,?,?"
				+ ",?,?,?,?,?"
				+ ",?,?,?,?,?"
				+ ",?,?,?,?,?"
				+ ",?,?,?,?,?"
				+ ",?,?,?,?,?)";
		PreparedStatement ps = conn.prepareStatement(sql);
		updateRecord(data, ps);
		if (ps.executeUpdate() > 0) {
			ret = true;
		}
		ps.close();
		return ret;
	}

	private void updateRecord(WCustomerData aData, PreparedStatement ps) throws SQLException {
		int index = 1;
		ps.setString(index++, aData.getCustomerID());//1
		ps.setInt(index++, aData.getCustomerType());//2
		ps.setString(index++, aData.getTitle());//3
		ps.setString(index++, aData.getName());//4
		ps.setString(index++, aData.getAliasName());//5
		ps.setInt(index++, aData.getSex());//6
		ps.setString(index++, aData.getDob());//7
		ps.setString(index++, aData.getNrcNo());//8
		ps.setString(index++, aData.getHouseNo());//9
		ps.setString(index++, aData.getStreet());//10
		ps.setString(index++, aData.getWard());//11
		ps.setString(index++, aData.getTownship());//12
		ps.setString(index++, aData.getCity());//13
		ps.setString(index++, aData.getDivision());//14
		ps.setString(index++, aData.getCountry());//15
		ps.setString(index++, aData.getPhone());//16
		ps.setString(index++, aData.getEmail());//17
		ps.setString(index++, aData.getFax());//18
		ps.setString(index++, aData.getPostalCode());//19
		if (aData.getStatus().equals("")) {
			aData.setStatus("Active");
		}
		ps.setString(index++, aData.getStatus());//20
		ps.setString(index++, aData.getOccupation());//21
		ps.setString(index++, aData.getFatherName());//22
		ps.setString(index++, aData.getUniversalId());//23
		ps.setInt(index++, aData.getmStatus());//24
		ps.setString(index++, aData.getOldNrcNo());//25
		ps.setString(index++, aData.getM1());//26
		ps.setString(index++, aData.getM2());//27
		ps.setString(index++, aData.getT1());//28
		ps.setString(index++, aData.getT2());//29
		ps.setString(index++, aData.getT3());//30
		ps.setString(index++, aData.getT4());//31
		ps.setString(index++, aData.getT5());//32
		ps.setString(index++, aData.getBcNo());//33
		ps.setInt(index++, aData.getN1());//34
		ps.setInt(index++, aData.getN2());//35
		ps.setInt(index++, aData.getN3());//36
		ps.setInt(index++, aData.getN4());//37
		ps.setInt(index++, aData.getN5());//38
		ps.setInt(index++, aData.getN6());//39
		ps.setString(index++, aData.getT6());//40
		// ps.setString(index++, aData.getT6());
	}
}
