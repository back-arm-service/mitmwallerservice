package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import com.nirvasoft.cms.shared.DivisionComboData;
import com.nirvasoft.cms.shared.DivisionComboDataSet;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;
import com.nirvasoft.rp.data.CityChannelData;
import com.nirvasoft.rp.data.CityStateComboDataSet;
import com.nirvasoft.rp.data.CityStateData;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.users.data.OTPReqData;
import com.nirvasoft.rp.users.data.UserData;
import com.nirvasoft.rp.users.data.UserRole;
import com.nirvasoft.rp.users.data.UserViewData;
import com.nirvasoft.rp.users.data.UserViewDataArr;
import com.nirvasoft.rp.users.data.UserViewDataset;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ServerUtil;

public class UserDao {
	public static boolean canDelete(long key, Connection conn) throws SQLException {

		ArrayList<Long> dbrs = new ArrayList<Long>();
		String sql = "Select n1 From JUN002_A Where n2=?";
		PreparedStatement stat = conn.prepareStatement(sql);
		stat.setLong(1, key);
		ResultSet result = stat.executeQuery();
		while (result.next()) {
			dbrs.add(result.getLong("n1"));

		}
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("UVM005_A");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("CreatedDate", (byte) 5));
		ret.getFields().add(new DBField("ModifiedDate", (byte) 5));
		ret.getFields().add(new DBField("UserId", (byte) 5));
		ret.getFields().add(new DBField("UserName", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 1));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 1));
		ret.getFields().add(new DBField("n6", (byte) 1));
		ret.getFields().add(new DBField("n7", (byte) 1));
		ret.getFields().add(new DBField("n8", (byte) 2));

		return ret;
	}

	public static DBRecord define(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 1));
		ret.getFields().add(new DBField("n4", (byte) 1));
		ret.getFields().add(new DBField("n5", (byte) 2));

		return ret;
	}

	public static DBRecord defineView() {
		DBRecord ret = new DBRecord();
		ret.setTableName("V_U001_A");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("autokey", (byte) 2));
		ret.getFields().add(new DBField("CreatedDate", (byte) 5));
		ret.getFields().add(new DBField("ModifiedDate", (byte) 5));
		ret.getFields().add(new DBField("UserId", (byte) 5));
		ret.getFields().add(new DBField("UserName", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		// ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		// ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("UN", (byte) 5));
		ret.getFields().add(new DBField("lock", (byte) 1));
		ret.getFields().add(new DBField("role", (byte) 1));
		return ret;
	}

	public static Result delete(long syskey, String modifieduserId, Connection conn) throws SQLException {
		Result res = new Result();
		String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
		boolean candelete = true;
		String userId = "";
		String sql1 = "SELECT Mobileuserid from mobilecajunction where Mobileuserid in "
				+ "(SELECT T1 FROM UVM005_A a WHERE Syskey=? and a.recordstatus<>4)";
		PreparedStatement stmt1 = conn.prepareStatement(sql1);
		// int rs1= stmt1.executeUpdate();
		stmt1.setLong(1, syskey);
		ResultSet rs1 = stmt1.executeQuery();
		while (rs1.next()) {
			candelete = false;

		}
		if (candelete) {

			res = PersonDao.deletePerson(syskey, modifieduserId, conn);

			if (res.isState()) {

				String s_sql = "SELECT T1 FROM UVM005_A a WHERE Syskey=? and a.recordstatus<>4";
				PreparedStatement sttmt = conn.prepareStatement(s_sql);
				sttmt.setLong(1, syskey);
				ResultSet result = sttmt.executeQuery();
				while (result.next()) {
					res.setUserid(result.getString("T1"));
					userId = result.getString("T1");
				}

				String sql = "delete From JUN002_A Where n1= ?";
				PreparedStatement stmt = conn.prepareStatement(sql);
				stmt.setLong(1, syskey);
				stmt.executeUpdate();

				sql = "UPDATE UVM005_A SET RecordStatus=4,modifieddate=? WHERE Syskey=?";
				stmt = conn.prepareStatement(sql);
				int j = 1;
				stmt.setString(j++, date);
				stmt.setLong(j++, syskey);
				int rs = stmt.executeUpdate();
				if (rs > 0) {
					String delsql = "DELETE FROM ForceChangePwd WHERE userId =?";
					PreparedStatement pstmt = conn.prepareStatement(delsql);
					pstmt.setString(1, userId);
					pstmt.executeUpdate();

					res.setState(true);
					res.setMsgDesc("Deleted Successfully");
				} else {
					res.setState(false);
					res.setMsgDesc("Deleted Fail");
				}
			}
		} else {

			res.setState(false);
			res.setMsgDesc("Cannot delete!");
		}

		return res;
	}

	// generate password
	public static String generatePassword() {

		String pwd = "";
		int pwdlength = 6;
		char ch;
		String num_list = "0123456789";
		String char_list = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String specialchar_list = "!#$%&*?@";
		// String specialchar_list = "!@#$%^&*()+`~";
		String list = "!#$%&*?@abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		StringBuffer randStr = new StringBuffer();

		for (int i = 0; i < pwdlength; i++) {
			Random randomGenerator = new Random();
			int number = 0;
			if (i == 0) {
				number = randomGenerator.nextInt(char_list.length());
				ch = char_list.charAt(number);
			} else if (i == 1) {
				number = randomGenerator.nextInt(specialchar_list.length());
				ch = specialchar_list.charAt(number);
			} else if (i == 2) {
				number = randomGenerator.nextInt(num_list.length());
				ch = num_list.charAt(number);
			} else {
				number = randomGenerator.nextInt(list.length());
				ch = list.charAt(number);
			}
			randStr.append(ch);
		}
		pwd = String.valueOf(randStr);
		return pwd;

	}

	// ///////////////////////////////////////////////////////////////
	public static UserViewDataset getAllUCJunctionData(String searchtext, Connection conn) throws SQLException {
		String searchText = searchtext.replace("'", "''");
		ArrayList<UserViewData> ret = new ArrayList<UserViewData>();
		String whereClause = "";
		if (searchText.equals("") || searchText.equals(null)) {
			whereClause = " where a.RecordStatus<>4 and a.n2=1 and a.n7<>11 and b.RecordStatus<>21";
		} else {
			whereClause = " where a.RecordStatus<>4 and a.n2=1 and a.n7<>11 and b.RecordStatus<>21  and "
					+ "(a.t1 like ? or a.t7 like ? or b.t2 like ?)";
		}
		String l_Query = "select b.t2 'UserName',a.t1,a.t7 from UVM005_A a Join UVM012_A b ON a.n4=b.syskey"
				+ whereClause;

		l_Query += " order by a.t1,b.t2,a.t7";

		PreparedStatement pstmt = conn.prepareStatement(l_Query);
		if (searchText.equals("") || searchText.equals(null)) {
		} else {
			for (int i = 1; i < 4; i++) {
				pstmt.setString(i, "%" + searchText + "%");
			}
		}

		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			UserViewData data = new UserViewData();
			{
				data.setT1(rs.getString("t1"));
				data.setUsername(rs.getString("UserName"));
				data.setT7(rs.getString("t7"));
				ret.add(data);
			}
		}
		UserViewDataset dataSet = new UserViewDataset();
		dataSet.setArlData(ret);

		return dataSet;

	}

	public static UserViewDataset getAllUserCIFDataList(String searchtext, Connection conn) throws SQLException {

		// new
		String searchText = searchtext.replace("'", "''");
		ArrayList<UserViewData> ret = new ArrayList<UserViewData>();
		String whereClause = "";
		if (searchText.equals("")) {
			whereClause = "where u.RecordStatus<>4 and m.mobileUserID = u.t1";
		} else {
			whereClause = "where (u.RecordStatus<>4 and m.mobileUserID = u.t1 and m.mobileUserID like ?) "
					+ "or (u.RecordStatus<>4 and m.mobileUserID = u.t1 and u.t2 like ?)";
		}

		String l_Query = "select distinct u.t1 as userid,u.t2 as username from MobileCAJunction m , UVM012_A u "
				+ whereClause;

		l_Query += " order by u.t1,u.t2";

		PreparedStatement pstmt = conn.prepareStatement(l_Query);
		if (searchText.equals("") || searchText.equals(null)) {
		} else {
			for (int i = 1; i < 3; i++) {
				pstmt.setString(i, "%" + searchText + "%");
			}
		}
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			UserViewData data = new UserViewData();
			{
				data.setT1(rs.getString("userid"));
				data.setUserName(rs.getString("username"));
				ret.add(data);
			}

		}
		UserViewDataset dataSet = new UserViewDataset();
		dataSet.setArlData(ret);
		return dataSet;
	}

	public static UserData getDBRecord(DBRecord adbr) {
		UserData ret = new UserData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("CreatedDate"));
		ret.setModifiedDate(adbr.getString("ModifiedDate"));
		ret.setUserId(adbr.getString("UserId"));
		ret.setUserName(adbr.getString("UserName"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));

		ret.setUsersyskey(adbr.getLong("usersysKey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(ServerUtil.decryptPIN(adbr.getString("t2")));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		// ret.setN1(adbr.getLong("n1"));
		// ret.setN2(adbr.getInt("n2"));
		// ret.setN3(adbr.getLong("n3"));
		// ret.setN4(adbr.getLong("n4"));
		// ret.setN5(adbr.getInt("n5"));
		// ret.setN6(adbr.getInt("n6"));
		// ret.setN7(adbr.getInt("n7"));
		// ret.setN8(adbr.getLong("n8"));

		return ret;
	}

	public static UserRole getDBRecords(DBRecord adbr) {
		UserRole ret = new UserRole();
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));

		ret.setUsersyskey(adbr.getLong("usersysKey"));

		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getInt("n3"));
		ret.setN4(adbr.getInt("n4"));
		ret.setN5(adbr.getInt("n5"));

		return ret;
	}

	public static UserViewData getDBViewRecord(DBRecord adbr) {
		UserViewData ret = new UserViewData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setAutokey(adbr.getLong("autokey"));
		ret.setCreatedDate(adbr.getString("CreatedDate"));
		ret.setModifiedDate(adbr.getString("ModifiedDate"));
		ret.setUserId(adbr.getString("UserId"));
		ret.setUserName(adbr.getString("UserName"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setUsersyskey(adbr.getLong("usersysKey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setUsername(adbr.getString("UN"));
		// ret.setN7(adbr.getInt("lock"));
		// ret.setN2(adbr.getInt("role"));
		return ret;
	}

	public static long getPersonSyskey(long usys, Connection con) throws SQLException {

		String sql = "SELECT * FROM UVM005_A WHERE syskey=?";
		PreparedStatement stat = con.prepareStatement(sql);
		stat.setLong(1, usys);
		ResultSet result = stat.executeQuery();
		long u = 0;
		while (result.next()) {
			u = result.getLong("n4");

		}

		return u;

	}

	public static Result insert(boolean isUserProfile, UserData obj, Connection conn) throws SQLException {
		Result res = new Result();
		if (!isCodeExist(obj, conn)) {
			try {
				String sql = DBMgr.insertString(define(), conn);
				PreparedStatement stmt = conn.prepareStatement(sql);
				DBRecord dbr = setDBRecord(obj);
				DBMgr.setValues(stmt, dbr);
				int rs = stmt.executeUpdate();

				if (rs > 0) {

					if (!isUserProfile) {
						UserRole jun = new UserRole();
						for (long l : obj.getRolesyskey()) {

							if (l != 0) {

								jun.setRecordStatus(obj.getRecordStatus());
								jun.setSyncBatch(obj.getSyncBatch());
								jun.setSyncStatus(obj.getSyncStatus());
								jun.setUsersyskey(obj.getSyskey());
								jun.setN1(obj.getSyskey());
								jun.setN2(l);
								res = insertUserRole(jun, conn);

							}

						}

						if (res.isState()) {

							res.setState(true);
							res.setMsgDesc("Saved Successfully");

						} else
							res.setMsgDesc("Cannot Save");
					} else {
						String query = "INSERT INTO ForceChangePwd(createddate,userId,password)  VALUES(?,?,?)";
						PreparedStatement stmt1 = conn.prepareStatement(query);
						int i = 1;
						stmt1.setString(i++, obj.getCreatedDate());
						stmt1.setString(i++, obj.getT1());
						stmt1.setString(i++, "00");
						int rst = stmt1.executeUpdate();
						if (rst > 0) {
							res.setState(true);
							res.setMsgDesc("Saved Successfully");
							res.setLoginID(obj.getT1());
							res.setPhNo(obj.getT4());
						} else {
							res.setState(false);
							res.setMsgDesc("Save Fail");
						}

					}

				} else
					res.setMsgDesc("Cannot Save");

			}

			catch (Exception e) {
				res.setMsgDesc("Cannot Save");
				e.printStackTrace();
			}

		} else
			res.setMsgDesc("Data already exist");

		return res;
	}

	public static Result insertUserRole(UserRole obj, Connection conn) throws SQLException {
		Result res = new Result();
		String sql = DBMgr.insertString(define("JUN002_A"), conn);
		PreparedStatement stmt = conn.prepareStatement(sql);
		DBRecord dbr = setDBRecord(obj);
		DBMgr.setValues(stmt, dbr);
		int rs = stmt.executeUpdate();

		if (rs > 0) {

			res.setState(true);
		}

		return res;
	}

	// MMPPM
	public static boolean isCodeExist(UserData obj, Connection conn) throws SQLException {

		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				" WHERE RecordStatus<>4 AND syskey <> " + obj.getSyskey() + " AND T1=\'" + obj.getT1() + "\'", "",
				conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}

	}

	public static UserData readBranchUserDataBySyskey(long syskey, Connection conn) throws SQLException {

		UserData ret = new UserData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "WHERE RecordStatus<>4 AND syskey=" + syskey, "", conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));

		return ret;

	}

	public static DBRecord setDBRecord(UserData data) {
		DBRecord ret = define();
		ret.setValue("syskey", data.getSyskey());
		ret.setValue("CreatedDate", data.getCreatedDate());
		ret.setValue("ModifiedDate", data.getModifiedDate());
		ret.setValue("UserId", data.getUserId());
		ret.setValue("UserName", data.getUserName());
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());

		ret.setValue("usersysKey", data.getUsersyskey());
		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t3", data.getT3());
		ret.setValue("t5", data.getT5());
		ret.setValue("t6", data.getT6());
		ret.setValue("t7", data.getT7());
		// ret.setValue("n1", data.getN1());
		// ret.setValue("n2", data.getN2());
		// ret.setValue("n3", data.getN3());
		// ret.setValue("n4", data.getN4());
		// ret.setValue("n5", data.getN5());
		// ret.setValue("n6", data.getN6());
		// ret.setValue("n7", data.getN7());
		// ret.setValue("n8", data.getN8());
		return ret;
	}

	public static DBRecord setDBRecord(UserRole data) {
		DBRecord ret = define("JUN002_A");
		ret.setValue("RecordStatus", data.getRecordStatus());
		ret.setValue("SyncStatus", data.getSyncStatus());
		ret.setValue("SyncBatch", data.getSyncBatch());
		ret.setValue("usersysKey", data.getUsersyskey());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		return ret;
	}

	public static Result update(boolean isUserProfile, UserData obj, Connection conn) throws SQLException {
		Result res = new Result();
		String userPwd = "";

		if (ServerUtil.decryptPIN(obj.getT2()).equals("")) // for immediately
															// update data after
															// saving
		{
			String userid = obj.getT1();
			String query = "Select t2 from UVM005_A where recordstatus <> 4 and t1=?";
			PreparedStatement stmt1 = conn.prepareStatement(query);
			stmt1.setString(1, userid);
			ResultSet result = stmt1.executeQuery();
			boolean flag = false;
			while (result.next()) {
				userPwd = result.getString("t2");
				obj.setT2(userPwd);
				flag = true;
			}
			if (!flag) {
				res.setState(false);
				res.setMsgDesc("Cannot Update");
			}
		}

		// String sql = DBMgr.updateString(" WHERE RecordStatus<>4 AND Syskey="
		// + obj.getSyskey(), define(), conn);
		// PreparedStatement stmt = conn.prepareStatement(sql);
		// DBRecord dbr = setDBRecord(obj);
		// DBMgr.setValues(stmt, dbr);

		String sql = "UPDATE UVM005_A SET t1=? ,t2=? ,t3=? ,t4=? ,t5=?,t6=?,t7=?, "
				+ "modifieddate = ? Where RecordStatus<>4 AND Syskey=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		int i = 1;
		stmt.setString(i++, obj.getT1());
		stmt.setString(i++, obj.getT2());
		stmt.setString(i++, obj.getT3());
		stmt.setString(i++, obj.getT4());
		stmt.setString(i++, obj.getT5());
		stmt.setString(i++, obj.getT6());
		stmt.setString(i++, obj.getT7());
		stmt.setString(i++, GeneralUtil.datetoString());
		stmt.setLong(i++, obj.getSyskey());
		int rs = stmt.executeUpdate();

		if (rs > 0) {
			if (!isUserProfile) {
				UserRole jun = new UserRole();

				for (long l : obj.getRolesyskey()) {
					System.out.println("Rolesyskye :" + l);
					jun.setN1(obj.getSyskey());
					jun.setN2(l);

					sql = "DELETE FROM JUN002_A WHERE n1=?";
					stmt = conn.prepareStatement(sql);
					stmt.setLong(1, jun.getN1());
					stmt.executeUpdate();

				}

				for (long l : obj.getRolesyskey()) {

					if (l != 0) {

						jun.setRecordStatus(obj.getRecordStatus());
						jun.setSyncBatch(obj.getSyncBatch());
						jun.setSyncStatus(obj.getSyncStatus());
						jun.setUsersyskey(obj.getUsersyskey());
						jun.setN1(obj.getSyskey());
						jun.setN2(l);
						res = insertUserRole(jun, conn);

					}

				}

				if (res.isState()) {

					res.setState(true);
					res.setMsgDesc("Updated Successfully");

				} else
					res.setMsgDesc("Cannot Update");
			} else {

				res.setState(true);
				res.setMsgDesc("Updated Successfully");
				res.setLoginID(obj.getT1());
				res.setPhNo(obj.getT4());

			}
		}
		return res;
	}

	public Result activateDeactivateUser(long syskey, Connection conn, String status, String userId)
			throws SQLException {
		Result res = new Result();
		String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sql = "";
		int restatus = 0;
		if (status.equalsIgnoreCase("Activate")) {
			restatus = 2;
		} else if (status.equalsIgnoreCase("Deactivate")) {
			restatus = 21;
		}
		try {
			sql = "UPDATE UVM012_A SET RecordStatus=?,t5=?,modifieddate=? WHERE Syskey IN(SELECT N4 FROM UVM005_A WHERE RecordStatus <> 4 and Syskey=?)";

			PreparedStatement stmt = conn.prepareStatement(sql);
			int i = 1;
			stmt.setInt(i++, restatus);
			stmt.setString(i++, userId);
			stmt.setString(i++, date);
			stmt.setLong(i++, syskey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				sql = "UPDATE UVM005_A SET RecordStatus= ?,modifieddate=? WHERE RecordStatus <> 4 and Syskey=?";
				stmt = conn.prepareStatement(sql);
				int j = 1;
				stmt.setInt(j++, restatus);
				stmt.setString(j++, date);
				stmt.setLong(j++, syskey);
				rs = stmt.executeUpdate();
				if (rs > 0) {
					res.setState(true);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return res;
	}

	public boolean checkFirstTimeLogin(long syskey, Connection conn) {
		boolean result = false;
		String pwd = "";
		String query = "SELECT password FROM ForceChangePwd WHERE userId IN(SELECT t1 FROM UVM005_A WHERE recordStatus <> 4 and Syskey=?)";
		try {
			PreparedStatement stmt1 = conn.prepareStatement(query);
			int k = 1;
			stmt1.setLong(k++, syskey);
			ResultSet rs = stmt1.executeQuery();
			while (rs.next()) {
				pwd = rs.getString("password");
				if (pwd.equals("00")) {
					result = true;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean checkOtp(String userId, String phoneNo, Connection conn) throws SQLException {
		boolean result = false;

		String query = "select Top(1) T4 from UVM005_A Where RecordStatus <> 4 and t1 = ? and t4 = ? ";
		PreparedStatement ps = conn.prepareStatement(query);
		int i = 1;
		ps.setString(i++, userId);
		ps.setString(i++, phoneNo);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			result = true;
		}
		rs.close();
		ps.close();
		return result;
	}

	// MMPPM
	public ArrayList<String> getActiveAcctListByLoginUserID(String aLoginUserID, Connection conn) throws SQLException {
		ArrayList<String> ret = new ArrayList<String>();
		String sql = "select AccountNo from MobileCAJunction where MobileUserID = ? and n1 = ? ";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, aLoginUserID);
		ps.setInt(2, 1);// 1 is active
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			String aCustomerID = rs.getString("AccountNo");
			ret.add(aCustomerID);
		}
		ps.close();
		rs.close();
		return ret;
	}

	// get user setup list
	public UserViewDataset getAllUserData(String searchtext, Connection conn, String operation) throws SQLException {
		int recordStatus = 0, n7 = 0;
		String searchText = searchtext.replace("'", "''");
		String whereClause = "";
		if (operation.equalsIgnoreCase("alluserprofile")) {
			if (searchText.equals("")) {
				whereClause = "where RecordStatus<>4 and role = 1 ";
			} else {
				whereClause = "where RecordStatus<>4 and role = 1" + " and (" + " t1 like '%" + searchText
						+ "%' or UN like '%" + searchText + "%' )";
			}

		} else if (operation.equalsIgnoreCase("allbankuser")) {
			if (searchText.equals("")) {
				whereClause = "where RecordStatus<>4 and role = 2 ";
			} else {
				whereClause = "where RecordStatus<>4 and role = 2" + " and (" + " t1 like '%" + searchText
						+ "%' or UN like '%" + searchText + "%')";
			}
		} else if (operation.equalsIgnoreCase("activate")) {
			if (searchText.equals("")) {
				whereClause = " where (RecordStatus = 21 or RecordStatus = 1) and role = 1 ";
			} else {
				whereClause = " where ((RecordStatus = 21 or RecordStatus = 1) and role = 1)" + " and " + "(t1 like '%"
						+ searchText + "%' or UN like '%" + searchText + "%') ";
			}

		} else {
			if (operation.equalsIgnoreCase("lock")) {
				recordStatus = 2;
				n7 = 0;

			} else if (operation.equalsIgnoreCase("deactivate")) {
				recordStatus = 2;
				n7 = 0;

			} else if (operation.equalsIgnoreCase("unlock")) {
				recordStatus = 2;
				n7 = 11;

			}

			if (searchText.equals("")) {
				whereClause = "where RecordStatus =" + recordStatus + " and lock =" + n7 + "and role = 1";
			} else {
				whereClause = "where RecordStatus =" + recordStatus + " and lock =" + n7 + "and role = 1 and "
						+ "(t1 like '%" + searchText + "%' or UN like '%" + searchText + "%') ";
			}
		}

		ArrayList<UserViewData> ret = new ArrayList<UserViewData>();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(defineView(), whereClause, "ORDER BY t1,UN", conn);

		for (int i = 0; i < dbrs.size(); i++) {
			ret.add(getDBViewRecord(dbrs.get(i)));
		}

		UserViewDataset dataSet = new UserViewDataset();
		dataSet.setArlData(ret);
		return dataSet;
	}

	// for channelList
	public ArrayList<CityChannelData> getChannelListByUserIDWithAll(String userID, Connection conn) {
		// TODO Auto-generated method stub
		return null;
	}

	public CityStateComboDataSet getCityCombolist(String state, Connection conn) throws SQLException {

		CityStateComboDataSet dataset = new CityStateComboDataSet();
		ArrayList<CityStateData> datalist = new ArrayList<CityStateData>();

		String splitstate = "";
		if (!(state.equals("") || state.equalsIgnoreCase("null"))) {
			splitstate = state.substring(0, 4);

			String sql = "select code,DespEng,DespMyan from addressref where code like ? AND "
					+ "code <> ?  AND code <> '00000000' and DespEng <> 'All' ORDER BY Code";
			PreparedStatement stmt = conn.prepareStatement(sql);
			int i = 1;
			stmt.setString(i++, splitstate + "%");
			stmt.setString(i++, splitstate + "0000");
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				CityStateData combo = new CityStateData();
				combo.setDespEng(res.getString("DespEng"));
				combo.setCode(res.getString("code"));
				datalist.add(combo);
			}

			CityStateData[] dataarray = new CityStateData[datalist.size()];
			dataarray = datalist.toArray(dataarray);

			dataset.setData(dataarray);
		}

		return dataset;
	}

	public String getCustomerID(String aUserID, Connection conn) throws SQLException {
		String ret = "";
		String sql = "select t7 as CustomerID from UVM012_A where t1 = ? and recordstatus <> 4 ";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, aUserID);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ret = rs.getString("CustomerID");
		}
		ps.close();
		rs.close();
		return ret;
	}

	public String getlastlogintimefail(String userid, Connection con) {
		String lastlogintimefail = "";
		String sql = "SELECT * FROM tblSession  WHERE UserID = ? AND Status = 8 AND LogInDateTime=(SELECT MAX(LogInDateTime) FROM tblSession WHERE Status = 8 AND (UserID = ? COLLATE SQL_Latin1_General_CP1_CS_AS))";
		String date = "";
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			int i = 1;
			stmt.setString(i++, userid);
			stmt.setString(i++, userid);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				date = res.getString("LogInDateTime");
			}
			if (!date.equalsIgnoreCase("")) {
				lastlogintimefail = date.substring(8, 10) + "-" + date.substring(5, 7) + "-" + date.substring(0, 4)
						+ " " + date.substring(11, 19) + "";
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lastlogintimefail;
	}

	public String getlasttimelogin(String userid, Connection con) {
		String lasttimelogin = "";
		String time = "";
		// String sql = "SELECT * FROM LoginHistory WHERE UserID = '"+userid+"'
		// AND Time = (SELECT MAX(Time) FROM LoginHistory WHERE UserID =
		// '"+userid+"' AND Activity = 'mobilesignin' AND Date = (SELECT
		// MAX(Date) FROM LoginHistory))";
		String sql = "SELECT * FROM LoginHistory WHERE Time = (SELECT MAX(Time) FROM LoginHistory WHERE  Activity = 'mobilesignin' AND UserID = ? AND Date = (SELECT MAX(Date) FROM LoginHistory WHERE UserID = ?))";
		String date = "";
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			int i = 1;
			stmt.setString(i++, userid);
			stmt.setString(i++, userid);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				time = res.getString("Time");
				date = res.getString("Date");
			}
			if (!date.equalsIgnoreCase("") && !time.equalsIgnoreCase("")) {
				lasttimelogin = date.substring(6, date.length()) + "-" + date.substring(4, 6) + "-"
						+ date.substring(0, 4) + " " + time + "";
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lasttimelogin;
	}

	// zmth

	public String getPhoneNo(String userId, Connection conn) throws SQLException {
		String phno = new String();

		String query = "select t4 from UVM005_A Where RecordStatus <> 4 and t1 = ? ";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, userId);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			phno = rs.getString("T4");
		}
		rs.close();
		ps.close();
		return phno;
	}

	// atn
	/*public ArrayList<CityStateData> getStateListByUserID(String userid, Connection conn) throws SQLException {

		ArrayList<CityStateData> datalist = new ArrayList<CityStateData>();
		PreparedStatement stmt;
		String sql = "";
		String regioncode = "";
		String sql1 = "select code,DespEng,DespMyan from addressref a ,UVM005_A as b where a.code=b.t5 " + "and b.t1='"
				+ userid + "' ORDER BY Code";
		stmt = conn.prepareStatement(sql1);
		ResultSet res1 = stmt.executeQuery();
		while (res1.next()) {
			regioncode = res1.getString("code");
			if (regioncode.equals("00000000")) {
				// sql = "select code,DespEng,DespMyan from addressref where
				// code like '%000000' ORDER BY Code";
				sql = "select code,DespEng,DespMyan from addressref where code in ('00000000','13000000','10000000') ORDER BY Code";
			} else {
				sql = "select code,DespEng,DespMyan from addressref where code ='" + regioncode + "' ORDER BY Code";
			}
			stmt = conn.prepareStatement(sql);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				CityStateData combo = new CityStateData();
				combo.setDespEng(res.getString("DespEng"));
				combo.setCode(res.getString("code"));
				datalist.add(combo);
			}
		}
		return datalist;
	}*/

	// atn
	/*public ArrayList<CityStateData> getStateListByUserIDWithAll(String userid, Connection conn) throws SQLException {

		ArrayList<CityStateData> datalist = new ArrayList<CityStateData>();
		PreparedStatement stmt;
		String sql = "";
		String regioncode = "";
		String sql1 = "select code,DespEng,DespMyan from addressref a ,UVM005_A as b where a.code=b.t5 " + "and b.t1='"
				+ userid + "' ORDER BY Code";
		stmt = conn.prepareStatement(sql1);
		ResultSet res1 = stmt.executeQuery();
		while (res1.next()) {
			regioncode = res1.getString("code");
			if (regioncode.equals("00000000")) {
				// sql = "select code,DespEng,DespMyan from addressref where
				// code like '%000000' ORDER BY Code";
				sql = "select code,DespEng,DespMyan from addressref where code in ('00000000','13000000','10000000') ORDER BY Code";
			} else if (regioncode.equals("10000000")) {
				sql = "select code,DespEng,DespMyan from addressref where code in ('00000000','10000000') ORDER BY Code";
			} else if (regioncode.equals("13000000")) {
				sql = "select code,DespEng,DespMyan from addressref where code in ('00000000','13000000') ORDER BY Code";
			}
			stmt = conn.prepareStatement(sql);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				CityStateData combo = new CityStateData();
				combo.setDespEng(res.getString("DespEng"));
				combo.setCode(res.getString("code"));
				datalist.add(combo);
			}
		}
		return datalist;
	}*/

	// CityState Combo //
	public CityStateComboDataSet getStateTypeCombolist(Connection conn) throws SQLException {

		CityStateComboDataSet dataset = new CityStateComboDataSet();
		ArrayList<CityStateData> datalist = new ArrayList<CityStateData>();

		String sql = "select code,DespEng,DespMyan from addressref where code like '%000000'  AND CODE <> '00000000' and DespEng <> 'All' ORDER BY Code";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			CityStateData combo = new CityStateData();
			combo.setDespEng(res.getString("DespEng"));
			combo.setCode(res.getString("code"));
			datalist.add(combo);
		}

		CityStateData[] dataarray = new CityStateData[datalist.size()];
		dataarray = datalist.toArray(dataarray);

		dataset.setData(dataarray);
		return dataset;
	}

	public ArrayList<CityStateData> getStateTypeCombolistNew(Connection conn) throws SQLException {

		ArrayList<CityStateData> datalist = new ArrayList<CityStateData>();

		String sql = "select code,DespEng,DespMyan from addressref where code like '%000000'  AND CODE <> '00000000' and DespEng <> 'All' ORDER BY Code";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			CityStateData combo = new CityStateData();
			combo.setDespEng(res.getString("DespEng"));
			combo.setCode(res.getString("code"));
			datalist.add(combo);
		}

		return datalist;
	}

	public DivisionComboDataSet getStatusList(long status, Connection conn) throws SQLException {
		DivisionComboDataSet dataset = new DivisionComboDataSet();
		ArrayList<DivisionComboData> datalist = new ArrayList<DivisionComboData>();
		DivisionComboData combo;
		String sql = "";
		if (status == 1) { // contend writer
			sql = "select n1,t1 from status where n1 in(6,1,2) order by n1 Desc";
		}
		if (status == 2) { // editor
			sql = "select n1,t1 from status where recordstatus<>4 and(n1=2 or n1=4 or n1=6)";
		}
		if (status == 3) { // publisher
			sql = "select n1,t1 from status where recordstatus<>4 and(n1=4 or n1=5 or n1=6)";
		}
		if (status == 4) { // master
			sql = "select n1,t1 from status where recordstatus<>4 and(n1=1 or n1=2 or n1=4 or n1=5 or n1=6)";
		}
		if (status == 5) {
			sql = "select n1,t1 from status where recordstatus<>4 and(n1=1 or n1=2 or n1=4 or n1=5 or n1=6)";

		}
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			combo = new DivisionComboData();
			combo.setCaption(res.getString("t1"));
			combo.setValue(res.getString("n1"));
			datalist.add(combo);
		}
		DivisionComboData[] dataarray = new DivisionComboData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		dataset.setData(dataarray);
		return dataset;
	}

	public UserData getUserbySyskey(long skskey, Connection conn) {
		UserData userdata = new UserData();
		String sql = "select UserID,Name from UserRegistration where autokey=?";
		try {
			PreparedStatement stmt = conn.prepareStatement(sql);
			int j = 1;
			stmt.setLong(j++, skskey);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				userdata.setUserId(rs.getString("UserID"));
				userdata.setUserName(rs.getString("Name"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return userdata;
	}

	public Result getUserId(String phNo, Connection con) {
		String userId = "";
		Result result = new Result();

		String query = "select t1 from UVM005_A where RecordStatus<>4 and n2 = 1 and t4=?";
		try {
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setString(1, phNo);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				userId = res.getString("t1");
			}
			if (!userId.equals("")) {
				result.setPhNo(phNo);
				result.setUserid(userId);
				result.setMsgCode("0000");
				result.setMsgDesc("Registered User with requested phone number exists");
			} else {
				result.setPhNo(phNo);
				result.setUserid(userId);
				result.setMsgCode("0014");
				result.setMsgDesc("Registered User with requested phone number doesn't exist");

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	public String getUserName(String userid, Connection con) {
		String username = "";
		String sql = "select t2 from UVM012_A where RecordStatus<>4 and t1=?";
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, userid);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				username = res.getString("t2");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return username;
	}

	public UserData getUserNameAndNrc(String userId, Connection conn) throws SQLException {

		UserData ret = new UserData();
		String pwd = "";

		String query = "Select  a.T1 'Id',a.T7 'Nrc',a.T2 'Password',b.T2 'Name' from UVM005_A a Join UVM012_A b ON a.T1=b.t1 where (a.t1=?"
				+ " COLLATE SQL_Latin1_General_CP1_CS_AS ) and a.recordStatus = 2 and a.n7 = 0 and a.n2 = 1";
		PreparedStatement stmt1 = conn.prepareStatement(query);
		stmt1.setString(1, userId);
		ResultSet result = stmt1.executeQuery();
		while (result.next()) {

			ret.setT1(result.getString("Id"));
			ret.setT7(result.getString("Nrc"));
			ret.setName(result.getString("Name"));
			pwd = result.getString("Password");
			ret.setT2(ServerUtil.decryptPIN(pwd));

		}

		return ret;
	}

	public Result lockUnlockUser(String userid, long syskey, Connection conn, String status) throws SQLException {
		Result res = new Result();
		String sql = "";
		int restatus = 0;
		int retryCount = 3;
		if (status.equalsIgnoreCase("Lock")) {
			restatus = 11;
			retryCount = 3;
		} else if (status.equalsIgnoreCase("Unlock")) {
			restatus = 0;
			retryCount = 0;
		}
		try {
			sql = "UPDATE UVM012_A SET modifieddate = ?, t5 = ?, n7=?, n1=? WHERE recordStatus <> 4 "
					+ "and Syskey IN(SELECT N4 FROM UVM005_A WHERE recordStatus <> 4 and Syskey=?)";

			PreparedStatement stmt = conn.prepareStatement(sql);
			int i = 1;
			stmt.setString(i++, GeneralUtil.datetoString());
			stmt.setString(i++, userid);
			stmt.setInt(i++, restatus);
			stmt.setInt(i++, retryCount);
			stmt.setLong(i++, syskey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				sql = "UPDATE UVM005_A SET modifieddate = ?, n7=?,n1=? WHERE recordStatus <> 4 and Syskey=?";
				stmt = conn.prepareStatement(sql);
				int j = 1;
				stmt.setString(j++, GeneralUtil.datetoString());
				stmt.setInt(j++, restatus);
				stmt.setInt(j++, retryCount);
				stmt.setLong(j++, syskey);
				rs = stmt.executeUpdate();
				if (rs > 0) {
					res.setState(true);// lock/unlock successfully
					if (checkFirstTimeLogin(syskey, conn)) { // check this user
																// is first time
																// login or not
						if (updateCreatedDate(syskey, conn) > 0) {
							res.setState(true);
						} else {
							res.setState(false);
						}
					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return res;
	}

	public Result markPrinted(String userId, Connection conn) throws SQLException {
		Result res = new Result();
		String sql = "";
		int printCount = 0;
		try {
			String query = "Select n8 From UVM012_A WHERE Syskey IN(SELECT N4 FROM UVM005_A WHERE recordStatus <> 4 and t1=?)";
			PreparedStatement stmt1 = conn.prepareStatement(query);
			stmt1.setString(1, userId);
			ResultSet result = stmt1.executeQuery();
			while (result.next()) {
				printCount = result.getInt("n8");
			}
			if (printCount == 1) {
				res.setState(false);
			} else {
				sql = "UPDATE UVM012_A SET n8=? WHERE Syskey IN(SELECT N4 FROM UVM005_A WHERE recordStatus <> 4 and t1=?)";
				PreparedStatement stmt = conn.prepareStatement(sql);
				int i = 1;
				stmt.setInt(i++, 1);
				stmt.setString(i++, userId);
				int rs = stmt.executeUpdate();
				if (rs > 0) {
					res.setState(true);

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return res;
	}

	public UserData readByUserID(String aUserID, Connection conn) throws SQLException {

		UserData ret = new UserData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "WHERE RecordStatus<>4 AND t1='" + aUserID + "'", "",
				conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));

		return ret;

	}

	public UserData readUserProfileDataBySyskey(long syskey, Connection conn) throws SQLException {

		UserData ret = new UserData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "WHERE RecordStatus<>4 AND syskey=" + syskey, "", conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));

		return ret;

	}

	public Result resetPasswordById(String userId, Connection conn) throws SQLException {
		// TODO Auto-generated method stub
		Result res = new Result();
		String phoneNo = "";
		Long syskey = 0L;
		// boolean flag=false;
		String pwd = ServerUtil.encryptPIN(generatePassword());
		String query2 = "update UVM005_A SET t2=? WHERE recordStatus <> 4 and t1=?";
		PreparedStatement stmt2 = conn.prepareStatement(query2);
		int i = 1;
		stmt2.setString(i++, pwd);
		stmt2.setString(i++, userId);
		int count = stmt2.executeUpdate();
		if (count > 0) {
			// after reset,update flag
			String query = "update UVM012_A SET n8=2 WHERE recordStatus <> 4 and t1=?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, userId);
			int count2 = stmt.executeUpdate();
			if (count2 > 0) {
				// get phone no
				String query3 = "SELECT t4,syskey from UVM005_A WHERE RecordStatus <> 4 and t1 =?";
				PreparedStatement stmt3 = conn.prepareStatement(query3);
				stmt3.setString(1, userId);
				ResultSet rs = stmt3.executeQuery();
				while (rs.next()) {
					phoneNo = rs.getString("t4");
					syskey = rs.getLong("syskey");
				}
				// before sending sms, update flag
				String query4 = "update UVM012_A SET n8=3 WHERE RecordStatus <> 4 and t1=?";
				PreparedStatement stmt4 = conn.prepareStatement(query4);
				stmt4.setString(1, userId);
				int count3 = stmt4.executeUpdate();
				if (count3 > 0) {
					// String smsMsg = "Dear Customer,Your new password :
					// "+ServerUtil.decryptPIN(pwd)+"";
					String Msg = ConnAdmin.readExternalUrl("SMSPwd");
					String smsMsg = Msg.replace("<msg>", "" + ServerUtil.decryptPIN(pwd) + "");
					OTPReqData smspwd = sendSMSPassword(phoneNo, smsMsg); // send
																			// SMS
					// after sending sms,update flag
					String query5 = "update UVM012_A SET n8=? WHERE RecordStatus <> 4 and t1=?";
					PreparedStatement stmt5 = conn.prepareStatement(query5);
					stmt5.setString(1, userId);
					if (smspwd.getCode().equals("0000")) // send successfully
															// case
					{
						stmt5.setInt(1, 4);
					} else if (smspwd.getCode().equals("0014")) // send failed
																// or invalid
																// phone no case
					{
						stmt5.setInt(1, 5);
					}
					int count4 = stmt5.executeUpdate();
					if (count4 > 0) {
						String query6 = "update UVM012_A SET t12=?,t13=? WHERE RecordStatus <> 4 and t1=?";
						PreparedStatement stmt6 = conn.prepareStatement(query6);
						int j = 1;
						stmt6.setString(j++, smspwd.getCode());
						stmt6.setString(j++, smspwd.getDesc());
						stmt6.setString(j++, userId);
						int count5 = stmt6.executeUpdate();
						if (count5 > 0) {
							res.setState(true);
							res.setMsgDesc("Password reset successfully.");
							lockUnlockUser(userId, syskey, conn, "Unlock");
						}
					}
				} else {
					res.setState(false);
					res.setMsgDesc("Password reset fail.");
				}

			} else {
				res.setState(false);
				res.setMsgDesc("Password reset fail.");
			}
		} else {
			res.setState(false);
			res.setMsgDesc("Password reset fail.");
		}

		return res;
	}

	public OTPReqData sendSMSPassword(String phoneNo, String smsMsg) {
		OTPReqData data = new OTPReqData();
		String result = "";
		try {
			// result = MITMessageService.sendSMS(phoneNo, smsMsg, "text");
			if (result.equals("000000")) {
				data.setCode("000000");
				data.setDesc("Invalid Ph No.");
				// data.setResetpwd(pwd);
				data.setphno(phoneNo);
			} else if (!(result.equals("") || result.equalsIgnoreCase("null"))) {
				data.setCode("0000");
				data.setDesc("Success");
				// data.setResetpwd(pwd);
				data.setphno(phoneNo);
			} else {
				data.setCode("0014");
				data.setDesc("Failed");
				// data.setResetpwd(pwd);
			}
		} catch (Exception e) {
			e.printStackTrace();
			data.setCode("0014");
			data.setDesc("Failed");
			// data.setResetpwd(pwd);
		}
		return data;
	}

	public int updateCreatedDate(long syskey, Connection conn) {
		int rs = 0;
		String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sql = "UPDATE ForceChangePwd SET createddate=? WHERE userId IN(SELECT t1 FROM UVM005_A WHERE recordStatus <> 4 and Syskey=?)";
		try {
			PreparedStatement stmt = conn.prepareStatement(sql);
			int j = 1;
			stmt.setString(j++, date);
			stmt.setLong(j++, syskey);
			rs = stmt.executeUpdate();
			if (rs > 0) {
				return rs;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;
	}
	
	public UserViewDataArr getGLList(String searchtext, int pageSize, int currentPage, Connection conn, boolean master)
			throws SQLException {
		int startPage = (currentPage - 1) * pageSize;
		int endPage = pageSize + startPage;

		String whereClause = "";
		if (!searchtext.equals("")) {
			whereClause = "and ( AccNo like ? or AccDesc like ?)";
		}
		ArrayList dataList = new ArrayList();
		UserViewData[] dataArr = null;
		UserViewDataArr dataSet = new UserViewDataArr();
		String query = "SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY accno desc) AS RowNum,AccNo,AccDesc from GL where 1=1 "
				+ whereClause + ")AS RowContraintResult WHERE (RowNum >? AND RowNum <=?)";
		PreparedStatement ps = conn.prepareStatement(query);
		int x = 1;
		if (!searchtext.equals("")) {
			ps.setString(x++, "%" + searchtext + "%");
			ps.setString(x++, "%" + searchtext + "%");
		}
		ps.setInt(x++, startPage);
		ps.setInt(x, endPage);

		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			UserViewData data = new UserViewData();
			data = readGLData(rs);
			dataList.add(data);
		}
		String sql1 = "SELECT COUNT(*) AS tot from GL where 1=1  " + whereClause;
		PreparedStatement stat = conn.prepareStatement(sql1);
		int y = 1;
		if (!searchtext.equals("")) {
			stat.setString(y++, "%" + searchtext + "%");
			stat.setString(y, "%" + searchtext + "%");
		}
		ResultSet rSet = stat.executeQuery();
		rSet.next();
		if (dataList.size() > 0) {
			dataArr = new UserViewData[dataList.size()];
			for (int i = 0; i < dataList.size(); i++) {
				dataArr[i] = ((UserViewData) dataList.get(i));
			}
		}
		dataSet.setTotalCount(rSet.getInt("tot"));
		dataSet.setdata(dataArr);
		dataSet.setSearchText(searchtext);

		dataSet.setCurrentPage(currentPage);
		dataSet.setPageSize(pageSize);
		dataSet.setMsgCode("0000");
		return dataSet;
	}
	public UserViewDataArr showAccountList(String searchtext, int pageSize, int currentPage, Connection conn, boolean master) throws SQLException
	  {
	    int recordStatus = 0; int n7 = 0;
	    int startPage = (currentPage - 1) * pageSize;
	    int endPage = pageSize + startPage;

	    String whereClause = "";
	    if (!searchtext.equals("")) {
	      whereClause = "and ( A.userid like ? or B.t3 like ?)";
	    }
	    ArrayList dataList = new ArrayList();
	    UserViewData[] dataArr = null;
	    UserViewDataArr dataSet = new UserViewDataArr();
	    String query = "SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY B.autokey desc) AS RowNum,AccNumber As accountNumber,A.userid As UserID,B.t3 As userName "
	    		+ "from WJunction A inner join register B on A.userid=B.userid where B.RecordStatus=1 " + whereClause + 
	      ")AS RowContraintResult WHERE (RowNum >? AND RowNum <=?)";
	    PreparedStatement ps = conn.prepareStatement(query);
	    int x = 1;
	    if (!searchtext.equals("")) {
	      ps.setString(x++, "%" + searchtext + "%");
	      ps.setString(x++, "%" + searchtext + "%");
	    }
	    ps.setInt(x++, startPage);
	    ps.setInt(x, endPage);

	    ResultSet rs = ps.executeQuery();
	    while (rs.next()) {
	      UserViewData data = new UserViewData();
	      data = readData(rs);
	      dataList.add(data);
	    }
	    String sql1 = "SELECT COUNT(*) AS tot from WJunction A inner join register B on A.userid=B.userid where B.RecordStatus=1 " + whereClause;
	    PreparedStatement stat = conn.prepareStatement(sql1);
	    int y = 1;
	    if (!searchtext.equals("")) {
	    	stat.setString(y++, "%" + searchtext + "%");
	    	stat.setString(y, "%" + searchtext + "%");
	    }
	    ResultSet rSet = stat.executeQuery();
	    if(rSet.next()){
	    	
	    }
	    if (dataList.size() > 0) {
	      dataArr = new UserViewData[dataList.size()];
	      for (int i = 0; i < dataList.size(); i++) {
	        dataArr[i] = ((UserViewData)dataList.get(i));
	      }
	    }
	    dataSet.setTotalCount(rSet.getInt("tot"));
	    dataSet.setdata(dataArr);
	    dataSet.setSearchText(searchtext);

	    dataSet.setCurrentPage(currentPage);
	    dataSet.setPageSize(pageSize);
	    dataSet.setMsgCode("0000");
	    return dataSet;
	  }
	private UserViewData readData(ResultSet rs) throws SQLException {
	    UserViewData ret = new UserViewData();
	    ret.setAccountNumber(rs.getString("userId"));
	    ret.setUsername(rs.getString("userName"));
	    return ret;
	  }
	private UserViewData readGLData(ResultSet rs) throws SQLException {
		UserViewData ret = new UserViewData();
		ret.setAccountNumber(rs.getString("AccNo"));
		ret.setUsername(rs.getString("AccDesc"));
		return ret;
	}
}