package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.nirvasoft.rp.data.FAQResponseData;

public class FAQDao {

	public List<FAQResponseData> getFAQList(Connection conn) throws SQLException {
		List<FAQResponseData> faqList = new ArrayList<FAQResponseData>();

		String sql = "select QuestionEng,AnswerEng,QuestionUni,AnswerUni From FAQ WHERE status =? ORDER BY autokey";

		PreparedStatement stmt = conn.prepareStatement(sql);
		int i = 1;
		stmt.setInt(i++, 1);

		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			FAQResponseData faqdata = new FAQResponseData();
			faqdata.setQuestionEng(rs.getString("QuestionEng"));
			faqdata.setAnswerEng(rs.getString("AnswerEng"));
			faqdata.setQuestionUni(rs.getString("QuestionUni"));
			faqdata.setAnswerUni(rs.getString("AnswerUni"));
			faqdata.setSelected(true);
			faqList.add(faqdata);
		}

		rs.close();
		stmt.close();

		return faqList;
	}

}
