package com.nirvasoft.rp.dao;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.nirvasoft.fcpayment.core.fcchannel.Object.ReferenceData;
import com.nirvasoft.rp.shared.SystemSettingData;
import com.nirvasoft.rp.util.SharedUtil;

public class SystemSettingDAO {

	// pl search option
	private ArrayList<ReferenceData> lstReferenceData = new ArrayList<ReferenceData>();

	// pl search option
	public boolean getColumnName(String aTableName, Connection conn)
			throws ParserConfigurationException, SAXException, IOException, ClassNotFoundException, SQLException {

		String l_query = new String("");
		boolean result = false;
		lstReferenceData = new ArrayList<ReferenceData>();
		l_query = "SELECT ORDINAL_POSITION ,COLUMN_NAME ,DATA_TYPE ,CHARACTER_MAXIMUM_LENGTH ,IS_NULLABLE ,COLUMN_DEFAULT ";
		l_query += "FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = ? ORDER BY ORDINAL_POSITION ASC; ";

		PreparedStatement pstmt = conn.prepareStatement(l_query);
		pstmt.setString(1, aTableName);
		ResultSet rs = pstmt.executeQuery();
		int i = 0;

		while (rs.next()) {
			ReferenceData refData = new ReferenceData();
			readRecordColumnName(refData, rs);
			lstReferenceData.add(i++, refData);
			result = true;
		}
		pstmt.close();
		return result;
	}

	// SSH for Currency Rate
	public String GetRateColumnName(String FormName, Connection conn) {
		String l_columnName = null;
		String T1 = "FERate";
		try {
			String l_query = "SELECT t3 FROM SystemSetting WHERE t1=? AND t2=?";
			PreparedStatement l_pstmt = conn.prepareStatement(l_query);
			l_pstmt.setString(1, T1);
			l_pstmt.setString(2, FormName);
			ResultSet l_Rs = l_pstmt.executeQuery();
			while (l_Rs.next()) {
				l_columnName = l_Rs.getString("t3");
			}
		} catch (Exception e) {
			e.printStackTrace();
			l_columnName = null;
		}
		return l_columnName;
	}
	// END SSH

	public ArrayList<ReferenceData> getReferenceDataList() {
		return lstReferenceData;
	}
	// pl end

	public SystemSettingData getSystemSettingData(SystemSettingData pSystemSettingData, Connection conn)
			throws SQLException {
		SystemSettingData ret = new SystemSettingData();
		StringBuffer l_query = new StringBuffer();
		int l_BNK = 0;
		String l_Query = "SELECT N1 FROM SystemSetting Where T1='BNK'";
		PreparedStatement pstmt1 = conn.prepareStatement(l_Query);
		ResultSet rs1 = pstmt1.executeQuery();
		while (rs1.next()) {
			l_BNK = rs1.getInt(1);
		}
		if (l_BNK != 4) {
			l_query.append(
					"SELECT " + "[SystemSetting].[syskey], [SystemSetting].[autokey], [SystemSetting].[createddate], "
							+ "[SystemSetting].[userid], [SystemSetting].[SyncStatus], [SystemSetting].[SyncBatch], "
							+ "[SystemSetting].[t1], [SystemSetting].[t2], [SystemSetting].[t3], "
							+ "[SystemSetting].[t4], [SystemSetting].[n1], [SystemSetting].[n2], [SystemSetting].[n3], [SystemSetting].[n4], "
							+ "[SystemSetting].[n5],[SystemSetting].[n6],[SystemSetting].[n7] FROM [SystemSetting] "
							+ "WHERE 1=1");
		} else {
			l_query.append(
					"SELECT " + "[SystemSetting].[syskey], [SystemSetting].[autokey], [SystemSetting].[createddate], "
							+ "[SystemSetting].[userid], [SystemSetting].[SyncStatus], [SystemSetting].[SyncBatch], "
							+ "[SystemSetting].[t1], [SystemSetting].[t2], [SystemSetting].[t3], "
							+ "[SystemSetting].[t4], [SystemSetting].[n1], [SystemSetting].[n2], [SystemSetting].[n3], [SystemSetting].[n4], "
							+ "[SystemSetting].[n5] FROM [SystemSetting] " + "WHERE 1=1");
		}

		prepareWhereClause(pSystemSettingData, l_query);
		PreparedStatement pstmt = conn.prepareStatement(l_query.toString());
		prepareCriteria(pstmt, pSystemSettingData);

		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			readRecord(ret, rs, l_BNK);
		}

		return ret;
	}

	public ArrayList<SystemSettingData> getSystemSettingDatas(Connection conn) throws SQLException {
		ArrayList<SystemSettingData> ret = new ArrayList<SystemSettingData>();
		StringBuffer l_query = new StringBuffer();

		int l_BNK = 0;
		String l_Query = "SELECT N1 FROM SystemSetting Where T1='BNK'";
		PreparedStatement pstmt1 = conn.prepareStatement(l_Query);
		ResultSet rs1 = pstmt1.executeQuery();
		while (rs1.next()) {
			l_BNK = rs1.getInt(1);
		}

		if (l_BNK != 4) {
			l_query.append(
					"SELECT [SystemSetting].[syskey], [SystemSetting].[autokey], [SystemSetting].[createddate], "
							+ "[SystemSetting].[userid], [SystemSetting].[SyncStatus], [SystemSetting].[SyncBatch], "
							+ "[SystemSetting].[t1], [SystemSetting].[t2], [SystemSetting].[t3], "
							+ "[SystemSetting].[t4], [SystemSetting].[n1], [SystemSetting].[n2], [SystemSetting].[n3], [SystemSetting].[n4], "
							+ "[SystemSetting].[n5],[SystemSetting].[n6],[SystemSetting].[n7] FROM [SystemSetting] "
							+ "WHERE 1=1");
		} else {
			l_query.append(
					"SELECT [SystemSetting].[syskey], [SystemSetting].[autokey], [SystemSetting].[createddate], "
							+ "[SystemSetting].[userid], [SystemSetting].[SyncStatus], [SystemSetting].[SyncBatch], "
							+ "[SystemSetting].[t1], [SystemSetting].[t2], [SystemSetting].[t3], "
							+ "[SystemSetting].[t4], [SystemSetting].[n1], [SystemSetting].[n2], [SystemSetting].[n3], [SystemSetting].[n4], "
							+ "[SystemSetting].[n5] FROM [SystemSetting] " + "WHERE 1=1");
		}

		PreparedStatement pstmt = conn.prepareStatement(l_query.toString());
		ResultSet rs = pstmt.executeQuery();

		while (rs.next()) {
			SystemSettingData object = new SystemSettingData();
			readRecord(object, rs, l_BNK);
			ret.add(object);
		}
		pstmt.close();
		return ret;
	}

	private void prepareCriteria(PreparedStatement aPS, SystemSettingData aSystemSettingDataBean) throws SQLException {
		int i = 1;
		if (!aSystemSettingDataBean.getT1().equals("")) {
			aPS.setString(i++, aSystemSettingDataBean.getT1());
		}
	}

	private void prepareWhereClause(SystemSettingData aSystemSettingDataBean, StringBuffer aQuery) {
		if (!aSystemSettingDataBean.getT1().equals("")) {
			aQuery.append(" AND [SystemSetting].[t1] = ?");
		}
	}

	private void readRecord(SystemSettingData aSystemSettingDataBean, ResultSet aRS, int pBNK) throws SQLException {

		aSystemSettingDataBean.setsyskey(aRS.getLong("syskey"));
		aSystemSettingDataBean.setautokey(aRS.getLong("autokey"));
		aSystemSettingDataBean.setCreatedDate(SharedUtil.formatDBDate2MIT(aRS.getString("createddate")));
		aSystemSettingDataBean.setUserId(aRS.getString("userid"));
		aSystemSettingDataBean.setSyncStatus(aRS.getInt("SyncStatus"));
		aSystemSettingDataBean.setSyncBatch(aRS.getLong("SyncBatch"));
		aSystemSettingDataBean.setT1(aRS.getString("t1"));
		aSystemSettingDataBean.setT2(aRS.getString("t2"));
		aSystemSettingDataBean.setT3(aRS.getString("t3"));
		aSystemSettingDataBean.setT4(aRS.getString("t4"));
		aSystemSettingDataBean.setN1(aRS.getInt("n1"));
		aSystemSettingDataBean.setN2(aRS.getInt("n2"));
		aSystemSettingDataBean.setN3(aRS.getInt("n3"));
		aSystemSettingDataBean.setN4(aRS.getInt("n4"));
		aSystemSettingDataBean.setN5(aRS.getInt("n5"));

		if (pBNK != 4) {
			aSystemSettingDataBean.setN6(aRS.getInt("n6"));
			aSystemSettingDataBean.setN7(aRS.getInt("n7"));
		}
	}

	private void readRecordColumnName(ReferenceData pReferenceData, ResultSet aRS) {
		try {
			pReferenceData.setCode(aRS.getString("COLUMN_NAME"));
			pReferenceData.setDescription(aRS.getString("DATA_TYPE"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	// pl end

	public void setReferenceDataList(ArrayList<ReferenceData> aReferenceDataList) {
		lstReferenceData = aReferenceDataList;
	}

	public boolean updateSQLPassword(String aOldPassword, String aNewPassword, String aUserID, Connection aConn)
			throws Exception {
		boolean l_result = false;

		try {
			String l_query = "{call sp_password (? ? ?)}";
			PreparedStatement l_pstmt = aConn.prepareStatement(l_query);
			CallableStatement l_call = aConn.prepareCall(l_query);
			l_call.setString(1, aOldPassword);
			l_call.setString(2, aNewPassword);
			l_call.setString(3, aUserID);
			// if(l_pstmt.executeUpdate() > 0)
			// l_result = true;
			// l_pstmt.executeUpdate();
			l_call.setQueryTimeout(DAOManager.getNormalTime());
			l_call.executeUpdate();
			l_result = true;
			l_pstmt.close();
		} catch (Exception e) {
			throw e;
		}

		return l_result;
	}

	public boolean updateSystemSettingT2ByT1(String pT1, String pT2, Connection p_conn) throws SQLException {
		boolean l_result = false;
		String l_query = "UPDATE SystemSetting SET T2=? WHERE T1 = ?";
		PreparedStatement l_pstmt = p_conn.prepareStatement(l_query);
		l_pstmt.setString(1, pT2);
		l_pstmt.setString(2, pT1);
		l_pstmt.setQueryTimeout(DAOManager.getNormalTime());
		if (l_pstmt.executeUpdate() > 0)
			l_result = true;
		return l_result;
	}
}
