package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.nirvasoft.rp.shared.DocumentData;
import com.nirvasoft.rp.shared.DocumentRequest;

public class DocumentDao {

	public List<DocumentData> getAllPdfByRegion(DocumentRequest req, Connection conn) throws SQLException {
		List<DocumentData> pdfList = new ArrayList<DocumentData>();

		String where = "";
		if (!req.getRegion().equalsIgnoreCase("All")) {
			where = " AND (Region = ? OR Region = 'ALL' OR Region = 'BLANK')";
		}

		String sql = "SELECT * FROM Document WHERE RecordStatus=1 AND Status='Publish'" + where;
		PreparedStatement stmt = conn.prepareStatement(sql);
		if (!req.getRegion().equalsIgnoreCase("All")) {
			stmt.setString(1, req.getRegion());
		}
		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			DocumentData doc = new DocumentData();
			doc.setAutoKey("AutoKey");
			doc.setCreatedDate("CreatedDate");
			doc.setModifiedDate("ModifiedDate");
			doc.setName(rs.getString("FileName"));
			doc.setUserId("UserId");
			doc.setStatus("Status");
			doc.setRecordStatus("RecordStatus");
			doc.setFileType("FileType");
			doc.setRegion(req.getRegion());
			doc.setLink(rs.getString("FilePath"));
			doc.setPostedBy("PostedBy");
			doc.setModifiedBy("ModifiedBy");
			doc.setFileSize("FileSize");
			doc.setPages("Pages");
			doc.setTitle("Title");
			doc.setKeyword(rs.getString("T1"));
			doc.setT2("T2");
			doc.setT3("T3");
			doc.setT4("T4");
			doc.setT5("T5");
			doc.setN1("N1");
			doc.setN2("N2");
			doc.setN3("N3");
			doc.setN4("N4");
			doc.setN5("N5");
			pdfList.add(doc);
		}

		rs.close();
		stmt.close();

		return pdfList;
	}

	public List<DocumentData> getKeyword(DocumentRequest req, Connection conn) throws SQLException {
		List<DocumentData> pdfList = new ArrayList<DocumentData>();

		String where = "";
		if (!req.getRegion().equalsIgnoreCase("All")) {
			where = " AND (Region = ? OR Region = 'ALL' OR Region = 'BLANK') ";
		}

		if (!req.getKeyword().trim().equals("")) {
			where += " AND T1 LIKE ?";
		}

		String sql = "SELECT * FROM Document WHERE RecordStatus=1 AND Status='Publish' AND FileType=?" + where;
		PreparedStatement stmt = conn.prepareStatement(sql);
		int i = 1;
		stmt.setString(i++, req.getType());
		if (!req.getRegion().equalsIgnoreCase("All")) {
			stmt.setString(i++, req.getRegion());
		}
		if (!req.getKeyword().trim().equals("")) {
			stmt.setString(i++, "%" + req.getKeyword() + "%");
		}

		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			DocumentData doc = new DocumentData();
			doc.setAutoKey("AutoKey");
			doc.setCreatedDate("CreatedDate");
			doc.setModifiedDate("ModifiedDate");
			doc.setName(rs.getString("FileName"));
			doc.setUserId("UserId");
			doc.setStatus("Status");
			doc.setRecordStatus("RecordStatus");
			doc.setFileType("FileType");
			doc.setRegion(req.getRegion());
			doc.setLink(rs.getString("FilePath"));
			doc.setPostedBy("PostedBy");
			doc.setModifiedBy("ModifiedBy");
			doc.setFileSize("FileSize");
			doc.setPages("Pages");
			doc.setTitle("Title");
			doc.setKeyword(rs.getString("T1"));
			doc.setT2("T2");
			doc.setT3("T3");
			doc.setT4("T4");
			doc.setT5("T5");
			doc.setN1("N1");
			doc.setN2("N2");
			doc.setN3("N3");
			doc.setN4("N4");
			doc.setN5("N5");
			pdfList.add(doc);
		}

		rs.close();
		stmt.close();

		return pdfList;
	}

	public List<DocumentData> getPdfByTypeAndRegion(DocumentRequest req, Connection conn) throws SQLException {
		List<DocumentData> pdfList = new ArrayList<DocumentData>();
		String where = "";
		if (!req.getRegion().equalsIgnoreCase("All")) {
			where = " AND (Region = ? OR Region = 'ALL' OR Region = 'BLANK')";
		}

		String sql = "SELECT * FROM Document WHERE RecordStatus=1 AND FileType=? AND Status='Publish'" + where;
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, req.getType());
		if (!req.getRegion().equalsIgnoreCase("All")) {
			stmt.setString(2, req.getRegion());
		}
		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			DocumentData doc = new DocumentData();
			doc.setAutoKey("AutoKey");
			doc.setCreatedDate("CreatedDate");
			doc.setModifiedDate("ModifiedDate");
			doc.setName(rs.getString("FileName"));
			doc.setUserId("UserId");
			doc.setStatus("Status");
			doc.setRecordStatus("RecordStatus");
			doc.setFileType("FileType");
			doc.setRegion(req.getRegion());
			doc.setLink(rs.getString("FilePath"));
			doc.setPostedBy("PostedBy");
			doc.setModifiedBy("ModifiedBy");
			doc.setFileSize("FileSize");
			doc.setPages("Pages");
			doc.setTitle("Title");
			doc.setKeyword(rs.getString("T1"));
			doc.setT2("T2");
			doc.setT3("T3");
			doc.setT4("T4");
			doc.setT5("T5");
			doc.setN1("N1");
			doc.setN2("N2");
			doc.setN3("N3");
			doc.setN4("N4");
			doc.setN5("N5");
			pdfList.add(doc);
		}

		rs.close();
		stmt.close();

		return pdfList;
	}

}
