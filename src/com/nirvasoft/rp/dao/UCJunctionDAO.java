package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.nirvasoft.rp.shared.DepositAccData;
import com.nirvasoft.rp.shared.DepositAccDataResult;

public class UCJunctionDAO {

	public static String mTableName = "UCJunction";
	public static String mTableName1 = "UAJunction";

	public static ArrayList<String> getCustomerID(String aUserID, Connection conn) throws SQLException {
		ArrayList<String> ret = new ArrayList<String>();
		String sql = "select CustomerID from " + mTableName + " where UserID = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, aUserID);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			String aCustomerID = rs.getString("CustomerID");
			ret.add(aCustomerID);
		}
		ps.close();
		rs.close();
		return ret;
	}

	public static boolean saveUAjunction(String userID, String sessionID, String customerid, DepositAccDataResult aData,
			Connection conn) throws SQLException {
		boolean ret = true;
		String sql = "";
		PreparedStatement ps = null;
		DepositAccData[] depositaccdataarr = null;
		depositaccdataarr = aData.getDataList();
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		int rs = 0;
		for (int i = 0; i < depositaccdataarr.length; i++) {
			if (ret) {
				if (i == 0) {
					sql = "Delete From " + mTableName1 + " Where UserID=?";
					ps = conn.prepareStatement(sql);
					ps.setString(1, userID);
					rs = ps.executeUpdate();
				}
				sql = "Insert Into " + mTableName1
						+ "([UserID],[SessionID],[CreatedDate],[AccNumber],[AccBalance],[CCY]) "
						+ " values(?,?,?,?,?,?) ";
				ps = conn.prepareStatement(sql);
				ps.setString(1, userID);
				ps.setString(2, sessionID);
				ps.setString(3, date);
				ps.setString(4, depositaccdataarr[i].getDepositAcc());
				ps.setDouble(5, Double.parseDouble(depositaccdataarr[i].getAvlBal().replace(",", "")));
				ps.setString(6, depositaccdataarr[i].getCcy());
				rs = ps.executeUpdate();
				if (rs > 0) {
					ret = true;
				} else {
					ret = false;
				}
			}
		}
		ps.close();
		return ret;
	}

	public boolean checkUserAndAccount(String aUserID, String aFromAcc, Connection conn) throws SQLException {
		boolean ret = false;
		String sql = "select AccNumber from UAJunction where UserID = ? and AccNumber=?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, aUserID);
		ps.setString(2, aFromAcc);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ret = true;
		}
		ps.close();
		rs.close();
		return ret;
	}

}
