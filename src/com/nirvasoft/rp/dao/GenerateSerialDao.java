package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;
import com.nirvasoft.rp.shared.ResultData;
import com.nirvasoft.rp.shared.TicketData;

public class GenerateSerialDao {

	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("Code_Serial"); // Table Name
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("T1", (byte) 5));
		ret.getFields().add(new DBField("T2", (byte) 5));
		ret.getFields().add(new DBField("T3", (byte) 5));
		ret.getFields().add(new DBField("T4", (byte) 5));
		ret.getFields().add(new DBField("T5", (byte) 5));
		ret.getFields().add(new DBField("T6", (byte) 5));
		ret.getFields().add(new DBField("T7", (byte) 5));
		ret.getFields().add(new DBField("T8", (byte) 5));
		ret.getFields().add(new DBField("T9", (byte) 5));
		ret.getFields().add(new DBField("T10", (byte) 5));
		return ret;
	}

	public static ResultData insert(TicketData obj, Connection aConnection) throws SQLException {
		ResultData res = new ResultData();
		String sql = DBMgr.insertString(define(), aConnection);
		PreparedStatement stmt = aConnection.prepareStatement(sql);
		DBRecord dbr = setDBRecord(obj);
		DBMgr.setValues(stmt, dbr);
		try {
			if (stmt.executeUpdate() > 0) {
				res.setState(true);
				res.setMsgCode("Save Successfully!");
				res.setKeyResult(obj.getSyskey());
			} else {
				res.setState(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	public static DBRecord setDBRecord(TicketData aObj) {
		DBRecord ret = define();
		ret.setValue("T1", aObj.getT1());
		ret.setValue("T2", aObj.getT2());
		ret.setValue("T3", aObj.getT3());
		ret.setValue("T4", aObj.getT4());
		ret.setValue("T5", aObj.getT5());
		ret.setValue("T6", aObj.getT6());
		ret.setValue("T7", aObj.getT7());
		ret.setValue("T8", aObj.getT8());
		ret.setValue("T9", aObj.getT9());
		ret.setValue("T10", aObj.getT10());

		return ret;
	}

	public String getMaxSerail(String syskey, Connection conn) throws SQLException {
		String result = "";
		String result1 = "";
		ResultData res = new ResultData();
		String sql = "SELECT (ISNULL(MAX(Id),1)) as serial FROM Code_Serial";
		PreparedStatement stmt;
		stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			result = rs.getString("serial");
		}
		if (!result.equals(null)) {
			if (Integer.parseInt(result) >= 0) {
				TicketData obj = new TicketData();
				obj.setT1(result);
				obj.setT2(syskey);
				res = insert(obj, conn);
			}
		}
		if (res.isState()) {
			String sql1 = "SELECT RIGHT('0000000'+Cast(ISNULL(MAX(Id),1) AS varchar),7) AS serial  FROM Code_Serial";
			PreparedStatement stmt1;
			stmt1 = conn.prepareStatement(sql1);
			ResultSet rss = stmt1.executeQuery();
			while (rss.next()) {
				result1 = rss.getString("serial");
			}
		}

		return result1;
	}
}
