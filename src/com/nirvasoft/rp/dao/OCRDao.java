package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.nirvasoft.rp.data.OCRRequestData;

public class OCRDao {

	public String getToken(String today, Connection conn) throws SQLException {
		String token = "";

		String sql = "SELECT token FROM Token WHERE createddate=?;";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, today);
		ResultSet rs = stmt.executeQuery();

		if (rs.next()) {
			token = rs.getString("token");
		}

		rs.close();
		stmt.close();

		return token;
	}

	public String insertToken(OCRRequestData request, Connection conn) throws SQLException {
		String messageCode = "";

		int result = 0;

		String query = "INSERT INTO Token(createddate,token,t1,t2,t3,t4,t5,n1,n2,n3,n4,n5) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
		PreparedStatement ps = conn.prepareStatement(query);

		int i = 1;
		ps.setString(i++, request.getCreatedDate());
		ps.setString(i++, request.getToken());
		ps.setString(i++, request.getT1());
		ps.setString(i++, request.getT2());
		ps.setString(i++, request.getT3());
		ps.setString(i++, request.getT4());
		ps.setString(i++, request.getT5());
		ps.setInt(i++, request.getN1());
		ps.setInt(i++, request.getN2());
		ps.setInt(i++, request.getN3());
		ps.setInt(i++, request.getN4());
		ps.setInt(i++, request.getN5());

		result = ps.executeUpdate();

		if (result > 0) {
			messageCode = "0000";
		} else {
			messageCode = "0014";
		}

		ps.close();

		return messageCode;
	}

}
