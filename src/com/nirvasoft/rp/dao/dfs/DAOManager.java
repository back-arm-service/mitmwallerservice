package com.nirvasoft.rp.dao.dfs;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.rp.util.FileUtil;
import com.nirvasoft.rpmini.framework.keyfactory.KeyFactory;

import password.DESedeEncryption;

public class DAOManager {
	public static String Driver = "";
	public static String URL = "";
	public static String UserName = "";
	public static String Password = "";
	public static String ConnString = "";

	public static String Driver1 = "";
	public static String URL1 = "";
	public static String UserName1 = "";
	public static String Password1 = "";
	public static String ConnString1 = "";
	public static String AbsolutePath = "";
	public static String appType = "1";
	public static boolean logflag = false;
	public static String MEDIA_STORE_FILE = "";
	DESedeEncryption myEncryptor;

	public DAOManager() {
		try {
			myEncryptor = new DESedeEncryption();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Loading Connection From Config Files
		ReadConnectionString();
		ReadConnectionString2();
		// ReadConnectionConfig();
		if(appType.equals("")){
			appType = "1";
		}
		if (Driver.equals("")) {
			Driver = "net.sourceforge.jtds.jdbc.Driver";
		}
		if (URL.equals("")) {
			URL = "jdbc:jtds:sqlserver://localhost:1433/Maha_006;";
		}
		if (UserName.equals("")) {
			UserName = "sa";
		}
		if (Password.equals("")) {
			Password = "123";
		}

		if (ConnString.equals("")) {
			ConnString = "jdbc:sqlserver://localhost:1433;" + "databaseName=Maha_006;user=sa;password=123";
		}
		logflag = false;
	}

	public Connection CreateConnection() {
		Connection conn = null;
		try {
			try {
				Class.forName(Driver);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			conn = DriverManager.getConnection(URL, UserName, Password);
			// conn=DriverManager.getConnection(ConnString);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conn;
	}

	public Connection CreateICBSConnection() {

		Connection conn = null;
		try {
			try {
				Class.forName(Driver1);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			conn = DriverManager.getConnection(URL1, UserName1, Password1);
			// conn=DriverManager.getConnection(ConnString);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conn;
	}

	// by MD
	public String getRefKey(Connection aConn) {
		String ret = new String();
		long l_Key = 0;
		String l_Prefix = "Ref";
		try {

			KeyFactory keyfactory = KeyFactory.getInstance();
			l_Key = keyfactory.generateKey("mmppm", aConn);

			if (l_Key != 0) {
				ret = l_Prefix + l_Key;
			} else {
				ret = "Key is not generated!";
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (aConn != null)
				try {
					if (!aConn.isClosed())
						aConn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return ret;
	}


	private void ReadConnectionString() {
		String l_Driver = "";
		String l_URL = "";
		String l_UserName = "";
		String l_Password = "";
		try {
			ArrayList<String> arl = FileUtil.readList(DAOManager.AbsolutePath + "/data/" + "ConnectionConfig.txt");
			for (int i = 0; i < arl.size(); i++) {
				if (!arl.get(i).equals("")) {
					if (arl.get(i).startsWith("Driver:")) {
						l_Driver = arl.get(i).split("Driver:")[1];
					} else if (arl.get(i).startsWith("URL:")) {
						l_URL = arl.get(i).split("URL:")[1];
					} else if (arl.get(i).startsWith("UserName:")) {
						l_UserName = arl.get(i).split("UserName:")[1];
					} else if (arl.get(i).startsWith("Password:")) {
						l_Password = arl.get(i).split("Password:")[1];
					}
				}
			}

			if ((!l_Driver.equals("")) && (!l_URL.equals("")) && (!l_UserName.equals("")) && (!l_Password.equals(""))) {
				DAOManager.Driver = l_Driver.trim();
				DAOManager.URL = l_URL.trim();
				DAOManager.UserName = l_UserName.trim();
				DAOManager.Password = myEncryptor.decrypt(l_Password.trim());

				// X.say(DAOManager.Driver);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void ReadConnectionString2() {
		String l_Driver = "";
		String l_URL = "";
		String l_UserName = "";
		String l_Password = "";
		try {
			ArrayList<String> arl = FileUtil.readList(DAOManager.AbsolutePath + "/data/" + "ConnectionConfig2.txt");
			for (int i = 0; i < arl.size(); i++) {
				if (!arl.get(i).equals("")) {
					if (arl.get(i).startsWith("Driver:")) {
						l_Driver = arl.get(i).split("Driver:")[1];
					} else if (arl.get(i).startsWith("URL:")) {
						l_URL = arl.get(i).split("URL:")[1];
					} else if (arl.get(i).startsWith("UserName:")) {
						l_UserName = arl.get(i).split("UserName:")[1];
					} else if (arl.get(i).startsWith("Password:")) {
						l_Password = arl.get(i).split("Password:")[1];
					}
				}
			}

			if ((!l_Driver.equals("")) && (!l_URL.equals("")) && (!l_UserName.equals("")) && (!l_Password.equals(""))) {
				DAOManager.Driver1 = l_Driver.trim();
				DAOManager.URL1 = l_URL.trim();
				DAOManager.UserName1 = l_UserName.trim();
				DAOManager.Password1 = myEncryptor.decrypt(l_Password.trim());

				// X.say(DAOManager.Driver);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	public static void ReadLogflag() {
		String flag = "";
		try {
			ArrayList<String> arl = FileUtil.readList(DAOManager.AbsolutePath + "/json/" + "logconfig.txt");
			for (int i = 0; i < arl.size(); i++) {
				if (!arl.get(i).equals("")) {
					if (arl.get(i).startsWith("flag:")) {
						flag = arl.get(i).split("flag:")[1];
					} 
				}
			}
			if(flag.equalsIgnoreCase("true")){
				logflag = true;
			}else{
				logflag = false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public String getNewCustomerID(Connection aConn) {
		String ret = new String();
		long l_Key = 0;
		String l_Prefix = "";
		try {

			KeyFactory keyfactory = KeyFactory.getInstance();
			l_Key = keyfactory.generateKey("mmppm", aConn);

			if (l_Key != 0) {
				ret = leadZeros("" + l_Prefix + l_Key, 7);
			} else {
				ret = "Key is not generated!";
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (aConn != null)
				try {
					if (!aConn.isClosed())
						aConn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return ret;
	}

	public static String leadZeros(String p, int size) {
		String ret = p;
		for (int i = p.length(); i < size; i++) {
			ret = "0" + ret;
		}
		return ret;
	}

	public String getNewAccoutNumber(Connection aConn) {
		String ret = new String();
		long l_Key = 0;
		String l_Prefix = "";
		try {

			KeyFactory keyfactory = KeyFactory.getInstance();
			l_Key = keyfactory.generateKey("mmppm", aConn);

			if (l_Key != 0) {
				ret = leadZeros("" + l_Prefix + l_Key, 10);
			} else {
				ret = "Key is not generated!";
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (aConn != null)
				try {
					if (!aConn.isClosed())
						aConn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return ret;
	}








	
	
}
