package com.nirvasoft.rp.dao.dfs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import com.nirvasoft.rp.shared.dfs.FilterData;
import com.nirvasoft.rp.shared.dfs.FilterDataset;
import com.nirvasoft.rp.shared.dfs.MultiTransferListingData;
import com.nirvasoft.rp.shared.dfs.MultiTransferListingDataset;
import com.nirvasoft.rp.shared.SharedLogic;
import com.nirvasoft.rp.shared.SharedUtil;
import com.nirvasoft.rp.shared.dfs.TransactionData;
import com.nirvasoft.rp.shared.dfs.TransactionDataset;
import com.nirvasoft.rp.shared.dfs.TransactionListingData;
import com.nirvasoft.rp.shared.dfs.TransactionListingDataset;
import com.nirvasoft.rp.util.dfs.FileUtil;
import com.nirvasoft.rp.util.dfs.GeneralUtility;

public class TransactionDAO {
	public TransactionDataset getTransactionList(TransactionDataset data,String fromdate,String todate,String status, String branchcode,
			String searchText,int currentPage,int pageSize,String transType,String pForm,Connection con) throws SQLException {
		TransactionDataset ret = new TransactionDataset();		
		ArrayList<TransactionData> datalist = new ArrayList<TransactionData>();
		
		String whereClause = "";	
		
			if (!fromdate.isEmpty()) {
				whereClause += " AND  EffectiveDate >= '" + fromdate + "' ";
			}

			if (!todate.isEmpty()) {
				whereClause += " AND EffectiveDate <= '" + todate + "' ";
			}
			if(status.equalsIgnoreCase("active")){
				whereClause += " AND AccTrans.Status NOT IN (254,255)  ";
			}
			else if(status.equalsIgnoreCase("Reversed") || status.equalsIgnoreCase("reversal")){
				whereClause += " AND AccTrans.Status IN (254,255) ";
			}
			
			if(searchText.equalsIgnoreCase("dr")){
				whereClause += " And AccTrans.TransType >= 500 ";
			}
			else if(searchText.equalsIgnoreCase("cr")){
				whereClause += " And AccTrans.TransType <500 ";
			}
			else{
				if(!searchText.equalsIgnoreCase("")){
					whereClause += " And ( AccTrans.BranchCode = '"+searchText+"' OR  AccTrans.Amount = Replace((CASE WHEN IsNumeric('"+searchText+"') = 1 THEN '"+searchText+"' ELSE '0' END),',','')" +
					" OR AccTrans.Description like '%"+searchText+"%'  OR AccTrans.AccNumber like '%"+searchText+"%' " +
					" OR AccTrans.TransRef like '%"+searchText+"%' OR (case when TransType in (99,10) then 'Deposit' when" +
					" TransType in (599,510) then 'Withdraw' when TransType in (840,340) then 'Contract' when TransType " +
					" in (705,205) then 'Transfer' when TransType in (606,106,841,341) then 'Disbursement' when TransType" +
					" in (850,350,107) then 'Repayment'	when TransType in (254,255) then 'Reversal'end) like '%"+searchText+"%')" ;
				}
			}
			
//		
//		if ((!searchText.isEmpty())) {
//			whereClause +=" AND (g.AccDesc LIKE '%" + searchText + "%' OR TransType LIKE '%"+searchText+"%'"
//					+ "OR TransRef LIKE '%"+searchText+"%' OR AccountTransaction.TransNo LIKE '%"+searchText+"%')";
//		}
		int l_startRecord=0;
		int l_endRecord=0;
		 l_startRecord = (currentPage- 1) * pageSize;
		 l_endRecord = l_startRecord + pageSize;
		String query = "";
		String Table_created_time = FileUtil.get24HoursFormat1();
		String TEMP_MAIN_TABLE = "##TRANSACTIONMAIN" + Table_created_time;
	
		query = "SET DATEFORMAT DMY;SELECT  * into " + TEMP_MAIN_TABLE + " FROM (SELECT AccTrans.*,bd.t4,(CASE WHEN AccTrans.Transref IN ("
				+ "SELECT TransRef FROM DenoAndTr) THEN 1 ELSE 0 END) AS DenoStatus FROM ("
				+ "SELECT A.*,B.TrCurCode AS TrCurCode,B.TrAmount,B.TrPrevBalance,B.T1 AS VoucherNo "
				+ "FROM (SELECT AccountTransaction.TransNo,BranchCode,WorkStation,TransRef,SerialNo,"
				+ "TellerId,SupervisorId,CONVERT(NVARCHAR,TransTime,120) AS TransTime,TransDate,Description,"
				+ "ChequeNo,CurrencyCode,CurrencyRate,Amount,TransType,AccountTransaction.AccNumber,PrevBalance,"
				+ "PrevUpdate,EffectiveDate,ContraDate,Status,AccRef,remark,SystemCode,SubRef "
				+ "FROM AccountTransaction UNION SELECT AccountTransactionOld.TransNo,BranchCode,"
				+ "WorkStation,TransRef,SerialNo,TellerId,SupervisorId,CONVERT(NVARCHAR,TransTime,120) AS TransTime, "
				+ "TransDate,Description,ChequeNo,CurrencyCode,CurrencyRate,Amount,TransType,AccountTransactionOld.AccNumber,"
				+ "PrevBalance,PrevUpdate,EffectiveDate,ContraDate,Status,AccRef,remark,SystemCode,SubRef "
				+ "FROM AccountTransactionOld) AS A LEFT OUTER JOIN AccountGLTransaction B ON A.TransNo = B.TransNo)"
				+ " AS AccTrans LEFT OUTER JOIN bankdatabases bd ON bd.BranchCode=AccTrans.BranchCode "
				+ "WHERE 1=1"+whereClause+") main";
		
		PreparedStatement pstmt = con.prepareStatement(query);
		pstmt.executeUpdate();
		pstmt.close();	
		String sql="";
		String whereRecordCount ="";
		if(pForm.equals("print")){
			whereRecordCount = "WHERE (RowNum1 > " + l_startRecord	+ " and RowNum1 <= " + l_endRecord + " )";
		}		
		sql = "SELECT * " + " FROM ( SELECT ROW_NUMBER() OVER (ORDER BY [TransNo] ASC) AS RowNum1, "
				+ " * from "+ TEMP_MAIN_TABLE +") AS RowConstrainedResult " + whereRecordCount ;
		System.out.println("slq ==>  " + sql);
		pstmt = con.prepareStatement(sql);
		ResultSet aRS = pstmt.executeQuery();
		//int srno = l_startRecord + 1;
		while (aRS.next()) {
			TransactionData pTransData = new TransactionData();			
			pTransData.setTransactionNumber(aRS.getInt("TransNo"));
			pTransData.setBranchCode(aRS.getString("t4"));
			pTransData.setWorkstation(aRS.getString("WorkStation"));
			pTransData.setTransactionReference(aRS.getLong("TransRef"));
			pTransData.setSerial(aRS.getInt("SerialNo"));
			pTransData.setUserID(aRS.getString("TellerId"));
			pTransData.setAuthorizerID(aRS.getString("SupervisorId"));
			pTransData.setTransactionTime(FileUtil.formatDBDateTime2MIT(aRS.getString("TransTime")));
			pTransData.setTransactionDate(FileUtil.formatDBDate2MIT(aRS.getString("TransDate")));
			pTransData.setDescription(aRS.getString("Description"));
			pTransData.setReferenceNumber(aRS.getString("ChequeNo"));
			pTransData.setCurrencyCode(aRS.getString("CurrencyCode"));
			pTransData.setCurrencyRate(aRS.getDouble("CurrencyRate"));
			pTransData.setAmount(aRS.getDouble("Amount"));
			pTransData.setTransactionType(aRS.getInt("TransType"));
			pTransData.setAccountNumber(aRS.getString("AccNumber"));
			pTransData.setPreviousBalance(aRS.getDouble("PrevBalance"));
			pTransData.setPreviousDate(FileUtil.formatDBDate2MIT(aRS.getString("PrevUpdate")));
			pTransData.setEffectiveDate(FileUtil.formatDBDate2MIT(aRS.getString("EffectiveDate")));
			pTransData.setContraDate(FileUtil.formatDBDate2MIT(aRS.getString("ContraDate")));
			pTransData.setStatus(aRS.getInt("Status"));
			pTransData.setAccRef(aRS.getString("AccRef"));
			pTransData.setRemark(aRS.getString("remark"));
			pTransData.setSystemCode(aRS.getInt("SystemCode"));
			pTransData.setSubRef(aRS.getString("SubRef"));
			pTransData.setTrCurCode(aRS.getString("TrCurCode"));
			pTransData.setTrAmount(aRS.getDouble("TrAmount"));
			pTransData.setTrPrevBalance(aRS.getDouble("TrPrevBalance"));
			pTransData.setDenoStatus(aRS.getInt("DenoStatus"));
			//pTransData.setAccName(aRS.getString("AccName"));
			datalist.add(pTransData);			
		}
		aRS.close();
		pstmt.close();
		
		//String v = TEMP_MAIN_TABLE ;
		PreparedStatement stat = con.prepareStatement("SELECT COUNT(*) AS tot FROM "+TEMP_MAIN_TABLE +" m where 1=1 " );
		ResultSet rSet = stat.executeQuery();
		rSet.next();
		ret.setTotalCount(rSet.getInt("tot"));		
		rSet.close();
		stat.close();
		
		TransactionData[] dataarry = new TransactionData[datalist.size()];
		
		for (int i = 0; i < datalist.size(); i++) {
		dataarry[i] = datalist.get(i);
		}
		ret.setData(dataarry);		
		ret.setCurrentPage(l_startRecord);
		ret.setPageSize(l_endRecord);
		
		return ret;
	}
	
	
	public ArrayList<TransactionData> getTransactionListReport(String fromdate,String todate,String status, String branchcode,
			String searchText,int currentPage,int pageSize,String transType,Connection con) throws SQLException {
		TransactionDataset ret = new TransactionDataset();		
		ArrayList<TransactionData> datalist = new ArrayList<TransactionData>();
		
		String whereClause = "";	
		
			if (!fromdate.isEmpty()) {
				whereClause += " AND  EffectiveDate >= '" + fromdate + "' ";
			}

			if (!todate.isEmpty()) {
				whereClause += " AND EffectiveDate <= '" + todate + "' ";
			}
			if(status.equalsIgnoreCase("active")){
				whereClause += " AND AccTrans.Status NOT IN (254,255)  ";
			}
			else if(status.equalsIgnoreCase("Reversed") || status.equalsIgnoreCase("reversal")){
				whereClause += " AND AccTrans.Status IN (254,255) ";
			}
			
			if(searchText.equalsIgnoreCase("dr")){
				whereClause += " And AccTrans.TransType >= 500 ";
			}
			else if(searchText.equalsIgnoreCase("cr")){
				whereClause += " And AccTrans.TransType <500 ";
			}
			else{
				if(!searchText.equalsIgnoreCase("")){
					whereClause += " And ( AccTrans.BranchCode = '"+searchText+"' OR  AccTrans.Amount = Replace((CASE WHEN IsNumeric('"+searchText+"') = 1 THEN '"+searchText+"' ELSE '0' END),',','')" +
					" OR AccTrans.Description like '%"+searchText+"%'  OR AccTrans.AccNumber like '%"+searchText+"%' " +
					" OR AccTrans.TransRef like '%"+searchText+"%' OR AccTrans.TransNo like '%"+searchText+"%'OR (case when TransType in (99,10) then 'Deposit' when" +
					" TransType in (599,510) then 'Withdraw' when TransType in (840,340) then 'Contract' when TransType " +
					" in (705,205) then 'Transfer' when TransType in (606,106,841,341) then 'Disbursement' when TransType" +
					" in (850,350,107) then 'Repayment'	when TransType in (254,255) then 'Reversal'end) like '%"+searchText+"%')" ;
				}
			}
			
		
//		if ((!searchText.isEmpty())) {
//			whereClause += " AND (g.AccDesc LIKE '%" + searchText + "%' OR TransType LIKE '%"+searchText+"%'"
//					+ "OR TransRef LIKE '%"+searchText+"%' OR AccountTransaction.TransNo LIKE '%"+searchText+"%')";
//		}
		int l_startRecord=0;
		int l_endRecord=0;
		 l_startRecord = (currentPage- 1) * pageSize;
		 l_endRecord = l_startRecord + pageSize;
		String query = "";
		String Table_created_time = FileUtil.get24HoursFormat1();
		String TEMP_MAIN_TABLE = "##TRANSACTIONMAIN" + Table_created_time;
	
		query = "SET DATEFORMAT DMY;SELECT  * into " + TEMP_MAIN_TABLE + " FROM (SELECT AccTrans.*,bd.t4,(CASE WHEN AccTrans.Transref IN ("
				+ "SELECT TransRef FROM DenoAndTr) THEN 1 ELSE 0 END) AS DenoStatus FROM ("
				+ "SELECT A.*,B.TrCurCode AS TrCurCode,B.TrAmount,B.TrPrevBalance,B.T1 AS VoucherNo "
				+ "FROM (SELECT AccountTransaction.TransNo,BranchCode,WorkStation,TransRef,SerialNo,"
				+ "TellerId,SupervisorId,CONVERT(NVARCHAR,TransTime,120) As TransTime,TransDate,Description,"
				+ "ChequeNo,CurrencyCode,CurrencyRate,Amount,TransType,AccountTransaction.AccNumber,PrevBalance,"
				+ "PrevUpdate,EffectiveDate,ContraDate,Status,AccRef,remark,SystemCode,SubRef "
				+ "FROM AccountTransaction UNION SELECT AccountTransactionOld.TransNo,BranchCode,"
				+ "WorkStation,TransRef,SerialNo,TellerId,SupervisorId,CONVERT(NVARCHAR,TransTime,120) AS TransTime, "
				+ "TransDate,Description,ChequeNo,CurrencyCode,CurrencyRate,Amount,TransType,AccountTransactionOld.AccNumber,"
				+ "PrevBalance,PrevUpdate,EffectiveDate, ContraDate,Status,AccRef,remark,SystemCode,SubRef "
				+ "FROM AccountTransactionOld) AS A LEFT OUTER JOIN AccountGLTransaction B ON A.TransNo = B.TransNo)"
				+ " as AccTrans LEFT OUTER JOIN bankdatabases bd ON bd.branchcode=AccTrans.BranchCode "
				+ "WHERE 1=1"+whereClause+") main";
		
		PreparedStatement pstmt = con.prepareStatement(query);
		pstmt.executeUpdate();
		pstmt.close();	
		String sql="";
		//String whereRecordCount ="";
//		if(pForm.equals("print")){
//			whereRecordCount = "WHERE (RowNum1 > " + l_startRecord	+ " and RowNum1 <= " + l_endRecord + " )";
//		}		
		sql = "SELECT * " + " FROM ( SELECT ROW_NUMBER() OVER (ORDER BY [TransNo] ASC) AS RowNum1, "
				+ " * from "+ TEMP_MAIN_TABLE +") AS RowConstrainedResult ";
		System.out.println("slq ==>  " + sql);
		pstmt = con.prepareStatement(sql);
		ResultSet aRS = pstmt.executeQuery();
		//int srno = l_startRecord + 1;
		while (aRS.next()) {
			TransactionData pTransData = new TransactionData();			
			pTransData.setTransactionNumber(aRS.getInt("TransNo"));
			pTransData.setBranchCode(aRS.getString("t4"));
			pTransData.setWorkstation(aRS.getString("WorkStation"));
			pTransData.setTransactionReference(aRS.getLong("TransRef"));
			pTransData.setSerial(aRS.getInt("SerialNo"));
			pTransData.setUserID(aRS.getString("TellerId"));
			pTransData.setAuthorizerID(aRS.getString("SupervisorId"));
			pTransData.setTransactionTime(FileUtil.formatDBDateTime2MIT(aRS.getString("TransTime")));
			pTransData.setTransactionDate(FileUtil.formatDBDate2MIT(aRS.getString("TransDate")));
			pTransData.setDescription(aRS.getString("Description"));
			pTransData.setReferenceNumber(aRS.getString("ChequeNo"));
			pTransData.setCurrencyCode(aRS.getString("CurrencyCode"));
			pTransData.setCurrencyRate(aRS.getDouble("CurrencyRate"));
			pTransData.setAmount(aRS.getDouble("Amount"));
			pTransData.setTransactionType(aRS.getInt("TransType"));
			pTransData.setAccountNumber(aRS.getString("AccNumber"));
			pTransData.setPreviousBalance(aRS.getDouble("PrevBalance"));
			pTransData.setPreviousDate(GeneralUtility.chngeyyyyMMddtoddMMyyyy5(aRS.getString("PrevUpdate")));
			pTransData.setEffectiveDate(GeneralUtility.chngeyyyyMMddtoddMMyyyy5(aRS.getString("EffectiveDate")));
			System.out.println("effectivedate-->"+aRS.getString("EffectiveDate"));
			pTransData.setContraDate(FileUtil.formatDBDate2MIT(aRS.getString("ContraDate")));
			pTransData.setStatus(aRS.getInt("Status"));
			pTransData.setAccRef(aRS.getString("AccRef"));
			pTransData.setRemark(aRS.getString("remark"));
			pTransData.setSystemCode(aRS.getInt("SystemCode"));
			pTransData.setSubRef(aRS.getString("SubRef"));
			pTransData.setTrCurCode(aRS.getString("TrCurCode"));
			pTransData.setTrAmount(aRS.getDouble("TrAmount"));
			pTransData.setTrPrevBalance(aRS.getDouble("TrPrevBalance"));
			pTransData.setDenoStatus(aRS.getInt("DenoStatus"));
			//pTransData.setAccName(aRS.getString("AccName"));
			datalist.add(pTransData);			
		}
		aRS.close();
		pstmt.close();
		
		//String v = TEMP_MAIN_TABLE ;
		PreparedStatement stat = con.prepareStatement("SELECT COUNT(*) AS tot FROM "+TEMP_MAIN_TABLE +" m where 1=1 " );
		ResultSet rSet = stat.executeQuery();
		rSet.next();
		ret.setTotalCount(rSet.getInt("tot"));		
		rSet.close();
		stat.close();
		
		TransactionData[] dataarry = new TransactionData[datalist.size()];
		
		for (int i = 0; i < datalist.size(); i++) {
		dataarry[i] = datalist.get(i);
		}
		//ret.setData(dataarry);		
		//ret.setCurrentPage(l_startRecord);
		//ret.setPageSize(l_endRecord);
		
		return datalist;
	}

	public TransactionListingDataset getDisbursementTransactions(FilterDataset aFilterDataset, Connection aConn) throws SQLException
	{
		int l_iProcess = 0;
		String l_Query = "";
		
		String l_UniqueID = "";
		String l_ContractNo = "";
		
		ResultSet l_Result;
		PreparedStatement l_Statement;
		
		TransactionListingDataset l_Dataset = new TransactionListingDataset();
		ArrayList<TransactionListingData> l_DataList = new ArrayList<TransactionListingData>();
		
		// Get Contract No
		l_ContractNo = aFilterDataset.getFilterList().get(0).getT1();
		
		// Unique ID for temp table
		//
		l_UniqueID = "_" + aFilterDataset.getUserID() + "_" + FileUtil.get24HoursFormat1();
		l_UniqueID = l_UniqueID.replace(".", "@@");

		// Delete temp table
		//
		l_Query = "IF OBJECT_ID ('tempdb..##tblTransRef$$') IS NOT NULL  DROP TABLE ##tblTransRef$$";

		l_Query = l_Query.replace("$$", l_UniqueID);
		l_Statement = aConn.prepareStatement(l_Query);

		l_Statement.executeUpdate();
		l_Statement.close();

		// Prepare temp table
		//
		l_Query = "SELECT TOP 1 A.TransRef INTO ##tblTransRef$$ FROM dbo.AccountTransaction As A WHERE A.AccRef = ? AND TransType IN (341, 841) AND A.[Status] < 254";
		l_Query = l_Query.replace("$$", l_UniqueID);

		l_Statement = aConn.prepareStatement(l_Query);
		l_Statement.setString(1, l_ContractNo);

		l_iProcess = l_Statement.executeUpdate();
		l_Statement.close();

		// Prepare temp table - Success
		//
		if (l_iProcess > 0)
		{
			l_Query = "SELECT A.TransRef, A.TransNo, CONVERT(VARCHAR(12), A.EffectiveDate, 3) As EffectiveDate, CONVERT(VARCHAR(20), A.TransTime, 20) As TransTime, "
					+ "A.AccNumber, A.[Description], (CASE WHEN A.TransType < 500 THEN 'Credit' ELSE 'Debit' END) As TransSide, A.TransType, A.CurrencyCode, A.Amount, A.AccRef, "
					+ "A.TellerID As UserID, A.Status FROM dbo.AccountTransaction As A INNER JOIN ##tblTransRef$$ As B ON A.TransRef = B.TransRef AND A.[Status] < 254 ORDER BY A.TransRef, A.TransNo";

			l_Query = l_Query.replace("$$", l_UniqueID);
			l_Statement = aConn.prepareStatement(l_Query);
	
			// Execute Query
			l_Result = l_Statement.executeQuery();

			while (l_Result.next()) 
			{
				TransactionListingData iData = new TransactionListingData();

				iData.setTransRef(l_Result.getInt("TransRef"));
				iData.setTransNo(l_Result.getInt("TransNo"));
				iData.setEffectiveDate(l_Result.getString("EffectiveDate"));
				iData.setTransactionTime(l_Result.getString("TransTime"));
				iData.setAccountNumber(l_Result.getString("AccNumber"));
				iData.setDescription(l_Result.getString("Description"));
				iData.setTransSide(l_Result.getString("TransSide"));
				iData.setTransactionType(l_Result.getInt("TransType"));
				iData.setCurrencyCode(l_Result.getString("CurrencyCode"));
				iData.setAmount(l_Result.getDouble("Amount"));
				iData.setAccRef(l_Result.getString("AccRef"));
				iData.setUserID(l_Result.getString("UserID"));
				iData.setStatus(l_Result.getInt("Status"));
				
				l_DataList.add(iData);
			}

			l_Result.close();
			l_Statement.close();
		}

		// Set User ID, User Name
		//
		l_Dataset.setUserID(aFilterDataset.getUserID());
		l_Dataset.setUserName(aFilterDataset.getUserName());

		// Add list
		l_Dataset.setDataList(l_DataList);
		
		// Return list
		return l_Dataset;
	}

	// DBS Function
	//
	public String getJoinItem(FilterData aFilterItem)
	{
		String l_JoinItem = "";
		String l_ItemID = aFilterItem.getItemid();

		switch (l_ItemID) 
		{
			// 17 - Product Name
			//
			case "17": 	l_JoinItem = "INNER JOIN dbo.T00005 As C ON A.AccNumber = C.T1 "
								   + "INNER JOIN dbo.ProductSetup As D ON C.T2 = D.ProductID";
						break;

			default: 	break;
		}
		
		return l_JoinItem;
	}

	// DBS Function
	//
	public boolean findFilterItem(String aItemID, ArrayList<FilterData> aFilterList)
	{
		String l_ItemID;
		boolean l_FindFilterItem = false;

		for (FilterData iFilterData : aFilterList) 
		{
			l_ItemID = iFilterData.getItemid();

			// 6 - Effective Date
			//
			if (l_ItemID.equals(aItemID))
			{
				l_FindFilterItem = true;
				break;
			}
		}

		return l_FindFilterItem;
	} 

	// DBS Function
	//
	public boolean findReversalFilter(ArrayList<FilterData> aFilterList)
	{
		String l_ItemID, l_T1;
		boolean l_FindReversalFilter = false;

		for (FilterData iFilterData : aFilterList) 
		{
			l_T1 = iFilterData.getT1();
			l_ItemID = iFilterData.getItemid();

			// 16 - Filter Type (dr/cr/rev), 2 - Reversal Transaction
			//
			if (l_ItemID.equals("16") && l_T1.equals("2"))
			{
				l_FindReversalFilter = true;
				break;
			}
		}

		return l_FindReversalFilter;
	} 

	// DBS Function
	//
	public String getCriteriaItem(FilterData aFilterItem)
	{
		String l_CriteriaItem = "";

		String l_T1 = aFilterItem.getT1();
		String l_ItemID = aFilterItem.getItemid();
		
		String l_DataType = aFilterItem.getDatatype();
		String l_FieldName = aFilterItem.getFieldname();
		String l_Condition = aFilterItem.getCondition();

		// Prepare table alias
		//
		if (l_ItemID.equals("17")) 		l_FieldName = "C." + l_FieldName;					// 17 - Product Name
		else 			 				l_FieldName = "A." + l_FieldName;

		// Convert to Date
		if (l_DataType.equals("date"))	l_FieldName = "CONVERT(DATE, " + l_FieldName + ")";
		
		// Prepare Condition
		//
		switch (l_Condition) 
		{
			case "bw":																		// bw - Begin With
			case "ew":																		// ew - End With
			case "c":	l_Condition = "LIKE ?"; break;										// c - Contains

			case "eq":  l_Condition = "= ?"; break;											// eq - Equal
			case "bt":  l_Condition = ">= ? AND " + l_FieldName + " <= ?"; break;			// bt - Between

			case "gt":  l_Condition = "> ?"; break;											// gt - Greater Than	
			case "geq": l_Condition = ">= ?"; break;										// geq - Greater Than Equal

			case "lt":  l_Condition = "< ?"; break;											// lt - Less Than	
			case "leq": l_Condition = "<= ?"; break;										// leq - Less Than Equal

			default: 	l_Condition = "= ?"; break;
		}

		// 16 - Filter Type (dr/cr/rev)
		//
		if (l_ItemID.equals("16"))		
		{
			switch (l_T1)
			{
				case "0":	l_CriteriaItem = "(A.TransType >= 500)"; break;					// 0 - Debit Transaction
				case "1": 	l_CriteriaItem = "(A.TransType < 500)"; break;					// 1 - Credit Transaction
				case "2":	l_CriteriaItem = "(A.[Status] >= 254)"; break;					// 2 - Reversal Transaction
				default:	break;
			}
		}
		// 29 - Transaction Type
		//
		else if (l_ItemID.equals("29"))		
		{
			switch (l_T1)
			{
				case "0": 	l_CriteriaItem = "(A.TransType IN (840, 340))"; break;			// 0 - Contracted
				case "1": 	l_CriteriaItem = "(A.TransType IN (841, 341, 606))"; break;		// 1 - Disbursed
				case "2": 	l_CriteriaItem = "(A.TransType IN (850, 350, 107))"; break;		// 2 - Repaid
				case "3": 	l_CriteriaItem = "(A.TransType IN (860, 360))"; break;			// 3 - Closed
				case "4": 	l_CriteriaItem = "(A.TransType IN (99, 10))"; break;			// 4 - Deposit
				case "5": 	l_CriteriaItem = "(A.TransType IN (599, 510))"; break;			// 5 - Withdraw
				case "6": 	l_CriteriaItem = "(A.TransType IN (705, 205))"; break;			// 6 - Transfer
				
				default: break;
			}
		}
		else
		{
			l_CriteriaItem = "(" + l_FieldName + " " + l_Condition + ")";
		}

		// Return criteria string
		return l_CriteriaItem;
	}

	// DBS Function
	//
	public boolean isInteger(Object object) 
	{
		boolean l_isInteger = false;
		
		if(object instanceof Integer) 
		{
			l_isInteger = true;
		} 
		else 
		{
			String string = object.toString();
			
			try 
			{
				Integer.parseInt(string);
				l_isInteger = true;
			} 
			catch(Exception e) 
			{
				
			}	
		}

		return l_isInteger;
	}
	
	// DBS Function
	//
	public int fillQueryParameters(ArrayList<FilterData> aFilterList, PreparedStatement aStatement) throws SQLException
	{
		int l_Index = 1;
		
		String l_T1, l_T2;
		String l_ItemID, l_DataType, l_Condition;
		
		for (FilterData iFilterData : aFilterList) 
		{	
			l_T1 = iFilterData.getT1();
			l_T2 = iFilterData.getT2();
			
			l_ItemID = iFilterData.getItemid();
			l_DataType = iFilterData.getDatatype();
			l_Condition = iFilterData.getCondition();

			if (l_DataType.equals("numeric"))
			{
				if (!isInteger(l_T1))	l_T1 = "-1";
				if (!isInteger(l_T2))	l_T2 = "-1";
			}
			
			// For Wild Card Search
			//
			switch (l_Condition)
			{
				case "bw":	l_T1 = l_T1 + "%";
							l_T2 = l_T2 + "%"; break;

				case "ew":	l_T1 = "%" + l_T1;
							l_T2 = "%" + l_T2; break;

				case "c":	l_T1 = "%" + l_T1 + "%";
							l_T2 = "%" + l_T2 + "%"; break;
				
				default: break;
			}
			
			// 16 - Filter Type (dr/cr/rev)
			//
			if (!l_ItemID.equals("16")&& !l_ItemID.equals("29") )
			{
				switch (l_DataType)
				{
					case "date":
					case "string":		aStatement.setString(l_Index++, l_T1); 	
										if (l_Condition.equals("bt")) aStatement.setString(l_Index++, l_T2); break; 

					case "numeric":	
					default: 			aStatement.setInt(l_Index++, Integer.parseInt(l_T1)); 	
										if (l_Condition.equals("bt")) aStatement.setInt(l_Index++, Integer.parseInt(l_T2)); break;
				}
			}
		}
		
		return (l_Index - 1);
	}

	// DBS Function
	//
	public TransactionListingDataset getTransactionListingData(FilterDataset aFilterDataset, Connection aConn) throws SQLException
	{
		int iIndex = 1;
		String l_Query = "";
		
		String l_JoinClause = "";
		String l_SelectClause = "";

		String l_TransactionType1 = "";
		
		String l_CriteriaSep = "OR";
		String l_CriteriaClause = "";
		String l_RecordCountClause = "";
		
		ResultSet l_Result;
		PreparedStatement l_Statement;

		int l_TotalRecords = 0;
		
		int l_PageNo, l_PageSize;
		int l_StartRecord, l_EndRecord;

		String l_JoinItem, l_CriteriaItem;
		ArrayList<FilterData> l_FilterList;

		ArrayList<String> l_JoinList = new ArrayList<String>();
		ArrayList<String> l_CriteriaList = new ArrayList<String>();
		
		TransactionListingDataset l_Dataset = new TransactionListingDataset();
		ArrayList<TransactionListingData> l_DataList = new ArrayList<TransactionListingData>();
		
		// Record Count Clause
		//
		l_RecordCountClause = "SELECT COUNT(1) As TotalRecords FROM dbo.AccountTransaction As A";

		// Select Clause
		//
		l_SelectClause = "SELECT ROW_NUMBER() OVER (ORDER BY A.TransNo DESC) AS RowNum, "
						+ "A.TransNo, A.WorkStation, A.TransRef, A.TellerID, A.AccNumber, CONVERT(VARCHAR(12), A.EffectiveDate, 103) As EffectiveDate, A.Description, A.ChequeNo, A.CurrencyCode, A.Amount, A.PrevBalance, A.TransType, "
						+ "A.Remark, A.[Status], A.SubRef, A.SerialNo, CONVERT(VARCHAR(20), A.TransTime, 20) As TransTime, A.SupervisorID, A.BranchCode, A.AccRef, ISNULL(B.T4, '') As BranchName, ISNULL(B.branchName, '') As longBranchName, ISNULL(C.AccDesc,'Customer Account') AS AccDesc FROM dbo.AccountTransaction As A LEFT JOIN dbo.BankDatabases As B ON A.BranchCode = B.BranchCode LEFT JOIN dbo.GL As C ON A.AccNumber = C.AccNo "; //add longbranchName by mmmyint

		// Get Filter List
		l_FilterList = aFilterDataset.getFilterList();
		
		// Clear Filter List
		if (l_FilterList.get(0).getItemid().equals(""))	l_FilterList.clear();

		// Check Filter List
		//
		for (FilterData iFilterData : l_FilterList) 
		{
			// Prepare Join Item
			l_JoinItem = getJoinItem(iFilterData);

			// Prepare Criteria Item
			l_CriteriaItem = getCriteriaItem(iFilterData);

			// Add to criteria list
			l_CriteriaList.add(l_CriteriaItem);

			// Add to join list
			if (!l_JoinItem.equals("")) l_JoinList.add(l_JoinItem);
		}

		// Prepare criteria clause
		//
		if (l_CriteriaList.size() > 0) 	
		{
			if (aFilterDataset.getFilterSource() == 0)	l_CriteriaSep = " OR ";				// 0 - Common Search
			else 										l_CriteriaSep = " AND ";			// 1 - Advanced Search

			l_CriteriaClause = "(" + String.join(l_CriteriaSep, l_CriteriaList) + ")";
		}
		
		// Clear criteria list
		l_CriteriaList.clear();
		
		// Add criteria clause
		if (!l_CriteriaClause.equals(""))	l_CriteriaList.add(l_CriteriaClause);

		// Add reversal filter
		//if (!findReversalFilter(l_FilterList))	l_CriteriaList.add("(A.[Status] < 254)");
		
		// Add EffectiveDate filter
		
//		if(SharedLogic.getSystemSettingT12N2("MRHF")==1){
//			if (aFilterDataset.getFilterSource() == 0)	l_CriteriaList.add("(EffectiveDate = '" + FileUtil.getTodayDate() + "')");
//		}else{
			if (aFilterDataset.getFilterSource() == 0)	l_CriteriaList.add("(EffectiveDate >= '" + FileUtil.getTodayDate() + "')");
//		}
		

		// Prepare criteria clause
		if (l_CriteriaList.size() > 0) 	l_CriteriaClause = " WHERE (" + String.join(" AND ", l_CriteriaList) + ")";

		// Prepare join clause
		if (l_JoinList.size() > 0) 	l_JoinClause = " " + String.join(" ", l_JoinList);

		// Get Total Records Count
		//
		l_Query = l_RecordCountClause + l_JoinClause + l_CriteriaClause;
		l_Statement = aConn.prepareStatement(l_Query);

		// Set parameters
		fillQueryParameters(l_FilterList, l_Statement);

		// Execute Query
		l_Result = l_Statement.executeQuery();

		if (l_Result.next())
		{
			l_TotalRecords = l_Result.getInt("TotalRecords");
			l_Dataset.setTotalRecords(l_TotalRecords);
		}

		l_Result.close();
		l_Statement.close();

		// Get Recordset
		//
		l_Query = "SELECT RowNum, TransNo, WorkStation, TransRef, TellerID, AccNumber, AccDesc, EffectiveDate, "
				+ "Description, ChequeNo, CurrencyCode, Amount, PrevBalance, TransType, Remark, [Status], SubRef, SerialNo, TransTime, SupervisorID, BranchCode, AccRef, BranchName, longBranchName "
				+ "FROM ( " + l_SelectClause + l_JoinClause + l_CriteriaClause + ") As Resultset WHERE (? = 0) OR (RowNum > ? AND RowNum <= ?)";
		l_Statement = aConn.prepareStatement(l_Query);
		
		// Set parameters
		//
		iIndex = fillQueryParameters(l_FilterList, l_Statement);

		// Calculate Pagination
		//
		l_PageNo = aFilterDataset.getPageNo();
		l_PageSize = aFilterDataset.getPageSize();

		l_StartRecord = (l_PageNo - 1) * l_PageSize;
		l_EndRecord = l_StartRecord + l_PageSize;
		
		// Move index pointer
		iIndex = iIndex + 1;
		
		// Parameters for Pagination
		//
		l_Statement.setInt(iIndex++, l_PageNo);
		l_Statement.setInt(iIndex++, l_StartRecord);
		l_Statement.setInt(iIndex++, l_EndRecord);

		// Execute Query
		l_Result = l_Statement.executeQuery();

		while (l_Result.next()) 
		{
			TransactionListingData iData = new TransactionListingData();

			iData.setTransNo(l_Result.getInt("TransNo"));
			iData.setTerminalID(l_Result.getString("WorkStation"));
			iData.setTransRef(l_Result.getInt("TransRef"));
			iData.setUserID(l_Result.getString("TellerID"));
			iData.setAccountNumber(l_Result.getString("AccNumber"));
			iData.setGlDescription(l_Result.getString("AccDesc"));
			iData.setEffectiveDate(l_Result.getString("EffectiveDate"));
			iData.setDescription(l_Result.getString("Description"));
			iData.setChequeNo(l_Result.getString("ChequeNo"));
			iData.setCurrencyCode(l_Result.getString("CurrencyCode"));
			iData.setAmount(l_Result.getDouble("Amount"));
			iData.setPreviousBalance(l_Result.getDouble("PrevBalance"));
			iData.setTransactionType(l_Result.getInt("TransType"));
			iData.setRemark(l_Result.getString("Remark"));
			iData.setStatus(l_Result.getInt("Status"));
			iData.setSubRef(l_Result.getString("SubRef"));
			iData.setSerialNo(l_Result.getInt("SerialNo"));
			iData.setTransactionTime(l_Result.getString("TransTime"));	
			iData.setSupervisorID(l_Result.getString("SupervisorID"));
			iData.setBranchCode(l_Result.getString("BranchCode"));
			iData.setAccRef(l_Result.getString("AccRef"));
//			if(SharedLogic.getSystemSettingT12N2("MRHF") == 1){
//				iData.setBranchName(l_Result.getString("BranchName"));
//			}else
				iData.setBranchName(l_Result.getString("longBranchName"));

			// Event Type
			//
			switch (iData.getTransactionType())
			{
				case 340: 													// Contracted - 340, 840
				case 840: 	l_TransactionType1 = "Contracted"; break;

				case 341: 													// Disbursed - 341, 606, 841
				case 606: 	
				case 841:	l_TransactionType1 = "Disbursed"; break;

				case 107:													// Repayment - 107, 350, 850
				case 350:	
				case 850:	l_TransactionType1 = "Repaid"; break;

				case 360:													// Closed - 360, 860
				case 860:	l_TransactionType1 = "Closed"; break;

				case 99:													// Deposit - 99, 10
				case 10:	l_TransactionType1 = "Deposit"; break;
				
				case 599:													// Withdraw - 599, 510
				case 510:	l_TransactionType1 = "Withdraw"; break;
				
				case 705:													// Transfer - 755, 205
				case 205:	l_TransactionType1 = "Transfer"; break;
				
				
				case 206:	
				case 706:	
				case 106:	l_TransactionType1 = "Gift Cheque"; break;	// Gift Cheque 106, 606, 206, 706 
				
				case 105:	
				case 605:	l_TransactionType1 = "Payment Order"; break;	// Payment Order 105, 605, 205, 705;
				
				case 203:
				case 103:
				case 603:
				case 703:	l_TransactionType1 = "Internal Remittance"; break;	// Payment Order 703, 203, 103, 603;
				
				default: 	l_TransactionType1 = ""; break;
			}

			iData.setTransactionType1(l_TransactionType1);

			l_DataList.add(iData);
		}

		l_Result.close();
		l_Statement.close();

		// Add list
		l_Dataset.setDataList(l_DataList);

		return l_Dataset;
	}
	public MultiTransferListingDataset getMultiTransferListingData(FilterDataset aFilterDataset, Connection aConn) throws SQLException
	{
		int iIndex = 1;
		String l_Query = "";
		
		String l_SelectClause = "";
		
		String l_CriteriaSep = "OR";
		String l_CriteriaClause = "";
		String l_GroupByClause = "";
		String l_RecordCountClause = "";
		
		ResultSet l_Result;
		PreparedStatement l_Statement;

		int l_TotalRecords = 0;
		
		int l_PageNo, l_PageSize;
		int l_StartRecord, l_EndRecord;
		
		String l_CriteriaItem;
		ArrayList<FilterData> l_FilterList;
		ArrayList<String> l_CriteriaList = new ArrayList<String>();
		
		MultiTransferListingDataset l_Dataset = new MultiTransferListingDataset();
		ArrayList<MultiTransferListingData> l_DataList = new ArrayList<MultiTransferListingData>();
		
		// Record Count Clause
		//
		l_RecordCountClause = "SELECT COUNT (1)AS TotalRecords FROM (SELECT TransferNo FROM dbo.MultiTransfer As A";
				
		// Select Clause
		//
		l_SelectClause  =  " SELECT  ROW_NUMBER() OVER (ORDER BY A.TransferNo DESC) AS RowNum, TransferNo,MIN(TransDate)  AS TransDate,"
						+ " SUM(Amount) AS Amount , MIN(Status) AS [Status], MIN(CurrencyCode) AS CurrencyCode FROM MultiTransfer A ";
		
		// Get Filter List
		l_FilterList = aFilterDataset.getFilterList();
		
		// Clear Filter List
		if (l_FilterList.get(0).getItemid() == "")	l_FilterList.clear();

		// Check Filter List
		//
		for (FilterData iFilterData : l_FilterList) 
		{
			// Prepare Criteria Item
			l_CriteriaItem = getCriteriaItemMultiTransfer(iFilterData);

			// Add to criteria list
			l_CriteriaList.add(l_CriteriaItem);

		}
		// Prepare criteria clause
		//
		if (l_CriteriaList.size() > 0) 	
		{
			if (aFilterDataset.getFilterSource() == 0)	l_CriteriaSep = " OR ";				// 0 - Common Search
			else 										l_CriteriaSep = " AND ";			// 1 - Advanced Search

			l_CriteriaClause = " WHERE  A.DRCR = 2 AND (" + String.join(l_CriteriaSep, l_CriteriaList) + ")";
		}
		
		//Prepare Group By Caluse
		l_GroupByClause = " GROUP BY TransferNo ";
		
		// Get Total Records Count
		//
		l_Query = l_RecordCountClause + l_CriteriaClause + l_GroupByClause +" ) B";
		l_Statement = aConn.prepareStatement(l_Query);

		// Set parameters
		fillQueryParameters(l_FilterList, l_Statement);

		// Execute Query
		l_Result = l_Statement.executeQuery();

		if(l_Result.next())
		{
			l_TotalRecords = l_Result.getInt("TotalRecords");
			l_Dataset.setTotalRecords(l_TotalRecords);
		}

		l_Result.close();
		l_Statement.close();

		// Get Recordset
		//
		l_Query = " SELECT RowNum, TransferNo, TransDate, Amount, [Status], CurrencyCode FROM "
					+ "( "+l_SelectClause+ l_CriteriaClause+ l_GroupByClause +" ) As Resultset "
					+ "WHERE (? = 0) OR (RowNum > ? AND RowNum <= ?)";
		
		l_Statement = aConn.prepareStatement(l_Query);
		
		// Set parameters
		//
		iIndex = fillQueryParameters(l_FilterList, l_Statement);

		// Calculate Pagination
		//
		l_PageNo = aFilterDataset.getPageNo();
		l_PageSize = aFilterDataset.getPageSize();

		l_StartRecord = (l_PageNo - 1) * l_PageSize;
		l_EndRecord = l_StartRecord + l_PageSize;
		
		// Move index pointer
		iIndex = iIndex + 1;
		
		// Parameters for Pagination
		//
		l_Statement.setInt(iIndex++, l_PageNo);
		l_Statement.setInt(iIndex++, l_StartRecord);
		l_Statement.setInt(iIndex++, l_EndRecord);

		// Execute Query
		l_Result = l_Statement.executeQuery();

		while (l_Result.next()) 
		{
			MultiTransferListingData iData = new MultiTransferListingData();

			iData.setTransferNo(l_Result.getInt("TransferNo"));
			iData.setTransDate(SharedUtil.formatDBDate2MIT(l_Result.getString("TransDate")));
			iData.setTotalAmount(l_Result.getDouble("Amount")+"");
			iData.setStatus(l_Result.getInt("Status")+"");
			iData.setCurrencyCode(l_Result.getString("CurrencyCode"));
			l_DataList.add(iData);
		}

		l_Result.close();
		l_Statement.close();

		// Add list
		//l_Dataset.setDataList(l_DataList);

		return l_Dataset;
	}
	
	public String getCriteriaItemMultiTransfer(FilterData aFilterItem)
	{
		String l_CriteriaItem = "";

		String l_T1 = aFilterItem.getT1();
		String l_ItemID = aFilterItem.getItemid();
		
		String l_DataType = aFilterItem.getDatatype();
		String l_FieldName = aFilterItem.getFieldname();
		String l_Condition = aFilterItem.getCondition();

		// Prepare table alias
		//
		l_FieldName = "A." + l_FieldName;

		// Convert to Date
		if (l_DataType.equals("date"))	l_FieldName = "CONVERT(DATE, " + l_FieldName + ")";
		
		// Prepare Condition
		//
		switch (l_Condition) 
		{
			case "bw":																		// bw - Begin With
			case "ew":																		// ew - End With
			case "c":	l_Condition = "LIKE ?"; break;										// c - Contains

			case "eq":  l_Condition = "= ?"; break;											// eq - Equal
			case "bt":  l_Condition = ">= ? AND " + l_FieldName + " <= ?"; break;			// bt - Between

			case "gt":  l_Condition = "> ?"; break;											// gt - Greater Than	
			case "geq": l_Condition = ">= ?"; break;										// geq - Greater Than Equal

			case "lt":  l_Condition = "< ?"; break;											// lt - Less Than	
			case "leq": l_Condition = "<= ?"; break;										// leq - Less Than Equal

			default: 	l_Condition = "= ?"; break;
		}
			
		l_CriteriaItem = "(" + l_FieldName + " " + l_Condition + ")";	

		// Return criteria string
		return l_CriteriaItem;
	}
	public int getMultiTransferStatus (int ptransferNo,String branchCode, Connection pConn) throws SQLException
	{
		int ret = 0;
		String sql = "SELECT TOP 1 [Status] FROM MultiTransfer WHERE TransferNo = ? AND branchCode = ? ";
		
		PreparedStatement pstmt = pConn.prepareStatement(sql);
		pstmt.setInt(1, ptransferNo);
		pstmt.setString(2, branchCode);
		ResultSet rs = pstmt.executeQuery();
		if(rs.next()) ret = rs.getInt("Status");		 
		 
		return ret;
	}
}