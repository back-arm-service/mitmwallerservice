package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.nirvasoft.rp.data.UtilityResponse;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.shared.TransferOutReq;
import com.nirvasoft.rp.shared.TransferOutRes;
import com.nirvasoft.rp.util.GeneralUtil;

public class UtilityDao {

	public UtilityResponse saveUtility(TransferOutReq aReq, TransferOutRes res, Connection conn) throws SQLException {
		UtilityResponse response = new UtilityResponse();
		// String totalAmount=aReq.getBillAmount()+aReq.getPenalty();
		String query = " INSERT INTO [dbo].[CNPMerchant] ([CreatedDate] ,[TransDate] ,[Transtime] ,[XREFNO] ,[TransferRefNo1] ,[TransferRefNo2]"
				+ ",[CustRefNo],[BillRefNo] ,[CurrencyCode],[Amount],[TotalAmount],[MerchantId] ,[MerchantName],[Name],[txnID],[TxnDateRes2]"
				+ ",[LastUpdateDateTime] ,[Status],[MessageCode],[MessageDescription] ,[T1],[T2],[ApiStep],[PaidBy],[DiscountAmount],[PenaltyAmount],[DepartmentAccount],[MerchantCode]) "
				+ "VALUES " + " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement ps = conn.prepareStatement(query);
		int i = 1;
		// Date date = new Date();
		// SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		ps.setString(i++, GeneralUtil.getDateYYYYMMDD());
		ps.setString(i++, res.getTransDate());
		ps.setString(i++, res.getTransDate());
		ps.setString(i++, "");// Xrefno // transdata.getT5()
		ps.setString(i++, res.getBankRefNo());// TransferRefNo1
		ps.setString(i++, "");// TransferRefNo2
		ps.setString(i++, aReq.getRefNo());// CustRefNo
		ps.setString(i++, aReq.getBillId());// BillRefNo
		ps.setString(i++, aReq.getCcyCode());// CurrencyCode
		ps.setDouble(i++, aReq.getAmount());// Amount
		ps.setDouble(i++, aReq.getAmount());// TotalAmount
		ps.setString(i++, "");// MerchantId
		ps.setString(i++, "");// MerchantName
		ps.setString(i++, aReq.getCusName());// Name
		ps.setString(i++, "");// txnID
		ps.setString(i++, GeneralUtil.getDateYYYYMMDDHHMMSS());// TxnDateRes2
		ps.setString(i++, "");// LastUpdateDateTime
		ps.setString(i++, "Posted");// Status
		ps.setString(i++, "");// MessageCode
		ps.setString(i++, "");// MessageDescription
		ps.setString(i++, aReq.getDeptName());
		ps.setString(i++, aReq.getTaxDesc());
		ps.setInt(i++, 0);
		ps.setString(i++, aReq.getPaidBy());// paidby
		ps.setString(i++, "0.00");// DiscountAmount
		ps.setString(i++, "0.00");// PenaltyAmount
		ps.setString(i++, aReq.getToAcc());// DepartmentAccount
		ps.setString(i++, aReq.getVendorCode());// vendorCode
		if (ps.executeUpdate() > 0) {
			response.setBillAmount(String.valueOf(aReq.getAmount()));
			response.setBillId(aReq.getBillId());
			response.setCcyCode(aReq.getCcyCode());
			response.setDueDate(aReq.getDueDate());
			response.setPenalty("0.00");
			response.setRefNo(aReq.getRefNo());
			response.setTxnDate(GeneralUtil.getDateYYYYMMDDHHMMSS());
			response.setTxnRef(res.getBankRefNo());
			response.setPaidBy(aReq.getPaidBy());
			response.setTotalAmount(String.valueOf(aReq.getAmount()));
			response.setAddN1("");
			response.setAddN2("");
			response.setAddN3("");
			response.setAddT1("");
			response.setAddT2("");
			response.setAddT3("");
			response.setDiscountAmout("0.00");
			response.setCommAmount("0.00");
			response.setCode("0000");
			response.setDesc("successfully");
		} else {
			response.setCode("0014");
			response.setDesc("fail");
		}
		ps.close();
		return response;
	}

	public UtilityResponse updateUtility(ResponseData resp, String transRef, Connection conn) throws SQLException {

		UtilityResponse response = new UtilityResponse();
		String query = "Update CNPMerchant set LastUpdateDateTime=?,MessageCode=?, MessageDescription=?, ApiStep=?,Status=?  where TransferRefNo1=?";
		PreparedStatement ps = conn.prepareStatement(query);
		String status = "";
		if (resp.getCode() == "0000") {
			status = "Approved";
		} else {
			status = "Approved";
		}
		int i = 1;
		ps.setString(i++, GeneralUtil.getDateYYYYMMDDHHMMSS());// LastUpdateDateTime
		// ps.setString(i++, "0");// Status
		ps.setString(i++, resp.getCode());// MessageCode
		ps.setString(i++, resp.getDesc());// MessageDescription
		ps.setInt(i++, 1);// api Step
		ps.setString(i++, status);
		ps.setString(i++, transRef);
		if (ps.executeUpdate() > 0) {
			response.setCode("0000");
			response.setDesc("successfully");
		} else {
			response.setCode("0014");
			response.setDesc("fail");
		}
		ps.close();
		return response;

	}

}
