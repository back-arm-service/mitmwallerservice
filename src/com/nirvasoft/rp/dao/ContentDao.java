
package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.cms.dao.UploadDao;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.shared.DivisionComboData;
import com.nirvasoft.cms.shared.DivisionComboDataSet;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;
import com.nirvasoft.rp.data.ContentListingDataList;
import com.nirvasoft.rp.shared.FilterData;
import com.nirvasoft.rp.shared.FilterDataset;

public class ContentDao {
	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("FMR002");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("createdtime", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("modifiedtime", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t13", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		ret.getFields().add(new DBField("n11", (byte) 2));
		ret.getFields().add(new DBField("n12", (byte) 2));
		ret.getFields().add(new DBField("n13", (byte) 2));
		return ret;
	}

	public static ArticleData getDBRecord(DBRecord adbr) {
		ArticleData ret = new ArticleData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("createddate"));
		ret.setCreatedTime(adbr.getString("createdtime"));
		ret.setModifiedDate(adbr.getString("modifieddate"));
		ret.setModifiedTime(adbr.getString("modifiedtime"));
		ret.setUserId(adbr.getString("userid"));
		ret.setUserName(adbr.getString("username"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setUserSyskey(adbr.getLong("usersyskey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		ret.setT8(adbr.getString("t8"));
		ret.setT9(adbr.getString("t9"));
		ret.setT10(adbr.getString("t10"));
		ret.setT11(adbr.getString("t11"));
		ret.setT12(adbr.getString("t12"));
		ret.setT13(adbr.getString("t13"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getLong("n5"));
		ret.setN6(adbr.getLong("n6"));
		ret.setN7(adbr.getLong("n7"));
		ret.setN8(adbr.getLong("n8"));
		ret.setN9(adbr.getLong("n9"));
		ret.setN10(adbr.getLong("n10"));
		ret.setN11(adbr.getLong("n11"));
		ret.setN12(adbr.getLong("n12"));
		ret.setN13(adbr.getLong("n13"));
		return ret;
	}

	public Resultb2b delete(Long syskey, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = "UPDATE FMR003 SET RecordStatus=4 WHERE syskey=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Successfully");
		} else {
			res.setMsgDesc("Deleting Unsuccessful");
		}
		return res;
	}

	public Resultb2b deleteAllByKey(Long syskey, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = "UPDATE FMR003 SET RecordStatus=4 WHERE n1=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
			res.setMsgDesc("Deleted Successfully");
		} else {
			res.setMsgDesc("Deleting Unsuccessful");
		}
		return res;
	}

	public DivisionComboDataSet getAcademicRef(Connection conn) throws SQLException {
		DivisionComboDataSet dataset = new DivisionComboDataSet();
		ArrayList<DivisionComboData> datalist = new ArrayList<DivisionComboData>();
		DivisionComboData combo;
		String sql = "select syskey,t2 from ACADEMICREFERENCE WHERE RECORDSTATUS=1  ORDER BY t2 ASC ";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			combo = new DivisionComboData();
			combo.setCaption(res.getString("t2"));
			combo.setValue(res.getString("syskey"));
			combo.setFlag(false);
			datalist.add(combo);
		}
		DivisionComboData[] dataarray = new DivisionComboData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		dataset.setData(dataarray);
		return dataset;
	}

	public FilterData getFilterData(String aItemID, ArrayList<FilterData> aFilterList) {
		FilterData l_FilterData = null;

		for (FilterData iFilterData : aFilterList) {
			if (iFilterData.getItemid().equals(aItemID)) {
				l_FilterData = iFilterData;
				break;
			}
		}

		return l_FilterData;
	}

	/* read */
	public ArticleData read(long syskey, Connection conn) throws SQLException {
		ArticleData ret = new ArticleData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				"where RecordStatus<>4 AND  RecordStatus = 1 AND syskey=" + syskey, "", conn);
		if (dbrs.size() > 0)
			ret = getDBRecord(dbrs.get(0));
		if (ret.getT5().equals("")) {
			ret.setT5("00000000");
		}
		/*
		 * if (ServerUtil.isUniEncoded(ret.getT2())) {
		 * ret.setT2(FontChangeUtil.uni2zg(ret.getT2())); }
		 */
		return ret;
	}

	public Resultb2b reduceCommentCount(Long syskey, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		String sql = "Update FMR002 SET n3 = n3-1 WHERE  RecordStatus = 1 AND RecordStatus<>4 AND syskey = ?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		int rs = stmt.executeUpdate();
		if (rs > 0) {
			res.setState(true);
		}
		return res;
	}

	/* searchList */
	public ContentListingDataList selectContent(FilterDataset req, Connection conn) throws SQLException {
		ContentListingDataList res = new ContentListingDataList();		

		int startPage = (req.getPageNo() - 1) * req.getPageSize();
		int endPage = req.getPageSize() + startPage;

		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		PreparedStatement stat;
		String whereclause = "";
		whereclause = " WHERE RecordStatus<>4 AND RecordStatus=1 AND t3 <> 'Video' ";

		// Get Filter List
		ArrayList<FilterData> l_FilterList = req.getFilterList();

		// Clear Filter List
		if(l_FilterList.get(0)== null){
		//if (l_FilterList.get(0).getItemid() == ""){
			l_FilterList.clear();
		}
		// 1 - Simple Search String [ , , , ]
		String simplesearch = "";
		FilterData l_FilterData = getFilterData("1", l_FilterList);
		if (l_FilterData != null)
			simplesearch = l_FilterData.getT1();		

		// 2 - status
		String statusString = "";
		l_FilterData = getFilterData("2", l_FilterList);
		if (l_FilterData != null)
			statusString = l_FilterData.getT1();

		if (!simplesearch.equals("")) {
			whereclause += " AND (t1 LIKE ? OR modifieddate LIKE ? OR modifiedtime LIKE ? OR t2 LIKE ? "
					+ "OR username LIKE ?) ";
		}

		if (!statusString.equalsIgnoreCase("")) {
			whereclause = whereclause + " AND (n7= ? ) ";
		}

		String sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY autokey desc) AS RowNum,* FROM"
				+ " ( select * from fmr002 " + whereclause + ") b) " + " AS RowConstrainedResult" 
				+ " WHERE (RowNum > ? AND RowNum <= ? )";

		PreparedStatement ps1 = conn.prepareStatement(sql);
		int i = 1;
		if (!simplesearch.equals("")) {
			for (int j = i; j < 6; j++) {
				ps1.setString(i++, "%" + simplesearch + "%");
			}
		}
		if (!statusString.equalsIgnoreCase("")) {
			ps1.setInt(i++,Integer.parseInt(statusString));
		}
		int startIndex = i;
		int endIndex = i+1;
			ps1.setInt(startIndex, startPage);
			ps1.setInt(endIndex, endPage);
		ResultSet rs = ps1.executeQuery();		
		int srno = startPage + 1;
		while (rs.next()) {
			ArticleData data = new ArticleData();
			data.setSrno(srno++);// srno
			data.setSyskey(rs.getLong("syskey"));
			data.setT1(rs.getString("t1"));// title
			data.setT2(rs.getString("t2"));// content
			data.setT3(rs.getString("t3"));// type
			data.setT8(rs.getString("t8"));// you tube video
			data.setT13(rs.getString("t10"));
			data.setN1(rs.getLong("n1"));
			data.setN2(rs.getLong("n2"));// like count
			data.setN3(rs.getLong("n3"));// comment count
			data.setN4(rs.getLong("n4"));
			data.setN5(rs.getLong("n5"));// usersk
			data.setN6(rs.getLong("n6"));
			data.setN7(rs.getLong("n7"));// publish
			data.setN8(rs.getLong("n8"));
			data.setN9(rs.getLong("n9"));
			data.setN10(rs.getLong("n10"));// video=0,youtube=1
			if (data.getN7() == 1) {
				data.setT10("Draft");
			}
			if (data.getN7() == 2) {
				data.setT10("Pending");
			}
			if (data.getN7() == 3) {
				data.setT10("Modification");
			}
			if (data.getN7() == 4) {
				data.setT10("Approve");
			}
			if (data.getN7() == 5) {
				data.setT10("Publish");
			}
			if (data.getN7() == 6) {
				data.setT10("Reject");
			}
			data.setModifiedDate(rs.getString("modifieddate"));
			data.setModifiedTime(rs.getString("modifiedtime"));
			data.setUserName(rs.getString("username"));
			data.setModifiedUserName(rs.getString("modifiedusername"));
			data.setUpload(UploadDao.search(String.valueOf(data.getSyskey()), "", conn).getUploads());
			data.setVideoUpload(UploadDao.searchVideoList(String.valueOf(data.getSyskey()), conn).getVideoUpload());
			datalist.add(data);
		}
		res.setPageSize(req.getPageSize());
		res.setCurrentPage(req.getPageNo());

		ArticleData[] dataarray = null;
		if (datalist.size() > 0) {
			dataarray = new ArticleData[datalist.size()];
			dataarray = datalist.toArray(dataarray);
		}

		String qry = "SELECT count(*) as recCount FROM (SELECT ROW_NUMBER() "
				+ "OVER (ORDER BY syskey desc) AS RowNum,* FROM (" + "select * from fmr002 " + whereclause
				+ ") b) AS RowConstrainedResult";
		int m = 1;
		PreparedStatement ps2 = conn.prepareStatement(qry);
		if (!simplesearch.equals("")) {
			for (int k = m; k < 6; k++) {
				ps2.setString(m++, "%" + simplesearch + "%");
			}
		}
		if (!statusString.equalsIgnoreCase("")) {
			ps2.setInt(m++,Integer.parseInt(statusString));
		}
		ResultSet result = ps2.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		res.setContentData(dataarray);
		return res;
	}

}
