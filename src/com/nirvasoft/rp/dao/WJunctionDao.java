package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nirvasoft.rp.data.EachAccount;
import com.nirvasoft.rp.shared.AccountListResponseData;
import com.nirvasoft.rp.shared.NewAccountRequestData;
import com.nirvasoft.rp.shared.NewAccountResponseData;
import com.nirvasoft.rp.shared.WJunction;

public class WJunctionDao {
	public static String mTableName = "WJunction";

	public NewAccountResponseData addNewAccount(NewAccountRequestData requestData, Connection conn)
			throws SQLException {
		NewAccountResponseData responseData = new NewAccountResponseData();

		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

		String sql = "INSERT INTO WJunction(UserID, CreatedDate, AccNumber, t2, n1) VALUES(?,?,?,?,?)";

		int i = 1;

		PreparedStatement preparedStatement = conn.prepareStatement(sql);

		preparedStatement.setString(i++, requestData.getUserID());
		preparedStatement.setString(i++, date);
		preparedStatement.setString(i++, requestData.getAccNumber());
		preparedStatement.setString(i++, requestData.getAccountType());
		preparedStatement.setInt(i++, requestData.getSerialNo());

		int result = preparedStatement.executeUpdate();

		if (result > 0) {
			responseData.setCode("0000");
			responseData.setDesc("Saved successfully.");
		} else {
			responseData.setCode("0014");
			responseData.setDesc("Saved fail!");
		}

		preparedStatement.close();

		return responseData;
	}

	public AccountListResponseData getAccountList(String userID, Connection conn) throws SQLException {
		AccountListResponseData responseData = new AccountListResponseData();

		String sql = "SELECT * FROM " + mTableName + " WHERE UserID = ?";

		PreparedStatement preparedStatement = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE,
				ResultSet.CONCUR_READ_ONLY);
		preparedStatement.setString(1, userID);

		ResultSet resultSet = preparedStatement.executeQuery();

		int totalRow = 0;
		resultSet.last();
		totalRow = resultSet.getRow();

		if (totalRow > 0) {
			resultSet.beforeFirst();

			EachAccount[] accountList = new EachAccount[totalRow];
			int x = 0;

			while (resultSet.next()) {
				EachAccount account = new EachAccount();
				account.setAccNumber(resultSet.getString("AccNumber"));
				account.setAccountType(resultSet.getString("t2"));
				account.setSerialNo(resultSet.getString("n1"));
				accountList[x++] = account;
			}

			responseData.setCode("0000");
			responseData.setDesc("Selected successfully.");
			responseData.setAccountList(accountList);
		} else {
			responseData.setCode("0014");
			responseData.setDesc("There is no data!");
		}

		resultSet.close();
		preparedStatement.close();

		return responseData;
	}

	public String getCIFByID(String userID, Connection conn) throws SQLException {
		String ret = "";
		String sql = "select CusID from " + mTableName + " where UserID = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, userID);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			ret = rs.getString("CusID");
		}
		rs.close();
		ps.close();
		return ret;
	}

	public WJunction getDataByID(String userID, Connection conn) throws SQLException {
		String sql = "select * from " + mTableName + " where userID = ?";
		WJunction ret = new WJunction();
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, userID);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ret = readData(rs, ret);
		}
		return ret;
	}

	public String getNameByID(String userID, Connection conn) throws SQLException {
		String ret = "";
		String sql = "select top 1 u.Name from WJunction w, UserRegistration u where w.HKey = u.AutoKey and w.UserID = ? and u.status <> 4";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, userID);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			ret = rs.getString("Name");
		}
		rs.close();
		ps.close();
		return ret;
	}

	public int getSerialNo(String userID, Connection conn) throws SQLException {
		int serialNo = 0;

		String sql = "SELECT TOP 1 n1 FROM " + mTableName + " WHERE UserID = ? ORDER BY n1 DESC";

		PreparedStatement preparedStatement = conn.prepareStatement(sql);
		preparedStatement.setString(1, userID);

		ResultSet resultSet = preparedStatement.executeQuery();

		if (resultSet.next()) {
			serialNo = resultSet.getInt("n1");
		}

		resultSet.close();
		preparedStatement.close();

		return serialNo;
	}
	
	public boolean checkAccNoByUserID(String userID, String accNo, Connection conn) throws SQLException {
		boolean ret = false;
		String sql = "select * from " + mTableName + " where UserID = ? and accNumber = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, userID);
		ps.setString(2, accNo);
		ResultSet rs = ps.executeQuery();
		if(rs.next()){
			ret = true;
		}
		return ret;
	}

	private WJunction readData(ResultSet rs, WJunction data) throws SQLException {
		int i = 1;
		data.setAutoKey(rs.getLong(i++));
		data.sethKey(rs.getLong(i++));
		data.setUserID(rs.getString(i++));
		data.setCreatedDate(rs.getString(i++));
		data.setAccNumber(rs.getString(i++));
		data.setCusID(rs.getString(i++));
		data.setStatus(rs.getInt(i++));
		data.setRegType(rs.getInt(i++));
		data.setT1(rs.getString(i++));
		data.setT2(rs.getString(i++));
		data.setT3(rs.getString(i++));
		data.setN1(rs.getInt(i++));
		data.setN2(rs.getInt(i++));
		data.setN3(rs.getInt(i++));
		return data;
	}

}
