package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.cms.shared.UploadData;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;

public class UploadDao {

	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("FMR006");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("createdtime", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("modifiedtime", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		return ret;
	}

	public static boolean insert(UploadData obj, Connection conn) throws SQLException {
		boolean res = false;
		String query = "INSERT INTO FMR006 (syskey, createddate, userid, " + "username, recordStatus, syncStatus, "
				+ "syncBatch, usersyskey, t1, " + "t2, t3, t4, " + "t5, t6, n1, "
				+ "n2, createdtime, t7, modifieddate, modifiedtime) " + "VALUES (?,?,?," + "?,?,?," + "?,?,?,"
				+ "?,?,?," + "?,?,?," + "?,?,?,?,?)";
		try {
			if (!isCodeExist(obj, conn)) {
				PreparedStatement ps = conn.prepareStatement(query);
				int i = 1;
				ps.setLong(i++, obj.getSyskey());
				ps.setString(i++, obj.getCreatedDate());
				ps.setString(i++, obj.getUserId());
				ps.setString(i++, obj.getUserName());
				ps.setInt(i++, obj.getRecordStatus());
				ps.setInt(i++, obj.getSyncStatus());
				ps.setLong(i++, obj.getSyncBatch());
				ps.setLong(i++, obj.getUserSyskey());
				ps.setString(i++, obj.getT1());
				ps.setString(i++, obj.getT2());
				ps.setString(i++, obj.getT3());
				ps.setString(i++, obj.getT4());
				ps.setString(i++, obj.getT5());
				ps.setString(i++, obj.getT6());
				ps.setLong(i++, obj.getN1());
				ps.setLong(i++, obj.getN2());
				ps.setString(i++, obj.getCreatedTime());
				ps.setString(i++, obj.getT7());
				ps.setString(i++, obj.getModifiedDate());
				ps.setString(i++, obj.getModifiedTime());

				if (ps.executeUpdate() > 0) {
					res = true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	public static boolean isCodeExist(UploadData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				" where RecordStatus<>4 AND RecordStatus = 1 AND syskey <> " + obj.getSyskey() + " AND T1='"
						+ obj.getT1() + "' AND T2='" + obj.getT2() + "'",
				"", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

}
