package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.cms.framework.Result;
import com.nirvasoft.rp.data.MobileuserData;
import com.nirvasoft.rp.data.MobileuserListingDataList;
import com.nirvasoft.rp.shared.FilterData;
import com.nirvasoft.rp.shared.FilterDataset;

public class MobileuserAdmDao {

	public Result approvedbyuser(long syskey, String status, Connection conn) {
		Result res = new Result();
		int effectedrow = 0;
		String query = " Update Register SET t38=? WHERE recordstatus<>4 And Syskey=" + syskey + "";

		try {
			PreparedStatement pstmt = conn.prepareStatement(query);
			int i = 1;
			pstmt.setString(i++, status);// approve,reject status
			effectedrow = pstmt.executeUpdate();
			if (effectedrow > 0) {
				res.setState(true);
				if(status.equalsIgnoreCase("8"))res.setMsgDesc("Rejected Successfully!");
				else res.setMsgDesc("Approved Successfully!");
				
				res.setKeyst(status);
			} else {
				res.setMsgDesc("No Such Article to Update!");
			}

		} catch (SQLException e) {

		}

		return res;
	}

	public FilterData getFilterData(String aItemID, ArrayList<FilterData> aFilterList) {
		FilterData l_FilterData = null;

		for (FilterData iFilterData : aFilterList) {
			if (iFilterData.getItemid().equals(aItemID)) {
				l_FilterData = iFilterData;
				break;
			}
		}

		return l_FilterData;
	}

	public MobileuserListingDataList getMobileuser(FilterDataset req, Connection conn) throws SQLException {
		MobileuserListingDataList res = new MobileuserListingDataList();

		int startPage = (req.getPageNo() - 1) * req.getPageSize();
		int endPage = req.getPageSize() + startPage;

		ArrayList<MobileuserData> datalist = new ArrayList<MobileuserData>();
		PreparedStatement stat;
		String whereclause = "";
		whereclause = " and  RecordStatus<>4";

		// Get Filter List
		ArrayList<FilterData> l_FilterList = req.getFilterList();

		// 1 - Simple Search String [ , , , ]
		String simplesearch = "";
		FilterData l_FilterData = getFilterData("1", l_FilterList);
		if (l_FilterData != null)
			simplesearch = l_FilterData.getT1();

		if (!simplesearch.equals("")) {
			whereclause += " AND (a.t1 LIKE ? OR a.t3 LIKE ? "
					+ "OR a.t20 LIKE ? OR a.t21 LIKE ? "
					+ "OR a.t24 LIKE ? OR a.t25 LIKE ? "
					+ "OR a.createdDate LIKE ?) ";
		}		
		String sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY autokey desc) AS RowNum,* FROM"
				+ " ( select a.autokey,a.syskey,a.t1,a.t3,a.t20,a.t21,a.t25,a.t24,a.createdDate,a.t16,b.accNumber,a.t38 from Register a, WJunction b where a.t1=b.userid"
				+ whereclause + ") b) " + " AS RowConstrainedResult" + " WHERE (RowNum > ? AND RowNum <= ?)";
		int i=1;		
		stat = conn.prepareStatement(sql);
		if (!simplesearch.equals("")) {
			for(int j = i ; j < 8 ; j++){
				stat.setString(i++, "%"  +simplesearch + "%");
			}
		}
		int startIndex = i;
		int endIndex = i+1;
		  stat.setInt(startIndex, startPage);
		  stat.setInt(endIndex, endPage);
		ResultSet rs = stat.executeQuery();
		while (rs.next()) {
			MobileuserData data = new MobileuserData();
			data.setAutokey(rs.getLong("autokey")); // Autokey
			data.setSyskey(rs.getLong("syskey"));
			data.setT1(rs.getString("t1")); // Userid
			data.setT3(rs.getString("t3")); // Username
			data.setT20(rs.getString("t20")); // FatherName
			data.setT21(rs.getString("t21")); // Nrc
			data.setT25(rs.getString("t25")); // Date of Birth
			data.setT24(rs.getString("t24")); // Address
			data.setCreatedDate(rs.getString("createdDate")); // Register date
			data.setT16(rs.getString("t16")); // Profile Photo
			data.setAccNumber(rs.getString("accNumber")); // accountNumber
			data.setT38(rs.getString("t38"));// approve,reject status
			datalist.add(data);
		}
		res.setPageSize(req.getPageSize());
		res.setCurrentPage(req.getPageNo());

		MobileuserData[] dataarray = null;
		if (datalist.size() > 0) {
			dataarray = new MobileuserData[datalist.size()];
			dataarray = datalist.toArray(dataarray);
		}

		String qry = "SELECT count(*) as recCount FROM (SELECT ROW_NUMBER() "
				+ "OVER (ORDER BY autokey desc) AS RowNum,* FROM ("
				+ "select a.autokey,a.t1,a.t3,a.t20,a.t21,a.t25,a.t24,a.createdDate,a.t16,b.accNumber from Register a, WJunction b where a.t1=b.userid "
				+ whereclause + ") b) AS RowConstrainedResult";
		stat = conn.prepareStatement(qry);
		int m =1;
		if (!simplesearch.equals("")) {
			for(int k = m ; k < 8 ; k++){
				stat.setString(m++, "%"  +simplesearch + "%");
			}
		}
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		res.setMobileuserData(dataarray);
		return res;
	}

}
