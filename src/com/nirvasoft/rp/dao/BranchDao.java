package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.fcpayment.core.data.fcchannel.CMSModuleConfigData;
import com.nirvasoft.rp.data.BranchData;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.shared.CMSMerchantData;
import com.nirvasoft.rp.shared.icbs.BranchSetupDataSet;
import com.nirvasoft.rp.util.GeneralUtil;

public class BranchDao {

	public Result deleteBranchSetup(String branchCode, Connection l_Conn) throws SQLException {
		Result ret = new Result();
		String query = "UPDATE Branch SET recordstatus = 4 WHERE BranchCode= ? ";

		PreparedStatement pstmt = l_Conn.prepareStatement(query);
		pstmt.setString(1, branchCode);
		if (pstmt.executeUpdate() > 0) {
			ret.setMsgCode("0000");
			ret.setMsgDesc("Deleted Successfully");
			ret.setState(true);
		} else {
			ret.setMsgCode("0014");
			ret.setMsgDesc("Delete Failed");
			ret.setState(false);
		}

		pstmt.close();
		return ret;
	}

	public ArrayList<BranchData> getAllBranch(Connection conn) throws SQLException {

		ArrayList<BranchData> datalist = new ArrayList<BranchData>();

		String sql = "select BranchCode,BranchName from Branch";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			BranchData combo = new BranchData();
			combo.setBranchCode(res.getString("BranchCode"));
			combo.setBranchName(res.getString("BranchName"));
			datalist.add(combo);
		}
		return datalist;
	}

	public BranchSetupDataSet getAllBranchSetupData(String searchtext, int pageSize, int currentPage, Connection conn)
			throws SQLException {
		String searchText = searchtext.replace("'", "''");
		ArrayList<BranchData> datalist = new ArrayList<BranchData>();
		BranchSetupDataSet ret = new BranchSetupDataSet();
		int startPage = (currentPage - 1) * pageSize;
		int endPage = pageSize + startPage;
		String whereClause = "";
		if (searchText.equals("")) {
			whereClause = "where RecordStatus<>4";
		} else {
			whereClause = " where RecordStatus<>4 and (BranchCode like ? or BranchName like ? "
					+ "or Address like ? or Phone like ? Or  Fax like ? )";
		}

		String l_Query = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY BranchCode) AS RowNum, "
				+ " syskey , BranchCode ,BranchName ,Address , Phone , Fax " + " FROM Branch " + whereClause
				+ " ) AS RowConstrainedResult " + " WHERE (RowNum > " + startPage + " AND RowNum <= " + endPage + ")";
		PreparedStatement ps = conn.prepareStatement(l_Query);
		if (searchText.equals("")) {

		} else {
			String pattern = "%" + searchText + "%";
			for (int i = 1; i < 6; i++) {
				ps.setString(i, pattern);
			}
		}
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			BranchData data = new BranchData();
			data.setSyskey(rs.getLong("syskey"));
			data.setBranchCode(rs.getString("BranchCode"));
			data.setBranchName(rs.getString("BranchName"));
			data.setAddress(rs.getString("Address"));
			data.setPhone(rs.getString("Phone"));
			data.setFax(rs.getString("Fax"));
			datalist.add(data);
		}

		BranchData[] dataarr = new BranchData[datalist.size()];
		for (int i = 0; i < datalist.size(); i++) {
			dataarr[i] = datalist.get(i);
		}

		ret.setData(dataarr);
		ret.setCurrentPage(currentPage);
		ret.setPageSize(pageSize);
		ret.setSearchText(searchText);

		PreparedStatement pstm = conn.prepareStatement("Select COUNT(*) As total FROM Branch " + whereClause);
		if (searchText.equals("")) {

		} else {
			String pattern = "%" + searchText + "%";
			for (int i = 1; i < 6; i++) {
				pstm.setString(i, pattern);
			}
		}
		ResultSet result = pstm.executeQuery();
		result.next();
		ret.setTotalCount(result.getInt("total"));

		return ret;
	}

	public BranchData getBranchSetupDataByID(String id, Connection l_Conn) throws SQLException {

		BranchData data = new BranchData();
		String l_Query = "SELECT syskey , BranchCode ,BranchName ,Address , Phone , Fax FROM Branch WHERE BranchCode = ? ";
		PreparedStatement pstmt = l_Conn.prepareStatement(l_Query);
		// System.out.println("Query :: "+ l_Query);
		pstmt.setString(1, id);
		ResultSet rs = pstmt.executeQuery();

		while (rs.next()) {
			data.setSyskey(rs.getLong("syskey"));
			data.setBranchCode(rs.getString("BranchCode"));
			data.setBranchName(rs.getString("BranchName"));
			data.setAddress(rs.getString("Address"));
			data.setPhone(rs.getString("Phone"));
			data.setFax(rs.getString("Fax"));
		}

		return data;
	}

	public boolean isCodeExist(String branchCode, Connection conn) throws SQLException {
		new ArrayList<CMSMerchantData>();
		boolean ret = false;
		String l_Query = " SELECT Top(1) BranchCode FROM Branch WHERE recordstatus <> 4 and BranchCode = ? ";

		PreparedStatement pstmt = conn.prepareStatement(l_Query);
		pstmt.setString(1, branchCode);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			ret = true;
		}

		return ret;
	}

	public Result saveBranchSetup(BranchData data, Connection aConn) throws SQLException {

		Result ret = new Result();

		if (!isCodeExist(data.getBranchCode(), aConn)) {

			String query = "INSERT INTO Branch (syskey ,createddate ,modifieddate ,userid ,username ,recordstatus ,BranchCode ,BranchName ,Address ,Phone ,Fax ) "
					+ "VALUES ( ?,?,?,?,?, 1 ,?,?,?,?,?)";
			PreparedStatement psmt = aConn.prepareStatement(query);
			int i = 1;
			psmt.setLong(i++, data.getSyskey());
			psmt.setString(i++, GeneralUtil.datetoString());
			psmt.setString(i++, GeneralUtil.datetoString());
			psmt.setString(i++, data.getUserid());
			psmt.setString(i++, data.getUsername());
			psmt.setString(i++, data.getBranchCode());
			psmt.setString(i++, data.getBranchName());
			psmt.setString(i++, data.getAddress());
			psmt.setString(i++, data.getPhone());
			psmt.setString(i++, data.getFax());

			if (psmt.executeUpdate() > 0) {
				ret.setMsgCode("0000");
				ret.setMsgDesc("Save Successfully");
				ret.setState(true);
				ret.setKeyResult(data.getSyskey());
			} else {
				ret.setMsgCode("0014");
				ret.setMsgDesc("Save Failed");
				ret.setState(false);
			}
		} else {
			ret.setMsgCode("0015");
			ret.setMsgDesc("Branch Code already exists!");
		}
		return ret;

	}

	public Result updateBranchSetup(BranchData data, Connection l_Conn) throws SQLException {
		Result ret = new Result();
		String query = "UPDATE Branch SET ModifiedDate =?, BranchName=?,"
				+ " Address=?, Phone=?, Fax=?  WHERE BranchCode=?";

		PreparedStatement pstmt = l_Conn.prepareStatement(query);
		int i = 1;
		pstmt.setString(i++, GeneralUtil.datetoString());
		pstmt.setString(i++, data.getBranchName());
		pstmt.setString(i++, data.getAddress());
		pstmt.setString(i++, data.getPhone());
		pstmt.setString(i++, data.getFax());
		pstmt.setString(i++, data.getBranchCode());

		if (pstmt.executeUpdate() > 0) {
			ret.setMsgCode("0000");
			ret.setMsgDesc("Updated Successfully");
			ret.setState(true);
			ret.setKeyResult(data.getSyskey());
		} else {
			ret.setMsgCode("0014");
			ret.setMsgDesc("Updated fail");
			ret.setState(false);
		}

		pstmt.close();
		return ret;
	}
	//atn
	public CMSModuleConfigData getCMSModuleConfig(Connection con) throws SQLException {
		CMSModuleConfigData aData = new CMSModuleConfigData();
		String sql = "select BranchCodeStart,BranchCodeLength,XREFPrefix, BankName from CMSModuleConfig where ActiveBank = 1 ";
		PreparedStatement stmt = con.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			aData.setBranchCodeStart(res.getInt("BranchCodeStart"));
			aData.setBranchCodeLength(res.getInt("BranchCodeLength"));
			aData.setXREFPrefix(res.getString("XREFPrefix"));
			aData.setBankName(res.getString("BankName"));
		}
		return aData;
	}

}
