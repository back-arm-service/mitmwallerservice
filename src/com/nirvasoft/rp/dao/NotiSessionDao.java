package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.rp.shared.NotiResData;
import com.nirvasoft.rp.shared.NotiSession;

public class NotiSessionDao {
	public static String mTableName = "NotiSession";

	public boolean delete(String userID, Connection conn) throws SQLException {
		boolean ret = false;
		String sql = "delete from " + mTableName + " where userID = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, userID);
		if (ps.executeUpdate() > 0) {
			ret = true;
		}
		return ret;
	}

	public long getMaxNotiKey(String userID, Connection conn) throws SQLException {
		long ret = 0;
		String sql = "select max(NotiKey) from " + mTableName + " where userID = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, userID);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			ret = rs.getInt(1);
		}
		return ret;
	}

	public NotiResData[] getNoti(String userID, Connection conn) throws SQLException {
		ArrayList<NotiResData> retList = new ArrayList<NotiResData>();
		NotiResData[] ret = null;
		String sql = "select NotiMessage, NotiType from " + mTableName + " where userID = ? order by NotiKey desc ";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, userID);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			NotiResData data = new NotiResData();
			data = readData(rs, data);
			retList.add(data);
		}
		if (retList.size() > 0) {
			ret = new NotiResData[retList.size()];
			for (int i = 0; i < retList.size(); i++) {
				ret[i] = retList.get(i);
			}
		}
		return ret;
	}

	private NotiResData readData(ResultSet rs, NotiResData data) throws SQLException {
		int i = 1;
		data.setNotiMessage(rs.getString(i++));
		data.setNotiType(rs.getString(i++));
		return data;
	}

	public boolean save(ArrayList<NotiSession> dataList, Connection conn) throws SQLException {
		boolean ret = false;
		String sql = "INSERT INTO " + mTableName + " ([CreatedDate],[UserID],[NotiKey],[NotiMessage],[Operator],"
				+ "[NotiType],[Count],[Status],[Size],[BatchNo],[T1],[T2],[T3],[n1],[n2],[n3]) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement ps = conn.prepareStatement(sql);
		if (dataList.size() > 0) {
			for (int i = 0; i < dataList.size(); i++) {
				updateData(ps, dataList.get(i));
				ps.addBatch();
			}
			ps.executeBatch();
			ret = true;
		}
		return ret;
	}

	private void updateData(PreparedStatement ps, NotiSession data) throws SQLException {
		int i = 1;
		ps.setString(i++, data.getCreatedDate());
		ps.setString(i++, data.getUserID());
		ps.setLong(i++, data.getNotiKey());
		ps.setString(i++, data.getNotiMessage());
		ps.setString(i++, data.getOperator());
		ps.setString(i++, data.getNotiType());
		ps.setInt(i++, data.getCount());
		ps.setInt(i++, data.getStatus());
		ps.setInt(i++, data.getSize());
		ps.setInt(i++, data.getBatchNo());
		ps.setString(i++, data.getT1());
		ps.setString(i++, data.getT2());
		ps.setString(i++, data.getT3());
		ps.setInt(i++, data.getN1());
		ps.setInt(i++, data.getN2());
		ps.setInt(i++, data.getN3());
	}
}
