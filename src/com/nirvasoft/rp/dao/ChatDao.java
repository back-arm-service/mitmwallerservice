package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.nirvasoft.rp.data.ChannelData;
import com.nirvasoft.rp.data.ContactData;
import com.nirvasoft.rp.data.ContantArr;
import com.nirvasoft.rp.shared.ChannelDataset;
import com.nirvasoft.rp.shared.Contact;
import com.nirvasoft.rp.shared.ContactArr;
import com.nirvasoft.rp.shared.FilterData;
import com.nirvasoft.rp.shared.FilterDataset;
import com.nirvasoft.rp.shared.ResponseData;

public class ChatDao {

	public ResponseData addContact(Contact req, Connection conn) throws Exception {
		ResponseData res = new ResponseData();
		req.setContactRef(getContactRef(conn));
		Contact contact = new Contact();
		contact = checkPhoneNo(req.getPhone(), conn);
		req.setT1(contact.getT1());// for chat contact wcs
		req.setT2(contact.getT2());
		if (!contact.getName().equals("")) {
			if (checkExistPhone(req.getUserID(), req.getPhone(), conn)) {
				req.setName(contact.getName());
				req.setSyskey(contact.getSyskey());
				String sql = "INSERT INTO contact([Syskey],[CreatedDate],[ModifiedDate],[UserID],[ContactRef],[Phone],[Name],[Photo] ,[Type] "
						+ ",[Status] ,[t1] ,[t2] ,[t3] ,[n1] ,[n2] ,[n3]) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
				PreparedStatement ps = conn.prepareStatement(sql);
				updateContactData(ps, req);
				if (ps.executeUpdate() > 0) {
					res.setCode("0000");
					res.setDesc("Contact added successfully");
				} else {
					res.setCode("0014");
					res.setDesc("Adding contact is failed");
					return res;
				}
			} else {
				res.setCode("0016");
				res.setDesc("Contact is already exist");
				return res;
			}
		} else {
			res.setCode("0014");
			res.setDesc("Contact not found");
			return res;
		}

		return res;
	}

	// atn
	/*
	 * public ChannelDataArr getChannelList(Connection con, SessionData sdata,
	 * String region) throws SQLException {
	 * 
	 * ArrayList<ChannelData> datalist = new ArrayList<ChannelData>();
	 * ChannelDataArr res = new ChannelDataArr(); String l_Query = ""; if
	 * (region.equals("00000000")) { region = "'10000000' ,'13000000'"; l_Query
	 * = "select * from channels where regionNumber in(" + region + ") "; } else
	 * { l_Query = "select * from channels where regionNumber='" + region + "' "
	 * ; } PreparedStatement pstmt = con.prepareStatement(l_Query); ResultSet rs
	 * = pstmt.executeQuery();
	 * 
	 * while (rs.next()) { ChannelData data = new ChannelData();
	 * data.setChannelSyskey(rs.getLong("channelSyskey"));
	 * data.setAutokey(rs.getLong("autokey"));
	 * data.setChannelName(rs.getString("channel"));
	 * data.setRegion(rs.getString("region"));
	 * data.setRegionNumber(rs.getString("regionNumber")); datalist.add(data); }
	 * ChannelData[] arraylist = new ChannelData[datalist.size()]; for (int i =
	 * 0; i < datalist.size(); i++) { arraylist[i] = datalist.get(i); }
	 * res.setChanneldata(arraylist); return res; }
	 */

	public boolean checkExistPhone(String userID, String phoneNo, Connection conn) throws SQLException {
		boolean ret = true;
		String sql = "select Phone from Contact where UserID = ? and Phone = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, userID);
		ps.setString(2, phoneNo);
		ResultSet res = ps.executeQuery();
		while (res.next()) {
			ret = false;
		}
		return ret;
	}

	public Contact checkPhoneNo(String phoneNo, Connection conn) throws SQLException {
		Contact contact = new Contact();
		/*
		 * String sql =
		 * "select AutoKey,Name from UserRegistration where PhoneNo = ?";
		 * PreparedStatement ps = conn.prepareStatement(sql); ps.setString(1,
		 * phoneNo); ResultSet res = ps.executeQuery(); while (res.next()) {
		 * contact.setName( res.getString("Name"));
		 * contact.setSysKey(res.getLong("AutoKey")); }
		 */
		String sql = "select syskey,t3,t51,t16 from Register where t1 = ?";// syskey
																			// =
																			// chatapi
																			// syskey
																			// ,
																			// t3
																			// =
																			// name
																			// ,t51
																			// =
																			// autokey
																			// of
																			// userresgistration
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, phoneNo);
		ResultSet res = ps.executeQuery();
		while (res.next()) {
			contact.setSyskey(res.getLong("syskey"));
			contact.setName(res.getString("t3"));
			contact.setT1(res.getString("t51"));
			contact.setT2(res.getString("t16"));
			contact.setCode("0000");
			contact.setDesc("Success");
		}
		return contact;
	}

	public int fillQueryParameters(ArrayList<FilterData> aFilterList, PreparedStatement aStatement)
			throws SQLException {
		int l_Index = 1;

		String l_T1, l_T2;
		String l_DataType, l_Condition;

		for (FilterData iFilterData : aFilterList) {
			l_T1 = iFilterData.getT1();
			l_T2 = iFilterData.getT2();

			l_DataType = iFilterData.getDatatype();
			l_Condition = iFilterData.getCondition();

			if (l_DataType.equals("numeric")) {
				if (!isInteger(l_T1))
					l_T1 = "-1";
				if (!isInteger(l_T2))
					l_T2 = "-1";
			}

			// For Wild Card Search
			//
			switch (l_Condition) {
			case "bw":
				l_T1 = l_T1 + "%";
				l_T2 = l_T2 + "%";
				break;

			case "ew":
				l_T1 = "%" + l_T1;
				l_T2 = "%" + l_T2;
				break;

			case "c":
				l_T1 = "%" + l_T1 + "%";
				l_T2 = "%" + l_T2 + "%";
				break;

			default:
				break;
			}

			switch (l_DataType) {
			case "date":
			case "string":

			case "simplesearch":
				aStatement.setString(l_Index++, l_T1);
				if (l_Condition.equals("bt"))
					aStatement.setString(l_Index++, l_T2);
				break;

			case "numeric":
			default:
				aStatement.setInt(l_Index++, Integer.parseInt(l_T1));
				if (l_Condition.equals("bt"))
					aStatement.setInt(l_Index++, Integer.parseInt(l_T2));
				break;
			}
		}

		return (l_Index - 1);
	}

	/*
	 * public ChannelDataset getChannelList(FilterDataset aFilterDataset,
	 * Connection aConn) throws SQLException { int iIndex = 1;
	 * 
	 * String l_Query = ""; String l_SelectClause = "";
	 * 
	 * String l_CriteriaSep = "OR"; String l_CriteriaClause = ""; String
	 * l_RecordCountClause = "";
	 * 
	 * ResultSet rs; PreparedStatement l_Statement;
	 * 
	 * int l_TotalRecords = 0;
	 * 
	 * int l_PageNo, l_PageSize; int l_StartRecord, l_EndRecord;
	 * 
	 * String l_CriteriaItem;
	 * 
	 * ArrayList<FilterData> l_FilterList; ArrayList<String> l_CriteriaList =
	 * new ArrayList<String>();
	 * 
	 * ChannelDataset l_Dataset = new ChannelDataset(); ArrayList<ChannelData>
	 * l_DataList = new ArrayList<ChannelData>();
	 * 
	 * // Record Count Clause // l_RecordCountClause =
	 * "SELECT COUNT(1) As TotalRecords FROM dbo.channels As A ";
	 * 
	 * // Select Clause // l_SelectClause =
	 * "SELECT ROW_NUMBER() OVER (ORDER BY A.autokey) AS RowNum, * FROM dbo.channels As A "
	 * ;
	 * 
	 * // Get Filter List l_FilterList = aFilterDataset.getFilterList();
	 * 
	 * // Clear Filter List
	 * 
	 * //if (l_FilterList.get(0).getItemid() == "") l_FilterList.clear();
	 * 
	 * 
	 * // Clear Filter List if (l_FilterList.equals("")) l_FilterList.clear();
	 * 
	 * // Check Filter List // for (FilterData iFilterData : l_FilterList) { //
	 * Prepare Criteria Item l_CriteriaItem = getCriteriaItem(iFilterData);
	 * 
	 * // Add to criteria list l_CriteriaList.add(l_CriteriaItem); }
	 * 
	 * // Prepare criteria clause // if (l_CriteriaList.size() > 0) { if
	 * (aFilterDataset.getFilterSource() == 0) l_CriteriaSep = " OR "; // 0 -
	 * Common Search else l_CriteriaSep = " AND "; // 1 - Advanced Search
	 * 
	 * l_CriteriaClause = "(" + String.join(l_CriteriaSep, l_CriteriaList) +
	 * ")"; }
	 * 
	 * // Clear criteria list l_CriteriaList.clear();
	 * 
	 * // Add criteria clause if (!l_CriteriaClause.equals(""))
	 * l_CriteriaList.add(l_CriteriaClause);
	 * 
	 * // Prepare criteria clause if (l_CriteriaList.size() > 0)
	 * l_CriteriaClause = " WHERE (" + String.join(" AND ", l_CriteriaList) +
	 * ")";
	 * 
	 * // Get Total Records Count // l_Query = l_RecordCountClause +
	 * l_CriteriaClause; l_Statement = aConn.prepareStatement(l_Query);
	 * 
	 * // Set parameters fillQueryParameters(l_FilterList, l_Statement);
	 * 
	 * // Execute Query rs = l_Statement.executeQuery();
	 * 
	 * if (rs.next()) { l_TotalRecords = rs.getInt("TotalRecords");
	 * l_Dataset.setTotalCount(l_TotalRecords); }
	 * 
	 * rs.close(); l_Statement.close();
	 * 
	 * l_Query = "SELECT RowNum, * FROM ( " + l_SelectClause + l_CriteriaClause
	 * + ") As Resultset WHERE (? = 0) OR (RowNum > ? AND RowNum <= ?)";
	 * l_Statement = aConn.prepareStatement(l_Query);
	 * 
	 * // Set parameters // iIndex = fillQueryParameters(l_FilterList,
	 * l_Statement);
	 * 
	 * // Calculate Pagination // l_PageNo = aFilterDataset.getPageNo();
	 * l_PageSize = aFilterDataset.getPageSize();
	 * 
	 * l_StartRecord = (l_PageNo - 1) * l_PageSize; l_EndRecord = l_StartRecord
	 * + l_PageSize;
	 * 
	 * // Move index pointer iIndex = iIndex + 1;
	 * 
	 * // Parameters for Pagination // l_Statement.setInt(iIndex++, l_PageNo);
	 * l_Statement.setInt(iIndex++, l_StartRecord); l_Statement.setInt(iIndex++,
	 * l_EndRecord);
	 * 
	 * // Execute Query rs = l_Statement.executeQuery();
	 * 
	 * while (rs.next()) { ChannelData data = new ChannelData();
	 * data.setChannelSyskey(rs.getLong("channelSyskey"));
	 * data.setAutokey(rs.getLong("autokey"));
	 * data.setChannelName(rs.getString("channel"));
	 * data.setRegion(rs.getString("region"));
	 * data.setRegionNumber(rs.getString("regionNumber")); l_DataList.add(data);
	 * }
	 * 
	 * rs.close(); l_Statement.close();
	 * 
	 * // Set User ID, User Name //
	 * l_Dataset.setUserID(aFilterDataset.getUserID());
	 * l_Dataset.setUserName(aFilterDataset.getUserName());
	 * 
	 * // Add list l_Dataset.setDataList(l_DataList);
	 * 
	 * return l_Dataset; }
	 */
	public ChannelDataset getChannelList(FilterDataset req, Connection conn) throws SQLException {
		ChannelDataset cdataset = new ChannelDataset();
		String query = "select t5 from UVM005_A where t1='" + req.getUserID() + "' ";
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs1 = ps.executeQuery();
		String regioncode = "";

		while (rs1.next()) {
			regioncode = rs1.getString("t5");
		}

		int startPage = (req.getPageNo() - 1) * req.getPageSize();
		int endPage = req.getPageSize() + startPage;

		ArrayList<ChannelData> datalist = new ArrayList<ChannelData>();
		PreparedStatement stat;
		String whereclause = "";
		// Get Filter List
		ArrayList<FilterData> l_FilterList = req.getFilterList();

		// Clear Filter List
		// if (l_FilterList.get(0).getItemid() == "")
		// l_FilterList.clear();
		if (l_FilterList.equals(""))
			l_FilterList.clear();

		// 1 - Simple Search String [ , , , ]
		String simplesearch = "";
		FilterData l_FilterData = getFilterData("1", l_FilterList);
		if (l_FilterData != null)
			simplesearch = l_FilterData.getT1();

		// 2 - State
		String state = "";
		l_FilterData = getFilterData("2", l_FilterList);
		if (l_FilterData != null)
			state = l_FilterData.getT1();

		if (state.equals("")) {
			state = "00000000";
		}

		if (!simplesearch.equals("")) {
			whereclause += "where (channel LIKE N'%" + simplesearch + "%' " + "OR Region LIKE N'%" + simplesearch
					+ "%' ) ";
			if (!state.equals("00000000")) {
				whereclause = whereclause + "And regionNumber = '" + state + "'";
			} else {
				if (!regioncode.equals("00000000")) {
					whereclause = whereclause + "And regionNumber = '" + regioncode + "' ";
				}
			}
		} else {

			if (!state.equals("00000000")) {
				whereclause = whereclause + "Where regionNumber = '" + state + "'";
			} else {
				if (!regioncode.equals("00000000")) {
					whereclause = whereclause + "Where regionNumber = '" + regioncode + "' ";
				}
			}
		}

		String sql = " SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY autokey desc) AS RowNum,* FROM ("
				+ "select * from channels " + whereclause + ") b) AS RowConstrainedResult WHERE RowNum >= '" + startPage
				+ "' AND RowNum <= '" + endPage + "' order by autokey desc";

		PreparedStatement ps1 = conn.prepareStatement(sql);
		ResultSet rs = ps1.executeQuery();
		// Execute Query
		rs = ps1.executeQuery();

		while (rs.next()) {
			ChannelData data = new ChannelData();
			data.setChannelSyskey(rs.getLong("channelSyskey"));
			data.setAutokey(rs.getLong("autokey"));
			data.setChannelName(rs.getString("channel"));
			data.setRegion(rs.getString("region"));
			data.setRegionNumber(rs.getString("regionNumber"));
			datalist.add(data);
		}

		rs.close();
		ps1.close();

		ChannelData[] dataarray = null;
		if (datalist.size() > 0) {
			dataarray = new ChannelData[datalist.size()];
			dataarray = datalist.toArray(dataarray);
		}

		String qry = "SELECT count(*) as recCount FROM (SELECT ROW_NUMBER() "
				+ "OVER (ORDER BY autokey desc) AS RowNum,* FROM (" + "select * from channels " + whereclause
				+ ") b) AS RowConstrainedResult";

		stat = conn.prepareStatement(qry);
		ResultSet result = stat.executeQuery();
		result.next();
		cdataset.setPageSize(req.getPageSize());
		cdataset.setPageNo(req.getPageNo());
		cdataset.setTotalCount(result.getInt("recCount"));
		cdataset.setDataList(datalist);
		cdataset.setUserID(req.getUserID());
		cdataset.setUserName(req.getUserName());
		return cdataset;
	}

	public ContantArr getChatContact(Connection con, String channelkey) throws SQLException {
		ArrayList<ContactData> datalist = new ArrayList<ContactData>();
		ContantArr res = new ContantArr();
		String l_Query = "";
		l_Query = "select syskey,t1,t3,t16 from register";
		/*
		 * if (channelkey.equals("1054") || channelkey.equals("1058") ||
		 * channelkey.equals("1059")) { l_Query =
		 * "select syskey,t1,t3,t16 from register where t36 in ('13000000','10000000,13000000','13000000,10000000')"
		 * ; } else if (channelkey.equals("1060") || channelkey.equals("1061")
		 * || channelkey.equals("1062")) { l_Query =
		 * "select syskey,t1,t3,t16 from register where t36 in ('10000000','10000000,13000000','13000000,10000000')"
		 * ; } else if (channelkey.equals("34434")) { l_Query =
		 * "select syskey,t1,t3,t16 from register where t36 like '%00000000%'";
		 * }
		 */
		PreparedStatement pstmt = con.prepareStatement(l_Query);
		ResultSet rs = pstmt.executeQuery();

		while (rs.next()) {
			ContactData data = new ContactData();
			data.setSyskey(rs.getLong("syskey"));
			data.setT1(rs.getString("t1"));
			data.setT3(rs.getString("t3"));
			data.setT16(rs.getString("t16"));
			datalist.add(data);
		}
		ContactData[] arraylist = new ContactData[datalist.size()];
		for (int i = 0; i < datalist.size(); i++) {
			arraylist[i] = datalist.get(i);
		}
		res.setContactArr(arraylist);
		return res;
	}

	public ContactArr getContact(String userID, Connection conn) throws SQLException {
		ArrayList<Contact> dataList = new ArrayList<Contact>();
		ContactArr res = new ContactArr();
		Contact[] dataArr = null;
		String sql = "select Syskey, phone, name, t2, t3 from Contact where userid = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, userID);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Contact data = new Contact();
			data = readContactData(rs, data);
			dataList.add(data);
		}
		if (dataList.size() > 0) {
			dataArr = new Contact[dataList.size()];
			for (int i = 0; i < dataList.size(); i++) {
				dataArr[i] = dataList.get(i);
			}
			res.setCode("0000");
			res.setDesc("Success");
			res.setDataList(dataArr);
		} else {
			res.setCode("0014");
			res.setDesc("No record found!");
		}
		return res;
	}

	public String getContactRef(Connection conn) throws SQLException {
		String ref = "";
		String sql = "select Format(ISNULL(Max(Convert(int,(RIGHT(ContactRef,7)))),0) + 1,'0000000') ContactRef from Contact";
		Statement ps = conn.createStatement();
		ResultSet res = ps.executeQuery(sql);
		while (res.next()) {
			ref = res.getString("ContactRef");
		}
		return ref;
	}

	public String getCriteriaItem(FilterData aFilterItem) {
		String l_CriteriaItem = "";

		String l_T1 = aFilterItem.getT1();
		String l_ItemID = aFilterItem.getItemid();

		String l_DataType = aFilterItem.getDatatype();
		String l_FieldName = aFilterItem.getFieldname();
		String l_Condition = aFilterItem.getCondition();

		// Prepare table alias
		l_FieldName = "A." + l_FieldName;

		// Prepare Condition
		//
		switch (l_Condition) {
		case "bw": // bw - Begin With
		case "ew": // ew - End With
		case "c":
			l_Condition = "LIKE ?";
			break; // c - Contains

		case "eq":
			l_Condition = "= ?";
			break; // eq - Equal
		case "bt":
			l_Condition = ">= ? AND " + l_FieldName + " <= ?";
			break; // bt - Between

		case "gt":
			l_Condition = "> ?";
			break; // gt - Greater Than
		case "geq":
			l_Condition = ">= ?";
			break; // geq - Greater Than Equal

		case "lt":
			l_Condition = "< ?";
			break; // lt - Less Than
		case "leq":
			l_Condition = "<= ?";
			break; // leq - Less Than Equal

		default:
			l_Condition = "= ?";
			break;
		}

		// Criteria Item
		l_CriteriaItem = "(" + l_FieldName + " " + l_Condition + ")";

		// Return criteria string
		return l_CriteriaItem;
	}

	public FilterData getFilterData(String aItemID, ArrayList<FilterData> aFilterList) {
		FilterData l_FilterData = null;

		for (FilterData iFilterData : aFilterList) {
			if (iFilterData.getItemid().equals(aItemID)) {
				l_FilterData = iFilterData;
				break;
			}
		}

		return l_FilterData;
	}

	public boolean isInteger(Object object) {
		boolean l_isInteger = false;

		if (object instanceof Integer) {
			l_isInteger = true;
		} else {
			String string = object.toString();

			try {
				Integer.parseInt(string);
				l_isInteger = true;
			} catch (Exception e) {

			}
		}

		return l_isInteger;
	}

	private Contact readContactData(ResultSet rs, Contact data) throws SQLException {
		int i = 1;
		data.setSyskey(rs.getLong(i++));
		data.setPhone(rs.getString(i++));
		data.setName(rs.getString(i++));
		data.setT18(rs.getString(i++));
		data.setChannelkey(rs.getString(i++));
		return data;
	}

	public ResponseData updateChannelKey(String channelkey, String syskey, String phone, Connection conn)
			throws SQLException {
		ResponseData response = new ResponseData();

		int result = 0;

		String query = "UPDATE Contact SET t3=? WHERE syskey=? AND userid=?";
		PreparedStatement ps = conn.prepareStatement(query);

		int i = 1;
		ps.setString(i++, channelkey);
		ps.setLong(i++, Long.parseLong(syskey));
		ps.setString(i++, phone);

		result = ps.executeUpdate();

		if (result > 0) {
			response.setCode("0000");
			response.setDesc("Updated successfully.");
		} else {
			response.setCode("0014");
			response.setDesc("Update failed.");
		}

		ps.close();

		return response;
	}

	private void updateContactData(PreparedStatement ps, Contact data) throws Exception {
		int i = 1;
		ps.setLong(i++, data.getSyskey());
		ps.setString(i++, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		ps.setString(i++, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		ps.setString(i++, data.getUserID());
		ps.setString(i++, data.getContactRef());
		ps.setString(i++, data.getPhone());
		ps.setString(i++, data.getName());
		ps.setString(i++, data.getPhoto());
		ps.setString(i++, "1");
		ps.setInt(i++, 1);
		ps.setString(i++, data.getT1());
		ps.setString(i++, data.getT2());
		ps.setString(i++, data.getT3());
		ps.setInt(i++, data.getN1());
		ps.setInt(i++, data.getN2());
		ps.setInt(i++, data.getN3());

	}

	public ContactArr getContactListByPhoneNo(String ID,String phone, Connection conn) throws SQLException {
		ContactArr res=new ContactArr();
		Contact[] dataArr = null;
		ArrayList<Contact> dataList = new ArrayList<Contact>();
		String query="select * from (select  t1,t3,t16 from REGISTER where t1 NOT In (select Phone from Contact where UserID=? and RecordStatus<>4) and RecordStatus<>4)  t  where t1 in ("+phone+")";
		PreparedStatement ps = conn.prepareStatement(query);
		int j=1;
		ps.setString(j++, ID);
		ResultSet ret=ps.executeQuery();
		while(ret.next()){
			Contact data = new Contact();
			data.setT1(ret.getString("t1"));
			data.setT3(ret.getString("t3"));
			data.setT16(ret.getString("t16"));
			dataList.add(data);
		}
		if (dataList.size() > 0) {
			dataArr = new Contact[dataList.size()];
			for (int i = 0; i < dataList.size(); i++) {
				dataArr[i] = dataList.get(i);
			}
			res.setCode("0000");
			res.setDesc("Success");
			res.setDataList(dataArr);
		} else {
			res.setCode("0014");
			res.setDesc("No record found!");
		}
		return res;
	}
}
