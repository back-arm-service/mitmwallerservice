package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.nirvasoft.rp.data.PswPolicyData;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.ResultTwo;
import com.nirvasoft.rp.shared.ForceChangePwdReq;
import com.nirvasoft.rp.shared.ForceChangePwdResponse;
import com.nirvasoft.rp.shared.PwdData;
import com.nirvasoft.rp.util.ServerUtil;

public class PwdDao {

	public int ChangePwd(String userId, Connection l_Conn) throws SQLException {

		int rs = 0;
		String sql = "Update UVM012_A set n10=0 where t1=? and recordStatus <> 4  ";
		PreparedStatement stmt1 = l_Conn.prepareStatement(sql);
		stmt1.setString(1, userId);
		rs = stmt1.executeUpdate();
		if (rs > 0) {
			return rs;

		}
		return rs;
	}

	public boolean checkCurrentPassword(PwdData data, Connection l_Conn) throws SQLException {

		String l_Query = "SELECT * FROM Register WHERE RecordStatus <> 4 and userid = ?";
		String psw = "";

		PreparedStatement pstmt = l_Conn.prepareStatement(l_Query);
		pstmt.setString(1, data.getUserid());

		ResultSet rs = pstmt.executeQuery();

		while (rs.next()) {
			psw = rs.getString("t41");
		}

		if (ServerUtil.hashPassword(data.getPassword()).equals(psw))
			return true;
		else
			return false;
	}

	public boolean checkOldPassword(PwdData data, Connection l_Conn) throws SQLException {
		boolean isPass = false;
		String oldPassword = ServerUtil.hashPassword(data.getPassword());
		String sql = "select t41 from register where userid=? and RecordStatus<>4";
		PreparedStatement ps = l_Conn.prepareStatement(sql);
		ps.setString(1, data.getUserid());
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			String pass = rs.getString("t41");
			if (pass.equalsIgnoreCase(oldPassword)) {
				isPass = true;
			} else {
				isPass = false;
			}
			/*pass = ServerUtil.decryptPIN(pass);
			if (pass.equals(data.getPassword())) {
				data.getPassword();
			}else {
				isPass = false;
			}*/
		}
		ps.close();
		rs.close();
		return isPass;
	}

	public boolean checkvalidDate(String userid, Connection l_Conn) throws SQLException {
		boolean result = false;
		Date registerdate = new Date();
		Date sysdate = new Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String chksevenday = "", createDate = "";
		String duedate = ConnAdmin.readExternalUrl("DueDate");
		int subduedate = Integer.parseInt(duedate);

		String sql = "Select createddate from ForceChangePwd where userId=?";
		PreparedStatement pstmt = l_Conn.prepareStatement(sql);
		pstmt.setString(1, userid);
		ResultSet res = pstmt.executeQuery();
		while (res.next()) {
			createDate = res.getString("createddate");
		}

		// createdDate ="20170321"; // need to change
		chksevenday = createDate.substring(0, 4) + "-" + createDate.substring(4, 6) + "-" + createDate.substring(6, 8);

		try {
			registerdate = df.parse(chksevenday);
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(registerdate);
		cal.add(Calendar.DATE, subduedate);
		Date ValueDate = cal.getTime();

		if (ValueDate.equals(sysdate) || ValueDate.after(sysdate)) {
			// need to do something
			result = true;

		} else {
			System.out.println("Value Date after   :: " + ValueDate);
			result = false;
		}

		return result;
	}

	public ForceChangePwdResponse forcechangePassword(ForceChangePwdReq data, Connection l_Conn)
			throws SQLException, ParseException {
		String userid = data.getUserID();
		ForceChangePwdResponse ret = new ForceChangePwdResponse();
		SessionDAO s_Dao = new SessionDAO();

		if (ForceChangePwdcheck(data.getOldPassword(), userid, l_Conn)) {

			if (data.getNewPassword().length() < 6 || data.getNewPassword().length() > 20) {
				ret.setCode("0014");
				ret.setDesc("Password length must be between 6 and 20 characters");
			} else {

				String query = "UPDATE UVM005_A SET  [t2]=?  WHERE RecordStatus <> 4 and ([t1]=? COLLATE SQL_Latin1_General_CP1_CS_AS )";
				PreparedStatement pstmt = l_Conn.prepareStatement(query);
				pstmt.setString(1, ServerUtil.encryptPIN(data.getNewPassword()));
				pstmt.setString(2, userid);

				if (pstmt.executeUpdate() > 0) {
					if (ChangePwd(userid, l_Conn) > 0) { // need to change
						s_Dao.updateUserSession(userid, l_Conn);// Kill Session

						String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
						String logoutquery = "UPDATE tblSession SET Status = ?, LastActivityDateTime = ?, LogOutDateTime = ? WHERE SessionID = ?";
						PreparedStatement pstmt1 = l_Conn.prepareStatement(logoutquery);
						int j = 1;
						pstmt1.setInt(j++, 9);
						pstmt1.setString(j++, date);
						pstmt1.setString(j++, date);
						pstmt1.setString(j++, data.getSessionID());
						int rs = pstmt1.executeUpdate();
						if (rs > 0) {
							ret.setCode("0000");
							ret.setDesc("Updated successfully");
						}

					}

				} else {
					ret.setCode("0014");
					ret.setDesc("Updated Failed");
				}
			}

		} else {
			ret.setCode("0014");
			ret.setDesc("Current Password Doesn't Exist!");
		}

		return ret;
	}

	public boolean ForceChangePwdcheck(String oldPwd, String userid, Connection l_Conn) throws SQLException {
		boolean response = false;

		String l_Query = "SELECT Top(1) t2 FROM UVM005_A WHERE RecordStatus <> 4 And t2 = ? and ([t1]=? COLLATE SQL_Latin1_General_CP1_CS_AS)";
		PreparedStatement pstmt = l_Conn.prepareStatement(l_Query);
		pstmt.setString(1, ServerUtil.encryptPIN(oldPwd));
		pstmt.setString(2, userid);

		ResultSet rs = pstmt.executeQuery();

		while (rs.next()) {

			rs.getString("t2");
			response = true;
		}

		return response;
	}

	public void LockUserAcc(String userName, Connection con) {
		try {
			String sql = "UPDATE UVM005_A SET n1=?,n7=? WHERE recordStatus <> 4 and (t1=? COLLATE SQL_Latin1_General_CP1_CS_AS)";
			PreparedStatement stmt = con.prepareStatement(sql);
			int i = 1;
			stmt.setInt(i++, 3);
			stmt.setInt(i++, 11);
			stmt.setString(i++, userName);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				sql = "UPDATE UVM012_A SET n1=?,n7=? WHERE recordStatus <> 4 and syskey = (Select n4 from UVM005_A where t1=?)";
				stmt = con.prepareStatement(sql);
				stmt.setString(1, userName);
				int j = 1;
				stmt.setInt(j++, 3);
				stmt.setInt(j++, 11);
				rs = stmt.executeUpdate();
				if (rs > 0) {
					System.out.println("update (Lock) successfully");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public PswPolicyData readPswPolicy(Connection con) throws SQLException {
		PswPolicyData data = new PswPolicyData();
		String sql = "SELECT * FROM tblPasswordpolicy";
		PreparedStatement stat = con.prepareStatement(sql);
		ResultSet result = stat.executeQuery();
		while (result.next()) {
			data.setPswminlength(result.getInt("MinimumLength"));
			data.setPswmaxlength(result.getInt("MaximumLength"));
			data.setSpchar(result.getInt("SpecialCharacter"));
			data.setUpchar(result.getInt("UpperCaseCharacter"));
			data.setLowerchar(result.getInt("LowerCaseCharacter"));
			data.setPswno(result.getInt("Number"));
		}
		return data;
	}

	public ResultTwo resetPassword(PwdData data, Connection l_Conn) throws SQLException {
		ResultTwo ret = new ResultTwo();
		if (data.getNewpassword().length() < 6 || data.getNewpassword().length() > 20) {
			ret.setMsgCode("0014");
			ret.setMsgDesc("Password length must be between 6 and 20 characters");
			ret.setState(false);
		} else {
			String query = "UPDATE Register SET [t41]=? WHERE RecordStatus<>4 and [userid]=?";
			PreparedStatement pstmt = l_Conn.prepareStatement(query);
			pstmt.setString(1, ServerUtil.hashPassword(data.getNewpassword()));
			pstmt.setString(2, data.getUserid());
			if (pstmt.executeUpdate() > 0) {
				ret.setMsgCode("0000");
				ret.setMsgDesc("Updated successfully. Please login again.");
				ret.setState(true);
			} else {
				ret.setMsgDesc("Updated fail.");
				ret.setMsgCode("0014");
				ret.setState(false);
			}
		}
		return ret;
	}

	public ResultTwo updatePwdData(PwdData data, Connection l_Conn) throws SQLException {
		ResultTwo ret = new ResultTwo();

		if (checkCurrentPassword(data, l_Conn)) {
			if (data.getPassword().equals(data.getNewpassword())) {
				ret.setMsgCode("0014");
				ret.setMsgDesc("New Password should not be the same as current password");
				ret.setState(false);
			} else if (data.getNewpassword().length() < 6 || data.getNewpassword().length() > 20) {
				ret.setMsgCode("0014");
				ret.setMsgDesc("Password length must be between 6 and 20 characters");
				ret.setState(false);
			} else {
				String query = "UPDATE Register SET [t41]=? WHERE RecordStatus<>4 and [userid]=?";
				PreparedStatement pstmt = l_Conn.prepareStatement(query);
				pstmt.setString(1, ServerUtil.hashPassword(data.getNewpassword()));
				pstmt.setString(2, data.getUserid());

				if (pstmt.executeUpdate() > 0) {
					ret.setMsgCode("0000");
					ret.setMsgDesc("Updated successfully. Please login again.");
					ret.setState(true);
				} else {
					ret.setMsgDesc("Updated fail.");
					ret.setMsgCode("0014");
					ret.setState(false);
				}
			}

		} else {
			ret.setMsgDesc("Current password doesn't exist!");
			ret.setMsgCode("0014");
			ret.setState(false);
		}

		return ret;
	}
}
