package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EPIXWebServiceDAO {

	public boolean isUsedWebService(Connection conn) throws SQLException {
		boolean rt = false;
		String sql = "select count(*) as count from CMSModuleConfig where ActiveBank = 1  and DBSource = 'Y' ";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			if (rs.getInt("count") == 0) {
				rt = false;
			} else {
				rt = true;
			}
		}
		ps.close();
		rs.close();
		return rt;
	}
}
