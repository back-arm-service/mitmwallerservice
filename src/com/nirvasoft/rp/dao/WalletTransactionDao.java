package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.rp.data.UtilityResponse;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.shared.CardData;
import com.nirvasoft.rp.shared.GoPaymentResponse;
import com.nirvasoft.rp.shared.MerchantListData;
import com.nirvasoft.rp.shared.SessionData;
import com.nirvasoft.rp.shared.TransferOutRes;
import com.nirvasoft.rp.shared.WalletTransaction;

public class WalletTransactionDao {
	public static String mTableName = "WalletTransaction";

	public ArrayList<WalletTransaction> getNoti(String userID, int notiCount, Connection conn) throws SQLException {
		ArrayList<WalletTransaction> ret = new ArrayList<WalletTransaction>();
		int topCount = 10;
		if (notiCount > topCount) {
			topCount = notiCount;
		}
		String sql = "select top " + topCount + " AutoKey,UserID, NotiMessage, NotiType, NotiKey from " + mTableName //,CreatedDate
				+ " where userID = ? and TransStatus = 2 order by Autokey desc ";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, userID);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			WalletTransaction data = new WalletTransaction();
			data = readData(rs, data);
			ret.add(data);
		}
		return ret;
	}

	public int getNotiCount(String userID, long notiKey, Connection conn) throws SQLException {
		int ret = 0;
		String sql = "select count(AutoKey) from " + mTableName
				+ " where userID = ? and AutoKey > ?  and TransStatus = 2";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, userID);
		ps.setLong(2, notiKey);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			ret = rs.getInt(1);
		}
		return ret;
	}

	public String getUserByTransID(String transID, Connection conn) throws SQLException {
		String ret = "";
		String sql = "select UserID from WalletTransaction where TransID = ? and TransStatus = 1";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, transID);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ret = rs.getString(1);
		}
		return ret;
	}

	WalletTransaction readData(ResultSet rs, WalletTransaction data) throws SQLException {
		int i = 1;
		data.setAutoKey(rs.getLong(i++));
		data.setUserID(rs.getString(i++));
		data.setNotiMessage(rs.getString(i++));
		data.setNotiType(rs.getString(i++));
		data.setNotiKey(rs.getInt(i++));
		//data.setCreatedDate(rs.getString(i++));
		return data;
	}

	public boolean save(WalletTransaction data, Connection conn) throws SQLException {
		boolean ret = false;
		String sql = "INSERT INTO " + mTableName + " ([CreatedDate],[ModifiedDate],[UserID],[FromWallet],[ToWallet],"
				+ "[FromOperator],[ToOperator],[FromAccount],[ToAccount],[Amount],[BankCharges],[CommissionCharges],[TransID],"
				+ "[SwitchKey],[RefKey],[TransType],[TransStatus],[Code],[Description],[NotiMessage],[NotiType],[NotiKey],[TransDate],"
				+ "[T1],[T2],[T3],[N1],[N2],[N3]) VALUES"
				+ "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement ps = conn.prepareStatement(sql);
		updateData(ps, data);
		if (ps.executeUpdate() > 0) {
			ret = true;
		}
		return ret;
	}
	//atn
	public boolean saveWalletTrans(WalletTransaction data,Connection pConn) throws SQLException {
		boolean ret = false;
		
		String sql = "INSERT INTO " + mTableName + " ([CreatedDate],[ModifiedDate],[UserID],[FromWallet],[ToWallet],"
				+ "[FromOperator],[ToOperator],[FromAccount],[ToAccount],[Amount],"
				+ "[BankCharges],[CommissionCharges],[TransID],[SwitchKey],[RefKey],"
				+ "[TransType],[TransStatus],[Code],[Description],[NotiMessage],"
				+ "[NotiType],[NotiKey],[TransDate],[MerchantId],[T1],"
				+ "[T2],[T3],[N1],[N2],[N3],"
				+ "[ToDescription],[DueDate],[OtherStatus]) "
				+ " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
				+ "?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
				+ "?, ?, ?, ?, ?, ?, ?, ?, ?,?,"
				+ "?,?,?)";
		PreparedStatement ps = pConn.prepareStatement(sql);
		updateData(ps, data);
		if (ps.executeUpdate() > 0) {
			ret = true;
		}
		return ret;
	}

	public boolean update(String transID, String switchKey, String refKey, String userID, String toName,
			Connection conn) throws SQLException {
		boolean ret = false;
		int tranStatus = 2;
		String sql = "update WalletTransaction set TransID = ?, SwitchKey = ?, RefKey = ?, TransStatus = ? , t2 = ? "
				+ "where UserID = ? and TransID = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setString(i++, transID);
		ps.setString(i++, switchKey);
		ps.setString(i++, refKey);
		ps.setInt(i++, tranStatus);
		ps.setString(i++, toName);
		ps.setString(i++, userID);
		ps.setString(i++, transID);
		if (ps.executeUpdate() > 0) {
			ret = true;
		}
		return ret;
	}
	
	public boolean updateForSkynet(GoPaymentResponse res,String bankCharges, String cardNo, String packageName, String vouncherType,String userID,String transID, Connection conn) throws SQLException {
		boolean ret = false;
		String sql = "update WalletTransaction set bankcharges = ?, RefKey = ? , otherresponsecode = ?"
				+ ", otherresponsedesc = ?, skynetcardno = ?, skynetpackage = ?, skynetvouchertype = ? where UserID = ? and TransID = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setString(i++, bankCharges);
		ps.setString(i++, res.getBankRefNumber());
		ps.setString(i++, res.getOtherResponseCode());
		ps.setString(i++, res.getOtherResponseDesc());
		ps.setString(i++, cardNo);
		ps.setString(i++, packageName);
		ps.setString(i++, vouncherType);
		ps.setString(i++, userID);
		ps.setString(i++, transID);
		if (ps.executeUpdate() > 0) {
			ret = true;
		}
		return ret;
	}
	
	public boolean updateForUtility(UtilityResponse res,String userID,String transID, Connection conn) throws SQLException {
		boolean ret = false;
		String sql = "update WalletTransaction set RefKey= ?, otherresponsecode = ?, otherresponsedesc = ?, otherStatus= ? where UserID = ? and TransID = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setString(i++, res.getiCBStxnRef());
		ps.setString(i++, res.getCode());
		ps.setString(i++, res.getDesc());
		ps.setString(i++, res.getMessage());
		ps.setString(i++, userID);
		ps.setString(i++, transID);
		if (ps.executeUpdate() > 0) {
			ret = true;
		}
		return ret;
	}

	private void updateData(PreparedStatement ps, WalletTransaction data) throws SQLException {
		int i = 1;
		ps.setString(i++, data.getCreatedDate());
		ps.setString(i++, data.getModifiedDate());
		ps.setString(i++, data.getUserID());
		ps.setString(i++, data.getFromWallet());
		ps.setString(i++, data.getToWallet());
		ps.setString(i++, data.getFromOperator());
		ps.setString(i++, data.getToOperator());
		ps.setString(i++, data.getFromAccount());
		ps.setString(i++, data.getToAccount());
		ps.setDouble(i++, data.getAmount());
		ps.setDouble(i++, data.getBankCharges());
		ps.setDouble(i++, data.getCommissionCharges());
		ps.setString(i++, data.getTransID());
		ps.setString(i++, data.getSwitchKey());
		ps.setString(i++, data.getRefKey());
		ps.setString(i++, data.getTransType());
		ps.setInt(i++, data.getTransStatus());
		ps.setString(i++, data.getCode());
		ps.setString(i++, data.getDesc());
		ps.setString(i++, data.getNotiMessage());
		ps.setString(i++, data.getNotiType());
		ps.setInt(i++, data.getNotiKey());
		ps.setString(i++, data.getTransDate());
		ps.setString(i++, data.getMerchantID());//atn
		ps.setString(i++, data.getT1());
		ps.setString(i++, data.getT2());
		//ps.setString(i++, data.getT3());
		ps.setString(i++, data.getEffectiveDate());
		ps.setInt(i++, data.getN1());
		ps.setInt(i++, data.getN2());
		ps.setInt(i++, data.getN3());
		//ps.setString(i++, data.getEffectiveDate());//Effectivedate
		//ps.setString(i++, "");//TransTime
		ps.setString(i++, data.getToDescription());//ToDescription
		ps.setString(i++, data.getDueDate());//DueDate
		ps.setString(i++, "");//OtherStatus
		//ps.setString(i++, data.getPendltyaccount());//Pendltyaccount
		//ps.setString(i++, data.getPendltyamount());//Pendltyaccount
		//ps.setString(i++, data.getDiscountAmount());//DiscountAmount
		
	}
	
	// atn
	public boolean checkCardNo(String cardNo, String userID, Connection pConn) throws SQLException {
		boolean ret = false;
		String sql = "Select cardNo from ContactCardNo where cardNo = ? And t1=?";
		PreparedStatement ps = pConn.prepareStatement(sql);
		ps.setString(1, cardNo);
		ps.setString(2, userID);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ret = true;
		}
		return ret;
	}

	// atn
	public boolean addContactCardNo(SessionData req, Connection pConn) throws SQLException {
		boolean ret = false;
		String sql = "Insert Into ContactCardNo([CardNo],[ContactName],[T1],[T2],[T3],[N1],[N2]) Values(?,?,?,?,?,?,?)";
		PreparedStatement ps = pConn.prepareStatement(sql);
		ps.setString(1, req.getT1());//cardno	
		ps.setString(2, req.getT2());//name
		ps.setString(3, req.getUserID());//userid
		ps.setString(4, "");
		ps.setString(5, "");
		ps.setInt(6, 0);
		ps.setInt(7, 0);
		if (ps.executeUpdate() > 0) {
			ret = true;
		}
		return ret;
	}
	
	// atn
	public TransferOutRes getCardNo(SessionData req, Connection pConn) throws SQLException {
		TransferOutRes ret = new TransferOutRes();
		ArrayList<CardData> cardList = new ArrayList<CardData>();
		String sql = "select CardNo,ContactName from ContactCardNo where t1 =?";
		PreparedStatement ps = pConn.prepareStatement(sql);
		ps.setString(1, req.getUserID());
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			CardData data = new CardData();
			data.setCardNo(rs.getString("CardNo"));
			data.setName(rs.getString("ContactName"));
			cardList.add(data);
		}
		CardData[] cardArr = new CardData[cardList.size()];
		for (int i = 0; i < cardList.size(); i++) {
			cardArr[i] = cardList.get(i);
		}
		ret.setCadData(cardArr);
		return ret;
	}

}
