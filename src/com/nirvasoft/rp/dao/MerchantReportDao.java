package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import com.nirvasoft.rp.framework.Lov3;
import com.nirvasoft.rp.framework.Ref;
import com.nirvasoft.rp.shared.DetailsData;
import com.nirvasoft.rp.shared.DispaymentList;
import com.nirvasoft.rp.shared.DispaymentTransactionData;
import com.nirvasoft.rp.shared.LOVSetupDetailData;
import com.nirvasoft.rp.shared.LoginUserMerchantInfo;
import com.nirvasoft.rp.shared.PayTemplateDetail;
import com.nirvasoft.rp.shared.PayTemplateDetailArr;
import com.nirvasoft.rp.shared.PayTemplateHeader;
import com.nirvasoft.rp.util.GeneralUtil;

public class MerchantReportDao {

	public LOVSetupDetailData[] getLOVbyHKey(long sysKey, String processingCode, Connection Conn) throws SQLException {

		int srno = 1;
		ArrayList<LOVSetupDetailData> datalist = new ArrayList<LOVSetupDetailData>();

		String query = "";
		PreparedStatement pstmt = null;
		;
		query = "SELECT Code, Description from LOVDetails where HKey = ?";
		pstmt = Conn.prepareStatement(query);
		pstmt.setLong(1, sysKey);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			LOVSetupDetailData data = new LOVSetupDetailData();
			data.setSrno(String.valueOf(srno++));
			data.setLovCde(rs.getString("Code"));
			data.setLovDesc1(rs.getString("Description"));

			if (processingCode.equalsIgnoreCase("02")) {
				data.setLovDesc2(getMerchantNameById(data.getLovCde(), Conn));
			}
			datalist.add(data);
		}
		LOVSetupDetailData[] dataarry = new LOVSetupDetailData[datalist.size()];
		for (int i = 0; i < datalist.size(); i++) {
			dataarry[i] = datalist.get(i);
		}
		return dataarry;

	}

	public Lov3 getMerchantIDListDetail(Connection conn, String User) throws SQLException {

		Lov3 lov3 = new Lov3();
		Ref[] refarray = null;
		ArrayList<Ref> reflist = new ArrayList<Ref>();
		System.out.println("user is; " + User);
		PreparedStatement stmt = conn.prepareStatement(
				"select p.MerchantID,p.MerchantName,p.ProcessingCode from CMSMerchant c, PayTemplateHeader p where c.UserId = p.MerchantID and c.RecordStatus <> 4 order by p.MerchantName");

		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			Ref ref = new Ref();
			ref.setvalue(rs.getString("MerchantID"));
			ref.setcaption(rs.getString("MerchantName"));
			reflist.add(ref);
		}

		stmt.close();
		rs.close();

		refarray = new Ref[reflist.size()];
		for (int i = 0; i < reflist.size(); i++) {
			refarray[i] = reflist.get(i);
		}
		System.out.println("Dao getmerchant" + refarray.length);
		lov3.setRef015(refarray);
		System.out.println("Dao getmerchant lov" + lov3.getRef015().length);

		return lov3;

	}

	public DispaymentList getMerchantList(int currentPage, int totalCount, int pageSize, String aMerchantID,
			String status, String aFromDate, String aToDate, String aFromAcc, String aToAcc, String did,
			String initiatedby, String transrefno, String processingCode, String proDisCode,
			PayTemplateDetail[] templateData, Connection conn) throws SQLException {
		DispaymentList res = new DispaymentList();
		ArrayList<DispaymentTransactionData> datalist = new ArrayList<DispaymentTransactionData>();

		int l_startRecord = (currentPage - 1) * pageSize;
		int l_endRecord = l_startRecord + pageSize;

		PayTemplateDetailArr[] DetailArray = null;
		GeneralUtil.readDataConfig();
		if (processingCode.equalsIgnoreCase("02")) {
		} else {
		}

		String aCriteria = "";
		if (!aFromDate.trim().equals("")) {
			aCriteria += " and d.TransDate >= ?";
		}

		if (!aToDate.trim().equals("")) {
			aCriteria += " and d.TransDate <= ?";
		}
		if (!aFromAcc.trim().equals("")) {
			aCriteria += " and FromAccount = ?";
		}

		if (!aToAcc.trim().equals("")) {
			aCriteria += " and ToAccount = ?";
		}

		if (!did.trim().equals("")) {
			aCriteria += " and CustomerCode = ?";
		}

		if (!status.trim().equalsIgnoreCase("ALL")) {
			if (status.trim().equalsIgnoreCase("SUCCESS")) {
				aCriteria += " and d.t14 = '1'";
			} else if (status.trim().equalsIgnoreCase("Reverse")) {
				aCriteria += " and d.t14 <> '1' and d.n2 = '4'";
			} else {// FAIL
				aCriteria += " and d.t14 <> '1'";
			}
		}

		if (!transrefno.trim().equals("")) {
			aCriteria += " and d.FlexRefNo = ?";
		}

		String l_Query = "";
		String l_Query_Total = "";
		String GT = "";

		l_Query = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum, d.*,"
				+ "d.CustomerCode AS DID FROM  DispaymentTransaction d, PayTemplateHeader p where p.ProcessingCode = '080400' and p.MerchantID = ? and d.T20 = ?"
				+ " And d.n3=4 " + aCriteria + ") As r  WHERE 1=1 and RowNum > " + l_startRecord + " and RowNum <= "
				+ l_endRecord;

		l_Query_Total = "select sum(GTA) as grandtotalAmount  from (select Amount as GTA"
				+ " from ( SELECT ROW_NUMBER() OVER (ORDER BY d.autokey desc) AS RowNum,"
				+ "d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d, PayTemplateHeader p"
				+ " where p.ProcessingCode = '080400' and p.MerchantID = ? And d.T20 =?" + " And d.n3=4 " + aCriteria
				+ ") As r) as g";

		PreparedStatement stmtTotal = conn.prepareStatement(l_Query_Total);
		int i = 1;
		stmtTotal.setString(i++, aMerchantID);
		stmtTotal.setString(i++, aMerchantID);
		if (!aFromDate.trim().equals("")) {
			stmtTotal.setString(i++, aFromDate.trim());
		}

		if (!aToDate.trim().equals("")) {
			stmtTotal.setString(i++, aToDate.trim());
		}
		if (!aFromAcc.trim().equals("")) {
			stmtTotal.setString(i++, aFromAcc.trim());
		}

		if (!aToAcc.trim().equals("")) {
			stmtTotal.setString(i++, aToAcc.trim());
		}

		if (!did.trim().equals("")) {
			stmtTotal.setString(i++, did.trim());
		}

		if (!transrefno.trim().equals("")) {
			stmtTotal.setString(i++, transrefno.trim());
		}
		System.out.println("Query1..." + l_Query);
		System.out.println("Query2... " + l_Query_Total);
		ResultSet rsTotal = stmtTotal.executeQuery();
		while (rsTotal.next()) {
			GT = rsTotal.getString("grandtotalAmount");

			if (GT != null) {

				DecimalFormat df = new DecimalFormat("###,000.00");
				String formatted = df.format(Double.parseDouble(GT));

				res.setGrandTotal(formatted);
				System.out.println("GrandTotal" + formatted);
			}

		}

		PreparedStatement stmt = conn.prepareStatement(l_Query);
		int j = 1;
		stmt.setString(j++, aMerchantID);
		stmt.setString(j++, aMerchantID);
		if (!aFromDate.trim().equals("")) {
			stmt.setString(j++, aFromDate.trim());
		}

		if (!aToDate.trim().equals("")) {
			stmt.setString(j++, aToDate.trim());
		}
		if (!aFromAcc.trim().equals("")) {
			stmt.setString(j++, aFromAcc.trim());
		}

		if (!aToAcc.trim().equals("")) {
			stmt.setString(j++, aToAcc.trim());
		}

		if (!did.trim().equals("")) {
			stmt.setString(j++, did.trim());
		}

		if (!transrefno.trim().equals("")) {
			stmt.setString(j++, transrefno.trim());
		}
		System.out.println("Query1..." + l_Query);
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			DispaymentTransactionData data = new DispaymentTransactionData();
			String aTransDate = "";
			aTransDate = rs.getString("TransDate");
			data.setTransactiondate(
					aTransDate.substring(6) + "/" + aTransDate.substring(4, 6) + "/" + aTransDate.substring(0, 4));
			data.setSyskey(rs.getLong("Autokey"));
			data.setTranrefnumber(rs.getString("FlexRefNo"));
			data.setActivebranch(rs.getString("FromBranch"));
			data.setDebitaccount(rs.getString("FromAccount"));
			data.setDebitbranch(rs.getString("T1"));
			data.setCollectionaccount(rs.getString("ToAccount"));
			data.setBranch("Online");

			Double amount = rs.getDouble("Amount");
			data.setAmountst(String.format("%,.2f", amount));
			data.setAmount(rs.getDouble("Amount"));
			if (!(rs.getString("t15").equals(""))) {
				data.setT15(Double.valueOf(rs.getString("t15")));
			}
			if (!(rs.getString("t18").equals(""))) {
				data.setT18(Double.valueOf(rs.getString("t18")));
			}

			data.setCommchargest(String.format("%,.2f", data.getT15()));
			data.setCommuchargest(String.format("%,.2f", data.getT18()));

			data.setDid(rs.getString("CustomerCode"));
			data.setCustomername(rs.getString("CustomerName"));
			data.setDescription(rs.getString("t16"));

			if (proDisCode.equalsIgnoreCase("01")) {
				data.setDistributorName(rs.getString("DistributorName"));
				data.setDistributorAddress(rs.getString("DistributorAddress"));
			}

			data.setMessageCode(rs.getString("t14"));// messagecode
			data.setMessageDesc(rs.getString("t12"));// messagedescription
			datalist.add(data);

		}
		res.setDetailData(DetailArray);
		res.setPageSize(pageSize);
		res.setCurrentPage(currentPage);
		String countSQL = "";
		countSQL = "select COUNT(*) as recCount from ( SELECT  d.*, d.CustomerCode AS DID FROM  DispaymentTransaction d  where 1=1 and d.t20 = ? ";

		countSQL += "  And d.n3=4  " + aCriteria + " ) As r ";

		PreparedStatement stat = conn.prepareStatement(countSQL);
		int k = 1;
		stat.setString(k++, aMerchantID);
		if (!aFromDate.trim().equals("")) {
			stat.setString(k++, aFromDate.trim());
		}

		if (!aToDate.trim().equals("")) {
			stat.setString(k++, aToDate.trim());
		}
		if (!aFromAcc.trim().equals("")) {
			stat.setString(k++, aFromAcc.trim());
		}

		if (!aToAcc.trim().equals("")) {
			stat.setString(k++, aToAcc.trim());
		}

		if (!did.trim().equals("")) {
			stat.setString(k++, did.trim());
		}

		if (!transrefno.trim().equals("")) {
			stat.setString(k++, transrefno.trim());
		}
		System.out.println("Count.." + countSQL);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		DispaymentTransactionData[] dataarry = new DispaymentTransactionData[datalist.size()];
		dataarry = datalist.toArray(dataarry);
		res.setData(dataarry);

		return res;
	}

	public String getMerchantNameById(String merchantId, Connection pConn) throws SQLException {
		String merName = "";
		PreparedStatement pstmt = null;
		;
		String query = "SELECT userName from CMSMerchant  where userId = ?";
		pstmt = pConn.prepareStatement(query);
		pstmt.setString(1, merchantId);
		ResultSet rs = pstmt.executeQuery();
		if (rs.next()) {
			merName = rs.getString("userName");
		}
		return merName;
	}

	public PayTemplateHeader getPaymentTemplateByID(String merchantID, String saveDirectory, Connection l_Conn)
			throws SQLException {
		PayTemplateHeader ret = new PayTemplateHeader();
		int count = 0;
		GeneralUtil.readDataConfig();

		String query = "select count(*) c from PayTemplateHeader H,PayTemplateDetail D where H.SysKey = D.HKey and FieldShow in (4,5,6) AND H.MerchantId = ?";
		PreparedStatement pstmt = l_Conn.prepareStatement(query);

		pstmt.setString(1, merchantID);
		ResultSet rs = pstmt.executeQuery();

		if (rs.next()) {
			count = rs.getInt("c");
		}
		query = "select H.SysKey,H.MerchantID ,H.MerchantName,H.ProcessingCode,H.Logo,"
				+ "D.HKey,D.FieldID,D.FieldOrder,D.DataType,D.Caption,D.DisPayField,D.LovKey, D.RegExpKey ,D.IsMandatory, H.DetailOrder "
				+ "from PayTemplateHeader H inner join PayTemplateDetail D "
				+ "on H.SysKey = D.HKey and FieldShow in (4,5,6) AND H.MerchantID=? order by D.FieldOrder ";

		pstmt = l_Conn.prepareStatement(query);
		System.out.println("getPaymentTemplaate by id in dao: " + query);
		pstmt.setString(1, merchantID);
		rs = pstmt.executeQuery();

		PayTemplateDetail[] detailData = new PayTemplateDetail[count];
		int index = 0;

		while (rs.next()) {
			ret.setSysKey(rs.getLong("SysKey"));
			ret.setMerchantID(rs.getString("MerchantID"));
			ret.setName(rs.getString("MerchantName"));
			ret.setProcessingCode(rs.getString("ProcessingCode").substring(0, 2));
			ret.setProNotiCode(rs.getString("ProcessingCode").substring(2, 4));
			ret.setProDisCode(rs.getString("ProcessingCode").substring(4, 6));
			// ret.setLogo(FileUtil.getPhoto(saveDirectory,rs.getBytes("Logo"),l_Conn));
			ret.setDetailOrder(rs.getString("DetailOrder"));

			PayTemplateDetail detail = new PayTemplateDetail();
			detail.setHkey(rs.getLong("HKey"));
			detail.setFieldID(rs.getString("FieldID"));
			detail.setFieldOrder(rs.getInt("FieldOrder"));
			detail.setCaption(rs.getString("Caption"));
			detail.setDataType(rs.getString("DataType"));
			detail.setDisPayField(rs.getString("DisPayField"));
			String regExp = rs.getString("RegExpKey");
			detail.setRegExpKey(regExp.trim());
			detail.setIsMandatory(rs.getInt("IsMandatory"));

			long lovkey = rs.getLong("LovKey");
			detail.setLovKey(lovkey);
			if (lovkey != 0) {
				detail.setLovData(getLOVbyHKey(lovkey, ret.getProcessingCode(), l_Conn));
			}

			detailData[index] = detail;
			index++;
		}
		ret.setData(detailData);

		if (!ret.getDetailOrder().equals("")) {

			String sql = "";
			int srno = 1;
			long headerSyskey = (long) 0.0;
			ArrayList<LOVSetupDetailData> datalist = new ArrayList<LOVSetupDetailData>();
			sql = "SELECT syskey from LOVHeader where Description = 'Items'";
			pstmt = l_Conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				headerSyskey = rs.getLong("syskey");
			}
			;
			sql = "SELECT Code, Description, Price from LOVDetails where HKey = ?";
			pstmt = l_Conn.prepareStatement(sql);
			pstmt.setLong(1, headerSyskey);

			rs = pstmt.executeQuery();
			while (rs.next()) {
				LOVSetupDetailData data = new LOVSetupDetailData();
				data.setSrno(String.valueOf(srno++));
				data.setLovCde(rs.getString("Code"));
				data.setLovDesc1(rs.getString("Description"));
				data.setPrice(rs.getDouble("Price"));

				datalist.add(data);
			}
			LOVSetupDetailData[] dataarry = new LOVSetupDetailData[datalist.size()];
			for (int i = 0; i < datalist.size(); i++) {
				dataarry[i] = datalist.get(i);
			}
			ret.setDetailLov(dataarry);
		}
		return ret;
	}

	public ArrayList<DetailsData> getshowdetails(String syskey, Connection pConn) throws SQLException {

		ArrayList<DetailsData> ret = new ArrayList<DetailsData>();
		String l_Query = "select  * from DisPaymentTransDetail where hkey = ?";

		PreparedStatement pstmt = pConn.prepareStatement(l_Query);

		pstmt.setString(1, syskey);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			DetailsData data = new DetailsData();
			data.setSysKey(rs.getLong("Syskey"));
			data.setHkey(rs.getLong("Hkey"));
			data.setSrno(rs.getInt("SrNo"));
			data.setCode(rs.getString("Code"));
			data.setDescription(rs.getString("Description"));
			data.setPrice(rs.getDouble("Price"));
			data.setQuantity(rs.getInt("Qty"));
			data.setAmount(rs.getDouble("Amount"));
			ret.add(data);
		}
		return ret;
	}

	public LoginUserMerchantInfo LoginUserMerchantInfo(String loginUser, Connection conn) throws SQLException {
		LoginUserMerchantInfo data = new LoginUserMerchantInfo();
		String query = "select * from CMSMerchant";
		PreparedStatement pstmt = conn.prepareStatement(query);
		ResultSet res = pstmt.executeQuery();
		if (res.next()) {
			data.setLoginUser(loginUser);
			data.setMerchantId(res.getString("userid"));
		}
		return data;
	}
}
