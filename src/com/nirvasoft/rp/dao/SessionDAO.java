
package com.nirvasoft.rp.dao;

import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.mgr.SessionMgr;
import com.nirvasoft.rp.shared.CheckOTPRequestData;
import com.nirvasoft.rp.shared.MSigninResponseData;
import com.nirvasoft.rp.shared.MobileLogoutResponseData;
import com.nirvasoft.rp.shared.OTPReq;
import com.nirvasoft.rp.shared.OTPRes;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.shared.SessionData;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ServerUtil;

public class SessionDAO {

	private String mTableName = "tblSession";
	private String mTableName1="OTPSession";

	public MSigninResponseData checkLogin(String userId, Connection conn) throws SQLException, ParseException {
		MSigninResponseData response = new MSigninResponseData();
		String lastlogintimesuccess = "";
		String max_time = "";
		int status = -1;
		int res = 0;
		String date = "";
		String lastActivity_dtime = "";
		String sql = "SELECT * FROM " + mTableName
				+ "  WHERE Status <> 8 AND UserID = ? AND LogInDateTime=(SELECT MAX(LogInDateTime) FROM " + mTableName
				+ " WHERE Status <> 8 AND (UserID = ? COLLATE SQL_Latin1_General_CP1_CS_AS))";
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setString(i++, userId);
		ps.setString(i++, userId);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			status = rs.getInt("Status");
			lastActivity_dtime = rs.getString("LastActivityDateTime");
			date = rs.getString("LogInDateTime");
		}
		if (!date.equalsIgnoreCase("")) {
			lastlogintimesuccess = date.substring(8, 10) + "-" + date.substring(5, 7) + "-" + date.substring(0, 4) + " "
					+ date.substring(11, 19) + "";
		}
		if (status != 0) {
			response.setCode("0000");
		}
		if (status == 0) {
			max_time = lastActivity_dtime.replace("-", "").substring(0, 17);
			String currentdtime = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
			String sessionLimit = ConnAdmin.readExternalUrl("SESSIONTIME");
			if (checkTimeStamp(max_time, currentdtime, sessionLimit) == true)
			{
				response.setCode("0014");
			} else {
				res = forceLogout(userId, conn); // force logout -- user can
													// signin
				if (res > 0) // Updating status for force logout succeed
				{
					response.setCode("0000");
				} else // Updating status for force logout failed
				{
					response.setCode("0014");
				}
			}
		}
		response.setLastTimeLoginSuccess(lastlogintimesuccess);
		ps.close();
		rs.close();
		return response;
	}

	public ResponseData checkOTPCode(String sessionID, String userID, String rKey, String otpCode, Connection conn)
			throws SQLException, ParseException {
		String sql = "";
		String query = "";
		int count = 0;
		int status = 0;
		boolean timeflag = false;
		ResponseData response = new ResponseData();
		long referenceKey = Long.parseLong(rKey);
		String userid = "", sessionid = "", otpcode = "", getotpdtime = "";
//		sql = "SELECT UserID,SessionID,OTPCode,GetOTPDateTime,Count FROM OTPSession WHERE AutoKey = ? and OTPCode = ?";
		sql = "SELECT UserID,SessionID,OTPCode,GetOTPDateTime,Count FROM OTPSession WHERE AutoKey = ? ";//ndh
		PreparedStatement pstmt = conn.prepareStatement(sql);
		int i = 1;
		pstmt.setLong(i++, referenceKey);
//		pstmt.setString(i++, otpCode);//ndh
		ResultSet result = pstmt.executeQuery();
		while (result.next()) {
			userid = result.getString("UserID");
			sessionid = result.getString("SessionID");
			otpcode = result.getString("OTPCode");
			getotpdtime = result.getString("GetOTPDateTime");
			count = result.getInt("Count");
		}
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		/*if (userid.equalsIgnoreCase(userID) && sessionid.equalsIgnoreCase(sessionID)
				&& otpcode.equalsIgnoreCase(otpCode)) {
			String dtime1 = getotpdtime.replace("-", "");// getotp datetime
			String dtime2 = date.replace("-", ""); // current datetime
			String otpsessionLimit = ConnAdmin.readExternalUrl("OTPSESSIONTIME");
			timeflag = checkTimeStamp(dtime1, dtime2, otpsessionLimit);
			if (timeflag == true) {
				status = 1;
			} else {
				status = 3;
			}
		} else {
			status = 2;
		}*/
		status = 1;
//		query = "UPDATE OTPSession SET CheckOTPDateTime = ?, Count = ?, Status = ? WHERE AutoKey = ? and OTPCode = ?";
		query = "UPDATE OTPSession SET CheckOTPDateTime = ?, Count = ?, Status = ? WHERE AutoKey = ? ";//ndh
		PreparedStatement pstmt1 = conn.prepareStatement(query);
		int j = 1;
		pstmt1.setString(j++, date);
		pstmt1.setInt(j++, ++count);
		pstmt1.setInt(j++, status);
		pstmt1.setLong(j++, referenceKey);
//		pstmt1.setString(j++, otpCode);//ndh
		int rs = pstmt1.executeUpdate();
		if (rs > 0) {
			/*
			 * if(timeflag == true) {
			 */
			response.setCode("0000");
			response.setDesc("Success");
			/*
			 * } if(timeflag == false) { response.setCode("0014");
			 * response.setDesc("Failed"); }
			 */
		} else {
			response.setCode("0014");
			response.setDesc("Invalid OTP code");
		}
		return response;
	}
	
	public ResponseData checkOTPCodeV2(String userID, String rKey, String otpCode, Connection conn)
			throws SQLException, ParseException {
		String sql = "";
		String query = "";
		String sql1 = "";
		int count = 0;
		int status = 0;
		boolean timeflag = false;
		ResponseData response = new ResponseData();
		long referenceKey = Long.parseLong(rKey);
		String userid = "", sessionid = "", otpcode = "", getotpdtime = "";
		sql1 = "update OTPSession set status=? where userid=? and status=?";
		PreparedStatement pstmt2 = conn.prepareStatement(sql1);
		int k = 1;
		pstmt2.setInt(k++, 0);
		pstmt2.setString(k++, userID);
		pstmt2.setInt(k++, 1);
		int resultset = pstmt2.executeUpdate();	
		if(resultset > 0){
			System.out.println("Status Update Successfully");
		}else System.out.println("No Record Found to Update Status");
		
		sql = "SELECT UserID,SessionID,OTPCode,GetOTPDateTime,Count FROM OTPSession WHERE AutoKey = ? and OTPCode = ?";
		PreparedStatement pstmt = conn.prepareStatement(sql);
		int i = 1;
		pstmt.setLong(i++, referenceKey);
		pstmt.setString(i++, otpCode);
		ResultSet result = pstmt.executeQuery();
		while (result.next()) {
			userid = result.getString("UserID");
			sessionid = result.getString("SessionID");
			otpcode = result.getString("OTPCode");
			getotpdtime = result.getString("GetOTPDateTime");
			count = result.getInt("Count");
		}
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		if (userid.equalsIgnoreCase(userID) && otpcode.equalsIgnoreCase(otpCode)) {
			String dtime1 = getotpdtime.replace("-", "");// getotp datetime
			String dtime2 = date.replace("-", ""); // current datetime
			String otpsessionLimit = ConnAdmin.readExternalUrl("OTPSESSIONTIME");
			status = 1;
			timeflag = checkTimeStamp(dtime1, dtime2, otpsessionLimit);
			if (timeflag == true) {
				status = 1;
			} else {
				status = 3;
			}
		} else {
			status = 2;
		}
		//status = 1;
		query = "UPDATE OTPSession SET CheckOTPDateTime = ?, Count = ?, Status = ? WHERE AutoKey = ? and OTPCode = ?";
		PreparedStatement pstmt1 = conn.prepareStatement(query);
		int j = 1;
		pstmt1.setString(j++, date);
		pstmt1.setInt(j++, ++count);
		pstmt1.setInt(j++, status);
		pstmt1.setLong(j++, referenceKey);
		pstmt1.setString(j++, otpCode);
		int rs = pstmt1.executeUpdate();
		if (rs > 0) {
			if (timeflag == true) {
				response.setCode("0000");
				response.setDesc(sessionid);

			}
			if (timeflag == false) {
				response.setCode("0014");
				response.setDesc("Your OTP was expired");
			}

		} else {
			response.setCode("0014");
			response.setDesc("Invalid OTP code");
		}
		return response;
	}
//	public ResponseData checkOTPCodeV2(String userID, String rKey, String otpCode,String deviceid,String t2,Connection conn)
//			throws SQLException, ParseException {
//		String sql = "";
//		String query = "";
//		String sql1 = "";
//		int count = 0;
//		int status = 0;
//		//boolean timeflag = false;
//		ResponseData response = new ResponseData();
//		long referenceKey = Long.parseLong(rKey);
//		String userid = "", sessionid = "", otpcode = "", getotpdtime = "";
//		sql1 = "update OTPSession set status=? where userid=? and status=?";
//		PreparedStatement pstmt2 = conn.prepareStatement(sql1);
//		int k = 1;
//		pstmt2.setInt(k++, 0);
//		pstmt2.setString(k++, userID);
//		pstmt2.setInt(k++, 1);
//		int resultset = pstmt2.executeUpdate();	
//		if(resultset > 0){
//			System.out.println("Status Update Successfully");
//		}else System.out.println("No Record Found to Update Status");
//		
//		sql = "SELECT UserID,SessionID,OTPCode,GetOTPDateTime,Count FROM OTPSession WHERE AutoKey = ?";//and OTPCode = ?
//		PreparedStatement pstmt = conn.prepareStatement(sql);
//		int i = 1;
//		pstmt.setLong(i++, referenceKey);
//		//pstmt.setString(i++, otpCode);
//		ResultSet result = pstmt.executeQuery();
//		while (result.next()) {
//			userid = result.getString("UserID");
//			sessionid = result.getString("SessionID");
//			//otpcode = result.getString("OTPCode");
//			getotpdtime = result.getString("GetOTPDateTime");
//			count = result.getInt("Count");
//		}
//		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
//		if (userid.equalsIgnoreCase(userID)) {//&& otpcode.equalsIgnoreCase(otpCode)
//			String dtime1 = getotpdtime.replace("-", "");// getotp datetime
//			String dtime2 = date.replace("-", ""); // current datetime
//			String otpsessionLimit = ConnAdmin.readExternalUrl("OTPSESSIONTIME");
//			status = 1;
//			//timeflag = checkTimeStamp(dtime1, dtime2, otpsessionLimit);
//			/*if (timeflag == true) {
//				status = 1;
//			} else {
//				status = 3;
//			}*/
//		} else {
//			status = 2;
//		}
//		//status = 1;
//		query = "UPDATE OTPSession SET CheckOTPDateTime = ?, Count = ?, Status = ? ,t1=?,t2=? WHERE AutoKey=?";
//		PreparedStatement pstmt1 = conn.prepareStatement(query);
//		int j = 1;
//		pstmt1.setString(j++, date);
//		pstmt1.setInt(j++, ++count);
//		pstmt1.setInt(j++, status);
//		pstmt1.setString(j++, deviceid);
//		pstmt1.setString(j++, t2);
//		pstmt1.setLong(j++, referenceKey);
//		//pstmt1.setString(j++, otpCode);
//		int rs = pstmt1.executeUpdate();
//		if (rs > 0) {
//			response.setCode("0000");
//			response.setDesc(sessionid);
//
//			/*if (timeflag == true) {
//				response.setCode("0000");
//				response.setDesc(sessionid);
//
//			}
//			if (timeflag == false) {
//				response.setCode("0014");
//				response.setDesc("Failed");
//			}*/
//
//		} else {
//			response.setCode("0014");
//			response.setDesc("Invalid OTP code");
//		}
//		return response;
//	}

	public Result checkSession(String sessionId, String userId, Connection conn) throws SQLException {
		Result response = new Result();
		String query1 = "";
		PreparedStatement p_stmt1 = null;
		int maxhour = 0, maxmins = 0, maxsec = 0;

		String lastactivitydt = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
		String lastActivityDateTime = "";
		int status = -1;
		String sql = "select LastActivityDateTime,Status from " + mTableName + " Where SessionID = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, sessionId);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			status = rs.getInt("Status");
			lastActivityDateTime = rs.getString("LastActivityDateTime");
		}

		ps.close();
		rs.close();
		if (status == 0) {
			if (!lastActivityDateTime.equals("") && lastActivityDateTime.length() > 19) {
				maxhour = Integer.parseInt(lastActivityDateTime.substring(11, 13));
				maxmins = Integer.parseInt(lastActivityDateTime.substring(14, 16));
				maxsec = Integer.parseInt(lastActivityDateTime.substring(17, 19));
			} else {
				response.setMsgCode("0014");
				response.setMsgDesc("Internal Error");

				return response;
			}
			Calendar sqltime = Calendar.getInstance();
			sqltime.set(Calendar.HOUR_OF_DAY, maxhour);
			sqltime.set(Calendar.MINUTE, maxmins);
			sqltime.set(Calendar.SECOND, maxsec);
			long maxtime = sqltime.getTime().getTime();
			final long onemin = 60000;// millisecs
			String sessionTime = ConnAdmin.readExternalUrl("SESSIONTIME");
			Date afterAddingThreeMins = new Date(maxtime + (Integer.parseInt(sessionTime) * onemin));
			Calendar nowtime = Calendar.getInstance();
			Time servertime = new Time(nowtime.getTime().getTime());
			if (afterAddingThreeMins.equals(servertime) || afterAddingThreeMins.after(servertime)) {
				query1 = "UPDATE " + mTableName + " SET LastActivityDateTime = ?  Where Status = 0  And SessionID = ? ";
				p_stmt1 = conn.prepareStatement(query1);
				int i = 1;
				p_stmt1.setString(i++, lastactivitydt);
				p_stmt1.setString(i++, sessionId);
				int row = p_stmt1.executeUpdate();
				if (row > 0) {
					response.setMsgCode("0000");
					response.setMsgDesc("success");
				}
			} else {

				query1 = "UPDATE " + mTableName
						+ " SET LastActivityDateTime = ?,Status = 5 Where Status = 0  And SessionID = ?";
				p_stmt1 = conn.prepareStatement(query1);
				int j = 1;
				p_stmt1.setString(j++, lastactivitydt);
				p_stmt1.setString(j++, sessionId);
				int row = p_stmt1.executeUpdate();
				if (row > 0) {
					response.setMsgCode("0016");
					response.setMsgDesc("Your session has expired.");
				}
			}
			p_stmt1.close();
		} else if (status == 5) {
			response.setMsgCode("0014");
			response.setMsgDesc("Invalid Session ID");
		} else if (status == 6) {
			response.setMsgCode("0014");
			response.setMsgDesc("Invalid Session ID");
		} else if (status == 9) {
			response.setMsgCode("0014");
			response.setMsgDesc("Invalid Session ID");
		} else if (status == -1) {
			response.setMsgCode("0014");
			response.setMsgDesc("Invalid Session ID");
		}
		return response;
	}

	public boolean checkSessionID(String sessionId, String userId, Connection conn) throws SQLException {
		boolean rt = true;
		String query1 = "";
		PreparedStatement p_stmt1 = null;
		String lastactivitydt = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
		String sql = "select LastActivityDateTime from " + mTableName + " Where Status = 0  And SessionID = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, sessionId);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			int maxhour = 0, maxmins = 0, maxsec = 0;
			maxhour = Integer.parseInt(rs.getString("LastActivityDateTime").substring(11, 13));
			maxmins = Integer.parseInt(rs.getString("LastActivityDateTime").substring(14, 16));
			maxsec = Integer.parseInt(rs.getString("LastActivityDateTime").substring(17, 19));
			Calendar sqltime = Calendar.getInstance();
			sqltime.set(Calendar.HOUR_OF_DAY, maxhour);
			sqltime.set(Calendar.MINUTE, maxmins);
			sqltime.set(Calendar.SECOND, maxsec);
			long maxtime = sqltime.getTime().getTime();
			System.out.println("Max Time from sql : " + maxtime);
			final long onemin = 60000;// millisecs
			String sessionTime = ConnAdmin.readExternalUrl("SESSIONTIME");
			Date afterAddingThreeMins = new Date(maxtime + (Integer.parseInt(sessionTime) * onemin));
			System.out.println("After Adding Time ::: " + afterAddingThreeMins);
			Calendar nowtime = Calendar.getInstance();
			Time servertime = new Time(nowtime.getTime().getTime());
			if (afterAddingThreeMins.equals(servertime) || afterAddingThreeMins.after(servertime)) {
				query1 = "UPDATE " + mTableName + " SET LastActivityDateTime = ?  Where Status = 0  And SessionID = ?";
				p_stmt1 = conn.prepareStatement(query1);
				int i = 1;
				p_stmt1.setString(i++, lastactivitydt);
				p_stmt1.setString(i++, sessionId);
				int row = p_stmt1.executeUpdate();
				if (row > 0) {
					rt = false;
				}
			} else {
				query1 = "UPDATE " + mTableName
						+ " SET LastActivityDateTime = ?,Status = 5 Where Status = 0  And SessionID = ?";
				p_stmt1 = conn.prepareStatement(query1);
				p_stmt1.setString(1, lastactivitydt);
				p_stmt1.setString(2, sessionId);
				int row = p_stmt1.executeUpdate();
				if (row > 0) {
					rt = true;
				}
			}
		}
		ps.close();
		rs.close();
		return rt;
	}

	public boolean checkTimeStamp(String timeStamp, String currentdtime, String sessionLimit)
			throws SQLException, ParseException {
		boolean rt = false;
		long sessionTime = Long.parseLong(sessionLimit);
		// Custom date format
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
		Long timelimit = (sessionTime * 60000); // 3 minutes
		Date d1 = null;
		Date d2 = null;

		d1 = format.parse(timeStamp);
		d2 = format.parse(currentdtime);

		// Get msec from each, and subtract.
		long diff = d2.getTime() - d1.getTime();
		if (diff < timelimit) {
			rt = true;
		}
		return rt;
	}

	public int forceLogout(String userId, Connection conn) throws SQLException, ParseException {
		int res = 0;
		String updatequery = "Update " + mTableName
				+ " set Status = ? WHERE UserID = ? AND Status <> 8 AND LogInDateTime=(SELECT MAX(LogInDateTime) FROM "
				+ mTableName + " WHERE Status <> 8 AND UserID = ?)";
		PreparedStatement ps = conn.prepareStatement(updatequery);
		int i = 1;
		ps.setInt(i++, 5);
		ps.setString(i++, userId);
		ps.setString(i++, userId);
		res = ps.executeUpdate();
		return res;
	}

	private long generateKey(String aRef, Connection mConn) throws SQLException {
		long l_RefKey = 0;
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		String sql = "Insert Into " + mTableName1 + "(UserID,Status,LogInDateTime) values(?,?,?)";
		PreparedStatement preparedstatement = mConn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		int i = 1;
		preparedstatement.setString(i++, aRef);
		preparedstatement.setString(i++, date);
		preparedstatement.setInt(i++, 0);
		preparedstatement.executeUpdate();
		ResultSet rs = preparedstatement.getGeneratedKeys();
		if (rs != null && rs.next()) {
			l_RefKey = rs.getLong(1);
		}

		rs.close();
		preparedstatement.close();

		return l_RefKey;
	}
	private long selectKey(String aRef, Connection mConn) throws SQLException {
		long l_RefKey = 0;
		String sql = "Select autokey from " + mTableName1 + " where userid= ? and status = 1";
		PreparedStatement  ps= mConn.prepareStatement(sql);
		ps.setString(1, aRef);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			l_RefKey = rs.getLong(1);
		}else{
			
		}
		rs.close();
		ps.close();

		return l_RefKey;
	}
	private long generateKeyOTP(String aRef,String fingerprint,String deviceID,String flag, Connection mConn) throws SQLException {
		int status=0;
		if(fingerprint.equals("1")){
			status=1;
		}
		long l_RefKey = 0;
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		String sql = "Insert Into OTPSession (UserID,GetOTPDateTime,CheckOTPDateTime,Status,t1,t2) values(?,?,?,?,?,?)";
		PreparedStatement preparedstatement = mConn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		int i = 1;
		preparedstatement.setString(i++, aRef);
		preparedstatement.setString(i++, date);
		preparedstatement.setString(i++, date);
		preparedstatement.setInt(i++, status);
		preparedstatement.setString(i++, deviceID);
		preparedstatement.setString(i++, flag);
		preparedstatement.executeUpdate();
		ResultSet rs = preparedstatement.getGeneratedKeys();
		if (rs != null && rs.next()) {
			l_RefKey = rs.getLong(1);
		}

		rs.close();
		preparedstatement.close();

		return l_RefKey;
	}

	public MobileLogoutResponseData getActivitiesHistory(SessionData data, Connection con)
			throws SQLException, ParseException {
		MobileLogoutResponseData res = new MobileLogoutResponseData();
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		String lasttimelogin = "";
		String lasttimelogout = "";
		String logindate = "";
		String min_dtime = "";
		String max_dtime = "";
		String duration = "";
		String sql = "SELECT LogInDateTime FROM " + mTableName + " WHERE SessionID = ?";
		PreparedStatement stmt = con.prepareStatement(sql);
		int i = 1;
		stmt.setString(i++, data.getSessionID());
		ResultSet result = stmt.executeQuery();
		while (result.next()) {
			logindate = result.getString("LogInDateTime");
		}
		if (!logindate.equalsIgnoreCase("")) {
			lasttimelogin = logindate.substring(8, 10) + "-" + logindate.substring(5, 7) + "-"
					+ logindate.substring(0, 4) + " " + logindate.substring(11, 19) + "";
			min_dtime = logindate.replace("-", "").substring(0, 17);
			max_dtime = date.replace("-", "").substring(0, 17);
			duration = getDuration(max_dtime, min_dtime);
		}
		// for logout,update status to 9
		String query = "UPDATE " + mTableName
				+ " SET Status = ?, LastActivityDateTime = ?, LogOutDateTime = ? WHERE SessionID = ?";
		PreparedStatement ps = con.prepareStatement(query);
		int j = 1;
		ps.setInt(j++, 9);
		ps.setString(j++, date);
		ps.setString(j++, date);
		ps.setString(j++, data.getSessionID());
		int rs = ps.executeUpdate();
		if (rs > 0) {
			lasttimelogout = date.substring(8, 10) + "-" + date.substring(5, 7) + "-" + date.substring(0, 4) + " "
					+ date.substring(11, 19) + "";
			res.setCode("0000");
			res.setDesc("Logged out Successfully");
		} else {
			res.setCode("0014");
			res.setDesc("Logged out Failed");
		}
		res.setUserID(data.getUserID());
		res.setSessionID(data.getSessionID());
		res.setLastTimeLogin(lasttimelogin);
		res.setLastTimeLogout(lasttimelogout);
		res.setDuration(duration);
		return res;
	}

	public String getDuration(String max_time, String min_time) throws ParseException {
		String duration = "";
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
		Date date1 = format.parse(min_time);
		Date date2 = format.parse(max_time);
		long difference = date2.getTime() - date1.getTime();
		int seconds = (int) (difference / 1000);
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.set(Calendar.SECOND, seconds);
		duration = new SimpleDateFormat("HH:mm:ss").format(calendar.getTime());
		return duration;
	}

	public CheckOTPRequestData getOTPCodeFromTbl(String sessionId, Connection conn) throws SQLException {
		CheckOTPRequestData res = new CheckOTPRequestData();
		String sql = "Select OTPCode,AutoKey,N1 from OTPSession Where SessionID = ? and GetOTPDateTime = (SELECT MAX(GetOTPDateTime) FROM OTPSession WHERE SessionID= ?)";
		PreparedStatement preparedstatement = conn.prepareStatement(sql);
		int i = 1;
		preparedstatement.setString(i++, sessionId);
		preparedstatement.setString(i++, sessionId);
		ResultSet result = preparedstatement.executeQuery();
		while (result.next()) {
			res.setOtpCode(result.getString("OTPCode"));
			res.setrKey(String.valueOf(result.getLong("AutoKey")));
		}
		return res;
	}

	public String getOTPGeneratedTime(Connection conn, String sessionId) throws SQLException {
		String getotptime = "";
		String sql = "SELECT GetOTPDateTime FROM OTPSession WHERE SessionID = ? and GetOTPDateTime = (SELECT MAX(GetOTPDateTime) FROM OTPSession WHERE SessionID=?)";
		PreparedStatement pstmt = conn.prepareStatement(sql);
		int i = 1;
		pstmt.setString(i++, sessionId);
		pstmt.setString(i++, sessionId);
		ResultSet result = pstmt.executeQuery();
		while (result.next()) {
			getotptime = result.getString("GetOTPDateTime");
		}
		return getotptime;
	}

	public Result insertLogOutSessionID(String sessionId, String userId, String activity, String expired,
			String duration, Connection conn) throws SQLException, ParseException {
		Result res = new Result();
		String max_time = "";
		String min_time = "";
		String duration_temp = "";
		String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sql = "INSERT INTO LoginHistory (CreatedDate ,UserID ,SessionID, Date ,Time ,Activity ,Expired , Duration) "
				+ " VALUES(?,?,?,?,?,?,?,? )";
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setString(i++, GeneralUtil.datetoString());
		ps.setString(i++, userId);
		ps.setString(i++, sessionId);
		ps.setString(i++, GeneralUtil.datetoString());
		ps.setString(i++, GeneralUtil.getTime());
		ps.setString(i++, activity);
		ps.setString(i++, expired);
		ps.setString(i++, duration_temp);
		int rst = ps.executeUpdate();
		if (rst > 0) {
			res.setState(true);
			res.setMsgCode("0000");
			res.setMsgDesc("Insert Successfully");
			if (!duration.equalsIgnoreCase("")) {
				String query = "SELECT MAX(Time) As maxTime,MIN(Time) As minTime FROM LoginHistory WHERE SessionID = ? AND (UserID = ? COLLATE SQL_Latin1_General_CP1_CS_AS) AND Date =?";
				PreparedStatement pstmt = conn.prepareStatement(query);
				int k = 1;
				pstmt.setString(k++, sessionId);
				pstmt.setString(k++, userId);
				pstmt.setString(k++, date);
				ResultSet rs = pstmt.executeQuery();
				while (rs.next()) {
					max_time = rs.getString("maxTime");
					min_time = rs.getString("minTime");

				}
				int row = 0;
				SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
				Date date1 = format.parse(min_time);
				Date date2 = format.parse(max_time);
				long difference = date2.getTime() - date1.getTime();
				int seconds = (int) (difference / 1000);
				Calendar calendar = Calendar.getInstance();
				calendar.set(Calendar.HOUR_OF_DAY, 0);
				calendar.set(Calendar.MINUTE, 0);
				calendar.set(Calendar.MILLISECOND, 0);
				calendar.set(Calendar.SECOND, seconds);
				duration = new SimpleDateFormat("HH:mm:ss").format(calendar.getTime());
				String query1 = "UPDATE LoginHistory SET duration =? WHERE Time = (SELECT MAX(Time) FROM LoginHistory WHERE SessionID = ? AND UserID = ? AND Date = ?)";
				PreparedStatement p_stmt1 = conn.prepareStatement(query1);
				int j = 1;
				p_stmt1.setString(j++, duration);
				p_stmt1.setString(j++, sessionId);
				p_stmt1.setString(j++, userId);
				p_stmt1.setString(j++, date);
				row = p_stmt1.executeUpdate();
				if (row == 0) {
					res.setState(false);
					res.setMsgCode("0014");
					res.setMsgDesc("Updating Duration Failed");
				}
			}
		} else {
			res.setState(false);
			res.setMsgCode("0014");
			res.setMsgDesc("Insert Failed");
		}
		ps.close();
		return res;
	}

	/*public OTPRes insertOTPSession(OTPReq request, String phoneNo, String otpCode, Connection conn)
			throws SQLException {
		// TODO Auto-generated method stub
		OTPRes res = new OTPRes();
		long l_RefKey = 0;
		int count_getOTP = 0;
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		String sql = "INSERT INTO OTPSession(UserID,SessionID,OTPCode,Type,GetOTPDateTime,N1) VALUES(?,?,?,?,?,?)";
		PreparedStatement preparedstatement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		int i = 1;
		preparedstatement.setString(i++, request.getUserID());
		preparedstatement.setString(i++, request.getSessionID());
		preparedstatement.setString(i++, otpCode);
		preparedstatement.setString(i++, request.getType());
		preparedstatement.setString(i++, date);
		preparedstatement.setInt(i++, ++count_getOTP);
		preparedstatement.executeUpdate();
		ResultSet rs = preparedstatement.getGeneratedKeys();
		if (rs != null && rs.next()) {
			l_RefKey = rs.getLong(1);
		}
		if (l_RefKey != 0) {
			res.setCode("0000");
			res.setDesc("Inserted Successfully");
			res.setrKey(String.valueOf(l_RefKey));
		} else {
			res.setCode("0014");
			res.setDesc("Insert Failed");
			res.setrKey("");
		}
		rs.close();
		preparedstatement.close();

		return res;
	}*/
	public OTPRes insertOTPSession(String type, String phoneNo,String otpCode,String fingerprint,String deviceID,String flag, Connection conn)
			throws SQLException {
		//String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		long key = 0L;
		String skey = new SessionMgr().generateOTP();
		key = generateKeyOTP(phoneNo,fingerprint,deviceID,flag, conn);
		String sessionId = String.valueOf(key) + skey;
		sessionId = ServerUtil.hashText(sessionId);
		
		OTPRes res = new OTPRes();
		long l_RefKey = 0;
		int count_getOTP = 0;
		String sql = "UPDATE OTPSession SET UserID = ?, SessionID = ?,OTPCode = ?, Type = ?, N1=? "
				+ "WHERE AutoKey = ?";
		
		//String sql = "INSERT INTO OTPSession(UserID,SessionID,OTPCode,Type,GetOTPDateTime,N1) VALUES(?,?,?,?,?,?)";
		PreparedStatement pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		int i = 1;
		pstmt.setString(i++, phoneNo);
		pstmt.setString(i++, sessionId);
		pstmt.setString(i++, otpCode);
		pstmt.setString(i++, type);
		//preparedstatement.setString(i++, date);
		pstmt.setInt(i++, ++count_getOTP);
		pstmt.setLong(i++, key);
		int rs = pstmt.executeUpdate();		
		if (rs > 0) {
			res.setCode("0000");
			res.setDesc("Inserted Successfully");
			res.setrKey(String.valueOf(key));
			res.setSessionID(sessionId);
		} else {
			res.setCode("0014");
			res.setDesc("Insert Fail");
		}
		pstmt.close();
		return res;
	}
	public Result insertSession(String userId, Connection conn) throws SQLException, GeneralSecurityException {
		Result res = new Result();
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		long key = 0L;
		String skey = new SessionMgr().generateOTP();
		key = selectKey(userId, conn);
		String sessionId = String.valueOf(key) + skey;
		sessionId = ServerUtil.hashText(sessionId);
		String query = "UPDATE " + mTableName1 + " SET UserID = ?, SessionID = ?,"
				+ " Status = ?, LogInDateTime = ? WHERE AutoKey = ?";
		PreparedStatement pstmt = conn.prepareStatement(query);
		int i = 1;
		pstmt.setString(i++, userId);
		pstmt.setString(i++, sessionId);
		pstmt.setInt(i++, 1);
		pstmt.setString(i++, date);
		pstmt.setLong(i++, key);
		int rst = pstmt.executeUpdate();
		if (rst > 0) {
			res.setState(true);
			res.setMsgCode("0000");
			res.setMsgDesc("Insert Successfully.");
			res.setSessionID(sessionId);
		} else {
			res.setState(false);
			res.setMsgCode("0014");
			res.setMsgDesc("Insert Failed.");
		}
		pstmt.close();
		return res;
	}

	public Result saveActivity(String sessionId, String userId, String activity, String expired, String duration,
			Connection conn) throws SQLException {
		Result res = new Result();
		String duration_temp = "";
		String sql = "INSERT INTO LoginHistory (CreatedDate ,UserID ,SessionID, Date ,Time ,Activity ,Expired , Duration) "
				+ " VALUES(?,?,?,?,?,?,?,? )";
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setString(i++, GeneralUtil.datetoString());
		ps.setString(i++, userId);
		ps.setString(i++, sessionId);
		ps.setString(i++, GeneralUtil.datetoString());
		ps.setString(i++, GeneralUtil.getTime());
		ps.setString(i++, activity);
		ps.setString(i++, expired);
		ps.setString(i++, duration_temp);
		int rst = ps.executeUpdate();
		if (rst > 0) {
			res.setState(true);
			res.setMsgCode("0000");
			res.setMsgDesc("Insert Successfully");
		} else {
			res.setState(false);
			res.setMsgCode("0014");
			res.setMsgDesc("Insert Failed");
		}
		ps.close();
		return res;
	}

	public ResponseData updateActivityTime_Old(String sessionId, String userId, Connection conn) throws SQLException {
		ResponseData res = new ResponseData();
		String Status = "";
		String sql = "select Status from OTPSession Where SessionID = ? and UserID=? ";//" + mTableName + "
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setString(i++, sessionId);
		ps.setString(i++, userId);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			Status = rs.getString("Status");
			if (Status.equals("1")) { 
				res.setCode("0000");
				res.setDesc("Permission Access");
			} else {
				res.setCode("0012");
				res.setDesc("Your session has expired. Please signin again.");
			}
		} else {
			res.setCode("0014");
			res.setDesc("Invalid Session ID");
		}
		ps.close();
		return res;
	}
	public ResponseData updateActivityTime(String sessionId, String userID, String formID, Connection conn) throws SQLException {
		ResponseData res = new ResponseData();
		String lastactivitydt = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
		String Status = "";
		
		String sql = "select Status from " + mTableName + " Where SessionID = ? AND UserID = ?;";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, sessionId);
		ps.setString(2, userID);
		ResultSet rs = ps.executeQuery();

		if (rs.next()) {
			Status = rs.getString("Status");
			if (Status.equals("0")) {
				String query1 = "UPDATE " + mTableName
						+ " SET LastActivityDateTime = ?  Where SessionID = ? AND UserID = ?;";
				PreparedStatement p_stmt1 = conn.prepareStatement(query1);
				p_stmt1.setString(1, lastactivitydt);
				p_stmt1.setString(2, sessionId);
				p_stmt1.setString(3, userID);
				int row = p_stmt1.executeUpdate();

				if (row > 0) {
					if(!formID.equals("")){
						String query = "SELECT t2,t3 FROM UVM022_A  WHERE syskey IN ("
								+ "SELECT n2 FROM UVM023_A WHERE  n1 IN ("
								+ "SELECT n2 FROM JUN002_A WHERE n1 IN ("
								+ "SELECT syskey FROM UVM005_A WHERE (t1=?)))) "
								+ "AND t3=? and RecordStatus<> 4  and n1=1 order by n6";
						PreparedStatement pstmt = conn.prepareStatement(query);
						pstmt.setString(1, userID);
						pstmt.setString(2, formID);
						ResultSet rset = pstmt.executeQuery();
						if (rset.next()) {
							res.setCode("0000");
							res.setDesc("Permission access");
						}else {
							res.setCode("0016");
							res.setDesc("User don't get permission");
						}
					}else{
						res.setCode("0000");
						res.setDesc("Update successfully");
					}
					
				} else {
					res.setCode("0014");
					res.setDesc("Updated Failed");
				}

				p_stmt1.close();
			} else {
				res.setCode("0014");
				res.setDesc("Your session has expired.Please signin again...");
			}
		} else {
			res.setCode("0016");
			res.setDesc("Your session has expired.Please signin again...");
		}

		ps.close();

		return res;
	}

	public ResponseData updateGetOTPCount(String referenceKey, Connection conn) throws SQLException, ParseException {
		ResponseData response = new ResponseData();
		String sql = "";
		String query = "";
		int count = 0;
		sql = "SELECT N1 FROM OTPSession WHERE AutoKey = ?";
		PreparedStatement pstmt = conn.prepareStatement(sql);
		int i = 1;
		pstmt.setLong(i++, Long.valueOf(referenceKey));
		ResultSet result = pstmt.executeQuery();
		while (result.next()) {
			count = result.getInt("N1");
		}
		query = "UPDATE OTPSession SET N1 = ? WHERE AutoKey = ?";
		PreparedStatement pstmt1 = conn.prepareStatement(query);
		int j = 1;
		pstmt1.setInt(j++, ++count);
		pstmt1.setLong(j++, Long.valueOf(referenceKey));
		int rs = pstmt1.executeUpdate();
		if (rs > 0) {
			response.setCode("0000");
			response.setDesc("Updated Successfully ");
		} else {
			response.setCode("0014");
			response.setDesc("Update Failed");
		}
		return response;
	}
	public ResponseData updateActivityTime(String sessionId, String userId, Connection conn) throws SQLException {
		ResponseData res = new ResponseData();
		String lastactivitydt = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
		String Status = "";
		String sql = "select Status from " + mTableName + " Where SessionID = ? and UserID=? ";
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setString(i++, sessionId);
		ps.setString(i++, userId);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			Status = rs.getString("Status");
			if (Status.equals("0")) {
				res.setCode("0000");
				res.setDesc("Updated Successfully");
				/*String query1 = "UPDATE " + mTableName
						+ " SET LastActivityDateTime = ?  Where SessionID = ? and UserID=? ";
				PreparedStatement p_stmt1 = conn.prepareStatement(query1);
				int j = 1;
				p_stmt1.setString(j++, lastactivitydt);
				p_stmt1.setString(j++, sessionId);
				p_stmt1.setString(j++, userId);
				int row = p_stmt1.executeUpdate();
				if (row > 0) {
					res.setCode("0000");
					res.setDesc("Updated Successfully");
				} else {
					res.setCode("0014");
					res.setDesc("Updated Failed");
				}
				p_stmt1.close();*/
			} else {
				res.setCode("0016");
				res.setDesc("Your session has expired. Please signin again.");
			}
		} else {
			res.setCode("0016");
			res.setDesc("Invalid UserID or Session ID");
		}
		ps.close();
		return res;
	}

	public boolean updateSecuritySession(String referenceKey, String Status, String sessionid, String userid,
			String preStatus, Connection conn) throws SQLException, ParseException {
		boolean res = false;
		String query = "";
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		String sql = "select Status from SecuritySession WHERE SecurityKey = ? and SessionId=? and Userid=? and Status=?";
		PreparedStatement pstmt1 = conn.prepareStatement(sql);
		int k = 1;
		pstmt1.setString(k++, referenceKey);
		pstmt1.setString(k++, sessionid);
		pstmt1.setString(k++, userid);
		pstmt1.setString(k++, preStatus);
		ResultSet rs1 = pstmt1.executeQuery();
		if (rs1.next()) {
			query = "UPDATE SecuritySession SET Status = ?,modifieddate=? WHERE SecurityKey = ? and SessionId=? and Userid=? and Status=?";
			PreparedStatement ps = conn.prepareStatement(query);
			int j = 1;
			ps.setString(j++, Status);
			ps.setString(j++, date);
			ps.setString(j++, referenceKey);
			ps.setString(j++, sessionid);
			ps.setString(j++, userid);
			ps.setString(j++, preStatus);
			int rs = ps.executeUpdate();
			if (rs > 0) {
				res = true;
			}
			ps.close();
		} else {
			res = false;
		}
		pstmt1.close();
		return res;
	}

	public int updateUserSession(String userId, Connection conn) throws SQLException, ParseException {
		int res = 0;
		String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
		String updatequery = "Update LoginHistory set Expired =?,Date = ?,Time =? Where Time = (Select MAX(Time) From LoginHistory Where ( UserID = ? COLLATE SQL_Latin1_General_CP1_CS_AS ) And Date = (Select MAX(Date) From LoginHistory Where ( UserID = ? COLLATE SQL_Latin1_General_CP1_CS_AS )))";
		PreparedStatement ps = conn.prepareStatement(updatequery);

		int i = 1;
		ps.setString(i++, "true");
		ps.setString(i++, date);
		ps.setString(i++, GeneralUtil.getTime());
		ps.setString(i++, userId);
		ps.setString(i++, userId);
		res = ps.executeUpdate();
		return res;
	}
	
	public boolean checkSessionExpired(String sessionId, String userId, Connection conn) throws SQLException {
		boolean rt = true;
		String query1 = "";
		PreparedStatement p_stmt1 = null;
		String nowDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		long sessionMinute = Long.parseLong(ConnAdmin.readExternalUrl("SESSIONTIME"));
		String sql = "select LastActivityDateTime from " + mTableName + " Where Status = 0  And SessionID = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, sessionId);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			String lastDateTime = rs.getString("LastActivityDateTime").substring(0, rs.getString("LastActivityDateTime").length()-2);
			long diffMinute = GeneralUtil.diffDateTime(nowDateTime, lastDateTime);
			if (diffMinute < sessionMinute) {
				query1 = "UPDATE " + mTableName + " SET LastActivityDateTime = ?  Where Status = 0  And SessionID = ?";
				p_stmt1 = conn.prepareStatement(query1);
				int i = 1;
				p_stmt1.setString(i++, nowDateTime);
				p_stmt1.setString(i++, sessionId);
				int row = p_stmt1.executeUpdate();
				if (row > 0) {
					rt = false;
				}
			} else {
				query1 = "UPDATE " + mTableName
						+ " SET LastActivityDateTime = ?,Status = 5 Where Status = 0  And SessionID = ?";
				p_stmt1 = conn.prepareStatement(query1);
				p_stmt1.setString(1, nowDateTime);
				p_stmt1.setString(2, sessionId);
				int row = p_stmt1.executeUpdate();
				if (row > 0) {
					rt = true;
				}
			}
		}
		ps.close();
		rs.close();
		return rt;
	}
	public boolean isSessionExpire(long profileKey, String sessionKey, Connection conn) throws SQLException {
        int status = 0;
        String sql = "SELECT status  FROM tblSession where  profilekey = ? AND SessionID = ? ";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, String.valueOf(profileKey));
        stmt.setString(2, sessionKey);
        ResultSet rss = stmt.executeQuery();
        if (rss.next()) {
            status = rss.getInt("status");
        }
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }
	public ResponseData checkDeviceId(String deviceid,Connection conn) throws SQLException{
		ResponseData res=new ResponseData();
		String userid = "";
		String t2="";
//		String sql = "select userid,t2 from " + mTableName1 + " Where t1=? ";
		//String sql = "select * from (select ROW_NUMBER() OVER (ORDER BY AutoKey DESC) AS RowNum,userid,t2 from OTPSession  where t1=?) AS RowConstrainedResult";
		String sql = "select * from (select ROW_NUMBER() OVER (ORDER BY AutoKey DESC) AS RowNum,userid,t2 from OTPSession  where t1=?) AS RowConstrainedResult";
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setString(i++, deviceid);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			userid = rs.getString("userid");
			t2=rs.getString("t2");
			if (userid !=null && t2.equals("0")) {
				res.setCode("0000");
				res.setDesc(userid);
				
			} else if(userid!=null && t2.equals("1")){
				res.setCode("0018");
				res.setDesc("your account logined with another phone ");
			}
			else {
			res.setCode("0018");
			res.setDesc("please singin with your phone no.");
		}
		}
		else{
			res.setCode("0018");
			res.setDesc("please singin with your phone no.");
		}
		ps.close();
		return res;
	}
	public ResponseData removeDeviceId(String deviceid,Connection conn) throws SQLException{
		ResponseData res=new ResponseData();
		String userid = "";
		String sql = "update REGISTER set t9=null where t9=? ";
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setString(i++, deviceid);
		int rs = ps.executeUpdate();
		if(rs>0){
			res.setCode("0000");
			res.setDesc("remove successfully");
		}
		else{
			res.setCode("0014");
			res.setDesc("Device ID does not exit.");
			
		}
		
		ps.close();
		return res;
	}
	public ResponseData selectDeviceID(String userid,Connection conn) throws SQLException{
		ResponseData res=new ResponseData();
		String deviceID;
//		String sql = "select * from (select ROW_NUMBER() OVER (ORDER BY AutoKey DESC) AS RowNum,t1 from OTPSession  where userid=? and status=1) AS RowConstrainedResult";
		String sql = "select top 1 * from OTPSession where userid=? and t1!='' order by autokey desc";
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setString(i++, userid);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			deviceID = rs.getString("t1");
			if (deviceID !=null ) {
				res.setCode("0000");
				res.setDesc(deviceID);
				
			} else {
				res.setCode("0014");
				res.setDesc("no device id");
			}
		} else {
			res.setCode("0018");
			res.setDesc("no device id");
		}
		ps.close();
		return res;
	}
	public ResponseData updateT2(String userid,String deviceID,String flag,Connection conn) throws SQLException{
		ResponseData res=new ResponseData();
		String sql = "update OTPSession set t2=? where userid=? and t1=? and status=1";
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setString(i++, flag);
		ps.setString(i++, userid);
		ps.setString(i++, deviceID);
		int rs = ps.executeUpdate();
		if (rs > 0) {
			/*
			 * if(timeflag == true) {
			 */
			res.setCode("0000");
			res.setDesc("Success");
			/*
			 * } if(timeflag == false) { response.setCode("0014");
			 * response.setDesc("Failed"); }
			 */
		} else {
			res.setCode("0014");
			res.setDesc("Invalid Device ID");
		}
		ps.close();
		return res;
	}

}
