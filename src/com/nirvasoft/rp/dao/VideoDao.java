package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.nirvasoft.cms.dao.ArticleDao;
import com.nirvasoft.cms.dao.OPTDao;
import com.nirvasoft.cms.dao.RegisterDao;
import com.nirvasoft.cms.dao.UploadDao;
import com.nirvasoft.cms.framework.ConnAdmin;
import com.nirvasoft.cms.framework.MrBean;
import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.cms.shared.ArticleDataSet;
import com.nirvasoft.cms.shared.PagerData;
import com.nirvasoft.cms.shared.RegisterData;
import com.nirvasoft.cms.shared.VideoData;
import com.nirvasoft.cms.shared.VideoDataSet;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;
import com.nirvasoft.database.SysKeyMgr;
import com.nirvasoft.rp.data.VideoListingDataList;
import com.nirvasoft.rp.shared.FilterData;
import com.nirvasoft.rp.shared.FilterDataset;

public class VideoDao {
	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("FMR002");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("autokey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("createdtime", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("modifiedtime", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t13", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		ret.getFields().add(new DBField("n11", (byte) 2));
		ret.getFields().add(new DBField("n12", (byte) 2));
		ret.getFields().add(new DBField("n13", (byte) 2));
		return ret;
	}

	public static DBRecord define(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t13", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		ret.getFields().add(new DBField("n11", (byte) 2));
		ret.getFields().add(new DBField("n12", (byte) 2));
		ret.getFields().add(new DBField("n13", (byte) 2));

		return ret;
	}

	public static ArticleData getArticalDBRecord(DBRecord adbr) {
		ArticleData ret = new ArticleData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setAutokey(adbr.getLong("autokey"));
		ret.setCreatedDate(adbr.getString("createddate"));
		ret.setCreatedTime(adbr.getString("createdtime"));
		ret.setModifiedDate(adbr.getString("modifieddate"));
		ret.setModifiedTime(adbr.getString("modifiedtime"));
		ret.setUserId(adbr.getString("userid"));
		ret.setUserName(adbr.getString("username"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setUserSyskey(adbr.getLong("usersyskey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		ret.setT8(adbr.getString("t8"));
		ret.setT9(adbr.getString("t9"));
		ret.setT10(adbr.getString("t10"));
		ret.setT11(adbr.getString("t11"));
		ret.setT12(adbr.getString("t12"));
		ret.setT13(adbr.getString("t13"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getLong("n5"));
		ret.setN6(adbr.getLong("n6"));
		ret.setN7(adbr.getLong("n7"));
		ret.setN8(adbr.getLong("n8"));
		ret.setN9(adbr.getLong("n9"));
		ret.setN10(adbr.getLong("n10"));
		ret.setN11(adbr.getLong("n11"));
		ret.setN12(adbr.getLong("n12"));
		ret.setN13(adbr.getLong("n13"));
		return ret;
	}

	// for dislike
	public static long getTotalDisLikeCount(long syskey, Connection conn) throws SQLException {
		long key = 0;
		String sql = " Select n8 from  FMR002 WHERE  RecordStatus = 1 AND syskey = " + syskey;
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = rs.getLong("n8");
		}
		return key;
	}

	public static long getTotalLikeCount(long syskey, Connection conn) throws SQLException {
		long key = 0;
		String sql = " Select n2 from  FMR002 WHERE  RecordStatus = 1 AND syskey = " + syskey;
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = rs.getLong("n2");
		}
		return key;
	}

	public static VideoData getVideoDBRecord(DBRecord adbr) {
		VideoData ret = new VideoData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("createddate"));
		ret.setCreatedTime(adbr.getString("createdtime"));
		ret.setModifiedDate(adbr.getString("modifieddate"));
		ret.setModifiedTime(adbr.getString("modifiedtime"));
		ret.setUserId(adbr.getString("userid"));
		ret.setUserName(adbr.getString("username"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		ret.setUserSyskey(adbr.getLong("usersyskey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		ret.setT8(adbr.getString("t8"));
		ret.setT9(adbr.getString("t9"));
		ret.setT10(adbr.getString("t10"));
		ret.setT11(adbr.getString("t11"));
		ret.setT12(adbr.getString("t12"));
		ret.setT13(adbr.getString("t13"));
		ret.setN1(adbr.getLong("n1"));
		ret.setN2(adbr.getLong("n2"));
		ret.setN3(adbr.getLong("n3"));
		ret.setN4(adbr.getLong("n4"));
		ret.setN5(adbr.getLong("n5"));
		ret.setN6(adbr.getLong("n6"));
		ret.setN7(adbr.getLong("n7"));
		ret.setN8(adbr.getLong("n8"));
		ret.setN9(adbr.getLong("n9"));
		ret.setN10(adbr.getLong("n10"));
		ret.setN11(adbr.getLong("n11"));
		ret.setN12(adbr.getLong("n12"));
		ret.setN13(adbr.getLong("n13"));
		return ret;
	}

	public static Resultb2b insert(ArticleData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		if (obj.getT5().equals("00000000")) {
			obj.setT5("");
		}
		String query = "INSERT INTO FMR002 (syskey, createddate, modifieddate, userid, username,"
				+ " recordStatus, syncStatus, syncBatch,usersyskey,t1, t2, t3, t4, t5, t6, t7, t8, t9, t10,"
				+ " t11, t12, t13,n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13,createdtime,modifiedtime,modifieduserid,modifiedusername)"
				+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?," + "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			if (!isCodeExist(obj, conn)) {
				PreparedStatement ps = conn.prepareStatement(query);
				ps.setLong(1, obj.getSyskey());
				ps.setString(2, obj.getCreatedDate());
				ps.setString(3, obj.getModifiedDate());
				ps.setString(4, obj.getUserId());
				ps.setString(5, obj.getUserName());
				ps.setInt(6, obj.getRecordStatus());
				ps.setInt(7, obj.getSyncStatus());
				ps.setLong(8, obj.getSyncBatch());
				ps.setLong(9, obj.getUserSyskey());
				ps.setString(10, obj.getT1());
				ps.setString(11, obj.getT2());
				ps.setString(12, obj.getT3());
				ps.setString(13, obj.getT4());
				ps.setString(14, obj.getT5());
				ps.setString(15, obj.getT6());
				ps.setString(16, obj.getT7());
				ps.setString(17, obj.getT8());
				ps.setString(18, obj.getT9());
				ps.setString(19, obj.getT10());
				ps.setString(20, obj.getT11());
				ps.setString(21, obj.getT12());
				ps.setString(22, obj.getT13());
				ps.setLong(23, obj.getN1());
				ps.setLong(24, obj.getN2());
				ps.setLong(25, obj.getN3());
				ps.setLong(26, obj.getN4());
				ps.setLong(27, obj.getN5());
				ps.setLong(28, obj.getN6());
				ps.setLong(29, obj.getN7());
				ps.setLong(30, obj.getN8());
				ps.setLong(31, obj.getN9());
				ps.setLong(32, obj.getN10());
				ps.setLong(33, obj.getN11());
				ps.setLong(34, obj.getN12());
				ps.setLong(35, obj.getN13());
				ps.setString(36, obj.getCreatedTime());
				ps.setString(37, obj.getModifiedTime());
				ps.setString(38, obj.getModifiedUserId());
				ps.setString(39, obj.getModifiedUserName());
				if (ps.executeUpdate() > 0) {
					res.setState(true);
					res.setMsgDesc("Saved Successfully!");
				} else {
					res.setMsgDesc("Article Already Exist!");
				}
			}
		} catch (SQLException e) {

		}
		return res;
	}

	public static boolean isCodeExist(ArticleData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				" where RecordStatus<>4 AND  RecordStatus = 1 AND syskey = " + obj.getSyskey() + " ", "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isDisLikeExist(Long syskey, Long userkey, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR008"),
				" where RecordStatus<>4 AND t4='dislike' AND RecordStatus = 1 AND n1 = " + syskey + " AND n2 = "
						+ userkey + " ",
				"", conn);
		/*
		 * ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR008"),
		 * "  n1 = " + syskey + " AND n2 = " + userkey + " ", "", conn);
		 */
		if (dbrs.size() > 0) {

			return true;

		} else {
			return false;
		}
	}

	public static boolean isLikeExist(Long syskey, Long userkey, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR008"),
				" where RecordStatus<>4 AND  RecordStatus = 1 AND t4='like' AND n1 = " + syskey + " AND n2 = " + userkey
						+ " ",
				"", conn);
		/*
		 * ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR008"),
		 * "  n1 = " + syskey + " AND n2 = " + userkey + " ", "", conn);
		 */
		if (dbrs.size() > 0) {

			return true;

		} else {
			return false;
		}
	}

	public static boolean isStatusExist(Long syskey, Long userkey, String type, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR008"),
				" where RecordStatus = 4 AND t4='" + type + "' AND n1 = " + syskey + " AND n2 = " + userkey + " ", "",
				conn);
		/*
		 * ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR008"),
		 * "  n1 = " + syskey + " AND n2 = " + userkey + " ", "", conn);
		 */
		if (dbrs.size() > 0) {

			return true;

		} else {
			return false;
		}
	}

	public static Resultb2b update(ArticleData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		if (obj.getT5().equals("00000000")) {
			obj.setT5("");
		}
		String query = "UPDATE FMR002 SET [syskey]=?, [modifieddate]=?, [userid]=?, [username]=?, "
				+ " [recordstatus]=?, [syncstatus]=?, [syncbatch]=?,[usersyskey]=?, [t1]=?, [t2]=?, [t3]=?,[t4]=?, [t5]=?,"
				+ " [t6]=?, [t7]=?,[t8]=?, [t9]=?,[t10]=?, [t11]=?,[t12]=?, [t13]=?, [n1]=?, [n2]=?, [n3]=?,"
				+ " [n4]=?, [n5]=?,[n6]=?, [n7]=?,[n8]=?, [n9]=?,[n10]=?, [n11]=?,[n12]=?, [n13]=?, [createdtime]=?,[modifiedtime]=?,[modifieduserid]=?,[modifiedusername]=? "
				+ "WHERE RecordStatus<>4 AND RecordStatus = 1 AND Syskey=" + obj.getSyskey() + "";

		try {
			if (isCodeExist(obj, conn)) {
				PreparedStatement pstmt = conn.prepareStatement(query);
				pstmt.setLong(1, obj.getSyskey());

				pstmt.setString(2, obj.getModifiedDate());
				pstmt.setString(3, obj.getUserId());
				pstmt.setString(4, obj.getUserName());
				pstmt.setInt(5, obj.getRecordStatus());
				pstmt.setInt(6, obj.getSyncStatus());
				pstmt.setLong(7, obj.getSyncBatch());
				pstmt.setLong(8, obj.getUserSyskey());
				pstmt.setString(9, obj.getT1());
				pstmt.setString(10, obj.getT2());
				pstmt.setString(11, obj.getT3());
				pstmt.setString(12, obj.getT4());
				pstmt.setString(13, obj.getT5());
				pstmt.setString(14, obj.getT6());
				pstmt.setString(15, obj.getT7());
				pstmt.setString(16, obj.getT8());
				pstmt.setString(17, obj.getT9());
				pstmt.setString(18, obj.getT10());
				pstmt.setString(19, obj.getT11());
				pstmt.setString(20, obj.getT12());
				pstmt.setString(21, obj.getT13());
				pstmt.setLong(22, obj.getN1());
				pstmt.setLong(23, obj.getN2());
				pstmt.setLong(24, obj.getN3());
				pstmt.setLong(25, obj.getN4());
				pstmt.setLong(26, obj.getN5());
				pstmt.setLong(27, obj.getN6());
				pstmt.setLong(28, obj.getN7());
				pstmt.setLong(29, obj.getN8());
				pstmt.setLong(30, obj.getN9());
				pstmt.setLong(31, obj.getN10());
				pstmt.setLong(32, obj.getN11());
				pstmt.setLong(33, obj.getN12());
				pstmt.setLong(34, obj.getN13());
				pstmt.setString(35, obj.getCreatedTime());
				pstmt.setString(36, obj.getModifiedTime());
				pstmt.setString(37, obj.getModifiedUserId());
				pstmt.setString(38, obj.getModifiedUserName());

				if (pstmt.executeUpdate() > 0) {
					res.setState(true);
					res.setMsgDesc("Updated Successfully!");
				} else {
					res.setMsgDesc("No Such Article to Update!");
				}
			}
		} catch (SQLException e) {

		}

		return res;
	}

	public FilterData getFilterData(String aItemID, ArrayList<FilterData> aFilterList) {
		FilterData l_FilterData = null;

		for (FilterData iFilterData : aFilterList) {
			if (iFilterData.getItemid().equals(aItemID)) {
				l_FilterData = iFilterData;
				break;
			}
		}

		return l_FilterData;
	}

	/* read */
	public ArticleData read(long syskey, Connection conn) throws SQLException {
		ArticleData ret = new ArticleData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				"where RecordStatus<>4 AND  RecordStatus = 1 AND syskey=" + syskey, "", conn);
		if (dbrs.size() > 0)
			ret = getArticalDBRecord(dbrs.get(0));
		if (ret.getT5().equals("")) {
			ret.setT5("00000000");
		}
		/*
		 * if (ServerUtil.isUniEncoded(ret.getT2())) {
		 * ret.setT2(FontChangeUtil.uni2zg(ret.getT2())); }
		 */
		return ret;
	}

	public long searchByID(String mobile, Connection conn) throws SQLException {
		long syskey;
		String sql = "Select syskey from UVM005_A WHERE RecordStatus<>4 AND t1=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, mobile);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			syskey = rs.getLong("syskey");

		} else {
			syskey = 0;
		}
		return syskey;
	}

	public long searchLikeUser(long n1, Connection conn, String userid) throws SQLException {
		long key;
		long userKey = OPTDao.searchByID(userid, conn);
		String sql = "Select n2 from FMR008 WHERE RecordStatus<>4 AND RecordStatus = 1 AND n1 = " + n1 + " AND n2 = '"
				+ userKey + "'";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			key = rs.getLong("n2");
		} else {
			key = 0;
		}
		return key;
	}

	public VideoDataSet searchLikeVideo(String type, long key, Connection conn) throws SQLException {
		// ArticleData ret = new ArticleData();
		System.out.println(type + " ^^");
		VideoDataSet res = new VideoDataSet();
		ArrayList<VideoData> datalist = new ArrayList<VideoData>();
		String whereclause = " WHERE RecordStatus<>4 AND RecordStatus = 1 AND t3 = '" + type + "'  AND n1 = " + key
				+ " ";
		System.out.println(whereclause);
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define("FMR008"), whereclause, " ORDER BY syskey ", conn);
		// .getDBRecords(define("FMR008"), conn);
		/*
		 * if (dbrs.size() > 0) ret = getDBRecord(dbrs.get(0));
		 */
		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getVideoDBRecord(dbrs.get(i)));
		}

		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}

		VideoData[] dataarray = new VideoData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);

		return res;
	}

	// videoSearch()
	public VideoListingDataList searchVideolist(FilterDataset req, Connection conn) throws SQLException {

		VideoListingDataList res = new VideoListingDataList();		

		int startPage = (req.getPageNo() - 1) * req.getPageSize();
		int endPage = req.getPageSize() + startPage;

		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		String whereclause = "";
		whereclause = " WHERE RecordStatus<>4 AND RecordStatus=1 AND t3='Video' ";

		// Get Filter List
		ArrayList<FilterData> l_FilterList = req.getFilterList();

		// Clear Filter List
		if (l_FilterList.get(0).getItemid() == "")
			l_FilterList.clear();

		// 1 - Simple Search String [ , , , ]
		String simplesearch = "";
		FilterData l_FilterData = getFilterData("1", l_FilterList);
		if (l_FilterData != null)
			simplesearch = l_FilterData.getT1();

		// 3 - status
		String statusString = "";
		l_FilterData = getFilterData("2", l_FilterList);
		if (l_FilterData != null)
			statusString = l_FilterData.getT1();

		if (!simplesearch.equals("")) {
			whereclause += " AND (t2 LIKE ? OR modifieddate LIKE ? OR modifiedtime LIKE ? OR username LIKE ? OR modifiedusername LIKE ? ) ";
		}		

		if (!statusString.equalsIgnoreCase("")) {
			whereclause = whereclause + " AND (n7=?) ";
		}

		String sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY autokey desc) AS RowNum,* FROM"
				+ " ( select * from fmr002" + whereclause + ") b) " + " AS RowConstrainedResult" 
				+ " WHERE (RowNum > ? AND RowNum <= ?)";

		PreparedStatement ps1 = conn.prepareStatement(sql);
		int i = 1;
		if (!simplesearch.equals("")) {
			for (int j = i; j < 6; j++) {
				ps1.setString(i++, "%" + simplesearch + "%");
			}
		}
		if (!statusString.equalsIgnoreCase("")) {
			ps1.setInt(i++,Integer.parseInt(statusString));
		}
		int startIndex = i;
		int endIndex = i+1;
			ps1.setInt(startIndex, startPage);
			ps1.setInt(endIndex, endPage);
		ResultSet rs = ps1.executeQuery();
		int srno = startPage + 1;
		while (rs.next()) {
			ArticleData data = new ArticleData();
			data.setSrno(srno++);// srno
			data.setSyskey(rs.getLong("syskey"));
			data.setAutokey(rs.getLong("autokey"));
			data.setT1(rs.getString("t1"));// title
			data.setT2(rs.getString("t2"));// content
			data.setT3(rs.getString("t3"));// type
			data.setT8(rs.getString("t8"));// you tube video
			data.setT13(rs.getString("t10"));
			data.setN1(rs.getLong("n1"));
			data.setN2(rs.getLong("n2"));// like count
			data.setN3(rs.getLong("n3"));// comment count
			data.setN4(rs.getLong("n4"));
			data.setN5(rs.getLong("n5"));// usersk
			data.setN6(rs.getLong("n6"));
			data.setN7(rs.getLong("n7"));// publish
			data.setN8(rs.getLong("n8"));
			data.setN9(rs.getLong("n9"));
			data.setN10(rs.getLong("n10"));// video=0,youtube=1
			if (data.getN7() == 1) {
				data.setT10("Draft");
			}
			if (data.getN7() == 2) {
				data.setT10("Pending");
			}
			if (data.getN7() == 3) {
				data.setT10("Modification");
			}
			if (data.getN7() == 4) {
				data.setT10("Approve");
			}
			if (data.getN7() == 5) {
				data.setT10("Publish");
			}
			if (data.getN7() == 6) {
				data.setT10("Reject");
			}
			data.setModifiedDate(rs.getString("modifieddate"));
			data.setModifiedTime(rs.getString("modifiedtime"));
			data.setUserName(rs.getString("username"));
			data.setModifiedUserName(rs.getString("modifiedusername"));
			data.setUpload(UploadDao.search(String.valueOf(data.getSyskey()), "", conn).getUploads());
			data.setVideoUpload(UploadDao.searchVideoList(String.valueOf(data.getSyskey()), conn).getVideoUpload());
			datalist.add(data);
		}
		res.setPageSize(req.getPageSize());
		res.setCurrentPage(req.getPageNo());

		ArticleData[] dataarray = null;
		if (datalist.size() > 0) {
			dataarray = new ArticleData[datalist.size()];
			dataarray = datalist.toArray(dataarray);
		}

		String qry = "SELECT count(*) as recCount FROM (SELECT ROW_NUMBER() "
				+ "OVER (ORDER BY syskey desc) AS RowNum,* FROM (" + "select * from fmr002 " + whereclause
				+ ") b) AS RowConstrainedResult";
		int m = 1;
		PreparedStatement ps2 = conn.prepareStatement(qry);
		if (!simplesearch.equals("")) {
			for (int k = m; k < 6; k++) {
				ps2.setString(m++, "%" + simplesearch + "%");
			}
		}
		if (!statusString.equalsIgnoreCase("")) {
			ps2.setInt(m++, Integer.parseInt(statusString));
		}
		ResultSet result = ps2.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		res.setVideoData(dataarray);
		return res;
	}

	/* atn */
	public ArticleDataSet searchVideoList(PagerData pgdata, long status, String statetype, Connection conn)
			throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		PreparedStatement stat;
		String whereclause = "";
		whereclause = " WHERE RecordStatus<>4  AND  RecordStatus = 1 and t3='Video' ";
		if (statetype.equalsIgnoreCase("")) {
			if (status == 1) {
				whereclause += "AND n7=6";// draft
			}
			if (status == 2) {
				whereclause += "AND n7=2";// pending
			}
			if (status == 3) {
				whereclause += "AND n7=4";// approved
			}
			if (status == 4) {
				whereclause += "AND n7=1";// approved
			}
			if (status == 5) {
				whereclause += "AND n7=1";// approved
			}
		}
		if (!statetype.equalsIgnoreCase("")) {
			whereclause += "AND (n7='" + statetype + "')";
		}
		if (!pgdata.getT1().equals("")) {
			whereclause += "AND (t1 LIKE N'%" + pgdata.getT1() + "%' OR  modifieddate LIKE N'%" + pgdata.getT1()
					+ "%' OR modifiedtime LIKE N'%" + pgdata.getT1() + "%' OR  t2 LIKE N'%" + pgdata.getT1()
					+ "%' OR  username LIKE N'%" + pgdata.getT1() + "%') ";
		}
		String sql = "";
		String regionCode = "";
		if (status == 5) { // admin
			sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc)AS RowNum , * from fmr002 "
					+ whereclause
					+ " and userid in (select t1 from uvm012 where syskey in (select n4 from   uvm005 where syskey in (select n1 from jun002 where n2 in(select syskey from uvm009 where t2<>'master'  )))))AS RowConstrainedResult  WHERE RowNum >="
					+ pgdata.getStart() + " and RowNum <=" + pgdata.getEnd();
		} else if (status == 1) { // content_writer
			sql = "select * from ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc)AS RowNum , * from fmr002 "
					+ whereclause + " and userid = '" + pgdata.getUserid()
					+ "' and userid in (select t1 from uvm012 where syskey in (select n4 from   uvm005 where syskey in (select n1 from jun002 where n2 in(select syskey from uvm009 where t2='Content Writer')))))AS RowConstrainedResult  WHERE RowNum >="
					+ pgdata.getStart() + " and RowNum <=" + pgdata.getEnd();
		} else {
			String query = "select t5 from uvm005_A where t1='" + pgdata.getUserid() + "'";
			PreparedStatement pstmt = conn.prepareStatement(query);
			ResultSet rst = pstmt.executeQuery();
			while (rst.next()) {
				regionCode = rst.getString("t5");
				if (regionCode.equals("00000000")) {
					sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc) AS RowNum,* FROM ( select * from fmr002 "
							+ whereclause + ") b)  AS RowConstrainedResult WHERE RowNum >= '" + pgdata.getStart()
							+ "' AND RowNum <= '" + pgdata.getEnd() + "' ";
				} else if (regionCode.equals("10000000")) {
					sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc) AS RowNum,* FROM ( select * from fmr002 "
							+ whereclause + "and t5!='13000000') b)  AS RowConstrainedResult WHERE RowNum >= '"
							+ pgdata.getStart() + "' AND RowNum <= '" + pgdata.getEnd() + "' ";
				} else if (regionCode.equals("13000000")) {
					sql = " SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc) AS RowNum,* FROM ( select * from fmr002 "
							+ whereclause + "and t5!='10000000') b)  AS RowConstrainedResult WHERE RowNum >= '"
							+ pgdata.getStart() + "' AND RowNum <= '" + pgdata.getEnd() + "' ";
				}
			}
		}
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ArticleData data = new ArticleData();
			data.setSyskey(rs.getLong("syskey"));
			data.setT1(rs.getString("t1"));// title
			data.setT2(rs.getString("t2"));// content
			data.setT3(rs.getString("t3"));// type
			data.setT8(rs.getString("t8"));// you tube video
			data.setN1(rs.getLong("n1"));
			data.setN2(rs.getLong("n2"));// like count
			data.setN3(rs.getLong("n3"));// comment count
			data.setN4(rs.getLong("n4"));
			data.setN5(rs.getLong("n5"));// usersk
			data.setN6(rs.getLong("n6"));
			data.setN7(rs.getLong("n7"));// publish
			data.setN8(rs.getLong("n8"));
			data.setN9(rs.getLong("n9"));
			data.setN10(rs.getLong("n10"));// video=0,youtube=1
			if (data.getN7() == 1) {
				data.setT10("Draft");
			}
			if (data.getN7() == 2) {
				data.setT10("Pending");
			}
			if (data.getN7() == 3) {
				data.setT10("Modification");
			}
			if (data.getN7() == 4) {
				data.setT10("Approve");
			}
			if (data.getN7() == 5) {
				data.setT10("Publish");
			}
			if (data.getN7() == 6) {
				data.setT10("Reject");
			}
			data.setModifiedDate(rs.getString("modifieddate"));
			data.setModifiedTime(rs.getString("modifiedtime"));
			data.setUserName(rs.getString("username"));
			data.setModifiedUserId(rs.getString("modifieduserid"));
			data.setModifiedUserName(rs.getString("modifiedusername"));
			data.setUpload(UploadDao.search(String.valueOf(data.getSyskey()), "", conn).getUploads());
			data.setVideoUpload(UploadDao.searchVideoList(String.valueOf(data.getSyskey()), conn).getVideoUpload());
			datalist.add(data);
		}
		res.setPageSize(pgdata.getSize());
		res.setCurrentPage(pgdata.getCurrent());
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		String qry = "";
		if (status == 5) { // admin
			qry = "select count(*) as recCount from ( SELECT ROW_NUMBER() OVER (ORDER BY syskey)AS RowNum , * from fmr002 "
					+ whereclause
					+ " and userid in (select t1 from uvm012 where syskey in (select n4 from   uvm005 where syskey in (select n1 from jun002 where n2 in(select syskey from uvm009 where t2<>'master'  )))))AS RowConstrainedResult";
		} else if (status == 1) { // content_writer
			qry = "select count(*) as recCount from ( SELECT ROW_NUMBER() OVER (ORDER BY syskey)AS RowNum , * from fmr002 "
					+ whereclause + " and userid = '" + pgdata.getUserid()
					+ "' and userid in (select t1 from uvm012 where syskey in (select n4 from   uvm005 where syskey in (select n1 from jun002 where n2 in(select syskey from uvm009 where t2='Content Writer')))))AS RowConstrainedResult";
		} else {
			if (regionCode.equals("00000000")) {
				qry = " SELECT count(*) as recCount FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc) AS RowNum,* FROM ( select * from fmr002 "
						+ whereclause + ") b)  AS RowConstrainedResult";
			} else if (regionCode.equals("10000000")) {
				qry = " SELECT count(*) as recCount FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc) AS RowNum,* FROM ( select * from fmr002 "
						+ whereclause + "and t5!='13000000') b)  AS RowConstrainedResult";
			} else if (regionCode.equals("13000000")) {
				qry = " SELECT count(*) as recCount FROM ( SELECT ROW_NUMBER() OVER (ORDER BY syskey desc) AS RowNum,* FROM ( select * from fmr002 "
						+ whereclause + "and t5!='10000000') b)  AS RowConstrainedResult";
			}
		}
		// stat = conn.prepareStatement("SELECT COUNT(*) AS recCount FROM FMR002
		// " + whereclause);
		stat = conn.prepareStatement(qry);
		ResultSet result = stat.executeQuery();
		result.next();
		res.setTotalCount(result.getInt("recCount"));
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}

	public Resultb2b updateLikeCountVideo(Long syskey, String userSK, String type, Connection conn, MrBean user)
			throws SQLException {
		long key = SysKeyMgr.getSysKey(1, "syskey", ConnAdmin.getConn(user.getUser().getOrganizationID()));
		Resultb2b res = new Resultb2b();
		long userKey = Long.parseLong(userSK);
		RegisterData userData = RegisterDao.readData(userKey, conn);

		res.setN1(ArticleDao.searchDisLikeOrNot(syskey, userSK, conn));
		// dislike count -1
		if (isDisLikeExist(syskey, userKey, conn)) {
			String sql = "Update FMR002 SET n8 = n8-1 WHERE syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			stmt.executeUpdate();

			// update like status
			String query = "UPDATE FMR008 SET RecordStatus=4 WHERE  RecordStatus = 1 AND t4='dislike' AND  n1 = '"
					+ syskey + "' AND n2 = '" + userKey + "'";
			PreparedStatement st = conn.prepareStatement(query);
			st.executeUpdate();

		}

		if (!isLikeExist(syskey, userKey, conn)) {
			String sql = "Update FMR002 SET n2 = n2+1 WHERE syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			// stmt.setLong(2, userKey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String todayDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
				if (!isStatusExist(syskey, userKey, "like", conn)) {
					String query = "INSERT INTO FMR008 (syskey,createddate, modifieddate, userid, username, RecordStatus, SyncStatus, SyncBatch, usersyskey,T1, T2,T3, T4,T5, T6,T7, T8,T9, T10,T11, T12,T13, T14,T15, n1,n2,n3, n4,n5, n6,n7, n8,n9, n10,n11, n12,n13, n14,n15) "
							+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					PreparedStatement psmt = conn.prepareStatement(query);

					psmt.setLong(1, key);
					psmt.setString(2, todayDate);
					psmt.setString(3, todayDate);
					psmt.setString(4, userData.getT1());
					psmt.setString(5, userData.getT3());
					psmt.setInt(6, 1);
					psmt.setInt(7, 1);
					psmt.setLong(8, 0);
					psmt.setLong(9, 0);
					psmt.setString(10, userData.getT1());
					psmt.setString(11, userData.getT3());
					psmt.setString(12, type);
					psmt.setString(13, "like");
					psmt.setString(14, "0");
					psmt.setString(15, "0");
					psmt.setString(16, "0");
					psmt.setString(17, "0");
					psmt.setString(18, "0");
					psmt.setString(19, "0");
					psmt.setString(20, "0");
					psmt.setString(21, "0");
					psmt.setString(22, "0");
					psmt.setString(23, "0");
					psmt.setString(24, "0");
					psmt.setLong(25, syskey);
					psmt.setLong(26, userKey);
					psmt.setLong(27, 0);
					psmt.setLong(28, 0);
					psmt.setLong(29, 0);
					psmt.setLong(30, 0);
					psmt.setLong(31, 0);
					psmt.setLong(32, 0);
					psmt.setLong(33, 0);
					psmt.setLong(34, 0);
					psmt.setLong(35, 0);
					psmt.setLong(36, 0);
					psmt.setLong(37, 0);
					psmt.setLong(38, 0);
					psmt.setLong(39, 0);
					if (psmt.executeUpdate() > 0) {
					} else {
					}
				} else {
					String query = "UPDATE FMR008 SET RecordStatus=1 WHERE  RecordStatus = 4 AND t4='like' AND n1 = '"
							+ syskey + "' AND n2 = '" + userKey + "'";
					PreparedStatement stm = conn.prepareStatement(query);

					int result = stm.executeUpdate();
					if (result > 0) {
						res.setState(true);
					} else {
						res.setState(false);
					}

				}

			}
		} else {
			String sql = "Update FMR002 SET n2 = n2-1 WHERE  RecordStatus = 1 AND syskey = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, syskey);
			int rs = stmt.executeUpdate();
			if (rs > 0) {
				res.setState(true);
				String query = "UPDATE FMR008 SET RecordStatus=4 WHERE  RecordStatus = 1 AND t4='like' AND n1 = '"
						+ syskey + "' AND n2 = '" + userKey + "'";
				PreparedStatement stm = conn.prepareStatement(query);

				int result = stm.executeUpdate();
				if (result > 0) {
					res.setState(true);
				} else {
					res.setState(false);
				}

			} else {
				res.setState(false);
			}

		}
		res.setN2(getTotalDisLikeCount(syskey, conn));
		res.setKeyResult(getTotalLikeCount(syskey, conn));
		return res;

	}

	public ArticleDataSet viewArticle(String searchVal, Connection conn) throws SQLException {
		ArticleDataSet res = new ArticleDataSet();
		ArrayList<ArticleData> datalist = new ArrayList<ArticleData>();
		String whereclause = " WHERE RecordStatus<>4 AND RecordStatus = 1  ";
		if (!searchVal.equals("")) {
			whereclause += "AND syskey =" + searchVal;
		}
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), whereclause, " ORDER BY autokey desc", conn);
		for (int i = 0; i < dbrs.size(); i++) {
			datalist.add(getArticalDBRecord(dbrs.get(i)));
		}
		if (datalist.size() > 0) {
			res.setState(true);
		} else {
			res.setState(false);
		}
		ArticleData[] dataarray = new ArticleData[datalist.size()];
		dataarray = datalist.toArray(dataarray);
		res.setData(dataarray);
		return res;
	}
}
