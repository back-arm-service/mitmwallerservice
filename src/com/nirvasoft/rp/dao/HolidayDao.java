package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class HolidayDao {
	public static String mTableName1 = "LastDates";
	public static String mTableName2 = "BankHoliday";

	public boolean getBankHolidayCheck(String pDate, Connection conn) throws SQLException {
		boolean result = false;
		PreparedStatement pstmt = conn
				.prepareStatement("SELECT * FROM " + mTableName2 + " WHERE CONVERT(DATETIME,HolidayDate) = ?");
		pstmt.setString(1, pDate);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			result = true;
		}
		return result;
	}

	public boolean isRunnedEOD(String pProcessedDate, Connection conn) throws SQLException {
		boolean result = false;
		String l_Query = "SELECT * FROM " + mTableName1 + " WHERE CONVERT(DateTime,LastProcessedDate) = ?";
		PreparedStatement pstmt = conn.prepareStatement(l_Query);
		pstmt.setString(1, pProcessedDate);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			result = true;
		}
		return result;
	}
}
