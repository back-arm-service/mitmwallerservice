
package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.nirvasoft.rp.data.CityStateData;
import com.nirvasoft.rp.data.RegisterRequestData;
import com.nirvasoft.rp.data.RegisterResTest;
import com.nirvasoft.rp.data.RegisterResponseData;
import com.nirvasoft.rp.data.UserInfoResponse;
import com.nirvasoft.rp.mgr.SessionMgr;
import com.nirvasoft.rp.shared.RegisterData;
import com.nirvasoft.rp.shared.SyskeyResponseData;
import com.nirvasoft.rp.shared.WalletRequestData;
import com.nirvasoft.rp.shared.WalletResponseData;

public class UserRegistrationDao {

	public boolean checkUser(String request, Connection pConn) throws SQLException {
		String l_Query = "SELECT UserId FROM Register WHERE Syskey = ? AND RecordStatus <> 4";
		PreparedStatement pstmt = pConn.prepareStatement(l_Query);
		pstmt.setString(1, request);
		ResultSet rs = pstmt.executeQuery();
		if (rs.next())
			return true;
		else
			return false;
	}
	
	public WalletResponseData checkUserByUserID(String userID, Connection pConn) throws SQLException {
		WalletResponseData ret = new WalletResponseData();
		String l_Query = "SELECT AccNumber,UserSysKey FROM Register r , WJunction w WHERE r.userid = w.UserID and r.userid = ? AND r.RecordStatus <> 4";
		PreparedStatement pstmt = pConn.prepareStatement(l_Query);
		pstmt.setString(1, userID);
		ResultSet rs = pstmt.executeQuery();
		if (rs.next()){
			ret.setMessageCode("0000");
			ret.setMessageDesc("User is already registered");
			ret.setsKey(String.valueOf(rs.getLong("UserSysKey")));
			ret.setAccountNo(rs.getString("AccNumber"));
		}
		else{
			ret.setMessageCode("0014");
		}
		return ret;
	}

	public WalletResponseData checkUserRegistrationV2(WalletRequestData data, Connection pConn) throws SQLException {
		WalletResponseData response = new WalletResponseData();
		boolean isUserValid = false;
		String token=data.getFcmToken();
		String deviceid=data.getDeviceId();
		long hkey = 0;
		// userid = '+959784412809'
		// syskey t51 username t7 t36 t16
		// 491 40059 Thura Aung MANDALAY 10000000 762018144245734.jpg
		String l_Query = "select syskey, t16, t51, username, t7, t36 from Register Where userid=? and RecordStatus <> 4";
		PreparedStatement pstmt = pConn.prepareStatement(l_Query);
		pstmt.setString(1, data.getUserID());
		ResultSet rs = pstmt.executeQuery();

		if (rs.next()) {
			hkey = (Long.parseLong(rs.getString("t51")));

			response.setsKey(String.valueOf(rs.getLong("syskey")));
			response.setName(rs.getString("username"));
			// t7="MANDALAY" OR "YANGON" OR "-"
			response.setRegion(rs.getString("t7"));
			response.setRegionNumber(rs.getString("t36"));
			// in order to get Photo Name
			String imageName = rs.getString("t16");
			if (imageName != null && imageName.length() != 0) {
				response.setT16(imageName);
			} else {
				response.setT16("user-icon.png");
			}
			isUserValid = true;
		}

		// AccNumber
		// 0020101100026360
		if (isUserValid) {
			if(token!=null && deviceid!=null){
			String query="update Register set t9=?,t10=? where userid=?";
			PreparedStatement pstmt2 = pConn.prepareStatement(query);
			int i = 1;
//			pstmt2.setString(i++, data.getT8());
			pstmt2.setString(i++,data.getDeviceId() );
			pstmt2.setString(i++,data.getFcmToken());
			
			pstmt2.setString(i++,data.getUserID());
//			pstmt2.setString(i++,data.getSessionID());
			int result=pstmt2.executeUpdate();
			if (result > 0) {
				response.setMessageCode("0000");
				response.setMessageDesc("User updated successfully.");
			} else {
				response.setMessageCode("0014");
				response.setMessageDesc("User updates failed.");
			}
			}
			if( token==null && deviceid!=null ){
				String query="update Register set t9=? where userid=?";
				PreparedStatement pstmt2 = pConn.prepareStatement(query);
				int i = 1;
//				pstmt2.setString(i++,data.getT8());
				pstmt2.setString(i++,data.getDeviceId());
				pstmt2.setString(i++,data.getUserID());
//				pstmt2.setString(i++,data.getSessionID());
				int result=pstmt2.executeUpdate();
				if (result > 0) {
					response.setMessageCode("0000");
					response.setMessageDesc("User updated successfully.");
				} else {
					response.setMessageCode("0014");
					response.setMessageDesc("User updates failed.");
				}
			}
		    l_Query = "select AccNumber from WJunction Where HKey=? and RegType=?";
			PreparedStatement pstmt1 = pConn.prepareStatement(l_Query);
			pstmt1.setLong(1, hkey);
			pstmt1.setInt(2, 0);
			ResultSet rs1 = pstmt1.executeQuery();
			if (rs1.next()) {
				response.setAccountNo(rs1.getString("AccNumber"));
			}
		}

		return response;
	}

	public WalletResponseData checkUserRegristrationDC(WalletRequestData data, Connection pConn) throws SQLException {
		WalletResponseData response = new WalletResponseData();
		boolean isUserValid = false;
		long hkey = 0;
		// userid = '+959784412809'
		// syskey username t16 t7 t36 t51
		// 491 Thura Aung 762018144245734.jpg MANDALAY 10000000 40059
		String l_Query = "SELECT * FROM Register WHERE userid=? AND recordstatus<>4";
		PreparedStatement pstmt = pConn.prepareStatement(l_Query);
		pstmt.setString(1, data.getUserID());
		ResultSet rs = pstmt.executeQuery();

		if (rs.next()) {
			hkey = (Long.parseLong(rs.getString("t51")));
			response.setUserId(rs.getString("userid"));
			response.setsKey(String.valueOf(rs.getLong("syskey")));
			response.setName(rs.getString("username"));
			// in order to get Photo Name
			response.setT16(rs.getString("t16"));
			// t7="YANGON", "MANDALAY" or "-"
			response.setRegion(rs.getString("t7"));
			// t36="13000000", "10000000" or "00000000"
			response.setRegionNumber(rs.getString("t36"));

			isUserValid = true;
		}

		// accnumber
		// 0020101100026360
		if (isUserValid) {
			l_Query = "SELECT accnumber FROM WJunction WHERE hkey=? and regtype=?";
			PreparedStatement pstmt1 = pConn.prepareStatement(l_Query);
			pstmt1.setLong(1, hkey);
			pstmt1.setInt(2, 0);
			ResultSet rs1 = pstmt1.executeQuery();
			if (rs1.next()) {
				response.setAccountNo(rs1.getString("accnumber"));
			}
		}

		return response;
	}

	public boolean checkUUID(WalletRequestData data, Connection pConn) throws SQLException {
		String l_Query = "SELECT syskey FROM Register WHERE userid=? AND RecordStatus <> 4 AND t9=?";
		PreparedStatement pstmt = pConn.prepareStatement(l_Query);
		pstmt.setString(1, data.getUserID());
		pstmt.setString(2, data.getField1());
		ResultSet rs = pstmt.executeQuery();

		if (rs.next()) {
			return true;
		} else {
			return false;
		}
	}

	public boolean checkUUIDDC(WalletRequestData data, Connection pConn) throws SQLException {
		String l_Query = "SELECT syskey FROM Register WHERE userid=? AND RecordStatus <> 4 AND t9=?";
		PreparedStatement pstmt = pConn.prepareStatement(l_Query);
		pstmt.setString(1, data.getUserID());
		pstmt.setString(2, data.getDeviceId());
		ResultSet rs = pstmt.executeQuery();

		if (rs.next()) {
			return true;
		} else {
			return false;
		}
	}

	private long generateKey(String userid, String sessionid, String phoneNo, String cusName, String nrc, int regType,
			Connection mConn) throws SQLException {
		long l_RefKey = 0;
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		String l_Query = "Insert Into UserRegistration ([UserID],[SessionID],[CreatedDate],[Name],[NRC]"
				+ ",[PhoneNo],[Status],[RegType]) Values (?,?,?,?,?,?,?,?)";
		PreparedStatement ps = mConn.prepareStatement(l_Query, Statement.RETURN_GENERATED_KEYS);
		int i = 1;
		ps.setString(i++, userid);
		ps.setString(i++, sessionid);
		ps.setString(i++, date);
		ps.setString(i++, cusName);
		ps.setString(i++, nrc);
		ps.setString(i++, phoneNo);
		ps.setInt(i++, 0);
		ps.setInt(i++, regType);
		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys();
		if (rs != null && rs.next()) {
			l_RefKey = rs.getLong(1);
		}

		rs.close();
		ps.close();
		return l_RefKey;
	}

	public List<RegisterResponseData> getAllRegister(Connection conn) throws SQLException {
		List<RegisterResponseData> resList = new ArrayList<RegisterResponseData>();

		String sql = "SELECT * FROM Register;";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			RegisterResponseData response = new RegisterResponseData();
			response.setSyskey(rs.getLong("syskey"));
			response.setCreatedDate(rs.getString("createddate"));
			response.setModifiedDate(rs.getString("modifieddate"));
			response.setUserID(rs.getString("userid"));
			response.setUserName(rs.getString("username"));
			response.setSyskey(rs.getLong("syskey"));
			response.setT1(rs.getString("t1"));
			response.setT2(rs.getString("t2"));
			response.setT3(rs.getString("t3"));
			response.setT4(rs.getString("t4"));
			response.setT5(rs.getString("t5"));
			response.setT6(rs.getString("t6"));
			response.setT7(rs.getString("t7"));
			response.setT8(rs.getString("t8"));
			response.setT9(rs.getString("t9"));
			response.setT10(rs.getString("t10"));
			response.setT11(rs.getString("t11"));
			response.setT12(rs.getString("t12"));
			response.setT13(rs.getString("t13"));
			response.setT14(rs.getString("t14"));
			response.setT15(rs.getString("t15"));
			response.setT16(rs.getString("t16"));
			response.setT17(rs.getString("t17"));
			response.setT18(rs.getString("t18"));
			response.setT19(rs.getString("t19"));
			response.setT20(rs.getString("t20"));
			response.setT21(rs.getString("t21"));
			response.setT22(rs.getString("t22"));
			response.setT23(rs.getString("t23"));
			response.setT24(rs.getString("t24"));
			response.setT25(rs.getString("t25"));
			response.setT26(rs.getString("t26"));
			response.setT27(rs.getString("t27"));
			response.setT28(rs.getString("t28"));
			response.setT29(rs.getString("t29"));
			response.setT30(rs.getString("t30"));
			response.setT31(rs.getString("t31"));
			response.setT32(rs.getString("t32"));
			response.setT33(rs.getString("t33"));
			response.setT34(rs.getString("t34"));
			response.setT35(rs.getString("t35"));
			response.setT36(rs.getString("t36"));
			response.setT37(rs.getString("t37"));
			response.setT38(rs.getString("t38"));
			response.setT39(rs.getString("t39"));
			response.setT40(rs.getString("t40"));
			response.setT41(rs.getString("t41"));
			response.setT42(rs.getString("t42"));
			response.setT43(rs.getString("t43"));
			response.setT44(rs.getString("t44"));
			response.setT45(rs.getString("t45"));
			response.setT46(rs.getString("t46"));
			response.setT47(rs.getString("t47"));
			response.setT48(rs.getString("t48"));
			response.setT49(rs.getString("t49"));
			response.setT50(rs.getString("t50"));
			response.setUsersyskey(rs.getLong("UserSysKey"));
			response.setT51(rs.getString("t51"));
			response.setT52(rs.getString("t52"));
			response.setT53(rs.getString("t53"));
			response.setT54(rs.getString("t54"));
			response.setT55(rs.getString("t55"));
			response.setT56(rs.getString("t56"));
			response.setT57(rs.getString("t57"));
			response.setT58(rs.getString("t58"));
			response.setT59(rs.getString("t59"));
			response.setT60(rs.getString("t60"));
			response.setT61(rs.getString("t61"));
			response.setT62(rs.getString("t62"));
			response.setT63(rs.getString("t63"));
			response.setT64(rs.getString("t64"));
			response.setT65(rs.getString("t65"));
			response.setT66(rs.getString("t66"));
			response.setT67(rs.getString("t67"));
			response.setT68(rs.getString("t68"));
			response.setT69(rs.getString("t69"));
			response.setT70(rs.getString("t70"));
			response.setT71(rs.getString("t71"));
			response.setT72(rs.getString("t72"));
			response.setT73(rs.getString("t73"));
			response.setT74(rs.getString("t74"));
			response.setT75(rs.getString("t75"));
			response.setT76(rs.getString("t76"));
			response.setT77(rs.getString("t77"));
			response.setT78(rs.getString("t78"));
			response.setT79(rs.getString("t79"));
			response.setT80(rs.getString("t80"));

			resList.add(response);
		}

		rs.close();
		stmt.close();

		return resList;
	}

	public ArrayList<RegisterData> getChannelList(Connection conn) throws SQLException {
		ArrayList<RegisterData> ret = new ArrayList<RegisterData>();
		String sql = "select syskey, userid, username from REGISTER   where userid in ('959000000001', '959000000002', '959000000003') order by autokey desc";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			RegisterData data = new RegisterData();
			data.setSyskey(rs.getLong(1));
			data.setUserid(rs.getString(2));
			data.setUsername(rs.getString(3));
			ret.add(data);
		}
		return ret;
	}

	public String[] getChannelSysKey(String newRegion, Connection pConn) throws SQLException {
		String[] sysKey = new String[3];

		String l_Query = "select * from Channels Where Region=?";
		PreparedStatement pstmt = pConn.prepareStatement(l_Query);
		pstmt.setString(1, newRegion);
		ResultSet rs = pstmt.executeQuery();

		int i = 0;

		while (rs.next()) {
			sysKey[i++] = String.valueOf(rs.getLong("ChannelSysKey"));
		}

		return sysKey;
	}

	public String[] getChannelSysKeyDC(Connection pConn) throws SQLException {
		String[] sysKeyArr = null;

		List<String> sysList = new ArrayList<String>();

		String l_Query = "select * from Channels;";
		PreparedStatement pstmt = pConn.prepareStatement(l_Query);
		ResultSet rs = pstmt.executeQuery();

		int i = 0;

		while (rs.next()) {
			sysList.add(String.valueOf(rs.getLong("channelsyskey")));

		}

		if (sysList.size() != 0) {
			sysKeyArr = new String[sysList.size()];

			for (String temp : sysList) {
				sysKeyArr[i++] = temp;
			}
		}

		return sysKeyArr;
	}

	public List<CityStateData> getCityList(Connection conn, String state) throws SQLException {

		ArrayList<CityStateData> datalist = new ArrayList<CityStateData>();

		String splitstate = "";
		if (!(state.equals("") || state.equalsIgnoreCase("null"))) {
			splitstate = state.substring(0, 4);

			String sql = "select code,DespEng,DespMyan from addressref where code like '" + splitstate
					+ "%' AND code <> '" + splitstate
					+ "0000'  AND code <> '00000000' and DespEng <> 'All' ORDER BY Code";
			PreparedStatement stmt = conn.prepareStatement(sql);
			ResultSet res = stmt.executeQuery();
			while (res.next()) {
				CityStateData combo = new CityStateData();
				combo.setDespEng(res.getString("DespEng"));
				combo.setCode(res.getString("code"));
				datalist.add(combo);
			}
		}
		return datalist;
	}

	public String getImageName(long syskey, Connection pConn) throws SQLException {
		String imageName = "user-icon.png";

		String l_Query = "select t16 from Register Where syskey=?";
		PreparedStatement pstmt = pConn.prepareStatement(l_Query);
		pstmt.setLong(1, syskey);
		ResultSet rs = pstmt.executeQuery();

		if (rs.next()) {
			imageName = rs.getString("t16");
		}

		return imageName;
	}

	public WalletResponseData getName(WalletRequestData data, Connection pConn) throws SQLException {
		WalletResponseData response = new WalletResponseData();
		// userid = '+959784412809'
		// username
		// Thura Aung
		String l_Query = "select username from Register Where userid=? and RecordStatus <> 4";
		PreparedStatement pstmt = pConn.prepareStatement(l_Query);
		pstmt.setString(1, data.getUserID());
		ResultSet rs = pstmt.executeQuery();

		if (rs.next()) {
			response.setMessageCode("0000");
			response.setName(rs.getString("username"));
		} else {
			response.setMessageCode("0014");
			response.setName("");
		}

		return response;
	}

	// t7=region(E.G. "YANGON", "MANDALAY", or "-")
	public RegisterResponseData getRegisterDC(long sysKey, Connection conn) throws SQLException {
		String query = "SELECT * FROM Register WHERE syskey=?";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setLong(1, sysKey);
		ResultSet rs = ps.executeQuery();

		RegisterResponseData response = new RegisterResponseData();

		if (rs.next()) {
			response.setMessageCode("0000");
			response.setMessageDesc("Getting user information is successful.");
			response.setSyskey(rs.getLong("syskey"));
			response.setCreatedDate(rs.getString("createddate"));
			response.setModifiedDate(rs.getString("modifieddate"));
			response.setUserID(rs.getString("userid"));
			response.setUserName(rs.getString("username"));
			response.setSyskey(rs.getLong("syskey"));
			response.setT1(rs.getString("t1"));
			response.setT2(rs.getString("t2"));
			response.setT3(rs.getString("t3"));
			response.setT4(rs.getString("t4"));
			response.setT5(rs.getString("t5"));
			response.setT6(rs.getString("t6"));
			response.setT7(rs.getString("t7"));
			response.setT8(rs.getString("t8"));
			response.setT9(rs.getString("t9"));
			response.setT10(rs.getString("t10"));
			response.setT11(rs.getString("t11"));
			response.setT12(rs.getString("t12"));
			response.setT13(rs.getString("t13"));
			response.setT14(rs.getString("t14"));
			response.setT15(rs.getString("t15"));
			response.setT16(rs.getString("t16"));
			response.setT17(rs.getString("t17"));
			response.setT18(rs.getString("t18"));
			response.setT19(rs.getString("t19"));
			response.setT20(rs.getString("t20"));
			response.setT21(rs.getString("t21"));
			response.setT22(rs.getString("t22"));
			response.setT23(rs.getString("t23"));
			response.setT24(rs.getString("t24"));
			response.setT25(rs.getString("t25"));
			response.setT26(rs.getString("t26"));
			response.setT27(rs.getString("t27"));
			response.setT28(rs.getString("t28"));
			response.setT29(rs.getString("t29"));
			response.setT30(rs.getString("t30"));
			response.setT31(rs.getString("t31"));
			response.setT32(rs.getString("t32"));
			response.setT33(rs.getString("t33"));
			response.setT34(rs.getString("t34"));
			response.setT35(rs.getString("t35"));
			response.setT36(rs.getString("t36"));
			response.setT37(rs.getString("t37"));
			response.setT38(rs.getString("t38"));
			response.setT39(rs.getString("t39"));
			response.setT40(rs.getString("t40"));
			response.setT41(rs.getString("t41"));
			response.setT42(rs.getString("t42"));
			response.setT43(rs.getString("t43"));
			response.setT44(rs.getString("t44"));
			response.setT45(rs.getString("t45"));
			response.setT46(rs.getString("t46"));
			response.setT47(rs.getString("t47"));
			response.setT48(rs.getString("t48"));
			response.setT49(rs.getString("t49"));
			response.setT50(rs.getString("t50"));
			response.setUsersyskey(rs.getLong("UserSysKey"));
			response.setT51(rs.getString("t51"));
			response.setT52(rs.getString("t52"));
			response.setT53(rs.getString("t53"));
			response.setT54(rs.getString("t54"));
			response.setT55(rs.getString("t55"));
			response.setT56(rs.getString("t56"));
			response.setT57(rs.getString("t57"));
			response.setT58(rs.getString("t58"));
			response.setT59(rs.getString("t59"));
			response.setT60(rs.getString("t60"));
			response.setT61(rs.getString("t61"));
			response.setT62(rs.getString("t62"));
			response.setT63(rs.getString("t63"));
			response.setT64(rs.getString("t64"));
			response.setT65(rs.getString("t65"));
			response.setT66(rs.getString("t66"));
			response.setT67(rs.getString("t67"));
			response.setT68(rs.getString("t68"));
			response.setT69(rs.getString("t69"));
			response.setT70(rs.getString("t70"));
			response.setT71(rs.getString("t71"));
			response.setT72(rs.getString("t72"));
			response.setT73(rs.getString("t73"));
			response.setT74(rs.getString("t74"));
			response.setT75(rs.getString("t75"));
			response.setT76(rs.getString("t76"));
			response.setT77(rs.getString("t77"));
			response.setT78(rs.getString("t78"));
			response.setT79(rs.getString("t79"));
			response.setT80(rs.getString("t80"));
		} else {
			response.setMessageCode("0014");
			response.setMessageDesc("There is no user.");
		}

		return response;
	}

	public List<CityStateData> getStateList(Connection conn) throws SQLException {
		List<CityStateData> stateList = new ArrayList<CityStateData>();

		CityStateData stateForGeneral = new CityStateData();
		stateForGeneral.setDespEng("GENERAL");
		stateForGeneral.setCode("ALL");
		stateList.add(stateForGeneral);

		String sql = "SELECT code, despeng, despmyan FROM AddressRef WHERE code LIKE '%000000' AND code<>'00000000' AND despeng<>'All' ORDER BY code;";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			CityStateData state = new CityStateData();
			state.setDespEng(rs.getString("DespEng"));
			state.setCode(rs.getString("Code"));
			stateList.add(state);
		}

		rs.close();
		stmt.close();

		return stateList;
	}

	public List<CityStateData> getStateListDC(Connection conn) throws SQLException {
		List<CityStateData> stateList = new ArrayList<CityStateData>();

		CityStateData stateForGeneral = new CityStateData();
		stateForGeneral.setDespEng("GENERAL");
		stateForGeneral.setCode("ALL");
		stateList.add(stateForGeneral);

		String sql = "SELECT code, despeng, despmyan FROM AddressRef WHERE code LIKE '%000000' "
				// + "AND code<>'00000000' "
				+ "AND despeng<>'All' ORDER BY code;";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			CityStateData state = new CityStateData();
			state.setDespEng(rs.getString("DespEng"));
			state.setCode(rs.getString("Code"));
			stateList.add(state);
		}

		rs.close();
		stmt.close();

		return stateList;
	}

	public SyskeyResponseData getTosyskey(String beneficiaryID, String sessionID, Connection conn) throws SQLException {
		String query = "SELECT Autokey FROM UserRegistration WHERE userid=? AND status <> 4";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, beneficiaryID);
		ResultSet rs = ps.executeQuery();

		SyskeyResponseData response = new SyskeyResponseData();

		if (rs.next()) {
			response.setMessageCode("0000");
			response.setMessageDesc("Getting user information is successful.");
			response.setSysKey(rs.getLong("Autokey"));
		} else {
			response.setMessageCode("0014");
			response.setMessageDesc("There is no user.");
		}

		return response;
	}

	public RegisterResponseData registerInsert(RegisterRequestData request, Connection conn) throws SQLException {
		RegisterResponseData response = new RegisterResponseData();

		int result = 0;

		String query = "INSERT INTO Register(syskey,createddate,modifieddate,userid,username,"
				+ "recordstatus,syncstatus,syncbatch,t1,t2," + "t3,t4,t5,t6,t7," + "t8,t9,t10,t11,t12,"
				+ "t13,t14,t15,t16,t17," + "t18,t19,t20,t21,t22," + "t23,t24,t25,t26,t27," + "t28,t29,t30,t31,t32,"
				+ "t33,t34,t35,t36,t37," + "t38,t39,t40,t41,t42," + "t43,t44,t45,t46,t47,"
				+ "t48,t49,t50,usersyskey,t51," + "t52,t53,t54,t55,t56," + "t57,t58,t59,t60,t61,"
				+ "t62,t63,t64,t65,t66," + "t67,t68,t69,t70,t71," + "t72,t73,t74,t75,t76,"
				+ "t77,t78,t79,t80) VALUES (?, ?, ?, ?, ?, " + "?, ?, ?, ?, ?, " + "?, ?, ?, ?, ?, " + "?, ?, ?, ?, ?, "
				+ "?, ?, ?, ?, ?, " + "?, ?, ?, ?, ?, " + "?, ?, ?, ?, ?, " + "?, ?, ?, ?, ?, " + "?, ?, ?, ?, ?, "
				+ "?, ?, ?, ?, ?, " + "?, ?, ?, ?, ?, " + "?, ?, ?, ?, ?, " + "?, ?, ?, ?, ?, " + "?, ?, ?, ?, ?, "
				+ "?, ?, ?, ?, ?, " + "?, ?, ?, ?, ?, " + "?, ?, ?, ?, ?, " + "?, ?, ?, ?);";
		PreparedStatement ps = conn.prepareStatement(query);

		int i = 1;
		ps.setLong(i++, request.getSyskey());
		ps.setString(i++, request.getCreatedDate());
		ps.setString(i++, request.getModifiedDate());
		ps.setString(i++, request.getUserID());
		ps.setString(i++, request.getUserName());
		ps.setInt(i++, request.getRecordstatus());
		ps.setInt(i++, request.getSyncstatus());
		ps.setInt(i++, request.getSyncbatch());
		ps.setString(i++, request.getT1());
		ps.setString(i++, request.getT2());
		ps.setString(i++, request.getT3());
		ps.setString(i++, request.getT4());
		ps.setString(i++, request.getT5());
		ps.setString(i++, request.getT6());
		ps.setString(i++, request.getT7());
		ps.setString(i++, request.getT8());
		ps.setString(i++, request.getT9());
		ps.setString(i++, request.getT10());
		ps.setString(i++, request.getT11());
		ps.setString(i++, request.getT12());
		ps.setString(i++, request.getT13());
		ps.setString(i++, request.getT14());
		ps.setString(i++, request.getT15());
		ps.setString(i++, request.getT16());
		ps.setString(i++, request.getT17());
		ps.setString(i++, request.getT18());
		ps.setString(i++, request.getT19());
		ps.setString(i++, request.getT20());
		ps.setString(i++, request.getT21());
		ps.setString(i++, request.getT22());
		ps.setString(i++, request.getT23());
		ps.setString(i++, request.getT24());
		ps.setString(i++, request.getT25());
		ps.setString(i++, request.getT26());
		ps.setString(i++, request.getT27());
		ps.setString(i++, request.getT28());
		ps.setString(i++, request.getT29());
		ps.setString(i++, request.getT30());
		ps.setString(i++, request.getT31());
		ps.setString(i++, request.getT32());
		ps.setString(i++, request.getT33());
		ps.setString(i++, request.getT34());
		ps.setString(i++, request.getT35());
		ps.setString(i++, request.getT36());
		ps.setString(i++, request.getT37());
		ps.setString(i++, request.getT38());
		ps.setString(i++, request.getT39());
		ps.setString(i++, request.getT40());
		ps.setString(i++, request.getT41());
		ps.setString(i++, request.getT42());
		ps.setString(i++, request.getT43());
		ps.setString(i++, request.getT44());
		ps.setString(i++, request.getT45());
		ps.setString(i++, request.getT46());
		ps.setString(i++, request.getT47());
		ps.setString(i++, request.getT48());
		ps.setString(i++, request.getT49());
		ps.setString(i++, request.getT50());
		ps.setLong(i++, request.getUsersyskey());
		ps.setString(i++, request.getT51());
		ps.setString(i++, request.getT52());
		ps.setString(i++, request.getT53());
		ps.setString(i++, request.getT54());
		ps.setString(i++, request.getT55());
		ps.setString(i++, request.getT56());
		ps.setString(i++, request.getT57());
		ps.setString(i++, request.getT58());
		ps.setString(i++, request.getT59());
		ps.setString(i++, request.getT60());
		ps.setString(i++, request.getT61());
		ps.setString(i++, request.getT62());
		ps.setString(i++, request.getT63());
		ps.setString(i++, request.getT64());
		ps.setString(i++, request.getT65());
		ps.setString(i++, request.getT66());
		ps.setString(i++, request.getT67());
		ps.setString(i++, request.getT68());
		ps.setString(i++, request.getT69());
		ps.setString(i++, request.getT70());
		ps.setString(i++, request.getT71());
		ps.setString(i++, request.getT72());
		ps.setString(i++, request.getT73());
		ps.setString(i++, request.getT74());
		ps.setString(i++, request.getT75());
		ps.setString(i++, request.getT76());
		ps.setString(i++, request.getT77());
		ps.setString(i++, request.getT78());
		ps.setString(i++, request.getT79());
		ps.setString(i, request.getT80());

		result = ps.executeUpdate();

		if (result > 0) {
			response.setMessageCode("0000");
			response.setMessageDesc("User registered successfully.");
		} else {
			response.setMessageCode("0014");
			response.setMessageDesc("User registration was unsuccessful.");
		}

		ps.close();

		return response;
	}

	// t7=region(E.G. "YANGON", "YANGON,MANDALAY")
	public RegisterResponseData registerSelect(long sysKey, String t7, Connection conn) throws SQLException {
		String region = "";
		String regionNumber = "";
		String query = "SELECT * FROM Register WHERE syskey=?";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setLong(1, sysKey);
		ResultSet rs = ps.executeQuery();

		RegisterResponseData response = new RegisterResponseData();

		if (rs.next()) {
			response.setMessageCode("0000");
			response.setMessageDesc("Getting user information is successful.");
			response.setSyskey(rs.getLong("syskey"));
			response.setCreatedDate(rs.getString("createddate"));
			response.setModifiedDate(rs.getString("modifieddate"));
			response.setUserID(rs.getString("userid"));
			response.setUserName(rs.getString("username"));
			response.setSyskey(rs.getLong("syskey"));
			response.setT1(rs.getString("t1"));
			response.setT2(rs.getString("t2"));
			response.setT3(rs.getString("t3"));
			response.setT4(rs.getString("t4"));
			response.setT5(rs.getString("t5"));
			response.setT6(rs.getString("t6"));

			region = rs.getString("t7");

			if (region.contains(t7)) {
				response.setT7(t7.toUpperCase());
			}

			response.setT8(rs.getString("t8"));
			response.setT9(rs.getString("t9"));
			response.setT10(rs.getString("t10"));
			response.setT11(rs.getString("t11"));
			response.setT12(rs.getString("t12"));
			response.setT13(rs.getString("t13"));
			response.setT14(rs.getString("t14"));
			response.setT15(rs.getString("t15"));
			response.setT16(rs.getString("t16"));
			response.setT17(rs.getString("t17"));
			response.setT18(rs.getString("t18"));
			response.setT19(rs.getString("t19"));
			response.setT20(rs.getString("t20"));
			response.setT21(rs.getString("t21"));
			response.setT22(rs.getString("t22"));
			response.setT23(rs.getString("t23"));
			response.setT24(rs.getString("t24"));
			response.setT25(rs.getString("t25"));
			response.setT26(rs.getString("t26"));
			response.setT27(rs.getString("t27"));
			response.setT28(rs.getString("t28"));
			response.setT29(rs.getString("t29"));
			response.setT30(rs.getString("t30"));
			response.setT31(rs.getString("t31"));
			response.setT32(rs.getString("t32"));
			response.setT33(rs.getString("t33"));
			response.setT34(rs.getString("t34"));
			response.setT35(rs.getString("t35"));

			String regionNumberFromDB = rs.getString("t36");

			if (t7.equalsIgnoreCase("MANDALAY")) {
				regionNumber = "10000000";
			} else if (t7.equalsIgnoreCase("YANGON")) {
				regionNumber = "13000000";
			} else {
				regionNumber = "00000000";
			}

			if (regionNumberFromDB.contains(regionNumber)) {
				response.setT36(regionNumber);
			}

			response.setT37(rs.getString("t37"));
			response.setT38(rs.getString("t38"));
			response.setT39(rs.getString("t39"));
			response.setT40(rs.getString("t40"));
			response.setT41(rs.getString("t41"));
			response.setT42(rs.getString("t42"));
			response.setT43(rs.getString("t43"));
			response.setT44(rs.getString("t44"));
			response.setT45(rs.getString("t45"));
			response.setT46(rs.getString("t46"));
			response.setT47(rs.getString("t47"));
			response.setT48(rs.getString("t48"));
			response.setT49(rs.getString("t49"));
			response.setT50(rs.getString("t50"));
			response.setUsersyskey(rs.getLong("UserSysKey"));
			response.setT51(rs.getString("t51"));
			response.setT52(rs.getString("t52"));
			response.setT53(rs.getString("t53"));
			response.setT54(rs.getString("t54"));
			response.setT55(rs.getString("t55"));
			response.setT56(rs.getString("t56"));
			response.setT57(rs.getString("t57"));
			response.setT58(rs.getString("t58"));
			response.setT59(rs.getString("t59"));
			response.setT60(rs.getString("t60"));
			response.setT61(rs.getString("t61"));
			response.setT62(rs.getString("t62"));
			response.setT63(rs.getString("t63"));
			response.setT64(rs.getString("t64"));
			response.setT65(rs.getString("t65"));
			response.setT66(rs.getString("t66"));
			response.setT67(rs.getString("t67"));
			response.setT68(rs.getString("t68"));
			response.setT69(rs.getString("t69"));
			response.setT70(rs.getString("t70"));
			response.setT71(rs.getString("t71"));
			response.setT72(rs.getString("t72"));
			response.setT73(rs.getString("t73"));
			response.setT74(rs.getString("t74"));
			response.setT75(rs.getString("t75"));
			response.setT76(rs.getString("t76"));
			response.setT77(rs.getString("t77"));
			response.setT78(rs.getString("t78"));
			response.setT79(rs.getString("t79"));
			response.setT80(rs.getString("t80"));
		} else {
			response.setMessageCode("0014");
			response.setMessageDesc("There is no user.");
		}

		return response;
	}

	public RegisterResponseData registerUpdate(RegisterRequestData request, Connection conn) throws SQLException {
		RegisterResponseData response = new RegisterResponseData();

		int result = 0;

		String today = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

		String query = "UPDATE Register SET modifieddate=?," + "t3=?,t7=?,t8=?,t20=?,"
				+ "t21=?,t22=?,t23=?,t24=?,t25=?," + "t26=?,t27=?,t28=?,t29=?,t30=?," + "t36=?,t38=?,"
				+ "t76=?,t77=?,t78=?,t79=?,t80=? WHERE syskey=?";
		PreparedStatement ps = conn.prepareStatement(query);

		int i = 1;
		ps.setString(i++, today);
		ps.setString(i++, request.getT3());
		ps.setString(i++, request.getT7());
		ps.setString(i++, request.getT8());
		ps.setString(i++, request.getT20());
		ps.setString(i++, request.getT21());
		ps.setString(i++, request.getT22());
		ps.setString(i++, request.getT23());
		ps.setString(i++, request.getT24());
		ps.setString(i++, request.getT25());
		ps.setString(i++, request.getT26());
		ps.setString(i++, request.getT27());
		ps.setString(i++, request.getT28());
		ps.setString(i++, request.getT29());
		ps.setString(i++, request.getT30());
		ps.setString(i++, request.getT36());
		ps.setString(i++, request.getT38());
		ps.setString(i++, request.getT76());
		ps.setString(i++, request.getT77());
		ps.setString(i++, request.getT78());
		ps.setString(i++, request.getT79());
		ps.setString(i++, request.getT80());
		ps.setLong(i, Long.parseLong(request.getHautosys()));

		result = ps.executeUpdate();

		if (result > 0) {
			response.setMessageCode("0000");
			response.setMessageDesc("User updated successfully.");
		} else {
			response.setMessageCode("0014");
			response.setMessageDesc("User updates failed.");
		}

		ps.close();

		return response;
	}

	public boolean resetUser(String userid, Connection conn) throws SQLException {
		boolean result = false;
		String query = "Delete From UserRegistration WHERE Userid = ? ";
		PreparedStatement pstmt = conn.prepareStatement(query);
		int j = 1;
		pstmt.setString(j++, userid);

		int rst = pstmt.executeUpdate();
		if (rst > 0) {
			query = "Delete From  WJunction where UserID=?";
			PreparedStatement ps = conn.prepareStatement(query);
			int i = 1;
			ps.setString(i++, userid);
			rst = ps.executeUpdate();
			if (rst > 0) {
				result = true;
			} else {
				result = false;
			}
			ps.close();
		} else {
			result = false;
		}
		pstmt.close();
		return result;
	}

	public boolean saveNewDifferentUUID(WalletRequestData data, Connection pConn) throws SQLException {
		String l_Query = "UPDATE Register SET t9=? WHERE syskey=?";
		PreparedStatement pstmt = pConn.prepareStatement(l_Query);
		pstmt.setString(1, data.getField1());
		pstmt.setLong(2, Long.parseLong(data.getsKey()));

		int rst = pstmt.executeUpdate();
		pstmt.close();

		if (rst > 0) {
			return true;
		} else {
			return false;
		}
	}

	public RegisterResponseData saveProfileContact(RegisterRequestData request, Connection conn) throws SQLException {
		RegisterResponseData response = new RegisterResponseData();
		String today = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		String query = "UPDATE Contact SET modifieddate=?, Name=?, t2=? WHERE Syskey=? AND status <> 4";
		PreparedStatement ps = conn.prepareStatement(query);

		int i = 1;
		ps.setString(i++, today);
		ps.setString(i++, request.getUserName());
		ps.setString(i++, request.getT16());
		ps.setLong(i++, request.getSyskey());

		ps.executeUpdate();

		response.setMessageCode("0000");
		response.setMessageDesc("User updated successfully.");
		ps.close();

		return response;
	}

	public RegisterResponseData saveProfileRegister(RegisterRequestData request, Connection conn) throws SQLException {
		RegisterResponseData response = new RegisterResponseData();

		int result = 0;

		String today = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

		String statement = "";

		if (request.getT16().trim().length() != 0) {
			statement = ", t16=?";
		}

		String query = "UPDATE Register SET modifieddate=?, username=?,t3=?" + statement + " WHERE syskey=?";
		PreparedStatement ps = conn.prepareStatement(query);

		int i = 1;
		ps.setString(i++, today);
		ps.setString(i++, request.getUserName());
		ps.setString(i++, request.getUserName());
		if (request.getT16().trim().length() != 0) {
			ps.setString(i++, request.getT16());
		}

		ps.setLong(i, request.getSyskey());

		result = ps.executeUpdate();

		if (result > 0) {
			response.setMessageCode("0000");
			response.setMessageDesc("User updated successfully.");
		} else {
			response.setMessageCode("0014");
			response.setMessageDesc("User updates failed.");
		}

		ps.close();

		return response;
	}

	public RegisterResponseData saveProfileUserRegistration(RegisterRequestData request, Connection conn)
			throws SQLException {
		RegisterResponseData response = new RegisterResponseData();

		int result = 0;

		String query = "UPDATE UserRegistration SET name=? WHERE userid=?";// modified
																			// query
																			// for
																			// profile
																			// update
																			// by
																			// wcs
		PreparedStatement ps = conn.prepareStatement(query);

		int i = 1;
		ps.setString(i++, request.getUserName());
		ps.setString(i, request.getUserID());

		result = ps.executeUpdate();

		if (result > 0) {
			response.setMessageCode("0000");
			response.setMessageDesc("User updated successfully.");
		} else {
			response.setMessageCode("0014");
			response.setMessageDesc("User updates failed.");
		}

		ps.close();

		return response;
	}

	// added chatapiskey for chat (wcs)
	public String saveUserRegistration(String accNumber, String customerid, String userid, String sessionid,
			String phoneNo, String cusName, String nrc, int regType, String institutionCode, String chatapiskey1,
			String region, String phoneType, String deviceId, String password, String appVersion, String regionNumber,
			Connection conn) throws SQLException {

		String skey = "";
		String reskey = "";
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		SessionMgr ssmgr = new SessionMgr();
		skey = ssmgr.generateOTP();
		long key = 0L;
		/* insert function into UserRegistration table */
		key = generateKey(userid, sessionid, phoneNo, cusName, nrc, regType, conn);
		skey = String.valueOf(key) + skey;
		String query = "UPDATE UserRegistration SET SessionID=? WHERE AutoKey = ? and userid=? and regType=? and status <> 4";
		PreparedStatement pstmt = conn.prepareStatement(query);
		int j = 1;
		pstmt.setString(j++, String.valueOf(skey));
		pstmt.setString(j++, String.valueOf(key));
		pstmt.setString(j++, userid);
		pstmt.setInt(j++, regType);
		int rst = pstmt.executeUpdate();
		pstmt.close();
		if (rst > 0) {
			query = "INSERT INTO WJunction(HKey, UserID, CreatedDate, AccNumber, CusId, Status, RegType, t1, t2, n1) "
					+ " VALUES(?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement ps = conn.prepareStatement(query);
			int i = 1;
			ps.setLong(i++, key);
			ps.setString(i++, userid);
			ps.setString(i++, date);
			ps.setString(i++, accNumber);
			ps.setString(i++, customerid);
			ps.setInt(i++, 0);
			ps.setInt(i++, regType);
			ps.setString(i++, institutionCode);
			ps.setString(i++, "Current Account");
			ps.setInt(i++, 1);
			rst = ps.executeUpdate();
			ps.close();

			if (rst > 0) {
				reskey = skey;
			} else {
				reskey = "";
			}

			/* start insert function into Register table */
			RegisterRequestData request = new RegisterRequestData();
			request.setSyskey(Long.parseLong(chatapiskey1));// wcs added
															// chatapisyskey for
															// t50 of register
															// tbl
			String today = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
			request.setCreatedDate(today);
			request.setModifiedDate(today);
			request.setUserID(userid);
			request.setUserName(cusName);
			request.setRecordstatus(1);
			request.setSyncstatus(1);
			request.setSyncbatch(0);
			request.setT1(userid);
			request.setT3(cusName);
			request.setT9(deviceId);
			request.setT16("user-icon.png");
			// syskey t51 username t7 t36 t16
			// 491 40059 Thura Aung MANDALAY 10000000 762018144245734.jpg
			request.setT38("0");
			if (!region.equals("") && !regionNumber.equals("")) {
				request.setT7(region);
				request.setT36(regionNumber);
			} else {
				request.setT7("-");
				request.setT36("00000000");
			}
			request.setT65(appVersion);
			request.setUsersyskey(Long.parseLong(chatapiskey1));
			request.setT61("phone");
			request.setT64(phoneType);
			request.setT50(chatapiskey1);// wcs added chatapisyskey for t50 of
											// register tbl
			request.setT51(String.valueOf(key));// wcs added autokey of user
												// registration tbl for t51 of
												// register tbl
			request.setT41(password);

			RegisterResponseData response = registerInsert(request, conn);
			/* end insert function into Register table */

			if (response.getMessageCode().equals("0000")) {
				reskey = String.valueOf(request.getSyskey());			

			} else {
				reskey = "";
			}
		} else {
			reskey = "";
		}
		return reskey;
	}

	public UserInfoResponse selectUserInfo(Connection conn) {

		UserInfoResponse response = new UserInfoResponse();
		List<RegisterResTest> userInfoList = new ArrayList<RegisterResTest>();

		try {
			String query = "SELECT * FROM registerbyAddress;";
			PreparedStatement ps = conn.prepareStatement(query);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				RegisterResTest userInfo = new RegisterResTest();
				userInfo.setSyskey(rs.getLong("syskey"));
				userInfo.setPolicyno(rs.getString("policyno"));
				userInfo.setTypeofcover(rs.getString("typeofcover"));
				userInfo.setAddress(rs.getString("address"));
				userInfo.setStateregion(rs.getString("stateregion"));
				userInfoList.add(userInfo);
			}

			if (userInfoList.size() != 0) {
				response.setMessageCode("0000");
				response.setMessageDesc("Success");
				response.setList(userInfoList);
			} else {
				response.setMessageCode("0014");
				response.setMessageDesc("Fail");
				response.setList(null);
			}
		} catch (SQLException e) {
			response.setMessageCode("0014");
			response.setMessageDesc("Fail");
			response.setList(null);
		}

		return response;
	}

	public boolean updateDeviceIdDC(WalletRequestData data, Connection pConn) throws SQLException {
		String l_Query = "UPDATE Register SET t9=? WHERE syskey=?";
		PreparedStatement pstmt = pConn.prepareStatement(l_Query);
		pstmt.setString(1, data.getDeviceId());
		pstmt.setLong(2, Long.parseLong(data.getsKey()));

		int rst = pstmt.executeUpdate();
		pstmt.close();

		if (rst > 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean updateRegion(String oldRegion, String oldRegionNumber, String newRegion, String syskey,
			Connection pConn) throws SQLException {
		String newRegionNumber = "";

		if (newRegion.equalsIgnoreCase("MANDALAY")) {
			newRegionNumber = "10000000";
		} else if (newRegion.equalsIgnoreCase("YANGON")) {
			newRegionNumber = "13000000";
		} else {
			newRegionNumber = "00000000";
		}

		String l_Query = "update Register set t7=t7+?, t36=t36+? Where syskey=?";
		PreparedStatement pstmt = pConn.prepareStatement(l_Query);
		pstmt.setString(1, "," + newRegion);
		pstmt.setString(2, "," + newRegionNumber);
		pstmt.setLong(3, Long.parseLong(syskey));

		int rst = pstmt.executeUpdate();
		pstmt.close();

		if (rst > 0) {
			return true;
		} else {
			return false;
		}
	}

}
