package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.cms.framework.Resultb2b;
import com.nirvasoft.cms.shared.ArticleData;
import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;

public class PostDao {
	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("FMR002");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("createddate", (byte) 5));
		ret.getFields().add(new DBField("createdtime", (byte) 5));
		ret.getFields().add(new DBField("modifieddate", (byte) 5));
		ret.getFields().add(new DBField("modifiedtime", (byte) 5));
		ret.getFields().add(new DBField("userid", (byte) 5));
		ret.getFields().add(new DBField("username", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t13", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 2));
		ret.getFields().add(new DBField("n6", (byte) 2));
		ret.getFields().add(new DBField("n7", (byte) 2));
		ret.getFields().add(new DBField("n8", (byte) 2));
		ret.getFields().add(new DBField("n9", (byte) 2));
		ret.getFields().add(new DBField("n10", (byte) 2));
		ret.getFields().add(new DBField("n11", (byte) 2));
		ret.getFields().add(new DBField("n12", (byte) 2));
		ret.getFields().add(new DBField("n13", (byte) 2));
		return ret;
	}

	public static Resultb2b insert(ArticleData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		// if (obj.getT5().equals("00000000")) {
		// obj.setT5("");
		// }
		String query = "INSERT INTO FMR002 (syskey, createddate, modifieddate, " + "userid, username, recordStatus, "
				+ "syncStatus, syncBatch, usersyskey," + "t1, t2, t3, " + "t4, t5, t6, " + "t7, t8, t9, "
				+ "t10, t11, t12, " + "t13, n1, n2, " + "n3, n4, n5, " + "n6, n7, n8, " + "n9, n10, n11, "
				+ "n12, n13, createdtime," + "modifiedtime, modifieduserid, modifiedusername) " + "VALUES (?,?,?,"
				+ "?,?,?," + "?,?,?," + "?,?,?," + "?,?,?," + "?,?,?," + "?,?,?," + "?,?,?," + "?,?,?," + "?,?,?,"
				+ "?,?,?," + "?,?,?," + "?,?,?)";
		try {
			if (!isCodeExist(obj, conn)) {
				PreparedStatement ps = conn.prepareStatement(query);
				int i = 1;
				ps.setLong(i++, obj.getSyskey());
				ps.setString(i++, obj.getCreatedDate());
				ps.setString(i++, obj.getModifiedDate());
				ps.setString(i++, obj.getUserId());
				ps.setString(i++, obj.getUserName());
				ps.setInt(i++, obj.getRecordStatus());
				ps.setInt(i++, obj.getSyncStatus());
				ps.setLong(i++, obj.getSyncBatch());
				ps.setLong(i++, obj.getUserSyskey());
				ps.setString(i++, obj.getT1());
				ps.setString(i++, obj.getT2());
				ps.setString(i++, obj.getT3());
				ps.setString(i++, obj.getT4());
				ps.setString(i++, obj.getT5());
				ps.setString(i++, obj.getT6());
				ps.setString(i++, obj.getT7());
				ps.setString(i++, obj.getT8());
				ps.setString(i++, obj.getT9());
				ps.setString(i++, obj.getT10());
				ps.setString(i++, obj.getT11());
				ps.setString(i++, obj.getT12());
				ps.setString(i++, obj.getT13());
				ps.setLong(i++, obj.getN1());
				ps.setLong(i++, obj.getN2());
				ps.setLong(i++, obj.getN3());
				ps.setLong(i++, obj.getN4());
				ps.setLong(i++, obj.getN5());
				ps.setLong(i++, obj.getN6());
				ps.setLong(i++, obj.getN7());
				ps.setLong(i++, obj.getN8());
				ps.setLong(i++, obj.getN9());
				ps.setLong(i++, obj.getN10());
				ps.setLong(i++, obj.getN11());
				ps.setLong(i++, obj.getN12());
				ps.setLong(i++, obj.getN13());
				ps.setString(i++, obj.getCreatedTime());
				ps.setString(i++, obj.getModifiedTime());
				ps.setString(i++, obj.getModifiedUserId());
				ps.setString(i++, obj.getModifiedUserName());

				if (ps.executeUpdate() > 0) {
					res.setState(true);
					res.setMsgDesc("Saved Successfully!");
				} else {
					res.setMsgDesc("Article Already Exist!");
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

	public static boolean isCodeExist(ArticleData obj, Connection conn) throws SQLException {
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				" where RecordStatus<>4 AND  RecordStatus = 1 AND syskey = " + obj.getSyskey() + " ", "", conn);
		if (dbrs.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static Resultb2b update(ArticleData obj, Connection conn) throws SQLException {
		Resultb2b res = new Resultb2b();
		if (obj.getT5().equals("00000000")) {
			obj.setT5("");
		}
		String query = "UPDATE FMR002 SET [modifieddate]=?, [userid]=?, "
				+ "[username]=?, [recordstatus]=?, [syncstatus]=?, " + "[syncbatch]=?,[usersyskey]=?, [t1]=?, "
				+ "[t2]=?, [t3]=?, [t4]=?, " + "[t5]=?, [t6]=?, [t7]=?," + "[t8]=?, [t9]=?, [t10]=?, "
				+ "[t11]=?,[t12]=?, [t13]=?, " + "[n1]=?, [n2]=?, [n3]=?, " + "[n4]=?, [n5]=?, [n6]=?, "
				+ "[n7]=?, [n8]=?, [n9]=?, " + "[n10]=?, [n11]=?, [n12]=?, "
				+ "[n13]=?, [modifiedtime]=?, [modifieduserid]=?, "
				+ "[modifiedusername]=? WHERE RecordStatus<>4 AND RecordStatus = 1 AND Syskey=?;";

		try {
			if (isCodeExist(obj, conn)) {
				PreparedStatement pstmt = conn.prepareStatement(query);
				int i = 1;
				pstmt.setString(i++, obj.getModifiedDate());
				pstmt.setString(i++, obj.getUserId());
				pstmt.setString(i++, obj.getUserName());
				pstmt.setInt(i++, obj.getRecordStatus());
				pstmt.setInt(i++, obj.getSyncStatus());
				pstmt.setLong(i++, obj.getSyncBatch());
				pstmt.setLong(i++, obj.getUserSyskey());
				pstmt.setString(i++, obj.getT1());
				pstmt.setString(i++, obj.getT2());
				pstmt.setString(i++, obj.getT3());
				pstmt.setString(i++, obj.getT4());
				pstmt.setString(i++, obj.getT5());
				pstmt.setString(i++, obj.getT6());
				pstmt.setString(i++, obj.getT7());
				pstmt.setString(i++, obj.getT8());
				pstmt.setString(i++, obj.getT9());
				pstmt.setString(i++, obj.getT10());
				pstmt.setString(i++, obj.getT11());
				pstmt.setString(i++, obj.getT12());
				pstmt.setString(i++, obj.getT13());
				pstmt.setLong(i++, obj.getN1());
				pstmt.setLong(i++, obj.getN2());
				pstmt.setLong(i++, obj.getN3());
				pstmt.setLong(i++, obj.getN4());
				pstmt.setLong(i++, obj.getN5());
				pstmt.setLong(i++, obj.getN6());
				pstmt.setLong(i++, obj.getN7());
				pstmt.setLong(i++, obj.getN8());
				pstmt.setLong(i++, obj.getN9());
				pstmt.setLong(i++, obj.getN10());
				pstmt.setLong(i++, obj.getN11());
				pstmt.setLong(i++, obj.getN12());
				pstmt.setLong(i++, obj.getN13());
				pstmt.setString(i++, obj.getModifiedTime());
				pstmt.setString(i++, obj.getModifiedUserId());
				pstmt.setString(i++, obj.getModifiedUserName());
				pstmt.setLong(i++, obj.getSyskey());

				if (pstmt.executeUpdate() > 0) {
					res.setState(true);
					res.setMsgDesc("Updated Successfully!");
				} else {
					res.setMsgDesc("No Such Article to Update!");
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

}
