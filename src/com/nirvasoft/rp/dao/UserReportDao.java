package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.rp.framework.Lov3;
import com.nirvasoft.rp.framework.Ref;
import com.nirvasoft.rp.shared.UserDetailReportData;
import com.nirvasoft.rp.shared.UserDetailReportList;

public class UserReportDao {

	public void getCustomerID(String userid, String branchCode, String tempTable, Connection conn) throws SQLException {

		String cif = "", sql = "";
		ArrayList<String> datalist = new ArrayList<String>();
		String whereClause = "";
		if (!branchCode.trim().equals("")) {
			whereClause += "and CustomerID Like ? ";
		}
		sql = "select * from ucjunction where userid =? " + whereClause;
		// System.out.println("getcustomerid:::::"+sql);
		PreparedStatement stmt = conn.prepareStatement(sql);
		int j = 1;
		stmt.setString(j++, userid);
		if (!branchCode.trim().equals("")) {
			stmt.setString(j++, branchCode + "%");
		}

		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			datalist.add(res.getString("CustomerID"));
		}

		for (int i = 0; i < datalist.size(); i++) {
			if (i == datalist.size() - 1) {
				cif += "" + datalist.get(i) + "";

			} else {
				cif += "" + datalist.get(i) + "" + " , ";
			}
		}
		sql = "update " + tempTable + " set CustomerID=? where userid=?";
		// System.out.println("update tempTable :::::::"+sql);
		PreparedStatement updateStmt = conn.prepareStatement(sql);
		int i = 1;
		updateStmt.setString(i++, cif);
		updateStmt.setString(i++, userid);
		updateStmt.executeUpdate();
	}

	public UserDetailReportList getUserReportList(int currentPage, int totalCount, int pageSize, String aFromDate,
			String aToDate, String userName, String nrc, String phoneno, String usertype, String status,
			String branchCode, String userID, int click, Connection conn) throws SQLException {
		int l_startRecord = (currentPage - 1) * pageSize;
		int l_endRecord = l_startRecord + pageSize;
		String tempTable = "tmpuser" + branchCode + userID;
		UserDetailReportList res = new UserDetailReportList();

		ArrayList<UserDetailReportData> datalist = new ArrayList<UserDetailReportData>();
		if (click == 0) {
			String l_query = "If Exists (Select * From SysObjects " + "Where Id = Object_Id('" + tempTable
					+ "') And SysStat & " + "0xf = 3) " + "Drop Table " + tempTable;
			PreparedStatement pstmt;
			pstmt = conn.prepareStatement(l_query);
			pstmt.executeUpdate();

			/*
			 * String dTable = "DROP TABLE IF EXISTS " + tempTable;
			 * PreparedStatement dstmt = conn.prepareStatement(dTable);
			 * dstmt.execute();
			 */
			String aCriteria = "";
			if (!aFromDate.trim().equals("")) {
				aCriteria += " and u.createddate >= ? ";
			}
			if (!aToDate.trim().equals("")) {
				aCriteria += " and u.createddate <= ? ";
			}
			if (!userName.trim().equals("")) {
				aCriteria += " and p.t2 =? ";
			}
			if (!nrc.trim().equals("")) {
				aCriteria += " and u.t7 = ? ";
			}
			if (!phoneno.trim().equals("")) {
				aCriteria += " and u.t4 = ? ";
			}
			if (!status.trim().equalsIgnoreCase("ALL")) {
				if (status.trim().equalsIgnoreCase("Save")) {
					aCriteria += " and u.RecordStatus = '1'";// MM
				} else if (status.trim().equalsIgnoreCase("Activate")) {
					aCriteria += " and u.RecordStatus = '2' and u.n7 != 11";// MM
				} else if (status.trim().equalsIgnoreCase("Deactivate")) {
					aCriteria += " and u.RecordStatus = '21'";// MM
				} else if (status.trim().equalsIgnoreCase("Delete")) {
					aCriteria += " and u.RecordStatus = '4'";// MM
				} else if (status.trim().equalsIgnoreCase("Lock")) {
					aCriteria += " and u.n7 = 11 and u.RecordStatus <> 4";// MM
				} else {
					aCriteria += " and u.n7 = 0 ";// MM
				}
			}

			aCriteria += " and u.n2 = ? ";// BranchUser

			String l_Query = "";
			UserReportDao dao = new UserReportDao();
			l_Query = "SELECT u.t1 as UserID, p.t2 AS UserName,u.t7 as NRC,u.t4 as PhoneNo, u.createddate,u.modifieddate,u.userid as createdby,p.t5 AS ModifiedBy,u.RecordStatus,u.n7 as lockstatus,u.autokey, convert(nvarchar(200),'') as CustomerID  "
					+ "into " + tempTable + "  FROM  UVM005_A u, UVM012_A p where u.t1 = p.t1 and u.n4 = p.syskey "
					+ aCriteria;
			// System.out.println("temptable insert :::::"+l_Query);
			PreparedStatement stmt = conn.prepareStatement(l_Query);
			int i = 1;
			if (!aFromDate.trim().equals("")) {
				stmt.setString(i++, aFromDate.trim());
			}
			if (!aToDate.trim().equals("")) {
				stmt.setString(i++, aToDate.trim());
			}
			if (!userName.trim().equals("")) {
				stmt.setString(i++, userName.trim());
			}
			if (!nrc.trim().equals("")) {
				stmt.setString(i++, nrc.trim());
			}
			if (!phoneno.trim().equals("")) {
				stmt.setString(i++, phoneno.trim());
			}
			stmt.setString(i++, usertype);
			if (stmt.executeUpdate() > 0) {
				PreparedStatement tstmt = conn.prepareStatement("select * from " + tempTable);
				// System.out.println("temptable data ::::: select * from
				// "+tempTable);
				ResultSet rs = tstmt.executeQuery();
				while (rs.next()) {
					dao.getCustomerID(rs.getString("UserID"), branchCode, tempTable, conn);
				}
			}
		}
		String whereClause = "";
		if (usertype.trim().equals("1")) {
			whereClause = "and customerID<>''";
		}
		String qurey = "select * from (SELECT ROW_NUMBER() OVER (ORDER BY userID) AS RowNum, *   FROM  " + tempTable
				+ "  where 1=1 " + whereClause + ") as r  WHERE 1=1" + " and RowNum >  " + l_startRecord
				+ "  and RowNum <= " + l_endRecord;
		// System.out.println("qurey..." + qurey);
		// System.out.println("user require :::::"+qurey);
		PreparedStatement prestmt = conn.prepareStatement(qurey);
		ResultSet resultSet = prestmt.executeQuery();
		while (resultSet.next()) {
			UserDetailReportData data = new UserDetailReportData();
			String aCreatedDate = "", aModifiedDate = "";
			data.setUserID(resultSet.getString("UserID"));
			data.setUsername(resultSet.getString("UserName"));
			data.setNrc(resultSet.getString("NRC"));
			data.setPhoneno(resultSet.getString("PhoneNo"));
			data.setCif(resultSet.getString("CustomerID"));
			aCreatedDate = resultSet.getString("createddate");
			data.setCreateddate(aCreatedDate.substring(6) + "/" + aCreatedDate.substring(4, 6) + "/"
					+ aCreatedDate.substring(0, 4));
			aModifiedDate = resultSet.getString("modifieddate");
			data.setModifieddate(aModifiedDate.substring(6) + "/" + aModifiedDate.substring(4, 6) + "/"
					+ aModifiedDate.substring(0, 4));
			data.setSyskey(resultSet.getLong("Autokey"));
			data.setCreatedby(resultSet.getString("createdby"));
			data.setModifiedby(resultSet.getString("ModifiedBy"));
			data.setN7(resultSet.getInt("lockstatus"));
			data.setRecordStatus(resultSet.getInt("RecordStatus"));
			datalist.add(data);
		}

		res.setPageSize(pageSize);
		res.setCurrentPage(currentPage);
		String countSQL = "";
		countSQL = "select COUNT(*) as recCount from ( SELECT  * FROM  " + tempTable + " where 1=1 " + whereClause
				+ ") As r ";
		// System.out.println("count tempTable:::::"+ countSQL);
		PreparedStatement stat = conn.prepareStatement(countSQL);
		ResultSet result = stat.executeQuery();
		result.next();
		// System.out.println("Ending Result");
		res.setTotalCount(result.getInt("recCount"));
		UserDetailReportData[] dataarry = new UserDetailReportData[datalist.size()];
		dataarry = datalist.toArray(dataarry);
		res.setData(dataarry);
		return res;
	}

	/*
	 * public void updateTempTable(String userID, String branchCode, String
	 * tempTable, Connection conn) throws SQLException { String sql = ""; String
	 * whereClause = ""; if (!branchCode.trim().equals("")) { whereClause +=
	 * "and CustomerID Like ? "; }
	 * 
	 * sql =
	 * "SELECT   CustomerID = STUFF((SELECT ',' + CustomerID   FROM UCJunction  "
	 * + "   WHERE userid = ?  " + whereClause +
	 * " FOR XML PATH (''))  , 1, 1, '') from UCJunction  WHERE userid =?  group by userid "
	 * ; PreparedStatement stmt = conn.prepareStatement(sql); int j = 1;
	 * stmt.setString(j++, userID); if (!branchCode.trim().equals("")) {
	 * stmt.setString(j++, branchCode + "%"); } stmt.setString(j++, userID);
	 * ResultSet res = stmt.executeQuery(); while (res.next()) { sql = "update "
	 * + tempTable + " set CustomerID=? where userid=?"; PreparedStatement
	 * updateStmt = conn.prepareStatement(sql); int i = 1;
	 * updateStmt.setString(i++, res.getString("CustomerID"));
	 * updateStmt.setString(i++, userID); updateStmt.executeUpdate() ; } }
	 */

	public Lov3 getUserType(Connection conn) throws SQLException {

		Lov3 lov3 = new Lov3();
		Ref[] refarray = null;
		ArrayList<Ref> reflist = new ArrayList<Ref>();
		String sql = "";
		sql = "select code,Description from lovdetails where hkey = ( select syskey from LOVHeader where Description = 'User Type')";
		PreparedStatement stmt = conn.prepareStatement(sql);

		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			Ref ref = new Ref();
			ref.setvalue(rs.getString("code"));
			ref.setcaption(rs.getString("Description"));
			reflist.add(ref);
		}

		stmt.close();
		rs.close();

		refarray = new Ref[reflist.size()];
		for (int i = 0; i < reflist.size(); i++) {
			refarray[i] = reflist.get(i);
		}
		lov3.setMsgCode("0000");
		lov3.setMsgDesc("Select successfully.");
		lov3.setRefUserType(refarray);

		return lov3;

	}
}
