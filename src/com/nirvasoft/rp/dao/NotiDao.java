package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.nirvasoft.rp.shared.AccountActivityData;
import com.nirvasoft.rp.shared.NotiAdminResponse;
import com.nirvasoft.rp.shared.NotiData;
import com.nirvasoft.rp.shared.NotificationDataSet;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.shared.NotiListData;
import com.nirvasoft.rp.shared.NotiListResponse;

public class NotiDao {
	public long insert(NotiData req, Connection aConnection) throws SQLException {
		String sql = "INSERT INTO Notification (createddatetime, modifieddatetime, title, description, type, "
				+ "typedescription, date, time, userid, username,modifiedhistory,t3) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement stmt = aConnection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		int i = 1;
		// createddatetime, modifieddatetime, title, description, type,
		stmt.setString(i++, req.getCreateddatetime());
		stmt.setString(i++, req.getModifieddatetime());
		stmt.setString(i++, req.getTitle());
		stmt.setString(i++, req.getDescription());
		stmt.setInt(i++, Integer.parseInt(req.getType()));

		// typedescription, date, time, userid, username,
		stmt.setString(i++, req.getTypedescription());
		stmt.setString(i++, req.getDate());
		stmt.setString(i++, req.getTime());
		stmt.setString(i++, req.getUserid());
		stmt.setString(i++, req.getUsername());
		// modifiedhistory
		stmt.setString(i++, req.getModifiedhistory());
		stmt.setString(i++,req.getT1());
		

		long responseAutokey = 0;
		responseAutokey = stmt.executeUpdate();
		if (responseAutokey > 0) {
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs != null && rs.next()) {
				responseAutokey = rs.getLong(1);
			}
		}
		return responseAutokey;
	}
	public NotiAdminResponse CheckUserToken(NotiData req,Connection aConnection)throws SQLException {
		String sessionID=req.getSessionid();
		String token=req.getT1();
		String userId=req.getUserid();
		NotiAdminResponse response=new NotiAdminResponse();
		String query="SELECT * from Register where  userid=? and t10=? and t41=?" ;
		PreparedStatement pstmt = aConnection.prepareStatement(query);
		int i = 1;
		pstmt.setString(i++, userId);
		pstmt.setString(i++, token);
		pstmt.setString(i++, sessionID);
		ResultSet result = pstmt.executeQuery();
		if (result !=null) {
			response.setCode("0000");
			response.setDesc("Successfully");
		}
		return response;
		
		
	}
	public NotiAdminResponse selectUserToken(NotiData req,Connection aConnection)throws SQLException {
//		String sessionID=req.getSessionid();
		String token;
		String userId=req.getUserid();
		NotiAdminResponse response=new NotiAdminResponse();
		String query="SELECT t10 from Register where  userid=?" ;
		PreparedStatement pstmt = aConnection.prepareStatement(query);
		int i = 1;
		pstmt.setString(i++, userId);
//		pstmt.setString(i++, sessionID);
		ResultSet result = pstmt.executeQuery();
		if (result.next()) {
			token = result.getString("t10");
			if (token !=null) {
				response.setCode("0000");
				response.setDesc("Successfully");
				response.setAutokey(token);
			} else {
				response.setCode("0014");
				response.setDesc("No FCM Token");
			}
		} else {
			response.setCode("0014");
			response.setDesc("No FCM Token");
		}
		pstmt.close();
		return response;
	}
	public long update(NotiData req, Connection aConnection) throws SQLException {
		String sql = "UPDATE Notification SET modifieddatetime=?, title=?, description=?, type=?, typedescription=?, "
				+ "date=?, time=?, lastuserid=?, lastusername=?, modifiedhistory=? "
				+ "WHERE recordstatus<>4 AND sendtofcmfirebase<>'finish' AND autokey=?;";
		PreparedStatement stmt = aConnection.prepareStatement(sql);
		int i = 1;
		// modifieddatetime=?, title=?, description=?, type=?,
		// typedescription=?,
		stmt.setString(i++, req.getModifieddatetime());
		stmt.setString(i++, req.getTitle());
		stmt.setString(i++, req.getDescription());
		stmt.setInt(i++, Integer.parseInt(req.getType()));
		stmt.setString(i++, req.getTypedescription());

		// date=?, time=?, lastuserid=?, lastusername=?, modifiedhistory=?
		stmt.setString(i++, req.getDate());
		stmt.setString(i++, req.getTime());
		stmt.setString(i++, req.getLastuserid());
		stmt.setString(i++, req.getLastusername());
		stmt.setString(i++, req.getModifiedhistory());

		// autokey=?
		stmt.setLong(i++, Long.parseLong(req.getAutokey()));

		long responseInt = 0;
		responseInt = stmt.executeUpdate();
		return responseInt;
	}
	public String getLastModifiedHistory(long autokey, Connection conn) throws SQLException {
		String sql = "SELECT modifiedhistory FROM Notification WHERE recordstatus<>4 AND autokey=?;";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, autokey);
		ResultSet rs = stmt.executeQuery();
		String modifiedHistory = "";
		if (rs != null && rs.next()) {
			modifiedHistory = rs.getString("modifiedhistory");
		}
		return modifiedHistory;
	}
	public NotiData getNotiTitleDescription(String date, String time, Connection conn) throws SQLException {
		String sql = "SELECT * FROM Notification " + "WHERE recordstatus<>4 AND sendtofcmfirebase<>'finish' " + "AND date=? "
				+ "AND time=?;";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, date);
		stmt.setString(2, time);
		ResultSet rs = stmt.executeQuery();
		NotiData res = new NotiData();
		if (rs != null && rs.next()) {
			setData(rs, res);
		}
		return res;
	}
	private void setData(ResultSet rs, NotiData res) throws SQLException {
		res.setAutokey(rs.getString("autokey"));
		res.setTitle(rs.getString("title"));
		res.setDescription(rs.getString("description"));
	}
	public long updateResponse(String status, String rescode, String resbody, String autokey, Connection aConnection)
			throws SQLException {
		String sql = "UPDATE Notification SET sendtofcmfirebase=?, responsecode=?, responsebody=? WHERE recordstatus<>4 AND autokey=?;";
		PreparedStatement stmt = aConnection.prepareStatement(sql);
		int i = 1;
		stmt.setString(i++, status);
		stmt.setString(i++, rescode);
		stmt.setString(i++, resbody);
		stmt.setLong(i++, Long.parseLong(autokey));
		long responseInt = 0;
		responseInt = stmt.executeUpdate();
		return responseInt;
	}
	public long updateFCMResponse(String status, String rescode, String resbody, String autokey, Connection aConnection)
			throws SQLException {
		String sql = "UPDATE Notification SET sendtofcmfirebase=?, responsecode=?, responsebody=?,recordstatus=? WHERE recordstatus<>4 AND autokey=?;";
		PreparedStatement stmt = aConnection.prepareStatement(sql);
		int i = 1;
		stmt.setString(i++, status);
		stmt.setString(i++, rescode);
		stmt.setString(i++, resbody);
		stmt.setInt(i++, 1);
		stmt.setLong(i++, Long.parseLong(autokey));
		long responseInt = 0;
		responseInt = stmt.executeUpdate();
		return responseInt;
	}
	public NotificationDataSet getAllNoti(String searchText, int pageSize, int currentPage, Connection conn)
			throws SQLException {
		NotificationDataSet response = null;

		String query = "";
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int startPage = (currentPage - 1) * pageSize;
		int endPage = pageSize + startPage;

		try {
			if (searchText.equals("")) {
				query = query.concat("SELECT * FROM ("
						+ "SELECT ROW_NUMBER() OVER (ORDER BY AutoKey DESC) AS RowNum, autokey, createddatetime, modifieddatetime, title, description, type, typedescription, date, time, userid, username, lastuserid, lastusername, modifiedhistory, sendtofcmfirebase, responsecode, responsebody, t1, t2, t3, n1, n2, n3 "
						+ "FROM [Notification] WHERE recordstatus<>4 "
						+ ") AS RowConstrainedResult WHERE (RowNum > ? AND RowNum <= ?);");

				preparedStatement = conn.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE,
						ResultSet.CONCUR_READ_ONLY);
				preparedStatement.setInt(1, startPage);
				preparedStatement.setInt(2, endPage);
				resultSet = preparedStatement.executeQuery();
			} else {
				searchText = searchText.replace("'", "''");

				query = query.concat("SELECT * FROM ("
						+ "SELECT ROW_NUMBER() OVER (ORDER BY AutoKey DESC) AS RowNum, autokey, createddatetime, modifieddatetime, title, description, type, typedescription, date, time, userid, username, lastuserid, lastusername, modifiedhistory, sendtofcmfirebase, responsecode, responsebody, t1, t2, t3, n1, n2, n3 "
						+ "FROM [Notification] " + "WHERE recordstatus<>4 AND title LIKE ? " + "OR description LIKE ? "
						+ ") AS RowConstrainedResult WHERE (RowNum > ? AND RowNum <= ?);");
				preparedStatement = conn.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE,
						ResultSet.CONCUR_READ_ONLY);

				for (int i = 1; i < 3; i++) {
					preparedStatement.setString(i, "%" + searchText + "%");
				}

				preparedStatement.setInt(3, startPage);
				preparedStatement.setInt(4, endPage);
				resultSet = preparedStatement.executeQuery();
			}

			int totalRow = 0;
			resultSet.last();
			totalRow = resultSet.getRow();

			if (totalRow == 0) {
				response = new NotificationDataSet();
				response.setMsgCode("0000");
				response.setMsgDesc("Data Not Found.");
				response.setSearchText(searchText);
				response.setPageSize(pageSize);
				response.setCurrentPage(currentPage);
				response.setData(null);
				response.setTotalCount(0);
			}

			if (totalRow != 0) {
				resultSet.beforeFirst();
				NotiData[] dataarr = new NotiData[totalRow];
				int x = 0;

				while (resultSet.next()) {
					NotiData data = new NotiData();
					data.setAutokey(resultSet.getString("autokey"));
					data.setCreateddatetime(resultSet.getString("createddatetime"));
					data.setModifieddatetime(resultSet.getString("modifieddatetime"));
					data.setTitle(resultSet.getString("title"));
					data.setDescription(resultSet.getString("description"));
					data.setType(resultSet.getString("type"));
					data.setTypedescription(resultSet.getString("typedescription"));
					data.setDate(resultSet.getString("date"));
					data.setTime(resultSet.getString("time"));
					data.setUserid(resultSet.getString("userid"));
					data.setUsername(resultSet.getString("username"));
					data.setLastuserid(resultSet.getString("lastuserid"));
					data.setLastusername(resultSet.getString("lastusername"));
					data.setModifiedhistory(resultSet.getString("modifiedhistory"));
					data.setSendtofcmfirebase(resultSet.getString("sendtofcmfirebase"));
					data.setResponsecode(resultSet.getString("responsecode"));
					data.setResponsebody(resultSet.getString("responsebody"));
					data.setT1(resultSet.getString("t1"));
					data.setT2(resultSet.getString("t2"));
					data.setT3(resultSet.getString("t3"));
					data.setN1(resultSet.getString("n1"));
					data.setN2(resultSet.getString("n2"));
					data.setN3(resultSet.getString("n3"));
					dataarr[x++] = data;
				}

				preparedStatement.close();
				resultSet.close();
				query = "";
				preparedStatement = null;
				resultSet = null;

				if (searchText.equals("")) {
					query = query.concat("SELECT COUNT(*) AS total FROM [Notification] WHERE recordstatus<>4;");
					preparedStatement = conn.prepareStatement(query);
					resultSet = preparedStatement.executeQuery();
					resultSet.next();
				} else {
					query = query.concat("SELECT COUNT(*) AS total " + "FROM [Notification] "
							+ "WHERE recordstatus<>4 AND title LIKE ? " + "OR description LIKE ?;");
					preparedStatement = conn.prepareStatement(query);

					for (int i = 1; i < 3; i++) {
						preparedStatement.setString(i, "%" + searchText + "%");
					}

					resultSet = preparedStatement.executeQuery();
					resultSet.next();
				}

				response = new NotificationDataSet();
				response.setMsgCode("0000");
				response.setMsgDesc("Selected successfully.");
				response.setSearchText(searchText);
				response.setPageSize(pageSize);
				response.setCurrentPage(currentPage);
				response.setData(dataarr);
				response.setTotalCount(resultSet.getInt("total"));
			}
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}

			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}

		return response;
	}
	public ResponseData tokenNotiCount(String userid,Connection conn) throws SQLException{
		int count=0;
		ResponseData data=new ResponseData();
		String query = "";
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		query="Select COUNT(*) As total FROM Notification where recordstatus<>4 AND userid=? AND n1='0'";
		preparedStatement = conn.prepareStatement(query);
		preparedStatement.setString(1, userid);
		resultSet = preparedStatement.executeQuery();
		resultSet.next();
		count=resultSet.getInt("total");
			data.setCode("0000");
			data.setCount(count);
			data.setDesc("Selected successfully.");
		
		
		return data;
	}
	public ResponseData removeNotiCount(String userid,Connection conn) throws SQLException{
		int count=0;
		ResponseData data=new ResponseData();
		String query = "";
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		query="UPDATE Notification SET n1='1' where recordstatus<>4 AND userid=?";
		preparedStatement = conn.prepareStatement(query);
		preparedStatement.setString(1, userid);

		if (preparedStatement.executeUpdate() > 0) {
			data.setCode("0000");
			data.setDesc("Remove successfully.");
		} else {
			data.setCode("0014");
			data.setDesc("No Noti Count");
		}
		
		
		return data;
	}
	public NotificationDataSet getAllTokenNoti(String userid,int pageSize, int currentPage, Connection conn)
			throws SQLException {
		NotificationDataSet response = null;
		String query = "";
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int startPage =0;
		int endPage = 20;
		query="SELECT * FROM ("+ "SELECT ROW_NUMBER() OVER (ORDER BY AutoKey DESC) AS RowNum,autokey,description,userid  "
			+ "FROM [Notification] WHERE recordstatus<>4 AND  userid=?"
			+ ") AS RowConstrainedResult WHERE  (RowNum > ? AND RowNum <= ?);";
		preparedStatement = conn.prepareStatement(query);
		preparedStatement.setString(1, userid);
		preparedStatement.setInt(2, startPage);
		preparedStatement.setInt(3, endPage);
		resultSet = preparedStatement.executeQuery();
		ArrayList<NotiData> dataList = new ArrayList<NotiData>();
		int x = 0;
			while (resultSet.next()) {
				NotiData data = new NotiData();
				data.setAutokey(resultSet.getString("autokey"));
				data.setUserid(resultSet.getString("userid"));
				data.setDescription(resultSet.getString("description"));
				dataList.add(data);
			}
			NotiData[] dataarr = new NotiData[dataList.size()];
			for (int i = 0; i < dataList.size(); i++) {
				dataarr[i] = dataList.get(i);
			}
			preparedStatement.close();
			resultSet.close();
//			query = "";
//			preparedStatement = null;
//			resultSet = null;
//			query = query.concat("SELECT COUNT(*) AS total " + "FROM [Notification] "
//					+ "WHERE recordstatus<>4 AND userid= ?  ");
//			preparedStatement = conn.prepareStatement(query);
//			preparedStatement.setString(1, userid);
//			resultSet = preparedStatement.executeQuery();
//			resultSet.next();
			response = new NotificationDataSet();
			response.setMsgCode("0000");
			response.setMsgDesc("Selected successfully.");
			response.setPageSize(pageSize);
			response.setCurrentPage(currentPage);
			response.setData(dataarr);
//			response.setTotalCount(resultSet.getInt("total"));
		
		return response;
	}
	public NotiData delete(long autoKey, Connection conn) throws SQLException {
		NotiData response = new NotiData();

		PreparedStatement preparedStatement = null;

		try {
			String query = "UPDATE Notification SET recordstatus=4 WHERE autokey=?;";
			preparedStatement = conn.prepareStatement(query);
			preparedStatement.setLong(1, autoKey);

			if (preparedStatement.executeUpdate() > 0) {
				response.setCode("0000");
				response.setDesc("Deleted successfully.");
			} else {
				response.setCode("0014");
				response.setDesc("No Noti Count");
			}
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}

		return response;
	}



	public NotiListResponse getNotiForApp(Connection conn) throws SQLException {
		NotiListResponse response = null;
		String query = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		query = "SELECT date, time, title, description FROM Notification WHERE recordstatus<>4 AND sendtofcmfirebase='sent' ORDER BY autokey DESC;";
		pst = conn.prepareStatement(query, rs.TYPE_SCROLL_INSENSITIVE, rs.CONCUR_READ_ONLY);
		rs = pst.executeQuery();

		int totalRow = 0;
		rs.last();
		totalRow = rs.getRow();

		if (totalRow == 0) {
			response = new NotiListResponse();
			response.setCode("0000");
			response.setDesc("No result found.");
			response.setData(null);
		}

		if (totalRow != 0) {
			rs.beforeFirst();
			NotiListData[] dataarr = new NotiListData[totalRow];
			int x = 0;
			while (rs.next()) {
				NotiListData pro = new NotiListData();
				pro.setDate(rs.getString("date"));
				pro.setTime(rs.getString("time"));
				pro.setTitle(rs.getString("title"));
				pro.setDescription(rs.getString("description"));
				dataarr[x++] = pro;
			}
			pst.close();
			conn.close();
			response = new NotiListResponse();
			response.setCode("0000");
			response.setDesc("Selected successfully.");
			response.setData(dataarr);
		}

		return response;
	}

}
