package com.nirvasoft.rp.dao;

import com.nirvasoft.cms.framework.Result;
import com.nirvasoft.rp.data.FeatureData;
import com.nirvasoft.rp.data.MerchantFeatureData;
import com.nirvasoft.rp.shared.CMSMerchantAccJunctionArr;
import com.nirvasoft.rp.shared.CMSMerchantAccRefData;
import com.nirvasoft.rp.shared.CMSMerchantData;
import com.nirvasoft.rp.shared.CMSMerchantDataSet;
import com.nirvasoft.rp.users.data.MerchantData;
import com.nirvasoft.rp.util.GeneralUtil;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class MerchantDao
{
  public static String leadZero(int code, int count)
  {
    String l_result = "";
    String l_code = String.valueOf(code);
    if (l_code.trim().length() < count) {
      for (int i = l_code.trim().length(); i < count; i++) {
        l_result = l_result + "0";
      }
    }
    l_result = l_result + code;
    return l_result;
  }

  public CMSMerchantData getMerchantAccount(Connection conn, String merchantType) throws Exception {
	  CMSMerchantData ret =new CMSMerchantData();
    String sql = "SELECT AccountNumber,UserName from MerchantFeature A inner join CMSMerchant B on A.MerchantID=B.UserId where merchantID=? ";
    PreparedStatement pstmt = conn.prepareStatement(sql);
    pstmt.setString(1, merchantType);
    ResultSet rs = pstmt.executeQuery();
    while (rs.next()) {
      ret.setAccountNumber(rs.getString("AccountNumber"));
      ret.setBranchname(rs.getString("UserName"));
      
    }
    pstmt.close();
    rs.close();

    return ret;
  }

  public boolean deletebyMerchantID(String aUserID, Connection l_Conn) throws SQLException
  {
    boolean ret = false;
    String l_Query1 = "delete from CMSMerchantAccRef where T1=?  ";
    PreparedStatement pstmt = l_Conn.prepareStatement(l_Query1);
    pstmt.setString(1, aUserID);

    if (pstmt.executeUpdate() > 0)
      ret = true;
    else {
      ret = false;
    }
    return ret;
  }

  public Result deleteCMSMerchantData(CMSMerchantData data, Connection l_Conn) throws SQLException {
    String userid = data.getUserId();
    Result ret = new Result();
    MerchantDao dao = new MerchantDao();
    boolean response = dao.canDelete(data.getUserId(), l_Conn);
    if (response) {
      String query = "UPDATE CMSMerchant SET RecordStatus = 4 Where userId = ? ";
      PreparedStatement psmt = l_Conn.prepareStatement(query);
      psmt.setString(1, data.getUserId());

      if (psmt.executeUpdate() > 0) {
        String delHeadersql = "";
        delHeadersql = "DELETE FROM MerchantFeature WHERE MerchantID =? ";
        PreparedStatement stmt4 = l_Conn.prepareStatement(delHeadersql);
        stmt4.setString(1, userid);
        int rest = stmt4.executeUpdate();

        ret.setState(true);
        ret.setMsgDesc("Deleted successfully.");
      }
      else {
        ret.setState(false);
        ret.setMsgDesc("Deleted Fail.");
      }
      psmt.close();
    }
    else {
      ret.setUserId(data.getUserId());
      ret.setState(false);
      ret.setMsgDesc("Cannot delete!");
    }

    return ret;
  }

  public boolean canDelete(String mID, Connection conn) throws SQLException {
    boolean ret = false;
    String l_Query = " Select Count(*) Count From MCommRateMapping Where RecordStatus <> 4 And MerchantID = ? ";
    int count = 0;
    PreparedStatement pstmt = conn.prepareStatement(l_Query);
    pstmt.setString(1, mID);
    ResultSet rs = pstmt.executeQuery();
    while (rs.next()) {
      count = Integer.parseInt(rs.getString("Count"));
    }

    if (count == 0) {
      ret = true;
    }
    return ret;
  }

  public ArrayList<CMSMerchantData> getAllMerchant(Connection conn)
    throws SQLException
  {
    ArrayList datalist = new ArrayList();

    String sql = "select UserId,UserName,T3 from CMSMerchant Where recordStatus <> 4 order by UserName ";

    PreparedStatement stmt = conn.prepareStatement(sql);
    ResultSet res = stmt.executeQuery();
    while (res.next()) {
      CMSMerchantData combo = new CMSMerchantData();
      combo.setUserId(res.getString("UserId"));
      combo.setUserName(res.getString("UserName"));
      datalist.add(combo);
    }
    return datalist;
  }

  public CMSMerchantDataSet getAllMerchantData(String searchtext, int pageSize, int currentPage, Connection conn) throws SQLException
  {
    String searchText = searchtext.replace("'", "''");
    ArrayList datalist = new ArrayList();
    CMSMerchantDataSet ret = new CMSMerchantDataSet();
    int startPage = (currentPage - 1) * pageSize;
    int endPage = pageSize + startPage;
    String whereClause = "";
    if (searchText.equals(""))
      whereClause = "where RecordStatus<>4";
    else {
      whereClause = " where RecordStatus<>4 and (t3 like ? or t4 like ? or t5 like ? or t6 like ? Or  t11 like ? or t12 like ? Or username like ? or userid like ?)";
    }

    String l_Query = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY p.[Userid],p.[UserName]) AS RowNum,  p.[syskey],p.[Userid], p.[UserName], p.[T3], p.[T4], p.[T5], p.[T6],p.[T10], p.[T11],p.[T12] FROM [CMSMerchant] AS p " + 
      whereClause + " ) AS RowConstrainedResult " + " WHERE (RowNum > " + 
      startPage + " AND RowNum <= " + endPage + ")";

    PreparedStatement ps = conn.prepareStatement(l_Query);
    if (!searchText.equals(""))
    {
      for (int i = 1; i < 9; i++) {
        ps.setString(i, "%" + searchText + "%");
      }
    }

    ResultSet rs = ps.executeQuery();
    while (rs.next()) {
      CMSMerchantData data = new CMSMerchantData();
      data.setSysKey(rs.getLong("syskey"));
      data.setUserId(rs.getString("Userid"));
      data.setUserName(rs.getString("UserName"));
      data.setT3(rs.getString("T3"));
      data.setT4(rs.getString("T4"));
      data.setT5(rs.getString("T5"));
      data.setT6(rs.getString("T6"));
      data.setT10(rs.getString("T10"));
      data.setT11(rs.getString("T11"));
      data.setT12(rs.getString("T12"));
      datalist.add(data);
    }

    CMSMerchantData[] dataarr = new CMSMerchantData[datalist.size()];
    for (int i = 0; i < datalist.size(); i++) {
      dataarr[i] = ((CMSMerchantData)datalist.get(i));
    }

    ret.setData(dataarr);
    ret.setCurrentPage(currentPage);
    ret.setPageSize(pageSize);
    ret.setSearchText(searchText);

    PreparedStatement pstm = conn.prepareStatement("Select COUNT(*) As total FROM CMSMerchant " + whereClause);
    if (!searchText.equals(""))
    {
      for (int i = 1; i < 9; i++) {
        pstm.setString(i, "%" + searchText + "%");
      }
    }
    ResultSet result = pstm.executeQuery();
    result.next();
    ret.setTotalCount(result.getInt("total"));

    return ret;
  }

  public MerchantData getAutoGenCode(Connection con) {
    MerchantData userdata = new MerchantData();
    String id = "";
    String sql = "SELECT CODE,Serial,n1,n2 FROM DistributorSetting WHERE ID = (SELECT MAX(ID) FROM DistributorSetting)";

    String codestr = "";
    String serial = "0000";
    String newSerial = "";
    int count = 0;
    int code = 0;
    try {
      Statement stmt = con.createStatement();
      ResultSet res = stmt.executeQuery(sql);
      while (res.next()) {
        codestr = res.getString("CODE");
        serial = res.getString("Serial");
        count = res.getInt("n1");
      }

      code = Integer.parseInt(serial) + 1;
      newSerial = leadZero(code, count);
      id = codestr + newSerial;
      userdata.setMerchantcode(id);
      userdata.setCode(String.valueOf(code));
      String query = "INSERT INTO DistributorSetting ( CODE ,Serial ,UserID,t1,t2,t3,t4,t5,n1,n2,n3,n4,n5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
      PreparedStatement psmt = con.prepareStatement(query);
      int j = 1;
      psmt.setString(j++, codestr);
      psmt.setString(j++, newSerial);
      psmt.setString(j++, id);
      psmt.setString(j++, "");
      psmt.setString(j++, "");
      psmt.setString(j++, "");
      psmt.setString(j++, "");
      psmt.setString(j++, "");
      psmt.setInt(j++, count);
      psmt.setInt(j++, 9999);
      psmt.setInt(j++, 0);
      psmt.setInt(j++, 0);
      psmt.setInt(j++, 0);
      if (psmt.executeUpdate() > 0) {
        System.out.println("update successfully");
      }
      else
        System.out.println("update fail");
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return userdata;
  }

  public CMSMerchantData getCMSMerchantDataByID(String acode, Connection l_Conn) throws SQLException
  {
    CMSMerchantData ret = new CMSMerchantData();
    String l_Query = "SELECT * FROM CMSMerchant WHERE UserId = ?";
    PreparedStatement pstmt = l_Conn.prepareStatement(l_Query);
    pstmt.setString(1, acode);
    ResultSet rs = pstmt.executeQuery();

    while (rs.next()) {
      ret.setUserId(rs.getString("UserId"));
      ret.setUserName(rs.getString("UserName"));
      ret.setT3(rs.getString("T3"));
      ret.setT4(rs.getString("T4"));
      ret.setT5(rs.getString("T5"));
      ret.setT6(rs.getString("T6"));
      ret.setT11(rs.getString("T11"));
      ret.setT12(rs.getString("T12"));
    }
    return ret;
  }

  public CMSMerchantAccJunctionArr getDataByMerchantID(String id, Connection conn)
  {
    CMSMerchantAccJunctionArr ret = new CMSMerchantAccJunctionArr();
    ArrayList dataList = new ArrayList();
    String l_Query = "SELECT t1,accountnumber,t2, t4 , branchCode FROM CMSMerchantAccRef WHERE t1=?";
    System.out.println("t1 is: " + id);
    try
    {
      PreparedStatement pstmt = conn.prepareStatement(l_Query);
      pstmt.setString(1, id);
      ResultSet rs = pstmt.executeQuery();

      while (rs.next()) {
        CMSMerchantAccRefData data = new CMSMerchantAccRefData();
        data.setT1(rs.getString("t1"));
        data.setAccountNumber(rs.getString("accountnumber"));
        data.setT2(rs.getString("t2"));
        data.setT4(rs.getString("t4"));
        data.setBranchCode(rs.getString("branchCode"));
        dataList.add(data);
      }
      CMSMerchantAccRefData[] res = new CMSMerchantAccRefData[dataList.size()];
      for (int i = 0; i < dataList.size(); i++) {
        res[i] = ((CMSMerchantAccRefData)dataList.get(i));
      }
      ret.setData(res);
    }
    catch (SQLException e)
    {
      e.printStackTrace();
    }

    return ret;
  }

  public CMSMerchantAccRefData getMerchantAcctNoByMID(String merchantId, Connection l_Conn) throws SQLException
  {
    CMSMerchantAccRefData ret = new CMSMerchantAccRefData();
    String l_Query = "Select AccountNumber,BranchCode from CMSMerchantAccRef Where T1 =?";
    PreparedStatement pstmt = l_Conn.prepareStatement(l_Query);
    pstmt.setString(1, merchantId);
    ResultSet rs = pstmt.executeQuery();

    if (rs.next()) {
      ret.setAccountNumber(rs.getString("AccountNumber"));
      ret.setBranchCode(rs.getString("BranchCode"));
    }

    return ret;
  }

  public CMSMerchantData getMerchantDataByID(String merchantId, Connection l_Conn) throws SQLException
  {
    CMSMerchantData ret = new CMSMerchantData();
    String l_Query = "";
    l_Query = "SELECT UserId,UserName,T3,T4,T5,T6,P.AccountNumber,t8,t9,t10,t12 "
    		+ "FROM CMSMerchant M  inner join MerchantFeature P on M.UserId=P.MerchantID "
    		+ "WHERE M.recordStatus <> 4 And M.UserId = ?  ";

    PreparedStatement pstmt = l_Conn.prepareStatement(l_Query);
    pstmt.setString(1, merchantId);
    System.out.println("M Q : " + l_Query + " ID : " + merchantId);
    ResultSet rs = pstmt.executeQuery();

    while (rs.next()) {
      //ret = getFeatureDataByMerchantID(merchantId, l_Conn);
      ret.setUserId(rs.getString("UserId"));
      ret.setUserName(rs.getString("UserName"));
      ret.setT3(rs.getString("T3"));
      ret.setT4(rs.getString("T4"));
      ret.setT5(rs.getString("T5"));
      ret.setT6(rs.getString("t6"));
      ret.setAccountNumber(rs.getString("AccountNumber"));
      ret.setFeature(rs.getString("t8"));
      ret.setBranchCode(rs.getString("t9"));
      ret.setT10(rs.getString("t10"));
      ret.setT12(rs.getString("t12"));
    }

    return ret;
  }

  /*public CMSMerchantData getFeatureDataByMerchantID(String merchantId, Connection l_Conn) throws SQLException {
    CMSMerchantData ret = new CMSMerchantData();

    String l_Query = "";
    l_Query = "SELECT Feature , AccountNumber ,branchCode,N1 FROM MerchantFeature Where MerchantID = ?";
    PreparedStatement pstmt = l_Conn.prepareStatement(l_Query);
    pstmt.setString(1, merchantId);
    ResultSet rs = pstmt.executeQuery();
    ArrayList datalist = new ArrayList();

    while (rs.next()) {
      FeatureData feature = new FeatureData();
      feature.setAccountNumber(rs.getString("AccountNumber"));
      feature.setBranchCode(rs.getString("branchCode"));
      feature.setFeature(rs.getString("Feature"));
      feature.setFeaturevalue(rs.getInt("Feature"));
      feature.setOrder(rs.getString("N1"));
      datalist.add(feature);
    }

    FeatureData[] arr = new FeatureData[datalist.size()];
    for (int i = 0; i < datalist.size(); i++) {
      arr[i] = ((FeatureData)datalist.get(i));
      ret.setData(arr);
    }

    return ret;
  }*/

  public CMSMerchantAccJunctionArr getMerchantMappingList(String searchText, int pageSize, int currentPage, Connection conn) throws SQLException {
    CMSMerchantAccJunctionArr ret = new CMSMerchantAccJunctionArr();
    ArrayList datalist = new ArrayList();
    int startPage = (currentPage - 1) * pageSize;
    int endPage = pageSize + startPage;

    String whereClause = "";
    if ((searchText.equals("")) || (searchText.isEmpty()))
      whereClause = " where a.RecordStatus<>4 ";
    else {
      whereClause = " where a.RecordStatus<>4 and a.t1 like ? or a.AccountNumber like ? or a.t3 like ? or b.UserName like ? ";
    }

    String query = "Select * from ( Select Row_Number() Over (Order By a.T1,a.AccountNumber,b.UserName) As RowNum, a.AccountNumber,a.branchCode,a.branchname, a.T1,a.T2,a.T3,a.T4,b.UserName from CMSMerchantAccRef a Join CMSMerchant b ON a.T1=b.UserId " + 
      whereClause + " ) " + "As RowConstrainedResult Where (RowNum > " + startPage + " And RowNum <= " + 
      endPage + ")";

    PreparedStatement ps = conn.prepareStatement(query);
    if (!searchText.equals(""))
    {
      for (int i = 1; i < 5; i++) {
        ps.setString(i, "%" + searchText + "%");
      }
    }
    ResultSet rs = ps.executeQuery();
    while (rs.next()) {
      CMSMerchantAccRefData data = new CMSMerchantAccRefData();
      data.setAccountNumber(rs.getString("AccountNumber"));
      data.setBranchCode(rs.getString("branchCode"));
      data.setBranchname(rs.getString("branchname"));
      data.setT1(rs.getString("T1"));
      data.setT2(rs.getString("T2"));
      data.setT3(rs.getString("T3"));
      data.setT4(rs.getString("T4"));
      data.setUserName(rs.getString("UserName"));
      datalist.add(data);
    }

    CMSMerchantAccRefData[] dataarr = new CMSMerchantAccRefData[datalist.size()];
    for (int i = 0; i < datalist.size(); i++) {
      dataarr[i] = ((CMSMerchantAccRefData)datalist.get(i));
    }

    ret.setData(dataarr);
    ret.setCurrentPage(currentPage);
    ret.setPageSize(pageSize);
    ret.setSearchText(searchText);

    PreparedStatement pstm = conn.prepareStatement(
      "Select COUNT(*) As total FROM CMSMerchantAccRef a inner join CMSMerchant b ON a.T1=b.UserId " + 
      whereClause);
    if (!searchText.equals(""))
    {
      for (int i = 1; i < 5; i++) {
        pstm.setString(i, "%" + searchText + "%");
      }
    }
    ResultSet result = pstm.executeQuery();
    result.next();
    ret.setTotalCount(result.getInt("total"));

    return ret;
  }

  public boolean isCodeExist(CMSMerchantData obj, Connection conn) throws SQLException {
    ArrayList dataList = new ArrayList();
    String l_Query = " SELECT * FROM CMSMerchant WHERE UserId = ? ";

    PreparedStatement pstmt = conn.prepareStatement(l_Query);
    pstmt.setString(1, obj.getUserId());
    ResultSet rs = pstmt.executeQuery();
    while (rs.next()) {
      CMSMerchantData data = new CMSMerchantData();
      data.setUserId(rs.getString("UserId"));
      dataList.add(data);
    }

    return dataList.size() > 0;
  }

  public Result saveCMSMerchantData(CMSMerchantData data, Connection aConn)
    throws SQLException
  {
    Result ret = new Result();
    String userId = "";
    String code = "";
    MerchantData userdata = new MerchantData();
    userdata = getAutoGenCode(aConn);
    userId = userdata.getMerchantcode();
    code = userdata.getCode();
    String query = "INSERT INTO CMSMerchant ( UserName ,UserId ,CreatedDate ,ModifiedDate ,T1 ,T3 ,T4 ,T5 ,T6,t7 ,T8 ,T9 ,T10, T11, T12, recordStatus) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    PreparedStatement psmt = aConn.prepareStatement(query);

    int i = 1;
    psmt.setString(i++, data.getUserName());
    psmt.setString(i++, userId);
    psmt.setString(i++, data.getCreatedDate());
    psmt.setString(i++, data.getModifiedDate());
    psmt.setString(i++, data.getT1());
    psmt.setString(i++, data.getT3());
    psmt.setString(i++, data.getT4());
    psmt.setString(i++, data.getT5());
    psmt.setString(i++, code);
    psmt.setString(i++, data.getAccountNumber());
    psmt.setString(i++, data.getFeature());
    psmt.setString(i++, data.getBranchCode());
    psmt.setString(i++, data.getT10());
    psmt.setString(i++, data.getT11());
    psmt.setString(i++, data.getChk());
    psmt.setInt(i++, 0);
    if (psmt.executeUpdate() > 0) {
      ret = saveMerchantFeature(aConn, userId, data);
    } else {
      ret.setMsgCode("0014");
      ret.setState(false);
    }

    return ret;
  }

  public String saveMerchantAccountJunction(CMSMerchantAccJunctionArr aData, String merchantid, Connection l_Conn)
    throws SQLException
  {
    String isupdate = "";

    String l_Query1 = "delete from CMSMerchantAccRef where T1=?  ";

    PreparedStatement pstmt = l_Conn.prepareStatement(l_Query1);

    pstmt.setString(1, merchantid);

    int rs = pstmt.executeUpdate();

    if (rs > 0)
    {
      isupdate = "1";
    }
    else
    {
      isupdate = "0";
    }

    String l_Query2 = "INSERT INTO CMSMerchantAccRef (CreatedDate ,ModifiedDate ,CreatedUserID , AccountNumber ,branchname ,branchCode ,T1 ,T2 ,T3 ,recordStatus) VALUES( ?,?,?,?,?,?,?,?,?,?)";

    PreparedStatement ps2 = l_Conn.prepareStatement(l_Query2);

    CMSMerchantAccRefData[] ucArr = aData.getData();

    for (CMSMerchantAccRefData data : ucArr)
    {
      updateData(ps2, data, merchantid, l_Conn);

      ps2.addBatch();
    }

    int[] status = ps2.executeBatch();

    for (int i : status)
    {
      if (i > 0)
      {
        isupdate = isupdate + "1";

        break;
      }

      isupdate = isupdate + "0";
    }

    System.out.println("str is: " + isupdate);

    return isupdate;
  }

  public Result updateMerchant(CMSMerchantData data, Connection l_Conn)
    throws SQLException
  {
    Result ret = new Result();

    String query = "UPDATE CMSMerchant SET ModifiedDate =?, UserName=?, T3=?, T4=?, T5=?, T6=?, T7=?, T8=?, T9=? ,t10=?,t11=?,t12=? WHERE UserId=?";

    PreparedStatement pstmt = l_Conn.prepareStatement(query);
    int i = 1;
    pstmt.setString(i++, data.getModifiedDate());
    pstmt.setString(i++, data.getUserName());
    pstmt.setString(i++, data.getT3());
    pstmt.setString(i++, data.getT4());
    pstmt.setString(i++, data.getT5());
    pstmt.setString(i++, data.getT6());
    pstmt.setString(i++, data.getAccountNumber());
    pstmt.setString(i++, data.getFeature());
    pstmt.setString(i++, data.getBranchCode());
    pstmt.setString(i++, data.getT10());
    pstmt.setString(i++, data.getT11());
    pstmt.setString(i++, data.getChk());
    pstmt.setString(i++, data.getUserId());
    if (pstmt.executeUpdate() > 0) {
      ret = saveMerchantFeature(l_Conn, data.getUserId(), data);
    } else {
      ret.setMsgDesc("Updated fail");
      ret.setState(false);
      ret.setMsgCode("0014");
    }
    ret.setUserId(data.getUserId());
    pstmt.close();

    return ret;
  }

  public Result saveMerchantFeature(Connection aConn, String merchantID, CMSMerchantData data) throws SQLException {
    String isupdate = "";
    Result ret = new Result();
    String l_Query = "INSERT INTO MerchantFeature (MerchantID ,Feature , AccountNumber ,branchCode, N1) VALUES (?,?,?,?,?)";
    PreparedStatement ps = aConn.prepareStatement(l_Query);
    int i = 1;
    ps.setString(i++, merchantID);
    ps.setString(i++, data.getFeature());
    ps.setString(i++, data.getAccountNumber());
    ps.setString(i++, data.getBranchCode());
    ps.setString(i++, data.getOrder());
      if (ps.executeUpdate() > 0) {
        ret.setMsgCode("0000");
        ret.setState(true);
        ret.setUserId(data.getUserId());
        ret.setKeyst(isupdate);
      }else{
    	  ret.setMsgCode("0014");
          ret.setState(false);
          ret.setUserId(data.getUserId());
          ret.setKeyst(isupdate);
      }
     
   // }

    return ret;
  }
  
  public Result updateMerchantFeature(Connection aConn, String merchantID, CMSMerchantData data) throws SQLException {
	    String isupdate = "";
	    Result ret = new Result();
	    String l_Query = "Update MerchantFeature Set Feature=? , AccountNumber=? ,branchCode=? , N1=? Where MerchantID=?";
	    PreparedStatement ps = aConn.prepareStatement(l_Query);
	    int i = 1;
	    ps.setString(i++, data.getFeature());
	    ps.setString(i++, data.getAccountNumber());
	    ps.setString(i++, data.getBranchCode());
	    ps.setString(i++, data.getOrder());
	    ps.setString(i++, merchantID);
	      if (ps.executeUpdate() > 0) {
	        ret.setMsgCode("0000");
	        ret.setState(true);
	        ret.setUserId(data.getUserId());
	        ret.setKeyst(isupdate);
	      }else{
	    	  ret.setMsgCode("0014");
	          ret.setState(false);
	          ret.setUserId(data.getUserId());
	          ret.setKeyst(isupdate);
	      }

	    return ret;
	  }

  private void updateData(PreparedStatement ps, CMSMerchantAccRefData data, String aMerchantId, Connection l_Conn)
    throws SQLException
  {
    String brname = "";
    String s_query = "select BranchName from Branch where BranchCode = ?";
    PreparedStatement psmt = l_Conn.prepareStatement(s_query);
    psmt.setString(1, data.getBranchCode());
    ResultSet rst = psmt.executeQuery();
    while (rst.next()) {
      brname = rst.getString("BranchName");
    }

    int i = 1;
    ps.setString(i++, GeneralUtil.datetoString());
    ps.setString(i++, GeneralUtil.datetoString());
    ps.setString(i++, data.getCreatedUserID());
    ps.setString(i++, data.getAccountNumber());
    ps.setString(i++, brname);
    ps.setString(i++, data.getBranchCode());
    ps.setString(i++, data.getT1());
    ps.setString(i++, data.getT2());
    ps.setString(i++, data.getT3());
    ps.setInt(i++, data.getRecordstatus());
  }

  public boolean updatePayTemplateData(String aMerchantId, String aMerchantName, Connection l_Conn) throws SQLException
  {
    boolean ret = false;
    String query = "Update PayTemplateHeader Set MerchantName = ? Where MerchantID = ? ";
    PreparedStatement pstmt = l_Conn.prepareStatement(query);
    pstmt.setString(1, aMerchantName);
    pstmt.setString(2, aMerchantId);
    if (pstmt.executeUpdate() > 0)
      ret = true;
    else {
      ret = false;
    }
    pstmt.close();
    return ret;
  }

  public MerchantFeatureData getMerchantAcc(String merchantID, Connection l_Conn)
    throws SQLException
  {
    MerchantFeatureData res = new MerchantFeatureData();
    String s_query = "select * from MerchantFeature where MerchantID = ? order by autokey desc";
    PreparedStatement psmt = l_Conn.prepareStatement(s_query);
    psmt.setString(1, merchantID);
    ResultSet rst = psmt.executeQuery();
    if (rst.next()) {
      res.setAutoKey(rst.getString("AutoKey"));
      res.setMerchantID(rst.getString("MerchantID"));
      res.setFeature(rst.getString("Feature"));
      res.setAccountNumber(rst.getString("AccountNumber"));
      res.setBranchCode(rst.getString("BranchCode"));
      res.setT1(rst.getString("t1"));
      res.setT2(rst.getString("t2"));
      res.setN1(String.valueOf(rst.getInt("n1")));
      res.setN2(String.valueOf(rst.getInt("n2")));
    }
    return res;
  }
  public MerchantFeatureData getMerchantAccV2(String merchantID, Connection l_Conn) throws SQLException {
    MerchantFeatureData res = new MerchantFeatureData();
    String s_query = "select t7 from CMSMerchant where userID = ?";
    PreparedStatement psmt = l_Conn.prepareStatement(s_query);
    psmt.setString(1, merchantID);
    ResultSet rst = psmt.executeQuery();
    if (rst.next()) {
      res.setAccountNumber(rst.getString("t7"));
    }
    return res;
  }
  public Result updateCMSMerchantData(CMSMerchantData data, Connection l_Conn) throws SQLException {
	    Result ret = new Result();

	    String query = "UPDATE CMSMerchant SET ModifiedDate =?, UserName=?, T3=?, T4=?, T5=?, T6=?,T7=?, T11=?, T12=?  WHERE UserId=?";

	    PreparedStatement pstmt = l_Conn.prepareStatement(query);
	    int i = 1;
	    pstmt.setString(i++, data.getModifiedDate());
	    pstmt.setString(i++, data.getUserName());
	    pstmt.setString(i++, data.getT3());
	    pstmt.setString(i++, data.getT4());
	    pstmt.setString(i++, data.getT5());
	    pstmt.setString(i++, data.getT6());
	    pstmt.setString(i++, data.getAccountNumber());
	    pstmt.setString(i++, data.getT11());
	    pstmt.setString(i++, data.getChk());
	    pstmt.setString(i++, data.getUserId());
	    if (pstmt.executeUpdate() > 0) {
	      ret = updateMerchantFeature(l_Conn, data.getUserId(), data);
	    } else {
	      ret.setMsgDesc("Updated fail");
	      ret.setState(false);
	      ret.setMsgCode("0014");
	    }
	    ret.setUserId(data.getUserId());
	    pstmt.close();

	    return ret;
	  }
}