package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.nirvasoft.rp.framework.LOVDetails;
import com.nirvasoft.rp.shared.AccountTypeListResponseData;

public class LOVDetailsDao {

	public AccountTypeListResponseData getAccountTypeList(Connection conn) throws SQLException {
		AccountTypeListResponseData responseData = new AccountTypeListResponseData();

		String sql = "SELECT Code, Description FROM LOVDetails WHERE HKey = (SELECT SysKey FROM LOVHeader WHERE Description = 'Account Type') ORDER BY SysKey;";

		PreparedStatement preparedStatement = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE,
				ResultSet.CONCUR_READ_ONLY);

		ResultSet resultSet = preparedStatement.executeQuery();

		int totalRow = 0;
		resultSet.last();
		totalRow = resultSet.getRow();

		if (totalRow > 0) {
			resultSet.beforeFirst();

			LOVDetails[] lovDetailsList = new LOVDetails[totalRow];
			int i = 0;

			while (resultSet.next()) {
				LOVDetails lovDetails = new LOVDetails();
				lovDetails.setValue(resultSet.getString("Code"));
				lovDetails.setCaption(resultSet.getString("Description"));
				lovDetailsList[i++] = lovDetails;
			}

			responseData.setCode("0000");
			responseData.setDesc("Select Successfully.");
			responseData.setAccountTypeList(lovDetailsList);
		} else {
			responseData.setCode("0014");
			responseData.setDesc("There is no data!");
		}

		resultSet.close();
		preparedStatement.close();

		return responseData;
	}

}
