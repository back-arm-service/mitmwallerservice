package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.nirvasoft.rp.shared.ImageData;
import com.nirvasoft.rp.shared.ImageRequest;
import com.nirvasoft.rp.shared.ImageResponse;

public class ImageDao {

	public List<ImageData> getAllImage(ImageRequest req, Connection conn) throws SQLException {
		List<ImageData> imageList = new ArrayList<ImageData>();

		String sql = "SELECT * FROM ImageLocation WHERE userid = ?;";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, req.getUserID());
		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			ImageData doc = new ImageData();
			doc.setSyskey("syskey");
			doc.setCreatedDate("createdDate");
			doc.setUserid("userid");
			doc.setImageName(rs.getString("imageName"));
			doc.setLatitude("latitude");
			doc.setLongitude("longitude");
			doc.setLocation("location");
			doc.setImagePath("imagePath");
			doc.setT1("t1");
			doc.setT2("t2");
			doc.setT3("t3");
			doc.setT4("t4");
			doc.setT5("t5");
			doc.setN1("n1");
			doc.setN2("n2");
			doc.setN3("n3");
			doc.setN4("n4");
			doc.setN5("n5");
			imageList.add(doc);
		}

		rs.close();
		stmt.close();

		return imageList;
	}

	public ImageResponse insertImage(ImageData imageData, Connection mConn) throws SQLException {
		ImageResponse res = new ImageResponse();
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		String l_Query = "Insert Into ImageLocation ([createdDate],[userid],[imageName],[latitude],[longitude]"
				+ ",[location],[imagePath]) Values (?,?,?,?,?,?,?)";
		PreparedStatement ps = mConn.prepareStatement(l_Query, Statement.RETURN_GENERATED_KEYS);
		int i = 1;
		ps.setString(i++, date);
		ps.setString(i++, imageData.getUserid());
		ps.setString(i++, imageData.getImageName());
		ps.setString(i++, imageData.getLatitude());
		ps.setString(i++, imageData.getLongitude());
		ps.setString(i++, imageData.getLocation());
		ps.setString(i++, imageData.getImagePath());
		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys();
		if (rs != null && rs.next()) {
			res.setCode("0000");
		} else {
			res.setCode("0014");
			res.setDesc("Fail.");
		}

		rs.close();
		ps.close();
		return res;
	}

}
