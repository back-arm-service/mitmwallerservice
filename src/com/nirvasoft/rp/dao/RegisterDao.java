package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.nirvasoft.rp.shared.RegisterData;

public class RegisterDao {
	public RegisterData readByID(long syskey, Connection conn) throws SQLException {
		RegisterData data = new RegisterData();

		/*
		 * ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
		 * "where RecordStatus=1 AND syskey='" + syskey + "'  ", "", conn); if
		 * (dbrs.size() > 0) { data = getDBRecord(dbrs.get(0));
		 * data.setT1(ServerUtil.encode(data.getT1()));
		 * 
		 * }
		 */

		String sql = "SELECT * FROM register Where recordStatus=1 and autokey= ? ";

		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);

		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			data.setSyskey(rs.getLong("syskey"));
			data.setAutokey(rs.getLong("autokey"));
			data.setUserid(rs.getString("userid"));
			data.setUsername(rs.getString("username"));
			data.setRecordStatus(rs.getInt("recordstatus"));
			data.setT1(rs.getString("t1"));
			data.setT2(rs.getString("t2"));
			data.setT3(rs.getString("t3"));
			data.setT4(rs.getString("t4"));
			data.setT5(rs.getString("t5"));
			data.setT6(rs.getString("t6"));
			data.setT7(rs.getString("t7"));
			data.setT8(rs.getString("t8"));
			data.setT9(rs.getString("t9"));
			data.setT10(rs.getString("t10"));
			data.setT11(rs.getString("t11"));
			data.setT12(rs.getString("t12"));
			data.setT13(rs.getString("t13"));
			data.setT14(rs.getString("t14"));
			data.setT15(rs.getString("t15"));
			data.setT16(rs.getString("t16"));
			data.setT17(rs.getString("t17"));
			data.setT18(rs.getString("t18"));
			data.setT19(rs.getString("t19"));
			data.setT20(rs.getString("t20"));
			// data.setT1(ServerUtil.encode(data.getT1()));
		}
		return data;
	}
}
