package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.nirvasoft.rp.shared.tokenData;
import com.nirvasoft.rp.util.GeneralUtil;

public class TokenDao {
	public boolean validToken(tokenData tdata, Connection l_Conn) throws SQLException {
		boolean status = false;
		PreparedStatement pstmt;
		Date nowDateTime = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd hh:mm:ss");
		String dateString = dateFormat.format(nowDateTime);
		String sql = "select T1 from partnerInfo Where userid=? And password=?";
		pstmt = l_Conn.prepareStatement(sql);
		pstmt.setString(1, tdata.getUserid());
		pstmt.setString(2, tdata.getPassword());
		ResultSet rs = pstmt.executeQuery();
		if (rs.next()) {
			String sql1 = "update partnerInfo set t1=?,t2=? Where userid=? And password=?";
			pstmt = l_Conn.prepareStatement(sql1);
			pstmt.setString(1, dateString);
			pstmt.setString(2, tdata.getToken());
			pstmt.setString(3, tdata.getUserid());
			pstmt.setString(4, tdata.getPassword());
			if (pstmt.executeUpdate() > 0) {
				status = true;
				System.out.println("Updated Last Date");
			}
		}
		return status;
	}

}
