package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;
import com.nirvasoft.fcpayment.core.data.fcchannel.DisPaymentTransactionData;
import com.nirvasoft.rp.framework.ConnAdmin;
import com.nirvasoft.rp.util.Response;

public class DispaymentTransationDao {
	public static DBRecord define(String tableName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tableName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("Transtime", (byte) 5));
		ret.getFields().add(new DBField("TransDate", (byte) 5));
		ret.getFields().add(new DBField("MobileUserID", (byte) 5));
		ret.getFields().add(new DBField("FromAccount", (byte) 5));
		ret.getFields().add(new DBField("ToAccount", (byte) 5));
		ret.getFields().add(new DBField("ToBranch", (byte) 5));
		ret.getFields().add(new DBField("FromBranch", (byte) 5));
		ret.getFields().add(new DBField("acerefno", (byte) 5));
		ret.getFields().add(new DBField("FlexRefNo", (byte) 5));
		ret.getFields().add(new DBField("CustomerCode", (byte) 5));
		ret.getFields().add(new DBField("CustomerName", (byte) 5));
		ret.getFields().add(new DBField("CustomerNumber", (byte) 5));
		ret.getFields().add(new DBField("ChequeNo", (byte) 5));
		ret.getFields().add(new DBField("RecMethod", (byte) 5));
		ret.getFields().add(new DBField("CurrencyCode", (byte) 5));
		ret.getFields().add(new DBField("Amount", (byte) 3));
		ret.getFields().add(new DBField("FileUploadDateTime", (byte) 5));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t8", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t13", (byte) 5));
		ret.getFields().add(new DBField("t14", (byte) 5));
		ret.getFields().add(new DBField("t15", (byte) 5));
		ret.getFields().add(new DBField("t16", (byte) 5));
		ret.getFields().add(new DBField("t17", (byte) 5));
		ret.getFields().add(new DBField("t18", (byte) 5));
		ret.getFields().add(new DBField("t19", (byte) 5));
		ret.getFields().add(new DBField("t20", (byte) 5));
		ret.getFields().add(new DBField("t21", (byte) 5));
		ret.getFields().add(new DBField("t22", (byte) 5));
		ret.getFields().add(new DBField("t23", (byte) 5));
		ret.getFields().add(new DBField("t24", (byte) 5));
		ret.getFields().add(new DBField("t25", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 1));
		ret.getFields().add(new DBField("n2", (byte) 1));
		ret.getFields().add(new DBField("n3", (byte) 1));
		ret.getFields().add(new DBField("n4", (byte) 1));
		ret.getFields().add(new DBField("n5", (byte) 1));
		ret.getFields().add(new DBField("n6", (byte) 1));
		ret.getFields().add(new DBField("n7", (byte) 1));
		ret.getFields().add(new DBField("n8", (byte) 1));
		ret.getFields().add(new DBField("n9", (byte) 1));
		ret.getFields().add(new DBField("n10", (byte) 1));
		ret.getFields().add(new DBField("n11", (byte) 1));
		ret.getFields().add(new DBField("n12", (byte) 1));
		ret.getFields().add(new DBField("n13", (byte) 1));
		ret.getFields().add(new DBField("n14", (byte) 1));
		ret.getFields().add(new DBField("n15", (byte) 1));
		ret.getFields().add(new DBField("n16", (byte) 1));
		ret.getFields().add(new DBField("n17", (byte) 1));
		ret.getFields().add(new DBField("n18", (byte) 1));
		ret.getFields().add(new DBField("n19", (byte) 1));
		ret.getFields().add(new DBField("n20", (byte) 1));

		return ret;
	}

	public static DisPaymentTransactionData getDBRecord(DBRecord adbr) {
		DisPaymentTransactionData ret = new DisPaymentTransactionData();
		ret.setTranstime(adbr.getString("Transtime"));
		ret.setTransdate(adbr.getString("TransDate"));
		ret.setMobileuserid(adbr.getString("MobileUserID"));
		ret.setFromAccount(adbr.getString("FromAccount"));
		ret.setToAccount(adbr.getString("ToAccount"));
		ret.setToBranch(adbr.getString("ToBranch"));
		ret.setFromBranch(adbr.getString("FromBranch"));
		ret.setAcerefno(adbr.getString("acerefno"));
		ret.setFlexrefno(adbr.getString("FlexRefNo"));
		ret.setCustomerCode(adbr.getString("CustomerCode"));
		ret.setCustomerName(adbr.getString("CustomerName"));
		ret.setCustomerNumber(adbr.getString("CustomerNumber"));
		ret.setChequeNo(adbr.getString("ChequeNo"));
		ret.setRecmethod(adbr.getString("RecMethod"));
		ret.setCurrencycode(adbr.getString("CurrencyCode"));
		ret.setAmount(adbr.getDouble("Amount"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(adbr.getString("t2"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));
		ret.setT8(adbr.getString("t8"));
		ret.setT9(adbr.getString("t9"));
		ret.setT10(adbr.getString("t10"));
		ret.setT11(adbr.getString("t11"));
		ret.setT12(adbr.getString("t12"));
		ret.setT13(adbr.getString("t13"));
		ret.setT14(adbr.getString("t14"));
		ret.setT15(adbr.getString("t15"));
		ret.setT16(adbr.getString("t16"));
		ret.setT17(adbr.getString("t17"));
		ret.setT18(adbr.getString("t18"));
		ret.setT19(adbr.getString("t19"));
		ret.setT20(adbr.getString("t20"));
		ret.setT21(adbr.getString("t21"));
		ret.setT22(adbr.getString("t22"));
		ret.setT23(adbr.getString("t23"));
		ret.setT24(adbr.getString("t24"));
		ret.setT25(adbr.getString("t25"));
		ret.setN1(adbr.getInt("n1"));
		ret.setN2(adbr.getInt("n2"));
		ret.setN3(adbr.getInt("n3"));
		ret.setN4(adbr.getInt("n4"));
		ret.setN5(adbr.getInt("n5"));
		ret.setN6(adbr.getInt("n6"));
		ret.setN7(adbr.getInt("n7"));
		ret.setN8(adbr.getInt("n8"));
		ret.setN9(adbr.getInt("n9"));
		ret.setN10(adbr.getInt("n10"));
		ret.setN11(adbr.getInt("n11"));
		ret.setN12(adbr.getInt("n12"));
		ret.setN13(adbr.getInt("n13"));
		ret.setN14(adbr.getInt("n14"));
		ret.setN15(adbr.getInt("n15"));
		ret.setN16(adbr.getInt("n16"));
		ret.setN17(adbr.getInt("n17"));
		ret.setN18(adbr.getInt("n18"));
		ret.setN19(adbr.getInt("19"));
		ret.setN20(adbr.getInt("n20"));

		return ret;
	}

	public static Response postTransation(DisPaymentTransactionData transdata, String tableName) throws SQLException {
		Response response = new Response();
		Connection conn = ConnAdmin.getConn();
		String sql = DBMgr.insertString(define(tableName), conn);
		PreparedStatement stmt = conn.prepareStatement(sql);
		DBRecord dbr = setDBRecord(transdata, tableName);
		DBMgr.setValues(stmt, dbr);
		stmt.executeUpdate();
		response.setId(transdata.getFlexrefno());
		response.setResponsecode("0000");
		response.setResponsedesc("Saved Successfully.");
		if (!conn.isClosed()) {
			conn.close();
		}
		return response;
	}

	public static DBRecord setDBRecord(DisPaymentTransactionData data, String tableName) {

		DBRecord ret = define(tableName);

		ret.setValue("Transtime", data.getTranstime());
		ret.setValue("TransDate", data.getTransdate());
		ret.setValue("MobileUserID", data.getMobileuserid());
		ret.setValue("FromAccount", data.getFromAccount());
		ret.setValue("ToAccount", data.getToAccount());
		ret.setValue("ToBranch", data.getToBranch());
		ret.setValue("FromBranch", data.getFromBranch());
		ret.setValue("acerefno", data.getAcerefno());
		ret.setValue("FlexRefNo", data.getFlexrefno());
		ret.setValue("CustomerCode", data.getCustomerCode());
		ret.setValue("CustomerName", data.getCustomerName());
		ret.setValue("CustomerNumber", data.getCustomerNumber());
		ret.setValue("ChequeNo", data.getChequeNo());
		ret.setValue("RecMethod", data.getRecmethod());
		ret.setValue("CurrencyCode", data.getCurrencycode());
		ret.setValue("Amount", data.getAmount());
		ret.setValue("FileUploadDateTime", data.getFileuploaddatetime());

		ret.setValue("t1", data.getT1());
		ret.setValue("t2", data.getT2());
		ret.setValue("t3", data.getT3());
		ret.setValue("t4", data.getT4());
		ret.setValue("t5", data.getT5());
		ret.setValue("t6", data.getT6());
		ret.setValue("t7", data.getT7());
		ret.setValue("t8", data.getT8());
		ret.setValue("t9", data.getT9());
		ret.setValue("t10", data.getT10());
		ret.setValue("t11", data.getT11());
		ret.setValue("t12", data.getT12());
		ret.setValue("t13", data.getT13());
		ret.setValue("t14", data.getT14());
		ret.setValue("t15", data.getT15());
		ret.setValue("t16", data.getT16());
		ret.setValue("t17", data.getT17());
		ret.setValue("t18", data.getT18());
		ret.setValue("t19", data.getT19());
		ret.setValue("t20", data.getT20());
		ret.setValue("t21", data.getT21());
		ret.setValue("t22", data.getT22());
		ret.setValue("t23", data.getT23());
		ret.setValue("t24", data.getT24());
		ret.setValue("t25", data.getT25());
		ret.setValue("n1", data.getN1());
		ret.setValue("n2", data.getN2());
		ret.setValue("n3", data.getN3());
		ret.setValue("n4", data.getN4());
		ret.setValue("n5", data.getN5());
		ret.setValue("n6", data.getN6());
		ret.setValue("n7", data.getN7());
		ret.setValue("n8", data.getN8());
		ret.setValue("n9", data.getN9());
		ret.setValue("n10", data.getN10());
		ret.setValue("n11", data.getN11());
		ret.setValue("n12", data.getN12());
		ret.setValue("n13", data.getN13());
		ret.setValue("n14", data.getN14());
		ret.setValue("n15", data.getN15());
		ret.setValue("n16", data.getN16());
		ret.setValue("n17", data.getN17());
		ret.setValue("n18", data.getN18());
		ret.setValue("n19", data.getN19());
		ret.setValue("n20", data.getN20());
		return ret;
	}

	public double getTransAmountPerDay(String productcode, String userid, String transdate, Connection conn)
			throws SQLException {
		double ret = 0.00;
		String sql = "select sum(Amount) as amount from dispaymenttransaction where  FlexRefNo like ? and t24 = ? and transdate = ? ";
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setString(i++, "%" + productcode + "%");
		ps.setString(i++, userid);
		ps.setString(i++, transdate);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ret = rs.getDouble(1);
		}
		ps.close();
		rs.close();
		return ret;
	}

	public double getTransAmountPerDayICBS(String fromAccount, String userid, String transdate, Connection conn)
			throws SQLException {
		double ret = 0.00;
		String sql = "select sum(Amount) as amount from dispaymenttransaction where fromAccount = ? and t24 = ? and transdate = ? ";
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setString(i++, fromAccount);
		ps.setString(i++, userid);
		ps.setString(i++, transdate);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ret = rs.getDouble(1);
		}
		ps.close();
		rs.close();
		return ret;
	}

	public boolean updateforReversal(DisPaymentTransactionData data, Connection conn) throws SQLException {
		boolean res = false;
		String sql = "update dispaymenttransaction set n9 =?, t2 = ?, n2 = ?, n5 = ?, t3 = ?, t4 = ? where t5 = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setInt(i++, data.getN9());
		ps.setString(i++, data.getT2());
		ps.setInt(i++, data.getN2());
		ps.setInt(i++, data.getN5());
		ps.setString(i++, data.getT3());
		ps.setString(i++, data.getT4());
		ps.setString(i++, data.getT5());
		if (ps.executeUpdate() > 0) {
			res = true;
		}
		return res;
	}

	public boolean updateforWalletTopup(DisPaymentTransactionData data, Connection conn) throws SQLException {
		boolean res = false;
		String sql = "update dispaymenttransaction set  t2 = ?, t6 = ? where t5 = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		int i = 1;
		ps.setString(i++, data.getT2());
		ps.setString(i++, data.getT6());
		ps.setString(i++, data.getT5());
		if (ps.executeUpdate() > 0) {
			res = true;
		}
		return res;
	}

	public boolean updateT6SkyPayException(String flexRefNo, Connection conn) throws SQLException {
		boolean ret = false;
		String sql = "update DisPaymentTransaction set N6 = 1 where FlexRefNo = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, flexRefNo);
		if (ps.executeUpdate() > 0) {
			ret = true;
		}
		return ret;
	}

}
