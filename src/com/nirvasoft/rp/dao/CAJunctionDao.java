package com.nirvasoft.rp.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.nirvasoft.rp.dao.icbs.CustomerDAO;
import com.nirvasoft.rp.shared.AccountCustomerData;
import com.nirvasoft.rp.shared.CAJunctionData;

public class CAJunctionDao {
	public static String mTableName = "CAJunction";
	private ArrayList<AccountCustomerData> lstCAJunBean;
	private AccountCustomerData CAJunBean;

	public CAJunctionDao() {
		lstCAJunBean = new ArrayList<AccountCustomerData>();
		CAJunBean = new AccountCustomerData();
	}

	public AccountCustomerData getCAJunctionBean() {
		return CAJunBean;
	}

	public ArrayList<AccountCustomerData> getCAJunctionBeanList() {
		return lstCAJunBean;
	}

	public boolean getCAJunctions(String aAccNumber, Connection conn)
			throws ParserConfigurationException, SAXException, IOException, ClassNotFoundException, SQLException {
		boolean result = false;
		PreparedStatement pstmt = conn.prepareStatement(
				"SELECT AccNumber, CustomerId, AccType, RType FROM CAJunction WHERE AccNumber=? ORDER BY RType");

		pstmt.setString(1, aAccNumber);
		pstmt.setQueryTimeout(DAOManager.getNormalTime());
		ResultSet rs = pstmt.executeQuery();

		int i = 0;
		while (rs.next()) {
			CustomerDAO l_CusDAO = new CustomerDAO();
			AccountCustomerData l_CAJunBean = new AccountCustomerData();
			readRecord(l_CAJunBean, rs);
			l_CAJunBean.setCustomer(l_CusDAO.getCustomer(l_CAJunBean.getCustomerID(), conn));
			lstCAJunBean.add(i++, l_CAJunBean);
			result = true;
		}
		pstmt.close();

		return result;
	}

	private void readRecord(AccountCustomerData aCAJunBean, ResultSet aRS) {
		try {
			aCAJunBean.setAccountNumber(aRS.getString("AccNumber"));
			aCAJunBean.setCustomerID(aRS.getString("CustomerId"));
			aCAJunBean.setAccountType(aRS.getInt("AccType"));
			aCAJunBean.setRelationType(aRS.getInt("RType"));

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean save(CAJunctionData data, Connection conn) throws SQLException {
		boolean ret = false;
		String sql = "INSERT INTO " + mTableName + " VALUES(?,?,?,?)";
		PreparedStatement ps = conn.prepareStatement(sql);
		updateRecord(data, ps);
		if (ps.executeUpdate() > 0) {
			ret = true;
		}
		ps.close();
		return ret;
	}

	public void setCAJunctionBean(AccountCustomerData aCAJunBean) {
		CAJunBean = aCAJunBean;
	}

	public void setCAJunctionBeanList(ArrayList<AccountCustomerData> aCAJunBeanList) {
		lstCAJunBean = aCAJunBeanList;
	}

	private void updateRecord(CAJunctionData aData, PreparedStatement ps) throws SQLException {
		int index = 1;
		ps.setString(index++, aData.getAccNumber());
		ps.setString(index++, aData.getCustomerId());
		ps.setInt(index++, aData.getAccType());
		ps.setInt(index++, aData.getrType());
	}
}
