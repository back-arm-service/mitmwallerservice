package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;
import com.nirvasoft.rp.shared.CashTransactionData;
import com.nirvasoft.rp.shared.Result;

public class CashTransactionDao {

	public static ArrayList<CashTransactionData> cashControlDepList(String userId, int type, String trdate,
			Connection aConn) throws SQLException {
		ArrayList<CashTransactionData> l_rs = new ArrayList<CashTransactionData>();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				"where mobileuserid='" + userId + "' and TrDatetime like '" + trdate + "%' and trtype=" + type, "",
				aConn);
		for (int i = 0; i < dbrs.size(); i++) {
			l_rs.add(getDBRecord(dbrs.get(i)));
		}
		return l_rs;

	}

	public static ArrayList<CashTransactionData> cashDepList(String userId, String fromDate, String toDate,
			int currentPage, int pageSize, Connection aConn) throws SQLException {
		ArrayList<CashTransactionData> l_rs = new ArrayList<CashTransactionData>();
		ArrayList<CashTransactionData> l_temp = new ArrayList<CashTransactionData>();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "where mobileuserid='" + userId
				+ "' and trtype=4 and TrDate BETWEEN '" + fromDate + "' AND '" + toDate + "' ", "", aConn);
		for (int i = 0; i < dbrs.size(); i++) {
			l_temp.add(getDBRecord(dbrs.get(i)));
		}
		int start = (currentPage - 1) * pageSize;
		for (int i = start; i < l_temp.size() && i < (pageSize + start); i++) {
			l_temp.get(i).setTotalSize(l_temp.size());
			l_rs.add(l_temp.get(i));
		}
		return l_rs;

	}

	public static CashTransactionData cashHoldingBalance(String userId, String trdate, Connection aConn)
			throws SQLException {
		CashTransactionData ret = new CashTransactionData();
		String l_sql = "select sum(Case when drcr=2 then denoamount else 0 end) as credit,"
				+ "sum(case when drcr=1 then denoamount else 0 end) as debit"
				+ " from cashtransaction where trdate like '" + trdate + "%' and mobileuserid='" + userId + "'";
		PreparedStatement pstmt = aConn.prepareStatement(l_sql);
		ResultSet rs = pstmt.executeQuery();
		while (rs.next()) {
			ret.setDebitAmount(rs.getDouble("debit"));
			ret.setCreditAmount(rs.getDouble("credit"));
			ret.setCode("0000");
			ret.setDesc("Success");
		}
		return ret;

	}

	public static DBRecord define() {
		DBRecord rec = new DBRecord();
		rec.setTableName("CashTransaction");
		rec.setFields(new ArrayList<DBField>());
		rec.getFields().add(new DBField("mobileUserID", (byte) 5));
		rec.getFields().add(new DBField("denoamount", (byte) 3));
		rec.getFields().add(new DBField("drcr", (byte) 1));
		rec.getFields().add(new DBField("trtype", (byte) 1));
		rec.getFields().add(new DBField("trDate", (byte) 5));
		rec.getFields().add(new DBField("trdatetime", (byte) 5));
		rec.getFields().add(new DBField("t1", (byte) 5));
		rec.getFields().add(new DBField("t2", (byte) 5));
		rec.getFields().add(new DBField("t3", (byte) 5));
		rec.getFields().add(new DBField("t4", (byte) 5));
		rec.getFields().add(new DBField("t5", (byte) 5));
		rec.getFields().add(new DBField("t6", (byte) 5));
		rec.getFields().add(new DBField("t7", (byte) 5));
		rec.getFields().add(new DBField("t8", (byte) 5));
		rec.getFields().add(new DBField("t9", (byte) 5));
		rec.getFields().add(new DBField("t10", (byte) 5));
		rec.getFields().add(new DBField("n1", (byte) 1));
		rec.getFields().add(new DBField("n2", (byte) 1));
		rec.getFields().add(new DBField("n3", (byte) 1));
		rec.getFields().add(new DBField("n4", (byte) 1));
		rec.getFields().add(new DBField("n5", (byte) 1));
		rec.getFields().add(new DBField("n6", (byte) 2));
		rec.getFields().add(new DBField("n7", (byte) 2));
		rec.getFields().add(new DBField("n8", (byte) 2));
		rec.getFields().add(new DBField("n9", (byte) 2));
		rec.getFields().add(new DBField("n10", (byte) 2));
		return rec;
	}

	public static DBRecord defineupdate() {
		DBRecord rec = new DBRecord();
		rec.setTableName("CashTransaction");
		rec.setFields(new ArrayList<DBField>());
		rec.getFields().add(new DBField("Id", (byte) 5));
		rec.getFields().add(new DBField("MobileUserID", (byte) 5));
		rec.getFields().add(new DBField("DenoAmount", (byte) 5));
		rec.getFields().add(new DBField("DrCr", (byte) 5));
		rec.getFields().add(new DBField("TrDate", (byte) 5));
		rec.getFields().add(new DBField("TrType", (byte) 5));
		rec.getFields().add(new DBField("TrDateTime", (byte) 5));
		rec.getFields().add(new DBField("t1", (byte) 5));
		rec.getFields().add(new DBField("t2", (byte) 5));
		rec.getFields().add(new DBField("t3", (byte) 5));
		rec.getFields().add(new DBField("t4", (byte) 5));
		rec.getFields().add(new DBField("t5", (byte) 5));
		rec.getFields().add(new DBField("t6", (byte) 5));
		rec.getFields().add(new DBField("t7", (byte) 5));
		rec.getFields().add(new DBField("t8", (byte) 5));
		rec.getFields().add(new DBField("t9", (byte) 5));
		rec.getFields().add(new DBField("t10", (byte) 5));
		rec.getFields().add(new DBField("n1", (byte) 1));
		rec.getFields().add(new DBField("n2", (byte) 1));
		rec.getFields().add(new DBField("n3", (byte) 1));
		rec.getFields().add(new DBField("n4", (byte) 1));
		rec.getFields().add(new DBField("n5", (byte) 1));
		rec.getFields().add(new DBField("n6", (byte) 1));
		rec.getFields().add(new DBField("n7", (byte) 1));
		rec.getFields().add(new DBField("n8", (byte) 1));
		rec.getFields().add(new DBField("n9", (byte) 1));
		rec.getFields().add(new DBField("n10", (byte) 1));
		return rec;
	}

	public static ArrayList<CashTransactionData> getCashDepositList(String userId, String trDate, Connection aConn)
			throws SQLException {
		ArrayList<CashTransactionData> l_rs = new ArrayList<CashTransactionData>();

		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(),
				"where mobileuserid='" + userId + "' and trtype=3 and TrDate='" + trDate + "'", "", aConn);
		for (int i = 0; i < dbrs.size(); i++) {
			l_rs.add(getDBRecord(dbrs.get(i)));
		}

		return l_rs;
	}

	public static CashTransactionData getDBRecord(DBRecord aRecord) {
		CashTransactionData mDenoDto = new CashTransactionData();
		mDenoDto.setId(aRecord.getLong("ID"));
		mDenoDto.setMobileuserid(aRecord.getString("mobileuserid"));
		mDenoDto.setDenoamount(aRecord.getDouble("DenoAmount"));
		mDenoDto.setDrcr(aRecord.getInt("DrCr"));
		mDenoDto.setTrtype(aRecord.getInt("trtype"));
		mDenoDto.setTrDate(aRecord.getString("trDate"));
		mDenoDto.setTrdatetime(aRecord.getString("trdatetime"));
		mDenoDto.setT1(aRecord.getString("t1"));
		mDenoDto.setT2(aRecord.getString("t2"));
		mDenoDto.setT3(aRecord.getString("t3"));
		mDenoDto.setT4(aRecord.getString("t4"));
		mDenoDto.setT5(aRecord.getString("t5"));
		mDenoDto.setT6(aRecord.getString("t6"));
		mDenoDto.setT7(aRecord.getString("t7"));
		mDenoDto.setT8(aRecord.getString("t8"));
		mDenoDto.setT9(aRecord.getString("t9"));
		mDenoDto.setT9(aRecord.getString("t10"));
		mDenoDto.setN1(aRecord.getInt("n1"));
		mDenoDto.setN2(aRecord.getInt("n2"));
		mDenoDto.setN3(aRecord.getInt("n3"));
		mDenoDto.setN4(aRecord.getInt("n4"));
		mDenoDto.setN5(aRecord.getInt("n5"));
		mDenoDto.setN6(aRecord.getInt("n6"));
		mDenoDto.setN7(aRecord.getInt("n7"));
		mDenoDto.setN8(aRecord.getInt("n8"));
		mDenoDto.setN9(aRecord.getInt("n9"));
		mDenoDto.setN9(aRecord.getInt("n10"));

		return mDenoDto;
	}

	public static CashTransactionData getLastCashTrDate(String userId, Connection aConn) throws SQLException {
		CashTransactionData l_rs = new CashTransactionData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(define(), "where mobileuserid='" + userId + "'", "", aConn);
		if (dbrs.size() > 0) {
			l_rs = getDBRecord(dbrs.get(0));
		}

		return l_rs;

	}

	public static ResultSet getSUMFunctionResultSet(String groupColumn, String sumStr, DBRecord aDBRecord,
			String aFilter, String aOrder, Connection aConnection) throws SQLException {
		Statement stmt = aConnection.createStatement();
		String sql = "select " + groupColumn + "," + sumStr + " from " + aDBRecord.getTableName() + " " + aFilter + ""
				+ aOrder;
		ResultSet ret = stmt.executeQuery(sql);
		return ret;
	}

	public static boolean insertDenomination(CashTransactionData denoData, Connection aConn) throws SQLException {
		String sql = DBMgr.insertString(define(), aConn);
		PreparedStatement stm = aConn.prepareStatement(sql);
		DBRecord dbr = setDBRecord(denoData);
		DBMgr.setValues(stm, dbr);
		boolean l_rs = false;
		if (stm.executeUpdate() > 0) {
			l_rs = true;
		}
		return l_rs;
	}

	public static DBRecord setDBRecord(CashTransactionData mDenoData) {
		DBRecord record = define();

		record.setValue("mobileuserId", (mDenoData.getMobileuserid() != null) ? mDenoData.getMobileuserid() : "");
		record.setValue("DenoAmount", mDenoData.getDenoamount());
		record.setValue("DrCr", mDenoData.getDrcr());
		record.setValue("TrType", mDenoData.getTrtype());
		record.setValue("TrDate", (mDenoData.getTrDate() != null) ? mDenoData.getTrDate() : "");
		record.setValue("Trdatetime", (mDenoData.getTrdatetime() != null) ? mDenoData.getTrdatetime() : "");
		record.setValue("t1", (mDenoData.getT1() != null) ? mDenoData.getT1() : "");
		record.setValue("t2", (mDenoData.getT2() != null) ? mDenoData.getT2() : "");
		record.setValue("t3", (mDenoData.getT3() != null) ? mDenoData.getT3() : "");
		record.setValue("t4", (mDenoData.getT4() != null) ? mDenoData.getT4() : "");
		record.setValue("t5", (mDenoData.getT5() != null) ? mDenoData.getT5() : "");
		record.setValue("t6", (mDenoData.getT6() != null) ? mDenoData.getT6() : "");
		record.setValue("t7", (mDenoData.getT7() != null) ? mDenoData.getT7() : "");
		record.setValue("t8", (mDenoData.getT8() != null) ? mDenoData.getT8() : "");
		record.setValue("t9", (mDenoData.getT9() != null) ? mDenoData.getT9() : "");
		record.setValue("t10", (mDenoData.getT10() != null) ? mDenoData.getT10() : "");
		record.setValue("N1", mDenoData.getN1());
		record.setValue("N2", mDenoData.getN2());
		record.setValue("N3", mDenoData.getN3());
		record.setValue("N4", mDenoData.getN4());
		record.setValue("N5", mDenoData.getN5());
		record.setValue("N6", mDenoData.getN6());
		record.setValue("N7", mDenoData.getN7());
		record.setValue("N8", mDenoData.getN8());
		record.setValue("N9", mDenoData.getN9());
		record.setValue("N10", mDenoData.getN10());
		return record;
	}

	public static DBRecord setDBRecordUpdate(CashTransactionData mCAData) {
		DBRecord record = define();
		record.setValue("t1", mCAData.getT1());
		record.setValue("t9", mCAData.getT9());
		return record;
	}

	public static Result update(CashTransactionData aObj, Connection aConnection) throws SQLException {
		String sql = DBMgr.updateString(
				"where ID=" + aObj.getId() + " and mobileuserid='" + aObj.getMobileuserid() + "'", defineupdate(),
				aConnection); // define
		PreparedStatement stmt = aConnection.prepareStatement(sql);
		DBRecord dbr = setDBRecordUpdate(aObj);
		DBMgr.setValues(stmt, dbr);
		Result l_rs = new Result();
		if (stmt.executeUpdate() > 0) {
			l_rs.setState(true);
		} else {
			l_rs.setState(false);
		}

		return l_rs;
	}
	
}
