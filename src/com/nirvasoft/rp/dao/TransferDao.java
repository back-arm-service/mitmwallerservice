package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.nirvasoft.rp.shared.TransferReqV2Data;

public class TransferDao {

	public boolean saveTransferTrans(TransferReqV2Data aData, Connection conn) throws SQLException {
		boolean res = false;
		String sql = "insert into MPOTransfer (APIKey,TransID,RefKey,SenderOperator,SenderAccount,"
				+ "SenderWallet,BeneOperator,BeneAccount,BeneWallet,Amount,Commission1,Commission2,"
				+ "Commission3,Narration,Status,SenderResCode,SenderResDesc,BeneResCode,BeneResDesc,SenderReqDateTime,SenderResDateTime,BeneReqDateTime,BeneResDateTime,T1,T2,T3,T4,T5,"
				+ "N1,N2,N3,N4,N5) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement ps = conn.prepareStatement(sql);
		updatePrepareStatement(ps, aData);
		int rst = ps.executeUpdate();
		if (rst > 0) {
			res = true;
		}
		ps.close();
		return res;
	}

	private void updatePrepareStatement(PreparedStatement ps, TransferReqV2Data aData) throws SQLException {
		int index = 1;
		ps.setString(index++, aData.getApikey());
		ps.setString(index++, aData.getTransID());
		ps.setString(index++, aData.getRefKey());
		ps.setString(index++, aData.getFromoperator());
		ps.setString(index++, aData.getFromaccount());
		ps.setString(index++, aData.getFromwallet());
		ps.setString(index++, aData.getTooperator());
		ps.setString(index++, aData.getToaccount());
		ps.setString(index++, aData.getTowallet());
		ps.setDouble(index++, aData.getAmount());
		ps.setDouble(index++, aData.getComm1());
		ps.setDouble(index++, aData.getComm2());
		ps.setDouble(index++, aData.getComm3());
		ps.setString(index++, aData.getNarration());
		ps.setInt(index++, aData.getStatus());
		ps.setString(index++, aData.getFromcode());
		ps.setString(index++, aData.getFromdesc());
		ps.setString(index++, aData.getTocode());
		ps.setString(index++, aData.getTodesc());
		ps.setString(index++, aData.getFromreqdatetime());
		ps.setString(index++, aData.getFromresdatetime());
		ps.setString(index++, aData.getToreqdatetime());
		ps.setString(index++, aData.getToresdatetime());
		ps.setString(index++, aData.getT1());
		ps.setString(index++, aData.getT2());
		ps.setString(index++, aData.getT3());
		ps.setString(index++, aData.getT4());
		ps.setString(index++, aData.getT5());
		ps.setInt(index++, aData.getN1());
		ps.setInt(index++, aData.getN2());
		ps.setInt(index++, aData.getN3());
		ps.setInt(index++, aData.getN4());
		ps.setInt(index++, aData.getN5());
	}
}
