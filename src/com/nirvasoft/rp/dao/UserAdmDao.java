package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import com.nirvasoft.database.DBField;
import com.nirvasoft.database.DBMgr;
import com.nirvasoft.database.DBRecord;
import com.nirvasoft.rp.dao.integration.SMSSettingDao;
import com.nirvasoft.rp.framework.Result;
import com.nirvasoft.rp.mgr.SMSMgr;
import com.nirvasoft.rp.shared.ResponseData;
import com.nirvasoft.rp.shared.SessionData;
import com.nirvasoft.rp.shared.integration.SMSParameter;
import com.nirvasoft.rp.shared.integration.SMSSettingData;
import com.nirvasoft.rp.users.data.UCJunction;
import com.nirvasoft.rp.users.data.UserData;
import com.nirvasoft.rp.users.data.UserViewData;
import com.nirvasoft.rp.users.data.UserViewDataset;
import com.nirvasoft.rp.util.GeneralUtil;
import com.nirvasoft.rp.util.ServerUtil;

public class UserAdmDao {
	/////////////
	// generate password
	public static String autogeneratePassword(int length) {

		String pwd = "";
		if (length == 0) {
			length = 6;
		}
		int pwdlength = length;

		char ch;
		String num_list = "0123456789";
		String char_list = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		// String specialchar_list = "!#$%&*?@";
		// String specialchar_list = "!@#$%^&*()+`~";
		// String list =
		// "!#$%&*?@abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		String list = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		StringBuffer randStr = new StringBuffer();

		for (int i = 0; i < pwdlength; i++) {
			Random randomGenerator = new Random();
			int number = 0;
			if (i == 0) {
				number = randomGenerator.nextInt(char_list.length());
				ch = char_list.charAt(number);
			} /*
				 * else if (i == 1) { number =
				 * randomGenerator.nextInt(specialchar_list.length()); ch =
				 * specialchar_list.charAt(number); }
				 */ else if (i == 2) {
				number = randomGenerator.nextInt(num_list.length());
				ch = num_list.charAt(number);
			} else {
				number = randomGenerator.nextInt(list.length());
				ch = list.charAt(number);
			}
			randStr.append(ch);
		}
		pwd = String.valueOf(randStr);
		return pwd;

	}

	public static DBRecord define() {
		DBRecord ret = new DBRecord();
		ret.setTableName("UVM005_A");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("CreatedDate", (byte) 5));
		ret.getFields().add(new DBField("ModifiedDate", (byte) 5));
		ret.getFields().add(new DBField("UserId", (byte) 5));
		ret.getFields().add(new DBField("UserName", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t2", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t5", (byte) 5));
		ret.getFields().add(new DBField("t6", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 1));
		ret.getFields().add(new DBField("n3", (byte) 2));
		ret.getFields().add(new DBField("n4", (byte) 2));
		ret.getFields().add(new DBField("n5", (byte) 1));
		ret.getFields().add(new DBField("n6", (byte) 1));
		ret.getFields().add(new DBField("n7", (byte) 1));
		ret.getFields().add(new DBField("n8", (byte) 2));

		return ret;
	}

	public static DBRecord define(String tabName) {
		DBRecord ret = new DBRecord();
		ret.setTableName(tabName);
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("n1", (byte) 2));
		ret.getFields().add(new DBField("n2", (byte) 2));
		ret.getFields().add(new DBField("n3", (byte) 1));
		ret.getFields().add(new DBField("n4", (byte) 1));
		ret.getFields().add(new DBField("n5", (byte) 2));

		return ret;
	}

	/*
	 * /////////// public static DBRecord defineView() { DBRecord ret = new
	 * DBRecord(); ret.setTableName("V_U001_A"); ret.setFields(new
	 * ArrayList<DBField>()); ret.getFields().add(new DBField("syskey", (byte)
	 * 2)); ret.getFields().add(new DBField("autokey", (byte) 2));
	 * ret.getFields().add(new DBField("CreatedDate", (byte) 5));
	 * ret.getFields().add(new DBField("ModifiedDate", (byte) 5));
	 * ret.getFields().add(new DBField("UserId", (byte) 5));
	 * ret.getFields().add(new DBField("UserName", (byte) 5));
	 * ret.getFields().add(new DBField("RecordStatus", (byte) 1));
	 * ret.getFields().add(new DBField("SyncStatus", (byte) 1));
	 * ret.getFields().add(new DBField("SyncBatch", (byte) 2)); //
	 * ret.getFields().add(new DBField("usersyskey", (byte) 2));
	 * ret.getFields().add(new DBField("t1", (byte) 5)); ret.getFields().add(new
	 * DBField("t2", (byte) 5)); ret.getFields().add(new DBField("t3", (byte)
	 * 5)); ret.getFields().add(new DBField("t7", (byte) 5)); //
	 * ret.getFields().add(new DBField("t2", (byte) 5)); ret.getFields().add(new
	 * DBField("UN", (byte) 5)); ret.getFields().add(new DBField("lock", (byte)
	 * 1)); ret.getFields().add(new DBField("role", (byte) 1));
	 * ret.getFields().add(new DBField("customerID", (byte) 5)); return ret; }
	 */
	///////////
	public static DBRecord defineView() {
		DBRecord ret = new DBRecord();
		ret.setTableName("Register");
		ret.setFields(new ArrayList<DBField>());
		ret.getFields().add(new DBField("syskey", (byte) 2));
		ret.getFields().add(new DBField("autokey", (byte) 2));
		ret.getFields().add(new DBField("CreatedDate", (byte) 5));
		ret.getFields().add(new DBField("ModifiedDate", (byte) 5));
		ret.getFields().add(new DBField("UserId", (byte) 5));
		ret.getFields().add(new DBField("UserName", (byte) 5));
		ret.getFields().add(new DBField("RecordStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncStatus", (byte) 1));
		ret.getFields().add(new DBField("SyncBatch", (byte) 2));
		// ret.getFields().add(new DBField("usersyskey", (byte) 2));
		ret.getFields().add(new DBField("t1", (byte) 5));
		ret.getFields().add(new DBField("t3", (byte) 5));
		ret.getFields().add(new DBField("t4", (byte) 5));
		ret.getFields().add(new DBField("t7", (byte) 5));
		ret.getFields().add(new DBField("t9", (byte) 5));
		ret.getFields().add(new DBField("t10", (byte) 5));
		ret.getFields().add(new DBField("t11", (byte) 5));
		ret.getFields().add(new DBField("t12", (byte) 5));
		ret.getFields().add(new DBField("t21", (byte) 5));
		ret.getFields().add(new DBField("n1", (byte) 2));
		return ret;
	}

	public static String generatePassword(Connection conn) throws SQLException {
		String num_list = "0123456789";
		String lowerchar_list = "abcdefghijklmnopqrstuvwxyz";
		String upperchar_list = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String specialchar_list = "!#$%&*?@";
		// String list =
		// "!#$%&*?@abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		String list = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

		int minimunlength = 0;
		int splength = 0;
		int uppercharlength = 0;
		int lowercharlength = 0;
		int numberlength = 0;

		StringBuffer randStr = new StringBuffer();
		int number = 0;

		String sql = "SELECT MinimumLength,SpecialCharacter,UpperCaseCharacter,LowerCaseCharacter,Number FROM tblPasswordpolicy";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet res = ps.executeQuery();
		while (res.next()) {
			minimunlength = res.getInt("MinimumLength");
			splength = res.getInt("SpecialCharacter");// 2
			uppercharlength = res.getInt("UpperCaseCharacter");// 1
			lowercharlength = res.getInt("LowerCaseCharacter");// 1
			numberlength = res.getInt("Number");// 0
		}

		String pwd = "";
		if (splength == 0 && uppercharlength == 0 && lowercharlength == 0 && numberlength == 0) {
			pwd = autogeneratePassword(minimunlength);
		} else {
			char ch;
			int count = 0;
			Random randomGenerator = new Random();
			if (splength != 0) {
				number = randomGenerator.nextInt(specialchar_list.length());
				ch = specialchar_list.charAt(number);
				randStr.append(ch);
				splength = splength - 1;
				count++;
			}
			if (uppercharlength != 0) {
				number = randomGenerator.nextInt(upperchar_list.length());
				ch = upperchar_list.charAt(number);
				randStr.append(ch);
				uppercharlength = uppercharlength - 1;
				count++;
			}
			if (lowercharlength != 0) {
				number = randomGenerator.nextInt(lowerchar_list.length());
				ch = lowerchar_list.charAt(number);
				randStr.append(ch);
				lowercharlength = lowercharlength - 1;
				count++;
			}
			if (numberlength != 0) {
				number = randomGenerator.nextInt(num_list.length());
				ch = num_list.charAt(number);
				randStr.append(ch);
				numberlength = numberlength - 1;
				count++;
			}
			if (splength != 0) {
				for (int f = 0; f < splength; f++) {
					number = randomGenerator.nextInt(specialchar_list.length());
					ch = specialchar_list.charAt(number);
					randStr.append(ch);
					count++;
				}
			}

			if (uppercharlength != 0) {
				for (int d = 0; d < uppercharlength; d++) {
					number = randomGenerator.nextInt(upperchar_list.length());
					ch = upperchar_list.charAt(number);
					randStr.append(ch);
					count++;
				}

			}
			if (numberlength != 0) {
				for (int j = 0; j < numberlength; j++) {
					number = randomGenerator.nextInt(num_list.length());
					ch = num_list.charAt(number);
					randStr.append(ch);
					count++;
				}
			}

			if (lowercharlength != 0) {
				for (int k = 0; k < lowercharlength; k++) {
					number = randomGenerator.nextInt(lowerchar_list.length());
					ch = lowerchar_list.charAt(number);
					randStr.append(ch);
					count++;
				}
			}

			if (count < minimunlength) {
				for (int i = 0; i < (minimunlength - count); i++) {
					number = randomGenerator.nextInt(list.length());
					ch = list.charAt(number);
					randStr.append(ch);
				}
			}
			pwd = String.valueOf(randStr);
		}
		return pwd;
	}

	public static UserData getDBRecord(DBRecord adbr) {
		UserData ret = new UserData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setCreatedDate(adbr.getString("CreatedDate"));
		ret.setModifiedDate(adbr.getString("ModifiedDate"));
		ret.setUserId(adbr.getString("UserId"));
		ret.setUserName(adbr.getString("UserName"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));

		ret.setUsersyskey(adbr.getLong("usersysKey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT2(ServerUtil.decryptPIN(adbr.getString("t2")));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT5(adbr.getString("t5"));
		ret.setT6(adbr.getString("t6"));
		ret.setT7(adbr.getString("t7"));

		return ret;
	}

	public static UserViewData getDBViewRecord(DBRecord adbr) {
		UserViewData ret = new UserViewData();
		ret.setSyskey(adbr.getLong("syskey"));
		ret.setAutokey(adbr.getLong("autokey"));
		ret.setCreatedDate(adbr.getString("CreatedDate"));
		ret.setModifiedDate(adbr.getString("ModifiedDate"));
		ret.setUserId(adbr.getString("UserId"));
		ret.setUserName(adbr.getString("UserName"));
		ret.setRecordStatus(adbr.getInt("RecordStatus"));
		ret.setSyncStatus(adbr.getInt("SyncStatus"));
		ret.setSyncBatch(adbr.getLong("SyncBatch"));
		// ret.setUsersyskey(adbr.getLong("usersysKey"));
		ret.setT1(adbr.getString("t1"));
		ret.setT3(adbr.getString("t3"));
		ret.setT4(adbr.getString("t4"));
		ret.setT7(adbr.getString("t7"));
		ret.setT9(adbr.getString("t9"));
		ret.setT10(adbr.getString("t10"));
		ret.setT11(adbr.getString("t11"));
		ret.setT12(adbr.getString("t12"));
		ret.setT21(adbr.getString("t21"));
		ret.setN1(adbr.getInt("N1"));

		return ret;
	}

	public Result activateDeactivateUser(long syskey, Connection conn, String status, String userId)
			throws SQLException {
		Result res = new Result();
		String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
		String readPExpiry = "";
		boolean isActivePExpiry = false;
		String addmonth;
		String todayDate;
		String sql = "";
		int restatus = 0;

		if (status.equalsIgnoreCase("Activate")) {
			restatus = 2;
			res.setMsgCode("0000");
		} else if (status.equalsIgnoreCase("Deactivate")) {
			restatus = 21;
			res = checkLockDeactive(status, syskey, conn);
		}
		try {
			if (res.getMsgCode().equals("0000")) {
				String sql1 = "Select PasswordExpiry From UserSetting where Active = 1";
				PreparedStatement stat = conn.prepareStatement(sql1);
				ResultSet result = stat.executeQuery();
				if (result.next()) {
					readPExpiry = result.getString("PasswordExpiry");
					isActivePExpiry = true;
				}
				if (isActivePExpiry) {
					todayDate = GeneralUtil.getDate();
					addmonth = GeneralUtil.addMonth(todayDate, Integer.parseInt(readPExpiry));
					sql = "UPDATE Register SET RecordStatus=?,modifieddate=?,t11=? WHERE Syskey=?";// t10=1
					PreparedStatement stmt = conn.prepareStatement(sql);
					int i = 1;
					stmt.setInt(i++, restatus);
					// stmt.setString(i++, userId);
					stmt.setString(i++, date);
					stmt.setString(i++, addmonth);
					stmt.setLong(i++, syskey);
					int rs = stmt.executeUpdate();
					if (rs > 0) {
						res.setState(true);
					} else {
						res.setState(false);
					}

				}
			} else {
				res.setState(false);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return res;
	}

	public Result canUpdate(String status, String loginID, Connection conn) throws SQLException {

		Result result = new Result();
		int totalCount = 0;
		String sql = "Select count(*) Count from TblSession Where UserId =? And Status = 0";

		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, loginID);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			totalCount = res.getInt("Count");
		}

		if (totalCount > 0) {
			result.setMsgCode("0014");
			result.setMsgDesc("Cannot " + status + ".Please forced Sign Out");
		} else {
			result.setMsgCode("0000");
			result.setMsgDesc("Success");
		}

		return result;
	}

	public Result checkLockDeactive(String status, long syskey, Connection conn) throws SQLException {

		Result result = new Result();
		int totalCount = 0;
		String sql = "Select count(*) Count from TblSession Where UserId =(Select t1 from  UVM005_A Where Syskey = ?) And Status = 0";

		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setLong(1, syskey);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			totalCount = res.getInt("Count");
		}

		if (totalCount > 0) {
			result.setMsgCode("0014");
			result.setMsgDesc("Cannot " + status + ".Please forced Sign Out.");
		} else {
			result.setMsgCode("0000");
			result.setMsgDesc("Success");
		}

		return result;
	}

	// get user setup list
	public UserViewDataset getAllUserData(String searchtext, Connection conn, String operation, boolean master)
			throws SQLException {
		int recordStatus = 0;
		int ustatus = 0;
		String searchText = searchtext.replace("'", "''");
		String whereClause = "";
		if (operation.equalsIgnoreCase("activate")) {
			if (searchText.equals("")) {
				whereClause = " where (RecordStatus = 21 or RecordStatus = 1)";
			} else {
				whereClause = " where (RecordStatus = 21 or RecordStatus = 1) and " + "(t1 like '%" + searchText
						+ "%' or UN like '%" + searchText + "%' or customerID like '%" + searchText + "%' ) ";
			}

		} else {
			if (operation.equalsIgnoreCase("lock")) {
				recordStatus = 2;
				ustatus = 0;

			} else if (operation.equalsIgnoreCase("deactivate")) {
				recordStatus = 2;
				ustatus = 0;

			} else if (operation.equalsIgnoreCase("unlock")) {
				recordStatus = 2;
				ustatus = 11;

			}

			if (searchText.equals("")) {
				whereClause = "where RecordStatus =" + recordStatus + " and n1 =" + ustatus;
			} else {
				whereClause = "where RecordStatus =" + recordStatus + " and n1 =" + ustatus + " and " + "(t1 like '%"
						+ searchText + "%' or userName like '%" + searchText + "%' ) ";
			}
		}

		ArrayList<UserViewData> ret = new ArrayList<UserViewData>();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(defineView(), whereClause, "ORDER BY autokey desc", conn);

		for (int i = 0; i < dbrs.size(); i++) {
			ret.add(getDBViewRecord(dbrs.get(i)));
		}

		UserViewDataset dataSet = new UserViewDataset();
		dataSet.setArlData(ret);
		return dataSet;
	}

	public UCJunction[] getCustIDByUID(String userid, Connection conn) {
		ArrayList<UCJunction> ret = new ArrayList<UCJunction>();
		UCJunction data = new UCJunction();
		try {
			String sql = "SELECT customerid FROM UCJunction WHERE UserId = ? ";

			PreparedStatement stat = conn.prepareStatement(sql);
			stat.setString(1, userid);
			ResultSet result = stat.executeQuery();

			while (result.next()) {
				data = new UCJunction();
				data.setCustomerid(result.getString("customerid"));
				ret.add(data);

			}

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}

		UCJunction[] dataarray = new UCJunction[ret.size()];

		for (int i = 0; i < ret.size(); i++) {
			dataarray[i] = ret.get(i);
		}
		return dataarray;
	}

	public UserData getUserNameAndNrc(SessionData udata, Connection conn) throws SQLException {

		UserData ret = new UserData();
		String pwd = "";
		// String query = "Select a.T1 'Id',a.T7 'Nrc',a.T2 'Password',b.T2
		// 'Name' from UVM005_A a Join UVM012_A b ON a.T1=b.t1 where "
		// + "(a.t1=? COLLATE SQL_Latin1_General_CP1_CS_AS ) and a.recordStatus
		// = 2 and a.n7 = 0 and a.n2 = 1";
		String query = "select userid 'Id',t21 'Nrc',t41 'Password',T3 'Name' from register where "
				+ "(t1=? COLLATE SQL_Latin1_General_CP1_CS_AS )";

		PreparedStatement stmt1 = conn.prepareStatement(query);
		stmt1.setString(1, udata.getLoginID());
		ResultSet result = stmt1.executeQuery();
		while (result.next()) {
			ret.setT1(result.getString("Id"));
			ret.setT21(result.getString("Nrc"));
			ret.setT3(result.getString("Name"));
			pwd = result.getString("Password");
			ret.setT41(ServerUtil.decryptPIN(pwd));
		}
		return ret;
	}

	public boolean isMasterUser(String userID, Connection con) throws SQLException {
		Boolean res = false;
		int flag = 0;
		String sql = "Select n8 from UVM005_A where t1=?";
		PreparedStatement stat = null;
		stat = con.prepareStatement(sql);
		stat.setString(1, userID);
		ResultSet result = stat.executeQuery();
		while (result.next()) {
			flag = result.getInt("n8");
		}
		if (flag == 1) {
			res = true;
		}
		return res;
	}

	public Result lockUnlockUser(String userid, long syskey, Connection conn, String status) throws SQLException {
		Result res = new Result();
		String sql = "";
		int restatus = 0;
		// int retryCount = 3;
		if (status.equalsIgnoreCase("Lock")) {
			restatus = 11;
			// retryCount = 3;
			res = checkLockDeactive(status, syskey, conn);
		} else if (status.equalsIgnoreCase("Unlock")) {
			restatus = 0;
			// retryCount = 0;
			res.setMsgCode("0000");
		}
		try {
			if (res.getMsgCode().equals("0000")) {
				sql = "UPDATE Register SET modifieddate = ?, n1=" + restatus + " WHERE Syskey=?";// t11=?
				PreparedStatement stmt = conn.prepareStatement(sql);
				int j = 1;
				stmt.setString(j++, GeneralUtil.datetoString());
				// stmt.setInt(j++, restatus);
				// stmt.setInt(j++, retryCount);
				stmt.setLong(j++, syskey);
				int rs = stmt.executeUpdate();
				if (rs > 0) {
					res.setState(true);// lock/unlock successfully
				} else {
					res.setState(false);

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return res;
	}

	public UserData readUserProfileDataBySyskey(long syskey, Connection conn) throws SQLException {
		UserData ret = new UserData();
		ArrayList<DBRecord> dbrs = DBMgr.getDBRecords(defineView(), "WHERE RecordStatus<>4 AND syskey=" + syskey, "",
				conn);
		if (dbrs.size() > 0)
			ret = getDBViewRecord(dbrs.get(0));

		return ret;

	}

	public Result resetPasswordById(SessionData udata, Connection conn) throws SQLException {
		Result res = new Result();
		String phoneNo = "";
		Long syskey = 0L;
		String name = "";
		String loginID = "";
		// boolean flag=false;
		// res = canUpdate("reset", udata.getLoginID(), conn);
		// if (res.getMsgCode().equals("0000")) {		
		// String query2 = "update UVM005_A SET t1=? WHERE recordStatus <> 4
		// and t1=? ";
		//String pwd = ServerUtil.encryptPIN(generatePassword(conn));
		String genpwd = generatePassword(conn);
		String encryptpwd = ServerUtil.encryptPIN(genpwd);
		String pwd = ServerUtil.hashPassword(genpwd);
		String query2 = "update Register SET t41=?  WHERE recordStatus <> 4 and userid=? ";
		PreparedStatement stmt2 = conn.prepareStatement(query2);
		int i = 1;
		stmt2.setString(i++, pwd);
		stmt2.setString(i++, udata.getLoginID());
		int count = stmt2.executeUpdate();
		if (count > 0) {
			// get phone no and name
			String query3 = "SELECT userid,t1,syskey,userName from Register WHERE RecordStatus <> 4 and userid =? ";
			PreparedStatement stmt3 = conn.prepareStatement(query3);
			stmt3.setString(1, udata.getLoginID());
			ResultSet rs = stmt3.executeQuery();
			while (rs.next()) {
				loginID = rs.getString("userid");
				phoneNo = rs.getString("t1");
				syskey = rs.getLong("syskey");
				name = rs.getString("userName");
			}
			// before sending sms, update flag
			String query4 = "update Register SET t11=? WHERE RecordStatus <> 4 and userid=? ";// before
																								// sms
			PreparedStatement stmt4 = conn.prepareStatement(query4);
			stmt4.setString(1, "3");
			stmt4.setString(2, loginID);
			int count1 = stmt4.executeUpdate();
			if (count1 > 0) {
				ArrayList<SMSSettingData> adata = new ArrayList<SMSSettingData>();
				SMSSettingDao sdao = new SMSSettingDao();
				ResponseData ret = new ResponseData();
				SMSParameter smsParam = new SMSParameter();
				SMSMgr smgr = new SMSMgr();
				adata = sdao.checkMessageSetting("2", "7", "", conn);
				smsParam.setPassword(ServerUtil.decryptPIN(encryptpwd));
				smsParam.setFromName(name);
				smsParam.setLoginID(loginID);
				ret = smgr.sendMessageService(adata, smsParam, phoneNo, "");// send SMS after sending sms,update flag-->>remove atn
				String query5 = "update Register SET t11=? WHERE RecordStatus <> 4 and t1=? ";
				PreparedStatement stmt5 = conn.prepareStatement(query5);
				if (ret.getCode().equals("0000")) { // send successfully
					stmt5.setString(1, "4");
				} else if (ret.getCode().equals("0014")) {// send failed or invalid ph:no case
					stmt5.setString(1, "5");
				}
				stmt5.setString(2, udata.getLoginID());
				int count4 = stmt5.executeUpdate();
				if (count4 > 0) {
					res = lockUnlockUser(udata.getUserID(), syskey, conn, "unLock");
					if (res.isState()) {
						res.setMsgDesc("Password reset successfully.");
					} else {
						res.setMsgDesc("Password reset fail.");
					}
				}
			} else {
				res.setState(false);
				res.setMsgDesc("Password reset fail.");
			}

		} else {
			res.setState(false);
			res.setMsgDesc("Password reset fail.");
		}
		// } else {
		// res.setState(false);
		// }
		return res;
	}
}
