package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.nirvasoft.cms.framework.Result;
import com.nirvasoft.rp.framework.Lov3;
import com.nirvasoft.rp.framework.Ref;
import com.nirvasoft.rp.shared.SessionData;

public class LOVAdmDao {
	public static String cTableName = "Channels";

	public Lov3 getAccountTransferTypes(Connection conn) throws SQLException {
		Lov3 response = null;

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			String sql = "SELECT Code, Description FROM LOVDetails WHERE HKey = (SELECT SysKey FROM LOVHeader WHERE Description = 'Transfer Type') ORDER BY SysKey;";
			preparedStatement = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			resultSet = preparedStatement.executeQuery();
			resultSet.last();
			int totalRow = resultSet.getRow();

			if (totalRow == 0) {
				response = new Lov3();
				response.setMsgCode("0014");
				response.setMsgDesc("There Is No Transfer Type At Database.");
				response.setRefAccountTransferType(null);
			}

			if (totalRow != 0) {
				resultSet.beforeFirst();

				Ref[] refArray = new Ref[totalRow];
				int i = 0;

				while (resultSet.next()) {
					Ref ref = new Ref();
					ref.setvalue(resultSet.getString("Code"));
					ref.setcaption(resultSet.getString("Description"));

					refArray[i++] = ref;
				}

				response = new Lov3();
				response.setMsgCode("0000");
				response.setMsgDesc("Select Successfully.");
				response.setRefAccountTransferType(refArray);
			}
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}

			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}

		return response;
	}

	public ArrayList<Result> getAllFeatures(Connection conn) throws SQLException {

		ArrayList<Result> datalist = new ArrayList<Result>();

		String sql = "select Code,Description from LOVDetails where hkey = (select syskey from LOVHeader where description = 'Feature') ORDER BY Syskey";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			Result combo = new Result();
			combo.setKeyst(res.getString("Code"));
			combo.setKeyString(res.getString("Description"));
			datalist.add(combo);
		}

		return datalist;
	}

	public Lov3 getChannelkey(SessionData data, Connection conn) throws SQLException {
		Lov3 response = null;

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String whereclause = "";
		try {
			whereclause = " where regionnumber='13000000'";

			String query = "SELECT ChannelSyskey,Channel FROM " + cTableName + whereclause + " ORDER BY autokey;";
			preparedStatement = conn.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			resultSet = preparedStatement.executeQuery();

			int totalRow = 0;
			resultSet.last();
			totalRow = resultSet.getRow();

			if (totalRow == 0) {
				response = new Lov3();
				response.setMsgCode("0014");
				response.setMsgDesc("There is no Channel");
				response.setRefchannelkey(null);
			}

			if (totalRow > 0) {
				resultSet.beforeFirst();

				Ref[] refArray = new Ref[totalRow];
				int i = 0;

				while (resultSet.next()) {
					Ref ref = new Ref();
					ref.setvalue(resultSet.getString("ChannelSyskey"));
					ref.setcaption(resultSet.getString("Channel"));
					refArray[i++] = ref;
				}

				response = new Lov3();
				response.setMsgCode("0000");
				response.setMsgDesc("Select successfully.");
				response.setRefchannelkey(refArray);
			}
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}

			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}

		return response;
	}

	public ArrayList<Result> getCodeFromLovdeatils(String merchantID, Connection conn) throws SQLException {

		ArrayList<Result> datalist = new ArrayList<Result>();
		String sql = "Select code,Description from lovdetails  Where hkey in (select code from lovdetails where hkey = ( select syskey from LOVHeader where Description like '%Advanced Commission%') And Description In (Select ProcessingCode From Paytemplateheader Where MerchantId  = ?) Group By Code ,Description )";

		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, merchantID);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			Result combo = new Result();
			combo.setMsgCode(res.getString("code"));
			combo.setMsgDesc(res.getString("description"));
			datalist.add(combo);
		}
		return datalist;
	}
	// SMS setting End

	public Lov3 getDomainType(Connection conn) throws SQLException {
		Lov3 response = null;

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			String query = "SELECT Code, Description, Price FROM LOVDetails WHERE HKey = (SELECT SysKey FROM LOVHeader WHERE Description = 'DomainType') ORDER BY SysKey;";
			preparedStatement = conn.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			resultSet = preparedStatement.executeQuery();

			int totalRow = 0;
			resultSet.last();
			totalRow = resultSet.getRow();

			if (totalRow == 0) {
				response = new Lov3();
				response.setMsgCode("0014");
				response.setMsgDesc("There is no domain name at database.");
				response.setRefFileType(null);
			}

			if (totalRow > 0) {
				resultSet.beforeFirst();

				Ref[] refArray = new Ref[totalRow];
				int i = 0;

				while (resultSet.next()) {
					Ref ref = new Ref();
					ref.setvalue(resultSet.getString("Code"));
					ref.setcaption(resultSet.getString("Description"));
					ref.setProcessingCode(resultSet.getString("Price"));
					refArray[i++] = ref;
				}

				response = new Lov3();
				response.setMsgCode("0000");
				response.setMsgDesc("Select successfully.");
				response.setDomainType(refArray);
			}
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}

			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}

		return response;
	}

	public Lov3 getFileType(Connection conn) throws SQLException {
		Lov3 response = null;

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			String query = "SELECT Code, Description, Price FROM LOVDetails WHERE HKey = (SELECT SysKey FROM LOVHeader WHERE Description = 'FileType') ORDER BY SysKey;";
			preparedStatement = conn.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			resultSet = preparedStatement.executeQuery();

			int totalRow = 0;
			resultSet.last();
			totalRow = resultSet.getRow();

			if (totalRow == 0) {
				response = new Lov3();
				response.setMsgCode("0014");
				response.setMsgDesc("There is no ticket status at database.");
				response.setRefFileType(null);
			}

			if (totalRow > 0) {
				resultSet.beforeFirst();

				Ref[] refArray = new Ref[totalRow];
				int i = 0;

				while (resultSet.next()) {
					Ref ref = new Ref();
					ref.setvalue(resultSet.getString("Code"));
					ref.setcaption(resultSet.getString("Description"));
					ref.setProcessingCode(resultSet.getString("Price"));
					refArray[i++] = ref;
				}

				response = new Lov3();
				response.setMsgCode("0000");
				response.setMsgDesc("Select successfully.");
				response.setRefFileType(refArray);
			}
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}

			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}

		return response;
	}

	// SMS setting start from paytemplateHeader
	/*public Lov3 getMerchantIDListDetail(Connection conn, String User) throws SQLException {

		Lov3 lov3 = new Lov3();
		Ref[] refarray = null;
		ArrayList<Ref> reflist = new ArrayList<Ref>();
		String sql = "";
		sql = "select p.MerchantID,p.MerchantName,p.ProcessingCode from CMSMerchant c, PayTemplateHeader p where c.UserId = p.MerchantID and c.RecordStatus <> 4 order by p.MerchantName";
		
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();		
		while (rs.next()) {
			Ref ref = new Ref();
			ref.setvalue(rs.getString("MerchantID"));
			ref.setcaption(rs.getString("MerchantName"));
			ref.setProcessingCode(rs.getString("ProcessingCode"));
			reflist.add(ref);
		}

		stmt.close();
		rs.close();

		refarray = new Ref[reflist.size()];
		for (int i = 0; i < reflist.size(); i++) {
			refarray[i] = reflist.get(i);
		}
		System.out.println("Dao getmerchant" + refarray.length);
		lov3.setRef015(refarray);
		System.out.println("Dao getmerchant lov" + lov3.getRef015().length);

		return lov3;

	}*/
	
	// SMS setting start from CMSMerchant
		public Lov3 getMerchantIDListDetail(Connection conn, String User) throws SQLException {

			Lov3 lov3 = new Lov3();
			Ref[] refarray = null;
			ArrayList<Ref> reflist = new ArrayList<Ref>();
			String sql = "";
			//sql = "select p.MerchantID,p.MerchantName,p.ProcessingCode from CMSMerchant c, PayTemplateHeader p where c.UserId = p.MerchantID and c.RecordStatus <> 4 order by p.MerchantName";
			sql = "select userid,UserName from CMSMerchant where RecordStatus <> 4 order by UserName";
			PreparedStatement stmt = conn.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();		
			
			while (rs.next()) {
				Ref ref = new Ref();
				ref.setvalue(rs.getString("userid"));
				ref.setcaption(rs.getString("UserName"));
				reflist.add(ref);
			}

			stmt.close();
			rs.close();

			refarray = new Ref[reflist.size()];
			for (int i = 0; i < reflist.size(); i++) {
				refarray[i] = reflist.get(i);
			}
			System.out.println("Dao getmerchant" + refarray.length);
			lov3.setRef015(refarray);
			System.out.println("Dao getmerchant lov" + lov3.getRef015().length);

			return lov3;

		}

	public ArrayList<Result> getOrderList(Connection conn) throws SQLException {

		ArrayList<Result> datalist = new ArrayList<Result>();

		String sql = "select Code,Description from LOVDetails where hkey = (select syskey from LOVHeader where description = 'Order') ORDER BY Syskey";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			Result combo = new Result();
			combo.setKeyst(res.getString("Code"));
			combo.setKeyString(res.getString("Description"));
			datalist.add(combo);
		}

		return datalist;

	}

	public Lov3 getPOCUserType(Connection conn) throws SQLException {
		Lov3 response = null;

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			String query = "SELECT Code, Description, Price FROM LOVDetails WHERE HKey = (SELECT SysKey FROM LOVHeader WHERE Description = 'POCUserType') ORDER BY SysKey;";
			preparedStatement = conn.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			resultSet = preparedStatement.executeQuery();

			int totalRow = 0;
			resultSet.last();
			totalRow = resultSet.getRow();

			if (totalRow == 0) {
				response = new Lov3();
				response.setMsgCode("0014");
				response.setMsgDesc("There is no ticket status at database.");
				response.setRefPOCUserType(null);
			}

			if (totalRow > 0) {
				resultSet.beforeFirst();

				Ref[] refArray = new Ref[totalRow];
				int i = 0;

				while (resultSet.next()) {
					Ref ref = new Ref();
					ref.setvalue(resultSet.getString("Code"));
					ref.setcaption(resultSet.getString("Description"));
					ref.setProcessingCode(resultSet.getString("Price"));
					refArray[i++] = ref;
				}

				response = new Lov3();
				response.setMsgCode("0000");
				response.setMsgDesc("Select successfully.");
				response.setRefPOCUserType(refArray);
			}
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}

			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}

		return response;
	}
	public Lov3 getLovType(String lovDesc,Connection conn) throws SQLException {
		Lov3 response = null;

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			String query = "SELECT Code, Description, Price FROM LOVDetails WHERE HKey = (SELECT SysKey FROM LOVHeader WHERE Description = ?) ORDER BY SysKey;";
			preparedStatement = conn.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);			
			preparedStatement.setString(1,lovDesc);
			resultSet = preparedStatement.executeQuery();
			int totalRow = 0;
			resultSet.last();
			totalRow = resultSet.getRow();

			if (totalRow == 0) {
				response = new Lov3();
				response.setMsgCode("0014");
				response.setMsgDesc("There is no domain name at database.");
				response.setRefFileType(null);
			}

			if (totalRow > 0) {
				resultSet.beforeFirst();

				Ref[] refArray = new Ref[totalRow];
				int i = 0;

				while (resultSet.next()) {
					Ref ref = new Ref();
					ref.setvalue(resultSet.getString("Code"));
					ref.setcaption(resultSet.getString("Description"));
					ref.setProcessingCode(resultSet.getString("Price"));
					refArray[i++] = ref;
				}

				response = new Lov3();
				response.setMsgCode("0000");
				response.setMsgDesc("Select successfully.");
				response.setLovType(refArray);
			}
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}

			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}

		return response;
	}
}
