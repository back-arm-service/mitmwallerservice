package com.nirvasoft.rp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.nirvasoft.rp.shared.TicketData;

public class TicketDao {

	public String getChannelNameByID(String id, Connection conn) throws SQLException {
		String ret = "";
		String sql = "select channel from channels where ChannelSysKey  = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, id);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ret = rs.getString(1);
		}
		return ret;
	}

	public TicketData insertTicket(TicketData request, Connection conn) throws SQLException {
		TicketData response = new TicketData();

		int result = 0;

		String query = "INSERT INTO TicketDetail(syskey, createddate, modifiedDate, userid, "
				+ "date, time, shootdate, shoottime, " + "t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, "
				+ "t11, t12, t13, t14, t15, t16, t17, t18, t19, t20, " + "n1, n2, n3, n4, n5, n6, n7, n8, n9, n10) "
				+ "VALUES (?, ?, ?, ?, " + "?, ?, ?, ?, " + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "
				+ "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
		PreparedStatement ps = conn.prepareStatement(query);

		int i = 1;
		ps.setLong(i++, 0);
		ps.setString(i++, request.getCreateddate());
		ps.setString(i++, request.getModifieddate());
		ps.setString(i++, request.getUserid());
		ps.setString(i++, request.getDate());
		ps.setString(i++, request.getTime());
		ps.setString(i++, request.getShootdate());
		ps.setString(i++, request.getShoottime());
		ps.setString(i++, request.getT1());
		ps.setString(i++, request.getT2());
		ps.setString(i++, request.getT3());
		ps.setString(i++, request.getT4());
		ps.setString(i++, request.getT5());
		ps.setString(i++, request.getT6());
		ps.setString(i++, request.getT7());
		ps.setString(i++, request.getT8());
		ps.setString(i++, request.getT9());
		ps.setString(i++, request.getT10());
		ps.setString(i++, request.getT11());
		ps.setString(i++, request.getT12());
		ps.setString(i++, request.getT13());
		ps.setString(i++, request.getT14());
		ps.setString(i++, request.getT15());
		ps.setString(i++, request.getT16());
		ps.setString(i++, request.getT17());
		ps.setString(i++, request.getT18());
		ps.setString(i++, request.getT19());
		ps.setString(i++, request.getT20());
		ps.setLong(i++, Long.parseLong(request.getN1()));
		ps.setInt(i++, Integer.parseInt(request.getN2()));
		ps.setLong(i++, Long.parseLong(request.getN3()));
		ps.setLong(i++, Long.parseLong(request.getN4()));
		ps.setLong(i++, Long.parseLong(request.getN5()));
		ps.setLong(i++, Long.parseLong(request.getN6()));
		ps.setLong(i++, Long.parseLong(request.getN7()));
		ps.setLong(i++, Long.parseLong(request.getN8()));
		ps.setLong(i++, Long.parseLong(request.getN9()));
		ps.setLong(i++, Long.parseLong(request.getN10()));

		result = ps.executeUpdate();

		if (result > 0) {
			response = request;
			response.setMessageCode("0000");
			response.setMessageDesc("Saved successfully.");
		} else {
			response.setMessageCode("0014");
			response.setMessageDesc("Save failed.");
		}

		ps.close();

		return response;
	}

}
