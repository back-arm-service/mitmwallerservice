/**
 * MessagingGateway.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.gateway.messaging.webservice;

public interface MessagingGateway extends java.rmi.Remote {
	public com.nirvasoft.gateway.messaging.webservice.Response saveTemplate(int TEMPLATEKEY,
			java.lang.String TEMPLATETYPE, int PRIORITY, java.lang.String USERNAME, java.lang.String PASSWORD,
			java.lang.String KEY, java.lang.String SUBJECT, java.lang.String SERVICECODE, java.lang.String DESIREDDATE,
			java.lang.String DESIREDTIME, int CHANNELNO, java.lang.String BRANCHCODE, int RETRIES,
			java.lang.String RETRIESINTERVAL, java.lang.String ATTACHMENTNAME, byte[] ATTACHMENT)
			throws java.rmi.RemoteException, com.nirvasoft.gateway.messaging.webservice.MessagingFault;

	public com.nirvasoft.gateway.messaging.webservice.Response saveTemplateIndividual(int TEMPLATEKEY,
			java.lang.String TEMPLATETYPE, java.lang.String ADDRESS, java.lang.String MESSAGE, int PRIORITY,
			java.lang.String USERNAME, java.lang.String PASSWORD, java.lang.String KEY, java.lang.String REFERENCEID,
			java.lang.String SUBJECT, java.lang.String CC, java.lang.String BCC, java.lang.String SERVICECODE,
			java.lang.String DESIREDDATE, java.lang.String DESIREDTIME, int CHANNELNO, java.lang.String BRANCHCODE,
			int RETRIES, java.lang.String RETRIESINTERVAL)
			throws java.rmi.RemoteException, com.nirvasoft.gateway.messaging.webservice.MessagingFault;

	public com.nirvasoft.gateway.messaging.webservice.Response sendEmail(java.lang.String EMAIL,
			java.lang.String MESSAGE, int PRIORITY, java.lang.String USERNAME, java.lang.String PASSWORD,
			java.lang.String KEY, java.lang.String SUBJECT, java.lang.String CC, java.lang.String BCC,
			java.lang.String SERVICECODE, java.lang.String DESIREDDATE, java.lang.String DESIREDTIME, int CHANNELNO,
			java.lang.String BRANCHCODE, int RETRIES, java.lang.String RETRIESINTERVAL)
			throws java.rmi.RemoteException, com.nirvasoft.gateway.messaging.webservice.MessagingFault;

	public com.nirvasoft.gateway.messaging.webservice.Response sendEmailAttachment(java.lang.String EMAIL,
			java.lang.String MESSAGE, int PRIORITY, java.lang.String USERNAME, java.lang.String PASSWORD,
			java.lang.String KEY, java.lang.String SUBJECT, java.lang.String CC, java.lang.String BCC,
			java.lang.String SERVICECODE, java.lang.String DESIREDDATE, java.lang.String DESIREDTIME, int CHANNELNO,
			java.lang.String BRANCHCODE, int RETRIES, java.lang.String RETRIESINTERVAL, java.lang.String ATTACHMENTNAME,
			byte[] ATTACHMENT)
			throws java.rmi.RemoteException, com.nirvasoft.gateway.messaging.webservice.MessagingFault;

	public com.nirvasoft.gateway.messaging.webservice.Response sendSMS(java.lang.String PHONENO,
			java.lang.String MESSAGE, int PRIORITY, java.lang.String USERNAME, java.lang.String PASSWORD,
			java.lang.String KEY, java.lang.String SERVICECODE, java.lang.String DESIREDDATE,
			java.lang.String DESIREDTIME, int CHANNELNO, java.lang.String BRANCHCODE, int RETRIES,
			java.lang.String RETRIESINTERVAL)
			throws java.rmi.RemoteException, com.nirvasoft.gateway.messaging.webservice.MessagingFault;
}
