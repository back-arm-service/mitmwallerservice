/**
 * MessagingGatewayImplService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.gateway.messaging.webservice;

public interface MessagingGatewayImplService extends javax.xml.rpc.Service {
	public com.nirvasoft.gateway.messaging.webservice.MessagingGateway getMessagingGatewayImplPort()
			throws javax.xml.rpc.ServiceException;

	public com.nirvasoft.gateway.messaging.webservice.MessagingGateway getMessagingGatewayImplPort(
			java.net.URL portAddress) throws javax.xml.rpc.ServiceException;

	public java.lang.String getMessagingGatewayImplPortAddress();
}
