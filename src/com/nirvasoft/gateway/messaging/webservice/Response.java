/**
 * Response.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.gateway.messaging.webservice;

public class Response implements java.io.Serializable {
	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			Response.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://webservice.messaging.gateway.nirvasoft.com/", "response"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ERRORCODE");
		elemField.setXmlName(new javax.xml.namespace.QName("", "ERRORCODE"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ERRORDESC");
		elemField.setXmlName(new javax.xml.namespace.QName("", "ERRORDESC"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("RESULT");
		elemField.setXmlName(new javax.xml.namespace.QName("", "RESULT"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("KEY");
		elemField.setXmlName(new javax.xml.namespace.QName("", "KEY"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("REFERENCEID");
		elemField.setXmlName(new javax.xml.namespace.QName("", "REFERENCEID"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	private java.lang.String ERRORCODE;

	private java.lang.String ERRORDESC;

	private java.lang.String RESULT;

	private java.lang.String KEY;

	private java.lang.String REFERENCEID;

	private java.lang.Object __equalsCalc = null;

	private boolean __hashCodeCalc = false;

	public Response() {
	}

	public Response(java.lang.String ERRORCODE, java.lang.String ERRORDESC, java.lang.String RESULT,
			java.lang.String KEY, java.lang.String REFERENCEID) {
		this.ERRORCODE = ERRORCODE;
		this.ERRORDESC = ERRORDESC;
		this.RESULT = RESULT;
		this.KEY = KEY;
		this.REFERENCEID = REFERENCEID;
	}

	@Override
	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Response))
			return false;
		Response other = (Response) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.ERRORCODE == null && other.getERRORCODE() == null)
						|| (this.ERRORCODE != null && this.ERRORCODE.equals(other.getERRORCODE())))
				&& ((this.ERRORDESC == null && other.getERRORDESC() == null)
						|| (this.ERRORDESC != null && this.ERRORDESC.equals(other.getERRORDESC())))
				&& ((this.RESULT == null && other.getRESULT() == null)
						|| (this.RESULT != null && this.RESULT.equals(other.getRESULT())))
				&& ((this.KEY == null && other.getKEY() == null)
						|| (this.KEY != null && this.KEY.equals(other.getKEY())))
				&& ((this.REFERENCEID == null && other.getREFERENCEID() == null)
						|| (this.REFERENCEID != null && this.REFERENCEID.equals(other.getREFERENCEID())));
		__equalsCalc = null;
		return _equals;
	}

	/**
	 * Gets the ERRORCODE value for this Response.
	 * 
	 * @return ERRORCODE
	 */
	public java.lang.String getERRORCODE() {
		return ERRORCODE;
	}

	/**
	 * Gets the ERRORDESC value for this Response.
	 * 
	 * @return ERRORDESC
	 */
	public java.lang.String getERRORDESC() {
		return ERRORDESC;
	}

	/**
	 * Gets the KEY value for this Response.
	 * 
	 * @return KEY
	 */
	public java.lang.String getKEY() {
		return KEY;
	}

	/**
	 * Gets the REFERENCEID value for this Response.
	 * 
	 * @return REFERENCEID
	 */
	public java.lang.String getREFERENCEID() {
		return REFERENCEID;
	}

	/**
	 * Gets the RESULT value for this Response.
	 * 
	 * @return RESULT
	 */
	public java.lang.String getRESULT() {
		return RESULT;
	}

	@Override
	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getERRORCODE() != null) {
			_hashCode += getERRORCODE().hashCode();
		}
		if (getERRORDESC() != null) {
			_hashCode += getERRORDESC().hashCode();
		}
		if (getRESULT() != null) {
			_hashCode += getRESULT().hashCode();
		}
		if (getKEY() != null) {
			_hashCode += getKEY().hashCode();
		}
		if (getREFERENCEID() != null) {
			_hashCode += getREFERENCEID().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	/**
	 * Sets the ERRORCODE value for this Response.
	 * 
	 * @param ERRORCODE
	 */
	public void setERRORCODE(java.lang.String ERRORCODE) {
		this.ERRORCODE = ERRORCODE;
	}

	/**
	 * Sets the ERRORDESC value for this Response.
	 * 
	 * @param ERRORDESC
	 */
	public void setERRORDESC(java.lang.String ERRORDESC) {
		this.ERRORDESC = ERRORDESC;
	}

	/**
	 * Sets the KEY value for this Response.
	 * 
	 * @param KEY
	 */
	public void setKEY(java.lang.String KEY) {
		this.KEY = KEY;
	}

	/**
	 * Sets the REFERENCEID value for this Response.
	 * 
	 * @param REFERENCEID
	 */
	public void setREFERENCEID(java.lang.String REFERENCEID) {
		this.REFERENCEID = REFERENCEID;
	}

	/**
	 * Sets the RESULT value for this Response.
	 * 
	 * @param RESULT
	 */
	public void setRESULT(java.lang.String RESULT) {
		this.RESULT = RESULT;
	}

}
