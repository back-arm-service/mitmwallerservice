/**
 * MessagingFault.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.gateway.messaging.webservice;

public class MessagingFault extends org.apache.axis.AxisFault implements java.io.Serializable {
	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			MessagingFault.class, true);

	static {
		typeDesc.setXmlType(
				new javax.xml.namespace.QName("http://webservice.messaging.gateway.nirvasoft.com/", "messagingFault"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("faultCode1");
		elemField.setXmlName(new javax.xml.namespace.QName("", "faultCode"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("faultString1");
		elemField.setXmlName(new javax.xml.namespace.QName("", "faultString"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	private java.lang.String faultCode1;

	private java.lang.String faultString1;

	private java.lang.Object __equalsCalc = null;

	private boolean __hashCodeCalc = false;

	public MessagingFault() {
	}

	public MessagingFault(java.lang.String faultCode1, java.lang.String faultString1) {
		this.faultCode1 = faultCode1;
		this.faultString1 = faultString1;
	}

	@Override
	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof MessagingFault))
			return false;
		MessagingFault other = (MessagingFault) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.faultCode1 == null && other.getFaultCode1() == null)
						|| (this.faultCode1 != null && this.faultCode1.equals(other.getFaultCode1())))
				&& ((this.faultString1 == null && other.getFaultString1() == null)
						|| (this.faultString1 != null && this.faultString1.equals(other.getFaultString1())));
		__equalsCalc = null;
		return _equals;
	}

	/**
	 * Gets the faultCode1 value for this MessagingFault.
	 * 
	 * @return faultCode1
	 */
	public java.lang.String getFaultCode1() {
		return faultCode1;
	}

	/**
	 * Gets the faultString1 value for this MessagingFault.
	 * 
	 * @return faultString1
	 */
	public java.lang.String getFaultString1() {
		return faultString1;
	}

	@Override
	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getFaultCode1() != null) {
			_hashCode += getFaultCode1().hashCode();
		}
		if (getFaultString1() != null) {
			_hashCode += getFaultString1().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	/**
	 * Sets the faultCode1 value for this MessagingFault.
	 * 
	 * @param faultCode1
	 */
	public void setFaultCode1(java.lang.String faultCode1) {
		this.faultCode1 = faultCode1;
	}

	/**
	 * Sets the faultString1 value for this MessagingFault.
	 * 
	 * @param faultString1
	 */
	public void setFaultString1(java.lang.String faultString1) {
		this.faultString1 = faultString1;
	}

	/**
	 * Writes the exception data to the faultDetails
	 */
	@Override
	public void writeDetails(javax.xml.namespace.QName qname, org.apache.axis.encoding.SerializationContext context)
			throws java.io.IOException {
		context.serialize(qname, null, this);
	}
}
