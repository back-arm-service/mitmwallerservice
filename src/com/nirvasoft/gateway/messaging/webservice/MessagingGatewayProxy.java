package com.nirvasoft.gateway.messaging.webservice;

public class MessagingGatewayProxy implements com.nirvasoft.gateway.messaging.webservice.MessagingGateway {
	private String _endpoint = null;
	private com.nirvasoft.gateway.messaging.webservice.MessagingGateway messagingGateway = null;

	public MessagingGatewayProxy() {
		_initMessagingGatewayProxy();
	}

	public MessagingGatewayProxy(String endpoint) {
		_endpoint = endpoint;
		_initMessagingGatewayProxy();
	}

	private void _initMessagingGatewayProxy() {
		try {
			messagingGateway = (new com.nirvasoft.gateway.messaging.webservice.MessagingGatewayImplServiceLocator())
					.getMessagingGatewayImplPort();
			if (messagingGateway != null) {
				if (_endpoint != null)
					((javax.xml.rpc.Stub) messagingGateway)._setProperty("javax.xml.rpc.service.endpoint.address",
							_endpoint);
				else
					_endpoint = (String) ((javax.xml.rpc.Stub) messagingGateway)
							._getProperty("javax.xml.rpc.service.endpoint.address");
			}

		} catch (javax.xml.rpc.ServiceException serviceException) {
		}
	}

	public String getEndpoint() {
		return _endpoint;
	}

	public com.nirvasoft.gateway.messaging.webservice.MessagingGateway getMessagingGateway() {
		if (messagingGateway == null)
			_initMessagingGatewayProxy();
		return messagingGateway;
	}

	@Override
	public com.nirvasoft.gateway.messaging.webservice.Response saveTemplate(int TEMPLATEKEY,
			java.lang.String TEMPLATETYPE, int PRIORITY, java.lang.String USERNAME, java.lang.String PASSWORD,
			java.lang.String KEY, java.lang.String SUBJECT, java.lang.String SERVICECODE, java.lang.String DESIREDDATE,
			java.lang.String DESIREDTIME, int CHANNELNO, java.lang.String BRANCHCODE, int RETRIES,
			java.lang.String RETRIESINTERVAL, java.lang.String ATTACHMENTNAME, byte[] ATTACHMENT)
			throws java.rmi.RemoteException, com.nirvasoft.gateway.messaging.webservice.MessagingFault {
		if (messagingGateway == null)
			_initMessagingGatewayProxy();
		return messagingGateway.saveTemplate(TEMPLATEKEY, TEMPLATETYPE, PRIORITY, USERNAME, PASSWORD, KEY, SUBJECT,
				SERVICECODE, DESIREDDATE, DESIREDTIME, CHANNELNO, BRANCHCODE, RETRIES, RETRIESINTERVAL, ATTACHMENTNAME,
				ATTACHMENT);
	}

	@Override
	public com.nirvasoft.gateway.messaging.webservice.Response saveTemplateIndividual(int TEMPLATEKEY,
			java.lang.String TEMPLATETYPE, java.lang.String ADDRESS, java.lang.String MESSAGE, int PRIORITY,
			java.lang.String USERNAME, java.lang.String PASSWORD, java.lang.String KEY, java.lang.String REFERENCEID,
			java.lang.String SUBJECT, java.lang.String CC, java.lang.String BCC, java.lang.String SERVICECODE,
			java.lang.String DESIREDDATE, java.lang.String DESIREDTIME, int CHANNELNO, java.lang.String BRANCHCODE,
			int RETRIES, java.lang.String RETRIESINTERVAL)
			throws java.rmi.RemoteException, com.nirvasoft.gateway.messaging.webservice.MessagingFault {
		if (messagingGateway == null)
			_initMessagingGatewayProxy();
		return messagingGateway.saveTemplateIndividual(TEMPLATEKEY, TEMPLATETYPE, ADDRESS, MESSAGE, PRIORITY, USERNAME,
				PASSWORD, KEY, REFERENCEID, SUBJECT, CC, BCC, SERVICECODE, DESIREDDATE, DESIREDTIME, CHANNELNO,
				BRANCHCODE, RETRIES, RETRIESINTERVAL);
	}

	@Override
	public com.nirvasoft.gateway.messaging.webservice.Response sendEmail(java.lang.String EMAIL,
			java.lang.String MESSAGE, int PRIORITY, java.lang.String USERNAME, java.lang.String PASSWORD,
			java.lang.String KEY, java.lang.String SUBJECT, java.lang.String CC, java.lang.String BCC,
			java.lang.String SERVICECODE, java.lang.String DESIREDDATE, java.lang.String DESIREDTIME, int CHANNELNO,
			java.lang.String BRANCHCODE, int RETRIES, java.lang.String RETRIESINTERVAL)
			throws java.rmi.RemoteException, com.nirvasoft.gateway.messaging.webservice.MessagingFault {
		if (messagingGateway == null)
			_initMessagingGatewayProxy();
		return messagingGateway.sendEmail(EMAIL, MESSAGE, PRIORITY, USERNAME, PASSWORD, KEY, SUBJECT, CC, BCC,
				SERVICECODE, DESIREDDATE, DESIREDTIME, CHANNELNO, BRANCHCODE, RETRIES, RETRIESINTERVAL);
	}

	@Override
	public com.nirvasoft.gateway.messaging.webservice.Response sendEmailAttachment(java.lang.String EMAIL,
			java.lang.String MESSAGE, int PRIORITY, java.lang.String USERNAME, java.lang.String PASSWORD,
			java.lang.String KEY, java.lang.String SUBJECT, java.lang.String CC, java.lang.String BCC,
			java.lang.String SERVICECODE, java.lang.String DESIREDDATE, java.lang.String DESIREDTIME, int CHANNELNO,
			java.lang.String BRANCHCODE, int RETRIES, java.lang.String RETRIESINTERVAL, java.lang.String ATTACHMENTNAME,
			byte[] ATTACHMENT)
			throws java.rmi.RemoteException, com.nirvasoft.gateway.messaging.webservice.MessagingFault {
		if (messagingGateway == null)
			_initMessagingGatewayProxy();
		return messagingGateway.sendEmailAttachment(EMAIL, MESSAGE, PRIORITY, USERNAME, PASSWORD, KEY, SUBJECT, CC, BCC,
				SERVICECODE, DESIREDDATE, DESIREDTIME, CHANNELNO, BRANCHCODE, RETRIES, RETRIESINTERVAL, ATTACHMENTNAME,
				ATTACHMENT);
	}

	@Override
	public com.nirvasoft.gateway.messaging.webservice.Response sendSMS(java.lang.String PHONENO,
			java.lang.String MESSAGE, int PRIORITY, java.lang.String USERNAME, java.lang.String PASSWORD,
			java.lang.String KEY, java.lang.String SERVICECODE, java.lang.String DESIREDDATE,
			java.lang.String DESIREDTIME, int CHANNELNO, java.lang.String BRANCHCODE, int RETRIES,
			java.lang.String RETRIESINTERVAL)
			throws java.rmi.RemoteException, com.nirvasoft.gateway.messaging.webservice.MessagingFault {
		if (messagingGateway == null)
			_initMessagingGatewayProxy();
		return messagingGateway.sendSMS(PHONENO, MESSAGE, PRIORITY, USERNAME, PASSWORD, KEY, SERVICECODE, DESIREDDATE,
				DESIREDTIME, CHANNELNO, BRANCHCODE, RETRIES, RETRIESINTERVAL);
	}

	public void setEndpoint(String endpoint) {
		_endpoint = endpoint;
		if (messagingGateway != null)
			((javax.xml.rpc.Stub) messagingGateway)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);

	}

}