/**
 * MessagingGatewayImplServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.nirvasoft.gateway.messaging.webservice;

public class MessagingGatewayImplServiceLocator extends org.apache.axis.client.Service
		implements com.nirvasoft.gateway.messaging.webservice.MessagingGatewayImplService {

	// Use to get a proxy class for MessagingGatewayImplPort
	private java.lang.String MessagingGatewayImplPort_address = "http://192.168.205.201:8080/MessagingGateway/msggateway";

	// The WSDD service name defaults to the port name.
	private java.lang.String MessagingGatewayImplPortWSDDServiceName = "MessagingGatewayImplPort";

	private java.util.HashSet ports = null;

	public MessagingGatewayImplServiceLocator() {
	}

	public MessagingGatewayImplServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName)
			throws javax.xml.rpc.ServiceException {
		super(wsdlLoc, sName);
	}

	public MessagingGatewayImplServiceLocator(org.apache.axis.EngineConfiguration config) {
		super(config);
	}

	@Override
	public com.nirvasoft.gateway.messaging.webservice.MessagingGateway getMessagingGatewayImplPort()
			throws javax.xml.rpc.ServiceException {
		java.net.URL endpoint;
		try {
			endpoint = new java.net.URL(MessagingGatewayImplPort_address);
		} catch (java.net.MalformedURLException e) {
			throw new javax.xml.rpc.ServiceException(e);
		}
		return getMessagingGatewayImplPort(endpoint);
	}

	@Override
	public com.nirvasoft.gateway.messaging.webservice.MessagingGateway getMessagingGatewayImplPort(
			java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
		try {
			com.nirvasoft.gateway.messaging.webservice.MessagingGatewayImplPortBindingStub _stub = new com.nirvasoft.gateway.messaging.webservice.MessagingGatewayImplPortBindingStub(
					portAddress, this);
			_stub.setPortName(getMessagingGatewayImplPortWSDDServiceName());
			return _stub;
		} catch (org.apache.axis.AxisFault e) {
			return null;
		}
	}

	@Override
	public java.lang.String getMessagingGatewayImplPortAddress() {
		return MessagingGatewayImplPort_address;
	}

	public java.lang.String getMessagingGatewayImplPortWSDDServiceName() {
		return MessagingGatewayImplPortWSDDServiceName;
	}

	/**
	 * For the given interface, get the stub implementation. If this service has
	 * no port for the given interface, then ServiceException is thrown.
	 */
	@Override
	public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
		try {
			if (com.nirvasoft.gateway.messaging.webservice.MessagingGateway.class
					.isAssignableFrom(serviceEndpointInterface)) {
				com.nirvasoft.gateway.messaging.webservice.MessagingGatewayImplPortBindingStub _stub = new com.nirvasoft.gateway.messaging.webservice.MessagingGatewayImplPortBindingStub(
						new java.net.URL(MessagingGatewayImplPort_address), this);
				_stub.setPortName(getMessagingGatewayImplPortWSDDServiceName());
				return _stub;
			}
		} catch (java.lang.Throwable t) {
			throw new javax.xml.rpc.ServiceException(t);
		}
		throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  "
				+ (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
	}

	/**
	 * For the given interface, get the stub implementation. If this service has
	 * no port for the given interface, then ServiceException is thrown.
	 */
	@Override
	public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface)
			throws javax.xml.rpc.ServiceException {
		if (portName == null) {
			return getPort(serviceEndpointInterface);
		}
		java.lang.String inputPortName = portName.getLocalPart();
		if ("MessagingGatewayImplPort".equals(inputPortName)) {
			return getMessagingGatewayImplPort();
		} else {
			java.rmi.Remote _stub = getPort(serviceEndpointInterface);
			((org.apache.axis.client.Stub) _stub).setPortName(portName);
			return _stub;
		}
	}

	@Override
	public java.util.Iterator getPorts() {
		if (ports == null) {
			ports = new java.util.HashSet();
			ports.add(new javax.xml.namespace.QName("http://webservice.messaging.gateway.nirvasoft.com/",
					"MessagingGatewayImplPort"));
		}
		return ports.iterator();
	}

	@Override
	public javax.xml.namespace.QName getServiceName() {
		return new javax.xml.namespace.QName("http://webservice.messaging.gateway.nirvasoft.com/",
				"MessagingGatewayImplService");
	}

	/**
	 * Set the endpoint address for the specified port name.
	 */
	public void setEndpointAddress(java.lang.String portName, java.lang.String address)
			throws javax.xml.rpc.ServiceException {

		if ("MessagingGatewayImplPort".equals(portName)) {
			setMessagingGatewayImplPortEndpointAddress(address);
		} else { // Unknown Port Name
			throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
		}
	}

	/**
	 * Set the endpoint address for the specified port name.
	 */
	public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address)
			throws javax.xml.rpc.ServiceException {
		setEndpointAddress(portName.getLocalPart(), address);
	}

	public void setMessagingGatewayImplPortEndpointAddress(java.lang.String address) {
		MessagingGatewayImplPort_address = address;
	}

	public void setMessagingGatewayImplPortWSDDServiceName(java.lang.String name) {
		MessagingGatewayImplPortWSDDServiceName = name;
	}

}
