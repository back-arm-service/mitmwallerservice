import java.util.Properties;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Hex;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.InternetAddress;
import javax.mail.Message;
import javax.mail.Transport;
import javax.mail.MessagingException;
public class HashValueSample {

	public static void main(String[] args)throws Exception {
		String token = "8OE8pxEeMaw5zaZmj-dt8yDG7C_3m1jEY2QO1teagY83IeDrYCTedA2DDkSqa8022bnuhiHbfmt3cynfaMkpQc6Rvrgld3qRecco7JXEKpIpEszP8e6zMYeCwqGERYFZKqfju49Ea2RAy4o__TVYxaxuIwwKKd7FeLLUbaO5QGnuXq1XIkpigJuk69-_8lfcRAzzoA";
		String senderCode = "10042";
		String partnerCode = "A00000480";
		String amount = "000000100000";
		String refNo = "M0023456688";
		String secretkey = "9489b3eee4eccf317";
		//String enquiry = new HashValueSample().enquiry(token, senderCode, amount, partnerCode, secretkey);
		//String confirm = new HashValueSample().confirm(token, partnerCode, senderCode, refNo, amount, secretkey);
		//System.out.println("Enquiry HashValue " + enquiry);
		//System.out.println("Confirm HashValue " + confirm);
		String result = new HashValueSample().sendMail("april.tn@mit.com.mm","testing.......");
		System.out.println("Send Email : " + result);

	}
	public String hmacSha1(String value, String key) {
		try {
			// Get an hmac_sha1 key from the raw key bytes
			byte[] keyBytes = key.getBytes("UTF-8");
			SecretKeySpec signingKey = new SecretKeySpec(keyBytes, "HmacSHA1");

			// Get an hmac_sha1 Mac instance and initialize with the signing key
			Mac mac = Mac.getInstance("HmacSHA1");
			mac.init(signingKey);

			// Compute the hmac on input data bytes
			byte[] rawHmac = mac.doFinal(value.getBytes("UTF-8"));

			// Convert raw bytes to Hex
			byte[] hexBytes = new Hex().encode(rawHmac);
			// Covert array of Hex bytes to a String

			return new String(hexBytes, "UTF-8").toUpperCase();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	public String enquiry(String token, String senderCode, String amount, String partnerCode, String secretkey) {
		String stringToHash = token + senderCode + amount + partnerCode;
		return hmacSha1(stringToHash, secretkey);
	}

	public String confirm(String token, String partnerCode, String senderCode, String refNo, String amount,
			String secretkey) {
		String stringToHash = token + partnerCode + senderCode + refNo + amount;
		return hmacSha1(stringToHash, secretkey);
	}
	
	public static String sendMail(String toMail, String subject) {
		// boolean res = false;
		String result = "";
		final String username = "verify@nirvasoft.com";
		final String password = "mit@6562O!9";
		String host = "smtp.gmail.com";
		Properties props = new Properties();
		props.put("mail.smtp.auth", true);
		props.put("mail.smtp.starttls.enable", true);
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.ssl.trust", host);
		props.put("mail.smtp.ssl.trust", "*");
		
		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			@Override
			protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
				return new javax.mail.PasswordAuthentication(username, password);
			}
		});
		try {
			String body = subject;
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toMail));
			message.setSubject(subject);
			message.setContent(body, "text/html; charset=utf-8");

			Transport.send(message);
			// System.out.println("Done");
			// res = true;
			result = subject;
		} catch (MessagingException e) {
			e.printStackTrace();
			// System.out.println("Error :"+e);
		} finally {
			session = null;
		}
		return result;
	}

}
